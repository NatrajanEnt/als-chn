

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

 <%@ page import="java.text.DecimalFormat" %>
             <%@ page import="java.text.NumberFormat" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
          
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
      
        <script type="text/javascript">
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });
            $(function () {
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script type="text/javascript">
            function savePage(value) {
                document.trialBalance.action = '/throttle/trialBalanceNew.do?param=' + value;
                document.trialBalance.submit();
            }

            function loadLedgerDataPage(levelGroupId, value, fDate, tDate) {
                $("#slapRateHead").text(levelGroupId);
                var url = '/throttle/viewTBDetailsNew.do?levelId=' + levelGroupId + '&param=' + value + '&pageNo=' + 1 + '&button=search&fromDate=' + fDate + '&toDate=' + tDate;
                $('#slabRateListSet').html('');
                $.ajax({
                    url: url,
                    type: "get",
                    success: function (data)
                    {
                        $('#slabRateListSet').html(data);
                    }
                });
            }
        </script>
        
        <style>
    #index td {
   color:white;
}
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.TrialBalance"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.TrialBalance"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
        
    <body>
        <form name="trialBalance" method="post" action="" >
            

            <%                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date();
                String fDate = "";
                String tDate = "";
                if (request.getAttribute("fromDate") != null) {
                    fDate = (String) request.getAttribute("fromDate");
                } else {
                    fDate = "01-04-2016";
                }
                if (request.getAttribute("toDate") != null) {
                    tDate = (String) request.getAttribute("toDate");
                } else {
                    tDate = dateFormat.format(date);
                }

            %>
            <center>
                
                <h2><%=ThrottleConstants.companyName%></h2>
                <table class="table table-info mb30 table-hover">
                    <thead>
                         <tr >
                             <th colspan="4"  ><b><spring:message code="finance.label.TrialBalance"  text="default text"/></b></th>&nbsp;&nbsp;&nbsp;                    
                    </tr>
                    </thead>
                <!--<table>-->
                    <tr>
                        <td><spring:message code="finance.label.FromDate"  text="default text"/>:</td>
                        <td><input type="text"  autocomplete="off" class='datepicker' value="<%=fDate%>" name="fromDate" style="height:40px;width: 260px;"/></td>
                        <td><spring:message code="finance.label.ToDate"  text="default text"/>:</td>
                        <td><input type="text"  autocomplete="off" class='datepicker' value="<%=tDate%>" name="toDate" style="height:40px;width: 260px;"/></td>
                    </tr>
                </table>
                <input type="button" id="saveButton" class="btn btn-success" value="<spring:message code="finance.label.FetchData"  text="default text"/>" name="search" onclick="savePage(this.name);" style="width:100px;height:35px;"/>
                <input type="button" class="btn btn-success" onclick="savePage(this.name);" name="export" value="<spring:message code="finance.label.Export"  text="default text"/>" style="width:100px;height:35px;">
            </center>
            <br>
            <table class="table table-info mb30 table-hover">
                <tr id="index" height="30">
                    <td  align="left" width="60%" style="background-color:#5BC0DE;" scope="col"><b><spring:message code="finance.label.ACCOUNTHEAD"  text="default text"/></b></td>
                    <td  align="right" width="20%" style="background-color:#5BC0DE;" scope="col"><b><spring:message code="finance.label.DEBITS"  text="default text"/></b></td>
                    <td  align="right" width="20%" style="background-color:#5BC0DE;" scope="col"><b><spring:message code="finance.label.CREDITS"  text="default text"/></b></td>
                </tr>
                <c:set var="debitAmount" value="${0.00}"/>
                <c:set var="creditAmount" value="${0.00}"/>
                <c:if test="${levelMasterList != null}">
                    <c:forEach items="${levelMasterList}" var="level">

                        <tr height="30">
                            <td  class="text1" ><b style="color: green; text-align: center; font-weight: bold; font-size: medium;"><c:out value="${level.groupName}"/></b></td>
                                <c:if test="${groupLedgerList != null}">
                                    <c:forEach items="${groupLedgerList}" var="group">
                                        <c:if test="${level.groupID == group.groupCode}">
                                            <c:set var="debitAmount" value="${debitAmount + group.debitAmt}"/> 
                                            <c:set var="creditAmount" value="${debitAmount + group.creditAmt}"/> 
                                        <td   align="right"><b style="color: green; text-align: center; font-weight: bold; font-size: medium;"><fmt:formatNumber pattern="##0.00" value="${group.debitAmt}"/></b></td>
                                        <td   align="right"><b style="color: green; text-align: center; font-weight: bold; font-size: medium;"><fmt:formatNumber pattern="##0.00" value="${group.creditAmt}"/></b></td>  
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                        </tr>
                        <c:set var="groupID" value="${level.groupID}"/>
                        <c:set var="levelgroupId" value="${level.groupID}"/>
                        <c:set var="levelgroupName" value="${level.groupName}"/>

                        <c:forEach items="${level.childList}" var="level1">
                            <c:set var="groupID" value="${level.groupID}"/>
                            <c:set var="levelgroupId" value="${level1.levelgroupId}"/>
                            <tr height="30">
                                <td   >&nbsp;&nbsp;&nbsp;
                                    <a href="" style="color: #0464BB; text-align: center; font-weight: bold; font-size: small;"  data-toggle="modal" data-target="#myModal" onclick="loadLedgerDataPage('<c:out value="${level1.levelgroupId}"/>', 'search', '<%=fDate%>', '<%=tDate%>')"><c:out value="${level1.levelgroupName}"/></a>
                                </td>
                                <c:if test="${subPrimaryLedgerList != null}">
                                    <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                        <c:if test="${level1.levelgroupId == subList.levelID}">
                                           
                                            <td   align="right"><b style="color: #0464BB; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                            <td  align="right"><b style="color: #0464BB; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                            </tr>
                            <c:if test="${ledgerTransactionList != null}">
                                <c:forEach items="${ledgerTransactionList}" var="ledger">
                                    <c:if test="${level1.levelgroupId == ledger.levelID}">
                                        <tr>
                                            <td >&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                            <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.debitAmmount}"/></td>
                                            <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.creditAmmount}"/></td>

                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                            <c:forEach items="${level1.childList}" var="level2">
                                <c:set var="groupID" value="${level1.groupID}"/>
                                <c:set var="levelgroupId" value="${level2.levelgroupId}"/>
                                <tr height="30">
                                    <td   >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="" style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"  data-toggle="modal" data-target="#myModal" onclick="loadLedgerDataPage('<c:out value="${level2.levelgroupId}"/>', 'search', '<%=fDate%>', '<%=tDate%>')"><c:out value="${level2.levelgroupName}"/></a>
                                    </td>
                                    <c:if test="${subPrimaryLedgerList != null}">
                                        <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                            <c:if test="${level2.levelgroupId == subList.levelID}">
                                                <td   align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                                <td   align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                </tr>
                                <c:if test="${ledgerTransactionList != null}">
                                    <c:forEach items="${ledgerTransactionList}" var="ledger">
                                        <c:if test="${level2.levelgroupId == ledger.levelID}">
                                            <tr>
                                                <td >&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                                <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.debitAmmount}"/></td>
                                                <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.creditAmmount}"/></td>

                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                                <c:forEach items="${level2.childList}" var="level3">
                                    <c:set var="groupID" value="${level2.groupID}"/>
                                    <c:set var="levelgroupId" value="${level3.levelgroupId}"/>
                                    <tr height="30">
                                        <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a href="" style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;" data-toggle="modal" data-target="#myModal" onclick="loadLedgerDataPage('<c:out value="${level3.levelgroupId}"/>', 'search', '<%=fDate%>', '<%=tDate%>')"><c:out value="${level3.levelgroupName}"/></a>
                                        </td>
                                        <c:if test="${subPrimaryLedgerList != null}">
                                            <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                                <c:if test="${level3.levelgroupId == subList.levelID}">
                                                    <td   align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                                    <td   align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                    </tr>
                                    <c:if test="${ledgerTransactionList != null}">
                                        <c:forEach items="${ledgerTransactionList}" var="ledger">
                                            <c:if test="${level3.levelgroupId == ledger.levelID}">
                                                <tr>
                                                    <td >&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                                    <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.debitAmmount}"/></td>
                                                    <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.creditAmmount}"/></td>

                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                    <c:forEach items="${level3.childList}" var="level4">
                                        <c:set var="groupID" value="${level3.groupID}"/>
                                        <c:set var="levelgroupId" value="${level4.levelgroupId}"/>
                                        <tr height="30">
                                            <td   >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <a href="" style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;" data-toggle="modal" data-target="#myModal" onclick="loadLedgerDataPage('<c:out value="${level4.levelgroupId}"/>', 'search', '<%=fDate%>', '<%=tDate%>')"><c:out value="${level4.levelgroupName}"/></a>
                                            </td>
                                            <c:if test="${subPrimaryLedgerList != null}">
                                                <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                                    <c:if test="${level4.levelgroupId == subList.levelID}">
                                                        <td   align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                                        <td   align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                        </tr>
                                        <c:if test="${ledgerTransactionList != null}">
                                            <c:forEach items="${ledgerTransactionList}" var="ledger">
                                                <c:if test="${level4.levelgroupId == ledger.levelID}">
                                                    <tr>
                                                        <td >&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                                        <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.debitAmmount}"/></td>
                                                        <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.creditAmmount}"/></td>

                                                    </tr>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                        <c:forEach items="${level4.childList}" var="level5">
                                            <c:set var="groupID" value="${level4.groupID}"/>
                                            <c:set var="levelgroupId" value="${level5.levelgroupId}"/>
                                            <tr height="30">
                                                <td   >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <a href="" style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;" data-toggle="modal" data-target="#myModal" onclick="loadLedgerDataPage('<c:out value="${level5.levelgroupId}"/>', 'search', '<%=fDate%>', '<%=tDate%>')"><c:out value="${level5.levelgroupName}"/></a>
                                                </td>
                                                <c:if test="${subPrimaryLedgerList != null}">
                                                    <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                                        <c:if test="${level5.levelgroupId == subList.levelID}">
                                                            <td   align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                                            <td   align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: small;"><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                            </tr>
                                            <c:if test="${ledgerTransactionList != null}">
                                                <c:forEach items="${ledgerTransactionList}" var="ledger">
                                                    <c:if test="${level5.levelgroupId == ledger.levelID}">
                                                        <tr>
                                                            <td >&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                                            <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.debitAmmount}"/></td>
                                                            <td  align ="right">&nbsp;&nbsp;&nbsp;<c:out value="${ledger.creditAmmount}"/></td>

                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>


                                        </c:forEach>

                                    </c:forEach>

                                </c:forEach>

                            </c:forEach>

                        </c:forEach>

                    </c:forEach>
                </c:if>
                <tr> 
                    <td align="right"><b style="color: green; text-align:right; font-weight: bold; font-size: medium;"><spring:message code="finance.label.Total"  text="default text"/></b></td>
                    <td    align="right"><b style="color: green; text-align: right; font-weight: bold; font-size: medium;"><fmt:formatNumber pattern="##0.00" value="${debitAmount}"/></b></td>
                    <td    align="right"><b style="color: green; text-align: right; font-weight: bold; font-size: medium;"><fmt:formatNumber pattern="##0.00" value="${creditAmount}"/></b></td>
                </tr> 
            </table>
            <br>
            <div class="modal fade" id="myModal" role="dialog" style="width: 100%;height: 100%">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()">&<spring:message code="finance.label.times"  text="default text"/>;</button>
                            <h4 class="modal-title"><spring:message code="finance.label.TrialBalance"  text="default text"/></h4>
                        </div>
                        <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 100%">

                        </div>
                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>
            <br>

            <script>
                function resetTheSlabDetails() {
                    $('#slabRateListSet').html('');
                }

            </script> 
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
 </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
