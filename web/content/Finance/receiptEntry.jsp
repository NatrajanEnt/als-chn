<%--  
    Document   : receiptEntry
    Created on : Mar 21, 2013, 10:50:00 PM
    Author     : Entitle
--%>
<%@ page import="ets.domain.util.ThrottleConstants" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script> 
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                var date2 = new Date();
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy'
                }).datepicker('setDate', date2)

            });
        </script>

    </head>
    <script language="javascript">
        function checkLedger(sno)
        {
            var ledgerId = document.getElementById("ledgerId").value;
            var Lid = document.getElementById("bankId" + sno).value;
            var temp = Lid.split("~");
            if (temp[0] == ledgerId) {
                alert("Select Other then Karikali cash")
                document.getElementById("bankId" + sno).value = 0;
            }
        }
//        window.onload = function ()
//        {
//            var currentDate = new Date();
//            var day = currentDate.getDate();
//            var month = currentDate.getMonth() + 1;
//            var year = currentDate.getFullYear();
//            var myDate = day + "-" + month + "-" + year;
//            document.getElementById("date").value = myDate;
//        }


        function sumReceiptAmt() {
            var sumAmt = 0;
            var totAmt = 0;
            sumAmt = document.getElementsByName('amount');
            for (i = 0; i < sumAmt.length; i++) {
                if (sumAmt[i].value != "") {
                    totAmt += parseFloat(sumAmt[i].value);
//                totAmt += parseInt(sumAmt[i].value);
                    document.getElementById('totalDebitAmt').value = parseFloat(totAmt).toFixed(2);
//                document.getElementById('totalDebitAmt').value=parseInt(totAmt);
                } else {
                    document.getElementById('totalDebitAmt').value = 0;
                }
            }
        }

        function bankIdVal()
        {
            var bankid1 = document.getElementsByName("bankid1");
            var bankid2 = document.getElementsByName("bankid2");
            //            alert("bankid1=== "+textValidation(bankid1[i]);
            // alert("bankid2=== "+bankid2);
        }
        function setSelectbox(i)
        {
            var selected = document.getElementsByName("selectedIndex");
            alert("selected==" + selected[i]);
            selected[i].checked = 1;

        }

        function setCheckBox(val) {
            // alert(val);
            // alert("in set cb");
            document.getElementById("selectedIndex" + val).checked = true;

        }

        function submitPage(value) {
            if (value == "Save") {
                if (document.getElementById('fromDate').value == "") {
                    alert("please select cash payment date");
                    document.getElementById('fromDate').focus();
                 } else if (selectedItemValidation() == 'SubmitForm') {
                    //validattion
                        document.manufacturer.action = '/throttle/insertReceiptEntry.do';
                        document.manufacturer.submit();
                }
            }
        }
         function selectedItemValidation() {
            var bankId = document.getElementsByName("bankId");
            var amount = document.getElementsByName("amount");
            var narration = document.getElementsByName("narration");
            var chec = 0;
            var index = 0;
            var mess = "SubmitForm";
            for (var i = 0; i < amount.length; i++) {
                index = parseInt(i) + parseInt(1);
                chec++;
                if (bankId[i].value == 0) {
                    alert("select Ledger Name for row " + index);
                    mess = 'NotSubmit';
                } else if (amount[i].value == "") {
                    alert("select amount for row " + index);
                    mess = 'NotSubmit';
                } else if (narration[i].value == "") {
                    alert("Give narration for row " + index);
                    mess = 'NotSubmit';
                } else {
                    mess = 'SubmitForm';

                }
            }
            if (mess == 'SubmitForm') {
                if (chec == 0) {
                    alert("Please click ADDROW And Then Proceed");
                    mess = 'NotSubmit';
                } else {
                    mess = 'SubmitForm';
                }
            }
            return mess;
        }

        function setValues(sno, accountEntryIds, accountsAmount, debitAmount, voucherCode, accountEntryDate, headerNarration, fullName, voucherId) {
            var temp = accountEntryIds.split(",");
            //alert(temp.length);
            var tempOne = fullName.split(",");
            var tempTwo = accountsAmount.split(",");
            //var temp3 = headerNarration.split(",");
            var tempThree = document.getElementById('entryNarration' + sno).value.split("@");
            var index = 0;
            DeleteRow();
            var chechkbox = document.getElementsByName("edit");
            for (i = 0; i < chechkbox.length; i++) {
                document.getElementById('edit' + i).checked = false;
            }
            document.getElementById('edit' + sno).checked = true;
            if (document.getElementById('edit' + sno).checked == true) {
                for (var i = 0; i < temp.length; i++) {
                    index = parseInt(i) + parseInt(1);
                    showRow();
                    document.getElementById('amount' + index).value = tempTwo[i];
                    document.getElementById('bankId' + index).value = tempOne[i];
                    document.getElementById('narration' + index).innerHTML = tempThree[i];
                    document.getElementById('selectedIndex' + index).checked = true;
                }
                document.getElementById('totalDebitAmt').value = debitAmount;
                document.getElementById('fromDate').value = accountEntryDate;
                document.getElementById('voucherCode').value = voucherCode;
                document.getElementById('voucherIdEdit').value = voucherId;

            }
        }

        var rowCount = 1;
        var sno = 0;
        var style = "text2";
        function showRow()
        {
            if (rowCount % 2 == 0) {
                style = "text2";
            } else {
                style = "text1";
            }
            sno++;
            var tab = document.getElementById("addRow");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell1 = "<td class='text1' height='25' >" + sno + "</td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(1);
//            var cell2 = "<td class='text1' height='30'><select class='form-control' id='bankId"+sno+"' onChange='checkLedger("+sno+");' style='width:225px'  name='bankid1'><option selected value=0>---Select---</option><c:if test = "${receiptLedgerList != null}" ><c:forEach items="${receiptLedgerList}" var="JLL"><option  value='<c:out value="${JLL.ledgerID}" />~<c:out value="${JLL.groupCode}" />~<c:out value="${JLL.levelID}" />~<c:out value="${JLL.ledgerCode}" />'><c:out value="${JLL.ledgerName}" /></option></c:forEach ></c:if></select></td>";
            var cell2 = "<td class='text1' height='30'><select class='form-control' id='bankId" + sno + "' onChange='setCheckBox(" + sno + ");' style='width:225px'  name='bankid1'><option selected value=0>---Select---</option><c:if test = "${receiptLedgerList != null}" ><c:forEach items="${receiptLedgerList}" var="JLL"><c:if test = "${JLL.ledgerID != 2}" ><option  value='<c:out value="${JLL.ledgerID}" />~<c:out value="${JLL.groupCode}" />~<c:out value="${JLL.levelID}" />~<c:out value="${JLL.ledgerCode}" />'><c:out value="${JLL.ledgerName}" /></option></c:if></c:forEach ></c:if></select></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell2;

            cell = newrow.insertCell(2);
//            var cell3 = "<td class='text1' height='30'><input type='text' name='amount' id='amount"+sno+"' maxlength='13' onkeyup='sumReceiptAmt();' onclick='bankIdVal(sno-1)' size='20' class='form-control' onkeypress='return onKeyPressBlockCharacters(event);'  /></td>";
            var cell3 = "<td class='text1' height='30'><input type='text' name='amount' id='amount" + sno + "' maxlength='13' onkeyup='sumReceiptAmt();' onclick='bankIdVal(sno-1)'  size='20' class='form-control'   /></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell3;

            cell = newrow.insertCell(3);
//            var cell4 = "<td height='30' class='tex1'> <div align='center'><select name='accType' id='accType"+sno+"' class='form-control'><option value='CREDIT'>CREDIT</option></select></div> </td>";
            var cell4 = "<td height='30' class='tex1'> <div align='center'><select name='accType' id='accType" + sno + "'  class='form-control'><option value='CREDIT'>CREDIT</option></select></div> </td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell4;

            cell = newrow.insertCell(4);
//            var cell5 = "<td class='text1' height='30'><input type='text' name='narration' id='narration"+sno+"'  size='20' class='form-control' /></td>";
            var cell5 = "<td class='text1' height='30'><textarea name='narration' id='narration" + sno + "'  size='20' class='form-control' /></textarea></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell5;

            cell = newrow.insertCell(5);
//            var cell6 = "<td class='text1' height='30'><input type='checkbox'  name='selectedIndex' value='"+sno+"'/></td>";
            var cell6 = "<td class='text1' height='30'><input type='checkbox'  id='selectedIndex" + sno + "' name='selectedIndex' value='" + sno + "'/></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell6;

            //            cell = newrow.insertCell(5);
            //            var cell6 = "<td alain='left' >3tretrt<input type='checkbox' name='deleteItem' value='"+snumber+"'/> </td>";
            //            cell.setAttribute("className",style);
            //            cell.innerHTML = cell6;

            var temp = sno - 1;

            rowCount++;
            // document.getElementById('AddRow').style.display = 'none';
        }


        function DeleteRow() {
            document.getElementById('AddRow').style.display = 'block';
            try {
                var table = document.getElementById("addRow");
                rowCount = table.rows.length;
                for (var i = 1; i < rowCount; i++) {
                    var row = table.rows[i];
                    var checkbox = row.cells[5].childNodes[0];
                    if (null != checkbox && true == checkbox.checked) {
                        if (rowCount <= 1) {
                            alert("Cannot delete all the rows");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                        sno--;
                        // snumber--;
                    }
                }
                sumReceiptAmt();
            } catch (e) {
                alert(e);
            }
        }


        function onKeyPressBlockCharacters(e)
        {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /[a-zA-Z]+$/;

            return !reg.test(keychar);

        }

        function printCashReceiptEntry(val, voucherNo) {
            document.manufacturer.action = '/throttle/printCashReceiptEntry.do?voucherCode=' + val + '&voucherNo=' + voucherNo;
            document.manufacturer.submit();
        }


        function viewCostCenterCashPayment(accountEntryIds, debitAmount, creditAmount, voucherCode, creditLedgerName, debitLedgerName, Credit, Debit) {
            document.manufacturer.action = '/throttle/viewCostCenterCashPayment.do?accountEntryIds=' + accountEntryIds + '&debitAmount=' + debitAmount + '&creditAmount=' + creditAmount + '&Entry=Cash Receipt&voucherCode=' + voucherCode + '&creditLedgerName=' + creditLedgerName + '&debitLedgerName=' + debitLedgerName + '&Credit=' + Credit + '&Debit=' + Debit;
            document.manufacturer.submit();
        }


        function costCenterCashPaymentEditView(accountEntryIds, debitAmount, creditAmount, voucherCode, val, creditLedgerName, debitLedgerName) {
            document.manufacturer.action = '/throttle/costCenterCashPaymentEditView.do?accountEntryIds=' + accountEntryIds + '&debitAmount=' + debitAmount + '&creditAmount=' + creditAmount + '&Entry=Cash Receipt&voucherCode=' + voucherCode + '&Param=' + val + '&creditLedgerName=' + creditLedgerName + '&debitLedgerName=' + debitLedgerName;
            document.manufacturer.submit();
        }



                </script>
                <body onload="showRow();">
                    <form name="manufacturer" method="post" >
                        <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                            <tr>
                                <td >
                        <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
            <!-- pointer table -->
            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp"%>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <input type="hidden" id="voucherCode" name="voucherCode" size="20" class="form-control" value="">
            <input type="hidden" id="voucherIdEdit" name="voucherIdEdit" size="20" class="form-control" value="">
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="780" id="bg" class="border">
                <tr>
                    <td colspan="6" class="contenthead" height="30">
                        <div class="contenthead" align="center">Receipt Entry Date</div></td>
                </tr>
                <tr>
                    <td class="texttitle1" height="30">Date </td>
                    <td class="text1" height="30">
                        <input type="textbox" id="fromDate" name="fromDate" value=""  size="20" class="datepicker">
                    </td>
                    <td class="texttitle1" height="30">Cash Head</td>
                    <td class="text1" height="30">
                        <input type="hidden" id="ledgerId" name="ledgerId"  size="20" class="form-control" readonly value="<%=ThrottleConstants.CashOnHandLedgerId%>">
                        <input type="text" id="cashHeadName" name="cashHeadName" value="Karikali Cash"  size="20" class="form-control" readonly>
                    </td>
                    <td class="texttitle1" height="30">
                        <input type="text" id="totalDebitAmt" name="totalDebitAmt" value=""  size="20" class="form-control" readonly>
                        DEBIT
                    </td>
                </tr>

            </table>
            <p>&nbsp;</p>
            <div align="center">
                <table width="780" class="border" border="3" id="addRow">
                    <tr class="contentsub">
                        <td class="contentsub" ><div class="contentsub">Sno</div></td>
                        <td class="contentsub" ><div class="contentsub">Ledger Name</div></td>
                        <td class="contentsub" ><div class="contentsub">Amount</div></td>
                        <td class="contentsub" ><div class="contentsub">Account Type</div></td>
                        <td class="contentsub" ><div class="contentsub">Narration</div></td>
                        <td class="contentsub" ><div class="contentsub">Select</div></td>
                    </tr>
                </table>
            </div>
            <p><br>
            </p>
            <table align="center" width="100%" border="0">
                <tr >
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right"><input type="button" id="AddRow" value="AddRow" class="button" onClick="showRow();"></td>
                    <td colspan="2" align="left"> <input type="button" id="DeleteRow1" value="DeleteRow" class="button" onClick="DeleteRow();">
                        <input type="button" value="Save" class="button" onClick="submitPage(this.value);"></td>
                </tr>
            </table>
            <!--  <center>
                  <input type="button" value="AddRow" class="button" onClick="showRow();">&nbsp;&nbsp;
                  <input type="button" value="DeleteRow" class="button" onClick="DeleteRow();">&nbsp;&nbsp;
                  <input type="button" value="Save" class="button" onClick="submitPage(this.value);">
              </center>
              <br>-->

            <c:if test = "${cashReceiptList != null}" >

                <table align="center" width="100%" border="0" id="table" class="sortable">
                    <thead>
                        <tr>
                            <th><h3>S.No</h3></th>
                    <th><h3>Entry Date</h3></th>
                    <th><h3>Voucher Code</h3></th>
                    <th><h3>Debit Ledger</h3></th>
                    <th><h3>Debit Amount</h3></th>
                    <th><h3>Credit Ledger</h3></th>
                    <th><h3>Credit Amount</h3></th>
                    <th><h3>Narration</h3></th>
                    <th><h3>Print</h3></th>
                    <th><h3>Edit</h3></th>
                    <th><h3>Cost Center</h3></th>
                    </tr>
                    </thead>
                    <tbody>
                        <% int index = 0;%>
                        <c:forEach items="${cashReceiptList}" var="CRL">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                            <tr>
                                <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${CRL.accountEntryDate}"/> </td>
                                <td class="<%=classText%>" align="left"> 
                                    <%--<c:out value="${CRL.voucherCode}" />--%>
                                    <c:out value="${CRL.voucherNo}" />
                                </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${CRL.debitLedgerName}"/></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${CRL.debitAmount}"/></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${CRL.creditLedgerName}"/></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${CRL.creditAmount}"/></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${CRL.headerNarration}"/></td>
                                <td class="<%=classText%>"  align="left"><a href="" onclick="printCashReceiptEntry('<c:out value="${CRL.voucherCode}" />', '<c:out value="${CRL.voucherNo}" />')">Print</a></td>
                                <td class="<%=classText%>" align="center"> 
                                    <input type="checkbox" name="edit" align="center" id="edit<%=index%>" onclick="setValues('<%=index%>', '<c:out value="${CRL.accountEntryIds}" />', '<c:out value="${CRL.accountsAmount}"/>', '<c:out value="${CRL.debitAmount}"/>', '<c:out value="${CRL.voucherCode}" />', ' <c:out value="${CRL.accountEntryDate}"/> ', '0', '<c:out value="${CRL.fullName}"/>', '<c:out value="${CRL.voucherId}"/>');" />
                                    <input type="hidden" id="entryNarration<%=index%>" name="entryNarration"  value="<c:out value="${CRL.entryNarration}"/>">
                                </td>
                                <td class="<%=classText%>"  align="left">
                                    <c:if test="${CRL.creditCount > 0 || CRL.debitCount > 0}" >
                                        <c:if test="${CRL.status == 0}" >
                                            <a href="" onclick="viewCostCenterCashPayment('<c:out value="${CRL.accountEntryIds}" />', '<c:out value="${CRL.debitAmount}"/>', '<c:out value="${CRL.creditAmount}"/>', '<c:out value="${CRL.voucherCode}" />', '<c:out value="${CRL.creditLedgerName}"/>', '<c:out value="${CRL.debitLedgerName}"/>', '<c:out value="${CRL.creditCount}"/>', '<c:out value="${CRL.debitCount}"/>')">Create</a>
                                        </c:if>

                                        <c:if test="${CRL.status == 1}" >
                                            <a href="" onclick="costCenterCashPaymentEditView('<c:out value="${CRL.accountEntryIds}" />', '<c:out value="${CRL.debitAmount}"/>', '<c:out value="${CRL.creditAmount}"/>', '<c:out value="${CRL.voucherCode}" />', 'View', '<c:out value="${CRL.creditLedgerName}"/>', '<c:out value="${CRL.debitLedgerName}"/>')">view</a>&nbsp;/
                                            <a href="" onclick="costCenterCashPaymentEditView('<c:out value="${CRL.accountEntryIds}" />', '<c:out value="${CRL.debitAmount}"/>', '<c:out value="${CRL.creditAmount}"/>', '<c:out value="${CRL.voucherCode}" />', 'Edit', '<c:out value="${CRL.creditLedgerName}"/>', '<c:out value="${CRL.debitLedgerName}"/>')">Edit</a>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${CRL.creditCount == 0 && CRL.debitCount == 0}" >
                                        &nbsp;
                                    </c:if>
                                </td>
                            </tr>

                            <% index++;%>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    </body>
</html>
