<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ page import="ets.domain.operation.business.OperationTO" %>
<%@ page import="java.util.*" %>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i>  <spring:message code="fuel.label.Ageing"  text="default text"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fuel.label.Fuel"  text="default text"/></a></li>
            <li class="active"><spring:message code="fuel.label.Ageing"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            </head>
            <body >
                <% String menuPath = "Milleage  >>  Mfr Milleage";
               request.setAttribute("menuPath", menuPath);
                %>
                <form name="mfrAgeing"  method="post" >
                    <!--            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                                    <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                                         pointer table 
                                        <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                                            <tr>
                                                <td >
                    <%@ include file="/content/common/path.jsp" %>
                </td></tr></table>-->
                    <!-- pointer table -->
                    <!--
                                    </div>
                                </div>-->
                    <!--            <br>
                                <br>
                                <br>-->
                    <br>
                    <br>
                    <table class="table table-info mb30 table-hover" id="bg" >
                        <thead>
                            <tr align="center">
                                <th colspan="4" ><spring:message code="fuel.label.ManufacturerAgeing"  text="default text"/></th>
                            </tr>
                        </thead>

                        <tr>
                            <td  height="30"><font color="red">*</font><spring:message code="fuel.label.Manufacture"  text="default text"/></td>
                            <td><select class="form-control" style="width:260px;height:40px;" name="mfrId" id="mfrId" onchange="viewMfrMilleage(this.value)">
                                    <option value="0">--<spring:message code="fuel.label.Select"  text="default text"/>--</option>
                                    <c:if test="${MfrList != null}">
                                        <c:forEach items="${MfrList}" var="mfr">
                                            <option value='<c:out value="${mfr.mfrId}"/>'><c:out value="${mfr.mfrName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td  height="30"><spring:message code="fuel.label.Status"  text="default text"/></td>
                            <td><select class="form-control" style="width:260px;height:40px;" name="mfrId" id="mfrId" >
                                    <option value="Y"><spring:message code="fuel.label.Active"  text="default text"/></option>
                                    <option value="N"><spring:message code="fuel.label.InActive"  text="default text"/></option>
                                </select>
                            </td>
                            <!--                    <td >Status</td>
                                                <td  height="30">
                                                    <select class="form-control" style="width:260px;height:40px;" name="status" id="status">
                                                        <option value="Y">Active</option>
                                                        <option value="N">In-Active</option>
                                                    </select>
                                                </td>-->

                        </tr>
                        <tr>
                            <td  height="30"><font color="red">*</font><spring:message code="fuel.label.FromYear"  text="default text"/></td>
                            <td  height="30">
                                <input type="text" class="form-control" style="width:260px;height:40px;"  name="ageing" id="ageing" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                            </td>
                            <td  height="30"><font color="red">*</font><spring:message code="fuel.label.ToYear"  text="default text"/></td>
                            <td  height="30">
                                <input type="text" class="form-control" style="width:260px;height:40px;" name="toAgeing" id="toAgeing" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                            </td>
                        </tr>
                        <tr>
                            <td  height="30" colspan="4" align="center">
                                <input align="center" type="button" class="btn btn-success" value="<spring:message code="fuel.label.Save"  text="default text"/>"  onClick="updateAge();">
                                <input type="hidden" id="ageingId" value=""  >
                            </td>
                        </tr>
                    </table>

                    <br>
                    <br>
                    <div id="mfrMilleageData"></div>
                    <script type="text/javascript">
                        function viewMfrMilleage(val) {
                            if (val != 0) {
                                $.ajax({
                                    url: './viewMfrMilleageAgeing.do',
                                    data: {mfrId: val},
                                    type: "GET",
                                    success: function(response) {
                                        $('#mfrMilleageData').html(response);
                                    },
                                    error: function(xhr, status, error) {
                                        console(xhr.responseText);
                                    }
                                });
                            }
                        }

                        function updateAge() {
                            if (document.getElementById('mfrId').value == '0') {
                                alert("Please select manufacturer");
                                document.getElementById('mfrId').focus();
                            } else if (document.getElementById('ageing').value == '') {
                                alert("Please enter mfr from Year");
                                document.getElementById('ageing').focus();
                            } else if (document.getElementById('toAgeing').value == '') {
                                alert("Please enter mfr To Year");
                                document.getElementById('toAgeing').focus();
                            } else {
                                var ageingId = $("#ageingId").val();
                                var mfrId = $("#mfrId").val();
                                var mfrAge = $("#ageing").val();
                                var mfrToAge = $("#toAgeing").val();
                                var status = $("#status").val();
                                var url = './saveMfrMilleageAgeing.do';
                                $.ajax({
                                    url: url,
                                    data: {mfrId: mfrId, mfrAge: mfrAge, mfrToAge: mfrToAge, status: status, ageingId: ageingId
                                    },
                                    type: "GET",
                                    success: function(response) {
                                        if (response.toString().trim() == 1 || response.toString().trim() == 2) {
                                            viewMfrMilleage(mfrId);
                                            document.getElementById('ageingId').value = '';
                                            document.getElementById('ageing').value = '';
                                            document.getElementById('toAgeing').value = '';
                                            document.getElementById('status').value = 'Y';
                                        } else {
                                            alert("Mfr age not update sucessfully");
                                        }
                                    },
                                    error: function(xhr, status, error) {
                                    }
                                });

                            }
                        }

                        function setDetails(ageingId, mfrId, mfrAge, mfrToAge, status) {
                            document.getElementById('ageingId').value = ageingId;
                            document.getElementById('mfrId').value = mfrId;
                            document.getElementById('ageing').value = mfrAge;
                            document.getElementById('toAgeing').value = mfrToAge;
                            document.getElementById('status').value = status;
                        }



                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

