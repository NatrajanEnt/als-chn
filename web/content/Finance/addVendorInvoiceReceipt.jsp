<%-- 
    Document   : addVendorInvoiceReceipt
    Created on : 30 Jan, 2017, 11:55:58 AM
    Author     : pavithra
--%>


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {

        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript">
    var httpReq;
    function setVendor(vendorTypeid) { //alert(str);
        var temp = "";
        $.ajax({
            url: "/throttle/vendorPaymentsJson.do",
            dataType: "json",
            data: {
                vendorType: vendorTypeid
            },
            success: function(temp) {
                if (temp != '') {
                    $('#vendorName').empty();
                    $('#vendorName').append(
                            $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                            )
                    $.each(temp, function(i, data) {
                        $('#vendorName').append(
                                $('<option value="' + data.vendorId + "~" + data.ledgerId + '" style="width:150px"></option>').val(data.vendorId + "~" + data.ledgerId).html(data.vendorName)
                                )
                    });
                } else {
                    $('#vendorName').empty();
                }
            }
        });

    }
</script>


<script type="text/javascript">
    var httpReq;
    function getLedgerList(levelId) { //alert(str);
        var temp = "";
//                                alert(levelId);
        $.ajax({
            url: "/throttle/getLedgerListforGroup.do",
            dataType: "json",
            data: {
                levelId: levelId
            },
            success: function(temp) {
//                                         alert(temp);
                if (temp != '') {
                    $('#debitLedgerId').empty();
                    $('#debitLedgerId').append(
                            $('<option></option>').val(0).html('---Select----')
                            )
                    $.each(temp, function(i, data) {
                        $('#debitLedgerId').append(
                                $('<option value="' + data.ledgerID + '" ></option>').val(data.ledgerID).html(data.ledgerName)
                                )
                    });
                } else {
                    $('#debitLedgerId').empty();
                }
            }
        });

    }
</script>
<script type="text/javascript">
    function searchPage(value) {
        var contractTypeId = document.getElementById("contractTypeId").value;
        var vendorTypeId = document.getElementById("vendorType").value;
        var vendorNameId = document.getElementById("vendorName").value;
        var invoiceSubmissionDate = document.getElementById("invoiceSubmissionDate").value;
        var invoiceDate = document.getElementById("invoiceDate").value;
        if (vendorTypeId == null || vendorTypeId == "" || vendorTypeId == "0") {
            alert('Please Select Vendor Type');
        } else if (contractTypeId == "0") {
            alert('Please Select Contract Type');
        } else if (vendorNameId == null || vendorNameId == "" || vendorNameId == "0~0") {
            alert('Please Select Vendor');
        } else if (invoiceSubmissionDate == null || invoiceSubmissionDate == "") {
            alert('Please Select Invoice Date');
        } else {
            document.payment.action = '/throttle/addVendorInvoiceReceipt.do?param=' + value;
            document.payment.submit();
        }
    }


    function submitPage(value)
    {

        var count = document.getElementById("count").value;
        var contractTypeId = document.getElementById("contractTypeId").value;
        var invoiceAmount = document.getElementById("invoiceAmount").value;
          var vendorNameId = document.getElementById("vendorName").value;
        var invoiceDate = document.getElementById("invoiceDate").value;
        var invoiceNo = document.getElementById("invoiceNo").value;

        var form = document.getElementById("payment");
        if (contractTypeId == 2) {
            var totalAmount = document.getElementById("totalAmtHire").value;
            if (parseFloat(invoiceAmount).toFixed(2) == parseFloat(totalAmount).toFixed(2)) {
                if (vendorNameId == null || vendorNameId == "" || vendorNameId == "0~0") {
                        alert('Please Select Vendor');
                    } else if (invoiceDate == null || invoiceDate == "") {
                        alert('Please Select Invoice Date');
                    } else if (invoiceNo == null || invoiceNo == "") {
                        alert('Please Select Invoice No');
                    } else {
                        form.setAttribute("enctype", "multipart/form-data");
                        document.payment.action = '/throttle/insertVendorInvoiceReceipt.do?count=' + count;
                        document.payment.submit();
                    }
                } else {
                    alert("Invoice Amount doesn't match");
                }
            } else if (contractTypeId == 1) {
                var totalAmount = document.getElementById("totalAmtDedicate").value;
                if (parseFloat(invoiceAmount).toFixed(2) == parseFloat(totalAmount).toFixed(2)) {
                    form.setAttribute("enctype", "multipart/form-data");
                    document.payment.action = '/throttle/insertVendorInvoiceReceiptDedicate.do?count=' + count;
                    document.payment.submit();
                } else {
                    alert("Invoice Amount doesn't match");
                }
            }
        }

    function setActiveIndHire(sno) {
        if (document.getElementById("selectedIndexHire" + sno).checked) {
            document.getElementById("activeIndHire" + sno).value = 'Y';
//            alert("SASASA=="+document.getElementById("totAmttHire" + sno).value);
            document.getElementById("hireCashAdv" + sno).value = document.getElementById("hireCashAdvs" + sno).value;
            document.getElementById("fuelAmount" + sno).value = document.getElementById("fuelAmounts" + sno).value;
            document.getElementById("contInsAmt" + sno).value = document.getElementById("contInsAmts" + sno).value;
            document.getElementById("vendorHaltingCharge" + sno).value = document.getElementById("vendorHaltingCharges" + sno).value;
            document.getElementById("otherCharge" + sno).value = document.getElementById("otherCharges" + sno).value;
            document.getElementById("totAmttHire" + sno).value = document.getElementById("totAmtHire" + sno).value;
//            document.getElementById("totAmttHire" + sno).value = + document.getElementById("totAmtHire" + sno).value + + document.getElementById("otherExpensesHire" + sno).value;
//            alert("SSSSSSS=="+document.getElementById("totAmttHire" + sno).value);
        } else {
            document.getElementById("activeIndHire" + sno).value = 'N';
            document.getElementById("hireCashAdv" + sno).value = '';
            document.getElementById("fuelAmount" + sno).value = '';
            document.getElementById("contInsAmt" + sno).value = '';
            document.getElementById("vendorHaltingCharge" + sno).value = '';
            document.getElementById("otherCharge" + sno).value = '';
            document.getElementById("totAmttHire" + sno).value = '';
        }
        setValueHire();
    }
    function setValueHire() {

        var temp = 0;
        var temp1 = 0;
        var temp2 = 0;
        var temp3 = 0;
        var temp4 = 0;
        var temp5 = 0;
        var cntr = 0;
        var fuelAmount = document.getElementsByName('fuelAmount');
        var hireCashAdv = document.getElementsByName('hireCashAdv');
        var contInsAmt = document.getElementsByName('contInsAmt');
        var haltingCharge = document.getElementsByName('vendorHaltingCharge');
        var otherCharge = document.getElementsByName('otherCharges');
        var totAmtt = document.getElementsByName('totAmttHire');
        var activeInd = document.getElementsByName('activeIndHire');
        for (var i = 0; i < totAmtt.length; i++) {
            if (activeInd[i].value == 'Y') {
                if (cntr == 0) {
                    temp = totAmtt[i].value;
                    temp1 = hireCashAdv[i].value;
                    temp2 = fuelAmount[i].value;
                    temp3 = contInsAmt[i].value;
                    temp4 = haltingCharge[i].value;
                    temp5 = otherCharge[i].value;
                } else {
                    temp = +temp + +totAmtt[i].value;
                    temp1 = +temp1 + +hireCashAdv[i].value;
                    temp2 = +temp2 + +fuelAmount[i].value;
                    temp3 = +temp3 + +contInsAmt[i].value;
                    temp4 = +temp4 + +haltingCharge[i].value;
                    temp5 = +temp5 + +otherCharge[i].value;
                }
                cntr++;


            }

        }
        document.getElementById("totalAmtHire").value = (parseFloat(temp)+parseFloat(temp4)+parseFloat(temp5)).toFixed(2);
        document.getElementById("totalHireCashAdv").value = parseFloat(temp1).toFixed(2);
        document.getElementById("totalFuelAmt").value = parseFloat(temp2).toFixed(2);
        document.getElementById("totalContInsAmt").value = parseFloat(temp3).toFixed(2);
        document.getElementById("totalHaltingCharge").value = parseFloat(temp4).toFixed(2);
        document.getElementById("totalOtherCharge").value = parseFloat(temp5).toFixed(2);
        document.getElementById("totalAmountHire").innerHTML = "Total Freight :&emsp;" +(parseFloat(temp)+parseFloat(temp4)+parseFloat(temp5)).toFixed(2);
    }
    function setActiveIndDedicate(sno) {
        if (document.getElementById("selectedIndexDedicate" + sno).checked) {
            document.getElementById("activeIndDedicate" + sno).value = 'Y';
            document.getElementById("totAmttDedicate" + sno).value = document.getElementById("totAmtDedicate" + sno).value;

        } else {
            document.getElementById("activeIndDedicate" + sno).value = 'N';
            document.getElementById("totAmttDedicate" + sno).value = '';
        }
        setValueDedicate();
    }
    function setValueDedicate() {

        var temp = 0;
        var temp1 = 0;
        var cntr = 0;
        var totAmtt = document.getElementsByName('totAmttDedicate');
        var activeInd = document.getElementsByName('activeIndDedicate');
        for (var i = 0; i < totAmtt.length; i++) {
            if (activeInd[i].value == 'Y') {
                if (cntr == 0) {
                    temp = totAmtt[i].value;
                } else {
                    temp = +temp + +totAmtt[i].value;
                }
                cntr++;


            }

        }
        document.getElementById("totalAmtDedicate").value = parseFloat(temp).toFixed(2);
        document.getElementById("totalAmountDedicate").innerHTML = '';
        document.getElementById("totalAmountDedicate").innerHTML = parseFloat(temp).toFixed(2);
    }

</script>
<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Add Vendor Invoice Receipt</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active">Add Vendor Invoice Receipt</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="payment" id="payment" class="form-horizontal form-bordered" method="post">
                     <%@ include file="/content/common/message.jsp" %>
                    <input type="hidden" name="virCode" id="virCode" value="<%=ThrottleConstants.vendorInvoiceReceipt+ThrottleConstants.financialYear+"-"%>"/>
                    <input type="hidden" name="virId" id="virId" value="0"/>

                    <font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                    <div align="center" id="status1">&nbsp;&nbsp;
                    </div>
                    </font>

                                <select type="hidden" name="contractTypeId" id="contractTypeId"  onchange="checkContractType(this.value);" style="visibility:hidden">
                                    <option value="2" selected>Hired</option>
                                </select>
                                <script>
                                 //   document.getElementById("contractTypeId").value = '<c:out value="${contractTypeId}"/>';
                                </script>
                                <select  name="vendorType" id="vendorType" onchange="setVendor(this.value);" style="visibility:hidden">
                                    <option value='0'>--Select--</option>
                                    <c:forEach items="${VendorTypeList}" var="vendor">
                                        <option value='<c:out value="${vendor.vendorTypeId}"/>'><c:out value="${vendor.vendorTypeValue}"/></option>
                                    </c:forEach>
                                </select>
                                <script>
                                    document.getElementById("vendorType").value = '<c:out value="${vendorType}"/>';
                                    document.getElementById("vendorName").value = '<c:out value="${vendorName}"/>';
                                </script>
                    <table class="table table-info mb30 table-hover">
                        <thead>
                            <tr height="30" id="index">
                                <th colspan="6" ><b>Add Vendor Invoice Receipt</b></th>
                            </tr>
                        </thead>
                        <tr height="40">
                            <td  ><font color=red>*</font><spring:message code="finance.label.Vendor"  text="default text"/></td>
                            <td  >
                                <select class="form-control" name="vendorName" id="vendorName" style="width:220px;height:40px;">
                                       <option value='0'>--Select--</option>
                                    <c:if test="${vendorList != null}">
                                        <c:forEach items="${vendorList}" var="vendor">
                                           
                                            <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerID}"/>'><c:out value="${vendor.vendorName}"/></option>
                                            <script>
                                                document.getElementById("vendorName").value = '<c:out value="${vendorName}"/>';
                                            </script>
                                        </c:forEach>
                                    </c:if>

                                </select>

                            </td>
                             <td><font color=red>*</font><spring:message code="finance.label.Invoice Submission Date"  text="Invoice Submission Date" /></td>
                            <td>
                                <input type="textbox" name="invoiceSubmissionDate" id="invoiceSubmissionDate" value="<c:out value="${invoiceSubmissionDate}"/>" autocomplete="off" class="datepicker" style="width:220px;height:40px;"  onchange="searchPage('search')"/>
                            </td>
                            </tr>
                        <tr height="40">
                            <td><font color=red>*</font><spring:message code="finance.label.InvoiceDate"  text="default text" /></td>
                            <td>
                                <input type="textbox" name="invoiceDate" id="invoiceDate" value="<c:out value="${invoiceDate}"/>" autocomplete="off" class="datepicker" style="width:220px;height:40px;"  />
                            </td>
                            <td   ><font color=red>*</font><spring:message code="finance.label.InvoiceNo"  text="default text"/></td>
                            <td   >
                                <input type="textbox" name="invoiceNo" id="invoiceNo" value="<c:out value="${invoiceNo}"/>" autocomplete="off" class="form-control" style="width:220px;height:40px;"/>
                            </td>
                        </tr>
                        <%--<tr  height="40">
                           <td><font color=red>*</font><spring:message code="finance.label.LevelName"  text="default text"/></td>
                            <td>
                                <select name="levelId" id="levelId" class="form-control"  onchange="getLedgerList(this.value)" style="width:260px;height:40px;">
                                    <c:if test="${expenseGroupList != null}">
                                        <option value="0" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                        <c:forEach items="${expenseGroupList}" var="exp">
                                            <option value='<c:out value="${exp.levelID}"/>'><c:out value="${exp.levelGroupName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                   <td><font color=red>*</font><spring:message code="finance.label.DebitLedgerName"  text="default text"/></td>
                    <td  style="border-right-color:#5BC0DE;">
                        <select name="debitLedgerId" id="debitLedgerId" class="form-control" style="width:260px;height:40px;">
                        </select>
                    </td>

                </tr>--%>
                        <tr  height="40">
                            <td  ><font color=red>*</font><spring:message code="finance.label.InvoiceAmount"  text="default text"/></td>
                            <td >
                                <input type="textbox" name="invoiceAmount" id="invoiceAmount" value="<c:out value="${invoiceAmount}"/>" autocomplete="off" onKeyPress="return onKeyPressBlockCharacters(event);" class="form-control" style="width:220px;height:40px;"/>
                            </td>
                            <td ><spring:message code="finance.label.Narration"  text="default text"/></td>
                            <td >
                                <textarea type="textbox"   name="remarks" id="remarks" value=""  class="form-control" style="width:220px;height:40px;"/><c:out value="${remarks}"/></textarea>
                            </td>
                            </tr>
                            <tr height="40">
                            <td ><spring:message code="finance.label.Attach"  text="default text"/></td>
                            <td >
                                <input type="file" id="vendorInvoiceFile" name="vendorInvoiceFile" size="20" class="form-control" maxlength="10" value="0" >
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <c:if test = "${contractTypeId == '2'}" >
                        <div>
                            <input type="hidden" name="totalAmtHire" id="totalAmtHire" value="0" />
                            <input type="hidden" name="totalContInsAmt" id="totalContInsAmt" value="0" />
                            <input type="hidden" name="totalFuelAmt" id="totalFuelAmt" value="0" />
                            <input type="hidden" name="totalHireCashAdv" id="totalHireCashAdv" value="0" />
                            <input type="hidden" name="totalHaltingCharge" id="totalHaltingCharge" value="0" />
                            <input type="hidden" name="totalOtherCharge" id="totalOtherCharge" value="0" />
                            <table class="table table-info mb30 table-hover" border="1" id="filterTable">
                                <thead>
                                    <tr align="center" id="index" height="30">
                                        <th height="30">S.No</th>
                                        <th height="30">Trip Code</th>
                                        <th height="30">Vehicle No</th>
                                        <th height="30">Customer</th>
                                        <th height="30">Container Type</th>
                                        <th height="30">Container Qty</th>
                                        <th height="30">Container Name</th>
                                        <th height="30">Route</th>
                                        <th height="30">Freight Amount</th>
                                        <th height="30">Halting Charges</th>
                                        <th height="30">Other Charges</th>
                                        <th height="30">Container Insurance</th>
                                        <th height="30">Cash Advance</th>
                                        <th height="30">Fuel Advance</th>
                                        <th height="30">Bill Status</th>
                                        <th height="30" >Select</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int sno = 0;
                            int index = 1;%>
                                    <c:set var="totalAmtHire" value="${0}"/>
                                    <c:forEach items="${vendorPaymentList}" var="list"> 
                                        <c:set var="totalAmtHire" value="${totalAmtHire + list.expense + list.otherExpense + list.haltingCharges}"/>
                                        <%

                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                        %>

                                        <tr  width="208" height="40" > 
                                            <td height="20"><%=index%></td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="tripIds" id="tripIds<%=index%>" value="<c:out value="${list.tripIds}"/>"/>
                                                <input type="hidden" name="tripIdHire" id="tripIdHire<%=index%>" value="<c:out value="${list.tripId}"/>"/>
                                                <c:out value="${list.tripCode}"/>
                                            </td>
                                            <td height="20">
                                                <input type="hidden" name="vehicleIdHire"  value="<c:out value="${list.vehicleNo}"/>"/>
                                                <c:out value="${list.vehicleNo}"/>
                                            </td>
                                            <td height="20">
                                                <input type="hidden" name="vehicleTypeIdHire"  value="<c:out value="${list.vehicleTypeId}"/>"/>
                                                <%--<c:out value="${list.vehicleTypeName}"/>--%><c:out value="${list.customerName}"/>
                                            </td>
                                            
                                            <td height="20" align="right">
                                                <input type="hidden" name="containerHire"  value="<c:out value="${list.container}"/>"/>
                                                <input type="hidden" name="tripTypes"  value="<c:out value="${list.tripType}"/>"/>
                                                <c:out value="${list.tripType}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="containerQtyHire" id="containerQtyHire<%=index%>" value="<c:out value="${list.containerQty}"/>"/>
                                                <c:out value="${list.containerQty}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="containerNo"  value="<c:out value="${list.containerNo}"/>"/>
                                                <c:out value="${list.containerNo}"/>
                                            </td>
                                            <td height="20">
                                                <input type="hidden" name="routeHire"  value="<c:out value="${list.route}"/>"/>
                                                <c:out value="${list.route}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="totAmtHire" id="totAmtHire<%=index%>" value="<c:out value="${list.expense}"/>"/>
                                                <c:out value="${list.expense}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="vendorHaltingCharges" id="vendorHaltingCharges<%=index%>" value="<c:out value="${list.haltingCharges}"/>"/>
                                                <c:out value="${list.haltingCharges}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="otherCharges" id="otherCharges<%=index%>" value="<c:out value="${list.otherExpense}"/>"/>
                                                <c:out value="${list.otherExpense}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="contInsAmts" id="contInsAmts<%=index%>" value="<c:out value="${list.containerInsuranceAmt}"/>"/>
                                                <c:out value="${list.containerInsuranceAmt}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="hireCashAdvs" id="hireCashAdvs<%=index%>" value="<c:out value="${list.hireCashAdv}"/>"/>
                                                <c:out value="${list.hireCashAdv}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="fuelAmounts" id="fuelAmounts<%=index%>" value="<c:out value="${list.fuelAmount}"/>"/>
                                                <c:out value="${list.fuelAmount}"/>
                                            </td>
                                            <td height="20" align="left">
                                                <c:if test = "${list.statusName == 0}" ><label style="color:red">Not Ready to Bill</label></c:if>
                                                <c:if test = "${list.statusName == 1}" ><label style="color:green">Ready to Bill</label></c:if>

                                                </td>
                                                <td height="20" align="center">
                                                    <input type="checkbox" name="selectedIndexHire" id="selectedIndexHire<%=index%>" onclick="setActiveIndHire('<%=index%>');" />
                                                <input type="hidden" name ="activeIndHire" id="activeIndHire<%=index%>" value="N"/>
                                                <input type="hidden" name="totAmttHire" id="totAmttHire<%=index%>" value=""/>
                                                <input type="hidden" name="contInsAmt" id="contInsAmt<%=index%>" value=""/>
                                                <input type="hidden" name="vendorHaltingCharge" id="vendorHaltingCharge<%=index%>" value=""/>
                                                <input type="hidden" name="otherCharge" id="otherCharge<%=index%>" value=""/>
                                                <input type="hidden" name="fuelAmount" id="fuelAmount<%=index%>" value=""/>
                                                <input type="hidden" name="hireCashAdv" id="hireCashAdv<%=index%>" value=""/>

                                            </td>
                                        </tr>
                                    <script>setActiveIndHire(<%=index%>);</script>
                                    <%
                                        index++;
                                        sno++;
                                    %>
                                </c:forEach>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <tr>
                                   <td colspan="12" align="right"><span id="totalAmountHire" style="color:black">
                                         Total Freight :&emsp;    <fmt:formatNumber pattern="##0.00" value="${totalAmtHire}"/>
                                        </span> 
                                    </td>
                                    <td colspan="2" align="right">&emsp;

                                    </td>

                                </tr>

                                </tbody>
                            </table>
                                        
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50" >50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                    <br/>
                     <script language="javascript" type="text/javascript">
                        setFilterGrid("filterTable");
                    </script>
                    
                        </div>
                    </c:if>
                    
                    
                    <c:if test = "${contractTypeId == '1'}" >
                        <div>
                            <input type="hidden" name="totalAmtDedicate" id="totalAmtDedicate" value="0" />
                            <table class="table table-info mb30 table-hover" border="1" >
                                <thead>
                                    <tr align="center" id="index" height="30">
                                        <th height="30">S.No</th>
                                        <th height="30">Trip Id's</th>
                                        <th height="30">Vehicle No</th>
                                        <th height="30">Vehicle Type</th>
                                        <th height="30">Contract From</th>
                                        <th height="30">Contract To</th>
                                        <th height="30">Contract Category</th>
                                        <th height="30">No Of Trips</th>
                                        <th height="30">Amount</th>
                                        <th height="30">Other Expense</th>
                                        <th height="30">Bill Status</th>
                                        <th height="30" >Select</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int sno = 0;
                            int index = 1;%>
                                    <c:set var="totalAmtDedicate" value="${0}"/>
                                    <c:forEach items="${vendorPaymentList}" var="list"> 
                                        <c:set var="totalAmtDedicate" value="${totalAmtDedicate + list.expense + list.otherExpense}"/>
                                        <%

                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                        %>

                                        <tr  width="208" height="40" > 
                                            <td height="20"><%=index%></td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="tripIdDedicate" id="tripIdDedicate<%=index%>" value="<c:out value="${list.tripId}"/>"/>
                                                <c:out value="${list.tripId}"/>
                                            </td>
                                            <td height="20">
                                                <input type="hidden" name="vehicleIdDedicate"  value="<c:out value="${list.vehicleId}"/>"/>
                                                <c:out value="${list.vehicleNo}"/>
                                            </td>
                                            <td height="20">
                                                <input type="hidden" name="vehicleTypeIdDedicate"  value="<c:out value="${list.vehicleTypeId}"/>"/>
                                                <c:out value="${list.vehicleTypeName}"/>
                                            </td>
                                            <td height="20">
                                                <input type="hidden" name="contractFrom"  value="<c:out value="${list.startDate}"/>"/>
                                                <c:out value="${list.startDate}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="contractTo"  value="<c:out value="${list.endDate}"/>"/>
                                                <c:out value="${list.endDate}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="contractCategory" id="contractCategory<%=index%>" value="<c:out value="${list.contractCategory}"/>"/>
                                                <c:if test = "${list.contractCategory == 2}" >Actual</c:if>
                                                <c:if test = "${list.contractCategory == 1}" >Fixed</c:if>

                                                </td>
                                                <td height="20" align="right">
                                                    <input type="hidden" name="noOfTripsDedicate" id="noOfTripsDedicate<%=index%>" value="<c:out value="${list.noOfTrips}"/>"/>
                                                <c:out value="${list.noOfTrips}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="totAmtDedicate" id="totAmtDedicate<%=index%>" value="<c:out value="${list.expense}"/>"/>
                                                <c:out value="${list.expense}"/>
                                            </td>
                                            <td height="20" align="right">
                                                <input type="hidden" name="otherExpensesDedicate" id="otherExpensesDedicate<%=index%>" value="<c:out value="${list.otherExpense}"/>"/>
                                                <c:out value="${list.otherExpense}"/>
                                            </td>
                                            <td height="20" align="left">
                                                <c:if test = "${list.statusName == 0}" ><label style="color:red">Not Ready to Bill</label></c:if>
                                                <c:if test = "${list.statusName == 1}" ><label style="color:green">Ready to Bill</label></c:if>

                                                </td>
                                                <td height="20" align="center">
                                                    <input type="checkbox" name="selectedIndexDedicate" id="selectedIndexDedicate<%=index%>" onclick="setActiveIndDedicate('<%=index%>');" checked/>
                                                <input type="hidden" name ="activeIndDedicate" id="activeIndDedicate<%=index%>" value="N"/>
                                                <input type="hidden" name="totAmttDedicate" id="totAmttDedicate<%=index%>" value=""/>

                                            </td>
                                        </tr>
                                    <script>setActiveIndDedicate(<%=index%>);</script>
                                    <%
                                        index++;
                                        sno++;
                                    %>
                                </c:forEach>
                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                <tr>
                                    <td colspan="9" align="right" ><span id="totalAmountDedicate" style="color:black">
                                            <fmt:formatNumber pattern="##0.00" value="${totalAmtDedicate}"/>
                                        </span> 
                                    </td>
                                    <td colspan="2" align="right">&emsp;

                                    </td>

                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </c:if>
                    <br>
                    <br>
                    <br>
                    <center>
                        <input type="button" value="Save" class="btn btn-success" id="saveButton" onClick="submitPage(this.value);" style="width:100px;height:35px;">
                        &emsp;<input type="reset" class="btn btn-success" value="<spring:message code="finance.label.Clear"  text="default text"/>" style="width:100pxsubmitPage;height:35px;">
                    </center>
                    <br>
                    <br>
                    <br>
                    <br>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

