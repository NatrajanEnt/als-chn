<%--
Document   : delivery note
Created on : oct 31, 2020, 4:06:44 PM
Author     : Hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Delivery Note Print</title>
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
        <%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
        <jsp:directive.page import="org.displaytag.sample.*" />
        <jsp:useBean id="now" class="java.util.Date" scope="request" />
        <%@page import="java.text.DecimalFormat"%>
    </head>

    <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/rupees.css" type="text/css" media="screen">
    <script type="text/javascript" src="/throttle/js/code39.js"></script>
    <script type="text/javascript">
        function printPage(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }
        function goBack() {
            document.waybillPrint.action = '/throttle/handleWayBillPaid.do';
            document.waybillPrint.submit();

        }

    </script>


</head>
<body>
    <%--@ include file="/content/common/path.jsp" --%>
    <center>
        <%-- @include file="/content/common/message.jsp" --%>
    </center>
    <form name="waybillPrint">
        <input type="hidden" name="wayBillMode" id="wayBillMode" value="4"/>
        <input type="hidden" class="textbox" name="fromLocation" id="fromLocation" value="<%= session.getAttribute("branchId")%>"/>
                <div id="printDiv" >
                    <table style="margin: 0px;font-family: Arial, Helvetica, sans-serif;width: 100%;"  align="center"  cellpadding="0"  cellspacing="0" border="1">
                        <tr >
                            <td  style="margin: 0px;height:25px;width:175px;font-weight:bold;">
                                <div style="width:200px;height:35px;float:left;display:block;"><img src="/throttle/images/HSSupplyLogo.png" style="width: 150px; height: 40px;"></div>                                
                            </td>
                           <td align="left" style="height:25px;width:525px;font-size:10px;font-weight:bold;size: 5px;">
                                HS Supply Solution Pvt Ltd<br/>s
                                <br/>
                                No 30, Chandralaya Apartment, Judge Jambulingam Rd, Mylapore,
                                &emsp; Chennai,Tamil Nadu 600004<br/>
                            </td>
                            <td >
                                <div id="externalbox" style="width:3in;margin: 10px;">
                                    <div align="center" style="font-size:12px;"><b>Delivery Note Ref No</b></div>
                                    <div id="inputdata"></div>
                                    <div align="center" style="font-size:12px;"><b>Delivery Note Date : 01-11-2020 10:10:00</b></div>
                                </div>
                            </td>
                        </tr>
<!--                    </table>
                    <table style="font-family: Arial, Helvetica, sans-serif;border:#000000 solid 1px; border-bottom:solid 1px;border-right:solid 1px;border-top:solid 1px;border-left:solid 1px;width: 100%;height:100px;" align="center"  cellpadding="0"  cellspacing="0" >-->
                        <tr>
                            <td style="height:30px;width:175ppx;font-weight:bold;">
                                <%--<c:set var="runSheetRefNo" value="" />--%>
                                <div>
                                    <table>                                       
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Delivery Location </b></td>
                                            <td style="font-size:10px;">: Coimbatore
                                            </td>
                                        </tr>
                                         <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>No Of pick List </b></td>
                                            <td style="font-size:10px;">: 6</td>
                                        </tr>
                                         <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>No Of Pack List </b></td>
                                            <td style="font-size:10px;">: 2</td>
                                        </tr>
<!--                                        <tr style="height: 20px">
							<td style="font-size:10px;"><b>Opening KM: <c:out value="${run.startKm}"/></b></td>
							 <td style="font-size:10px;" align="center"><b>&emsp;Closing KM: <c:out value="${run.endKm}"/></b></td>
					                                            
                                        </tr>-->
                                    </table>
                                </div>
                            </td>
                            <td align="left" style="height:30px;width:550px;font-weight:bold;">
                                <div>
                                    <table>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px"><b>Transporter </b></td>
                                            <td style="font-size:10px;">: 
                                               National Transport
                                            </td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Driver </b></td>
                                            <td style="font-size:10px;">: 
                                                Ragu
                                            </td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Driver Mobile</b></td>
                                            <td style="font-size:10px;">: 
                                                984578889
                                            </td>
                                        </tr>
                                        
                                        
                                    </table>
                                </div>
                            </td>
                            <td style="height:30px;width:275px;text-align: right">
                                <div>
                                    <table>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;">&emsp;<b>Vehicle No</b></td>
                                            <td style="font-size:10px;"> : TN13OA8654 </td>
                                        </tr>
                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Packing</b></td>
                                            <td style="font-size:10px;"> : Pack/20-21/001 , Pack/20-21/002</td>
                                        </tr>
<!--                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Vehicle Type </b></td>
                                            <td style="font-size:10px;">: <c:out value="${run.vehicleTypeName}"/></td>
                                        </tr>-->
<!--                                        <tr style="height: 30px">
                                            <td style="font-size:10px;"><b>Starting Time: <c:out value="${run.startDate}"/></b></td>
                                            <td style="font-size:10px;" align="center"><b>&emsp;&emsp;&emsp;&emsp;Closing Time: <c:out value="${run.endDate}"/></b></td>
                                        </tr>
                                        -->

                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                                     
                    <table style="margin: 0px;font-family: Arial, Helvetica, sans-serif;width: 100%;"  align="center"  cellpadding="0"  cellspacing="0" border>
                            <thead>
                                <tr height="40">
                                    <th style="font-size:10px;width: 5%;">S.No</th>
                                    <th style="font-size:10px;width: 15%;">Item Code</th>
                                    <th style="font-size:10px;width: 15%;">Delivery Note No</th>
                                    <th style="font-size:10px;width: 8%;">Company Name</th>
                                    <th style="font-size:10px;width: 7%;">Spec</th>
                                    <th style="font-size:10px;width: 7%;">Company Code</th>
                                    <!--<th style="font-size:10px; width: 15%;">Group</th>-->
                                    <th style="font-size:10px; width: 17%;">Order Qty</th>
                                    <th style="font-size:10px;width: 10%;">UOM</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">1</td>
                                        <td align="center" style="font-size:10px;">AC101RFH45 </td>
                                        <td align="center" style="font-size:10px;">1337 </td>
                                        <td align="center" style="font-size:10px;">Voltas </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">AC101RFH45</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">10 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">2</td>
                                        <td align="center" style="font-size:10px;">COOKER101 </td>
                                        <td align="center" style="font-size:10px;">1339 </td>
                                        <td align="center" style="font-size:10px;">ButterFly </td>
                                        <td align="center" style="font-size:10px;">2019 </td>
                                        <td align="center" style="font-size:10px;">COOKER101</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">10 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">3</td>
                                        <td align="center" style="font-size:10px;">COOKER102 </td>
                                        <td align="center" style="font-size:10px;">1340 </td>
                                        <td align="center" style="font-size:10px;">Prestige </td>
                                        <td align="center" style="font-size:10px;">2018 </td>
                                        <td align="center" style="font-size:10px;">COOKER102</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">10 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">4</td>
                                        <td align="center" style="font-size:10px;">AC101RFH50 </td>
                                        <td align="center" style="font-size:10px;">1341 </td>
                                        <td align="center" style="font-size:10px;">kelvinator </td>
                                        <td align="center" style="font-size:10px;">2018 </td>
                                        <td align="center" style="font-size:10px;">AC101RFH50</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">20.0 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">5</td>
                                        <td align="center" style="font-size:10px;">MIXIE101 </td>
                                        <td align="center" style="font-size:10px;">1342 </td>
                                        <td align="center" style="font-size:10px;">Butterflty </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">MIXIE101</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">20.0 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                    <tr height="30" >
                                        <td align="center" style="font-size:10px;">6</td>
                                        <td align="center" style="font-size:10px;">Fridge101 </td>
                                        <td align="center" style="font-size:10px;">1342 </td>
                                        <td align="center" style="font-size:10px;">Samsung </td>
                                        <td align="center" style="font-size:10px;">2020 </td>
                                        <td align="center" style="font-size:10px;">Fridge101</td>
                                        <!--<td align="center" style="font-size:10px;">Generic </td>-->
                                        <td align="center" style="font-size:10px;">4.0 </td>
                                        <td align="center" style="font-size:10px;">Nos </td>
                                        
                                    </tr>
                                        <!--<td colspan="2" style="font-size:10px;" align="right"><b>Total</b></td>-->
<!--                                        <td align="center" style="font-size:10px;">
                                            40
                                       </td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>                                        -->
                                    </tr>
                            </tbody>    
                    </table>
                </div>
            <br>
            <center>
                <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
                <!--<input type="button" class="button" name="back" value="Back" onClick="goBack();" > &nbsp;-->
            </center>
            <br>
            <br>
        <%--</c:if>--%>
    </form>
    <script type="text/javascript">
        /* <![CDATA[ */
        function get_object(id) {
            var object = null;
            if (document.layers) {
                object = document.layers[id];
            } else if (document.all) {
                object = document.all[id];
            } else if (document.getElementById) {
                object = document.getElementById(id);
            }
            return object;
        }
        //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
        get_object("inputdata").innerHTML = DrawCode39Barcode('DN0001', 0);
        /* ]]> */
    </script>
</body>
</html>









