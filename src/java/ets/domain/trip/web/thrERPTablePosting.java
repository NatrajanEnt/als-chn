/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.trip.web;

import java.net.*;
import java.io.*;
import java.text.*;

import java.sql.SQLException;
import java.text.ParseException;
import org.json.JSONObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
//import junit.swingui.StatusLine;
import org.json.JSONArray;
import org.json.simple.parser.JSONParser;

public class thrERPTablePosting extends Thread {
//public class thrERPTablePosting {

    String tripId = null;
    String vehicleId = null;
    String compId = null;
    String statusId = null;
    String actStatus = null;
    String tripIds = null;
    String vendId = null;
    String invoiceId = null;

    public thrERPTablePosting(String tripSheetId, String vehicleIds, String companyId, String statId, String activeStatus, String trpIds, String vendIds) throws ParseException, ClassNotFoundException, SQLException, org.json.simple.parser.ParseException {
        tripId = tripSheetId;
        compId = companyId;
        statusId = statId;
        actStatus = activeStatus;
        tripIds = trpIds;
        String[] tempVendId = vendIds.split("-");
        invoiceId = tempVendId[0];
        vendId = tempVendId[1];
        System.out.println("vehicleId---ERP---" + vehicleIds);
        System.out.println("statusId---ERP---" + statusId);
        System.out.println("actStatus---ERP---" + actStatus);
        System.out.println("tripId---ERP---" + tripId);
        System.out.println("tripIds---ERP---" + tripIds);
        System.out.println("invoiceId---ERP---" + invoiceId);
        System.out.println("vendId---ERP---" + vendId);
        if (vehicleIds == null) {
            vehicleId = "0";
        } else {
            vehicleId = vehicleIds;
        }
    }
//    public static void main(String args[]) throws ParseException, ClassNotFoundException, SQLException {
//        thrERPTablePosting program = new thrERPTablePosting();
//        program.run();
//    }

    public void run() {
        try {
            Connection con = null;
            Statement statement = null;
            Statement statement1 = null;
            ResultSet rs = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;
            ResultSet rs4 = null;
            ResultSet rs5 = null;
            ResultSet rs6 = null;
            ResultSet rs7 = null;
            ResultSet rs8 = null;
            ResultSet rs9 = null;
            ResultSet rs10 = null;
            String fileName = "jdbc_url.properties";
            InputStream is = getClass().getResourceAsStream("/" + fileName);
            Properties dbProps = new Properties();

            try {
                dbProps.load(is);//this may throw IOException
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("ex.printStackTrace()");
            }

            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            try {

                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                PreparedStatement getDKTDetails = null;
                PreparedStatement insertDKTDetails = null;
                PreparedStatement getContDetails = null;
                PreparedStatement insertContDetails = null;
                PreparedStatement getTxnDetails = null;
                PreparedStatement getTxnFuelDetails = null;
                PreparedStatement getTxnFuelCash = null;
                PreparedStatement getTxnFastTag = null;
                PreparedStatement getTxnFuelRecovery = null;
                PreparedStatement getTxnFuelSubCodeList = null;
                PreparedStatement insertTxnDetails = null;
                PreparedStatement checkDktStatus = null;
                PreparedStatement updateTripDktStatus = null;
                PreparedStatement updateDKTDetails = null;
                PreparedStatement updateTripDktStatus1 = null;
                PreparedStatement getHireTxnDetails = null;
                PreparedStatement getHireCntrctTxnDetails = null;
                PreparedStatement insertHireTxnDetails = null;
                PreparedStatement getHireTxnFuelDetails = null;
                PreparedStatement getHireTxnContainerInsDetails = null;
                PreparedStatement getHireTotalCreditTxnDetails = null;
                PreparedStatement getHireTxnTDS = null;
                PreparedStatement updateTxnStatus = null;
                PreparedStatement insertFuelTxnDetails = null;

                statement = con.createStatement();

                String sql = "SELECT a.trip_code,ifnull(b.tripIds,a.trip_id)as trip_id,DATE_FORMAT(a.trip_actual_start_date,'%Y%m%d') AS jobstartdate,DATE_FORMAT(trip_actual_end_date,'%Y%m%d') AS jobenddate,\n"
                        + "(select ifnull(DivCharge,0) from ts_trip_diversion dt where a.trip_id= dt.trip_id limit 1)as divertCharge,\n"
                        + "(select ifnull(OTCharges,0) from ts_trip_over_tonnage ot where a.trip_id= ot.trip_id and submit_status= 1 limit 1)as oTCharges,\n"
                        + "(SELECT mapping_Id FROM ts_cities WHERE city_Id=(select tr.actual_point_id from  ts_trip_route_course tr where tr.trip_id = a.trip_id and point_sequence=1) LIMIT 1) AS cityfromcode,\n"
                        + "if((select count(*) from  ts_trip_route_course tr where tr.trip_id =a.trip_id)>2,\n"
                        + "(SELECT mapping_Id FROM ts_cities WHERE city_Id=(select tr.actual_point_id from  ts_trip_route_course tr where tr.trip_id = a.trip_id and point_sequence =2) LIMIT 1),\n"
                        + "(SELECT mapping_Id FROM ts_cities WHERE city_Id=(select tr.actual_point_id from  ts_trip_route_course tr where tr.trip_id = a.trip_id and point_sequence=2) LIMIT 1)) AS cityTocode,\n"
                        + "if((select count(*) from  ts_trip_route_course tr where tr.trip_id =a.trip_id)>2,\n"
                        + "(SELECT mapping_Id FROM ts_cities WHERE city_Id=(select tr.actual_point_id from  ts_trip_route_course tr where tr.trip_id = a.trip_id and point_sequence =(select count(*) from  ts_trip_route_course tr where tr.trip_id =a.trip_id)) LIMIT 1),\n"
                        + "null) AS cityReturncode,\n"
                        + "estimated_km AS distance,IF(b.vend_Id !=1,'Hired','Owned') AS ownedhired,b.regno AS hiredvehicleno,\n"
                        + "IFNULL(if(b.tripIds  like '%A%',0.00 ,(a.estimated_revenue)),0.00) AS hireCharges,\n"
                        + "customerName AS partyName,\n"
                        + "(SELECT Erp_Id FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS AccCode,\n"
                        + "ifnull((select  d.allocated_fuel from ts_trip_fuel_recovery d where a.trip_id =d.trip_id and b.vehicle_id = vh_id),0) AS dieselsupplies,\n"
                        + "ifnull((select  d.consumed_fuel from  ts_trip_fuel_recovery d where a.trip_id =d.trip_id and b.vehicle_id = vh_id),0) AS dieselConsumed,\n"
                        + "ifnull((select  d.recovery_fuel from  ts_trip_fuel_recovery d where a.trip_id =d.trip_id and b.vehicle_id = vh_id),0) AS dieselRecovered,\n"
                        + "trip_start_km AS openingkm,\n"
                        + "trip_end_km AS closingkm,total_km_run AS totalkmrun,(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=1) AS size_20,\n"
                        + "(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=2 AND container_category NOT IN (6,2,3)) AS size_40,\n"
                        + "(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=2 AND container_category=6) AS size_40HC,\n"
                        + "(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=2 AND container_category=2) AS size_40FT,\n"
                        + "(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=2 AND container_category=3) AS size_40OT,\n"
                        + "if(IFNULL((SELECT NetWt FROM ts_trip_over_tonnage ot where ot.trip_Id=a.trip_Id and submit_status=1 limit 1),0)=0,\n"
                        + "(select sum(tonnage) from ts_trip_container tcn where tcn.trip_id =a.trip_id),(SELECT NetWt FROM ts_trip_over_tonnage ot where ot.trip_Id=a.trip_Id and submit_status=1 limit 1)) AS netwt,\n"
                        + "if(IFNULL((SELECT ActWt FROM ts_trip_over_tonnage ot where ot.trip_Id=a.trip_Id and submit_status=1 limit 1),0)=0,\n"
                        + "(select sum(tonnage) from ts_trip_container tcn where tcn.trip_id =a.trip_id),(SELECT ActWt FROM ts_trip_over_tonnage ot where ot.trip_Id=a.trip_Id and submit_status=1 limit 1)) AS grosswt,\n"
                        + "(SELECT if(container_type_id = 1,'20','40') FROM ts_trip_container WHERE a.trip_Id=trip_Id limit 1) AS container_type,\n"
                        + "(SELECT mt.is_Export  FROM  ts_consignment_note cn,ts_trip_consignment tc,ts_movement_type_master mt WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id and mt.id = cn.movement_type) AS isExport,\n"
                        + "(SELECT movement_type  FROM  ts_consignment_note cn,ts_trip_consignment tc WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id) AS movement,\n"
                        + "(SELECT mt.movement_type  FROM  ts_consignment_note cn,ts_trip_consignment tc,ts_movement_type_master mt WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id and mt.id = cn.movement_type) AS MovType,\n"
                        + "(select erp_id from papl_vehicle_master vm where vm.vehicle_Id=b.vehicle_Id) as trailerCode,0 as consigneeCode,\n"
                        + "(select erp_Id from papl_emp_master em,ts_trip_driver c where em.emp_Id=c.emp_Id and a.trip_id = c.trip_id limit 1) as DriverCode,\n"
                        + "(select erp_Id from papl_emp_master em,ts_trip_driver c where em.emp_Id=c.cleaner_Id and a.trip_id = c.trip_id limit 1) as CleanerCode,\n"
                        + "invoice_No,IFNULL(if(b.tripIds  like '%A%',0.00,(a.estimated_revenue)),0.00)+ifnull((select sum(expense_value) from ts_trip_expense where trip_Id=a.trip_Id and expense_Id=1021 and expense_type in(1,4) and approval =1),0.00)+\n"
                        + "ifnull((select sum(expense_value) from ts_trip_expense where trip_Id=a.trip_Id and expense_Id != 1021 and expense_type in(1,4) and approval !=2),0.00)\n"
                        + "+ifnull((SELECT divCharge FROM ts_trip_diversion dt where dt.trip_Id=a.trip_id limit 1),0.00)+ifnull((SELECT OTCharges FROM ts_trip_over_tonnage ot where ot.trip_Id=a.trip_Id and submit_status = 1 limit 1),0.00)as invoice_value,\n"
                        + "IF(b.vend_Id !=1,(select erp_Id from papl_vendor_master where vendor_Id=b.vend_Id),'DC001') AS subCode,\n"
                        + "estimated_revenue,\n"
                        + "ifnull((select if(expense_type = 1,if(halt_contract_amt >0,DATEDIFF(trip_actual_end_date, trip_actual_start_date),null),null) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),0) as haltingDay,\n"
                        + "ifnull((select if(expense_type = 1,if(halt_contract_amt >0,DATE_FORMAT(a.haltStartDate,'%Y%m%d'),null),null) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),null) AS haltingFrom,\n"
                        + "ifnull((select if(expense_type = 1,if(halt_contract_amt >0,DATE_FORMAT(a.haltEndDate,'%Y%m%d'),null),null) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),null) AS  haltingTo,\n"
                        + "(select if(expense_type = 1,if(halt_contract_amt >0,if(approval = 1,expense_value,if(approval = 2, null,0)),0),0) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id) as haltingAmt,\n"
                        + "(select if(expense_type = 1,if(halt_contract_amt >0,if(approval = 1,a.halt_cntrct,if(approval = 2, null,0)),0),0) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id) as HaltCntrctAmnt,\n"
                        + "(select if(expense_type = 1,if(halt_contract_amt >0,if(approval = 1,a.halt_days,if(approval = 2, null,0)),0),0)from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id) AS halt_days,\n"
                        + "ifnull((select if(expense_type = 1,if(clr_contract_amt >0,DATE_FORMAT(a.clrStartDate,'%Y%m%d'),null),null) from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id and approval=1),null) AS clrFrom,\n"
                        + "ifnull((select if(expense_type = 1,if(clr_contract_amt >0,DATE_FORMAT(a.clrEndDate,'%Y%m%d'),null),null) from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id and approval=1),null) AS  clrTo,\n"
                        + "(select if(expense_type = 1,if(clr_contract_amt >0,if(approval = 1,expense_value,if(approval = 2, null,0)),0),0) from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id) as clrAmt,\n"
                        + "(select if(expense_type = 1,if(clr_contract_amt >0,if(approval = 1,a.clr_cntrct,if(approval = 2, null,0)),0),0) from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id) as clrCntrctAmnt,\n"
                        + "(select if(expense_type = 1,if(clr_contract_amt >0,if(approval = 1,a.clr_days,if(approval = 2, null,0)),0),0)from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id and approval=1)AS clr_days,\n"
                        + "0 as TrpEndORClsr,\n"
                        + "(SELECT if(tally_billing_type = 1,'RCM','BOS') FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS TrpInvType,\n"
                        + "(SELECT concat(if(trip_type = 1,'Load','Empty'),' ',\n"
                        + "(SELECT if(container_type_id = 1,'20','40') FROM ts_trip_container WHERE a.trip_Id=trip_Id limit 1),' ',\n"
                        + "if(movement_Type in(1,2,19,20,25,26,29,30,31,32,33,34,35),'MOFUSSIL','LOCAL')) FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS TripType,\n"
                        + "(select ifnull(cust_ref_no,0)as job_no FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm,ts_consignment_container_details ccd\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id and cn.consignment_order_Id=ccd.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id limit 1) AS JobNo,\n"
                        + "(select if(movement_Type =1,shiping_line_no,if(movement_Type =2,customer_order_reference_no,0)) FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS BESBNo,\n"
                        + "(select ifnull(bill_of_entry,0)as job_no FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS BLBKGNo,\n"
                        + "(select sum(te.expense_value) from ts_trip_expense te where a.trip_Id=te.trip_Id and te.expense_value !=0 and expense_id=1014 and expense_type in (1,4,3)) as weighAmt,\n"
                        + "(select sum(te.expense_value) from ts_trip_expense te where a.trip_Id=te.trip_Id and te.expense_value !=0 and expense_id=1034 and expense_type in (1,4,3)) as LOLOAmnt,\n"
                        + "(select sum(te.expense_value) from ts_trip_expense te where a.trip_Id=te.trip_Id and te.expense_value !=0 and expense_id=1035 and expense_type in (1,4,3)) as LoadAmt,\n"
                        + "(select sum(te.expense_value) from ts_trip_expense te where a.trip_Id=te.trip_Id and te.expense_value !=0 and expense_id=1027 and expense_type in (1,4,3)) as TollCharge, \n"
                        + "(select sum(te.expense_value) from ts_trip_expense te where a.trip_Id=te.trip_Id and te.expense_value !=0 and expense_id=1023 and expense_type in (1,4)) as OtherExpenseTuti, \n"
                        + "(select sum(te.expense_value) from ts_trip_expense te where a.trip_Id=te.trip_Id and expense_id not in(1014,1027,1034,1035,1021,1049,1023,1052) and expense_type in (1,4)) as OtherChargeTuti, \n"
                        + "(select sum(te.expense_value) from ts_trip_expense te where a.trip_Id=te.trip_Id and expense_id not in(1014,1027,1034,1021,1049,1052) and expense_type in (1)) as OtherCharge, \n"
                        + "now() as runDate,b.vend_Id \n"
                        + "FROM ts_trip_master a ,ts_trip_vehicle b,papl_vehicle_type_master v\n"
                        + "WHERE  a.trip_Id=b.trip_Id \n"
                        + "AND v.vehicle_type_Id= a.vehicletypeid\n"
                        + "AND a.trip_Id=? and b.vehicle_id = ? and a.comp_Id=? and b.vend_Id=? and b.active_ind=? limit 1";

                String sql1 = "insert into Dkt "
                        + "(JobStartDate,JobEndDate,CityFromCode,CityToCode,Distance,Owned_Hired,HiredVehicleNo,HireCharges,Partyname,AccCode,"
                        + "DieselSupplies,DieselConsumed,OpeningKM,ClosingKM,TotalKMRun,Size_20,Size_40,Size_40HC,Size_FT,Size_OT,"
                        + "NetWt,Closed,CompanyCode,BrnCd,RUNDATE,JobReachedDate,JobDate,DieselRecovered,IsExport,TrlJobId,post_ind,TrailerCode, ConsigneeCode, DriverCode, CleanerCode,  CityReturnedCode,SubCode,"
                        + "InvNo, InvAmount,FactoryHaltFromDate,FactoryHaltToDate,ChennaiHaltFromDate,ChennaiHaltToDate,"
                        + "MofussilHaltingDays,LocalHaltingDays,moffisalhalting_amt,localhalting_amt,"
                        + "HaltingDays,HaltCntrctAmnt,TrpEndORClsr,TrpInvType,tripType,GrossWt,JobNo,BESBNo,BLBKGNo,divCharge,OTCharge,MovType,company_id,WeigAmnt,LOLOAmnt,LoadAmt,TollCharge,OtherCharge,OtherTUT) Values"
                        + "(?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,"
                        + "0,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,?,?,?)";

                String sql2 = "SELECT @acount:=@acount+1 serial_number,type_name,container_no,sealNo,\n"
                        + "replace(replace(replace(IF(vehicle_Id=0,regNo,(SELECT reg_No FROM papl_vehicle_reg_no WHERE vehicle_Id=tv.vehicle_Id AND active_Ind='Y' LIMIT 1)),' ',''),'-',''),' ','') AS vehicleName,\n"
                        + "ifnull(tv.tripIds,tc.trip_id)as trip_id,now() as RunDate\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_container tc,ts_trip_vehicle tv,\n"
                        + "papl_container_type_master ct \n"
                        + "WHERE tc.container_type_Id=ct.type_id AND tc.trip_Id=tv.trip_Id and tv.trip_Id=? and tv.vehicle_id = ? and tv.vend_id =? and tv.active_ind= ? ";

                String sql3 = "insert into ContDetails "
                        + "(ContainerSrlNo,ContainerSize,ContainerName,SealNo,CompanyCode,BrnCd,VehicleName,TrlJobId,post_ind,RUNDATE,company_id) Values"
                        + "(?,?,?,?,?,?,?,?,?,?,?)";

//              String sql4 = "SELECT 'JV' AS txnType,@acount:=@acount+1 serial_number,te.trip_Id,te.expense_date,erp_Id AS AccCode,erp_subCode AS subCode,"
//                        + "CONCAT('EXP INC VEH#',SUBSTRING(reg_no,-4,4),' AT TMP ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration1,"
//                        + "CONCAT('PD ',alise_Name, ' AT ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration2,"
//                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no "
//                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,"
//                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em "
//                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND tm.destination_Id=tc.city_Id and tm.trip_Id=?";
                String sql4 = "SELECT te.expense_id as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,ifnull(tv.tripIds,te.trip_Id)as trip_id,(te.expense_date) as expense_date,\n"
                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
                        + "erp_id_tuti As AccCodeTuti,erp_subcode_tuti As subCodeTuti, \n"
                        + "erp_id_coc As AccCodeCoc,erp_subcode_coc As subCodeCoc, \n"
                        + "CONCAT(replace(replace(replace(reg_No,' ',''),'-',''),' ',''),'-',tm.customername,'-',tm.routeinfo) AS narration1COC,\n"
                        + "CONCAT('EXP INC VEH#',SUBSTRING(reg_no,-4,4),' AT TMP ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration1,\n"
//                        + "CONCAT('PD ',if(expense_type = 1,alise_Name,'cross border expense'), ' AT ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration2,\n"
                        + "CONCAT('PD ',if(expense_type = 1 and expense_id = 1033,'RTO expense',alise_Name), ' AT ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS ','-',expense_remarks) AS narration2,\n"
                        + "CONCAT('BEING TRLR ',SUBSTRING(reg_no,-4,4),' ', \n"
                        + "(select city_name from ts_cities where city_id in \n"
                        + "(select point_id  from ts_trip_route_course r where\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)),' TRIP EXP PAID FOR ',DATEDIFF(tm.trip_actual_end_date, tm.trip_actual_start_date),' DAYS ON: ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'))as narration1Tuti,\n"                           
                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
                        + "as DriverLc\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,ts_trip_vehicle tv,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id \n"
                        + "AND te.trip_Id=tv.trip_Id AND tm.trip_Id=tv.trip_Id AND td.trip_Id=tv.trip_Id \n"
                        + "AND tm.destination_Id=tc.city_Id and td.vehicle_id = te.vehicle_id and tv.vehicle_id = td.vehicle_id and  tm.trip_Id=? and  te.vehicle_id = ? and tv.active_ind= ? and expense_value !=0 and if(tv.vehicle_id != 0,expense_id not in(1010,1047,1011,1049,1021),expense_id not in(1010,1047,1011,1021)) and te.expense_type not in(3,4) and tv.vend_id= 1 order by te.expense_id desc";
                //normal expenses own end

                String sql6 = "SELECT 'JV' AS txnType,@acount:=@acount+1 serial_number,if(fe.fuel_locat_id = 21,'','DC')as CHQNO, acc_code,sub_code,sum(fuelAmount) AS debit,\n"
                        + "FuelDateTime as fuel_date,\n"
                        + "concat((select replace(replace(replace(rg.reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no rg\n"
                        + "where rg.vehicle_Id=v.vehicle_Id AND rg.active_Ind='Y'),\n"
                        + "'-',(tm.customername),'-',\n"
                        + "(select city_name from ts_cities where city_id in\n"
                        + "(select actual_point_id  from ts_trip_route_course r where  r.trip_id = tf.tripid and\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)))as narration1,\n"
                        + "concat('Slip No.',tf.slipno,' & Date ',DATE_FORMAT(tf.FuelDateTime ,'%d-%m-%Y'))as narration2,\n"
                        + "(select replace(replace(replace(reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no r where r.vehicle_Id=v.vehicle_Id AND r.active_Ind='Y')as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from\n"
                        + "papl_emp_master em where em.emp_Id=d.emp_Id)as DriverLc,\n"
                        + "tf.slipno,fe.bunk_name,\n"
                        + "DATE_FORMAT(tf.FuelDateTime ,'%d-%m-%Y') as slipDate\n"
                        + "FROM ra_fuel_location_master_epos fe,\n"
                        + "ra_trip_fuel_epos tf,ts_trip_driver d ,ts_trip_vehicle v ,ts_trip_master tm\n"
                        + "where fe.fuel_locat_id = tf.fuel_locat_id and v.trip_id = tf.tripid\n"
                        + "and tm.trip_id =tf.tripid and d.trip_id = tf.tripid\n"
                        + "and d.vehicle_id = tf.vehicle_id and d.vehicle_id = v.vehicle_id  and  tf.tripid=?  and  d.vehicle_id = ? and tf.fuel_locat_id !=21 group by tf.fuel_locat_id"; //Fuel

                String sql7 = "SELECT te.expense_id as expenseId,@acount:=@acount+1 serial_number,'' as CHQNO,'JV' AS txnType,te.trip_Id,(te.expense_date) as expense_date,\n"
                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
                        + "erp_id_tuti As AccCodeTuti,erp_subcode_tuti As subCodeTuti, \n"
                        + "concat(replace(replace(replace(reg_No,' ',''),'-',''),' ',''),\n"
                        + "'-',(tm.customername),'-',\n"
                        + "(select city_name from ts_cities where city_id in\n"
                        + "(select actual_point_id  from ts_trip_route_course r where\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)))as narration1,\n"
                        + "(select concat('Slip No.',tf.slipno,' & Date ',DATE_FORMAT(tf.FuelDateTime ,'%d-%m-%Y')) from ra_trip_fuel_epos tf \n"
                        + "where tf.tripid = tm.trip_id limit 1)as narration2,\n"
                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
                        + "as DriverLc\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n"
                        + "AND tm.destination_Id=tc.city_Id and td.vehicle_id = te.vehicle_id and tm.trip_Id=? and  te.vehicle_id = ?  and expense_value !=0 and expense_id in(1047) and te.expense_type not in(3,4) order by te.expense_id desc";

                String sql9 = "SELECT te.expense_id as expenseId,@acount:=@acount+1 serial_number,'' as CHQNO,'JV' AS txnType,te.trip_Id,(te.expense_date) as expense_date,\n"
                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
                        + "erp_id_tuti As AccCodeTuti,erp_subcode_tuti As subCodeTuti, \n"
                        + "CONCAT('From ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' To ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y')) AS narration2,\n"
                        + "concat(replace(replace(replace(reg_No,' ',''),'-',''),' ',''),\n"
                        + "'-',(tm.customername),'-',\n"
                        + "(select city_name from ts_cities where city_id in\n"
                        + "(select actual_point_id  from ts_trip_route_course r where\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)))as narration1,\n"
                        + "CONCAT('BEING TRLR ',SUBSTRING(reg_no,-4,4),' ', \n"
                        + "(select city_name from ts_cities where city_id in \n"
                        + "(select point_id  from ts_trip_route_course r where\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)),' TRIP EXP PAID FOR ',DATEDIFF(tm.trip_actual_end_date, tm.trip_actual_start_date),' DAYS ON: ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'))as narration1Tuti,\n"                           
                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
                        + "as DriverLc\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n"
                        + "AND tm.destination_Id=tc.city_Id and td.vehicle_id = te.vehicle_id and expense_value !=0 and expense_id in(1011) and tm.trip_Id=? and  te.vehicle_id = ? and te.expense_type not in(3,4) order by te.expense_id desc";// Fastatg

                String sql14 = "SELECT 'JV' AS txnType,@acount:=@acount+1 serial_number,'DR'as CHQNO,'' acc_code,'' as sub_code,recovery_amount AS debit,\n"
                        + " FuelDateTime as fuel_date,\n"
                        + "concat((select replace(replace(replace(rg.reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no rg\n"
                        + "where rg.vehicle_Id=v.vehicle_Id AND rg.active_Ind='Y'),\n"
                        + "'-',(tm.customername),'-',\n"
                        + "ifnull((select group_concat(point_name order by point_sequence SEPARATOR '-') from ts_trip_route_course x where x.trip_id = tm.trip_id\n"
                        + "and x.point_type in ('Loading','UnLoading','Loading Point','UnLoading Point','PickUp','Drop')),concat(UPPER(tm.routeinfo))))as narration1,\n"
                        + "concat(recovery_fuel,'@',round(recovery_amount/recovery_fuel,2))as narration2,\n"
                        + "(select replace(replace(replace(reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no r where r.vehicle_Id=v.vehicle_Id AND r.active_Ind='Y')as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from\n"
                        + "papl_emp_master em where em.emp_Id=d.emp_Id)as DriverLc,'1036' as expenseId\n"
                        + "FROM ts_trip_fuel_recovery tf,ts_trip_driver d ,ts_trip_vehicle v ,ts_trip_master tm,\n"
                        + "ra_trip_fuel_epos f \n"
                        + "where v.trip_id = tf.trip_id and tf.trip_id =f.tripId\n"
                        + "and tm.trip_id =tf.trip_id and d.trip_id = tf.trip_id\n"
                        + "and d.vehicle_id = tf.vh_id and d.vehicle_id = v.vehicle_id and recovery_amount !=0 and  tf.trip_id= ? \n"
                        + "and tf.vh_id = ? and v.active_ind = ? group by tf.trip_id"; // fuelRecovery

                String sql5 = "insert into Transactions\n"
                        + "(TRXTYP,SRLNO,DOCDATE,ACCCODE,SUBCODE,NARRATION1,NARRATION2,DEBIT,CREDIT,CompanyCode,TrlJobId,post_ind,VehicleNo,licenseNo,CHQNO,expense_id,company_id) Values\n"
                        + "(?,?,?,?,?,?,?,?,?,?,?,0,?,?,?,?,?)";

                String sql8 = "select sub_code from ra_fuel_location_master_epos where sub_Code is not null and sub_code = ?";

                String sql10 = "select trljobId from Dkt where trljobId = ?";

                String sql11 = "UPDATE \n"
                        + "ts_trip_vehicle tv \n"
                        + "SET dkt_status = ? \n"
                        + "WHERE \n"
                        + "trip_id = ? and tv.active_ind = ?";

                String sql12 = "update Dkt set \n"
                        + "JobStartDate = ?,JobEndDate= ?,CityFromCode= ?,CityToCode= ?,Distance= ?,Owned_Hired= ?,HiredVehicleNo= ?,HireCharges= ?,Partyname= ?,AccCode= ?,\n"
                        + "DieselSupplies= ?,DieselConsumed= ?,OpeningKM= ?,ClosingKM= ?,TotalKMRun= ?,Size_20= ?,Size_40= ?,Size_40HC= ?,Size_FT= ?,Size_OT= ?,\n"
                        + "NetWt= ?,Closed= ?,CompanyCode= ?,BrnCd= ?,RUNDATE= ?,JobReachedDate= ?,JobDate= ?,DieselRecovered= ?,IsExport= ?,TrlJobId= ?,TrailerCode= ?, ConsigneeCode= ?, DriverCode= ?, CleanerCode= ?,  CityReturnedCode= ?,SubCode= ?,\n"
                        + "InvNo= ?, InvAmount= ?,FactoryHaltFromDate= ?,FactoryHaltToDate= ?,ChennaiHaltFromDate= ?,ChennaiHaltToDate= ?,\n"
                        + "MofussilHaltingDays= ?,LocalHaltingDays= ?,moffisalhalting_amt= ?,localhalting_amt= ?,post_ind =1,TrpEndORClsr = 1, \n"
                        + "HaltingDays= ?,HaltCntrctAmnt= ?,TrpInvType= ?,tripType= ?,GrossWt= ?,JobNo= ?,BESBNo= ?,BLBKGNo= ?,divCharge= ?,OTCharge= ?,MovType= ?,WeigAmnt= ?,LOLOAmnt = ?,LoadAmt= ? ,TollCharge=?,OtherCharge=?,OtherTUT=?,company_id= ?\n"
                        + "where TrlJobId = ?";

                String sql13 = "UPDATE \n"
                        + "ts_trip_vehicle tv \n"
                        + "SET dkt_status = ? \n"
                        + "WHERE \n"
                        + "trip_id = ? and tv.active_ind = ?";

                String sql15 = "SELECT vir_id,vhd.movType,vh.invoice_no as venInvNo,DATE_FORMAT(vh.invoice_date,'%Y%m%d') as venInvDate,te.expense_id as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,\n"
                        + "ifnull(tv.tripIds,te.trip_Id)as tripId,(te.expense_date) as expense_date,\n"
                        + "hire_erp_Id AS AccCode,hire_erp_subCode AS subCode,\n"
                        + "hire_erp_id_tuti As AccCodeTuti,hire_erp_subcode_tuti As subCodeTuti,\n"
                        + "hire_erp_id_coc As AccCodeCoc,hire_erp_subcode_coc As subCodeCoc,\n"
                        + "Hire_Mof_Erp_Id_che As haltMofChe,Hire_Loc_Erp_Id_che As haltLocChe, \n"
                        + "Hire_Mof_20_Erp_Id_tuti As haltMof20Tuti,Hire_Mof_40_Erp_Id_tuti As haltMof40Tuti, \n"
                        + "Hire_Loc_20_Erp_Id_tuti As haltLoc20Tuti,Hire_Loc_40_Erp_Id_tuti As haltLoc40Tuti, \n"
                        + "CONCAT(tm.customername ,'- ',tm.routeinfo) AS narration1,\n"
                        + "CONCAT('',if(expense_type = 1 and expense_id = 1033,'RTO expense',alise_Name)) AS narration2,\n"
                        + "expense_value AS debit,\n"
                        + "tv.regno as reg_no,\n"
                        + "'' as DriverLc,\n"
                        + "'' as slipno,\n"
                        + "'' as slipDate,vm.vendor_name\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_trip_vehicle tv,papl_vendor_master vm,\n"
                        + "finance_vendor_invoice_master vh,finance_vendor_invoice_hire vhd,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em \n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id \n"
                        + "and vh.vir_id= vhd.invoiceid and tv.trip_vehicle_id = vhd.tripIds \n"
                        + "and vm.vendor_id =vh.vendorid and vm.active_ind='Y'\n"
                        + "AND te.trip_Id=tv.trip_Id AND tm.trip_Id=tv.trip_Id\n"
                        + "and  vir_id = ? \n"
                        + "and expense_value !=0 and expense_id not in(1010,1047,1011,1021,1049,1050)\n"
                        + "and tv.vend_id != 1 and te.expense_type !=4 order by te.expense_id desc";

                //normal expenses HIRE end
                String sql16 = "insert into Transactions\n"
                        + "(TRXTYP,SRLNO,DOCDATE,ACCCODE,SUBCODE,NARRATION1,NARRATION2,DEBIT,CREDIT,CompanyCode,TrlJobId,post_ind,VehicleNo,licenseNo,CHQNO,expense_id,company_id,venInvNo,venInvDate,tripType,SlipNo,SlipDate) Values \n"
                        + "(?,?,?,?,?,?,?,?,?,?,?,0,?,?,?,?,?,?,?,?,?,?)";

                String sql17 = "SELECT vhd.movType,vh.invoice_no as venInvNo,DATE_FORMAT(vh.invoice_date,'%Y%m%d') as venInvDate,0 as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,\n"
                        + "ifnull(tv.tripIds,tv.trip_Id)as tripId,(vh.invoice_date) as expense_date,\n"
                        + "'' AS AccCode,'' AS subCode,\n"
                        + "'' As AccCodeTuti,'' As subCodeTuti,\n"
                        + "'' As AccCodeCoc,'' As subCodeCoc,\n"
                        + "CONCAT(tm.customername ,'- ',tm.routeinfo) AS narration1,\n"
                        + "CONCAT(tm.customername ,'- ',tm.routeinfo) AS narration2,\n"
                        + "vhd.invoice_amount AS debit,tv.regno as reg_no,\n"
                        + "'' as DriverLc,\n"
                        + "'' as slipno,\n"
                        + "'' as slipDate,vm.vendor_name\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_trip_vehicle tv,\n"
                        + "finance_vendor_invoice_master vh,finance_vendor_invoice_hire vhd,papl_vendor_master vm\n"
                        + "WHERE tm.trip_Id=tv.trip_Id and vhd.trip_id=tv.trip_id and vm.vendor_id =vh.vendorid and vm.active_ind='Y'\n"
                        + "and vh.vir_id= vhd.invoiceid and tv.trip_vehicle_id = vhd.tripIds\n"
                        + "and vir_id=? group by vhd.tripIds"; //hire contract 

                String sql18 = "SELECT v.tripIds as tripId,concat('Liters - ',Liters)as Liters,vir_id,vhd.movType,vh.invoice_no as venInvNo,DATE_FORMAT(vh.invoice_date,'%Y%m%d') as venInvDate,'JV' AS txnType,@acount:=@acount+1 serial_number,if(fe.fuel_locat_id = 21,'','DC')as CHQNO, acc_code,sub_code,\n"
                        + "(fuelAmount) AS debit,\n"
                        + "FuelDateTime as fuel_date, \n"
                        + "concat((select replace(replace(replace(rg.reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no rg \n"
                        + "where rg.vehicle_Id=v.vehicle_Id AND rg.active_Ind='Y'), \n"
                        + "'-',(tm.customername),'-',\n"
                        + "(select city_name from ts_cities where city_id in \n"
                        + "(select actual_point_id  from ts_trip_route_course r where  r.trip_id = tf.tripid and\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)))as narration1,\n"
                        + "concat('Slip No.',tf.slipno,' & Date ',DATE_FORMAT(tf.FuelDateTime ,'%d-%m-%Y'))as narration2, \n"
                        + "v.regno as reg_no,\n"
                        + "'' as DriverLc,vm.erp_id,\n"
                        + "tf.slipno,\n"
                        + "DATE_FORMAT(tf.FuelDateTime ,'%d-%m-%Y') as slipDate,fe.bunk_name,vm.vendor_name\n"
                        + "FROM ra_fuel_location_master_epos fe,\n"
                        + "ra_trip_fuel_epos tf,ts_trip_vehicle v ,ts_trip_master tm ,\n"
                        + "finance_vendor_invoice_master vh,finance_vendor_invoice_hire vhd,papl_vendor_master vm\n"
                        + "where fe.fuel_locat_id = tf.fuel_locat_id and v.trip_id = tf.tripid \n"
                        + "and tm.trip_id =tf.tripid and vm.vendor_id =vh.vendorid and vm.active_ind='Y'\n"
                        + "and vh.vir_id= vhd.invoiceid and v.trip_vehicle_id = vhd.tripIds\n"
                        + "and vir_id=? \n"
                        + "and tf.fuel_locat_id !=21 group by v.trip_vehicle_id,tf.fuel_locat_id"; //Fuel Hire

                String sql19 = "SELECT vhd.movType,vh.invoice_no as venInvNo,DATE_FORMAT(vh.invoice_date,'%Y%m%d') as venInvDate,0 as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,\n"
                        + "ifnull(tv.tripIds,tv.trip_Id)as tripId,(vh.invoice_date) as expense_date,\n"
                        + "'' AS AccCode,'' AS subCode,\n"
                        + "'' As AccCodeTuti,'' As subCodeTuti,\n"
                        + "'' As AccCodeCoc,'' As subCodeCoc,\n"
                        + "CONCAT(tm.customername ,'- ',tm.routeinfo) AS narration1,\n"
                        + "'Container Insurance payable' AS narration2,\n"
                        + "vh.cont_Insurance_Amt AS debit,tv.regno as reg_no,\n"
                        + "'' as DriverLc,vm.erp_Id,\n"
                        + "'' as slipno,\n"
                        + "'' as slipDate,vm.vendor_name\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_trip_vehicle tv,\n"
                        + "finance_vendor_invoice_master vh,finance_vendor_invoice_hire vhd,papl_vendor_master vm\n"
                        + "WHERE tm.trip_Id=tv.trip_Id and vhd.trip_id=tv.trip_id and vm.vendor_id =vh.vendorid and vm.active_ind='Y'\n"
                        + "and vh.vir_id= vhd.invoiceid and tv.trip_vehicle_id = vhd.tripIds and vh.cont_Insurance_Amt !=0 \n"
                        + "and vir_id=? group by vh.vir_id"; //hire Container Insurance payable 

                String sql20 = "SELECT vhd.movType,vh.invoice_no as venInvNo,DATE_FORMAT(vh.invoice_date,'%Y%m%d') as venInvDate,0 as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,\n"
                        + "ifnull(tv.tripIds,tv.trip_Id)as tripId,(vh.invoice_date) as expense_date,\n"
                        + "'' AS AccCode,'' AS subCode,\n"
                        + "'' As AccCodeTuti,'' As subCodeTuti,\n"
                        + "'' As AccCodeCoc,'' As subCodeCoc,\n"
                        + "'' AS narration1,\n"
                        + "'Vendor Credit' AS narration2,\n"
                        + "vh.total_amount AS debit,tv.regno as reg_no,\n"
                        + "'' as DriverLc,vm.erp_Id,\n"
                        + "'' as slipno,\n"
                        + "'' as slipDate,vm.vendor_name\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_trip_vehicle tv,\n"
                        + "finance_vendor_invoice_master vh,finance_vendor_invoice_hire vhd,papl_vendor_master vm\n"
                        + "WHERE tm.trip_Id=tv.trip_Id and vhd.trip_id=tv.trip_id and vm.vendor_id =vh.vendorid and vm.active_ind='Y' \n"
                        + "and vh.vir_id= vhd.invoiceid and tv.trip_vehicle_id = vhd.tripIds\n"
                        + "and vir_id=?  group by vh.vir_id"; //hire getHireTotalCreditTxnDetails

                String sql21 = "SELECT erp_id,tds_erp_id,ifnull(vh.total_amount,0)as total_amount,ifnull(vh.tds_percent,0)as tds_percent,ifnull(vh.tds_amount,0)as tds_amount,vhd.movType,vh.invoice_no as venInvNo,DATE_FORMAT(vh.invoice_date,'%Y%m%d') as venInvDate,0 as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,\n"
                        + "ifnull(tv.tripIds,tv.trip_Id)as tripId,(vh.invoice_date) as expense_date,\n"
                        + "'' AS AccCode,'' AS subCode,\n"
                        + "'' As AccCodeTuti,'' As subCodeTuti,\n"
                        + "'' As AccCodeCoc,'' As subCodeCoc,\n"
                        + "'' AS narration1,\n"
                        + "concat('TDS @',vh.tds_percent,'%') AS narration2,\n"
                        + "vh.total_amount AS debit,tv.regno as reg_no,\n"
                        + "'' as DriverLc,vm.erp_Id,\n"
                        + "'' as slipno,\n"
                        + "'' as slipDate,vm.vendor_name\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_trip_vehicle tv,\n"
                        + "finance_vendor_invoice_master vh,finance_vendor_invoice_hire vhd,papl_vendor_master vm\n"
                        + "WHERE tm.trip_Id=tv.trip_Id and vhd.trip_id=tv.trip_id and vm.vendor_id =vh.vendorid and vm.active_ind='Y'\n"
                        + "and vh.vir_id= vhd.invoiceid and tv.trip_vehicle_id = vhd.tripIds and vh.tds_amount !=0\n"
                        + "and vir_id=? group by vh.vir_id"; //hire getHireTxnTDS

                String sql22 = "UPDATE \n"
                        + "finance_vendor_invoice_master vh \n"
                        + "SET txn_status = ? \n"
                        + "WHERE \n"
                        + "vir_id = ?";

                String sql23 = "insert into Transactions\n"
                        + "(TRXTYP,SRLNO,DOCDATE,ACCCODE,SUBCODE,NARRATION1,NARRATION2,DEBIT,CREDIT,CompanyCode,TrlJobId,post_ind,VehicleNo,licenseNo,CHQNO,expense_id,company_id,SlipNo,SlipDate) Values\n"
                        + "(?,?,?,?,?,?,?,?,?,?,?,0,?,?,?,?,?,?,?)";

                getDKTDetails = con.prepareStatement(sql);
                insertDKTDetails = con.prepareStatement(sql1);
                getContDetails = con.prepareStatement(sql2);
                insertContDetails = con.prepareStatement(sql3);
                getTxnDetails = con.prepareStatement(sql4);
                getTxnFuelDetails = con.prepareStatement(sql6);
                getTxnFuelCash = con.prepareStatement(sql7);
                getTxnFastTag = con.prepareStatement(sql9);
                getTxnFuelRecovery = con.prepareStatement(sql14);
                getTxnFuelSubCodeList = con.prepareStatement(sql8);
                insertTxnDetails = con.prepareStatement(sql5);
                checkDktStatus = con.prepareStatement(sql10);
                updateTripDktStatus = con.prepareStatement(sql11);
                updateDKTDetails = con.prepareStatement(sql12);
                updateTripDktStatus1 = con.prepareStatement(sql13);
                getHireTxnDetails = con.prepareStatement(sql15);
                insertHireTxnDetails = con.prepareStatement(sql16);
                getHireCntrctTxnDetails = con.prepareStatement(sql17);
                getHireTxnFuelDetails = con.prepareStatement(sql18);
                getHireTxnContainerInsDetails = con.prepareStatement(sql19);
                getHireTotalCreditTxnDetails = con.prepareStatement(sql20);
                getHireTxnTDS = con.prepareStatement(sql21);
                updateTxnStatus = con.prepareStatement(sql22);
                insertFuelTxnDetails = con.prepareStatement(sql23);

                System.out.println("Posting tripId=getDKTDetails==" + tripId);
                System.out.println("Posting vehicleId=getDKTDetails==" + vehicleId);
                System.out.println("Posting check Tripids" + tripIds);
                int status = 0;
                int movementType = 0;
                String checkDkt = null;
                try {
                    System.out.println("FIRST---" + tripId);
                    if (tripId != null) {
                        checkDktStatus.setString(1, tripIds);
                        rs1 = checkDktStatus.executeQuery();
                        while (rs1.next()) {
                            checkDkt = rs1.getString("trljobId");
                            System.out.println("checkDktONE" + checkDkt);
                        }
                        System.out.println("checkDkt@@@@" + checkDkt);
//                        if ("12".equals(statusId) || checkDkt == null) {
                        if (checkDkt == null) {
                            System.out.println("inside checkDkt null");
                            System.out.println("getDKTDetails---$$$$--trip end---" + tripId + "----" + vehicleId + "----" + compId);
                            getDKTDetails.setString(1, tripId);
                            getDKTDetails.setString(2, vehicleId);
                            getDKTDetails.setString(3, compId);
                            getDKTDetails.setString(4, vendId);
                            getDKTDetails.setString(5, actStatus);
                            rs = getDKTDetails.executeQuery();
                            while (rs.next()) {
                                System.out.println("getDKTDetails--insert-@@@@-----" + tripId + "----" + vehicleId + "----" + compId);
                                movementType = rs.getInt("movement");
                                insertDKTDetails.setString(1, rs.getString("jobstartdate"));
                                insertDKTDetails.setString(2, rs.getString("jobenddate"));
                                insertDKTDetails.setString(3, rs.getString("cityfromcode"));
                                insertDKTDetails.setString(4, rs.getString("citytocode"));
                                insertDKTDetails.setString(5, rs.getString("distance"));
                                insertDKTDetails.setString(6, rs.getString("ownedhired"));
                                insertDKTDetails.setString(7, rs.getString("hiredvehicleno"));
                                insertDKTDetails.setString(8, rs.getString("hireCharges"));
                                insertDKTDetails.setString(9, rs.getString("partyName"));
                                insertDKTDetails.setString(10, "14001");
                                insertDKTDetails.setString(11, rs.getString("dieselsupplies"));
                                insertDKTDetails.setString(12, rs.getString("dieselConsumed"));
                                insertDKTDetails.setString(13, rs.getString("openingkm"));
                                insertDKTDetails.setString(14, rs.getString("closingkm"));
                                insertDKTDetails.setString(15, rs.getString("totalkmrun"));
                                insertDKTDetails.setString(16, rs.getString("size_20"));
                                insertDKTDetails.setString(17, rs.getString("size_40"));
                                insertDKTDetails.setString(18, rs.getString("size_40HC"));
                                insertDKTDetails.setString(19, rs.getString("size_40FT"));
                                insertDKTDetails.setString(20, rs.getString("size_40OT"));
                                insertDKTDetails.setString(21, rs.getString("netwt"));
                                insertDKTDetails.setString(22, "1");
                                insertDKTDetails.setString(23, "1");
                                insertDKTDetails.setString(24, "1");
                                insertDKTDetails.setString(25, rs.getString("runDate"));
                                insertDKTDetails.setString(26, rs.getString("jobenddate"));
                                insertDKTDetails.setString(27, rs.getString("jobenddate"));
                                insertDKTDetails.setString(28, rs.getString("dieselRecovered"));
                                insertDKTDetails.setString(29, rs.getString("isExport"));
                                insertDKTDetails.setString(30, rs.getString("trip_Id"));

                                insertDKTDetails.setString(31, rs.getString("trailerCode"));
                                insertDKTDetails.setString(32, rs.getString("AccCode"));    // consigneecode
                                insertDKTDetails.setString(33, rs.getString("DriverCode"));
                                insertDKTDetails.setString(34, rs.getString("CleanerCode"));
                                insertDKTDetails.setString(35, rs.getString("cityReturncode"));

                                if ("1461".equals(compId)) {  // chennai
                                    if ("BOS".equals(rs.getString("TrpInvType"))) {
                                        insertDKTDetails.setString(36, "DC028");  //  Subcode chennai
                                    } else {
                                        insertDKTDetails.setString(36, "DC001"); //   Subcode chennai
                                    }
                                }

                                if ("1470".equals(compId)) {  // tutti
                                    if ("BOS".equals(rs.getString("TrpInvType"))) {
                                        insertDKTDetails.setString(36, "DC026");  //  Subcode tutti      chakiat 
                                    } else {
                                        insertDKTDetails.setString(36, "DC024"); //   Subcode tutti      ca log pvt 
                                    }
                                }
                                
                                if ("1472".equals(compId)) {  // COC
                                    if ("BOS".equals(rs.getString("TrpInvType"))) {
                                        insertDKTDetails.setString(36, rs.getString("AccCode"));  //  Subcode coc
                                    } else {
                                        insertDKTDetails.setString(36, rs.getString("AccCode")); //   Subcode coc
                                    }
                                }

                                insertDKTDetails.setString(37, rs.getString("invoice_No"));
                                insertDKTDetails.setString(38, rs.getString("invoice_value"));
                                System.out.println("movementType-DKT--" + movementType);

                                insertDKTDetails.setString(39, rs.getString("haltingFrom"));  // factory 
                                insertDKTDetails.setString(40, rs.getString("haltingTo"));
                                insertDKTDetails.setString(41, rs.getString("clrFrom"));
                                insertDKTDetails.setString(42, rs.getString("clrTo"));

                                insertDKTDetails.setString(43, rs.getString("halt_days"));   //moffusill haltingDay
                                insertDKTDetails.setString(44, rs.getString("clr_days"));         //local haltingDay                  

                                insertDKTDetails.setString(45, rs.getString("haltingAmt"));      //moffusill amount
                                insertDKTDetails.setString(46, rs.getString("clrAmt"));                      //local amount

                                insertDKTDetails.setString(47, rs.getString("halt_days"));   //factory moffusil days
                                insertDKTDetails.setString(48, rs.getString("HaltCntrctAmnt")); //factory moffusil amount

                                insertDKTDetails.setString(49, rs.getString("TrpEndORClsr"));
                                insertDKTDetails.setString(50, rs.getString("TrpInvType"));
                                String tripType = "";
                                String tuttiTripType = "";
                                if ("1461".equals(compId)) {  // chennai
                                    insertDKTDetails.setString(51, rs.getString("tripType"));
                                }
                                if ("1470".equals(compId)) {  // tutti
                                    System.out.println("tuttiTripType-BEFORE--" + rs.getString("tripType"));
                                    if ("MOFUSSIL".equals(rs.getString("tripType"))) {
                                        tuttiTripType = rs.getString("tripType").replace("MOFUSSIL", "");
                                        System.out.println("tuttiTripType--after -" + tuttiTripType);
                                    } else {
                                        tuttiTripType = rs.getString("tripType");
                                    }
                                    if (!"1".equals(rs.getString("vend_Id"))) {
                                        tripType = "ON HIRE " + tuttiTripType;
                                    } else {
                                        tripType = tuttiTripType;
                                    }
                                    insertDKTDetails.setString(51, tripType);
                                }

                                if ("1472".equals(compId)) {  // COC
                                    insertDKTDetails.setString(51, rs.getString("tripType"));
                                }

                                insertDKTDetails.setString(52, rs.getString("grosswt"));

                                insertDKTDetails.setString(53, rs.getString("JobNo"));
                                insertDKTDetails.setString(54, rs.getString("BESBNo"));
                                insertDKTDetails.setString(55, rs.getString("BLBKGNo"));
                                insertDKTDetails.setString(56, rs.getString("divertCharge"));
                                insertDKTDetails.setString(57, rs.getString("oTCharges"));
                                insertDKTDetails.setString(58, rs.getString("MovType"));
                                insertDKTDetails.setString(59, compId);
                                insertDKTDetails.setString(60, rs.getString("weighAmt"));
                                insertDKTDetails.setString(61, rs.getString("LOLOAmnt"));
                                insertDKTDetails.setString(62, rs.getString("LoadAmt"));
                                insertDKTDetails.setString(63, rs.getString("TollCharge"));
                                
                                if ("1470".equals(compId)) {  // tuti
                                    insertDKTDetails.setString(64, rs.getString("OtherChargeTuti"));
                                    insertDKTDetails.setString(65, rs.getString("OtherExpenseTuti"));
                                }else{
                                    insertDKTDetails.setString(64, rs.getString("OtherCharge"));
                                    insertDKTDetails.setString(65, "0");
                                }
                                
                                System.out.println("inside insert Dkt");
                                status = insertDKTDetails.executeUpdate();
                                System.out.println("DKT=$$$$ insert status--" + status);
//                            }

                                getContDetails.setString(1, tripId);
                                getContDetails.setString(2, vehicleId);
                                getContDetails.setString(3, vendId);
                                getContDetails.setString(4, actStatus);
                                rs1 = getContDetails.executeQuery();
                                while (rs1.next()) {
                                    System.out.println("entered insert containers----" + tripId);
                                    insertContDetails.setString(1, rs1.getString("serial_number"));
                                    insertContDetails.setString(2, rs1.getString("type_name"));
                                    insertContDetails.setString(3, rs1.getString("container_no"));
                                    insertContDetails.setString(4, rs1.getString("sealNo"));
                                    insertContDetails.setString(5, "1");
                                    insertContDetails.setString(6, "1");
                                    insertContDetails.setString(7, rs1.getString("vehicleName"));
                                    insertContDetails.setString(8, rs1.getString("trip_Id"));

                                    insertContDetails.setString(9, "0");
                                    insertContDetails.setString(10, rs1.getString("RunDate"));
                                    insertContDetails.setString(11, compId);

                                    status = insertContDetails.executeUpdate();
                                    System.out.println("Container= " + status);
                                }
                            }

//                            checkDktStatus.setString(1, tripIds);
//                            rs1 = checkDktStatus.executeQuery();
//                            while (rs1.next()) {
//                            updateTripDktStatus.setString(1, tripId);
//                            updateTripDktStatus.setString(2, actStatus);
//                            if ("1".equals(vendId)) {
                            updateTripDktStatus.setString(1, "1");
                            updateTripDktStatus.setString(2, tripId);
                            updateTripDktStatus.setString(3, actStatus);
//                            } else {
//                                updateTripDktStatus.setString(1, "4");
//                                updateTripDktStatus.setString(2, tripId);
//                                updateTripDktStatus.setString(3, actStatus);
//                            }
                            status = updateTripDktStatus.executeUpdate();

                            System.out.println("updateTripDktStatus= 12 status " + status);
//                            }

                        } else if (checkDkt != null) {  // trip settled
                            System.out.println("inside checkDkt not null");
                            if ("13".equals(statusId) || "14".equals(statusId)) {  // trip Closure or settlemet
                                System.out.println("getDKTDetails-- trip Closure or settlemet -$$$$-----" + tripId + "----" + vehicleId + "----" + compId + "  --statusId====" + statusId + "vendId====" + vendId);
                                getDKTDetails.setString(1, tripId);
                                getDKTDetails.setString(2, vehicleId);
                                getDKTDetails.setString(3, compId);
                                getDKTDetails.setString(4, vendId);
                                getDKTDetails.setString(5, actStatus);
                                rs = getDKTDetails.executeQuery();
                                while (rs.next()) {
                                    System.out.println("getDKTDetails--update -@@@@-----" + tripId + "----" + vehicleId + "----" + compId);
                                    movementType = rs.getInt("movement");
                                    updateDKTDetails.setString(1, rs.getString("jobstartdate"));
                                    updateDKTDetails.setString(2, rs.getString("jobenddate"));
                                    updateDKTDetails.setString(3, rs.getString("cityfromcode"));
                                    updateDKTDetails.setString(4, rs.getString("citytocode"));
                                    updateDKTDetails.setString(5, rs.getString("distance"));
                                    updateDKTDetails.setString(6, rs.getString("ownedhired"));
                                    updateDKTDetails.setString(7, rs.getString("hiredvehicleno"));
                                    updateDKTDetails.setString(8, rs.getString("hireCharges"));
                                    updateDKTDetails.setString(9, rs.getString("partyName"));
                                    updateDKTDetails.setString(10, "14001");
                                    updateDKTDetails.setString(11, rs.getString("dieselsupplies"));
                                    updateDKTDetails.setString(12, rs.getString("dieselConsumed"));
                                    updateDKTDetails.setString(13, rs.getString("openingkm"));
                                    updateDKTDetails.setString(14, rs.getString("closingkm"));
                                    updateDKTDetails.setString(15, rs.getString("totalkmrun"));
                                    updateDKTDetails.setString(16, rs.getString("size_20"));
                                    updateDKTDetails.setString(17, rs.getString("size_40"));
                                    updateDKTDetails.setString(18, rs.getString("size_40HC"));
                                    updateDKTDetails.setString(19, rs.getString("size_40FT"));
                                    updateDKTDetails.setString(20, rs.getString("size_40OT"));
                                    updateDKTDetails.setString(21, rs.getString("netwt"));
                                    updateDKTDetails.setString(22, "1");
                                    updateDKTDetails.setString(23, "1");
                                    updateDKTDetails.setString(24, "1");
                                    updateDKTDetails.setString(25, rs.getString("runDate"));
                                    updateDKTDetails.setString(26, rs.getString("jobenddate"));
                                    updateDKTDetails.setString(27, rs.getString("jobenddate"));
                                    updateDKTDetails.setString(28, rs.getString("dieselRecovered"));
                                    updateDKTDetails.setString(29, rs.getString("isExport"));
                                    updateDKTDetails.setString(30, rs.getString("trip_Id"));

                                    updateDKTDetails.setString(31, rs.getString("trailerCode"));
                                    updateDKTDetails.setString(32, rs.getString("AccCode"));
                                    updateDKTDetails.setString(33, rs.getString("DriverCode"));
                                    updateDKTDetails.setString(34, rs.getString("CleanerCode"));
                                    updateDKTDetails.setString(35, rs.getString("cityReturncode"));

                                    if ("1461".equals(compId)) {  // chennai
                                        if ("BOS".equals(rs.getString("TrpInvType"))) {
                                            updateDKTDetails.setString(36, "DC028");  //  Subcode chennai
                                        } else {
                                            updateDKTDetails.setString(36, "DC001"); //   Subcode chennai
                                        }
                                    }

                                    if ("1470".equals(compId)) {  // tutti
                                        if ("BOS".equals(rs.getString("TrpInvType"))) {
                                            updateDKTDetails.setString(36, "DC026");  //  Subcode tutti
                                        } else {
                                            updateDKTDetails.setString(36, "DC024"); //   Subcode tutti
                                        }
                                    }

                                    if ("1472".equals(compId)) {  // COC
                                        if ("BOS".equals(rs.getString("TrpInvType"))) {
                                            updateDKTDetails.setString(36, rs.getString("AccCode"));  //  Subcode COC
                                        } else {
                                            updateDKTDetails.setString(36, rs.getString("AccCode")); //   Subcode COC
                                        }
                                    }
                                    updateDKTDetails.setString(37, rs.getString("invoice_No"));
                                    updateDKTDetails.setString(38, rs.getString("invoice_value"));
                                    System.out.println("movementType-DKT--" + movementType);

                                    updateDKTDetails.setString(39, rs.getString("haltingFrom"));  // factory 
                                    updateDKTDetails.setString(40, rs.getString("haltingTo"));
                                    updateDKTDetails.setString(41, rs.getString("clrFrom"));
                                    updateDKTDetails.setString(42, rs.getString("clrTo"));

                                    updateDKTDetails.setString(43, rs.getString("halt_days"));   //moffusill haltingDay
                                    updateDKTDetails.setString(44, rs.getString("clr_days"));         //local haltingDay                  

                                    updateDKTDetails.setString(45, rs.getString("haltingAmt"));      //moffusill amount
                                    updateDKTDetails.setString(46, rs.getString("clrAmt"));                      //local amount

                                    updateDKTDetails.setString(47, rs.getString("halt_days"));   //factory moffusil days
                                    updateDKTDetails.setString(48, rs.getString("HaltCntrctAmnt")); //factory moffusil amount

                                    updateDKTDetails.setString(49, rs.getString("TrpInvType"));
                                    String tripType = "";
                                    String tuttiTripType = "";
                                    if ("1461".equals(compId)) {  // chennai
                                        updateDKTDetails.setString(50, rs.getString("tripType"));
                                    }
                                    if ("1470".equals(compId)) {  // tutti
                                        System.out.println("tuttiTripType-BEFORE--" + rs.getString("tripType"));
                                        if ("MOFUSSIL".equals(rs.getString("tripType"))) {
                                            tuttiTripType = rs.getString("tripType").replace("MOFUSSIL", "");
                                            System.out.println("tuttiTripType--after -" + tuttiTripType);
                                        } else {
                                            tuttiTripType = rs.getString("tripType");
                                        }
                                        if (!"1".equals(rs.getString("vend_Id"))) {
                                            tripType = "ON HIRE " + tuttiTripType;
                                        } else {
                                            tripType = tuttiTripType;
                                        }
                                        updateDKTDetails.setString(50, tripType);
                                    }

                                    if ("1472".equals(compId)) {  // COC
                                        updateDKTDetails.setString(50, rs.getString("tripType"));
                                    }

                                    updateDKTDetails.setString(51, rs.getString("grosswt"));

                                    updateDKTDetails.setString(52, rs.getString("JobNo"));
                                    updateDKTDetails.setString(53, rs.getString("BESBNo"));
                                    updateDKTDetails.setString(54, rs.getString("BLBKGNo"));
                                    updateDKTDetails.setString(55, rs.getString("divertCharge"));
                                    updateDKTDetails.setString(56, rs.getString("oTCharges"));
                                    updateDKTDetails.setString(57, rs.getString("MovType"));
                                    updateDKTDetails.setString(58, rs.getString("weighAmt"));
                                    updateDKTDetails.setString(59, rs.getString("LOLOAmnt"));
                                    updateDKTDetails.setString(60, rs.getString("LoadAmt"));
                                    updateDKTDetails.setString(61, rs.getString("TollCharge"));
//                                    updateDKTDetails.setString(62, rs.getString("OtherCharge"));
                                    if ("1470".equals(compId)) {  // tuti
                                        updateDKTDetails.setString(62, rs.getString("OtherChargeTuti"));
                                        updateDKTDetails.setString(63, rs.getString("OtherExpenseTuti"));
                                    }else{
                                        updateDKTDetails.setString(62, rs.getString("OtherCharge"));
                                        updateDKTDetails.setString(63, "0");
                                    }

                                    updateDKTDetails.setString(64, compId);
                                    updateDKTDetails.setString(65, tripIds);
                                    status = updateDKTDetails.executeUpdate();
                                    System.out.println("DKT=updateDKTDetails$$$$ update status--- " + status);
                                }
//                                if ("13".equals(statusId)) {
//                                    updateTripDktStatus.setString(1, tripId);
//                                    updateTripDktStatus.setString(2, actStatus);
//                                    status = updateTripDktStatus1.executeUpdate();
//                                    System.out.println("updateTripDktStatus1 - 13 status--= " + status);
//                                }
//                                if ("14".equals(statusId)) {
//                                if ("1".equals(vendId)) {
                                updateTripDktStatus1.setString(1, "2");
                                updateTripDktStatus1.setString(2, tripId);
                                updateTripDktStatus1.setString(3, actStatus);
//                                } else {
//                                    updateTripDktStatus1.setString(1, "4");
//                                    updateTripDktStatus1.setString(2, tripId);
//                                    updateTripDktStatus1.setString(3, actStatus);
//                                }

                                status = updateTripDktStatus1.executeUpdate();
                                System.out.println("updateTripDktStatus1 -13 or 14 status--= " + status);
//                                }
                            }
                            System.out.println("befor statusId@@@@--" + statusId);
                            if ("14".equals(statusId) && "1".equals(vendId)) {
                                System.out.println("statusId@@@@--" + statusId);
                                double credit = 0.00D;
                                int sno = 0;
                                int sno1 = 0;
                                String expenseDate = "";
                                String expenseDateNormal = "";
                                String CHQNO = "";
                                String lcNo = null;
                                String expenseId = "";
                                String regNo = null;
                                String accCode = null;
                                String accCode1 = null;
                                String subCode = null;
                                String subCode1 = null;
                                String narrate1 = null;
                                String narrate2 = null;
                                String tripIdSet = null;
                                getTxnDetails.setString(1, tripId);
                                getTxnDetails.setString(2, vehicleId);
                                getTxnDetails.setString(3, actStatus);
                                rs2 = getTxnDetails.executeQuery();
                                while (rs2.next()) {
                                    sno1 = sno1 + 1;
                                    regNo = rs2.getString("reg_no");
                                    lcNo = rs2.getString("DriverLc");
                                    expenseId = rs2.getString("expense_id");
                                    narrate2 = rs2.getString("narration1");
                                    System.out.println("narrate2----" + narrate2);
                                    tripIdSet = rs2.getString("trip_Id");

                                    if ("1461".equals(compId)) {  // chennai
                                        accCode = rs2.getString("AccCode");
                                        if ("70005".equals(accCode)) {
                                            if (movementType == 2 || movementType == 28) {
                                                accCode = "70009";
                                            }
                                        }
                                        if ("47010".equals(accCode)) {
                                            if (movementType == 1 || movementType == 2 || movementType == 18
                                                    || movementType == 19 || movementType == 20 || movementType == 21
                                                    || movementType == 24 || movementType == 25 || movementType == 26 || movementType == 27
                                                    || movementType == 28 || movementType == 29 || movementType == 30 || movementType == 31
                                                    || movementType == 32 || movementType == 33 || movementType == 34) {
                                                accCode = "47011";
                                            }
                                        }
                                        subCode = rs2.getString("subCode");
                                        narrate1 = rs2.getString("narration1");
                                    }

                                    if ("1470".equals(compId)) {  // tutti
                                        accCode = rs2.getString("AccCodeTuti");
                                        subCode = rs2.getString("subCodeTuti");
                                        narrate1 = rs2.getString("narration1Tuti");
                                    }
                                    if ("1472".equals(compId)) {  // coc
                                        accCode = rs2.getString("AccCodeCoc");
                                        subCode = rs2.getString("subCodeCoc");
                                        narrate1 = rs2.getString("narration1COC");
                                    }

                                    sno = rs2.getInt("serial_number");
                                    credit = credit + rs2.getDouble("debit");
                                    expenseDateNormal = rs2.getString("expense_date");
                                    System.out.println("normal EXpense------" + rs2.getString("expense_date"));
                                    insertTxnDetails.setString(1, rs2.getString("txnType"));
                                    insertTxnDetails.setString(2, rs2.getString("serial_number"));
                                    insertTxnDetails.setString(3, rs2.getString("expense_date"));
                                    insertTxnDetails.setString(4, accCode);
                                    insertTxnDetails.setString(5, subCode);
                                    insertTxnDetails.setString(6, narrate1);
                                    insertTxnDetails.setString(7, rs2.getString("narration2"));
                                    insertTxnDetails.setString(8, rs2.getString("debit"));
                                    insertTxnDetails.setString(9, "0");
                                    insertTxnDetails.setString(10, "1");
                                    insertTxnDetails.setString(11, tripIdSet);

                                    if (Integer.parseInt(expenseId) == 1030 || Integer.parseInt(expenseId) == 1039) {
                                        insertTxnDetails.setString(12, null);
                                        insertTxnDetails.setString(13, null);
                                    } else {
                                        insertTxnDetails.setString(12, regNo);
                                        insertTxnDetails.setString(13, lcNo);
                                    }
                                    insertTxnDetails.setString(14, CHQNO);
                                    insertTxnDetails.setString(15, rs2.getString("expense_id"));
                                    insertTxnDetails.setString(16, compId);
                                    regNo = rs2.getString("reg_no");
                                    status = insertTxnDetails.executeUpdate();
                                    System.out.println("insertTxnDetails= " + status);
                                }
                                
                                if(tripIdSet == null){
                                   tripIdSet =  tripId; 
                                }else{
                                   tripIdSet =  tripIdSet;
                                }
                                
                                System.out.println("tripIdSet-$$$-------"+tripIdSet);
                                
                                System.out.println("thrERPTablePosting sno======" + sno);
                                System.out.println("thrERPTablePosting credit======" + credit);

                                System.out.println("normal EXpense--credit----" + expenseDateNormal);
                                if (sno != 0) {
                                    sno = sno + 1;
                                    sno1 = sno1 + 1;
                                    System.out.println("thrERPTablePosting credit Enter======" + credit);

                                    insertTxnDetails.setString(1, "JV");
                                    insertTxnDetails.setInt(2, sno1);
                                    insertTxnDetails.setString(3, expenseDateNormal);
                                    if ("1461".equals(compId)) {              // chennai
                                        insertTxnDetails.setString(4, "15007");
                                    }
                                    if ("1470".equals(compId)) {              //tuti
                                        insertTxnDetails.setString(4, "22025");
                                    }
                                    if ("1472".equals(compId)) {             // cochin
                                        insertTxnDetails.setString(4, "14011");
                                    }
                                    insertTxnDetails.setString(5, null);
                                    insertTxnDetails.setString(6, null);
                                    insertTxnDetails.setString(7, narrate2);
                                    insertTxnDetails.setString(8, "0");
                                    insertTxnDetails.setString(9, credit + "");
                                    insertTxnDetails.setString(10, "1");
                                    insertTxnDetails.setString(11, tripIdSet);

                                    if (Integer.parseInt(expenseId) == 1030 || Integer.parseInt(expenseId) == 1039) {
                                        insertTxnDetails.setString(12, null);
                                        insertTxnDetails.setString(13, null);
                                    } else {
                                        insertTxnDetails.setString(12, regNo);
                                        insertTxnDetails.setString(13, lcNo);
                                    }
                                    insertTxnDetails.setString(14, CHQNO);
                                    insertTxnDetails.setInt(15, 1);  // 1 as notrmal other expense credit
                                    insertTxnDetails.setString(16, compId);

                                    status = insertTxnDetails.executeUpdate();
                                    System.out.println("insertTxnDetails Credit= " + status);
                                }

                                System.out.println("getTxnDetails sno======" + sno);

//                              starts getTxnFuelCash
                                getTxnFuelCash.setString(1, tripId);
                                getTxnFuelCash.setString(2, vehicleId);
                                rs3 = getTxnFuelCash.executeQuery();
                                while (rs3.next()) {
                                    if (sno == 0) {
                                        sno = Integer.parseInt(rs3.getString("serial_number"));
                                    } else {
                                        sno = sno + 1;
                                    }
                                    accCode = rs3.getString("AccCode");
                                    credit = credit + rs3.getDouble("debit");
                                    expenseDate = rs3.getString("expense_date");
                                    subCode = rs3.getString("subCode");

                                    if ("1047".equals(rs3.getString("expenseId"))) { // fuel expense driver CASH
                                        System.out.println("expenseDate------DCassh--" + expenseDate);
                                        CHQNO = "DC";
                                        accCode1 = "46019";
                                        subCode1 = "";
                                        System.out.println("Driver CASH");
                                        insertTxnDetails.setString(1, rs3.getString("txnType"));
                                        insertTxnDetails.setInt(2, 2);
                                        insertTxnDetails.setString(3, expenseDate);
                                        insertTxnDetails.setString(4, accCode1);
                                        insertTxnDetails.setString(5, subCode1);
                                        insertTxnDetails.setString(6, rs3.getString("narration1"));
                                        insertTxnDetails.setString(7, rs3.getString("narration2"));
                                        insertTxnDetails.setString(8, "0");
                                        insertTxnDetails.setString(9, rs3.getString("debit"));
                                        insertTxnDetails.setString(10, "1");
                                        insertTxnDetails.setString(11, tripIdSet);

                                        insertTxnDetails.setString(12, regNo);
                                        insertTxnDetails.setString(13, lcNo);
                                        insertTxnDetails.setString(14, CHQNO);
                                        insertTxnDetails.setString(15, rs3.getString("expenseId"));
                                        insertTxnDetails.setString(16, compId);
                                        status = insertTxnDetails.executeUpdate();
                                        System.out.println("insertTxnDetails= " + status);

                                        if (sno != 0) {
                                            sno = sno + 1;
                                            System.out.println("thrERPTablePosting 1047 credit Enter======" + credit);

                                            insertTxnDetails.setString(1, "JV");
                                            insertTxnDetails.setInt(2, 1);
                                            insertTxnDetails.setString(3, expenseDate);
                                            insertTxnDetails.setString(4, "15007");
                                            insertTxnDetails.setString(5, null);
                                            insertTxnDetails.setString(6, rs3.getString("narration1"));
                                            insertTxnDetails.setString(7, rs3.getString("narration2"));
                                            insertTxnDetails.setString(8, rs3.getString("debit"));
                                            insertTxnDetails.setString(9, "0");
                                            insertTxnDetails.setString(10, "1");
                                            insertTxnDetails.setString(11, tripIdSet);

                                            insertTxnDetails.setString(12, regNo);
                                            insertTxnDetails.setString(13, lcNo);
                                            insertTxnDetails.setString(14, CHQNO);
                                            insertTxnDetails.setInt(15, 3);  //3 as  fuel expense driver CASH  Credit
                                            insertTxnDetails.setString(16, compId);

                                            status = insertTxnDetails.executeUpdate();
                                            System.out.println("insertTxnDetails Credit= " + status);
                                        }
                                    }
                                }
//                              starts getTxnFastTag
                                String accCodeCrdt = "";
                                getTxnFastTag.setString(1, tripId);
                                getTxnFastTag.setString(2, vehicleId);
                                rs6 = getTxnFastTag.executeQuery();
                                while (rs6.next()) {
                                    if (sno == 0) {
                                        sno = Integer.parseInt(rs6.getString("serial_number"));
                                    } else {
                                        sno = sno + 1;
                                    }
                                    accCode = rs6.getString("AccCode");
                                    credit = credit + rs6.getDouble("debit");
                                    expenseDate = rs6.getString("expense_date");
                                    subCode = rs6.getString("subCode");
                                    if ("1011".equals(rs6.getString("expenseId"))) { // fast tag toll
                                        System.out.println("expenseDate---FT-" + expenseDate);
                                        CHQNO = "FT";
                                        if ("1472".equals(compId)) {
                                            accCode1 = "46010";
                                            accCodeCrdt = "16012";
                                            subCode1 = null;
                                            narrate1 = rs6.getString("narration1");
                                        }else if ("1461".equals(compId)) {
                                            accCode1 = "46097";
                                            accCodeCrdt = "22001";
                                            subCode1 = "CI008";
                                            narrate1 = rs6.getString("narration1");
                                        }else if ("1470".equals(compId)) {
                                            accCode1 = "46010";
                                            accCodeCrdt = "14019";
                                            subCode1 = null;
                                            narrate1 = rs6.getString("narration1Tuti");
                                        }
                                        regNo = rs6.getString("reg_no");
                                        lcNo = rs6.getString("DriverLc");
                                        expenseId = rs6.getString("expenseid");
                                        System.out.println("FT CRedit");
                                        insertTxnDetails.setString(1, rs6.getString("txnType"));
                                        insertTxnDetails.setInt(2, 1);
                                        insertTxnDetails.setString(3, expenseDate);
                                        insertTxnDetails.setString(4, accCode1);
                                        insertTxnDetails.setString(5, null);
                                        insertTxnDetails.setString(6, narrate1);
                                        insertTxnDetails.setString(7, rs6.getString("narration2"));
                                        insertTxnDetails.setString(8, rs6.getString("debit"));
                                        insertTxnDetails.setString(9, "0");
                                        insertTxnDetails.setString(10, "1");
                                        insertTxnDetails.setString(11, tripIdSet);

                                        insertTxnDetails.setString(12, regNo);
                                        insertTxnDetails.setString(13, lcNo);
                                        insertTxnDetails.setString(14, CHQNO);
                                        insertTxnDetails.setString(15, rs6.getString("expenseid"));
                                        insertTxnDetails.setString(16, compId);
                                        status = insertTxnDetails.executeUpdate();
                                        System.out.println("insertTxnDetails= " + status);

                                        if (sno != 0) {
                                            sno = sno + 1;
                                            System.out.println("thrERPTablePosting credit Enter======" + credit);
                                            System.out.println("thrERPTablePosting expenseDate Enter======" + expenseDate);
                                            CHQNO = "FT";
                                            insertTxnDetails.setString(1, "JV");
                                            insertTxnDetails.setInt(2, 2);
                                            insertTxnDetails.setString(3, expenseDate);
                                            insertTxnDetails.setString(4, accCodeCrdt);
                                            insertTxnDetails.setString(5, subCode1);
                                            insertTxnDetails.setString(6, narrate1);
                                            insertTxnDetails.setString(7, rs6.getString("narration2"));
                                            insertTxnDetails.setString(8, "0");
                                            insertTxnDetails.setString(9, rs6.getString("debit"));
                                            insertTxnDetails.setString(10, "1");
                                            insertTxnDetails.setString(11, tripIdSet);

                                            insertTxnDetails.setString(12, regNo);
                                            insertTxnDetails.setString(13, lcNo);
                                            insertTxnDetails.setString(14, CHQNO);
                                            insertTxnDetails.setInt(15, 2);  // 2 as fast tag toll credit
                                            insertTxnDetails.setString(16, compId);
                                            status = insertTxnDetails.executeUpdate();
                                            System.out.println("insertTxnDetails Credit= " + status);
                                        }

                                    }
                                }
                                System.out.println("getTxnFuelCashAndFastTag sno======" + sno);

                                //  starts getTxnFuelDetails  FUEL Bunk
                                String accCode2 = "";
                                String subCode2 = "";
                                String CHQNO2 = "";
                                String regNo2 = "";
                                String lcNo2 = "";
                                String narration1 = "";
                                String narration2 = "";
                                String bunkName = "";
                                getTxnFuelDetails.setString(1, tripId);
                                getTxnFuelDetails.setString(2, vehicleId);
                                System.out.println("tripId FUEL---" + tripId);
                                System.out.println("vehicleId FUEL---" + vehicleId);
                                Double credits = 0.00;
                                rs4 = getTxnFuelDetails.executeQuery();
                                int snos = 1;
                                while (rs4.next()) {
                                    snos = snos + 1;
                                    accCode2 = rs4.getString("acc_Code");
                                    CHQNO2 = rs4.getString("CHQNO");
                                    subCode2 = rs4.getString("sub_code");
                                    regNo2 = rs4.getString("reg_no");
                                    lcNo2 = rs4.getString("DriverLc");
                                     if ("1470".equals(compId)) {  // tutti
                                        narration1 = "BEING TRLR DIESEL BILLS AMOUNT PAYABLE TO '"+rs4.getString("bunk_name")+"'";
                                    }else{
                                        narration1 = rs4.getString("narration1");
                                     }
                                    narration2 = rs4.getString("narration2");
                                    credits = Double.parseDouble(rs4.getString("debit"));

                                    getTxnFuelSubCodeList.setString(1, subCode2);
                                    rs5 = getTxnFuelSubCodeList.executeQuery();
                                    while (rs5.next()) {
                                        System.out.println("RS4 subcode----" + rs5.getString("sub_code"));
                                        System.out.println("RS5 subcode----" + rs4.getString("sub_code"));
                                        insertFuelTxnDetails.setString(1, rs4.getString("txnType"));
                                        insertFuelTxnDetails.setInt(2, 2);
                                        //                              insertTxnDetails.setString(2, rs2.getString("serial_number"));
                                        insertFuelTxnDetails.setString(3, rs4.getString("fuel_date"));
                                        insertFuelTxnDetails.setString(4, accCode2);
                                        insertFuelTxnDetails.setString(5, subCode2);
                                        insertFuelTxnDetails.setString(6, narration1);
                                        insertFuelTxnDetails.setString(7, narration2);
                                        insertFuelTxnDetails.setString(8, "0");
                                        insertFuelTxnDetails.setString(9, rs4.getString("debit"));
                                        insertFuelTxnDetails.setString(10, "1");
                                        insertFuelTxnDetails.setString(11, tripIdSet);

                                        insertFuelTxnDetails.setString(12, regNo2);
                                        insertFuelTxnDetails.setString(13, lcNo2);
                                        insertFuelTxnDetails.setString(14, CHQNO2);
                                        insertFuelTxnDetails.setString(15, "1010");
                                        insertFuelTxnDetails.setString(16, compId);
                                        insertFuelTxnDetails.setString(17, rs4.getString("slipno"));
                                        insertFuelTxnDetails.setString(18, rs4.getString("slipDate"));
                                        status = insertFuelTxnDetails.executeUpdate();
                                        System.out.println("insertgetTxnFuelDetails tripid "+tripId+ " = " + status);

                                        if (snos != 0) {
                                            System.out.println("thrERPTablePosting 1010 FUEL credit Enter======" + credits);
                                            insertFuelTxnDetails.setString(1, "JV");
                                            insertFuelTxnDetails.setInt(2, 1);
                                            insertFuelTxnDetails.setString(3, rs4.getString("fuel_date"));
                                            insertFuelTxnDetails.setString(4, "46019");
                                            insertFuelTxnDetails.setString(5, null);
                                            insertFuelTxnDetails.setString(6, narration1);
                                            insertFuelTxnDetails.setString(7, narration2);
                                            insertFuelTxnDetails.setString(8, credits + "");
                                            insertFuelTxnDetails.setString(9, "0");
                                            insertFuelTxnDetails.setString(10, "1");
                                            insertFuelTxnDetails.setString(11, tripIdSet);

                                            insertFuelTxnDetails.setString(12, regNo2);
                                            insertFuelTxnDetails.setString(13, lcNo2);
                                            insertFuelTxnDetails.setString(14, CHQNO2);
                                            insertFuelTxnDetails.setInt(15, 4); // 4 as Fuel expense Credit 
                                            insertFuelTxnDetails.setString(16, compId);
                                            insertFuelTxnDetails.setString(17, rs4.getString("slipno"));
                                            insertFuelTxnDetails.setString(18, rs4.getString("slipDate"));

                                            status = insertFuelTxnDetails.executeUpdate();
                                            System.out.println("insertTxnDetails FUEL Credit tripid "+tripId+ " = " + status);
                                        }
                                    }

                                }

                                String accCodeRecCrdt = "46019";
                                getTxnFuelRecovery.setString(1, tripId);
                                getTxnFuelRecovery.setString(2, vehicleId);
                                getTxnFuelRecovery.setString(3, actStatus);
                                rs6 = getTxnFuelRecovery.executeQuery();
                                while (rs6.next()) {
                                    if (sno == 0) {
                                        sno = Integer.parseInt(rs6.getString("serial_number"));
                                    } else {
                                        sno = sno + 1;
                                    }
                                    accCode = rs6.getString("acc_code");
                                    credit = credit + rs6.getDouble("debit");
                                    expenseDate = rs6.getString("fuel_date");
                                    subCode = rs6.getString("sub_Code");
                                    if ("1036".equals(rs6.getString("expenseId"))) { // Fuel Recovery credit
                                        System.out.println("expenseDate---FT-" + expenseDate);
                                        CHQNO = "DR";
                                        accCode1 = "15007";
                                        subCode1 = null;
                                        regNo = rs6.getString("reg_no");
                                        lcNo = rs6.getString("DriverLc");
                                        expenseId = rs6.getString("expenseid");
                                        System.out.println("DR CRedit");
                                        insertTxnDetails.setString(1, rs6.getString("txnType"));
                                        insertTxnDetails.setInt(2, 1);
                                        insertTxnDetails.setString(3, expenseDate);
                                        insertTxnDetails.setString(4, accCode1);
                                        insertTxnDetails.setString(5, subCode1);
                                        insertTxnDetails.setString(6, rs6.getString("narration1"));
                                        insertTxnDetails.setString(7, rs6.getString("narration2"));
                                        insertTxnDetails.setString(8, rs6.getString("debit"));
                                        insertTxnDetails.setString(9, "0");
                                        insertTxnDetails.setString(10, "1");
                                        insertTxnDetails.setString(11, tripIdSet);

                                        insertTxnDetails.setString(12, regNo);
                                        insertTxnDetails.setString(13, lcNo);
                                        insertTxnDetails.setString(14, CHQNO);
                                        insertTxnDetails.setString(15, expenseId); //expenseId
                                        insertTxnDetails.setString(16, compId);
                                        status = insertTxnDetails.executeUpdate();
                                        System.out.println("insertTxnDetails= " + status);

                                        if (sno != 0) {
                                            sno = sno + 1;
                                            System.out.println("thrERPTablePosting DR credit Enter======" + credit);
                                            CHQNO = "DR";
                                            insertTxnDetails.setString(1, "JV");
                                            insertTxnDetails.setInt(2, 2);
                                            insertTxnDetails.setString(3, expenseDate);
                                            insertTxnDetails.setString(4, accCodeRecCrdt);
                                            insertTxnDetails.setString(5, subCode1);
                                            insertTxnDetails.setString(6, rs6.getString("narration1"));
                                            insertTxnDetails.setString(7, rs6.getString("narration2"));
                                            insertTxnDetails.setString(8, "0");
                                            insertTxnDetails.setString(9, rs6.getString("debit"));
                                            insertTxnDetails.setString(10, "1");
                                            insertTxnDetails.setString(11, tripIdSet);

                                            insertTxnDetails.setString(12, regNo);
                                            insertTxnDetails.setString(13, lcNo);
                                            insertTxnDetails.setString(14, CHQNO);
                                            insertTxnDetails.setInt(15, 5);  // 2 as Fuel Recovery credit
                                            insertTxnDetails.setString(16, compId);
                                            status = insertTxnDetails.executeUpdate();
                                            System.out.println("insertTxnDetails Credit= " + status);
                                        }

                                    }
                                }
                                System.out.println("getTxnFuelCashAndFastTag sno======" + sno);

//                            checkDktStatus.setString(1, tripIds);
//                            rs1 = checkDktStatus.executeQuery();
//                            while (rs1.next()) {
//                                if ("1".equals(vendId)) {
                                updateTripDktStatus1.setString(1, "2");
                                updateTripDktStatus1.setString(2, tripId);
                                updateTripDktStatus1.setString(3, actStatus);
//                                }
                                status = updateTripDktStatus1.executeUpdate();
                                System.out.println("updateTripDktStatus1 - 14 status--= " + status);
                            }
                        }
                    }
//                 own transaction entries ended          
//----      --- Hire transaction entries ended 
                    System.out.println("invoiceId@@HIRE@@--" + invoiceId);
                    if ("14".equals(statusId) && !"1".equals(vendId) && !"0".equals(invoiceId)) {
                        System.out.println("statusId@@HIRE@@--" + statusId);
                        double credit = 0.00D;
                        int sno = 0;
                        int sno1 = 0;
                        int sno2 = 0;
                        int sno3 = 0;
                        int sno4 = 0;
                        String expenseDate = "";
                        String expenseDateNormal = "";
                        String CHQNO = "";
                        String lcNo = null;
                        String expenseId = "";
                        String regNo = null;
                        String accCode = null;
                        String accCode1 = null;
                        String subCode = null;
                        String subCode1 = null;
                        String narrate1 = null;
                        String narrate2 = null;
                        String tripIdSet = null;
                     
//                        if (sno != 0) {
                        getHireCntrctTxnDetails.setString(1, invoiceId);
//                                    getHireCntrctTxnDetails.setString(2, tripIds);
                        rs8 = getHireCntrctTxnDetails.executeQuery();
                        while (rs8.next()) {
                            if (sno == 0) {
                                sno1 = sno1 + 1;
                            } else {
                                sno1 = sno + 1;
                            }
                            
                            if ("1470".equals(compId)) {  // tutti
                                narrate1 = "BEING TRANSPORTATION CHARGES PAYABLE TO '"+rs8.getString("vendor_name")+"'";
                            }else{
                                narrate1 = rs8.getString("narration1");
                            }
                            
                            System.out.println("getHireTotalCreditTxnDetails  sno1---"+sno1);
                            System.out.println("thrERPTablePosting credit Enter=HIRE=====" + credit);
                            expenseDateNormal = rs8.getString("expense_date");
                            insertHireTxnDetails.setString(1, "JV");
                            insertHireTxnDetails.setString(2, rs8.getString("serial_number"));
                            insertHireTxnDetails.setString(3, expenseDateNormal);
                            if ("1461".equals(compId)) {              // chennai
                                insertHireTxnDetails.setString(4, "");
                            }
                            if ("1470".equals(compId)) {              //tuti
                                insertHireTxnDetails.setString(4, "");
                            }
                            if ("1472".equals(compId)) {             // cochin
                                insertHireTxnDetails.setString(4, "");
                            }
                            
                            insertHireTxnDetails.setString(5, null);
                            insertHireTxnDetails.setString(6, narrate1);
                            insertHireTxnDetails.setString(7, rs8.getString("narration2"));
                            insertHireTxnDetails.setString(8, rs8.getString("debit"));
                            insertHireTxnDetails.setString(9, "0");
                            insertHireTxnDetails.setString(10, "1");
                            insertHireTxnDetails.setString(11, rs8.getString("tripId"));
                            insertHireTxnDetails.setString(12, rs8.getString("reg_no"));
                            insertHireTxnDetails.setString(13, lcNo);
                            insertHireTxnDetails.setString(14, CHQNO);
                            insertHireTxnDetails.setInt(15, 0);  // contract amount debit
                            insertHireTxnDetails.setString(16, compId);
                            insertHireTxnDetails.setString(17, rs8.getString("venInvNo"));
                            insertHireTxnDetails.setString(18, rs8.getString("venInvDate"));
                            insertHireTxnDetails.setString(19, rs8.getString("movType"));
                            insertHireTxnDetails.setString(20, rs8.getString("SlipNo"));
                            insertHireTxnDetails.setString(21, rs8.getString("SlipDate"));

                            status = insertHireTxnDetails.executeUpdate();
                            System.out.println("getHireCntrctTxnDetails HIRE contrct Credit= " + status);
                        }
//                        }


                        System.out.println("sno1sno1sno1---" + sno1);
                        System.out.println("getHireTotalCreditTxnDetails---" + invoiceId);
                        if (sno1 != 0) {
                            getHireTotalCreditTxnDetails.setString(1, invoiceId);
                            rs8 = getHireTotalCreditTxnDetails.executeQuery();
                            while (rs8.next()) {
                                sno1 = sno1 + 1;
                                System.out.println("thrERPTablePosting getHireTotalCreditTxnDetails Enter=HIRE=====" + credit);
                                expenseDateNormal = rs8.getString("expense_date");
                                insertHireTxnDetails.setString(1, "JV");
                                insertHireTxnDetails.setInt(2, sno1);
                                insertHireTxnDetails.setString(3, expenseDateNormal);
                                if ("1461".equals(compId)) {              // chennai
                                    insertHireTxnDetails.setString(4, "22001");
                                }
                                if ("1470".equals(compId)) {              //tuti
                                    insertHireTxnDetails.setString(4, "22001");
                                }
                                if ("1472".equals(compId)) {             // cochin
                                    insertHireTxnDetails.setString(4, "");
                                }
                                insertHireTxnDetails.setString(5, rs8.getString("erp_id"));
                                insertHireTxnDetails.setString(6, rs8.getString("narration1"));
                                insertHireTxnDetails.setString(7, rs8.getString("narration2"));
                                insertHireTxnDetails.setString(8, "0");
                                insertHireTxnDetails.setString(9, rs8.getString("debit"));
                                insertHireTxnDetails.setString(10, "1");
                                insertHireTxnDetails.setString(11, rs8.getString("tripId"));
                                insertHireTxnDetails.setString(12, rs8.getString("reg_no"));
                                insertHireTxnDetails.setString(13, lcNo);
                                insertHireTxnDetails.setString(14, CHQNO);
                                insertHireTxnDetails.setInt(15, 0);  // total vendor Credit
                                insertHireTxnDetails.setString(16, compId);
                                insertHireTxnDetails.setString(17, rs8.getString("venInvNo"));
                                insertHireTxnDetails.setString(18, rs8.getString("venInvDate"));
                                insertHireTxnDetails.setString(19, rs8.getString("movType"));
                                insertHireTxnDetails.setString(20, rs8.getString("SlipNo"));
                                insertHireTxnDetails.setString(21, rs8.getString("SlipDate"));

                                status = insertHireTxnDetails.executeUpdate();
                                System.out.println("getHireTotalCreditTxnDetails HIRE contrct Credit= " + status);
                            }
                        }
                        System.out.println("getHireTxnTDS----" + invoiceId + "---" + sno1);
                        if (sno1 != 0) {
                            getHireTxnTDS.setString(1, invoiceId);
                            rs8 = getHireTxnTDS.executeQuery();
                            while (rs8.next()) {
                                sno1 = sno1 + 1;
//                                        sno3 = sno3 + 1;
                                System.out.println("thrERPTablePosting getHireTxnContainerInsDetails Enter=HIRE=====" + credit);
                                expenseDateNormal = rs8.getString("expense_date");
                                insertHireTxnDetails.setString(1, "JV");
                                insertHireTxnDetails.setInt(2, sno1);
                                insertHireTxnDetails.setString(3, expenseDateNormal);
                                if ("1461".equals(compId)) {              // chennai
                                    insertHireTxnDetails.setString(4, "22012");
                                }
                                if ("1470".equals(compId)) {              //tuti
                                    insertHireTxnDetails.setString(4, "22012");
                                }
                                if ("1472".equals(compId)) {             // cochin
                                    insertHireTxnDetails.setString(4, "");
                                }
                                insertHireTxnDetails.setString(5, rs8.getString("tds_erp_id"));
                                insertHireTxnDetails.setString(6, rs8.getString("narration1"));
                                insertHireTxnDetails.setString(7, rs8.getString("narration2"));
                                insertHireTxnDetails.setString(8, "0");
                                insertHireTxnDetails.setString(9, rs8.getString("tds_amount"));
                                insertHireTxnDetails.setString(10, "1");
                                insertHireTxnDetails.setString(11, rs8.getString("tripId"));
                                insertHireTxnDetails.setString(12, rs8.getString("reg_no"));
                                insertHireTxnDetails.setString(13, lcNo);
                                insertHireTxnDetails.setString(14, CHQNO);
                                insertHireTxnDetails.setInt(15, 0);  // contract amount debit
                                insertHireTxnDetails.setString(16, compId);
                                insertHireTxnDetails.setString(17, rs8.getString("venInvNo"));
                                insertHireTxnDetails.setString(18, rs8.getString("venInvDate"));
                                insertHireTxnDetails.setString(19, rs8.getString("movType"));
                                insertHireTxnDetails.setString(20, rs8.getString("SlipNo"));
                                insertHireTxnDetails.setString(21, rs8.getString("SlipDate"));

                                status = insertHireTxnDetails.executeUpdate();
                                System.out.println("getHireTxnTDS HIRE contrct Credit= " + status);

                                if (sno1 != 0) {
                                    sno1 = sno1 + 1;
                                    expenseDateNormal = rs8.getString("expense_date");
                                    insertHireTxnDetails.setString(1, "JV");
                                    insertHireTxnDetails.setInt(2, sno1);
                                    insertHireTxnDetails.setString(3, expenseDateNormal);
                                    if ("1461".equals(compId)) {              // chennai
                                        insertHireTxnDetails.setString(4, "22001");
                                    }
                                    if ("1470".equals(compId)) {              //tuti
                                        insertHireTxnDetails.setString(4, "22001");
                                    }
                                    if ("1472".equals(compId)) {             // cochin
                                        insertHireTxnDetails.setString(4, "");
                                    }
                                    insertHireTxnDetails.setString(5, rs8.getString("erp_id"));
                                    insertHireTxnDetails.setString(6, rs8.getString("narration1"));
                                    insertHireTxnDetails.setString(7, "TDS Debit");
                                    insertHireTxnDetails.setString(8, rs8.getString("tds_amount"));
                                    insertHireTxnDetails.setString(9, "0");
                                    insertHireTxnDetails.setString(10, "1");
                                    insertHireTxnDetails.setString(11, rs8.getString("tripId"));
                                    insertHireTxnDetails.setString(12, rs8.getString("reg_no"));
                                    insertHireTxnDetails.setString(13, lcNo);
                                    insertHireTxnDetails.setString(14, CHQNO);
                                    insertHireTxnDetails.setInt(15, 0);  // contract amount debit
                                    insertHireTxnDetails.setString(16, compId);
                                    insertHireTxnDetails.setString(17, rs8.getString("venInvNo"));
                                    insertHireTxnDetails.setString(18, rs8.getString("venInvDate"));
                                    insertHireTxnDetails.setString(19, rs8.getString("movType"));
                                    insertHireTxnDetails.setString(20, rs8.getString("SlipNo"));
                                    insertHireTxnDetails.setString(21, rs8.getString("SlipDate"));

                                    status = insertHireTxnDetails.executeUpdate();
                                    System.out.println("getHireTxnTDS HIRE contrct Credit= " + status);
                                }
                            }
                        }
                        System.out.println("getHireTxnContainerInsDetails--" + invoiceId + "---" + sno1);
                        if (sno1 != 0) {
                            getHireTxnContainerInsDetails.setString(1, invoiceId);
                            rs8 = getHireTxnContainerInsDetails.executeQuery();
                            while (rs8.next()) {
                                sno1 = sno1 + 1;
                                System.out.println("thrERPTablePosting getHireTxnContainerInsDetails Enter=HIRE=====" + credit);
                                expenseDateNormal = rs8.getString("expense_date");
                                insertHireTxnDetails.setString(1, "JV");
                                insertHireTxnDetails.setInt(2, sno1);
                                insertHireTxnDetails.setString(3, expenseDateNormal);
                                if ("1461".equals(compId)) {              // chennai
                                    insertHireTxnDetails.setString(4, "22020");
                                }
                                if ("1470".equals(compId)) {              //tuti
                                    insertHireTxnDetails.setString(4, "22020");
                                }
                                if ("1472".equals(compId)) {             // cochin
                                    insertHireTxnDetails.setString(4, "");
                                }
                                insertHireTxnDetails.setString(5, null);
                                insertHireTxnDetails.setString(6, rs8.getString("narration1"));
                                insertHireTxnDetails.setString(7, rs8.getString("narration2") + " credit");
                                insertHireTxnDetails.setString(8, "0");
                                insertHireTxnDetails.setString(9, rs8.getString("debit"));
                                insertHireTxnDetails.setString(10, "1");
                                insertHireTxnDetails.setString(11, rs8.getString("tripId"));
                                insertHireTxnDetails.setString(12, rs8.getString("reg_no"));
                                insertHireTxnDetails.setString(13, lcNo);
                                insertHireTxnDetails.setString(14, CHQNO);
                                insertHireTxnDetails.setInt(15, 0);  // contract amount debit
                                insertHireTxnDetails.setString(16, compId);
                                insertHireTxnDetails.setString(17, rs8.getString("venInvNo"));
                                insertHireTxnDetails.setString(18, rs8.getString("venInvDate"));
                                insertHireTxnDetails.setString(19, rs8.getString("movType"));
                                insertHireTxnDetails.setString(20, rs8.getString("SlipNo"));
                                insertHireTxnDetails.setString(21, rs8.getString("SlipDate"));

                                status = insertHireTxnDetails.executeUpdate();
                                System.out.println("getHireTxnContainerInsDetails HIRE contrct Credit= " + status);

                                if (sno1 != 0) {
                                    sno1 = sno1 + 1;
                                    expenseDateNormal = rs8.getString("expense_date");
                                    insertHireTxnDetails.setString(1, "JV");
                                    insertHireTxnDetails.setInt(2, sno1);
                                    insertHireTxnDetails.setString(3, expenseDateNormal);
                                    if ("1461".equals(compId)) {              // chennai
                                        insertHireTxnDetails.setString(4, "22001");
                                    }
                                    if ("1470".equals(compId)) {              //tuti
                                        insertHireTxnDetails.setString(4, "22001");
                                    }
                                    if ("1472".equals(compId)) {             // cochin
                                        insertHireTxnDetails.setString(4, "");
                                    }
                                    insertHireTxnDetails.setString(5, rs8.getString("erp_id"));
                                    insertHireTxnDetails.setString(6, rs8.getString("narration1"));
                                    insertHireTxnDetails.setString(7, rs8.getString("narration2") + " debit");
                                    insertHireTxnDetails.setString(8, rs8.getString("debit"));
                                    insertHireTxnDetails.setString(9, "0");
                                    insertHireTxnDetails.setString(10, "1");
                                    insertHireTxnDetails.setString(11, rs8.getString("tripId"));
                                    insertHireTxnDetails.setString(12, rs8.getString("reg_no"));
                                    insertHireTxnDetails.setString(13, lcNo);
                                    insertHireTxnDetails.setString(14, CHQNO);
                                    insertHireTxnDetails.setInt(15, 0);  // contract amount debit
                                    insertHireTxnDetails.setString(16, compId);
                                    insertHireTxnDetails.setString(17, rs8.getString("venInvNo"));
                                    insertHireTxnDetails.setString(18, rs8.getString("venInvDate"));
                                    insertHireTxnDetails.setString(19, rs8.getString("movType"));
                                    insertHireTxnDetails.setString(20, rs8.getString("SlipNo"));
                                    insertHireTxnDetails.setString(21, rs8.getString("SlipDate"));

                                    status = insertHireTxnDetails.executeUpdate();
                                    System.out.println("getHireTxnContainerInsDetails HIRE contrct Credit= " + status);
                                }
                            }
                        }
                        
                        //starts

                        System.out.println("invoiceId-QQQ-first-" + invoiceId);
                        getHireTxnDetails.setString(1, invoiceId);
                        rs7 = getHireTxnDetails.executeQuery();
                        while (rs7.next()) {
                            sno1 = sno1 + 1;
                            regNo = rs7.getString("reg_no");
                            lcNo = rs7.getString("DriverLc");
                            expenseId = rs7.getString("expense_id");
                            narrate2 = rs7.getString("narration1");
                            System.out.println("narrate2----" + narrate2);
                            tripIdSet = rs7.getString("tripId");
                            System.out.println("tripIdSet----"+tripIdSet);
                            System.out.println("compId-HIRE---"+compId);
                            if ("1461".equals(compId)) {  // chennai
                                if ("1052".equals(rs7.getString("expense_id"))) { // if vendor halt
                                    if ("Load 40 MOFUSSIL".equals(rs7.getString("movType")) || "Load 20 MOFUSSIL".equals(rs7.getString("movType"))) {
                                        accCode = rs7.getString("haltMofChe");
                                    } else if ("Load 40 LOCAL".equals(rs7.getString("movType")) || "Load 20 LOCAL".equals(rs7.getString("movType"))) {
                                        accCode = rs7.getString("haltLocChe");
                                    }
                                    subCode = rs7.getString("subCode");
                                } else {
                                    accCode = rs7.getString("AccCode");
                                    subCode = rs7.getString("subCode");
                                }
                                narrate1 = rs7.getString("narration1");
                            }
                            
                            if ("1470".equals(compId)) {  // tutti
                                narrate1 = "BEING TRANSPORTATION CHARGES PAYABLE TO '"+rs7.getString("vendor_name")+"'";
                                if ("1052".equals(rs7.getString("expense_id"))) { // if vendor halt
                                    if ("Load 40 MOFUSSIL".equals(rs7.getString("movType"))) {
                                        accCode = rs7.getString("haltMof40Tuti");
                                    } else if ("Load 20 MOFUSSIL".equals(rs7.getString("movType"))) {
                                        accCode = rs7.getString("haltMof20Tuti");
                                    } else if ("Load 40 LOCAL".equals(rs7.getString("movType"))) {
                                        accCode = rs7.getString("haltLoc40Tuti");
                                    } else if ("Load 20 LOCAL".equals(rs7.getString("movType"))) {
                                        accCode = rs7.getString("haltLoc20Tuti");
                                    }
                                    subCode = rs7.getString("subCodeTuti");
                                } else if ("1014".equals(rs7.getString("expense_id"))) { // if weighment charge
                                    if ("Load 40 MOFUSSIL".equals(rs7.getString("movType")) || "Load 20 MOFUSSIL".equals(rs7.getString("movType"))) {
                                        accCode = "51005";
                                    } else if ("Load 40 LOCAL".equals(rs7.getString("movType")) || "Load 20 LOCAL".equals(rs7.getString("movType"))) {
                                        accCode = "51008";
                                    }
                                    subCode = rs7.getString("subCodeTuti");
//                                } else if ("1034".equals(rs7.getString("expense_id"))) { // if LOLO charge
//                                    accCode = "46072";
//                                    subCode = "LO031";
                                } else { // other expense
                                    accCode = rs7.getString("AccCodeTuti");
                                    subCode = rs7.getString("subCodeTuti");
                                }
                            }
                            if ("1472".equals(compId)) {  // coc
                                accCode = rs7.getString("AccCodeCoc");
                                subCode = rs7.getString("subCodeCoc");
                                narrate1 = rs7.getString("narration1");
                            }
                            
                            System.out.println("ACCCODE-HIRE--"+accCode);
                            System.out.println("SUBCODE-HIRE--"+subCode);
                            System.out.println("narrate1-HIRE--"+narrate1);

//                            sno = rs7.getInt("serial_number");
                            System.out.println("sno-HIRE---"+sno1);
                            System.out.println("expensedate-HIRE---"+rs7.getString("expense_date"));
                            credit = credit + rs7.getDouble("debit");
                            System.out.println("credit-HIRE---"+credit);
                            expenseDateNormal = rs7.getString("expense_date");
                            System.out.println("normal EXpense---HIRE---" + rs7.getString("expense_date"));
                            insertHireTxnDetails.setString(1, rs7.getString("txnType"));
                            insertHireTxnDetails.setInt(2, sno1);
                            insertHireTxnDetails.setString(3, rs7.getString("expense_date"));
                            insertHireTxnDetails.setString(4, accCode);
                            insertHireTxnDetails.setString(5, subCode);
                            insertHireTxnDetails.setString(6, narrate1);
                            insertHireTxnDetails.setString(7, rs7.getString("narration2"));
                            insertHireTxnDetails.setString(8, rs7.getString("debit"));
                            insertHireTxnDetails.setString(9, "0");
                            insertHireTxnDetails.setString(10, "1");
                            insertHireTxnDetails.setString(11, tripIdSet);
                            insertHireTxnDetails.setString(12, rs7.getString("reg_no"));
                            insertHireTxnDetails.setString(13, lcNo);
                            insertHireTxnDetails.setString(14, CHQNO);
                            insertHireTxnDetails.setString(15, rs7.getString("expense_id"));
                            insertHireTxnDetails.setString(16, compId);
                            insertHireTxnDetails.setString(17, rs7.getString("venInvNo"));
                            insertHireTxnDetails.setString(18, rs7.getString("venInvDate"));
                            insertHireTxnDetails.setString(19, rs7.getString("movType"));
                            insertHireTxnDetails.setString(20, rs7.getString("SlipNo"));
                            insertHireTxnDetails.setString(21, rs7.getString("SlipDate"));
//                            insertHireTxnDetails.setString(22, invoiceId);
                            regNo = rs7.getString("reg_no");
                            status = insertHireTxnDetails.executeUpdate();
                            System.out.println("insertTxnDetails= " + status);
                        }
                        System.out.println("thrERPTablePosting HIRE sno======" + sno);
                        System.out.println("thrERPTablePosting HIRE credit======" + credit);

                        System.out.println("normal EXpense--credit--HIRE--" + expenseDateNormal);

                        System.out.println("getHireCntrctTxnDetails---" + invoiceId);

                        System.out.println("SNOaaa---" + sno);
//ends
                        

                        System.out.println("getTxnDetails sno======" + sno);

                        //  starts getHireTxnFuelDetails  FUEL Bunk
                        String accCode2 = "";
                        String subCode2 = "";
                        String CHQNO2 = "";
                        String regNo2 = "";
                        String lcNo2 = "";
                        String narration1 = "";
                        String narration2 = "";
                        getHireTxnFuelDetails.setString(1, invoiceId);
//                                getHireTxnFuelDetails.setString(2, tripIds);
                        System.out.println("invoiceId FUEL---" + invoiceId);
                        Double credits = 0.00;
                        rs4 = getHireTxnFuelDetails.executeQuery();
                        int snos = 1;
                        while (rs4.next()) {
                            snos = snos + 1;
                            accCode2 = rs4.getString("acc_Code");
                            CHQNO2 = rs4.getString("CHQNO");
                            subCode2 = rs4.getString("sub_code");
                            regNo2 = rs4.getString("reg_no");
                            lcNo2 = rs4.getString("DriverLc");
                             if ("1470".equals(compId)) {  // tutti
                                narration1 = "BEING DIESEL ADVANCE PAID TO "+rs4.getString("vendor_name") +" THROUGH '"+rs4.getString("bunk_name")+"'";
                             }else{
                                narration1 = rs4.getString("narration1");
                             }
                            narration2 = rs4.getString("narration2");
                            credits = Double.parseDouble(rs4.getString("debit"));

                            getTxnFuelSubCodeList.setString(1, subCode2);
                            rs5 = getTxnFuelSubCodeList.executeQuery();
                            while (rs5.next()) {
                                System.out.println("RS4 subcode----" + rs5.getString("sub_code"));
                                System.out.println("RS5 subcode----" + rs4.getString("sub_code"));
                                insertHireTxnDetails.setString(1, rs4.getString("txnType"));
                                insertHireTxnDetails.setInt(2, 2);
                                //                              insertTxnDetails.setString(2, rs2.getString("serial_number"));
                                insertHireTxnDetails.setString(3, rs4.getString("fuel_date"));
                                insertHireTxnDetails.setString(4, "22001");
                                insertHireTxnDetails.setString(5, subCode2);
                                insertHireTxnDetails.setString(6, narration1);
                                insertHireTxnDetails.setString(7, narration2);
                                insertHireTxnDetails.setString(8, "0");
                                insertHireTxnDetails.setString(9, credits + "");
                                insertHireTxnDetails.setString(10, "1");
                                insertHireTxnDetails.setString(11, rs4.getString("tripId"));

                                insertHireTxnDetails.setString(12, regNo2);
                                insertHireTxnDetails.setString(13, lcNo2);
                                insertHireTxnDetails.setString(14, CHQNO2);
                                insertHireTxnDetails.setString(15, "1010");
                                insertHireTxnDetails.setString(16, compId);
                                insertHireTxnDetails.setString(17, rs4.getString("venInvNo"));
                                insertHireTxnDetails.setString(18, rs4.getString("venInvDate"));
                                insertHireTxnDetails.setString(19, rs4.getString("movType"));
                                insertHireTxnDetails.setString(20, rs4.getString("SlipNo"));
                                insertHireTxnDetails.setString(21, rs4.getString("SlipDate"));
                                status = insertHireTxnDetails.executeUpdate();
                                System.out.println("insertgetTxnFuelDetails= " + status);

                                if (snos != 0) {
                                    System.out.println("thrERPTablePosting 1010 FUEL credit Enter======" + rs4.getString("debit"));
                                    insertHireTxnDetails.setString(1, "JV");
                                    insertHireTxnDetails.setInt(2, 1);
                                    insertHireTxnDetails.setString(3, rs4.getString("fuel_date"));
                                    insertHireTxnDetails.setString(4, "22001");
                                    insertHireTxnDetails.setString(5, rs4.getString("erp_id"));
                                    insertHireTxnDetails.setString(6, narration1);
                                    insertHireTxnDetails.setString(7, rs4.getString("Liters"));
                                    insertHireTxnDetails.setString(8, rs4.getString("debit"));
                                    insertHireTxnDetails.setString(9, "0");
                                    insertHireTxnDetails.setString(10, "1");
                                    insertHireTxnDetails.setString(11, rs4.getString("tripId"));

                                    insertHireTxnDetails.setString(12, regNo2);
                                    insertHireTxnDetails.setString(13, lcNo2);
                                    insertHireTxnDetails.setString(14, CHQNO2);
                                    insertHireTxnDetails.setInt(15, 4); // 4 as Fuel expense Credit 
                                    insertHireTxnDetails.setString(16, compId);
                                    insertHireTxnDetails.setString(17, rs4.getString("venInvNo"));
                                    insertHireTxnDetails.setString(18, rs4.getString("venInvDate"));
                                    insertHireTxnDetails.setString(19, rs4.getString("movType"));
                                    insertHireTxnDetails.setString(20, rs4.getString("SlipNo"));
                                    insertHireTxnDetails.setString(21, rs4.getString("SlipDate"));

                                    status = insertHireTxnDetails.executeUpdate();
                                    System.out.println("insertTxnDetails FUEL HIRE Credit= " + status);
                                }
                            }
                        }

                        System.out.println("getTxnFuelCashAndFastTag HIRE sno======" + sno);

//                            checkDktStatus.setString(1, tripIds);
//                            rs1 = checkDktStatus.executeQuery();
//                            while (rs1.next()) {
                        updateTxnStatus.setString(1, "2");
                        updateTxnStatus.setString(2, invoiceId);
                        status = updateTxnStatus.executeUpdate();

                        System.out.println("updateTxnStatus HIRE - 14 status--= " + status);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("Posting Caught SqlException A: " + e.getMessage());
                }

            } catch (Exception e) {
                System.out.println("Posting Caught IOException B: " + e.getMessage());

            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException e) {
                        System.out.println("connection exception occur" + e.getMessage());
                    }
                }
            }
//            con.close();
        } catch (Exception e) {
            System.out.println("Posting Caught Exception C: " + e.getMessage());
        }
    }

}
