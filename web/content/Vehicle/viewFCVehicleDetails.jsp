<%-- 
    Document   : fCVehicleDetail
    Created on : May 30, 2016, 4:46:47 PM
    Author     : hp
--%>


<%@ page import="ets.domain.util.ThrottleConstants" %>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">-->

<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });


    $(document).ready(function() {
        $(function() {
            $("#fromDate1").datepicker({
                defaultDate: "-d",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd-mm-yy",
//                                        maxDate:"-d",
                onClose: function(selectedDate) {
                    var d = selectedDate.split("-");
                    var da = d[0];
                    var mo = d[1];
                    var yr = parseInt(d[2]) + 1;
                    var nd = da + "-" + mo + "-" + yr;
                    $("#toDate1").datepicker("setDate", nd);
                }
            });
            $("#toDate1").datepicker({
                changeMonth: true,
                dateFormat: "dd-mm-yy",
            });
        });
    });

</script>
<script>
    $(document).ready(function() {
        $(function() {
            $("#fcDate").datepicker({
                defaultDate: "-d",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd-mm-yy",
//                                        maxDate:"-d",
                onClose: function(selectedDate) {
                    var d = selectedDate.split("-");
                    var da = d[0];
                    var mo = d[1];
                    var yr = parseInt(d[2]) + 1;
                    var nd = da + "-" + mo + "-" + yr;
                    $("#fcExpiryDate").datepicker("setDate", nd);
                }
            });
            $("#fcExpiryDate").datepicker({
                changeMonth: true,
                dateFormat: "dd-mm-yy",
            });
        });
    });
</script>

<script language="javascript">

    function CalWarrantyDate(selDate) {

        var seleDate = selDate.split('-');
        var dd = seleDate[0];
        var mm = seleDate[1];
        var yyyy = seleDate[2];

        var today = new Date();
        var dd1 = today.getDate();
        var mm1 = today.getMonth() + 1; //January is 0!
        var yyyy1 = today.getFullYear();

        //                if(dd<10){dd='0'+dd}if(mm<10){mm='0'+mm}today = mm+'-'+dd+'-'+yyyy;alert("today"+today);alert("value"+value);

        var selecedDate = new Date(yyyy, mm, dd);
        var currentDate = new Date(yyyy1, mm1, dd1);
        var Days = Math.floor((selecedDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24));
        //alert("DAYS--"+Days);
        document.addVehicle.war_period.value = Days;

    }



    function submitPage2(value) {
        if (value == "Save") {
            alert(value);

            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }

    }
    function submitPage(value) {
        if (value == "save") {

            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }
    function submitPageDedicate(value) {

        var owner = document.getElementById("asset").value;
        document.getElementById("asset").value = owner;

        if (validate() == 'fail') {
            return;
        } else if (document.addVehicle.vendorId.value == '') {
            alert('Please Enter Vendor name');
            document.addVehicle.vendorId.focus();
            return;
        } else if (document.addVehicle.regNo.value == '') {
            alert('Please Enter Vehicle Registration Number');
            document.addVehicle.regNo.focus();
            return;
        } else if (document.addVehicle.regNoCheck.value == 'exists') {
            alert('Vehicle RegNo already Exists');
            return;
            //                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
            //                    return;

        } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
            return;
        } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
            return;
        } else if (isSelect(document.addVehicle.typeId, 'Vehicle type')) {
            return;
        }
        if (value == "save") {

            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }
    function submitPage1(value) {

        var owner = document.getElementById("asset").value;
        document.getElementById("asset").value = owner;

        if (validate() == 'fail') {
            return;
        } else if (document.addVehicle.vendorId.value == '') {
            alert('Please Enter Vendor name');
            document.addVehicle.vendorId.focus();
            return;
        } else if (document.addVehicle.regNo.value == '') {
            alert('Please Enter Vehicle Registration Number');
            document.addVehicle.regNo.focus();
            return;
        } else if (document.addVehicle.regNoCheck.value == 'exists') {
            alert('Vehicle RegNo already Exists');
            return;
            //                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
            //                    return;

        } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
            return;
        } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
            return;
        } else if (isSelect(document.addVehicle.typeId, 'Vehicle type')) {
            return;
        }

        if (value == "save1") {
            // alert("cgsd77f");
            //                    document.addVehicle.nextFCDate.value = dateFormat(document.addVehicle.nextFCDate);
            //     document.addVehicle.dateOfSale.value = dateFormat(document.addVehicle.dateOfSale);
            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }


    var httpRequest;
    function getVehicleDetails() {
        var vehicleId = $("#vehicleId").val();
        if (document.addVehicle.regNo.value != '' && vehicleId == '') {
            var url = '/throttle/checkVehicleExists.do?regno=' + document.addVehicle.regNo.value;

            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function() {
                go1();
            };
            httpRequest.send(null);
        }
    }


    function go1() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var response = httpRequest.responseText;
                var temp = response.split('-');
                if (response != "") {
                    alert('Vehicle Already Exists');
                    document.getElementById("Status").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                    document.addVehicle.regNo.focus();
                    document.addVehicle.regNo.select();
                    document.addVehicle.regNoCheck.value = 'exists';
                } else
                {
                    document.addVehicle.regNoCheck.value = 'Notexists';
                    document.getElementById("Status").innerHTML = "";
                }
            }
        }
    }

</script>




<script>

    function deleteRows(sno) {
        if (sno != 1) {
            rowCount--;
            document.getElementById("addTyres").deleteRow(sno);
        } else {
            alert("Can't Delete The Row");
        }
    }

    var httpReq;
    var temp = "";
    function getVehicleType(mfrId) { //alert(str);
        var fleetTypeId = $("#fleetTypeId").val();
        $.ajax({
            url: "/throttle/getMfrVehicleType.do",
            dataType: "json",
            data: {
                mfrId: mfrId,
                fleetTypeId: fleetTypeId
            },
            success: function(temp) {
                // alert(data);
                if (temp != '') {
                    $('#vehicleTypeId').empty();
                    $('#vehicleTypeId').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    $.each(temp, function(i, data) {
                        $('#vehicleTypeId').append(
                                $('<option style="width:150px"></option>').val(data.Id + "~" + data.AxleTypeId).html(data.Name)
                                )
                    });
                } else {
                    $('#vehicleTypeId').empty();
                }
            }
        });

    }



    function getModelAndTonnage(str) {
        var fleetTypeId = $("#fleetTypeId").val();
        var vehicleTypeId = str.split("~")[0];
        document.getElementById("typeId").value = vehicleTypeId;
        var fleetTypeId = $("#fleetTypeId").val();
        $.ajax({
            url: "/throttle/getModels1.do",
            dataType: "text",
            data: {
                typeId: vehicleTypeId,
                mfrId: document.addVehicle.mfrId.value
            },
            success: function(temp) {
                // alert(data);
                if (temp != '') {
                    setOptions1(temp, document.addVehicle.modelId);
                    document.getElementById("modelId").value = '<c:out value="${modelId}"/>';
                    getVehAxleName(str);
                } else {
                    setOptions1(temp, document.addVehicle.modelId);
                    alert('There is no model based on Vehicle Type Please add first');
                }
            }
        });
        // for tonnage now
        $.ajax({
            url: "/throttle/getTonnage.do",
            dataType: "text",
            data: {
                typeId: vehicleTypeId,
                fleetTypeId: fleetTypeId
            },
            success: function(temp) {
                if (temp != '') {
                    setOptions2(temp, document.addVehicle.seatCapacity);
                } else {

                    alert('Tonnage is not there please set first in vehicle type');
                }
            }
        });
    }


    function setOptions1(text, variab) {
//                                                                alert("setOptions on page")
        //          
//                                                                                      alert("variab = "+variab)
        variab.options.length = 0;
        //                                alert("1")
        var option0 = new Option("--select--", '0');
        //                                alert("2")
        variab.options[0] = option0;
        //                                alert("3")


        if (text != "") {
//                                                                        alert("inside the condition")
            var splt = text.split('~');
            var temp1;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
//                                                                            alert("splt.length ="+splt.length)
//                                                                            alert("for loop ="+splt[i])
                temp1 = splt[i].split('-');
                option0 = new Option(temp1[1], temp1[0])
//                                        alert("option0 ="+option0)
                variab.options[i + 1] = option0;
            }
        }
    }

    function setOptions2(text) {
        //                                alert("setOptions on page")
        //                               alert("text = "+text)
        if (text != "") {
            //                                        alert(text);
            document.getElementById('seatCapacity').value = text;

        }
    }


    function getInsCompanyName(val) {

        var issueCmpId = document.getElementById("IssuingCmnyName").value;
        alert(issueCmpId);
        $.ajax({
            url: '/throttle/getInsCompanyName.do?issueCmpId=' + issueCmpId,
            // alert(url);
            data: {issueCmpId: issueCmpId},
            dataType: 'json',
            success: function(data) {

                if (data !== '') {
                    $.each(data, function(i, data) {
                        var temp = data.Name.split('~');
                        alert(temp[0]);
                        alert(temp[1]);
                        alert(temp[2]);
                        document.getElementById("ins_AgentName").value = temp[0];
                        document.getElementById("ins_AgentCode").value = temp[1];
                        document.getElementById("ins_AgentMobile").value = temp[2];
                    });
                }
            }
        });
    }





    function validate()
    {
        var tyreIds = document.getElementsByName("tyreIds");
        var tyreDate = document.getElementsByName("tyreDate");
        var positionIds = document.getElementsByName("positionIds");
        var itemIds = document.getElementsByName("itemIds");
        var tyreExists = document.getElementsByName("tyreExists");

        var cntr = 0;
        for (var i = 0; i < tyreIds.length; i++) {
            for (var j = 0; j < tyreIds.length; j++) {
                if ((tyreIds[i].value == tyreIds[j].value) && (tyreIds[i].value == tyreIds[j].value) && (i != j) && (tyreIds[i].value != '')) {
                    cntr++;
                }
            }
            if (parseInt(cntr) > 0) {
                alert("Same Tyre Number should not exists twice");
                return "fail";
                break;
            }
        }

        for (var i = 0; i < positionIds.length; i++) {
            for (var j = 0; j < positionIds.length; j++) {
                if ((positionIds[i].value == positionIds[j].value) && (positionIds[i].value == positionIds[j].value) && (i != j) && (positionIds[i].value != '0')) {
                    cntr++;
                }
            }
            if (parseInt(cntr) > 0) {
                alert("Tyre Positions should not be repeated");
                return "fail";
                break;
            }
        }

        for (var i = 0; i < tyreIds.length; i++) {

            if (itemIds[i].value != '0') {
                if (positionIds[i].value == '0') {
                    alert('Please Select Position');
                    positionIds[i].focus();
                    return "fail";
                } else if (tyreIds[i].value == '') {
                    alert('Please Enter Tyre No');
                    tyreIds[i].focus();
                    return "fail";
                } else if (tyreExists[i].value == 'exists') {
                    alert('Tyre number ' + tyreIds[i].value + ' is already fitted to another vehicle');
                    tyreIds[i].focus();
                    tyreIds[i].select();
                    return "fail";
                }
            }
        }

        return "pass";
    }




    var httpRequest;
    function checkTyreId(val)
    {
        val = val - 1;
        var tyreNo = document.getElementsByName("tyreIds");
        var tyreExists = document.getElementsByName("tyreExists");
        if (tyreNo[val].value != '') {
            var url = '/throttle/checkVehicleTyreNo.do?tyreNo=' + tyreNo[val].value;
            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function() {
                processRequest(tyreNo[val].value, val);
            };
            httpRequest.send(null);
        }
    }


    function processRequest(tyreNo, index)
    {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                var tyreExists = document.getElementsByName("tyreExists");
                if (httpRequest.responseText.valueOf() != "") {
                    document.getElementById("userNameStatus").innerHTML = "Tyre No " + tyreNo + " Already Exists in " + httpRequest.responseText.valueOf();
                    tyreExists[index].value = 'exists';
                } else {
                    document.getElementById("userNameStatus").innerHTML = "";
                    tyreExists[index].value = 'notExists';
                }
            } else
            {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    function openAttachedInfo() {
        document.getElementById("asset").value = "2";
        document.getElementById("attachInfo").style.display = "block";
    }
    function closeAttachedInfo() {
        document.getElementById("asset").value = "1";
        document.getElementById("attachInfo").style.display = "none";
    }

    function blockNonNumbers(obj, e, allowDecimal, allowNegative)
    {
        var key;
        var isCtrl = false;
        var keychar;
        var reg;

        if (window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
        } else if (e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
        }

        if (isNaN(key))
            return true;

        keychar = String.fromCharCode(key);

        // check for backspace or delete, or if Ctrl was pressed
        if (key == 8 || isCtrl)
        {
            return true;
        }

        reg = /\d/;
        var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
        var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

        return isFirstN || isFirstD || reg.test(keychar);
    }


</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();

        $("#insurance").focus();
        $("#vehicleFitnessCertificate").focus();
    });


</script>
<script type="text/javascript">

    function updateFCDetails() {


        var paymentTypeFC = document.getElementById("paymentTypeFC").value;


        var ledgerId = document.getElementById("vendorIdFC").value;
        var cashOnHandLedgerId = document.getElementById("cashOnHandLedgerId").value;
        var cashOnHandLedgerCode = document.getElementById("cashOnHandLedgerCode").value;
        var paymentDateFC = document.getElementById("paymentDateFC").value;

        var bankIdFC = "0";
        if (document.getElementById("bankIdFC").value == null) {
            bankIdFC = "0";
        }
        else {
            bankIdFC = document.getElementById("bankIdFC").value;
        }
        var clearanceIdFC = "0";
        if (document.getElementById("clearanceIdFC").value == null) {
            clearanceIdFC = "0";
        }
        else {
            clearanceIdFC = document.getElementById("clearanceIdFC").value;
        }

        var bankBranchIdFC = "0";
        if (document.getElementById("bankBranchIdFC").value == null) {
            bankBranchIdFC = "0";
        }
        else {
            bankBranchIdFC = document.getElementById("bankBranchIdFC").value;
        }
        var chequeDateFC = "0";
        if (document.getElementById("chequeDateFC").value == null) {
            chequeDateFC = "";
        } else {
            chequeDateFC = document.getElementById("chequeDateFC").value;
        }
        var chequeNoFC = "0";
        if (document.getElementById("chequeNoFC").value == null) {
            chequeNoFC = "";
        } else {
            chequeNoFC = document.getElementById("chequeNoFC").value;
        }
        var chequeAmountFC = "0";
        if (document.getElementById("chequeAmountFC").value == null) {
            chequeAmountFC = "0";
        } else {
            chequeAmountFC = document.getElementById("chequeAmountFC").value;
        }

        var vehicleId = $("#vehicleId").val();
        var rtoDetail = document.getElementById("rtoDetail").value;
        var fcReceiptNo = document.getElementById("fcReceiptNo").value;
        var fcDate = document.getElementById("fcDate").value;
        var fcExpiryDate = document.getElementById("fcExpiryDate").value;
        var fcAmount = document.getElementById("fcAmount").value;
        var fcRemarks = document.getElementById("fcRemarks").value;
        var reportNo = document.getElementById("reportNo").value;
        var odometer = document.getElementById("odometer").value;
        var fcid = document.getElementById("fcid").value;
        if (document.addVehicle.rtoDetail.value == '') {
            alert('Please Enter Vehicle RtoDetail ');
            $("#rtoDetail").focus();
            return;
        } else if (document.addVehicle.fcDate.value == '') {
            alert('Please Enter Vehicle FcDate');
            $("#fcDate").focus();
            return;
        } else if (document.addVehicle.fcReceiptNo.value == '') {
            alert('Please Enter Vehicle FcReceiptNo');
            $("#fcReceiptNo").focus();
            return;
        } else if (document.addVehicle.fcAmount.value == '') {
            alert('Please Enter Vehicle FcAmount');
            $("#fcAmount").focus();
            return;
        } else if (document.addVehicle.fcExpiryDate.value == '') {
            alert('Please Enter Vehicle FcExpiryDate');
            $("#fcExpiryDate").focus();
            return;
        } else if (document.addVehicle.reportNo.value == '') {
            alert('Please Enter Vehicle reportNo');
            $("#reportNo").focus();
            return;
        } else if (document.addVehicle.odometer.value == '') {
            alert('Please Enter Vehicle odometer');
            $("#odometer").focus();
            return;
        }
        var url = '';
        var insertStatus = 0;
        url = './insertFCDetails.do';
        $.ajax({
            url: url,
            data: {rtoDetail: rtoDetail, fcDate: fcDate, fcReceiptNo: fcReceiptNo, fcAmount: fcAmount, fcExpiryDate: fcExpiryDate, vehicleId: vehicleId,
                fcRemarks: fcRemarks, paymentTypeFC: paymentTypeFC, bankIdFC: bankIdFC, bankBranchIdFC: bankBranchIdFC,
                chequeDateFC: chequeDateFC, chequeNoFC: chequeNoFC, chequeAmountFC: chequeAmountFC,
                clearanceIdFC: clearanceIdFC, fcid: fcid, ledgerId: ledgerId, cashOnHandLedgerId: cashOnHandLedgerId,
                cashOnHandLedgerCode: cashOnHandLedgerCode, paymentDateFC: paymentDateFC,
                fleetTypeId: 1, reportNo: reportNo, odometer: odometer
            },
            type: "GET",
            success: function(response) {
                insertStatus = response.toString().trim();
                if (insertStatus == 0) {
                    var str = "Vehicle FcDetails added failed ";
                    $("#status1").text(str).css("color", "red");

                } else {
                    var str = "Vehicle FcDetails added sucessfully ";
                    $("#status1").text(str).css("color", "green");

                }

            },
            error: function(xhr, status, error) {
            }
        });
        document.getElementById("fcNext").style.visibility = 'hidden';

    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.VehicleFC"  text="Vehicle FC"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="stores.label.VehicleFC"  text="Vehicle FC"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">

        <div class="panel-body">
            <c:if test="${vehicleFCDetails != null}">
                <c:forEach items="${vehicleFCDetails}" var="Fc">
                    <c:if test = "${oldVehicleFC != null}">
                        <c:forEach items="${oldVehicleFC}" var="oldFC">
                            <c:if test="${oldFC.activeInd == 'N'}">
                                <c:set var="vehicleId" value="${Fc.vehicleId}"/>   
                                <c:set var="fcid" value="${Fc.fcid}"/>   
                                <c:set var="activeInd" value="${oldFC.activeInd}"/> 
                            </c:if>
                        </c:forEach>
                        </c:if>
                        <c:if test="${activeInd == 'N'}">
                            <table align="right">
                                <tr align="right">
                                    <td align="right">
                                        <a href='/throttle/viewOldVehicleFC.do?vehicleId=<c:out value="${vehicleId}" />&regNo=<c:out value="${regNo}"/>&fcid=<c:out value="${fcid}" />&listId=3&fleetTypeId=1'>
                                            <img src="images/insurance.png" title="Old FC Details" style="height:30px;width:30px;">
                                            <font color="red" size="4">Old FC Details</font></a>&emsp;&emsp;&emsp;</td>
                                </tr>
                            </table>
                        </c:if>
                    
                    <br/>
                    <br/>
                    <%--</c:forEach>--%>
                    <%--</c:if>--%>
                    <!--    <body onLoad="addRow();setImages(0,0,0,0,0,0);hideAdds(1),hideAdds1(1);advanceFuelTabHide()">-->
                    <body onLoad="checkPaymentMode();
                            getBankBranch('<c:out value="${bankId}"/>');
                            getBankBranchRoadTax('<c:out value="${bankIdRT}"/>');
                            getBankBranchFC('<c:out value="${bankIdFC}"/>');
                            getBankBranchPermit('<c:out value="${bankIdPermit}"/>');
                            getBankPayment('<c:out value="${paymentTypeIns}"/>');
                            addOem(0, 1, '', '', '', 0, 0, 0);
                            vehicleOEMRow();
                            vehicleStepneyDetails(1);
                            addRow1();
                            setImages(0, 0, 0, 0, 0, 0);
                            hideAdds(1);
                            hideAdds1(1);">

                        <style>
                            body {
                                font:13px verdana;
                                font-weight:normal;
                            }
                        </style>
                        <form name="addVehicle" method="post" enctype="multipart/form-data" >
                            <%--<%@ include file="/content/common/path.jsp" %>--%>


                            <center><font color="black" size="4"><c:out value="${regNo}"/></font></center>
                                <%@ include file="/content/common/message.jsp" %>
                            <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="<c:out value="${fleetTypeId}"/>" />
                            <input type="hidden" id="cashOnHandLedgerId" name="cashOnHandLedgerId" value='<%=ThrottleConstants.CashOnHandLedgerId%>'>
                            <input type="hidden" id="cashOnHandLedgerCode" name="cashOnHandLedgerCode" value='<%=ThrottleConstants.CashOnHandLedgerCode%>'>
                            <br>

                            <font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                            <div align="center" id="status1">&nbsp;&nbsp;
                            </div>
                            </font>


                            <br>
                            <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="1" />
                            <div id="tabs">
                                <ul>
                                    <li><a href="#vehicleFitnessCertificate"><span><spring:message code="trucks.label.VehicleInspection"  text="default text"/></span></a></li>
                                    <li id="tab1"><a href="#vehicleInfo"><span><spring:message code="trucks.label.Vehicle"  text="default text"/></span></a></li>
                                    <li><a href="#FCDocuments"><span><spring:message code="trucks.label.FC Documents "  text="FC Documents"/></span></a></li>
                                   <!-- <li id="tab2"><a href="#vehicleOem"><span><spring:message code="trucks.label.OEM"  text="default text"/></span></a></li>
                                    <li id="tab3"><a href="#vehicleDepreciationTab"><span><spring:message code="trucks.label.Depreciation"  text="default text"/></span></a></li>-->
                                    <!--<li><a href="#vehicleFileAttachments"><span><spring:message code="trucks.label.FileAttachments"  text="default text"/></span></a></li>-->
                                </ul>
                                <div id="vehicleFitnessCertificate">
                                    <%--<c:if test="${vehicleFCDetails != null}">--%>
                                    <%--<c:forEach items="${vehicleFCDetails}" var="Fc">--%>
                                    <table class="table table-info mb30 table-hover">
                                        <thead>
                                            <tr>
                                                <th colspan="6"  height="30">
                                        <div  align="center">FC Details</div>
                                        </th>
                                        </tr>
                                        </thead>
                                        <tr>
                                            <td ><spring:message code="trucks.label.RTODetails"  text="default text"/></td>
                                            <td ><input type="text" name="rtoDetail" readonly id="rtoDetail" maxlength="25" class="form-control" style="width:260px;height:40px;"  value='<c:out value="${Fc.rtoDetail}" />' ></td>
                                            <td ><spring:message code="trucks.label.ApplicationNo"  text="Application No"/></td>
                                            <td ><input name="reportNo" id="reportNo" readonly type="text" class="form-control" style="width:260px;height:40px;" id="toDate1"  date-format="dd-mm-yyyy" style="width:250px" value='<c:out value="${Fc.reportNo}" />' ></td> 

                                        </tr>

                                        <tr>
                                            <td ><spring:message code="trucks.label.ReceiptNo"  text="default text"/></td>
                                            <td><input type="text" name="fcReceiptNo" readonly maxlength="25" id="fcReceiptNo" class="form-control" style="width:260px;height:40px;"  value='<c:out value="${Fc.fcReceiptNo}" />' ></td>  
                                            <td ><spring:message code="trucks.label.Odometer"  text="default text"/></td>
                                            <td ><input type="text" name="odometer" readonly id="odometer"  class="form-control" style="width:260px;height:40px;" value='<c:out value="${Fc.odometer}" />' ></td>
                                        </tr>
                                        <tr>
                                            <td ><spring:message code="trucks.label.DateofStart"  text="default text"/></td>
                                            <td ><input name="fcDate" id="fcDate" readonly type="text" class="form-control datepicker" style="width:260px;height:40px;" value='<c:out value="${Fc.fcDate}"/>' ></td>  

                                            <td ><spring:message code="trucks.label.DateofExpiry"  text="default text"/></td>
                                            <td ><input name="fcExpiryDate" id="fcExpiryDate" readonly type="text" class="form-control datepicker"    style="width:260px;height:40px;" value='<c:out value="${Fc.fcExpiryDate}"/>' ></td> 

                                        </tr>
                                        <tr>
                                            <td ><spring:message code="trucks.label.FCAmount"  text="default text"/></td>
                                            <td ><input type="text" name="fcAmount" maxlength="7" readonly id="fcAmount" class="form-control" style="width:260px;height:40px;"  value='<c:out value="${Fc.fcAmount}" />' onkeypress="return onKeyPressBlockCharacters(event)" onchange="fcAmountSet(this.value)"></td>
                                            <td ><spring:message code="trucks.label.Remarks"  text="default text"/></td>
                                            <td ><input type="text" name="fcRemarks" readonly id="fcRemarks"  class="form-control" style="width:260px;height:40px;" value='<c:out value="${Fc.fcRemarks}" />' ></td>
                                        <input type="hidden" id="fcid" name="fcid" value=''>
                                        </tr>

                                        <tr style="display:none">
                                            <td class="text2" id="vendorFC" style="visibility: hidden;" class="text1" height="30"><spring:message code="trucks.label.VendorName"  text="default text"/></td>
                                            <td class="text2" id="vendorFC1" style="visibility: hidden;"><select name="vendorIdFC" id="vendorIdFC" class="form-control" style="width:260px;height:40px;"  style="width:129px;height:20px;">
                                                    <c:if test="${vendorListCompliance != null}">
                                                        <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                                        <c:forEach items="${vendorListCompliance}" var="vendor">
                                                            <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
                                                        </c:forEach>
                                                    </c:if>          
                                                </select>
                                                <!--                        <script>
                                                                            document.getElementById("vendorIdFC").value = '<c:out value="${bankBranchIdFC}"/>'
                                                                        </script>-->
                                            </td>    
                                            <td class="text2" id="bankPaymentModeFC" style="visibility: hidden;" class="text1" style="width:129px;" height="30"><spring:message code="trucks.label.PaymentMode"  text="default text"/>:</td>
                                            <td class="text2" id="bankPaymentModeFC1" style="visibility: hidden;" class="text1" style="width:129px;" height="30">
                                                <select class="form-control" style="width:260px;height:40px;" name="paymentTypeFC" id="paymentTypeFC" style="width:129px" onchange="getFCBankPayment()" >
                                                    <option value="0">----<spring:message code="trucks.label.Select"  text="default text"/>----</option>
                                                    <!--<option value="1">Cash Payment</option>-->
                                                    <option value="2"><spring:message code="trucks.label.BankPayment"  text="default text"/></option>
                                                </select> 
                                            </td>
                                            <!--                    <script>
                                                                    document.getElementById("paymentTypeFC").value = '<c:out value="${paymentTypeFC}"/>'
                                                                </script>-->
                                        </tr>
                                        <tr style="display:none">
                                            <td class="text1" id="bankPaymentDateFC" style="visibility: hidden;"><spring:message code="trucks.label.PaymentDate"  text="default text"/></td>
                                            <td class="text1" id="bankPaymentDateFC1" style="visibility: hidden;"><input type="text" name="paymentDateFC" id="paymentDateFC" class="datepicker" style="width:129px;" value='' ></td>
                                        </tr>
                                        <c:if test="${paymentTypeFC == '2' || fcid == null}"> 
                                            <tr style="display:none">
                                            <tr id="bankPayment9" style="display:none">
                                                <td colspan="6" class="contenthead" height="30">
                                                    <div  class="contenthead" align="center"><spring:message code="trucks.label.BankPaymentDetails"  text="default text"/></div>
                                                </td>
                                            </tr>
                                            <tr id="bankPayment10" style="visibility: hidden;">
                                                <td class="text2" height="30"><spring:message code="trucks.label.BankHead"  text="default text"/></td>
                                                <td class="text2"><select name="bankIdFC" id="bankIdFC" class="form-control" style="width:260px;height:40px;"  onchange="getBankBranchFC(this.value)" style="width:129px; height:20px;">
                                                        <c:if test="${primarybankList != null}">
                                                            <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                                            <c:forEach items="${primarybankList}" var="bankVar">
                                                                <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>
                                                            </c:forEach>
                                                        </c:if>          
                                                    </select>
                                                    <!--                        <script>
                                                                                document.getElementById("bankIdFC").value = '<c:out value="${bankIdFC}"/>'
                                                                            </script>-->
                                                </td>
                                                <td class="text2" height="30"><spring:message code="trucks.label.Bankbranch"  text="default text"/> :</td>
                                                <td class="text2"> <select name="bankBranchIdFC" id="bankBranchIdFC" class="form-control" style="width:260px;height:40px;" style="width:129px; height:20px;">
                                                        <c:if test="${bankBranchList != null}">
                                                            <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                                            <c:forEach items="${bankBranchList}" var="branch">
                                                                <option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.bankname}"/></option>
                                                            </c:forEach>
                                                        </c:if>
                                                    </select>
                                                    <!--                         <script>
                                                                                document.getElementById("bankBranchIdFC").value ='<c:out value="${vendorIdFC}"/>'
                                                                            </script>            -->
                                                </td>

                                            </tr> 
                                            <tr id="bankPayment11" style="visibility: hidden;">
                                                <td class="text1"><font color="red">*</font><spring:message code="trucks.label.ChequeDate"  text="default text"/></td>
                        <!--                        <input type="hidden" id="fcid" name="fcid" value='<c:out value="${fcid}"/>'>
                                                <input type="hidden" id="clearanceIdFC" name="clearanceIdFC" value='<c:out value="${clearanceIdFC}"/>'>-->
                                                <!--<input type="hidden" id="fcid" name="fcid" value=''>-->
                                            <input type="hidden" id="clearanceIdFC" name="clearanceIdFC" value=''>
                                            <td class="text1" height="30"><input name="chequeDateFC" id="chequeDateFC" type="text" style="width:129px;" class="datepicker" value='' ></td>
                                            <td><font color="red">*</font><spring:message code="trucks.label.ChequeNo"  text="default text"/></td>
                                            <td height="30"><input name="chequeNoFC" id="chequeNoFC" style="width:129px;" maxlength="45" type="text" class="form-control" style="width:260px;height:40px;" value=''></td>
                                            </tr>
                                            <tr id="bankPayment12" style="visibility: hidden;">
                                                <td class="text2" height="30"><spring:message code="trucks.label.ChequeAmount"  text="default text"/></td>
                                                <td class="text2" height="30"><input name="chequeAmountFC" id="chequeAmountFC" style="width:129px;" maxlength="7" type="text" class="form-control" style="width:260px;height:40px;" value='' onkeypress="return blockNonNumbers(this, event, true, false);" readonly></td>

                                            </tr>

                                            <!--</table>-->

                                        </c:if>
                                        <!--                    <tr>
                                                             <br>
                                        
                                        <%
                                                    int index3 = 1;

                                        %>
                                        
                                        
                                        <c:if test="${FCList == null }" >
                                            <br>
                                            <center><font color="red" size="2"> no records found </font></center>
                                        </c:if>
                                        <c:if test="${FCList != null }" >
                                            <table align="center" width="700" cellpadding="0" cellspacing="0"  class="border">
                            
                                                <tr>
                            
                                                    <td class="contentsub" height="30"><div class="contentsub">Sno</div></td>
                                                    <td class="contentsub" height="30"><div class="contentsub">Vehicle Number</div></td>
                                                    <td class="contentsub" height="30"><div class="contentsub">FC Date</div></td>
                                                    <td class="contentsub" height="30"><div class="contentsub">RDO Detail</div></td>
                                                    <td class="contentsub" height="30"><div class="contentsub">Receipt Number</div></td>
                                                    <td class="contentsub" height="30"><div class="contentsub">FC Amount</div></td>
                                                    <td class="contentsub" height="30"><div class="contentsub">FC Expiry Date</div></td>
                                                    <td class="contentsub" height="30"><div class="contentsub">&nbsp;</div></td>
                                                </tr>
                                            <%
            String style2 = "text1";%>
                                            <c:forEach items="${FCList}" var="FCL" >
                                                <%
                                    if ((index3 % 2) == 0) {
                                        style2 = "text1";
                                    } else {
                                        style2 = "text2";
                                    }%>
                                                <tr>
                                                    <td class="<%= style2 %>" height="30" style="padding-left:30px; "> <%= index3 %> </td>
                                                    <td class="<%= style2 %>" height="30" style="padding-left:30px; "> <c:out value="${FCL.regNo}" /></td>
                                                    <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.fcdate}" /></td>
                                                    <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.rtodetail}" /></td>
                                                    <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.receiptno}" /></td>
                                                    <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.fcamount}" /></td>
                                                    <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.fcexpirydate}" /></td>
                                                    <td class="<%= style2 %>" height="30" style="padding-left:30px; "><a href='/throttle/vehicleFCDetail.do?vehicleId=<c:out value="${FCL.vehicleId}" />&fcid=<c:out value="${FCL.fcid}" />'>alter </a> </td>
                                                </tr>
                                                <% index3++; %>
                                            </c:forEach>
                                        </table>
                                        </c:if>
                                        <br>    
                                                </tr>-->
                                    </table>
                                </c:forEach>
                            </c:if >
                            <center>

                    <!--<input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Save"  text="default text"/>" id="fcNext" name="Save"  onClick="updateFCDetails();" style="visibility: block"/>-->
                                <a  class="nexttab" ><input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Next"  text="default text"/>" name="Next" /></a>

                            </center>
                            <script>
                                function fcAmountSet(val) {
                                    document.getElementById("chequeAmountFC").value = val;
                                }
                                function getBankBranchFC(bankIdFC) {
                                    var temp = "";
                                    $.ajax({
                                        url: '/throttle/getBankBranchDetails.do',
                                        data: {bankId: bankIdFC},
                                        dataType: 'json',
                                        success: function(temp) {
                                            //                                             alert(url);
                                            if (temp != '') {
                                                $('#bankBranchIdFC').empty();
                                                $('#bankBranchIdFC').append(
                                                        $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                                                        )
                                                $.each(temp, function(i, data) {
                                                    $('#bankBranchIdFC').append(
                                                            $('<option value="' + data.branchId + "~" + data.bankLedgerId + '" style="width:150px"></option>').val(data.branchId + "~" + data.bankLedgerId).html(data.bankName)
                                                            )
                                                    document.getElementById("bankBranchIdFC").value = '<c:out value="${vendorIdFC}"/>'
                                                });
                                            } else {
                                                $('#bankBranchIdFC').empty();
                                            }
                                        }
                                    });
                                }

                                function getFCBankPayment() {
                                    var val = document.getElementById("paymentTypeFC").value;
                                    //                           alert(ownerShips);
                                    if (val == 2) {
                                        document.getElementById("bankPayment9").style.visibility = 'visible';
                                        document.getElementById("bankPayment10").style.visibility = 'visible';
                                        document.getElementById("bankPayment11").style.visibility = 'visible';
                                        document.getElementById("bankPayment12").style.visibility = 'visible';
                                    }
                                    else if (val == 1) {
                                        document.getElementById("bankPayment9").style.visibility = 'hidden';
                                        document.getElementById("bankPayment10").style.visibility = 'hidden';
                                        document.getElementById("bankPayment11").style.visibility = 'hidden';
                                        document.getElementById("bankPayment12").style.visibility = 'hidden';
                                    }
                                }


                            </script>


                        </div>

                        <div id="FCDocuments">
                            <%int img1 = 0;%>
                            <c:if test="${vehicleUploadFileDetails != null}">
                                <table class="table table-info"   style="width: 950px;" align="center">
                                    <thead>
                                    <th><spring:message code="trucks.label.SNo"  text="default text"/></th>
                                    <th>&emsp;&emsp;&emsp;&emsp;<spring:message code="trucks.label.FileName"  text="default text"/></th>
                                    <th>&nbsp;<spring:message code="trucks.label.Remarks"  text="default text"/></th>
                                    <th>&nbsp;Updated At</th>
                                    <th>&nbsp;Action</th>
                                    </thead>
                                    <c:forEach items="${vehicleUploadFileDetails}" var="ins">
                                        <tr>
                                            <td>&nbsp;<%=img1+1%></td>

                                            <td>
                                                <c:if test="${ins.activeInd == 'Y'}">
                                                    <font style="color:green">
                                                </c:if>                         
                                                <c:if test="${ins.activeInd == 'N'}">
                                                    <font style="color:red">
                                                </c:if>   &emsp;&emsp;<c:out value ="${ins.fileName}"/></font></td>
                                            <td><c:if test="${ins.activeInd == 'Y'}">
                                                    <font style="color:green">
                                                </c:if>                         
                                                <c:if test="${ins.activeInd == 'N'}">
                                                    <font style="color:red">
                                                </c:if>   <c:out value ="${ins.remarks}"/></font></td>
                                            <td><c:if test="${ins.activeInd == 'Y'}">
                                                    <font style="color:green">
                                                </c:if>                         
                                                <c:if test="${ins.activeInd == 'N'}">
                                                    <font style="color:red">
                                                </c:if>   <c:out value ="${ins.expiryDate}"/></font></td>

                                            <td><a href="#" onclick="showPdf(<c:out value ="${ins.uploadId}"/>);">
                                                    <img src="images/download.png" title="click here to download" style="height:30px;width:30px;margin-left:-01px;"></td>
                                                    </tr>
            <!--                                        <div class="modal fade" id="myModal<%=img1%>" role="dialog" style="width: 100%;height: 100%">
                                                        <div class="modal-dialog modal-lg">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()"></button>
                                                                    <h4 class="modal-title"><c:out value ="${ins.fileName}"/></h4>
                                                                </div>
                                                                <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 80%">
                                                                    <img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${ins.uploadId}"/>" style="width: 95%;height: 75%" class="img-responsive" title="<c:out value ="${ins.remarks}"/>">
                                                                </div>
                                                                <div class="modal-footer">
            
            
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>   -->
                                                    <input  type="hidden" id="uploadId" name="uploadId" value="<c:out value ="${ins.uploadId}"/>"/>
                                                    <%img1++;%>
                                                </c:forEach>
                                </table>
                            </c:if>

                        </div>

                        <div id="vehicleInfo">
                            <input type="hidden" name="regNoCheck" id="regNoCheck" value='' >
                            <table border="0" class="border" align="center" width="1000" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td colspan="6">
                                        <table class="table table-info mb30 table-hover">
                                            <thead>
                                            <th colspan="6"  height="30">
                                            <div  align="center"><spring:message code="trucks.label.Vehicle"  text="default text"/> </div>
                                            </th>
                                            </thead>
                                            <tr width="100%">
                                            <div id="groupInfo" style="display: none;">

                                                <td height="30" width="20%"><font color="red" size="4">*</font><font style="bold"><spring:message code="trucks.label.VehicleOwnerShip"  text="default text"/></font></td>
                                                <td width="20%">  <select class="form-control" style="width:260px;height:40px;" name="ownerShips" id="ownerShips"   disabled>
                                                        <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                                        <option value="1" ><spring:message code="trucks.label.Own"  text="default text"/></option>
                                                        <option value="2" ><spring:message code="trucks.label.Dedicate"  text="default text"/></option>
                                                        <option value="3" ><spring:message code="trucks.label.Hire"  text="default text"/> </option>
                                                        <option value="4" ><spring:message code="trucks.label.Replacement"  text="default text"/></option>
                                                        <input type="hidden" name="groupNo" id="groupNo" class="form-control" style="width:260px;height:40px;" value="1">
                                                    </select>
                                                    <script>
                                                        document.getElementById("ownerShips").value = '<c:out value="${ownership}"/>';
                                                    </script>
                                                    <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>" /></td>
                                            </div>

                                            <input type="hidden" name="asset" id="asset" class="form-control" style="width:260px;height:40px;" value="1">
                                            <td  colspan="4">
                                                <div id="attachInfo" style="display: none">
                                                    <table width="100%" cellpadding="0" cellspacing="2"  id="vandornameId" >
                                                        <tr>
                                                            <td height="30%" width="20%"><font color="red">*</font><spring:message code="trucks.label.VendorName"  text="default text"/></td>

                                                            <td width="10%">
                                                                <c:if test = "${vendorNameList != null}" >
                                                                    <c:forEach items="${vendorNameList}" var="vnl">
                                                                        <input type="text" id ="vendorId" value='<c:out value="${vnl.vendorId}"/>' />
                                                                        <input type="text" id ="vendorName" value='<c:out value="${vnl.vendorName}"/>' />
                                                                    </c:forEach >
                                                                </c:if>
                                                                <c:if test = "${leasingCustList != null}" >
                                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <select class="form-control" style="width:260px;height:40px;" name="vendorId" id="vId"   disabled>
                                                                        <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                                                        <c:forEach items="${leasingCustList}" var="LCList">
                                                                            <option value='<c:out value="${LCList.vendorId}" />'><c:out value="${LCList.vendorName}" /></option>
                                                                        </c:forEach >
                                                                    </c:if>
                                                                </select>
                                                                <script>
                                                                    document.getElementById("vId").value = '<c:out value="${vendorId}"/>';
                                                                </script>        
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                </tr>
                                <tr>
                                    <td height="30"><font color="red">*</font><spring:message code="trucks.label.VehicleNumber"  text="default text"/></td>
                                    <td height="30"><input maxlength='13'  name="regNo" type="text" class="form-control" style="width:260px;height:40px;" value="<c:out value="${regNo}"/>" readonly ></td>


                                    <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.MFRs"  text="default text"/></td>
                                    <td  width="20%" style="border-right-color:#5BC0DE;"><select class="form-control" style="width:260px;height:40px;" name="mfrId" id="mfrId"   disabled>
                                            <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                            <c:if test = "${MfrList != null}" >
                                                <c:forEach items="${MfrList}" var="Dept">
                                                    <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById("mfrId").value = '<c:out value="${mfrId}"/>';
                                        </script>
                                    </td>


                                </tr>
                                <tr>
                                    <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.VehicleType"  text="default text"/></td>
                                    <td  width="20%">
                                        <input type="hidden" name="typeId" id="typeId" value="<c:out value="${typeId}"/>" />
                                        <input type="hidden" name="axleTypeId" id="axleTypeId" value="<c:out value="${axleTypeId}"/>" />
                                        <select class="form-control" style="width:260px;height:40px;" name="vehicleTypeId" id="vehicleTypeId" disabled >
                                            <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                            <c:if test = "${TypeList != null}" >
                                                <c:forEach items="${TypeList}" var="Type">
                                                    <option value='<c:out value="${Type.typeId}" />~<c:out value="${Type.axleTypeId}" />'><c:out value="${Type.typeName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <c:if test="${vehicleId != null}">
                                            <script>
                                                document.getElementById("vehicleTypeId").value = '<c:out value="${typeId}"/>~<c:out value="${axleTypeId}"/>';
                                                    getModelAndTonnage('<c:out value="${typeId}"/>~<c:out value="${axleTypeId}"/>');
                                            </script>
                                        </c:if>
                                    </td>

                                    <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.Model"  text="default text"/> </td>
                                    <td style="border-right-color:#5BC0DE;" height="30" >
                                        <select class="form-control" style="width:260px;height:40px;" name="modelId" id="modelId"  disabled>
                                            <option value="0">---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                        </select>
                                        <script>
                                            document.getElementById("modelId").value = '<c:out value="${modelId}"/>'
                                        </script>
                                    </td>
                                </tr>
                            </table>
                            </td></tr>

                            <tr>
                                <td colspan="6">

                                    <div id="hiredGroup" >
                                        <table class="table table-info mb30 table-hover">
                                            <tr  width="20%">
                                                <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.RegistrationDate"  text="default text"/></td>

                                                <td height="30" width="20%"><input name="dateOfSale" type="text" class="datepicker" value="<c:out value="${dateOfSale}" />" size="20" style="width:260px;height:40px;" disabled>

                                                </td>
                                                <!--                    <td></td>-->

                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.FleetCenter"  text="default text"/></td>
                                                <td  >
                                                    <select class="form-control" style="width:260px;height:40px;" name="opId" id="opId"   disabled>
                                                        <c:if test = "${OperationPointList != null}" >
                                                            <c:forEach items="${OperationPointList}" var="Dept">
                                                                <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /> </option>
                                                            </c:forEach >
                                                        </c:if>
                                                    </select>
                                                    <script>
                                                        document.getElementById("opId").value = '<c:out value="${opId}"/>';
                                                    </script>
                                                </td>
                                            </tr>

                                            <tr width="20%">
                                                <td height="30" ><font color="red">*</font><spring:message code="trucks.label.EngineNo"  text="default text"/></td>
                                                <td height="30" ><input maxlength='20' name="engineNo" type="text" class="form-control" style="width:260px;height:40px;"readonly value="<c:out value="${engineNo}"/>" size="20" ></td>
                                                <td height="30" width="20%"><font color="red">*</font><spring:message code="trucks.label.ChassisNo"  text="default text"/></td>
                                                <td height="30" width="20%"><input maxlength='20'  type="text" name="chassisNo" size="20" readonly class="form-control" style="width:260px;height:40px;" value="<c:out value="${chassisNo}"/>" > </td>
                                            </tr>
                                            <tr>
                                                <td height="30" style="display: none"><font color="red">*</font><spring:message code="trucks.label.NoOfAxles"  text="default text"/></td>
                                                <td height="30" style="display: none"><select name="axles" class="form-control" style="width:260px;height:40px;"  disabled>
                                                        <option value="2"><spring:message code="trucks.label.DoubleAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.TripleAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.FourAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.FiveAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.SixAxle"  text="default text"/></option>
                                                        <option value="3"><spring:message code="trucks.label.MoreThan6Axle"  text="default text"/></option>
                                                    </select></td>
                                                <td height="30"><spring:message code="trucks.label.Tonnage(MT)"  text="default text"/></td>
                                                <td height="30"><input name="seatCapacity" id="seatCapacity" type="text" readonly class="form-control" style="width:260px;height:40px;" value="<c:out value="${seatCapacity}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="15" value="0" size="20" ></td>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.WarrantyDate"  text="default text"/></td>
                                                <td height="30"><input name="warrantyDate" id="warrantyDate" style="width:260px;height:40px;" type="text" disabled class="datepicker" value="<c:out value="${warrantyDate}"/>" size="20" onchange="CalWarrantyDate(this.value)" ></td>
                                                <td height="30"><spring:message code="trucks.label.Warranty(days)"  text="default text"/></td>
                                                <td height="30"><input name="war_period" id="war_period" type="text" readonly  class="form-control" style="width:260px;height:40px;" value="<c:out value="${warPeriod}"/>" size="20"  onKeyPress="return onKeyPressBlockCharacters(event);" ></td>
                                            </tr>
                                            <tr>
                                                <td height="30"><spring:message code="trucks.label.VehicleCostAsOnDate"  text="default text"/></td>
                                                <td  ><input type="text" name="vehicleCost" class="form-control" style="width:260px;height:40px;" maxlength="15" readonly value="<c:out value="${vehicleCost}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="setVehicleCost(this.value);" /></td>
                                                <td height="30"><spring:message code="trucks.label.Depreciation"  text="default text"/>(%)</td>
                                                <td  ><input type="text" name="vehicleDepreciation" id="vehicleDepreciation" readonly class="form-control" style="width:260px;height:40px;" maxlength="15" value="<c:out value="${vehicleDepreciation}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            </tr>
                                            <script>
                                                function setVehicleCost(value) {
                                                    $("#vehicleDepreciationCost").val(value);
                                                }
                                            </script>
                                            <tr>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.ColdStorage"  text="default text"/></td>
                                                <td  ><select class="form-control" style="width:260px;height:40px;" name="classId" id="classId"  disabled>
                                                        <c:if test = "${ClassList != null}" >
                                                            <c:forEach items="${ClassList}" var="Dept">
                                                                <option value="<c:out value="${Dept.classId}" />"><c:out value="${Dept.className}" /></option>
                                                            </c:forEach >
                                                        </c:if>
                                                    </select>
                                                    <script>
                                                        document.getElementById("classId").value = "1012";
                                                    </script>
                                                </td>
                                                <td height="30"> <font color="red">*</font><spring:message code="trucks.label.KMReading"  text="default text"/></td>
                                                <td  ><input maxlength='20'  name="kmReading" type="text" class="form-control" style="width:260px;height:40px;" readonly value="<c:out value="${kmReading}"/>" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" ></td>
                                            </tr>

                                            <tr>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.DailyRunKM"  text="default text"/></td>
                                                <td height="30"> <input  type="text"  id="dailyKm"  name="dailyKm" readonly class="form-control" style="width:260px;height:40px;" maxlength="10" value="<c:out value="${dailyKm}"/>" size="20"  onKeyPress="return onKeyPressBlockCharacters(event);" > </td>
                                                <td height="30"> <font color="red">*</font><spring:message code="trucks.label.DailyRunHM"  text="default text"/></td>
                                                <td  > <input type="text" id="dailyHm" name="dailyHm"  class="form-control" style="width:260px;height:40px;" readonly value="<c:out value="${dailyHm}"/>" maxlength="10" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" ></td>
                                            </tr>
                                            <tr>
                                                <td height="30"> <font color="red">*</font><spring:message code="trucks.label.VehicleColor"  text="default text"/></td>
                                                <td><input type="text"  name="vehicleColor" id="vehicleColor" class="form-control" style="width:260px;height:40px;" readonly maxlength="20" value="<c:out value="${vehicleColor}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" ></td>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.VehicleUsage"  text="default text"/></td>
                                                <td  ><select class="form-control" style="width:260px;height:40px;" name="usageId"   disabled>
                                                        <option value="1" ><spring:message code="trucks.label.ShortTrip"  text="default text"/></option>
                                                        <option value="2" selected><spring:message code="trucks.label.LongTrip"  text="default text"/></option>
                                                    </select></td>
                                            </tr>
                                            <tr>

                                                <td height="30"> <font color="red">*</font><spring:message code="trucks.label.GPSTrackingSystem"  text="default text"/></td>
                                                <td  >
                                                    <select class="form-control" style="width:260px;height:40px;" name="gpsSystem" id="gpsSystem" onchange="openGpsInfo(this.value)"  disabled>
                                                        <option value='Yes' ><spring:message code="trucks.label.Yes"  text="default text"/></option>
                                                        <option value='No' selected><spring:message code="trucks.label.No"  text="default text"/></option>
                                                    </select>
                                                    <script type="text/javascript">
                                                        function openGpsInfo(val) {
                                                            if (val == 'Yes') {
                                                                document.getElementById("trckingId").style.display = "block";
                                                            } else {
                                                                document.getElementById("trckingId").style.display = "none";
                                                            }
                                                        }
                                                    </script>
                                                </td>
                                                <td> &nbsp;<input type="hidden" name="groupId" value="0"/><spring:message code="trucks.label.Status"  text="default text"/></td> 
                                                <td>
                                                    <select name="activeInd" id="activeInd"   class="form-control" style="width:260px;height:40px;" disabled>
                                                        <option value="Y"><spring:message code="trucks.label.Active"  text="default text"/></option>
                                                        <option value="N"><spring:message code="trucks.label.InActive"  text="default text"/></option>
                                                    </select>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td><spring:message code="trucks.label.Remarks"  text="default text"/> :</td>
                                                <td  colspan="3"><textArea id="description" name="description" cols="5" rows="5" style="width:260px;height:40px;" readonly><c:out value="${description}" /></textArea></td>
                                                                        </tr>



                                <script type="text/javascript">


                                    function ChangeDropdowns(value) {

                                        if (value == "1") {
                                            document.getElementById("attachInfo").style.display = "none";
                                        }

                                        if (value == "2" || value == "3" || value == "4") {

                                            document.getElementById("attachInfo").style.display = "block";
                                        }
                                        //
                                        if (value == "1") {
                                            document.getElementById("hiredGroup").style.display = "block";
                                            //                         document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2" || value == "3" || value == "4") {
                                            document.getElementById("hiredGroup").style.display = "none";
                                        }
                                        if (value == "1") {
                                            document.getElementById("hiredGroup").style.display = "block";
                                            //                         document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("hiredGroup").style.display = "none";
                                        }
                                        if (value == "2" || value == "3" || value == "4") {

                                            document.getElementById("attachInfo").style.display = "block";
                                        }

                                        //    function hideAdd(value){

                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("add").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "1") {
                                            document.getElementById("add").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }

                                        //    function hideAdds(value){


                                        if (value == "1" || value == "2") {
                                            document.getElementById("adds").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4") {
                                            document.getElementById("adds").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }

                                        //       function hideAdds1(value){


                                        if (value == "1" || value == "3" || value == "4") {
                                            document.getElementById("adds1").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2") {
                                            document.getElementById("adds1").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }


                                        if (value == "2") {
                                            document.getElementById("groupNo").style.display = "block";

                                        }
                                        if (value == "1") {
                                            document.getElementById("groupNo").style.display = "block";
                                        }

                                        if (value == "4") {
                                            ocument.getElementById("groupNo").style.display = "block";
                                        }

                                        if (value == "3") {
                                            document.getElementById("groupNo").style.display = "block";
                                        }

                                        $('#vandornameId').show();

                                        if (value != "1") {
                                            document.getElementById("bankPayment1").style.display = "block";
                                            document.getElementById("bankPayment2").style.display = "block";
                                            document.getElementById("bankPayment3").style.display = "block";
                                            document.getElementById("bankPayment4").style.display = "block";
                                        }
                                        else {
                                            document.getElementById("bankPayment1").style.display = "none";
                                            document.getElementById("bankPayment2").style.display = "none";
                                            document.getElementById("bankPayment3").style.display = "none";
                                            document.getElementById("bankPayment4").style.display = "none";
                                        }

                                    }

                                    function hideAdd(value) {

                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("add").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "1") {
                                            document.getElementById("add").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }
                                    function hideAdds(value) {


                                        if (value == "1" || value == "2") {
                                            document.getElementById("adds").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4") {
                                            document.getElementById("adds").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }
                                    function hideAdds1(value) {


                                        if (value == "1" || value == "3" || value == "4") {
                                            document.getElementById("adds1").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2") {
                                            document.getElementById("adds1").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }


                                                                                                                                                                                                        </script>
</table></div>
</td>
</tr>
<tr>
                                <td colspan="6">
                                    <div id="trckingId" style="display: none;">
                                        <table width="50%" cellpadding="0" cellspacing="2">
                                            <tr>
                                                <td height="30"><font color="red">*</font><spring:message code="trucks.label.GPSSystemId"  text="default text"/></td>
                                                <td align="center">
                                                    <select class="form-control" style="width:260px;height:40px;" name="gpsSystemId" >
                                                        <option value="1" selected>0001</option>
                                                        <option value="2" >0002</option>
                                                        <option value="3" >0003</option>
                                                    </select>
                                                </td>
                                                <td height="30">&nbsp;</td>
                                                <td>&nbsp;</td>

                                            </tr>
                                        </table>
                        </div> </td>
</tr>
</table>
                    <center>
                            <a  class="nexttab" ><input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Next"  text="default text"/>" name="Next" /></a>

                    </center>
            </div>                       
            <div id="vehicleOem" style="display:none;">
                 <table class="table table-info mb30 table-hover" id="positionViewTable">
                <thead>
                    <tr>
                        <th><spring:message code="trucks.label.AxleType"  text="default text"/></th>
                        <th><spring:message code="trucks.label.Left"  text="default text"/></th>
                        <th><spring:message code="trucks.label.Right"  text="default text"/></th>
                        <th></th>
                        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>
                    </tr>
                    </thead>
                </table>

                <br> <br>
                  <script type="text/javascript">
                      function getVehAxleName(str) {
                          var axleTypeId = str.split("~")[1];
                          var typeId = axleTypeId;
                          $("#axleTypeId").val(axleTypeId);
                          //                        var typeId=document.getElementById("typeId").value;

                          var axleTypeName = "";
                          var leftSideTyreCount = 0;
                          var rightSideTyreCount = 0;
                          var axleDetailId = 0;
                          var count = 1;

                          $.ajax({
                              url: '/throttle/getVehAxleName.do',
                              // alert(url);
                              data: {typeId: typeId},
                              dataType: 'json',
                              success: function(data) {
                                  if (data !== '') {
                                      $.each(data, function(i, data) {
                                          axleTypeName = data.AxleTypeName;
                                          leftSideTyreCount = data.LeftSideTyreCount;
                                          rightSideTyreCount = data.RightSideTyreCount;
                                          axleDetailId = data.AxleDetailId;
                                          positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, count, axleDetailId);
                                          count++;
                                      });
                                  }
                              }
                          });
                      }


                      function positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, rowCount, axleDetailId) {
                          var rowCount = "";
                          var style = "text2";

                          rowCount = document.getElementById('selectedRowCount').value;
                          rowCount++;
                          var tab = document.getElementById("positionViewTable");
                          var newrow = tab.insertRow(rowCount);
                          newrow.id = 'rowId' + rowCount;

                          cell = newrow.insertCell(0);
                          var cell2 = "<td class='text1' height='30'><input type='hidden' name='vehicleAxleDetailId' id='vehicleAxleDetailId" + rowCount + "' value='" + axleDetailId + "'  class='form-control' /><input type='hidden' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='" + axleTypeName + "'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                          //                        var cell2 = "<td class='text1' height='30'><input type='text' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='"+ axleTypeName +"'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell2;

                          cell = newrow.insertCell(1);
                          var cell3 = "<td height='30' class='tex1' name='leftTyreIds'  id='leftTyreIds2' colspan=" + leftSideTyreCount + ">";
                          for (var i = 0; i < leftSideTyreCount; i++) {
                              cell3 += parseInt(i + 1) + ":<input type='text'  id='tyreNoLeft" + axleDetailId + parseInt(i + 1) + "'/>Depth<input type='text' maxlength='3'  name='treadDepthLeft' id='treadDepthLeft" + axleDetailId + parseInt(i + 1) + "' style='width:25px;' onkeypress='return blockNonNumbers(this, event, true, false);'/>mm<select name='leftItemIds' id='tyreMfrLeft" + axleDetailId + parseInt(i + 1) + "' class='form-control' onchange='updateVehicleTyreDetails(" + axleDetailId + ",0," + i + ",this.value,1);'><option value='0'>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select><br>";
                          }
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell3 + "</td>";
                          for (var i = 0; i < leftSideTyreCount; i++) {
                              fillVehicleTyreDetails(axleDetailId, 0, i);
                          }

                          cell = newrow.insertCell(2);
                          var cell4 = "<td class='text1' height='30' name='rightTyreIds' id='rightTyreIds2' colspan=" + rightSideTyreCount + ">";
                          for (var j = 0; j < rightSideTyreCount; j++) {
                              cell4 += parseInt(j + 1) + ":<input type='text'  id='tyreNoRight" + axleDetailId + parseInt(j + 1) + "' />Depth<input type='text' maxlength='3' name='treadDepthRight' id='treadDepthRight" + axleDetailId + parseInt(j + 1) + "' style='width:25px;' onkeypress='return blockNonNumbers(this, event, true, false);'/>mm<select name='rightItemIds' id='tyreMfrRight" + axleDetailId + parseInt(j + 1) + "' class='form-control' onchange='updateVehicleTyreDetails(" + axleDetailId + ",1," + j + ",this.value,1);'><option value='0'>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select><br>";
                          }
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell4 + "</td>";
                          for (var j = 0; j < rightSideTyreCount; j++) {
                              fillVehicleTyreDetails(axleDetailId, 1, j);
                          }


                          cell = newrow.insertCell(3);
                          var cell5 = "<td class='text1' height='30'><input type='hidden' name='position' id='position'  value='" + parseInt(i) + "' ></td>";
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell5;

                          document.getElementById('selectedRowCount').value = rowCount;


                      }

                      function fillVehicleTyreDetails(axleDetailId, positionName, positionNo) {
                          var vehicleId = $("#vehicleId").val();
                          var axleTypeId = $("#axleTypeId").val();
                          if (vehicleId != 0) {
                              positionNo = parseInt(positionNo + 1);
                              if (positionName == '0') {
                                  positionName = 'Left';
                              } else {
                                  positionName = 'Right';
                              }
                              $.ajax({
                                  url: '/throttle/getVehAxleTyreNo.do',
                                  // alert(url);
                                  data: {axleDetailId: axleDetailId, positionName: positionName,
                                      positionNo: positionNo, vehicleId: vehicleId, axleTypeId: axleTypeId},
                                  dataType: 'json',
                                  success: function(data) {
                                      if (data !== '') {
                                          $.each(data, function(i, data) {
                                              //                                                axleTypeName = data.AxleTypeName;
                                              document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = data.TyreNo;
                                              document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = data.TyreDepth;
                                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = data.TyreMfr;
                                          });
                                      }
                                  }
                              });
                          }
                          //                            alert(document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value);
                          //                            document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = 100;
                      }
                      function updateVehicleTyreDetails(axleDetailId, positionName, positionNo, val, updateType) {
                          var vehicleId = $("#vehicleId").val();
                          positionNo = parseInt(positionNo + 1);
                          var url = '';
                          if (positionName == '0') {
                              positionName = 'Left';
                          } else {
                              positionName = 'Right';
                          }
                          if (updateType == 1) {

                          }
                          var treadDepth = document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value;
                          var tyreNo = document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value;
                          if (treadDepth == '') {
                              alert("Please fill tread depth for " + positionName + " " + positionNo);
                              document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).focus();
                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                              return;
                          } else if (tyreNo == '') {
                              alert("Please fill tyre no for " + positionName + " " + positionNo);
                              document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                              return;
                          } else {
                              url = './updateVehicleTyreNo.do';
                              $.ajax({
                                  url: url,
                                  data: {
                                      axleDetailId: axleDetailId, positionName: positionName, positionNo: positionNo,
                                      updateValue: val, depthVal: treadDepth, updateType: updateType, vehicleId: vehicleId,
                                      tyreNo: tyreNo
                                  },
                                  type: "GET",
                                  success: function(response) {
                                      if (response.toString().trim() == 0) {
                                          document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = '';
                                          document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = '';
                                          document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                                          alert("Tyre No Already Mapped Please Enter Valid Tyre No");
                                          document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                                      }
                                  },
                                  error: function(xhr, status, error) {
                                  }
                              });
                          }

                      }

                      //jp End Ajex insCompny list
                                                                                                                                        </script>

                    <table class="table table-info mb30 table-hover"  id="oEMtable">
                    <thead>
                    <tr >
                        <th  align="center" height="30" ><spring:message code="trucks.label.SNo"  text="default text"/></th>
                        <th   align="center" height="30"><spring:message code="trucks.label.Stepney/Battery"  text="default text"/></th>
                        <th   align="center" height="30"><spring:message code="trucks.label.Details"  text="default text"/></th>
                    </tr>
                    </thead>

                    
                 <input type="hidden" name="oemIdCount" id="oemIdCount" value="<c:out value="${oemListSize}"/>" />
                <input type="hidden" name="rowCounted" id="rowCounted" value=""/>
               <script>
                   function vehicleStepneyDetails(val, sno) {

                       if (val == 1) {

                           $("#oemMfr" + sno).show();
                           $("#oemDepth" + sno).show();
                           $("#oemTyreNo" + sno).show();
                           $("#oemBattery" + sno).hide();
                           //                        
                       } else if (val == 2) {
                           $("#oemBattery" + sno).show();
                           $("#oemMfr" + sno).hide();
                           $("#oemDepth" + sno).hide();
                           $("#oemTyreNo" + sno).hide();

                       }
                   }

                                                                                                                                                                                      </script>
               <script>

                   function insertVehicleAxle() {

                       var vehicleId = $("#vehicleId").val();
                       var url = '';
                       var oemId = [];
                       var oemDetailsId = [];
                       var oemMfr = [];
                       var oemDepth = [];
                       var oemTyreNo = [];
                       var oemBattery = [];
                       var oemDetailsIds = document.getElementsByName("oemDetailsId");
                       var oemMfrs = document.getElementsByName("oemMfr");
                       var oemDepths = document.getElementsByName("oemDepth");
                       var oemTyreNos = document.getElementsByName("oemTyreNo");
                       var oemBatterys = document.getElementsByName("oemBattery");
                       var oemIds = document.getElementsByName("oemId");
                       for (var i = 0; i < oemDetailsIds.length; i++) {
                           oemDetailsId.push(oemDetailsIds[i].value);
                           oemMfr.push(oemMfrs[i].value);
                           oemTyreNo.push(oemTyreNos[i].value);
                           oemDepth.push(oemDepths[i].value);
                           oemBattery.push(oemBatterys[i].value);
                           oemId.push(oemIds[i].value);
                           if (oemDetailsIds[i].value == 1) {
                               if (oemMfrs[i].value == '') {
                                   alert('Please select MFR ');
                                   return;
                               } else if (oemTyreNos[i].value == '') {
                                   alert('Please Enter The TyreNo ');
                                   return;
                               } else if (oemDepths[i].value == '') {
                                   alert('Please Enter The Depth ');
                                   return;
                               }
                           } else if (oemDetailsIds[i].value == 2) {
                               if (oemBattery[i].value == '') {
                                   alert('Please select battty ');
                                   return;
                               }
                           }

                       }
                       var insertStatus = 0;
                       var oemIdResponse = "";
                       url = './insertOemDetails.do';
                       $("#oEmNext").hide();
                       $.ajax({
                           url: url,
                           data: {oemDetailsIdVal: oemDetailsId, vehicleId: vehicleId, oemMfrVal: oemMfr, oemTyreNoVal: oemTyreNo, oemDepthVal: oemDepth,
                               oemBatteryVal: oemBattery, oemIdVal: oemId
                           },
                           type: "GET",
                           dataType: 'json',
                           success: function(data) {
                               var r = 0;
                               $.each(data, function(i, data) {
                                   //                                alert(data.OemId);
                                   oemIds[r].value = data.OemId;
                                   r++;
                               });
                               $("#Status").text("Vehicle OEM added sucessfully ");
                               $("#oEmNext").show();
                           },
                           error: function(xhr, status, error) {
                           }
                       });
                       //                        $("#oEmNext").hide();
                   }

                   function addOem(sno, oemDetailsId, batteryNo, tyreNo, depth, mfrId, oemId, addType) {
                       var rowCount = sno;
                       var style = "text2";
                       rowCount = document.getElementById('rowCounted').value;
                       rowCount++;


                       var tab = document.getElementById("oEMtable");
                       var newrow = tab.insertRow(rowCount);
                       newrow.id = 'rowId' + rowCount;

                       cell = newrow.insertCell(0);
                       var cell0 = "<td class='text1' height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell0;

                       cell = newrow.insertCell(1);
                       var cell1 = "";
                       var oemDetailsName = "";
                       if (oemDetailsId == 1) {
                           oemDetailsName = "Stepney";
                       } else {
                           oemDetailsName = "Battery";
                       }
                       if (addType == 1) {
                           cell1 = "<td class='text1' height='30'><input type='hidden' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "'>'" + oemDetailsName + "'</td>";
                       } else {
                           cell1 = "<td class='text1' height='30'><select class='form-control' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "' onchange='batteryTextHideShow(this.value," + rowCount + ");'><option value='1'>Stepney</option><option value='2'>battery</option></select></td>";
                       }
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell1;

                       cell = newrow.insertCell(2);
                       var cell2 = "<td  height='30' class='tex1' ><span id='batterySpan" + rowCount + "' style='display:none'>Bat:<input type='text'  name='oemBattery' id='oemBattery" + rowCount + "' maxlength='13' placeholder='BatteryNo'  size='20' class='form-control' value='" + batteryNo + "'  /></span><span id='tyreSpan" + rowCount + "'>Mfr:<select name='oemMfr' id='oemMfr" + rowCount + "' class='form-control' style='width:124px'  ><option value=0>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select>\n\
                                                                                                                                                                                                        TyreNo:<input type='text'  name='oemTyreNo' id='oemTyreNo" + rowCount + "' placeholder='TyreNo'   size='20' class='form-control' value='" + tyreNo + "'  />Depth:<input type='text'  name='oemDepth' id='oemDepth" + rowCount + "' maxlength='13'   size='20' class='form-control' placeholder='TyreDepth' value='" + depth + "' />mm</span></td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell2;
                       $("#oemMfr" + rowCount).val(mfrId);
                       if (addType == 1) {
                           if (oemDetailsId == 2) {
                               $("#batterySpan" + rowCount).show();
                               $("#tyreSpan" + rowCount).hide();
                           }
                       }
                       cell = newrow.insertCell(3);
                       var cell3 = "<td class='text1' height='25' ><input type='hidden' name='oemId' id='oemId" + rowCount + "' value='" + oemId + "'  class='form-control' /></td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell3;
                       document.getElementById('rowCounted').value = rowCount;
                   }

                   function batteryTextHideShow(value, rowCount) {
                       if (value == 1) {
                           $("#batterySpan" + rowCount).hide();
                           $("#tyreSpan" + rowCount).show();
                       } else {
                           $("#batterySpan" + rowCount).show();
                           $("#tyreSpan" + rowCount).hide();
                       }
                   }

                                                                                                                          </script>  
                                <%int addRowCount = 1;%>    
                                <c:if test="${oemList != null}">
                                    <c:forEach items="${oemList}" var="item">
                        <script>
                            addOem('<%=addRowCount%>', '<c:out value="${item.oemDetailsId}"/>', '<c:out value="${item.oemBattery}"/>', '<c:out value="${item.oemTyreNo}"/>', '<c:out value="${item.oemDepth}"/>', '<c:out value="${item.oemMfr}"/>', '<c:out value="${item.oemId}"/>', 1);
                        </script>
                                    </c:forEach>
                                </c:if>                     
                       
                  </table>  
                            

                <br>
                <center>
                            <td><a  class="nexttab" ><input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Next"  text="default text"/>" name="Next" /></a>

                </center>


            </div>
                                <div id="vehicleDepreciationTab"  style="display:none;">
                <table class="table table-info mb30 table-hover" >
                    <tr>
                        <td ><spring:message code="trucks.label.VehicleCost"  text="default text"/></td>
                        <td ><input type="text" name="vehicleDepreciationCost" id="vehicleDepreciationCost" class="form-control" style="width:260px;height:40px;" value='<c:out value="${vehicleCost}"/>'  readonly></td>

                        <td ><spring:message code="trucks.label.DepreciationYear"  text="default text"/></td>
                        <td >

                            <select name="depreciationType" id="depreciationType" class="form-control" style="width:260px;height:40px;"   onChange="removeRowDetails();" disabled>
                                <option value="0"><spring:message code="trucks.label.Select"  text="default text"/></option>
                                <option value="1">1<spring:message code="trucks.label.Year"  text="default text"/></option>
                                <option value="2">2<spring:message code="trucks.label.Year"  text="default text"/></option>
                                <option value="3">3<spring:message code="trucks.label.Year"  text="default text"/></option>
                                <option value="4">4<spring:message code="trucks.label.Year"  text="default text"/></option>
                                <option value="5">5<spring:message code="trucks.label.Year"  text="default text"/></option>
                            </select>
                        </td>
                    <script>
                        document.getElementById("depreciationType").value = '<c:out value="${depreciationSize}"/>';
                    </script>
                    </tr>

                </table>
                <table class="table table-info" >
                     <thead>
                        <th  align="center" height="30" ><spring:message code="trucks.label.SNo"  text="default text"/></th>
                        <th   align="center" height="30"><spring:message code="trucks.label.Year"  text="default text"/></th>
                        <th  height="30" ><spring:message code="trucks.label.Depreciation"  text="default text"/>(%)</th>
                        <th  height="30" ><spring:message code="trucks.label.AnnualDepreciationCost"  text="default text"/></th>
                        <th  height="30" ><spring:message code="trucks.label.MonthlyDepreciationCost"  text="default text"/></th>
                        </thead>
                    
                                <%int rcount = 1;%>
                                <c:if test="${depreciationList != null}">
                                    <c:forEach items="${depreciationList}" var="dep">
                            <tr>
                                <td  height='25' ><input type='hidden' name='serNO' id='serNO<%=rcount%>' value='<%=rcount%>'  class='form-control' /><%=rcount%></td>
                                <td  height='30'>
                                    <input type='hidden' name='vehicleYear' id='vehicleYear<%=rcount%>' value='<%=rcount%>'  class='form-control' />
                                    <label name='vehicleYears' id='vehicleYears<%=rcount%>'><%=rcount%>-<spring:message code="trucks.label.Year"  text="default text"/></label>
                                </td>
                                <td height='30' >
                                    <input type='text' value='<c:out value="${dep.depreciation}"/>' name='depreciation' id='depreciation<%=rcount%>' maxlength='13'   size='20' class='form-control'  onchange="vehicleCostCalculation('<%=rcount%>');" readonly=/>
                                </td>
                                <td  height='30'><input type='hidden' value='<c:out value="${dep.yearCost}"/>' name='yearCost' id='yearCost<%=rcount%>' maxlength='13'   size='20' class='form-control'    /><span id='yearCostSpan<%=rcount%>' ><c:out value="${dep.yearCost}"/></span></td>
                                <td height='30' ><input type='hidden' value='<c:out value="${dep.perMonth}"/>' name='perMonth' id='perMonth<%=rcount%>' maxlength='13'   size='20' class='form-control' onchange='perMonthCalculation(<%=rcount%>);'  /><span id='perMonthSpan<%=rcount%>'><c:out value="${dep.perMonth}"/></span></td>
                            </tr>
                                        <%rcount++;%>
                                    </c:forEach>
                                </c:if>


                </table>
                <br>
                <br>
                <center>
                    <input type="button" class="btn btn-success" name="Save" id="depreciationSave" value="<spring:message code="trucks.label.Save"  text="default text"/>" onclick="saveVehicleDepreciation(this.name);" style="display: none"/>&nbsp;
                    <a  class="nexttab" ><input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Next"  text="default text"/>" name="Next" /></a>
                </center>
                <script type="text/javascript">
                    function saveVehicleDepreciation() {
                        var depreciationType = document.getElementById("depreciationType").value;
                        var vehicleYear = document.getElementsByName("vehicleYear");
                        var depreciation = document.getElementsByName("depreciation");
                        var yearCost = document.getElementsByName("yearCost");
                        var perMonth = document.getElementsByName("perMonth");
                        var status = 0;
                        for (var i = 1; i <= depreciationType; i++) {
                            if ($("#depreciation" + i).val() == '') {
                                var vehicleYear = $("#vehicleYear" + i).val();
                                alert("Please enter the depreciation for Year " + vehicleYear);
                                $("#depreciation" + i).focus();
                                return;
                            } else {
                                if (i == 1) {
                                    vehicleYear = $("#vehicleYear" + i).val();
                                    depreciation = $("#depreciation" + i).val();
                                    yearCost = $("#yearCost" + i).val();
                                    perMonth = $("#perMonth" + i).val();
                                } else {
                                    vehicleYear = vehicleYear + "~" + $("#vehicleYear" + i).val();
                                    depreciation = depreciation + "~" + $("#depreciation" + i).val();
                                    yearCost = yearCost + "~" + $("#yearCost" + i).val();
                                    perMonth = perMonth + "~" + $("#perMonth" + i).val();
                                }
                                status = 1;
                            }
                            //alert("i == " + i);
                        }
                        if (status == 1) {
                            var responseStatus = 0;
                            var vehicleId = $("#vehicleId").val();
                            var url = '';
                            url = './saveVehicleDepreciation.do';
                            $.ajax({
                                url: url,
                                data: {vehicleYear: vehicleYear, depreciation: depreciation, yearCost: yearCost, perMonth: perMonth, vehicleId: vehicleId
                                },
                                type: "GET",
                                success: function(response) {
                                    responseStatus = response.toString().trim();
                                    if (responseStatus == 0) {
                                        $("#StatusMsg").text("Vehicle Premit added failed ");
                                    } else {
                                        $("#StatusMsg").text("Vehicle Permit added sucessfully ");
                                    }

                                },
                                error: function(xhr, status, error) {
                                }
                            });
                        }

                    }
                    function removeRowDetails() {
                        var tab = document.getElementById("vehicleCostTable");
                        //find current no of rows
                        var rowCountLength = document.getElementById('vehicleCostTable').rows.length;
                        rowCountLength = parseInt(rowCountLength);
                        for (var s = rowCountLength; s > 1; s--) {
                            $('#vehicleCostTable tr:last-child').remove();
                        }
                        vehicleCostAddRow();
                    }

                    function vehicleCostAddRow() {
                        var rowCount = "";
                        var style = "text2";
                        var axleCount = document.getElementById('depreciationType').value;
                        for (var i = 0; i < parseInt(axleCount); i++) {

                            var tab = document.getElementById("vehicleCostTable");
                            var rowCount = document.getElementById("vehicleCostTable").rows.length;
                            var newrow = tab.insertRow(rowCount);
                            newrow.id = 'rowId' + rowCount;

                            var cell = newrow.insertCell(0);
                            var cell1 = "<td  height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell1;

                            cell = newrow.insertCell(1);
                            var cell2 = "<td  height='30'><input type='hidden' name='vehicleYear' id='vehicleYear" + rowCount + "' value='" + rowCount + "'  class='form-control' /><label name='vehicleYears' id='vehicleYears" + rowCount + "'>" + (rowCount) + "-Year</label></td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(2);
                            var cell3 = "";
                            if (parseInt(axleCount - 2) == parseInt(i)) {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");lastRowPercent(" + rowCount + ")' /></td>";
                            } else if (parseInt(axleCount - 1) == parseInt(i)) {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");' readonly /></td>";
                            } else {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");' /></td>";
                            }
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell3;

                            cell = newrow.insertCell(3);
                            var cell4 = "<td  height='30'><input type='hidden' value = '' name='yearCost' id='yearCost" + rowCount + "' maxlength='13'   size='20' class='form-control'    /><span id='yearCostSpan" + rowCount + "'>0.00</span></td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell4;

                            cell = newrow.insertCell(4);
                            var cell5 = "";
                            if (parseInt(axleCount - 1) == parseInt(i)) {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            } else if (parseInt(axleCount) == parseInt(i) + 1) {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            } else {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control' onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            }
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell5;

                        }

                    }


                    function lastRowPercent(rowCount) {
                        var depreciation = document.getElementsByName("depreciation");
                        var yearCost = document.getElementsByName("yearCost");
                        var perMonth = document.getElementsByName("perMonth");
                        var depreciationLength = parseInt(depreciation.length - 1);
                        var percentSum = 0;
                        var row = parseInt(rowCount + 1);
                        for (var i = 0; i < depreciationLength; i++) {
                            percentSum += parseInt(depreciation[i].value);
                        }
                        var balancePercent = parseInt(100 - percentSum);
                        if (balancePercent <= 0) {
                            alert("Depreciation planning is incorrect please plan again");
                            for (var i = 1; i <= depreciationLength; i++) {
                                $("#depreciation" + i).val('');
                                $("#yearCost" + i).val('');
                                $("#yearCostSpan" + i).text('0.00');
                                $("#perMonth" + i).val('');
                                $("#perMonthSpan" + i).text('0.00');
                            }
                            $("#depreciation" + row).val('');
                            $("#yearCost" + row).val('');
                            $("#yearCostSpan" + row).text('0.00');
                            $("#perMonth" + row).val('');
                            $("#perMonthSpan" + row).text('0.00');
                        } else {
                            var row = parseInt(rowCount + 1);
                            $("#depreciation" + row).val(balancePercent);
                            vehicleCostCalculation(row);
                            $("#depreciation" + 1).focus();
                        }
                    }
                    function vehicleCostCalculation(rowCount) {
                        for (var i = 0; i < parseInt(rowCount); i++) {
                            var depCost = document.getElementById('depreciation' + rowCount).value;
                            var vehicleCost = document.getElementById('vehicleDepreciationCost').value;
                            var depCostPercent = parseInt(depCost) / 100;
                            var depCostPerYear = parseFloat(depCostPercent * parseInt(vehicleCost)).toFixed(2);
                            document.getElementById('yearCost' + rowCount).value = depCostPerYear
                            $('#yearCostSpan' + rowCount).text(depCostPerYear);
                            var perMothCost = parseFloat(depCostPerYear / 12);
                            document.getElementById('perMonth' + rowCount).value = perMothCost.toFixed(2);
                            $('#perMonthSpan' + rowCount).text(perMothCost.toFixed(2));
                        }
                    }
                    function showPdf(val) {
                        window.open('/throttle/displayVehicleLogoBlobData.do?uploadId=' + val, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                    }


                                                                            </script>
            </div>
                    <div id="vehicleFileAttachments" style="display:none">
                            <%int img = 0;%>

                            <c:if test="${vehicleUploadsDetails != null}">
                                                                                                <table class="table table-info"   style="width: 950px;" align="center">
                                                                                                     <thead>
                         <th><spring:message code="trucks.label.SNo"  text="default text"/></th>
                         <!--<th>&emsp;&emsp;&emsp;<spring:message code="trucks.label.File"  text="default text"/></th>-->
                         <th>&emsp;&emsp;&emsp;&emsp;<spring:message code="trucks.label.FileName"  text="default text"/></th>
                         <th>&nbsp;<spring:message code="trucks.label.Remarks"  text="default text"/></th>
                         <th>&nbsp;<spring:message code="trucks.label.Action"  text="Action"/></th>
                </thead>
                                    <c:forEach items="${vehicleUploadsDetails}" var="cust">
                                                                                                        <tr>
                                                                                                            <td>&nbsp;<%=img+1%></td>
<!--                                                                                                            <td style="width: 100px;"> 
                                                                                                                <img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>"  style="width: 90px;height: 40px" data-toggle="modal" data-target="#myModal<%=img%>" title="<c:out value ="${cust.remarks}"/>"/>
                                                                                                            </td>-->
                                                                                                        <td>&emsp;&emsp;<c:out value ="${cust.fileName}"/></td>
                                                                                                        <td><c:out value ="${cust.remarks}"/></td>
                                                                                                        <td><a href="#" onclick="showPdf(<c:out value ="${cust.uploadId}"/>);">
	                                                                                                                    <img src="images/download.png" title="click here to download" style="height:30px;width:30px;margin-left:-01px;">
	                     
                                                                                                            </a></td>
                                                                                                        </tr>
                                                                                                        
                                                                                                                  <div class="modal fade" id="myModal<%=img%>" role="dialog" style="width: 100%;height: 100%">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()">&<spring:message code="trucks.label.times"  text="default text"/>;</button>
                            <h4 class="modal-title"><c:out value ="${cust.fileName}"/></h4>
                        </div>
                        <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 80%">
  <img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>" style="width: 95%;height: 75%" class="img-responsive" title="<c:out value ="${cust.remarks}"/>">
                        </div>
                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>
                                                                                                        <input  type="hidden" id="uploadId" name="uploadId" value="<c:out value ="${cust.uploadId}"/>"/>
                                        <%img++;%>
                                    </c:forEach>
                               <script>
                                   function resetTheSlabDetails() {
                                       $('#slabRateListSet').html('');
                                   }

                                     </script>                                                                  </table>
                                </c:if>
                

            </div>
                                      
            
                                      

                                      
        </div>

                    <script>

                        $(".nexttab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                        $(".pretab").click(function() {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected - 1);
                        });


                        function vehicleValidation() {//pavi
                            var owner = document.getElementById("ownerShips").value;
                            document.getElementById("asset").value = owner;
                            if (owner == 1 || owner == 5) {
                                if (document.addVehicle.regNo.value == '') {
                                    alert('Please Enter Vehicle Registration Number');
                                    document.addVehicle.regNo.focus();
                                    return false;
                                } else if (document.addVehicle.regNoCheck.value == 'exists') {
                                    alert('Vehicle RegNo already Exists');
                                    document.addVehicle.regNoCheck.focus();
                                    return false;
                                } else if (document.addVehicle.mfrId.value == 0) {
                                    alert('Please select Manufacturer');
                                    document.addVehicle.mfrId.focus();
                                    return false;
                                } else if (document.addVehicle.typeId.value == 0) {
                                    alert('Please select Vehicle Type');
                                    document.addVehicle.typeId.focus();
                                    return false;
                                } else if (document.addVehicle.modelId.value == 0) {
                                    alert('Please select Vehicle Model');
                                    document.addVehicle.modelId.focus();
                                    return false;
                                } else if (document.addVehicle.dateOfSale.value == '') {
                                    alert("Please select Registration Date");
                                    document.addVehicle.dateOfSale.focus();
                                    return false;
                                } else if (document.addVehicle.engineNo.value == '') {
                                    alert("Please enter Engine Number");
                                    document.addVehicle.engineNo.focus();
                                    return false;
                                } else if (document.addVehicle.chassisNo.value == '') {
                                    alert("Please enter Chassis Number");
                                    document.addVehicle.chassisNo.focus();
                                    return false;
                                } else if (document.addVehicle.seatCapacity.value == '' || document.addVehicle.seatCapacity.value == 0) {
                                    alert("Please enter Vehicle Tonnage");
                                    document.addVehicle.seatCapacity.focus();
                                    return false;
                                } else if (document.addVehicle.warrantyDate.value == '') {
                                    alert("Please select Warranty Date");
                                    document.addVehicle.warrantyDate.focus();
                                    return false;
                                } else if (document.addVehicle.war_period.value == '' || document.addVehicle.war_period.value == 0) {
                                    alert("Please select Warranty Period");
                                    document.addVehicle.war_period.focus();
                                    return false;
                                } else if (document.addVehicle.vehicleCost.value == '') {
                                    alert("Please select Vehicle Cost");
                                    document.addVehicle.vehicleCost.focus();
                                    return false;
                                } else if (document.addVehicle.vehicleDepreciation.value == '') {
                                    alert("Please select Vehicle Depreciation");
                                    document.addVehicle.vehicleDepreciation.focus();
                                    return false;
                                    //                }else if(document.addVehicle.classId == 0){
                                    //                    alert("Please select Vehicle Class");
                                    //                    document.addVehicle.classId.focus();
                                    //                    return false;
                                } else if (document.addVehicle.kmReading.value == '') {
                                    alert("Please select Km Reading");
                                    document.addVehicle.kmReading.focus();
                                    return false;
                                } else if (document.addVehicle.dailyKm.value == '') {
                                    alert("Please select Daily Km Reading");
                                    document.addVehicle.dailyKm.focus();
                                    return false;
                                } else if (document.addVehicle.dailyHm.value == '') {
                                    alert('Please select Daily Hm reading');
                                    document.addVehicle.dailyHm.focus();
                                    return false;
                                } else if (document.addVehicle.vehicleColor.value == '') {
                                    alert('Please fill vehicle color');
                                    document.addVehicle.vehicleColor.focus();
                                    return false;
                                } else {
                                    saveVehicleDetails();
                                    return true;

                                }
                            } else {
                                if (document.addVehicle.regNo.value == '') {
                                    alert('Please Enter Vehicle Registration Number');
                                    document.addVehicle.regNo.focus();
                                    return false;
                                } else if (document.addVehicle.regNoCheck.value == 'exists') {
                                    alert('Vehicle RegNo already Exists');
                                    document.addVehicle.regNoCheck.focus();
                                    return false;
                                } else {
                                    saveVehicleDetails();
                                    return true;

                                }
                            }

                        }
                        function saveVehicleDetails() {
                            $("#vehicleDetailSaveButton").hide();
                            var usageId = document.addVehicle.usageId.value;
                            var warPeriod = document.addVehicle.war_period.value;
                            var mfrId = document.addVehicle.mfrId.value;
                            var classId = document.addVehicle.classId.value;
                            var modelId = document.addVehicle.modelId.value;
                            var dateOfSale = document.addVehicle.dateOfSale.value;
                            var regNo = document.addVehicle.regNo.value;
                            var description = document.addVehicle.description.value;
                            var engineNo = document.addVehicle.engineNo.value;
                            var chassisNo = document.addVehicle.chassisNo.value;
                            var opId = document.addVehicle.opId.value;
                            var seatCapacity = document.addVehicle.seatCapacity.value;
                            var kmReading = document.addVehicle.kmReading.value;
                            var vehicleCost = document.addVehicle.vehicleCost.value;
                            var vehicleColor = document.addVehicle.vehicleColor.value;
                            var groupId = document.addVehicle.groupId.value;
                            var gpsSystem = document.addVehicle.gpsSystem.value;
                            var warrantyDate = document.addVehicle.warrantyDate.value;
                            var asset = document.addVehicle.asset.value;
                            var axles = document.addVehicle.axles.value;
                            var vehicleDepreciation = document.addVehicle.vehicleDepreciation.value;
                            var dailyKm = document.addVehicle.dailyKm.value;
                            var dailyHm = document.addVehicle.dailyHm.value;
                            var gpsSystemId = document.addVehicle.gpsSystemId.value;
                            var ownerShips = document.addVehicle.ownerShips.value;
                            var typeId = document.addVehicle.typeId.value;
                            var vendorId = document.addVehicle.vendorId.value;
                            var axleTypeId = document.addVehicle.axleTypeId.value;
                            var vehicleId = document.addVehicle.vehicleId.value;
                            var activeInd = document.addVehicle.activeInd.value;
                            var insertStatus = 0;
                            if (vendorId == '') {
                                vendorId = 0;
                            }
                            //                alert("typeId == "+typeId);
                            var vehicleId = $("#vehicleId").val();
                            if (vehicleId == '') {
                                vehicleId = 0;
                            }
                            var statusName = "";
                            var url = "";
                            if (vehicleId == 0) {
//                            alert("added")
                                url = "./saveVehicleDetails.do";
                                statusName = "added";
                            } else {
//                            alert("updated")
                                url = "./updateVehicleDetails.do";
                                statusName = "updated";
                            }
                            $.ajax({
                                url: url,
                                data: {usageId: usageId,
                                    war_period: warPeriod, mfrId: mfrId, classId: classId, modelId: modelId,
                                    dateOfSale: dateOfSale, regNo: regNo, description: description, regNo: regNo,
                                            engineNo: engineNo, chassisNo: chassisNo, opId: opId, seatCapacity: seatCapacity,
                                    kmReading: kmReading, vehicleColor: vehicleColor, groupId: groupId,
                                    gpsSystem: gpsSystem, warrantyDate: warrantyDate, asset: asset, axles: axles, vehicleCost: vehicleCost,
                                    vehicleDepreciation: vehicleDepreciation, dailyKm: dailyKm, dailyHm: dailyHm,
                                    gpsSystemId: gpsSystemId, ownerShips: ownerShips, typeId: typeId, vendorId: vendorId,
                                    axleTypeId: axleTypeId, vehicleId: vehicleId, activeInd: activeInd
                                },
                                type: "GET",
                                success: function(response) {
                                    vehicleId = response.toString().trim();
                                    if (vehicleId == 0) {
                                        insertStatus = 0;
                                        $("#Status").text("Vehicle " + regNo + " " + statusName + " failed ");
                                    } else {
                                        insertStatus = vehicleId;
                                        $("#Status").text("Vehicle " + regNo + " " + statusName + " sucessfully ");
                                        $("#vehicleId").val(vehicleId);
                                        $("#tyreSave").show();
                                        $("#depreciationSave").show();
                                        $("#oEmNext").show();
                                        $("#insuranceNext").show();
                                        $("#roadTaxNext").show();
                                        $("#fcNext").show();
                                        $("#permitNext").show();
                                    }
                                },
                                error: function(xhr, status, error) {
                                }
                            });
                            return insertStatus;
                        }

                        function vehicleValidationold() {//pavi

                            var ownerShips = document.getElementById("ownerShips").value;
                            var owner = document.getElementById("asset").value;
                            document.getElementById("asset").value = owner;
                            if (document.addVehicle.regNo.value == '') {
                                alert('Please Enter Vehicle Registration Number');
                                document.addVehicle.regNo.focus();
                                return false;
                            } else if (document.addVehicle.regNoCheck.value == 'exists') {
                                alert('Vehicle RegNo already Exists');
                                document.addVehicle.regNoCheck.focus();
                                return false;
                            } else if (document.addVehicle.mfrId.value == 0) {
                                alert('Please select Manufacturer');
                                document.addVehicle.mfrId.focus();
                                return false;
                            } else if (document.addVehicle.typeId.value == 0) {
                                alert('Please select Vehicle Type');
                                document.addVehicle.typeId.focus();
                                return false;
                            } else if (document.addVehicle.modelId.value == 0) {
                                alert('Please select Vehicle Model');
                                document.addVehicle.modelId.focus();
                                return false;
                            } else if (document.addVehicle.dateOfSale.value == '') {
                                alert("Please select Registration Date");
                                document.addVehicle.dateOfSale.focus();
                                return false;
                            } else if (document.addVehicle.engineNo.value == '') {
                                alert("Please enter Engine Number");
                                document.addVehicle.engineNo.focus();
                                return false;
                            } else if (document.addVehicle.chassisNo.value == '') {
                                alert("Please enter Chassis Number");
                                document.addVehicle.chassisNo.focus();
                                return false;
                            } else if (document.addVehicle.seatCapacity.value == '' || document.addVehicle.seatCapacity.value == 0) {
                                alert("Please enter Vehicle Tonnage");
                                document.addVehicle.seatCapacity.focus();
                                return false;
                            } else if (document.addVehicle.warrantyDate.value == '') {
                                alert("Please select Warranty Date");
                                document.addVehicle.warrantyDate.focus();
                                return false;
                            } else if (document.addVehicle.war_period.value == '' || document.addVehicle.war_period.value == 0) {
                                alert("Please select Warranty Period");
                                document.addVehicle.war_period.focus();
                                return false;
                            } else if (document.addVehicle.vehicleCost.value == '') {
                                alert("Please select Vehicle Cost");
                                document.addVehicle.vehicleCost.focus();
                                return false;
                            } else if (document.addVehicle.vehicleDepreciation.value == '') {
                                alert("Please select Vehicle Depreciation");
                                document.addVehicle.vehicleDepreciation.focus();
                                return false;
                                //                }else if(document.addVehicle.classId == 0){
                                //                    alert("Please select Vehicle Class");
                                //                    document.addVehicle.classId.focus();
                                //                    return false;
                            } else if (document.addVehicle.kmReading.value == '') {
                                alert("Please select Km Reading");
                                document.addVehicle.kmReading.focus();
                                return false;
                            } else if (document.addVehicle.dailyKm.value == '') {
                                alert("Please select Daily Km Reading");
                                document.addVehicle.dailyKm.focus();
                                return false;
                            } else if (document.addVehicle.dailyHm.value == '') {
                                alert('Please select Daily Hm reading');
                                document.addVehicle.dailyHm.focus();
                                return false;
                            } else if (document.addVehicle.vehicleColor.value == '') {
                                alert('Please fill vehicle color');
                                document.addVehicle.vehicleColor.focus();
                                return false;
                            } else {
                                saveVehicleDetails();
                                return true;

                            }
                        }

                        function saveVehicleDetailsold() {
                            $("#vehicleDetailSaveButton").hide();
                            var usageId = document.addVehicle.usageId.value;
                            var warPeriod = document.addVehicle.war_period.value;
                            var mfrId = document.addVehicle.mfrId.value;
                            var classId = document.addVehicle.classId.value;
                            var modelId = document.addVehicle.modelId.value;
                            var dateOfSale = document.addVehicle.dateOfSale.value;
                            var regNo = document.addVehicle.regNo.value;
                            var description = document.addVehicle.description.value;
                            var engineNo = document.addVehicle.engineNo.value;
                            var chassisNo = document.addVehicle.chassisNo.value;
                            var opId = document.addVehicle.opId.value;
                            var seatCapacity = document.addVehicle.seatCapacity.value;
                            var kmReading = document.addVehicle.kmReading.value;
                            var vehicleCost = document.addVehicle.vehicleCost.value;
                            var vehicleColor = document.addVehicle.vehicleColor.value;
                            var groupId = document.addVehicle.groupId.value;
                            var gpsSystem = document.addVehicle.gpsSystem.value;
                            var warrantyDate = document.addVehicle.warrantyDate.value;
                            var asset = document.addVehicle.asset.value;
                            var axles = document.addVehicle.axles.value;
                            var vehicleDepreciation = document.addVehicle.vehicleDepreciation.value;
                            var dailyKm = document.addVehicle.dailyKm.value;
                            var dailyHm = document.addVehicle.dailyHm.value;
                            var gpsSystemId = document.addVehicle.gpsSystemId.value;
                            var ownerShips = document.addVehicle.ownerShips.value;
                            var typeId = document.addVehicle.typeId.value;
                            var vendorId = document.addVehicle.vendorId.value;
                            var axleTypeId = document.addVehicle.axleTypeId.value;
                            var vehicleId = document.addVehicle.vehicleId.value;
                            var activeInd = document.addVehicle.activeInd.value;
                            var insertStatus = 0;
                            if (vendorId == '') {
                                vendorId = 0;
                            }
                            //                alert("typeId == "+typeId);
                            var vehicleId = $("#vehicleId").val();
                            if (vehicleId == '') {
                                vehicleId = 0;
                            }
                            var statusName = "";
                            var url = "";
                            if (vehicleId == 0) {
                                url = "./saveVehicleDetails.do";
                                statusName = "added";
                            } else {
                                url = "./updateVehicleDetails.do";
                                statusName = "updated";
                            }
                            $.ajax({
                                url: url,
                                data: {usageId: usageId,
                                    war_period: warPeriod, mfrId: mfrId, classId: classId, modelId: modelId,
                                    dateOfSale: dateOfSale, regNo: regNo, description: description, regNo: regNo,
                                            engineNo: engineNo, chassisNo: chassisNo, opId: opId, seatCapacity: seatCapacity,
                                    kmReading: kmReading, vehicleColor: vehicleColor, groupId: groupId,
                                    gpsSystem: gpsSystem, warrantyDate: warrantyDate, asset: asset, axles: axles, vehicleCost: vehicleCost,
                                    vehicleDepreciation: vehicleDepreciation, dailyKm: dailyKm, dailyHm: dailyHm,
                                    gpsSystemId: gpsSystemId, ownerShips: ownerShips, typeId: typeId, vendorId: vendorId,
                                    axleTypeId: axleTypeId, vehicleId: vehicleId, activeInd: activeInd
                                },
                                type: "GET",
                                success: function(response) {
                                    vehicleId = response.toString().trim();
                                    if (vehicleId == 0) {
                                        insertStatus = 0;
                                        $("#Status").text("Vehicle " + regNo + " " + statusName + " failed ");
                                    } else {
                                        insertStatus = vehicleId;
                                        $("#Status").text("Vehicle " + regNo + " " + statusName + " sucessfully ");
                                        $("#vehicleId").val(vehicleId);
                                        $("#tyreSave").show();
                                        $("#depreciationSave").show();
                                        $("#oEmNext").show();
                                        $("#insuranceNext").show();
                                        $("#roadTaxNext").show();
                                        $("#fcNext").show();
                                        $("#permitNext").show();
                                    }
                                },
                                error: function(xhr, status, error) {
                                }
                            });
                            return insertStatus;
                        }

                        function saveVehicleTyre() {
                            var itemIds = document.getElementsByName("itemIds");
                            var positionIds = document.getElementsByName("typeId");
                            var tyreIds = document.getElementsByName("tyreIds");
                            var tyreNos = document.getElementsByName("tyreNos");
                            var tyreExists = document.getElementsByName("tyreExists");
                            //                var tyreDate = document.getElementsByName("tyreDate");
                            //                var description = document.getElementById("description").value;
                            var tyreId = "";
                            var item = "";
                            var pos = "";
                            var tyreNo = "";
                            var date = "";
                            var vehicleId = "";
                            for (var i = 0; i < itemIds.length; i++) {
                                //                    if(itemIds[i+1].value == 0){
                                //                        alert("Please select item for row "+i)
                                //                        document.getElementById("itemIds"+i).focus();
                                //                        return;
                                //                    }else if(positionIds[i+1].value == 0){
                                //                        alert("Please select position for row "+i)
                                //                        document.getElementById("positionIds"+i).focus();
                                //                        return;
                                //                    }else if(tyreIds[i+1].value == 0){
                                //                        alert("Please enter tyre no for row "+i)
                                //                        document.getElementById("tyreIds"+i).focus();
                                //                        return;
                                //                    }else if(tyreDate[i+1].value == 0){
                                //                        alert("Please select tyre date for row "+i)
                                //                        document.getElementById("tyreDate"+i).focus();
                                //                        return;
                                //                    }else{
                                tyreId = document.getElementById("tyreIds" + i).value;
                                item = document.getElementById("itemIds" + i).value;
                                pos = document.getElementById("positionIds" + i).value;
                                tyreNo = document.getElementById("tyreNos" + i).value;
                                date = document.getElementById("tyreDate" + i).value;
                                vehicleId = document.getElementById("vehicleId").value;
                                var urlInfo = "";
                                if (tyreId == 0) {
                                    urlInfo = "./saveVehicleTyre.do";
                                } else {
                                    urlInfo = "./updateVehicleTyre.do";
                                }
                                $.ajax({
                                    url: urlInfo,
                                    data: {itemId: item, pos: pos,
                                        tyreId: tyreId,
                                        tyreNo: tyreNo,
                                        date: date, vehicleId: vehicleId, description: description
                                    },
                                    type: "GET",
                                    success: function(response) {
                                        $("#tyreId" + i).val(response.toString().trim());
                                    },
                                    error: function(xhr, status, error) {
                                    }
                                });
                                //                    }
                            }


                        }

                                                                                                                                </script>

        <script type="text/javascript">


            function advanceFuelTabHide() {

                alert(<%=request.getAttribute("vehicle_id")%>);
                if ('<%=request.getAttribute("vehicle_id")%>' == 'null') {//Mk and topay

                    alert("hifguhfgdi")
                    $('#vehicleInsurane').hide();
                } else if ('<%=request.getAttribute("vehicle_id")%>' != 'null') {


                    $('#vehicleInsurane').show();
                }

            }
                                                                    </script>
                    <c:if test="${vehicleId > '0' && editStatus == '1'}">
        <script>
            $("#vehicleDetailUpdateButton").show();
            $("#vehicleDetailSaveButton").hide();
            $("#tyreSave").show();
            $("#depreciationSave").hide();
            $("#oEmNext").show();
            $("#insuranceNext").show();
            $("#roadTaxNext").show();
            $("#fcNext").show();
            $("#permitNext").show();
                                                                                                                                                                                                        </script>

                    </c:if>       
                    <c:if test="${vehicleId > '0' && editStatus == '0'}">
        <script>
            $("#vehicleDetailUpdateButton").hide();
            $("#vehicleDetailSaveButton").hide();
            $("#tyreSave").hide();
            $("#depreciationSave").hide();
            $("#oEmNext").hide();
            $("#insuranceNext").hide();
            $("#roadTaxNext").hide();
            $("#fcNext").hide();
            $("#permitNext").hide();
                                                                                        </script>

                    </c:if>  
    </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>

</div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


