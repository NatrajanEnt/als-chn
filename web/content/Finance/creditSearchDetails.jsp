<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

            $(document).ready(function() {
                $("#showTable").hide();

            });
        </script>
        <script type="text/javascript">
            function submitPage(value) {
                document.manufacturer.action = "/throttle/saveDebitNote.do";
                document.manufacturer.submit();      
            }


        </script>

        
        <!--</script>-->
    <style>
    #index td {
   color:white;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.CreditDetails"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.CreditDetails"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
        
        
        

    <body onload="setValue();
            sorter.size(10);">
        <form name="accountReceivable" action=""  method="post">
            <table  class="table table-info mb30 table-hover" >
                <thead>
                <tr id="index">
                    <th colspan="6" ><spring:message code="finance.label.CreditDetails"  text="default text"/></th></tr>
                </thead>
                            
                                <!--<table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >-->
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="finance.label.CustomerName"  text="default text"/></td>
                                        <td height="30" >
                                            <c:if test="${customerList != null}">
                                                  <select  name="customerId" id="customerId"  class="form-control"  style="width:280px;height:40px;">
                                                      <option value="0">--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                                      <c:forEach items="${customerList}" var="cus">
                                                          <option value="<c:out value="${cus.customerId}"/>"><c:out value="${cus.customerName}"/></option>
                                                      </c:forEach>
                                                  </select>
                                            </c:if>
                                        </td>
                                        <!--<td colspan="2"></td>-->

<!--                                    </tr>
                                    <tr>-->
                                        <td><font color="red">*</font><spring:message code="finance.label.FromDate"  text="default text"/></td>
                                        <td height="30"><input autocomplete="off" name="fromDate" id="fromDate" type="text" class="datepicker" value="" style="width:280px;height:40px;" ></td>
                                        <td><font color="red">*</font><spring:message code="finance.label.ToDate"  text="default text"/></td>
                                        <td height="30"><input autocomplete="off" name="toDate" id="toDate" type="text" class="datepicker" value="" style="width:280px;height:40px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" align="center"><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="finance.label.Search"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;"> </td>
                                    </tr>
                                </table>
                            
                    </td>
                </tr>
            </table>

            <br>
            <br>

            <br>
            <br>
            <br>
            <div id="showTable">
                <table class="table table-info mb30 table-hover" >
                    <thead>
                        <tr height="45" id="index" style="background-color:#5BC0DE;" >
                            <th><h3 align="center"><spring:message code="finance.label.Sno"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.CustomerName"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.CreditNoteNo"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.Date"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.CreditAmount"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.Reason"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.Delete"  text="default text"/></h3></th>
                            <th><h3 align="center"><spring:message code="finance.label.Modify"  text="default text"/></h3></th>
                        </tr>
                    </thead>
                    <tbody>


                        <tr height="30" id="index" style="background-color:#5BC0DE;">

                            <td align="center">1</td>
                            <td align="left">SPRK AGENCIES</td>
                            <td align="left">DBNV0123</td>
                            <td align="left">01-02-2014</td>
                            <td align="left">30000</td>
                            <td align="left">Ok</td>
                            <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                            <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>


                        </tr>
                        <tr height="30" id="index" style="background-color:#5BC0DE;">

                            <td align="center">2</td>
                            <td align="left">K P Agencies</td>
                            <td align="left">DBNV012378</td>
                            <td align="left">07-02-2014</td>
                            <td align="left">40000</td>
                            <td align="left">Ok</td>
                            <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                            <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>


                        </tr>
                        <tr height="30" id="index" style="background-color:#5BC0DE;">

                            <td align="center">3</td>
                            <td align="left">MALNI TRADERS</td>
                            <td align="left">DBNV0124</td>
                            <td align="left">07-02-2014</td>
                            <td align="left">25000</td>
                            <td align="left">Ok</td>
                            <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                            <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>


                        </tr>
                        <tr height="30" id="index" style="background-color:#5BC0DE;">

                            <td align="center">4</td>
                            <td align="left">Sarada Company</td>
                            <td align="left">DBNV012512</td>
                            <td align="left">07-01-2014</td>
                            <td align="left">15000</td>
                            <td align="left">Ok</td>
                            <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                            <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>


                        </tr>
                        <tr height="30" id="index" style="background-color:#5BC0DE;">

                            <td align="center">5</td>
                            <td align="left">Sri Ramji Traders</td>
                            <td align="left">DBNV012345</td>
                            <td align="left">15-02-2014</td>
                            <td align="left">10000</td>
                            <td align="left">Ok</td>
                            <td align="left"><input type="checkbox" name="edit" value="" id="edit" onclick="modifyExpenseDetails()"/></td>
                            <td align="left"><input type="checkbox" name="edit1" value="" id="edit1" onclick="modifyExpenseDetails()"/></td>


                        </tr>

                    </tbody>
                </table>
                <center>
                    <td><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="finance.label.Print"  text="default text"/>" onclick="submitPage(this.name);"></td>
                    <td><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="finance.label.Search"  text="default text"/>" onclick="submitPage(this.name);"></td>
                </center>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                <div id="controls">
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 0);
                </script>

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>