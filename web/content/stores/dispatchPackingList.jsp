<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>


<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script><!-- External script -->
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 


<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.PackingList"  text="Dispatch Packing List"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="stores.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="stores.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Delivery"  text="Delivery"/></a></li>
            <li class="active"><spring:message code="stores.label.PickList"  text="Dispatch Packing List"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onload="setFocus();">

                <!--                                <body onload="setFocus();getItemNames();">-->

                <form name="searchPart"  method="post"  >

                    <%@ include file="/content/common/message.jsp" %>

                   
                    <!--</div>-->
                    <!--</div>-->

                    <span id="successsMsg" align="center"><font color="green" size="5px;"></font></span>
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr >
                                    <th ><spring:message code="stores.label.S.No"  text="default text"/></th>
                                    <th  ><spring:message code="stores.label.PackingNo"  text="PackingNo"/></th>
                                    <th ><spring:message code="stores.label.DispatchNo"  text="DispatchNo"/></th>
                                    <th ><spring:message code="stores.label.Origin"  text="Origin"/> </th>
                                    <th ><spring:message code="stores.label.Destination"  text="Destination"/> </th>
                                    <th ><spring:message code="stores.label.PickOrder"  text="PickOrder"/> </th>
                                    <th ><spring:message code="stores.label.UOM"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.RoLevel"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Rack"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Sub-Rack"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Action"  text="default text"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr height="30">                    
                                            <td> 1 </td>                                         
                                            <td> Pack/20-21/001 </td>                                         
                                            <td> 1339 </td>                                         
                                            <td> Chennai</td>                                           
                                            <td> Coimbatore</td>                                           
                                            <td> 10.0</td>                                           
                                            <td> Nos</td>                                           
                                            <td> 5</td>                                           
                                            <td> A101</td>                                           
                                            <td> A101A</td>          
                                            <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                    </tr>
                                            
                                    <tr height="30">                    
                                            <td> 2 </td>                                         
                                            <td> Pack/20-21/002 </td>                                         
                                            <td> 1340 </td>   
                                            <td> Chennai</td>                                           
                                            <td> Coimbatore</td>
                                            <td> 10.0</td>                                           
                                            <td> Nos</td>                                           
                                            <td> 2</td>                                           
                                            <td> B101</td>                                           
                                            <td> B101A</td>                                           
                                            <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                    </tr>
                            </tbody>



                        </table>

                    <br>
                     <center>
                            <input type="button" value="<spring:message code="stores.label.DeliveryNote"  text="Generate"/>" class="btn btn-success" onClick="generatePickList()">                
                        </center>
                        <script>
                            function generatePickList(){
                                alert("Delivery Note Generated ...!");
                            }
                        </script>
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
            </body>

        </div>


    </div>
</div>





<%@ include file="../common/NewDesign/settings.jsp" %>


