<%-- 
    Document   : profitAndLossExcel
    Created on : 1 Mar, 2016, 10:41:30 AM
    Author     : pavithra
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.math.BigDecimal"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page import="ets.domain.util.ThrottleConstants" %>
<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "ProfitAndLoss"+curDate+".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
   <%                
                ArrayList level1 = (ArrayList) request.getAttribute("levelMasterList");
                Iterator itr1 = level1.iterator();
                Iterator itr2 = level1.iterator();
                Iterator itr3 = level1.iterator();
                Iterator itr4 = level1.iterator();
                Iterator itr5 = level1.iterator();
                Iterator itr6 = level1.iterator();
                Iterator it1 = level1.iterator();
                FinanceTO lLevel1 = null;
                FinanceTO lLevel2 = null;
                FinanceTO lLevel3 = null;
                FinanceTO lLevel4 = null;
                FinanceTO lLevel5 = null;
                FinanceTO lLevel6 = null;
                FinanceTO levelMaster = null;
                DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                Date date = new Date();
                String fDate = "";
                String tDate = "";
                if (request.getAttribute("fromDate") != null) {
                    fDate = (String) request.getAttribute("fromDate");
                } else {
                    fDate = "01-04-2016";
                }
                if (request.getAttribute("toDate") != null) {
                    tDate = (String) request.getAttribute("toDate");
                } else {
                    tDate = dateFormat.format(date);
                }
                double debitAmount = 0.0;
                double creditAmount = 0.0;
                double netIncome = 0.0;
                double netExpense = 0.0;
                double expense=0.0;
                 double income = 0.0;
                
                //pavi
                FinanceTO details1 = null;
                FinanceTO details2 = null;
                FinanceTO details3 = null;
                FinanceTO details4 = null;
                FinanceTO details5 = null;
                FinanceTO group = null;
                FinanceTO subLevel2 = null;
                FinanceTO subLevel3 = null;
                FinanceTO subLevel4 = null;
                FinanceTO subLevel5 = null;
                FinanceTO subLevel1 = null;
                BigDecimal nettExpense;
                BigDecimal nettIncome;
                BigDecimal pAndL;
            %>
            <center>
                <h2><%=ThrottleConstants.companyName%></h2>
                <h2>Trial Balance</h2>
                <br>
               <table>
                    <tr>
                        <td>From : <%=request.getAttribute("fromDate")%></td>
                        <td></td>
                        <td>To : <%=request.getAttribute("toDate")%></td>
                        <td></td>
                    </tr>
                </table>
                <br>
                
            </center>
            <br>
            <table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <td  align="left" width="60%" class="contenthead" scope="col"><b>INCOME</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>DEBIT</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>CREDIT</b></td>
                </tr>
                <%
                    int cntr = 0;
                    while (itr1.hasNext()) {
                        cntr++;
//                        System.out.println("333");
                        lLevel1 = new FinanceTO();
                        lLevel1 = (FinanceTO) itr1.next();
                        System.out.println("lLevel1: " + lLevel1.getGroupID());
                          if("Income".equalsIgnoreCase(lLevel1.getGroupName())
                                        ){
                %>

                    <%ArrayList groupLedgerList = (ArrayList) request.getAttribute("groupLedgerList");
                            Iterator groupList = groupLedgerList.iterator();
                            if (groupLedgerList != null) {
                                while (groupList.hasNext()) {
                                    group = new FinanceTO();
                                    group = (FinanceTO) groupList.next();
                                    if (lLevel1.getGroupID().equals(group.getGroupCode())) {
                                        debitAmount += group.getDebitAmt();
                                        creditAmount += group.getCreditAmt();
                                         BigDecimal groupDebit = new BigDecimal(group.getDebitAmt());
                                    BigDecimal groupCredit = new BigDecimal(group.getCreditAmt());
                                    groupDebit=groupDebit.setScale(2, BigDecimal.ROUND_DOWN);
                                    groupCredit=groupCredit.setScale(2, BigDecimal.ROUND_DOWN);
                        %>
                        <%}
                        }
                    }%>
                
                <%
                    levelMaster = new FinanceTO();
                    levelMaster.setGroupID(lLevel1.getGroupID());
                    levelMaster.setLevelgroupId(lLevel1.getGroupID());
                    levelMaster.setLevelgroupName(lLevel1.getGroupName());
                    itr2 = lLevel1.getChildList().iterator();
                    while (itr2.hasNext()) {
                        lLevel2 = new FinanceTO();
                        lLevel2 = (FinanceTO) itr2.next();
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel2.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName());
//                                                  
%>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel2.getLevelgroupName()%></b>
                    </td>

                    <%ArrayList subPrimaryLedgerList1 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary1 = subPrimaryLedgerList1.iterator();
                        if (subPrimaryLedgerList1 != null) {

                            while (subPrimary1.hasNext()) {
                                subLevel1 = new FinanceTO();
                                subLevel1 = (FinanceTO) subPrimary1.next();
                                if (lLevel2.getLevelgroupId().equals(subLevel1.getLevelID())) {
                                     BigDecimal levelDebit = new BigDecimal(subLevel1.getDebitAmt());
                                    BigDecimal levelCredit = new BigDecimal(subLevel1.getCreditAmt());
                                    levelDebit=levelDebit.setScale(2, BigDecimal.ROUND_DOWN);
                                    levelCredit=levelCredit.setScale(2, BigDecimal.ROUND_DOWN);
                    %>

                    <td  class="text1"  align="right" style="color: #0090FF; text-align: right; font-weight: bold; font-size: small;"><%=levelDebit%></td>
                    <td  class="text1"  align="right" style="color: #0090FF; text-align: right; font-weight: bold; font-size: small;"><%=levelCredit%></td>
                        <% }
                       }
                   }%>
                </tr>
                <%
                    ArrayList ledgerTransactionList1 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr1 = ledgerTransactionList1.iterator();
                    if (ledgerTransactionList1 != null) {
                        while (detailsItr1.hasNext()) {
                            details1 = new FinanceTO();
                            details1 = (FinanceTO) detailsItr1.next();
                            if (lLevel2.getLevelgroupId().equals(details1.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details1.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details1.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details1.getCreditAmmount()%></td>

                </tr>
                <%
                            }
                        }
                    }
                %>

                <%
                    itr3 = lLevel2.getChildList().iterator();
                    while (itr3.hasNext()) {
                        lLevel3 = new FinanceTO();
                        lLevel3 = (FinanceTO) itr3.next();
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel3.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName());
%>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel3.getLevelgroupName()%></b>
                    </td>
                    <%ArrayList subPrimaryLedgerList2 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary2 = subPrimaryLedgerList2.iterator();
                        if (subPrimaryLedgerList2 != null) {

                            while (subPrimary2.hasNext()) {
                                subLevel2 = new FinanceTO();
                                subLevel2 = (FinanceTO) subPrimary2.next();
                                if (lLevel3.getLevelgroupId().equals(subLevel2.getLevelID())) {
                                      BigDecimal levelDebit1 = new BigDecimal(subLevel2.getDebitAmt());
                                    BigDecimal levelCredit1 = new BigDecimal(subLevel2.getCreditAmt());
                                      levelDebit1=levelDebit1.setScale(2, BigDecimal.ROUND_DOWN);
                                    levelCredit1=levelCredit1.setScale(2, BigDecimal.ROUND_DOWN);
                    %>

                    <td  class="text1"  align="right"><b><%=levelDebit1%></b></td>
                    <td  class="text1"  align="right"><b><%=levelCredit1%></b></td>
                        <% }
                       }
                   }%>

                </tr>
                <%
                    ArrayList ledgerTransactionList2 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr2 = ledgerTransactionList2.iterator();
                    if (ledgerTransactionList2 != null) {
                        while (detailsItr2.hasNext()) {
                            details2 = new FinanceTO();
                            details2 = (FinanceTO) detailsItr2.next();
                            if (lLevel3.getLevelgroupId().equals(details2.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details2.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details2.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details2.getCreditAmmount()%></td>
                </tr>
                <%
                            }
                        }
                    }
                %>
                <%
                    itr4 = lLevel3.getChildList().iterator();
                    while (itr4.hasNext()) {
                        lLevel4 = new FinanceTO();
                        lLevel4 = (FinanceTO) itr4.next();
                        System.out.println("    lLevel4: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName());
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel4.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName());
                %>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel4.getLevelgroupName()%></b>
                    </td>
                    <%ArrayList subPrimaryLedgerList3 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary3 = subPrimaryLedgerList3.iterator();
                        if (subPrimaryLedgerList3 != null) {

                            while (subPrimary3.hasNext()) {
                                subLevel3 = new FinanceTO();
                                subLevel3 = (FinanceTO) subPrimary3.next();
                                if (lLevel4.getLevelgroupId().equals(subLevel3.getLevelID())) {
                                    BigDecimal levelDebit2 = new BigDecimal(subLevel3.getDebitAmt());
                                    BigDecimal levelCredit2 = new BigDecimal(subLevel3.getCreditAmt());
                                     levelDebit2=levelDebit2.setScale(2, BigDecimal.ROUND_DOWN);
                                    levelCredit2=levelCredit2.setScale(2, BigDecimal.ROUND_DOWN);
                    %>

                    <td  class="text1"  align="right"><b><%=levelDebit2%></b></td>
                    <td  class="text1"  align="right"><b><%=levelCredit2%></b></td>
                        <% }
                       }
                   }%>

                </tr>

                <%
                    ArrayList ledgerTransactionList3 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr3 = ledgerTransactionList3.iterator();
                    if (ledgerTransactionList3 != null) {
                        while (detailsItr3.hasNext()) {
                            details3 = new FinanceTO();
                            details3 = (FinanceTO) detailsItr3.next();
                            if (lLevel4.getLevelgroupId().equals(details3.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details3.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details3.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details3.getCreditAmmount()%></td>
                </tr>
                <%
                            }
                        }
                    }
                %>
                <%
                    itr5 = lLevel4.getChildList().iterator();
                    while (itr5.hasNext()) {
                        lLevel5 = new FinanceTO();
                        lLevel5 = (FinanceTO) itr5.next();
//                        System.out.println("     lLevel5: " + lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel5.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                %>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel5.getLevelgroupName()%></b>
                    </td>
                    <%ArrayList subPrimaryLedgerList4 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary4 = subPrimaryLedgerList4.iterator();
                        if (subPrimaryLedgerList4 != null) {

                            while (subPrimary4.hasNext()) {
                                subLevel4 = new FinanceTO();
                                subLevel4 = (FinanceTO) subPrimary4.next();
//                                System.out.println("  pavitestingggggggggggggggggggg " + lLevel5.getLevelgroupId() + "->" + subLevel4.getLevelID());
                                if (lLevel5.getLevelgroupId().equals(subLevel4.getLevelID())) {
//                                    System.out.println("group.getGroupID()" + subLevel4.getLevelID() + "-->" + lLevel5.getLevelgroupId());
                    %>

                    <td  class="text1"  align="right"><b><%=subLevel4.getDebitAmt()%></b></td>
                    <td  class="text1"  align="right"><b><%=subLevel4.getCreditAmt()%></b></td>
                        <% }
                       }
                   }%>
                </tr>
                <%
                    ArrayList ledgerTransactionList4 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr4 = ledgerTransactionList4.iterator();
                    if (ledgerTransactionList4 != null) {

                        while (detailsItr4.hasNext()) {

                            details4 = new FinanceTO();
                            details4 = (FinanceTO) detailsItr4.next();
//                         System.out.println("  pavi ###############" + lLevel5.getLevelgroupId() + "->" + details4.getLevelID());   
                            if (lLevel5.getLevelgroupId().equals(details4.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details4.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details4.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details4.getCreditAmmount()%></td>
                </tr>
                <%
                            }
                        }
                    }
                %>

                <%
                    itr6 = lLevel5.getChildList().iterator();
                    while (itr6.hasNext()) {
                        lLevel6 = new FinanceTO();
                        lLevel6 = (FinanceTO) itr6.next();
//                        System.out.println("     lLevel6: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName() + "-" + lLevel5.getLevelgroupName() + "-" + lLevel6.getLevelgroupName());
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel6.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName() + "->" + lLevel6.getLevelgroupName());
                %>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel6.getLevelgroupName()%></b>
                    </td>
                    <%ArrayList subPrimaryLedgerList5 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary5 = subPrimaryLedgerList5.iterator();
                        if (subPrimaryLedgerList5 != null) {

                            while (subPrimary5.hasNext()) {
                                subLevel5 = new FinanceTO();
                                subLevel5 = (FinanceTO) subPrimary5.next();
//                                System.out.println("  pavitestingggggggggggggggggggg " + lLevel6.getLevelgroupId() + "->" + subLevel5.getLevelID());
                                if (lLevel6.getLevelgroupId().equals(subLevel5.getLevelID())) {
//                                    System.out.println("group.getGroupID()" + subLevel5.getLevelID() + "-->" + lLevel6.getLevelgroupId());
                    %>

                    <td  class="text1"  align="right"><b><%=subLevel5.getDebitAmt()%></b></td>
                    <td  class="text1"  align="right"><b><%=subLevel5.getCreditAmt()%></b></td>
                        <% }
                       }
                   }%>
                </tr>
                <% ArrayList ledgerTransactionList5 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr5 = ledgerTransactionList5.iterator();
                    if (ledgerTransactionList5 != null) {
                        while (detailsItr5.hasNext()) {
                            details5 = new FinanceTO();
                            details5 = (FinanceTO) detailsItr5.next();
//                            System.out.println("  pavi $$$$$$$$$$$$$$$" + lLevel6.getLevelgroupId() + "->" + details5.getLevelID());
                            if (lLevel6.getLevelgroupId().equals(details5.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details5.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details5.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details5.getCreditAmmount()%></td>
                </tr>
                <%
                            }
                        }
                    }
                %>

                <%
                                        }
                                    }
                                }
                            }
                        }
                    }
                    }
                    if (cntr > 0) {
                %>
                <%
                       
                        BigDecimal debitNo = new BigDecimal(debitAmount);
                    BigDecimal creditNo = new BigDecimal(creditAmount);
                    netIncome = debitAmount - creditAmount;
                    String netIncomee = new Double(netIncome).toString();
                     if (netIncomee.contains("-")) {
                                    String temp[] = netIncomee.split("-");
                                     income = Double.parseDouble(temp[1]);
                      }else{
                       income = Double.parseDouble(netIncomee);
                      }
                                   
                                   nettIncome = new BigDecimal(income);
                                   //BigDecimal netIncome = debitNo.subtract(creditNo) ;
                      debitNo=debitNo.setScale(2, BigDecimal.ROUND_DOWN);
                                    creditNo=creditNo.setScale(2, BigDecimal.ROUND_DOWN);
                                    nettIncome=nettIncome.setScale(2, BigDecimal.ROUND_DOWN);
                %>
                <tr>
                    <td  class="text1"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;">Total</b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"><%=debitNo%></b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"><%=creditNo%></b></td>
                </tr>
                <tr>
                    <td  class="text1"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;">Net Income</b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"></b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"><%=nettIncome%></b></td>
                </tr>
                <%
                    }
                %>
            </table>
            <br>
            <table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">
                 <tr height="30">
                    <td  align="left" width="60%" class="contenthead" scope="col"><b>EXPENSE</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>DEBIT</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>CREDIT</b></td>
                </tr>
                <%
//                   
                            level1 = (ArrayList) request.getAttribute("levelMasterList");
                        itr1 = level1.iterator();
                        itr2 = level1.iterator();
                        itr3 = level1.iterator();
                        itr4 = level1.iterator();
                        itr5 = level1.iterator();
                        itr6 = level1.iterator();
                        lLevel1 = null;
                        lLevel2 = null;
                        lLevel3 = null;
                        lLevel4 = null;
                        lLevel5 = null;
                        lLevel6 = null;
                        levelMaster = null;
                           debitAmount = 0;
                 creditAmount = 0;
                          details1 = null;
                 details2 = null;
                 details3 = null;
                 details4 = null;
                 details5 = null;
                 group = null;
                 subLevel2 = null;
                 subLevel3 = null;
                 subLevel4 = null;
                 subLevel5 = null;
                 subLevel1 = null;
                            System.out.println("22222");
                            int cntrr = 0;
                    while (itr1.hasNext()) {
                        cntrr++;
//                        System.out.println("333");
                        lLevel1 = new FinanceTO();
                        lLevel1 = (FinanceTO) itr1.next();
                        System.out.println("lLevel1: " + lLevel1.getGroupID());
                          if("Expense".equalsIgnoreCase(lLevel1.getGroupName())
                                        ){
                %>

                        <%ArrayList groupLedgerList = (ArrayList) request.getAttribute("groupLedgerList");
                            Iterator groupList = groupLedgerList.iterator();
                            if (groupLedgerList != null) {
                                while (groupList.hasNext()) {
                                    group = new FinanceTO();
                                    group = (FinanceTO) groupList.next();
    //                         System.out.println("  pavi grouppppppppppppppppppppppp " + lLevel1.getLevelgroupId() + "->" + group.getLevelID());  
                                    if (lLevel1.getGroupID().equals(group.getGroupCode())) {
//                                        System.out.println("primary group.getGroupID()" + group.getGroupCode() + "-->" + lLevel1.getGroupID());
                                        debitAmount += group.getDebitAmt();
                                        creditAmount += group.getCreditAmt();
                                         BigDecimal groupDebit = new BigDecimal(group.getDebitAmt());
                                    BigDecimal groupCredit = new BigDecimal(group.getCreditAmt());
                                    groupDebit=groupDebit.setScale(2, BigDecimal.ROUND_DOWN);
                                    groupCredit=groupCredit.setScale(2, BigDecimal.ROUND_DOWN);
                                    //String groupDebits = groupDebit.toPlainString();
//                                     String groupCredits = groupCredit.toPlainString();
                        %>
                        <%}
                        }
                    }%>
                <%
                    levelMaster = new FinanceTO();
                    levelMaster.setGroupID(lLevel1.getGroupID());
                    levelMaster.setLevelgroupId(lLevel1.getGroupID());
                    levelMaster.setLevelgroupName(lLevel1.getGroupName());
                    itr2 = lLevel1.getChildList().iterator();
                    while (itr2.hasNext()) {
                        lLevel2 = new FinanceTO();
                        lLevel2 = (FinanceTO) itr2.next();
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel2.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName());
//                                                  
%>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel2.getLevelgroupName()%></b>
                    </td>

                    <%ArrayList subPrimaryLedgerList1 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary1 = subPrimaryLedgerList1.iterator();
                        if (subPrimaryLedgerList1 != null) {

                            while (subPrimary1.hasNext()) {
                                subLevel1 = new FinanceTO();
                                subLevel1 = (FinanceTO) subPrimary1.next();
//                         System.out.println("  pavitestingggggggggggggggggggg " + lLevel2.getLevelgroupId() + "->" + subLevel1.getLevelID());  
                                if (lLevel2.getLevelgroupId().equals(subLevel1.getLevelID())) {
                                     BigDecimal levelDebit = new BigDecimal(subLevel1.getDebitAmt());
                                    BigDecimal levelCredit = new BigDecimal(subLevel1.getCreditAmt());
                                    levelDebit=levelDebit.setScale(2, BigDecimal.ROUND_DOWN);
                                    levelCredit=levelCredit.setScale(2, BigDecimal.ROUND_DOWN);
//                                    System.out.println(" sub group.getGroupID()" + subLevel1.getLevelID() + "-->" + lLevel2.getLevelgroupId());
                    %>

                    <td  class="text1"  align="right" style="color: #0090FF; text-align: right; font-weight: bold; font-size: small;"><%=levelDebit%></td>
                    <td  class="text1"  align="right" style="color: #0090FF; text-align: right; font-weight: bold; font-size: small;"><%=levelCredit%></td>
                        <% }
                       }
                   }%>
                </tr>
                <%
                    ArrayList ledgerTransactionList1 = (ArrayList) request.getAttribute("ledgerTransactionList");
 //                System.out.println("ledgerTransactionList size"+ledgerTransactionList1.size());
                    Iterator detailsItr1 = ledgerTransactionList1.iterator();
                    if (ledgerTransactionList1 != null) {
                        while (detailsItr1.hasNext()) {
                            details1 = new FinanceTO();
                            details1 = (FinanceTO) detailsItr1.next();
 //                         System.out.println("  pavi******************** " + lLevel2.getLevelgroupId() + "->" + details1.getLevelID());  
                            if (lLevel2.getLevelgroupId().equals(details1.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details1.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details1.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details1.getCreditAmmount()%></td>

                </tr>
                <%
                            }
                        }
                    }
                %>

                <%
                    itr3 = lLevel2.getChildList().iterator();
                    while (itr3.hasNext()) {
                        lLevel3 = new FinanceTO();
                        lLevel3 = (FinanceTO) itr3.next();
//                                                        System.out.println("   lLevel3: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName());
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel3.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName());
//                                                        System.out.println("l;l;l;l;l;l;"+lLevel3.getDebitAmount());
//                                                        System.out.println("l;l;l;l;l;l;"+lLevel3.getCreditAmount());
%>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel3.getLevelgroupName()%></b>
                    </td>
                    <%ArrayList subPrimaryLedgerList2 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary2 = subPrimaryLedgerList2.iterator();
                        if (subPrimaryLedgerList2 != null) {

                            while (subPrimary2.hasNext()) {
                                subLevel2 = new FinanceTO();
                                subLevel2 = (FinanceTO) subPrimary2.next();
//                         System.out.println("  pavitestingggggggggggggggggggg " + lLevel3.getLevelgroupId() + "->" + subLevel2.getLevelID());  
                                if (lLevel3.getLevelgroupId().equals(subLevel2.getLevelID())) {
                                      BigDecimal levelDebit1 = new BigDecimal(subLevel2.getDebitAmt());
                                    BigDecimal levelCredit1 = new BigDecimal(subLevel2.getCreditAmt());
                                      levelDebit1=levelDebit1.setScale(2, BigDecimal.ROUND_DOWN);
                                    levelCredit1=levelCredit1.setScale(2, BigDecimal.ROUND_DOWN);
//                                    System.out.println(" sub group.getGroupID()" + subLevel2.getLevelID() + "-->" + lLevel3.getLevelgroupId());
                    %>

                    <td  class="text1"  align="right" style="color: #0090FF; text-align: right; font-weight: bold; font-size: small;"><%=levelDebit1%></td>
                    <td  class="text1"  align="right" style="color: #0090FF; text-align: right; font-weight: bold; font-size: small;"><%=levelCredit1%></td>
                        <% }
                       }
                   }%>

                </tr>
                <%
                    ArrayList ledgerTransactionList2 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr2 = ledgerTransactionList2.iterator();
                    if (ledgerTransactionList2 != null) {
                        while (detailsItr2.hasNext()) {
                            details2 = new FinanceTO();
                            details2 = (FinanceTO) detailsItr2.next();
//                         System.out.println("  pavi+++++++++++++++++++++ " + lLevel3.getLevelgroupId() + "->" + details2.getLevelID()+"+++++++++++++++++"+levelMaster.getLevelgroupId());   
                            if (lLevel3.getLevelgroupId().equals(details2.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details2.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details2.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details2.getCreditAmmount()%></td>
                </tr>
                <%
                            }
                        }
                    }
                %>
                <%
                    itr4 = lLevel3.getChildList().iterator();
                    while (itr4.hasNext()) {
                        lLevel4 = new FinanceTO();
                        lLevel4 = (FinanceTO) itr4.next();
                        System.out.println("    lLevel4: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName());
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel4.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName());
                %>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel4.getLevelgroupName()%></b>
                    </td>
                    <%ArrayList subPrimaryLedgerList3 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary3 = subPrimaryLedgerList3.iterator();
                        if (subPrimaryLedgerList3 != null) {

                            while (subPrimary3.hasNext()) {
                                subLevel3 = new FinanceTO();
                                subLevel3 = (FinanceTO) subPrimary3.next();
//                                System.out.println("  pavitestingggggggggggggggggggg " + lLevel4.getLevelgroupId() + "->" + subLevel3.getLevelID());
                                if (lLevel4.getLevelgroupId().equals(subLevel3.getLevelID())) {
                                    BigDecimal levelDebit2 = new BigDecimal(subLevel3.getDebitAmt());
                                    BigDecimal levelCredit2 = new BigDecimal(subLevel3.getCreditAmt());
                                     levelDebit2=levelDebit2.setScale(2, BigDecimal.ROUND_DOWN);
                                    levelCredit2=levelCredit2.setScale(2, BigDecimal.ROUND_DOWN);
//                                    System.out.println("group.getGroupID()" + subLevel3.getLevelID() + "-->" + lLevel4.getLevelgroupId());
                    %>

                    <td  class="text1"  align="right"><b><%=levelDebit2%></b></td>
                    <td  class="text1"  align="right"><b><%=levelCredit2%></b></td>
                        <% }
                       }
                   }%>

                </tr>

                <%
                    ArrayList ledgerTransactionList3 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr3 = ledgerTransactionList3.iterator();
                    if (ledgerTransactionList3 != null) {
                        while (detailsItr3.hasNext()) {
                            details3 = new FinanceTO();
                            details3 = (FinanceTO) detailsItr3.next();
 //                         System.out.println("  pavi@@@@@@@@@@@@@@@@@ " + lLevel4.getLevelgroupId() + "->" + details3.getLevelID());   
                            if (lLevel4.getLevelgroupId().equals(details3.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details3.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details3.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details3.getCreditAmmount()%></td>
                </tr>
                <%
                            }
                        }
                    }
                %>
                <%
                    itr5 = lLevel4.getChildList().iterator();
                    while (itr5.hasNext()) {
                        lLevel5 = new FinanceTO();
                        lLevel5 = (FinanceTO) itr5.next();
//                        System.out.println("     lLevel5: " + lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel5.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                %>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel5.getLevelgroupName()%></b>
                    </td>
                    <%ArrayList subPrimaryLedgerList4 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary4 = subPrimaryLedgerList4.iterator();
                        if (subPrimaryLedgerList4 != null) {

                            while (subPrimary4.hasNext()) {
                                subLevel4 = new FinanceTO();
                                subLevel4 = (FinanceTO) subPrimary4.next();
//                                System.out.println("  pavitestingggggggggggggggggggg " + lLevel5.getLevelgroupId() + "->" + subLevel4.getLevelID());
                                if (lLevel5.getLevelgroupId().equals(subLevel4.getLevelID())) {
//                                    System.out.println("group.getGroupID()" + subLevel4.getLevelID() + "-->" + lLevel5.getLevelgroupId());
                    %>

                    <td  class="text1"  align="right"><b><%=subLevel4.getDebitAmt()%></b></td>
                    <td  class="text1"  align="right"><b><%=subLevel4.getCreditAmt()%></b></td>
                        <% }
                       }
                   }%>
                </tr>
                <%
                    ArrayList ledgerTransactionList4 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr4 = ledgerTransactionList4.iterator();
                    if (ledgerTransactionList4 != null) {

                        while (detailsItr4.hasNext()) {

                            details4 = new FinanceTO();
                            details4 = (FinanceTO) detailsItr4.next();
//                         System.out.println("  pavi ###############" + lLevel5.getLevelgroupId() + "->" + details4.getLevelID());   
                            if (lLevel5.getLevelgroupId().equals(details4.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details4.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details4.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details4.getCreditAmmount()%></td>
                </tr>
                <%
                            }
                        }
                    }
                %>

                <%
                    itr6 = lLevel5.getChildList().iterator();
                    while (itr6.hasNext()) {
                        lLevel6 = new FinanceTO();
                        lLevel6 = (FinanceTO) itr6.next();
//                        System.out.println("     lLevel6: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName() + "-" + lLevel5.getLevelgroupName() + "-" + lLevel6.getLevelgroupName());
                        levelMaster = new FinanceTO();
                        levelMaster.setGroupID(lLevel1.getGroupID());
                        levelMaster.setLevelgroupId(lLevel6.getLevelgroupId());
                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName() + "->" + lLevel6.getLevelgroupName());
                %>
                <tr height="30">
                    <td  class="text1" >
                        <b><%=lLevel6.getLevelgroupName()%></b>
                    </td>
                    <%ArrayList subPrimaryLedgerList5 = (ArrayList) request.getAttribute("subPrimaryLedgerList");
                        Iterator subPrimary5 = subPrimaryLedgerList5.iterator();
                        if (subPrimaryLedgerList5 != null) {

                            while (subPrimary5.hasNext()) {
                                subLevel5 = new FinanceTO();
                                subLevel5 = (FinanceTO) subPrimary5.next();
//                                System.out.println("  pavitestingggggggggggggggggggg " + lLevel6.getLevelgroupId() + "->" + subLevel5.getLevelID());
                                if (lLevel6.getLevelgroupId().equals(subLevel5.getLevelID())) {
//                                    System.out.println("group.getGroupID()" + subLevel5.getLevelID() + "-->" + lLevel6.getLevelgroupId());
                    %>

                    <td  class="text1"  align="right"><b><%=subLevel5.getDebitAmt()%></b></td>
                    <td  class="text1"  align="right"><b><%=subLevel5.getCreditAmt()%></b></td>
                        <% }
                       }
                   }%>
                </tr>
                <% ArrayList ledgerTransactionList5 = (ArrayList) request.getAttribute("ledgerTransactionList");
                    Iterator detailsItr5 = ledgerTransactionList5.iterator();
                    if (ledgerTransactionList5 != null) {
                        while (detailsItr5.hasNext()) {
                            details5 = new FinanceTO();
                            details5 = (FinanceTO) detailsItr5.next();
//                            System.out.println("  pavi $$$$$$$$$$$$$$$" + lLevel6.getLevelgroupId() + "->" + details5.getLevelID());
                            if (lLevel6.getLevelgroupId().equals(details5.getLevelID())) {
                %>
                <tr>
                    <td class="text1"><%=details5.getLedgerName()%> A/C </td>
                    <td class="text1" align ="right"><%=details5.getDebitAmmount()%></td>
                    <td class="text1" align ="right"><%=details5.getCreditAmmount()%></td>
                </tr>
                <%
                            }
                        }
                    }
                %>

                <%
                                        }
                                    }
                                }
                            }
                        }
                    }
                    }
                    if (cntrr > 0) {
                %>
                <%  
                        
                         BigDecimal debitNo = new BigDecimal(debitAmount);
                    BigDecimal creditNo = new BigDecimal(creditAmount);
                    netExpense = debitAmount - creditAmount;
                    String netExpensee = new Double(netExpense).toString();
                      if (netExpensee.contains("-")) {
                                    String temp[] = netExpensee.split("-");
                                     expense = Double.parseDouble(temp[1]);
                      }else{
                       expense = Double.parseDouble(netExpensee);
                      }
                                   nettExpense = new BigDecimal(expense);
                                   //BigDecimal netIncome = debitNo.subtract(creditNo) ;
                      debitNo=debitNo.setScale(2, BigDecimal.ROUND_DOWN);
                                    creditNo=creditNo.setScale(2, BigDecimal.ROUND_DOWN);
                                    nettExpense=nettExpense.setScale(2, BigDecimal.ROUND_DOWN);
                                    
                        %>
                <tr>
                    <td  class="text1"><b style="color:#0090FF; text-align: center; font-weight: bold; font-size: medium;">Total</b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"><%=debitNo%></b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"><%=creditNo%></b></td>
                </tr>
                 <tr>
                    <td  class="text1"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;">Net Expense</b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"></b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"><%=nettExpense%></b></td>
                </tr>
                <%
                    }
                 pAndL = new BigDecimal(income - expense);
                      pAndL=pAndL.setScale(2, BigDecimal.ROUND_DOWN);
                %>
            </table>
            <br>
            <table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr>
                    <td  class="text1"><b style="color: green; text-align: center; font-weight: bold; font-size: medium;">Profit and Loss</b></td>
                    <td  class="text1"  align="right"><b style="color: #0090FF; text-align: center; font-weight: bold; font-size: medium;"></b></td>
                    <td  class="text1"  align="right"><b style="color: green; text-align: center; font-weight: bold; font-size: medium;"><%= pAndL %></b></td>
                </tr> 
            </table>
            <br>
           

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

