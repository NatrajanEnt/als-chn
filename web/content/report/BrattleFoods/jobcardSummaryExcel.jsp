<%-- 
    Document   : jobSummary
    Created on : Jun 12, 2014, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
        <form name="jobSummary" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "JobcardSummary-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
            
           <c:if test = "${jobcardSummaryDriverIssue != null}" >
                <div style="text-align: center">Driver Issue</div>
                <table style="width: 1100px" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                            <th><h3>Sno</h3></th>
                            <th><h3>Jobcard No</h3></th>
                            <th><h3>Vehicle No</h3></th>
                            <th><h3>Service Type Name</h3></th>
                            <th><h3>Created Date</h3></th>
                            <th><h3>Jobcard Days</h3></th>
                        </tr>
                    </thead>
                    <% int i=1;%>
                    <tbody>
                        <c:forEach items="${jobcardSummaryDriverIssue}" var="job">
                        <tr>
                            <td><%=i%></td>
                            <td><c:out value="${job.jobCardNo}"/></td>
                            <td><c:out value="${job.vehicleNo}"/></td>
                            <td><c:out value="${job.serviceTypeName}"/></td>
                            <td><c:out value="${job.createdDate}"/></td>
                            <td><c:out value="${job.jobCardDays}"/></td>
                        </tr>
                        <%i++;%>
                        </c:forEach>
                    </tbody>

                </table>
            </c:if>
            <br>
            <br>
            <br>
            <c:if test = "${jobcardSummary != null}" >
                <div style="text-align: center">Other Issue</div>
                <table style="width: 1100px" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                            <th><h3>Sno</h3></th>
                            <th><h3>Jobcard No</h3></th>
                            <th><h3>Vehicle No</h3></th>
                            <th><h3>Service Type Name</h3></th>
                            <th><h3>Created Date</h3></th>
                            <th><h3>Jobcard Days</h3></th>
                        </tr>
                    </thead>
                    <% int j=1;%>
                    <tbody>
                        <c:forEach items="${jobcardSummary}" var="job">
                        <tr>
                            <td><%=j%></td>
                            <td><c:out value="${job.jobCardNo}"/></td>
                            <td><c:out value="${job.vehicleNo}"/></td>
                            <td><c:out value="${job.serviceTypeName}"/></td>
                            <td><c:out value="${job.createdDate}"/></td>
                            <td><c:out value="${job.jobCardDays}"/></td>
                        </tr>
                        <%j++;%>
                        </c:forEach>
                    </tbody>

                </table>
            </c:if>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
       
    </body>    
</html>