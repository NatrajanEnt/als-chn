<%-- 
    Document   : viewTBDetailsNew
    Created on : 16 Feb, 2016, 3:02:26 PM
    Author     : pavithra
--%>
<html>
    <head>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %><%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
    </head>


	 <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

    <body onload="submitPage('search')">
        <form name="trialBalance" method="post" >
            <div id="ledgerDataList">
                <br>
                <p>&nbsp;</p>
                <div align="center">
                    <c:if test = "${ledgerTransactionList == null}" >
                        <center>
                            <font color="red">No Records Found</font>
                        </center>
                    </c:if>
                    <c:if test = "${ledgerTransactionList != null}" >
                        <table class="table table-bordered"">
                            <tr id="index">
                                <td style="background-color:#5BC0DE;" width="10%" ><spring:message code="finance.label.Sno"  text="default text"/></td>
                                <td style="background-color:#5BC0DE;" width="10%" ><spring:message code="finance.label.Date"  text="default text"/></td>
                                <td style="background-color:#5BC0DE;" width="70%"><spring:message code="finance.label.Particulars"  text="default text"/></td>                            
                                <td style="background-color:#5BC0DE;" width="10%"><spring:message code="finance.label.Debit"  text="default text"/></td>
                                <td style="background-color:#5BC0DE;" width="10%"><spring:message code="finance.label.Credit"  text="default text"/></td>
                            </tr>
                            <%
                                int cntr = 0;
                            %>
                            <c:set var="creditTotal" value="${0}" />
                            <c:set var="debitTotal" value="${0}" />
                            <c:forEach items="${ledgerTransactionList}" var="txnlist">
                                <c:set var="creditTotal" value="${creditTotal + txnlist.creditValue}"/>
                                <c:set var="debitTotal" value="${debitTotal + txnlist.debitValue}"/>
                                <tr>
                                    <% cntr++;%>
                                    <td  ><%=cntr%></td>
                                    <td  ><c:out value="${txnlist.accountEntryDate}" /></td>
                                    <td  ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C&nbsp;(<c:out value="${txnlist.accountNarration}" /> - <c:out value="${txnlist.tripId}" />) </td>
                                    <td  align ="right"><c:out value="${txnlist.debitValue}" /></td>
                                    <td  align ="right"><c:out value="${txnlist.creditValue}" /></td>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td colspan="3" align="right"   height="30"><b><spring:message code="finance.label.SubTotal"  text="default text"/></b></td>
                                <td   align="right"  height="30">
                                    <b>
                                        <fmt:formatNumber type="number"  maxFractionDigits="2" value="${debitTotal}" />
                                    </b>
                                </td>
                                <td   align="right"  height="30">
                                    <b>
                                        <fmt:formatNumber type="number"  maxFractionDigits="2" value="${creditTotal}" />
                                    </b>
                                </td>
                            </tr>

                        </table>
                    </c:if>
                </div>
                <br>
                <br>
                <div >
                    <table width="150px"  height="20" align="center" border="0" cellpadding="0" cellspacing="0"   >
                        <tr>
                            <%  int pageNo = (Integer) request.getAttribute("pageNo");

                                if (pageNo > 1) {
                            %>
                            <td width="30"><img onclick="submitPage('First')" src="/throttle/images/Previous2.gif" align="middle" border="0" alt="First"></td>                
                            <td width="30"><img onclick="submitPage('Prev')" src="/throttle/images/previous.gif" align="middle" border="0" alt="Start"></td>        

                            <%} else {%>
                            <td width="30"><img src="/throttle/images/Previous2Over.gif" align="middle" border="0" alt="First"></td>        
                            <td width="30"><img src="/throttle/images/previousOver.gif" align="middle" border="0" alt="Start"></td>                

                            <%}%>
                        <input type="hidden" name="pageNo" id="pageNo" value='<%=pageNo%>'>
                        <input type="hidden" name="last" id="last" value='<%=request.getAttribute("totalPages")%>'>
                        <%
                            int totalPages = (Integer) request.getAttribute("totalPages");
                            if (totalPages > 0) {
                        %>
                        <td width="30"  style="border:1px; border-style:solid; border-color:#ffffff;" align="left" >
                            <select  onchange="submitPage(this.name)"     name="GoTo" id="goTO" value="">   
                                <%
                                    for (int i = 0; i < totalPages; i++) {
                                        if (pageNo == (i + 1)) {
                                %>
                                <option selected value='<%=i + 1%>'><%=i + 1%></option>
                                <%
                                } else {
                                %>
                                <option value='<%=i + 1%>'><%=i + 1%></option>
                                <%
                                        }
                                    }
                                %>  
                            </select>

                        </td>
                        <%
                            }
                        %>

                        <%
                            if (pageNo < totalPages) {
                        %> 
                        <td width="30"><img src="/throttle/images/Next.gif"  onclick="submitPage('Next')"  align="middle" border="0" alt="Next"></td>		
                        <td width="30"><img src="/throttle/images/Next2.gif" onclick="submitPage('Last')" align="middle" border="0" alt="Last"></td>
                            <%} else {%>
                        <td width="30"><img src="/throttle/images/NextOver.gif"   align="middle" border="0" alt="Next"></td>		
                        <td width="30"><img src="/throttle/images/Next2Over.gif" align="middle" border="0" alt="Last"></td>
                            <%}%>
                        </tr>
                        <input type="hidden" name="button" value="" id="button">
                    </table>
                </div>
                <input type="hidden" name="levelGroupId" id="levelGroupId" value="<c:out value="${levelId}"/>"/>        
                <input type="hidden" name="parameterType" id="parameterType" value="<c:out value="${parameterType}"/>"/>        
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
<script>
    function submitPage(value) {
        var levelGroupId = '<c:out value="${levelId}"/>';
        if (value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value == 'First' || value == 'Last') {
//            alert(value);
            var temp = "";
            if (value == "GoTo") {
                temp = document.getElementById("goTO").value;
                if (temp != 'null') {
                    document.getElementById("pageNo").value = temp;
                }
            }
            else if (value == "First") {
                temp = "1";
                document.getElementById("pageNo").value = temp;
                value = 'GoTo';
            } else if (value == "Last") {
                temp = document.getElementById("last").value;
                document.getElementById("pageNo").value = temp;
                value = 'GoTo';
            }
            document.getElementById("button").value = value;
            var pageNos = document.getElementById("pageNo").value;
            var buttonval = document.getElementById("button").value;
            $('#ledgerDataList').html('');
            var url = '/throttle/viewTBDetailsNew.do?levelId=' + levelGroupId + '&param=' + value + '&pageNo=' + pageNos + '&button=' + buttonval;
            $.ajax({
                url: url,
                type: "get",
                success: function (data)
                {
                    $('#ledgerDataList').html(data);
                }
            });
        }
//        else if(value == 'export'){
//                     document.ledgerReport.action='/throttle/dayBook.do?param=export';
//                     document.ledgerReport.submit();
//               
//                }

    }
</script>                    