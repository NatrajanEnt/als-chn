
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>

        

        <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
        <title>City Master </title>
   <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Old Tax Details</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Old Tax Details</a></li>
            <li class="active">Old Tax Details</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body >
        <form name="cityMaster"  method="POST">

            

            <h2 align="center">Permit Details</h2>


           <table class="table table-info mb30 table-hover" id="table" style="width:100%">
                <thead height="30">
                   <tr id="tableDesingTH" >
                        <th> S.No </th>
                        <th> Vehicle No  </th>
                        <th> Receipt No  </th>
                        <th> Receipt Date  </th>
                        <th> Location </th>
                        <th> Amount </th>
                        <th> Period </th>
                        <th> Road Tax From Date </th>
                        <th> Road Tax To Date </th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${oldVehicletax != null}">
                        <c:forEach items="${oldVehicletax}" var="oldTax">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td align="left"> <%= sno%> </td>
                                <td align="left"> <c:out value="${regNo}"/></td>
                                <td align="left"> <c:out value="${oldTax.roadtaxreceiptno}" /></td>
                                <td align="left"><c:out value="${oldTax.roadtaxreceiptdate}"/></td>
                                <td align="left"><c:out value="${oldTax.roadtaxpaidlocation}"/></td>
                                <td align="left"><c:out value="${oldTax.roadtaxamount}"/></td>
                                <td align="left"><c:out value="${oldTax.roadtaxperiod}"/></td>
                                <td align="left"><c:out value="${oldTax.roadTaxFromDate}"/></td>
                                <td align="left"><c:out value="${oldTax.roadTaxToDate}"/></td>
                                <td align="left"><c:out value="${oldTax.roadTaxRemarks}"/></td>
                            </tr>



                        </c:forEach>
                    </tbody>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
            </table>
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <br>
            <br>
            <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>
            <br>
            <br>
            <br>
        </form>
    </body>
 </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>