/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

/**
 *
 * @author ramprasath
 */
import ets.domain.programmingfree.excelexamples.calculateDistance;
import ets.domain.report.business.DprTO;
import ets.domain.report.business.ReportTO;
//import ets.domain.thread.web.EmailSSL;
import ets.domain.thread.web.SendEmail;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import net.sf.json.JSONException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.sql.SQLException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.ArrayList;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Properties;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import java.util.*;
import java.text.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.*;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import org.jsoup.Jsoup;

public class tripAutomation extends Thread {

    int tripId = 0;
    int vehicleId = 0;
    int companyId = 0;
    String regNo = null;
    String gpsId = null;

    public tripAutomation(DprTO dprTO) throws ParseException, ClassNotFoundException, SQLException, org.json.simple.parser.ParseException {
        tripId = Integer.parseInt(dprTO.getTripId());
        vehicleId = Integer.parseInt(dprTO.getVehicleId());
        //companyId = Integer.parseInt(dprTO.getCompanyId());
        regNo = dprTO.getRegno();
        gpsId = dprTO.getGpsId();
    }

    public void run() {
        try {

            Connection con = null;
            Statement statement = null;
            Statement statement1 = null;
            ResultSet rs = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;
            ResultSet rs4 = null;

            PreparedStatement insertMail = null;

            System.out.println("I am in TripAutomation.........");

            String fileName = "jdbc_url.properties";
            InputStream is = getClass().getResourceAsStream("/" + fileName);
            Properties dbProps = new Properties();

            try {
                dbProps.load(is);//this may throw IOException
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("ex.printStackTrace()");
            }

            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            try {
                if (gpsId != null && !"".equals(gpsId)) {
                    Class.forName(dbClassName).newInstance();
                    con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                    PreparedStatement getMovementType = null;
                    PreparedStatement getRoute = null;
                    PreparedStatement getStatusId = null;
                    PreparedStatement getLocationDet = null;
                    PreparedStatement updateRouteCourse = null;
                    PreparedStatement updateTrip = null;
                    PreparedStatement updateTripStatus = null;
                    PreparedStatement getClearanceType = null;

                    String sql = "SELECT tm.trip_Id,sub_status,ifnull(movement_type_flag,0) as movement_type_flag, cn.comp_id \n"
                                + "FROM ts_trip_master tm,ts_trip_consignment tc,ts_consignment_note cn,ts_movement_type_master mtm\n"
                                + "WHERE tm.trip_Id=tc.trip_Id AND tc.consignment_Order_Id=cn.consignment_order_Id AND cn.movement_type=mtm.id \n"
                                + "AND trip_status_Id=10 AND IFNULL(movement_type_flag,0)!=0 and cn.comp_id=1470 \n"
                                + "AND tm.trip_Id=?";

                    String sql1 = "SELECT trip_route_course_Id,actual_point_id,latitude,longtitude,ifnull(movement_status,0) as movement_status  FROM  ts_trip_route_course \n"
                            + "WHERE trip_Id=? AND IFNULL(movement_status,0) IN (0,1) order by point_sequence limit 1";

                    String sql3 = "INSERT INTO ts_trip_status_details(trip_id,status_id,status_remarks,created_by,created_on,duration_date,duration_time,location_id)"
                            + "values (?,?,?,'1403',now(),curdate(),curtime(),?)";

                    String sql4 = "update ts_trip_route_course set movement_status=?, route_id = ? where trip_route_course_Id=?";
                    String sql5 = "update ts_trip_master set trip_status_Id=?,sub_status=? where trip_Id=?";
                    String sql7 = "select ifnull(clearance_type,'1') as clearance_type from ts_trip_master where trip_id=?";

                    int movementType = 0;
                    int subStatus = 0;
                    int tripRouteId = 0;
                    String destLat = null;
                    String destLon = null;
                    String actualPointId = null;
                    int statusId = 0;
                    int inOutFlag = 0;
                    int lastStatus = 0;

                    double distance = 0.00D;
                    double distance2 = 0.00D;
                    double clearance_type = 0.00D;

                    getMovementType = con.prepareStatement(sql);
                    getRoute = con.prepareStatement(sql1);

                    updateRouteCourse = con.prepareStatement(sql4);
                    updateTrip = con.prepareStatement(sql5);
                    updateTripStatus = con.prepareStatement(sql3);
                    getClearanceType = con.prepareStatement(sql7);

                    System.out.println("tripId-----GPS----"+tripId);
                    //to find Movement Type and substatus
                    getMovementType.setInt(1, tripId);
                    rs = getMovementType.executeQuery();
                    if (rs.next()) {
                        movementType = rs.getInt("movement_type_flag");
                        subStatus = rs.getInt("sub_status");
                        companyId = rs.getInt("comp_id");
                    }
                    
                    System.out.println("getMovementType +"+getMovementType);

                    if (movementType != 0) {
                        //to find destination
                        getRoute.setInt(1, tripId);
                        rs1 = getRoute.executeQuery();
                        if (rs1.next()) {
                            tripRouteId = rs1.getInt("trip_route_course_Id");
                            destLat = rs1.getString("latitude");
                            destLon = rs1.getString("longtitude");
                            actualPointId = rs1.getString("actual_point_id");
                        }

                        //to find Clearance Type
                        getClearanceType.setInt(1, tripId);
                        rs3 = getClearanceType.executeQuery();
                        if (rs3.next()) {
                            clearance_type = rs3.getInt("clearance_type");
                        }
                        
                        System.out.println("clearance_type +"+clearance_type);

                        String orderBy = "order_by";

                        if (clearance_type == 1) {
                            orderBy = "order_by_1";
                        } else if (clearance_type == 2) {
                            orderBy = "order_by_2";
                        } else if (clearance_type == 3) {
                            orderBy = "order_by_3";
                        } else if (clearance_type == 4) {
                            orderBy = "order_by_4";
                        } else if (clearance_type == 5) {
                            orderBy = "order_by_5";
                        } else if (clearance_type == 6) {
                            orderBy = "order_by_6";
                        }
                        
                        System.out.println("orderBy%%%%%"+orderBy);
                        
                        String sql2 = "SELECT status_Id,inout_flag,last_status FROM ts_status_master WHERE "
                                + " status_id not in (select status_id from ts_trip_status_details where trip_id=?) AND trip_movement_type=? \n"
                                + " and " + orderBy + " > 0 order by " + orderBy + " LIMIT 1;";

                        getStatusId = con.prepareStatement(sql2);
                        //to find next Stauts
                        getStatusId.setInt(1, tripId);
                        getStatusId.setInt(2, movementType);
                        rs2 = getStatusId.executeQuery();
                        if (rs2.next()) {
                            statusId = rs2.getInt("status_Id");
                            inOutFlag = rs2.getInt("inout_flag");
                            lastStatus = rs2.getInt("last_status");
                        }

                        // get GPS Location
                         System.out.println("befor soap service......");
                        String Location = "";
                        String latitude = "";
                        String longitude = "";
                        String Latitude = "";
                        String Longitude = "";
                        String lastUpdateTime = "";

                        String sql8 = "select location, latitude, longitude, gps_date from ts_vehicle_location where vehicle_id=?";
                        getLocationDet = con.prepareStatement(sql8);
                        getLocationDet.setInt(1, vehicleId);
                        System.out.println("getLocationDet +"+getLocationDet);
                        rs4 = getLocationDet.executeQuery();
                        if (rs4.next()) {
                            Location = rs4.getString("location");
                            longitude = rs4.getString("longitude");
                            latitude = rs4.getString("latitude");
                            lastUpdateTime = rs4.getString("gps_date");
                        }

                        Latitude = "" + latitude;
                        Longitude = "" + longitude;

//                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                        java.util.Date dDate = sdf.parse(lastUpdateTime);
//                        String pattern = "dd-MM-yyyy HH:mm:ss";
//                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//                        String date = simpleDateFormat.format(dDate);
                        System.out.println("cur date " + lastUpdateTime);//
                        System.out.println("statusId===" + statusId);
                        System.out.println("inOutFlag===" + inOutFlag);
                        System.out.println("tripRouteId===" + tripRouteId);
                        System.out.println("destLat===" + destLat);
                        System.out.println("destLon===" + destLon);
                        System.out.println("Latitude===" + Latitude);
                        System.out.println("Longitude===" + Longitude);

//                        Find Distance
                        calculateDistance cd = new calculateDistance();
                        distance = cd.distance(Double.parseDouble(Latitude), Double.parseDouble(Longitude),
                                Double.parseDouble(destLat), Double.parseDouble(destLon), 'K');

                          System.out.println("distance==km=" + distance);
                        distance = distance * 1000;

                         System.out.println("distance==m=" + distance);
//                        Find Distance
                        if (lastStatus == 0) {
                            distance2 = 3000;  //in
                        } else {
                            distance2 = 2000;  //out
                        }
                         System.out.println("inOutFlag==m=" + inOutFlag);
                         System.out.println("distance==m=" + distance);
                         System.out.println("distance2==m=" + distance2);
                         System.out.println("TripId==m=" + tripId);
                         System.out.println("lastStatus==m=" + lastStatus);

                        if ((inOutFlag == 1 && distance < 3000) || (inOutFlag == 2 && distance > distance2)) {
                              System.out.println("inside---statusId-"+statusId);
                            //////////////////////////////////////////////////////////////
                            int multipleFactory = 0;
                            int statusCount = 0;
                            int factCount = 0;
                            String factCounts = "0";
                            String statusCounts = "0";

                            if (statusId == 36 || statusId == 52 || statusId == 53 || statusId == 54 || statusId == 59) {
//                          if ("36".equals(statusId) || "52".equals(statusId) || "53".equals(statusId) || "54".equals(statusId) || "59".equals(statusId)) {

                                String sqlCnt = "select ifnull(count(*),0) as count from  ts_trip_route_course where trip_id=? and point_type like '%loading%'";
                                ResultSet rs5 = null;
                                PreparedStatement getfactCounts = null;
                                getfactCounts = con.prepareStatement(sqlCnt);
                                getfactCounts.setInt(1, tripId);
                                rs5 = getfactCounts.executeQuery();
                                if (rs5.next()) {
                                    factCounts = rs5.getString("count");
                                }

                                String sqlSts = "  select ifnull(count(*),0)+1 as scount from ts_trip_status_details where trip_id=? and status_id in (36,52,53,54,59)";
                                ResultSet rs6 = null;
                                PreparedStatement getstatCounts = null;
                                getstatCounts = con.prepareStatement(sqlSts);
                                getstatCounts.setInt(1, tripId);
                                rs6 = getstatCounts.executeQuery();
                                if (rs6.next()) {
                                    statusCounts = rs6.getString("scount");
                                }

                                factCount = Integer.parseInt(factCounts);
                                statusCount = Integer.parseInt(statusCounts);
                                multipleFactory = factCount - statusCount;
                            }

                            if (multipleFactory > 0 && "36".equals(statusId)) {
                                statusId = 34;
                            }

                            ////////////////////////////////////////////////////////////   
                            
                            updateRouteCourse.setInt(1, inOutFlag);
                            updateRouteCourse.setInt(2, 1);
                            updateRouteCourse.setInt(3, tripRouteId);

                            System.out.println("statusId----"+statusId);
                            System.out.println("lastStatus----"+lastStatus);
                            
                            if (lastStatus == 0) {
                                updateTrip.setInt(1, 10);
                            } else {
                                updateTrip.setInt(1, 12);
                            }

                            updateTrip.setInt(2, statusId);
                            updateTrip.setInt(3, tripId);

                            updateTripStatus.setInt(1, tripId);
                            updateTripStatus.setInt(2, statusId);
                            updateTripStatus.setString(3, Location);
                            updateTripStatus.setString(4, actualPointId);

                            int routeUpdate = 0;
                            int tripUpdate = 0;
                            int tripStatusUpdate = 0;
                            int tripStatusUpdate1 = 0;
                            int tripStatusUpdate2 = 0;

                            routeUpdate = updateRouteCourse.executeUpdate();
                            System.out.println("routeUpdate NEW: "+tripId+ ": "+ routeUpdate);
                            tripUpdate = updateTrip.executeUpdate();
                            System.out.println("tripUpdate NEW : "+tripId+ ": "+ tripUpdate);
                            tripStatusUpdate = updateTripStatus.executeUpdate();
                            System.out.println("tripStatusUpdate@@@@@@ NEW: : "+tripId+ ": "+ tripStatusUpdate);
                            System.out.println("statusIdstatusId---: "+tripId+ ": "+ statusId);

//                            if ("40".equals(statusId) || "46".equals(statusId) || "52".equals(statusId)) {
                            if (statusId == 40 || statusId == 46) {
                                System.out.println("statusId=="+statusId);
                                PreparedStatement updateTripEnd = null;
                                updateTripEnd = con.prepareStatement("UPDATE ts_trip_master "
                                        + " SET trip_actual_end_date =  now(), trip_actual_end_time = curtime(),"
                                        + " vehicle_destination_actual_reporting_date =now(),   vehicle_destination_actual_reporting_time =curtime(), "
                                        + " vehicle_actual_unloading_date = now() , vehicle_actual_unloading_time =curtime() where trip_id=? ");
                                updateTripEnd.setInt(1, tripId);
                                tripStatusUpdate1=updateTripEnd.executeUpdate();
                                System.out.println("updateTripEnd=="+tripStatusUpdate1);
                                
                                PreparedStatement updateAvailability = null;
                                updateAvailability = con.prepareStatement("UPDATE ts_vehicle_availability "
                                        + " SET available_ind = 'Y', trip_id = 0, city_id = 0, expected_arrival_date =null ,"
                                        + " expected_arrival_time = null, job_card_id = 0 where vehicle_id = ? ");
                                updateAvailability.setInt(1, vehicleId);
                                tripStatusUpdate2=updateAvailability.executeUpdate();
                                System.out.println("updateAvailability=="+updateAvailability);
                                String sqlCnote = " select consignment_order_id from ts_trip_consignment where trip_id = ?";
                                int CnoteNo = 0;
                                int count = 0;

                                ResultSet rs7 = null;
                                PreparedStatement getCnot = null;
                                getCnot = con.prepareStatement(sqlCnote);
                                getCnot.setInt(1, tripId);
                                rs7 = getCnot.executeQuery();
                                if (rs7.next()) {
                                    CnoteNo = rs7.getInt("consignment_order_id");
                                }

                                String sqlContainr = " select count(*) as val from ts_consignment_container_details where consignment_order_id=? and (planned_status is null) ";
                                ResultSet rs8 = null;
                                PreparedStatement getContainer = null;
                                getContainer = con.prepareStatement(sqlContainr);
                                getContainer.setInt(1, CnoteNo);
                                rs8 = getContainer.executeQuery();
                                
                                if (rs8.next()) {
                                    count = rs8.getInt("val");
                                }

                                if (count == 0) {
                                    PreparedStatement updateConsignStatus = null;
                                    updateConsignStatus = con.prepareStatement("UPDATE ts_consignment_note SET Consignment_Order_Status = ?  where consignment_order_id = ? ");
                                    updateConsignStatus.setInt(1, 12);
                                    updateConsignStatus.setInt(2, CnoteNo);
                                    updateConsignStatus.executeUpdate();
                                }

                                String sqldays = " select concat(date_format(trip_actual_start_date,'%d-%m-%Y') ,'~', date_format(trip_actual_end_date,'%d-%m-%Y') ) as days "
                                        + " from ts_trip_master where trip_id=? and vehicle_vendor_id=1 ";
                                ResultSet rs9 = null;
                                PreparedStatement getTrpDays = null;
                                String tripDays = "";
                                getTrpDays = con.prepareStatement(sqldays);
                                getTrpDays.setInt(1, tripId);
                                rs9 = getTrpDays.executeQuery();
                                if (rs9.next()) {
                                    tripDays = rs9.getString("days");
                                }

                                String sqlEmpId = " select b.emp_id as emp_id from ts_trip_driver a left join papl_emp_master b on (a.emp_id=b.emp_id and b.active_ind='Y') "
                                        + " where trip_id = ? and a.active_ind='Y' ";
                                ResultSet rs10 = null;
                                PreparedStatement getTrpEmp = null;
                                int empId = 0;
                                getTrpEmp = con.prepareStatement(sqlEmpId);
                                getTrpEmp.setInt(1, tripId);
                                rs10 = getTrpEmp.executeQuery();
                                if (rs10.next()) {
                                    empId = rs10.getInt("emp_id");
                                }

                                System.out.println("tripDays=" + tripDays);

                                if (!"".equals(tripDays) && tripDays != null) {
                                    String[] tripDay = tripDays.split("~");
                                    System.out.println("dates=" + tripDay[0] + " to " + tripDay[1]);
                                    String[] dateList = getDateList(tripDay[0], tripDay[1]);
                                    int dailyBata = 0;
                                    int driverPadi = 0;
                                    int cleanerPadi = 0;
                                    for (int j = 0; j < dateList.length; j++) {
                                        String dateLists = dateList[j];
                                        System.out.println("dateList[j]::" + dateLists);

                                        String sqlBhataCnt = " select count(1) as cnt from ts_daily_driverbata where effective_date= STR_TO_DATE(?,'%d-%m-%Y') and emp_id=? ";
                                        ResultSet rs11 = null;
                                        PreparedStatement getBhat = null;
                                        int empCnt = 0;
                                        getBhat = con.prepareStatement(sqlBhataCnt);
                                        getBhat.setString(1, dateLists);
                                        getBhat.setInt(2, empId);
                                        rs11 = getBhat.executeQuery();
                                        if (rs11.next()) {
                                            empCnt = rs11.getInt("cnt");
                                        }

                                        String sqlBhataDay = " select concat(round(dwage_pday),'~',round(cwage_pday)) as bata from driver_salaryconfig_master where active_ind='Y' and comp_id = ? limit 1";
                                        ResultSet rs12 = null;
                                        PreparedStatement getBhata = null;
                                        String empBhata = "";
                                        getBhata = con.prepareStatement(sqlBhataDay);
                                        getBhata.setInt(1, companyId);
                                        rs12 = getBhata.executeQuery();
                                        if (rs12.next()) {
                                            empBhata = rs12.getString("bata");
                                        }

                                        double dpadi = 0;
                                        double cpadi = 0;

                                        System.out.println("bhata driver salary config : " + empBhata);
                                        if (empCnt == 0) {
                                            System.out.println("check if inside over");
                                            if (empBhata != null) {
                                                System.out.println("bata if inside over");
                                                String[] temp = empBhata.split("~");
                                                int result = Integer.parseInt(temp[0]);
                                                int result1 = Integer.parseInt(temp[1]);
                                                driverPadi = driverPadi + result;
                                                System.out.println("cleanerPadicleanerPadicleanerPadicleanerPadi--" + driverPadi);
                                                cleanerPadi = cleanerPadi + result1;
                                                System.out.println("cleanerPadicleanerPadicleanerPadi--" + cleanerPadi);

                                                dpadi = Double.parseDouble(temp[0]);
                                                cpadi = Double.parseDouble(temp[1]);
                                            } else {
                                                System.out.println("bata else over here");
                                                driverPadi = driverPadi + 150;
                                                cleanerPadi = cleanerPadi + 100;
                                                dpadi = 150.00;
                                                cpadi = 100.00;
                                            }

                                            PreparedStatement updateBhataInsert = null;
                                            updateBhataInsert = con.prepareStatement("insert into ts_daily_driverbata(emp_id,effective_date,trip_id,driver_padi,cleaner_padi,created_on) "
                                                    + " values (?, STR_TO_DATE(?,'%d-%m-%Y'), ? , ?, ?, now() )");

                                            updateBhataInsert.setInt(1, empId);
                                            updateBhataInsert.setString(2, dateLists);
                                            updateBhataInsert.setInt(3, tripId);
                                            updateBhataInsert.setString(4, dpadi + "");
                                            updateBhataInsert.setString(5, cpadi + "");
                                            dailyBata = updateBhataInsert.executeUpdate();
                                            System.out.println("dailyBata==" + dailyBata);

                                        }
                                    }
//
                                    String totalBata = " select concat(sum(driver_padi),'~',sum(cleaner_padi)) as bata from ts_daily_driverbata where trip_id = ? ";

                                    ResultSet rs13 = null;
                                    PreparedStatement gettotBhata = null;
                                    String totBhata = "";
                                    gettotBhata = con.prepareStatement(totalBata);
                                    gettotBhata.setInt(1, companyId);
                                    rs13 = gettotBhata.executeQuery();
                                    if (rs13.next()) {
                                        totBhata = rs13.getString("bata");
                                    }
                                    System.out.println("totalBata==" + totBhata);
                                    System.out.println("driverPadi==" + driverPadi);
                                    System.out.println("cleanerPadi==" + cleanerPadi);

                                    String tripDriver = "select b.emp_id as tripDriver from\n" +
                                                        "ts_trip_driver a left join papl_emp_master b on (a.emp_id=b.emp_id and b.active_ind='Y')\n" +
                                                        "where trip_id = ? and a.active_ind='Y';";

                                    ResultSet rs14 = null;
                                    PreparedStatement getTripDriver = null;
                                    String driverId = "";
                                    getTripDriver = con.prepareStatement(tripDriver);
                                    getTripDriver.setInt(1, tripId);
                                    rs14 = getTripDriver.executeQuery();
                                    if (rs14.next()) {
                                        driverId = rs14.getString("tripDriver");
                                    }
                                    System.out.println("driverId--@#$--"+driverId);
                                    
                                    String tripCleaner = "select b.emp_id as tripCleaner from\n" +
                                                        "ts_trip_driver a left join papl_emp_master b on (b.emp_id=a.cleaner_id and b.active_ind='Y')\n" +
                                                        "where trip_id = ? and a.active_ind='Y'";

                                    ResultSet rs15 = null;
                                    PreparedStatement getTripCleaner = null;
                                    String cleanerId = "";
                                    getTripCleaner = con.prepareStatement(tripCleaner);
                                    getTripCleaner.setInt(1, tripId);
                                    rs15 = getTripCleaner.executeQuery();
                                    if (rs15.next()) {
                                        cleanerId = rs15.getString("tripCleaner");
                                    }
                                    System.out.println("cleanerId--@#$--"+cleanerId);

                                    if (driverPadi != 0 && dailyBata != 0) {
                                        System.out.println("final insert for driver/cleaner bata over here");
                                        String driverBata = "insert into ts_trip_expense "
                                                + " ( trip_id, vehicle_Id, expense_id, expense_by, expense_date, expense_remarks, \n"
                                                + "        expense_type, applicable_tax_percentage, expense_value,currency, total_expense_value, \n"
                                                + "        document_required, created_by, created_on,expense_cat) \n"
                                                + "        values ( ?, ?, ?, ?, now() ,'System Update',\n"
                                                + "        2, 0.00, ? ,1, ?, 0,  1043, now(),'1') ";

                                        PreparedStatement updateBhataInsert = null;
                                        updateBhataInsert = con.prepareStatement(driverBata);

                                        updateBhataInsert.setInt(1, tripId);
                                        updateBhataInsert.setInt(2, vehicleId);
                                        updateBhataInsert.setString(3, "1024");
                                        updateBhataInsert.setString(4, driverId);
                                        updateBhataInsert.setString(5, driverPadi + "");
                                        updateBhataInsert.setString(6, driverPadi + "");
                                        dailyBata = updateBhataInsert.executeUpdate();
                                        System.out.println("dailyBata==" + dailyBata);

                                        PreparedStatement updateBhataInsert1 = null;
                                        updateBhataInsert1 = con.prepareStatement(driverBata);

                                        updateBhataInsert1.setInt(1, tripId);
                                        updateBhataInsert1.setInt(2, vehicleId);
                                        updateBhataInsert1.setString(3, "1031");
                                        updateBhataInsert1.setString(4, cleanerId);
                                        updateBhataInsert1.setString(5, cleanerPadi + "");
                                        updateBhataInsert1.setString(6, cleanerPadi + "");
                                        dailyBata = updateBhataInsert1.executeUpdate();
                                        System.out.println("dailyBata==" + dailyBata);

                                    }

                                }
                            }

                        }
                    }

                }
            } catch (SQLException e) {
                System.err.println("Caught SQLException: " + e.getMessage());
                if (insertMail != null) {
//                    insertMail.setString(1, e.getMessage());
//                    insertMail.setString(2, e.getMessage());
//                    insertMail.executeUpdate();
                }
            } finally {
                con.close();
            }
        } catch (Exception e) {
            //   System.err.println("Caught Exception: " + e.getMessage());
//            ReportTO rTO = new ReportTO();
//            rTO.setMailSubjectTo("Trip Automation Error");
//            rTO.setMailContentTo("vehicle No : "+regNo);
//            rTO.setMailIdTo("arun@entitlesolutions.com");
//            rTO.setMailIdCc("throttleerror@gmail.com");
            // new EmailSSL(rTO).start();
        }
    }

    public String[] getDateList(String startDate, String endDate) {
        String[] dateList = new String[365];
        String[] datList = null;
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Date startDt = df.parse(startDate);
            Date endDt = df.parse(endDate);
            Calendar startCal, endCal;
            startCal = Calendar.getInstance();
            startCal.setTime(startDt);
            endCal = Calendar.getInstance();
            endCal.setTime(endDt);
            System.out.println("startDate" + startDate);
            System.out.println("endDate" + endDate);
            //Just in case the dates were transposed this prevents infinite loop
            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                startCal.setTime(endDt);
                endCal.setTime(startDt);
            }
            startCal.add(Calendar.DATE, -1);
            int i = 0;
            do {
                startCal.add(Calendar.DAY_OF_MONTH, 1);
                System.out.println("Day: " + df.format(startCal.getTime()));
                dateList[i] = df.format(startCal.getTime());
                System.out.println("Day++: " + dateList[i]);
                i++;
            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis());
            datList = new String[i];
            for (int m = 0; m < datList.length; m++) {
                datList[m] = dateList[m];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return datList;
    }

}
