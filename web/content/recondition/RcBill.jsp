
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
</head>
<body>
    <form name="grn" method="post">

        <% int index = 0;%> 
        <c:if test = "${billHeaderInfo != null}" >
            <table  class="table table-info mb30 table-hover" style="width: 40%">
                <c:forEach items="${billHeaderInfo}" var="bill"> 
                    <% if (index == 0) {%>   
                    <tr>
                        <td class="contentsub" colspan="2" align="center"> WO&nbsp;&nbsp;: &nbsp;&nbsp;<c:out value="${bill.woId}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Vendor :</td>
                        <td class="text1"><c:out value="${bill.vendorName}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Invoice No:</td>
                        <td class="text1"><c:out value="${bill.invoiceNo}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Invoice Date :</td>
                        <td class="text1"><c:out value="${bill.receiveDate}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Invoice Amount :</td>
                        <td class="text1"><c:out value="${bill.price}"/></td>
                    </tr>


                    <% index++;
            }%>               
                </c:forEach>
            </table>

            <table  class="border" style="width: 50%"> 
                <tr>
                    <td class="contentsub" >S No</td>
                    <td class="contentsub" >Item<br> Name</td>                        
                    <td class="contentsub" >Tyre<br> No</td>                        
                    <td class="contentsub" >Material<br>Cost</td>
                    <td class="contentsub" >Material<br>Tax</td>
                    <td class="contentsub" >Labour<br> Cost</td>
                    <td class="contentsub" >Labour<br> Tax</td>
                    <td class="contentsub" > Amount</td>
                    <td class="contentsub" > RC Status</td>
                </tr>
                <%  index = 0;%> 
                <c:forEach items="${billHeaderInfo}" var="bill"> 
                    <%
        String classText = "";
        int oddEven = index % 2;
        if (oddEven > 0) {
            classText = "text2";
        } else {
            classText = "text1";
        }
                    %>	
                    <tr>
                        <td class="text1" ><%=index + 1%></td>
                        <td class="text1" ><c:out value="${bill.itemName}"/></td>                          
                        <td class="text1" ><c:out value="${bill.tyreNo}"/></td>
                        <td class="text1" ><c:out value="${bill.materialCostExternal}"/></td>
                        <td class="text1" ><c:out value="${bill.tax}"/></td>
                        <td class="text1" ><c:out value="${bill.laborAmount}"/></td>
                        <td class="text1" ><c:out value="${bill.laborwithTax}"/></td>
                        <td class="text1" >
                            <fmt:formatNumber value="${bill.materialCostExternal+bill.tax+bill.laborAmount+bill.laborwithTax}" pattern="##.00"/>
                        </td>
                        <td class="text1" ><c:out value="${bill.rcstatus}"/></td>
                    </tr>
                    <% index++;%>
                </c:forEach>

            </table>
        </c:if>
        <br>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>


<%@ include file="/content/common/NewDesign/settings.jsp" %>
