<%-- 
    Document   : handleVendorPayment
    Created on : 9 Feb, 2016, 3:39:38 PM
    Author     : pavithra
--%>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
         <script type="text/javascript">
        
                          var httpReq;
                            var temp = "";
                            function setVendor(vendorTypeid) { //alert(str);
                                $.ajax({
                                    url: "/throttle/vendorPaymentsJson.do",
                                    dataType: "json",
                                    data: {
                                        vendorType: vendorTypeid
                                    },
                                    success: function (temp) {
//                                         alert(temp);
                                        if (temp != '') {
                                            $('#vendorName').empty();
                                            $('#vendorName').append(
                                                    $('<option style="width:150px"></option>').val(0).html('--Select--')
                                                    )
                                            $.each(temp, function (i, data) {
                                                $('#vendorName').append(
                                                        $('<option style="width:150px"></option>').val(data.vendorId+ "~" + data.vendorName).html(data.vendorName)
                                                        )
                                            });
                                        } else {
                                            $('#vendorName').empty();
                                        }
                                    }
                                });

                            }
        </script>
    </head>
    <script type="text/javascript">
        function submitPage(value)
        {
           if(textValidation(document.vendor.vendorName,'vendorType')){
                return;
            }
            else if(textValidation(document.vendor.vendorName,'vendorName')){
                return;
            }
           

            document.vendor.action="/throttle/vendorPaymentView.do";
            document.vendor.submit();

        }

             

    </script>
</head>
<body>
    <form name="vendor">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
        <table width="700" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                    </h2></td>
                <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
            </tr>
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:700px;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1">Vendor Payment</li>
                        </ul>
                        <div id="first">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <c:if test = "${VendorTypeList != null}" >

                                        <td class="text" height="30"><font color="red">*</font>Vendor Type</td>
                                        <td class="text">
                                            <select class="form-control" name="vendorType" id="vendorType" onchange="setVendor(this.value);">
                                                <option value='0'>-Select-</option>
                                                <c:forEach items="${VendorTypeList}" var="vendor">
                                                    <option value="<c:out value="${vendor.vendorTypeId}"/>"><c:out value="${vendor.vendorTypeValue}"/></option>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </c:if>
                                    <td>Vendor Name</td>
                                    <td>
                                         <select class="form-control" name="vendorName" id="vendorName" style="width:125px;">
                                                                       
                                                                    </select>
                                    </td>
                                </tr>
                                <tr>
                                   
                                    <td colspan="4">
                                        <center>
                                        <input type="button" class="button" name="search"  onclick="submitPage(this);" value="Search">
                                        </center></td>
                                </tr>
                            </table>
                        </div></div>
                </td>
            </tr>
        </table>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
      
</body>
</html>

