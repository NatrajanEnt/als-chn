
<!DOCTYPE html>
<html>
    <head>
        <title</title>

        <meta charset="utf-8">
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="/css/common.css" type="text/css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="/css/mandrill.css" type="text/css" media="screen" charset="utf-8">
        <link rel="stylesheet" href="/css/popup.css" type="text/css" media="screen" charset="utf-8">
        <!--[if lte IE 9]>
            <link rel="stylesheet" href="/css/ie8.css" type="text/css" media="screen" charset="utf-8">
        <![endif]-->
        <script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
        <script type="text/javascript" src="/js/mdcommon.min.js"></script>
        <script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.min.js"></script>
        <% String eventId = (String) request.getParameter("eventId");%>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-329148-80', 'auto', {
                'allowLinker': true
            });
            ga('require', 'linker');
            ga('linker:autoLink', [/(.*\.)?mandrill(app)?\.com$/], false, true);
            ga('send', 'pageview');

        </script>

        <script type="text/javascript">
            //$(function() {
            //    $('#tabs').tabs();
            //});
            $(document).ready(function () {

                $('#show-tab-html').addClass('btn-selected');
                $('#tab-text').hide();
                var frame = $('#html-frame')[0];
                var resizeFrame = function () {
                    frame.style.height = (frame.contentWindow.document.body.scrollHeight + 60) + 'px';
                };
                //We're doing this via innerHTML without an obvious onload event, so instead just back off and resize the iframe as we go
                resizeFrame();
                setTimeout(resizeFrame, 1000);
                setTimeout(resizeFrame, 2000);
                setTimeout(resizeFrame, 5000);
                setTimeout(resizeFrame, 10000);

                $('#show-tab-html').click(function (event) {
                    $('#tab-text').fadeOut('250');
                    $('#tab-html').fadeIn('250');
                    $('#show-tab-text').removeClass('btn-selected');
                    $('#show-tab-html').addClass('btn-selected');
                    event.preventDefault();
                });

                $('#show-tab-text').click(function (event) {
                    $('#tab-html').fadeOut('250');
                    $('#tab-text').fadeIn('250');
                    $('#show-tab-html').removeClass('btn-selected');
                    $('#show-tab-text').addClass('btn-selected');
                    event.preventDefault();
                });
            });
        </script>
    </head>
    <body>
        <div id="flash">

        </div>
        <div id="content">
            <div class="popup-header">
                <ul class="linear-list button-nav above0">
                    <li><button href="#tab-html" id="show-tab-html" class="btn" >Content</button></li>                
                    <!--<li><button href="#tab-text" id="show-tab-text" class="btn" >Text</button></li>-->            
                </ul>
            </div>
            <div class="tab-container">
                <div id="tab-html" class="tab-content">
                    <iframe id="html-frame" src="/throttle/viewTemplateContent.do?eventId=<%=eventId%>" width="100%" sandbox="allow-same-origin"></iframe>
                </div>            
            </div>
        </div>
    </body>
</html>
