<%-- 
    Document   : viewclosedbill
    Created on : Dec 10, 2013, 12:51:26 PM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
       <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
        <script>
            function submitPage()
            {
                        //alert('hi');
                        document.billDetails.action="/throttle/viewbillpage.do";
                        document.billDetails.submit();
            }
//            function showBillDeatil(invoiceId){
//                //alert("hi");
//                document.billDetails.action="/throttle/showinvoicedetail.do?invoiceId="+invoiceId;
//                document.billDetails.submit();
//
//            }

function showBillDeatil(invoiceId,tripId){
//                alert("hi"+tripId);
                document.billDetails.action="/throttle/showinvoicedetail.do?invoiceId="+invoiceId + "&tripId="+tripId;
                document.billDetails.submit();

            }
        </script>
    </head>
    <%
        String menuPath = "Finance >> View Bills";
        request.setAttribute("menuPath", menuPath);
    %>
    <body>
        <form name="billDetails" method="post">
             <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp"%>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
               <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1"> View Bill Details</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                      <td><font color="red">*</font>Customer Name</td>
                                      <td height="30">
                                         <select class="textbox" name="customerId" id="customerId"  style="width:125px;"><option value="">---Select---</option>
                                                <c:forEach items="${customerList}" var="customerList">
                                                    <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                                    </c:forEach>
                                            </select>
                                      </td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td> <input type="hidden" name="days" id="days" value="" /></td>
                                        <td><input type="button" class="button"   value="FETCH DATA" onclick="submitPage();"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                    </tr>
                    </table>
            <br>
            <br>
            <c:if test="${closedBillList !=nul}">
             <table align="center" border="0" id="table" class="sortable" style="width:1700px;" >
               <thead>
                    <tr height="45">
                        <th width="34" class="contentsub">S.No</th>
                        <th width="150" class="contentsub">Bill.No</th>
                        <th width="150" class="contentsub">Invoice No</th>
                        <th width="100" class="contentsub">Bill Date</th>
                        <th width="130" class="contentsub">Customer</th>
                        <th width="130" class="contentsub">Contract Type</th>
                        <th width="130" class="contentsub">Amount</th>
                        <th width="130" class="contentsub">Status</th>
                        <th width="130" class="contentsub">Details</th>
                    </tr>
               </thead>
               <tbody>
                   <%int sno=1;
                   int index =0;
                   String classText="";
                   %>
                   <c:forEach items="${closedBillList}" var="closedBillList">
                       <%int oddEven = index % 2;
                     if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                       %>
                   <tr>
                       <td class="<%=classText%>" height="30"><%=sno++%></td>
                       <td class="<%=classText%>" height="30"><c:out value="${closedBillList.invoiceCode}"/></td>
                       <td class="<%=classText%>" height="30"><c:out value="${closedBillList.invoiceNo}"/></td>
                       <td class="<%=classText%>" height="30"><c:out value="${closedBillList.createddate}"/></td>
                       <td class="<%=classText%>" height="30"><c:out value="${closedBillList.invoicecustomer}"/></td>
                       <td class="<%=classText%>" height="30"><c:out value="${closedBillList.billingTypeName}"/></td>
                       <td class="<%=classText%>" height="30"><c:out value="${closedBillList.grandTotal}"/></td>
                       <td class="<%=classText%>" height="30">Not Paid</td>
<!--                       <td class="<%=classText%>" height="30"><a href="" onclick="showBillDeatil('<c:out value="${closedBillList.invoiceId}"/>')">Details</a></td>-->
                       <td class="<%=classText%>" height="30"><a href="" onclick="showBillDeatil('<c:out value="${closedBillList.invoiceId}"/>','<c:out value="${closedBillList.tripId}"/>')">Details</a></td>
                   </tr>
                   <%index++;%>
                   </c:forEach>
               </tbody>
           </table>
            </c:if>
              <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" >5</option>
                        <option value="10">10</option>
                        <option value="20" selected="selected">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>
