<%-- 
    Document   : GRSummaryReport
    Created on : Feb 12, 2016, 4:20:53 PM
    Author     : Jp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css">
            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */



            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }
        </style>
        <script>
            $(document).ready(function() {
                $('.ball, .ball1').removeClass('stop');
                $('.trigger').click(function() {
                    $('.ball, .ball1').toggleClass('stop');
                });
            });

        </script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#vehicleNo').val(tmp[1]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>



        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
//                } else if (document.getElementById('grNo').value == '') {
//                    alert("Please select GR NO");
//                    document.getElementById('grNo').focus();
//                } else if (document.getElementById('consignorNo').value == '') {
//                    alert("Please select consignor No");
//                    document.getElementById('consignorNo').focus();
//                } else if (document.getElementById('vehicleNo').value == '') {
//                    alert("Please select vehicleNo");
//                    document.getElementById('vehicleNo').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleGRSummary.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleGRSummary.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }

            function viewCustomerProfitDetails(tripIds) {
//            alert(tripIds);
                window.open('/throttle/viewGrReportExpenseDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>
    <body onload="getVehicleNos();">
        <!--<body>-->
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">GR Summary</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red"></font>GR No</td>
                                        <td height="30"><input name="grNo" id="grNo" type="text" class="text" style="width:129px;" value="<c:out value="${grNo}"/>" ></td>
                                        <td><font color="red"></font>Container No</td>
                                        <td height="30"><input name="containerNo" id="containerNo" type="text" class="text" style="width:129px;" value="<c:out value="${containerNo}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td><font color="red"></font>Vehicle No</td>
                                        <td height="30"><input name="vehicleNo" id="vehicleNo" type="text" class="text" style="width:129px;" value="<c:out value="${vehicleNo}"/>" ></td>
                                        <td><font color="red"></font>Movement Type</td>
                                        <td height="30">
                                        <select name="movementType" id="movementType" type="text" class="text" style="width:129px;" value="">
                                            <option value=''>--select--</option>     
                                            <option value='Import'>Import</option>     
                                            <option value='Export'>Export</option>     
                                            <option value='Repo'>Repo</option>     
                                            <option value='Import DSO'>Import DSO</option>     
                                            <option value='Export DSO'>Export DSO</option>     
                                        </select>
                                            <script>
                                                document.getElementById("movementType").value='<c:out value="${movementType}"/>'  
                                            </script>
                                            <!--<input name="movementType" id="movementType" type="text" class="text" value="<c:out value="${movementType}"/>"></td>-->
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:129px;" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:129px;" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td>
                                    <br>   
                                        </td>
                                    </tr>
                                    <tr>
                                           <td><font color="red"></font>Trip status</td>
                                       <td>
                                                <select class="textbox"  id="tripStatus"  name="tripStatus" style="width:129px;" value="">
                                                  <option selected  value="">---Select---</option>
                                                    <c:if test = "${tripStatusList != null}" >
                                                        <c:forEach items="${tripStatusList}" var="trip">
                                                            <option  value='<c:out value="${trip.tripStatusId}" />'> <c:out value="${trip.tripStatusName}" /></option>
                                                            </c:forEach >
                                                        </c:if>
                                                </select>
                                            </td>     
                                            
                                            <script>
                                                document.getElementById("tripStatus").value='<c:out value="${tripStatus}"/>'  
                                            </script>
                                    </tr>
                                      
                                    <tr>
                                        <td>
                                    <br>   
                                        </td>
                                    </tr>
                                    
                                    <tr>
                                        <td colspan="2" align="right"><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="2"><input type="button" class="button" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>

                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>


            <br>
            <br>
            <c:if test = "${GRSummaryList == null && GRSummaryListSize != null}" >
                <center>
                    <font color="blue">Please Wait Your Request is Processing</font>
                    <div class="container">
                        <div class="content">
                            <div class="circle"></div>
                            <div class="circle1"></div>
                        </div>
                    </div>
                </center>
            </c:if>

            <br>
            <br>

            <c:if test = "${GRSummaryList != null && GRSummaryListSize == null}" >
                <!--<table border="1" id="table" align="center" width="100%" cellpadding="0" cellspacing="1" >-->
                <table align="center" width="100%" border="0" id="table" class="sortable">
                    <thead>
                        <tr>
                            <td rowspan="2" class="contenthead">S.No</td>
                            <td rowspan="2" class="contenthead">Booking Office</td>
                            <td rowspan="2" class="contenthead">Empty Pick Up</td>
                            <!--                            <td rowspan="2" class="contenthead">MFR Name</td>-->
                            <td rowspan="2" class="contenthead">Movement Type</td>
                            <td rowspan="2" class="contenthead">G.R No.</td>
                            <td rowspan="2" class="contenthead">Challan No.</td>
                            <td rowspan="2" class="contenthead">TPT Name</td>
                            <td rowspan="2" class="contenthead">GR Submission Date</td>
                            <td rowspan="2" class="contenthead">Trip Sheet No.</td>
                            <td rowspan="2" class="contenthead">Amount</td>
                            <td rowspan="2" class="contenthead">Diesel Qtty.</td>
                            <td rowspan="2" class="contenthead">Trip Sheet Submit</td>
                            <td rowspan="2" class="contenthead">Date of Issue</td>
                            <td colspan="3" class="contenthead" style="text-align:center">Shipper Detail</td>
                            <td colspan="2" class="contenthead" style="text-align:center">Movement </td>
                            <td colspan="6" class="contenthead" style="text-align:center">Conatiner Details </td>
                            <td colspan="6" class="contenthead" style="text-align:center">Revenue </td>

                        </tr> 
                        <tr>
                            <td class="contenthead">Consignee</td>
                            <td class="contenthead">Consignor </td>
                            <td class="contenthead">Billing Party </td>

                            <td class="contenthead">Origin </td>
                            <td class="contenthead">Destination </td>

                            <td class="contenthead">Conatiner No</td>
                            <td class="contenthead">20' </td>
                            <td class="contenthead">40' </td>
                            <td class="contenthead">Type L/E </td>
                            <td class="contenthead">S.liNe </td>
                            <td class="contenthead">Lorry No. </td>

                            <td class="contenthead">Bill No.</td>
                            <td class="contenthead">Transporation Charges</td>
                            <td class="contenthead">Detention Charges</td>
                            <td class="contenthead">Weightment</td>
                            <td class="contenthead">Other Charges</td>
                            <td class="contenthead">Total</td>

                        </tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                    <c:set var="totalTransportationCharge" value="${0}"/>
                    <c:forEach items="${GRSummaryList}" var="csList">
                        <tr>
                            <td align="center"><%=sno%></td>
                            <td align="left">DICT(Sonepat)</td>
                            <td align="left"><c:out value="${csList.emptyPickup}"/></td>
                            <td align="left"><c:out value="${csList.movementType}"/></td>

                            <td align="left"><c:out value="${csList.grNo}"/></td>
                            <td align="left"><c:out value="${csList.challanNo}"/></td>
                            <td align="left"><c:out value="${csList.transporter}"/></td>
                            <%--<td align="left"><c:out value="${csList.grDate}"/></td>--%>
                            <td align="left"><c:out value=""/></td>
                            <td align="left"><c:out value="${csList.tripCode}"/></td>
                            <td align="left"><c:out value="${csList.tripFuelAmount}"/></td>
                            <td align="left"><c:out value="${csList.liters}"/></td>
                            <td align="left"><c:out value=""/></td>
                            <td align="left"><c:out value="${csList.grDate}"/></td>
                            <td align="left"><c:out value="${csList.consigneeName}"/></td>
                            <td align="left"><c:out value="${csList.consignorName}"/></td>
                            <td align="left"><c:out value="${csList.billingParty}"/></td>
                            <td align="left">
                                <c:if test = "${csList.movementType == 'Import' || csList.movementType == 'Import DSO' }">
                                    <c:out value="${csList.destination}"/>
                                </c:if>
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Export DSO' || csList.movementType == 'Repo' }">
                                    <c:out value="${csList.origin}"/>
                                </c:if>
                            </td>
                            <td align="left">
                                <c:if test = "${csList.movementType == 'Import' || csList.movementType == 'Import DSO' }">
                                    <c:out value="${csList.origin}"/>
                                </c:if>
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Export DSO' || csList.movementType == 'Repo' }">
                                    <c:out value="${csList.destination}"/>
                                </c:if>
                            </td>
                            <%--<td align="left"><c:out value="${csList.destination}"/></td>--%>
                            <td align="left"><c:out value="${csList.containerNo}"/></td>
                            <td align="left"><c:out value="${csList.twentyFT}"/></td>
                            <td align="left"><c:out value="${csList.fortyFT}"/></td>
                            <%--<td align="left"><c:out value="${csList.containerType}"/></td>--%>
                            <td align="center">
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Import'|| csList.movementType == 'Export DSO'|| csList.movementType == 'Import DSO' }" >
                                    L
                                </c:if>
                                <c:if test = "${csList.movementType == 'Repo'}">
                                    E
                                </c:if>
                            </td>
                            <td align="left"><c:out value="${csList.linerName}"/></td>
                            <td align="left"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="left"></td>
                            <td align="left"><a href="#" onclick="viewCustomerProfitDetails('<c:out value="${csList.tripId}"/>');"><c:out value="${csList.tripFuelAmount + csList.requestedAdvance}"/></a></td>
                            <td align="left"></td>
                            <td align="left"></td>
                            <td align="left"></td>
                            <td align="left"><c:out value="${csList.tripFuelAmount + csList.requestedAdvance}"/></td>
                        </tr>
                        <%
                   index++;
                     sno++;
                        %>

                    </c:forEach>

                </table>
            </c:if>

            <br/>
            <br/>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <br/>
            <br/>
            <br/> 
            <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table",0);
        </script>
            <script>          
             function getVehicleNos(){
            //onkeypress='getList(sno,this.id)'
            var oTextbox = new AutoSuggestControl(document.getElementById("vehicleNo"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
            }
            </script>          

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>