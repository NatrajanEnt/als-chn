<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

    </head>
    <script language="javascript">
        function submitPage(){
            if(textValidation(document.approve.paidamt,'Paid Amount')){
                return;
            }
            $("#saveButton").hide();
            document.approve.action = '/throttle/saveTripAmount.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>


    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
                        request.setAttribute("menuPath", "Trip >> Payment");
                        String status = request.getParameter("status");
                        String tripid = request.getParameter("tripid");
                        String tripCode = request.getParameter("tripCode");
                        String customerName = request.getParameter("customerName");
                        String tripday = request.getParameter("tripday");
                        String advicedate = request.getParameter("advicedate");
                        String estimatedadvance = request.getParameter("estimatedadvance");
                        String requestedadvance = request.getParameter("requestedadvance");
                        String cnoteName = request.getParameter("cnoteName");
                        String vehicleTypeName = request.getParameter("vehicleTypeName");
                        String routeName = request.getParameter("routeName");
                        String driverName = request.getParameter("driverName");
                        String planneddate = request.getParameter("planneddate");
                        String estimatedexpense = request.getParameter("estimatedexpense");
                        String actualadvancepaid = request.getParameter("actualadvancepaid");
                        String vegno = request.getParameter("vegno");
                        String tripAdvaceId = request.getParameter("tripAdvaceId");

            %>
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30">
                        <div class="contenthead">Trip Payment</div></td>
                </tr>


                <tr>
                    <td class="text2" height="30">Cnote Name</td>
                    <td class="text2" height="30"><%=cnoteName%></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Vehicle Type</td>
                    <td class="text1" height="30"><%=vehicleTypeName%></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Vehicle No</td>
                    <td class="text2" height="30"><%=vegno%></td>
                    <input type="hidden" name="tripCodeEmail" value='<%=tripCode%>'/>
                    <input type="hidden" name="vehicleNoEmail" value='<%=vegno%>'/>
                    <input type="hidden" name="routeInfoEmail" value='<%=routeName%>'/>
                    <input type="hidden" name="customerNameEmail" value='<%=customerName%>'/>
                    <input type="hidden" name="cNotesEmail" value='<%=cnoteName%>'/>
                    <input type="hidden" name="dateval" value='<c:out value="${dateval}"/>'/>
                    <input type="hidden" name="type" value='<c:out value="${type}"/>'/>
                    <input type="hidden" name="active" value='<c:out value="${active}"/>'/>
                    <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>'/>
                </tr>
                <tr>
                    <td class="text1" height="30">Route Name</td>
                    <td class="text1" height="30"><%=routeName%></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Driver Name</td>
                    <td class="text2" height="30"><%=driverName%></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Planned Date</td>
                    <td class="text1" height="30"><%=planneddate%></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Actual Advance Paid</td>
                    <td class="text2" height="30"><%=actualadvancepaid%></td>
                </tr>

                <tr>
                    <td class="text1" height="30">Estimated Expense</td>
                    <td class="text1" height="30"><%=estimatedexpense%></td>
                </tr>

                <tr>
                    <td class="text2" height="30">Estimated Amount</td>
                    <%if (estimatedadvance.equals("")) {%>
                    <td class="text2" height="30">0</td>
                    <%} else {%>
                    <td class="text2" height="30"><%=estimatedadvance%></td>
                    <%}%>
                </tr>
                <%
                            String appramt = "";
                            if (status.equals("0")) {
                                appramt = estimatedadvance;
                %>
                <tr>
                    <td class="text1" height="30">To Be Paid </td>
                    <td class="text1" height="30"><%=estimatedadvance%></td>
                </tr>
                <%} else {
                                    appramt = requestedadvance;
                %>
                <tr>
                    <td class="text2" height="30">Approved Amount</td>
                    <td class="text2" height="30"><%=requestedadvance%></td>
                </tr>
                <%}%>
                <tr>
                    <td class="text1" height="30">Trip Day</td>
                    <td class="text1" height="30"><%=tripday%></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Paid Amt.</td>
                    <td class="text2" height="30"><input name="paidamt" id="paidamt" type="text" class="form-control" value="" onchange="checkamt()"></td>
                </tr>

                <input type="hidden" name="paidstatus" value="Y"/>
                <input name="tripday" type="hidden" class="form-control" value="<%=tripday%>" readonly>
                <input type="hidden" name="tripid" value="<%=tripid%>"/>
                <input type="hidden" name="tripAdvaceId" value="<%=tripAdvaceId%>"/>
                <input type="hidden" name="advicedate" value="<%=advicedate%>"/>
                <input name="tobepaidtoday" id="tobepaidtoday" type="hidden" class="form-control" value="<%=appramt%>" readonly>


            </table>
            <br>
            <center>
                <input type="button" value="save" class="button" id="saveButton" onClick="submitPage();">
                &emsp;
                <!--                <input type="reset" class="button" value="Clear">-->
            </center>


            <script>
                function checkamt(){
                    var reqvalue=document.getElementById("tobepaidtoday").value;                    
                    var paidamt=document.getElementById("paidamt").value;                    
                    var reqpnt = parseInt(reqvalue) * (10/100);
                    var allowvalue = parseInt(reqvalue)+parseInt(reqpnt);
                    
                    if( parseInt(paidamt) > parseInt(allowvalue)){
                        alert("You can pay only 10% max on the request amount.");
                        document.approve.paidamt.value=0;
                        document.getElementById("mySubmit").style.display = "none";
                        document.approve.advancerequestamt.focus();
                    }else{
                        document.getElementById("mySubmit").style.display = "block";
                    }

                }
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
