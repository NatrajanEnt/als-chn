<%--
    Document   : driverSettlementReport
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
   <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">


            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user

                $('#primaryDriver').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getSecondaryDriverName.do",
                            dataType: "json",
                            data: {
                                driverName: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                var primaryDriver = $('#primaryDriver').val();
                                if(items == '' && primaryDriver != ''){
                                    alert("Invalid Primary Driver Name");
                                    $('#primaryDriver').val('');
                                    $('#primaryDriverId').val('');
                                    $('#primaryDriver').focus();
                                }else{
                                }
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var id = ui.item.Id;
                        $('#primaryDriver').val(value);
                        $('#primaryDriverId').val(id);
                        $('#secondaryDriverOne').focus();
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
                };
                });


                function submitPage(value) {
                   if(value == "search"){
                            document.driverSettlement.action = '/throttle/secondaryDriverSettlement.do';
                            document.driverSettlement.submit();
                    }
                    else{
                            document.driverSettlement.action = '/throttle/saveSecondaryDriverSettlement.do';
                            document.driverSettlement.submit();
                    }
                }

                function setValues(){
                     if('<%=request.getAttribute("primaryDriverId")%>' != 'null'){
                    document.getElementById('primaryDriverId').value='<%=request.getAttribute("primaryDriverId")%>';
                }
                     if('<%=request.getAttribute("primaryDriver")%>' != 'null'){
                    document.getElementById('primaryDriver').value='<%=request.getAttribute("primaryDriver")%>';
                }
                     if('<%=request.getAttribute("fromDate")%>' != 'null'){
                    document.getElementById('fromDate').value='<%=request.getAttribute("fromDate")%>';
                }
                     if('<%=request.getAttribute("toDate")%>' != 'null'){
                    document.getElementById('toDate').value='<%=request.getAttribute("toDate")%>';
                }
                }

        function viewTripDetails(tripId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId='+tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
         function viewVehicleDetails(vehicleId) {
                window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }

        </script>


    </head>
    <body onload="setValues();">
        <form name="driverSettlement" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Secondary Driver Settlement</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="0" border="0" align="center" class="table4" >
                                    <tr>
                                        <td class="text1" align="center" height="30">Fleet Centre</td>
                                        <td class="text1" height="30">
                                         <c:if test="${getSecondaryFleet!= null}">
                                                 <select name="secondaryFleet" id="secondaryFleet" class="textbox" onchange="fleetDriver();" >
                                <option value='0~~0~~~'>--select--</option>
                                <c:forEach items="${getSecondaryFleet}" var="fleet">
                                  <option value='<c:out value="${fleet.name}"/>'> <c:out value="${fleet.name}"/> </option>
                                </c:forEach>
                                  
                               
                            </select>
                                            </c:if>
                                        </td>
                                            
                                            
                                            <script type="text/javascript">
                                                 function fleetDriver() {
                                    var secondaryFleet = $("#secondaryFleet").val();
                                    $.ajax({
                                        url: "/throttle/selectSecFleetDriver.do",
                                        dataType: "json",
                                        data: {
                                            secondaryFleet: secondaryFleet
                                            
                                        },
                                        success: function(data) {
                                            //alert(data);
                                            if (data != '') {
                                                $('#primaryDriver').empty();
                                                $('#primaryDriver').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                                $.each(data, function(i, data) {
    //                                                alert(data.Id);
                                                    $('#primaryDriver').append(
                                                            $('<option style="width:90px"></option>').attr("value",data.Id).text(data.Name)
                                                            )
                                                });
                                            }else{
                                                $('#primaryDriver').empty();
                                                $('#primaryDriver').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                            }
                                        }
                                    });
                                }

                                               
                                           
                            </script>

                                    
                                        <td class="text1" align="center" height="30">Primary Driver Name</td>
                                        <td class="text1" height="30">
                                              <select name="primaryDriver" id="primaryDriver" class="textbox" >
                                                <option value=''> </option>
                                            </select>
                                 <!--           <input type="text" class="textbox" id="primaryDriver"  name="primaryDriver" autocomplete="off" value="<c:out value="${primaryDriverName}"/>"/>
                                              -->
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text1" align="center" height="30"> <font color="red">*</font>From Date</td>
                                        <td class="text1" height="30"><input type="text" class="datepicker" id="fromDate" name="fromDate" autocomplete="off" value="<c:out value="${fromDate}"/>"/></td>
                                        <td class="text1" align="center" height="30"> <font color="red">*</font>To Date</td>
                                        <td class="text1" height="30"><input type="text" class="datepicker" id="toDate"  name="toDate" autocomplete="off" value="<c:out value="${toDate}"/>"/></td>
                                    </tr>

                                    <tr>

                                        <td class="text1" height="30" colspan="2" align="center"></td>
                                        <td class="text1" height="30"  align="center"><input type="button" class="button" value="Search" name="search" onClick="submitPage(this.name)"/></td>

                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
                                      <c:if test = "${tripClosureDetailsSize != '0'}" >
        <div id="tabs" >
                <ul>

                    <li><a href="#tripDetails"><span>Trip Details</span></a></li>
                    <li><a href="#expense"><span>Expense Details </span></a></li>
                      <li><a href="#driverAdvance"><span>Driver Advance</span></a></li>
                      <li><a href="#summary"><span>summary</span></a></li>
                </ul>
            <br>
             <div id="tripDetails">

            <c:if test = "${tripClosureDetails != null}" >
                <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                           <td class="contenthead" align="center">S.No</td>
                           <td class="contenthead" align="center">Trip Code</td>
                           <td class="contenthead" align="center">Vehicle No</td>
                           <td class="contenthead" align="center">Customer Name</td>
                           <td class="contenthead" align="center">Driver Name</td>
                           <td class="contenthead" align="center">Route Name</td>
                           <td class="contenthead" align="center">Start Date</td>
                           <td class="contenthead" align="center">End Date</td>
                           <td class="contenthead" align="center">Total Run Km</td>
                           <td class="contenthead" align="center">Total Hours</td>
                           <td class="contenthead" align="center">Total Days</td>
                           <td class="contenthead"  align="center">Total Weight</td>
                           <td class="contenthead"  align="center">Estimated Expense</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${tripClosureDetails}" var="closure">

                             <c:set var="totalEstimatedExpense" value="${totalEstimatedExpense + closure.rcmExpense}"></c:set>
                             <c:set var="totalRunKms" value="${totalRunKms + closure.totalKmRun}"></c:set>
                             <c:set var="totalRunHours" value="${totalRunHours + closure.totalReeferRun}"></c:set>
                             <c:set var="totalDays" value="${totalDays + closure.totalDays}"></c:set>
                             <c:set var="totalFuelConsumed" value="${totalFuelConsumed + closure.fuelConsumption}"></c:set>
                             <c:set var="driverId" value="${closure.primaryDriverId}"></c:set>
                             <c:set var="primaryDriverName" value="${closure.primaryDriverName}"></c:set>
                         
                             <c:set var="tripBalance" value="${closure.advanceAmount - closure.totalExpense }"></c:set>


                              <input type="hidden" name="tripBalance" id="tripBalance" value="<c:out value="${tripBalance}"/>"/>
                              <input type="hidden" name="driverId" id="driverId" value="<c:out value="${driverId}"/>"/>
                              <input type="hidden" name="totalRcmAllocation" id="totalRcmAllocation" value="<c:out value="${totalEstimatedExpense}"/>"/>

                              <input type="hidden" name="tripId" id="tripId" value="<c:out value="${closure.tripId}"/>"/>
                               <input type="hidden" name="fuelPrice" id="fuelPrice" value="<c:out value="${closure.fuelCost}"/>"/>
                               <input type="hidden" name="runKMs" id="runKM" value="<c:out value="${closure.totalKmRun}"/>"/>
                               <input type="hidden" name="runHours" id="runHours" value="<c:out value="${closure.totalReeferRun}"/>"/>
                               <input type="hidden" name="fuelConsumption" id="fuelConsumption" value="<c:out value="${closure.fuelConsumption}"/>"/>
                               <input type="hidden" name="rcmAllocation" id="rcmAllocation" value="<c:out value="${closure.rcmExpense}"/>"/>
                               <input type="hidden" name="bpclAllocation" id="bpclAllocation" value="<c:out value="${closure.advanceAmount}"/>"/>
                               <input type="hidden" name="extraExpense" id="extraExpense" value="<c:out value="${closure.extraExpense}"/>"/>
                               <input type="hidden" name="totalMiscellaneous" id="totalMiscellaneous" value="<c:out value="${closure.miscExpense}"/>"/>
                               <input type="hidden" name="driverBhatta" id="driverBhatta" value="<c:out value="${closure.driverBatta}"/>"/>
                               <input type="hidden" name="totalExpense" id="totalExpense" value="<c:out value="${closure.totalExpense}"/>"/>


                           <%
                                        String classText3 = "";
                                        int oddEven = sno % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                            <tr height="30">

                                <td class="<%=classText3%>" align="center"><%=sno%></td>
                                <td class="<%=classText3%>" align="left">
                                    <a href="" onclick="viewTripDetails('<c:out value="${closure.tripId}"/>');"><c:out value="${closure.tripCode}"/></a></td>
                                <td class="<%=classText3%>" align="left">
                                       <a href="" onclick="viewVehicleDetails('<c:out value="${closure.vehicleId}"/>')"><c:out value="${closure.regNo}"/></a></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.customerName}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.primaryDriverName}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.routeInfo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.startDate}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.endDate}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.totalKmRun}"/></td>
                                <td class="<%=classText3%>"  align="left"><c:out value="${closure.totalHours}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.totalDays}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.totalWeight}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${closure.rcmExpense}"/></td>

                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>

                </table>



                <br/>
                <br/>

            </c:if>
                   <center>
                        <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>

             </div>
          <div id="expense">
                <c:if test = "${tripClosureDetails != null}" >
              <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                           <td class="contenthead" align="center">S.No</td>
                           <td class="contenthead" align="center">Trip Code</td>
                           <td class="contenthead" align="center">Vehicle No</td>
                           <td align="center" class="contenthead" >Route Name</td>
                           <td align="center" class="contenthead" >Fuel Expense</td>
                           <td align="center" class="contenthead" >Toll Expense</td>
                           <td align="center" class="contenthead" >Add Toll Expense</td>
                           <td align="center" class="contenthead" >Misc Expense</td>
                           <td align="center" class="contenthead" >Parking</td>
                           <td align="center" class="contenthead" >Pre-Cooling</td>
                           <td align="center" class="contenthead" >Extra Expense</td>
                           <td align="center" class="contenthead" >System Expense(Fuel + Toll + Add Toll +<br>Misc + Parking + Pre-Cooling)</td>
                           <td class="contenthead"  align="center">Total Expense</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${tripClosureDetails}" var="expense">
                                <c:set var="totalFuelExpense" value="${totalFuelExpense + expense.fuelExpense}"></c:set>
                                <c:set var="totalTollAmount" value="${totalTollAmount + expense.tollAmount}"></c:set>
                                <c:set var="addTotalTollAmount" value="${addTotalTollAmount + expense.additionalTollCost}"></c:set>
                                <c:set var="totalMiscExpense" value="${totalMiscExpense + expense.miscExpense}"></c:set>
                                <c:set var="totalParkingExpense" value="${totalParkingExpense + expense.parkingCost}"></c:set>
                                <c:set var="totalPreCoolingExpense" value="${totalPreCoolingExpense + expense.preCollingCost}"></c:set>
                                <c:set var="totalSystemExpense" value="${totalSystemExpense + expense.systemExpense}"></c:set>
                                <c:set var="totalExtraExpense" value="${totalExtraExpense + expense.extraExpense}"></c:set>
                                <c:set var="totalExpenses" value="${totalExpenses + expense.totalExpense}"></c:set>


                                 <input type="hidden" name="totalFuelExpense" id="totalFuelExpense" value="<c:out value="${totalFuelExpense}"/>"/>
                                 <input type="hidden" name="totalTollAmount" id="totalTollAmount" value="<c:out value="${totalTollAmount}"/>"/>
                                 <input type="hidden" name="addTotalTollAmount" id="addTotalTollAmount" value="<c:out value="${addTotalTollAmount}"/>"/>
                                 <input type="hidden" name="totalMiscExpense" id="totalMiscExpense" value="<c:out value="${totalMiscExpense}"/>"/>
                                 <input type="hidden" name="totalParkingExpense" id="totalParkingExpense" value="<c:out value="${totalParkingExpense}"/>"/>
                                 <input type="hidden" name="totalPreCoolingExpense" id="totalPreCoolingExpense" value="<c:out value="${totalPreCoolingExpense}"/>"/>
                                 <input type="hidden" name="totalSystemExpense" id="totalSystemExpense" value="<c:out value="${totalSystemExpense}"/>"/>
                                 <input type="hidden" name="totalExtraExpense" id="totalExtraExpense" value="<c:out value="${totalExtraExpense}"/>"/>
                                 <input type="hidden" name="totalExpenses" id="totalExpenses" value="<c:out value="${totalExpenses}"/>"/>


                             <%
                                        String classText3 = "";
                                        int oddEven = sno % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                            <tr height="30">
                                 <td class="<%=classText3%>" align="center"><%=sno%></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.tripCode}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.regNo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.routeInfo}"/></td>

                                <td class="<%=classText3%>" align="left"><c:out value="${expense.fuelExpense}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.tollAmount}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.additionalTollCost}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.miscExpense}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.parkingCost}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.preCollingCost}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.extraExpense}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.systemExpense}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${expense.totalExpense}"/></td>


                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>
                             <tr height="30">
                                 <td  align="center" colspan="4">Total Expense</td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalFuelExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalTollAmount}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${addTotalTollAmount}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalMiscExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalParkingExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalPreCoolingExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalExtraExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalSystemExpense}"/></td>
                                <td  align="left"><fmt:formatNumber pattern="##0.00" value="${totalExpenses}"/></td>


                            </tr>
                </c:if>
                </table>
              <br/>
              <br/>
               <center>
                        <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
          </div>
          <div id="driverAdvance">
                <c:if test = "${tripClosureDetails != null}" >
              <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr height="45" >
                            <td  class="contenthead"  align="center">S.No</td>
                           <td class="contenthead"  align="center">Trip Code</td>
                           <td class="contenthead"  align="center">Vehicle No</td>
                           <td class="contenthead"  align="center">Route Name</td>
                           <td class="contenthead"  align="center">Driver Name</td>
                           <td class="contenthead"  align="center">Advance Amount</td>
                        </tr>
                        <% int index = 0, sno = 1;%>
                        <c:forEach items="${tripClosureDetails}" var="advance">
                              <c:set var="totalAdvanceAmount" value="${ totalAdvanceAmount + advance.advanceAmount}"></c:set>
                             <input type="hidden" name="totalAdvanceAmount" id="totalAdvanceAmount" value="<c:out value="${totalAdvanceAmount}"/>"
                              <%
                                        String classText3 = "";
                                        int oddEven = sno % 2;
                                        if (oddEven > 0) {
                                            classText3 = "text1";
                                        } else {
                                            classText3 = "text2";
                                        }
                            %>
                            <tr height="30">
                                 <td class="<%=classText3%>" align="center"><%=sno%></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.tripCode}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.regNo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.routeInfo}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.primaryDriverName}"/></td>
                                <td class="<%=classText3%>" align="left"><c:out value="${advance.advanceAmount}"/></td>

                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>
                        </c:forEach>
                            <tr height="30">
                                <td  align="center">&nbsp;</td>
                                <td  align="left">&nbsp;</td>
                                <td  align="left">&nbsp;</td>
                                <td  align="left">&nbsp;</td>
                                <td  align="left">Total Advance Paid</td>
                                <td  align="left"><c:out value="${totalAdvanceAmount}"/></td>

                            </tr>
                </c:if>

                </table>
              <br/>
              <br/>
               <center>
                        <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
          </div>
          
            <div id="summary">

                 <table  border="1" class="border" align="center" width="800px" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Driver Settlement Details
                            </td>
                        </tr>


                        <tr height="25">
                            <td class="text1">Driver Name</td>
                            <td class="text1"><label><c:out value="${primaryDriverName}"/></label></td>
                            <td class="text1">Total No of Trips</td>
                            <td class="text1"><label ><c:out value="${tripClosureDetailsSize}"/></label></td>
                        </tr>

                        <tr height="25">
                            <td class="text2">Total Run Kms</td>
                            <td class="text2"><label><c:out value="${totalRunKms}"/></label></td>
                            <td class="text2">Total Run Hours</td>
                            <td class="text2"><label><c:out value="${totalRunHours}"/></label></td>
                        </tr>



                        <tr height="25">
                            <td class="text1">Total No of Days</td>
                            <td class="text1"><label><c:out value="${totalDays}"/></label></td>
                            <td class="text1" colspan="2"> &nbsp;</td>
                        </tr>

                        <tr height="25">
                            <td class="text2">Toll Amount </td>
                            <td class="text2"><label><c:out value="${totalTollAmount}"/></label></td>
                            <td class="text2">Total Misc Cost</td>
                            <td class="text2"><label><c:out value="${totalMiscExpense}"/></label></td>
                        </tr>

                       <%-- <tr height="25">
                            <td class="text1">Total Driver Bhatta </td>
                            <td class="text1"><label><c:out value="${totalDriverBatta}"/></label></td>
                            <td class="text1">Total Driver Incentives </td>
                            <td class="text1"><label><c:out value="${totalDriverIncentive}"/></label></td>
                        </tr>
                        --%>
                        <tr height="25">
                            <td class="text2">RCM</td>
                            <td class="text2"><label><c:out value="${totalEstimatedExpense}"/></label></td>
                            <td class="text2" colspan="2"> &nbsp;</td>
                        </tr>

                        <tr height="25">
                            <td class="text1">Total Extra Expense </td>
                            <td class="text1"><label><c:out value="${totalExtraExpense}"/></label></td>
                            <td class="text1">Total System Expense </td>
                            <td class="text1"><label><c:out value="${totalSystemExpense}"/></label></td>
                        </tr>

                        <tr height="25">
                            <td class="text2">Total Fuel Consumed </td>
                            <td class="text2"><label><fmt:formatNumber pattern="##0.00" value="${totalFuelConsumed}"/></label></td>
                            <td class="text2">Total Fuel Expense </td>
                            <td class="text2"><label><fmt:formatNumber pattern="##0.00" value="${totalFuelExpense}"/></label></td>
                        </tr>


                        <tr height="25">
                            <td class="text1">Total Advance Paid </td>
                            <td class="text1"><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAdvanceAmount}" /></label></td>
                            <td class="text1" colspan="2"> &nbsp;</td>

                        </tr>

                        <tr height="25">
                            <td class="text2">Actual Expenses </td>
                            <td class="text2"><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalExpenses}" /></label></td>
                            <td class="text2" colspan="2"> &nbsp; </td>

                        </tr>
                           <c:set var="endingBalance" value="${totalAdvanceAmount - totalExpenses+startingBalance}"></c:set>
                           <c:set var="balanceAmount" value="${totalAdvanceAmount - totalExpenses+startingBalance}"></c:set>
                        <tr height="25">
                            <td class="text1">Starting Balance </td>
                            <td class="text1"><input type="hidden" name="startingBalance" id="startingBalance" value="<fmt:formatNumber pattern="##0.00" value="${startingBalance}" />"/><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${startingBalance}" /></label></td>
                            <td class="text1">Ending Balance</td>
                            <td class="text1"><input type="hidden" name="endingBalance" id="endingBalance" value="<fmt:formatNumber pattern="##0.00" value="${totalAdvanceAmount - totalExpenses+startingBalance}" />"/><label><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAdvanceAmount - totalExpenses+startingBalance}" /></label></td>

                        </tr>
                        


                        <tr height="25">
                            <td class="text2">Balance Amount </td>
                            <td class="text2"><input type="hidden" name="balanceAmount" id="balanceAmount" value="<fmt:formatNumber pattern="##0.00" value="${totalAdvanceAmount - totalExpenses}" />"/><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalAdvanceAmount - totalExpenses}" /><label></label></td>
                            <td class="text2" colspan="2"> &nbsp; </td>
                        </tr>

                        <c:if test = "${balanceAmount >  10000 }" >
                            <tr>
                                <td class="text1">Pay Amount</td>
                                <td class="text1"><input type="text" name="payAmount" readonly value="<fmt:formatNumber pattern="##0.00" value="${balanceAmount}" />" /></td>
                                <td class="text1">Pay Mode</td>
                                <td class="text1">
                                    <select name="paymentMode" id="paymentMode">
                                        <option value="Carry To Salary">Carry Forward to Salary</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Account Deposit">Deposit to Account</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text1">Carry Forward Amount</td>
                                <td class="text1"><input type="text" name="cfAmount" readonly value="10000" /></td>
                                <td class="text1">Pay Mode</td>
                                <td class="text1">
                                    <select name="paymentMode" id="paymentMode">
                                        <option value="cfs">Carry Forward to Settlement</option>
                                    </select>
                                </td>
                            </tr>
                        </c:if>
                        <c:if test = "${balanceAmount <  0 && ((balanceAmount * -1) >  10000)}" >
                            <tr>
                                <td class="text1">Pay Amount 3</td>
                                <td class="text1"><input type="text" name="payAmount" readonly value="<fmt:formatNumber pattern="##0.00" value="${balanceAmount}" />" /></td>
                                <td class="text1">Pay Mode</td>
                                <td class="text1">
                                    <select name="paymentMode" id="paymentMode">
                                        <option value="Carry To Salary">Carry Forward to Salary</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Account Deposit">Deposit to Account</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="text1">Carry Forward Amount</td>
                                <td class="text1"><input type="text" name="cfAmount" readonly value="10000" /></td>
                                <td class="text1">Pay Mode</td>
                                <td class="text1">
                                    <select name="paymentMode" id="paymentMode">
                                        <option value="cfs">Carry Forward to Settlement</option>
                                    </select>
                                </td>
                            </tr>
                            <input type="hidden" name="cfAmount" readonly value="0" />
                        </c:if>
                        <c:if test = "${ (balanceAmount <  10000 && (balanceAmount < 0))}" >
                            <tr>
                                <td class="text1">Pay Amount 4</td>
                                <td class="text1"><input type="text" name="payAmount" readonly value="<fmt:formatNumber pattern="##0.00" value="${balanceAmount}" />" /></td>
                                <td class="text1">Pay Mode</td>
                                <td class="text1">
                                    <select name="paymentMode" id="paymentMode">
                                        <option value="Carry To Salary">Carry Forward to Salary</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Account Deposit">Deposit to Account</option>
                                    </select>
                                </td>
                            </tr>
                            <input type="hidden" name="cfAmount" readonly value="0" />
                        </c:if>
                        <c:if test = "${ ((balanceAmount <  10000) && (balanceAmount > 0))}" >
                            <tr>
                                <td class="text1">Pay Amount 5</td>
                                <td class="text1"><input type="text" name="payAmount" readonly value="<fmt:formatNumber pattern="##0.00" value="${balanceAmount}" />" /></td>
                                <td class="text1">Pay Mode</td>
                                <td class="text1">
                                    <select name="paymentMode" id="paymentMode">
                                        <option value="Carry To Salary">Carry Forward to Salary</option>
                                        <option value="Cash">Cash</option>
                                        <option value="Account Deposit">Deposit to Account</option>
                                    </select>
                                </td>
                            </tr>
                            <input type="hidden" name="cfAmount" readonly value="0" />
                        </c:if>
                  
                        
                        <tr height="25">
                            <td class="text1">Remarks for Extra Expenses</td>
                            <td class="text1" ><textarea name="settlementRemarks" id="settlementRemarks" cols="40" rows=""></textarea></td>
                        </tr>
                    </table>
                <br/>
                <br/>
                     <center>
                            <input type="button" class="button" name="save" id="save" value="save" onclick="submitPage();"/>
                        </center>

                <br/>
                <br/>


             </div>
               </div>
                                      </c:if>
                 <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>