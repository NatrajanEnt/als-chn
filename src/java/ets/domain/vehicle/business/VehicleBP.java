/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.vehicle.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.vehicle.data.VehicleDAO;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class VehicleBP {

    public VehicleBP() {
    }
    VehicleDAO vehicleDAO;

    public VehicleDAO getVehicleDAO() {
        return vehicleDAO;
    }

    public void setVehicleDAO(VehicleDAO vehicleDAO) {
        this.vehicleDAO = vehicleDAO;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetMfrList() throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = vehicleDAO.getMfrList();
//        if (MfrList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return MfrList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processActiveMfrList() throws FPRuntimeException, FPBusinessException {
        ArrayList ModelList = new ArrayList();
        ArrayList activeModelList = new ArrayList();
        VehicleTO vehicleTO = null;
        ModelList = vehicleDAO.getMfrList();
        Iterator itr = ModelList.iterator();
        while (itr.hasNext()) {
            vehicleTO = new VehicleTO();
            vehicleTO = (VehicleTO) itr.next();
            if (vehicleTO.getActiveInd().equalsIgnoreCase("y")) {
                activeModelList.add(vehicleTO);
            }
        }
        return activeModelList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetModelList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList MfrList = new ArrayList();
        MfrList = vehicleDAO.getModelList(vehicleTO);
        return MfrList;
    }

    public ArrayList processActiveModelList() throws FPRuntimeException, FPBusinessException {
        ArrayList modelList = new ArrayList();
        ArrayList activeModelList = new ArrayList();
        VehicleTO vehicleTO = new VehicleTO();
        vehicleTO.setFleetTypeId("1");
        modelList = vehicleDAO.getModelList(vehicleTO);
        Iterator itr = modelList.iterator();
        while (itr.hasNext()) {
            vehicleTO = new VehicleTO();
            vehicleTO = (VehicleTO) itr.next();
            if (vehicleTO.getActiveInd().equalsIgnoreCase("y")) {
                activeModelList.add(vehicleTO);
            }
        }
        return activeModelList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetTypeList(VehicleTO vehicleTo) throws FPRuntimeException, FPBusinessException {
        ArrayList TypeList = new ArrayList();
        TypeList = vehicleDAO.getTypeList(vehicleTo);
        System.out.println("TypeList size bp= " + TypeList.size());
//        if (TypeList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return TypeList;
    }

    public ArrayList processActiveTypeList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList typeList = new ArrayList();
        typeList = vehicleDAO.getTypeList(vehicleTO);
        return typeList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetFuelList() throws FPRuntimeException, FPBusinessException {

        ArrayList FuelList = new ArrayList();
        FuelList = vehicleDAO.getFuelList();
//        if (FuelList.size() == 0) {
//            throw new FPBusinessException("EM-MFR-01");
//        }
        return FuelList;
    }

    /**
     * This method used to Add MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    /*   public ArrayList addDesignation() throws FPRuntimeException, FPBusinessException {
     ArrayList DesignaList = new ArrayList();
     DesignaList = designationDAO.getDesignaList();
     return DesignaList;
     }
    
     /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processInsertMfrDetails(VehicleTO vehicleTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.doMfrDetails(vehicleTO, UserId);
//        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-MFR-02");
//        }
        return insertStatus;
    }

    public int saveResetVehicleKM(VehicleTO vehicleTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveResetVehicleKM(vehicleTO, UserId);
//        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-MFR-02");
//        }
        return insertStatus;
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processModifyMfrDetails(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.doMfrDetailsModify(List, UserId);
//        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-MFR-02");
//        }
        return insertStatus;
    }

    public int insertModelDetails(VehicleTO vehicleTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.insertModelDetails(vehicleTO, UserId);
        return insertStatus;

    }

    /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processInsertTypeDetails(VehicleTO vehicleTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.doTypeDetails(vehicleTO, UserId);
//        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-MFR-02");
//        }
        return insertStatus;
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processModifyTypeDetails(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.doTypeDetailsModify(List, UserId);
//        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-MFR-02");
//        }
        return insertStatus;
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processModifyModelDetails(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.doModelDetailsModify(List, UserId);
//        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-MFR-02");
//        }
        return insertStatus;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetUsageList() throws FPRuntimeException, FPBusinessException {

        ArrayList usageList = new ArrayList();
        usageList = vehicleDAO.getUsageList();
//        if (usageList.size() == 0) {
//            throw new FPBusinessException("EM-MFR-01");
//        }
        return usageList;
    }

    public ArrayList processActiveUsageList() throws FPRuntimeException, FPBusinessException {

        ArrayList usageList = new ArrayList();
        ArrayList activeUsageList = new ArrayList();
        VehicleTO vehicleTO = null;
        usageList = vehicleDAO.getUsageList();

        Iterator itr = usageList.iterator();
        while (itr.hasNext()) {
            vehicleTO = new VehicleTO();
            vehicleTO = (VehicleTO) itr.next();
            if (vehicleTO.getActiveInd().equalsIgnoreCase("y")) {
                activeUsageList.add(vehicleTO);
            }
        }
        return activeUsageList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetClassList() throws FPRuntimeException, FPBusinessException {

        ArrayList usageList = new ArrayList();
        usageList = vehicleDAO.getClassList();
//        if (usageList.size() == 0) {
//            throw new FPBusinessException("EM-MFR-01");
//        }
        return usageList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetopList() throws FPRuntimeException, FPBusinessException {

        ArrayList operationPointList = new ArrayList();
        operationPointList = vehicleDAO.getopList();
//        if (operationPointList.size() == 0) {
//            throw new FPBusinessException("EM-MFR-01");
//        }
        return operationPointList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetspList() throws FPRuntimeException, FPBusinessException {

        ArrayList servicePointList = new ArrayList();
        servicePointList = vehicleDAO.getspList();
//        if (servicePointList.size() == 0) {
//            throw new FPBusinessException("EM-MFR-01");
//        }
        return servicePointList;
    }

    /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processAddVehicleDetails(VehicleTO vehicleTO, int UserId, int companyId) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        int vehicle_id = 0;
        String vehicleDetails = vehicleDAO.doAddVehicleDetails(vehicleTO, UserId,companyId);
        String[] temp = vehicleDetails.split("~");
        vehicle_id = Integer.parseInt(temp[0]);
        if (vehicle_id == 0) {
            throw new FPBusinessException("EM-VEHICLE-02");
        }
        vehicleTO.setVehicleId(String.valueOf(vehicle_id));

        insertStatus = vehicleDAO.doInsertVehicleClass(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-03");
        }

        insertStatus = vehicleDAO.doInsertVehicleEngine(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-04");
        }

        insertStatus = vehicleDAO.doInsertVehicleOper(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-05");
        }
        insertStatus = vehicleDAO.doInsertVehicleFc(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-06");
        }

        insertStatus = vehicleDAO.doInsertVehicleReg(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-07");
        }

        insertStatus = vehicleDAO.doInsertVehicleUsage(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-08");
        }
        insertStatus = vehicleDAO.doInsertVehicleCust(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-09");
        }
        System.out.println("checktest");
        insertStatus = vehicleDAO.doInsertVehicleHist(vehicleTO, UserId);
        if (vehicleTO.getItemIds() != null) {
            System.out.println("new2");
            if (!vehicleTO.getItemIds()[0].equalsIgnoreCase("0")) {
                System.out.println("new3");
                insertStatus = vehicleDAO.doInsertVehicleTyre(vehicleTO, UserId, companyId);
            }
        }
        System.out.println("check1:");
        return vehicle_id;
    }

    /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processUpdateVehicleDetails(VehicleTO vehicleTO, int UserId,int companyId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.doAlterVehicleDetails(vehicleTO, UserId,companyId);
        insertStatus = vehicleDAO.doInsertVehicleClass(vehicleTO, UserId);
        insertStatus = vehicleDAO.doInsertVehicleEngine(vehicleTO, UserId);
//        insertStatus = vehicleDAO.doInsertVehicleOper(vehicleTO, UserId);
        insertStatus = vehicleDAO.doInsertVehicleFc(vehicleTO, UserId);
        insertStatus = vehicleDAO.doInsertVehicleReg(vehicleTO, UserId);
        insertStatus = vehicleDAO.doInsertVehicleUsage(vehicleTO, UserId);
        insertStatus = vehicleDAO.doInsertVehicleCust(vehicleTO, UserId);
        insertStatus = vehicleDAO.doInsertVehicleHist(vehicleTO, UserId);
        if (vehicleTO.getItemIds() != null) {
            insertStatus = vehicleDAO.doAlterVehicleTyre(vehicleTO, companyId, UserId);
        }

        if (insertStatus == 0) {
            throw new FPBusinessException("EM-MFR-02");
        }
        return insertStatus;
    }

    /**
     * This method used to Get vehicle List
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processVehicleList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleList = new ArrayList();
        vehicleList = vehicleDAO.getVehicleList(vehicleTO);

        return vehicleList;
    }

    public ArrayList processVehicleDetail(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleList = new ArrayList();
        vehicleList = vehicleDAO.getVehicleDetail(vehicleTO);

        return vehicleList;
    }

    public ArrayList processTyreItems() throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleList = new ArrayList();
        vehicleList = vehicleDAO.getTyreItems();
        return vehicleList;
    }

    public ArrayList getGroupList() throws FPRuntimeException, FPBusinessException {

        ArrayList groupList = new ArrayList();
        groupList = vehicleDAO.getGroupList();
        return groupList;
    }

    public String processAvailableTyres(int itemId, int companyId) throws FPRuntimeException, FPBusinessException {
        String tyres = "";
        Iterator itr;
        int cntr = 0;
        ArrayList tyreList = new ArrayList();
        VehicleTO veh = new VehicleTO();
        tyreList = vehicleDAO.getAvailableTyres(itemId);
        if (tyreList.size() == 0) {
            return tyres;
        } else {
            itr = tyreList.iterator();
            while (itr.hasNext()) {
                veh = new VehicleTO();
                veh = (VehicleTO) itr.next();
                if (cntr == 0) {
                    tyres = veh.getTyreId() + "~" + veh.getTyreNo();
                    cntr++;
                } else {
                    tyres = tyres + "^" + veh.getTyreId() + "~" + veh.getTyreNo();
                }
            }
        }
        return tyres;
    }

    public ArrayList processTyrePosition(int vehicleId) throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = vehicleDAO.getVehicleTyreDetails(vehicleId);
        if (MfrList.size() == 0) {
            //throw new FPBusinessException("EM-VEHICLE-01");
        }
        return MfrList;
    }

    public ArrayList getTyrePostions() {
        ArrayList itemList = new ArrayList();
        itemList = vehicleDAO.getTyrePostions();
        if (itemList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-04");
        }
        return itemList;
    }

    public String checkVehicleTyreNo(String regNo) {
        String mess = "";
        mess = vehicleDAO.checkVehicleTyre(regNo);
        return mess;
    }

    public int vehicleKmUpdate(int vehicleId, String km, String hm, int userId) {
        int stat = 0;
        stat = vehicleDAO.vehicleKmUpdate(vehicleId, km, hm, userId);
        return stat;
    }

    //-----------Vehicle Finance Updates starts here---------------
    public int saveVehicleFinance(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehicleFinance(vehicleTO, companyId, userId);
        return insertStatus;
    }

    public int saveFleetPurchase(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveFleetPurchase(vehicleTO, companyId, userId);
        return insertStatus;
    }

    //-----------Vehicle Insurance Updates starts here---------------
    public String getVehicleDetails(String regNo) {
        System.out.println("..........ajax in BP.....................");
        String vehicleDetails = "";
        vehicleDetails = vehicleDAO.getVehicleDetails(regNo);
        return vehicleDetails;
    }

    //-----------Vehicle Insurance Updates starts here---------------
    public String getVehicleExisting(String regNo, String vPAge) {
        String vehicleDetails = "";
        vehicleDetails = vehicleDAO.getVehicleExisting(regNo, vPAge);
        return vehicleDetails;
    }

    //-----------Vehicle Insurance Updates starts here---------------
    public int saveVehicleInsurance(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehicleInsurance(vehicleTO, companyId, userId);
        return insertStatus;
    }

    //-----------Vehicle AMC Updates starts here---------------
    public int saveVehicleAMC(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehicleAMC(vehicleTO, companyId, userId);
        return insertStatus;
    }

    //-----------Vehicle Accident Updates starts here---------------
    public int saveVehicleAccident(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehicleAccident(vehicleTO, companyId, userId);
        return insertStatus;
    }

    public ArrayList processGetAccidentVehicleList(String regno) throws FPRuntimeException, FPBusinessException {
        ArrayList AccidentVehicleList = new ArrayList();
        AccidentVehicleList = vehicleDAO.getAccidentVehicleList(regno);
        //  if (AccidentVehicleList.size() == 0) {
        //   throw new FPBusinessException("EM-GEN-01");
        //    }
        return AccidentVehicleList;
    }

    public ArrayList getAccidentVehicleDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList AccidentVehicleList = new ArrayList();
        AccidentVehicleList = vehicleDAO.getAccidentVehicleDetails();
        //  if (AccidentVehicleList.size() == 0) {
        //   throw new FPBusinessException("EM-GEN-01");
        //    }
        return AccidentVehicleList;
    }

    public ArrayList processAlterAccidentVehicleList(String regNo) throws FPRuntimeException, FPBusinessException {

        ArrayList AccidentVehicleList = new ArrayList();
        AccidentVehicleList = vehicleDAO.alterAccidentVehicleList(regNo);
        if (AccidentVehicleList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return AccidentVehicleList;
    }

    public int updateVehicleAccident(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.updateVehicleAccident(vehicleTO, companyId, userId);
        return insertStatus;
    }

    //-----------Vehicle Road Tax Updates starts here---------------
    public int saveVehicleRoadTax(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehicleRoadTax(vehicleTO, companyId, userId);
        return insertStatus;
    }

    //-----------Vehicle Fc Details---------------
    public int saveVehicleFc(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehicleFc(vehicleTO, companyId, userId);
        return insertStatus;
    }

    //-----------Vehicle getVehicleProfileDetails starts here---------------
    public ArrayList getVehicleProfileDetails(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList profileDetails = new ArrayList();
        ArrayList profileDetailsNew = new ArrayList();
        VehicleTO vehicleTo = new VehicleTO();
        profileDetails = vehicleDAO.getVehicleProfileDetails(vehicleTO);
        double fcAmount = 0;
        double insuranceAmount = 0;
        double roadTaxAmount = 0;
        double fleetAmount = 0;
        double serviceCost = 0;
        double tripExpenses = 0;
        String fuelDetails = "";
        int tripFuelLiter = 0;
        double tripFuelAmount = 0;
        double tripRunKm = 0;
        Iterator it = profileDetails.iterator();
        while (it.hasNext()) {

            vehicleTo = new VehicleTO();
            vehicleTo = (VehicleTO) it.next();
            fcAmount = vehicleDAO.getFcAmount(vehicleTO);
            insuranceAmount = vehicleDAO.getInsuranceAmount(vehicleTO);
            roadTaxAmount = vehicleDAO.getRoadTaxAmount(vehicleTO);
            fuelDetails = vehicleDAO.getTripFuelDetail(vehicleTO);
            if (fuelDetails != null && fuelDetails.contains("~")) {
                tripFuelLiter = Integer.parseInt(fuelDetails.split("~")[1]);
                tripFuelAmount = Double.parseDouble(fuelDetails.split("~")[2]);
                tripRunKm = Double.parseDouble(fuelDetails.split("~")[0]);
            }
            //   tripExpenses = vehicleDAO.getTripExpenses(vehicleTO);
            serviceCost = getServiceCost(vehicleTO);
            vehicleTo.setFcAmount(fcAmount);
            vehicleTo.setPremiumPaidAmount(insuranceAmount);
            vehicleTo.setRoadTaxPaidAmount(roadTaxAmount);
            vehicleTo.setServiceCost(serviceCost);
            vehicleTo.setFuelAmount(tripFuelAmount);
            vehicleTo.setFuelLiters(tripFuelLiter);
            vehicleTo.setTripRunKm(tripRunKm);
            profileDetailsNew.add(vehicleTo);

        }
        return profileDetailsNew;
    }

    //-----------Vehicle getVehicleProfileTyre starts here---------------
    public ArrayList getVehicleProfileTyre(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList profileDetails = new ArrayList();
        profileDetails = vehicleDAO.getVehicleProfileTyre(vehicleTO);
        return profileDetails;
    }

    //-----------Vehicle getServiceCost---------------
    public double getServiceCost(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        double serviceCost = 0;
        serviceCost = vehicleDAO.getServiceCost(vehicleTO);
        return serviceCost;
    }

    //-----------Vehicle getServiceCost---------------
    public ArrayList getVehicleRegNos() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleRegNos = new ArrayList();
        vehicleRegNos = vehicleDAO.getVehicleRegNos();
        return vehicleRegNos;
    }

    public ArrayList getRouteList() throws FPRuntimeException, FPBusinessException {
        ArrayList routeList = new ArrayList();
        routeList = vehicleDAO.getRouteListNew();
        return routeList;
    }

    public ArrayList getCompanyList() throws FPRuntimeException, FPBusinessException {
        ArrayList companyList = new ArrayList();
        companyList = vehicleDAO.getCompanyList();
        return companyList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processActiveRouteList() throws FPRuntimeException, FPBusinessException {
        ArrayList RouteList = new ArrayList();
        //ArrayList activeModelList = new ArrayList();
        //VehicleTO vehicleTO = null;
        RouteList = vehicleDAO.getRouteList();
        /*Iterator itr = RouteList.iterator();
         while(itr.hasNext()){
         vehicleTO = new VehicleTO();
         vehicleTO = (VehicleTO)itr.next();
         if(vehicleTO.getActiveInd().equalsIgnoreCase("y"))
         {
         activeModelList.add(vehicleTO);
         }
         }*/
        return RouteList;
    }

    /**
     * This method used to Get getCusList Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCusList() throws FPRuntimeException, FPBusinessException {
        ArrayList custList = new ArrayList();
        custList = vehicleDAO.getCusList();
        return custList;
    }

    public ArrayList getLPSProductList() throws FPBusinessException, FPRuntimeException {
        System.out.println("..................iii");
        ArrayList productList = new ArrayList();
        productList = vehicleDAO.getLPSProductList();
        return productList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList alterRouteDetails(String routeId) throws FPRuntimeException, FPBusinessException {
        ArrayList routeDetails = new ArrayList();
        System.out.println("BP");
        routeDetails = vehicleDAO.alterRouteDetails(routeId);
        return routeDetails;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertRouteDetails(VehicleTO vehicleTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.insertRouteDetails(vehicleTO, UserId);
        return insertStatus;

    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetRouteList() throws FPRuntimeException, FPBusinessException {
        ArrayList routeList = new ArrayList();
        routeList = vehicleDAO.getRouteListDetails();
        return routeList;
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processModifyRouteDetails(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.doRouteDetailsModify(List, UserId);
        return insertStatus;
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int ModifyRouteDetails(VehicleTO vehicleTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.ModifyRouteDetails(vehicleTO, UserId);
//        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-MFR-02");
//        }
        return insertStatus;
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList viewVehicleLubrication(String vehicleNo) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleLubri = new ArrayList();
        vehicleLubri = vehicleDAO.viewVehicleLubrication(vehicleNo);
        return vehicleLubri;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleFinanceList(VehicleTO vehicleTO, int UserId) throws FPRuntimeException, FPBusinessException {
        ArrayList VehicleInsList = new ArrayList();
        VehicleInsList = vehicleDAO.getVehicleFinanceList(vehicleTO, UserId);
//        if (VehicleInsList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return VehicleInsList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleInsuranceList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList VehicleInsList = new ArrayList();
        VehicleInsList = vehicleDAO.getVehicleInsuranceList(vehicleTO);
        return VehicleInsList;
    }

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleAMCList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleAmcList = new ArrayList();
        vehicleAmcList = vehicleDAO.getVehicleAMCList(vehicleTO);
        return vehicleAmcList;
    }

    public ArrayList vehicleInsuranceDetail(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleInsurance = new ArrayList();
        vehicleInsurance = vehicleDAO.getvehicleInsurance(vehicleTO);
        System.out.println("vehicleInsuranceList BP= " + vehicleInsurance.size());
        return vehicleInsurance;
    }

    public ArrayList vehicleAMCDetail(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleAMC = new ArrayList();
        vehicleAMC = vehicleDAO.getvehicleAMC(vehicleTO);
        System.out.println("vehicleInsuranceList BP= " + vehicleAMC.size());
        return vehicleAMC;
    }

    public ArrayList vehicleFinanceDetail(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleInsurance = new ArrayList();
        vehicleInsurance = vehicleDAO.vehicleFinanceDetail(vehicleTO);
        System.out.println("vehicleInsuranceList BP= " + vehicleInsurance.size());
        return vehicleInsurance;
    }

    public ArrayList updateVehicleDetail(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleDetail = new ArrayList();
        vehicleDetail = vehicleDAO.getUpdateVehicleDetail(vehicleTO);
        System.out.println("vehicleInsuranceList BP= " + vehicleDetail.size());
        return vehicleDetail;
    }

    public int processUpdateVehicleInsurance(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("bp test===");
        int insertStatus = 0;
        insertStatus = vehicleDAO.processUpdateVehicleInsurance(vehicleTO, userId);
        return insertStatus;
    }

    public int processUpdateVehicleAMC(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("bp test===");
        int insertStatus = 0;
        insertStatus = vehicleDAO.processUpdateVehicleAMC(vehicleTO, userId);
        return insertStatus;
    }

    public ArrayList getRoadTaxList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList RoadTaxList = new ArrayList();
        RoadTaxList = vehicleDAO.getRoadTaxList(vehicleTO);
        System.out.println("RoadTaxList bp = " + RoadTaxList.size());
        return RoadTaxList;
    }

    public ArrayList vehicleRoadTax(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleRoadTax = new ArrayList();
        vehicleRoadTax = vehicleDAO.getvehicleRoadTax(vehicleTO);
        System.out.println("vehicleRoadTax BP= " + vehicleRoadTax.size());
        return vehicleRoadTax;
    }

    public ArrayList updateTax(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleTax = new ArrayList();
        vehicleTax = vehicleDAO.getUpdateTax(vehicleTO);
        System.out.println("vehicleTax BP= " + vehicleTax.size());
        return vehicleTax;
    }

    public int processUpdateVehicleTax(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("bp test===");
        int insertStatus = 0;
        insertStatus = vehicleDAO.processUpdateVehicleTax(vehicleTO, userId);
        return insertStatus;
    }

    public ArrayList getFCList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList FCList = new ArrayList();
        FCList = vehicleDAO.getFCList(vehicleTO);
        System.out.println("FCList bp = " + FCList.size());
        return FCList;
    }

    public ArrayList updateFC(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleFC = new ArrayList();
        vehicleFC = vehicleDAO.getvehicleFC(vehicleTO);
        System.out.println("vehicleFC BP= " + vehicleFC.size());
        return vehicleFC;
    }

    public ArrayList vehicleFCDetail(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleFCDetail = new ArrayList();
        vehicleFCDetail = vehicleDAO.getUpdateFCDetail(vehicleTO);
        System.out.println("vehicleTax BP= " + vehicleFCDetail.size());
        return vehicleFCDetail;
    }

    public int processUpdateVehicleFC(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("bp test===");
        int insertStatus = 0;
        insertStatus = vehicleDAO.processUpdateVehicleFC(vehicleTO, userId);
        return insertStatus;
    }

    public int saveVehiclePermit(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehiclePermit(vehicleTO, companyId, userId);
        return insertStatus;
    }

    public ArrayList getVehiclePermitList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList VehiclePermitList = new ArrayList();
        VehiclePermitList = vehicleDAO.getVehiclePermitList(vehicleTO);
        return VehiclePermitList;
    }

    public ArrayList vehiclePermitDetail(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("in bp====");
        ArrayList vehicleInsurance = new ArrayList();
        vehicleInsurance = vehicleDAO.getvehiclePermit(vehicleTO);
        System.out.println("vehiclePermitList BP= " + vehicleInsurance.size());
        return vehicleInsurance;
    }

    public int processUpdateVehiclePermit(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("bp test===");
        int insertStatus = 0;
        insertStatus = vehicleDAO.processUpdateVehiclePermit(vehicleTO, userId);
        return insertStatus;
    }

    public ArrayList getLeasingCustomerList() throws FPRuntimeException, FPBusinessException {
        ArrayList leasingCustList = new ArrayList();
        leasingCustList = vehicleDAO.getLeasingCustomerList();
        return leasingCustList;
    }

    public String getToLocSugg(String toLocation) {
        toLocation = toLocation + "%";
        toLocation = vehicleDAO.getToLocSugg(toLocation);
        return toLocation;
    }

    public ArrayList getVehicleTypeList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleTypeList = new ArrayList();
        vehicleTypeList = vehicleDAO.getVehicleTypeList();
        return vehicleTypeList;
    }

    public ArrayList processTrailerList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleList = new ArrayList();
        System.out.println("i m here 3");
        vehicleList = vehicleDAO.getTrailerDetail(vehicleTO);

        return vehicleList;
    }

    public int processAddTrailerDetails(VehicleTO vehicleTO, int UserId, int companyId) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        int vehicle_id = 0;
        vehicle_id = vehicleDAO.doAddTrailerDetails(vehicleTO, UserId);
//        vehicleTO.getVehicleId(vehicle_id);
        if (vehicleTO.getItemIds() != null) {
            if (!vehicleTO.getItemIds()[0].equalsIgnoreCase("0")) {
                insertStatus = vehicleDAO.doInsertTrailerTyre(vehicleTO, UserId, companyId, vehicle_id);
            }
        }
        return vehicle_id;
    }

    public ArrayList processTrailerDetail(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleList = new ArrayList();
        vehicleList = vehicleDAO.getTrailerList(vehicleTO);

        return vehicleList;
    }

    public int processUpdateTrailerDetails(VehicleTO vehicleTO, int companyId, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.doAlterTrailerDetails(vehicleTO, UserId);

        if (insertStatus == 0) {
            throw new FPBusinessException("EM-MFR-02");
        }
        return insertStatus;
    }

    public ArrayList getVehicleReplacement() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleReplacementList = new ArrayList();
        vehicleReplacementList = vehicleDAO.getVehicleReplacementList();
        return vehicleReplacementList;
    }

    public int insertVehicleReplacement(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.insertVehicleReplacement(vehicleTO, userId);
//        if (insertStatus == 0) {
//            throw new FPBusinessException("EM-MFR-02");
//        }
        return insertStatus;
    }

    public int updateVehicleReplacement(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = vehicleDAO.updateVehicleReplacement(vehicleTO, userId);
        return status;
    }

    public ArrayList processVendorList() throws FPBusinessException, FPRuntimeException {
        ArrayList vendorList = new ArrayList();
        vendorList = vehicleDAO.getVendorList();
        return vendorList;
    }

    public ArrayList getVendorVehicleRegNo(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList regNoList = new ArrayList();
        regNoList = vehicleDAO.getVendorVehicleRegNo(vehicleTO);
        return regNoList;
    }

    public ArrayList getVendorVehicleRegNos(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList regNosList = new ArrayList();
        regNosList = vehicleDAO.getVendorVehicleRegNos(vehicleTO);
        return regNosList;
    }

    public ArrayList getRegNo(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList regNoList = new ArrayList();
        regNoList = vehicleDAO.getRegNo(vehicleTO);
        return regNoList;
    }

    public ArrayList getRegNos(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList regNosList = new ArrayList();
        regNosList = vehicleDAO.getRegNos(vehicleTO);
        return regNosList;
    }

    public ArrayList processTrailerTypeList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {

        ArrayList trailerTypeList = new ArrayList();
        System.out.println("i m here 3");
        trailerTypeList = vehicleDAO.getTrailerTypeList(vehicleTO);

        return trailerTypeList;
    }

    public ArrayList vendorNameList(String venId) throws FPRuntimeException, FPBusinessException {
        System.out.println("in BP  getting vendor name for popup");

        ArrayList vendorNameList = new ArrayList();

        vendorNameList = vehicleDAO.vendorNameList(venId);

        return vendorNameList;
    }

    public ArrayList GetAxleList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {

        ArrayList AxleList = new ArrayList();
        AxleList = vehicleDAO.GetAxleList(vehicleTO);
        return AxleList;
    }

    public int insertVehicleTyre(VehicleTO vehicleTO, int UserId, int companyId, String vehicleFront, String leftTyreIds, String leftItemIds, String rightTyreIds, String rightItemIds) throws FPRuntimeException, FPBusinessException {
        int insertVehicleTyre = 0;
        insertVehicleTyre = vehicleDAO.insertVehicleTyre(vehicleTO, UserId, companyId, vehicleFront, leftTyreIds, leftItemIds, rightTyreIds, rightItemIds);
        return insertVehicleTyre;
    }

    public int insertAxleMaster(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        insertStatus = vehicleDAO.insertAxleMaster(vehicleTO, userId);
        return insertStatus;
    }

    public int insertAxletypeMaster(VehicleTO vehicleTO, int userId, String vehicleFront, String axleRight, String axleLeft, int lastinsertedId) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;

        insertStatus = vehicleDAO.insertAxletypeMaster(vehicleTO, userId, vehicleFront, axleRight, axleLeft, lastinsertedId);

        return insertStatus;
    }

    public ArrayList getVehAxleName(int axleId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleAxleDetail = new ArrayList();
        vehicleAxleDetail = vehicleDAO.getVehAxleName(axleId);
        return vehicleAxleDetail;
    }

    public ArrayList getVehAxleTyreNo(VehicleTO vehTo) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleAxleDetail = new ArrayList();
        vehicleAxleDetail = vehicleDAO.getVehAxleTyreNo(vehTo);
        return vehicleAxleDetail;
    }

    public String addVehicleDetails(VehicleTO vehicleTO, int UserId, int companyId) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        int vehicle_id = 0;
        String vehicleDetails = vehicleDAO.doAddVehicleDetails(vehicleTO, UserId,companyId);
        String[] temp = vehicleDetails.split("~");
        vehicle_id = Integer.parseInt(temp[0]);
        if (vehicle_id == 0) {
            throw new FPBusinessException("EM-VEHICLE-02");
        }
        vehicleTO.setVehicleId(String.valueOf(vehicle_id));

        insertStatus = vehicleDAO.doInsertVehicleClass(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-03");
        }

        insertStatus = vehicleDAO.doInsertVehicleEngine(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-04");
        }

        insertStatus = vehicleDAO.doInsertVehicleOper(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-05");
        }
        insertStatus = vehicleDAO.doInsertVehicleFc(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-06");
        }

        insertStatus = vehicleDAO.doInsertVehicleReg(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-07");
        }

        insertStatus = vehicleDAO.doInsertVehicleUsage(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-08");
        }
        insertStatus = vehicleDAO.doInsertVehicleCust(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-09");
        }
        System.out.println("checktest");
        insertStatus = vehicleDAO.doInsertVehicleHist(vehicleTO, UserId);
        System.out.println("check1:");
        return vehicleDetails;
    }

    public int updateVehicleDetails(VehicleTO vehicleTO, int UserId, int companyId) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        int vehicle_id = 0;
        vehicle_id = vehicleDAO.doAlterVehicleDetails(vehicleTO, UserId, companyId);
        if (vehicle_id == 0) {
            throw new FPBusinessException("EM-VEHICLE-02");
        }
        vehicleTO.setVehicleId(String.valueOf(vehicle_id));

        insertStatus = vehicleDAO.doInsertVehicleClass(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-03");
        }

        insertStatus = vehicleDAO.doInsertVehicleEngine(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-04");
        }

        insertStatus = vehicleDAO.doInsertVehicleOper(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-05");
        }
        insertStatus = vehicleDAO.doInsertVehicleFc(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-06");
        }

        insertStatus = vehicleDAO.doInsertVehicleReg(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-07");
        }

        insertStatus = vehicleDAO.doInsertVehicleUsage(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-08");
        }
        insertStatus = vehicleDAO.doInsertVehicleCust(vehicleTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-VEHICLE-09");
        }
        System.out.println("checktest");
        insertStatus = vehicleDAO.doInsertVehicleHist(vehicleTO, UserId);
        System.out.println("check1:");
        return vehicle_id;
    }

    public int vehicleTyreDetails(VehicleTO vehicleTO, int vehicleId, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        insertStatus = vehicleDAO.vehicleTyreDetails(vehicleTO, vehicleId, userId, madhavaramSp);
        return insertStatus;
    }

    public int updateVehicleTyreDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = vehicleDAO.updateVehicleTyreDetails(vehicleTO, userId, madhavaramSp);
        System.out.println("updateStatus===========" + updateStatus);
        return updateStatus;
    }

    public int updateInsuranceDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {

        System.out.println("updateInsuranceDetails entered BP");
        int updateStatus = 0;
        SqlMapClient session = vehicleDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateStatus = vehicleDAO.updateInsuranceDetails(vehicleTO, userId, madhavaramSp, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return updateStatus;
    }

    public int insertRoadTaxDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        SqlMapClient session = vehicleDAO.getSqlMapClient();
        try {
            session.startTransaction();
            System.out.println("bp insertRoadTaxDetails");
            updateStatus = vehicleDAO.insertRoadTaxDetails(vehicleTO, userId, madhavaramSp, session);
            System.out.println("updateStatus bp" + updateStatus);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return updateStatus;
    }

    public int insertFCDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        SqlMapClient session = vehicleDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateStatus = vehicleDAO.insertFCDetails(vehicleTO, userId, madhavaramSp, session);
            System.out.println("updateStatus bp" + updateStatus);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return updateStatus;
    }

    public int insertPermitDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        SqlMapClient session = vehicleDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateStatus = vehicleDAO.insertPermitDetails(vehicleTO, userId, madhavaramSp, session);
            System.out.println("updateStatus bp" + updateStatus);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return updateStatus;
    }

    public int insertVehicleDepreciation(VehicleTO vehicleTO, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = vehicleDAO.insertVehicleDepreciation(vehicleTO, userId, madhavaramSp);
        return updateStatus;
    }

    public ArrayList getVehiclesDetails(String vehicleId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclesDetails = new ArrayList();
        vehiclesDetails = vehicleDAO.getVehiclesDetails(vehicleId);
        return vehiclesDetails;
    }

    public ArrayList getVehiclesInsuranceList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclesList = new ArrayList();
        vehiclesList = vehicleDAO.getVehiclesInsuranceList(vehicleTO);
        return vehiclesList;
    }

    public ArrayList getFleetPurchaseList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fleetPurchaseList = new ArrayList();
        fleetPurchaseList = vehicleDAO.getFleetPurchaseList(vehicleTO);
        return fleetPurchaseList;
    }

    public ArrayList getVehiclesFCList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclesFCList = new ArrayList();
        vehiclesFCList = vehicleDAO.getVehiclesFCList(vehicleTO);
        return vehiclesFCList;
    }

    public ArrayList getTrailerInsuranceList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclesList = new ArrayList();
        vehiclesList = vehicleDAO.getTrailerInsuranceList(vehicleTO);
        return vehiclesList;
    }

    public ArrayList getTrailerFCList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclesFCList = new ArrayList();
        vehiclesFCList = vehicleDAO.getTrailerFCList(vehicleTO);
        return vehiclesFCList;
    }

    public ArrayList getVehicleDepreciationList(String vehicleId) throws FPRuntimeException, FPBusinessException {
        ArrayList leasingCustList = new ArrayList();
        leasingCustList = vehicleDAO.getVehicleDepreciationList(vehicleId);
        return leasingCustList;
    }

    public int saveFileUploadDetails(String actualFilePath, String fileSaved, String narration, String documentId, VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("BP");
        int status = 0;
        status = vehicleDAO.saveFileUploadDetails(actualFilePath, fileSaved, narration, documentId, vehicleTO, userId);
        return status;
    }

    public int updateFileUploadDetails(String actualFilePath, String fileSaved, String narration, String documentId, String uploadId, VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("BP update");
        int status = 0;
        status = vehicleDAO.updateFileUploadDetails(actualFilePath, fileSaved, narration, documentId, uploadId, vehicleTO, userId);
        return status;
    }

    public int insertOemDetails(VehicleTO vehicleTO, String oemDetailsId, String oemMfr, String oemTyreNo, String oemDepth, String oemBattery, String oemId, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("BP");
        int oemId1 = 0;
        oemId1 = vehicleDAO.insertOemDetails(vehicleTO, oemDetailsId, oemMfr, oemTyreNo, oemDepth, oemBattery, oemId, userId);
        return oemId1;
    }

    public ArrayList getOemList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList oemList = new ArrayList();
        oemList = vehicleDAO.getOemList(vehicleTO);
        return oemList;
    }

    public ArrayList getTrailerNoList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = vehicleDAO.getTrailerNoList(vehicleTO);
        return vehicleList;
    }

    public int insertVessel(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = vehicleDAO.insertVessel(vehicleTO, userId);
        return updateStatus;
    }

    public ArrayList getVesselNameList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = vehicleDAO.getVesselNameList(vehicleTO);
        return vehicleList;
    }

    public int insertDischargeVessel(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = vehicleDAO.insertDischargeVessel(vehicleTO, userId);
        return updateStatus;
    }

    public ArrayList getTrailerTypeList() throws FPRuntimeException, FPBusinessException {
        VehicleTO vehicleTo = new VehicleTO();
        ArrayList TypeList = new ArrayList();
        TypeList = vehicleDAO.getTrailerTypeList(vehicleTo);
        return TypeList;
    }

    public int processInsertTrailerTypeDetails(VehicleTO vehicleTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.processInsertTrailerTypeDetails(vehicleTO, UserId);
        return insertStatus;
    }

    public int processModifyTrailerTypeDetails(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = vehicleDAO.processModifyTrailerTypeDetails(List, UserId);
        return insertStatus;
    }

    public ArrayList getMfrList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList MfrList = new ArrayList();
        MfrList = vehicleDAO.getMfrList(vehicleTO);
        return MfrList;
    }

    public ArrayList getTypeList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList TypeList = new ArrayList();
        TypeList = vehicleDAO.getTypeList(vehicleTO);
        return TypeList;
    }

    public String addTrailerDetails(VehicleTO vehicleTO, int UserId, int companyId) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        int vehicle_id = 0;
        SqlMapClient session = vehicleDAO.getSqlMapClient();
        String vehicleDetails = "";
        try {
            session.startTransaction();
            vehicleDetails = vehicleDAO.doAddTrailereDetails(vehicleTO, UserId, session);
            String[] temp = vehicleDetails.split("~");
            vehicle_id = Integer.parseInt(temp[0]);
            if (vehicle_id == 0) {
                throw new FPBusinessException("EM-VEHICLE-02");
            }
            vehicleTO.setVehicleId(String.valueOf(vehicle_id));

            insertStatus = vehicleDAO.doInsertTrailerClass(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-03");
            }

            insertStatus = vehicleDAO.doInsertTrailerEngine(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-04");
            }

            insertStatus = vehicleDAO.doInsertTrailerOper(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-05");
            }
            insertStatus = vehicleDAO.doInsertTrailerFc(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-06");
            }

            insertStatus = vehicleDAO.doInsertTrailerReg(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-07");
            }

            insertStatus = vehicleDAO.doInsertTrailerUsage(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-08");
            }
            insertStatus = vehicleDAO.doInsertTrailerCust(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-09");
            }
            System.out.println("checktest");
            insertStatus = vehicleDAO.doInsertTrailerHist(vehicleTO, UserId, session);
            System.out.println("check1:");
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return vehicleDetails;
    }

    public int trailerTyreDetails(VehicleTO vehicleTO, int vehicleId, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        insertStatus = vehicleDAO.trailerTyreDetails(vehicleTO, vehicleId, userId, madhavaramSp);
        return insertStatus;
    }

    public int updateTrailerDetails(VehicleTO vehicleTO, int UserId, int companyId) throws FPRuntimeException, FPBusinessException {
        System.out.println("vehicle BP");
        int insertStatus = 0;
        int vehicle_id = 0;
        SqlMapClient session = vehicleDAO.getSqlMapClient();
        try {
            session.startTransaction();
            vehicle_id = vehicleDAO.doAlterTrailer(vehicleTO, UserId, session);
            if (vehicle_id == 0) {
                throw new FPBusinessException("EM-VEHICLE-02");
            }
            vehicleTO.setVehicleId(String.valueOf(vehicle_id));

            insertStatus = vehicleDAO.doInsertTrailerClass(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-03");
            }

            insertStatus = vehicleDAO.doInsertTrailerEngine(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-04");
            }

            insertStatus = vehicleDAO.doInsertTrailerOper(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-05");
            }
            insertStatus = vehicleDAO.doInsertTrailerFc(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-06");
            }

            insertStatus = vehicleDAO.doInsertTrailerReg(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-07");
            }

            insertStatus = vehicleDAO.doInsertTrailerUsage(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-08");
            }
            insertStatus = vehicleDAO.doInsertTrailerCust(vehicleTO, UserId, session);
            if (insertStatus == 0) {
                throw new FPBusinessException("EM-VEHICLE-09");
            }
            System.out.println("checktest");
            insertStatus = vehicleDAO.doInsertTrailerHist(vehicleTO, UserId, session);
            System.out.println("check1:");
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return vehicle_id;
    }

    public ArrayList vendorListCompliance(String vendorTypeIdCompliance) throws FPRuntimeException, FPBusinessException {
        System.out.println("in BP  getting vendor name for popup");
        ArrayList vendorListCompliance = new ArrayList();
        vendorListCompliance = vehicleDAO.vendorListCompliance(vendorTypeIdCompliance);
        return vendorListCompliance;
    }

    public ArrayList vendorList(String vendorTypeId) throws FPRuntimeException, FPBusinessException {
        System.out.println("in BP  getting vendor name for popup");
        ArrayList vendorList = new ArrayList();
        vendorList = vehicleDAO.vendorList(vendorTypeId);
        return vendorList;
    }

    public ArrayList getTrailerAxleTyreNo(VehicleTO vehTo) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleAxleDetail = new ArrayList();
        vehicleAxleDetail = vehicleDAO.getTrailerAxleTyreNo(vehTo);
        return vehicleAxleDetail;
    }

    public int updateTrailerTyreDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {
        int updateStatus = 0;
        updateStatus = vehicleDAO.updateTrailerTyreDetails(vehicleTO, userId, madhavaramSp);
        return updateStatus;
    }

    public int insertTrailerOemDetails(VehicleTO vehicleTO, String oemDetailsId, String oemMfr, String oemTyreNo, String oemDepth, String oemBattery, String oemId, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("BP");
        int oemId1 = 0;
        oemId1 = vehicleDAO.insertTrailerOemDetails(vehicleTO, oemDetailsId, oemMfr, oemTyreNo, oemDepth, oemBattery, oemId, userId);
        return oemId1;
    }

    public ArrayList getTrailerOemList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList oemList = new ArrayList();
        oemList = vehicleDAO.getTrailerOemList(vehicleTO);
        return oemList;
    }

    public ArrayList getTrailerDetails(String trailerId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclesDetails = new ArrayList();
        vehiclesDetails = vehicleDAO.getTrailerDetails(trailerId);
        return vehiclesDetails;
    }

    public ArrayList getPrimarybankList() throws FPBusinessException, FPRuntimeException {
        ArrayList primarybankList = new ArrayList();
        primarybankList = vehicleDAO.getPrimarybankList();
        return primarybankList;
    }

    public ArrayList getVehiclesRegistrationList(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclesRegistrationList = new ArrayList();
        vehiclesRegistrationList = vehicleDAO.getVehiclesRegistrationList(vehicleTO);
        return vehiclesRegistrationList;
    }

    public ArrayList getvehicleUploadsDetails(VehicleTO vehicleTO) {
        ArrayList vehicleUploadsDetails = new ArrayList();
        vehicleUploadsDetails = vehicleDAO.getvehicleUploadsDetails(vehicleTO);
        return vehicleUploadsDetails;
    }

    public ArrayList getvehicleUploadsDetailsPermit(VehicleTO vehicleTO, String type) {
        ArrayList vehicleUploadsDetailsPermit = new ArrayList();
        vehicleUploadsDetailsPermit = vehicleDAO.getvehicleUploadsDetailsPermit(vehicleTO, type);
        return vehicleUploadsDetailsPermit;
    }

    public ArrayList getDisplayLogoBlobData(String uploadId) {
        ArrayList profileList = new ArrayList();
        profileList = vehicleDAO.getDisplayLogoBlobData(uploadId);
        return profileList;
    }

    public int saveRegistrationDetails(VehicleTO vehicleTO, int userId, int madhavaramSp) throws FPRuntimeException, FPBusinessException {

        System.out.println("saveRegistrationDetails entered BP");
        int updateStatus = 0;
        SqlMapClient session = vehicleDAO.getSqlMapClient();
        try {
            session.startTransaction();
            updateStatus = vehicleDAO.saveRegistrationDetails(vehicleTO, userId, madhavaramSp, session);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return updateStatus;
    }
//Vehicle

    public ArrayList vehiclePermitDetails(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclePermitDetails = new ArrayList();
        vehiclePermitDetails = vehicleDAO.getVehiclePermitDetails(vehicleTO);

        VehicleTO rcTO = null;

        Iterator itr = vehiclePermitDetails.iterator();
        while (itr.hasNext()) {
            rcTO = new VehicleTO();
            rcTO = (VehicleTO) itr.next();
            System.out.println(rcTO.getVehicleId() + "vijay" + rcTO.getRegNo());
            rcTO.setVehiclePermit1Details(vehicleDAO.getVehiclePermit1Details(vehicleTO, rcTO.getVehicleId(), rcTO.getRegNo()));
            rcTO.setVehiclePermit2Details(vehicleDAO.getVehiclePermit2Details(vehicleTO, rcTO.getVehicleId(), rcTO.getRegNo()));
            rcTO.setVehiclePermit3Details(vehicleDAO.getVehiclePermit3Details(vehicleTO, rcTO.getVehicleId(), rcTO.getRegNo()));
            //////System.out.println("rcTO.getSparesAmount() Hari"+rcTO.getSparesAmount());
        }

        return vehiclePermitDetails;
    }

//    public ArrayList vehiclePermit1Details(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
//        ArrayList vehiclePermit1Details = new ArrayList();
//        vehiclePermit1Details = vehicleDAO.getVehiclePermit1Details(vehicleTO);
//        return vehiclePermit1Details;
//    }
//    public ArrayList vehiclePermit2Details(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
//        ArrayList vehiclePermit2Details = new ArrayList();
//        vehiclePermit2Details = vehicleDAO.getVehiclePermit2Details(vehicleTO);
//        return vehiclePermit2Details;
//    }
//    public ArrayList vehiclePermit3Details(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
//        ArrayList vehiclePermit3Details = new ArrayList();
//        vehiclePermit3Details = vehicleDAO.getVehiclePermit3Details(vehicleTO);
//        return vehiclePermit3Details;
//    }
    public ArrayList getOldVehiclePermit(VehicleTO vehicleTO, String vehicleId, String permitId, String permitType) throws FPRuntimeException, FPBusinessException {
        ArrayList OldVehiclePermit = new ArrayList();
        OldVehiclePermit = vehicleDAO.getOldVehiclePermit(vehicleTO, vehicleId, permitId, permitType);
        return OldVehiclePermit;
    }

    public ArrayList getPermitDetails(VehicleTO vehicleTO, String vehicleId, String regNo, String permitType) throws FPRuntimeException, FPBusinessException {
        ArrayList PermitDetails = new ArrayList();
        PermitDetails = vehicleDAO.getPermitDetails(vehicleTO, vehicleId, regNo, permitType);
        return PermitDetails;
    }

    public ArrayList getVehicleFCDetails(VehicleTO vehicleTO, String vehicleId, String regNo) throws FPRuntimeException, FPBusinessException {
        ArrayList getVehicleFCDetails = new ArrayList();
        getVehicleFCDetails = vehicleDAO.getVehicleFCDetails(vehicleTO, vehicleId, regNo);
        return getVehicleFCDetails;
    }

    public ArrayList getOldVehicleFC(VehicleTO vehicleTO, String vehicleId, String fcid) throws FPRuntimeException, FPBusinessException {
        ArrayList OldVehicleFC = new ArrayList();
        OldVehicleFC = vehicleDAO.getOldVehicleFC(vehicleTO, vehicleId, fcid);
        return OldVehicleFC;
    }

    public ArrayList getRoadTaxDetails(VehicleTO vehicleTO, String vehicleId, String regNo) throws FPRuntimeException, FPBusinessException {
        ArrayList RoadTaxDetails = new ArrayList();
        RoadTaxDetails = vehicleDAO.getRoadTaxDetails(vehicleTO, vehicleId, regNo);
        return RoadTaxDetails;
    }

    public ArrayList getOldVehicletax(VehicleTO vehicleTO, String vehicleId, String id) throws FPRuntimeException, FPBusinessException {
        ArrayList OldVehicleTax = new ArrayList();
        OldVehicleTax = vehicleDAO.getOldVehicleTax(vehicleTO, vehicleId, id);
        return OldVehicleTax;
    }

    public ArrayList getVehicleInsuranceDetails(VehicleTO vehicleTO, String vehicleId, String regNo) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleInsuranceDetails = new ArrayList();
        vehicleInsuranceDetails = vehicleDAO.getVehicleInsuranceDetails(vehicleTO, vehicleId, regNo);
        return vehicleInsuranceDetails;
    }

    public ArrayList getOldVehicleInsurance(VehicleTO vehicleTO, String vehicleId, String insuranceId) throws FPRuntimeException, FPBusinessException {
        ArrayList OldVehicleInsurance = new ArrayList();
        OldVehicleInsurance = vehicleDAO.getOldVehicleInsurance(vehicleTO, vehicleId, insuranceId);
        return OldVehicleInsurance;
    }
//Vehicle

    public ArrayList vehiclePollutionDetails(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclePollutionDetails = new ArrayList();
        vehiclePollutionDetails = vehicleDAO.vehiclePollutionDetails(vehicleTO);
        return vehiclePollutionDetails;
    }

    public int saveVehiclePollutionDetails(VehicleTO vehicleTO, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehiclePollutionDetails(vehicleTO, userId);
        return insertStatus;
    }

    public ArrayList getVehiclePollutionDetails(VehicleTO vehicleTO, String vehicleId, String regNo) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclePollutionDetails = new ArrayList();
        vehiclePollutionDetails = vehicleDAO.getVehiclePollutionDetails(vehicleTO, vehicleId, regNo);
        return vehiclePollutionDetails;
    }

    public int updateVehiclePollutionDetails(VehicleTO vehicleTO, int userId) {
        int updateStatus = 0;
        updateStatus = vehicleDAO.updateVehiclePollutionDetails(vehicleTO, userId);
        return updateStatus;
    }

    public ArrayList getOldVehiclePollution(VehicleTO vehicleTO, String vehicleId, String regNo) throws FPRuntimeException, FPBusinessException {
        ArrayList oldVehiclePollutionDetails = new ArrayList();
        oldVehiclePollutionDetails = vehicleDAO.getOldVehiclePollution(vehicleTO, vehicleId, regNo);
        return oldVehiclePollutionDetails;
    }

    public ArrayList vehicleGreentaxDetails(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleGreentaxDetails = new ArrayList();
        vehicleGreentaxDetails = vehicleDAO.getVehicleGreentaxDetails(vehicleTO);
        return vehicleGreentaxDetails;
    }

    public int saveVehicleGreenTax(VehicleTO vehicleTO, int companyId, int userId) {
        int insertStatus = 0;
        insertStatus = vehicleDAO.saveVehicleGreenTax(vehicleTO, companyId, userId);
        return insertStatus;
    }

    public ArrayList getDocumentType(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList documentType = new ArrayList();
        documentType = vehicleDAO.getDocumentType(vehicleTO);
        return documentType;
    }

    public ArrayList getGTDetails(VehicleTO vehicleTO, String vehicleId, String regNo) throws FPRuntimeException, FPBusinessException {
        ArrayList getGTDetails = new ArrayList();
        getGTDetails = vehicleDAO.getGTDetails(vehicleTO, vehicleId, regNo);
        return getGTDetails;
    }

    public int processUpdateVehicleGT(VehicleTO vehicleTO, int userId) throws FPRuntimeException, FPBusinessException {
        System.out.println("bp test===");
        int insertStatus = 0;
        insertStatus = vehicleDAO.processUpdateVehicleGT(vehicleTO, userId);
        return insertStatus;
    }

    public ArrayList getOldVehicleGT(VehicleTO vehicleTO, String vehicleId, String greentaxId) throws FPRuntimeException, FPBusinessException {
        ArrayList oldVehicleGT = new ArrayList();
        oldVehicleGT = vehicleDAO.getOldVehicleGT(vehicleTO, vehicleId, greentaxId);
        return oldVehicleGT;
    }

    public ArrayList vehicleUploadFileDetails(VehicleTO vehicleTO, String vehicleId, String documentId) {
        ArrayList vehicleUploadsDetails = new ArrayList();
        vehicleUploadsDetails = vehicleDAO.vehicleUploadFileDetails(vehicleTO, vehicleId, documentId);
        return vehicleUploadsDetails;
    }
    
     public int deleteUploadVehData(String uploadId) throws FPRuntimeException, FPBusinessException {
        System.out.println("bp test==="+uploadId);
        int insertStatus = 0;
        insertStatus = vehicleDAO.deleteUploadVehData(uploadId);
        return insertStatus;
    }
      public String getFileFormat(String uploadId) throws FPRuntimeException, FPBusinessException {
        String fileFormat = "";
        VehicleTO veh = new VehicleTO();
        fileFormat = vehicleDAO.getFileFormat(uploadId);
          System.out.println("fileFormat---in BP="+fileFormat);
        return fileFormat;
    }
      public String getPreviousPolicyNo(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        String previousPolicyNo = "";
        previousPolicyNo = vehicleDAO.getPreviousPolicyNo(vehicleTO);
          System.out.println("fileFormat---in BP="+previousPolicyNo);
        return previousPolicyNo;
    }
      
      
     public ArrayList getVehicleTrips(VehicleTO vehicleTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripCode = new ArrayList();
        tripCode = vehicleDAO.getVehicleTrips(vehicleTO);
        return tripCode;
    } 
      
}
 