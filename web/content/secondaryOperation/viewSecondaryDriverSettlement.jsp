<%--
    Document   : driverSettlementReport
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
   <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">


          /*  $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user

                $('#primaryDriver').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getSecondaryDriverName.do",
                            dataType: "json",
                            data: {
                                driverName: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                var primaryDriver = $('#primaryDriver').val();
                                if(items == '' && primaryDriver != ''){
                                    alert("Invalid Primary Driver Name");
                                    $('#primaryDriver').val('');
                                    $('#primaryDriverId').val('');
                                    $('#primaryDriver').focus();
                                }else{
                                }
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var id = ui.item.Id;
                        $('#primaryDriver').val(value);
                        $('#primaryDriverId').val(id);
                        $('#secondaryDriverOne').focus();
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
                };
                });
*/

                function submitPage(value) {
                            document.driverSettlement.action = '/throttle/viewSecondaryDriverSettlement.do';
                            document.driverSettlement.submit();

                }

                function setValues(){
                     if('<%=request.getAttribute("secondaryFleet")%>' != 'null'){
                    document.getElementById('secondaryFleet').value='<%=request.getAttribute("secondaryFleet")%>';
                    fleetDriver();

                }
                     if('<%=request.getAttribute("primaryDriverId")%>' != 'null'){
                    document.getElementById('primaryDriverId').value='<%=request.getAttribute("primaryDriverId")%>';
                }
                   
                     if('<%=request.getAttribute("fromDate")%>' != 'null'){
                    document.getElementById('fromDate').value='<%=request.getAttribute("fromDate")%>';
                }
                     if('<%=request.getAttribute("toDate")%>' != 'null'){
                    document.getElementById('toDate').value='<%=request.getAttribute("toDate")%>';
                }
                }

        function viewTripDetails(tripId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId='+tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
         function viewVehicleDetails(vehicleId) {
                window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }

        </script>


    </head>
    <body onload="setValues();">
        <form name="driverSettlement" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Secondary Driver Settlement</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="0" border="0" align="center" class="table4" >
                                    <tr>
                                         <td class="text1" align="center" height="30">Fleet Centre</td>
                                        <td class="text1" height="30">
                                         <c:if test="${getSecondaryFleet!= null}">
                                                 <select name="secondaryFleet" id="secondaryFleet" class="textbox" onchange="fleetDriver();" >
                                <option value='0~~0~~~'>--select--</option>
                                <c:forEach items="${getSecondaryFleet}" var="fleet">
                                  <option value='<c:out value="${fleet.name}"/>'> <c:out value="${fleet.name}"/> </option>
                                </c:forEach>
                                  
                               
                            </select>
                                            </c:if>
                                        </td>
                                            
                                            
                                            <script type="text/javascript">
                                                 function fleetDriver() {
                                    var secondaryFleet = $("#secondaryFleet").val();
                                    $.ajax({
                                        url: "/throttle/selectSecFleetDriver.do",
                                        dataType: "json",
                                        data: {
                                            secondaryFleet: secondaryFleet
                                            
                                        },
                                        success: function(data) {
                                            //alert(data);
                                            if (data != '') {
                                                $('#primaryDriver').empty();
                                                $('#primaryDriver').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                                $.each(data, function(i, data) {
    //                                                alert(data.Id);
                                                    $('#primaryDriver').append(
                                                            $('<option style="width:90px"></option>').attr("value",data.Id).text(data.Name)
                                                            )
                                                });
                                            }else{
                                                $('#primaryDriver').empty();
                                                $('#primaryDriver').append(
                                                        $('<option></option>').val(0).html('--select--'))
                                            }
                                        }
                                    });

                                    
                                }

                                               
                                           
                            </script>

                                    
                                    
                                    
                                        <td class="text1" align="center" height="30">Primary Driver Name</td>
                                        <td class="text1" height="30">

                                         <!--   <input type="text" class="textbox" id="primaryDriver"  name="primaryDriver" autocomplete="off" value="<c:out value="${primaryDriverName}"/>"/>
                                            <input type="hidden" class="textbox" id="primaryDriverId"  name="primaryDriverId" autocomplete="off" value="<c:out value="${primaryDriverId}"/>"/> -->
                                            <select name="primaryDriver" id="primaryDriver" class="textbox" >
                                                <option value=''> </option>
                                            </select>
                                            <script>
                                            if('<%=request.getAttribute("primaryDriver")%>' != 'null'){
                                                $('#primaryDriver').val(1035);
                                            }
                                            </script>
                                                                                    
                                        </td>
                                        
                                    </tr>
                                    <tr>
                                        <td class="text1" align="center" height="30"> <font color="red">*</font>From Date</td>
                                        <td class="text1" height="30"><input type="text" class="datepicker" id="fromDate" name="fromDate" autocomplete="off" value="<c:out value="${fromDate}"/>"/></td>
                                        <td class="text1" align="center" height="30"> <font color="red">*</font>To Date</td>
                                        <td class="text1" height="30"><input type="text" class="datepicker" id="toDate"  name="toDate" autocomplete="off" value="<c:out value="${toDate}"/>"/></td>
                                    </tr>

                                    <tr>

                                        <td class="text1" height="30" colspan="2" align="center"></td>
                                        <td class="text1" height="30"  align="center"><input type="button" class="button" value="Search" name="search" onClick="submitPage(this.name)"/></td>

                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <c:if test="${driverSettlementDetailsSize == '0'}">
                                        <center><font color="red">No Records Found</font></center>
                                    </c:if>
                               <c:if test="${driverSettlementDetailsSize != '0'}">
                <table width="900" align="center" border="0" id="table1" class="sortable">
                    <thead>
                        <tr height="45" >
                            <th>S.No</th>
                            <th>Driver Name </th>
                            <th>Settlement Date</th>
                            <th>Starting Balance</th>
                            <th>Ending Balance</th>
                            <th>Payment Mode</th>
                            <th>Remarks</th>
                            <th>Select</th>
                        </tr>
                    </thead>
                      <% int index = 0;
                                int sno1 = 1;
                    %>
                    <tbody>
                        <c:forEach items="${driverSettlementDetails}" var="driver">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>


                             <tr height="30">
                                <td align="left" class="<%=classText%>"><%=sno1%></td>
                                <td align="left" class="<%=classText%>"><label><c:out value="${driver.primaryDriverName}"/></label></td>
                                <td align="left" class="<%=classText%>"><label><c:out value="${driver.settlementDate}"/></label></td>
                                <td align="left" class="<%=classText%>"><label ><c:out value="${driver.startingBalance}"/></label></td>
                                <td align="left" class="<%=classText%>"><label ><c:out value="${driver.endingBalance}"/></label></td>
                                <td align="left" class="<%=classText%>"><label ><c:out value="${driver.paymentMode}"/></label></td>
                                <td align="left" class="<%=classText%>"><label><c:out value="${driver.settlementRemarks}"/></label></td>
                                  <td align="left" class="<%=classText%>"><label>
                                          <a href="/throttle/viewSecondaryTripDriverSettlement.do?driverId=<c:out value="${driver.primaryDriverId}"/>&driverName=<c:out value="${driver.primaryDriverName}"/>&fromDate=<c:out value="${fromDate}"/>&toDate=<c:out value="${toDate}"/>&settlementId=<c:out value="${driver.tripSettlementId}"/>">View Details</a>
                               </label></td>
                            </tr>
                              <%sno1++;%>
                            <%index++;%>
                                     </c:forEach>
                    </tbody>
                </table>
                    </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>