
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>

<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
            <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
            <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
            <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
                <script language="javascript" src="/throttle/js/validate.js"></script>


                <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
                    <script src="/throttle/js/jquery.ui.core.js"></script>
                    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

                    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
                        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
                            <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
                            <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
                                <script language="javascript" src="/throttle/js/validate.js"></script>
                                <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
                                <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
                                <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
                                <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
                                <style type="text/css" title="currentStyle">
                                    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
                                </style>

                                <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

                                <!-- jQuery libs -->
                                <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
                                <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

                                <!-- Our jQuery Script to make everything work -->

                                <script  type="text/javascript" src="js/jq-ac-script.js"></script>


                                <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
                                    <script src="/throttle/js/jquery.ui.core.js"></script>
                                    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
                                    <script type="text/javascript">
                                        $(document).ready(function () {
                                            $("#datepicker").datepicker({
                                                showOn: "button",
                                                buttonImage: "calendar.gif",
                                                buttonImageOnly: true

                                            });
                                        });

                                        $(function () {
                                            //alert("cv");
                                            $(".datepicker").datepicker({
                                                /*altField: "#alternate",
                                                 altFormat: "DD, d MM, yy"*/
                                                changeMonth: true, changeYear: true
                                            });
                                        });
                                    </script>
                                    </head>
                                    <script language="javascript">
                                        function submitPage(value) {
                                            var checkBillDetails = false;
                                            checkBillDetails = billDetailsValidation();
                                            if (isEmpty(document.jobCardBill.invoiceNo.value)) {
                                                alert("please enter invoice no");
                                                document.jobCardBill.invoiceNo.focus();
                                            } else if (isEmpty(document.jobCardBill.invoiceRemarks.value)) {
                                                alert("please enter invoice invoiceRemarks");
                                                document.jobCardBill.invoiceRemarks.focus();
                                            } else if (checkBillDetails && confirm("Are you sure to submit")) {
                                                document.jobCardBill.action = "/throttle/saveJobCardBilldetails.do";
                                                document.jobCardBill.submit();
                                            }
                                        }

                                        function billDetailsValidation() {
                                            var groupList = document.getElementsByName("groupList");
                                            var subGroupList = document.getElementsByName("subGroupList");
                                            var expenseRemarks = document.getElementsByName("expenseRemarks");
                                            var cost = document.getElementsByName("cost");
                                            var labour = document.getElementsByName("labour");
                                            var total = document.getElementsByName("total");
                                            for (var i = 0; i < groupList.length; i++) {
                                                if (expenseRemarks[i].value == '') {
                                                    alert("Please enter remarks for row " + i + 1);
                                                    expenseRemarks[i].focus();
                                                    return false;
                                                } else if (cost[i].value == '') {
                                                    alert("Please cost for row " + i + 1);
                                                    cost[i].focus();
                                                    return false;
                                                } else if (labour[i].value == '') {
                                                    alert("Please labour value for row " + i + 1);
                                                    labour[i].focus();
                                                    return false;
                                                } else {
                                                    return true;
                                                }
                                            }
                                        }
                                        function calcluateTotalAmt() {
                                            document.getElementById("totalValue").innerHTML =
                                                    parseFloat(document.jobCardBill.sparesAmt.value) + parseFloat(document.jobCardBill.consumableAmt.value) + parseFloat(document.jobCardBill.laborAmt.value) +
                                                    parseFloat(document.jobCardBill.othersAmt.value) + parseFloat(document.jobCardBill.vatAmt.value) + parseFloat(document.jobCardBill.serviceTaxAmt.value);
                                            document.getElementById("totalValue").innerHTML = parseFloat(document.getElementById("totalValue").innerHTML).toFixed(2);
                                            document.jobCardBill.totalAmt.value = parseFloat(document.getElementById("totalValue").innerHTML).toFixed(2);
                                        }

                                    </script>
                                    <div class="pageheader">
                                        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Job Card Billing" text="Job Card Billing"/> </h2>
                                        <div class="breadcrumb-wrapper">
                                            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                                            <ol class="breadcrumb">
                                                <li><spring:message code="head.label.Home" text="Home"/></li>
                                                <li><spring:message code="hrms.label.Master" text="Service"/></a></li>
                                                <li class=""><spring:message code="hrms.label.Job Card Billing" text="Job Card Billing"/></li>

                                            </ol>
                                        </div>
                                    </div>

                                    <div class="contentpanel">
                                        <div class="panel panel-default">
                                            <div class="panel-body">

                                                <body onload="addRow(1)">

                                                    <form method="post" name="jobCardBill" action= "jobCardBillStore.do">
                                                        <!-- copy there from end -->
                                                        <!--<   div id="print" >-->
                                                        <!--<div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >-->
                                                        <!--<div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">-->
                                                        <!-- pointer table -->
                                                        <!--                                                                    <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                                                                                                                                <tr>
                                                                                                                                    <td >
                                                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                                                    </td></tr></table>-->
                                                        <!-- pointer table -->

                                                        <!--</div>-->
                                                        <!--</div>-->

                                                        <!-- message table -->
                                                        <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                                                            <tr>
                                                                <td >
                                                                    <%@ include file="/content/common/message.jsp" %>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                        <!-- message table -->
                                                        <!-- copy there  end -->
                                                        <%
                                                                    int c = 0;
                                                        %>
                                                        <c:if test = "${jcList != null}" >
                                                            <c:forEach items="${jcList}" var="list">
                                                                <br>
                                                                    <br>
                                                                        <br>
                                                                            <table class="table table-info mb30 table-hover" style="width:50%">
                                                                                <thead><tr>
                                                                                        <th  colspan="6" height="30"><div >Record Vendor Bill Details</div></th>
                                                                                    </tr>

                                                                                    <tr>

                                                                                        <th ><b>JobCardNo</b></th>
                                                                                        <th ><c:out value="${list.jcMYFormatNo}"/>
                                                                                            <input type="hidden" name="jcNo" value='<c:out value="${list.jcMYFormatNo}"/>'/>
                                                                                            <input type="hidden" name="jcCreatedDate" value='<c:out value="${list.createdDate}"/>'/>
                                                                                            <input type="hidden" name="jcModelName" value='<c:out value="${list.modelName}"/>'/>
                                                                                            <input type="hidden" name="jcMfrName" value='<c:out value="${list.mfrName}"/>'/>
                                                                                            <input type="hidden" name="jcVehicleNo" value='<c:out value="${list.vehicleNo}"/>'/>
                                                                                            <input type="hidden" name="jcCustomerName" value='<c:out value="${list.customerName}"/>'/>

                                                                                        </th>

                                                                                        <th ><b>UID</b></th>
                                                                                        <th ><c:out value="${list.vehicleNo}"/></th>
                                                                                        <th ><b>Vendor</b></th>
                                                                                        <th ><c:out value="${list.vendorName}"/></th>
                                                                                    </tr>

                                                                                    </tbody>
                                                                            </table>
                                                                            <input type="hidden" name="jobCardId" value='<c:out value="${list.jobCardId}"/>'/>


                                                                        </c:forEach >

                                                                    </c:if>

                                                                    <br>
                                                                        <table class="table table-info mb30 table-hover" style="width:50%">
                                                                            <thead><tr>
                                                                                    <th align="center" colspan="4" height="30"><strong>Vendor Bill Particulars</strong></th>
                                                                                </tr>
                                                                                <tr height="30">
                                                                                    <td ><b>Invoice No</b></td>
                                                                                    <td ><input type="text" name="invoiceNo" style='width:180px;height:40px' id="invoiceNo" value="" class="form-control" /> </td>
                                                                                    <td ><b>Invoice Remarks</b></td>
                                                                                    <td ><textarea name="invoiceRemarks" id="invoiceRemarks" style='width:180px;height:40px' rowspan="3" colspan="6" class="form-control"></textarea></td>
                                                                                </tr>
                                                                                <tr height="30">

                                                                                    <td ><b>Invoice Amount</b></td>
                                                                                    <td >
                                                                                        <input type="text" name="invoiceAmount" id="invoiceAmount" value="" style='width:180px;height:40px' class="form-control" readonly/>
                                                                                    </td>
                                                                                    <td ><b>Invoice Date</b></td>
                                                                                    <td >
                                                                                        <input type="text" name="invoiceDate" style='width:180px;height:40px' id="invoiceDate" value="<%=session.getAttribute("currentDate")%>"  class="datepicker"/>
                                                                                    </td>
                                                                                </tr>
                                                                                <!--                            <td ><b>Total Amount</b></td>
                                                                                                            <td >
                                                                                                                <input type="text" name="totalAmount" id="totalAmount" value="0" class="form-control" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                                                                                                                <input type="text" name="totalAmountTemp" id="totalAmountTemp" value="0" class="form-control" onKeyPress='return onKeyPressBlockCharacters(event);'/>
                                                                                                            </td>-->
                                                                                </tr></thead>
                                                                        </table>

                                                                        <br>
                                                                            <table class="table table-info mb30 table-hover"  id="suppExpenseTBL" style="width:50%">
                                                                                <thead><tr >
                                                                                        <th width="50" ></th>
                                                                                        <th ></th>
                                                                                        <th ></th>
                                                                                        <th ></th>
                                                                                        <th colspan="3" align="center"> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; Parts GST</th>
                                                                                        <th ></th>
                                                                                        <th ></th>
                                                                                        <th colspan="3" align="center">Labour GST</th>
                                                                                        <th ></th>
                                                                                    </tr></thead>
                                                                                <thead><tr >
                                                                                        <th width="50" >S No&nbsp;</th>
                                                                                        <th ><font color='red'>*</font>Service Type</th>
                                                                                        <!--                                                                                    <th ><font color='red'>*</font>Group</th>
                                                                                                                                                                                <th ><font color='red'>*</font>Sub Group</th>-->
                                                                                        <th ><font color='red'>*</font>Description</th>
                                                                                        <th ><font color='red'>*</font>Quantity</th>
                                                                                        <th ><font color='red'>*</font>CGST</th>
                                                                                        <th ><font color='red'>*</font>SGST</th>
                                                                                        <th ><font color='red'>*</font>IGST</th>
                                                                                        <th ><font color='red'>*</font>Part Cost</th>
                                                                                        <th ><font color='red'>*</font>CGST</th>
                                                                                        <th ><font color='red'>*</font>SGST</th>
                                                                                        <th ><font color='red'>*</font>IGST</th>
                                                                                        <th ><font color='red'>*</font>Labour</th>
                                                                                        <th ><font color='red'>*</font>Total</th>
                                                                                    </tr></thead>
                                                                                <tr>
                                                                                    <td colspan="11"  align="center">
                                                                                        <input class="btn btn-success" type="button" value="Add Row" onClick="addRow(2);"/>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <br>
                                                                                <br>
                                                                                    <br>
                                                                                        <table class="table table-info mb30 table-hover" style="width:50%">
                                                                                            <thead>
                                                                                                <tr>
                                                                                                    <th colspan="4" >Summary</th>
                                                                                                </tr>
                                                                                            </thead>
                                                                                            <tr>
                                                                                                <td>Total Cost</td>
                                                                                                <td colspan="3"><input type="text" name="totalCost" id="totalCost" style='width:180px;height:40px' class="form-control"/></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Total Labour</td>
                                                                                                <td ><input type="text" name="totalLabour" id="totalLabour"  style='width:180px;height:40px' class="form-control"/></td>
                                                                                                <td>Labour Remarks</td>
                                                                                                <td ><textarea name="labourRemarks" id="labourRemarks"></textarea></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Total Amount</td>
                                                                                                <td colspan="3"><input type="text" name="totalAmount" style='width:180px;height:40px' id="totalAmount" class="form-control"/></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Part GST Amount</td>
                                                                                                <td ><input type="text" name="vatAmount" style='width:180px;height:40px' id="vatAmount" class="form-control" onkeyup="calculateInvoiceAmount()"/></td>
                                                                                                <td>Part GST Remarks</td>
                                                                                                <td ><textarea name="vatRemarks" style='width:180px;height:40px' id="vatRemarks"></textarea></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td>Service GST Amount</td>
                                                                                                <td ><input type="text" name="serviceTaxAmount" style='width:180px;height:40px' id="serviceTaxAmount" class="form-control" onkeyup="calculateInvoiceAmount()"/></td>
                                                                                                <td>Service GST Remarks</td>
                                                                                                <td ><textarea name="serviceTaxRemarks" id="serviceTaxRemarks"></textarea></td>
                                                                                            </tr>
                                                                                            <tr>
                                                                                                <td colspan="4" align="center"><input class="btn btn-success" type="button" value="Save" onClick="submitPage();"/></td>
                                                                                            </tr>
                                                                                        </table>
                                                                                        <script>


                                                                                            var httpRequest1;
                                                                                            function getSubGroupList(billGroupId, sno) {
                                                                                                if (billGroupId != 'null') {
                                                                                                    var list1 = eval("document.jobCardBill.subGroupList" + sno);
                                                                                                    while (list1.childNodes[0]) {
                                                                                                        list1.removeChild(list1.childNodes[0])
                                                                                                    }

                                                                                                    var url = '/throttle/getBillSubGroupDetails.do?billGroupId=' + billGroupId;

                                                                                                    if (window.ActiveXObject)
                                                                                                    {
                                                                                                        httpRequest1 = new ActiveXObject("Microsoft.XMLHTTP");
                                                                                                    } else if (window.XMLHttpRequest)
                                                                                                    {
                                                                                                        httpRequest1 = new XMLHttpRequest();
                                                                                                    }
                                                                                                    httpRequest1.open("POST", url, true);
                                                                                                    httpRequest1.onreadystatechange = function () {
                                                                                                        go(sno);
                                                                                                    };
                                                                                                    httpRequest1.send(null);
                                                                                                }
                                                                                            }

                                                                                            function go(sno) {
                                                                                                if (httpRequest1.readyState == 4) {
                                                                                                    if (httpRequest1.status == 200) {
                                                                                                        var response = httpRequest1.responseText;
                                                                                                        var list = eval("document.jobCardBill.subGroupList" + sno);
                                                                                                        var details = response.split(',');
                                                                                                        var probId = 0;
                                                                                                        var probName = '--select--';
                                                                                                        var x = document.createElement('option');
                                                                                                        var name = document.createTextNode(probName);
                                                                                                        x.appendChild(name);
                                                                                                        x.setAttribute('value', probId)
                                                                                                        list.appendChild(x);
                                                                                                        for (i = 1; i < details.length; i++) {
                                                                                                            temp = details[i].split('-');
                                                                                                            probId = temp[0];
                                                                                                            probName = temp[1];
                                                                                                            x = document.createElement('option');
                                                                                                            name = document.createTextNode(probName);
                                                                                                            x.appendChild(name);
                                                                                                            x.setAttribute('value', probId)
                                                                                                            list.appendChild(x);
                                                                                                        }
                                                                                                    }
                                                                                                }


                                                                                            }



                                                                                            var rowCount = 1;
                                                                                            var sno = 0;
                                                                                            var rowCount1 = 1;
                                                                                            var sno1 = 0;
                                                                                            var httpRequest;
                                                                                            var httpReq;
                                                                                            var styl = "";

                                                                                            function addRow(val) {
                                                                                                if (parseInt(rowCount1) % 2 == 0)
                                                                                                {
                                                                                                    styl = "text2";
                                                                                                } else {
                                                                                                    styl = "text1";
                                                                                                }

                                                                                                var sn = sno - 1;
                                                                                                if (sn >= 0 && $("#netExpense" + sn).val() == '') {
                                                                                                    alert("Please enter the expense");
                                                                                                    $("#netExpense" + sn).focus();
                                                                                                } else {
                                                                                                    sno1++;
                                                                                                    var tab = document.getElementById("suppExpenseTBL");
                                                                                                    //find current no of rows
                                                                                                    var rowCountNew = document.getElementById('suppExpenseTBL').rows.length;
                                                                                                    rowCountNew--;
                                                                                                    var newrow = tab.insertRow(rowCountNew);
                                                                                                    cell = newrow.insertCell(0);
                                                                                                    var cell0 = "<td class='text1' height='25' style='width:10px;'> <input type='hidden' name='editId' id='editId" + sno + "' value=''/>" + sno1 + "</td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(1);
                                                                                                    cell0 = "<td class='text1' height='25'><select class='form-control' id='serviceList" + sno + "' style='width:100px;height:40px'  name='serviceList'  onchange='getSubGroupList(this.value," + sno + ")'><option selected value=0>---Select---</option> <c:if test="${serviceList != null}" ><c:forEach items="${serviceList}" var="sl"><option  value='<c:out value="${sl.serviceTypeId}" />'><c:out value="${sl.serviceTypeName}" /> </c:forEach > </c:if> </select>\n\
                                                                                                    <input type='hidden' name='groupList' id='groupList" + sno + "' value='0'/> <input type='hidden' name='subGroupList' id='subGroupList" + sno + "' value='0'/></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(2);

//                                                                                                    cell0 = "<td class='text1' height='25'><select class='form-control' id='groupList" + sno + "' style='width:150px;height:40px'  name='groupList'  onchange='getSubGroupList(this.value," + sno + ")'><option selected value=0>---Select---</option> <c:if test="${groupList != null}" ><c:forEach items="${groupList}" var="gl"><option  value='<c:out value="${gl.billGroupId}" />'><c:out value="${gl.billGroupName}" /> </c:forEach > </c:if> </select></td>";
//                                                                                                    cell.setAttribute("className", styl);
//                                                                                                    cell.innerHTML = cell0;
//                                                                                                    cell = newrow.insertCell(3);
//
//                                                                                                    cell0 = "<td class='text1' height='25'><select class='form-control' id='subGroupList" + sno + "' style='width:150px;height:40px'  name='subGroupList'><option selected value=0>---Select---</option> </select></td>";
//                                                                                                    cell.setAttribute("className", styl);
//                                                                                                    cell.innerHTML = cell0;
//                                                                                                    cell = newrow.insertCell(4);
                                                                                                    cell0 = "<td class='text1' height='25' ><textarea rows='3' cols='30' class='form-control' style='width:100px;height:40px' name='expenseRemarks' id='expenseRemarks" + sno + "'></textarea></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(3);

                                                                                                    var cell0 = "<input type='text' name='quantity' id='quantity" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='' onkeyup='calculateTotal(" + sno + ")'></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(4);

                                                                                                    var cell0 = "<input type='text' name='partCGST' id='partCGST" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='0' onchange='checkValue(" + sno + ")'></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(5);

                                                                                                    var cell0 = "<input type='text' name='partSGST' id='partSGST" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='0' ></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(6);

                                                                                                    var cell0 = "<input type='text' name='partIGST' id='partIGST" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='0'  ></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(7);




                                                                                                    var cell0 = "<input type='text' name='cost' id='cost" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='' onkeyup='calculateTotal(" + sno + ")'></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(8);

                                                                                                    var cell0 = "<input type='text' name='labourCGST' id='labourCGST" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='0' onkeyup='calculateTotal(" + sno + ")' ></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(9);


                                                                                                    var cell0 = "<input type='text' name='labourSGST' id='labourSGST" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='0' onkeyup='calculateTotal(" + sno + ")' ></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(10);

                                                                                                    var cell0 = "<input type='text' name='labourIGST' id='labourIGST" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='0' onkeyup='calculateTotal(" + sno + ")' ></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(11);

                                                                                                    var cell0 = "<input type='text' name='labour' id='labour" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:150px;height:40px'  value='' onkeyup='calculateTotal(" + sno + ")' ></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    cell = newrow.insertCell(12);

                                                                                                    var cell0 = "<input type='text' name='total' id='total" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:100px;height:40px'  value='' readonly ></td>";
                                                                                                    cell.setAttribute("className", styl);
                                                                                                    cell.innerHTML = cell0;
                                                                                                    $(document).ready(function () {
                                                                                                        $("#datepicker").datepicker({
                                                                                                            showOn: "button",
                                                                                                            buttonImage: "calendar.gif",
                                                                                                            buttonImageOnly: true

                                                                                                        });
                                                                                                    });
                                                                                                    $(function () {
                                                                                                        $(".datepicker").datepicker({
                                                                                                            changeMonth: true, changeYear: true
                                                                                                        });
                                                                                                    });
                                                                                                    rowCount1++;
                                                                                                    sno++;
                                                                                                }
                                                                                            }


                                                                                            function checkValue(sno) {
                                                                                                var cost = document.getElementsByName("cost");
                                                                                                var pSgst = document.getElementsByName("partSGST");
                                                                                                var pCgst = document.getElementsByName("partCGST");
                                                                                                var pIgst = document.getElementsByName("partIGST");
                                                                                                for (var i = 0; i < cost.length; i++) {
                                                                                                    if (cost[i].value != "") {
                                                                                                        var cost = document.getElementsByName("cost").value = "";
                                                                                                    }
                                                                                                }
                                                                                            }





                                                                                            function calculateTotal(sno) {
                                                                                                var cost = document.getElementById("cost" + sno).value;
                                                                                                var labour = document.getElementById("labour" + sno).value;
                                                                                                var total = document.getElementById("total" + sno).value;
                                                                                                if (cost != '' && labour == '') {
                                                                                                    document.getElementById("total" + sno).value = cost;
                                                                                                    //                            calculateTotalCost();
                                                                                                    //                            calculateTotalAmount();
                                                                                                } else if (cost == '' && labour != '') {
                                                                                                    document.getElementById("total" + sno).value = labour;
                                                                                                    //                            calculateTotalLabour();
                                                                                                    //                            calculateTotalAmount();

                                                                                                } else if (cost != '' && labour != '') {
                                                                                                    document.getElementById("total" + sno).value = parseInt(labour) + parseInt(cost);
                                                                                                }
                                                                                                calculateTotalCost();
                                                                                                calculateTotalLabour();
                                                                                                calculateTotalAmount();
                                                                                                calculateInvoiceAmount();
                                                                                                calculateInvoiceAmount();
                                                                                            }

                                                                                            function calculateTotalCost() {
                                                                                                var cost = document.getElementsByName("cost");
                                                                                                var pSgst = document.getElementsByName("partSGST");
                                                                                                var pCgst = document.getElementsByName("partCGST");
                                                                                                var pIgst = document.getElementsByName("partIGST");
                                                                                                var gst = 0;
                                                                                                var exclusivegstAmount = 0;
                                                                                                var gstAmount = 0;
                                                                                                var totalCost = 0;

                                                                                                for (var i = 0; i < cost.length; i++) {
                                                                                                    if (cost[i].value != "") {
                                                                                                        totalCost = parseFloat(cost[i].value) + parseFloat(totalCost);
                                                                                                        gst = parseFloat(pSgst[i].value) + parseFloat(pCgst[i].value) + parseFloat(pIgst[i].value)
                                                                                                        exclusivegstAmount = (parseFloat(cost[i].value) * 100) / (100 + parseFloat(gst))
                                                                                                        gstAmount = parseFloat(cost[i].value) - parseFloat(exclusivegstAmount) + parseFloat(gstAmount)
                                                                                                    }
                                                                                                }
                                                                                                document.getElementById("totalCost").value = parseFloat(totalCost);
                                                                                                document.getElementById("vatAmount").value = parseFloat(gstAmount).toFixed(2);
                                                                                            }
                                                                                            function calculateTotalLabour() {
                                                                                                var labour = document.getElementsByName("labour");
                                                                                                var lSgst = document.getElementsByName("labourSGST");
                                                                                                var lCgst = document.getElementsByName("labourCGST");
                                                                                                var lIgst = document.getElementsByName("labourIGST");
                                                                                                var gst = 0;
                                                                                                var exclusivegstAmount = 0;
                                                                                                var gstAmount = 0;
                                                                                                var totalLabour = 0;

                                                                                                for (var i = 0; i < labour.length; i++) {
                                                                                                    if (labour[i].value != "") {
                                                                                                        totalLabour = parseFloat(labour[i].value) + parseFloat(totalLabour);
                                                                                                        gst = parseFloat(lSgst[i].value) + parseFloat(lCgst[i].value) + parseFloat(lIgst[i].value)
                                                                                                        exclusivegstAmount = (parseFloat(labour[i].value) * 100) / (100 + parseFloat(gst))
                                                                                                        gstAmount = parseFloat(labour[i].value) - parseFloat(exclusivegstAmount) + parseFloat(gstAmount)

                                                                                                    }
                                                                                                }
                                                                                                document.getElementById("totalLabour").value = parseFloat(totalLabour);
                                                                                                document.getElementById("serviceTaxAmount").value = parseFloat(gstAmount).toFixed(2);


                                                                                            }
                                                                                            function calculateTotalAmount() {
                                                                                                var total = document.getElementsByName("total");
                                                                                                var totalAmount = 0;
                                                                                                for (var i = 0; i < total.length; i++) {
                                                                                                    if (total[i].value != "") {
                                                                                                        totalAmount = parseFloat(total[i].value) + parseFloat(totalAmount);
                                                                                                    }
                                                                                                }
                                                                                                document.getElementById("totalAmount").value = parseFloat(totalAmount);
                                                                                            }
                                                                                            function calculateInvoiceAmount() {
                                                                                                var vatAmount = document.getElementById("vatAmount").value;
                                                                                                var serviceTaxAmount = document.getElementById("serviceTaxAmount").value;
                                                                                                var totalAmount = document.getElementById("totalAmount").value;
                                                                                                if (vatAmount == "") {
                                                                                                    vatAmount = 0;
                                                                                                }
                                                                                                if (serviceTaxAmount == "") {
                                                                                                    serviceTaxAmount = 0;
                                                                                                }
                                                                                                if (totalAmount == "") {
                                                                                                    totalAmount = 0;
                                                                                                }
//                                                                                                document.getElementById("invoiceAmount").value = parseFloat(vatAmount) + parseFloat(serviceTaxAmount) + parseFloat(totalAmount);
                                                                                                document.getElementById("invoiceAmount").value = parseFloat(totalAmount);

                                                                                            }


                                                                                                </script>





                                                                                                <br>


                                                                                            <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
                                                                                            </body>
                                                                                            </div>
                                                                                            </div>
                                                                                            </div>
                                                                                            <%@ include file="/content/common/NewDesign/settings.jsp" %>
