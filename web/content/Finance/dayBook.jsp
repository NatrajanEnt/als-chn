

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ page import="ets.domain.util.ThrottleConstants" %>
 <%@ page import="java.text.DecimalFormat" %>
             <%@ page import="java.text.NumberFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>  
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>   
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
          
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function () {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });
        </script>

    <script language="javascript">

        function submitPage1(val)
        {
            if (val == 'fetchData') {
                var ledgerId = document.ledgerReport.ledgerId.value;
                if (ledgerId != '0')
                {
                    document.ledgerReport.action = '/throttle/ledgerReport.do';
                    document.ledgerReport.submit();
                } else {
                    alert('please select ledger');
                    document.lederReport.ledgerReport.focus();
                }
            } else if (val == 'export') {
                var ledgerId = document.ledgerReport.ledgerId.value;
                if (ledgerId != '0')
                {
                    document.ledgerReport.action = '/throttle/exportLedgerReport.do';
                    document.ledgerReport.submit();
                } else {
                    alert('please select ledger');
                    document.lederReport.ledgerReport.focus();
                }
            }

        }
        function setValues() {
            var ledgerId = <%=request.getAttribute("ledgerId")%>;
            var fromDate = '<%=request.getAttribute("fromDate")%>';
            var toDate = '<%=request.getAttribute("toDate")%>';
            if (toDate != 'null') {
                document.ledgerReport.toDate.value = toDate;
            }
            if (fromDate != 'null') {
                document.ledgerReport.fromDate.value = fromDate;
            }
            if (ledgerId != null) {

                document.ledgerReport.ledgerId.value = ledgerId;
            }
        }

//            function submitPage(val){
//                //alert("am here..")
//                if(val == 'fetch'){
//                   document.ledgerReport.action='/throttle/dayBook.do?param=search';
//                   document.ledgerReport.submit();
//                }else if(val == 'export'){
//                     document.ledgerReport.action='/throttle/dayBook.do?param=export';
//                     document.ledgerReport.submit();
//               
//                }
//                
//            }
        function submitPage(value)
        {

            if (value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value == 'First' || value == 'Last') {

                var temp = "";
                if (value == "GoTo") {
                    temp = document.ledgerReport.GoTo.value;
                    if (temp != 'null') {
                        document.ledgerReport.pageNo.value = temp;
                    }
                }
                else if (value == "First") {
                    temp = "1";
                    document.ledgerReport.pageNo.value = temp;
                    value = 'GoTo';
                } else if (value == "Last") {
                    temp = document.ledgerReport.last.value;
                    document.ledgerReport.pageNo.value = temp;
                    value = 'GoTo';
                }
                document.ledgerReport.button.value = value;
                document.ledgerReport.action = "/throttle/dayBook.do?param=search";
                document.ledgerReport.submit();
            }
            else if (value == 'export') {
                document.ledgerReport.action = '/throttle/dayBook.do?param=export';
                document.ledgerReport.submit();

            }

        }
        // }

    </script>
    
    
    <style>
    #index td {
   color:white;
}
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.DayBook"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
       <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.DayBook"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    
    
    
    <body onload="setValues();">
        <%//System.out.println(request.getAttribute("fromDate"));
        %>
        <form name="ledgerReport" method="post" >
          


            <%

             DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
             Date date = new Date();
             String fDate = "";
             String tDate = "";
             if(request.getAttribute("fromDate") != null) {
                 fDate = (String) request.getAttribute("fromDate");
             }else{
                 fDate = dateFormat.format(date);
             }
             if(request.getAttribute("toDate") != null) {
                 tDate = (String)request.getAttribute("toDate");
             }else{
                 tDate = dateFormat.format(date);
             }

            %>
            <center>
                <h2><%=ThrottleConstants.companyName%></h2>
                <table class="table table-info mb30 table-hover">
                    <thead>
                         <tr height="30" id="index">
                             <th colspan="4"  ><b><spring:message code="finance.label.DayBook"  text="default text"/></b></th>&nbsp;&nbsp;&nbsp;                    
                         </tr>
                    </thead>
                
                    <tr>
                        <td><spring:message code="finance.label.FromDate"  text="default text"/>:</td>
                        <td><input type="textbox" class='datepicker' value="<%=fDate%>" name="fromDate" style="width:260px;height:40px;"/></td>
                        <td><spring:message code="finance.label.ToDate"  text="default text"/>:</td>
                        <td><input type="textbox" class='datepicker' value="<%=tDate%>" name="toDate" style="width:260px;height:40px;"/></td>
                    </tr>
                </table>
                <input type="button"  value="<spring:message code="finance.label.FetchData"  text="default text"/>" name="search" class="btn btn-success" name="Fetch Data" onClick="submitPage(this.name)" style="width:100px;height:35px;">
                <input type="button" class="btn btn-success" onclick="tableToExcel('table', 'State Master')" value="<spring:message code="finance.label.Excel"  text="default text"/>" style="width:100px;height:35px;">
            </center>
            <div align="center">
                    <table class="table table-info mb30 table-hover"  id="table">
                        <thead>
                        <tr id="index">
                            <th style="background-color:#5BC0DE;" width="10%" ><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th style="background-color:#5BC0DE;" width="10%" ><spring:message code="finance.label.Date"  text="default text"/></th>
                            <th style="background-color:#5BC0DE;" width="70%"><spring:message code="finance.label.Particulars"  text="default text"/></th>                            
                            <th style="background-color:#5BC0DE;" width="65%"><spring:message code="finance.label.Narration"  text="default text"/></th>                            
                            <th style="background-color:#5BC0DE;" width="15%"><spring:message code="finance.label.TripNo"  text="default text"/></th>                            
                            <th style="background-color:#5BC0DE;" width="65%"><spring:message code="finance.label.VehicleNo"  text="default text"/></th>
                            <th style="background-color:#5BC0DE;" width="10%"><spring:message code="finance.label.Debit"  text="default text"/></th>
                            <th style="background-color:#5BC0DE;" width="10%"><spring:message code="finance.label.Credit"  text="default text"/></th>
                        </tr>
                        </thead>
                        <c:set var="creditTotal" value="${0.00}" />
                        <c:set var="debitTotal" value="${0.00}" />
                        <%
                       // String openingBalance = (String)request.getAttribute("openingBalance");
                       // String amountType = (String)request.getAttribute("amountType");
                       // String debitOpen = "";
                       // String creditOpen = "";
                        int cntr = 0;
                        %>
  <c:if test = "${ledgerTransactionList != null}" >
                        <c:forEach items="${ledgerTransactionList}" var="txnlist">
                            <tr>
                                <% cntr++; %>
                                <td  ><%=cntr%></td>
                                <td  ><c:out value="${txnlist.accountEntryDate}" /></td>
                                <c:if test = "${txnlist.accountType == 'CREDIT'}" >

                                    <c:set var="creditTotal" value="${creditTotal + txnlist.accountAmount}"/>
                                     <td  ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C</td>
                                         <td  ><c:out value="${txnlist.accountNarration}" /></td>
                                           <td  ><c:out value="${txnlist.tripId}" /></td>
                                     <td  width="95%"><c:out value="${txnlist.vehicleNo}" /></td> 
                                    <td  align ="right">&nbsp;</td>
                                      <td  align ="right"><fmt:formatNumber pattern="##0.00" value="${txnlist.accountAmount}"/></td>                                                                        
                                </c:if>
                                <c:if test = "${txnlist.accountType == 'DEBIT'}" >
                                    <c:set var="debitTotal" value="${debitTotal + txnlist.accountAmount}"/>
                                    <td  ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C</td>
                                        <%--&nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>--%> 
                                        <td  ><c:out value="${txnlist.accountNarration}" /></td>
                                          <td  ><c:out value="${txnlist.tripId}" /></td>
                                     <td  width="95%"><c:out value="${txnlist.vehicleNo}" /></td> 
                                    <td  align ="right"><fmt:formatNumber pattern="##0.00" value="${txnlist.accountAmount}"/></td>
                                    <td  align ="right">&nbsp;</td>
                                </c:if>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td colspan="6" align="right"   height="30" style="color: black"><b><spring:message code="finance.label.SubTotal"  text="default text"/></b></td>
                            <td   align="right"  height="30" style="color: black"><b>
                                    <fmt:formatNumber pattern="##0.00" value="${debitTotal}"/>
                                    <%--<fmt:formatNumber type="number"  maxFractionDigits="2" value="${debitTotal}" />--%>
                                </b></td>
                            <td   align="right"  height="30" style="color: black"><b>
                                     <fmt:formatNumber pattern="##0.00" value="${creditTotal}"/>
                                </b></td>
                        </tr>
                        <%--
                      <c:if test = "${creditTotal > debitTotal}" >
                          <tr>
                              <td colspan="3" align="right"   height="30"><b>Closing Balance</b></td>
                              <td   align="right"  height="30">&nbsp;</td>
                              <td   align="right"  height="30"><b><c:out value="${creditTotal-debitTotal}" /> &nbsp;Cr.</b></td>
                          </tr>
                      </c:if>
                      <c:if test = "${creditTotal < debitTotal}" >
                          <tr>
                              <td colspan="3" align="right"   height="30"><b>Closing Balance</b></td>
                              <td   align="right"  height="30"><b><c:out value="${debitTotal - creditTotal}" /> &nbsp;Dr.</b></td>
                              <td   align="right"  height="30">&nbsp;</td>
                          </tr>
                      </c:if>
                      <c:if test = "${creditTotal < debitTotal}" >
                          <tr>
                              <td colspan="3" align="right"   height="30"><b>Closing Balance</b></td>
                              <td   align="right"  height="30"><b><c:out value="${debitTotal - creditTotal}" /> &nbsp;Dr.</b></td>
                              <td   align="right"  height="30">&nbsp;</td>
                          </tr>
                      </c:if>
                      <c:if test = "${creditTotal == debitTotal}" >
                          <tr>
                              <td colspan="3" align="right"   height="30"><b>Closing Balance</b></td>
                              <td   align="right"  height="30"><b><c:out value="${debitTotal - creditTotal}" /> &nbsp;Dr.</b></td>
                              <td   align="right"  height="30">&nbsp;</td>
                          </tr>
                      </c:if>
                        --%>


                        <%--modified on 20th April 2013
                        <c:forEach items="${ledgerTransactionList}" var="txnlist">
                            <tr>
                                <td  ><c:out value="${txnlist.accountEntryDate}" /></td>
                            <c:if test = "${txnlist.accountType == 'DEBIT'}" >
                                <td  ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Dr.</td>
                                <td  align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                                <td  align ="right">&nbsp;</td>
                            </c:if>
                            <c:if test = "${txnlist.accountType == 'CREDIT'}" >
                                <td  >&nbsp;&nbsp;&nbsp;To &nbsp;<c:out value="${txnlist.ledgerName}" />&nbsp; A/C <br>
                                                    &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>
                                <td  align ="right">&nbsp;</td>
                                <td  align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                            </c:if>
                        </tr>
                        </c:forEach>
                        --%>
                       <c:if test = "${totalAmountList != null}" >
                            <tr>
                                <td colspan="6" align="right"   height="30" style="color: black"><b><spring:message code="finance.label.Total"  text="default text"/></b></td>
                                <td   align="right"  height="30" colspan="1" style="color: black"><b>
                                        <%--<fmt:formatNumber value="${totalDebitAmount}" pattern="##0.00"/>--%>
                                        <%--<fmt:formatNumber type="number"  maxFractionDigits="2" value="${totalDebitAmount}" />--%>
                                         <fmt:formatNumber pattern="##0.00" value="${totalDebitAmount}"/>
                                    </b></td>
                                <td   align="right"  height="30" style="color: black"><b> 
                                        <%--<fmt:formatNumber type="number"  maxFractionDigits="2" value="${totalCreditAmount}" />--%>
                                        <fmt:formatNumber pattern="##0.00" value="${totalCreditAmount}"/>
                                    </b></td>
                            </tr>
                        </c:if>
                    </table>

                </c:if>
    <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
            </div>
            <br>
            <%@ include file="/content/common/pagination.jsp"%>  
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>