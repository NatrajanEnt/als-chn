
<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script>
            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#preStartLocationId').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityFromName.do",
                            dataType: "json",
                            data: {
                                cityFrom: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#preStartLocation').val(tmp[0]);
                        $('#preStartLocationId').val(tmp[1]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
                };
            });
        </script>
         <script type="text/javascript">
        function calculateDate(){
        var endDates = document.getElementById("tripScheduleDate").value;
        var tempDate1 = endDates.split("-");
        var stDates = document.getElementById("preStartDate").value;
        var tempDate2 = stDates.split("-");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        if(prevTime.getTime() < thisTime.getTime()){
            if(difference > 0){
            alert(" Selected Date is greater than scheduled date ");
            document.getElementById("preStartDate").value = document.getElementById("todayDate").value;
        }
        }
        }
         function calculateTime(){
        var endDates = document.getElementById("tripScheduleDate").value;
        var endTimeIds = document.getElementById("tripScheduleTime").value;
        var tempDate1 = endDates.split("-");
        var tempTime3 = endTimeIds.split(" ");
        var tempTime1 = tempTime3[0].split(":");
        if(tempTime3[1] =="PM"){
          tempTime1[0] = 12 + parseInt(tempTime1[0]);
        }
        var stDates = document.getElementById("preStartDate").value;
        var hour = document.getElementById("tripPreStartHour").value;
        var minute = document.getElementById("tripPreStartMinute").value;
        var stTimeIds = hour + ":" + minute + ":" + "00";
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2],tempDate2[1],tempDate2[0],tempTime2[0],tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2],tempDate1[1],tempDate1[0],parseInt(tempTime1[0]),tempTime1[1]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference/1000/60/60);
          if(prevTime.getTime() < thisTime.getTime()){
         if(hoursDifference > 0){
            alert("Selected Time is greater than scheduled Time ");
            document.getElementById("tripPreStartHour").focus();
                 }
          }
     }
        </script>

        <script language="">

            function submitPage(){
                var errorMsg1="";
                if(isEmpty(document.getElementById("preStartDate").value)){
                     alert('please enter pre Start date date');
                    document.getElementById("preStartDate").focus();
                    return;
                }
                if(isEmpty(document.getElementById("preOdometerReading").value)){
                     alert('please enter pre odometer reading');
                    document.getElementById("preOdometerReading").focus();
                    return;
                }
                if(isEmpty(document.getElementById("preStartHM").value)){
                     alert('please enter pre start hour reading');
                    document.getElementById("preStartHM").focus();
                    return;
                }
                if(parseFloat(document.getElementById("preOdometerReading").value) == 0){
                    errorMsg1 += "The value of start odometer meter reading is 0\n";
                }
                if(parseFloat(document.getElementById("preStartHM").value )== 0){
                    errorMsg1 += "The value of start hour meter  reading is 0";
                }
                if(errorMsg1 != ""){
                var r=confirm(errorMsg1);
                if (r==true)
              {
               document.trip.action = '/throttle/savePreTripSheet.do';
                document.trip.submit();
              }
                }else{
               document.trip.action = '/throttle/savePreTripSheet.do';
                document.trip.submit();
                }
            }

        </script>
    </head>
    <body >

        <form name="trip" method="post">
            <%
                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String preStartDate = sdf.format(today);
            %>
            <%@ include file="/content/common/path.jsp" %>
            <br>
          <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">
                                <c:if test = "${tripDetails != null}" >
                                    <c:forEach items="${tripDetails}" var="trip">
                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                        <td> <c:out value="${trip.orderRevenue}" /></td>

                                     </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                        <td> <c:out value="${trip.orderExpense}" /></td>
                                        <input type="hidden" name="estimatedAdvancePerDay" value='<c:out value="${trip.estimatedAdvancePerDay}" />'>

                                     </tr>
                                    <c:set var="profitMargin" value="" />
                                     <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                     <c:set var="orderExpense" value="${trip.orderExpense}" />
                                     <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                                     <%
                                     String profitMarginStr = "" + (Double)pageContext.getAttribute("profitMargin");
                                     String revenueStr = "" + (String)pageContext.getAttribute("orderRevenue");
                                     float profitPercentage = 0.00F;
                                     if(!"".equals(revenueStr) && !"".equals(profitMarginStr)){
                                         profitPercentage = Float.parseFloat(profitMarginStr)*100/Float.parseFloat(revenueStr);
                                     }


                                     %>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                        <td>  <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)
                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                        <td>

                                        <td>
                                     </tr>
                                </table>
                                </c:forEach>
                             </c:if>
                            </div>
                        </div>

                    </td>
                </tr>
            </table>
            <br>
            <br>
            <br>
            <br>
            <br>
            <table width="100%">
                <% int loopCntr = 0;%>
                <c:if test = "${tripDetails != null}" >
                    <c:forEach items="${tripDetails}" var="trip">
                        <% if(loopCntr == 0) {%>
                        <tr>
                            <td class="contenthead" >Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                            <td class="contenthead" >Trip Code: <c:out value="${trip.tripCode}"/></td>
                            <td class="contenthead" >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                            <td class="contenthead" >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                            <td class="contenthead" >Status: <c:out value="${trip.status}"/></td>
                        </tr>
                        <% }%>
                        <% loopCntr++;%>
                    </c:forEach>
                </c:if>
            </table>
            <div id="tabs" >
                <ul>

                    <li><a href="#preStart"><span>PreStart</span></a></li>
                    <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                    <li><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                    <li><a href="#statusDetail"><span>Status History</span></a></li>
                    <!--
                    <li><a href="#cleanerDetail"><span>Cleaner</span></a></li>-->
                    <!--                    <li><a href="#advDetail"><span>Advance</span></a></li>-->
                    <!--<li><a href="#expDetail"><span>Expense Details</span></a></li>-->
                    <!--                    <li><a href="#summary"><span>Remarks</span></a></li>-->
                </ul>

                  <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="6" >Trip Details</td>
                        </tr>

                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">


                        <tr>
<!--                            <td class="text1"><font color="red">*</font>Trip Sheet Date</td>
                            <td class="text1"><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                            <td class="text1">Consignment No(s)</td>
                            <td class="text1">
                                <c:out value="${trip.cNotes}" />
                            </td>
                            <td class="text1">Billing Type</td>
                            <td class="text1">
                                <c:out value="${trip.billingType}" />
                            </td>
                        </tr>
                        <tr>
<!--                            <td class="text2">Customer Code</td>
                            <td class="text2">BF00001</td>-->
                            <td class="text2">Customer Name</td>
                            <td class="text2">
                                <c:out value="${trip.customerName}" />
                                <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                            </td>
                            <td class="text2">Customer Type</td>
                            <td class="text2" colspan="3" >
                                <c:out value="${trip.customerType}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text1">Route Name</td>
                            <td class="text1">
                                <c:out value="${trip.routeInfo}" />
                            </td>
<!--                            <td class="text1">Route Code</td>
                            <td class="text1" >DL001</td>-->
                            <td class="text1">Reefer Required</td>
                            <td class="text1" >
                                <c:out value="${trip.reeferRequired}" />
                            </td>
                            <td class="text1">Order Est Weight (MT)</td>
                            <td class="text1" >
                                <c:out value="${trip.totalWeight}" />
                            </td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Type</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleTypeName}" />
                            </td>
                            <td class="text2"><font color="red">*</font>Vehicle No</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleNo}" />

                            </td>
                            <td class="text2">Vehicle Capacity (MT)</td>
                            <td class="text2">
                                <c:out value="${trip.vehicleTonnage}" />

                            </td>
                        </tr>

                        <tr>
                            <td class="text1">Veh. Cap [Util%]</td>
                            <td class="text1">
                                <c:out value="${trip.vehicleCapUtil}" />
                            </td>
                            <td class="text1">Special Instruction</td>
                            <td class="text1">-</td>
                            <td class="text1">Trip Schedule</td>
                          <td class="text1"><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                             <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                            <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'></td>
                        </tr>


                        <tr>
                            <td class="text2"><font color="red">*</font>Driver </td>
                            <td class="text2" colspan="5" >
                                 <c:out value="${trip.driverName}" />
                            </td>

                        </tr>
                        <tr>
                            <td class="text1">Product Info </td>
                            <td class="text1" colspan="5" >
                                 <c:out value="${trip.productInfo}" />
                            </td>

                        </tr>
                            </c:forEach>
                       </c:if>
                    </table>
                    <br/>

                    
                    <br/>
  <c:if test = "${expiryDateDetails != null}" >
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="4" >Vehicle Compliance Check</td>
                        </tr>
                          <c:forEach items="${expiryDateDetails}" var="expiryDate">
                        <tr>
                            <td class="text2">Vehicle FC Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text1">Vehicle Insurance Valid UpTo</td>
                            <td class="text1"><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Vehicle Permit Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                        </tr>
                        <tr>
                            <td class="text2">Road Tax Valid UpTo</td>
                            <td class="text2"><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                        </tr>
                        </c:forEach>
                    </table>
                    <br/>
                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                       </c:if>

                </div>
                <div id="routeDetail">

                    <c:if test = "${tripPointDetails != null}" >
                    <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                        <tr >
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Point Name</td>
                                <td class="contenthead" height="30" >Type</td>
                                <td class="contenthead" height="30" >Route Order</td>
                                <td class="contenthead" height="30" >Address</td>
                                <td class="contenthead" height="30" >Planned Date</td>
                                <td class="contenthead" height="30" >Planned Time</td>
                      </tr>
                        <% int index2 = 1; %>
                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                         <%
                                        String classText1 = "";
                                        int oddEven = index2 % 2;
                                        if (oddEven > 0) {
                                            classText1 = "text1";
                                        } else {
                                            classText1 = "text2";
                                        }
                            %>
                        <tr >
                                 <td class="<%=classText1%>" height="30" ><%=index2++%></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointAddress}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                <td class="<%=classText1%>" height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                      </tr>
                        </c:forEach >
                    </table>
                    <br/>

                    <table border="0" class="border" align="centver" width="100%" cellpadding="0" cellspacing="0" >
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <tr>
                                        <td class="text1" width="150"> Estimated KM</td>
                                        <td class="text1" width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                        <td class="text1" colspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="text2" width="150"> Estimated Reefer Hour</td>
                                        <td class="text2" width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                        <td class="text2" colspan="4">&nbsp;</td>
                                    </tr>

                                </c:forEach>
                            </c:if>
                        </table>
                    <br/>
                    <br/>
                    <center>
                            <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                        </center>
                    </c:if>
                    <br>
                    <br>
                </div>
                 <div id="statusDetail">
                    <% int index1 = 1; %>

                    <c:if test = "${statusDetails != null}" >
                        <table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                            <tr >
                                <td class="contenthead" height="30" >S No</td>
                                <td class="contenthead" height="30" >Status Name</td>
                                <td class="contenthead" height="30" >Remarks</td>
                                <td class="contenthead" height="30" >Created User Name</td>
                                <td class="contenthead" height="30" >Created Date</td>
                            </tr>
                            <c:forEach items="${statusDetails}" var="statusDetails">
                                 <%
                                        String classText = "";
                                        int oddEven1 = index1 % 2;
                                        if (oddEven1 > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                                <tr >
                                    <td class="<%=classText%>" height="30" ><%=index1++%></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.statusName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripRemarks}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.userName}" /></td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${statusDetails.tripDate}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                        <br/>
                        <br/>
                    </c:if>
                    <br>
                    <br>
                </div>

                <div id="preStart">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" colspan="8" >Trip Pre Start Details</td>
                        </tr>

                        <tr>
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                            <td class="text1">Trip Planned Pre Start Date</td>
                            <td class="text1"> <c:out value="${trip.preStartLocationPlanDate}" /></td>
                            <td class="text1" >Trip Planned Pre Start Time</td>
                            <td class="text1"> <c:out value="${trip.preStartLocationPlanTime}" /></td>
                            <td class="text1">Pre Start Location / Distance</td>
                            <td class="text1"> <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                </c:forEach>
                            </c:if>

                        </tr>
                        <tr>

                            <td class="text1"><font color="red">*</font>Trip Actual Pre Start Date</td>
                            <td class="text1"><input type="text" name="preStartDate" id="preStartDate" class="datepicker" value="<%=preStartDate%>" onchange="calculateDate();">
                            <input type="hidden" name="todayDate" id="todayDate" value="<%=preStartDate%>"/></td>
                            <td class="text1" ><font color="red">*</font>Trip Actual Pre Start Time</td>
                            <td class="text1" height="25" colspan="3" align="left" >   HH:<select name="tripPreStartHour" id="tripPreStartHour" class="textbox" onchange="calculateTime();">
                                    <option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="tripPreStartMinute" id="tripPreStartMinute" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>

                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Trip Pre Start Odometer Reading(KM)</td>
                            <td class="text1"><input type="text" name="preOdometerReading" id="preOdometerReading" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" value=""></td>
                            <td class="text1"><font color="red">*</font>Trip Pre Start Reefer Reading(HM)</td>
                            <td class="text1"><input type="text" name="preStartHM"  id="preStartHM" onKeyPress="return onKeyPressBlockCharacters(event);" class="textbox" value="0"></td>
<!--                            <td class="text1"><font color="red">*</font>Trip Pre Start Location</td>
                            <td class="text1"><input type="hidden" name="preStartLocation" id="preStartLocation" class="textbox" value=""><input type="text" name="preStartLocationId" id="preStartLocationId" class="textbox" value=""></td>-->
                            <td class="text1" >Pre Start Trip Remarks</td>
                            <td class="text1" >

                                <textarea rows="3" cols="30" class="textbox" name="preTripRemarks" id="preTripRemarks"   style="width:142px"></textarea>
                                <input type="hidden" name="tripSheetId" value="<%=request.getParameter("tripSheetId")%>" />
                        </tr>
                        <!--                        <tr>
                                                     <td class="text1">Trip Status</td>
                                                 <td> <select name="statusId" id="statusId" class="textbox"  style="width:125px;">
                                                <option value="0"> -Select- </option>
                                                <option value="6" selected>Trip Pre Start </option>
                                                </select></td>

                                                </tr>-->
                    </table>
                    <br/>
                    <br/>

                    <br/>

                    <center>
                        <input type="button" class="button" name="Save" value="Save" onclick="submitPage();"/>
                    </center>
                </div>


            </div>
                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>