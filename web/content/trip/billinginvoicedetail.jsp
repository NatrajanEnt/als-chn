<%-- 
    Document   : billingdetail
    Created on : Dec 10, 2013, 2:58:42 PM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script>
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
            function goback(){
                history.go(-1);
            }
        </script>
    </head>
    <body>
        <form name="viewbillGeneration" method="post">
            <br>
            Finance >> Generate Invoice
            
            <c:set var="routeInfo" value="" />
            <c:set var="VehicleInfo" value="" />
            <div id="print" >
                <font style="font-size:12px;font-family:Arial;">
                <c:if test="${invoiceHeader !=null}">
                    <c:forEach items="${invoiceHeader}" var="headerInfo">
                        <table style="width:800px;" align="center" cellpadding="0" cellspacing="0" border="0">
                            <tr>
                                <td align="left" width="40%" ><img src="/throttle/images/Brattle_Logo.png" alt="throttle Logo" width="88" height="75" /></td>
                                <td align="left" >&nbsp;&nbsp;&nbsp;&nbsp;<font style="font-size:18px;font-family:Arial;">Invoice</font></td>
                            </tr>
                        </table>
                        <br>
                        <table  align="center" cellpadding="0" cellspacing="0" border="0"  rules="all" frame="hsides" style="border-collapse: collapse;width:800px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;" >

                            <tr>
                                <td valign="top" width="40%" >
                                    <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0" style="border-spacing:0px;border-collapse: collapse;font-size:12px;font-family:Arial;" >
                                        <tr>
                                            <td width="50%"><b><h4>Freight Logistics Pvt Ltd.</h4></b></td>
                                        </tr>
                                        <tr>
                                            <td width="50%">15/1, Asaf Ali Road,</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">New Delhi,</td>
                                        </tr>
                                        <tr>
                                            <td width="50%">110002</td>
                                        </tr>
                                    </table>
                                </td>
                                <td  width="60%" valign="top">

                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="1"  style="border-spacing:0px;border-collapse: collapse;border-color:#000000;font-size:12px;font-family:Arial;" >
                                        <tr>
                                            <td width="50%">Invoice No :<br><b><c:out value="${headerInfo.invoiceNo}"/></b></td>
                                            <td>Dated :<br><b><c:out value="${headerInfo.billDate}"/></b></td>
                                        </tr>
                                        <tr>
                                            <td>Delivery Note:<br><b><c:out value="${headerInfo.vehicleNo}"/></b></td>
                                            <c:set var="VehicleInfo" value="${headerInfo.vehicleNo}" />
                                            <td>Mode/Terms Of Payment</td>
                                        </tr>
                                        <tr>
                                            <td>Suppliers's Ref. <br><b><c:out value="${headerInfo.cNotes}"/></b></td>
                                            <td>Other Reference(s)</td>
                                        </tr>
                                    </table>
                                </td>


                            </tr>
                            <tr>
                                <td valign="top">
                                    <table width="100%" align="left" cellpadding="0" cellspacing="0" border="0" style="font-size:12px;font-family:Arial;">
                                        <tr>
                                            <td width="50%"><b>Buyer</b></td>
                                        </tr>
                                        <tr>
                                            <td width="50%"><h4>M/S <c:out value="${headerInfo.customerName}"/></h4></td>
                                        </tr>
                                        <tr>
                                            <td width="50%"><c:out value="${headerInfo.customerAddress}"/></td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="1" style="border-collapse: collapse;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;" >
                                        <tr>
                                            <td>Buyer's Order No :<b></b></td>
                                            <td>Dated :<b></b></td>
                                        </tr>
                                        <tr>
                                            <td width="50%">Delivery Document No:<br><b><c:out value="${headerInfo.vehicleNo}"/></b></td>
                                            <td>Dated :<br><b><c:out value="${headerInfo.tripEndDate}"/></b></td>
                                        </tr>
                                        <tr>
                                            <td>Despatched through. <b></b></td>
                                            <td>Destination :<br><b><c:out value="${headerInfo.routeInfo}"/></b></td>
                                            <c:set var="routeInfo" value="${headerInfo.routeInfo}" />
                                        </tr>

                                    </table>
                                </td>


                            </tr>
                            <tr>
                                <td valign="top">
                                    &nbsp;
                                </td>
                                <td>
                                    <table width="95%" align="center" cellpadding="0" cellspacing="0" border="0" style="border-color:#000000;border-style:solid;font-size:12px;font-family:Arial; " >
                                        <tr>
                                            <td colspan="2" >Term Of Delivery</td>
                                        </tr>
                                        <tr><td colspan="2" style="text-align: right;">&nbsp;</td></tr>
                                        <tr>
                                            <td colspan="2" ><b>SALES</b></td>
                                        </tr>

                                    </table>
                                </td>


                            </tr>
                        </table>
                </c:forEach>
              </c:if>
                <c:set var="grandTotal" value="0" />
                <c:set var="subTotal" value="0" />
                    <c:if test="${invoiceDetails != null}">
                <table  align="center" cellpadding="0" cellspacing="0" border="0" rules="all" frame="hsides" style="border-collapse: collapse;width:800px;border:1px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;" >
                    <tr>
                    <b>
                        <td >S.No</td>
                        <td >Description Of Goods</td>
                        <td >Quantity</td>
                        <td >Rate</td>
                        <td align='right' >Amount</td>
                    </b>
                    </tr>
                    <%int sno=1;%>
                    <c:forEach items="${invoiceDetails}" var="invoicedetailList">
                        <tr style="height: 150px;" valign="top">
                        <td><%=sno++%></td>
                        <td>
                            <%--<c:out value="${invoicedetailList.routeInfo}"/> --%>
                            <%
                                     String routeInfo = (String)pageContext.getAttribute("routeInfo");
                                     String temp = "";
                                     int len = routeInfo.length();
                                     System.out.println("length:"+len);
                                     boolean status = true;
                                     int cntr = 0;
                                     int lengthPerLine = 60;
                                     while(status){
                                         System.out.println("length in loop:"+len+ " cntr "+ cntr+ " temp "+ temp);
                                         if(len >= lengthPerLine){
                                             temp = temp + "<br>" + routeInfo.substring(cntr*lengthPerLine, ((cntr*lengthPerLine)+ lengthPerLine));
                                             cntr++;
                                             len = len - lengthPerLine;
                                         }else{
                                             temp = temp + "<br>" + routeInfo.substring(cntr*lengthPerLine, routeInfo.length());
                                             status = false;
                                         }
                                             
                                     }
                                    System.out.println("temp: "+temp);
                            %>
                            <%=temp%>
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align='right'><c:out value="${invoicedetailList.freightAmount}"/></td>
                        <c:set var="grandTotal" value="${invoicedetailList.freightAmount + grandTotal}" />
                    </tr>
                    </c:forEach>
                    
                    <tr>
                        <td colspan="4" style="text-align: right;">Sub Total </td>
                        <%
                        NumberFormat formatter2 = new DecimalFormat("#0.00");
                        String subTot = "" + (Double)pageContext.getAttribute("grandTotal");
                            float subTotal = 0.00F;
                            subTotal = Float.parseFloat(subTot);
                        %>

                        <td align="right" ><b><%=formatter2.format(java.lang.Math.ceil(subTotal)) %> </b></td>
                    </tr>

                </table>
                    </c:if>



                    <c:if test="${invoiceDetailExpenses != null}">
                <table  align="center" cellpadding="0" cellspacing="0" border="0" rules="all" frame="hsides" style="border-collapse: collapse;width:800px;border:1px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;" >
                    <tr>
                        <td  >S.No</td>
                        <td >Description Of Service</td>
                        <td >Quantity</td>
                        <td >Rate</td>
                        <td align='right' >Amount</td>
                    </tr>
                    <%int sno=1;%>
                    <c:forEach items="${invoiceDetailExpenses}" var="invoicedetailExpList">
                    <tr>
                        <td><%=sno++%></td>
                        <td><c:out value="${invoicedetailExpList.expenseRemarks}"/></td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td align='right'>
                            <c:out value="${invoicedetailExpList.totalValue}"/>
                        </td>
                    </tr>
                    <c:if test="${invoicedetailExpList.taxPercentage != '0'}">
                        <tr>
                            <td>&nbsp;</td>
                            <td align="right" > @ <c:out value="${invoicedetailExpList.taxPercentage}"/> % Tax</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align='right'><c:out value="${invoicedetailExpList.taxValue}"/></td>
                        </tr>
                    </c:if>
                        <c:set var="subTotal" value="${invoicedetailExpList.nettValue + subTotal}" />
                        <c:set var="grandTotal" value="${invoicedetailExpList.nettValue + grandTotal}" />
                    
                    </c:forEach>
                    <tr>
                        <td colspan="4" style="text-align: right;">Sub Total </td>

                        <%
                        NumberFormat formatter2 = new DecimalFormat("#0.00");
                        String subTot = "" + (Double)pageContext.getAttribute("subTotal");
                            float subTotal = 0.00F;
                            subTotal = Float.parseFloat(subTot);
                        %>

                        <td align="right" ><b><%=formatter2.format(java.lang.Math.ceil(subTotal)) %> </b></td>
                    </tr>
                    <tr>
                        <td colspan="4" style="text-align: right;">Total Charges</td>
                        <%
                         formatter2 = new DecimalFormat("#0.00");
                         subTot = "" + (Double)pageContext.getAttribute("grandTotal");
                            subTotal = 0.00F;
                            subTotal = Float.parseFloat(subTot);
                        %>

                        <td align="right" ><b><%=formatter2.format(java.lang.Math.ceil(subTotal)) %> </b></td>
                    </tr>
                </table>
                    </c:if>
                <table align="center" cellpadding="0" cellspacing="0" style="width:800px;border:1px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;">
                    <tr>
                        <td><lable>Amount Chargeable (in words)</lable><br>
                    <b>Rupees:</b><%
                            String grandTot = "" + (Double)pageContext.getAttribute("grandTotal");
                            float spareTotal = 0.00F;
                            spareTotal = Float.parseFloat(grandTot);
                        %>
                        <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                            <% spareTotalRound.setRoundedValue(String.valueOf(spareTotal));%>                            
                            <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                            <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                            <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; Only</b>
                        </jsp:useBean>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <lable>Remarks:</lable><br>
                    <lable>Being Amount provided for the Freight charges from </lable><br>
                    <lable><c:out value="${routeInfo}"/> vide vehicle no. <c:out value="${VehicleInfo}"/></lable><br>
                    <lable>Company service Tax No : <b>AABCL5755FSD002</b> </lable>
                    <lable>Company's PAN : <b>AABCL5755F</b> </lable><br>
                    <lable>Declaration: </lable><br>
                    <lable>Service Tax payable by Consignor / Consignee.</lable>
                    <lable>This is to certify and confirm that we have not availed. </lable>
                    <lable>Cenvat credit of duty on inputs and capital goods and </lable>
                    <lable>Providing transport service to you under the provision of </lable>
                    <lable>the cenvat credit rules 2004 and also have not availed.</lable><br>
                    <lable>Benefit of notification no. 12/2003 Service tax dated june</lable>
                    <lable>2003 (ide Notification no. 32/2004 Service tax dated on 03/12/2004)</lable><br>
                        </td>
                    </tr>
                    <tr><td align="right"> &nbsp;</td></tr>
                    <tr><td align="right"> &nbsp;</td></tr>
                    <tr><td align="right"> &nbsp;</td></tr>
                    <tr><td align="right"> &nbsp;</td></tr>
                    <tr><td align="right"> 
                            <table cellpadding="0" cellspacing="0" border="1" rules="all" style="border-collapse: collapse;" width="50%">
                                <tr>
                                    <td style="text-align: right"><b>for Freight Logistics Pvt Ltd.</b>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        Authorised Signatory.</td>
                                </tr>
                            </table></td></tr>

            </table>
                        <br>
                        <center>This is a Computer Generated Invoice</center>
            </font>
            </div>
            <br>
            <br>

            <center>
                <input type="button" class="button"  value="Print" onClick="print('print');" >&nbsp;&nbsp;&nbsp;<input type="button" class="button"  value="Back" onClick="goback();" >
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>