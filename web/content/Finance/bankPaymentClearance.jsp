


        <%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function () {

                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    dateFormat: 'dd-mm-yy',
                    changeMonth: true, changeYear: true
                });

            });
        </script>
        <script type="text/javascript">
            function getBankBranch(bankId) {
                var temp = "";
                $.ajax({
                    url: '/throttle/getBankBranchDetails.do',
                    data: {bankId: bankId},
                    dataType: 'json',
                    success: function (temp) {
//                                         alert(temp);
                        if (temp != '') {
                            $('#bankBranchId').empty();
                            $('#bankBranchId').append(
                                    $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                                    )
                            $.each(temp, function (i, data) {
                                $('#bankBranchId').append(
                                        $('<option value="' + data.branchId + "~" + data.bankLedgerId + '" style="width:150px"></option>').val(data.branchId + "~" + data.bankLedgerId).html(data.branchName)
                                        )
                            });
                        } else {
                            $('#bankBranchId').empty();
                        }
                    }
                });
            }
            function clearCheque() {
                var clearanceDate = document.getElementById('clearanceDate').value;
                var clearanceStatus = document.getElementById('clearanceStatus').value;
                var clearanceremark = document.getElementById('clearanceremark').value;
                var sno = document.getElementById('sno').value;
                var bankPaymentId = document.getElementById('bankPaymentId' + sno).value;
                var debitLedgerId = document.getElementById('debitLedgerId' + sno).value;
                var creditLedgerId = document.getElementById('creditLedgerId' + sno).value;
                var chequeAmount = document.getElementById('chequeAmount' + sno).value;
                var chequeNo = document.getElementById('chequeNo' + sno).value;
                var chequeDate = document.getElementById('chequeDate' + sno).value;
                var entryType = document.getElementById('entryType' + sno).value;
                var formReferenceCode = document.getElementById('formReferenceCode' + sno).value;
                var url = "";
                var updateStatus = 0;
                url = "./clearChequeUpdate.do";
                $.ajax({
                    url: url,
                    data: {clearanceDate: clearanceDate, clearanceStatus: clearanceStatus, clearanceremark: clearanceremark, bankPaymentId: bankPaymentId, debitLedgerId: debitLedgerId,
                        creditLedgerId: creditLedgerId, chequeAmount: chequeAmount, chequeNo: chequeNo, chequeDate: chequeDate, entryType: entryType,formReferenceCode:formReferenceCode},
                    type: "GET",
                    success: function (response) {
                        updateStatus = response.toString().trim();
                        // alert(updateStatus);
                        if (updateStatus == 0) {
                            var str = "Cheque clearance failed ";
//                            var result = str.fontcolor("red");
//                            document.getElementById("status").innerHTML = result;
                            $("#status1").text(str).css("color", "red");
//                    $('#status').val(result) ;
                        } else {
                            var str = "Cheque cleared Successfully";
                               $("#status1").text(str).css("color", "green");
//  $('#status').val(result) ;
                            $("#saveButton").hide();
                        }
                    },
                    error: function (xhr, status, error) {
                    }
                });
            }
            function sendSno(value) {
//                alert(value);
                document.getElementById('sno').value = value;
            }
            function submit() {
                document.clearance.action = '/throttle/bankPaymentClearance.do';
                document.clearance.submit();
            }
            function submitPage(value) {
                document.clearance.action = '/throttle/bankPaymentClearance.do';
                document.clearance.submit();
            }
        </script>
<!--        <style>
            .modal {
                opacity: 0;
                visibility: hidden;
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                text-align: left;
                background: rgba(0,0,0, .9);
                transition: opacity .25s ease;
            }

            .modal__bg {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                cursor: pointer;
            }

            .modal-state {
                display: none;
            }

            .modal-state:checked + .modal {
                opacity: 1;
                visibility: visible;
            }

            .modal-state:checked + .modal .modal__inner {
                top: 0;
            }

            .modal__inner {
                transition: top .25s ease;
                position: absolute;
                top: -20%;
                right: 0;
                bottom: 0;
                left: 0;
                width: 50%;
                margin: auto;
                overflow: auto;
                background: #fff;
                border-radius: 5px;
                padding: 1em 2em;
                height: 50%;
            }

            .modal__close {
                position: absolute;
                right: 2em;
                top: 1em;
                width: 1.1em;
                height: 1.1em;
                cursor: pointer;
            }

            .modal__close:after,
            .modal__close:before {
                content: '';
                position: absolute;
                width: 2px;
                height: 1.5em;
                background: #ccc;
                display: block;
                transform: rotate(45deg);
                left: 50%;
                margin: -3px 0 0 -1px;
                top: 0;
            }

            .modal__close:hover:after,
            .modal__close:hover:before {
                background: #aaa;
            }

            .modal__close:before {
                transform: rotate(-45deg);
            }

            @media screen and (max-width: 768px) {

                .modal__inner {
                    width: 90%;
                    height: 90%;
                    box-sizing: border-box;
                }
            }


            /* Other
             * =============================== */
            body {
                padding: 1%;
                font: 1/1.5em sans-serif;
                text-align: center;
            }

            .btn {
                cursor: pointer;
                background: #27ae60;
                display: inline-block;
                padding: .5em 1em;
                color: #fff;
                border-radius: 3px;
            }

            .btn:hover,
            .btn:focus {
                background: #2ecc71;
            }

            .btn:active {
                background: #27ae60;
                box-shadow: 0 1px 2px rgba(0,0,0, .2) inset;
            }

            .btn--blue {
                background: #2980b9;
            }

            .btn--blue:hover,
            .btn--blue:focus {
                background: #3498db;
            }

            .btn--blue:active {
                background: #2980b9;
            }

            p img {
                max-width: 200px;
                height: auto;
                float: left;
                margin: 0 1em 1em 0;
            }
        </style>-->
    <%
         DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
         Date date = new Date();
         String fDate = "";
         String tDate = "";
         if (request.getAttribute("fromDate") != null) {
             fDate = (String) request.getAttribute("fromDate");
         } else {
             fDate = dateFormat.format(date);
         }
         if (request.getAttribute("toDate") != null) {
             tDate = (String) request.getAttribute("toDate");
         } else {
             tDate = dateFormat.format(date);
         }
    %>

    
    
    
    <style>
    #index td {
   color:white;
}
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.BankClearance"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.BankClearance"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    
    
    
    <body onload="setFocus();" >
        <form name="clearance" action=""  method="post">
            
            <%@ include file="/content/common/message.jsp" %>
              <table class="table table-info mb30 table-hover">
                  <thead>
                <tr id="index">
                    <th  ><b><spring:message code="finance.label.BankClearance"  text="default text"/></b></th>
                    <th colspan="4" align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;"></span>&nbsp;</th>
                </tr>
                  </thead>
<!--                        <div class="tabs" align="center" style="width:800" >-->
                            <!--<table class="table table-bordered" width="800" cellpadding="0" cellspacing="1" border="0" align="left" class="table4" height="70px">-->
                                <tr>
                                    <td><font color="red">*</font><spring:message code="finance.label.FromDate"  text="default text"/></td>
                                    <td height="30"><input name="fromDate" id="fromDate" value="<%=fDate%>" type="text" class="datepicker" style="width:260px;height:40px;"></td>
                                    <td><font color="red">*</font><spring:message code="finance.label.ToDate"  text="default text"/></td>
                                    <td height="30"><input name="toDate" id="toDate" value="<%=tDate%>" type="text" class="datepicker" style="width:260px;height:40px;"></td>
                                    <td></td>
<!--                                    <td></td>
                                    <td></td>-->
                                </tr>
                                <tr>
                                    <td>Bank Name </td>
                                    <td>
                                        <select name="bankId" id="bankId" class="form-control"  onchange="getBankBranch(this.value)" style="width:260px;height:40px;">
                                            <c:if test="${primarybankList != null}">
                                                <option value="0" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                                <c:forEach items="${primarybankList}" var="bankVar">
                                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>
                                                </c:forEach>
                                            </c:if>          
                                        </select>
                                        <script>
                                            if(<c:out value="${bankId}"/> != ''){
                                            document.getElementById("bankId").value='<c:out value="${bankId}"/>';
                                            getBankBranch('<c:out value="${bankId}"/>');
                                        }
                                        </script>
                                    <td><spring:message code="finance.label.Branch"  text="default text"/></td>
                                    <td  height="30">
                                        <select name="bankBranchId" id="bankBranchId" class="form-control" style="width:260px;height:40px;">
                                        </select><script>
                                            if(<c:out value="${bankBranchId}"/> != ''){
                                            document.getElementById("bankBranchId").value='<c:out value="${bankBranchId}"/>';
                                        }
                                        </script>
                                        
                                    </td>
                                    <td></td>
<!--                                    <td></td>
                                    <td></td>-->
                                </tr>
                                <tr>
                                    <td  height="30"><spring:message code="finance.label.Code"  text="default text"/></td>
                                    <td>
                                        <input type="text" name="referenceCode" id="referenceCode" class="form-control" value="<c:out value="${referenceCode}"/>" style="width:260px;height:40px;">
                                    </td>
                                    <td  height="30" colspan="3" >
                                       &nbsp;&nbsp;&nbsp;<input type="button"  value="<spring:message code="finance.label.FetchData"  text="default text"/>"  class="btn btn-success" name="Fetch Data" onClick="submitPage(this.value)" style="width:100px;height:35px;">&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                            </table>
                    </td>
                </tr>
            </table>

            <br/>
            <c:if test = "${BPClearanceDetails != null}" >
                 <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th><spring:message code="finance.label.CLCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.ReferenceCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.CreditLedger"  text="default text"/></th>
                            <th><spring:message code="finance.label.DebitLedger"  text="default text"/></th>
                            <th><spring:message code="finance.label.ChequeNo"  text="default text"/></th>
                            <th><spring:message code="finance.label.ChequeDate"  text="default text"/></th>
                            <th><spring:message code="finance.label.ChequeAmount"  text="default text"/></th>
                            <th><spring:message code="finance.label.ClearanceDate"  text="default text"/></th>
                            <th><spring:message code="finance.label.PaymentMode"  text="default text"/></th>
                            <th><spring:message code="finance.label.Type"  text="default text"/></th>
                            <th><spring:message code="finance.label.Status"  text="default text"/></th>
                            <th>&nbsp;</th>
                    </thead>
                    <tbody>
                        <% int sno = 0;
                            int index = 1;%>
                        <c:forEach items="${BPClearanceDetails}" var="list"> 

                            <tr  width="208" height="40" > 
                                <td  height="20"><%=index%></td>
                                <td  height="20"><%=ThrottleConstants.bankClearnceCode%><c:out value="${list.bankPaymentId}"/></td>
                                <td  height="20"><c:out value="${list.formReferenceCode}"/></td>
                                <td  height="20"><c:out value="${list.creditLedgerName}"/></td>
                                <td  height="20"><c:out value="${list.debitLedgerName}"/></td>
                                <td  height="20"><c:out value="${list.chequeNo}"/></td>
                                <td  height="20"><c:out value="${list.chequeDate}"/></td>
                                <td  height="20"><c:out value="${list.chequeAmount}"/></td>
                                <td  height="20"><c:out value="${list.chequeClearanceDate}"/>
                                    <input type="hidden" name="bankPaymentId" id="bankPaymentId<%=sno%>" value="<c:out value="${list.bankPaymentId}" />" />
                                    <input type="hidden" name="debitLedgerId" id="debitLedgerId<%=sno%>" value="<c:out value="${list.debitLedgerId}" />"/>
                                    <input type="hidden" name="creditLedgerId" id="creditLedgerId<%=sno%>" value="<c:out value="${list.creditLedgerId}" />"/>
                                    <input type="hidden" name="chequeAmount" id="chequeAmount<%=sno%>" value="<c:out value="${list.chequeAmount}" />"/>
                                    <input type="hidden" name="chequeNo" id="chequeNo<%=sno%>" value="<c:out value="${list.chequeNo}" />"/>
                                    <input type="hidden" name="chequeDate" id="chequeDate<%=sno%>" value="<c:out value="${list.chequeDate}" />"/>
                                    <input type="hidden" name="entryType" id="entryType<%=sno%>" value="<c:out value="${list.entryType}" />"/>
                                    <input type="hidden" name="formReferenceCode" id="formReferenceCode<%=sno%>" value="<c:out value="${list.formReferenceCode}" />"/>
                                </td>
                                <td  height="20"> 
                                    <c:if test = "${list.paymentMode == '1' }" >
                                        <spring:message code="finance.label.Cheque"  text="default text"/>
                                    </c:if>
                                    <c:if test = "${list.paymentMode == '2' }" >
                                       <spring:message code="finance.label.DD"  text="default text"/> 
                                    </c:if>
                                    <c:if test = "${list.paymentMode == '3' }" >
                                        <spring:message code="finance.label.RTGS"  text="default text"/>
                                    </c:if>
                                </td>
                                <td  height="20"> 
                                    <c:if test = "${list.entryType == 'PAYMENT' }" >
                                        <spring:message code="finance.label.Issued"  text="default text"/>
                                    </c:if>
                                    <c:if test = "${list.entryType == 'RECEIPT' }" >
                                        <spring:message code="finance.label.Deposited"  text="default text"/>
                                    </c:if>
                                </td>
                                <td  height="20"> 
                                    <c:if test = "${list.status == '1' }" >
                                        <spring:message code="finance.label.Cleared"  text="default text"/>
                                    </c:if>
                                    <c:if test = "${list.status == '2' }" >

                                       <spring:message code="finance.label.Bounced"  text="default text"/> 
                                    </c:if>
                                    <c:if test = "${list.status == '3' }" >

                                        <spring:message code="finance.label.Cancelled"  text="default text"/>
                                    </c:if>
                                </td>
                                <td  height="20"> 
                                    <c:if test = "${list.chequeClearanceDate == '' }" >
                            <center>  
                                <button  style="width: 90px;height: 40px;background-color:#5BC0DE;" data-toggle="modal" data-target="#myModal" onclick="sendSno('<%=sno%>');"/><spring:message code="finance.label.Sno"  text="default text"/>Update</button>
                                <!--<label class="btn" for="modal-1" onclick="sendSno('<%=sno%>');">Update</label>-->
                            </center>
                        </c:if>
                        <c:if test = "${list.chequeClearanceDate != '' }" >

                            &nbsp;
                        </c:if>
                        </td>
                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                    </tbody>
                </table>
                        
                <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
                <br>
                <br>
                 <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
            
            <c:if test = "${BPClearanceDetails == null}" >
                <spring:message code="finance.label.NoRecordsFound"  text="default text"/>
            </c:if>
                <br>
                <div class="modal fade" id="myModal" role="dialog" style="width: 100%;height: 100%">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" onclick="submit();resetTheSlabDetails();">&<spring:message code="finance.label.times"  text="default text"/>;</button>
                            <h4 class="modal-title" style="color:#5BC0DE;"><center><spring:message code="finance.label.BankTransaction"  text="default text"/><center></h4>
                        </div>
                        <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 80%">
                              <br>  <br>
                            <font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                    <div align="center" id="status1">&nbsp;&nbsp;
                    </div>
                    </font>
                <br>
              
                 <table class="table table-bordered mb30 table-hover" style="width:90%;" align="center" >
                    <tr  height="40">
                    <input type="hidden" name="sno" id="sno" value="" />
                    <td><font color="red">*</font><spring:message code="finance.label.Status"  text="default text"/></td>
                    <td>  <select name="clearanceStatus" id="clearanceStatus"  class="form-control" style="width:260px;height:40px;" >
                            <option value="0" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                            <option value="1" ><spring:message code="finance.label.Cleared"  text="default text"/></option>
                            <option value="2" ><spring:message code="finance.label.Bounced"  text="default text"/></option>
                            <option value="3" ><spring:message code="finance.label.Cancelled"  text="default text"/></option>
                        </select>
                    </td>
                    </tr>
                    <tr  height="40">
                        <td><font color="red">*</font><spring:message code="finance.label.TransactionDate"  text="default text"/></td>
                        <td> <input type="textbox" name="clearanceDate" id="clearanceDate" value="" class="datepicker" style="width:260px;height:40px;"/>

                        </td>
                    </tr>
                    <tr  height="40">
                        <td><font color="red">*</font><spring:message code="finance.label.Remarks"  text="default text"/></td>
                        <td> <textarea name="narration" class="form-control"  id="clearanceremark"  style="width:260px;height:40px;"/></textarea>
                        </td>
                    </tr>
                </table>
                <br>
                <br>
               
                <center>
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                    <input type="button" value="<spring:message code="finance.label.Save"  text="default text"/>" class="btn btn-success" name="Save" id="saveButton" onClick="clearCheque();" style="width:100px;height:35px;">
                </center>
                        </div>
                        <div class="modal-footer">


                        </div>
                    </div>
                </div>
            </div>
       
<!--        <input class="modal-state" id="modal-1" type="checkbox" />
        <div class="modal">
            <label class="modal__bg" for="modal-1"></label>
            <div class="modal__inner">
                <label class="modal__close" for="modal-1" onclick="submit();"></label>
                <center><h2 style="font-size:16px;">Bank Transaction</h2></center>
                <br>
                <font style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                <div align="center" id="status">&nbsp;&nbsp;
                </div>
                </font>
                <br>
                <table width="90%" align="center"  style="width: 90%">
                    <tr  height="40">
                    <input type="hidden" name="sno" id="sno" value="" />
                    <td><font color="red">*</font>Satus</td>
                    <td>  <select name="clearanceStatus" id="clearanceStatus"  class="form-control" style="width:140px;height:20px;" >
                            <option value="0" selected>--Select--</option>
                            <option value="1" >Cleared</option>
                            <option value="2" >Bounced</option>
                            <option value="3" >Cancelled</option>
                        </select>
                    </td>
                    </tr>
                    <tr  height="40">
                        <td><font color="red">*</font>Transaction Date</td>
                        <td> <input type="textbox" name="clearanceDate" id="clearanceDate" value="" class="datepicker" style="width:140px;height:20px;"/>

                        </td>
                    </tr>
                    <tr  height="40">
                        <td><font color="red">*</font>Remarks</td>
                        <td> <textarea name="narration" class="form-control"  id="clearanceremark"  style="width:140px;height:20px;"/></textarea>
                        </td>
                    </tr>
                </table>
                <br>
                <br>
                <br>
                <br>
                <center>
                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                    <input type="button" value="Save" class="button" name="Save" id="saveButton" onClick="clearCheque();">
                </center>
                <p><img src="http://i.imgur.com/HnrkBwB.gif" alt="" />Aliquam in sagittis nulla. Curabitur euismod diam eget risus venenatis, sed dictum lectus bibendum. Nunc nunc nisi, hendrerit eget nisi id, rhoncus rutrum velit. Nunc vel mauris dolor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam fringilla quis nisi eget imperdiet.</p>
            </div>
        </div>-->
         <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>