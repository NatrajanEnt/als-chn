<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function submitPage(value) {
        if (value == 'ExportExcel') {
            alert(value);
            document.orderExpenseRevenue.action = '/throttle/handleOrderExpenseRevenueSearch.do?param=ExportExcel';
            document.orderExpenseRevenue.submit();
        } else {
            document.orderExpenseRevenue.action = '/throttle/handleOrderExpenseRevenueSearch.do?param=Search';
            document.orderExpenseRevenue.submit();

        }
    }

    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }

    function viewCustomerProfitDetails(tripIds, tripStatusId) {
        //alert(tripIds);
        window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId=' + tripIds + '&tripStatusId=' + tripStatusId + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
    }
</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">العربية</a>
              </span>-->

    <style type="text/css">





        .container {width: 960px; margin: 0 auto; overflow: hidden;}
        .content {width:800px; margin:0 auto; padding-top:50px;}
        .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

        /* STOP ANIMATION */



        /* Second Loadin Circle */

        .circle1 {
            background-color: rgba(0,0,0,0);
            border:5px solid rgba(100,183,229,0.9);
            opacity:.9;
            border-left:5px solid rgba(0,0,0,0);
            /*	border-right:5px solid rgba(0,0,0,0);*/
            border-radius:50px;
            /*box-shadow: 0 0 15px #2187e7; */
            /*	box-shadow: 0 0 15px blue;*/
            width:40px;
            height:40px;
            margin:0 auto;
            position:relative;
            top:-50px;
            -moz-animation:spinoffPulse 1s infinite linear;
            -webkit-animation:spinoffPulse 1s infinite linear;
            -ms-animation:spinoffPulse 1s infinite linear;
            -o-animation:spinoffPulse 1s infinite linear;
        }

        @-moz-keyframes spinoffPulse {
            0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
        }
        @-webkit-keyframes spinoffPulse {
            0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
        }
        @-ms-keyframes spinoffPulse {
            0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
        }
        @-o-keyframes spinoffPulse {
            0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
        }



    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.AccountsReceivableReport" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="operations.reports.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.AccountsReceivableReport" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <!--    <body onload="setValue();">-->
                    <form name="orderExpenseRevenue" action=""  method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <%@ include file="/content/common/message.jsp" %>
                        <!--            <br>
                                    <br>
                                    <br>
                                    <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                                        <tr id="exp_table" >
                                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                                <div class="tabs" align="left" style="width:850;">
                                                    <ul class="tabNavigation">
                                                        <li style="background:#76b3f1"><spring:message code="operations.reports.label.OrderExpenseRevenueSearch" text="default text"/></li>
                                                    </ul>
                                                    <div id="first">-->
                        <table class="table table-info mb30 table-hover" >
                            <thead><tr><th colspan="4"><spring:message code="operations.reports.label.OrderExpenseRevenueSearch" text="default text"/></th></tr></thead>
                            <tr>
                                <td><font color="red">*</font><spring:message code="operations.reports.label.TripStartDate" text="default text"/></td>
                                <td height="30"><input name="fromDate" id="fromDate"  autocomplete="off" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                <td><font color="red">*</font><spring:message code="operations.reports.label.TripEndDate" text="default text"/></td>
                                <td height="30"><input name="toDate" id="toDate"  autocomplete="off" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="right"><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                <td colspan="2"><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td>
                            </tr>
                        </table>
                        <!--                            </div></div>
                                            </td>
                                        </tr>
                                    </table>
                        
                        
                                    <br>
                                    <br>
                                    <br>
                        -->
                        <c:if test = "${orderExpenseRevenue != null}" >
                            <table class="table table-info mb30 table-hover" id="table" >
                                <thead>
                                    <tr>
                                <th><h3><spring:message code="operations.reports.label.SNo" text="default text"/></h3></th>
                                <th><h3><spring:message code="operations.reports.label.SegmentName" text="default text"/></h3></th>
                                <th><h3><spring:message code="operations.reports.label.OrderNo" text="default text"/></h3></th>
                                <th><h3><spring:message code="operations.reports.label.FreightCharge" text="default text"/></h3></th>
                                <th><h3><spring:message code="operations.reports.label.NoOfTrip" text="default text"/></h3></th>
                                <th><h3><spring:message code="operations.reports.label.EstimatedExpense" text="default text"/></h3></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <%int sno = 1;%>
                                    <c:forEach items="${orderExpenseRevenue}" var="month">
                                        <tr>
                                            <td><%=sno%></td>
                                            <td><c:out value="${month.segmentName}"/></td>
                                            <td><c:out value="${month.customerOrderReferenceNo}"/></td>
                                            <td><c:out value="${month.freight}"/></td>
                                            <td align="center"> <a href="#" onclick="viewCustomerProfitDetails('<c:out value="${month.tripId}"/>', '<c:out value="${month.tripStatusId}"/>');"><c:out value="${month.noOfTrips}"/></a></td>
                                            <td><c:out value="${month.estimatedExpenses}"/></td>
                                        </tr>
                                        <%sno++;%>
                                    </c:forEach>
                                </tbody>
                            </table>
                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" selected="selected">5</option>
                                        <option value="10">10</option>
                                        <option value="20">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 1);
                            </script>      
                        </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>