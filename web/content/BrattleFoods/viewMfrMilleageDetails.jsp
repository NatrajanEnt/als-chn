<%--
    Document   : newfuelprice
    Created on : Oct 27, 2013, 5:04:09 PM
    Author     : Arul
--%>

<script language="javascript" src="/throttle/js/validate.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body >
        <form name="fuleprice"  method="post" >
            <br>
            <br>
            <table class="table table-info mb30 table-hover" id="table" class="sortable">
                <thead>
                <tr height="30">
                <th><h3>S.No</h3></th>
                <th><h3>Age Name</h3></th>
                <th><h3>Status</h3></th>
                <th><h3>Action</h3></th>
                </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${mfrMilleageAgeing != null}" >
                        <%
                                            sno++;
                                           String className = "text1";
                                           if ((sno % 1) == 0) {
                                               className = "text1";
                                           } else {
                                               className = "text2";
                                           }
                        %>
                        <c:forEach items="${mfrMilleageAgeing}" var="mfrMilleageAgeing">
                            <tr height="30">
                                <td class="<%=className%>"  height="30"> <%= sno++%></td>
                                <td class="<%=className%>" align="left" ><c:out value="${mfrMilleageAgeing.mfrAge}"/> to <c:out value="${mfrMilleageAgeing.mfrToAge}"/>&nbsp;Years</td>
                                <td class="<%=className%>" align="left" >
                                    <c:if test="${mfrMilleageAgeing.status == 'Y'}">
                                        Active
                                    </c:if>
                                    <c:if test="${mfrMilleageAgeing.status == 'N'}">
                                        In-Active
                                    </c:if>
                                </td>
                                <td class="<%=className%>" align="left" ><input type="checkbox" id="ageingIdStatus" onclick="setDetails('<c:out value="${mfrMilleageAgeing.ageingId}"/>','<c:out value="${mfrMilleageAgeing.mfrId}"/>','<c:out value="${mfrMilleageAgeing.mfrAge}"/>','<c:out value="${mfrMilleageAgeing.mfrToAge}"/>','<c:out value="${mfrMilleageAgeing.status}"/>')"/></td>
                            </tr>
                        </c:forEach>
                    </c:if>

                </tbody>
            </table>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

