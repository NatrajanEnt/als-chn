<%--
    Document   : viewTripClosureVehicleList
    Created on : Jul 18, 2014, 11:32:08 AM
    Author     : Arul
--%>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>


        <form name="viewTripClosureVehicle"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>

            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>TripCode</h3></th>
                        <th><h3>VehicleNo</h3></th>
                        <th><h3>RouteInfo</h3></th>
                        <th><h3>StartDate</h3></th>
                        <th><h3>EndDate</h3></th>
                        <th><h3>TransitDays</h3></th>
                        <th><h3>TotalKm</h3></th>
                        <th><h3>TotalHours</h3></th>
                        <th><h3>VehicleMileage</h3></th>
                        <th><h3>ReeferMileage</h3></th>
                        <th><h3>FuelConsumption</h3></th>
                        <th><h3>FuelCost</h3></th>
                        <th><h3>FuelExpense</h3></th>
                        <th><h3>OtherExpense</h3></th>
                        <th><h3>SystemExpense</h3></th>
                        <th><h3>RouteExpense</h3></th>
                        <th><h3>TotalExpense</h3></th>
                        <th><h3>Action</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <% int rowNo = 1;%>
                    <c:if test = "${tripSettlementVehicleList != null}">
                        <c:forEach items="${tripSettlementVehicleList}" var="tripVehicleList">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%=rowNo++%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.tripCode}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.regNo}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.routeInfo}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.startDate}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.endDate}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.tripTransitDays}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.totalKm}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.totalHours}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.vehicleMileage}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.reeferMileage}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.fuelConsumption}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.fuelCost}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.fuelExpense}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.tollExpense+tripVehicleList.driverIncentive+tripVehicleList.driverBhatta+tripVehicleList.miscValue+tripVehicleList.parkingCost}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.systemExpenses}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.routeExpenses}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${tripVehicleList.totalExpenses}"/></td>
                                <td class="<%=className%>"  align="left">
                                <a href="viewTripSettlement.do?tripSheetId=<c:out value="${tripId}"/>&vehicleId=<c:out value="${tripVehicleList.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />">Settlement</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>