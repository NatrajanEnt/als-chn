<%--
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "Trip_Planning_"+curDate+".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test = "${consignmentList != null}" >
            <table class="table table-info mb30 table-bordered" id="table" width="100%" >
                            <thead>
                                <c:if test = "${pnrUser == 1}" >
                                    <tr >
                                        <th align="center">Sno:<c:out value="${pnrUser}"/>:</th>
                                        <th align="center">BookingNo</th>
                                        <th align="center">PNR No</th>
                                        <th align="center">VesselName</th>
                                        <th align="center">Move Type</th>
                                        <th align="center">Consignment Date</th>
                                        <th align="center">PNR Date</th>
                                        <th align="center">Interim Details </th>
                                        <th align="center">Container Type </th>
                                        <!--<th align="center">Container Type(40) </th>-->                                        
                                        <th align="center">Status </th>
                                        <th align="center">Loading/Unloading On </th>
                                        <th align="center">Created By</th>
                                    </tr>
                                </c:if>
                                <c:if test = "${pnrUser != 1}" >
                                    <tr >
                                        <th align="center">Sno</th>
                                        <th align="center">ConsigNo</th>
                                        <th align="center">Consignment <br>Date</th>    
                                        <th align="center">Booking No / BOE</th>                                        
                                        <th align="center">Move Type</th>
                                        <th align="center">Customer</th>                                        
                                        <th align="center">Route Details </th>
                                        <th align="center">20 FT</th>
                                        <th align="center">40 FT</th>
                                        <th align="center">Total</th>
                                        <th align="center">Goods Desc </th>
                                        <th align="center">Ton</th>
                                        <th align="center">Loading/Unloading On </th>
                                        <th align="center">Created By</th>
                                    </tr>
                                </c:if>
                            </thead>
                            <% int index = 0;
                                        int sno = 1;
                            %>
                            <tbody>
                                <c:set var="totalVolume" value="${0 }"/>
                                <c:set var="totalWeight" value="${0 }"/>
                                <c:forEach items="${consignmentList}" var="cnl">
                                    <%--<c:if test = "${cnl.plannedStatus != 1}" >--%>
                                        <c:set var="totalVolume" value="${totalVolume + cnl.totalVolume }"/>
                                        <c:set var="totalWeight" value="${totalWeight + cnl.totalWeights }"/>
                                        <%-- <input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/><c:out value="${cnl.consignmentNoteNo}"/>--%>
                                        <tr>

                                            <td align="center" ><%=sno%></td>

                                            <td align="center" >
                                                <input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                                                <input type="hidden" name="containerId" id="containerId<%=index%>" value="<c:out value="${cnl.containerTypeId}"/>"/>
                                                <input type="hidden" name="movementType" id="movementType<%=index%>" value="<c:out value="${cnl.movementType}"/>"/>
                                                <input type="hidden" name="movementTypeId" id="movementTypeId<%=index%>" value="<c:out value="${cnl.movementTypeId}"/>"/>
                                                <c:out value="${cnl.consignmentNoteNo}"/>
                                                
                                                <!--<a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>-->
                                            </td>
                                            <td align="center" ><c:out value="${cnl.consignmentOrderDate}"/></td>
                                            <td align="center" ><c:out value="${cnl.orderReferenceNo}"/></td>
                                            <td align="center" ><c:out value="${cnl.movementType}"/></td>
                                            <td align="center" ><c:out value="${cnl.customerName}"/></td>

                                            <td align="center" ><c:out value="${cnl.routeName}"/></td>

                                            <td align="center"  >
                                                <c:out value="${cnl.containerTypeName}"/>
                                            </td>
                                            <td align="center" >
                                                <c:out value="${cnl.containerTypeNameForty}"/>
                                            </td>
                                             <td align="center"  >
                                                <c:out value="${cnl.totalContainer}"/>
                                            </td>
                                            
                                            <c:if test = "${pnrUser != 1}" >
                                            <td align="center" ><c:out value="${cnl.desc}"/></td>    
                                            <td align="center" >
                                                <input type="hidden" name="packagesNos" id="packagesNos<%=index%>" value="<c:out value="${cnl.packageNos}"/>"/>
                                                <input type="hidden" name="weights" id="weights<%=index%>" value="<c:out value="${cnl.totalWeights}"/>"/>
                                                <input type="hidden" name="productVolume" id="productVolume<%=index%>" value="<c:out value="${cnl.totalVolume}"/>"/>
                                                
                                                &emsp;<c:out value="${cnl.conWeight}"/>
                                            <%--<c:out value="${cnl.packageNos}"/>/<c:out value="${cnl.totalWeights}"/>/<c:out value="${cnl.totalVolume}"/>--%>
                                            </td>
                                            
                                            </c:if>
                                           
                                            <td align="center" ><c:out value="${cnl.loadUnloadDate}"/>&nbsp;
                                               <br> <c:out value="${cnl.loadUnloadTime}"/></td>
                                            <td align="center" ><c:out value="${cnl.userName}"/></td>

                                        </tr>
                                        <%index++;%>
                                        <%sno++;%>
                                    <%--</c:if>--%>
                                </c:forEach>
                            </tbody>
                        </table>
        </c:if>

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
