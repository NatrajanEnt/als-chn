<%-- 
    Document   : DailyVehicleGpsReport
    Created on : aug 16, 2016, 09:37:16 AM
    Author     : raj
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>


        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    
    <body>
        <form name="dailyVehicleGps" method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "dailyVehicleGpsReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
           %>
           
            <br>
            <c:if test="${userLoginList != '0'}">
                <table align="center" border="0" id="table" class="sortable" width="100%" >
                    <thead>
                        <tr height="30">
                            <th align="center"><h3>S.No</h3></th>
                            <th align="center"><h3>Gps Mobile No</h3></th>
                            <th align="center"><h3>Vehicle No</h3></th>
                            <th align="center"><h3>Party Name</h3></th>
                            <th align="center"><h3>Route</h3></th>
                            <th align="center"><h3>Location</h3></th>
                            <th align="center"><h3>Idle Time</h3></th>
                            <th align="center"><h3>Remarks</h3></th>
                            <th align="center"><h3>Category</h3></th>
                        </tr> 
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${gpsVehiclelist}" var="gpsVehiclelist">
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>
                            <tr height="50">
                                <td class="<%=classText%>"><%=index++%></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.gpsSimNo}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.vehicleNo}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.billingParty}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.routeName}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.gpsLocation}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.idleTime}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.remarks}"/></td>
                                <td class="<%=classText%>"><c:out value="${gpsVehiclelist.vendorName}"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <br/>
                <br/>
                <br/>
                <br/>
                <br/>
               
            </c:if>
        </form>
    </body>    
</html>