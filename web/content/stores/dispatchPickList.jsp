<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>


<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script><!-- External script -->
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 


<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.PickListPlan"  text="PickListPlan"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="stores.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="stores.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Delivery"  text="Delivery"/></a></li>
            <li class="active"><spring:message code="stores.label.PickListPlan"  text="PickListPlan"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onload="setFocus();">

                <!--                                <body onload="setFocus();getItemNames();">-->

                <form name="searchPart"  method="post"  >

                    <%@ include file="/content/common/message.jsp" %>

                   
                    <!--</div>-->
                    <!--</div>-->

                    <span id="successsMsg" align="center"><font color="green" size="5px;"></font></span>
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                               <tr >
                                    <th ><spring:message code="stores.label.S.No"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Delivery Note No"  text="Delivery Note No"/></th>
                                    <%--<th ><spring:message code="stores.label.Mfr"  text="default text"/></th>
                                    <th  ><spring:message code="stores.label.Model"  text="default text"/></th>--%>
                                    <th ><spring:message code="stores.label.MfrCode"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.CompanyCode"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.ItemName"  text="default text"/></th>                        
                                  <%--  <th ><spring:message code="stores.label.Group"  text="default text"/> </th>--%>
                                    <th ><spring:message code="stores.label.Order Qty"  text="Order Qty"/> </th>
                                    <%--<th ><spring:message code="stores.label.RC"  text="default text"/></th>--%>
                                    <th ><spring:message code="stores.label.UOM"  text="default text"/></th>
                                    <%--<th ><spring:message code="stores.label.MaxQty"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.RoLevel"  text="default text"/></th>--%>
                                    <th ><spring:message code="stores.label.Rack"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Sub-Rack"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Action"  text="default text"/></th>
                                </tr>
                            </thead>
                            <tbody>
                                   <tr height="30">                    
                                            <td> 1 </td>                                         
                                            <td> 1337 </td>                                         
<!--                                            <td> Voltas </td>                                         
                                            <td> 2020 </td>                                           -->
                                            <td> AC101RFH45 </td>                                           
                                            <td> AC101RFH45 </td>                                           
                                            <td> AC101RFH45 </td>                                           
                                            <!--<td> Generic</td>-->                                           
                                            <td> 10.0</td>                                           
                                            <!--<td> N</td>-->                                           
                                            <td> Nos</td>                                           
                                            <!--<td> 10</td>-->                                           
                                            <!--<td> 5</td>-->                                           
                                            <td> A101</td>                                           
                                            <td> A101A</td>          
                                            <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                    </tr>
                                            
                                    <tr height="30">                    
                                            <td> 2 </td>                                         
                                            <td> 1339 </td>                                         
<!--                                            <td> ButterFly </td>                                         
                                            <td> 2019 </td>                                           -->
                                            <td> COOKER101 </td>                                           
                                            <td> COOKER101 </td>                                           
                                            <td> COOKER101 </td>                                           
                                            <!--<td> Generic</td>-->                                           
                                            <td> 10.0</td>                                           
                                            <!--<td> N</td>-->                                           
                                            <td> Nos</td>                                           
                                            <!--<td> 10</td>-->                                           
                                            <!--<td> 2</td>-->                                           
                                            <td> B101</td>                                           
                                            <td> B101A</td>                                           
                                            <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                    </tr>
                                    <tr height="30">                    
                                            <td> 3 </td>                                         
                                            <td> 1340 </td>                                         
<!--                                            <td> Prestige </td>                                         
                                            <td> 2018 </td>                                           -->
                                            <td> COOKER102 </td>                                           
                                            <td> COOKER102 </td>                                           
                                            <td> COOKER102 </td>                                           
                                            <!--<td> Generic</td>-->                                           
                                            <td> 10.0</td>                                           
                                            <!--<td> N</td>-->                                           
                                            <td> Nos</td>                                           
                                            <!--<td> 10</td>-->                                           
                                            <!--<td> 2</td>-->                                           
                                            <td> C101</td>                                           
                                            <td> C101A</td>                                           
                                            <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                    </tr>
                                    <tr height="30">                    
                                            <td> 4 </td>                                         
                                            <td> 1341 </td>                                         
<!--                                            <td> kelvinator </td>                                         
                                            <td> 2020 </td>                                           -->
                                            <td> AC101RFH50 </td>                                           
                                            <td> AC101RFH50 </td>                                           
                                            <td> AC101RFH50 </td>                                           
                                            <!--<td> Generic</td>-->                                           
                                            <td> 20.0</td>                                           
                                            <!--<td> N</td>-->                                           
                                            <td> Nos</td>                                           
                                            <!--<td> 10</td>-->                                           
                                            <!--<td> 8</td>-->                                           
                                            <td> D101</td>                                           
                                            <td> D101A</td>          
                                            <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                    </tr>
                                    <tr height="30">                    
                                            <td> 5 </td>                                         
                                            <td> 1342 </td>                                         
<!--                                            <td> Butterflty </td>                                         
                                            <td> 2020 </td>                                           -->
                                            <td> MIXIE101 </td>                                           
                                            <td> MIXIE101 </td>                                           
                                            <td> MIXIE101 </td>                                           
                                            <!--<td> Generic</td>-->                                           
                                            <td> 20.0</td>                                           
                                            <!--<td> N</td>-->                                           
                                            <td> Nos</td>                                           
                                            <!--<td> 10</td>-->                                           
                                            <!--<td> 8</td>-->                                           
                                            <td> E101</td>                                           
                                            <td> E101A</td>          
                                            <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                    </tr>
                                    <tr height="30">                    
                                            <td> 6 </td>                                         
                                            <td> 1343 </td>                                         
<!--                                            <td> Samsung </td>                                         
                                            <td> 2020 </td>                                           -->
                                            <td> Fridge101 </td>                                           
                                            <td> Fridge101 </td>                                           
                                            <td> Fridge101 </td>                                           
                                            <!--<td> Generic</td>-->                                           
                                            <td> 4.0</td>                                           
                                            <!--<td> N</td>-->                                           
                                            <td> Nos</td>                                           
                                            <!--<td> 10</td>-->                                           
                                            <!--<td> 2</td>-->                                           
                                            <td> F101</td>                                           
                                            <td> F101A</td>          
                                            <td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>        
                                    </tr>
                            </tbody>



                        </table>

                    <br>
                     <table class="table table-info mb30 table-hover">
                            <!--<thead><tr><th colspan="4"><spring:message code="stores.label.Dispatch"  text="Dispatch "/></th></tr></thead>-->
                            <tr>
                                <td ><font color="red">*</font><spring:message code="stores.label.Pick Person"  text="Pick Person"/></td>
                                <td ><select class="form-control" style="width:260px;height:40px;" name="driverId" id="driverId">
                                        <option value='0'>-<spring:message code="stores.label.Select"  text="default text"/>-</option>
                                        <option value='1'>Ragu</option>
                                        <option value='2'>Prakash</option>
                                        <option value='3'>Rajesh</option>
                                        <option value='4'>Ashok</option>
                                    </select></td>
                            </tr>
                    </table>
                     <center>
                            <input type="button" value="<spring:message code="stores.label.PickList Plan"  text="PickList Plan"/>" class="btn btn-success" onClick="generatePickList()">                
                          &emsp;  <input type="button" value="<spring:message code="stores.label.Print"  text="Print"/>" class="btn btn-success" onClick="getDeliveryNotePrint();">                
                        </center>
                        <script>
                            function getDeliveryNotePrint() {
                                var val=1;
                              
                                window.open('/throttle/getPickListPrint.do?deliveryNoteId=' + val, "Print Run Sheet",
                                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                        }
                            function generatePickList(){
                                if($("#driverId").val() == "0"){
                                    alert("please select Pick Person");
                                    return;
                                }else{
                                    alert("PickList Planned...!");
                                }
                            }
                        </script>
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
            </body>

        </div>


    </div>
</div>





<%@ include file="../common/NewDesign/settings.jsp" %>


