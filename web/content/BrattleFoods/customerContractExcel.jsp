<%-- 
    Document   : CostomerContractExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %> 
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <%
              String menuPath = "Customer >> Customer Contract list";
              request.setAttribute("menuPath", menuPath);
              String dateval = request.getParameter("dateval");
              String active = request.getParameter("active");
              String type = request.getParameter("type");
    %>

    <body>

        <form name="tripSheet" action=""  method="post">
            <%
                       Date dNow = new Date();
                       SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                       //System.out.println("Current Date: " + ft.format(dNow));
                       String curDate = ft.format(dNow);
                       String expFile = "CustomerContractList-" + curDate + ".xls";

                       String fileName = "attachment;filename=" + expFile;
                       response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                       response.setHeader("Content-disposition", fileName);
            %>



            <br>
            <br>
            <br>

            <br><br>
            <c:if test="${billingTypeId == 1}">Point to Point - Based</c:if>
            <c:if test="${billingTypeId == 6}">Weight Basis</c:if>
            <br>
            <br>
            <c:if test="${billingTypeId == 1}">
              <c:if test="${ptpBillingList != null}">
                <table align="center" border="0" id="table" class="sortable" style="width:1700px;" >
                    <thead>
                        <tr height="50">
                            <th>SNo</th>
                            <th>Movement</th>
                            <th>Vehicle Type</th>
                            <th>Load Type</th>
                            <th>Container Type</th>
                            <th>Container Qty</th>
                            <th>First Pick UP</th>
                            <th>Interim Point 1</th>
                            <th>Interim Point 2</th>
                            <th>Interim Point 3</th>
                            <th>Interim Point 4</th>
                            <th>Final Drop Point</th>
                            <th>Travel Km</th>
                            <th>Rate with Reefer</th>
                            <th>Rate W/O Reefer</th>
                            <th>Base Freight</th>
                            <th>TAT (Hours)</th>
                            <th>Status</th>
                            <th>Created By</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index1 = 1;%>
                        <c:forEach items="${ptpBillingList}" var="ptpl">
                            <%
                                        String className1 = "text1";
                                        if ((index1 % 2) == 0) {
                                            className1 = "text1";
                                        } else {
                                            className1 = "text2";
                                        }
                            %>
                            <tr >
                                <td class="form-control"  >
                                    <%=index1%>
                                </td>
                                <td class="form-control" align="left" >
                                    <c:if test="${ptpl.contractCategory =='1'}">
                                        Export
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='2'}">
                                        Import
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='17'}">
                                        Vessel Empty
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='18'}">
                                        Stuffing Empty
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='19'}">
                                        Local PNR
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='20'}">
                                        Ex-Bonding
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='21'}">
                                        De-Stuff-Empty
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='23'}">
                                        PNR-MOFF
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='24'}">
                                        Offload - Mov
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='25'}">
                                        EX-AP-TY
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='26'}">
                                        EX-AP-RE
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='27'}">
                                        Local-Exp
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='28'}">
                                        Local-Imp
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='29'}">
                                        Coastal Inbound ( Import Moff )
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='30'}">
                                        Coastal Inbound ( Import Local )
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='31'}">
                                        Coastal Outbound ( Export Moff )
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='32'}">
                                        Coastal Outbound ( Export Local )
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='33'}">
                                        Repo (Moff)
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='34'}">
                                        Repo (Local)
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='35'}">
                                        Lease Box (Moff)
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='36'}">
                                        Lease Box (Local)
                                    </c:if>
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.vehicleTypeName}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:if test="${ptpl.loadTypeId =='1'}">
                                        Empty Trip
                                    </c:if>
                                    <c:if test="${ptpl.loadTypeId =='2'}">
                                        Load Trip
                                    </c:if>
                                </td>
                                <td class="form-control" align="left">
                                    <c:if test="${ptpl.containerTypeId =='1'}">
                                        20'
                                    </c:if>
                                    <c:if test="${ptpl.containerTypeId =='2'}">
                                        40'
                                    </c:if>
                                    <c:if test="${ptpl.containerTypeId =='4'}">
                                        0
                                    </c:if>
                                </td>
                                <td class="form-control" align="left">
                                    <c:if test="${ptpl.containerQty1 =='1'}">
                                        1
                                    </c:if>
                                    <c:if test="${ptpl.containerQty1 =='2'}">
                                        2
                                    </c:if>
                                    <c:if test="${ptpl.containerQty1 =='0'}">
                                        0
                                    </c:if>
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.firstPickupName}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.point1Name}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.point2Name}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.point3Name}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.point4Name}" />
                                </td>    
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.finalPointName}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.totalKm}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.rateWithReefer}" />
                                </td>    
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.rateWithoutReefer}" />
                                </td>    
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.baseFreight}"/>
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.tat}"/>
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.activeInd}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.userName}" />
                                </td>
                            </tr>

                            <%index1++;%>
                        </c:forEach>
                    </tbody>
                </table>
               </c:if>  
            </c:if>
            <c:if test="${billingTypeId == 6}">
              <c:if test="${localBillingList != null}">
                <table align="center" border="0" id="table" class="sortable" style="width:1700px;" >
                    <thead>
                        <tr height="50">
                            <th>SNo</th>
                            <th>Movement</th>
                            <th>Vehicle Type</th>
                            <th>Load Type</th>
                            <th>Container Type</th>
                            <th>Container Qty</th>
                            <th>First Pick UP</th>
                            <th>Interim Point 1</th>
                            <th>Interim Point 2</th>
                            <th>Interim Point 3</th>
                            <th>Interim Point 4</th>
                            <th>Final Drop Point</th>
                            <th>Travel Km</th>
                            <th>Rate with Reefer</th>
                            <th>Rate without Reefer</th>
                            <th>From(Tonnage)</th>
                            <th>To(Tonnage)</th>
                            <th>Base Freight</th>
                            <th>TAT (Hours)</th>
                            <th>Status</th>
                            <th>Created By</th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${localBillingList}" var="ptpl">
                            <%
                                        String className2 = "text1";
                                        if ((index % 2) == 0) {
                                            className2 = "text1";
                                        } else {
                                            className2 = "text2";
                                        }
                            %>
                            <tr >
                                <td class="form-control"  >
                                    <%=index%>
                                </td>
                                <td class="form-control" align="left" >
                                    <c:if test="${ptpl.contractCategory =='1'}">
                                        Export
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='2'}">
                                        Import
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='17'}">
                                        Vessel Empty
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='18'}">
                                        Stuffing Empty
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='19'}">
                                        Local PNR
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='20'}">
                                        Ex-Bonding
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='21'}">
                                        De-Stuff-Empty
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='23'}">
                                        PNR-MOFF
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='24'}">
                                        Offload - Mov
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='25'}">
                                        EX-AP-TY
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='26'}">
                                        EX-AP-RE
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='27'}">
                                        Local Export
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='28'}">
                                        Local Import
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='29'}">
                                        Coastal Inbound ( Import Moff )
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='30'}">
                                        Coastal Inbound ( Import Local )
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='31'}">
                                        Coastal Outbound ( Export Moff )
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='32'}">
                                        Coastal Outbound ( Export Local )
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='33'}">
                                        Repo (Moff)
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='34'}">
                                        Repo (Local)
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='35'}">
                                        Lease Box (Moff)
                                    </c:if>
                                    <c:if test="${ptpl.contractCategory =='36'}">
                                        Lease Box (Local)
                                    </c:if> 
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.vehicleTypeName}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:if test="${ptpl.loadTypeId =='1'}">
                                        Empty Trip
                                    </c:if>
                                    <c:if test="${ptpl.loadTypeId =='2'}">
                                        Load Trip
                                    </c:if>
                                </td>
                                <td class="form-control" align="left">
                                    <c:if test="${ptpl.containerTypeId =='1'}">
                                        20'
                                    </c:if>
                                    <c:if test="${ptpl.containerTypeId =='2'}">
                                        40'
                                    </c:if>
                                    <c:if test="${ptpl.containerTypeId =='4'}">
                                        0
                                    </c:if>
                                </td>
                                <td class="form-control" align="left">
                                    <c:if test="${ptpl.containerQty1 =='1'}">
                                        1
                                    </c:if>
                                    <c:if test="${ptpl.containerQty1 =='2'}">
                                        2
                                    </c:if>
                                    <c:if test="${ptpl.containerQty1 =='0'}">
                                        0
                                    </c:if>
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.firstPickupName}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.point1Name}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.point2Name}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.point3Name}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.point4Name}" />
                                </td>    
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.finalPointName}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.totalKm}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.rateWithReefer}" />
                                </td>    
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.rateWithoutReefer}" />
                                </td>    
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.fromCBM}"/>
                                </td>    
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.toCBM}"/>
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.baseFreight}"/>
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.tat}"/>
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.activeInd}" />
                                </td>
                                <td class="form-control" align="left">
                                    <c:out value="${ptpl.userName}" />
                                </td>
                            </tr>

                            <%index++;%>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
          </c:if>    
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>