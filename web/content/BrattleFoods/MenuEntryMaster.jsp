<%-- 
    Document   : MenuEntryMaster
    Created on : 21 Oct, 2015, 1:12:13 PM
    Author     : Gulshan kumar
--%>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">

    function submitPage()
    {
        var errStr = "";
        if (document.getElementById("entrymenu").value == "") {
            errStr = "Please enter the Menu.\n";
            alert(errStr);
            document.getElementById("entrymenu").focus();
        }
        else if (document.getElementById("entrytype").value == "0")
        {
            errStr = "Please select the Type.\n";
            alert(errStr);
            document.getElementById("entrytype").focus();
        }
        if (errStr == "") {
            document.productCategory.action = " /throttle/SaveMenuEntryMaster.do";
            document.productCategory.method = "post";
            document.productCategory.submit();
        }

    }


</script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body onload="document.productCategory.rtonamemaster.focus();">
        <form name="productCategory"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td class="contenthead" colspan="8" >Entry Master</td>
                    </tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Menu :</td>
                    <td class="text1"><input type="text" name="entrymenu" id="entrymenu" class="textbox" value=""/>
                    </td>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Type :</td>
                    <td class="text1">
                        <select id="entrytype" name="entrytype"  class="textbox" value="" autocomplete="off">
                            <option value="0" >Select</option>
                            <option value="1" >Menu</option>
                            <option value="2" >Sub Menu</option>
                            <option value="3" >Modules</option>
                        </select>
                    </td>
                    </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <h2 align="center">List Of The Menu Entry </h2>

            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>
                    <tr height="30">
                        <th width="50"><h3>S.No</h3></th>
                        <th width="120"><h3>Menu Name</h3></th>
                        <th width="150"><h3>Type</h3></th>
                    </tr>
                </thead>
                <tbody>

                    <% int sno = 0;%>
                    <c:if test = "${Menuentrylist != null}">
                        <c:forEach items="${Menuentrylist}" var="mel">
                            <%
                                sno++;
                                String className = "text1";
                                if ((sno % 1) == 0) {
                                    className = "text1";
                                } else {
                                    className = "text2";
                                }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="center"> <%= sno%>  </td>
                                <td class="<%=className%>"  align="center"> <c:out value="${mel.entrymenu}" /></td>
                                <td class="<%=className%>"  align="center">
                                    <c:if test="${mel.entrytype == '1'}" >
                                        Menu 
                                    </c:if>
                                    <c:if test="${mel.entrytype == '2'}" >
                                        Sub Menu 
                                    </c:if>
                                    <c:if test="${mel.entrytype == '3'}" >
                                        Module 
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>