package ets.domain.scheduler.web;

import ets.domain.report.business.ReportTO;
import ets.domain.thread.web.SendEmail;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

class sqlserviceconn6 extends Thread {

    String TallyCompId = "";
    String TripNo = "";
    String transactionType = "";
    String tranNo = "";
    String itemName = "";
    String itemGroup = "";
    String itemUOM = "";
    String itemQty = "";
    String itemRate = "";
    String itemTaxRate = "";
    String itemHSNSACCode = "";
    String itemTaxAmount = "";
    String itemTaxAssessable = "";
    String itemTotalAmount = "";

    public sqlserviceconn6(ReportTO reportTO) {

        TallyCompId = reportTO.getTallyCmpId();
        TripNo = reportTO.getTripNos();
        transactionType = reportTO.getTransactionType();
        tranNo = reportTO.getTranNo();
        itemName = reportTO.getItemName();
        itemGroup = reportTO.getItemGroup();
        itemUOM = reportTO.getItemUOM();
        itemQty = reportTO.getItemQty();
        itemRate = reportTO.getItemRate();
        itemTaxRate = reportTO.getItemTaxRate();
        itemHSNSACCode = reportTO.getItemHSNSACCode();
        itemTaxAmount = reportTO.getItemTaxAmount();
        itemTaxAssessable = reportTO.getItemTaxAssessable();
        itemTotalAmount = reportTO.getItemTotalAmount();

    }

    public void run() {
        int updateStatus = 0;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sqlQuery = "";
        String tripNo = "";
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conn = DriverManager.getConnection("jdbc:sqlserver://123.136.170.227:1433;ABCTEST=company", "sa", "triway");

            if (conn != null) {
                System.out.println("Database Successfully connected");

                sqlQuery = "INSERT INTO [ABCTEST].[dbo].[InventoryTransactionDetails](TallyCmpId,TripNo,TransactionType,\n" +
                        "TranNo,ItemName,ItemGroup,ItemUOM,ItemQty,\n"
                        + "itemRate,ItemTaxRate,ItemHSNSACCode,ItemTaxAmount,\n"
                        + "ItemTaxAssessable,ItemTotalAmount) VALUES "
                        + "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                stmt = conn.prepareStatement(sqlQuery);
                 
                System.out.println("TallyCompId="+TallyCompId);
                System.out.println("TripNo="+tranNo);
                System.out.println("transactionType="+transactionType);
                System.out.println("tranNo="+tranNo);
                System.out.println("itemName="+itemName);
                System.out.println("itemGroup="+itemGroup);
                System.out.println("itemUOM="+itemUOM);
                System.out.println("itemQty="+itemQty);
                System.out.println("itemRate="+itemRate);
                System.out.println("itemTaxRate="+itemTaxRate);
                System.out.println("itemHSNSACCode="+itemHSNSACCode);
                System.out.println("itemTaxAmount="+itemTaxAmount);
                System.out.println("itemTaxAssessable="+itemTaxAssessable);
                System.out.println("itemTotalAmount="+itemTotalAmount);
              
                stmt.setString(1, TallyCompId);
                stmt.setString(2, TripNo);
                stmt.setString(3, transactionType);
                stmt.setString(4, tranNo);
                stmt.setString(5, itemName);
                stmt.setString(6, itemGroup);
                stmt.setString(7, itemUOM);
                stmt.setString(8, itemQty);
                stmt.setString(9, itemRate);
                stmt.setString(10, itemTaxRate);
                stmt.setString(11, itemHSNSACCode);
                stmt.setString(12, itemTaxAmount);
                stmt.setString(13, itemTaxAssessable);
                stmt.setString(14, itemTotalAmount);

                updateStatus = stmt.executeUpdate();
                System.out.println("stmt-----"+stmt);
                System.out.println("UPDATE = " + updateStatus);

//                while (rs.next()) {
//                    tripNo = rs.getString("TripNo");
//                }
//                System.out.println("tripNo==========" + tripNo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
              if (conn != null) {            
                  try {
                      conn.close();
                  } catch (SQLException ex) {
                      Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                  }
              }
            }
        if (updateStatus != 0) {
//        if (!"".equals(updateStatus) && "0".equals(updateStatus)) {

            String fileNameNew = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/" + fileNameNew);
            try {
                dbProps.load(is);//this may throw IOException
            } catch (IOException ex) {
                Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
            }
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            int status = 0;
            Connection con = null;
            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try {
                    PreparedStatement updateMailDetails = null;
                    String sql = "UPDATE InventoryTransactionDetails SET \n"
                            + " throttle_postind = 1"
                            + " WHERE  TripNo = ?";
                    updateMailDetails = con.prepareStatement(sql);
                    updateMailDetails.setString(1, TripNo);
                    status = updateMailDetails.executeUpdate();
                    System.out.println("NEW status = " + status);

                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("TripNo doesn't exist.");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }finally{
              if (con != null) {            
                  try {
                      con.close();
                  } catch (SQLException ex) {
                      Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                  }
              }
            }
        }
    }
}
