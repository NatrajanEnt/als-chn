<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>

<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@ page import="ets.domain.racks.business.RackTO" %> 

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script >
function submitPage(value){
if(value == "add"){
document.manageRocks.action = '/throttle/addSubRack.do';
document.manageRocks.submit();
}else if(value == 'alter'){
document.manageRocks.action = '/throttle/handleViewSubRackAlter.do';
document.manageRocks.submit();
}
}
</script>


<script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>

     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.SubRack"  text="SubRacks"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="general.label.stores"  text="Stores"/></a></li>
          <li class="active"><spring:message code="stores.label.SubRack"  text="SubRacks"/></li>
        </ol>
      </div>
      </div>


<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
<body>
<form name="manageRocks" method="post" >
  

<table class="table table-info mb30 table-hover" id="table">
<thead>

<tr>
<th ><spring:message code="stores.label.RackName"  text="default text"/>
</th>
<th ><spring:message code="stores.label.SubRackName"  text="default text"/>
</th>
<th ><spring:message code="stores.label.SubRackDescription"  text="default text"/>
</th>
<th ><spring:message code="stores.label.Status"  text="default text"/>
</th>
</tr>
</thead>
<% int index=0; %>
  <c:if test = "${subRackLists != null}" >
      <c:forEach items="${subRackLists}" var="subRack"> 
<%
String classText = "";
int oddEven = index % 2;
if (oddEven > 0) {
classText = "text2";
} else {
classText = "text1";
}
%>	
<tr>
<td ><c:out value="${subRack.rackName}"/> </td>
<td ><c:out value="${subRack.subRackName}"/> </td>
<td ><c:out value="${subRack.subRackDescription}"/> </td>
<td ><c:out value="${subRack.subRackStatus}"/> </td>
</tr>
 <%
  index++;
  %>
</c:forEach>
</c:if>    
</table>
<br>
<center>
<input type="button" class="btn btn-success" value="<spring:message code="stores.label.ADD"  text="default text"/>" name="add" onClick="submitPage(this.name)">
<input type="button" class="btn btn-success" value="<spring:message code="stores.label.ALTER"  text="default text"/>" name="alter" onClick="submitPage(this.name)">
</center>
<input type="hidden" value="" name="reqfor">
<br>

<script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>

<%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
</body>

      </div>
      </div>
      </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
