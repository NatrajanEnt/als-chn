/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.accident.data;

import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import ets.domain.util.FPLogUtils;
import ets.domain.accident.business.AccidentTO;
import java.io.File;
import java.io.FileInputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Date;
import java.util.Calendar;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class AccidentDAO extends SqlMapClientDaoSupport {

    private Object vendorId;

    public AccidentDAO() {
    }
    private final static String CLASS = "AccidentDAO";

    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    
     public int saveFileUploadDetails(String actualFilePath, String fileSaved,AccidentTO accidentTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {


            map.put("fileName", fileSaved);
            map.put("userId", userId);
            map.put("vehicleID", accidentTO.getVehicleId());
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] uploadfile = new byte[(int) file.length()];
            System.out.println("uploadfile = " + uploadfile);
            fis.read(uploadfile);
            fis.close();
            map.put("uploadfile", uploadfile);
            System.out.println("the saveFileUploadDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("accident.saveFileUploadDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveFileUploadDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveFileUploadDetails List", sqlException);
        }

        return status;
    }
    //jp accident
    public ArrayList getVehicleAccidentList(String accidentId) {
               Map map = new HashMap();
               ArrayList vehicleAccidentList = new ArrayList();
               map.put("Accident_Id",accidentId);
               try {
                   vehicleAccidentList = (ArrayList) getSqlMapClientTemplate().queryForList("accident.getVehicleAccidentLists", map);
                   System.out.println("getVehicleAccidentList = " + vehicleAccidentList.size());
               } catch (Exception sqlException) {
                   sqlException.printStackTrace();
                   /*
                    * Log the exception and propagate to the calling class
                    */
                   FPLogUtils.fpDebugLog("getVehicleAccidentList Error" + sqlException.toString());
                   FPLogUtils.fpErrorLog("sqlException" + sqlException);
                   throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleAccidentList", sqlException);
               }
               return vehicleAccidentList;
           }
        public ArrayList getVehicleAccident() {
               Map map = new HashMap();
               ArrayList VehicleAccident = new ArrayList();
       //        map.put("vehicleId",vehicleId);
               try {
                   VehicleAccident = (ArrayList) getSqlMapClientTemplate().queryForList("accident.getVehicleAccident", map);
                   System.out.println("getVehicleAccident = " + VehicleAccident.size());
               } catch (Exception sqlException) {
                   sqlException.printStackTrace();
                   /*
                    * Log the exception and propagate to the calling class
                    */
                   FPLogUtils.fpDebugLog("getVehicleAccident Error" + sqlException.toString());
                   FPLogUtils.fpErrorLog("sqlException" + sqlException);
                   throw new FPRuntimeException("EM-SYS-01", CLASS, "getVehicleAccident", sqlException);
               }
               return VehicleAccident;
           }
           public int insertVehicleAccidentDetails(AccidentTO accidentTO, int UserId, String[] actualFilePath, String tripSheetId1, String[] fileSaved, String[] vehicleDetails,String[] accidentUploadId,String altVehicleId) {
               System.out.println("vehicle DAO");
               Map map = new HashMap();
      
               /*
                * set the parameters in the map for sending to ORM
                */
               FileInputStream fis = null;
               int tripId = 0;
               int Accident_Id = 0;
               int vehicleDetails2 = 0;
               int ins_MobileNo = 0;
               int imageInsertStatus=0;
                byte[] blobData = new byte[100];
              String alterVehicleid="";
               /*
                * set the parameters in the map for sending to ORM
                */
                 try {
   
                   if (tripSheetId1 != null && tripSheetId1 != "") {
                       tripId = Integer.parseInt(tripSheetId1);
                       map.put("tripSheetId", tripId);
                   }
  
                   // System.out.println("actualFilePath[0] = " + actualFilePath[0]);
                   System.out.println("actualFilePath.length = " + actualFilePath.length);
                   map.put("vehicleId", accidentTO.getVehicleId());
                   map.put("userId", UserId);
                   map.put("acc_VehicleNo", accidentTO.getAcc_VehicleNo());
       
                   map.put("acc_VehicleDetail", accidentTO.getAcc_VehicleDetail());
                   map.put("acc_DriverName", accidentTO.getAcc_DriverName());
                   map.put("acc_DriverLicNo", accidentTO.getAcc_DriverLicNo());
                   map.put("acc_InsurerName", accidentTO.getAcc_InsurerName());
                   map.put("acc_PolicyNo", accidentTO.getAcc_PolicyNo());
       
                   map.put("acc_ContactDetail", accidentTO.getAcc_ContactDetail());
                   map.put("acc_ContactPerson", accidentTO.getAcc_ContactPerson());
                   map.put("casualty_Name", accidentTO.getCasualty_Name());
                   map.put("casualty_Address", accidentTO.getCasualty_Address());
                   map.put("casualty_ConDetail", accidentTO.getCasualty_ConDetail());
                   map.put("casualty_Type", accidentTO.getCasualty_Type());
       
                   map.put("casualty_AlterNo", accidentTO.getCasualty_AlterNo());
                   map.put("casualty_ClaimDetail", accidentTO.getCasualty_ClaimDetail());
                   map.put("acc_ReferenceNo", accidentTO.getAcc_ReferenceNo());
                   map.put("acc_SurveyorNo", accidentTO.getAcc_SurveyorNo());
       //      map.put("ownership", accidentTO.getLeasingCustId());
                   map.put("acc_FirNo", accidentTO.getAcc_FirNo());
                   map.put("witeness_Name", accidentTO.getWiteness_Name());
                   map.put("witeness_Address", accidentTO.getWiteness_Address());
                   map.put("witeness_Contact", accidentTO.getWiteness_Contact());
                   map.put("rto_Fine", accidentTO.getRto_Fine());
                   map.put("rto_StationRelease", accidentTO.getRto_StationRelease());
                   map.put("rto_Date", accidentTO.getRto_Date());
                   map.put("ownerShips", accidentTO.getOwnerShips());
       
                   map.put("workshop_Type", accidentTO.getWorkshop_Type());
                   map.put("ins_Tie_up", accidentTO.getIns_Tie_up());
                   map.put("adhoc_Type", accidentTO.getAdhoc_Type());
                   map.put("surv_InspectionDate", accidentTO.getSurv_InspectionDate());
       
                   map.put("surv_QuotationAmt", accidentTO.getSurv_QuotationAmt());
       
                   map.put("surv_InsurerName", accidentTO.getSurv_InsurerName());
                   map.put("surv_PolicyNo", accidentTO.getSurv_PolicyNo());
                   map.put("reim_Date", accidentTO.getReim_Date());
                   map.put("reim_Amt", accidentTO.getReim_Amt());
                   map.put("reim_VehicleExcpen", accidentTO.getReim_VehicleExcpen());
                   map.put("reim_BillAmt", accidentTO.getReim_BillAmt());
       
                   map.put("lawyerName", accidentTO.getLawyerName());
                   map.put("law_ContactNo", accidentTO.getLaw_ContactNo());
                   map.put("law_court_Address", accidentTO.getLaw_court_Address());
                   map.put("law_Fees", accidentTO.getLaw_Fees());
                   map.put("miscels_Fees", accidentTO.getMiscels_Fees());
                   map.put("ins_Insurer", accidentTO.getIns_Insurer());
                   map.put("ins_Owner", accidentTO.getIns_Owner());
                   map.put("adhoc_BillAmt", accidentTO.getAdhoc_BillAmt());
                   map.put("adhoc_PaidAmt", accidentTO.getAdhoc_PaidAmt());
                   map.put("ploice_Address", accidentTO.getPloice_Address());
                   map.put("acc_SurveyorName", accidentTO.getAcc_SurveyorName());
                   map.put("spotAddress", accidentTO.getSpotAddress());
                   map.put("accidentDate", accidentTO.getAccidentDate());
                   map.put("accFirDate", accidentTO.getAccFirDate());
                   map.put("perOfDisability", accidentTO.getPerOfDisability());
                   
                   map.put("Accident_Id", accidentTO.getAccident_Id());
       
                     System.out.println("accidentTO.getAccident_Id() = " + accidentTO.getAccident_Id());
                  
   
                
       
                   System.out.println("map firsteerr:::::: = " + map);
                   Accident_Id = (Integer) getSqlMapClientTemplate().update("accident.updateAddVehicleAccident", map);
                   if(Accident_Id > 0){
                      
                       int insertStat = (Integer) getSqlMapClientTemplate().update("accident.updateAccidentStat", map);
                   } 
                   System.out.println("Accident_Id = " + Accident_Id);
                    if (Accident_Id > 0) {
  
                         for (int x = 0; x < actualFilePath.length; x++) {
                       String vehicleDetailfile = vehicleDetails[x];
                       System.out.println("fileName map = " + map);
                       System.out.println("actualFilePath = " + actualFilePath[x]);
                       File file = new File(actualFilePath[x]);
                       System.out.println("file = " + file);
                       fis = new FileInputStream(file);
                       System.out.println("fis = " + fis);
                       blobData = new byte[(int) file.length()];
                       System.out.println("blobData = " + blobData);
                       map.put("blobData", blobData);
                       fis.read(blobData);
                       fis.close();
                       map.put("fileName", fileSaved[x]);
                       map.put("uploadFileType", vehicleDetails[x]);
                       System.out.println("podfile map = " + map);
                       System.out.println("the saveTripPodDetails123455" + map);
   
   
                       System.out.println("podfile map = " + map);
                       System.out.println("the saveTripPodDetails123455" + map);
                    System.out.println("map uploads:::::: = " + map);
                   imageInsertStatus = (Integer) getSqlMapClientTemplate().update("accident.insertAttachVehicleAccident", map);
                   System.out.println("imageInsertStatus = " + imageInsertStatus);
                   }
                         }
   
   
                   
  
                   
       
               } catch (Exception sqlException) {
                   sqlException.printStackTrace();
                   /*
                    * Log the exception and propagate to the calling class
                    */
       
                   FPLogUtils.fpDebugLog("insertAddVehicleAccident Error" + sqlException.toString());
                   FPLogUtils.fpErrorLog("sqlException" + sqlException);
                   throw new FPRuntimeException("EM-MFR-02", CLASS, "insertAddVehicleAccident", sqlException);
               }
               return Accident_Id;
           }
       
           public ArrayList processVehicleAccidentDetail(AccidentTO accidentTO) {
       
               Map map = new HashMap();
               ArrayList VehicleAccidentDetailUnique = new ArrayList();
               /*
                * set the parameters in the map for sending to ORM
                */
       
               map.put("accident_Id", accidentTO.getAccident_Id());
       
       
       
               System.out.println("mapEdit= = " + map);
               try {
       
                   VehicleAccidentDetailUnique = (ArrayList) getSqlMapClientTemplate().queryForList("accident.processVehicleAccidentDetails", map);
                   System.out.println("VehicleAccidentDetailUniqueee= " + VehicleAccidentDetailUnique.size());
       
               } catch (Exception sqlException) {
                   sqlException.printStackTrace();
                   /*
                    * Log the exception and propagate to the calling class
                    */
                   FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
                   FPLogUtils.fpErrorLog("sqlException" + sqlException);
                   throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
               }
               return VehicleAccidentDetailUnique;
           }
           public int updateSaveAccidentVehicle(AccidentTO accidentTO, int UserId, String[] actualFilePath, String tripSheetId1, String[] fileSaved, String[] vehicleDetails,String[] insuImageId, String[] editStatus,String[] editStatus1,String[] editStatus2) {
               System.out.println("vehicle DAO");
               Map map = new HashMap();
       
               /*
                * set the parameters in the map for sending to ORM
                */
              FileInputStream fis = null;
           int tripId = 0;
           int Accident_Id = 0;
           int vehicle_id = 0;
           int vehicleDetails2 = 0;
           int ins_MobileNo = 0;
           int imageInsertStatus = 0;
           int updateInDetail =0;
           String alterVehicleid = "";
           byte[] blobData = new byte[100];
           int y=0;
               /*
                * set the parameters in the map for sending to ORM
                */
               try {
       
                   if (tripSheetId1 != null && tripSheetId1 != "") {
                       tripId = Integer.parseInt(tripSheetId1);
                       map.put("tripSheetId", tripId);
                   }
   
       
                   System.out.println("accidentTO   vehicle acc no" + accidentTO.getAcc_VehicleNo());
                   System.out.println("accidentTO   vehicle acc no1" + accidentTO.getAcc_VehicleDetail());
                   System.out.println("accidentTO   vehicle acc no2" + accidentTO.getAcc_DriverLicNo());
       
       
                   map.put("vehicleId", accidentTO.getVehicleId());
                   map.put("userId", UserId);
                   map.put("acc_VehicleNo", accidentTO.getAcc_VehicleNo());
       
                   map.put("acc_VehicleDetail", accidentTO.getAcc_VehicleDetail());
                   map.put("acc_DriverName", accidentTO.getAcc_DriverName());
                   map.put("acc_DriverLicNo", accidentTO.getAcc_DriverLicNo());
                   map.put("acc_InsurerName", accidentTO.getAcc_InsurerName());
                   map.put("acc_PolicyNo", accidentTO.getAcc_PolicyNo());
       
                   map.put("acc_ContactDetail", accidentTO.getAcc_ContactDetail());
                   map.put("acc_ContactPerson", accidentTO.getAcc_ContactPerson());
                   map.put("casualty_Name", accidentTO.getCasualty_Name());
                   map.put("casualty_Address", accidentTO.getCasualty_Address());
                   map.put("casualty_ConDetail", accidentTO.getCasualty_ConDetail());
                   map.put("casualty_Type", accidentTO.getCasualty_Type());
       
                   map.put("casualty_AlterNo", accidentTO.getCasualty_AlterNo());
                   map.put("casualty_ClaimDetail", accidentTO.getCasualty_ClaimDetail());
                   map.put("acc_ReferenceNo", accidentTO.getAcc_ReferenceNo());
                   map.put("acc_SurveyorNo", accidentTO.getAcc_SurveyorNo());
       //      map.put("ownership", accidentTO.getLeasingCustId());
                   map.put("acc_FirNo", accidentTO.getAcc_FirNo());
                   map.put("witeness_Name", accidentTO.getWiteness_Name());
                   map.put("witeness_Address", accidentTO.getWiteness_Address());
                   map.put("witeness_Contact", accidentTO.getWiteness_Contact());
                   map.put("rto_Fine", accidentTO.getRto_Fine());
                   map.put("rto_StationRelease", accidentTO.getRto_StationRelease());
                   map.put("rto_Date", accidentTO.getRto_Date());
                   map.put("ownerShips", accidentTO.getOwnerShips());
       
                   map.put("workshop_Type", accidentTO.getWorkshop_Type());
                   map.put("ins_Tie_up", accidentTO.getIns_Tie_up());
                   map.put("adhoc_Type", accidentTO.getAdhoc_Type());
                   map.put("surv_InspectionDate", accidentTO.getSurv_InspectionDate());
       
                   map.put("surv_QuotationAmt", accidentTO.getSurv_QuotationAmt());
       
                   map.put("surv_InsurerName", accidentTO.getSurv_InsurerName());
                   map.put("surv_PolicyNo", accidentTO.getSurv_PolicyNo());
                   map.put("reim_Date", accidentTO.getReim_Date());
                   map.put("reim_Amt", accidentTO.getReim_Amt());
                   map.put("reim_VehicleExcpen", accidentTO.getReim_VehicleExcpen());
                   map.put("reim_BillAmt", accidentTO.getReim_BillAmt());
       
                   map.put("lawyerName", accidentTO.getLawyerName());
                   map.put("law_ContactNo", accidentTO.getLaw_ContactNo());
                   map.put("law_court_Address", accidentTO.getLaw_court_Address());
                   map.put("law_Fees", accidentTO.getLaw_Fees());
                   map.put("miscels_Fees", accidentTO.getMiscels_Fees());
       
                   map.put("ins_Insurer", accidentTO.getIns_Insurer());
                   map.put("ins_Owner", accidentTO.getIns_Owner());
                   map.put("adhoc_BillAmt", accidentTO.getAdhoc_BillAmt());
                   map.put("adhoc_PaidAmt", accidentTO.getAdhoc_PaidAmt());
                   map.put("ploice_Address", accidentTO.getPloice_Address());
                   map.put("acc_SurveyorName", accidentTO.getAcc_SurveyorName());
                   map.put("spotAddress", accidentTO.getSpotAddress());
                   map.put("accidentDate", accidentTO.getAccidentDate());
                   map.put("accFirDate", accidentTO.getAccFirDate());
                   map.put("perOfDisability", accidentTO.getPerOfDisability());
       
                  // map.put("accident_Id", accidentTO.getAccident_Id());
   
   
                    alterVehicleid = accidentTO.getAccident_Id();
              // map.put("accident_Id", Integer.parseInt(accidentTO.getAccident_Id()));
               System.out.println("map first:::::: = " + map);
               int status = 0;
               String code = "";
               String[] temp;
               int z=0;
               int stat =0;
               int currentYear = 0;
               String newYear = "";
               int VehicleLedgerId = 0;
                 if (alterVehicleid != null && !"".equals(alterVehicleid)) {
                   System.out.println("alterVehicleid = " + alterVehicleid);
                   map.put("Accident_Id", alterVehicleid);
                   Accident_Id = (Integer) getSqlMapClientTemplate().update("accident.updateAddVehicleAccident", map);
   
   
                   System.out.println("accident_Id = " + Accident_Id);
//                   System.out.println("actualFilePath.length = " + actualFilePath.length);
//                   System.out.println("insuImageId.length = " + insuImageId.length);
//                   System.out.println("editStatus.length = " + editStatus.length);
//                   System.out.println("vehicleDetails = " + vehicleDetails.length);
                   for (int x = 0; x < vehicleDetails.length; x++) {
                       System.out.println("x value= " + x);
                       String vehicleDetailfile = vehicleDetails[x];
                       System.out.println("fileName map = " + map);
   //                    System.out.println("actualFilePath = " + actualFilePath[x]);
                       if ( actualFilePath.length>y) {
                           File file = new File(actualFilePath[y]);
                           map.put("fileName", fileSaved[y]);
                           System.out.println("file = " + file);
                           fis = new FileInputStream(file);
                           System.out.println("fis = " + fis);
                           blobData = new byte[(int) file.length()];
                           System.out.println("podFile = " + blobData);
                           fis.read(blobData);
                           fis.close();
   
                       }
                       if (x < insuImageId.length) {
                           System.out.println("insuImageId.length = " + insuImageId.length);
                           System.out.println("editStatus2.length = " + editStatus2.length);
                           if (z < editStatus.length) {
                               System.out.println("editStatus[z] = " + editStatus[z]);
                               if (actualFilePath.length>0 && editStatus1.length>0) {
                                   if(Integer.parseInt(editStatus1[z]) == x){
                                   map.put("insuImageId", editStatus[z]);
                                   map.put("uploadFileType", vehicleDetails[x]);
                                   map.put("blobData", blobData);
                                   System.out.println(" map for all = " + map);
                                   imageInsertStatus = (Integer) getSqlMapClientTemplate().update("accident.updateallAccidentUploadDetails", map);
                                   y++;
                                   z++;
                                   }
   
                               } else if(Integer.parseInt(editStatus2[stat])== x){
                                    map.put("insuImageId", editStatus[z]);
                                   System.out.println("i am in else condition ");
                                   System.out.println(" vehicleDetails[x] = " +  vehicleDetails[x]);
                                   map.put("uploadFileType", vehicleDetails[x]);
                                   System.out.println(" map upload file Type = " + map);
                                   imageInsertStatus = (Integer) getSqlMapClientTemplate().update("accident.updatefileTypeAccidentDetails", map);
                                   int temp1 = stat+1;
                                   z++;
                                   if( temp1<editStatus2.length)
                                           stat++;
   
                               }
                           }
   
                       } else{
   
                           map.put("uploadFileType", vehicleDetails[x]);
                           map.put("blobData", blobData);
                           imageInsertStatus = (Integer) getSqlMapClientTemplate().update("accident.insertAttachVehicleAccident", map);
                           y++;
                       }
                       System.out.println("the saveTripPodDetails123455" + map);
                   }
   
   
            }
               System.out.println("Accident_Id = " + Accident_Id);
       
       
       
                   //int insertStatus=0;
       
   //                    System.out.println("map Second::::::" + map);
   //                    Accident_Id = (Integer) getSqlMapClientTemplate().update("accident.updateAddVehicleAccident", map);
   //                    System.out.println("Accident_Ideeeee----" + Accident_Id);
       
               } catch (Exception sqlException) {
                   sqlException.printStackTrace();
                   /*
                    * Log the exception and propagate to the calling class
                    */
       
                   FPLogUtils.fpDebugLog("insertAddVehicleAccident Error" + sqlException.toString());
                   FPLogUtils.fpErrorLog("sqlException" + sqlException);
                   throw new FPRuntimeException("EM-MFR-02", CLASS, "insertAddVehicleAccident", sqlException);
               }
               return Accident_Id;
       }
             public ArrayList getVehicleAccidentImageDetails(int accident_Id) {
           Map map = new HashMap();
           ArrayList AccidentUploadDetails = new ArrayList();
           map.put("accident_Id", accident_Id);
   
           System.out.println("map" + map);
   
           try {
   
               AccidentUploadDetails = (ArrayList) getSqlMapClientTemplate().queryForList("accident.vehicleAccidentImageDetails", map);
               System.out.println("vehicleAccidentImageDetails size:" + AccidentUploadDetails.size());
           } catch (Exception sqlException) {
               sqlException.printStackTrace();
               /*
                * Log the exception and propagate to the calling class
                */
               FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
               FPLogUtils.fpErrorLog("sqlException" + sqlException);
               throw new FPRuntimeException("EM-SYS-01", CLASS, "vehicleAccidentImageDetails", sqlException);
           }
           return AccidentUploadDetails;
   
    } 
             
             public ArrayList processVehicleAccidentHistory(AccidentTO accidentTO) {

               Map map = new HashMap();
               ArrayList vehicleAccidentHistory = new ArrayList();
               /*
                * set the parameters in the map for sending to ORM
                */

            
              
               try {

                   vehicleAccidentHistory = (ArrayList) getSqlMapClientTemplate().queryForList("accident.getVehicleAccidentHistory", map);
                   System.out.println("vehicleAccidentHistory= " + vehicleAccidentHistory.size());

               } catch (Exception sqlException) {
                   sqlException.printStackTrace();
                   /*
                    * Log the exception and propagate to the calling class
                    */
                   FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
                   FPLogUtils.fpErrorLog("sqlException" + sqlException);
                   throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
               }
               return vehicleAccidentHistory;
           }
             public ArrayList getVehicleAccidentHistorylist(AccidentTO accidentTO,String vehicleId) {
	   
	                  Map map = new HashMap();
	                  ArrayList vehicleAccidentHistorylist = new ArrayList();
	                  /*
	                   * set the parameters in the map for sending to ORM
	                   */
	   
	                  map.put("vehicleId", vehicleId);
	   
	   
	   
	                  System.out.println("map=" + map);
	                  try {
	   
	                      vehicleAccidentHistorylist = (ArrayList) getSqlMapClientTemplate().queryForList("accident.VehicleAccidentHistoryList", map);
	                      System.out.println("vehicleAccidentHistorylist= " + vehicleAccidentHistorylist.size());
	   
	                  } catch (Exception sqlException) {
	                      sqlException.printStackTrace();
	                      /*
	                       * Log the exception and propagate to the calling class
	                       */
	                      FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
	                      FPLogUtils.fpErrorLog("sqlException" + sqlException);
	                      throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
	                  }
	                  return vehicleAccidentHistorylist;
	              }
	                public ArrayList viewvehicleAccidentHistorylist(AccidentTO accidentTO) {
	   
	                  Map map = new HashMap();
	                  ArrayList viewvehicleAccidentHistorylist = new ArrayList();
	                  /*
	                   * set the parameters in the map for sending to ORM
	                   */
	   
	                   map.put("accident_Id", accidentTO.getAccident_Id());
	                   map.put("insId", accidentTO.getInsId());
	   
	   
	   
	                  System.out.println("map= = " + map);
	                  try {
	   
	                      viewvehicleAccidentHistorylist = (ArrayList) getSqlMapClientTemplate().queryForList("accident.viewVehicleAccidentHistoryList", map);
	                      System.out.println("viewvehicleAccidentHistorylist= " + viewvehicleAccidentHistorylist.size());
	   
	                  } catch (Exception sqlException) {
	                      sqlException.printStackTrace();
	                      /*
	                       * Log the exception and propagate to the calling class
	                       */
	                      FPLogUtils.fpDebugLog("getVehicleDetail Error" + sqlException.toString());
	                      FPLogUtils.fpErrorLog("sqlException" + sqlException);
	                      throw new FPRuntimeException("EM-MFR-02", CLASS, "getVehicleDetail", sqlException);
	                  }
	                  return viewvehicleAccidentHistorylist;
           }

   
//             public ArrayList ProcessInsCompanyList() {
//        
//        Map map = new HashMap();
//        ArrayList InsComanyNameList = new ArrayList();
//        /*
//         * set the parameters in the map for sending to ORM
//         */
//
//        //    map.put("vehicleId", accidentTO.getVehicleId());
//        try {
//            // map.put("insId",insId);
//            // System.out.println("i m here 4");
//            InsComanyNameList = (ArrayList) getSqlMapClientTemplate().queryForList("accident.getProcessInsCompanyLists", map);
//            System.out.println("InsComanyNameList size:" + InsComanyNameList.size());
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getProcessInsCompanyLists Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MFR-02", CLASS, "getProcessInsCompanyLists", sqlException);
//        }
//        return InsComanyNameList;
//    }
             
             
    public int addAccidentVehicle(AccidentTO accidentTO,int userId) {
        Map map = new HashMap();
        int insertStatus = 0;
        int vehicleId =0;
        try {         
            vehicleId =Integer.parseInt(accidentTO.getVehicleId());
            System.out.println("vehicleId = " + vehicleId); 
            map.put("accVehicleNo", vehicleId);
            int insuranceId = (Integer) getSqlMapClientTemplate().queryForObject("accident.getVehicleInsuranceId", map);
            map.put("insuranceId", insuranceId);
            map.put("userId",userId);
            System.out.println("map = " + map);
            insertStatus = (Integer) getSqlMapClientTemplate().update("accident.updateAccidentDetail", map);
            System.out.println("insertStatus :" + insertStatus);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("addAccidentVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "addAccidentVehicle", sqlException);
        }
        return insertStatus;

    }
     public ArrayList getVehicleList() {
        ArrayList vehicleList = null;
        Map map = new HashMap();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("accident.getVehicleList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vehicleList;
    }
}
