<%--
    Document   : searchCustomerWiseProfitability
    Created on : Oct 30, 2013, 5:06:46 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
 <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>


 <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
<%
        String menuPath = "Finance >> Daily Advance Advice";
        request.setAttribute("menuPath", menuPath);
    %>
    <body>
                 <form name="customerWise" method="post">
             <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
            <table width="650" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                </h2></td>
                <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
        <div class="tabs" align="left" style="width:900;">
    <ul class="tabNavigation">
            <li style="background:#76b3f1">Finance Advance Advice</li>
    </ul>
            <div id="first">
<table width="600" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                   
                                    <tr>
                                        <td height="30"><font color="red">*</font>From Date</td>
                                        <td height="30"><input type="text" name="fromDate" class="form-control" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.fromDate,'dd-mm-yyyy',this)"/></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td><input type="text" name="toDate" class="form-control" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.settle.toDate,'dd-mm-yyyy',this)"/></td>
                                        <td>&nbsp;</td>
                                        <td><input type="button" class="button" name="search" onClick="submitPage(this);" value="Search"></td>
                                    </tr>
                                </table>
            </div>
            </div>
    </td>
    </tr>
    </table>
            <br>
                                           <br>
             <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
    <thead>

                    <tr >
                        <th width="34" class="contentsub">S.No</th>
                        <th width="100" class="contentsub">Advice Date</th>
                        <th width="150" class="contentsub">Details</th>

                    </tr>
             </thead>
             
                <tbody>
                    <tr>
				<td class="text1">1</td>
                                <td class="text1">01-11-2013</td>
                                <td class="text1"><a href="/throttle/BrattleFoods/financeAdviceDetails.jsp">view details</a></td>
			</tr>
                    <tr>
				<td class="text2">2</td>
                                <td class="text2">02-11-2013</td>
                                <td class="text2"><a href="/throttle/BrattleFoods//financeAdviceDetails.jsp">view details</a></td>
			</tr>
                    <tr>
				<td class="text1">3</td>
                                <td class="text1">03-11-2013</td>
                                <td class="text1"><a href="/throttle/BrattleFoods//financeAdviceDetails.jsp">view details</a></td>
			</tr>
                    <tr>
				<td class="text2">4</td>
                                <td class="text2">04-11-2013</td>
                                <td class="text2"><a href="/throttle/BrattleFoods//financeAdviceDetails.jsp">view details</a></td>
			</tr>
                    
                 
                  </tbody>
            </table>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        
    </body>
</html>
