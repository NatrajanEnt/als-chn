<%-- 
    Document   : tripPlanning
    Created on : Oct 29, 2013, 1:18:37 PM
    Author     : srinivasan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        
    </head>
    <script language="javascript">
        function submitWindow(){
            document.tripPlanning.action = '/throttle/BrattleFoods/viewTripPlanning.jsp';
            document.tripPlanning.submit();
        }
        function exportExcel(){
            document.tripPlanning.action = '/throttle/BrattleFoods/tripPlanningExcel.jsp';
            document.tripPlanning.submit();
        }
        function importExcel(){
            document.tripPlanning.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
            document.tripPlanning.submit();
        }
    </script>
    <body>
        <% String menuPath = "Operations >>  Trip Planning";
          request.setAttribute("menuPath", menuPath);
        %>
        <form name="tripPlanning" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:900;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Trip Planning</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td width="80" height="30"><font color="red">*</font>From Date</td>
                                        <td width="182"><input name="fromDate" type="text" class="datepicker" size="20"></td>
                                        <td width="80" height="30"><font color="red">*</font>To Date</td>
                                        <td width="182"><input name="toDate" type="text" class="datepicker" size="20"></td>
                                        <td><input type="button"   value="Search" class="button" name="search" onClick="submitWindow()"></td>
                                    </tr>
                                    
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table width="95%"  align="center" cellpadding="0" cellspacing="0" class="border">
                <thead>
                    <tr>
                        <td width="35" class="contentsub">Sno</td>
                        <td width="55" class="contentsub">CN No</td>
                        <td width="135" class="contentsub">Customer Name </td>
                        <td width="135" class="contentsub">Customer Code </td>
                        <td width="127" class="contentsub">Origin </td>
                        <td width="121" class="contentsub">Destination </td>
                        <td width="121" class="contentsub">Vehicle Type </td>
                        <td width="74" class="contentsub">Weight(MT)</td>
                        <td width="174" height="30" class="contentsub">Schedule Pickup Date &amp; Time </td>
                        <td width="174" height="30" class="contentsub">Estimated Delivery Date &amp; Time</td>
                        <td width="174" height="30" class="contentsub">Transit Days</td>
                        <td width="104" height="30" class="contentsub">Business Type </td>
                        <td width="134" height="30" class="contentsub">Service Type </td>
                    </tr>
                </thead>
                    <tr>
                        <td class="text1">1</td>
                        <td class="text1"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10012</a></td>
                        <td class="text1">M/S Sundaram & Co</td>
                        <td class="text1">BF001</td>
                        <td class="text1">Kashipur</td>
                        <td class="text1">Delhi</td>
                        <td class="text1">TATA/3118</td>
                        <td class="text1">20</td>
                        <td class="text1">07/11/2013&amp;07:30AM</td>
                        <td class="text1">10/11/2013&amp;05:30PM</td>
                        <td class="text1">4.5Days</td>
                        <td class="text1">Primary</td>
                        <td class="text1">FTL</td>
                    </tr>
                    <tr>
                        <td class="text2">2<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text2"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10879</a></td>
                        <td class="text2">M/S John & Co</td>
                        <td class="text2">BF019</td>
                        <td class="text2">Kashipur</td>
                        <td class="text2">Balwal</td>
                        <td class="text2">TATA/3118</td>
                        <td class="text2">35</td>
                        <td class="text2">07/11/2011&amp;10:00AM</td>
                        <td class="text2">08/11/2011&amp;10:00AM</td>
                        <td class="text2" >1 Day</td>
                        <td class="text2" >Primary</td>
                        <td class="text2">FTL</td>
                    </tr>
                    <tr>
                        <td class="text1">3<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text1"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10457</a></td>
                        <td class="text1">M/S Balaji & Co</td>
                        <td class="text1">BF005</td>
                        <td class="text1">Balwal</td>
                        <td class="text1">Delhi</td>
                        <td class="text1">AL/2516</td>
                        <td class="text1">25</td>
                        <td class="text1">07/11/2013&amp;04:00PM</td>
                        <td class="text1">09/11/2013&amp;04:00PM</td>
                        <td class="text1">2 Days</td>
                        <td class="text1">Primary</td>
                        <td class="text1">FTL</td>
                    </tr>
                    <tr>
                        <td class="text2">4</td>
                        <td class="text2"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10144</a></td>
                        <td class="text2" >M/S Prakash & Co</td>
                        <td class="text2">BF147</td>
                        <td class="text2">Kashipur</td>
                        <td class="text2">Chennai</td>
                        <td class="text2">TATA/3118</td>
                        <td class="text2">27</td>
                        <td class="text2">07/11/2013&amp;11:00AM</td>
                        <td class="text2">10/11/2013&amp;11:00AM</td>
                        <td class="text2">4 Days</td>
                        <td class="text2">Primary</td>
                        <td class="text2" >FTL</td>
                    </tr>
                    <tr>
                        <td class="text2">4<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text2"><a href="/throttle/BrattleFoods/consignmentNote.jsp">CN/13-14/10879</a></td>
                        <td class="text2">M/S John & Co</td>
                        <td class="text2">BF019</td>
                        <td class="text2">Kashipur</td>
                        <td class="text2">Balwal</td>
                        <td class="text2">TATA/3118</td>
                        <td class="text2">35</td>
                        <td class="text2">07/11/2011&amp;10:00AM</td>
                        <td class="text2">08/11/2011&amp;10:00AM</td>
                        <td class="text2" >1 Day</td>
                        <td class="text2" >Primary</td>
                        <td class="text2">FTL</td>
                    </tr>
                    
            </table>
            <br>
            <br>
            <center>
            <input type="button"   value="Download To Excel" class="button" name="search" onClick="exportExcel()">&nbsp;&nbsp;&nbsp;
            <input type="button"   value="Upload Trip Planning" class="button" name="search" onClick="importExcel()">
            </center>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
