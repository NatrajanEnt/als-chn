/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.billing.web;

/**
 *
 * @author vinoth
 */

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import ets.domain.billing.business.BillingBP;
import ets.domain.billing.business.BillingTO;
import ets.domain.operation.business.OperationBP;
import ets.domain.users.business.LoginBP;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.trip.web.TripCommand;
import ets.domain.util.ParveenErrorConstants;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;

public class BillingController extends BaseController {

    BillingCommand billingCommand;
    BillingBP billingBP;
    OperationBP operationBP;
    TripBP tripBP;
    LoginBP loginBP;
    TripCommand tripCommand;
    public BillingBP getBillingBP() {
        return billingBP;
    }

    public void setBillingBP(BillingBP billingBP) {
        this.billingBP = billingBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public BillingCommand getBillingCommand() {
        return billingCommand;
    }

    public void setBillingCommand(BillingCommand billingCommand) {
        this.billingCommand = billingCommand;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }
    
    

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        //////System.out.println("request.getRequestURI() = " + request.getRequestURI());
        binder.closeNoCatch();  initialize(request);

    }

     public ModelAndView viewClosedOrderForBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Display Closed Trips";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        String customerId = "";
        String companyId = "";
        String fromDate = "";
        String toDate = "";
        String billOfEntry = "";
        String shippingBillNo = "";
        String type = "";
        String movementType = "";
        try {

            path = "content/billing/viewClosedOrder.jsp";
            int userId = (Integer) session.getAttribute("userId");
            companyId = (String) session.getAttribute("companyId");
            System.out.println("companyId=="+companyId);
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            customerName = request.getParameter("Customer");
            customerId = request.getParameter("customerId");
            billOfEntry = request.getParameter("billOfEntryNo");
            shippingBillNo = request.getParameter("shipingBillNo");
            type = request.getParameter("type");
            movementType = request.getParameter("movementType");
            request.setAttribute("customerName", customerName);
            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("billOfEntryNo", billOfEntry);
            request.setAttribute("shippingBillNo", shippingBillNo);
            request.setAttribute("type", type);
            request.setAttribute("movementType", movementType);
            billingTO.setUserId(userId+"");
            billingTO.setFromDate(fromDate);
            billingTO.setToDate(toDate);
            billingTO.setCustomerName(customerName);
            billingTO.setCustomerId(customerId);
            billingTO.setBillOfEntryNo(billOfEntry);
            billingTO.setShipingBillNo(shippingBillNo);
            billingTO.setBillingType(type);
            billingTO.setMovementType(movementType);
            billingTO.setCompanyId(companyId);
//            if (!"0".equals(customerName)) {}
                 ArrayList movementTypeList = new ArrayList();
                movementTypeList = operationBP.getMovementTypeList();
                request.setAttribute("movementTypeList", movementTypeList);
                ArrayList customerList = new ArrayList();
                customerList = operationBP.getTripCustomerList();
                request.setAttribute("customerList", customerList);
                System.out.println("customerList Size Is :::" + customerList.size());
                
                ArrayList closedTrips = new ArrayList();
                closedTrips = billingBP.getOrdersForBilling(billingTO);
                request.setAttribute("closedTrips", closedTrips);
            

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

     
     
    public ModelAndView viewClosedRepoOrderForBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        System.out.println("viewClosedRepoOrderForBilling");
        String path = "";
        HttpSession session = request.getSession();
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Display Closed Trips";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String customerName = "";
        String customerId = "";
        String fromDate = "";
        String toDate = "";
        String billOfEntry = "";
        String shippingBillNo = "";
        String type = "";
        String movementType = "";
        try {

            path = "content/billing/viewClosedRepoOrder.jsp";
            int userId = (Integer) session.getAttribute("userId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            customerName = request.getParameter("Customer");
            customerId = request.getParameter("billingParty");
            billOfEntry = request.getParameter("billOfEntryNo");
            shippingBillNo = request.getParameter("shipingBillNo");
            type = request.getParameter("type");
            movementType = request.getParameter("movementType");
            request.setAttribute("customerName", customerName);
            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("billOfEntryNo", billOfEntry);
            request.setAttribute("shippingBillNo", shippingBillNo);
            request.setAttribute("type", type);
            request.setAttribute("movementType", movementType);
            billingTO.setUserId(userId+"");
            billingTO.setFromDate(fromDate);
            billingTO.setToDate(toDate);
            billingTO.setCustomerName(customerName);
            billingTO.setCustomerId(customerId);
            billingTO.setBillOfEntryNo(billOfEntry);
            billingTO.setShipingBillNo(shippingBillNo);
            billingTO.setBillingType(type);
            billingTO.setMovementType(movementType);
//            if (!"0".equals(customerName)) {}
                 ArrayList movementTypeList = new ArrayList();
                movementTypeList = operationBP.getMovementTypeList();
                request.setAttribute("movementTypeList", movementTypeList);
                ArrayList customerList = new ArrayList();
                customerList = operationBP.getTripCustomerList();
                request.setAttribute("customerList", customerList);
                System.out.println("customerList Size Is :::" + customerList.size());

                ArrayList closedTrips = new ArrayList();
                closedTrips = billingBP.getRepoOrdersForBilling(billingTO);
                request.setAttribute("closedTrips", closedTrips);
                System.out.println("closedTrips.size()="+closedTrips.size());


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView gererateOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Generate Bill ";
        String customerId="",tripSheetId="";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            String billList="Billing";
            customerId=request.getParameter("custId");
            tripSheetId=request.getParameter("tripSheetId");
            System.out.println("tripSheetId in controller:"+tripSheetId);
            System.out.println("custId in controller:"+customerId);
            request.setAttribute("customerId", customerId);
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("tripSheetId", tripSheetId);

            BillingTO billingTO = new BillingTO();

            String consignmentOrderId = request.getParameter("consignmentOrderId");
            request.setAttribute("consignmentOrderIds", consignmentOrderId);
             System.out.println("consignmentOrderId"+consignmentOrderId);
             
            String reeferRequired = request.getParameter("reeferRequired");
            billingTO.setConsignmentOrderId(consignmentOrderId);
            billingTO.setTripSheetId(tripSheetId);
            ArrayList ordersToBeBillingDetails = billingBP.getOrdersToBeBillingDetails(billingTO);
            ArrayList tripsOtherExpenseDetails = billingBP.getOrdersOtherExpenseDetails(billingTO);
            ArrayList gsttaxDetails=billingBP.getGSTTaxDetails(billingTO);
            if(gsttaxDetails.size()>0){
            Iterator it =gsttaxDetails.iterator();
            while(it.hasNext()){
             BillingTO billTO =  (BillingTO)it.next();
             if("CGST".equalsIgnoreCase(billTO.getGstName())){
                 System.out.println("CGST:"+billTO.getGstPercentage());
              request.setAttribute("CGST", billTO.getGstPercentage());
             }else if("SGST".equalsIgnoreCase(billTO.getGstName())){
                  System.out.println("SGST:"+billTO.getGstPercentage());
              request.setAttribute("SGST", billTO.getGstPercentage());
             }else if("IGST".equalsIgnoreCase(billTO.getGstName())){
                  System.out.println("IGST:"+billTO.getGstPercentage());
              request.setAttribute("IGST", billTO.getGstPercentage());
             }
            }
            }
            boolean articleFlag=false;
             if(ordersToBeBillingDetails.size()>0){
             Iterator itr1 =ordersToBeBillingDetails.iterator();
             while(itr1.hasNext()){
              BillingTO billTO1 =  (BillingTO)itr1.next();
//               request.setAttribute("gstType", billTO1.getGstType());
//                 System.out.println("gstType"+billTO1.getGstType());
                 System.out.println("billTO1.getCommodityCategory()"+billTO1.getCommodityCategory());
              if("General".equalsIgnoreCase(billTO1.getCommodityCategory()) ){
             articleFlag=true;
               
             }
              break;
             }
             }
             System.out.println("articleFlag:"+articleFlag);
             request.setAttribute("articleFlag", articleFlag);
             
            if(ordersToBeBillingDetails.size()>0){
                BillingTO billingTO1 =  (BillingTO)ordersToBeBillingDetails.get(0);
                 request.setAttribute("billingParty", billingTO1.getCustomerName());
                 request.setAttribute("billingPartyAddress", billingTO1.getCustomerAddress());
                 request.setAttribute("billingPartyId", billingTO1.getCustomerId());
                 request.setAttribute("customerId", billingTO1.getCustomerId());
                 request.setAttribute("movementType", billingTO1.getMovementType());
                 request.setAttribute("billList", billList);
                 request.setAttribute("billOfEntryNo", billingTO1.getBillOfEntryNo());
                 request.setAttribute("shipingBillNo", billingTO1.getShipingBillNo());
                 request.setAttribute("organizationId", billingTO1.getOrganizationId());
                 request.setAttribute("billingState", billingTO1.getBillingState());
                 request.setAttribute("GSTNo", billingTO1.getGstNo());
                 request.setAttribute("grDate", billingTO1.getGrDate());
                 request.setAttribute("panNo", billingTO1.getPanNo());
                 request.setAttribute("companyType", billingTO1.getCompanyType());
                 System.out.println("companyType"+ billingTO1.getCompanyType());
                 System.out.println("grDate:"+billingTO1.getGrDate());
                 System.out.println("movementType:"+billingTO1.getMovementType());
                 System.out.println("shipingBillNo:"+billingTO1.getShipingBillNo());
                 System.out.println("billOfEntryNo:"+billingTO1.getBillOfEntryNo());
                 System.out.println("billingState:"+billingTO1.getBillingState());
                 System.out.println("organizationId:"+billingTO1.getOrganizationId());
                 System.out.println("GSTNo:"+billingTO1.getGstNo());
            }
            request.setAttribute("tripsToBeBilledDetails", ordersToBeBillingDetails);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);
            request.setAttribute("reeferRequired", reeferRequired);
            request.setAttribute("consignmentOrderId", consignmentOrderId);
            String revenueApprovalStatus = "0";
            if(consignmentOrderId != null){
                request.setAttribute("consignmentOrderCount", consignmentOrderId.split(",").length);
                revenueApprovalStatus = tripBP.getRevenueApprovalStatus(customerId, "1");//1 indicates gps non usage approval request
            }
           System.out.println("revenueApprovalStatus:"+revenueApprovalStatus);
           request.setAttribute("revenueApprovalStatus", revenueApprovalStatus);
            path = "content/billing/viewOrderBillDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
     public ModelAndView gererateRepoOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        String menuPath = "Finance  >>  Generate Bill ";
        String customerId="",tripSheetId="", tripSheetIds="";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            customerId=request.getParameter("custId");
            tripSheetId=request.getParameter("tripSheetId");
            tripSheetIds=request.getParameter("tripIds");
            System.out.println(" tripSheetId:"+tripSheetId);
            request.setAttribute("customerId", customerId);
            request.setAttribute("pageTitle", pageTitle);
            request.setAttribute("tripSheetId", tripSheetIds);

            BillingTO billingTO = new BillingTO();
            String billList="RepoBilling";
            String consignmentOrderId = request.getParameter("consignmentOrderId");
             System.out.println("consignmentOrderId"+consignmentOrderId);
            String reeferRequired = request.getParameter("reeferRequired");
            billingTO.setConsignmentOrderId(consignmentOrderId);
            billingTO.setTripSheetId(tripSheetIds);
            ArrayList ordersToBeBillingDetails = billingBP.getOrdersToBeBillingDetails(billingTO);
            ArrayList tripsOtherExpenseDetails = billingBP.getOrdersOtherExpenseDetails(billingTO);
            
            
             ArrayList gsttaxDetails=billingBP.getGSTTaxDetails(billingTO);
            if(gsttaxDetails.size()>0){
            Iterator it =gsttaxDetails.iterator();
            while(it.hasNext()){
             BillingTO billTO =  (BillingTO)it.next();
             if("CGST".equalsIgnoreCase(billTO.getGstName())){
                 System.out.println("CGST:"+billTO.getGstPercentage());
              request.setAttribute("CGST", billTO.getGstPercentage());
             }else if("SGST".equalsIgnoreCase(billTO.getGstName())){
                  System.out.println("SGST:"+billTO.getGstPercentage());
              request.setAttribute("SGST", billTO.getGstPercentage());
             }else if("IGST".equalsIgnoreCase(billTO.getGstName())){
                  System.out.println("IGST:"+billTO.getGstPercentage());
              request.setAttribute("IGST", billTO.getGstPercentage());
             }
            }
            }
             boolean articleFlag=true;
             if(ordersToBeBillingDetails.size()>0){
             Iterator itr1 =ordersToBeBillingDetails.iterator();
             while(itr1.hasNext()){
              BillingTO billTO1 =  (BillingTO)itr1.next();
               request.setAttribute("gstType", billTO1.getGstType());
               System.out.println("gstType"+billTO1.getGstType());
              if(billTO1.getArticleName() !="Rice" && billTO1.getArticleName() !="rice" && billTO1.getArticleName() !="RICE"){
             articleFlag=false;
               
             }
              break;
             }
             }
             request.setAttribute("articleFlag", articleFlag);
            if(ordersToBeBillingDetails.size()>0){
                BillingTO billingTO1 =  (BillingTO)ordersToBeBillingDetails.get(0);
                 request.setAttribute("billingParty", billingTO1.getCustomerName());
                  request.setAttribute("billingPartyAddress", billingTO1.getCustomerAddress());
                 request.setAttribute("billingPartyId", billingTO1.getCustomerId());
                 request.setAttribute("customerId", billingTO1.getCustomerId());
                 request.setAttribute("movementType", billingTO1.getMovementType());
                 request.setAttribute("billList", billList);
                 request.setAttribute("billOfEntryNo", billingTO1.getBillOfEntryNo());
                 request.setAttribute("shipingBillNo", billingTO1.getShipingBillNo());
                  request.setAttribute("organizationId", billingTO1.getOrganizationId());
                 request.setAttribute("billingState", billingTO1.getBillingState());
                 request.setAttribute("GSTNo", billingTO1.getGstNo());
                 request.setAttribute("grDate", billingTO1.getGrDate());
                 request.setAttribute("panNo", billingTO1.getPanNo());
                 System.out.println("grDate:"+billingTO1.getGrDate());
                 System.out.println("movementType:"+billingTO1.getMovementType());
                 System.out.println("shipingBillNo:"+billingTO1.getShipingBillNo());
                 System.out.println("billOfEntryNo:"+billingTO1.getBillOfEntryNo());
                 System.out.println("billingState:"+billingTO1.getBillingState());
                 System.out.println("organizationId:"+billingTO1.getOrganizationId());
                 System.out.println("GSTNo:"+billingTO1.getGstNo());
            }
            request.setAttribute("tripsToBeBilledDetails", ordersToBeBillingDetails);
            request.setAttribute("tripsOtherExpenseDetails", tripsOtherExpenseDetails);
            request.setAttribute("reeferRequired", reeferRequired);
            request.setAttribute("consignmentOrderId", consignmentOrderId);
            if(consignmentOrderId != null){
            if(consignmentOrderId.contains(",")){
            request.setAttribute("consignmentOrderCount", consignmentOrderId.split(",").length);
            }else{
            request.setAttribute("consignmentOrderCount", 1);
            }
            }
           String revenueApprovalStatus = tripBP.getRevenueApprovalStatus(customerId, "1");//1 indicates gps non usage approval request
           System.out.println("revenueApprovalStatus:"+revenueApprovalStatus);
           request.setAttribute("revenueApprovalStatus", revenueApprovalStatus);
            path = "content/billing/viewOrderRepoBillDetails.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
     
     public ModelAndView updateTollAndDetaintionForBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        System.out.println("updateTollAndDetaintionForBillingNEW");

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        //tripCommand = command;
        billingCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

//            String consignmentOrderIds[]=request.getParameterValues("consignmentOrderIds");
            String billingParty=request.getParameter("billingParty");
            String billingPartyOld=request.getParameter("billingPartyOld");
            System.out.println("billingPartyIdOld="+billingPartyOld);
            String billingPartyId=request.getParameter("billingPartyId");
            System.out.println("billingPartyId="+billingPartyId);
            String billList=request.getParameter("billList");
            System.out.println("billList="+billList);
            request.setAttribute("billList", billList);
            String movementType=request.getParameter("movementType");
            String grNo[]=request.getParameterValues("grNo");
            String consignmentOrderIds[]=request.getParameterValues("consignmentOrderIds");
            String tripId[] = request.getParameterValues("tripId");
            String tripIds = request.getParameter("tripIds");
            String tollOld[] = request.getParameterValues("tollChargeOld");
            String toll[] = request.getParameterValues("tollCharge");
            String detaintionOld[] = request.getParameterValues("detaintionChargeOld");
            String detaintion[] = request.getParameterValues("detaintionCharge");
            String vehicleId[] = request.getParameterValues("vehicleId");
            String driverId[] = request.getParameterValues("driverId");
            String containerNoOld[] = request.getParameterValues("containerNoOld");
            String containerNo[] = request.getParameterValues("containerNo");
            String tripContainerId[] = request.getParameterValues("tripContainerId");
            String consignmentConatinerId[] = request.getParameterValues("consignmentConatinerId");
            String greenTaxOld[] = request.getParameterValues("greenTaxOld");
            String greenTax[] = request.getParameterValues("greenTax");
            String otherExpense[] = request.getParameterValues("otherExpense");
            String otherExpenseOld[] = request.getParameterValues("otherExpenseOld");
            String weightment[] = request.getParameterValues("weightmentCharge");
            String weightmentOld[] = request.getParameterValues("weightmentChargeOld");
            String expenseType[] = request.getParameterValues("expenseType"); //expense type for weightment
            String shippingBillNoOld[] = request.getParameterValues("shippingBillNoOld");
            String shippingBillNo[] = request.getParameterValues("shippingBillNo");
            String billOfEntry[]=request.getParameterValues("billOfEntry");
            String billOfEntryOld[]=request.getParameterValues("billOfEntryOld");


            insertStatus = tripBP.updateTollAndDetaintionForBilling(grNo,tripId,toll,tollOld,detaintion,detaintionOld, vehicleId, driverId,containerNo,containerNoOld,tripContainerId,
                    consignmentConatinerId,greenTax,greenTaxOld,shippingBillNo,shippingBillNoOld,billOfEntry,billOfEntryOld,billList,movementType,billingPartyOld,billingParty,otherExpense,otherExpenseOld,weightment,weightmentOld,expenseType,userId);
            //insertStatus = tripBP.updateBillingPartyForBilling(tripId,consignmentOrderIds,billOfEntry,billingParty,billingPartyId,movementType,userId);

            System.out.println("insertStatus"+insertStatus);
            request.setAttribute("sessionPageParam",session.getAttribute("sessionPageParam"));
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip  Details Updated Successfully....");
            mv = gererateOrderBill(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
       // return new ModelAndView(path);
        return mv;
    }

     public ModelAndView updateTollAndDetaintionForRepoBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        System.out.println("updateTollAndDetaintionForBillingNEW");

        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        int userId = (Integer) session.getAttribute("userId");
        String path = "";
        //tripCommand = command;
        billingCommand = command;
        String menuPath = "Operation  >>  View start Trip Sheet ";
        int insertStatus = 0;
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            TripTO tripTO = new TripTO();

            String billingPartyId=request.getParameter("billingPartyId");
            String billingParty=request.getParameter("billingParty");
            String billingPartyOld=request.getParameter("billingPartyOld");
            String consignmentOrderIds[]=request.getParameterValues("consignmentOrderIds");

            String billList=request.getParameter("billList");
            System.out.println("billList1===="+billList);
            request.setAttribute("billList", billList);
            String movementType=request.getParameter("movementType");
            String grNo[]=request.getParameterValues("grNo");
            String tripId[] = request.getParameterValues("tripId");
            String tripIds = request.getParameter("tripIds");
            System.out.println("tripIdsREPO"+tripIds);
            String tollOld[] = request.getParameterValues("tollChargeOld");
            String toll[] = request.getParameterValues("tollCharge");
            String detaintionOld[] = request.getParameterValues("detaintionChargeOld");
            String detaintion[] = request.getParameterValues("detaintionCharge");
            String vehicleId[] = request.getParameterValues("vehicleId");
            String driverId[] = request.getParameterValues("driverId");
            String containerNoOld[] = request.getParameterValues("containerNoOld");
            String containerNo[] = request.getParameterValues("containerNo");
            String tripContainerId[] = request.getParameterValues("tripContainerId");
            String consignmentConatinerId[] = request.getParameterValues("consignmentConatinerId");
            String greenTaxOld[] = request.getParameterValues("greenTaxOld");
            String greenTax[] = request.getParameterValues("greenTax");
            String otherExpense[] = request.getParameterValues("otherExpense");
            String otherExpenseOld[] = request.getParameterValues("otherExpenseOld");
            String weightment[] = request.getParameterValues("weightmentCharge");
            String weightmentOld[] = request.getParameterValues("weightmentChargeOld");
            String expenseType[] = request.getParameterValues("expenseType"); //expense type for weightment


            insertStatus = tripBP.updateTollAndDetaintionForRepoBilling(grNo,tripId,toll,tollOld,detaintion,detaintionOld, vehicleId, driverId, containerNo,containerNoOld,tripContainerId,
                    consignmentConatinerId,greenTax,greenTaxOld,billList,movementType,billingPartyOld,billingParty,otherExpense,otherExpenseOld,weightment,weightmentOld,expenseType,userId);
            insertStatus = tripBP.updateBillingPartyForRepoBilling(tripId,consignmentOrderIds,billingParty,billingPartyId,movementType,userId);


//            System.out.println("Raj  Here For Path Test");
//            path = "gererateRepoOrderBill.do?tripIds="+tripIds;
//            mv = new ModelAndView(path);
            
            System.out.println("insertStatus"+insertStatus);
            request.setAttribute("sessionPageParam",session.getAttribute("sessionPageParam"));
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Trip  Details Updated Successfully....");
            mv = gererateRepoOrderBill(request, response, command);
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Designation data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
       // return new ModelAndView(path);
        return mv;
    }


    public ModelAndView saveOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            BillingTO billingTO = new BillingTO();
            
            String companyId = (String) session.getAttribute("companyId");
            billingTO.setCompanyId(companyId);
            System.out.println("companyId--"+companyId);

            String consignmentOrderId = request.getParameter("consignmentOrderId");
            String tripSheetId = request.getParameter("tripSheetId");
            billingTO.setConsignmentOrderId(consignmentOrderId);
            billingTO.setTripSheetId(tripSheetId);
            String invoiceNo = request.getParameter("invoiceNo");
            String customerId = request.getParameter("customerId");
            String customerAddress = request.getParameter("billingPartyAddress");
            String totalRevenue = request.getParameter("totalRevenue");
          //  String estimatedRevenue = request.getParameter("estimatedRevenue");
            String totalExpToBeBilled = request.getParameter("totalExpToBeBilled");
            //String grandTotal = request.getParameter("grandTotal");
            String noOfTrips = request.getParameter("noOfTrips");
            String noOfOrders = request.getParameter("noOfOrders");
            String billingPartyId = request.getParameter("billingPartyId");
            String billingParty = request.getParameter("billingParty");
            String remarks = request.getParameter("detaintionRemarks");
            String grandTotal = request.getParameter("netAmount");
            String repoOrder = request.getParameter("repoOrder");
            String totalTax = request.getParameter("totalTax");
            String cgstAmount = request.getParameter("cgstAmount");
            String sgstAmount = request.getParameter("sgstAmount");
            String igstAmount = request.getParameter("igstAmount");
            String organizationId = request.getParameter("organizationId");
            String billingState = request.getParameter("billingState");
            String GSTNo = request.getParameter("GSTNo");
            String igstPercentage = request.getParameter("IGST");
            String cgstPercentage = request.getParameter("CGST");
            String sgstPercentage = request.getParameter("SGST");
            String totalProfitMarginPercent = request.getParameter("totalProfitMarginPercent");
            String profitMarginAmount = request.getParameter("profitMarginAmount");
            String hireChrg = request.getParameter("hireChrg");
            String haltingChrg = request.getParameter("haltingChrg");
            String loadUnloadChrg = request.getParameter("loadUnloadChrg");
            String addChrg = request.getParameter("addChrg");

            String otherExpense = request.getParameter("totOtherExpense");
            billingTO.setIgstPercentage(igstPercentage);
            billingTO.setCgstPercentage(cgstPercentage);
            billingTO.setSgstPercentage(sgstPercentage);
            billingTO.setIgstAmount(igstAmount);
            billingTO.setCgstAmount(cgstAmount);
            billingTO.setSgstAmount(sgstAmount);
            billingTO.setTotalTax(totalTax);
            billingTO.setOrganizationId(organizationId);
            billingTO.setBillingState(billingState);
            billingTO.setGstNo(GSTNo);
            billingTO.setOrderCount(noOfOrders);
            billingTO.setNoOfTrips(noOfTrips);
            billingTO.setUserId(userId+"");
            billingTO.setCustomerId(customerId);
            billingTO.setTotalRevenue(totalRevenue);
            billingTO.setTotalExpToBeBilled(totalExpToBeBilled);
            billingTO.setCustomerAddress(customerAddress);
            billingTO.setBillingPartyId(billingPartyId);
            billingTO.setBillingParty(billingParty);
            billingTO.setRemarks(remarks);
            billingTO.setGrandTotal(grandTotal);
            billingTO.setTotalProfitMarginPercent(totalProfitMarginPercent);
            billingTO.setProfitMarginAmount(profitMarginAmount);
            
            billingTO.setHireChrg(hireChrg);
            billingTO.setHaltingChrg(haltingChrg);
            billingTO.setLoadUnloadChrg(loadUnloadChrg);
            billingTO.setAddChrg(addChrg);

            billingTO.setOtherExpense(otherExpense);
            //generate tripcode
            String[] tripIds = request.getParameterValues("tripId");
            String[] tripIdIGSTAmount = request.getParameterValues("tripIGSTAmount");
            String[] tripIdCGSTAmount = request.getParameterValues("tripCGSTAmount");
            String[] tripIdSGSTAmount = request.getParameterValues("tripSGSTAmount");
            
            int existStatus = tripBP.getTripExistInBilling(tripIds);
            int insertStatus = 0;
            if (existStatus == 0) {
            insertStatus = billingBP.saveOrderBill(billingTO,userId);
                       
          int tripGst= billingBP.updateTripGst(String.valueOf(userId),tripIds,
                  tripIdIGSTAmount,tripIdCGSTAmount,tripIdSGSTAmount);
            request.setAttribute("consignmentOrderId", consignmentOrderId);
            request.setAttribute("invoiceNo", invoiceNo);
            request.setAttribute("invoiceId", insertStatus);

//            path = "content/trip/responsePage.jsp";
//            path = "viewOrderBillDetailsForSubmit.do?invoiceId="+invoiceId+"&submitStatus="+0+"&cancelStatus="+1;
            System.out.println("request sessionPageParam:"+request.getParameter("sessionPageParam"));
            System.out.println("session sessionPageParam:"+session.getAttribute("sessionPageParam"));
            request.setAttribute("sessionPageParam",session.getAttribute("sessionPageParam"));
            
            path = "viewOrderBillDetailsForSubmit.do?invoiceId="+insertStatus+"&repoOrder="+repoOrder+"&submitStatus="+0+"&cancelStatus=1&sessionPageParam="+session.getAttribute("sessionPageParam");
//            mv = viewOrderBillDetailsForSubmit(request,response,command);
            System.out.println("path:"+path);
            }else{
             path = "gererateOrderBill.do?sessionPageParam="+session.getAttribute("sessionPageParam");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

  public ModelAndView viewOrderBillForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = null;
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String fromDate = "";
        String toDate = "";
        String customerId = "";
        String tripType = "";
        String submitStatus = "";
        String param = "";
        String billNo = "";
        String grNo = "";
        try {
            session = request.getSession();
            userId = (Integer) session.getAttribute("userId");
            param = request.getParameter("param");
            customerId = request.getParameter("customerId");
            fromDate = request.getParameter("fromDate");
            toDate = request.getParameter("toDate");
            submitStatus = request.getParameter("submitStatus");
            tripType = request.getParameter("tripType");
            grNo = request.getParameter("grNo");
            billNo = request.getParameter("billNo");

            request.setAttribute("customerId", customerId);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("tripType", tripType);
            request.setAttribute("submitStatus", submitStatus);
            request.setAttribute("billNo", billNo);
            request.setAttribute("grNo", grNo);

            int status=0;
            String cancelStatus = request.getParameter("cancelStatus");
            String billingNo = request.getParameter("billingNo");

             if(cancelStatus !=null){
               status= billingBP.updateCancel(billingNo,userId);
               System.out.println("status"+status);
               request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, billingNo +" billingNo  cancelled Successfully....");
             }

            ArrayList customerList = new ArrayList();
            customerList = operationBP.getTripCustomerList();
            request.setAttribute("customerList", customerList);
            System.out.println("customerList Size Is :::" + customerList.size());

            ArrayList closedBillList = new ArrayList();
            //closedBillList = operationBP.getClosedBilledList(customerId, fromDate, toDate);
            closedBillList = billingBP.viewBillForSubmission(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo);
            request.setAttribute("closedBillList", closedBillList);
            System.out.println("closedBillList Size Is :::" + closedBillList.size());



        if(!"ExcelExport".equals(param)){
        path = "content/billing/viewOrderBillForSubmission.jsp";
        }
        else{
        path = "content/billing/viewOrderBillForSubmissionExport.jsp";
        }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    
   public ModelAndView viewOrderBillDetailsForSubmit(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        int userId = 0;
        HttpSession session = null;
        billingCommand = command;
        String menuPath = "";
        BillingTO billingTO = new BillingTO();
        TripTO tripTO = new TripTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  View Bill For Submission";
        request.setAttribute("pageTitle", pageTitle);
        String status = "1";
        request.setAttribute("status", status);

        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            session = request.getSession();
            int roleId = (Integer) session.getAttribute("RoleId");
            System.out.println("roleId=@@@@......"+roleId);
            request.setAttribute("roleId", roleId);

            //for cancell the invoice after generate the bill
            String cancel = "0";
            request.setAttribute("cancelStatus", cancel);
            String cancelStatus= request.getParameter("cancelStatus");
            String repoOrder = request.getParameter("repoOrder");
            request.setAttribute("repoOrder", repoOrder);
             //for cancell the invoice after generate the bill
            System.out.println("cancelStatus===="+cancelStatus);
            if("1".equals(cancelStatus)){
            path = "content/billing/viewOrderBillDetailsForSubmissionPrintPreview.jsp";
            }else{
            path = "content/billing/viewOrderBillDetailsForSubmission.jsp";
            }

            String invoiceId = request.getParameter("invoiceId");
            billingTO.setInvoiceId(invoiceId);
            tripTO.setInvoiceId(invoiceId);
            request.setAttribute("invoiceId", invoiceId);
            String companyId = (String) session.getAttribute("companyId");
            System.out.println("companyId=="+companyId); 
            billingTO.setCompanyId(companyId);
             
            ArrayList invoiceHeader = tripBP.getOrderInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);
            ArrayList invoiceTaxDetails = billingBP.getInvoiceGSTTaxDetails(billingTO);
            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceTaxDetails", invoiceTaxDetails);
            request.setAttribute("invoiceTaxCheck", "1");
            System.out.println("invoiceTaxDetailsSize:"+invoiceTaxDetails.size());
            request.setAttribute("invoiceTaxDetailsSize", invoiceTaxDetails.size());
             String[] tempRemarks=null;
            Iterator itr = invoiceHeader.iterator();
            TripTO tpTO = new TripTO();
            while(itr.hasNext()){
                tpTO = (TripTO) itr.next();
                request.setAttribute("billingPartyId", tpTO.getBillingPartyId());
                request.setAttribute("billingParty", tpTO.getBillingParty());
                System.out.println("billingParty:"+tpTO.getBillingParty());
                request.setAttribute("customerName", tpTO.getCustomerName());
                request.setAttribute("taxValue", tpTO.getTaxValue());
                request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                request.setAttribute("panNo", tpTO.getPanNo());
                request.setAttribute("gstNo", tpTO.getGstNo());
                request.setAttribute("cityName", tpTO.getCityName());
                request.setAttribute("state", tpTO.getState());
                request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                request.setAttribute("billDate", tpTO.getBillDate());
                request.setAttribute("movementType", tpTO.getMovementType());
                request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                request.setAttribute("billingType", tpTO.getBillingType());
                request.setAttribute("userName", tpTO.getUserName());
                System.out.println("remaarks:"+tpTO.getRemarks());
                if(!"".equals(tpTO.getRemarks()) ){
                 tempRemarks=tpTO.getRemarks().split(",");
                 if(tempRemarks.length>=1){
                 request.setAttribute("firstRemarks",tempRemarks[0]);
                     System.out.println("firstRemarks:"+tempRemarks[0]);
                 }
                 if(tempRemarks.length>=2){
                 request.setAttribute("secondRemarks",tempRemarks[1]);
                    }
                 if(tempRemarks.length>=3){
                 request.setAttribute("thirdRemarks",tempRemarks[2]);
                    }
                }
                String invoiceCode=tpTO.getInvoiceCode();
//                String[] tempId = invoiceCode.split("-");
//                String temp=tempId[0];
//                String temp1=tempId[1];
//                String[] tmpId = temp1.split("/");
//                String tmp=tmpId[0];
//                String tmp1=tmpId[1];
//                String iCode=tmpId[2];
                String iCode=invoiceCode;
                System.out.println("tCode"+iCode);
                request.setAttribute("invoiceCode", iCode);

            }
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
            request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }
            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = tripBP.getOrderInvoiceDetailsList(tripTO);
            request.setAttribute("invoiceDetailsList", invoiceDetailsList);
            ArrayList tripDetails = new ArrayList();
            tripDetails = billingBP.getTripDetails(billingTO);
            request.setAttribute("tripDetails", tripDetails);
            request.setAttribute("invoiceId", invoiceId);
            request.setAttribute("submitStatus", request.getParameter("submitStatus"));
            System.out.println("invoiceDetailsList.size():"+invoiceDetailsList.size());
            request.setAttribute("invoiceDetailsListSize", invoiceDetailsList.size());
            
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    
    public ModelAndView submitOrderBill(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";

        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            BillingTO billingTO = new BillingTO();
            TripTO tripTO = new TripTO();

            String invoiceId = request.getParameter("invoiceId");
            billingTO.setInvoiceId(invoiceId);
            tripTO.setInvoiceId(invoiceId);
            request.setAttribute("invoiceId", invoiceId);
            int userId = (Integer) session.getAttribute("userId");
            int status = billingBP.submitBill(invoiceId, userId);

            ArrayList invoiceHeader = tripBP.getInvoiceHeader(tripTO);
            ArrayList invoiceDetails = tripBP.getInvoiceDetails(tripTO);
            ArrayList invoiceDetailExpenses = tripBP.getInvoiceDetailExpenses(tripTO);

            request.setAttribute("invoiceHeader", invoiceHeader);
            request.setAttribute("invoiceDetails", invoiceDetails);
            if (invoiceDetailExpenses.size() > 0) {
                request.setAttribute("invoiceDetailExpenses", invoiceDetailExpenses);
            }
            ArrayList tripDetails = new ArrayList();
            tripDetails = tripBP.getTripDetails(tripTO);
            request.setAttribute("tripDetails", tripDetails);
//            path = "content/billing/orderBillingInvoice.jsp";
//session.getAttribute("sessionPageParam")
            request.setAttribute("sessionPageParam",session.getAttribute("sessionPageParam"));
            path = "viewOrderBillDetailsForSubmit.do?invoiceId="+invoiceId+"&submitStatus=1";
            
           

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    
    public ModelAndView printPreviewForBilling(HttpServletRequest request, HttpServletResponse response, BillingCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Finance  >>  Generate Bill ";
        ModelAndView mv = null;
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            int userId = (Integer) session.getAttribute("userId");
//            if (!loginBP.checkAuthorisation(userFunctions, "Designation-List")) {
//                path = "content/common/NotAuthorized.jsp";
//            } else {
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            //ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Trip Sheet";
            request.setAttribute("pageTitle", pageTitle);

            String status = "0";
            request.setAttribute("status", status);

            BillingTO billingTO = new BillingTO();

            String tripSheetIds = request.getParameter("tripSheetIds");
            String taxCheck = request.getParameter("taxCheck");
            String igstAmount = request.getParameter("igstAmount");
            String sgstAmount = request.getParameter("sgstAmount");
            String cgstAmount = request.getParameter("cgstAmount");
            String netAmount = request.getParameter("netAmount");
            String IGST = request.getParameter("IGST");
            String CGST = request.getParameter("CGST");
            String SGST = request.getParameter("SGST");
            String GSTNo = request.getParameter("GSTNo");
            String panNo = request.getParameter("panNo");
            
            request.setAttribute("panNo", panNo);
            request.setAttribute("GSTNo", GSTNo);
            request.setAttribute("taxCheck", taxCheck);
            request.setAttribute("igstAmount", igstAmount);
            request.setAttribute("sgstAmount", sgstAmount);
            request.setAttribute("cgstAmount", cgstAmount);
            request.setAttribute("netAmount", netAmount);
            request.setAttribute("IGST", IGST);
            request.setAttribute("CGST", CGST);
            request.setAttribute("SGST", SGST);
            billingTO.setTripSheetIds(tripSheetIds);

            ArrayList invoiceHeader = new ArrayList();
            invoiceHeader = billingBP.getPreviewHeader(billingTO);
            request.setAttribute("invoiceHeader", invoiceHeader);
            System.out.println("getPreviewHeader.size():"+invoiceHeader.size());
             String[] tempRemarks=null;
            Iterator itr = invoiceHeader.iterator();
            TripTO tpTO = new TripTO();
            while(itr.hasNext()){
                tpTO = (TripTO) itr.next();
                request.setAttribute("billingParty", tpTO.getBillingParty());
                request.setAttribute("billingPartyId", tpTO.getBillingPartyId());
                System.out.println("billingParty:"+tpTO.getBillingParty());
                request.setAttribute("customerName", tpTO.getCustomerName());
                request.setAttribute("customerAddress", tpTO.getCustomerAddress());
                request.setAttribute("cityName", tpTO.getCityName());
                request.setAttribute("state", tpTO.getState());
                request.setAttribute("invoicecode", tpTO.getInvoiceCode());
                request.setAttribute("consignmentOrderNos", tpTO.getcNotes());
                request.setAttribute("consignmentOrderDate", tpTO.getConsignmentOrderDate());
                request.setAttribute("billDate", tpTO.getBillDate());
                request.setAttribute("movementType", tpTO.getMovementType());
                request.setAttribute("containerTypeName", tpTO.getContainerTypeName());
                request.setAttribute("billingType", tpTO.getBillingType());
                request.setAttribute("userName", tpTO.getUserName());
                System.out.println("remaarks:"+tpTO.getRemarks());
                if(!"".equals(tpTO.getRemarks()) ){
                 tempRemarks=tpTO.getRemarks().split(",");
                 if(tempRemarks.length>=1){
                 request.setAttribute("firstRemarks",tempRemarks[0]);
                     System.out.println("firstRemarks:"+tempRemarks[0]);
                 }
                 if(tempRemarks.length>=2){
                 request.setAttribute("secondRemarks",tempRemarks[1]);
                    }
                 if(tempRemarks.length>=3){
                 request.setAttribute("thirdRemarks",tempRemarks[2]);
                    }
                }
                System.out.println("tpTO.getInvoiceCode()"+tpTO.getInvoiceCode());
                String invoiceCode=tpTO.getInvoiceCode();
                 if("NA".equals(tpTO.getInvoiceCode())){
                     invoiceCode= "ICTR/18-19/TPT/0000000";
                }
                String[] tempId = invoiceCode.split("-");
                String temp=tempId[0];
                String temp1=tempId[1];
                String[] tmpId = temp1.split("/");
                String tmp=tmpId[0];
                String tmp1=tmpId[1];
                String iCode=tmpId[2];
                System.out.println("tCode"+iCode);
                request.setAttribute("invoiceCode", iCode);

            }

            ArrayList invoiceDetailsList = new ArrayList();
            invoiceDetailsList = billingBP.getBillingDetailsForPreview(billingTO);
            request.setAttribute("invoiceDetailsList", invoiceDetailsList);
            request.setAttribute("invoiceTaxCheck", "2");
            request.setAttribute("invoiceDetailsListSize",invoiceDetailsList.size());
            System.out.println("getBillingDetailsForPreview.size():"+invoiceDetailsList.size());

//          path = "content/trip/responsePage.jsp";
            path = "content/billing/viewOrderBillDetailsForSubmissionPreview.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            return new ModelAndView("content/common/error.jsp");
//
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Consignment data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

}
