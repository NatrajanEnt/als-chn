
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
    function checksubmit()
    {
        var errStr = "";


        var nameCheckStatus = $("#checkListNameStatus").text();
//        if(textValidation(document.checkList.checkListName,'checkListName')){
//            return;
//        }
//        if(document.checkList.checkListStage.value==0){
//            alert("checkList Type should not be Empty");
//             document.checkList.checkListStage.focus();
//            return;
//            }
//        if(textValidation(document.checkList.checkListDate,'checkListDate')){
//            return;
//        }
        if (document.getElementById("checkListName").value == "") {
            errStr = "Please enter Check List Name.\n";
            alert(errStr);
            document.getElementById("checkListName").focus();
        } else if (nameCheckStatus != "") {
            errStr = "CheckList Name Already Exists.\n";
            alert(errStr);
            document.getElementById("checkListName").focus();
        }
        else if (document.checkList.checkListStage.value == 0) {
            alert("checkList Type should not be Empty");
            document.checkList.checkListStage.focus();
            return;
        }
        else if (document.getElementById("checkListStage").value == "") {
            errStr = "Please enter check List Stage.\n";
            alert(errStr);
            document.getElementById("checkListStage").focus();
        }
        else if (document.getElementById("checkListDate").value == "") {
            errStr = "Please enter Check List Date.\n";
            alert(errStr);
            document.getElementById("checkListDate").focus();
        }

        if (errStr == "") {
            document.checkList.action = " /throttle/saveCheckList.do";
            document.checkList.method = "post";
            document.checkList.submit();
        }
    }



    function setValues(sno, checkListName, checkListStage, checkListDate, checkListStatus, checkListId) {
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("checkListId").value = checkListId;
        document.getElementById("checkListName").value = checkListName;
        document.getElementById("checkListStage").value = checkListStage;
        document.getElementById("checkListDate").value = checkListDate;
        document.getElementById("checkListStatus").value = checkListStatus;
    }






    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }

    var httpRequest;
    function checkCheckListName() {

        var checkListName = document.getElementById('checkListName').value;

        var url = '/throttle/checkCheckList.do?checkListName=' + checkListName;
        if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function() {
            processRequest();
        };
        httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#checkListNameStatus").text('Please Check checkList Name ' + val + ' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#checkListNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">Check List Master</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="document.standardCharges.stChargeName.focus();">


                <form name="checkList"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">CheckList Details</td></tr>
                        <input type="hidden" name="checkListId" id="checkListId" value=""  />

                        <td  colspan="4" align="center" style="display: none" id="nameStatus"><label id="checkListNameStatus" style="color: red"></label></td>
                        </tr>
                        <tr>
                            <td >&nbsp;&nbsp;<font color="red">*</font>CheckList Name</td>
                            <td ><input type="text" name="checkListName" id="checkListName" class="form-control" style="width:240px;height:40px;" maxlength="50" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCheckListName();" autocomplete="off"></td>

                            <td >&nbsp;&nbsp;<font color="red">*</font>Stage</td>
                            <td >
                                <select name="checkListStage" id="checkListStage" class="form-control" style="width:240px;height:40px;">
                                    <option value="0">--Select--</option>
                                    <c:if test="${stageList != null}">
                                        <c:forEach items="${stageList}" var="stage">
                                            <option value='<c:out value="${stage.stageId}"/>'><c:out value="${stage.stageName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td >&nbsp;&nbsp;<font color="red">*</font>Effective Date</td>
                            <td ><input type="textbox" class="datepicker , form-control" style="width:240px;height:40px;" name="checkListDate" id="checkListDate" value="" /></td>
                            <td >&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                            <td >
                                <select  align="center" class="form-control" style="width:240px;height:40px;" name="checkListStatus" id="checkListStatus" >
                                    <option value='Y'>Active</option>
                                    <option value='N' id="inActive" style="display: none">In-Active</option>
                                </select>
                            </td>
                        </tr>
                    </table>
                    <tr>
                        <td>
                    <center>
                        <input type="button" class="btn btn-info" value="Save" name="Submit" onClick="checksubmit()">
                    </center>
                    </td>
                    </tr>


                    <h2 align="center">CheckList View</h2>
                   <table class="table table-info mb30 table-hover" id="table" style="width:100%">
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                                <th> S.No </th>
                        <th> CheckList Name </th>
                        <th> CheckList Type </th>
                        <th> Effective Date </th>
                        <th> Select </th>
                        </tr>
                        </thead>
                        <tbody>
                            <% int sno = 0;%>
                            <c:if test = "${checkList != null}">
                                <c:forEach items="${checkList}" var="checkList">
                                    <%
                                                sno++;
                                                String className = "text1";
                                                if ((sno % 1) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>

                                    <tr>
                                        <td  align="left"> <%= sno + 1%> </td>
                                        <td  align="left"> <c:out value="${checkList.checkListName}" /></td>
                                        <td  align="left"> <c:out value="${checkList.checkListStage}" /></td>
                                        <td  align="left"> <c:out value="${checkList.checkListDate}"/></td>
                                        <td> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${checkList.checkListName}" />', '<c:out value="${checkList.checkListStage}" />', '<c:out value="${checkList.checkListDate}" />', '<c:out value="${checkList.checkListStatus}" />', '<c:out value="${checkList.checkListId}" />');" /></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </c:if>
                    </table>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>