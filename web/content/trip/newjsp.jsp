<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">

//     function saveHireVehicle() {
//
//                var vehicleNo = document.getElementById("hireVehicleNo").value;
//                var driverName = document.getElementById("hireDriverName").value;
//                var consignmentOrderId = document.getElementById("consignmentOrderId").value;
//                document.tripExpense.action = "/throttle/saveHireVehicle.do?vehicleNo=" + vehicleNo + "&driverName=" + driverName;
//
//                document.tripExpense.submit();
//            }

$(document).ready(function() {
    $("#haltStartDate").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
//        maxDate: '0',
        onClose: function( selectedDate ) {
        $("#haltEndDate" ).datepicker( "option", "minDate", selectedDate );
        }
    });
    $("#haltEndDate").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
//        maxDate: '0',
        onClose: function( selectedDate ) {
        $("#haltStartDate" ).datepicker( "option", "maxDate", selectedDate );
        }
    });
});

    function fuelPrice1(rowVal) {
        
        var fuelMode = document.getElementById('fuelMode' + rowVal).value;
        var fuelPrice ="";
        var fuelLters ="";
        var fuelAmount ="";
        if(fuelMode == "2"){
            fuelPrice = document.getElementById('fuelPriceRemarks' + rowVal).value;
            fuelLters = document.getElementById("fuelLtrs" + rowVal).value;
            fuelAmount = (fuelPrice * fuelLters).toFixed(2);
            document.getElementById("fuelAmount" + rowVal).value = fuelAmount;
            $('#fuelAmountE' + rowVal).prop('readonly', true);
        }else{
            fuelPrice = document.getElementById('fuelPriceRemarks' + rowVal).value;
            var temp1 = fuelPrice.split("~");
    //        document.getElementById('rowFuelPrice' + rowVal).value = temp1[1];
            document.getElementById('rowFuelPrice' + rowVal).value = fuelPrice;
            fuelPrice = document.getElementById('rowFuelPrice' + rowVal).value;
            var fuelRate = document.getElementById('bunkName' + rowVal).value;
            var temp = fuelRate.split('~');
             fuelLters = document.getElementById("fuelLtrs" + rowVal).value;
             fuelAmount = (fuelPrice * fuelLters).toFixed(2);
            document.getElementById("fuelAmount" + rowVal).value = fuelAmount;
            $('#fuelAmount' + rowVal).prop('readonly', true);
        }
        
        
    }
    function fuelPrice2(rowVal) {
        var fuelPrice = document.getElementById('fuelPriceRemarksE' + rowVal).value;
        var fuelMode = document.getElementById('fuelModeE' + rowVal).value;
        var fuelLters ="";
        var fuelAmount ="";
        if(fuelMode == "2"){
            fuelLters = document.getElementById("fuelLtrsE" + rowVal).value;
            fuelAmount = (fuelPrice * fuelLters).toFixed(2);
            document.getElementById("fuelAmountE" + rowVal).value = fuelAmount;
            $('#fuelAmountE' + rowVal).prop('readonly', true);
        }else{
        var temp1 = fuelPrice.split("~");
        document.getElementById('fuelPriceE' + rowVal).value = fuelPrice;
//        document.getElementById('fuelPriceE' + rowVal).value = temp1[1];
        fuelPrice = document.getElementById('fuelPriceE' + rowVal).value;
        var fuelRate = document.getElementById('bunkIdE' + rowVal).value;
        var temp = fuelRate.split('~');
         fuelLters = document.getElementById("fuelLtrsE" + rowVal).value;
         fuelAmount = (fuelPrice * fuelLters).toFixed(2);
        document.getElementById("fuelAmountE" + rowVal).value = fuelAmount;
        $('#fuelAmountE' + rowVal).prop('readonly', true);
        }
    }
    
    function setFuelAmount(rowVal) {
        var fuelPrice = document.getElementById('fuelPriceRemarksE' + rowVal).value;
        var temp1 = fuelPrice.split("~");
        document.getElementById('fuelPriceE' + rowVal).value = temp1[1];
        fuelPrice = document.getElementById('fuelPriceE' + rowVal).value;
        var fuelLters = document.getElementById("fuelLtrsE" + rowVal).value;
        var fuelAmount = (fuelPrice * fuelLters).toFixed(2);
        document.getElementById("fuelAmountE" + rowVal).value = fuelAmount;
        $('#fuelAmountE' + rowVal).prop('readonly', true);
    }
    
    

    function setFuelPrice(rowVal, val) {
        var temp = val.split('~');
        document.getElementById('fuelPriceE' + rowVal).value = temp[1];
        fuelPrice2(rowVal);
    }


    function submitExtraFuelDetails() {

        var fuelMode = document.getElementsByName("fuelMode");
        var bunkName = document.getElementsByName("bunkName");
        var bunkId = document.getElementsByName("bunkId");
        var slipNo = document.getElementsByName("slipNo");
        var fuelDate = document.getElementsByName("fuelDate");
        var fuelLtrs = document.getElementsByName("fuelLtrs");
        var fuelAmount = document.getElementsByName("fuelAmount");
        var fuelFilledName = document.getElementsByName("fuelFilledName");
        var fuelRemarks = document.getElementsByName("fuelRemarks");
        for (var i = 0; i < fuelMode.length; i++) {
//        alert("bunkIdbunkId--"+bunkId[i].value);
            if (fuelMode[i].value == '' || fuelMode[i].value == 0) {
                alert("please select fuel mode");
                return false;
                fuelMode[i].focus();
            } else if ((fuelMode[i].value == 1 && bunkName[i].value == '') || (fuelMode[i].value == 1 && bunkName[i].value == 0)) {
                alert("please select bunk name");
                return false;
                bunkName[i].focus();
//            } else if ((fuelMode[i].value == 2 && bunkName[i].value == '') || (fuelMode[i].value == 2 && bunkName[i].value == 0)) {
//                $("#bunkName"+i).val("20~0.00");
//                alert($("#bunkName"+i).val())
            } else if (fuelDate[i].value == '') {
                alert("please enter date of fueling");
                return false;
                fuelDate[i].focus();
            } else if (fuelLtrs[i].value == '' || fuelLtrs[i].value == 0) {
                alert("please enter fuel litters");
                return false;
                fuelLtrs[i].focus();
            } else if (fuelAmount[i].value == '' || fuelAmount[i].value == 0) {
                alert("please enter fuel amount");
                return false;
                fuelAmount[i].focus();
            }
        }
//        alert("my welcome alert="+fuelMode.length);
//        if (fuelMode.length >= 0) {
        document.tripExpense.action = "/throttle/saveTripExtraFuelDetailes.do";
        document.tripExpense.submit();
//        } else {
//            alert("Please add Fuel details and then proceed")
//        }

    }


    function submitTripFuelDetails() {
        //alert("compensationFuel");
        var allocatedFuel = document.getElementById("allocatedFuel").value;
        var consumedFuel = document.getElementById("consumedFuel").value;
        var compensationFuel = document.getElementById("compensationFuel").value;
        var recoveryFuel = document.getElementById("recoveryFuel").value;
        var recoveryAmount = document.getElementById("recoveryAmount").value;
        var fuelSlipNo = document.getElementById("fuelSlipNo").value;
        if (compensationFuel == '') {
            document.getElementById("compensationFuel").value = 0;
            compensationFuel = document.getElementById("compensationFuel").value;
        }
        if (allocatedFuel == '') {
            alert("please enter trip allocated fuel");
            return false;
            allocatedFuel.focus();
        } else if (consumedFuel == '') {
            alert("please enter trip consumed fuel");
            return false;
            consumedFuel.focus();
        } else if (compensationFuel == '') {
            //document.getElementById("compensationFuel").value = 0;
            alert("please enter trip compensation fuel");
            return false;
            compensationFuel.focus();
        } else if (recoveryFuel == '') {
            alert("please enter trip recovery fuel");
            return false;
            recoveryFuel.focus();
        } else if (recoveryAmount == '') {
            alert("please enter trip recovery fuel amount");
            return false;
            recoveryAmount.focus();
        } else {
//         alert("fdsfdsf");
            document.tripExpense.action = "/throttle/saveTripFuelRecovery.do";
            document.tripExpense.submit();
        }

    }

    function calculateFuelRecovery() {
        var allocatedFuel = document.getElementById("allocatedFuel").value;
        var filledFuel = document.getElementById("filledFuel").value;
        var compensationFuel = document.getElementById("compensationFuel").value;
        var fuelPriceDetail = document.getElementById("fuelPriceDetail").value;
        var enrouteFuel = document.getElementById("enrouteFuel").value;
        document.getElementById("consumedFuel").value = (parseFloat(filledFuel)).toFixed(2);
//        document.getElementById("consumedFuel").value = (parseFloat(filledFuel) + parseFloat(enrouteFuel)).toFixed(2);
        var consumedFuel = document.getElementById("consumedFuel").value;
        if (compensationFuel == '') {
            document.getElementById("compensationFuel").value = 0;
            compensationFuel = 0;
        }
        if (consumedFuel == '') {
            consumedFuel = 0;
        }
        var recoveryFuel = 0;
        if (parseFloat(consumedFuel) >= parseFloat(allocatedFuel)) {
//            if (parseFloat(compensationFuel) > (parseFloat(consumedFuel) - parseFloat(allocatedFuel))) {
//                alert("compensation fuel cannot be greater than max recoverable fuel");
//                document.getElementById("compensationFuel").value = 0;
//                compensationFuel = 0;
//            }
            recoveryFuel = parseFloat(consumedFuel) - parseFloat(allocatedFuel) - parseFloat(compensationFuel);
            document.getElementById("recoveryFuel").value = parseFloat(recoveryFuel).toFixed(2);
            document.getElementById('compensationFuel').readOnly = false;
        } else {
            recoveryFuel = 0;
            document.getElementById("recoveryFuel").value = 0;
            document.getElementById("compensationFuel").value = 0;
            document.getElementById('compensationFuel').readOnly = true;
        }


        var recFuelAmt = parseFloat(recoveryFuel) * parseFloat(fuelPriceDetail);
        document.getElementById("recoveryAmount").value = parseFloat(recFuelAmt).toFixed(2);
    }


    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function () {
        $(".datepicker").datepicker({
            dateFormat: 'dd-mm-yy',
            changeMonth: true, changeYear: true
        });
    });</script>

<script>
    function showBunks(rowVal){
        var fuelMode = document.getElementById('fuelMode' + rowVal).value;
        var bunkName = document.getElementById('bunkName' + rowVal).value;
//        alert("=====>"+fuelMode);
        if (fuelMode == 2) {
            $('#bunkName' + rowVal).empty();
            $('#bunkName' + rowVal).append(
                    $('<option selected></option>').val('20~0.00').html('IOCL Fleet Card')
                    )
            $('#bunkName' + rowVal).append(
                    $('<option></option>').val('21~0.00').html('Cash')
                    )
//            $('#fuelPriceRemarksEn' + rowVal).show();
//            $('#fuelPriceRemarks' + rowVal).hide();
            
        }else {
            getAjaxBunkList(rowVal);
//            $('#fuelPriceRemarks' + rowVal).show();
//            $('#fuelPriceRemarksEn' + rowVal).hide();
        }

    }
    
    function getAjaxBunkList(val) {
        $.ajax({
            url: '/throttle/getAjaxBunkList.do',
            data: {compId:val},
            dataType: 'json',
            success: function (data) {
                $('#bunkName' + val).empty();
//                $('#bunkName' + val).append(
////                        $('<option ></option>').val("0").html("--select--")
//                        )
                $.each(data, function (i, data) {
                    $('#bunkName' + val).append(
                            $('<option style="width:150px"></option>').val(data.bunkId+"~"+data.bunkName+"~"+data.locationId+"~"+data.fuelPrice).html(data.bunkName)
                            )
                });
            }
        });
    }

    function addFuelRow()
    {
        var poItems = 0;
        var rowCount = '';
        var sno = '';
        var snumber = '';
//       alert("my step 1")
        var currentDate = new Date();
        var day = currentDate.getDate();
        var month = currentDate.getMonth() + 1;
        var year = currentDate.getFullYear();
        var myDate = day + "-" + month + "-" + year;
        if (sno < 9) {
            sno++;
            var tab = document.getElementById("fuelTBL");
            var rowCount = tab.rows.length;
            snumber = parseInt(rowCount) - 1;
            var newrow = tab.insertRow(parseInt(rowCount) - 1);
            newrow.height = "30px";
            var cell = newrow.insertCell(0);
            var cell0 = "<td><input type='hidden'  name='fuelItemId' /> " + snumber + "</td>";
            cell.innerHTML = cell0;
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text2'><select class='form-control' style='width:200px;height:40px;' id='fuelMode" + snumber + "'  onchange='showBunks(" + snumber + ");' style='width:123px'  name='fuelMode' >\n\
            <option value='1'>Standard Fueling</option><option value='2'>EnRoute Fueling</option></select> \n\
            </td>";
            
            cell.innerHTML = cell0;
            cell = newrow.insertCell(2);
//            var cell0 = "<td class='form-control' height='30' ><input type='hidden' id='bunkId" + snumber + "' name='bunkId' value='0'><select name='bunkName' id ='bunkName" + snumber + "' class='form-control' onchange='setFuelPriceList(" + snumber + ");' style='width:120px;height:40px;'  ><option value='0'>--select---</option> <c:if test="${tripDiesel.fuelingMode == '0' || tripDiesel.fuelingMode == '1'}"><c:if test="${details.companyId != '0'}">"
//            "<option  value='<c:out value="${details.locationId}" />~<c:out value="${details.fuelPrice}" />'><c:out value="${details.bunkName}" /></option> </c:if> </c:if> <c:if test="${tripDiesel.fuelingMode == '2'}"><c:if test="${details.companyId == '0'}"><option  value='<c:out value="${details.locationId}" />~<c:out value="${details.fuelPrice}" />'><c:out value="${details.bunkName}" /></option></c:if></c:if><input type='hidden' id='uniqueId" + snumber + "' name='uniqueId' value=''/><input type='hidden' id='bunkPlace" + snumber + "' name='bunkPlace' value=''/> </select>";
            var cell0 = "<td class='form-control' height='30' ><input type='hidden' id='bunkId" + snumber + "' name='bunkId' value='0'><select name='bunkName' id ='bunkName" + snumber + "' class='form-control' onchange='setFuelPriceList(" + snumber + ");' style='width:180px;height:40px;'  ><option value='0'>--select---</option> <c:if test="${bunkList != null}" ><c:forEach items="${bunkList}" var="details"><option  value='<c:out value="${details.bunkId}" />'><c:out value="${details.bunkName}" /> </c:forEach > </c:if> <input type='hidden' id='uniqueId" + snumber + "' name='uniqueId' value=''/><input type='hidden' id='bunkPlace" + snumber + "' name='bunkPlace' value=''/> </select>";
//           cell.setAttribute("className", styls);
            
            cell.innerHTML = cell0;
            cell = newrow.insertCell(3);
            cell0 = "<td class='text1'><input name='slipNo' id='slipNo" + snumber + "' type='text'  style='width:120px;height:40px;' value='' class='form-control' /></td>";
            cell.innerHTML = cell0;
            
            cell = newrow.insertCell(4);
            cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate" + snumber + "' type='text' class='form-control datepicker' onchange='getAddRowFuelPrice(" + snumber + ");' style='width:120px;height:40px;' value='" + myDate + "' class='form-control' /></td>";
            cell.innerHTML = cell0;
            cell = newrow.insertCell(5);
            cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' onchange='fuelPrice1(" + snumber + ");' type='text' class='form-control' id='fuelLtrs" + snumber + "' value='0' style='width:120px;height:40px;'  onKeyPress='return onKeyPressBlockCharacters(event);'  class='form-control' /></td>";
            cell.innerHTML = cell0;
            
            cell = newrow.insertCell(6);
//            cell0 = "<td class='text1'><input type='hidden' id='rowFuelPrice" + snumber + "' name='rowFuelPrice' value='0'><input type='text' id='fuelPriceRemarksEn" + snumber + "' name='fuelPriceRemarksEn' value='0' onchange='fuelPrice1(" + snumber + ");' class='form-control' style='display:none;'><select name='fuelPriceRemarks' id ='fuelPriceRemarks" + snumber + "' class='form-control' onchange='fuelPrice1(" + snumber + ");' style='width:200px;height:40px;'  ><option value='0'>--select---</option> </select></td>";
            cell0 = "<td class='text1'><input type='hidden' id='rowFuelPrice" + snumber + "' name='rowFuelPrice' value='0'><input type='text' id='fuelPriceRemarks" + snumber + "' name='fuelPriceRemarks' value='0' onchange='fuelPrice1(" + snumber + ");' class='form-control' onKeyPress='return onKeyPressBlockCharacters(event);'></td>";
            cell.innerHTML = cell0;
            
            cell = newrow.insertCell(7);
            cell0 = "<td class='text1'><input name='fuelAmount'  onBlur='sumFuel();' type='text' class='form-control' value='0' style='width:120px;height:40px;' id='fuelAmount" + snumber + "'  onKeyPress='return onKeyPressBlockCharacters(event);'   class='form-control' /></td>";
            cell.innerHTML = cell0;
            cell = newrow.insertCell(8);
            cell0 = "<td class='text1'><input name='fuelFilledBy'  type='hidden' class='textbox' id='fuelFilledBy'  value='<%= session.getAttribute("userId")%>' />\n\
            <input name='fuelFilledName' type='text' style='width:120px;height:40px;' class='form-control' id='fuelFilledName' class='Textbox' value='<%= session.getAttribute("userName")%>' /></td>";
            cell.innerHTML = cell0;
            cell = newrow.insertCell(9);
            cell0 = "<td class='text1'><textarea name='fuelRemarks' id='fuelRemarks" + snumber + "' class='textbox' type='text' rows='2' cols='10'></textarea></td>";
            cell.innerHTML = cell0;
//           cell = newrow.insertCell(9);
//           var cell1 = "<td><input type='checkbox' name='deleteItem' value='" + snumber + "'/> </td>";
//           cell.innerHTML = cell1;

            $(".datepicker").datepicker({
                changeMonth: true, changeYear: true
            });
        }
    }

    function delFuelRow() {
        try {
            var table = document.getElementById("fuelTBL");
            rowCount = table.rows.length - 1;
            for (var i = 2; i < rowCount; i++) {
                var row = table.rows[i];
                var checkbox = row.cells[7].childNodes[0];
                if (null != checkbox && true == checkbox.checked) {
                    if (rowCount <= 1) {
                        alert("Cannot delete all the rows");
                        break;
                    }
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                    sno--;
                }
            }
        } catch (e) {
            alert(e);
        }
    }


    function setFuelPriceList(val) {
        var temp = "";
        var temp1 = "";
        var fuelDate = "";
        temp = document.getElementById('bunkName' + val).value;
        fuelDate = document.getElementById('fuelDate' + val).value;
        temp1 = temp.split("~");
        var bunkId = temp1[0];
        $('#bunkId' + val).val(bunkId);
        $.ajax({
            url: '/throttle/fuelPriceList.do',
            data: {bunkId: bunkId, fuelDate: fuelDate},
            dataType: 'json',
            success: function (data) {
                $('#fuelPriceRemarks' + val).empty();
                $('#fuelPriceRemarks' + val).append(
                        $('<option style="width:200px"></option>').val("0~0").html("--select--")
                        )
                $.each(data, function (i, data) {
                    $('#fuelPriceRemarks' + val).append(
                            $('<option style="width:200px"></option>').val(data.Name).html(data.Name)
                            )
                });
            }
        });
    }


    function setFuelPriceList1(vals) {
        var temp = "";
        var temp1 = "";
        var fuelDate = "";
        var fuelPriceRmrk = "";
        //alert(vals);
        temp = document.getElementById('bunkIdE' + vals).value;
        fuelPriceRmrk = document.getElementById('fuelPriceRmrk' + vals).value;
        fuelDate = document.getElementById('fuelDateE' + vals).value;
        temp1 = temp.split("~");
        var bunkId = temp1[0];
        //alert(bunkId)
        $.ajax({
            url: '/throttle/fuelPriceList.do',
            data: {bunkId: bunkId, fuelDate: fuelDate},
            dataType: 'json',
            success: function (data) {
                $('#fuelPriceRemarksE' + vals).empty();
//                $('#fuelAmountE' + val).val(0);
                $('#fuelPriceRemarksE' + vals).append(
                        $('<option style="width:200px"></option>').val("0~0").html("--select--")
                        )
                $.each(data, function (i, data) {
                    $('#fuelPriceRemarksE' + vals).append(
                            $('<option style="width:200px"></option>').val(data.Name).html(data.Name)
                            )
                });
                 $("#fuelPriceRemarksE" + vals).val(fuelPriceRmrk);
            }
        });
    }


</script>

<script>
    function getAddRowFuelPrice(sno) {
        var temp = "";
        var temp1 = "";
        var temp2 = "";
        temp1 = document.getElementById('bunkName' + sno).value;
        temp2 = document.getElementById('fuelDate' + sno).value;
//               alert("temp1::"+temp1);
        temp = temp1.split("~");
        var bunkId = temp[0];
        // alert(bunkId);
        var url = "/throttle/getAddRowFuelPrice.do?bunkId=" + bunkId + "&fuelDate=" + temp2;
        if (window.ActiveXObject)
        {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest)
        {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function () {
            processAjax11(sno);
        };
        httpReq.send(null);
    }

    function processAjax11(sno){

        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
//                   alert("SNo:::"+sno);
//                   alert("Success:::"+temp);
                document.getElementById("rowFuelPrice" + sno).value = parseFloat(temp).toFixed(2);
            } else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }

        }
    }
</script>

 <script>
    function dateDiff() {
        var fromDate = document.getElementById('graReportDate').value;
        var toDate = document.getElementById('graEndDate').value;
        var graReportHour = document.getElementById('graReportHour').value;
        var graEndHour = document.getElementById('graEndHour').value;
        var graReportMin = document.getElementById('graReportMinute').value;
        var graEndMin = document.getElementById('graEndMinute').value;
        var tatHours = document.getElementById('tatHours').value;
        var tatRate = document.getElementById('tatRate').value;
        var detFromHour = document.getElementById('detFromHour').value;
        var detToHour = document.getElementById('detToHour').value;
        var earDate = fromDate.split("-");
        var nexDate = toDate.split("-");
        // Date calculation 
        var prevTime = new Date(earDate[2], earDate[1], earDate[0]); // Feb 1, 2011
        var thisTime = new Date(nexDate[2], nexDate[1], nexDate[0]);
//       // Date calculation 
        var date1 = new Date(fromDate.split("-")[2], fromDate.split("-")[1] - 1, fromDate.split("-")[0]);
        var date2 = new Date(toDate.split("-")[2], toDate.split("-")[1] - 1, toDate.split("-")[0]);
        //        Date validation 
        if (date1 <= date2) {

            var diffTime = (Math.abs(date2.getTime() - date1.getTime())) / 1000;
            diffTime /= (60);
            var diffHour = (graEndHour - graReportHour) * 60;
            var diffMin = (graEndMin - graReportMin);
            var totaldHours = +diffTime + +diffHour + +diffMin;
            var totalHours = totaldHours / (60);
            var remainder = totalHours % 1;
            var quotient = totalHours - remainder;
            var totalMin = totaldHours % 60;
            //alert(totalHours);
            var totalDays = totalHours / 24;
            //alert(totalDays);

            document.getElementById('totalHours').value = quotient + ':' + totalMin;
            document.getElementById('totDays').value = Math.round(totalDays);
//            document.getElementById('totDays').value = Math.round(totalDays);
            document.getElementById('graDiffDate').value = detFromHour + ' - ' + detToHour;
            var detentionCharge = 0;
            if (totalHours > detFromHour) {

                if (totalHours > detFromHour && totalHours <= detToHour) {

                    detentionCharge = Math.round(totalDays) * tatRate;
                } else {
                    detentionCharge = Math.round(totalDays) * tatRate;
                }

            } else {
                detentionCharge = 0;
            }

//            var detentionHours = "";
//            if (tatHours < totalHours) {
//                detentionHours = ((totaldHours) - (tatHours * 60));
//            } else {
//                detentionHours = 0;
//            }

//            var detenHrs = detentionHours / 60;
//            var remai = detenHrs % 1;
//            var quoti = detenHrs - remai;
//
//            var detenMin = detentionHours % 60;
//            if (detenMin < 15) {
//                document.getElementById('graDiffDate').value = quoti + ':' + 00;
//                detentionHours = quoti;
//            } else if (detenMin >= 15 && detenMin < 45) {
//                document.getElementById('graDiffDate').value = quoti + ':' + 30;
//                quoti = ((quoti * 60) + 30) / 60;
//                detentionHours = quoti;
//            } else {
//                detentionHours = +quoti + +1;
//                document.getElementById('graDiffDate').value = detentionHours + ':' + 00;
//            }

            document.getElementById('detentionCharge').value = detentionCharge.toFixed(2);
        }
    }


     function setDateDiff() {
        var fromDate = document.getElementById('haltStartDate').value;
        var toDate = document.getElementById('haltEndDate').value;
        var graReportHour = document.getElementById('haltStartHour').value;
        var graEndHour = document.getElementById('haltEndHour').value;
        var graReportMin = document.getElementById('haltStartMinute').value;
        var graEndMin = document.getElementById('haltEndMinute').value;
        var tatHours = document.getElementById('tatHours').value;
        var tatRate = document.getElementById('tatRate').value;
        var detFromHour = document.getElementById('detFromHour').value;
        var detToHour = document.getElementById('detToHour').value;
        var earDate = fromDate.split("-");
        var nexDate = toDate.split("-");
        // Date calculation 
        var prevTime = new Date(earDate[2], earDate[1], earDate[0]); // Feb 1, 2011
        var thisTime = new Date(nexDate[2], nexDate[1], nexDate[0]);
//       // Date calculation 
        var date1 = new Date(fromDate.split("-")[2], fromDate.split("-")[1] - 1, fromDate.split("-")[0]);
        var date2 = new Date(toDate.split("-")[2], toDate.split("-")[1] - 1, toDate.split("-")[0]);
        //        Date validation 
        if (date1 <= date2) {

            var diffTime = (Math.abs(date2.getTime() - date1.getTime())) / 1000;
            diffTime /= (60);
            var diffHour = (graEndHour - graReportHour) * 60;
            var diffMin = (graEndMin - graReportMin);
            var totaldHours = +diffTime + +diffHour + +diffMin;
            var totalHours = totaldHours / (60);
            var remainder = totalHours % 1;
            var quotient = totalHours - remainder;
            var totalMin = totaldHours % 60;
            //alert(totalHours);
            var totalDays = totalHours / 24;
            //alert(totalDays);

            document.getElementById('totalHours').value = quotient + ':' + totalMin;
            document.getElementById('totDays').value = Math.round(totalDays);
//            document.getElementById('totDays').value = Math.round(totalDays);
            document.getElementById('graDiffDate').value = detFromHour + ' - ' + detToHour;
            var detentionCharge = 0;
            if (totalHours > detFromHour) {

                if (totalHours > detFromHour && totalHours <= detToHour) {

                    detentionCharge = Math.round(totalDays) * tatRate;
                } else {
                    detentionCharge = Math.round(totalDays) * tatRate;
                }

            } else {
                detentionCharge = 0;
            }

//            var detentionHours = "";
//            if (tatHours < totalHours) {
//                detentionHours = ((totaldHours) - (tatHours * 60));
//            } else {
//                detentionHours = 0;
//            }

//            var detenHrs = detentionHours / 60;
//            var remai = detenHrs % 1;
//            var quoti = detenHrs - remai;
//
//            var detenMin = detentionHours % 60;
//            if (detenMin < 15) {
//                document.getElementById('graDiffDate').value = quoti + ':' + 00;
//                detentionHours = quoti;
//            } else if (detenMin >= 15 && detenMin < 45) {
//                document.getElementById('graDiffDate').value = quoti + ':' + 30;
//                quoti = ((quoti * 60) + 30) / 60;
//                detentionHours = quoti;
//            } else {
//                detentionHours = +quoti + +1;
//                document.getElementById('graDiffDate').value = detentionHours + ':' + 00;
//            }

            document.getElementById('detentionCharge').value = detentionCharge.toFixed(2);
        }
    }


    function calculateDetentionCharge() {

        var detentionCharge = 0;
        var tatRate = document.getElementById('tatRate').value;
        var totalDays = document.getElementById('totDays').value;
        detentionCharge = totalDays * tatRate;
        document.getElementById('detentionCharge').value = detentionCharge.toFixed(2);
    }

</script>


<style>

    .loading {

        border:10px solid red;

        display:none;

    }

    .button1{border: 1px black solid ; color:#000 !important;  }



    .spinner{

        position: fixed;

        top: 50%;

        left: 50%;

        margin-left: -50px; /* half width of the spinner gif */

        margin-top: -50px; /* half height of the spinner gif */

        text-align:center;

        z-index:1234;

        overflow: auto;

        width: 300px; /* width of the spinner gif */

        height: 300px; /*hight of the spinner gif +2px to fix IE8 issue */

    }

    .ui-loader-background {

        width:100%;

        height:100%;

        top:0;

        padding: 0;

        margin: 0;

        background: rgba(0, 0, 0, 0.3);

        display:none;

        position: fixed;

        z-index:100;

    }

    .ui-loader-background {

        display:block;



    }

</style>


<script>
    
    function setContainerSelect(snoo) {
        document.getElementById("selectedIndexCon" + snoo).checked = true;
    }

    function onKeyPressBlockCharacters1(sno3, e) {
        var fieldLength = document.getElementById('containerNo' + sno3).value.length;
        if (fieldLength <= 3) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /\d/;
            return !reg.test(keychar);
        } else if (fieldLength <= 11) {
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /[a-zA-Z]+$/;
            return !reg.test(keychar);
        }
    }


    function saveContainer() {

        var val = document.getElementById("tripId").value;
//        alert("val::"+val);
        var selectedIndex = document.getElementsByName('selectedIndexCon');
        var containerNo = document.getElementsByName('containerNo');
        var sealNo = document.getElementsByName('sealNo');
        var tonnage = document.getElementsByName('tonnage');
        var tripConId = document.getElementsByName('tripConId');
        var custRefNo = document.getElementsByName('custRefNo');
//        alert("val::"+tripConId);
        var temp = "";
        var temp1 = "";
        var cntr = 0;
        var temp2 = "";
        var temp3 = "";
        var temp4 = "";
        if (selectedIndex.length > 0) {
            for (var i = 0; i < selectedIndex.length; i++) {
//                alert("i-===" + i);
//                alert(selectedIndex[i].checked);
                if (selectedIndex[i].checked == true) {
//                    alert(cntr);
//                    alert("containerNo[i].length-===" + containerNo[i].value.length);
//                    alert("containerNo[i].value-===" + containerNo[i].value);
                    if (containerNo[i].value.length != 11 || containerNo[i].value == "") {
                        alert("Please Enter Valid Container No");
                        return false;
                    }
                    if (sealNo[i].value == "") {
                        alert("Please Enter Valid Seal No");
                        return false;
                    }
                    if (tonnage[i].value == "") {
                        alert("Please Enter Tonnage");
                        return false;
                    }
                    if (custRefNo[i].value == "") {
                        alert("Please Enter Customer Reference No");
                        return false;
                    }
                    if (cntr == 0) {
                        temp = tripConId[i].value;
                        temp1 = containerNo[i].value;
                        temp2 = sealNo[i].value;
                        temp3 = tonnage[i].value;
                        temp4 = custRefNo[i].value;
                        // alert(temp);
                        // alert(temp2);
                    } else {
                        temp = temp + "," + tripConId[i].value;
                        temp1 = temp1 + "," + containerNo[i].value;
                        temp2 = temp2 + "," + sealNo[i].value;
                        temp3 = temp3 + "," + tonnage[i].value;
                        temp4 = temp4 + "," + custRefNo[i].value;
                    }
                    cntr++;
                }
            }
        }


        var confirms = confirm("Do you really want to update?");
        if (confirms == true) {
            var myWindow;
            myWindow = window.open("/throttle/updateTripContainers.do?tripId=" + val + "&containerNoTemp=" + temp1 + "&containerTon=" + temp3 + "&containerSeal=" + temp2 + "&tripConId=" + temp + "&custRefNo=" + temp4);
//            myWindow = window.open("/throttle/updateTripContainers.do?tripId=" + val + "&containerNoTemp=asds435353&containerTon=100&containerSeal=121&tripConId=2647");
//                    myWindow.close();
//setTimeout(function () { myWindow.close();}, 300);
            location.reload();
        }

    }
</script>


<script>

    function saveTripEndDetails() {

        var startKm = document.getElementById("startKM").value;
        var endKm = document.getElementById("endOdometerReading").value;
        if (document.getElementById("movementType").value != '3') {
            if (isEmpty(document.getElementById("vehicleactreportdate").value)) {
                alert('please enter vehicle actual report date');
                document.getElementById("vehicleactreportdate").focus();
                return;
            }
            if (isEmpty(document.getElementById("vehicleloadreportdate").value)) {
                alert('please enter vehicle load report date');
                document.getElementById("vehicleloadreportdate").focus();
                return;
            }
        }
        if (isEmpty(document.getElementById("endDate").value)) {
            alert('please enter vehicle end date');
            document.getElementById("endDate").focus();
            return;
        }
        if (isEmpty(document.getElementById("tripEndHour").value)) {
            alert('please enter trip End Hour');
            document.getElementById("tripEndHour").focus();
            return;
        }
        if (isEmpty(document.getElementById("tripEndMinute").value)) {
            alert('please enter trip End Minute');
            document.getElementById("tripEndMinute").focus();
            return;
        }
        if (isEmpty(document.getElementById("endOdometerReading").value)) {
            alert('please enter end odometer reading');
            document.getElementById("endOdometerReading").focus();
            return;
        }
        if (isEmpty(document.getElementById("endHM").value)) {
            alert('please enter end HM');
            document.getElementById("endHM").focus();
            return;
        }
        if (isEmpty(document.getElementById("tripDetainHour").value)) {
            alert('please enter Detain Hours');
            document.getElementById("tripDetainHour").focus();
        }
        if (document.getElementById("movementType").value == '1' || document.getElementById("movementType").value == '6') {

            if (isEmpty(document.getElementById("shipingLineNo").value)) {
                alert('please enter Shipping Bill Number');
                document.getElementById("slN").focus();
                return;
            }
        }
        if (document.getElementById("movementType").value == '2') {

            if (isEmpty(document.getElementById("billOfEntry").value)) {
                alert('please enter Bill of Entry  Number');
                document.getElementById("boE").focus();
                return;
            }
        }

        if (document.getElementById("commodityCategory").value == '0' || document.getElementById("commodityCategory").value == '') {
            alert('please enter commodity category');
            document.getElementById("commodityCategory").focus();
            return;
        }
        if (document.getElementById("commodityName").value == '') {
            alert('please enter commodity name');
            document.getElementById("commodityName").focus();
            return;
        }


        if (endKm < startKm) {
            alert('End Km cannot be lesser than start Km !please check .');
            document.getElementById("endOdometerReading").focus();
            return;
        } else {
            document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            document.tripExpense.action = '/throttle/updateEndTripSheetDuringClosure.do';
            document.tripExpense.submit();
        }
    }
    function saveTripOverRideDetails() {
        if (isEmpty(document.getElementById("setteledKm").value)) {
            alert('please enter override end odometer reading');
            document.getElementById("setteledKm").focus();
        }
        if (isEmpty(document.getElementById("setteledHm").value)) {
            alert('please enter override end reefer reading');
            document.getElementById("setteledHm").focus();
        }
        if (isEmpty(document.getElementById("overrideRemarks").value)) {
            alert('please enter override remarks');
            document.getElementById("overrideRemarks").focus();
        } else {
            document.tripExpense.action = '/throttle/updateOverrideTripSheetDuringClosure.do';
            document.tripExpense.submit();
        }
    }
    function saveTripStartDetails() {
        var tripStartKm = document.getElementById("tripStartKm").value;
        var tripStartHm = document.getElementById("tripStartHm").value;
        if (tripStartKm == "" || tripStartKm == "0.00") {
            alert('please enter tripStartKm');
            document.getElementById("tripStartKm").focus();
            return;
        }
        if (tripStartHm == "" || tripStartHm == "0.00") {
            alert('please enter tripStartHm');
            document.getElementById("tripStartHm").focus();
            return;
        } else {
            document.tripExpense.action = '/throttle/updateStartTripSheetDuringClosure.do';
            document.tripExpense.submit();
        }
    }
    function calTotalKM() {
        var startKM = document.getElementById("startKM").value;
        var endKM = document.getElementById("endOdometerReading").value;
        document.getElementById("totalKM").value = endKM - startKM;
    }
    function calSettelTotalKM() {
        var startKM = document.getElementById("startKM").value;
        var endKM = document.getElementById("setteledKm").value;
        var TotalSettelKM = endKM - startKM;
        document.getElementById("setteltotalKM").value = TotalSettelKM;
    }
    function calTotalHM() {
        var startHM = document.getElementById("startHM").value;
        var endHM = document.getElementById("endHM").value;
        document.getElementById("totalHrs").value = endHM - startHM;
    }
    function calSettelTotalHM() {
        var startHM = document.getElementById("startHM").value;
        var endHM = document.getElementById("setteledHm").value;
        var TotalSettelHm = endHM - startHM;
        document.getElementById("setteltotalHrs").value = TotalSettelHm;
    }
    function saveOdoApprovalRequest1111() {
//        alert("alert1");
//        document.getElementById("spinner").style.display = "block";
//        document.getElementById("loader").style.display = "block";
//        document.getElementById('loader').className += 'ui-loader-background';
        var odoUsageRemarks = document.tripExpense.odoUsageRemarks.value;
        var requestType = 1; //odo request
        saveApprovalRequest(odoUsageRemarks, requestType, 1)

    }
    function saveOdoApprovalRequest(val) {
//        alert("alert2="+val);
//        document.getElementById("spinner").style.display = "block";
//        document.getElementById("loader").style.display = "block";
//        document.getElementById('loader').className += 'ui-loader-background';
        var remarks = document.tripExpense.odoUsageRemarks.value;
        var requestType = 1; //odo request
        var tripId = document.tripExpense.tripSheetId.value;
        remarks = remarks.trim();
        var rcmExp = document.tripExpense.estimatedExpense.value;
        var nettExp = document.tripExpense.nettExpense.value;
        var url = "/throttle/saveClosureApprovalRequest.do?tripId=" + tripId + "&requestType=" + requestType + "&rcmExp=" + rcmExp + "&nettExp=" + nettExp + "&remarks=" + remarks;
        document.tripExpense.action = url;
        document.tripExpense.submit();
    }
    function saveOdoReApprovalRequest() {
//        document.getElementById("spinner").style.display = "block";
//        document.getElementById("loader").style.display = "block";
//        document.getElementById('loader').className += 'ui-loader-background';
        var odoUsageRemarks = document.tripExpense.odoUsageReApprovalRemarks.value;
        var requestType = 1; //odo request
        saveApprovalRequest(odoUsageRemarks, requestType, 2)
    }
    function saveExpDeviationApprovalRequest() {
//        document.getElementById("spinner").style.display = "block";
//        document.getElementById("loader").style.display = "block";
//        document.getElementById('loader').className += 'ui-loader-background';
        var remarks = document.tripExpense.expDeviationRemarks.value;
        var requestType = 2; //exp request
        saveApprovalRequest(remarks, requestType, 3)
    }
    function saveExpDeviationApprovalRequest(val) {
//        document.getElementById("spinner").style.display = "block";
//        document.getElementById("loader").style.display = "block";
//        document.getElementById('loader').className += 'ui-loader-background';
        var remarks = document.tripExpense.expDeviationRemarks.value;
        var requestType = 2; //exp request
        //saveApprovalRequest(remarks, requestType, 3);

        var tripId = document.tripExpense.tripSheetId.value;
        remarks = remarks.trim();
        var rcmExp = document.tripExpense.estimatedExpense.value;
        var nettExp = document.tripExpense.nettExpense.value;
        var url = "/throttle/saveClosureApprovalRequest.do?tripId=" + tripId + "&requestType=" + requestType + "&rcmExp=" + rcmExp + "&nettExp=" + nettExp + "&remarks=" + remarks;
        document.tripExpense.action = url;
        document.tripExpense.submit();
    }
    function saveexpDeviationReApprovalRequest() {
//        document.getElementById("spinner").style.display = "block";
//        document.getElementById("loader").style.display = "block";
//        document.getElementById('loader').className += 'ui-loader-background';
        var remarks = document.tripExpense.expDeviationReApprovalRemarks.value;
        var requestType = 2; //exp request
        saveApprovalRequest(remarks, requestType, 4)
    }

    var httpReq;
    var temp = "";
    function saveApprovalRequest(remarks, requestType, val) {
        var tripId = document.tripExpense.tripSheetId.value;
        remarks = remarks.trim();
        var rcmExp = document.tripExpense.estimatedExpense.value;
        var nettExp = document.tripExpense.nettExpense.value;
        if (remarks != '') {
            var url = "/throttle/saveClosureApprovalRequest.do?tripId=" + tripId + "&requestType=" + requestType + "&remarks=" + remarks + "&rcmExp=" + rcmExp + "&nettExp=" + nettExp;
            //alert(url);
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function () {
                processOdoApprovalRequest(requestType, val);
            };
            httpReq.send(null);
        } else {
            alert('please enter request description');
        }

    }

    function processOdoApprovalRequest(requestType, val) {
        if (httpReq.readyState == 4) {
            if (httpReq.status == 200) {
                temp = httpReq.responseText.valueOf();
                //alert(temp);
                if (temp != '0' && temp != '' && temp != null && temp != 'null') {
                    if (val == 1) {
                        $('#odoApprovalRequest').hide();
                        $('#odoPendingApproval').show();
                    }
                    if (val == 2) {
                        $('#odoReApprovalRequest').hide();
                        $('#odoPendingReApproval').show();
                    }
                    if (val == 3) {
                        $('#expDeviationApprovalRequest').hide();
                        $('#expDeviationPendingApproval').show();
                    }
                    if (val == 4) {
                        $('#expDeviationReApprovalRequest').hide();
                        $('#expDeviationPendingReApproval').show();
                    }
                }
            } else {
                //alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
        document.tripExpense.action = '/throttle/viewTripExpense.do';
        document.tripExpense.submit();
    }


    function calculateDays() {
        var fromDate = document.getElementById("startDate").value;
        var toDate = document.getElementById("endDate").value;
        fromDate = fromDate.split("-");
        toDate = toDate.split("-");
        var date1 = new Date(fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2]);
        var date2 = new Date(toDate[1] + '/' + toDate[0] + '/' + toDate[2]);
        var days1 = (date2 - date1) / 1000 / 60 / 60 / 24;
        days1 = days1 - (date2.getTimezoneOffset() - date1.getTimezoneOffset()) / (60 * 24);
        document.getElementById("totalDays1").value = days1 + 1;
        var stTimeIds = document.getElementById("startTime").value;
        var hour1 = document.getElementById("tripEndHour").value;
        var minute1 = document.getElementById("tripEndMinute").value;
        var endTimeIds = hour1 + ":" + minute1 + ":" + "00";
        var date1 = new Date(fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2] + ' ' + stTimeIds);
        var date2 = new Date(toDate[1] + '/' + toDate[0] + '/' + toDate[2] + ' ' + endTimeIds);
        var days1 = (date2 - date1) / 1000 / 60 / 60;
        days1 = days1 - (date2.getTimezoneOffset() - date1.getTimezoneOffset()) / (60 * 24);
        document.getElementById("tripTransitHour").value = Math.floor(days1);
        var fromDate = document.getElementById("vehicleactreportdate").value;
        var endDate = document.getElementById("endDate").value;
        var factoryInHours = document.getElementById("vehicleactreporthour").value;
        var factoryInMin = document.getElementById("vehicleactreportmin").value;
        var factoryTimeIds = factoryInHours + ":" + factoryInMin + ":" + "00";
        fromDate = fromDate.split("-");
        toDate = endDate.split("-");
        var date1 = new Date(fromDate[1] + '/' + fromDate[0] + '/' + fromDate[2] + ' ' + factoryTimeIds);
        var date2 = new Date(toDate[1] + '/' + toDate[0] + '/' + toDate[2] + ' ' + endTimeIds);
        var days1 = (date2 - date1) / 1000 / 60 / 60;
        days1 = days1 - (date2.getTimezoneOffset() - date1.getTimezoneOffset()) / (60 * 24);
        document.getElementById("tripFactoryToIcdHour").value = Math.floor(days1);
    }

    function setEndTime() {
        var endtime = document.getElementById('endTime').value;
        var endtimes = endtime.split(":");
        document.tripExpense.tripEndHour.value = endtimes[0];
        document.tripExpense.tripEndMinute.value = endtimes[1];
    }
    function currentTime() {
//                alert("hellogopi");
        var date, curr_hour, curr_minute;
        date = new Date();
        curr_hour = date.getHours();
        curr_minute = date.getMinutes();
        if (parseInt(curr_hour) < 10) {
            curr_hour = '0' + curr_hour;
        }
        if (parseInt(curr_minute) < 10) {
            curr_minute = '0' + curr_minute;
        }
        document.getElementById("vehicleloadreporthour").value = curr_hour;
        document.getElementById("vehicleloadreportmin").value = curr_minute;
        document.getElementById("vehicleactreporthour").value = curr_hour;
        document.getElementById("vehicleactreportmin").value = curr_minute;
        document.getElementById("tripEndHour").value = curr_hour;
        document.getElementById("tripEndMinute").value = curr_minute;
    }

</script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<%
    Date today = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    String todayDate = sdf.format(today);
%>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function () {
//alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });</script>
<script type="text/javascript" language="javascript">
    function submitPage() {

        var expenseType = document.getElementsByName("expenseType");
        var expenseName = document.getElementsByName("expenseName");
        var check = 0;
        for (var i = 0; i < expenseType.length; i++) {
            if (expenseName[i].value != 0) {

                if (expenseType[i].value == 0) {
                    check = 1;
                }
            }
            if (check == 1) {
                alert("please select the expense type in row" + (i + 1));
                document.getElementById("expenseType" + i).focus();
                return;
            }
        }

        document.getElementById("spinner").style.display = "block";
        document.getElementById("loader").style.display = "block";
        document.getElementById('loader').className += 'ui-loader-background';
        document.tripExpense.action = '/throttle/saveTripOtherExpense.do';
        document.tripExpense.submit();
    }

    function submitPageDone() {
        var txt;
        var r = confirm("Press a button!");
        if (r == true) {
            document.tripExpense.action = '/throttle/saveDoneTripOtherExpense.do';
            document.tripExpense.submit();
        } else {
            txt = "You pressed Cancel!";
        }
    }

    function updateDetention() {
        var detention = document.getElementById('detentionCharge').value;
        var haltStartDate = document.getElementById('haltStartDate').value;
        var haltEndDate = document.getElementById('haltEndDate').value;
        if ((haltStartDate == "00-00-0000" || haltStartDate == "")) {
            alert('Please select Halt Start Date');
            document.getElementById("haltStartDate").focus();
            return;
        }else if((haltEndDate == "00-00-0000" || haltEndDate == "")){
            alert('Please select Halt End Date');
            document.getElementById("haltEndDate").focus();
            return;
        } else {
            $("#detentionVal").hide();
            document.tripExpense.action = '/throttle/updateDetentionCharge.do';
            document.tripExpense.submit();
        }
    }

    function closeTrip() {
//        alert("111111");
//        if (isEmpty(document.getElementById("endDate").value)) {
//            alert('Alert!!please go to Edit Trip End Details Tab,fill the proper data and then proceed.');
//            document.getElementById("endDate").focus();
//            return;
//        } else {
//            alert("2222");
        var mapId = 0;
        var cnt = 0;
        var mappingId = document.getElementsByName("mappingId");
        var allocatedFuel = document.getElementById("allocatedFuel").value;
        var consumedFuel = document.getElementById("consumedFuel").value;
        var recoveryFuel = document.getElementById("recoveryFuel").value;
        var recoveryAmount = document.getElementById("recoveryAmount").value;
        var fuelSlipNo = document.getElementById("fuelSlipNo").value;
        var eWayBillNo = document.getElementById("eWayBillNo").value;
        var invoiceNo = document.getElementById("invoiceNo").value;
        var invoiceValue = document.getElementById("invoiceValue").value;
        var invoiceDate = document.getElementById("invoiceDate").value;
        var invoiceWeight = document.getElementById("invoiceWeight").value;
        var totDays = document.getElementById("totDays").value;
        var closingKM = document.getElementById("closingKM").value;
        var haltStartDate = document.getElementById('haltStartDate').value;
        var haltEndDate = document.getElementById('haltEndDate').value;
        var hireVehicleDriverDetailsSize = '<c:out value="${hireVehicleDriverDetailsSize}"/>';
//            alert("3333-----"+hireVehicleDriverDetailsSize);
        if ((closingKM == '' || closingKM == 0) && hireVehicleDriverDetailsSize <= 0 ) {
            alert("please enter closing KM");
            return false;
            closingKM.focus();
        }else if (allocatedFuel == '') {
            alert("please enter trip allocated fuel");
            return false;
            allocatedFuel.focus();
        } else if (consumedFuel.value == '') {
            alert("please enter trip consumed fuel");
            return false;
            consumedFuel.focus();
        } else if (recoveryFuel.value == '') {
            alert("please enter trip recovery fuel");
            return false;
            recoveryFuel.focus();
        } else if (recoveryAmount.value == '') {
            alert("please enter trip recovery fuel amount");
            return false;
            recoveryAmount.focus();
        } else if ('<c:out value="${totDays}"/>' == '') {
            alert("please enter total halting days");
            return false;
            totDays.focus();
        }else if ((haltStartDate == "00-00-0000" || haltStartDate == "")) {
            alert('Please select Halt Start Date');
            document.getElementById("haltStartDate").focus();
            return;
        }else if((haltEndDate == "00-00-0000" || haltEndDate == "")){
            alert('Please select Halt End Date');
            document.getElementById("haltEndDate").focus();
            return;
        } else {

//            for (var i = 0; i < mappingId.length; i++) {
//                if (mappingId[i].value == 1 || mappingId[i].value == 0 || mappingId[i].value == '' || mappingId[i].value == null) {
//                    if (i == 0 || i == 1 || i == mappingId.length - 1) {
//                        mapId = 1;
//        }
//                }
//            }
//            if (mapId == 1) {
//                $("#validateMsg").text("Please enter the Mapping Code for Point names in Location Master")
//                $("#validateMsgHire").text("Please enter the Mapping Code for Point names in Location Master")
//            } else {
            document.getElementById("spinner").style.display = "block";
            document.getElementById("loader").style.display = "block";
            document.getElementById('loader').className += 'ui-loader-background';
            document.tripExpense.action = '/throttle/closeTrip.do';
            document.tripExpense.submit();
//            }
        }

    }


    function setExpenseTypeDetails(value, sno) {
        document.getElementById('marginValue' + sno).readOnly = true;
        if (value == '1') {//bill to customer
            document.getElementById('taxPercentage' + sno).value = '';
            document.getElementById('expenses' + sno).value = '';
            //alert("fgd");
            document.getElementById('billModeTemp' + sno).disabled = false;
            document.getElementById('taxPercentage' + sno).readOnly = false;
        } else { // do not bill to customer
            document.getElementById('billMode' + sno).value = 0;
            document.getElementById('billModeTemp' + sno).value = 0;
            document.getElementById('taxPercentage' + sno).value = 0;
            document.getElementById('taxPercentage' + sno).readOnly = true;
            document.getElementById('billModeTemp' + sno).disabled = true;
            document.getElementById('marginValue' + sno).value = 0;
            document.getElementById('expenses' + sno).value = '0';
            document.getElementById('netExpense' + sno).value = '0';
        }
    }
    function setBillMode(sno) {
        document.getElementById('billMode' + sno).value = document.getElementById('billModeTemp' + sno).value;
        var billModeValue = document.getElementById('billModeTemp' + sno).value;
        document.getElementById('marginValue' + sno).value = 0;
        if (billModeValue == '2') {//Not pass thru
            document.getElementById('marginValue' + sno).readOnly = false;
        } else {
            document.getElementById('marginValue' + sno).readOnly = true;
        }
    }
    function checkAvailableAdvanceHide(sno) {

        var avaialbleAdvance = document.getElementById("availableAdvance").value;
        var expesnes = document.getElementsByName("expenses");
        //    for(var i=0;i<expesnes.length;i++){

        var enteredAmount = document.getElementById("expenses" + sno).value;
        alert("enteredAmount:" + enteredAmount + "avaialbleAdvance:" + avaialbleAdvance);
        if (parseFloat(avaialbleAdvance) >= parseFloat(enteredAmount)) {
            avaialbleAdvance = parseFloat(avaialbleAdvance) - parseFloat(enteredAmount);
            document.getElementById("availableAdvance").value = avaialbleAdvance;
        } else {
            //document.getElementById("expenses"+sno).value="";
            alert("your Amount is exceeding .U want to continue..?");
            avaialbleAdvance = parseFloat(avaialbleAdvance) - parseFloat(enteredAmount);
            document.getElementById("availableAdvance").value = avaialbleAdvance;
            // return;
        }
        calTotalExpenses(sno);
        //  }

    }
    function getAvailableAdvance() {
        saveOdoApprovalRequest('New');
        var paidAdvance = document.getElementById("paidAdvance").value;
        var saveExpesne =<%=request.getAttribute("totalExpesne")%>;
        if (saveExpesne == 'null' || saveExpesne == null) {
            saveExpesne = 0;
        }
        var expenseId = document.getElementById("expenseName" + i).value
        var expesnes = document.tripExpense.expenses;
        var enteredAmount = "";
        for (var i = 0; i < expesnes.length; i++) {

            enteredAmount = enteredAmount + document.getElementById("expenses" + i).value;
        }
        document.getElementById("availableAdvance").value = paidAdvance - (enteredAmount + saveExpesne);
        document.getElementById("availableAdvanceSpan").innerHTML = "Available:  " + (paidAdvance - (enteredAmount + saveExpesne)) + "Rs/-";
    }
    function calTotalExpenses(sno) {

        var marginValue = document.getElementById('marginValue' + sno).value;
        var tax = document.getElementById('taxPercentage' + sno).value;
        if (tax == '') {
            tax = '0';
        }
        if (marginValue == '') {
            marginValue = '0';
        }

        var expenseAmount = document.getElementById('expenses' + sno).value;
        var totalAmount = parseFloat(expenseAmount) + parseFloat(marginValue);
        var totalAmount = totalAmount + (totalAmount * parseFloat(tax) / 100);
        //                var netAmount =  Math.round(parseFloat(totalAmount).toFixed(0))  + Math.round(parseFloat(expenseAmount).toFixed(0))   ;
        //                document.getElementById('netExpense'+sno).value = netAmount;
        document.getElementById('netExpense' + sno).value = totalAmount.toFixed(2);
    }

    function totalNetAmount() {
        var tax = document.getElementsByNames("").value;
        var expenseAmount = document.getElementById('expenses').value;
        var totalAmount = tax / 100 * expenseAmount;
        var netAmount = Math.round(parseFloat(totalAmount).toFixed(0)) + Math.round(parseFloat(expenseAmount).toFixed(0));
        document.getElementById('netExpense').value = netAmount;
    }

    function openPopup1(tripExpenseId) {
        var url = '/throttle/viewExpensePODDetails.do?tripExpenseId=' + tripExpenseId;
        window.open(url, 'PopupPage', 'height=500,width=700,scrollbars=yes,resizable=yes');
    }

    function popUp(url) {
        var http = new XMLHttpRequest();
        http.open('HEAD', url, false);
        http.send();
        if (http.status != 404) {
            popupWindow = window.open(
                    url, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        } else {
            var url1 = "/throttle/content/trip/fileNotFound.jsp";
            popupWindow = window.open(
                    url1, 'popUpWindow', 'height=400,width=500,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes');
        }
    }

    function hideFactoryDetails() {
        var movementType = document.getElementById("movementType").value;
        if (movementType == '3') {
            $("tr.excludeRepo").hide();
            $("td.slN").hide();
            $("td.boE").hide();
        } else if (movementType == '2') {
            $("tr.excludeRepo").show();
            $("td.slN").hide();
            $("td.boE").show();
            $("td.boE").addClass("text1");
        } else if (movementType == '1' || movementType == '6') {
            $("tr.excludeRepo").show();
            $("td.slN").show();
            $("td.boE").hide();
            $("td.slN").addClass("text1");
        } else {
            $("td.slN").hide();
            $("td.boE").hide();
        }
    }

</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });</script>


<style>
    #index td {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
    #index th {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;

    }
    #tabHead th {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Primary Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><spring:message code="general.label.home"  text="Home"/></li>
            <li>Primary Operation</li>
            <li class="active">Trip Closure</li>
        </ol>
    </div>
</div>

<script>
//    function callTab(){
//        $(location).attr('pathname');
//        alert("Hai"+location);
//        if(location == 'http://localhost:8084/throttle/saveTripOtherExpense.do'){
//            alert("Inside::::");
//            document.getElementById("container").style.visibility = "hidden";
//            document.getElementById("tabDetails").children[0].style.display = "none";
////            document.getElementById("expenseTab").click();
//        }
//    }

    function callTab() {
        var urlPath = $(location).attr('pathname');
//      alert("Hai"+urlPath);
//      alert(urlPath.indexOf('saveTripOtherExpense.do'));
//      alert(urlPath.indexOf('saveTripFuelRecovery.do'));
        if (urlPath.indexOf('saveTripOtherExpense.do') > 0 || urlPath.indexOf('saveTripExtraFuelDetailes.do') > 0) {
            //         alert("Inside::::");
            document.getElementById("next").click();
        }
        if (urlPath.indexOf('saveTripFuelRecovery.do') > 0) {
            //         alert("Inside::::");
            document.getElementById("next").click();
            document.getElementById("next1").click();
        }
    }
</script>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="callTab();
                    setDateDiff();
                    // calculateFuelRecovery();
                    getAvailableAdvance();
                    hideFactoryDetails();
                    calculateDays();
                    saveOdoApprovalRequest('New');
                  ">
                <form name="tripExpense" method="post" >
                    <%
                    Date today1 = new Date();
                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
                    String endDate = sdf1.format(today1);
                    String revenueStr = "";
                    %>
                    <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                        <tr id="exp_table" >
                            <td colspan="8" bgcolor="#5BC0DE" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:300;">

                                    <div id="first">
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue: INR </b></font></td>
                                                        <td><font color="white"><b> <c:out value="${trip.orderRevenue}" /></b></font></td>

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense: INR </b></font></td>
                                                        <td> <font color="white"><b><c:out value="${trip.orderExpense}" /></b></font></td>

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>paid Expense:</b></font></td>
                                                        <td id="paidExpenses"> <font color="white"><b> 
                                                                <c:out value="${trip.paidExpense}" />
                                                            </b></font>

                                                            <input type="hidden" name="paidExpense" value='<c:out value="${trip.paidExpense}" />'>

                                                        </td>
                                                    </tr>
                                                    <c:set var="profitMargin" value="" />
                                                    <c:set var="orderRevenue" value="${trip.orderRevenue}" />
                                                    <c:set var="orderExpense" value="${trip.orderExpense}" />
                                                    <c:set var="paidExpense" value="${trip.paidExpense}" />
                                                    <c:set var="profitMargin" value="${trip.orderRevenue - trip.orderExpense}" />
                                                    <%
                                                                String profitMarginStr = "" + (Double) pageContext.getAttribute("profitMargin");
                                                                revenueStr = "" + (String) pageContext.getAttribute("orderRevenue");
                                                                float profitPercentage = 0.00F;
                                                                if (!"".equals(revenueStr) && !"".equals(profitMarginStr)) {
                                                                    profitPercentage = Float.parseFloat(profitMarginStr) * 100 / Float.parseFloat(revenueStr);
                                                                }


                                                    %>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td> <font color="white"><b> <%=new DecimalFormat("#0.00").format(profitPercentage)%>(%)</b></font>
                                                            <input type="hidden" name="profitMargin" value='<c:out value="${profitMargin}" />'>
                                                        </td>

                                                    </tr>
                                                </table>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <br>


                    <br>
                    <br>
                    <br>
                    <br>
                    <div id="spinner" class="spinner" style="display:none;" >
                        <img id="img-spinner" src="images/page-loader2.gif" alt="Loading" />
                    </div>

                    <table width="100%">
                        <% int loopCntr = 0;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <% if (loopCntr == 0) {%>
                                <tr id="index" height="30">
                                    <td>Cust Ref No: <c:out value="${trip.customerOrderReferenceNo}" /></td>
                                    <td >Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                                    <td >Trip Code: <c:out value="${trip.tripCode}"/></td>
                                    <td >Customer:&nbsp;<c:out value="${trip.customerName}"/></td>
                                    <td >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                                    <td >Status: <c:out value="${trip.status}"/>
                                        <input type="hidden" name="vendorId" value='1' />
                                        <input type="hidden" name="movementType" id="movementType" value='<c:out value="${trip.movementType}"/>' />
                                        <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                        <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                        <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                                        <input type="hidden" name="tripType" id="tripType" value='<c:out value="${tripType}"/>' />
                                        <input type="hidden" name="moTypeName" id="moTypeName" value='<c:out value="${moTypeName}"/>' />
                                        <input type="hidden" name="statusId" id="statusId" value='<c:out value="${statusId}"/>' /></td>
                                </tr>
                                <tr id="index" height="30">
                                    <!--<td >Vessel : &emsp; <c:out value="${trip.linerName}"/></td>-->
                                    <td >Container Qty : &emsp;
                                        <c:if test = "${trip.containerQuantity == 1}" >
                                            1 * 40 
                                        </c:if> 
                                        <c:if test = "${trip.containerQuantity == 2}" >
                                            1 * 20 
                                        </c:if> 
                                        <c:if test = "${trip.containerQuantity == 3}" >
                                            2 * 20 
                                        </c:if> 
                                    </td>
                                    <td >Container No(s) : &emsp;<c:out value="${trip.containerNo}"/></td>
                                    <td>Movement Type  : &emsp;<c:out value="${moTypeName}"/></td>
                                    <td>Tally Billing Type  : &emsp;<c:out value="${tallyBillingType}"/></td>
                                <input type="hidden" name="vessel" id="vessel" value='<c:out value="${trip.linerName}"/>' />
                                <input type="hidden" name="contQty" value='<c:out value="${trip.containerQuantity}"/>' />
                                <input type="hidden" name="containerNos" value='<c:out value="${trip.containerNo}"/>' />
                                </tr>
                                <% }%>
                                <% loopCntr++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <div id="tabs" >
                        <ul class="nav nav-tabs"  id="tabDetails">

                            <!--<li class="active" data-toggle="tab" style="display:none;"><a href="#systemExpDetail"><span>System Expenses</span></a></li>-->
                            <li class="active" data-toggle="tab"><a href="#otherExpDetail"><span>Other Expenses</span></a></li>
                                <c:if test="${hireVehicleDriverDetailsSize <= 0}">
                                <li data-toggle="tab"><a href="#bunkDetail"><span>Fuel Details</span></a></li>
                                </c:if>



                            <c:if test="${containerDetailsSize > 0 && movementType != 7 }">
                                <li data-toggle="tab"><a href="#containerss"><span>Container Details</span></a></li>
                                </c:if>
                            <!--<li data-toggle="tab"><a href="#endDetail"><span>Edit Trip End Details</span></a></li>-->
                            <!--<li data-toggle="tab"><a href="#editStartDetail"><span>Edit Trip Start Details</span></a></li>-->
                            <!--<li data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>-->
                            <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s)/Route Course/Trip Plan</span></a></li>
                            <!--<li data-toggle="tab"><a href="#advance"><span>Advance</span></a></li>-->
                            <!--<li data-toggle="tab"><a href="#startDetail"><span >Trip Start Details</span></a></li>-->
                            <!--<li data-toggle="tab"><a href="#endDetails"><span>Trip End Details</span></a></li>-->
                            <%--<c:if test="${tripType == 1}">--%>
                            <!--<li data-toggle="tab"><a href="#podDetail"><span>Trip POD Details</span></a></li>-->
                            <%--</c:if>--%>
                            <%--<c:if test="${fuelTypeId == 1002}">--%>
                            <!--<li data-toggle="tab"><a href="#bpclDetail"><span>BPCL Transaction History</span></a></li>-->
                            <%--</c:if>--%>
                            <li data-toggle="tab"><a href="#statusDetail"><span>Status History</span></a></li>
                            <li data-toggle="tab"><a href="#closure"><span>Closure Summary</span></a></li>
                        </ul>

                        <input type="hidden" id="ownership" name="ownership" value="<c:out value="${vehicleOwnerShip}"/>">


                        <div id="systemExpDetail"  class="tab-pane active" style="display:none;">
                            <c:if test = "${gpsKm == 0}" >
                                <c:if test = "${odometerApprovalStatus == null}" >
                                    <div id="odoApprovalRequest" style="display:block;">

                                        <div  >Request Description</div>
                                        <br>
                                        <textarea name="odoUsageRemarks" style="width:600px;height:80px;">GPS data is un available. please approve trip closure based on odometer and reefer reading.</textarea>
                                        <br>
                                        <br>
                                        <input type="button" style="width:150px;" class="btn btn-success" onclick="saveOdoApprovalRequest('New');" value="request approval" />
                                    </div>
                                    <br>
                                    <div id="odoPendingApproval" style="display:none;"  >
                                        <br>
                                        request has been submitted.
                                        <br>
                                        <br>
                                        <b>odometer usage for expense calculation request is pending approval</b>
                                    </div>
                                </c:if>
                                <c:if test = "${odometerApprovalStatus == 0}" >
                                    <br>
                                    <br>
                                    <div  >Request Status: <b>request pending approval</b></div>
                                    <br>
                                </c:if>
                                <c:if test = "${odometerApprovalStatus == 2}" >
                                    <div id="odoReApprovalRequest" style="display:none;">
                                        <div  >Request Status : <b>request rejected</b></div>
                                        <br>
                                        <div  >Request Description</div>
                                        <br>
                                        <textarea name="odoUsageReApprovalRemarks"  style="width:600px;height:80px;"></textarea>
                                        <br>
                                        <br>
                                        <input type="button" style="width:150px;" class="button" onclick="saveOdoReApprovalRequest();" value="request re approval" />
                                    </div>
                                    <br>
                                    <div id="odoPendingReApproval" style="display:none;" >
                                        <br>
                                        request has been submitted
                                        <br>
                                        <br>
                                        <b>odometer usage for expense calculation re-request is pending approval</b>
                                    </div>
                                </c:if>
                            </c:if>

                            <%
                                        Float estimatedExpense = 0.00F;
                                        Float totalExpense = 0.00F;
                                        Float paidAdvance = 0.00F;
                                        Float dieselAmount = 0.00F;
                                        if (request.getAttribute("dieselCost") != null) {
                                            dieselAmount = Float.parseFloat((String) request.getAttribute("dieselCost"));
                                        }
                                        if (request.getAttribute("paidAdvance") != null) {
                                            paidAdvance = Float.parseFloat((String) request.getAttribute("paidAdvance"));
                                        }
                                        if (request.getAttribute("estimatedExpense") != null) {
                                            estimatedExpense = Float.parseFloat((String) request.getAttribute("estimatedExpense"));
                                        }
                                        String tripType = (String) request.getAttribute("tripType");
                                        Float secTollCost = 0.00F;


                                        Float secAddlTollCost = 0.00F;
                                        Float secMiscCost = 0.00F;
                                        Float secParkingAmount = 0.00F;
                                        if (request.getAttribute("secTollAmount") != null && !"".equals(request.getAttribute("secTollAmount"))) {
                                            secTollCost = Float.parseFloat((String) request.getAttribute("secTollAmount"));
                                            secAddlTollCost = Float.parseFloat((String) request.getAttribute("secAddlTollAmount"));
                                            secMiscCost = Float.parseFloat((String) request.getAttribute("secMiscAmount"));
                                            secParkingAmount = Float.parseFloat((String) request.getAttribute("secParkingAmount"));
                                        }
                                        if((String) request.getAttribute("tollAmount") == null){
                                            request.setAttribute("tollAmount", "0");
                                        }
                                        if((String) request.getAttribute("driverBatta") == null){
                                            request.setAttribute("driverBatta", "0");
                                        }
                                        if((String) request.getAttribute("driverIncentive") == null){
                                            request.setAttribute("driverIncentive", "0");
                                        }
                                        if((String) request.getAttribute("fuelPrice") == null){
                                            request.setAttribute("fuelPrice", "0");
                                        }
                                        if((String) request.getAttribute("milleage") == null){
                                            request.setAttribute("milleage", "0");
                                        }
                                        if((String) request.getAttribute("reeferConsumption") == null){
                                            request.setAttribute("reeferConsumption", "0");
                                        }
                                        if((String) request.getAttribute("runKm") == null){
                                            request.setAttribute("runKm", "0");
                                        }
                                        if((String) request.getAttribute("runHm") == null){
                                            request.setAttribute("runHm", "0");
                                        }
                                        if((String) request.getAttribute("totaldays") == null){
                                            request.setAttribute("totaldays", "0");
                                        }
                                        if((String) request.getAttribute("gpsKm") == null){
                                            request.setAttribute("gpsKm", "0");
                                        }
                                        if((String) request.getAttribute("gpsHm") == null){
                                            request.setAttribute("gpsHm", "0");
                                        }
                                        if((String) request.getAttribute("bookedExpense") == null){
                                            request.setAttribute("bookedExpense", "0");
                                        }
                                        if((String) request.getAttribute("miscValue") == null){
                                            request.setAttribute("miscValue", "0");
                                        }
                                        if((String) request.getAttribute("extraExpenseValue") == null){
                                            request.setAttribute("extraExpenseValue", "0");
                                        }
                                        if((String) request.getAttribute("fuelTypeId") == null){
                                            request.setAttribute("fuelTypeId", "0");
                                        }
                                       
                                        System.out.println("Toll ::"+(String) request.getAttribute("tollAmount"));
                                        System.out.println("driverBatta ::"+(String) request.getAttribute("driverBatta"));
                                        System.out.println("driverIncentive ::"+(String) request.getAttribute("driverIncentive"));
                                        System.out.println("fuelPrice ::"+(String) request.getAttribute("fuelPrice"));
                                        System.out.println("milleage ::"+(String) request.getAttribute("milleage"));
                                        System.out.println("reeferConsumption ::"+(String) request.getAttribute("reeferConsumption"));
                                        System.out.println("runKm ::"+(String) request.getAttribute("runKm"));
                                        
                                        Float tollRate = Float.parseFloat((String) request.getAttribute("tollAmount"));
                                        Float driverBattaPerDay = Float.parseFloat((String) request.getAttribute("driverBatta"));
                                        Float driverIncentivePerKm = Float.parseFloat((String) request.getAttribute("driverIncentive"));
                                        Float fuelPrice = Float.parseFloat((String) request.getAttribute("fuelPrice"));
                                        Float milleage = Float.parseFloat((String) request.getAttribute("milleage"));
                                        Float reeferMileage = Float.parseFloat((String) request.getAttribute("reeferConsumption"));

                                        Float runKm = Float.parseFloat((String) request.getAttribute("runKm"));
                                        Float runHm = Float.parseFloat((String) request.getAttribute("runHm"));

                                       /*
        */
                                        String totalDaysValue = (String) request.getAttribute("totaldays");
                                        int gpsKm = Integer.parseInt((String) request.getAttribute("gpsKm"));
                                        int gpsHm = Integer.parseInt((String) request.getAttribute("gpsHm"));
                                        Float totalDays;
                                        if(totalDaysValue != null){
                                        totalDays = Float.parseFloat((String) request.getAttribute("totaldays"));
                                        }else{
                                        totalDays = Float.parseFloat("0.00");
                                        }
                                        Float bookedExpense = Float.parseFloat((String) request.getAttribute("bookedExpense"));
                                        Float miscRate = Float.parseFloat((String) request.getAttribute("miscValue"));
                                        int driverCount = 0;
                                        if (request.getAttribute("driverCount") != null) {
                                            driverCount = Integer.parseInt((String) request.getAttribute("driverCount"));
                                        }
                                        //out.println("driverCount"+driverCount);
                                        Float extraExpenseValue = 0.00F;
                                        extraExpenseValue = Float.parseFloat((String) request.getAttribute("extraExpenseValue"));
                                        Float dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                                        out.println("runKm"+runKm);
                                        out.println("milleage"+milleage);
                                        out.println("runHm"+runHm);
                                        out.println("reeferMileage"+reeferMileage);
                                        out.println("dieselUsed:::"+dieselUsed);
                                        Float driverBatta = totalDays * driverBattaPerDay * driverCount;
                                        Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                                        Float dieselCost = dieselUsed * fuelPrice;
                                        Float tollCost = runKm * tollRate;
                                        Float driverIncentive = runKm * driverIncentivePerKm;
//                                        out.println("dieselUsed:::"+tollRate);

                                        Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                                        String fuelTypeId = (String) request.getAttribute("fuelTypeId");
                                        Float preColingAmount = 1.5f;
                                        Float fuelUsed = 0.00F;
                                        Float fuelCost = 0.00F;
                                        if ("2".equals(tripType)) {//secondary
                                            fuelUsed = (runKm / milleage);
                                            fuelCost = fuelUsed * fuelPrice;
                                            if (fuelTypeId.equals("1003")) {
                                                systemExpense = fuelCost + secTollCost + secAddlTollCost + secMiscCost + secParkingAmount + (fuelPrice * preColingAmount);
                                                preColingAmount = preColingAmount * fuelPrice;
                                            } else {
                                                systemExpense = fuelCost + secTollCost + secAddlTollCost + secMiscCost + secParkingAmount;
                                                preColingAmount = 0.0f;
                                            }
                                        }

                                       // Float nettExpense = systemExpense + bookedExpense;
                                        int tripCount = (Integer) request.getAttribute("tripCnt");
                                        Float totalPaidExpense = dieselAmount + paidAdvance;
                                        Float nettExpense = totalPaidExpense + bookedExpense;
                                        Float estActualExpDiff = estimatedExpense - nettExpense;
                            %>

                            <input type="hidden" name="vehicleId" value='<c:out value="${vehicleId}"/>' />
                            <input type="hidden" name="estimatedExpense" value='<%=new DecimalFormat("#0.00").format(estimatedExpense)%>' />
                            <input type="hidden" name="extraExpenseValue" value='<%=new DecimalFormat("#0.00").format(extraExpenseValue)%>' />
                            <input type="hidden" name="tollRate" value='<%=new DecimalFormat("#0.00").format(tollRate)%>' />

                            <input type="hidden" name="secTollCost" value='<%=new DecimalFormat("#0.00").format(secTollCost)%>' />
                            <input type="hidden" name="secAddlTollCost" value='<%=new DecimalFormat("#0.00").format(secAddlTollCost)%>' />
                            <input type="hidden" name="secMiscCost" value='<%=new DecimalFormat("#0.00").format(secMiscCost)%>' />
                            <input type="hidden" name="secParkingAmount" value='<%=new DecimalFormat("#0.00").format(secParkingAmount)%>' />

                            <input type="hidden" name="driverBattaPerDay" value='<%=new DecimalFormat("#0.00").format(driverBattaPerDay)%>' />
                            <input type="hidden" name="driverIncentivePerKm" value='<%=new DecimalFormat("#0.00").format(driverIncentivePerKm)%>' />
                            <input type="hidden" name="fuelPrice" value='<%=new DecimalFormat("#0.00").format(fuelPrice)%>' />
                            <input type="hidden" name="mileage" value='<%=new DecimalFormat("#0.00").format(milleage)%>' />
                            <input type="hidden" name="reeferMileage" value='<%=new DecimalFormat("#0.00").format(reeferMileage)%>' />
                            <input type="hidden" name="runKm" value='<%=new DecimalFormat("#0.00").format(runKm)%>' />
                            <input type="hidden" name="runHm" value='<%=new DecimalFormat("#0.00").format(runHm)%>' />
                            <input type="hidden" name="totalDays" value='<%=new DecimalFormat("#0.00").format(totalDays)%>' />

                            <input type="hidden" name="dieselUsed" value='<%=new DecimalFormat("#0.00").format(dieselUsed)%>' />
                            <input type="hidden" name="dieselCost" value='<%=new DecimalFormat("#0.00").format(dieselCost)%>' />
                            <input type="hidden" name="tollCost" value='<%=new DecimalFormat("#0.00").format(tollCost)%>' />
                            <input type="hidden" name="driverIncentive" value='<%=new DecimalFormat("#0.00").format(driverIncentive)%>' />
                            <input type="hidden" name="driverBatta" value='<%=new DecimalFormat("#0.00").format(driverBatta)%>' />
                            <input type="hidden" name="miscRate" value='<%=new DecimalFormat("#0.00").format(miscRate)%>' />
                            <input type="hidden" name="miscValue" value='<%=new DecimalFormat("#0.00").format(miscValue)%>' />
                            <input type="hidden" name="bookedExpense" value='<%=new DecimalFormat("#0.00").format(bookedExpense)%>' />
                            <input type="hidden" name="preColingAmount" value='<%=new DecimalFormat("#0.00").format(preColingAmount)%>' />
                            <input type="hidden" name="nettExpense" value='<%=new DecimalFormat("#0.00").format(nettExpense)%>' />
                            <input type="hidden" name="paidAdvance" id="paidAdvance" value='<%=new DecimalFormat("#0.00").format(paidAdvance)%>' />
                            <input type="hidden" name="dieselAmount" value='<%=new DecimalFormat("#0.00").format(dieselAmount)%>' />

                            <input type="hidden" name="tripType" value='<c:out value="${tripType}" />' />
                            <input type="hidden" name="gpsKm" value='<c:out value="${gpsKm}" />' />
                            <input type="hidden" name="gpsHm" value='<c:out value="${gpsHm}" />' />


                            <c:if test = "${tripType == 1 }" >
                                <c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1))  }" >
                                    <br>
                                    <br>
                                    <div id="expenseDiv" style="display:none ">
                                        <table  border="1" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                            <tr>
                                                <td   >Diesel Used</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(dieselUsed)%></td>
                                            </tr>
                                            <tr>
                                                <td  >Diesel Rate / Ltr (INR)</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(fuelPrice)%></td>
                                            </tr>
                                            <tr>
                                                <td  >Diesel Cost (1)</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(dieselCost)%></td>
                                            </tr>
                                            <tr>
                                                <td  >Toll Cost (2)</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(tollCost)%></td>
                                            </tr>
                                            <tr>
                                                <td  >Driver Incentive (3)</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(driverIncentive)%></td>
                                            </tr>
                                            <tr>
                                                <td  >Driver Bhatta (4)</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(driverBatta)%></td>
                                            </tr>
                                            <tr>
                                                <td  >MIS (@kmrun*<c:out value="${miscValue}"/>*5%) (5)</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(miscValue)%></td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" >&nbsp;</td>
                                            </tr>
                                            <tr></tr>
                                            <tr>
                                                <td  >Total Expense (System Computed)</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(systemExpense)%></td>
                                            <input type="text" name="systemExpense"  id="systemExpense" value='<%=new DecimalFormat("#0.00").format(systemExpense)%>' />
                                            </tr>
                                        </table>
                                    </div>

                                    <div id="hiredDiv" style="display: none;">
                                        <table class="table table-info mb30 table-hover" id="bg">
                                            <tr>
                                                <td   >Hired Vehicle Charges</td>
                                                <td   align="right" ><c:out value="${orderExpense}" /></td>
                                            </tr>

                                        </table>
                                        <input type="hidden" name="hiredCharges" id="hiredCharges" value="<c:out value="${orderExpense}" />">
                                    </div>
                                    <div id="paidExpense" style="display:none; margin:1%;" style="display:none;" >
                                        <table border="0" style=" outline: 3px solid #5BC0DE;border-color:#5BC0DE;display:none; "  align="left" width="30%" cellpadding="0" cellspacing="0" id="bg" >
                                            <tr >
                                                <td   colspan="3" align="center">Trip Expense Summary</td>

                                            </tr>
                                            <tr >
                                                <td   >Diesel Cost</td>
                                                <td  ><b>:</b></td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(dieselAmount)%></td>
                                            </tr>
                                            <tr>
                                                <td   >Advance  Paid</td>
                                                <td   ><b>:</b></td>
                                                <td  align="right" ><%=new DecimalFormat("#0.00").format(paidAdvance)%></td>
                                            </tr>
                                            <tr>
                                                <td  >Total  Paid</td>
                                                <td  ><b>:</b></td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(totalPaidExpense)%></td>
                                            </tr>

                                        </table>
                                    </div>
                                </c:if>

                            </c:if>
                            <script>
                                var checkVehicleType = document.getElementById("ownership").value;
//                                alert("checkVehicleType="+checkVehicleType);
                                if (parseInt(checkVehicleType) == 3) {
                                    document.getElementById("expenseDiv").style.display = 'none';
                                    document.getElementById("hiredDiv").style.display = 'block';
                                    document.getElementById("bunkDetail").style.display = 'none';
                                } else {
                                    document.getElementById("bunkDetail").style.display = 'block';
                                    document.getElementById("paidExpense").style.display = 'block';
                                    document.getElementById("expenseDiv").style.display = 'none';
                                    document.getElementById("hiredDiv").style.display = 'none';
                                }
                            </script>

                            <c:if test = "${tripType == 2 }" >
                                <c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1)) }" >
                                    <br>
                                    <br>
                                    <table  border="1" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                        <tr>
                                            <td   >Fuel Used</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(fuelUsed)%></td>
                                        </tr>
                                        <tr>
                                            <td  >Fuel Rate / Kg (INR)</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(fuelPrice)%></td>
                                        </tr>
                                        <c:if test="${fuelTypeId == 1003}">
                                            <tr>
                                                <td  >Pre - Cooling CNG</td>
                                                <td   align="right" ><%=new DecimalFormat("#0.00").format(preColingAmount)%></td>
                                            </tr>
                                        </c:if>
                                        <tr>
                                            <td  >Fuel Cost (1)</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(fuelCost)%></td>
                                        </tr>
                                        <input type="hidden" name="fuelUsed" value='<%=new DecimalFormat("#0.00").format(fuelUsed)%>' />
                                        <input type="hidden" name="fuelPrice" value='<%=new DecimalFormat("#0.00").format(fuelPrice)%>' />
                                        <input type="hidden" name="fuelCost" value='<%=new DecimalFormat("#0.00").format(fuelCost)%>' />
                                        <tr>
                                            <td  >Toll Cost (2)</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(secTollCost)%></td>
                                        </tr>
                                        <tr>
                                            <td  >Addl Toll Cost (3)</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(secAddlTollCost)%></td>
                                        </tr>
                                        <tr>
                                            <td  >Misc Cost (4)</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(secMiscCost)%></td>
                                        </tr>
                                        <tr>
                                            <td  >Parking Cost (5)</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(secParkingAmount)%></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" >&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td  >Total Expense (System Computed)</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(systemExpense)%></td>
                                        <input type="hidden" name="systemExpense"  id="systemExpense" value='<%=new DecimalFormat("#0.00").format(systemExpense)%>' />
                                        </tr>
                                    </table>
                                </c:if>
                            </c:if>
                            <br>
                            <br>
                            <br>

                            <c:if test = "${approveProcessHistory != null}" >
                                <br>
                                <br><br>
                                <br>
                                <table class="table table-info mb30 table-hover" style="display:none;" >
                                    <tr>
                                        <td  colspan="9" ><center style="color:black;font-size: 14px;">
                                        Approve Process History
                                    </center> </td>
                                    </tr>
                                    <tr id="index" height="30">
                                        <td  height="30" >Request By</td>
                                        <td  height="30" >Request On</td>
                                        <td  height="30" >Request Remarks</td>
                                        <td  height="30" >Run KM</td>
                                        <td  height="30" >Run HM</td>
                                        <td  height="30" >Status</td>
                                        <td  height="30" >Approver Remarks</td>
                                        <td  height="30" >Approver</td>
                                        <td  height="30" >Approver Processed On</td>
                                    </tr>
                                    <c:forEach items="${approveProcessHistory}" var="approveHistory">
                                        <c:if test = "${approveHistory.requestType == 1}" >
                                            <tr >
                                                <td  ><c:out value="${approveHistory.approvalRequestBy}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvalRequestOn}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvalRequestRemarks}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.runKm}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.runHm}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvalStatus}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvalRemarks}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvedBy}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvedOn}" />&nbsp;</td>

                                            </tr>
                                        </c:if>

                                    </c:forEach >
                                </table>
                            </c:if>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:90px;height:35px;font-weight: bold;padding: 2px;"/></a>
                            </center>
                            <br>

                        </div>
                        <div id="closure" style="display:none;">
                            <br>
                            <%--<c:if test = "${(gpsKm != 0) || ((gpsKm == 0) && (odometerApprovalStatus == 1)) }" >--%>
                            <div id="closureDiv" style="display:block;">
                                <c:if test="${vehicleId == 0}">
                                    <table  border="1" style=" outline: 3px solid #5BC0DE;display:none;" class="table table-info mb30 table-hover" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                    </c:if>
                                    <c:if test="${vehicleId != 0}">
                                        <!--                                        <table  border="1" style=" outline: 3px solid #5BC0DE;" class="table table-info mb30 table-hover" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                    </c:if>
                                    <tr>
                                        <td  >RCM Cost</td>
                                        <td   align="right" ><%=new DecimalFormat("#0.00").format(estimatedExpense)%></td>
                                    </tr>
                                    <c:if test="${tripType == 1}">
                                        <tr>
                                            <td  >System Expense + Booked Expense</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${tripType == 2 && fuelTypeId == 1002}">
                                        <tr>
                                            <td  >System Expense + Booked Expense</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                        </tr>
                                    </c:if>
                                    <c:if test="${tripType == 2 && fuelTypeId == 1003}">
                                        <tr>
                                            <td  >Pre - Cooling CNG</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(preColingAmount)%></td>
                                        </tr>
                                        <tr>
                                            <td  >System Expense + Booked Expense + Precooling Price</td>
                                            <td   align="right" ><%=new DecimalFormat("#0.00").format(nettExpense)%></td>
                                        </tr>
                                    </c:if>

                                    <tr>
                                        <td  >Deviation</td>
                                        <td   align="right" >
                                    <%
                                                Float deviation = estimatedExpense - (nettExpense);
                                                Float deviationPercentage = (deviation / estimatedExpense) * 100;
                                                if (deviation > 0) {
                                    %>
                                    <font color="green">
                                    <%=new DecimalFormat("#0.00").format(deviation)%>
                                    <br>
                                    <%=new DecimalFormat("#0.00").format(deviationPercentage)%> &nbsp;%
                                    </font>
                                    <%} else {%>
                                    <font color="red">
                                    <%=new DecimalFormat("#0.00").format(deviation)%>
                                    <br>
                                    <%=new DecimalFormat("#0.00").format(deviationPercentage)%> &nbsp;%
                                    </font>
                                    <%}%>
                                </td>
                            </tr>
                            <tr>
                                <td  >Profit Margin(%)</td>
                                <td   align="right" >
                                    <%
                                                Float profitValue = Float.parseFloat(revenueStr) - (nettExpense);
                                                Float profitMargin = (profitValue / Float.parseFloat(revenueStr)) * 100;
                                                if (profitValue > 0) {
                                    %>
                                    <font color="green"><%=new DecimalFormat("#0.00").format(profitMargin)%> </font>
                                    <%} else {%>
                                    <font color="red"><%=new DecimalFormat("#0.00").format(profitMargin)%> </font>
                                    <%}%>
                                </td>
                            </tr>


                        </table>-->
                                    
                                    <table class="table table-info mb30 table-hover">
                                        <thead>
                                            <tr>
                                                <th  colspan="4" >Invoice Details</th>
                                            </tr>
                                        </thead>
                                        <tr>
                                            <td ><font color="red">*</font> E-Way Bill No</td>
                                            <td >
                                                <input type="text" name="eWayBillNo" id="eWayBillNo" class="form-control"  style="width:250px;height:40px" /></td>
                                            <td ><font color="red">*</font>  E-Way Bill Expiry</td>
                                            <td >
                                                <input type="text" readonly name="expiryDate" readonly id="expiryDate" class="datepicker" value="" style="width:250px;height:40px"/>
                                                HH:<select name="expiryHour" id="expiryHour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                MI:<select name="expiryMin" id="expiryMin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td ><font color="red">*</font> Invoice Value</td>
                                            <td >
                                                <input type="text" name="invoiceValue" id="invoiceValue" class="form-control" style="width:250px;height:40px" value="0"   onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                            </td>
                                            <td ><font color="red">*</font> Invoice Date</td>
                                            <td >
                                                <input type="text" readonly name="invoiceDate" readonly id="invoiceDate" class="datepicker" value="" style="width:250px;height:40px"/>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td ><font color="red">*</font> Invoice No</td>
                                            <td ><input type="text" name="invoiceNo" autocomplete="off" id="invoiceNo"  class="form-control" style="width:250px;height:40px" /></td>
                                            <td ><font color="red">*</font> Invoice Weight(Tonnage)</td>
                                            <td ><input type="text" name="invoiceWeight" id="invoiceWeight" class="form-control" style="width:250px;height:40px"  value="0"   onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                        </tr>
                                    </table>         
                                    <c:if test="${hireVehicleDriverDetailsSize > 0}">
                                        <table class="table table-info mb30 table-hover" style="width:100%;" id="containerTBL1" >
                                            <thead>
                                                <tr>
                                                    <th class="contenthead">Sno</th>
                                                    <th class="contenthead">Vehicle No</th>
                                                    <th class="contenthead">Driver Name</th>
                                                    <th class="contenthead">Driver Mobile No</th>
                                                    <th class="contenthead">LHC Rate</th>
                                                    <th class="contenthead">Transporter</th>
                                                    <th class="contenthead">Action</th>

                                                </tr>
                                            </thead>
                                            <% int ind = 0;
                                               int sn = 1;
                                            %>
                                            <c:forEach items="${hireVehicleDriverDetails}" var="no">
                                                <tr>
                                                    <td  ><%=sn%>

                                                    </td>
                                                    <td  >
                                                        <input type="hidden" name="hirevehicleId" id="hirevehicleId" value="<c:out value="${no.vehicleId}"/>"/>

                                                        <input type="text" class="form-control" style="width:150px;"  maxlength="11" name="hireVehicleNo" id="hireVehicleNo"  value="<c:out value="${no.regNo}"/>" />
                                                    </td>
                                                    <td >

                                                        <input type="text" class="form-control" style="width:150px;" name="hireDriverName" id="hireDriverName"  value="<c:out value="${no.driverName}"/>"/>
                                                        <input type="hidden" name="hireDriverId" id="hireDriverId" value="<c:out value="${no.driverId}"/>"/>
                                                    </td>
                                                    <td  >

                                                        <input type="text" class="form-control" style="width:200px;"  onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12" name="driverMobile" id="driverMobile" value="<c:out value="${no.driverMobile}"/>"/>

                                                    </td>
                                                    <td  >

                                                        <input type="text" class="form-control" style="width:200px;"  onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12"  onchange="autoCheck()" name="currRate" id="currRate" value="<c:out value="${no.currRate}"/>"/>

                                                    </td>
                                                    <td><c:out value="${no.vendorName}"/></td>
                                                <input type="hidden"   id="transportName" name="transportName" value="<c:out value="${no.vendorName}"/>" />
                                                <script>
                                                    $("#transporterName").text(document.getElementById("transportName").value);
                                                </script>    
                                                <td><input type="hidden" name="lhcRateEdit" id="lhcRateEdit" value="0"  />
                                                    <input type="checkbox" name="selectedIndex" id="selectedIndex" value="0"  onchange="checkSelectedItem()" />
                                                </td>
                                                <script>
                                                    function autoCheck() {
                                                        //                                                alert("Close Trip")
                                                        document.getElementById("selectedIndex").checked = true;
                                                        document.getElementById("lhcRateEdit").value = '1';
                                                    }
                                                    function checkSelectedItem() {
                                                        //                                                alert("Close Trip")
                                                        if (document.getElementById("selectedIndex").checked == true) {
                                                            document.getElementById("lhcRateEdit").value = '1';
                                                        } else {
                                                            document.getElementById("lhcRateEdit").value = '0';
                                                        }
                                                    }
                                                </script>
                                                </tr>
                                                <%ind++;%>
                                                <%sn++;%>
                                            </c:forEach >

                                        </table>
                                        <!--                                <br>
                                                                        <center>
                                                                            <input type="button" class="btn btn-success" name="SaveDetails" value="SaveDetails" onclick="saveHireVehicle();" style="width:100px;height:35px;font-weight: bold;"/>                              
                                                                            &nbsp;
                                                                            &nbsp;                                
                                                                            <a><input type="button" class="btn btn-success btnNext" value="Next" text="Next" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                                                        </center>-->



                                    </c:if>
                            </div>
                            <div id="closureHired" style="display: none;">
                                <table  border="1" style=" outline: 3px solid #5BC0DE;"  class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr>
                                        <td  >RCM Cost</td>
                                        <td   align="right" ><%=new DecimalFormat("#0.00").format(estimatedExpense)%></td>
                                    </tr>
                                    <c:if test="${tripType == 1}">
                                        <tr>
                                            <td  >System Expense + Booked Expense</td>
                                            <td   align="right" >
                                                <%
                                               if((String) request.getAttribute("estimatedExpense") == null){
                                            request.setAttribute("estimatedExpense", "0");
                                        }
                                               if((String) request.getAttribute("bookedExpense") == null){
                                            request.setAttribute("bookedExpense", "0");
                                        }
                                               Float exp1= Float.parseFloat((String) request.getAttribute("estimatedExpense"));
                                               Float book1=Float.parseFloat((String) request.getAttribute("bookedExpense"));
                                               totalExpense=exp1+book1;
                                                %>
                                                <%=new DecimalFormat("#0.00").format(totalExpense)%>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td  >Deviation</td>
                                        <td   align="right" >
                                            <%
                                                        Float deviation1 = estimatedExpense - (totalExpense);
                                                        Float deviationPercentage1 = (deviation1 / estimatedExpense) * 100;
                                                        if (deviation > 0) {
                                            %>
                                            <font color="green">
                                            <%=new DecimalFormat("#0.00").format(deviation1)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentage1)%> &nbsp;%
                                            </font>
                                            <%} else {%>
                                            <font color="red">
                                            <%=new DecimalFormat("#0.00").format(deviation1)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentage1)%> &nbsp;%
                                            </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  >Profit Margin(%)</td>
                                        <td   align="right" >
                                            <%
                                                        Float profitValue1 = Float.parseFloat(revenueStr) - (totalExpense);
                                                        Float profitMargin1 = (profitValue1 / Float.parseFloat(revenueStr)) * 100;
                                                        if (profitValue > 0) {
                                            %>
                                            <font color="green"><%=new DecimalFormat("#0.00").format(profitMargin1)%> </font>
                                            <%} else {%>
                                            <font color="red"><%=new DecimalFormat("#0.00").format(profitMargin1)%> </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div id="closureNew" style="display: none;">
                                <table  border="1" style=" outline: 3px solid #5BC0DE;" class="border" align="left" width="30%" cellpadding="0" cellspacing="0" id="bg">
                                    <tr>
                                        <td  >RCM Cost</td>
                                        <td   align="right" ><%=new DecimalFormat("#0.00").format(estimatedExpense)%></td>
                                    </tr>
                                    <c:if test="${tripType == 1}">
                                        <tr>
                                            <td  >System Expense + Booked Expense</td>
                                            <td   align="right" >
                                                <%

                                               Float exp2= totalPaidExpense;
                                               Float book2=Float.parseFloat((String) request.getAttribute("bookedExpense"));
                                               totalExpense=exp2+book2;
                                                %>
                                                <%=new DecimalFormat("#0.00").format(totalExpense)%>
                                            </td>
                                        </tr>
                                    </c:if>
                                    <tr>
                                        <td  >Deviation</td>
                                        <td   align="right" >
                                            <%
                                                        Float deviationNew = estimatedExpense - (totalExpense);
                                                        Float deviationPercentageNew = (deviationNew / estimatedExpense) * 100;
                                                        if (deviationNew > 0) {
                                            %>
                                            <font color="green">
                                            <%=new DecimalFormat("#0.00").format(deviationNew)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentageNew)%> &nbsp;%
                                            </font>
                                            <%} else {%>
                                            <font color="red">
                                            <%=new DecimalFormat("#0.00").format(deviationNew)%>
                                            <br>
                                            <%=new DecimalFormat("#0.00").format(deviationPercentageNew)%> &nbsp;%
                                            </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td  >Profit Margin(%)</td>
                                        <td   align="right" >
                                            <%
                                                        Float profitValueNew = Float.parseFloat(revenueStr) - (totalExpense);
                                                        Float profitMarginNew = (profitValueNew / Float.parseFloat(revenueStr)) * 100;
                                                        if (profitValueNew > 0) {
                                            %>
                                            <font color="green"><%=new DecimalFormat("#0.00").format(profitMarginNew)%> </font>
                                            <%} else {%>
                                            <font color="red"><%=new DecimalFormat("#0.00").format(profitMarginNew)%> </font>
                                            <%}%>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <script>
                                var checkVehicleType1 = document.getElementById("ownership").value;
                                if (parseInt(checkVehicleType1) == 3) {
                                    document.getElementById("bunkDetail").style.display = 'none';
                                    document.getElementById("closureDiv").style.display = 'none';
                                    document.getElementById("closureHired").style.display = 'block';
                                } else {
                                    document.getElementById("bunkDetail").style.display = 'block';
                                    document.getElementById("closureDiv").style.display = 'none';
                                    document.getElementById("closureHired").style.display = 'none';
                                    document.getElementById("closureNew").style.display = 'block';
                                }
                            </script>

                            <br>

                            <%
                                        if (estActualExpDiff > 0) {
                            %>
                            <!--                            <cenetr>
                                                            <input type="button" class="btn btn-success" name="Save" style="width:80px;height:30px;padding:1px;font-weight: bold" value="close trip" onclick="closeTrip();" />
                                                            </center>-->
                            <%} else {%>

                            <c:if test = "${expenseDeviationApprovalStatus == null}" >
                                <div id="expDeviationApprovalRequest" style="display:none;">
                                    <div align="left"  >Request Description</div>
                                    <br><textarea name="expDeviationRemarks" style="width:600px;height:80px;"></textarea>
                                    <input type="button" style="width:130px;" class="btn btn-success"  onclick="saveExpDeviationApprovalRequest('New');" value="request approval" />
                                </div>
                                <br>
                                <div id="expDeviationPendingApproval" style="display:none;" >
                                    <br>
                                    request has been submitted
                                    <b>expense deviation request is pending approval</b>
                                </div>
                            </c:if>

                            <c:if test = "${expenseDeviationApprovalStatus == 0}" >
                                <div  >Request Status: <b>request pending approval</b></div>
                            </c:if>
                            <c:if test = "${expenseDeviationApprovalStatus == 1}" >
                                <br>
                                <font color="red"> <div align="center" id="validateMsgHire"></div></font>
                                <br>
                                <center>
                                    <input type="button" class="btn btn-success" value="close trip" onclick="closeTrip();" style="width:80px;height:30px;padding:1px;font-weight: bold"/>
                                </center>
                            </c:if>
                            <c:if test = "${expenseDeviationApprovalStatus == 2}" >
                                <div id="expDeviationReApprovalRequest" style="display:block;">
                                    <div  >Request Status : <b>request rejected</b></div>
                                    <br>
                                    <div  >Request Description</div>
                                    <br><textarea name="expDeviationReApprovalRemarks"   style="width:600px;height:80px;"></textarea>
                                    <br>
                                    <br>
                                    <input type="button" style="width:150px;" class="button"  onclick="saveexpDeviationReApprovalRequest();" value="request re approval" />
                                </div>
                                <br>
                                <div id="expDeviationPendingReApproval" style="display:none;"  >
                                    <br>
                                    request has been submitted
                                    <br>
                                    <br>
                                    <b>expense deviation re-request is pending approval</b>
                                </div>
                            </c:if>



                            <%}%>

                            <br>
                            <br>
                            <c:if test = "${approveProcessHistory != null}" >
                                <table class="table table-info mb30 table-hover" style="display:none;" >
                                    <tr>
                                        <td  colspan="9" ><center style="color:black;font-size: 14px;">
                                        Approve Process History
                                    </center> </td>
                                    </tr>
                                    <tr id="index" height="30">
                                        <td  height="30" >Request By</td>
                                        <td  height="30" >Request On</td>
                                        <td  height="30" >Request Remarks</td>
                                        <td  height="30" >RCM Expense</td>
                                        <td  height="30" >Actual Expense</td>
                                        <td  height="30" >Status</td>
                                        <td  height="30" >Approver Remarks</td>
                                        <td  height="30" >Approver</td>
                                        <td height="30" >Approver Processed On</td>
                                    </tr>
                                    <c:forEach items="${approveProcessHistory}" var="approveHistory">
                                        <c:if test = "${approveHistory.requestType == 2}" >
                                            <tr >
                                                <td  ><c:out value="${approveHistory.approvalRequestBy}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvalRequestOn}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvalRequestRemarks}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.rcmExpense}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.nettExpense}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvalStatus}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvalRemarks}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvedBy}" />&nbsp;</td>
                                                <td  ><c:out value="${approveHistory.approvedOn}" />&nbsp;</td>

                                            </tr>
                                        </c:if>

                                    </c:forEach >
                                </table>
                            </c:if>
                            <%--</c:if>
                            <c:if test = "${((gpsKm == 0) && (odometerApprovalStatus != 1)) }" >
                                <br>
                                <b>please pass through km usage process</b>

                            </c:if>
                            --%>
                            <br>
                            <font color="red"> <div align="center" id="validateMsg"></div></font>
                            <br>
                            <center>
                                <a>
                                    <input type="button" class="btn btn-success" name="Save" style="width:100px;height:40px;" value="close trip" onclick="closeTrip();" />
                                </a>
                            </center>

                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>
                            <br>
                            <br>
                        </div>

                        <c:if test="${containerDetailsSize > 0 && movementType != 7 }">
                            <div id="containerss" class="tab-pane">

                                <% int index10 = 0; int containerCount = 0; %>

                                <c:if test = "${containerDetails != null}" >

                                    <table  class="table table-info mb30 table-hover" style="width:100%" >
                                        <!--<table border="0endDetail" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                                        <thead>
                                            <tr id="index" >
                                                <th   >S No</th>
                                                <th   >Container Type</th>
                                                <th   >Container No</th>
                                                <th   >Seal No</th>
                                                <th   >Wgt(Tonnage)</th>
                                                <th   >Goods Desc</th>
                                                <th   >CustRefNo</th>
                                                <th   >Action</th>
                                            </tr>
                                        </thead>
                                        <c:forEach items="${containerDetails}" var="container">
                                            <%
                                                   containerCount++;
                                                   String classText = "";
                                                   int oddEven1 = index10 % 2;
                                                   if (oddEven1 > 0) {
                                                       classText = "text1";
                                                   } else {
                                                       classText = "text2";
                                                   }
                                            %>
                                            <tr >
                                                <td  > &emsp; &emsp;<%=index10+1%></td>
                                                <td  > <input type="hidden" name="tripConId" id="tripConId<%=index10%>" value="<c:out value="${container.tripContainerId}" />">&emsp; &emsp;<c:out value="${container.containerName}" /></td>
                                                <td  >
                                                    <input type="text" name="containerNo" id="containerNo<%=index10%>" maxlength='11' class="form-control" value='<c:out value="${container.containerNo}" />' onKeyPress="return onKeyPressBlockCharacters1(<%=index10%>, event);" onChange="setContainerSelect(<%=index10%>)"/>
                                                </td>
                                                <td  >
                                                    <input type="text" name="sealNo" id="sealNo<%=index10%>" class="form-control" style="width:160px"  value='<c:out value="${container.sealNo}" />' onChange="setContainerSelect(<%=index10%>)"/>
                                                </td>
                                                <td  >
                                                    <input type="text" name="tonnage" id="tonnage<%=index10%>" class="form-control" style="width:160px" value='<c:out value="${container.tonnage}" />' onChange="setContainerSelect(<%=index10%>)" readonly/>
                                                </td>
                                                <td  > &emsp; <c:out value="${container.description}" /></td>
                                                <td  >
                                                    <input type="text" name="custRefNo" id="custRefNo<%=index10%>" class="form-control" style="width:160px" value='<c:out value="${container.customerOrderReferenceNo}" />' onChange="setContainerSelect(<%=index10%>)"/>
                                                </td>
                                                <td>
                                                    <input type="checkbox" name="selectedIndexCon"  id="selectedIndexCon<%=index10%>" value="<%=index10%>"/>
                                                </td>
                                            </tr>
                                            <%index10++;%>
                                        </c:forEach>
                                    </table>
                                </c:if>
                                <center><input type="button" class="btn btn-success" name="add" value="SaveContainer" style="width:110px;height:25px;padding:2px; " onclick="saveContainer();" />
                                </center>
                                <br/>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                            </div>
                            <input type="hidden" id="containerSelection" name="containerSelection" value="0"/>
                        </c:if>



                        <c:set var="totalConsumedFuel" value="${0}"/>    
                        <c:if test="${tripDieselDetails != null}">
                            <c:forEach items="${tripDieselDetails}" var="tripDiesel">
                                <c:set var="totalConsumedFuel" value="${totalConsumedFuel + tripDiesel.dieselUsed}"  />
                            </c:forEach>
                        </c:if> 
                        <c:if test="${hireVehicleDriverDetailsSize <= 0}">
                            <div id="bunkDetail" style="display:block;">
                            </c:if>
                            <c:if test="${hireVehicleDriverDetailsSize > 0}">
                                <div id="bunkDetail" style="display:none;">
                                </c:if>


                                <table class="table table-info mb30 table-hover" align="center" style="width:50%">
                                    <td>Total KM : <label id="totKM"><c:out value="${totalKM}"/></label></td>
                                    <td>Total Tonnage : <c:out value="${tonnage}" />
                                    </td>
                                    <td>Mileage : <c:out value="${mileage}" />
                                        <input type="hidden" id="mileageVal" name="mileageVal" value="<c:out value="${mileage}" />"></td>
                                </table>

                                <center>
                                    <font color="red"><b>Total Fuel Used : <c:out value="${totalConsumedFuel}"/> </b></font>
                                    &emsp;<font color="green"><b>System Allocatd Fuel : <c:out value="${allocatedFuel}" /> </b></font>
                                </center>

                                <table class="table table-info mb30 table-hover" width="100%" id="fuelTBL" name="fuelTBL" >
                                    <thead style="background-color:#5BC0DE;width:100%;height:30px;color:white;text-align: left;font-size: 13px;"> 
                                        <tr height="30px">
                                            <th>S No</th>
                                            <th>Fueling Type</th>
                                            <th>Bunk Name</th>
                                            <th>Slip No</th>
                                            <th>Date</th>
                                            <th>Ltrs</th>
                                            <th>Price</th>
                                            <th>Amount</th>
                                            <th>Person</th>
                                            <th>Remarks<font color="red"></font></th>
                                        </tr>
                                    </thead>   
                                    <% int indexss = 1; %>
                                    <% int indexs = 1; %>
                                    <c:if test="${tripDieselDetails != null}">
                                        <c:forEach items="${tripDieselDetails}" var="tripDiesel">
                                            <tr height="30px">
                                                <td><%=indexss++%></td>   
                                                <td>
                                                    <input type="hidden" class="form-control" id="tripAdvanceId<%=indexs%>" name="tripAdvanceId" value="<c:out value="${tripDiesel.tripAdvanceId}"/>" style="width:120px;height:40px;"/>
                                                    <input type="hidden" class="form-control" id="fuelModeE<%=indexs%>" name="fuelModeE" value="<c:out value="${tripDiesel.fuelingMode}"/>" style="width:120px;height:40px;"/>
                                                    <input type="hidden" class="form-control" id="tmpBunkIdE<%=indexs%>" name="tmpBunkIdE" value="<c:out value="${tripDiesel.bunkId}"/>" style="width:120px;height:40px;"/>
                                                    <input type="hidden" class="form-control" id="tmpFuelPriceE<%=indexs%>" name="tmpFuelPriceE" value="<c:out value="${tripDiesel.fuelPrice}"/>" style="width:120px;height:40px;"/>
                                                    <input type="hidden" class="form-control" id="fuelPriceRmrk<%=indexs%>" name="fuelPriceRmrk" value="<c:out value="${tripDiesel.fuelPriceRmrk}"/>" style="width:120px;height:40px;"/>
                                                    <c:if test="${tripDiesel.fuelingMode == '0' || tripDiesel.fuelingMode == '1'}">
                                                        Standard Fueling
                                                    </c:if>
                                                    <c:if test="${tripDiesel.fuelingMode == '2'}">
                                                        EnRoute Fueling
                                                    </c:if>

                                                </td>
                                                <c:if test="${tripDiesel.fuelingMode == '0' || tripDiesel.fuelingMode == '1'}">
                                                  <td><select class="form-control" name="bunkIdE" id="bunkIdE<%=indexs%>" style="width:200px;height:40px;" onChange="setFuelPriceList1(<%=indexs%>)">
                                                          <c:if test="${bunkList != null}" >
                                                            <%--<c:if test="${tripDiesel.bunkId != '0'}">--%>
                                                                <c:forEach items="${bunkList}" var="details">
                                                                    <option  value='<c:out value="${details.locationId}" />~<c:out value="${details.fuelPrice}" />'><c:out value="${details.bunkName}" /></option>
                                                               </c:forEach >
                                                            <%--</c:if>--%>
                                                            </c:if>

                                                    </select>
                                                         <script>
                                                        $("#bunkIdE" +<%=indexs%>).val('<c:out value="${tripDiesel.bunkId}" />' + '~' + '<c:out value="${tripDiesel.fuelPrice}" />');
                                                        </script>
                                                 </td>
                                                  </c:if>
                                                 <c:if test="${tripDiesel.fuelingMode == '2'}">
                                                  <td><select class="form-control" name="bunkIdE" id="bunkIdE<%=indexs%>" style="width:200px;height:40px;" onChange="setFuelPriceList1(<%=indexs%>)">
                                                         <c:if test="${tripDiesel.bunkId == '21'}">
                                                         <option  value='21~0.00'>Cash</option>
                                                         </c:if>
                                                         <c:if test="${tripDiesel.bunkId != '21'}">
                                                         <option  value='20~0.00' selected>IOCL Fleet Card</option>
                                                         <option  value='21~0.00'>Cash</option>
                                                         </c:if>
                                                    </select>
                                                     <script>
                                                       $("#bunkIdE" +<%=indexs%>).val('<c:out value="${tripDiesel.bunkId}" />' + '~' + '<c:out value="${tripDiesel.fuelPrice}" />');
                                                    </script>
                                                  </td>
                                                 </c:if>
                                                        
                                                <td><input type="hidden" class="form-control" id="uniqueIdE<%=indexs%>" name="uniqueIdE" value="<c:out value="${tripDiesel.uniqueId}"/>" style="width:120px;height:40px;"/>
                                                    <input type="text" class="form-control" id="slipNoE<%=indexs%>" name="slipNoE" value="<c:out value="${tripDiesel.ticketNo}"/>" style="width:120px;height:40px;"/>
                                                </td>
                                                <td><input type="text" class="form-control datepicker" id="fuelDateE<%=indexs%>" name="fuelDateE" value="<c:out value="${tripDiesel.fuelDate}"/>" style="width:120px;height:40px;"/>
                                                </td>
                                                <td><input type="text" class="form-control" id="fuelLtrsE<%=indexs%>" name="fuelLtrsE" value="<c:out value="${tripDiesel.dieselUsed}"/>" onchange='fuelPrice2(<%=indexs%>);' style="width:120px;height:40px;"/>
                                                </td>
                                                     
                                                <c:if test="${tripDiesel.fuelingMode == '0' || tripDiesel.fuelingMode == '1'}">
                                                <td>
                                                    <!--<select type="text"  class="form-control" id="fuelPriceRemarksE<%=indexs%>" name="fuelPriceRemarksE" onChange="setFuelAmount(<%=indexs%>)" ><option value="0~0" style="width:120px;height:40px;">--select---</option></select>-->
                                                    <input type="text" class="form-control" id="fuelPriceRemarksE<%=indexs%>" name="fuelPriceRemarksE" value="<c:out value="${tripDiesel.dieselCost/tripDiesel.dieselUsed}"/>" onchange='fuelPrice2(<%=indexs%>);' />
                                                    <input type="hidden" class="form-control" id="fuelPriceE<%=indexs%>" name="fuelPriceE" value="<c:out value="${tripDiesel.dieselCost/tripDiesel.dieselUsed}"/>" />
                                                </td>
                                                <script>
                                                     setFuelPriceList1('<%=indexs%>');
                                                </script>
                                               </c:if>
                                               <c:if test="${tripDiesel.fuelingMode == '2'}">
                                                <td>
                                                    <input type="text" class="form-control" id="fuelPriceRemarksE<%=indexs%>" name="fuelPriceRemarksE" value="<c:out value="${tripDiesel.dieselCost/tripDiesel.dieselUsed}"/>" onchange='fuelPrice2(<%=indexs%>);' />
                                                    <input type="hidden" class="form-control" id="fuelPriceE<%=indexs%>" name="fuelPriceE" value="<c:out value="${tripDiesel.dieselCost/tripDiesel.dieselUsed}"/>" />
                                                </td>
                                                <script>
                                                    $("#fuelPriceRemarksE" +<%=indexs%>).val('<c:out value="${tripDiesel.fuelPriceRmrk}" />');
                                                </script>
                                                </c:if>
                                                <td><input type="text" class="form-control" id="fuelAmountE<%=indexs%>" name="fuelAmountE" value="<c:out value="${tripDiesel.dieselCost}"/>" readonly style="width:120px;height:40px;"/>
                                                </td>
                                                <td>
                                                    <c:out value="${userName}"/>
                                                </td>
                                                <td><input type="text" class="form-control" id="fuelRemarksE<%=indexs%>" name="fuelRemarksE" value="<c:out value="${tripDiesel.remarks}"/>" style="width:120px;height:40px;"/>
                                                </td>
                                            </tr>                     
                                            <script>
                                               
                                            </script>
                                            <%indexs++;%>
                                        </c:forEach>
                                    </c:if>
                                    <tr>
                                        <td colspan="8" align="center">
                                            <input type="button" name="add" value="add" onclick="addFuelRow()" id="add" class="btn btn-success" style="width:100px;height:40px;"/>
                                            &nbsp;&nbsp;&nbsp; <input type="button" class="btn btn-success"  id="saveExtraFuel" value="Save" name="saveExtraFuel" onclick="submitExtraFuelDetails();"/>
                                    </tr>



                                </table>

                                <table class="table table-info mb30 table-hover" id="fuelRecoveryTable" >
                                    <thead>
                                        <tr>
                                            <th  colspan="4" >Fuel Recovery Details</th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td >Considered Allocated Fuel</td>
                                        <td >
                                            <input type="text" name="allocatedFuel" id="allocatedFuel"  class="form-control" onchange="calculateFuelRecovery();"  value="<c:out value="${allocatedFuel}" />" style="width:250px;height:40px"  onKeyPress="return onKeyPressBlockCharacters(event);"  /></td>
                                        <td > Filled Fuel</td>
                                        <td ><input type="text" name="filledFuel" id="filledFuel" readonly class="form-control" style="width:250px;height:40px" onchange="calculateFuelRecovery();"   value="<c:out value="${totalConsumedFuel}"/>"   onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                    </tr>
                                    <tr>
                                        <td > Compensation Fuel</td>
                                        <td >

                                            <input type="text" name="compensationFuel" id="compensationFuel" class="form-control"  onchange="calculateFuelRecovery();"  value="<c:out value="${compensationFuel}" />" style="width:250px;height:40px"  onKeyPress="return onKeyPressBlockCharacters(event);"  />
                                        </td>
                                        <td > Recovery Fuel</td>
                                        <td >
                                            <input type="text" readonly name="recoveryFuel" id="recoveryFuel" class="form-control"  onchange="calculateFuelRecovery();"  value="<c:out value="${recoveryFuel}" />" style="width:250px;height:40px"  onKeyPress="return onKeyPressBlockCharacters(event);"  />
                                        </td>

                                    </tr>
                                    <tr>

                                        <td > Recovery Amount</td>
                                        <td ><input type="text" name="recoveryAmount" id="recoveryAmount" readonly class="form-control" style="width:250px;height:40px"  value="<c:out value="${recoveryAmount}" />"   onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                        <td > Cur.Fuel Price</td>
                                        <td >
                                            <input type="text" name="fuelPriceDetail" readonly  id="fuelPriceDetail" value="<c:out value="${fuelPriceDetail}"/>" class="form-control" style="width:250px;height:40px"  value="<c:out value="${recoveryAmount}" />"   onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                        </td>
                                    </tr>

                                    <tr>
                                        <!--                                            <td > Fuel Slip No</td>
                                                                                    <td >-->
                                    <input type="hidden" name="fuelSlipNo" id="fuelSlipNo" class="form-control" value="<c:out value="${fuelSlipNo}" />" style="width:250px;height:40px"   />
                                    <td > Consumed Fuel</td>
                                    <td >
                                        <input type="text" name="consumedFuel" id="consumedFuel" readonly  class="form-control" style="width:250px;height:40px" onchange="calculateFuelRecovery();"   value="<c:out value="${totalConsumedFuel}" />"   onKeyPress="return onKeyPressBlockCharacters(event);" />
                                        <input type="hidden" name="enrouteFuel" id="enrouteFuel"  class="form-control" value="<c:out value="${totalConsumedFuel - allocatedFuel}" />"    />
                                    </td>
                                    <td ><font color="red"></font> Remarks</td>
                                    <td >
                                        <textarea  name="fuelRecoveryRemaks" id="fuelRecoveryRemaks" class="form-control" style="width:250px;height:40px"   /><c:out value="${fuelRemarks}" /></textarea>
                                    </td>
                                    </tr>


                                </table>


                                <center>
                                    <c:if test="${fuelPriceDetail != null}">
                                        <input type="button" class="btn btn-success"  id="saveFuelRecovery" value="Save" name="saveFuelRecovery" onclick="submitTripFuelDetails();"/>
                                    </c:if>
                                    <c:if test="${fuelPriceDetail == null}">
                                        <center><font color="red">Fuel Price Detail Not Found!</font></center>
                                        </c:if>
                                    &nbsp;&nbsp;&nbsp; <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" id="next1" style="width:100px;height:35px;font-weight: bold;"/></a>
                                    &nbsp;&nbsp;&nbsp; 
                                    <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:40px;"/></a>
                                </center>
                                <br>
                                <br>


                            </div>

                            <div id="tripDetail" style="display: none;">
                                <table class="table table-info mb30 table-hover" id="bg">
                                    <tr id="index" height="30px">
                                        <td  colspan="6" >Trip Details</td>
                                    </tr>

                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">


                                            <tr height="30">
                                                <td ><font color="red">*</font>Trip Sheet Date</td>
                                                <td ><input type="text" name="tripDate" class="datepicker" value=""></td>
                                                <td >CNote No(s)</td>
                                                <td >
                                                    <c:out value="${trip.cNotes}" />
                                                    <input type="hidden" name="cNotesEmail" value='<c:out value="${trip.cNotes}"/>' />
                                                </td>
                                                <td >Billing Type</td>
                                                <td >
                                                    <c:out value="${trip.billingType}" />
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td >Customer Code</td>
                                                <td >BF00001</td>
                                                <td >Customer Name</td>
                                                <td >
                                                    <c:out value="${trip.customerName}" />
                                                    <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${trip.customerName}" />'>
                                                    <input type="hidden" name="tripSheetId" Id="tripSheetId" class="textbox" value='<c:out value="${trip.tripId}" />'>
                                                    <input type="hidden" name="tripId" Id="tripId" class="textbox" value='<c:out value="${trip.tripId}" />'>
                                                </td>
                                                <td >Customer Type</td>
                                                <td  colspan="3" >
                                                    <c:out value="${trip.customerType}" />
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td >Route Name</td>
                                                <td >
                                                    <c:out value="${trip.routeInfo}" />
                                                </td>
                                                <td >Route Code</td>
                                                <td  >DL001</td>
                                                <td >Reefer Required</td>
                                                <td  >
                                                    <c:out value="${trip.reeferRequired}" />
                                                </td>
                                                <td >Order Est Weight (MT)</td>
                                                <td  >
                                                    <c:out value="${trip.totalWeight}" />
                                                </td>
                                            </tr>
                                            <tr height="30">
                                                <td >Vehicle Type</td>
                                                <td >
                                                    <c:out value="${trip.vehicleTypeName}" />
                                                </td>
                                                <td >Vehicle No</td>
                                                <td >
                                                    <c:out value="${trip.vehicleNo}" />
                                                    <input type="hidden" name="vehicleNoEmail" value='<c:out value="${trip.vehicleNo}"/>' />

                                                </td>
                                                <td >Vehicle Capacity (MT)</td>
                                                <td >
                                                    <c:out value="${trip.vehicleTonnage}" />

                                                </td>
                                            </tr>

                                            <tr height="30">
                                                <td >Veh. Cap [Util%]</td>
                                                <td >
                                                    <c:out value="${trip.vehicleCapUtil}" />
                                                </td>
                                                <td >Special Instruction</td>
                                                <td >-</td>
                                                <td >Trip Schedule</td>
                                                <td ><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" /> </td>
                                            </tr>


                                            <tr height="30">
                                                <td >Driver </td>
                                                <td  colspan="5" >
                                                    <c:out value="${trip.driverName}" />
                                                </td>

                                            </tr>
                                            <tr height="30">
                                                <td >Product Info </td>
                                                <td  colspan="5" >
                                                    <c:out value="${trip.productInfo}" />
                                                </td>

                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </table>
                                <br/>
                                <br/>

                                <c:if test = "${expiryDateDetails != null}" >
                                    <table class="table table-info mb30 table-hover" id="bg">
                                        <tr id="index" height="30px">
                                            <td colspan="4" >Vehicle Compliance Check</td>
                                        </tr>
                                        <c:forEach items="${expiryDateDetails}" var="expiryDate">
                                            <tr  height="30px">
                                                <td >Vehicle FC Valid UpTo</td>
                                                <td ><label><font color="green"><c:out value="${expiryDate.fcExpiryDate}" /></font></label></td>
                                            </tr>
                                            <tr  height="30px">
                                                <td >Vehicle Insurance Valid UpTo</td>
                                                <td ><label><font color="green"><c:out value="${expiryDate.insuranceExpiryDate}" /></font></label></td>
                                            </tr>
                                            <tr  height="30px">
                                                <td >Vehicle Permit Valid UpTo</td>
                                                <td ><label><font color="green"><c:out value="${expiryDate.permitExpiryDate}" /></font></label></td>
                                            </tr>
                                            <tr  height="30px">
                                                <td >Road Tax Valid UpTo</td>
                                                <td ><label><font color="green"><c:out value="${expiryDate.roadTaxExpiryDate}" /></font></label></td>
                                            </tr>
                                        </c:forEach>
                                    </table>
                                </c:if>

                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                            <c:if test="${tripType == 1}">
                                <div id="podDetail" style="display: none;">
                                    <c:if test="${viewPODDetails != null}">
                                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                            <tr id="tabHead" height="30px">
                                                <th width="50" >S No&nbsp;</th>
                                                <th >City Name</th>
                                                <th >POD file Name</th>
                                                <th >LR Number</th>
                                                <th >POD Remarks</th>
                                            </tr>
                                            <% int index2 = 1;%>
                                            <c:forEach items="${viewPODDetails}" var="viewPODDetails">
                                                <%
                                                            String classText3 = "";
                                                            int oddEven = index2 % 2;
                                                            if (oddEven > 0) {
                                                                classText3 = "text1";
                                                            } else {
                                                                classText3 = "text2";
                                                            }
                                                %>
                                                <tr>
                                                    <td class="<%=classText3%>" ><%=index2++%></td>
                                                    <td class="<%=classText3%>" ><c:out value="${viewPODDetails.cityName}"/></td>
                                                    <td class="<%=classText3%>" ><a  href="JavaScript:popUp('/throttle/uploadFiles/Files/<c:out value="${viewPODDetails.podFile}"/>');"><c:out value="${viewPODDetails.podFile}"/></a></td>
                                                    <td class="<%=classText3%>" ><c:out value="${viewPODDetails.lrNumber}"/></td>
                                                    <td class="<%=classText3%>" ><c:out value="${viewPODDetails.podRemarks}"/></td>
                                                </tr>
                                            </c:forEach>

                                        </table>
                                    </c:if>
                                    <br>
                                    <br>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                    <br>
                                </div>
                            </c:if>
                            <div id="routeDetail">

                                <c:if test = "${tripPointDetails != null}" >
                                    <table class="table table-info mb30 table-hover" >
                                        <tr id="index" height="30px">
                                            <td height="30" >S No</td>
                                            <td  height="30" >Point Name</td>
                                            <td  height="30" >Type</td>
                                            <td  height="30" >Route Order</td>
                                            <td  height="30" >Address</td>
                                            <td  height="30" >Planned Date</td>
                                            <td  height="30" >Planned Time</td>
                                        </tr>
                                        <% int index2 = 1;%>
                                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                                            <%
                                                        String classText1 = "";
                                                        int oddEven = index2 % 2;
                                                        if (oddEven > 0) {
                                                            classText1 = "text1";
                                                        } else {
                                                            classText1 = "text2";
                                                        }
                                            %>
                                            <tr>
                                                <td  ><%=index2++%></td>
                                                <td  ><c:out value="${tripPoint.pointName}" />
                                                    <input type="hidden" id="pointName" name="pointName" value="<c:out value="${tripPoint.pointName}" />"/>
                                                    <input type="hidden" id="mappingId" name="mappingId" value="<c:out value="${tripPoint.mappingCode}" />"/></td>
                                                <td  ><c:out value="${tripPoint.pointType}" /></td>
                                                <td  ><c:out value="${tripPoint.pointSequence}" /></td>
                                                <td  ><c:out value="${tripPoint.pointAddress}" /></td>
                                                <td  ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                                <td  ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                            </tr>
                                        </c:forEach >
                                    </table>
                                    <br/>

                                    <table class="table table-info mb30 table-hover" >
                                        <c:if test = "${tripDetails != null}" >
                                            <c:forEach items="${tripDetails}" var="trip">
                                                <tr>
                                                    <td  width="150"> Estimated KM</td>
                                                    <td  width="120" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                                                    <td  colspan="4">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td  width="150"> Estimated Reefer Hour</td>
                                                    <td  width="120"> <c:out value="${trip.estimatedTransitHours * 60 / 100}" />&nbsp;</td>
                                                    <td  colspan="4">&nbsp;</td>
                                                </tr>

                                            </c:forEach>
                                        </c:if>
                                    </table>

                                </c:if>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                            <div id="preStart" style="display: none;">
                                <c:if test = "${tripPreStartDetails != null}" >
                                    <table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >
                                        <c:forEach items="${tripPreStartDetails}" var="preStartDetails">
                                            <tr>
                                                <td  colspan="4" >Pre Start Details</td>
                                            </tr>
                                            <tr >
                                                <td  >Pre Start Date</td>
                                                <td  ><c:out value="${preStartDetails.preStartDate}" /></td>
                                                <td  >Pre Start Time</td>
                                                <td  ><c:out value="${preStartDetails.preStartTime}" /></td>
                                            </tr>
                                            <tr>
                                                <td  height="30" >Pre Odometer Reading</td>
                                                <td  height="30" ><c:out value="${preStartDetails.preOdometerReading}" /></td>
                                                <c:if test = "${tripDetails != null}" >
                                                    <td >Pre Start Location / Distance</td>
                                                    <td > <c:out value="${trip.preStartLocation}" /> / <c:out value="${trip.preStartLocationDistance}" />KM</td>
                                                </c:if>
                                            </tr>
                                            <tr>
                                                <td  >Pre Start Remarks</td>
                                                <td  ><c:out value="${preStartDetails.preTripRemarks}" /></td>
                                            </tr>
                                        </c:forEach >
                                    </table>
                                    <br/>
                                    <br/>
                                    <br/>
                                    <center>
                                        <a  class="nexttab" href="#"><input type="button" class="button" value="Next" name="Save" /></a>
                                    </center>
                                </c:if>
                                <br>
                                <br>
                            </div>
                            <div id="startDetail" style="display: none;">
                                <c:if test = "${tripStartDetails != null}" >
                                    <table class="table table-info mb30 table-hover" >
                                        <c:forEach items="${tripStartDetails}" var="startDetails">
                                            <tr id="index" height="30">
                                                <td  colspan="6" > Trip Start Details</td>
                                            </tr>
                                            <tr  height="30">
                                                <td  >Trip Planned Start Date</td>
                                                <td  ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                                <td  >Trip Planned Start Time</td>
                                                <td  ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                                <td  >Trip Start Reporting Date</td>
                                                <td  ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                                            </tr>
                                            <tr  height="30">
                                                <td  height="30" >Trip Start Reporting Time</td>
                                                <td  height="30" ><c:out value="${startDetails.startReportingTime}" />&nbsp;</td>
                                                <td  height="30" >Trip Loading date</td>
                                                <td  height="30" ><c:out value="${startDetails.loadingDate}" />&nbsp;</td>
                                                <td  height="30" >Trip Loading Time</td>
                                                <td  height="30" ><c:out value="${startDetails.loadingTime}" />&nbsp;</td>
                                            </tr>
                                            <tr  height="30">
                                                <td  >Trip Loading Temperature</td>
                                                <td  ><c:out value="${startDetails.loadingTemperature}" />&nbsp;</td>
                                                <td  >Trip Actual Start Date</td>
                                                <td  ><c:out value="${startDetails.startDate}" />&nbsp;</td>
                                                <td  >Trip Actual Start Time</td>
                                                <td  ><c:out value="${startDetails.startTime}" />&nbsp;</td>
                                            </tr>
                                            <tr  height="30">
                                                <td  height="30" >Trip Start Odometer Reading(KM)</td>
                                                <td  height="30" ><c:out value="${startDetails.startOdometerReading}" />&nbsp;</td>
                                                <td  height="30" >Trip Start Reefer Reading(HM)</td>
                                                <td  height="30" ><c:out value="${startDetails.startHM}" />&nbsp;</td>
                                                <td  height="30" colspan="2" ></td>
                                            </tr>
                                        </c:forEach >
                                    </table>

                                    <c:if test = "${tripUnPackDetails != null}" >
                                        <table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1" style="display:none">
                                            <tr id="index" height="30">
                                                <td width="20"  align="center" height="30" >Sno</td>
                                                <td  height="30" >Product/Article Code</td>
                                                <td  height="30" >Product/Article Name </td>
                                                <td  height="30" >Batch </td>
                                                <td  height="30" ><font color='red'>*</font>No of Packages</td>
                                                <td  height="30" ><font color='red'>*</font>Uom</td>
                                                <td  height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                                <td  height="30" ><font color='red'>*</font>Loaded Package Nos</td>
                                                <td  height="30" ><font color='red'>*</font>UnLoaded Package Nos</td>
                                                <td  height="30" ><font color='red'>*</font>Shortage</td>
                                            </tr>


                                            <%int i1 = 1;%>
                                            <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                                <tr>
                                                    <td><%=i1%></td>
                                                    <td><input type="text"  class="form-control" name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly style="width:130px;height:40px;"/></td>
                                                    <td><input type="text" class="form-control" name="productNames" id="productNames" value="<c:out value="${tripunpack.articleName}"/>" readonly style="width:130px;height:40px;"/></td>
                                                    <td><input type="text" class="form-control" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly style="width:130px;height:40px;"/></td>
                                                    <td><input type="text" class="form-control" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly style="width:130px;height:40px;"/></td>
                                                    <td><input type="text" class="form-control" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly style="width:130px;height:40px;"/></td>
                                                    <td><input type="text" class="form-control" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly style="width:130px;height:40px;"/></td>
                                                    <td><input type="text" class="form-control" name="loadedpackages" id="loadedpackages<%=i1%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:130px;height:40px;"/>
                                                        <input type="hidden" class="form-control" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>" />
                                                        <input type="hidden" class="form-control" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>
                                                    </td>
                                                    <td><input type="text" class="form-control" name="unloadedpackages" id="unloadedpackages<%=i1%>" onblur="computeShortage(<%=i1%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" style="width:130px;height:40px;"   /></td>
                                                    <td><input type="text" class="form-control" name="shortage" id="shortage<%=i1%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly style="width:130px;height:40px;" /></td>
                                                </tr>
                                                <%i1++;%>
                                            </c:forEach>

                                            <br/>
                                            <br/>
                                            <br/>
                                        </table>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>
                                        <br/>

                                    </c:if>
                                    <br>
                                    <br>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                    <br>
                                </c:if>
                            </div>
                            <div id="editStartDetail" style="display:none">
                                <c:if test = "${tripStartDetails != null}" >
                                    <table class="table table-info mb30 table-hover" >
                                        <c:forEach items="${tripStartDetails}" var="startDetails">
                                            <tr id="index" height="30">
                                                <td  colspan="6" > Trip Start Details</td>
                                            </tr>
                                            <tr height="30">
                                                <td  >Trip Planned Start Date</td>
                                                <td  ><c:out value="${startDetails.planStartDate}" />&nbsp;</td>
                                                <td  >Trip Planned Start Time</td>
                                                <td  ><c:out value="${startDetails.planStartTime}" />&nbsp;</td>
                                                <td  >Trip Start Reporting Date</td>
                                                <td  ><c:out value="${startDetails.startReportingDate}" />&nbsp;</td>

                                            </tr>

                                            <tr height="30">
                                                <td  height="30" >Trip Loading Temperature</td>
                                                <td  height="30" >
                                                    <input type="text" class="form-control" name="tripStartLoadTemp" id="tripStartLoadTemp"  value='<c:out value="${startDetails.loadingTemperature}" />' style="width:180px;height:40px;" />
                                                    &nbsp;</td>
                                                <td  height="30" >Trip Actual Start Date</td>
                                                <td  height="30" >
                                                    <input type="text" class="datepicker , form-control" name="tripStartDate" id="tripStartDate"  value='<c:out value="${startDetails.startDate}" />' style="width:180px;height:40px;"/>

                                                </td>
                                                <td  height="30" >Trip Actual Start Time</td>
                                                <td  height="30" >
                                                    <c:set var="startTime" value="${startDetails.startTime}"/>
                                                    <%
                                                                Object startTime = (Object) pageContext.getAttribute("startTime");
                                                                String stTime = (String) startTime;
                                                                String[] temp = null;
                                                                temp = stTime.split(":");
                                                    %>
                                                    HH: <select name="tripStartTimeHr" class="textbox" id="tripStartTimeHr" style="width:60px;height:40px;">
                                                        <option value='00'>00</option>
                                                        <option value='01'>01</option>
                                                        <option value='02'>02</option>
                                                        <option value='03'>03</option>
                                                        <option value='04'>04</option>
                                                        <option value='05'>05</option>
                                                        <option value='06'>06</option>
                                                        <option value='07'>07</option>
                                                        <option value='08'>08</option>
                                                        <option value='09'>09</option>
                                                        <option value='10'>10</option>
                                                        <option value='11'>11</option>
                                                        <option value='12'>12</option>
                                                        <option value='13'>13</option>
                                                        <option value='14'>14</option>
                                                        <option value='15'>15</option>
                                                        <option value='16'>16</option>
                                                        <option value='17'>17</option>
                                                        <option value='18'>18</option>
                                                        <option value='19'>19</option>
                                                        <option value='20'>20</option>
                                                        <option value='21'>21</option>
                                                        <option value='22'>22</option>
                                                        <option value='23'>23</option>
                                                    </select>
                                                    MI: <select name="tripStartTimeMin" class="form-control" id="tripStartTimeMin" style="width:60px;height:40px;">
                                                        <option value='00'>00</option>
                                                        <option value='01'>01</option>
                                                        <option value='02'>02</option>
                                                        <option value='03'>03</option>
                                                        <option value='04'>04</option>
                                                        <option value='05'>05</option>
                                                        <option value='06'>06</option>
                                                        <option value='07'>07</option>
                                                        <option value='08'>08</option>
                                                        <option value='09'>09</option>
                                                        <option value='10'>10</option>
                                                        <option value='11'>11</option>
                                                        <option value='12'>12</option>
                                                        <option value='13'>13</option>
                                                        <option value='14'>14</option>
                                                        <option value='15'>15</option>
                                                        <option value='16'>16</option>
                                                        <option value='17'>17</option>
                                                        <option value='18'>18</option>
                                                        <option value='19'>19</option>
                                                        <option value='20'>20</option>
                                                        <option value='21'>21</option>
                                                        <option value='22'>22</option>
                                                        <option value='23'>23</option>
                                                        <option value='24'>24</option>
                                                        <option value='25'>25</option>
                                                        <option value='26'>26</option>
                                                        <option value='27'>27</option>
                                                        <option value='28'>28</option>
                                                        <option value='29'>29</option>
                                                        <option value='30'>30</option>
                                                        <option value='31'>31</option>
                                                        <option value='32'>32</option>
                                                        <option value='33'>33</option>
                                                        <option value='34'>34</option>
                                                        <option value='35'>35</option>
                                                        <option value='36'>36</option>
                                                        <option value='37'>37</option>
                                                        <option value='38'>38</option>
                                                        <option value='39'>39</option>
                                                        <option value='40'>40</option>
                                                        <option value='41'>41</option>
                                                        <option value='42'>42</option>
                                                        <option value='43'>43</option>
                                                        <option value='44'>44</option>
                                                        <option value='45'>45</option>
                                                        <option value='46'>46</option>
                                                        <option value='47'>47</option>
                                                        <option value='48'>48</option>
                                                        <option value='49'>49</option>
                                                        <option value='50'>50</option>
                                                        <option value='51'>51</option>
                                                        <option value='52'>52</option>
                                                        <option value='53'>53</option>
                                                        <option value='54'>54</option>
                                                        <option value='55'>55</option>
                                                        <option value='56'>56</option>
                                                        <option value='57'>57</option>
                                                        <option value='58'>58</option>
                                                        <option value='59'>59</option>

                                                    </select>
                                                    <script>
                                                        document.getElementById("tripStartTimeHr").value = '<%=temp[0]%>';
                                                        document.getElementById("tripStartTimeMin").value = '<%=temp[1]%>';</script>
                                                    &nbsp;</td>
                                            </tr>
                                            <tr height="30">
                                                <td  >Trip Start Odometer Reading(KM)</td>
                                                <td  >
                                                    <input type="text" class="form-control" name="tripStartKm" id="tripStartKm"  value='<c:out value="${startDetails.startOdometerReading}"/>' style="width:180px;height:40px;"/>
                                                    &nbsp;</td>
                                                <td  >Trip Start Reefer Reading(HM)</td>
                                                <td  >
                                                    <input type="text" class="form-control" name="tripStartHm" id="tripStartHm" value='<c:out value="${startDetails.startHM}"/>' style="width:180px;height:40px;" />
                                                </td>
                                                <td  colspan="2" ></td>
                                            </tr>
                                        </c:forEach >
                                    </table>


                                    <br/>
                                    <center>
                                        <input type="button" class="btn btn-success" name="Save" style="width:220px;height:30px;font-weight: bold;padding:1px;" value="Update Trip Start Details" onclick="saveTripStartDetails();" />
                                    </center>

                                    <br>
                                    <br>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                    <br>
                                </c:if>
                            </div>
                            <div id="endDetails"  style="display: none;">
                                <c:if test = "${tripEndDetails != null}" >
                                    <table class="table table-info mb30 table-hover" >
                                        <c:forEach items="${tripEndDetails}" var="endDetails">
                                            <tr id="index" height="30">
                                                <td  colspan="6" > Trip End Details</td>
                                            </tr>
                                            <tr height="30">
                                                <td  >Trip Planned End Date</td>
                                                <td  ><c:out value="${endDetails.planEndDate}" /></td>
                                                <td  >Trip Planned End Time</td>
                                                <td  ><c:out value="${endDetails.planEndTime}" /></td>
                                                <td  >Trip Actual Reporting Date</td>
                                                <td  ><c:out value="${endDetails.endReportingDate}" /></td>
                                            </tr>
                                            <tr height="30">
                                                <td  height="30" >Trip Actual Reporting Time</td>
                                                <td  height="30" ><c:out value="${endDetails.endReportingTime}" /></td>
                                                <td  height="30" >Trip Unloading Date</td>
                                                <td  height="30" ><c:out value="${endDetails.unLoadingDate}" /></td>
                                                <td  height="30" >Trip Unloading Time</td>
                                                <td  height="30" ><c:out value="${endDetails.unLoadingTime}" /></td>
                                            </tr>
                                            <tr height="30">
                                                <td  >Trip Unloading Temperature</td>
                                                <td  ><c:out value="${endDetails.unLoadingTemperature}" /></td>
                                                <td  >Trip Actual End Date</td>
                                                <td  ><c:out value="${endDetails.endDate}" /></td>
                                                <td  >Trip Actual End Time</td>
                                                <td  > <c:out value="${endDetails.endTime}" /></td>
                                            </tr>
                                            <tr height="30">
                                                <td  height="30" >Trip End Odometer Reading(KM)</td>
                                                <td  height="30" ><c:out value="${endDetails.endOdometerReading}" /></td>
                                                <td  height="30" >Trip End Reefer Reading(HM)</td>
                                                <td  height="30" ><c:out value="${endDetails.endHM}" /></td>
                                                <td  height="30" >Total Odometer Reading(KM)</td>
                                                <td  height="30" ><c:out value="${endDetails.totalKM}" /></td>
                                            </tr>
                                            <tr height="30">
                                                <td  >Total Reefer Reading(HM)</td>
                                                <td  ><c:out value="${endDetails.totalHrs}" /></td>
                                                <td  >Total Duration Hours</td>
                                                <td  ><c:out value="${endDetails.durationHours}" /></td>
                                                <td  >Total Days</td>
                                                <td  ><c:out value="${endDetails.totalDays}" />

                                            </tr>

                                        </c:forEach >
                                    </table>

                                    <br/>
                                    <br/>
                                    <br/>

                                </c:if>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                            <div id="endDetail" style="display:none;">
                                <c:if test = "${tripEndDetails != null}" >
                                    <table class="table table-info mb30 table-hover" >
                                        <c:forEach items="${tripEndDetails}" var="endDetails">
                                            <tr id="index" height="30">
                                                <td  colspan="4" >Enter Trip End Details</td>
                                            </tr>
                                            <tr  height="30">
                                                <td  >Planned End Date</td>
                                                <td  ><c:out value="${endDetails.planEndDate}" /></td>

                                                <td  >Planned End Time</td>
                                                <td  ><c:out value="${endDetails.planEndTime}" /></td>
                                            </tr>
                                            <tr class="excludeRepo"  height="30">
                                                <td ><font color="red">*</font>Vehicle Actual (Factory In) Date</td>
                                                <td ><input type="text" name="vehicleactreportdate" id="vehicleactreportdate" class="datepicker , form-control" readonly value="<c:out value="${endDetails.endReportingDate}" />" style="width:180px;height:40px;"></td>
                                                <td  height="25" ><font color="red">*</font>Vehicle Actual (Factory In) Time </td>
                                                <td  colspan="3" align="left" height="25" >
                                                    HH:<select name="vehicleactreporthour" id="vehicleactreporthour" class="textbox" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                    MI:<select name="vehicleactreportmin" id="vehicleactreportmin" class="textbox" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                                            </tr>
                                            <c:if test="${endDetails.endReportingDate == null}">
                                                <script>
                                                    document.tripExpense.vehicleactreportdate.value = '<%=endDate%>';</script>
                                                </c:if>
                                                <c:if test="${endDetails.endReportingTime != null}">
                                                <script>
                                                    var endtime = '<c:out value="${endDetails.endReportingTime}" />';
                                                    var endtimes = endtime.split(":");
                                                    document.tripExpense.vehicleactreporthour.value = endtimes[0];
                                                    document.tripExpense.vehicleactreportmin.value = endtimes[1];
                                                </script>
                                            </c:if>
                                            <tr class="excludeRepo"  height="30">
                                                <td ><font color="red">*</font>Vehicle Actual (Factory Out) Date</td>
                                                <td ><input type="text" name="vehicleloadreportdate" id="vehicleloadreportdate" readonly class="datepicker , form-control" value="<c:out value="${endDetails.unLoadingDate}" />" style="width:180px;height:40px;"></td>
                                                <td  height="25" ><font color="red">*</font>Vehicle Actual (Factory Out) Time </td>
                                                <td  colspan="3" align="left" height="25" >HH:<select name="vehicleloadreporthour" id="vehicleloadreporthour" class="textbox" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                    MI:<select name="vehicleloadreportmin" id="vehicleloadreportmin" class="textbox" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                                            </tr>
                                            <c:if test="${endDetails.unLoadingDate == null}">
                                                <script>
                                                    document.tripExpense.vehicleloadreportdate.value = '<%=endDate%>';</script>
                                                </c:if>
                                                <c:if test="${endDetails.unLoadingTime != null}">
                                                <script>
                                                    var endtime = '<c:out value="${endDetails.unLoadingTime}" />';
                                                    var endtimes = endtime.split(":");
                                                    document.tripExpense.vehicleloadreporthour.value = endtimes[0];
                                                    document.tripExpense.vehicleloadreportmin.value = endtimes[1];
                                                </script>
                                            </c:if>
                                            <tr  height="30">
                                                <td   ><font color="red">*</font>End (ICD IN)Date  </td>
                                                <td   >
                                                    <input type="text" name="endDate" id="endDate" readonly class="datepicker , form-control" onchange="calculateDays();"  value="<c:out value="${endDetails.endDate}" />" style="width:180px;height:40px;">
                                                </td>
                                                <td   ><font color="red">*</font>End (ICD IN )Time<c:out value="${endDetails.endTime}" /> </td>
                                                <td   >HH:<select name="tripEndHour" id="tripEndHour" class="textbox" onchange="calculateDays();" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                                    MI:<select name="tripEndMinute" id="tripEndMinute" class="textbox"onchange="calculateDays();" style="width:60px;height:40px;"><option value="00" selected>00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select>
                                                </td>
                                            </tr>
                                            <c:if test="${endDetails.endDate == null}">
                                                <script>
                                                    document.tripExpense.endDate.value = '<%=endDate%>';</script>
                                                </c:if>
                                                <c:if test="${endDetails.endTime != null}">
                                                <script>
                                                    var endtime = '<c:out value="${endDetails.endTime}" />';
                                                    var endtimes = endtime.split(":");
                                                    document.tripExpense.tripEndHour.value = endtimes[0];
                                                    document.tripExpense.tripEndMinute.value = endtimes[1];
                                                </script>
                                            </c:if>

                                            <tr  height="30">
                                                <td ><font color="red">*</font> Trip End Odometer Reading(KM)</td>
                                                <td ><input type="text" name="endOdometerReading" id="endOdometerReading" class="form-control" value='<c:out value="${endDetails.endOdometerReading}" />' onKeyPress="return onKeyPressBlockCharacters(event);" onchange="calTotalKM();" style="width:180px;height:40px;"></td>
                                                <td ><font color="red">*</font>Trip End Reefer Reading(HM)</td>
                                                <td ><input type="text" name="endHM" id="endHM" class="form-control" value='<c:out value="${endDetails.endHM}" />' onchange="calTotalHM();" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"></td>
                                            </tr>


                                            <c:if test="${startTripDetails != null}">
                                                <c:forEach items="${startTripDetails}" var="tripDetails">
                                                    <input type="hidden" name="startKM" id="startKM" value="<c:out value="${tripDetails.startOdometerReading}"/>" />
                                                    <input type="hidden" name="startHM" id="startHM" value="<c:out value="${tripDetails.startHM}"/>" />
                                                    <input type="hidden" name="startDate" id="startDate" value="<c:out value="${tripDetails.startDate}"/>" />
                                                    <input type="hidden" name="startTime" id="startTime" value="<c:out value="${tripDetails.startTime}"/>" />
                                                </c:forEach>
                                            </c:if>


                                            <tr  height="30">
                                                <td > Total Odometer Reading(KM)</td>
                                                <td ><input type="text" readonly name="totalKM" id="totalKM" class="form-control" value='<c:out value="${endDetails.totalKM}" />' onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"></td>
                                                <td > Total Reefer Reading(HM)</td>
                                                <td ><input type="text" readonly name="totalHrs" id="totalHrs" class="form-control" value='<c:out value="${endDetails.totalHrs}" />' onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"></td>
                                            </tr>
                                            <tr  height="30">

                                                <td  >Total Duration Hours</td>
                                                <td  ><input type="text" name="tripTransitHour"  id="tripTransitHour" class="form-control"  value="<c:out value="${endDetails.durationHours}" />" style="width:180px;height:40px;"></td>

                                                <td  >Total Days</td>

                                                <td ><input type="text" name="totalDays1" readonly id="totalDays1" class="form-control"  value="<c:out value="${endDetails.totalDays}" />" style="width:180px;height:40px;"></td>


                                            </tr>
                                            <tr class="excludeRepo"  height="30"> 

                                                <td  height="30" >Total Factory In to ICD In Hours</td>

                                                <td ><input type="text" name="tripFactoryToIcdHour"  id="tripFactoryToIcdHour" class="form-control"  value="<c:out value="${endDetails.tripFactoryToIcdHour}" />" style="width:180px;height:40px;"></td>
                                                <td  height="30" ><font color="red">*</font>Detain Hours</td>
                                                <td  height="30" ><input type="text" name="tripDetainHour"  id="tripDetainHour" class="form-control"  value="<c:out value="${endDetails.tripDetainHours}" />" style="width:180px;height:40px;"></td>



                                            </tr>
                                            <tr  height="30" style="display:none">
                                                <c:forEach items="${tripDetails}" var="trip">
                                                    <td class="boE"  style="display: none"height="30">Bill of Entry: </td>
                                                    <td class="boE"><input type="text" id="billOfEntry" name="billOfEntry" value="<c:out value="${trip.billOfEntry}" />" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"/></td>
                                                    <td class="slN"  style="display: none"height="30">Shipping Bill No:</td>
                                                    <td class="slN"> <input type="text" id="shipingLineNo" name="shipingLineNo" value="<c:out value="${trip.shipingLineNo}" />" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:180px;height:40px;"/></td>
                                                    </c:forEach>
                                                <td  ><font color='red'>*</font>Commodity Category</td>
                                                <td><select id="commodityCategory" name="commodityCategory" style="width:180px;height:40px;"/>>
                                            <option value="0">---select-----</option>
                                            <c:forEach items="${comodityDetails}" var="com">
                                                <option value='<c:out value="${com.commodityName}" />'><c:out value="${com.commodityName}" /></option>
                                            </c:forEach>
                                            </select>
                                            <script>
                                                document.getElementById("commodityCategory").value = '<c:out value="${articleCode}" />'
                                            </script>  
                                            </td>
                                            </tr>
                                            <tr  height="30" >
                                                <td>Commodity</td>
                                                <td > <input type="text" id="commodityName" name="commodityName" style="width:180px;height:40px;"
                                                             value="" class="form-control"  /></td>
                                                <td colspan="2">&nbsp;</td>

                                            <script>
                                                document.getElementById("commodityName").value = '<c:out value="${articleName}" />'
                                            </script>
                                            </tr>
                                        </c:forEach >
                                    </table>
                                    <br/>
                                </c:if>
                                <br>
                                <%--      <c:if test = "${tripUnPackDetails != null}" >
                                          <table border="0" class="border" align="left" width="100%" cellpadding="0" cellspacing="0" id="addTyres1">
                                              <tr  height="30">
                                                  <td colspan="10"  align="center" height="30" >
                                              <center style="color:black;font-size: 14px;">
                                                  Consignment Unloading Details
                                              </center> 
                                              </td>
                                              </tr>
                                              <tr id="index" height="30">
                                                  <td width="30"  align="center" height="30" >Sno</td>
                                                  <td  height="30" >Product/Article Code</td>
                                                  <td  height="30" ><font color='red'>*</font>Product/Article Name </td>
                                                                                          <td  height="30" >Batch </td>
                                                                                          <td  height="30" ><font color='red'></font>No of Packages</td>
                                                                                          <td  height="30" ><font color='red'></font>Uom</td>
                                                                                          <td  height="30" ><font color='red'></font>Total Weight (in Kg)</td>
                                                                                          <td  height="30" ><font color='red'></font>Loaded Package Nos</td>
                                                                                          <td  height="30" ><font color='red'></font>UnLoaded Package Nos</td>
                                                                                          <td  height="30" ><font color='red'></font>Shortage</td>
                                              </tr>


                                    <%int i2 = 1;%>
                                    <c:forEach items="${tripUnPackDetails}" var="tripunpack">
                                        <tr>
                                            <td><%=i2%></td>
                                            <td><input type="text"  style="width:240px;height:40px;" class="form-control" name="productCodes" id="productCodes" value="<c:out value="${tripunpack.articleCode}"/>" readonly/></td>
                                            <td><input type="text" style="width:240px;height:40px;" class="form-control" name="productNames11" id="productNames11" value="<c:out value="${tripunpack.articleName}"/>" />
                                                <input type="hidden" name="productbatch" id="productbatch" value="<c:out value="${tripunpack.batch}"/>" readonly/>
                                                <input   type="hidden" name="packagesNos" id="packagesNos" value="<c:out value="${tripunpack.packageNos}"/>" readonly/>
                                                <input type="hidden" name="productuom" id="productuom" value="<c:out value="${tripunpack.uom}"/>" readonly/>
                                                <input type="hidden" name="weights" id="weights" value="<c:out value="${tripunpack.packageWeight}"/> " readonly/>
                                                <input type="hidden" name="loadedpackages" id="loadedpackages<%=i2%>" value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly/>
                                                <input type="hidden" name="consignmentId" value="<c:out value="${tripunpack.consignmentId}"/>"/>
                                                <input type="hidden" name="tripArticleId" value="<c:out value="${tripunpack.tripArticleid}"/>"/>

                                                <input type="hidden" name="unloadedpackages" id="unloadedpackages<%=i2%>" onblur="computeShortage(<%=i2%>);"  value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"    />
                                                <input type="hidden" name="shortage" id="shortage<%=i2%>"  value="<c:out value="${tripunpack.loadpackageNos}"/>" readonly /></td>
                                        </tr>
                                        <%i2++;%>
                                    </c:forEach>

                                    <br/>
                                </table>
                                <br/>
                                <br/>
                            </c:if>  --%>
                                <br>
                                <br>
                                <br>
                                <br>
                                <br>
                                <center>
                                    <input type="button" class="btn btn-success" name="Save" style="width:200px;height:30px;font-weight: bold;padding:1px;" value="Update Trip End Details" onclick="saveTripEndDetails();" />
                                </center>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                            <div id="statusDetail" >
                                <% int index1 = 1;%>

                                <%--<c:if test="${contractStatus == null}">--%>
                                <table  class="table table-info mb30 table-border" align="center"  id="bg">
                                    <thead>
                                        <tr >
                                            <th colspan="18">Customer Detention Details</th>
                                        </tr>
                                    </thead>
                                    <tr >
                                        <td ><font color="red">*</font>Halt Start Date/Time</td>
                                        <!--<td ><input type="text" readonly name="haltStartDate" readonly id="haltStartDate" class="datepicker" value="" style="width:250px;height:40px"/></td>-->
                                           <%-- <c:out value="${tripActualStartDate}"/> <c:out value="${tripActualStartHour}"/>:<c:out value="${tripActualStartMin}"/>:00</td>--%>
                                        <td ><input type="text"  id="haltStartDate" name="haltStartDate" class="datepicker" value="<c:out value="${haltStartDate}"/>" onchange="setDateDiff()" style="width:120px;height:40px"/>
                                            <!--<input name="haltStartDate" id="haltStartDate" type="text" class="datepicker , form-control"   autocomplete='off'  onchange="setDateDiff()" style="width:120px;height:40px"  value="<c:out value="${tripActualStartDate}"/>">-->
                                        <!--<td  ><font color="red">*</font>Trip Actual Start Time</td>-->
                                        HH: <select name="haltStartHour"  id="haltStartHour" onchange="setDateDiff();"   autocomplete='off' class="textbox" style="width:50px;height:40px">
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                            </select>
                                            MI: <select name="haltStartMinute"  id="haltStartMinute"  onchange="setDateDiff();" class="textbox" style="width:50px;height:40px" >
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                                <option value='24'>24</option>
                                                <option value='25'>25</option>
                                                <option value='26'>26</option>
                                                <option value='27'>27</option>
                                                <option value='28'>28</option>
                                                <option value='29'>29</option>
                                                <option value='30'>30</option>
                                                <option value='31'>31</option>
                                                <option value='32'>32</option>
                                                <option value='33'>33</option>
                                                <option value='34'>34</option>
                                                <option value='35'>35</option>
                                                <option value='36'>36</option>
                                                <option value='37'>37</option>
                                                <option value='38'>38</option>
                                                <option value='39'>39</option>
                                                <option value='40'>40</option>
                                                <option value='41'>41</option>
                                                <option value='42'>42</option>
                                                <option value='43'>43</option>
                                                <option value='44'>44</option>
                                                <option value='45'>45</option>
                                                <option value='46'>46</option>
                                                <option value='47'>47</option>
                                                <option value='48'>48</option>
                                                <option value='49'>49</option>
                                                <option value='50'>50</option>
                                                <option value='51'>51</option>
                                                <option value='52'>52</option>
                                                <option value='53'>53</option>
                                                <option value='54'>54</option>
                                                <option value='55'>55</option>
                                                <option value='56'>56</option>
                                                <option value='57'>57</option>
                                                <option value='58'>58</option>
                                                <option value='59'>59</option>

                                            </select>
                                             <script>
                                                document.getElementById("haltStartHour").value = '<c:out value="${haltStartHour}"/>';
                                                document.getElementById("haltStartMinute").value = '<c:out value="${haltStartMinute}"/>';
                                                                                         </script></td>
                                         <td ><font color="red">*</font>Halt End Date/Time</td>
                                         <td ><input type="text" id="haltEndDate" name="haltEndDate" class="datepicker" value="<c:out value="${haltEndDate}"/>" onchange="setDateDiff()" style="width:120px;height:40px"/>
                                             <!--<input name="haltEndDate" id="haltEndDate" type="text" class="datepicker , form-control"   autocomplete='off'  onchange="setDateDiff()" style="width:250px;height:40px"  value="<c:out value="${tripActualStartDate}"/>"></td>-->
                                        <!--<td  ><font color="red">*</font>Trip Actual Start Time</td>-->
                                        HH: <select name="haltEndHour"  id="haltEndHour" onchange="setDateDiff();"   autocomplete='off' class="textbox" style="width:60px;height:40px">
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                            </select>
                                           
                                            MI: <select name="haltEndMinute"  id="haltEndMinute"  onchange="setDateDiff();" class="textbox" style="width:60px;height:40px" >
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                                <option value='24'>24</option>
                                                <option value='25'>25</option>
                                                <option value='26'>26</option>
                                                <option value='27'>27</option>
                                                <option value='28'>28</option>
                                                <option value='29'>29</option>
                                                <option value='30'>30</option>
                                                <option value='31'>31</option>
                                                <option value='32'>32</option>
                                                <option value='33'>33</option>
                                                <option value='34'>34</option>
                                                <option value='35'>35</option>
                                                <option value='36'>36</option>
                                                <option value='37'>37</option>
                                                <option value='38'>38</option>
                                                <option value='39'>39</option>
                                                <option value='40'>40</option>
                                                <option value='41'>41</option>
                                                <option value='42'>42</option>
                                                <option value='43'>43</option>
                                                <option value='44'>44</option>
                                                <option value='45'>45</option>
                                                <option value='46'>46</option>
                                                <option value='47'>47</option>
                                                <option value='48'>48</option>
                                                <option value='49'>49</option>
                                                <option value='50'>50</option>
                                                <option value='51'>51</option>
                                                <option value='52'>52</option>
                                                <option value='53'>53</option>
                                                <option value='54'>54</option>
                                                <option value='55'>55</option>
                                                <option value='56'>56</option>
                                                <option value='57'>57</option>
                                                <option value='58'>58</option>
                                                <option value='59'>59</option>

                                            </select>
                                            <script>
                                                document.getElementById("haltEndHour").value = '<c:out value="${haltEndHour}"/>';
                                                document.getElementById("haltEndMinute").value = '<c:out value="${haltEndMinute}"/>';
                                            </script></td>
                                        </td>
                                        <!--<td ><input type="text" readonly name="haltEndDate" readonly id="haltEndDate" class="datepicker" value="" style="width:250px;height:40px"/></td>-->
                                            <%--<c:out value="${tripActualEndDate}"/> <c:out value="${tripActualEndHour}"/>:<c:out value="${tripActualEndMin}"/>:00</td>--%>
                                        <td style="display:none;" ><input name="graReportDate" id="graReportDate" type="text" class="datepicker , form-control"   autocomplete='off'  onchange="dateDiff()" style="width:250px;height:40px"  value="<c:out value="${tripActualStartDate}"/>"></td>
                                        <td style="display:none;"  ><font color="red">*</font>Trip Actual Start Time</td>
                                        <td  style="display:none;"  align="left"  >HH: <select name="graReportHour"  id="graReportHour" onchange="dateDiff();"   autocomplete='off' class="textbox" style="width:60px;height:40px">
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                            </select>
                                            MI: <select name="graReportMinute"  id="graReportMinute"  onchange="dateDiff();" class="textbox" style="width:60px;height:40px" >
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                                <option value='24'>24</option>
                                                <option value='25'>25</option>
                                                <option value='26'>26</option>
                                                <option value='27'>27</option>
                                                <option value='28'>28</option>
                                                <option value='29'>29</option>
                                                <option value='30'>30</option>
                                                <option value='31'>31</option>
                                                <option value='32'>32</option>
                                                <option value='33'>33</option>
                                                <option value='34'>34</option>
                                                <option value='35'>35</option>
                                                <option value='36'>36</option>
                                                <option value='37'>37</option>
                                                <option value='38'>38</option>
                                                <option value='39'>39</option>
                                                <option value='40'>40</option>
                                                <option value='41'>41</option>
                                                <option value='42'>42</option>
                                                <option value='43'>43</option>
                                                <option value='44'>44</option>
                                                <option value='45'>45</option>
                                                <option value='46'>46</option>
                                                <option value='47'>47</option>
                                                <option value='48'>48</option>
                                                <option value='49'>49</option>
                                                <option value='50'>50</option>
                                                <option value='51'>51</option>
                                                <option value='52'>52</option>
                                                <option value='53'>53</option>
                                                <option value='54'>54</option>
                                                <option value='55'>55</option>
                                                <option value='56'>56</option>
                                                <option value='57'>57</option>
                                                <option value='58'>58</option>
                                                <option value='59'>59</option>

                                            </select>
                                            <script>
                                                document.getElementById("graReportHour").value = '<c:out value="${tripActualStartHour}"/>';
                                                document.getElementById("graReportMinute").value = '<c:out value="${tripActualStartMin}"/>';
                                            </script>
                                        </td>
                                    </tr>
                                    <tr  style="display:none;">    
                                        <td ><font color="red">*</font>Trip Actual End Date</td>
                                        <td ><input name="graEndDate" id="graEndDate" type="text" autocomplete="off" class="datepicker , form-control" style="width:250px;height:40px"  onchange="dateDiff()" value="<c:out value="${tripActualEndDate}"/>"></td>
                                        <td ><font color="red">*</font>Trpi Actual End Time</td>
                                        <td  align="left"  >HH: <select name="graEndHour"  id="graEndHour"  onchange="dateDiff();" class="textbox" style="width:60px;height:40px">
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                            </select>
                                            MI: <select name="graEndMinute"  id="graEndMinute"  onchange="dateDiff();"  class="textbox" style="width:60px;height:40px" >
                                                <option value='00'>00</option>
                                                <option value='01'>01</option>
                                                <option value='02'>02</option>
                                                <option value='03'>03</option>
                                                <option value='04'>04</option>
                                                <option value='05'>05</option>
                                                <option value='06'>06</option>
                                                <option value='07'>07</option>
                                                <option value='08'>08</option>
                                                <option value='09'>09</option>
                                                <option value='10'>10</option>
                                                <option value='11'>11</option>
                                                <option value='12'>12</option>
                                                <option value='13'>13</option>
                                                <option value='14'>14</option>
                                                <option value='15'>15</option>
                                                <option value='16'>16</option>
                                                <option value='17'>17</option>
                                                <option value='18'>18</option>
                                                <option value='19'>19</option>
                                                <option value='20'>20</option>
                                                <option value='21'>21</option>
                                                <option value='22'>22</option>
                                                <option value='23'>23</option>
                                                <option value='24'>24</option>
                                                <option value='25'>25</option>
                                                <option value='26'>26</option>
                                                <option value='27'>27</option>
                                                <option value='28'>28</option>
                                                <option value='29'>29</option>
                                                <option value='30'>30</option>
                                                <option value='31'>31</option>
                                                <option value='32'>32</option>
                                                <option value='33'>33</option>
                                                <option value='34'>34</option>
                                                <option value='35'>35</option>
                                                <option value='36'>36</option>
                                                <option value='37'>37</option>
                                                <option value='38'>38</option>
                                                <option value='39'>39</option>
                                                <option value='40'>40</option>
                                                <option value='41'>41</option>
                                                <option value='42'>42</option>
                                                <option value='43'>43</option>
                                                <option value='44'>44</option>
                                                <option value='45'>45</option>
                                                <option value='46'>46</option>
                                                <option value='47'>47</option>
                                                <option value='48'>48</option>
                                                <option value='49'>49</option>
                                                <option value='50'>50</option>
                                                <option value='51'>51</option>
                                                <option value='52'>52</option>
                                                <option value='53'>53</option>
                                                <option value='54'>54</option>
                                                <option value='55'>55</option>
                                                <option value='56'>56</option>
                                                <option value='57'>57</option>
                                                <option value='58'>58</option>
                                                <option value='59'>59</option>
                                            </select>
                                            <script>
                                                document.getElementById("graEndHour").value = '<c:out value="${tripActualEndHour}"/>';
                                                document.getElementById("graEndMinute").value = '<c:out value="${tripActualEndMin}"/>';
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total (Hrs)</td>
                                        <td><input type="text" id="totalHours" name="totalHours" autocomplete="off"  value=""  readonly  class="form-control" style="width:250px;height:40px"></td>
                                        <td>Total (Days)</td>
                                        <td>
                                            <input type="text" id="totDays" readOnly name="totDays" onchange="calculateDetentionCharge();" autocomplete="off" onKeyPress='return onKeyPressBlockCharacters(event);' value=''  class="form-control" style="width:250px;height:40px"></td>

                                    </tr>
                                    <tr>
                                        <td>Detention(Hours)</td>
                                        <td><input type="text" id="graDiffDate" name="graDiffDate" readonly autocomplete="off"  value=""  class="form-control" style="width:250px;height:40px"></td>
                                        <td>
                                            <!--TAT (Hrs)-->
                                        </td>
                                        <td>
                                            <input type="hidden" id="tatHours" name="tatHours"  autocomplete="off"  value="<c:out value="${tatHours}"/>" onchange="calculateDetentionCharge();"  class="form-control" style="width:250px;height:40px">
                                            <input type="hidden" id="detFromHour" name="detFromHour"  autocomplete="off"  value="<c:out value="${detFromHour}"/>" onchange="calculateDetentionCharge();"  class="form-control" style="width:250px;height:40px">
                                            <input type="hidden" id="detToHour" name="detToHour"  autocomplete="off"  value="<c:out value="${detToHour}"/>" onchange="calculateDetentionCharge();"  class="form-control" style="width:250px;height:40px">
                                        </td>

                                    </tr>
                                    <tr>

                                        <td>Charges(per day)</td>
                                        <td>
                                            <input type="text" id="tatRate" name="tatRate" autocomplete="off" readonly value="<c:out value="${tatRate}"/>"   class="form-control" style="width:250px;height:40px"></td>
                                        <td>Total Amount (INR)</td>
                                        <td>
                                            <input type="text" id="detentionCharge" name="detentionCharge" autocomplete="off"  value="0" readonly  class="form-control" style="width:250px;height:40px">
                                    </tr>

                                    <tr>
                                        <td colspan="4" align="center">
                                            <input type="button" class="btn btn-success" id="detentionVal" name="Save" style="width:100px;height:40px;" value="SAVE" onclick="updateDetention();" />
                                        </td>
                                    </tr>

                                </table>
                                <%--</c:if>--%>
                                <c:if test = "${statusDetails != null}" >
                                    <table class="table table-info mb30 table-hover" >
                                        <tr id="index" height="30">
                                            <td  height="30" >S No</td>
                                            <td  height="30" >Status Name</td>
                                            <td  height="30" >Status DateTime</td>
                                            <td  height="30" >Duration</td>
                                            <td  height="30" >Remarks</td>
                                            <td  height="30" >Created User Name</td>
                                            <td  height="30" >Created Date</td>
                                        </tr>
                                        <c:forEach items="${statusDetails}" var="statusDetails">
                                            <%
                                                        String classText = "";
                                                        int oddEven1 = index1 % 2;
                                                        if (oddEven1 > 0) {
                                                            classText = "text1";
                                                        } else {
                                                            classText = "text2";
                                                        }
                                            %>
                                            <tr height="30">
                                                <td  ><%=index1++%></td>
                                                <td  ><c:out value="${statusDetails.statusName}" /></td>
                                                <td  ><c:out value="${statusDetails.logDate}" />&nbsp;<c:out value="${statusDetails.logTime}" /></td>
                                                <td  ><c:out value="${statusDetails.durationHours}" /></td>
                                                <td  ><c:out value="${statusDetails.tripRemarks}" /></td>
                                                <td  ><c:out value="${statusDetails.userName}" /></td>
                                                <td  ><c:out value="${statusDetails.tripDate}" /></td>
                                            </tr>
                                        </c:forEach >
                                    </table>
                                    <br/>
                                    <br/>
                                    <br/>
                                </c:if>
                                <br>
                                <br>
                                <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                </center>
                                <br>
                                <br>
                            </div>
                            <c:if test="${fuelTypeId == 1002}">
                                <div id="bpclDetail" style="display: none;">
                                    <% int index10 = 1;%>


                                    <c:if test = "${bpclTransactionHistory != null}" >
                                        <c:set var="totalAmount" value="0"/>
                                        <table class="table table-info mb30 table-hover" >
                                            <tr id="index" height="30">
                                                <td  height="30" >S No</td>
                                                <td  height="30" >Trip Code</td>
                                                <td  height="30" >Vehicle No</td>
                                                <td  height="30" >Transaction History Id</td>
                                                <td  height="30" >BPCL Transaction Id</td>
                                                <td  height="30" >BPCL Account Id</td>
                                                <td  height="30" >Dealer Name</td>
                                                <td  height="30" >Dealer City</td>
                                                <td  height="30" >Transaction Date</td>
                                                <td  height="30" >Accounting Date</td>
                                                <td  height="30" >Transaction Type</td>
                                                <td  height="30" >Currency</td>
                                                <td  height="30" >Transaction Amount</td>
                                                <td  height="30" >Volume Document No</td>
                                                <td  height="30" >Amount Balance</td>
                                                <td  height="30" >Petromiles Earned</td>
                                                <td  height="30" >Odometer Reading</td>
                                            </tr>
                                            <c:forEach items="${bpclTransactionHistory}" var="bpclDetail">
                                                <%
                                                            String classText10 = "";
                                                            int oddEven10 = index10 % 2;
                                                            if (oddEven10 > 0) {
                                                                classText10 = "text1";
                                                            } else {
                                                                classText10 = "text2";
                                                            }
                                                %>
                                                <tr height="30">
                                                    <td  ><%=index10++%></td>
                                                    <td  ><c:out value="${bpclDetail.tripCode}" /></td>
                                                    <td  ><c:out value="${bpclDetail.vehicleNo}" /></td>
                                                    <td  ><c:out value="${bpclDetail.transactionHistoryId}" /></td>
                                                    <td  ><c:out value="${bpclDetail.bpclTransactionId}" /></td>
                                                    <td  ><c:out value="${bpclDetail.accountId}" /></td>
                                                    <td  ><c:out value="${bpclDetail.dealerName}" /></td>
                                                    <td  ><c:out value="${bpclDetail.dealerCity}" /></td>
                                                    <td  ><c:out value="${bpclDetail.transactionDate}" /></td>
                                                    <td  ><c:out value="${bpclDetail.accountingDate}" /></td>
                                                    <td  ><c:out value="${bpclDetail.transactionType}" /></td>
                                                    <td  align="right"><c:out value="${bpclDetail.currency}" /></td>
                                                    <td  align="right"><c:out value="${bpclDetail.amount}" /></td>
                                                    <c:set var="totalAmount" value="${bpclDetail.amount + totalAmount}"  />
                                                    <td  align="right"><c:out value="${bpclDetail.volumeDocNo}" /></td>
                                                    <td  align="right"><c:out value="${bpclDetail.amoutBalance}" /></td>
                                                    <td  align="right"><c:out value="${bpclDetail.petromilesEarned}" /></td>
                                                    <td  align="right"><c:out value="${bpclDetail.odometerReading}" /></td>
                                                </tr>
                                            </c:forEach >

                                            <tr height="30">
                                                <td  height="30" style="text-align: right;color:black" colspan="12">Total Amount</td>
                                                <td  height="30" style="text-align: right;color:black;"  ><c:out value="${totalAmount}"/></td>
                                                <td  height="30" style="text-align: right"  colspan="4">&nbsp;</td>
                                            </tr>
                                        </table>
                                        <br/>
                                        <br/>
                                        <br/>
                                    </c:if>
                                    <br>
                                    <br>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                    <br>
                                </div>
                            </c:if>
                            <c:if test="${tripAdvanceDetails != null}">
                                <div id="advance" style="display: none;">
                                    <c:if test="${tripAdvanceDetails != null}">
                                        <table  class="table table-info mb30 table-hover" id="bg">
                                            <tr id="index" height="30">
                                                <td  width="30">Sno</td>
                                                <td  width="90">Advance Date</td>
                                                <td  width="90">Trip Day</td>
                                                <td  width="120">Estimated Advance</td>
                                                <td  width="120">Requested Advance</td>
                                                <td  width="90"> Type</td>
                                                <td  width="120">Requested By</td>
                                                <td  width="120">Requested Remarks</td>
                                                <td  width="120">Approved By</td>
                                                <td  width="120">Approved Remarks</td>
                                                <td  width="120">Paid Advance</td>
                                            </tr>
                                            <%int index7 = 1;%>
                                            <c:forEach items="${tripAdvanceDetails}" var="tripAdvance">
                                                <c:set var="totalAdvancePaid" value="${ totalAdvancePaid + tripAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                <%
                                                            String classText7 = "";
                                                            int oddEven7 = index7 % 2;
                                                            if (oddEven7 > 0) {
                                                                classText7 = "text1";
                                                            } else {
                                                                classText7 = "text2";
                                                            }
                                                %>
                                                <tr height="30">
                                                    <td ><%=index7++%></td>
                                                    <td ><c:out value="${tripAdvance.advanceDate}"/></td>
                                                    <td >DAY&nbsp;<c:out value="${tripAdvance.tripDay}"/></td>
                                                    <td ><c:out value="${tripAdvance.estimatedAdance}"/></td>
                                                    <td ><c:out value="${tripAdvance.requestedAdvance}"/></td>
                                                    <c:if test = "${tripAdvance.requestType == 'A'}" >
                                                        <td >Adhoc</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvance.requestType == 'B'}" >
                                                        <td >Batch</td>
                                                    </c:if>
                                                    <c:if test = "${tripAdvance.requestType == 'M'}" >
                                                        <td >Manual</td>
                                                    </c:if>
                                                    <td ><c:out value="${tripAdvance.approvalRequestBy}"/></td>
                                                    <td ><c:out value="${tripAdvance.approvalRequestRemarks}"/></td>
                                                    <td ><c:out value="${tripAdvance.approvedBy}"/></td>
                                                    <td ><c:out value="${tripAdvance.approvalRemarks}"/></td>
                                                    <td ><c:out value="${tripAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                </tr>
                                            </c:forEach>

                                            <tr height="30">

                                                <td  colspan="10" align="right" style="color:black"><b>Total Advance Paid </b>&emsp;</td>
                                                <td  align="right"  style="color:black"><b><c:out value="${totalAdvancePaid}"/></b></td>
                                            </tr>
                                        </table>
                                        <br/>
                                        <c:if test="${tripAdvanceDetailsStatus != null}">
                                            <table  class="table table-info mb30 table-hover" id="bg">
                                                <tr  height="30">
                                                    <td  colspan="13" >
                                                <center style="color:black;font-size: 14px;">
                                                    Advance Approval Status Details
                                                </center>
                                                </td>
                                                </tr>
                                                <tr id="index" height="30">
                                                    <td  width="30">Sno</td>
                                                    <td  width="90">Request Date</td>
                                                    <td  width="90">Trip Day</td>
                                                    <td  width="120">Estimated Advance</td>
                                                    <td  width="120">Requested Advance</td>
                                                    <td  width="90"> Type</td>
                                                    <td  width="120">Requested By</td>
                                                    <td  width="120">Requested Remarks</td>
                                                    <td  width="120">Approval Status</td>
                                                    <td  width="120">Paid Status</td>
                                                </tr>
                                                <%int index13 = 1;%>
                                                <c:forEach items="${tripAdvanceDetailsStatus}" var="tripAdvanceStatus">
                                                    <%
                                                                String classText13 = "";
                                                                int oddEven11 = index13 % 2;
                                                                if (oddEven11 > 0) {
                                                                    classText13 = "text1";
                                                                } else {
                                                                    classText13 = "text2";
                                                                }
                                                    %>
                                                    <tr height="30">

                                                        <td ><%=index13++%></td>
                                                        <td ><c:out value="${tripAdvanceStatus.advanceDate}"/></td>
                                                        <td >DAY&nbsp;<c:out value="${tripAdvanceStatus.tripDay}"/></td>
                                                        <td ><c:out value="${tripAdvanceStatus.estimatedAdance}"/></td>
                                                        <td ><c:out value="${tripAdvanceStatus.requestedAdvance}"/></td>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'A'}" >
                                                            <td >Adhoc</td>
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'B'}" >
                                                            <td >Batch</td>
                                                        </c:if>
                                                        <c:if test = "${tripAdvanceStatus.requestType == 'M'}" >
                                                            <td >Manual</td>
                                                        </c:if>

                                                        <td ><c:out value="${tripAdvanceStatus.approvalRequestBy}"/></td>
                                                        <td ><c:out value="${tripAdvanceStatus.approvalRequestRemarks}"/></td>
                                                        <td >
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== ''}" >
                                                                &nbsp
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '1' }" >
                                                                Request Approved
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '2' }" >
                                                                Request Rejected
                                                            </c:if>
                                                            <c:if test = "${tripAdvanceStatus.approvalStatus== '0'}" >
                                                                Approval in  Pending
                                                            </c:if>
                                                            &nbsp;</td>
                                                        <td >
                                                            <c:if test = "${ tripAdvanceStatus.approvalStatus== '1' || tripAdvanceStatus.approvalStatus== 'N'}" >
                                                                Yet To Pay
                                                            </c:if>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </table>
                                            <br/>
                                            <br/>

                                            <c:if test="${vehicleChangeAdvanceDetailsSize != '0'}">
                                                <table  class="table table-info mb30 table-hover" id="bg">
                                                    <tr  height="30">
                                                        <td  colspan="13" > 
                                                    <center style="color:black;font-size: 14px;">
                                                        Old Vehicle Advance Details
                                                    </center></td>
                                                    </tr>
                                                    <tr id="index" height="30">
                                                        <td  width="30">Sno</td>
                                                        <td  width="90">Advance Date</td>
                                                        <td  width="90">Trip Day</td>
                                                        <td  width="120">Estimated Advance</td>
                                                        <td  width="120">Requested Advance</td>
                                                        <td  width="90"> Type</td>
                                                        <td  width="120">Requested By</td>
                                                        <td  width="120">Requested Remarks</td>
                                                        <td  width="120">Approved By</td>
                                                        <td  width="120">Approved Remarks</td>
                                                        <td  width="120">Paid Advance</td>
                                                    </tr>
                                                    <%int index17 = 1;%>
                                                    <c:forEach items="${vehicleChangeAdvanceDetails}" var="vehicleChangeAdvance">
                                                        <c:set var="totalVehicleChangeAdvancePaid" value="${ totalVehicleChangeAdvancePaid + vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"></c:set>
                                                        <%
                                                                    String classText17 = "";
                                                                    int oddEven17 = index17 % 2;
                                                                    if (oddEven17 > 0) {
                                                                        classText17 = "text1";
                                                                    } else {
                                                                        classText17 = "text2";
                                                                    }
                                                        %>
                                                        <tr height="30">
                                                            <td ><%=index7++%></td>
                                                            <td ><c:out value="${vehicleChangeAdvance.advanceDate}"/></td>
                                                            <td >DAY&nbsp;<c:out value="${vehicleChangeAdvance.tripDay}"/></td>
                                                            <td ><c:out value="${vehicleChangeAdvance.estimatedAdance}"/></td>
                                                            <td ><c:out value="${vehicleChangeAdvance.requestedAdvance}"/></td>
                                                            <c:if test = "${vehicleChangeAdvance.requestType == 'A'}" >
                                                                <td >Adhoc</td>
                                                            </c:if>
                                                            <c:if test = "${vehicleChangeAdvance.requestType == 'B'}" >
                                                                <td >Batch</td>
                                                            </c:if>
                                                            <c:if test = "${vehicleChangeAdvance.requestType == 'M'}" >
                                                                <td >Manual</td>
                                                            </c:if>
                                                            <td ><c:out value="${vehicleChangeAdvance.approvalRequestBy}"/></td>
                                                            <td ><c:out value="${vehicleChangeAdvance.approvalRequestRemarks}"/></td>
                                                            <td ><c:out value="${vehicleChangeAdvance.approvedBy}"/></td>
                                                            <td ><c:out value="${vehicleChangeAdvance.approvalRemarks}"/></td>
                                                            <td ><c:out value="${vehicleChangeAdvance.paidAdvance + totalpaidAdvance}"/></td>
                                                        </tr>
                                                    </c:forEach>

                                                    <tr height="30">


                                                        <td  colspan="10" align="right"><b>Total Advance Paid </b>&emsp;</td>
                                                        <td  align="right"><b><c:out value="${totalVehicleChangeAdvancePaid}"/></b></td>

                                                    </tr>
                                                </table>
                                            </c:if>
                                        </c:if>

                                    </c:if>
                                    <br>
                                    <br>
                                    <center>
                                        <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                    <br>
                                </div>
                            </c:if>

                            <c:set var="totExpenseValue" value="0" />
                            <c:if test="${otherExpenseDetails != null}">
                                <c:forEach items="${otherExpenseDetails}" var="expenseDetails">
                                    <c:set var="totExpenseValue" value="${totExpenseValue + expenseDetails.expenseValue}"/>
                                </c:forEach>  
                            </c:if>

                            <div id="otherExpDetail" style="width: auto;padding-left: 2px;">
                                <c:if test="${routeDeviation != 0 && routeDeviation != null}">

                                    <center>
                                        <font color="black"><b>Route Deviated for this Trip</b></font>
                                    </center>                                    
                                </c:if>
                                <c:if test="${contractStatus != null}">
                                    <br>
                                    <center>
                                        <font color="red"><b><c:out value="${contractStatus}"/></b></font>
                                    </center>
                                    <br>
                                </c:if>


                                <div>
                                    <c:if test="${hireVehicleDriverDetailsSize > 0}">
                                        <table align="center" style="display:none;">
                                        </c:if>
                                        <c:if test="${hireVehicleDriverDetailsSize <= 0}">
                                            <table align="center">
                                            </c:if>

                                            <tr>
                                                <td>&emsp;<font color="red">*</font>Opening KM &emsp;</td>
                                                <td>
                                                    <input type="text" name="openKM" id="openKM" onchange="kmCalculator()" value="<c:out value="${openingKM}"/>"   onKeyPress='return onKeyPressBlockCharacters(event);'  class="form-control" style="width:150px;height:40px;"> 
                                                    <input type="hidden" name="openKMP" id="openKMP" value="<c:out value="${openingKM}"/>" > 
                                                </td>
                                                <td>&emsp;<font color="red">*</font>Closing KM&emsp;</td>
                                                <td> 
                                                    <input type="text" name="closingKM" id="closingKM" onchange="kmCalculator()" value="<c:out value="${endKM}"/>"  class="form-control"  onKeyPress='return onKeyPressBlockCharacters(event);'  style="width:150px;height:40px;">
                                                    <input type="hidden" name="closingKMP" id="closingKMP" value="<c:out value="${endKM}"/>" >
                                                </td>
                                                <td>&emsp;Total KM  &emsp;</td><td><input type="text" readonly name="totalKMs" id="totalKMs" value="<c:out value="${totalKM}"/>" class="form-control" style="width:150px;height:40px;"></td>
                                            </tr>
                                            <script>
                                                function kmCalculator() {

                                                    var openKM = document.getElementById("openKM").value;
                                                    var closingKM = document.getElementById("closingKM").value;
                                                    var openKMP = document.getElementById("openKMP").value;
                                                    var closingKMP = document.getElementById("closingKMP").value;
                                                    var totalKM = 0;
                                                    if (parseFloat(openKM) < parseFloat(closingKM)) {
                                                        totalKM = parseFloat(closingKM) - parseFloat(openKM);
                                                        document.getElementById("totalKMs").value = totalKM;
                                                        //document.getElementById("openKMP").value = openKM;
                                                        //document.getElementById("closingKMP").value = closingKM;

                                                    } else {
                                                        alert("Please enter valid KM");
//                                                        document.getElementById("openKM").value = openKMP;
//                                                        document.getElementById("closingKM").value = closingKMP;
                                                        totalKM = parseFloat(closingKMP) - parseFloat(openKMP);
                                                        document.getElementById("totalKMs").value = totalKM;
                                                    }

                                                    fuelLtrCalculation();
                                                }



                                            </script>
                                        </table>  

                                        <%
                                        String startDate = sdf.format(today);
                                        %>
                                        <center>
                                            <input type="hidden" name="rowCounted" id="rowCounted" value=""/>
                                            <input type="hidden" class="text" name="admin" value="<c:out value="${admin}"/>" >
                                        </center>
                                        <br>
                                        <table class="table table-info mb30 table-hover"  id="suppExpenseTBL" style="width:100%" >
                                            <tr id="index" height="40px;">
                                                <th><font color='red'>*</font>Expense Name</th>
                                                <th><font color='red'>*</font>Expense By</th>
                                                <!--                                                <th><font color='red'>*</font>Expense Date</th>-->

                                                <th><font color='red'>*</font>Expense Type</th>
                                                <th><font color='red'>*</font>Borne By</th>
                                                <th><font color='red'>*</font>Expense Value</th>
                                                <th>Expense Remarks</th>
                                                <th>Status</th>
                                                <!--                                                <th>Total Expenses</th>
                                                                                                <th>Document Required</th>-->
                                            </tr>
                                            <tr>

                                                <td ><td>
                                                <td >Total Expenses<td>
                                                <td ><span id="totExpenseAmt"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totExpenseValue}" /></span>
                                                    <input type="hidden"name="tripExpAmount" id="tripExpAmount" value="<c:out value="${totExpenseValue}"/>"></td>
                                                <td >
                                                    <input type="hidden" name="driverId" id="driverId" value='<c:out value="${driverId}"/>' /></td>
                                            </tr>
                                        </table><br><br>

                                        <center>
                                            <input type="hidden" name="rowCounted" id="rowCounted" value=""/>
                                            <input type="hidden" class="text" name="admin" value="<c:out value="${admin}"/>" >
                                            &emsp;   <input type="button" class="btn btn-success" name="add" value="Add" onclick="addRow(0, '0', '0', '0', '<%=startDate%>', '', '2', '0', '0', 0, 0);"
                                                            style="width:50px;height:30px;padding:1px;font-weight: bold"/>
                                            <!--&emsp;  <input type="reset" class="btn btn-success" value="Clear" style="width:50px;height:30px;padding:1px;font-weight: bold">-->
                                            &emsp; <input type="button" class="btn btn-success" value="Save " name="Save expenses" onclick="submitPage();" style="width:50px;height:30px;padding:1px;font-weight: bold"/>
                                            &emsp;


                                        </center>
                                        <br>        
                                        <center>
                                            <a><input type="button" class="btn btn-success btnNext" value="<spring:message code="vendors.label.NEXT" text="default text"/>" name="Next" id="next" style="width:100px;height:35px;font-weight: bold;"/></a>
                                            <a><input type="button" class="btn btn-success btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                                        </center>

                                        <br/>
                                        <br/>

                                        <style>
                                            #otherExpDetail
                                            {padding-left:-2px;}
                                        </style> 
                                </div>
                                <c:set var="totExpenseValue" value="0" />
                                <div style="border: #ffffff solid" >
                                    <%int count = 0;%>
                                    <input type="hidden" name="count" id="count" value="<c:out value="${otherExpenseDetailsSize}"/>"/>
                                    <c:if test="${otherExpenseDetails != null}">
                                        <h4 style="display:none" align="center">Expense Details</h4>
                                        <table class="table table-info mb30 table-hover" id="bg" style="display:none;">
                                            <!--<table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">-->
                                            <tr id="tabHead" height="30">
                                                <th width="50" >SNo&nbsp;</th>
                                                <th >Expense Name</th>
                                                <th >Expense By</th>
                                                <th >Expense Date</th>
                                                <th >Expense Remarks</th>
                                                <th >Expense Type</th>
                                                <th >Borne By</th>
                                                <th >Expense Amount</th>
                                                <th >Total Expenses</th>
                                                <th >Document Required</th>
                                                <th >Delete Expense</th>
                                                <th >Edit Expense</th>

                                            </tr>
                                            <% int index5 = 1;%>
                                            <c:forEach items="${otherExpenseDetails}" var="expenseDetails">
                                                <%
                                                            String classText5 = "";
                                                            int oddEven3 = index5 % 2;
                                                            if (oddEven3 > 0) {
                                                                classText5 = "text1";
                                                            } else {
                                                                classText5 = "text2";
                                                            }
                                                %>
                                                <%count++;%>
                                                <tr height="30">
                                                    <c:set var="totExpenseValue" value="${totExpense + expenseDetails.expenseValue }"/>
                                                    <td  ><%=index5++%></td>
                                                    <td  ><c:out value="${expenseDetails.expenseName}"/>&nbsp;</td>
                                                    <td  ><c:out value="${expenseDetails.employeeName}"/>&nbsp;</td>
                                                    <td  ><c:out value="${expenseDetails.expenseDate}"/>&nbsp;<c:out value="${expenseDetails.expenseHour}"/>:<c:out value="${expenseDetails.expenseMinute}"/>&nbsp</td>
                                                    <td  ><c:out value="${expenseDetails.expenseRemarks}"/>&nbsp;</td>
                                                    <c:if test = "${expenseDetails.expenseType == '1'}" >
                                                        <td  >Bill To Customer </td>
                                                    </c:if>
                                                    <c:if test = "${expenseDetails.expenseType == '2'}" >
                                                        <td  >Do Not Bill To Customer </td>
                                                    </c:if>
                                                    <c:if test = "${expenseDetails.expenseType == '0'}" >
                                                        <td  >&nbsp; </td>
                                                    </c:if>
                                                    <td  >
                                                        <c:if test = "${expenseDetails.borneBy == '0'}" >
                                                            Company
                                                        </c:if> 
                                                        <c:if test = "${expenseDetails.borneBy != '0'}" >
                                                            Driver
                                                        </c:if> 
                                                    </td>

                                                    <td  ><c:out value="${expenseDetails.taxPercentage}"/>&nbsp;</td>
                                                    <td  >
                                                        <c:if test="${expenseDetails.expenseValue != null}">
                                                            <c:out value="${expenseDetails.expenseValue}"/>
                                                        </c:if>
                                                        <c:if test="${expenseDetails.expenseValue == null}">
                                                            0
                                                        </c:if>
                                                    </td>
                                                    <td  >
                                                        <c:if test="${expenseDetails.totalExpenseAmount != null}">
                                                            <c:out value="${expenseDetails.totalExpenseAmount}"/>
                                                        </c:if>
                                                        <c:if test="${expenseDetails.totalExpenseAmount == null}">
                                                            0
                                                        </c:if>
                                                    </td>

                                                    <td   height="30"> <c:if test="${expenseDetails.documentRequired=='1'}">Yes </c:if> <c:if test="${expenseDetails.documentRequired=='0'}">No</c:if> </td>
                                                    <c:if test = "${expenseDetails.expenseCategory == '1'}" >
                                                        <td  height="30"><input type="checkbox"  name="delete" value="" id="delete<%=count%>" onclick="deleteExpenseDetails(<%=count%>)"/></td>
                                                        <td  height="30"><input type="checkbox"  name="edit" value="" id="edit<%=count%>" onclick="modifyExpenseDetails(<%=count%>)"/></td>
                                                        </c:if>
                                                        <c:if test = "${expenseDetails.expenseCategory == '2'}" >
                                                        <td  height="30"><input type="checkbox" name="delete" value="" id="delete<%=count%>" onclick="deleteExpenseDetails(<%=count%>)"/>
                                                        <td  height="30"><input type="checkbox" name="edit" value="" id="edit<%=count%>" onclick="modifyExpenseDetails(<%=count%>)"/>
                                                        </c:if>
                                                        <input type="hidden" name="expenseId" id="expenseId<%=count%>" value="<c:out value="${expenseDetails.tripExpenseId}"/>"/>
                                                        <input type="hidden" name="borneBy" id="borneBy<%=count%>" value="<c:out value="${expenseDetails.borneBy}"/>"/>
                                                        <input type="hidden" name="employeeNames" id="employeeNames<%=count%>" value="<c:out value="${expenseDetails.employeeId}"/>"/>
                                                        <input type="hidden" name="expenseNames" id="expenseNames<%=count%>" value="<c:out value="${expenseDetails.expenseId}"/>"/>
                                                        <input type="hidden" name="expenseDates" id="expenseDates<%=count%>" value="<c:out value="${expenseDetails.expenseDate}"/>"/>
                                                        <input type="hidden" name="expenseHours" id="expenseHours<%=count%>" value="<c:out value="${expenseDetails.expenseHour}"/>"/>
                                                        <input type="hidden" name="expenseMinutes" id="expenseMinutes<%=count%>" value="<c:out value="${expenseDetails.expenseMinute}"/>"/>
                                                        <input type="hidden" name="expenseRemarkss" id="expenseRemarkss<%=count%>" value="<c:out value="${expenseDetails.expenseRemarks}"/>"/>
                                                        <input type="hidden" name="expenseTypes" id="expenseTypes<%=count%>" value="<c:out value="${expenseDetails.expenseType}"/>"/>
                                                        <input type="hidden" name="passThroughStatuss" id="passThroughStatuss<%=count%>" value="<c:out value="${expenseDetails.passThroughStatus}"/>"/>
                                                        <input type="hidden" name="taxPercentages" id="taxPercentages<%=count%>" value="<c:out value="${expenseDetails.taxPercentage}"/>"/>
                                                        <input type="hidden" name="marginValues" id="marginValues<%=count%>" value="<c:out value="${expenseDetails.marginValue}"/>"/>
                                                        <input type="hidden" name="expenseValues" id="expenseValues<%=count%>" value="<c:out value="${expenseDetails.expenseValue}"/>"/></td>
                                                <input type="hidden" name="totalExpenseAmounts" id="totalExpenseAmounts<%=count%>" value="<c:out value="${expenseDetails.totalExpenseAmount}"/>"/>
                                                </td>
                                                </tr>

                                            </c:forEach>

                                        </table>
                                    </c:if>
                                    <br>
                                    <script>
                                        function modifyExpenseDetails(sno) {
                                            var count = parseInt(document.getElementById("count").value);
                                            var sno1 = 0;
                                            for (var i = 1; i <= count; i++) {
                                                if (i != sno) {
                                                    document.getElementById("edit" + i).checked = false;
                                                } else {
                                                    document.getElementById("edit" + i).checked = true;
                                                    document.getElementById('editId' + sno1).value = document.getElementById("expenseId" + i).value;
                                                    document.getElementById('expenseName' + sno1).value = document.getElementById("expenseNames" + i).value;
                                                    document.getElementById('employeeName' + sno1).value = document.getElementById("employeeNames" + i).value;
                                                    document.getElementById('expenseDate' + sno1).value = document.getElementById("expenseDates" + i).value;
                                                    //                                                document.getElementById('expenseHour' + sno1).value = document.getElementById("expenseHours" + i).value;
                                                    //                                                document.getElementById('expenseMinute' + sno1).value = document.getElementById("expenseMinutes" + i).value;
                                                    document.getElementById('expenseRemarks' + sno1).value = document.getElementById("expenseRemarkss" + i).value;
                                                    document.getElementById('expenseType' + sno1).value = document.getElementById("expenseTypes" + i).value;
                                                    //                                                document.getElementById('billModeTemp' + sno1).value = document.getElementById("passThroughStatuss" + i).value;
                                                    document.getElementById('taxPercentage' + sno1).value = document.getElementById("taxPercentages" + i).value;
                                                    //                                                document.getElementById('marginValue' + sno1).value = document.getElementById("marginValues" + i).value;
                                                    document.getElementById('expenses' + sno1).value = document.getElementById("expenseValues" + i).value;
                                                    document.getElementById('netExpense' + sno1).value = document.getElementById("totalExpenseAmounts" + i).value;
                                                    document.getElementById('borneBys' + sno1).value = document.getElementById("borneBy" + i).value;
                                                }
                                            }
                                        }
                                        function deleteExpenseDetails(sno) {
                                            //alert("hi:"+sno);
                                            var count = parseInt(document.getElementById("count").value);
                                            //                                        alert(count);
                                            var sno1 = 0;
                                            for (var i = 1; i <= count; i++) {
                                                if (i != sno) {
                                                    document.getElementById("delete" + i).checked = false;
                                                } else {
                                                    document.getElementById("delete" + i).checked = true;
                                                    document.getElementById('deleteId' + sno1).value = document.getElementById("expenseId" + i).value;
                                                    // document.getElementById('editId' + sno1).value = document.getElementById("expenseId" + i).value;
                                                    document.getElementById('expenseName' + sno1).value = document.getElementById("expenseNames" + i).value;
                                                    document.getElementById('employeeName' + sno1).value = document.getElementById("employeeNames" + i).value;
                                                    document.getElementById('expenseDate' + sno1).value = document.getElementById("expenseDates" + i).value;
                                                    //                                                document.getElementById('expenseHour' + sno1).value = document.getElementById("expenseHours" + i).value;
                                                    //                                                document.getElementById('expenseMinute' + sno1).value = document.getElementById("expenseMinutes" + i).value;
                                                    document.getElementById('expenseRemarks' + sno1).value = document.getElementById("expenseRemarkss" + i).value;
                                                    document.getElementById('expenseType' + sno1).value = document.getElementById("expenseTypes" + i).value;
                                                    //                                                document.getElementById('billModeTemp' + sno1).value = document.getElementById("passThroughStatuss" + i).value;
                                                    document.getElementById('taxPercentage' + sno1).value = document.getElementById("taxPercentages" + i).value;
                                                    //                                                document.getElementById('marginValue' + sno1).value = document.getElementById("marginValues" + i).value;
                                                    document.getElementById('expenses' + sno1).value = document.getElementById("expenseValues" + i).value;
                                                    document.getElementById('netExpense' + sno1).value = document.getElementById("totalExpenseAmounts" + i).value;
                                                    document.getElementById('borneBys' + sno1).value = document.getElementById("borneBy" + i).value;
                                                }
                                            }
                                        }
                                    </script>
                                    <table class="table table-info mb30 table-hover" >
                                        <!--<table width="300" cellpadding="0" cellspacing="1" border="0" align="right">-->
                                        <tr>
                                            <td id="availableAdvanceSpan" style="color:black;font-size: 15px;"> <b> </b></td>


                                        </tr>
                                        <input type="hidden" name="availableAdvance"  id="availableAdvance" value="" />

                                    </table>
                                    <br>


                                </div>


                                <script>
                                    var rowCount = 1;
                                    var sno = 0;
                                    var rowCount1 = 1;
                                    var sno1 = 0;
                                    var httpRequest;
                                    var httpReq;
                                    var styl = "";
                                    function addRow(count, tripExpenseId, employeeId, expenseId, expenseDate, expenseRemarks, expenseType, expenseValue, totalExpenseAmount, borneBys, approval) {
                                        //                                            alert("count=" + count);
                                        //                                             alert("expense : " + approval);
                                        //alert("expenseId : " + expenseId);
                                        //                                            alert("borneBys=" + borneBys);
                                        if (parseInt(rowCount1) % 2 == 0)
                                        {
                                            styl = "text2";
                                        } else {
                                            styl = "text1";
                                        }
                                        //                                            for (var i = 1; i < count; i++) {
                                        if (sno == 0) {
                                            sno++;
                                        }
                                        var tab = document.getElementById("suppExpenseTBL");
                                        var newrow = tab.insertRow(sno);
                                        //find current no of rows
                                        //  var rowCountNew = document.getElementById('suppExpenseTBL').rows.length;

                                        var rowCountNew = sno;
                                        rowCountNew = document.getElementById('rowCounted').value;
                                        rowCountNew++;
                                        // rowCountNew--;
                                        //                                    alert(sno);

                                        //                                            var newrow = tab.insertRow(rowCountNew);
                                        //                                            cell = newrow.insertCell(0);
                                        //                                            var cell0 = "<td class='text1' height='25' style='width:10px;'> <input type='hidden' name='editId' id='editId" + sno + "' value=''/><input type='hidden' name='deleteId' id='deleteId" + sno + "' value=''/>" + sno1 + "</td>";
                                        //                                            cell.setAttribute("className", styl);
                                        //                                            cell.innerHTML = cell0;

//                                            alert("tripExpenseId::"+tripExpenseId);
                                        if (tripExpenseId != 0) {
                                            cell = newrow.insertCell(0);
                                            cell0 = "<td class='text1' height='25' style='width:90px;'>\n\
                                        <input type='hidden' name='editId' id='editId" + sno + "' value=''/><input type='hidden' name='deleteId' id='deleteId" + sno + "' value=''/>\n\
                                        <select class='form-control' style='width:180px;height:40px;' id='expenseName" + sno + "' onchange='checkExpenseType(" + sno + ");'  name='expenseName'>\n\
                                        <option  value=0>---Select---</option> <c:if test="${expenseDetails != null}" ><c:forEach items="${expenseDetails}" var="expense"><option  value='<c:out value="${expense.expenseId}" />'><c:out value="${expense.expenseName}" /></option></c:forEach > </c:if> </select></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            $("#expenseName" + sno).val(expenseId);
                                            $("#editId" + sno).val(tripExpenseId);
                                        }

                                        if (tripExpenseId == 0 && document.getElementById("movementType").value != '17') {
                                            cell = newrow.insertCell(0);
                                            cell0 = "<td class='text1' height='25' style='width:90px;'>\n\
                                        <input type='hidden' name='editId' id='editId" + sno + "' value=''/><input type='hidden' name='deleteId' id='deleteId" + sno + "' value=''/>\n\
                                        <select class='form-control' style='width:180px;height:40px;' id='expenseName" + sno + "' onchange='checkExpenseType(" + sno + ");'  name='expenseName'>\n\
                                        <option  value=0>---Select---</option> <c:if test="${userId =='1789' }" ><option  value='1024'>Driver Batta</option> <option  value='1031'>Cleaner Bata</option> </c:if><c:if test="${expenseDetails != null}" ><c:forEach items="${expenseDetails}" var="expense"> <c:if test="${companyId =='1461' }" ><c:if test="${expense.expenseType =='2' }" > <c:if test="${expense.expenseId != '1021'}" ><option  value='<c:out value="${expense.expenseId}" />'><c:out value="${expense.expenseName}" /></option> </c:if></c:if><c:if test="${companyId !='1461' }" ><option  value='<c:out value="${expense.expenseId}" />'><c:out value="${expense.expenseName}" /></option> </c:if> </c:if></c:forEach > </c:if> </select></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            $("#expenseName" + sno).val(expenseId);
                                            $("#editId" + sno).val(tripExpenseId);
                                        }
                                        if (tripExpenseId == 0 && document.getElementById("movementType").value == '17') {
                                            cell = newrow.insertCell(0);
                                            cell0 = "<td class='text1' height='25' style='width:90px;'>\n\
                                        <input type='hidden' name='editId' id='editId" + sno + "' value=''/><input type='hidden' name='deleteId' id='deleteId" + sno + "' value=''/>\n\
                                        <select class='form-control' style='width:180px;height:40px;' id='expenseName" + sno + "' onchange='checkExpenseType(" + sno + ");'  name='expenseName'>\n\
                                        <option  value=0>---Select---</option> <c:if test="${expenseDetails != null}" ><c:forEach items="${expenseDetails}" var="expense"> <option  value='<c:out value="${expense.expenseId}" />'><c:out value="${expense.expenseName}" /></option> </c:forEach > </c:if> </select></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            $("#expenseName" + sno).val(expenseId);
                                            $("#editId" + sno).val(tripExpenseId);
                                        }

                                        cell = newrow.insertCell(1);
                                        cell0 = "<td class='text1' height='25' style='width:90px;'><select class='form-control' style='width:110px;height:40px;' id='employeeName" + sno + "'  name='employeeName'><option selected value=0>Company</option> <c:if test="${driverNameDetails != null}" ><c:forEach items="${driverNameDetails}" var="driver"><option selected value='<c:out value="${driver.employeeId}" />'><c:out value="${driver.employeeName}" /> </c:forEach > </c:if> </select></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        //                                            $("#employeeName" + sno).val(employeeId);
                                        //                                            cell = newrow.insertCell(2);
                                        //                                            var cell0 = "<td class='text1' height='25' style='width:70px;'>
                                        //                                            </td>";
                                        //                                            cell.setAttribute("className", styl);
                                        //                                            cell.innerHTML = cell0;
                                        //                                            

                                        cell = newrow.insertCell(2);
                                        var cell0 = "<td class='text1' height='25' style='width:90px;'>\n\
                                            <select class='form-control' id='expenseType" + sno + "'   name='expenseType' style='width:110px;height:40px;' onchange='setExpenseTypeDetails(this.value," + sno + ")'><option value='0' selected>-Select-</option><option  value='1' selected>Bill To Customer</option><option value='2'>Do Not Bill Customer</option><c:if test="${vehicleId == 0}"><option value='3'>Pay To Vendor</option></c:if></select>\n\
                                            <input type='hidden'  name='taxPercentage' id='taxPercentage" + sno + "'   onchange='calTotalExpenses(" + sno + ");' style='width:110px'  onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' value = '0' >\n\
                                            <input type='hidden'  name='marginValue' id='marginValue" + sno + "'   onchange='calTotalExpenses(" + sno + ");' style='width:110px;height:40px;' onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' value = '0' ><input type='hidden'  name='billMode' id='billMode" + sno + "' value = '0' ><input type='hidden' name='billModeTemp' id='billModeTemp" + sno + "' value='0'/><input type='hidden'  name='billMode' id='billMode" + sno + "' value = '0' >\n\
                                            <input type='hidden' name='expenseHour' id='expenseHour" + sno + "' value='00'/>\n\
                                            <input type='hidden' name='expenseMinute' id='expenseMinute" + sno + "' value='00'/>\n\
                                            <input type='hidden' value=''   name='expenseDate'  id='expenseDate" + sno + "'   class='datepicker' style='width:110px;height:40px;'>\n\
                                            </td>";
                                        cell.setAttribute("className", "text1");
                                        cell.innerHTML = cell0;
                                        $("#expenseType" + sno).val(expenseType);
                                        $("#expenseDate" + sno).val(expenseDate);
                                        cell = newrow.insertCell(3);
                                        var cell0 = "<td class='text1' height='25' style='width:90px;'><select class='form-control' id='borneBys" + sno + "'   name='borneBys' style='width:110px;height:40px;'><option value='0' selected>Company</option><option  value='1' >Driver</option></select></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        $("#borneBys" + sno).val(borneBys);
                                        //                                        cell = newrow.insertCell(6);
                                        //                                        var cell0 = "</td>";
                                        //                                        cell.setAttribute("className", styl);
                                        //                                        cell.innerHTML = cell0;
                                        ////                                      $("#taxPercentage" + sno).val(taxPercentage);

                                        cell = newrow.insertCell(4);
                                        var cell0 = "<td class='text1' height='10'  style='width:60px;' align='right'>\n\
                                        <input type='hidden'  name='taxPercentage' id='taxPercentage" + sno + "'   onchange='calTotalExpenses(" + sno + ");' style='width:110px'  onKeyPress='return onKeyPressBlockCharacters(event);' class='form-control' value = '' >\n\
                                        <input type='text'  name='expenses' id='expenses" + sno + "' class='form-control' style='width:110px;height:40px;' value=''  onKeyPress='return onKeyPressBlockCharacters(event);' onKeyup='checkAvailableAdvance(this.value);' >\n\
                                        <input readonly type='hidden' name='currency' id='currency" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:110px;height:40px;'  value='0' >\n\
                                        <input readonly type='hidden' name='netExpense' id='netExpense" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:110px;height:40px;'  class='form-control' value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        $("#expenses" + sno).val(expenseValue);
                                        //                                            cell = newrow.insertCell(5);
                                        //                                            var cell0 = "<td class='text1' height='25'  style='width:90px;'></td>";
                                        //                                            cell.setAttribute("className", styl);
                                        //                                            cell.innerHTML = cell0;
                                        //                                            $("#netExpense" + sno).val(totalExpenseAmount);

                                        cell = newrow.insertCell(5);
                                        cell0 = "<td class='text1' height='25' style='width:120px;'><input readonly type='hidden' name='currency' id='currency" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:110px;height:40px;'  value='0' ><input readonly type='hidden' name='netExpense' id='netExpense" + sno + "' onKeyPress='return onKeyPressBlockCharacters(event);' style='width:110px;height:40px;'  class='form-control' value='' ><textarea rows='3' cols='20' class='form-control' name='expenseRemarks' id='expenseRemarks" + sno + "' style='width:120px;height:50px'></textarea>\n\
                                        <input  type='hidden' name='activeValue' id='activeValue" + sno + "' value='0' >&emsp;<input  type='hidden' name='documentRequired' id='documentRequired" + sno + "' onclick ='setActiveValues(" + sno + ");' value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        $("#expenseRemarks" + sno).val(expenseRemarks);
                                        cell = newrow.insertCell(6);
                                        var cell0 = "<td class='text1' height='25' style='width:120px;'>\n\
                                        <div id='repair" + sno + "' style='display:none;'><select name='repairApproval' id='repairApproval" + sno + "'>\n\
                                        <option selected value='2'>Not Approved</option>\n\
                                        <option  value='1'>Approved</option>\n\
                                        </select></div>\n\
                                        <div id='customerhalt" + sno + "' style='display:none;'><select name='haltingApproval' id='haltingApproval" + sno + "'>\n\
                                        <option selected value='2'>Not Approved</option>\n\
                                        <option  value='1'>Approved</option>\n\
                                        </select></div>\n\
                                        </td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        $("#repairApproval" + sno).val(approval);
                                        $("#haltingApproval" + sno).val(approval);
                                        if (expenseId == '1013') {
                                            document.getElementById("repair" + sno).style.display = 'block';
                                        }
                                        if (expenseId == '1021') {
                                            document.getElementById("customerhalt" + sno).style.display = 'block';
                                        }


                                        if (tripExpenseId != 0) {
                                            $("#expenseName" + sno).css('pointer-events', 'none');
                                            $("#employeeName" + sno).css('pointer-events', 'none');
//                                            $("#expenseType" + sno).css('pointer-events', 'none');
                                            $("#borneBys" + sno).css('pointer-events', 'none');
                                        }

                                        document.getElementById('rowCounted').value = rowCount;
                                        rowCount1++;
                                        sno++;
                                    }
                                    </script>
                                    <!--Goes here-->
                                <c:set var="addRowCounts" value="0" />
                                <%int addRowCount = 0;%>     
                                <c:if test="${otherExpenseDetails != null}">
                                    <c:forEach items="${otherExpenseDetails}" var="expenseDetails">
                                        <c:set var="addRowCounts" value="${addRowCount}"/>
                                        <%
                                        addRowCount++;
                                        %>
                                        <script>
                                            //                                                alert("enter");
                                            addRow('<%=addRowCount%>',
                                                    '<c:out value="${expenseDetails.tripExpenseId}"/>',
                                                    '<c:out value="${expenseDetails.employeeId}"/>',
                                                    '<c:out value="${expenseDetails.expenseId}"/>',
                                                    '<c:out value="${expenseDetails.expenseDate}"/>',
                                                    '<c:out value="${expenseDetails.expenseRemarks}"/>',
                                                    '<c:out value="${expenseDetails.expenseType}"/>',
                                                    '<c:out value="${expenseDetails.expenseValue}"/>',
                                                    '<c:out value="${expenseDetails.totalExpenseAmount}"/>',
                                                    '<c:out value="${expenseDetails.borneBy}"/>',
                                                    '<c:out value="${expenseDetails.approvalStatus}"/>')
                                                    ;
                                        </script>
                                        <input type="hidden" name="addRowCount" id="addRowCount" value="<%=addRowCount%>" >
                                    </c:forEach>
                                </c:if>  

                                <script type="text/javascript">
                                    function checkExpenseType(sno) {
                                        var expenses = document.getElementsByName("expenseName");
                                        var expenseNames = document.getElementsByName("expenseNames");
                                        var expenseName = document.getElementById("expenseName" + sno).value;
                                        var check = 0;
                                        for (var i = 0; i < expenseNames.length; i++) {
                                            // alert(expenseName);
                                            if (expenseName == '1013') {
                                                document.getElementById("repair" + sno).style.display = 'block';
                                            } else {
                                                document.getElementById("repair" + sno).style.display = 'none';
                                            }
                                            if (expenseName == '1021') {
                                                document.getElementById("customerhalt" + sno).style.display = 'block';
                                            } else {
                                                document.getElementById("customerhalt" + sno).style.display = 'none';
                                            }
                                        }



//                                            if (check == 1) {
//                                                //                                                        alert("Same expense type is already selected, Do you want to proceed?");
//                                                //document.getElementById("expenseName" + sno).value = "0";
//                                                //document.getElementById("expenseName" + sno).focus();
//                                                return;
//                                            }
//                                            for (var i = 0; i <= expenses.length; i++) {
//                                                if (expenseName != 0) {
//                                                    if (i != sno) {
//                                                        if (expenses[i].value == expenseName) {
//                                                            check = 1;
//                                                        }
//                                                        if (check == 1) {
//                                                            //                                                                    alert("Same expense type is already selected, Do you want to proceed?");
//                                                            //document.getElementById("expenseName" + sno).value = "0";
//                                                            //document.getElementById("expenseName" + sno).focus();
//                                                            return;
//                                                        }
//                                                    }
//                                                }
//                                            }


                                    }

                                    function setActiveValues(sno) {
                                        if (document.getElementById('documentRequired' + sno).checked) {

                                            document.getElementById('activeValue' + sno).value = "1";
                                        } else {

                                            document.getElementById('activeValue' + sno).value = "0";
                                        }
                                    }
                                    function setCheckboxTrue(sno) {
                                        if (document.getElementById('expenses' + sno) != "" || document.getElementById('expenses' + sno) != 0) {
                                            document.getElementById("documentRequired" + sno).checked = true;
                                        } else {
                                            document.getElementById('documentRequired' + sno).checked == false;
                                        }
                                    }

                                    function checkAvailableAdvance(val) {
                                        var tot = 0;
                                        var expenses = document.getElementsByName("expenses");
                                        for (var i = 0; i < expenses.length; i++) {
                                            if (expenses[i].value != '') {
                                                tot = tot + parseFloat(expenses[i].value);
                                            } else {
                                                //expenses[i].value = 0.00;
                                            }

                                        }
                                        $('#totExpenseAmt').text(tot);
                                        document.getElementById('tripExpAmount').value = parseFloat(tot).toFixed(2);
                                    }
                                </script>

                            </div>

                            <script>
                                $('.btnNext').click(function () {
                                    $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                });
                                $('.btnPrevious').click(function () {
                                    $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                }
                                );</script>

                            <script>
                                fuelLtrCalculation();
                                function fuelLtrCalculation() {
                                    var runKM = $("#totalKMs").val();
                                    var mileage = $("#mileageVal").val();
                                    var allocatedFuel = $("#allocatedFuel").val();
                                    $("#totKM").text(runKM);
                                    if (mileage != 0) {
                                        allocatedFuel = runKM / mileage;
                                        allocatedFuel = allocatedFuel.toFixed(2);
                                        $("#allocatedFuel").val(allocatedFuel);
                                    }
                                }
                            </script>
                        </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
            <div id="loader"></div>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
