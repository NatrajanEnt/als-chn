/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.accident.business;

/**
 *
 * @author karudaiyar Subramaniam
 */
public class AccidentTO {

    public AccidentTO() {
    }
    
    
    
    
     private String ins_policy = "";
     private String ins_Name = "";
     private String ins_Address = "";
     private String ins_MobileNo = "";
     private String ins_PolicyNo = "";
     private String ins_IdvValue = "";
     private String issuingCmnyName = "";
     private String ins_AgentName = "";
     private String ins_AgentCode = "";
     private String ins_AgentMobileNo = "";
     private String uploadFileType = "";
     //jp Vehicle Accident
     
     
    
      private String vehicleDetails = "";
      private String insId = "";
      private String acc_SurveyorName = "";
      private String ploice_Address = "";
      private String accident_Id = "";
     private String acc_VehicleNo = "";
     private String acc_VehicleDetail = "";
     private String acc_DriverName = "";
     private String acc_DriverLicNo = "";
     private String acc_InsurerName = "";
     private String acc_PolicyNo = "";
     private String acc_ContactDetail = "";
     private String acc_ContactPerson = "";
     private String casualty_Name = "";
     private String casualty_Address = "";
     private String casualty_ConDetail = "";
     private String casualty_Type = "";
     private String casualty_AlterNo = "";
     private String casualty_ClaimDetail = "";
     private String acc_ReferenceNo = "";
     private String acc_SurveyorNo = "";
     private String acc_FirNo = "";
     private String witeness_Name = "";
     private String witeness_Address = "";
     private String witeness_Contact = "";
     private String rto_Fine = "";
     private String rto_StationRelease = "";
     private String rto_Date = "";
     private String rto_Upload = "";
     private String workshop_Type = "";
     private String ins_Tie_up = "";
     private String adhoc_Type = "";
     private String surv_InspectionDate = "";
     private String surv_QuotationAmt = "";
     private String surv_InsurerName = "";
     private String surv_PolicyNo = "";
     private String reim_Date = "";
     private String reim_Amt = "";
     private String reim_VehicleExcpen = "";
     private String reim_BillAmt = "";
     private String fileName = "";
     private String fileName1 = "";
     private String fileName2 = "";
     private String fileName3= "";
     private String vehicleStatus= "";
     private String lawyerName= "";
     private String law_ContactNo= "";
     private String law_court_Address= "";
     private String law_Fees= "";
     private String miscels_Fees= "";
     private String ins_Insurer= "";
     private String adhoc_BillAmt= "";
     private String ins_Owner= "";
     private String adhoc_PaidAmt= "";
     private int accInsertStat= 0;
     private String spotAddress= "";
     private String accFirDate= "";
     private String perOfDisability= "";

     private String accidentOccurance= "";
     private String createdOn= "";
     //end
    private String depthVal = "";
    private String depreciationId = "";
    private String vehicleYear = "";
    private String depreciation = "";
    private String yearCost = "";
    private String perMonth = "";
    private String vehicleAmt = "";
    private String monthlyAmount = "";
    private String balanceAmount = "";
    private String emiPayer = "";
    private String amountPaid = "";
    private String paymentMadeOn = "";
    private String paymentDate = "";
    private String paymentId = "";
    private String emiAmtPaid = "";
    private String balanceEmi = "";
    private String lastEmiBalance = "";
    private String totalBalance = "";
    private String roadTaxId = "";
    private String insPolicy = "";
    private String prevPolicy = "";
    private String packageType = "";
    private String insName = "";
    private String insAddress = "";
    private String insMobileNo = "";
    private String insPolicyNo = "";
    private String insIdvValue = "";
    private String fromDate1 = "";
    private String toDate1 = "";
    private String premiunAmount = "";
    private String axleDetailId = "";
    private String positionName = "";
    private String positionNo = "";
    private String updateValue = "";
    private String updateType = "";
    private String axleId = "";
    private String vehicleAxleId = "";
    private String axleTypeId = "";
    private String axleTypeName = "";
    private String axleCount = "";
    private String vehicleFront = "";
    private String axleLeft = "";
    private String axleRight = "";

    private String vName = "";
    private String cType = "";
    private String eRegNo = "";
    private String rRegNo = "";
    private String rDate = "";

    private String activityGroupId = "";
    private String trailerId = "";
    private String trailerNo = "";
    private String bodyWorkGroupId = "";
    private String mfrId = "";
    private String roleId = "";
    private String mfrName = "";
    private String activeInd = "";
    private String description = "";
    private String[] mfrNames = null;
    private String[] mfrIds = null;
    private String[] modelIds = null;
    private String[] tyreType = null;
    private String[] serialNo = null;
    private String[] positionId = null;
    private String[] itemIds = null;
    private String[] typeIds = null;
    private String[] typeNames = null;
    private String[] fuelIds = null;
    private String[] fuelNames = null;

    private String[] tyreDate = null;
    private String tyreD = null;

    private String typeId = "";
    private String typeName = "";
    private String fuelId = "";
    private String fuelName = "";
    private int fuelLiters = 0;
    private double fuelAmount = 0;
    private String modelId = "";
    private String modelName = "";
    private String usageId = "";
    private int usagId = 0;
    private String regno = "";
    private String kmReading = "";
    private String actualKm = "";
    private String totalKm = "";
    private String reason = "";

    private String usageName = "";
    private String classId = "";
    private String className = "";
    private String war_period = "";
    private String dateOfSale = "";
    private String registrationDate = "";
    private String engineNo = "";
    private String chassisNo = "";
    private String nextFCDate = "0000-00-00";
    private String seatCapacity = "";
    private String vehicleNumber = "";
    private String stateSymbol = "";
    private String districtSymbol = "";
    private String initial = "";
    private String vechileNo = "";
    private String vehicleId = "";
    private String opId = "";
    private String spId = "";
    private String regNo = "";
    private String itemId = "";
    private String companyId = "";
    private String companyName = "";
    private String hmReading = "0";
    //private String kmReading = "";
    private String custId = "0";
    private String custName = "";
    private String dailyKm = "";
    private String dailyHm = "";
    private String itemName = "";
    private String tyreNo = "";
    private String tyreId = "";
    private String posId = "";
    private String posName = "";
    private String fservice = "";
    private String percentage = "";
    private String[] tyreIds = null;
    private String[] tyreNos = null;
    private String[] regNos = null;
    int startIndex = 0;
    int endIndex = 0;
    private String vehicleCost = "0";
    private String vehicleDepreciation = "0";
    private String vehicleColor = "";
    private String ownerShips = "";

    //----------Insurance------------------
    private String insuranceCompanyName = "";
    private String premiumNo = "";
    private String premiumPaidDate = "";
    private double premiumPaidAmount = 0;
    private double vehicleValue = 0;
    private String premiumExpiryDate = "";
    private String insuarnceNCB = "";
    private String insuarnceRemarks = "";

    //---------Accident---------------
    private String accidentSpot = "";
    private String accidentDate = "";
    private String driverName = "";
    private String policeStation = "";
    private String firNo = "";
    private String firDate = "";
    private String firPreparedBy = "";
    private String accidentRemarks = "";

    //---------Road Tax--------------------
    private String roadTaxReceiptNo = "";
    private String roadTaxReceiptDate = "";
    private String roadTaxPaidLocation = "";
    private String roadTaxPaidOffice = "";
    private String roadTaxPeriod = "";
    private double roadTaxPaidAmount = 0;
    private String roadTaxRemarks = "";

    //---------FC Details--------------------
    private String rtoDetail = "";
    private String fcDate = "";
    private String fcReceiptNo = "";
    private double fcAmount = 0;
    private String fcExpiryDate = "";
    private String fcRemarks = "";

    private double serviceCost = 0;
    private double tripRunKm = 0;

    //Rathimeena route Master Start
    private String routeId = "";
    private String routeCode = "";
    private String routeName = "";
    private String fromLocation = "";
    private String toLocation = "";
    private String viaRoute = "";
    private String km = "";
    private String tollAmount = "";
    private String tonnageRate = "";
    private String sla = "";
    private String eligibleTrip = "";
    private String driverBata = "";
    //Rathimeena route Master End
    //Rathimeena vehicle group Master End
    private String groupId = "";
    private String groupName = "";
    private String routeKm = "";
    private String routeToll = "";
    private String driveBata = "";
    private String vehicleNo = "";

    private String companyname = "";
    private String premiumno = "";
    private String premiumpaiddate = "";
    private String premiumamount = "";
    private String vehiclevalue = "";
    private String expirydate = "";
    private String ncb = "";
    private String remarks = "";
    private String insuranceid = "";
    private String id = "";
    private String roadtaxreceiptno = "";
    private String roadtaxreceiptdate = "";
    private String roadtaxpaidlocation = "";
    private String roadtaxamount = "";
    private String roadtaxperiod = "";
    private String fcid = "";
    private String rtodetail = "";
    private String receiptno = "";
    private String fcamount = "";
    private String fcexpirydate = "";
    private String fcdate = "";

    private String permitType = "";
    private String permitNo = "";
    private double permitAmount = 0;
    private String permitPaidDate = "";
    private String permitExpiryDate = "";
    private String permitRemarks = "";
    private String permitid = "";

    private String agreementYear = "";
    private String fromDate = "";
    private String toDate = "";
    private String street = "";
    private String city = "";
    private String state = "";
    private String toCity = "";
    private String marketVehiclePercentage = "";
    private String phoneNo = "";
    private String mobileNo = "";
    private String emailId = "";
    private String asset = "";
    private String leasingCustId = "";
    private String ownership = "";

    private String vendorId = "";
    private String vendorName = "";
    private String bankerName = "";
    private String bankerAddress = "";
    private String financeAmount = "";
    private String roi = "";
    private String interestType = "";
    private String emiMonths = "";
    private String emiAmount = "";
    private String emiStartDate = "";
    private String emiEndDate = "";
    private String emiPayDay = "";
    private String payMode = "";
    //Vehicle AMC
    private String amcId = "";
    private String amcCompanyName = "";
    private String amcAmount = "";
    private String amcDuration = "";
    private String amcChequeNo = "";
    private String amcChequeDate = "";
    private String amcFromDate = "";
    private String amcToDate = "";

    private String vehicleMake = "";
    private String vehicleModel = "";
    private String vehicleUsage = "";
    private String amcRemarks = "";
    //Vehicle Profile
    private String insExpiryDate = "";
    private String insPremiumAmount = "";
    private String roadTaxFromDate = "";
    private String nextRoadTaxDate = "";
    private String financeId = "";
    private String receiptNo = "";

    private String roadTaxAmount = "";
    private String roadTaxToDate = "";

    // CLPL manage vehicle
    private String gpsSystem = "";
    private String warrantyDate = "";

    private String fromLocationId = "";
    private String toLocationId = "";
    private String ToLocationid = "";
    private String tonnageRateMarket = "";

    private String productId = "";
    private String productname = "";
    private String Depot = "";
    private String depotVal = "";

    private String vehicleTypeId = "";
    private String vehicleTypeName = "";

//Brattle Foods
    private String axles = "";
    private String dailyRunKm = "";
    private String dailyRunHm = "";
    private String gpsSystemId = "";
    private String tonnage = "";
    private String capacity = "";
    private String[] tonnages = null;
    private String[] capacities = null;

//Brattle Foods
    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    //Rathimeena vehicle group Master End

    public String getActualKm() {
        return actualKm;
    }

    public void setActualKm(String actualKm) {
        this.actualKm = actualKm;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getFservice() {
        return fservice;
    }

    public void setFservice(String fservice) {
        this.fservice = fservice;
    }

    public String getOpId() {
        return opId;
    }

    public void setOpId(String opId) {
        this.opId = opId;
    }

    public String getSpId() {
        return spId;
    }

    public void setSpId(String spId) {
        this.spId = spId;
    }

    public String getVechileNo() {
        return vechileNo;
    }

    public void setVechileNo(String vechileNo) {
        this.vechileNo = vechileNo;
    }

    public String getDistrictSymbol() {
        return districtSymbol;
    }

    public void setDistrictSymbol(String districtSymbol) {
        this.districtSymbol = districtSymbol;
    }

    public String getInitial() {
        return initial;
    }

    public void setInitial(String initial) {
        this.initial = initial;
    }

    public String getStateSymbol() {
        return stateSymbol;
    }

    public void setStateSymbol(String stateSymbol) {
        this.stateSymbol = stateSymbol;
    }

    public String getChassisNo() {
        return chassisNo;
    }

    public void setChassisNo(String chassisNo) {
        this.chassisNo = chassisNo;
    }

    public String getDateOfSale() {
        return dateOfSale;
    }

    public void setDateOfSale(String dateOfSale) {
        this.dateOfSale = dateOfSale;
    }

    public String getEngineNo() {
        return engineNo;
    }

    public void setEngineNo(String engineNo) {
        this.engineNo = engineNo;
    }

    public String getNextFCDate() {
        return nextFCDate;
    }

    public void setNextFCDate(String nextFCDate) {
        this.nextFCDate = nextFCDate;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getSeatCapacity() {
        return seatCapacity;
    }

    public void setSeatCapacity(String seatCapacity) {
        this.seatCapacity = seatCapacity;
    }

    public String getVehicleNumber() {
        return vehicleNumber;
    }

    public void setVehicleNumber(String vehicleNumber) {
        this.vehicleNumber = vehicleNumber;
    }

    public String getWar_period() {
        return war_period;
    }

    public void setWar_period(String war_period) {
        this.war_period = war_period;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getUsageId() {
        return usageId;
    }

    public void setUsageId(String usageId) {
        this.usageId = usageId;
    }

    public String getUsageName() {
        return usageName;
    }

    public void setUsageName(String usageName) {
        this.usageName = usageName;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getFuelId() {
        return fuelId;
    }

    public void setFuelId(String fuelId) {
        this.fuelId = fuelId;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getMfrId() {
        return mfrId;
    }

    public void setMfrId(String mfrId) {
        this.mfrId = mfrId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String[] getFuelIds() {
        return fuelIds;
    }

    public void setFuelIds(String[] fuelIds) {
        this.fuelIds = fuelIds;
    }

    public String getFuelName() {
        return fuelName;
    }

    public void setFuelName(String fuelName) {
        this.fuelName = fuelName;
    }

    public String[] getFuelNames() {
        return fuelNames;
    }

    public void setFuelNames(String[] fuelNames) {
        this.fuelNames = fuelNames;
    }

    public double getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(double fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public int getFuelLiters() {
        return fuelLiters;
    }

    public void setFuelLiters(int fuelLiters) {
        this.fuelLiters = fuelLiters;
    }

    public String[] getTypeIds() {
        return typeIds;
    }

    public void setTypeIds(String[] typeIds) {
        this.typeIds = typeIds;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String[] getTypeNames() {
        return typeNames;
    }

    public void setTypeNames(String[] typeNames) {
        this.typeNames = typeNames;
    }

    public String[] getMfrIds() {
        return mfrIds;
    }

    public void setMfrIds(String[] mfrIds) {
        this.mfrIds = mfrIds;
    }

    public String[] getMfrNames() {
        return mfrNames;
    }

    public void setMfrNames(String[] mfrNames) {
        this.mfrNames = mfrNames;
    }

    public String getMfrName() {
        return mfrName;
    }

    public void setMfrName(String mfrName) {
        this.mfrName = mfrName;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String[] getModelIds() {
        return modelIds;
    }

    public void setModelIds(String[] modelIds) {
        this.modelIds = modelIds;
    }

    public String[] getPositionId() {
        return positionId;
    }

    public void setPositionId(String[] positionId) {
        this.positionId = positionId;
    }

    public String[] getSerialNo() {
        return serialNo;
    }

    public void setSerialNo(String[] serialNo) {
        this.serialNo = serialNo;
    }

    public String[] getTyreType() {
        return tyreType;
    }

    public void setTyreType(String[] tyreType) {
        this.tyreType = tyreType;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public int getUsagId() {
        return usagId;
    }

    public void setUsagId(int usagId) {
        this.usagId = usagId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getHmReading() {
        return hmReading;
    }

    public void setHmReading(String hmReading) {
        this.hmReading = hmReading;
    }

    public String getKmReading() {
        return kmReading;
    }

    public void setKmReading(String kmReading) {
        this.kmReading = kmReading;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getDailyHm() {
        return dailyHm;
    }

    public void setDailyHm(String dailyHm) {
        this.dailyHm = dailyHm;
    }

    public String getDailyKm() {
        return dailyKm;
    }

    public void setDailyKm(String dailyKm) {
        this.dailyKm = dailyKm;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String getTyreId() {
        return tyreId;
    }

    public void setTyreId(String tyreId) {
        this.tyreId = tyreId;
    }

    public String[] getTyreIds() {
        return tyreIds;
    }

    public void setTyreIds(String[] tyreIds) {
        this.tyreIds = tyreIds;
    }

    public String getPosId() {
        return posId;
    }

    public void setPosId(String posId) {
        this.posId = posId;
    }

    public String getPosName() {
        return posName;
    }

    public void setPosName(String posName) {
        this.posName = posName;
    }

    public String[] getTyreNos() {
        return tyreNos;
    }

    public void setTyreNos(String[] tyreNos) {
        this.tyreNos = tyreNos;
    }

    public String getPercentage() {
        return percentage;
    }

    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public String[] getRegNos() {
        return regNos;
    }

    public void setRegNos(String[] regNos) {
        this.regNos = regNos;
    }

    public String getActivityGroupId() {
        return activityGroupId;
    }

    public void setActivityGroupId(String activityGroupId) {
        this.activityGroupId = activityGroupId;
    }

    public String getBodyWorkGroupId() {
        return bodyWorkGroupId;
    }

    public void setBodyWorkGroupId(String bodyWorkGroupId) {
        this.bodyWorkGroupId = bodyWorkGroupId;
    }

    public String getVehicleColor() {
        return vehicleColor;
    }

    public void setVehicleColor(String vehicleColor) {
        this.vehicleColor = vehicleColor;
    }

    public String getVehicleCost() {
        return vehicleCost;
    }

    public void setVehicleCost(String vehicleCost) {
        this.vehicleCost = vehicleCost;
    }

    public String getVehicleDepreciation() {
        return vehicleDepreciation;
    }

    public void setVehicleDepreciation(String vehicleDepreciation) {
        this.vehicleDepreciation = vehicleDepreciation;
    }

    public String getInsuarnceNCB() {
        return insuarnceNCB;
    }

    public void setInsuarnceNCB(String insuarnceNCB) {
        this.insuarnceNCB = insuarnceNCB;
    }

    public String getInsuarnceRemarks() {
        return insuarnceRemarks;
    }

    public void setInsuarnceRemarks(String insuarnceRemarks) {
        this.insuarnceRemarks = insuarnceRemarks;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public String getPremiumExpiryDate() {
        return premiumExpiryDate;
    }

    public void setPremiumExpiryDate(String premiumExpiryDate) {
        this.premiumExpiryDate = premiumExpiryDate;
    }

    public String getPremiumNo() {
        return premiumNo;
    }

    public void setPremiumNo(String premiumNo) {
        this.premiumNo = premiumNo;
    }

    public double getPremiumPaidAmount() {
        return premiumPaidAmount;
    }

    public void setPremiumPaidAmount(double premiumPaidAmount) {
        this.premiumPaidAmount = premiumPaidAmount;
    }

    public String getPremiumPaidDate() {
        return premiumPaidDate;
    }

    public void setPremiumPaidDate(String premiumPaidDate) {
        this.premiumPaidDate = premiumPaidDate;
    }

    public double getVehicleValue() {
        return vehicleValue;
    }

    public void setVehicleValue(double vehicleValue) {
        this.vehicleValue = vehicleValue;
    }

    public String getAccidentDate() {
        return accidentDate;
    }

    public void setAccidentDate(String accidentDate) {
        this.accidentDate = accidentDate;
    }

    public String getAccidentRemarks() {
        return accidentRemarks;
    }

    public void setAccidentRemarks(String accidentRemarks) {
        this.accidentRemarks = accidentRemarks;
    }

    public String getAccidentSpot() {
        return accidentSpot;
    }

    public void setAccidentSpot(String accidentSpot) {
        this.accidentSpot = accidentSpot;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getFirDate() {
        return firDate;
    }

    public void setFirDate(String firDate) {
        this.firDate = firDate;
    }

    public String getFirNo() {
        return firNo;
    }

    public void setFirNo(String firNo) {
        this.firNo = firNo;
    }

    public String getFirPreparedBy() {
        return firPreparedBy;
    }

    public void setFirPreparedBy(String firPreparedBy) {
        this.firPreparedBy = firPreparedBy;
    }

    public String getPoliceStation() {
        return policeStation;
    }

    public void setPoliceStation(String policeStation) {
        this.policeStation = policeStation;
    }

    public double getRoadTaxPaidAmount() {
        return roadTaxPaidAmount;
    }

    public void setRoadTaxPaidAmount(double roadTaxPaidAmount) {
        this.roadTaxPaidAmount = roadTaxPaidAmount;
    }

    public String getRoadTaxPaidLocation() {
        return roadTaxPaidLocation;
    }

    public void setRoadTaxPaidLocation(String roadTaxPaidLocation) {
        this.roadTaxPaidLocation = roadTaxPaidLocation;
    }

    public String getRoadTaxPaidOffice() {
        return roadTaxPaidOffice;
    }

    public void setRoadTaxPaidOffice(String roadTaxPaidOffice) {
        this.roadTaxPaidOffice = roadTaxPaidOffice;
    }

    public String getRoadTaxPeriod() {
        return roadTaxPeriod;
    }

    public void setRoadTaxPeriod(String roadTaxPeriod) {
        this.roadTaxPeriod = roadTaxPeriod;
    }

    public String getRoadTaxReceiptDate() {
        return roadTaxReceiptDate;
    }

    public void setRoadTaxReceiptDate(String roadTaxReceiptDate) {
        this.roadTaxReceiptDate = roadTaxReceiptDate;
    }

    public String getRoadTaxReceiptNo() {
        return roadTaxReceiptNo;
    }

    public void setRoadTaxReceiptNo(String roadTaxReceiptNo) {
        this.roadTaxReceiptNo = roadTaxReceiptNo;
    }

    public String getRoadTaxRemarks() {
        return roadTaxRemarks;
    }

    public void setRoadTaxRemarks(String roadTaxRemarks) {
        this.roadTaxRemarks = roadTaxRemarks;
    }

    public double getFcAmount() {
        return fcAmount;
    }

    public void setFcAmount(double fcAmount) {
        this.fcAmount = fcAmount;
    }

    public String getFcDate() {
        return fcDate;
    }

    public void setFcDate(String fcDate) {
        this.fcDate = fcDate;
    }

    public String getFcExpiryDate() {
        return fcExpiryDate;
    }

    public void setFcExpiryDate(String fcExpiryDate) {
        this.fcExpiryDate = fcExpiryDate;
    }

    public String getFcReceiptNo() {
        return fcReceiptNo;
    }

    public void setFcReceiptNo(String fcReceiptNo) {
        this.fcReceiptNo = fcReceiptNo;
    }

    public String getFcRemarks() {
        return fcRemarks;
    }

    public void setFcRemarks(String fcRemarks) {
        this.fcRemarks = fcRemarks;
    }

    public double getServiceCost() {
        return serviceCost;
    }

    public void setServiceCost(double serviceCost) {
        this.serviceCost = serviceCost;
    }

    public String getRtoDetail() {
        return rtoDetail;
    }

    public void setRtoDetail(String rtoDetail) {
        this.rtoDetail = rtoDetail;
    }

    public double getTripRunKm() {
        return tripRunKm;
    }

    public void setTripRunKm(double tripRunKm) {
        this.tripRunKm = tripRunKm;
    }

    //Rathimeena route Master Start
    public String getDriverBata() {
        return driverBata;
    }

    public void setDriverBata(String driverBata) {
        this.driverBata = driverBata;
    }

    public String getEligibleTrip() {
        return eligibleTrip;
    }

    public void setEligibleTrip(String eligibleTrip) {
        this.eligibleTrip = eligibleTrip;
    }

    public String getKm() {
        return km;
    }

    public void setKm(String km) {
        this.km = km;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public String getFromLocation() {
        return fromLocation;
    }

    public void setFromLocation(String fromLocation) {
        this.fromLocation = fromLocation;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getTonnageRate() {
        return tonnageRate;
    }

    public void setTonnageRate(String tonnageRate) {
        this.tonnageRate = tonnageRate;
    }

    public String getViaRoute() {
        return viaRoute;
    }

    public void setViaRoute(String viaRoute) {
        this.viaRoute = viaRoute;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getDriveBata() {
        return driveBata;
    }

    public void setDriveBata(String driveBata) {
        this.driveBata = driveBata;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getRouteKm() {
        return routeKm;
    }

    public void setRouteKm(String routeKm) {
        this.routeKm = routeKm;
    }

    public String getRouteToll() {
        return routeToll;
    }

    public void setRouteToll(String routeToll) {
        this.routeToll = routeToll;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public String getFcamount() {
        return fcamount;
    }

    public void setFcamount(String fcamount) {
        this.fcamount = fcamount;
    }

    public String getFcdate() {
        return fcdate;
    }

    public void setFcdate(String fcdate) {
        this.fcdate = fcdate;
    }

    public String getFcexpirydate() {
        return fcexpirydate;
    }

    public void setFcexpirydate(String fcexpirydate) {
        this.fcexpirydate = fcexpirydate;
    }

    public String getFcid() {
        return fcid;
    }

    public void setFcid(String fcid) {
        this.fcid = fcid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInsuranceid() {
        return insuranceid;
    }

    public void setInsuranceid(String insuranceid) {
        this.insuranceid = insuranceid;
    }

    public String getNcb() {
        return ncb;
    }

    public void setNcb(String ncb) {
        this.ncb = ncb;
    }

    public String getPremiumamount() {
        return premiumamount;
    }

    public void setPremiumamount(String premiumamount) {
        this.premiumamount = premiumamount;
    }

    public String getPremiumno() {
        return premiumno;
    }

    public void setPremiumno(String premiumno) {
        this.premiumno = premiumno;
    }

    public String getPremiumpaiddate() {
        return premiumpaiddate;
    }

    public void setPremiumpaiddate(String premiumpaiddate) {
        this.premiumpaiddate = premiumpaiddate;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getRoadtaxamount() {
        return roadtaxamount;
    }

    public void setRoadtaxamount(String roadtaxamount) {
        this.roadtaxamount = roadtaxamount;
    }

    public String getRoadtaxpaidlocation() {
        return roadtaxpaidlocation;
    }

    public void setRoadtaxpaidlocation(String roadtaxpaidlocation) {
        this.roadtaxpaidlocation = roadtaxpaidlocation;
    }

    public String getRoadtaxperiod() {
        return roadtaxperiod;
    }

    public void setRoadtaxperiod(String roadtaxperiod) {
        this.roadtaxperiod = roadtaxperiod;
    }

    public String getRoadtaxreceiptdate() {
        return roadtaxreceiptdate;
    }

    public void setRoadtaxreceiptdate(String roadtaxreceiptdate) {
        this.roadtaxreceiptdate = roadtaxreceiptdate;
    }

    public String getRoadtaxreceiptno() {
        return roadtaxreceiptno;
    }

    public void setRoadtaxreceiptno(String roadtaxreceiptno) {
        this.roadtaxreceiptno = roadtaxreceiptno;
    }

    public String getRtodetail() {
        return rtodetail;
    }

    public void setRtodetail(String rtodetail) {
        this.rtodetail = rtodetail;
    }

    public String getVehiclevalue() {
        return vehiclevalue;
    }

    public void setVehiclevalue(String vehiclevalue) {
        this.vehiclevalue = vehiclevalue;
    }

    public double getPermitAmount() {
        return permitAmount;
    }

    public void setPermitAmount(double permitAmount) {
        this.permitAmount = permitAmount;
    }

    public String getPermitExpiryDate() {
        return permitExpiryDate;
    }

    public void setPermitExpiryDate(String permitExpiryDate) {
        this.permitExpiryDate = permitExpiryDate;
    }

    public String getPermitNo() {
        return permitNo;
    }

    public void setPermitNo(String permitNo) {
        this.permitNo = permitNo;
    }

    public String getPermitPaidDate() {
        return permitPaidDate;
    }

    public void setPermitPaidDate(String permitPaidDate) {
        this.permitPaidDate = permitPaidDate;
    }

    public String getPermitRemarks() {
        return permitRemarks;
    }

    public void setPermitRemarks(String permitRemarks) {
        this.permitRemarks = permitRemarks;
    }

    public String getPermitType() {
        return permitType;
    }

    public void setPermitType(String permitType) {
        this.permitType = permitType;
    }

    public String getPermitid() {
        return permitid;
    }

    public void setPermitid(String permitid) {
        this.permitid = permitid;
    }

    public String getAgreementYear() {
        return agreementYear;
    }

    public void setAgreementYear(String agreementYear) {
        this.agreementYear = agreementYear;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getAsset() {
        return asset;
    }

    public void setAsset(String asset) {
        this.asset = asset;
    }

    public String getLeasingCustId() {
        return leasingCustId;
    }

    public void setLeasingCustId(String leasingCustId) {
        this.leasingCustId = leasingCustId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public String getOwnership() {
        return ownership;
    }

    public void setOwnership(String ownership) {
        this.ownership = ownership;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getBankerAddress() {
        return bankerAddress;
    }

    public void setBankerAddress(String bankerAddress) {
        this.bankerAddress = bankerAddress;
    }

    public String getBankerName() {
        return bankerName;
    }

    public void setBankerName(String bankerName) {
        this.bankerName = bankerName;
    }

    public String getEmiAmount() {
        return emiAmount;
    }

    public void setEmiAmount(String emiAmount) {
        this.emiAmount = emiAmount;
    }

    public String getEmiEndDate() {
        return emiEndDate;
    }

    public void setEmiEndDate(String emiEndDate) {
        this.emiEndDate = emiEndDate;
    }

    public String getEmiMonths() {
        return emiMonths;
    }

    public void setEmiMonths(String emiMonths) {
        this.emiMonths = emiMonths;
    }

    public String getEmiPayDay() {
        return emiPayDay;
    }

    public void setEmiPayDay(String emiPayDay) {
        this.emiPayDay = emiPayDay;
    }

    public String getEmiStartDate() {
        return emiStartDate;
    }

    public void setEmiStartDate(String emiStartDate) {
        this.emiStartDate = emiStartDate;
    }

    public String getFinanceAmount() {
        return financeAmount;
    }

    public void setFinanceAmount(String financeAmount) {
        this.financeAmount = financeAmount;
    }

    public String getInterestType() {
        return interestType;
    }

    public void setInterestType(String interestType) {
        this.interestType = interestType;
    }

    public String getPayMode() {
        return payMode;
    }

    public void setPayMode(String payMode) {
        this.payMode = payMode;
    }

    public String getRoi() {
        return roi;
    }

    public void setRoi(String roi) {
        this.roi = roi;
    }

    public String getInsExpiryDate() {
        return insExpiryDate;
    }

    public void setInsExpiryDate(String insExpiryDate) {
        this.insExpiryDate = insExpiryDate;
    }

    public String getInsPremiumAmount() {
        return insPremiumAmount;
    }

    public void setInsPremiumAmount(String insPremiumAmount) {
        this.insPremiumAmount = insPremiumAmount;
    }

    public String getAmcAmount() {
        return amcAmount;
    }

    public void setAmcAmount(String amcAmount) {
        this.amcAmount = amcAmount;
    }

    public String getAmcChequeDate() {
        return amcChequeDate;
    }

    public void setAmcChequeDate(String amcChequeDate) {
        this.amcChequeDate = amcChequeDate;
    }

    public String getAmcChequeNo() {
        return amcChequeNo;
    }

    public void setAmcChequeNo(String amcChequeNo) {
        this.amcChequeNo = amcChequeNo;
    }

    public String getAmcCompanyName() {
        return amcCompanyName;
    }

    public void setAmcCompanyName(String amcCompanyName) {
        this.amcCompanyName = amcCompanyName;
    }

    public String getAmcDuration() {
        return amcDuration;
    }

    public void setAmcDuration(String amcDuration) {
        this.amcDuration = amcDuration;
    }

    public String getAmcFromDate() {
        return amcFromDate;
    }

    public void setAmcFromDate(String amcFromDate) {
        this.amcFromDate = amcFromDate;
    }

    public String getAmcId() {
        return amcId;
    }

    public void setAmcId(String amcId) {
        this.amcId = amcId;
    }

    public String getAmcToDate() {
        return amcToDate;
    }

    public void setAmcToDate(String amcToDate) {
        this.amcToDate = amcToDate;
    }

    public String getAmcRemarks() {
        return amcRemarks;
    }

    public void setAmcRemarks(String amcRemarks) {
        this.amcRemarks = amcRemarks;
    }

    public String getVehicleMake() {
        return vehicleMake;
    }

    public void setVehicleMake(String vehicleMake) {
        this.vehicleMake = vehicleMake;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getVehicleUsage() {
        return vehicleUsage;
    }

    public void setVehicleUsage(String vehicleUsage) {
        this.vehicleUsage = vehicleUsage;
    }

    public String getNextRoadTaxDate() {
        return nextRoadTaxDate;
    }

    public void setNextRoadTaxDate(String nextRoadTaxDate) {
        this.nextRoadTaxDate = nextRoadTaxDate;
    }

    public String getRoadTaxFromDate() {
        return roadTaxFromDate;
    }

    public void setRoadTaxFromDate(String roadTaxFromDate) {
        this.roadTaxFromDate = roadTaxFromDate;
    }

    public String getFinanceId() {
        return financeId;
    }

    public void setFinanceId(String financeId) {
        this.financeId = financeId;
    }

    public String getGpsSystem() {
        return gpsSystem;
    }

    public void setGpsSystem(String gpsSystem) {
        this.gpsSystem = gpsSystem;
    }

    public String getWarrantyDate() {
        return warrantyDate;
    }

    public void setWarrantyDate(String warrantyDate) {
        this.warrantyDate = warrantyDate;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getRoadTaxAmount() {
        return roadTaxAmount;
    }

    public void setRoadTaxAmount(String roadTaxAmount) {
        this.roadTaxAmount = roadTaxAmount;
    }

    public String getRoadTaxToDate() {
        return roadTaxToDate;
    }

    public void setRoadTaxToDate(String roadTaxToDate) {
        this.roadTaxToDate = roadTaxToDate;
    }

    public String[] getTyreDate() {
        return tyreDate;
    }

    public void setTyreDate(String[] tyreDate) {
        this.tyreDate = tyreDate;
    }

    public String getTyreD() {
        return tyreD;
    }

    public void setTyreD(String tyreD) {
        this.tyreD = tyreD;
    }

    public String getMarketVehiclePercentage() {
        return marketVehiclePercentage;
    }

    public void setMarketVehiclePercentage(String marketVehiclePercentage) {
        this.marketVehiclePercentage = marketVehiclePercentage;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getSla() {
        return sla;
    }

    public void setSla(String sla) {
        this.sla = sla;
    }

    public String getFromLocationId() {
        return fromLocationId;
    }

    public void setFromLocationId(String fromLocationId) {
        this.fromLocationId = fromLocationId;
    }

    public String getToLocationId() {
        return toLocationId;
    }

    public void setToLocationId(String toLocationId) {
        this.toLocationId = toLocationId;
    }

    public String getToLocationid() {
        return ToLocationid;
    }

    public void setToLocationid(String ToLocationid) {
        this.ToLocationid = ToLocationid;
    }

    public String getTonnageRateMarket() {
        return tonnageRateMarket;
    }

    public void setTonnageRateMarket(String tonnageRateMarket) {
        this.tonnageRateMarket = tonnageRateMarket;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getDepot() {
        return Depot;
    }

    public void setDepot(String Depot) {
        this.Depot = Depot;
    }

    public String getDepotVal() {
        return depotVal;
    }

    public void setDepotVal(String depotVal) {
        this.depotVal = depotVal;
    }
    //Rathimeena route Master End

    //Brattle Foods
    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    //Brattle Foods
    public String getAxles() {
        return axles;
    }

    public void setAxles(String axles) {
        this.axles = axles;
    }

    public String getDailyRunKm() {
        return dailyRunKm;
    }

    public void setDailyRunKm(String dailyRunKm) {
        this.dailyRunKm = dailyRunKm;
    }

    public String getDailyRunHm() {
        return dailyRunHm;
    }

    public void setDailyRunHm(String dailyRunHm) {
        this.dailyRunHm = dailyRunHm;
    }

    public String getGpsSystemId() {
        return gpsSystemId;
    }

    public void setGpsSystemId(String gpsSystemId) {
        this.gpsSystemId = gpsSystemId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getcType() {
        return cType;
    }

    public void setcType(String cType) {
        this.cType = cType;
    }

    public String geteRegNo() {
        return eRegNo;
    }

    public void seteRegNo(String eRegNo) {
        this.eRegNo = eRegNo;
    }

    public String getrDate() {
        return rDate;
    }

    public void setrDate(String rDate) {
        this.rDate = rDate;
    }

    public String getrRegNo() {
        return rRegNo;
    }

    public void setrRegNo(String rRegNo) {
        this.rRegNo = rRegNo;
    }

    public String getvName() {
        return vName;
    }

    public void setvName(String vName) {
        this.vName = vName;
    }

    public String getOwnerShips() {
        return ownerShips;
    }

    public void setOwnerShips(String ownerShips) {
        this.ownerShips = ownerShips;
    }

    public String getTonnage() {
        return tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String[] getTonnages() {
        return tonnages;
    }

    public void setTonnages(String[] tonnages) {
        this.tonnages = tonnages;
    }

    public String[] getCapacities() {
        return capacities;
    }

    public void setCapacities(String[] capacities) {
        this.capacities = capacities;
    }

    public String getAxleDetailId() {
        return axleDetailId;
    }

    public void setAxleDetailId(String axleDetailId) {
        this.axleDetailId = axleDetailId;
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    public String getPositionNo() {
        return positionNo;
    }

    public void setPositionNo(String positionNo) {
        this.positionNo = positionNo;
    }

    public String getUpdateValue() {
        return updateValue;
    }

    public void setUpdateValue(String updateValue) {
        this.updateValue = updateValue;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public String getAxleId() {
        return axleId;
    }

    public void setAxleId(String axleId) {
        this.axleId = axleId;
    }

    public String getVehicleAxleId() {
        return vehicleAxleId;
    }

    public void setVehicleAxleId(String vehicleAxleId) {
        this.vehicleAxleId = vehicleAxleId;
    }

    public String getAxleTypeId() {
        return axleTypeId;
    }

    public void setAxleTypeId(String axleTypeId) {
        this.axleTypeId = axleTypeId;
    }

    public String getAxleTypeName() {
        return axleTypeName;
    }

    public void setAxleTypeName(String axleTypeName) {
        this.axleTypeName = axleTypeName;
    }

    public String getAxleCount() {
        return axleCount;
    }

    public void setAxleCount(String axleCount) {
        this.axleCount = axleCount;
    }

    public String getVehicleFront() {
        return vehicleFront;
    }

    public void setVehicleFront(String vehicleFront) {
        this.vehicleFront = vehicleFront;
    }

    public String getAxleLeft() {
        return axleLeft;
    }

    public void setAxleLeft(String axleLeft) {
        this.axleLeft = axleLeft;
    }

    public String getAxleRight() {
        return axleRight;
    }

    public void setAxleRight(String axleRight) {
        this.axleRight = axleRight;
    }

    public String getInsPolicy() {
        return insPolicy;
    }

    public void setInsPolicy(String insPolicy) {
        this.insPolicy = insPolicy;
    }

    public String getPrevPolicy() {
        return prevPolicy;
    }

    public void setPrevPolicy(String prevPolicy) {
        this.prevPolicy = prevPolicy;
    }

    public String getPackageType() {
        return packageType;
    }

    public void setPackageType(String packageType) {
        this.packageType = packageType;
    }

    public String getInsName() {
        return insName;
    }

    public void setInsName(String insName) {
        this.insName = insName;
    }

    public String getInsAddress() {
        return insAddress;
    }

    public void setInsAddress(String insAddress) {
        this.insAddress = insAddress;
    }

    public String getInsMobileNo() {
        return insMobileNo;
    }

    public void setInsMobileNo(String insMobileNo) {
        this.insMobileNo = insMobileNo;
    }

    public String getInsPolicyNo() {
        return insPolicyNo;
    }

    public void setInsPolicyNo(String insPolicyNo) {
        this.insPolicyNo = insPolicyNo;
    }

    public String getInsIdvValue() {
        return insIdvValue;
    }

    public void setInsIdvValue(String insIdvValue) {
        this.insIdvValue = insIdvValue;
    }

    public String getFromDate1() {
        return fromDate1;
    }

    public void setFromDate1(String fromDate1) {
        this.fromDate1 = fromDate1;
    }

    public String getToDate1() {
        return toDate1;
    }

    public void setToDate1(String toDate1) {
        this.toDate1 = toDate1;
    }

    public String getPremiunAmount() {
        return premiunAmount;
    }

    public void setPremiunAmount(String premiunAmount) {
        this.premiunAmount = premiunAmount;
    }

    public String getRoadTaxId() {
        return roadTaxId;
    }

    public void setRoadTaxId(String roadTaxId) {
        this.roadTaxId = roadTaxId;
    }

    public String getVehicleAmt() {
        return vehicleAmt;
    }

    public void setVehicleAmt(String vehicleAmt) {
        this.vehicleAmt = vehicleAmt;
    }

    public String getMonthlyAmount() {
        return monthlyAmount;
    }

    public void setMonthlyAmount(String monthlyAmount) {
        this.monthlyAmount = monthlyAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getEmiPayer() {
        return emiPayer;
    }

    public void setEmiPayer(String emiPayer) {
        this.emiPayer = emiPayer;
    }

    public String getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(String amountPaid) {
        this.amountPaid = amountPaid;
    }

    public String getPaymentMadeOn() {
        return paymentMadeOn;
    }

    public void setPaymentMadeOn(String paymentMadeOn) {
        this.paymentMadeOn = paymentMadeOn;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getEmiAmtPaid() {
        return emiAmtPaid;
    }

    public void setEmiAmtPaid(String emiAmtPaid) {
        this.emiAmtPaid = emiAmtPaid;
    }

    public String getBalanceEmi() {
        return balanceEmi;
    }

    public void setBalanceEmi(String balanceEmi) {
        this.balanceEmi = balanceEmi;
    }

    public String getLastEmiBalance() {
        return lastEmiBalance;
    }

    public void setLastEmiBalance(String lastEmiBalance) {
        this.lastEmiBalance = lastEmiBalance;
    }

    public String getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(String totalBalance) {
        this.totalBalance = totalBalance;
    }

    public String getVehicleYear() {
        return vehicleYear;
    }

    public void setVehicleYear(String vehicleYear) {
        this.vehicleYear = vehicleYear;
    }

    public String getDepreciation() {
        return depreciation;
    }

    public void setDepreciation(String depreciation) {
        this.depreciation = depreciation;
    }

    public String getYearCost() {
        return yearCost;
    }

    public void setYearCost(String yearCost) {
        this.yearCost = yearCost;
    }

    public String getPerMonth() {
        return perMonth;
    }

    public void setPerMonth(String perMonth) {
        this.perMonth = perMonth;
    }

    public String getDepreciationId() {
        return depreciationId;
    }

    public void setDepreciationId(String depreciationId) {
        this.depreciationId = depreciationId;
    }

    public String getDepthVal() {
        return depthVal;
    }

    public void setDepthVal(String depthVal) {
        this.depthVal = depthVal;
    }

    public String getAcc_SurveyorName() {
        return acc_SurveyorName;
    }

    public void setAcc_SurveyorName(String acc_SurveyorName) {
        this.acc_SurveyorName = acc_SurveyorName;
    }

    public String getPloice_Address() {
        return ploice_Address;
    }

    public void setPloice_Address(String ploice_Address) {
        this.ploice_Address = ploice_Address;
    }

    public String getAccident_Id() {
        return accident_Id;
    }

    public void setAccident_Id(String accident_Id) {
        this.accident_Id = accident_Id;
    }

    public String getAcc_VehicleNo() {
        return acc_VehicleNo;
    }

    public void setAcc_VehicleNo(String acc_VehicleNo) {
        this.acc_VehicleNo = acc_VehicleNo;
    }

    public String getAcc_VehicleDetail() {
        return acc_VehicleDetail;
    }

    public void setAcc_VehicleDetail(String acc_VehicleDetail) {
        this.acc_VehicleDetail = acc_VehicleDetail;
    }

    public String getAcc_DriverName() {
        return acc_DriverName;
    }

    public void setAcc_DriverName(String acc_DriverName) {
        this.acc_DriverName = acc_DriverName;
    }

    public String getAcc_DriverLicNo() {
        return acc_DriverLicNo;
    }

    public void setAcc_DriverLicNo(String acc_DriverLicNo) {
        this.acc_DriverLicNo = acc_DriverLicNo;
    }

    public String getAcc_InsurerName() {
        return acc_InsurerName;
    }

    public void setAcc_InsurerName(String acc_InsurerName) {
        this.acc_InsurerName = acc_InsurerName;
    }

    public String getAcc_PolicyNo() {
        return acc_PolicyNo;
    }

    public void setAcc_PolicyNo(String acc_PolicyNo) {
        this.acc_PolicyNo = acc_PolicyNo;
    }

    public String getAcc_ContactDetail() {
        return acc_ContactDetail;
    }

    public void setAcc_ContactDetail(String acc_ContactDetail) {
        this.acc_ContactDetail = acc_ContactDetail;
    }

    public String getAcc_ContactPerson() {
        return acc_ContactPerson;
    }

    public void setAcc_ContactPerson(String acc_ContactPerson) {
        this.acc_ContactPerson = acc_ContactPerson;
    }

    public String getCasualty_Name() {
        return casualty_Name;
    }

    public void setCasualty_Name(String casualty_Name) {
        this.casualty_Name = casualty_Name;
    }

    public String getCasualty_Address() {
        return casualty_Address;
    }

    public void setCasualty_Address(String casualty_Address) {
        this.casualty_Address = casualty_Address;
    }

    public String getCasualty_ConDetail() {
        return casualty_ConDetail;
    }

    public void setCasualty_ConDetail(String casualty_ConDetail) {
        this.casualty_ConDetail = casualty_ConDetail;
    }

    public String getCasualty_Type() {
        return casualty_Type;
    }

    public void setCasualty_Type(String casualty_Type) {
        this.casualty_Type = casualty_Type;
    }

    public String getCasualty_AlterNo() {
        return casualty_AlterNo;
    }

    public void setCasualty_AlterNo(String casualty_AlterNo) {
        this.casualty_AlterNo = casualty_AlterNo;
    }

    public String getCasualty_ClaimDetail() {
        return casualty_ClaimDetail;
    }

    public void setCasualty_ClaimDetail(String casualty_ClaimDetail) {
        this.casualty_ClaimDetail = casualty_ClaimDetail;
    }

    public String getAcc_ReferenceNo() {
        return acc_ReferenceNo;
    }

    public void setAcc_ReferenceNo(String acc_ReferenceNo) {
        this.acc_ReferenceNo = acc_ReferenceNo;
    }

    public String getAcc_SurveyorNo() {
        return acc_SurveyorNo;
    }

    public void setAcc_SurveyorNo(String acc_SurveyorNo) {
        this.acc_SurveyorNo = acc_SurveyorNo;
    }

    public String getAcc_FirNo() {
        return acc_FirNo;
    }

    public void setAcc_FirNo(String acc_FirNo) {
        this.acc_FirNo = acc_FirNo;
    }

    public String getWiteness_Name() {
        return witeness_Name;
    }

    public void setWiteness_Name(String witeness_Name) {
        this.witeness_Name = witeness_Name;
    }

    public String getWiteness_Address() {
        return witeness_Address;
    }

    public void setWiteness_Address(String witeness_Address) {
        this.witeness_Address = witeness_Address;
    }

    public String getWiteness_Contact() {
        return witeness_Contact;
    }

    public void setWiteness_Contact(String witeness_Contact) {
        this.witeness_Contact = witeness_Contact;
    }

    public String getRto_Fine() {
        return rto_Fine;
    }

    public void setRto_Fine(String rto_Fine) {
        this.rto_Fine = rto_Fine;
    }

    public String getRto_StationRelease() {
        return rto_StationRelease;
    }

    public void setRto_StationRelease(String rto_StationRelease) {
        this.rto_StationRelease = rto_StationRelease;
    }

    public String getRto_Date() {
        return rto_Date;
    }

    public void setRto_Date(String rto_Date) {
        this.rto_Date = rto_Date;
    }

    public String getRto_Upload() {
        return rto_Upload;
    }

    public void setRto_Upload(String rto_Upload) {
        this.rto_Upload = rto_Upload;
    }

    public String getWorkshop_Type() {
        return workshop_Type;
    }

    public void setWorkshop_Type(String workshop_Type) {
        this.workshop_Type = workshop_Type;
    }

    public String getIns_Tie_up() {
        return ins_Tie_up;
    }

    public void setIns_Tie_up(String ins_Tie_up) {
        this.ins_Tie_up = ins_Tie_up;
    }

    public String getAdhoc_Type() {
        return adhoc_Type;
    }

    public void setAdhoc_Type(String adhoc_Type) {
        this.adhoc_Type = adhoc_Type;
    }

    public String getSurv_InspectionDate() {
        return surv_InspectionDate;
    }

    public void setSurv_InspectionDate(String surv_InspectionDate) {
        this.surv_InspectionDate = surv_InspectionDate;
    }

    public String getSurv_QuotationAmt() {
        return surv_QuotationAmt;
    }

    public void setSurv_QuotationAmt(String surv_QuotationAmt) {
        this.surv_QuotationAmt = surv_QuotationAmt;
    }

    public String getSurv_InsurerName() {
        return surv_InsurerName;
    }

    public void setSurv_InsurerName(String surv_InsurerName) {
        this.surv_InsurerName = surv_InsurerName;
    }

    public String getSurv_PolicyNo() {
        return surv_PolicyNo;
    }

    public void setSurv_PolicyNo(String surv_PolicyNo) {
        this.surv_PolicyNo = surv_PolicyNo;
    }

    public String getReim_Date() {
        return reim_Date;
    }

    public void setReim_Date(String reim_Date) {
        this.reim_Date = reim_Date;
    }

    public String getReim_Amt() {
        return reim_Amt;
    }

    public void setReim_Amt(String reim_Amt) {
        this.reim_Amt = reim_Amt;
    }

    public String getReim_VehicleExcpen() {
        return reim_VehicleExcpen;
    }

    public void setReim_VehicleExcpen(String reim_VehicleExcpen) {
        this.reim_VehicleExcpen = reim_VehicleExcpen;
    }

    public String getReim_BillAmt() {
        return reim_BillAmt;
    }

    public void setReim_BillAmt(String reim_BillAmt) {
        this.reim_BillAmt = reim_BillAmt;
    }

    public String getFileName1() {
        return fileName1;
    }

    public void setFileName1(String fileName1) {
        this.fileName1 = fileName1;
    }

    public String getFileName2() {
        return fileName2;
    }

    public void setFileName2(String fileName2) {
        this.fileName2 = fileName2;
    }

    public String getFileName3() {
        return fileName3;
    }

    public void setFileName3(String fileName3) {
        this.fileName3 = fileName3;
    }

    public String getVehicleStatus() {
        return vehicleStatus;
    }

    public void setVehicleStatus(String vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }

    public String getLawyerName() {
        return lawyerName;
    }

    public void setLawyerName(String lawyerName) {
        this.lawyerName = lawyerName;
    }

    public String getLaw_ContactNo() {
        return law_ContactNo;
    }

    public void setLaw_ContactNo(String law_ContactNo) {
        this.law_ContactNo = law_ContactNo;
    }

    public String getLaw_court_Address() {
        return law_court_Address;
    }

    public void setLaw_court_Address(String law_court_Address) {
        this.law_court_Address = law_court_Address;
    }

    public String getLaw_Fees() {
        return law_Fees;
    }

    public void setLaw_Fees(String law_Fees) {
        this.law_Fees = law_Fees;
    }

    public String getMiscels_Fees() {
        return miscels_Fees;
    }

    public void setMiscels_Fees(String miscels_Fees) {
        this.miscels_Fees = miscels_Fees;
    }

    public String getIns_Insurer() {
        return ins_Insurer;
    }

    public void setIns_Insurer(String ins_Insurer) {
        this.ins_Insurer = ins_Insurer;
    }

    public String getAdhoc_BillAmt() {
        return adhoc_BillAmt;
    }

    public void setAdhoc_BillAmt(String adhoc_BillAmt) {
        this.adhoc_BillAmt = adhoc_BillAmt;
    }

    public String getIns_Owner() {
        return ins_Owner;
    }

    public void setIns_Owner(String ins_Owner) {
        this.ins_Owner = ins_Owner;
    }

    public String getAdhoc_PaidAmt() {
        return adhoc_PaidAmt;
    }

    public void setAdhoc_PaidAmt(String adhoc_PaidAmt) {
        this.adhoc_PaidAmt = adhoc_PaidAmt;
    }

    public int getAccInsertStat() {
        return accInsertStat;
    }

    public void setAccInsertStat(int accInsertStat) {
        this.accInsertStat = accInsertStat;
    }

    public String getSpotAddress() {
        return spotAddress;
    }

    public void setSpotAddress(String spotAddress) {
        this.spotAddress = spotAddress;
    }

    public String getAccFirDate() {
        return accFirDate;
    }

    public void setAccFirDate(String accFirDate) {
        this.accFirDate = accFirDate;
    }

    public String getPerOfDisability() {
        return perOfDisability;
    }

    public void setPerOfDisability(String perOfDisability) {
        this.perOfDisability = perOfDisability;
    }

    public String getAccidentOccurance() {
        return accidentOccurance;
    }

    public void setAccidentOccurance(String accidentOccurance) {
        this.accidentOccurance = accidentOccurance;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getInsId() {
        return insId;
    }

    public void setInsId(String insId) {
        this.insId = insId;
    }

    public String getVehicleDetails() {
        return vehicleDetails;
    }

    public void setVehicleDetails(String vehicleDetails) {
        this.vehicleDetails = vehicleDetails;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getIns_policy() {
        return ins_policy;
    }

    public void setIns_policy(String ins_policy) {
        this.ins_policy = ins_policy;
    }

    public String getIns_Name() {
        return ins_Name;
    }

    public void setIns_Name(String ins_Name) {
        this.ins_Name = ins_Name;
    }

    public String getIns_Address() {
        return ins_Address;
    }

    public void setIns_Address(String ins_Address) {
        this.ins_Address = ins_Address;
    }

    public String getIns_MobileNo() {
        return ins_MobileNo;
    }

    public void setIns_MobileNo(String ins_MobileNo) {
        this.ins_MobileNo = ins_MobileNo;
    }

    public String getIns_PolicyNo() {
        return ins_PolicyNo;
    }

    public void setIns_PolicyNo(String ins_PolicyNo) {
        this.ins_PolicyNo = ins_PolicyNo;
    }

    public String getIns_IdvValue() {
        return ins_IdvValue;
    }

    public void setIns_IdvValue(String ins_IdvValue) {
        this.ins_IdvValue = ins_IdvValue;
    }

    public String getIssuingCmnyName() {
        return issuingCmnyName;
    }

    public void setIssuingCmnyName(String issuingCmnyName) {
        this.issuingCmnyName = issuingCmnyName;
    }

    public String getIns_AgentName() {
        return ins_AgentName;
    }

    public void setIns_AgentName(String ins_AgentName) {
        this.ins_AgentName = ins_AgentName;
    }

    public String getIns_AgentCode() {
        return ins_AgentCode;
    }

    public void setIns_AgentCode(String ins_AgentCode) {
        this.ins_AgentCode = ins_AgentCode;
    }

    public String getIns_AgentMobileNo() {
        return ins_AgentMobileNo;
    }

    public void setIns_AgentMobileNo(String ins_AgentMobileNo) {
        this.ins_AgentMobileNo = ins_AgentMobileNo;
    }

    public String getUploadFileType() {
        return uploadFileType;
    }

    public void setUploadFileType(String uploadFileType) {
        this.uploadFileType = uploadFileType;
    }

    

}
