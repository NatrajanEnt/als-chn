<%--
    Document   : customerProductMaster
    Created on : Dec 17, 2013, 4:27:20 PM
    Author     : admin
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript">



    function onKeyPressBlockCharacters(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /[a-zA-Z]+$/;

        return !reg.test(keychar);

    }





    function extractNumber(obj, decimalPlaces, allowNegative)
    {
        var temp = obj.value;

        // avoid changing things if already formatted correctly
        var reg0Str = '[0-9]*';
        if (decimalPlaces > 0) {
            reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
        } else if (decimalPlaces < 0) {
            reg0Str += '\\.?[0-9]*';
        }
        reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
        reg0Str = reg0Str + '$';
        var reg0 = new RegExp(reg0Str);
        if (reg0.test(temp))
            return true;

        // first replace all non numbers
        var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
        var reg1 = new RegExp(reg1Str, 'g');
        temp = temp.replace(reg1, '');

        if (allowNegative) {
            // replace extra negative
            var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
            var reg2 = /-/g;
            temp = temp.replace(reg2, '');
            if (hasNegative)
                temp = '-' + temp;
        }

        if (decimalPlaces != 0) {
            var reg3 = /\./g;
            var reg3Array = reg3.exec(temp);
            if (reg3Array != null) {
                // keep only first occurrence of .
                //  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
                var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
                reg3Right = reg3Right.replace(reg3, '');
                reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
                temp = temp.substring(0, reg3Array.index) + '.' + reg3Right;
            }
        }

        obj.value = temp;
    }
    function blockNonNumbers(obj, e, allowDecimal, allowNegative)
    {
        var key;
        var isCtrl = false;
        var keychar;
        var reg;

        if (window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
        }
        else if (e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
        }

        if (isNaN(key))
            return true;

        keychar = String.fromCharCode(key);

        // check for backspace or delete, or if Ctrl was pressed
        if (key == 8 || isCtrl)
        {
            return true;
        }

        reg = /\d/;
        var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : true;
        var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : true;

        return isFirstN || isFirstD || reg.test(keychar);
    }



    function submitpage()
    {
        var temp = "";
        var tempMinimumTemp = "";
        var tempMaximumTemperature = "";
        var tempReeferRequired = "";
        var temp1 = "";
        var temp1MinimumTemp = "";
        var temp1MaximumTemperature = "";
        var temp1ReeferRequired = "";
//        var temp1 = "";
        var index = document.getElementsByName('productSelect');
        var productCategorys = document.getElementsByName('productCategoryIds');
        var reeferRequired = document.getElementsByName('reeferRequireds');
        var minimumTemperature = document.getElementsByName('reeferMinimumTemperatures');
        var maximumTemperature = document.getElementsByName('reeferMaximumTemperatures');
        var cntr = 0;
        var counts = 0;
        for (var i = 0; i < index.length; i++) {
            //alert(selectedConsignment[i].checked);
            if (index[i].checked == true) {
                if (cntr == 0) {
                    temp = productCategorys[i].value;
                    tempMinimumTemp = minimumTemperature[i].value;
                    tempMaximumTemperature = maximumTemperature[i].value;
                    tempReeferRequired = reeferRequired[i].value;
                    document.getElementById("productCategory").value = '';
                    document.getElementById("productCategory").value = temp + "," + 0;
                    document.getElementById("tempMinimumTemp").value = '';
                    document.getElementById("tempMinimumTemp").value = tempMinimumTemp;
                    document.getElementById("tempMaximumTemperature").value = '';
                    document.getElementById("tempMaximumTemperature").value = tempMaximumTemperature;
                    document.getElementById("tempReeferRequired").value = '';
                    document.getElementById("tempReeferRequired").value = tempReeferRequired;
                } else {
                    temp = temp + "," + productCategorys[i].value;
                    document.getElementById("productCategory").value = '';
                    document.getElementById("productCategory").value = temp;

                    tempMinimumTemp = tempMinimumTemp + "," + minimumTemperature[i].value;
                    document.getElementById("tempMinimumTemp").value = '';
                    document.getElementById("tempMinimumTemp").value = tempMinimumTemp;

                    tempMaximumTemperature = tempMaximumTemperature + "," + maximumTemperature[i].value;
                    document.getElementById("tempMaximumTemperature").value = '';
                    document.getElementById("tempMaximumTemperature").value = tempMaximumTemperature;

                    tempReeferRequired = tempReeferRequired + "," + reeferRequired[i].value;
                    document.getElementById("tempReeferRequired").value = '';
                    document.getElementById("tempReeferRequired").value = tempReeferRequired;
                }
                cntr++;
            } else {
                if (counts == 0) {
                    temp1 = productCategorys[i].value;
                    temp1MinimumTemp = minimumTemperature[i].value;
                    temp1MaximumTemperature = maximumTemperature[i].value;
                    temp1ReeferRequired = reeferRequired[i].value;
                    document.getElementById("productCategoryUnchecked").value = '';
                    document.getElementById("productCategoryUnchecked").value = temp1 + "," + 0;
                    document.getElementById("temp1MinimumTemp").value = '';
                    document.getElementById("temp1MinimumTemp").value = temp1MinimumTemp;
                    document.getElementById("temp1MaximumTemperature").value = '';
                    document.getElementById("temp1MaximumTemperature").value = temp1MaximumTemperature;
                    document.getElementById("temp1ReeferRequired").value = '';
                    document.getElementById("temp1ReeferRequired").value = temp1ReeferRequired;
                } else {
                    temp1 = temp1 + "," + productCategorys[i].value;
                    document.getElementById("productCategoryUnchecked").value = '';
                    document.getElementById("productCategoryUnchecked").value = temp1;

                    temp1MinimumTemp = temp1MinimumTemp + "," + minimumTemperature[i].value;
                    document.getElementById("temp1MinimumTemp").value = '';
                    document.getElementById("temp1MinimumTemp").value = temp1MinimumTemp;

                    temp1MaximumTemperature = temp1MaximumTemperature + "," + maximumTemperature[i].value;
                    document.getElementById("temp1MaximumTemperature").value = '';
                    document.getElementById("temp1MaximumTemperature").value = temp1MaximumTemperature;

                    temp1ReeferRequired = temp1ReeferRequired + "," + reeferRequired[i].value;
                    document.getElementById("temp1ReeferRequired").value = '';
                    document.getElementById("temp1ReeferRequired").value = temp1ReeferRequired;
                }
                counts++;
            }
        }
        if (cntr > 0) {
//         alert("temp+++++++"+temp);
//            alert("temp1+++++++"+temp1);
            document.customerProductview.action = "/throttle/saveCustomerProduct.do?productCategoryId=" + temp;
            document.customerProductview.submit();
        } else {
            alert("Please Select Any One And Then Proceed");
        }
    }
//{
//    var index = document.getElementsByName("productSelect");
//    var chec=0;
//    for(var i=0;i<index.length ;i++){
//        if(index[i].checked){
//           chec++;
//           document.customerProductview.action = "/throttle/saveCustomerProduct.do";
//           document.customerProductview.submit();
//    }
//    }
//    if(chec == 0){
//        alert("Please Select Any One And Then Proceed");
//    }
//}
</script>

<style>
    #index th {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>
<style>
    #tabindex td {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Sales</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Sales</a></li>
            <li class="active">New Product Creation</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="">
                <form name="customerProductview"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <br>
                    <table width="980" align="center" cellpadding="0" cellspacing="0" class="border">
                        <tr  id="index" height="30px;">
                            <th colspan="4">Customer Product Master</th>
                        </tr>
                        </thead>
                        <tr height="30px;">
                            <td class="text1">Customer Name</td>
                            <td class="text1"><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="textbox"><c:out value="${customerName}"/></td>
                            <td class="text1">Customer Code</td>
                            <td class="text1"><c:out value="${customerCode}"/></td>
                        </tr>
                    </table>
                    <br>
                    <br>


                    <h2 align="center">List Of Product Category</h2>

                    <c:if test = "${customerProductCategoryList != null}">
                        <% int sno = 1,index = 0;%>
                        <table width="815" align="center" border="0" id="table" class="sortable">
                            <thead>
                                <tr height="50" id="index">
                                    <th><h3>S.No</h3></th>
                            <th><h3>Product Category Name</h3></th>
                            <th><h3>Reefer Required</h3></th>
                            <th><h3>Reefer Minimum Temperature</h3></th>
                            <th><h3>Reefer Maximum Temperature</h3></th>
                            <th><h3>Select</h3></th>
                            </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${customerProductCategoryList}" var="custProduct">
                                    <tr>
                                        <td align="left"> <%=sno%> </td>
                                        <td align="left"> <input type="hidden" name="productCategoryIds" id="productCategoryIds<%=index%>" value="<c:out value="${custProduct.productCategoryId}"/>"  /> <c:out value="${custProduct.productCategoryName}" /></td>
                                        <td align="left">
                                            <select name="reeferRequireds" id="reeferRequireds<%=index%>">
                                                <option value="Y" >Yes</option>
                                                <option value="N" >No</option>
                                            </select>
                                            <script>
                                                document.getElementById('reeferRequireds' +<%=index%>).value = '<c:out value="${custProduct.reeferRequired}" />';
                                            </script>

                                        </td>
                                        <td align="left"><input type="text" name="reeferMinimumTemperatures" id="reeferMinimumTemperatures<%=index%>" value="<c:out value="${custProduct.reeferMinimumTemperature}" />" maxlength="6" onblur="extractNumber(this, 2, true);" onkeyup="extractNumber(this, 2, true);" onkeypress="return onKeyPressBlockCharacters(this, event, true, false);" /></td>
                                        <td align="left"><input type="text" name="reeferMaximumTemperatures" id="reeferMaximumTemperatures<%=index%>" value="<c:out value="${custProduct.reeferMaximumTemperature}" />" maxlength="6" onblur="extractNumber(this, 2, true);" onkeyup="extractNumber(this, 2, true);" onkeypress="return onKeyPressBlockCharacters(this, event, true, false);" /> </td>
                                        <td align="left"> <c:if test="${custProduct.activeInd == 'Y'}">
                                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                                <input type="checkbox" name="productSelect" id="productSelect"  value="<c:out value="${custProduct.productCategoryId}"/>" checked/>
                                            </c:if>
                                            <c:if test="${custProduct.activeInd == 'N'}">
                                                <input type="hidden" name="count" id="count" value="<%=sno%>"/>
                                                <input type="checkbox" name="productSelect" id="productSelect"  value="<c:out value="${custProduct.productCategoryId}"/>"/>
                                            </c:if>

                                        </td>
                                    </tr>
                                    <%sno++;%>
                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </c:if>
                    </table>
                    <br>
                    <br>
                    <br>
                    <center>
                        <input type="button" class="button" value="Save" style="width: 120px" onclick="submitpage(this.value);"/>
                        <!--     <input type="SUBMIT" value="Submit!">-->
                    </center>
                    <input type="hidden" name="productCategory" id="productCategory" value=""/>
                    <input type="hidden" name="productCategoryUnchecked" id="productCategoryUnchecked" value=""/>
                    <input type="hidden" name="tempMinimumTemp" id="tempMinimumTemp" value=""/>
                    <input type="hidden" name="tempMaximumTemperature" id="tempMaximumTemperature" value=""/>
                    <input type="hidden" name="tempReeferRequired" id="tempReeferRequired" value=""/>
                    <input type="hidden" name="temp1MinimumTemp" id="temp1MinimumTemp" value=""/>
                    <input type="hidden" name="temp1MaximumTemperature" id="temp1MaximumTemperature" value=""/>
                    <input type="hidden" name="temp1ReeferRequired" id="temp1ReeferRequired" value=""/>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50" selected="selected">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>