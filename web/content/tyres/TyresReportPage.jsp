<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>
<%@page import="java.sql.*,java.util.Iterator,java.util.ArrayList,java.text.DecimalFormat,ets.domain.tyres.business.TyresTO" pageEncoding="UTF-8"%>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<head>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>


    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
    <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display = 'none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display = 'block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display = 'none';
        }

        function viewReport() {
            var err = "";
            if (document.getElementById("tyreNumber").value == "") {
                err = err + "Tyre No is Not Filled \n";

            }
            if (err == "") {
                document.tyreReportVehicle.action = "/throttle/tyresReportPage.do";
                document.tyreReportVehicle.method = "post";
                document.tyreReportVehicle.submit();
            }

        }

        function getTyreNo() {
            var oTextbox = new AutoSuggestControl(document.getElementById("tyreNumber"), new ListSuggestions("tyreNumber", "/throttle/handleTyreNoSuggestions.do?"));
            //getVehicleDetails(document.getElementById("regno"));
        }

    </script>




</head>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.Departmentccc" text="Tyre Wise Report"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.HRMSdd" text="Tyre"/></a></li>
            <li class="active"><spring:message code="hrms.label.Department33" text="Tyre Wise Report"/></li>



        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">    

            <body onload="getTyreNo();">
                <form name="tyreReportVehicle" >

                    <%
                        String tyreNumber = (String) request.getAttribute("tyreNumber");
                        if (tyreNumber == null) {
                            tyreNumber = "";
                        }

                        String fromDate = (String) request.getAttribute("fromDate");
                        if (fromDate == null) {
                            fromDate = "";
                        }

                        String toDate = (String) request.getAttribute("toDate");
                        if (toDate == null) {
                            toDate = "";
                        }

                    %>

                    <table class="table table-bordered">
                        <tr>
                            <td>Tyre No</td><td>
                                <input type="text" name="tyreNumber" id="tyreNumber" class="form-control" style="width:220px;height:40px;" value="<%=tyreNumber%>" />
                            </td>


                            <td colspan="2"><input type="button" name="search" value="View List"  class="btn btn-success" onclick="viewReport()" /></td>
                        </tr>
                    </table>
                    <br>
                    <c:if test="${tyresDetails == null}">

                        <br>
                        <br>
                        <font color="red" size="3"> Tyre is not used yet (or) invalid Tyre no </font>
                    </c:if>   
                         <c:if test="${rcInfo != null}">
                    <h2><b>RC History</b></h2>
                    <br>
                    <table class="table table-info mb30 table-hover " id="table">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Vendor</th>
                                <th>RC Date</th>
                                <th>RC Cost</th>
                                <th>Remarks</th>


                            </tr>
                        </thead>
                       
                            <% int index = 0;%>
                            <c:set var="totalPrice" value="${0}"/>
                            <c:forEach items="${rcInfo}" var="rc">


                                <%
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>
                                <tr>
                                    <td >  <%=index + 1%></td>
                                    <c:set var="totalPrice" value="${totalPrice+rc.price}" />
                                    <td ><c:out value="${rc.vendorName}"/></td>
                                    <td ><c:out value="${rc.receivedDate}"/></td>
                                    <td ><c:out value="${rc.price}"/></td>
                                    <td ><c:out value="${rc.tyreRotationRemarks}"/></td>

                                </tr>
                                <%
                                    index++;
                                %>
                            </c:forEach>
                            <br>
                            <br>


                        </table>
                    </c:if> 

                    <c:if test="${tyresDetails != null}">
                        <% int sino = 1;%>
                        <c:forEach items="${tyresDetails}" var="td">
                            <h2><b> Tyre Make / Pattern / Size : </b></h2> <c:out value="${td.itemName}"/> (Buying Cost: INR 45,000)

                            <br>

                            <h2><b> Usage History </b></h2>

                            <br>
                            <table class="table table-info mb30 table-hover " id="table">
                                <thead>
                                    <tr>
                                        <th>S.No</th>
                                        <th>Vehicle No</th>
                                        <th>Position</th>       
                                        <th>Date</th>           
                                        <th>KM In</th>
                                        <th>Type</th>
                                        <th>KM Out</th>
                                        <th>Cost Per KM</th>

                                    </tr>
                                </thead>
                                <tr>
                                    <td><%=sino%></td>
                                    <td><c:out value="${td.vehicleRegNo}"/></td>
                                    <td><c:out value="${td.tyrePosition}"/></td>

                                    <td><c:out value="${td.fromDate}"/></td>
                                    <td><c:out value="${td.fromKm}"/></td>
                                    <c:if test="${td.rcType=='N'}">
                                        <td>New</td>
                                    </c:if>
                                    <c:if test="${td.rcType!='N'}">
                                        <td>RC</td>
                                    </c:if>
                                    <td><c:out value="${td.toKm}"/></td>

                                    <%--<td><c:out value="${(45000+totalPrice)/(td.toKm-td.fromKm)}"/></td>--%>
                                </tr>

                            </table>

                            <%
                                  sino++;
                            %>
                        </c:forEach>
                    </c:if>

<!--                    <th>Total Price :  <c:out value="${totalPrice}"/>
                    </th> -->



                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="../common/NewDesign/settings.jsp" %>
