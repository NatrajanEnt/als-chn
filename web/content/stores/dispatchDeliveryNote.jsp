<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>


<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import="ets.domain.company.business.CompanyTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script><!-- External script -->
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script> 
<script language="javascript" src="/throttle/js/validate.js"></script>
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 


<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.PackingList"  text="Dispatch Delivery Note"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="stores.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="stores.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Delivery"  text="Delivery"/></a></li>
            <li class="active"><spring:message code="stores.label.PickList"  text="Dispatch Delivery Note"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onload="setFocus();">

                <!--                                <body onload="setFocus();getItemNames();">-->

                <form name="searchPart"  method="post"  >

                    <%@ include file="/content/common/message.jsp" %>

                   
                    <!--</div>-->
                    <!--</div>-->
                    
                    <table class="table table-info mb30 table-hover">
                            <thead><tr><th colspan="4"><spring:message code="stores.label.Dispatch"  text="Dispatch "/></th></tr></thead>
                            <tr>
                                <td ><font color="red">*</font><spring:message code="stores.label.Transporter"  text="Transporter"/></td>
                                <td ><select class="form-control" style="width:260px;height:40px;" name="vendorId" id="vendorId">
                                        <option value='0'>-<spring:message code="stores.label.Select"  text="default text"/>-</option>
                                        <option value='1'>National Transaport</option>
                                        <option value='2'>KK Transaport</option>
                                        <option value='3'>classic Transaport</option>
                                        <option value='4'>D&D Transaport</option>
                                    <%--    <c:forEach items="${vendorList}" var="vendor"> 
                                            <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>     
                                        </c:forEach>  --%>
                                    </select></td>
                            
                                <td ><font color="red">*</font><spring:message code="stores.label.VehicleNo"  text="VehicleNo"/></td>
                                <td ><select class="form-control" style="width:260px;height:40px;" name="vehicleId" id="vehicleId" >
                                        <option value='0'>-<spring:message code="stores.label.Select"  text="default text"/>-</option>
                                        <option value='1'>TN13OA8654</option>
                                        <option value='2'>TN13OA4387</option>
                                        <option value='3'>TN05OA0965</option>
                                        <option value='4'>TN08OA8654</option>
                                    </select></td>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font><spring:message code="stores.label.Delivery Person"  text="Delivery Person"/></td>
                                <td ><select class="form-control" style="width:260px;height:40px;" name="driverId" id="driverId">
                                        <option value='0'>-<spring:message code="stores.label.Select"  text="default text"/>-</option>
                                        <option value='1'>Ragu</option>
                                        <option value='2'>Prakash</option>
                                        <option value='3'>Rajesh</option>
                                        <option value='4'>Ashok</option>
                                    </select></td>
                            </tr>
                    </table>

                    <span id="successsMsg" align="center"><font color="green" size="5px;"></font></span>
                        <table class="table table-info mb30 table-hover" >
                            <thead>
                                <tr >
                                    <th ><spring:message code="stores.label.S.No"  text="default text"/></th>
                                    <th  ><spring:message code="stores.label.DeliveryNoteNo"  text="DeliveryNoteNo"/></th>
                                    <th  ><spring:message code="stores.label.PackingNo"  text="PackingNo"/></th>
                                    <th ><spring:message code="stores.label.No of pickups"  text="No of pickups"/></th>
                                    <th ><spring:message code="stores.label.Origin"  text="Origin"/> </th>
                                    <th ><spring:message code="stores.label.Destination"  text="Destination"/> </th>
                                    <th ><spring:message code="stores.label.Order OTY"  text="Order OTY"/> </th>
                                    <th ><spring:message code="stores.label.UOM"  text="default text"/></th>
                                    <!--<th ><spring:message code="stores.label.RoLevel"  text="default text"/></th>-->
<!--                                    <th ><spring:message code="stores.label.Rack"  text="default text"/></th>
                                    <th ><spring:message code="stores.label.Sub-Rack"  text="default text"/></th>-->
                                    <!--<th ><spring:message code="stores.label.Action"  text="default text"/></th>-->
                                    <!--<th ><spring:message code="stores.label.Print"  text="default text"/></th>-->
                                </tr>
                            </thead>
                            <tbody>
                                    <tr height="30">                    
                                            <td> 1 </td>                                         
                                            <td> DN/20-21/001 </td>                                         
                                            <td> Pack/20-21/001,Pack/20-21/002 </td>                                         
                                            <td> 6 </td>                                         
                                            <td> Chennai</td>                                           
                                            <td> Coimbatore</td>                                           
                                            <td> 20.0</td>                                           
                                            <td> Nos</td>                                           
                                            <!--<td> 10</td>-->                                           
<!--                                            <td> A101</td>                                           
                                            <td> A101A</td>          -->
                                            <!--<td class='text1' height='25'><input type='checkbox' name='selectedInd'> </td>-->        
                                    </tr>
                                            
                                    
                            </tbody>



                        </table>

                    <br>
                     <center>
                            <!--<input type="button" value="<spring:message code="stores.label.Delivery Note"  text="Delivery Note"/>" class="btn btn-success" onClick="generatePickList()">-->                
                            <input type="button" value="<spring:message code="stores.label.Delivery Note"  text="Delivery Note"/>" class="btn btn-success" onClick="getDeliveryNotePrint();">                
                        </center>
                        
                        <script>
                            function getDeliveryNotePrint() {
                                var val=1;
                                if($("#vendorId").val() == "0"){
                                    alert("please select Vendor");
                                    return;
                                }else if($("#vehicleId").val() == "0"){
                                    alert("please select Vehicle");
                                    return;
                                }else if($("#driverId").val() == "0"){
                                    alert("please select Delivery Person");
                                    return;
                                }else{
                                window.open('/throttle/getDeliveryNotePrint.do?deliveryNoteId=' + val, "Print Run Sheet",
                                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                                }
                        }
                            function generatePickList(){
                                alert("Delivery Note Generated ...!");
                            }
                        </script>
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
            </body>

        </div>

    </div>
</div>





<%@ include file="../common/NewDesign/settings.jsp" %>


