<%--
    Document   : viewTripClosureVehicleList
    Created on : Jul 18, 2014, 11:32:08 AM
    Author     : Arul
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>



<div class="pageheader">
    <c:if test="${statusId == '12'}">
            <h2><i class="fa fa-edit"></i>Trip Closure</h2>
    </c:if>
    <c:if test="${statusId == '13'}">
            <h2><i class="fa fa-edit"></i>Trip Settlement</h2>
    </c:if>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.PrimaryOperation"  text="PrimaryOperation"/></a></li>
            
            <c:if test="${statusId == '12'}">
                    <li class="active">Trip Closure</li>
            </c:if>
            <c:if test="${statusId == '13'}">
                    <li class="active">Trip Settlement</li>
            </c:if>
        </ol>
    </div>
</div>

    <div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

    <body>


        <form name="viewTripClosureVehicle"  method="post" >
 
            <br>
            <br>

            <table class="table table-info mb30 table-bordered" id="table">

                <thead>

                    <tr height="30">
                        <th>S.No</th>
                        <th>Trip Code</th>
                        <th>Route Info</th>
                        <th>Vehicle No</th>
                        <th>Origin City</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <!--<th>Transit Days</th>-->
                        <th>Start Km</th>
                        <th>End Km</th>
                        <th>Total Km</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

             <% int index = 1;%>
                    <c:if test = "${tripClosureVehicleList != null}">
                        <c:forEach items="${tripClosureVehicleList}" var="tripVehicleList">
                            <%
                                        String className = "text1";
                                                if ((index % 2) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>
                            <tr>
                                <td   class="form-control"  >
                                     <%=index%>
                                </td>
                                <td class="form-control"> <c:out value="${tripVehicleList.tripCode}" /></td>
                                <td class="form-control"> <c:out value="${tripVehicleList.routeInfo}" /></td>
                                <td class="form-control">
                                    <c:if test="${tripVehicleList.activeInd == 'N'}">
                                        <font color="red" ><c:out value="${tripVehicleList.regNo}"/></font>
                                    </c:if>
                                    <c:if test="${tripVehicleList.activeInd == 'Y'}">
                                    <c:out value="${tripVehicleList.regNo}"/>
                                    </c:if>
                                </td>
                                <td class="form-control"> <c:out value="${tripVehicleList.originCityName}"/></td>
                                <td class="form-control"> <c:out value="${tripVehicleList.startDate}"/></td>
                                <td class="form-control"> 
                                <c:if test="${tripVehicleList.vehStatusId == '12' || tripVehicleList.vehStatusId == '13'}">
                                    <c:out value="${tripVehicleList.endDate}"/>
                                </c:if>
                                </td>
                                <%--<td class="form-control"> <c:out value="${tripVehicleList.transitDays1}"/></td>--%>
                                <td class="form-control"> <c:out value="${tripVehicleList.startKm}"/></td>
                                <td class="form-control"> <c:out value="${tripVehicleList.endKm}"/></td>
                                <td class="form-control"> <c:out value="${tripVehicleList.totalKm}"/></td>
                                <td class="form-control">
                                    <c:if test="${statusId == '12' && tripVehicleList.vehStatusId == '10' && tripVehicleList.activeInd == 'Y' }">
                                        <a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&movementType=<c:out value="${tripVehicleList.movementTypeId}"/>" > StatusUpdate </a>
                                    </c:if>
                                    <c:if test="${statusId == '12' && tripVehicleList.vehStatusId == '12'}">
                                       <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripId}"/>&vehicleId=<c:out value="${tripVehicleList.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&activeVehicle=<c:out value="${tripVehicleList.activeInd}" />">Closure</a>
                                    </c:if>
                                    <c:if test="${statusId == '13' && tripVehicleList.vehStatusId != '10'}">
                                       <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripId}"/>&vehicleId=<c:out value="${tripVehicleList.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&activeVehicle=<c:out value="${tripVehicleList.activeInd}" />">Settlement</a>
                                    </c:if>
                                </td>
                            </tr>
                             <%index++;%>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=index%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    </div>
    </div>
</div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>
