<%-- 
    Document   : customerOutstandingUpload
    Created on : Jan 04, 2014, 12:38:56 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    function submitPage(value) {
        if (value == 'Proceed') {
            document.upload.action = '/throttle/saveCustomerOutStandingHistory.do';
            document.upload.submit();
        } else {
            document.upload.action = '/throttle/customerOutStandingHistoryUpload.do';
            document.upload.submit();
        }
    }
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>    
    <body>
        <c:if test="${customerOutStandingList == null}">
        <form name="upload" method="post" enctype="multipart/form-data">
        </c:if>
        <c:if test="${customerOutStandingList != null}">
        <form name="upload" method="post">
        </c:if>
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <%
            if(request.getAttribute("errorMessage")!=null){
            String errorMessage=(String)request.getAttribute("errorMessage");                
            %>
            <center><b><font color="red" size="1"><%=errorMessage%></font></b></center>
                        <%}%>
            <c:if test="${customerOutStandingList == null && updateCustomerOutStanding == null}">
            <table border="0" cellpadding="0" cellspacing="0" width="780" align="center">
                <tr>
                    <td class="contenthead" colspan="4">Customer Outstanding Upload</td>
                </tr>
                <tr>
                    <td class="text2">Select file</td>
                    <td class="text2"><input type="file" name="importCustomerOutstanding" id="importCustomerOutstanding" ></td>                             
                    <td class="text2">Last Transaction Date</td>
                    <td class="text2"><c:out value="${lastUploadCustomerOutStandingDate}"/></td>                             
                </tr>
                <tr>
                    <td colspan="4" class="text1" align="center" ><input type="button" value="Submit" name="Submit" onclick="submitPage(this.value)" >
                </tr>
            </table>
            </c:if>    
            <br>
            <% int i = 1 ;%>
            <% int index = 0;%> 
            <%int oddEven = 0;%>
            <%  String classText = "";%>    
            <c:if test="${customerOutStandingList != null}">
                <table align="center">
                    <tr>
                        <td class="contenthead">S No</td>
                        <td class="contenthead">Status</td>
                        <td class="contenthead">Customer Code</td>
                        <td class="contenthead">Customer Name</td>
                        <td class="contenthead">Previous Outstanding Amount</td>
                        <td class="contenthead">Outstanding Amount</td>
                    </tr>
                    <c:forEach items="${customerOutStandingList}" var="outstanding">
                        <%
                      oddEven = index % 2;
                      if (oddEven > 0) {
                          classText = "text2";
                      } else {
                          classText = "text1";
                        }
                        %>
                        <tr>
                                <td class="<%=classText%>" ><font color="green"><%=i++%></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="customerId" id="customerId" value="<c:out value="${outstanding.customerId}"/>" />OK</font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="customerCode" id="customerCode" value="<c:out value="${outstanding.customerCode}"/>" /><c:out value="${outstanding.customerCode}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="customerName" id="customerName" value="<c:out value="${outstanding.customerName}"/>" /><c:out value="${outstanding.customerName}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="previousOutStandingAmount" id="previousOutStandingAmount" value="<c:out value="${outstanding.amount}"/>" /><c:out value="${outstanding.amount}"/></font></td>
                                <td class="<%=classText%>" ><font color="green"><input type="hidden" name="outStandingAmount" id="outStandingAmount" value="<c:out value="${outstanding.outStandingAmount}"/>" /><c:out value="${outstanding.outStandingAmount}"/></font></td>
                        </tr>
                        <%index++;%>
                    </c:forEach>
                </table>
                <br>
                <c:if test="${updateCustomerOutStanding == null}">
                <center>
                    <input type="button" class="button" value="Proceed" onclick="submitPage(this.value)"/>
                </center>
                </c:if>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>