<%-- 
    Document   : displayTripSheetWise
    Created on : Jun 11, 2013, 10:18:07 PM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {

                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
        <script>
            function submitPage(value){
               if(value == 'Fetch Data'){
               var param = 'No';

               } else{
               var param = 'ExportExcel';
               }
               //alert('param is::;'+param);
                if(textValidation(document.tripwise.fromDate,'From Date')){
                    return;
                }else if(textValidation(document.tripwise.toDate,'To Date')){
                    return;
                }
                //alert(check);
                document.tripwise.action="/throttle/searchTripWiseDetails.do?param="+param;
                document.tripwise.submit();
                
            }
            function setFocus(){
                var consignmentType='<%=request.getAttribute("consignmentType")%>';
                var ownership='<%=request.getAttribute("ownerShip")%>';
                var sdate='<%=request.getAttribute("fromDate")%>';
                var edate='<%=request.getAttribute("toDate")%>';
                
                if(consignmentType!='null'){
                document.tripwise.consignmentType.value=consignmentType;
                }
                if(ownership!='null'){
                document.tripwise.ownership.value=ownership;
                }
                if(sdate!='null' && edate!='null'){
                    document.tripwise.fromDate.value=sdate;
                    document.tripwise.toDate.value=edate;
                }
            }
        </script>
    </head>
    <body onload="setFocus()" >
        <form name="tripwise" action=""  method="post">
         <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
        <br/>
        <table cellpadding="0" cellspacing="2" align="center" border="0" width="950" id="report" bgcolor="#97caff" style="margin-top:0px;">
                <tr>
                    <td><b>Tripsheet Wise Report</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:900px">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <td>Ownership : </td>
                                    <td><select class="form-control" style="width:123px; " name="ownership">
                                            <option value="0">All</option>
                                            <option value="1">Own</option>
                                            <option value="2">Attach</option>
                                        </select>
                                    </td>
                                    <td  height="30">Consignment type:   </td>
                                    <td  height="30">
                                        <select class="form-control" style="width:123px;" name="consignmentType">
                                            <option selected  value="">All</option>
                                            <option  value='Paid'>Paid</option>
                                            <option  value='ToPay'>ToPay</option>
                                        </select>

                                    </td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>From Date :</td>
                                    <td  height="30"><input type="text" name="fromDate" id="fromDate" class="datepicker" value="" />  </td>

                                    <td  height="30"><font color="red">*</font>To Date : </td>
                                    <td  height="30"><input type="text" name="toDate" id="toDate" class="datepicker" value="" /></td>
                                </tr>
                                 <tr>
                                     <td  height="30" colspan="4" align="center" >
                                            <input type="button"  value="Fetch Data"  class="button" name="Fetch Data" onClick="submitPage(this.value)">&nbsp;&nbsp;&nbsp;
                                            <input type="button"  value="Export to Excel"  class="button" name="excelExport" onClick="submitPage(this.value)">
                                    </td>

                                </tr>

                            </table>
                            
                        </div>
                    </td>
                </tr>
            </table>
        <br>
        <%
                            DecimalFormat df = new DecimalFormat("0.00##");
                            String tripId = "",tripDate = "",destination = "",vehicleNumber = "",productName = "",crossing = "";
                            String departureName = "",gpno = "",totalTonnage = "",distance = "",totalFreightAmount = "";
                            String consigneeName = "",invoiceNo = "";
                            double grandTotalFreight = 0,subTotalFreight = 0,totalSettlement = 0,grandTotalSettlementAmount = 0,subTotalSettlementAmount = 0;
                            double grandTotalTonnage = 0,subTotalTonnage = 0,subTotalExpenses = 0,grandTotalExpenses = 0,totalCrossing = 0;
                            String className = "text2",settlementAmount = "";
                            int index = 0;
                            int sno = 0;
%>
                    <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                            <c:if test = "${tripSheetDetailsList != null}" >
                                <%
                                 ArrayList tripSheetDetailsList = (ArrayList) request.getAttribute("tripSheetDetailsList");
                                Iterator detailsItr = tripSheetDetailsList.iterator();
                                ReportTO repTO = null;
                                 if (tripSheetDetailsList.size() != 0) {
                                 %>
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30" width="80">Trip Id</td>
                                <td class="contentsub" height="30" width="100">Consignee Name</td>
                                <td class="contentsub" height="30" width="80">Vehicle No</td>
                                <td class="contentsub" height="30" width="80">GP No</td>
                                <td class="contentsub" height="30" width="80">Invoice No</td>
                                <td class="contentsub" height="30">Destination</td>
                                <td class="contentsub" height="30">Distance</td>
                                <td class="contentsub" height="30">Tonnage Total</td>
                                <td class="contentsub" height="30">Freight</td>
                                <td class="contentsub" height="30">Crossing</td>
                                <td class="contentsub" height="30">Settlement Amount</td>
                </tr>
                            <%
                                while (detailsItr.hasNext()) {
                                         index++;
                                        sno++;
                                            if(index%2 == 0){
                                            className = "text2";

                                            }else {
                                            className = "text1";
                                        }
                                        repTO = new ReportTO();
                                        repTO = (ReportTO) detailsItr.next();
                                        tripId  = repTO.getTripId();
                                        if(tripId == null){
                                            tripId = "";
                                            }
                                        tripDate  = repTO.getTripDate();
                                        if(tripDate == null){
                                            tripDate = "";
                                            }
                                        consigneeName  = repTO.getConsigneeName();
                                        if(consigneeName == null){
                                            consigneeName = "";
                                            }
                                        vehicleNumber  = repTO.getVehicleId();
                                        if(vehicleNumber == null){
                                            vehicleNumber = "";
                                            }
                                        gpno  = repTO.getGpno();
                                        if(gpno == null){
                                            gpno = "";
                                            }
                                        invoiceNo  = repTO.getInvoiceNo();
                                        if(invoiceNo == null){
                                            invoiceNo = "";
                                            }
                                        destination  = repTO.getDestination();
                                        if(destination == null){
                                            destination = "";
                                            }
                                        distance  = repTO.getDistance();
                                        if(distance == null){
                                            distance = "";
                                            }
                                        crossing  = repTO.getCrossing();
                                        totalCrossing += Double.parseDouble(crossing);
                                        if(crossing == null){
                                            crossing = "";
                                            }
                                        totalTonnage  = repTO.getTotalTonnage();
                                        if(totalTonnage == null){
                                            totalTonnage = "";
                                            }
                                        grandTotalTonnage += Double.parseDouble(totalTonnage);
                                        totalFreightAmount  = repTO.getTotalFreightAmount();
                                        grandTotalFreight += Double.parseDouble(totalFreightAmount);
                                        if(totalFreightAmount == null){
                                            totalFreightAmount = "";
                                            }
                                        settlementAmount  = repTO.getSettlementAmount();
                                        subTotalSettlementAmount += Double.parseDouble(settlementAmount);
                                       grandTotalSettlementAmount += Double.parseDouble(settlementAmount);
                                        if(settlementAmount == null){
                                            settlementAmount = "";
                                            }
                                        
                                        
                                        %>
                <tr>
                          <td class="<%=className%>"  height="30"><%=sno%></td>
                          <td class="<%=className%>"  height="30"><%=tripDate%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  height="30"><%=tripId%></td>
                          <td class="<%=className%>"  height="30"><%=consigneeName%></td>
                          <td class="<%=className%>"  height="30"><%=vehicleNumber%></td>
                          <td class="<%=className%>"  height="30"><%=gpno%></td>
                          <td class="<%=className%>"  height="30"><%=invoiceNo%></td>
                          <td class="<%=className%>"  height="30"><%=destination%></td>
                          <td class="<%=className%>" align="right" height="30"><%=distance%></td>
                          <td class="<%=className%>" align="right" height="30"><%=totalTonnage%></td>
                          <td class="<%=className%>" align="right" height="30"><%=totalFreightAmount%></td>
                          <td class="<%=className%>" align="right" height="30"><%=crossing%></td>
                          <td class="<%=className%>" align="right" height="30"><%=settlementAmount%></td>
                        </tr>
                <%

                                    }
                                if(className.equalsIgnoreCase("text1")){
                                    className = "text2";
                                    }else{
                                    className = "text1";
                                    }
                                %>
                                <tr>
                                    <td class="<%=className%>"  height="30" colspan="8">&nbsp;</td>
                                    <td class="<%=className%>" align="right" height="30" ><b>Total</b></td>
                                    <td class="<%=className%>" align="right" height="30" ><b><%=df.format(grandTotalTonnage)%></b></td>
                                    <td class="<%=className%>" align="right" height="30" ><b><%=df.format(grandTotalFreight)%></b></td>
                                    <td class="<%=className%>" align="right" height="30" ><b><%=df.format(totalCrossing)%></b></td>
                                    <td class="<%=className%>" align="right" height="30" ><b><%=df.format(subTotalSettlementAmount)%></b></td>
                                </tr>
                        <%
                                     }
%>

                            </c:if>
                        </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    
    
    
</html>
