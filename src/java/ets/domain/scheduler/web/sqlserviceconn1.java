package ets.domain.scheduler.web;

import ets.domain.report.business.ReportTO;
import ets.domain.report.business.TallyTO;
import ets.domain.thread.web.SendEmail;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

class sqlserviceconn1 extends Thread {

    String srlNo = "";
    String ContainerSize = "";
    String ContainerName = "";
    String SealNo = "";
    String CompanyCode = "";
    String BrnCd = "";
    String VehicleName = "";
    String TrlJobId = "";

    public sqlserviceconn1(TallyTO reportTO) {

        srlNo = reportTO.getSrlNo();
        ContainerSize = reportTO.getContainerSize();
        ContainerName = reportTO.getContainerName();
        SealNo = reportTO.getSealNo();
        CompanyCode = reportTO.getCompanyCode();
        BrnCd = reportTO.getBrnCd();
        VehicleName = reportTO.getVehicleName();
        TrlJobId = reportTO.getTrlJobId();

    }

    public void run() {
        int updateStatus = 0;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sqlQuery = "";
        String tripNo = "";
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conn = DriverManager.getConnection("jdbc:sqlserver://188.188.188.80;TrsptFA", "Trt", "TrtFA@1234%");

            if (conn != null) {
                System.out.println("Database Successfully connected");

                sqlQuery = "INSERT INTO [TrsptFA].[dbo].[contdetails](ContainerSrlNo,ContainerSize,ContainerName,SealNo,CompanyCode,BrnCd,VehicleName,TrlJobId) VALUES "
                        + "(?,?,?,?,?,?,?,?)";

                stmt = conn.prepareStatement(sqlQuery);

                stmt.setString(1, srlNo);
                stmt.setString(2, ContainerSize);
                stmt.setString(3, ContainerName);
                stmt.setString(4, SealNo);
                stmt.setString(5, CompanyCode);
                stmt.setString(6, BrnCd);
                stmt.setString(7, VehicleName);
                stmt.setString(8, TrlJobId);

                updateStatus = stmt.executeUpdate();
                System.out.println("UPDATE = " + updateStatus);

//                while (rs.next()) {
//                    tripNo = rs.getString("TripNo");
//                }
//                System.out.println("tripNo==========" + tripNo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        if (updateStatus != 0) {
//        if (!"".equals(updateStatus) && "0".equals(updateStatus)) {

            String fileNameNew = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/" + fileNameNew);
            try {
                dbProps.load(is);//this may throw IOException
            } catch (IOException ex) {
                Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
            }
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            int status = 0;
            Connection con = null;
            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try {
                    PreparedStatement updateMailDetails = null;
                    String sql = "UPDATE ContDetails SET \n"
                            + " post_ind = 1"
                            + " WHERE  TrlJobId = ? and ContainerSrlNo=?";
                    updateMailDetails = con.prepareStatement(sql);
                    updateMailDetails.setString(1, TrlJobId);
                    updateMailDetails.setString(2, srlNo);
                    status = updateMailDetails.executeUpdate();
                    System.out.println("NEW TRIP CONTAINER CHANGES = " + status);

                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("TripNo doesn't exist.");
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}
