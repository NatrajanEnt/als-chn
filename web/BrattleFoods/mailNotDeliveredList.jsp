<%-- 
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    </head>
    <%
                String menuPath = "Mail Details >> View Mail Details";
                request.setAttribute("menuPath", menuPath);
    %>
    <body>
        <form name="tripSheet" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@include file="/content/common/message.jsp" %>

            <br>
            <table width="100" border="1" align="center" cellpadding="0" cellspacing="0">
                <tbody>
                    <% int i = 1;%> 
                    <% int index = 0;%> 
                    <%int oddEven = 0;%>
                    <%  String classText = "";%>   
                    <c:if test="${mailNotDeliveredList != null}">
                        <tr class="contentsub">
                            <td>S.No</td>
                            <td width="200px">Mail Subject</td>
                            <td width="200px">Resend</td>
                            <td width="200px">Mail Content</td>
                            <td width="200px">Mail To</td>
                            <td width="200px">Mail Cc</td>
                            <td width="200px">Mail Date</td>
                        </tr>
                        <c:forEach items="${mailNotDeliveredList}" var="mail">
                            <% oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                             classText = "text1";
                                         }%>
                            <tr>
                                <td class="<%=classText%>"><%=i%></td>
                                <td class="<%=classText%>"><c:out value="${mail.mailSubjectTo}"/></td>
                                <td class="<%=classText%>" width="40" align="left">
                                    <img src="images/mailSendingImage.jpg" alt="Mail Alert"   title="Mail Alert" style="width: 30px;height: 30px" onclick="mailTriggerPage('<c:out value="${mail.mailSendingId}"/>');"/>
                                </td>
                                <td class="<%=classText%>">
                                    <!--<a href="" onclick="viewMailContent('<c:out value="${mail.mailContentTo}"/>')">View Content</a></td>-->
                                    <a href="" onclick="viewMailContent('<%=i%>')">View Content</a>
                                    <input type="hidden" id="mailContent<%=i%>" value="<c:out value="${mail.mailContentTo}"/>"/></td>
                                <td class="<%=classText%>" ><c:out value="${mail.mailIdTo}"/></td>
                                <td class="<%=classText%>" ><c:out value="${mail.mailIdCc}"/></td>
                                <td class="<%=classText%>" ><c:out value="${mail.mailDate}"/></td>
                            </tr>
                            <%i++;%>
                        </c:forEach>
                    </c:if>

                </tbody>
            </table>
            <script type="text/javascript">
                function viewMailContent(sno){
                    var content = document.getElementById("mailContent"+sno).value;
                    var myWindow = window.open("", "MsgWindow", "width=500, height=1000");
                    myWindow.document.write(content);
                }

                function mailTriggerPage(mailSendingId){
                    window.open('/throttle/viewMailResendDetails.do?mailSendingId='+mailSendingId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                }
            </script>       
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
