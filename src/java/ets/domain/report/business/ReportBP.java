/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.util.FPUtil;
import ets.domain.report.data.ReportDAO;
import ets.domain.section.data.SectionDAO;
import ets.domain.util.ThrottleConstants;
import static ets.domain.util.ThrottleConstants.vehicleUtilReportPath;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import net.sf.json.JSONException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.jsoup.Jsoup;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.time.LocalDate;  

/**
 *
 * @author sabreesh
 */
public class ReportBP {

    private ReportDAO reportDAO;
    private SectionDAO sectionDAO;
    static FPUtil fpUtil = FPUtil.getInstance();
    static final int poTextSplitSize = Integer.parseInt(fpUtil.getInstance().getProperty("PO_ADDRESS_SPLIT"));

    public ReportDAO getReportDAO() {
        return reportDAO;
    }

    public void setReportDAO(ReportDAO repDAO) {
        this.reportDAO = repDAO;
    }

    public static FPUtil getFpUtil() {
        return fpUtil;
    }

    public static void setFpUtil(FPUtil fpUtil) {
        ReportBP.fpUtil = fpUtil;
    }

    public SectionDAO getSectionDAO() {
        return sectionDAO;
    }

    public void setSectionDAO(SectionDAO sectionDAO) {
        this.sectionDAO = sectionDAO;
    }

    public ArrayList processBillList(ReportTO mrsTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = reportDAO.getBillList(mrsTO, fromDate, toDate);
        if (MfrList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return MfrList;
    }

    public ArrayList processSalesTaxSummary(ReportTO mrsTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList MfrSalesList = new ArrayList();
        MfrSalesList = reportDAO.getSalesBillTaxSummary(mrsTO, fromDate, toDate);
        //////System.out.println("salesTax size=" + MfrSalesList.size());
        if (MfrSalesList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return MfrSalesList;
    }

    public ArrayList processPurchaseReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList purchaseList = new ArrayList();
        purchaseList = reportDAO.getPurchaseList(repTO);
        if (purchaseList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
        }
        return purchaseList;
    }

    public ArrayList processComplaintList(ReportTO repTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList complaintList = new ArrayList();
        complaintList = reportDAO.getComplaintList(repTO, fromDate, toDate);

//        if (complaintList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
//        }
        return complaintList;
    }

    public ArrayList processStockWorthReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList stockWorthList = new ArrayList();
        stockWorthList = reportDAO.getStockWorthList(repTO);
        if (stockWorthList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return stockWorthList;
    }

    public ArrayList processRateList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList rateList = new ArrayList();
        rateList = reportDAO.getRateList(repTO);
        if (rateList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rateList;
    }

    public ArrayList processPeriodicServiceList(int companyId, String regNo, String date) throws FPRuntimeException, FPBusinessException {

        ArrayList rateList = new ArrayList();
        rateList = reportDAO.getPeriodicServiceVehicleList(companyId, regNo, date);
        //rateList = reportDAO.getPeriodicServiceList(companyId, regNo, date);
        if (rateList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rateList;
    }

    public ArrayList processDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processInsuranceDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getInsuranceDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processRoadTaxDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getRoadTaxDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processPermitDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.getPermitDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processAMCDueList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList dueList = new ArrayList();
        dueList = reportDAO.processAMCDueList(repTO);
        if (dueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return dueList;
    }

    public ArrayList processBodyBillList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillList = new ArrayList();
        bodyBillList = reportDAO.getbodyBillList(repTO);
        if (bodyBillList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return bodyBillList;
    }

    public ArrayList processContractorActivities(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList ContractorActivities = new ArrayList();
        ContractorActivities = reportDAO.getContractorActivities(repTO);
        if (ContractorActivities.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return ContractorActivities;
    }

    public ArrayList processBodyBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillDetails = new ArrayList();
        bodyBillDetails = reportDAO.getbodyBillDetails(repTO);
        return bodyBillDetails;
    }

    public ArrayList processHikedBodyBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList bodyBillDetails = new ArrayList();
        bodyBillDetails = reportDAO.getHikedbodyBillDetails(repTO);
        return bodyBillDetails;
    }

    public ArrayList processBillDetails(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList BillDetails = new ArrayList();
        BillDetails = reportDAO.getBillDetails(repTO);
        return BillDetails;
    }

    public ArrayList processActivityList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList ActivityList = new ArrayList();
        ActivityList = reportDAO.getActivityList(repTO);
        return ActivityList;
    }

    public ArrayList processTotalPrices(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList TotalPrices = new ArrayList();
        TotalPrices = reportDAO.getTotalPrices(repTO);
        return TotalPrices;
    }

    public ArrayList processStockIssueReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList IssueList = new ArrayList();
        IssueList = reportDAO.getStockIssueReport(repTO);
        if (IssueList.size() == 0) {
//            throw new FPBusinessException("EM-GEN-01");
        }
        return IssueList;
    }

    public ArrayList processStoresEffReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList IssueList = new ArrayList();
        IssueList = reportDAO.getStoresEffReport(repTO);
        if (IssueList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return IssueList;
    }

    public ArrayList processReceivedStockReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getReceivedStockReport(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processReceivedStockTaxSummary(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getReceivedStockTaxSummary(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processInVoiceItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getInvoiceItems(repTO);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processRCWOItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        ArrayList List = new ArrayList();
        receivedList = reportDAO.getRCWOItems(repTO);
        Iterator itr;
        itr = receivedList.iterator();
        ReportTO purch = null;
        String remarks = "";
        while (itr.hasNext()) {
            purch = new ReportTO();
            purch = (ReportTO) itr.next();
            remarks = purch.getRemarks();
            purch.setRemarks(remarks);
            /*
             if (remarks.length() >= 150) {
             remarks = remarks.substring(0, 150);
             }
             */
            purch.setAddressSplit(addressSplit(purch.getAddress()));
            purch.setRemarksSplit(remarksSplit(remarks, 75));
            List.add(purch);
        }

        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    public ArrayList processGdDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        String fromService = "";
        String toService = "";
        serviceEffList = reportDAO.getGdDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");

        }
        return serviceEffList;
    }

    public String[] addressSplit(String longText) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = poTextSplitSize;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public String[] remarksSplit(String longText, int textLength) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = textLength;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public ArrayList processServiceSummary(String usageTypeId, String ServiceTypId, String custId, String compId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList receivedList = new ArrayList();
        receivedList = reportDAO.getServiceSummary(usageTypeId, ServiceTypId, custId, compId, fromDate, toDate);
        if (receivedList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return receivedList;
    }

    public ArrayList processMovingAvgReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList MovingAvg = new ArrayList();
        ArrayList MovingAverage = new ArrayList();
        float stockBalance = 0.0f;
        int itemId = 0;
        int companyId = Integer.parseInt(repTO.getCompanyId());
        String lastPurchased = "";

        //////System.out.println("Moving avg Com-->" + companyId);
        MovingAvg = reportDAO.getMovingAvgReport(repTO);
        if (MovingAvg.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {

            Iterator itr = MovingAvg.iterator();
            ReportTO report = new ReportTO();
            while (itr.hasNext()) {
                report = (ReportTO) itr.next();
                ////////System.out.println("report.getSpltQty()" + report.getSpltQty());
                String monthSplit[] = report.getSpltQty().split("-");
                ////////System.out.println("MonthSplit-->length" + monthSplit.length);

                report.setFirstHalfDays(monthSplit[0]);
                report.setLastHalfDays(monthSplit[1]);

                itemId = reportDAO.getItemId(report.getPaplCode());
                stockBalance = sectionDAO.getItemStock(itemId, Integer.parseInt(repTO.getCompanyId()));
                ////////System.out.println("stockBalance-->"+stockBalance);
                report.setItemQty(stockBalance + "");

                lastPurchased = sectionDAO.getItemStockLastPurchased(itemId, companyId);
                ////////System.out.println("In Section BP LastPurchased Have-->" + lastPurchased);

                String temp[] = lastPurchased.split("-");
                ////////System.out.println("Qty-->" + temp[0] + "Price-->" + temp[1]);

                report.setPrice(Float.valueOf(temp[1]));
                MovingAverage.add(report);
            }
        }
        return MovingAverage;
    }

    public ArrayList processSerEffReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getSerEffReport(repTO);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processStRequestList(String fromdate, String toDate, int fromId, int toId, String type) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getStRequestList(fromdate, toDate, fromId, toId, type);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processStDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getStDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillList(String vendorId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillList(vendorId, fromDate, toDate);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillTaxSummary(String vendorId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillTaxSummary(vendorId, fromDate, toDate);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processRcBillDetail(String reqId) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.getRcBillDetail(reqId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }

    public ArrayList processServiceCostList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceCostList = new ArrayList();
        serviceCostList = reportDAO.getServiceCostList(repTO);
        if (serviceCostList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceCostList;
    }

    public ArrayList processWoList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        woList = reportDAO.getWoList(repTO);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return woList;
    }

    public ArrayList processWarrantyServiceList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList serviceList = new ArrayList();
        serviceList = reportDAO.getWarrantyServiceList(repTO);
        if (serviceList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceList;
    }

    public ArrayList processTechniciansList() throws FPBusinessException, FPRuntimeException {
        ArrayList tempList = new ArrayList();
        tempList = reportDAO.techniciansList();
        if (tempList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-03");
        }
        return tempList;

    }

    public ArrayList processRcItemList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList RcItemList = new ArrayList();
        RcItemList = reportDAO.getRcItemList(repTO);
        if (RcItemList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return RcItemList;
    }

    public ArrayList processRcHistoryList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList RcHistoryList = new ArrayList();
        RcHistoryList = reportDAO.getRcHistoryList(repTO);
        if (RcHistoryList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return RcHistoryList;
    }

    public ArrayList processTyreList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList tyreList = new ArrayList();
        tyreList = reportDAO.getTyreList(repTO);
        if (tyreList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return tyreList;
    }

    public ArrayList processOrdersList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList orderList = new ArrayList();
        orderList = reportDAO.getOrdersList(repTO);
        if (orderList.size() == 0) {
            System.out.println("zero in bp");
            //throw new FPBusinessException("EM-GEN-01");
        }
        return orderList;
    }

    public ArrayList handleJobCardWorkOrderList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList orderList = new ArrayList();
        orderList = reportDAO.handleJobCardWorkOrderList(repTO);
        if (orderList.size() == 0) {
            System.out.println("zero in bp");
            //throw new FPBusinessException("EM-GEN-01");
        }
        return orderList;
    }

    public ArrayList processStockPurchase(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getStockPurchase(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processRcBillsReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getRcBillsReport(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processStatusReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList lastList = new ArrayList();
        List = reportDAO.getStatusReport(repTO);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Date dat = new Date();
            //////System.out.println("st" + dat.getSeconds());
            Iterator itr = List.iterator();
            ReportTO reportTO = null;
            while (itr.hasNext()) {
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                lastList = reportDAO.getPrevious(reportTO.getJobCardId(), reportTO.getRegNo());
                Iterator itr1 = lastList.iterator();
                ReportTO repotTO = null;
                if (itr1.hasNext()) {
                    repotTO = new ReportTO();
                    repotTO = (ReportTO) itr1.next();

                    reportTO.setLastProblem(reportDAO.getLastProblem(repotTO.getJobCardId()));
                    reportTO.setLastRemarks(repotTO.getLastRemarks());
                    reportTO.setLastKm(repotTO.getLastKm());
                    reportTO.setLastStatus(repotTO.getLastStatus());
                    reportTO.setLastTech(repotTO.getLastTech());
                    reportTO.setLastDate(repotTO.getLastDate());
                }
            }
            Date dat1 = new Date();
            //////System.out.println("et" + dat1.getSeconds());
            //////System.out.println("tt" + (dat1.getSeconds() - dat.getSeconds()));

        }
        return List;
    }

    public ArrayList processTaxwiseItems(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList itemList = new ArrayList();
        itemList = reportDAO.getTaxwiseItems(repTO);
        if (itemList.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        }
        return itemList;
    }

    public ArrayList processVehMfrComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = reportDAO.getMfrVehComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            reportTO.setVehCount(reportDAO.getMfrVehCount(reportTO.getMfrId()));
        }
        return List;
    }

    public ArrayList processVehModelComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = reportDAO.getModelVehComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            reportTO.setVehCount(reportDAO.getModelVehCount(reportTO.getMfrId(), reportTO.getModelId()));
        }
        return List;
    }

    public ArrayList processVehAgeComparisionReport(ReportTO repTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList List1 = new ArrayList();
        int count = 0;
        List = reportDAO.getVehAgeComparisionReport(repTO);
        Iterator itr = List.iterator();
        ReportTO reportTO = null;
        while (itr.hasNext()) {
            reportTO = (ReportTO) itr.next();
            if (reportTO.getVehCount().equalsIgnoreCase("0")) {
                count++;

            }
            //////System.out.println("reportTO.getVehCount()" + reportTO.getVehCount());
        }
        if (count == 6) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    // rajesh
    public ArrayList salesTrendGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList salesTrendGraph = new ArrayList();
        salesTrendGraph = reportDAO.salesTrendGraph(reportTO);
        return salesTrendGraph;
    }

    public ArrayList vendorTrendGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vendorTrendGraph = new ArrayList();
        vendorTrendGraph = reportDAO.vendorTrendGraph(reportTO);
        return vendorTrendGraph;
    }

    public ArrayList mfr() throws FPRuntimeException, FPBusinessException {

        ArrayList mfr = new ArrayList();
        mfr = reportDAO.mfr();
        return mfr;
    }

    public ArrayList usage() throws FPRuntimeException, FPBusinessException {

        ArrayList usage = new ArrayList();
        usage = reportDAO.usage();
        return usage;
    }

    public ArrayList vehicleType() throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleType = new ArrayList();
        vehicleType = reportDAO.vehicleType();
        return vehicleType;
    }

    public String processItemSuggests(String vendorName) {
        String vendorNames = "";
        vendorName = vendorName + "%";
        vendorNames = reportDAO.getItemSuggests(vendorName);
        return vendorNames;
    }

    public String problemItemSuggests(String problem) {
        String problems = "";
        problem = problem + "%";
        problems = reportDAO.getproblemSuggests(problem);
        return problems;
    }

    public ArrayList problemDistributionGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList problemDistributionGraph = new ArrayList();
        problemDistributionGraph = reportDAO.problemDistributionGraph(reportTO);
        return problemDistributionGraph;
    }

    public ArrayList getColorList(int sizeValue) throws FPRuntimeException, FPBusinessException {
        ArrayList colorList = new ArrayList();
        colorList = reportDAO.getColorList(sizeValue);
        return colorList;
    }

    public ArrayList categoryRcReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList categoryRcReport = new ArrayList();
        categoryRcReport = reportDAO.categoryRcReport(reportTO);
        return categoryRcReport;
    }

    public ArrayList categoryNewReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList categoryNewReport = new ArrayList();
        categoryNewReport = reportDAO.categoryNewReport(reportTO);
        return categoryNewReport;
    }
    // end rajesh

    //shankar
    public ArrayList getActiveCategories() throws FPRuntimeException, FPBusinessException {

        ArrayList categories = new ArrayList();
        categories = reportDAO.getActiveCategories();
        return categories;
    }

    public ArrayList stockWorthGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList stockWorthGraph = new ArrayList();
        stockWorthGraph = reportDAO.stockWorthGraph(reportTO);
        return stockWorthGraph;
    }

    public ArrayList CompanyNameList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList companyNameList = new ArrayList();
        companyNameList = reportDAO.CompanyNameList(reportTO);
        return companyNameList;
    }

    public ArrayList monthNameList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList monthNameList = new ArrayList();
        monthNameList = reportDAO.monthNameList(reportTO);
        return monthNameList;
    }

    public ArrayList getManufacturerList() throws FPRuntimeException, FPBusinessException {

        ArrayList manufacturerList = new ArrayList();
        manufacturerList = reportDAO.getManufacturerList();
        return manufacturerList;
    }

    public ArrayList getUsageTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList usageTypeList = new ArrayList();
        usageTypeList = reportDAO.getUsageTypeList();
        return usageTypeList;
    }

    public ArrayList getVehicleTypeList() throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleTypeList = new ArrayList();
        vehicleTypeList = reportDAO.getVehicleTypeList();
        return vehicleTypeList;
    }

    public ArrayList processCategoryStRequestList(String fromDate, String toDate, int fromSpId, int toSpId) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        serviceEffList = reportDAO.processCategoryStRequestList(fromDate, toDate, fromSpId, toSpId);
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return serviceEffList;
    }
    // end shankar

    public String getVatPercentages() throws FPRuntimeException, FPBusinessException {
        String vatList = "";
        vatList = reportDAO.getVatPercentages();
        return vatList;
    }

    public ArrayList processTaxwiseServiceCostList(ReportTO repTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        String vatList = "";
        String[] temp = null;
        String[] billNo = null;
        ArrayList vatValues = null;
        ArrayList billList = new ArrayList();
        serviceEffList = reportDAO.processTaxwiseServiceCostList(repTO);
        vatList = getVatPercentages();
        ReportTO reportTO = null;

        ReportTO report = null;
        float Amount = 0.0f;
        float taxAmount = 0.0f;
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr = serviceEffList.iterator();
            while (itr.hasNext()) {
                vatValues = new ArrayList();
                reportTO = new ReportTO();
                reportTO = (ReportTO) itr.next();
                billNo = reportTO.getBillNo().split(",");
                temp = vatList.split(",");

                for (int j = 0; j < temp.length; j++) {
                    Amount = 0.0f;
                    Amount = reportDAO.getVatTotalAmount(billNo, temp[j]);
                    report = new ReportTO();
                    report.setAmount(Amount);
                    taxAmount = Float.parseFloat(temp[j]) * Amount;
                    report.setTaxAmount(taxAmount / 100);
                    vatValues.add(report);
                }
                Amount = 0.0f;
                report = new ReportTO();
                Amount = reportDAO.getContractAmount(billNo);
                report.setAmount(Amount);
                reportTO.setContractAmnt(Amount + "");
                report.setTaxAmount(0.0f);
                //////System.out.println("contr" + Amount);
                vatValues.add(report);
                reportTO.setVatValues(vatValues);
                //////System.out.println("service tax amount in BP++:" + reportTO.getTax());
            }

        }
        return serviceEffList;
    }

    public ArrayList processTaxwisePO(ReportTO repTO) throws FPRuntimeException, FPBusinessException {
        ArrayList serviceEffList = new ArrayList();
        String vatList = "";
        String[] temp = null;
        String supplyId = null;
        ArrayList vatValues = null;
        ArrayList billList = new ArrayList();
        serviceEffList = reportDAO.processTaxwisePO(repTO);
        vatList = getVatPercentages();
        ReportTO reportTO = null;

        ReportTO report = null;
        float Amount = 0.0f;
        float taxAmount = 0.0f;
        if (serviceEffList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr = serviceEffList.iterator();
            while (itr.hasNext()) {
                vatValues = new ArrayList();
                reportTO = new ReportTO();

                reportTO = (ReportTO) itr.next();
                supplyId = reportTO.getSupplyId();
                temp = vatList.split(",");

                for (int j = 0; j < temp.length; j++) {
                    Amount = 0.0f;
                    //////System.out.println("supplyId" + supplyId + "tax per" + temp[j]);
                    Amount = reportDAO.getPOVatTotalAmount(supplyId, temp[j]);
                    report = new ReportTO();
                    report.setAmount(Amount);

                    taxAmount = Float.parseFloat(temp[j]) * Amount;
                    report.setTaxAmount(taxAmount / 100);
                    //////System.out.println("Amount" + Amount + "tax" + report.getTaxAmount());
                    vatValues.add(report);
                }
                reportTO.setVatValues(vatValues);
            }

        }
        return serviceEffList;
    }

    public ArrayList gettopProblem(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList gettopProblem = new ArrayList();
        gettopProblem = reportDAO.gettopProblem(reportTO);
        return gettopProblem;
    }

    //Hari
    public ArrayList processServiceChartData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        woList = reportDAO.getServiceChartData(fromDate, toDate);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return woList;
    }

    public ArrayList handleServiceDailyMIS(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList woList = new ArrayList();
        ArrayList newList = new ArrayList();
        woList = reportDAO.handleServiceDailyMIS(fromDate, toDate);
        if (woList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr;
            itr = woList.iterator();
            ReportTO rto = null;
            ReportTO rtoNew = new ReportTO();
            String problem = "";
            int jcId = 0;
            while (itr.hasNext()) {
                rto = new ReportTO();
                rto = (ReportTO) itr.next();
                if (jcId != Integer.parseInt(rto.getJobCardId())) {
                    if (jcId != 0) {
                        newList.add(rtoNew);
                    }
                    rtoNew = new ReportTO();
                    rtoNew = rto;
                } else {
                    rtoNew.setProblemName(rtoNew.getProblemName() + "," + rto.getProblemName());
                }
                jcId = Integer.parseInt(rto.getJobCardId());

            }
        }
        System.out.println("wolist:" + woList.size());
        System.out.println("newlist:" + newList.size());
        return newList;
    }

    public ArrayList processRcExpenseGraph(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList rcExpenseSummary = new ArrayList();
        rcExpenseSummary = reportDAO.getRcExpenseData(reportTO);
        if (rcExpenseSummary.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcExpenseSummary;
    }

    public ArrayList processScrapGraphData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList scrapValue = new ArrayList();
        scrapValue = reportDAO.getScrapGraphData(fromDate, toDate);
        if (scrapValue.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return scrapValue;
    }

    public ArrayList processGetColourList() throws FPRuntimeException, FPBusinessException {

        ArrayList colourList = new ArrayList();
        colourList = reportDAO.getColourList();
        if (colourList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return colourList;
    }

    public ArrayList processGetContractVendor(int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList contractVendor = new ArrayList();
        contractVendor = reportDAO.getContractVendor(vendorId);
        if (contractVendor.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return contractVendor;
    }

    public ArrayList processExternalLabourBillGraphData(String fromDate, String toDate, int vendorId) throws FPRuntimeException, FPBusinessException {

        ArrayList externalLabour = new ArrayList();
        externalLabour = reportDAO.getExternalLabourBillGraphData(fromDate, toDate, vendorId);
        if (externalLabour.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return externalLabour;
    }


    /*
     * Return The Count of Parts send for Serviced and received by Service point
     */
    public ArrayList processRcTrendGraphData(String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList rcServiceData = new ArrayList();
        rcServiceData = reportDAO.getRcTrendGraphData(fromDate, toDate);
        if (rcServiceData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcServiceData;
    }

    public ArrayList processVehicleServiceGraphData(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleServiceData = new ArrayList();
        vehicleServiceData = reportDAO.getVehicleServiceGraphData(reportTO);
        if (vehicleServiceData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return vehicleServiceData;
    }

    public ArrayList processMileageGraphData(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList mileageData = new ArrayList();
        mileageData = reportDAO.getMileageGraphData(reportTO);
        if (mileageData.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return mileageData;
    }

    public ArrayList processRcVendorList() throws FPRuntimeException, FPBusinessException {

        ArrayList rcVendorList = new ArrayList();
        rcVendorList = reportDAO.getRcVendorList();
        if (rcVendorList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcVendorList;
    }

    public ArrayList processRcExpenseAmount(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList rcExpenseAmount = new ArrayList();
        rcExpenseAmount = reportDAO.getRcExpenseAmount(reportTO);
        if (rcExpenseAmount.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return rcExpenseAmount;
    }

    public Float processActualPrice(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        Float actualPrice = 0.0f;
        actualPrice = reportDAO.getActualPrice(reportTO);

        return actualPrice;
    }
//    bala

    public ArrayList processUsageTypewiseData(ReportTO report) throws FPRuntimeException, FPBusinessException {

        ArrayList usageList = new ArrayList();
        usageList = reportDAO.getUsageTypewiseData(report);
        if (usageList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return usageList;
    }
//    public ArrayList processServiceTypewiseSummary(ReportTO report) throws FPRuntimeException, FPBusinessException {
//
//            ArrayList serviceList = new ArrayList();
//            serviceList = reportDAO.getServiceTypewiseData(report);
//            if (serviceList.size() == 0) {
//                throw new FPBusinessException("EM-GEN-01");
//            }
//            return serviceList;
//    }
//    bala ends

    public ArrayList processTallyXMLSummary(int companyId, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList tallyXMLSummary = new ArrayList();
        tallyXMLSummary = reportDAO.getTallyXMLSummary(companyId, fromDate, toDate);
        if (tallyXMLSummary.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        //////System.out.println("tallyXMLSummarySize in BP:" + tallyXMLSummary.size());
        return tallyXMLSummary;
    }

    public int processTallyXMLReport(ReportTO report) throws FPRuntimeException, FPBusinessException {

        int tallyXMLReport = 0;
        tallyXMLReport = reportDAO.getTallyXMLReport(report);
        if (tallyXMLReport == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        //////System.out.println("tallyXMLReportSize in BP:" + tallyXMLReport);
        return tallyXMLReport;
    }

    public ArrayList processModifyTallyXMLPage(String xmlId) throws FPRuntimeException, FPBusinessException {

        ArrayList modifyTallyXMLPage = new ArrayList();
        modifyTallyXMLPage = reportDAO.getModifyTallyXMLPage(xmlId);
        if (modifyTallyXMLPage.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        //////System.out.println("tallyXMLReportSize in BP:" + modifyTallyXMLPage.size());
        return modifyTallyXMLPage;
    }

    public int processModifyTallyXMLReport(ReportTO report, String xmlId) throws FPRuntimeException, FPBusinessException {

        int modifyTallyXMLReport = 0;
        modifyTallyXMLReport = reportDAO.getModifyTallyXMLReport(report, xmlId);
        if (modifyTallyXMLReport == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        //////System.out.println("modifyTallyXMLReportSize in BP:" + modifyTallyXMLReport);
        return modifyTallyXMLReport;
    }

    public String handleDriverSettlement(String driName) {
        String driverName = "";
        driName = driName + "%";
        driverName = reportDAO.handleDriverSettlement(driName);
        return driverName;
    }

    public String handleVehicleNo(String regno) {
        String vehileRegNo = "";
        regno = regno + "%";
        vehileRegNo = reportDAO.handleVehicleNo(regno);
        return vehileRegNo;
    }

    public ArrayList driverSettlementReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        ArrayList driverSettlementReport = new ArrayList();
        driverSettlementReport = reportDAO.getDriverSettlementReport(reportTO);
        if (driverSettlementReport.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        //////System.out.println("tallyXMLReportSize in BP:" + driverSettlementReport.size());
        return driverSettlementReport;
    }

    public ArrayList searchProDriverSettlement(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList proDriverSettlement = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        proDriverSettlement = reportDAO.searchProDriverSettlement(fromDate, toDate, regNo, driId);
        if (proDriverSettlement.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return proDriverSettlement;
    }

    public ArrayList cleanerTripDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList cleanerTrip = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        cleanerTrip = reportDAO.cleanerTripDetails(fromDate, toDate, regNo, driId);
        if (cleanerTrip.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return cleanerTrip;
    }

    public ArrayList getFixedExpDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList fixedExpDetails = new ArrayList();
        String[] temp = null;
        /*if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        fixedExpDetails = reportDAO.getFixedExpDetails(fromDate, toDate, regNo, driId);
        //////System.out.println("fixedExpDetails.size().. BP : " + fixedExpDetails.size());
        if (fixedExpDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return fixedExpDetails;
    }

    public ArrayList getDriverExpDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList driverExpDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        driverExpDetails = reportDAO.getDriverExpDetails(fromDate, toDate, regNo, driId);
        if (driverExpDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return driverExpDetails;
    }

    public ArrayList getAdvDetails(String fromDate, String toDate, String regNo, int driId, String tripIds) throws FPBusinessException, FPRuntimeException {
        ArrayList AdvDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        AdvDetails = reportDAO.getAdvDetails(fromDate, toDate, regNo, driId, tripIds);
        if (AdvDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return AdvDetails;
    }

    public ArrayList getFuelDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList fuelDetails = new ArrayList();
        /**
         * String[] temp = null; if (fromDate != "" && toDate != "") { temp =
         * fromDate.split("-"); String sDate = temp[2] + "-" + temp[1] + "-" +
         * temp[0]; fromDate = sDate; temp = toDate.split("-");
         *
         * String eDate = temp[2] + "-" + temp[1] + "-" + temp[0]; toDate =
         * eDate; }
         */
        fuelDetails = reportDAO.getFuelDetails(fromDate, toDate, regNo, driId);
        if (fuelDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return fuelDetails;
    }

    public ArrayList getHaltDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList haltDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        haltDetails = reportDAO.getHaltDetails(fromDate, toDate, regNo, driId);
        if (haltDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return haltDetails;
    }

    public ArrayList getRemarkDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        remarkDetails = reportDAO.getRemarkDetails(fromDate, toDate, regNo, driId);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public ArrayList getSummaryDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        remarkDetails = reportDAO.getSummaryDetails(fromDate, toDate, regNo, driId);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public int insertEposTripExpenses(String tripId, String expenseDesc, String expenseAmt, String expenseDate, String expenseRemarks, int userId) {
        int status = 0;
        status = reportDAO.insertEposTripExpenses(tripId, expenseDesc, expenseAmt, expenseDate, expenseRemarks, userId);
        return status;
    }

    public int getTripCountDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int tripCount = 0;
        tripCount = reportDAO.getTripCountDetails(fromDate, toDate, regNo, driId);
        return tripCount;
    }

    public int getTotalTonnageDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int totalTonnage = 0;
        totalTonnage = reportDAO.getTotalTonnageDetails(fromDate, toDate, regNo, driId);
        return totalTonnage;
    }

    public int getOutKmDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int outKm = 0;
        outKm = reportDAO.getOutKmDetails(fromDate, toDate, regNo, driId);
        return outKm;
    }

    public int getInKmDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int inKm = 0;
        inKm = reportDAO.getInKmDetails(fromDate, toDate, regNo, driId);
        return inKm;
    }

    public ArrayList getTotFuelDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        ArrayList totalFuelDetails = new ArrayList();
        totalFuelDetails = reportDAO.getTotFuelDetails(fromDate, toDate, regNo, driId);
        return totalFuelDetails;
    }

    public int getDriverExpenseDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int driExpense = 0;
        driExpense = reportDAO.getDriverExpenseDetails(fromDate, toDate, regNo, driId);
        return driExpense;
    }

    public int getDriverSalaryDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int driExpense = 0;
        driExpense = reportDAO.getDriverSalaryDetails(fromDate, toDate, regNo, driId);
        return driExpense;
    }

    public int getGeneralExpenseDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int getExpense = 0;
        getExpense = reportDAO.getGeneralExpenseDetails(fromDate, toDate, regNo, driId);
        return getExpense;
    }

    public int getDriverAdvanceDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int advDetails = 0;
        advDetails = reportDAO.getDriverAdvanceDetails(fromDate, toDate, regNo, driId);
        return advDetails;
    }

    public int getDriverBataDetails(String fromDate, String toDate, String regNo, int driId) throws FPBusinessException, FPRuntimeException {
        /*String[] temp = null;
         if (fromDate != "" && toDate != "") {
         temp = fromDate.split("-");
         String sDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         fromDate = sDate;
         temp = toDate.split("-");

         String eDate = temp[2] + "-" + temp[1] + "-" + temp[0];
         toDate = eDate;
         }*/
        int advDetails = 0;
        advDetails = reportDAO.getDriverBataDetails(fromDate, toDate, regNo, driId);
        return advDetails;
    }

    public ArrayList getSettleCloseDetails(String fromDate, String toDate, String regNo, String driName) throws FPBusinessException, FPRuntimeException {
        ArrayList remarkDetails = new ArrayList();
        remarkDetails = reportDAO.getSettleCloseDetails(fromDate, toDate, regNo, driName);
        if (remarkDetails.size() == 0) {
            //throw new FPBusinessException("EM-MRS1-05");
        }
        return remarkDetails;
    }

    public ArrayList getVehicleCurrentStatus(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleCurrentStatus = new ArrayList();
        vehicleCurrentStatus = reportDAO.getVehicleCurrentStatus(reportTO);
        return vehicleCurrentStatus;
    }

    public ArrayList getCompanyWiseTripReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList companyWiseTripReport = new ArrayList();
        companyWiseTripReport = reportDAO.getCompanyWiseTripReport(reportTO);
        return companyWiseTripReport;
    }

    public ArrayList getVehicleList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleList();
        return vehicleList;
    }

    public ArrayList getVehiclePerformance(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehiclePerformance = new ArrayList();
        vehiclePerformance = reportDAO.getVehiclePerformance(reportTO);
        return vehiclePerformance;
    }

    public ArrayList getDriverPerformance(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverPerformance = new ArrayList();
        driverPerformance = reportDAO.getDriverPerformance(reportTO);
        return driverPerformance;
    }

    public String getDriverEmbarkedDate(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        String driverEmbarked = "";
        driverEmbarked = reportDAO.getDriverEmbarkedDate(reportTO);
        return driverEmbarked;
    }

    /**
     * This method used to LPS Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlpsCount(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList lpsCount = new ArrayList();
        lpsCount = reportDAO.getlpsCount(reportTO);
        return lpsCount;
    }

    public ArrayList getlpsTrippedList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList lpsTrippedList = new ArrayList();
        lpsTrippedList = reportDAO.getlpsTrippedList(reportTO);
        return lpsTrippedList;
    }

    public ArrayList gettripList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripList = new ArrayList();
        tripList = reportDAO.gettripList(reportTO);
        return tripList;
    }

    /**
     * This method used to Trip wise P&L Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getlocationList() throws FPRuntimeException, FPBusinessException {
        ArrayList locationList = new ArrayList();
        locationList = reportDAO.getlocationList();
        return locationList;
    }

    public ArrayList getTripWisePandL(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripWiseList = new ArrayList();
        tripWiseList = reportDAO.getTripWisePandL(reportTO, userId);
        return tripWiseList;
    }

    public ArrayList getTripWisetotal(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripWiseTotal = new ArrayList();
        tripWiseTotal = reportDAO.getTripWisetotal(reportTO, userId);
        return tripWiseTotal;
    }

    /**
     * This method used to Trip wise P&L Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleWisePandL(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleWiseList = new ArrayList();
        vehicleWiseList = reportDAO.getVehicleWisePandL(reportTO, userId);
        return vehicleWiseList;
    }

    public ArrayList getVehicleWisetotal(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleWiseTotal = new ArrayList();
        vehicleWiseTotal = reportDAO.getVehicleWisetotal(reportTO, userId);
        return vehicleWiseTotal;
    }

    public ArrayList getCustomerList() throws FPBusinessException, FPRuntimeException {
        ArrayList customerList = new ArrayList();
        customerList = reportDAO.getCustomerList();
        return customerList;
    }

    /**
     * This method used to District names List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictNameList() throws FPRuntimeException, FPBusinessException {
        ArrayList districtNameList = new ArrayList();
        districtNameList = reportDAO.getDistrictNameList();
        return districtNameList;
    }

    /**
     * This method used to get District Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictSummaryList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList districtSummaryList = new ArrayList();
        districtSummaryList = reportDAO.getDistrictSummaryList(reportTO, userId);
        return districtSummaryList;
    }

    /**
     * This method used to get District Wise Details Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getDistrictDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList districtDetailsList = new ArrayList();
        districtDetailsList = reportDAO.getDistrictDetailsList(reportTO, userId);
        return districtDetailsList;
    }

    /**
     * This method used to get Trip Sheet Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsheetDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetDetailsList = new ArrayList();
        tripSheetDetailsList = reportDAO.getTripsheetDetailsList(reportTO, userId);
        return tripSheetDetailsList;
    }

    /**
     * This method used to get Consignee Wise Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsigneeWiseList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList consigneeWiseDetailsList = new ArrayList();
        consigneeWiseDetailsList = reportDAO.getConsigneeWiseList(reportTO, userId);
        return consigneeWiseDetailsList;
    }

    /**
     * This method used to get Trip Settlement Wise Summart Report.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getTripsettlementWiseList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripsettlementDetailsList = new ArrayList();
        tripsettlementDetailsList = reportDAO.getTripsettlementWiseList(reportTO, userId);
        return tripsettlementDetailsList;
    }

    public ArrayList getDieselReportList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList dieselReportList = new ArrayList();
        dieselReportList = reportDAO.getDieselReportList(reportTO, userId);
        return dieselReportList;
    }

    /**
     * This method used to get ConsigneeName Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getConsigneeNameSuggestions(String consigneeName) {
        //  //////System.out.println("i am in consignee name ajax ");
        String sugggestions = "";
        ////////System.out.println("consigneeName =  " + consigneeName);
        sugggestions = reportDAO.getConsigneeNameSuggestions(consigneeName);
        return sugggestions;
    }

    /**
     * This method used to get Trip Id Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getTripIdSuggestions(String tripId) {
        String sugggestions = "";
        sugggestions = reportDAO.getTripIdSuggestions(tripId);
        return sugggestions;
    }

    /**
     * This method used to get Vehicle Number Suggestions..
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public String getVehicleNumberSuggestions(String vehicleNo) {
        String sugggestions = "";
        sugggestions = reportDAO.getVehicleNumberSuggestions(vehicleNo);
        return sugggestions;
    }

    public ArrayList getCustomerOverDueList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerOverDueList = new ArrayList();
        customerOverDueList = reportDAO.getCustomerOverDueList(reportTO, userId);
        return customerOverDueList;
    }

    public ArrayList getSalesDashboard(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList response = new ArrayList();
        response = reportDAO.getSalesDashboard(reportTO, userId);
        return response;
    }

    public ArrayList getOpsDashboard(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList response = new ArrayList();
        response = reportDAO.getOpsDashboard(reportTO, userId);
        return response;
    }

    /**
     * This method used to get Account Head List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getAccountReceivableList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList accountReceivableList = new ArrayList();
        accountReceivableList = reportDAO.getAccountReceivableList(reportTO, userId);
        return accountReceivableList;
    }

    /**
     * This method used to get Customer Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getCustomerWiseProfitList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerWiseProfitList = new ArrayList();
        customerWiseProfitList = reportDAO.getCustomerWiseProfitList(reportTO, userId);
        return customerWiseProfitList;
    }

    /**
     * This method used to get Vehicle Details List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        vehicleDetailsList = reportDAO.getVehicleDetailsList(reportTO, userId);
        return vehicleDetailsList;

    }

    public ArrayList getTripSheetDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        ArrayList OperationExpensesList = new ArrayList();
        ArrayList tripEarnings = new ArrayList();
        ArrayList tripEarningsList = new ArrayList();
        tripDetails = reportDAO.getTripSheetDetails(reportTO);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = tripDetails.iterator();
        ReportTO repTO1 = new ReportTO();

        /*
         while (itr1.hasNext()) {
         rpTO = new ReportTO();
         repTO1 = (ReportTO) itr1.next();
         tripEarnings = reportDAO.getTripEarningsList(reportTO);
         reportTO.setTripId(repTO1.getTripSheetId());
         rpTO.setTripId(reportTO.getTripId());
         rpTO.setConsignmentName(repTO1.getConsignmentName());
         rpTO.setTripCode(repTO1.getTripCode());
         rpTO.setCustomerName(repTO1.getCustomerName());
         rpTO.setConsignmentName(repTO1.getConsignmentName());
         rpTO.setCustomerTypeId(repTO1.getCustomerTypeId());
         rpTO.setBillingType(repTO1.getBillingType());
         rpTO.setRouteName(repTO1.getRouteName());
         rpTO.setVehicleTypeName(repTO1.getVehicleTypeName());
         rpTO.setReeferRequired(repTO1.getReeferRequired());
         rpTO.setVehicleNo(repTO1.getVehicleNo());
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setDriverName(repTO1.getDriverName());
         rpTO.setCompanyName(repTO1.getCompanyName());
         //////System.out.println("the tripearnings list" + tripEarnings.size());
         if (tripEarnings.size() > 0) {
         Iterator itr2 = tripEarnings.iterator();
         while (itr2.hasNext()) {
         dprTO = (DprTO) itr2.next();
         //////System.out.println("repTO2.getTripNos() = " + dprTO.getTripNos());
         //////System.out.println("repTO2.getFreightAmount() = " + dprTO.getFreightAmount());
         rpTO.setVehicleId(dprTO.getVehicleId());
         rpTO.setTripId(dprTO.getTripNos());
         rpTO.setFreightAmount(dprTO.getFreightAmount());
         rpTO.setOtherExpenseAmount(dprTO.getOtherExpenseAmount());
         //vehicleEarningsList.add(rpTO);
         }
         } else {
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setTripNos("0");
         rpTO.setFreightAmount("0.00");
         rpTO.setOtherExpenseAmount("0.00");
         //vehicleEarningsList.add(rpTO);
         }
         DprTO dpr = new DprTO();
         double driverSalary = 0;
         driverSalary = reportDAO.getTripDriverSalary(reportTO);
         reportTO.setVehicleDriverSalary(driverSalary);
         DprTO dpr1 = new DprTO();
         OperationExpensesList = reportDAO.getTripOperationExpenseList(reportTO);
         if (OperationExpensesList.size() > 0) {
         Iterator itr4 = OperationExpensesList.iterator();
         while (itr4.hasNext()) {
         dpr1 = (DprTO) itr4.next();
         rpTO.setTollAmount(dpr1.getTollAmount());
         rpTO.setFuelAmount(dpr1.getFuelAmount());
         rpTO.setDriverIncentive(dpr1.getDriverIncentive());
         rpTO.setDriverBata(dpr1.getDriverBata());
         rpTO.setMiscAmount(dpr1.getMiscAmount());
         rpTO.setDriverExpense(dpr1.getDriverBata() + dpr1.getDriverIncentive() + dpr1.getMiscAmount());
         rpTO.setRouteExpenses(dpr1.getRouteExpenses());
         rpTO.setTripOtherExpense(dpr1.getTripOtherExpense());
         rpTO.setTotlalOperationExpense(dpr1.getTollAmount() + dpr1.getFuelAmount() + dpr1.getDriverIncentive() + dpr1.getMiscAmount() + dpr1.getDriverBata());
         //////System.out.println("repTO2.getmiscamount() = " + dpr1.getTollAmount());
         //vehicleEarningsList.add(rpTO);
         }
         } else {
         rpTO.setVehicleId(repTO1.getVehicleId());
         rpTO.setTollAmount(0.00);
         rpTO.setFuelAmount(0.00);
         rpTO.setDriverIncentive(0.00);
         rpTO.setDriverBata(0.00);
         rpTO.setDriverExpense(0.00);
         rpTO.setRouteExpenses(0.00);
         rpTO.setTripOtherExpense(0.00);
         rpTO.setTotlalOperationExpense(0.00);
         //vehicleEarningsList.add(rpTO);
         }
         rpTO.setNetExpense(rpTO.getTotlalOperationExpense());
         freightAmount = Double.parseDouble(rpTO.getFreightAmount());
         rpTO.setNetProfit(freightAmount - rpTO.getNetExpense());
         tripEarningsList.add(rpTO);
         }
         */
        return tripDetails;
    }

    /**
     * This method used to get Vehicle Wise Profit List.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getVehicleWiseProfitList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        ArrayList vehicleEarnings = new ArrayList();
        ArrayList vehicleFixedExpensesList = new ArrayList();
        ArrayList vehicleOperationExpensesList = new ArrayList();
        ArrayList vehicleMaintainExpensesList = new ArrayList();
        ArrayList vehicleEarningsList = new ArrayList();
//        ArrayList EarningsList = new ArrayList();
        vehicleDetailsList = reportDAO.getVehicleDetailsList(reportTO, userId);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = vehicleDetailsList.iterator();
        ReportTO repTO1 = new ReportTO();
        while (itr1.hasNext()) {
            rpTO = new ReportTO();
            repTO1 = (ReportTO) itr1.next();
            reportTO.setVehicleId(repTO1.getVehicleId());
            vehicleEarnings = reportDAO.getVehicleEarningsList(reportTO, userId);
            if (vehicleEarnings.size() > 0) {
                Iterator itr2 = vehicleEarnings.iterator();
                while (itr2.hasNext()) {
                    dprTO = (DprTO) itr2.next();
                    //////System.out.println("repTO2.getTripNos() = " + dprTO.getTripNos());
                    //////System.out.println("repTO2.getFreightAmount() = " + dprTO.getFreightAmount());
                    rpTO.setVehicleId(dprTO.getVehicleId());
                    rpTO.setTripId(dprTO.getTripId());
                    rpTO.setTripNos(dprTO.getTripNos());
                    rpTO.setFreightAmount((Double.parseDouble(dprTO.getFreightAmount()) + dprTO.getOtherRevenue()) + "");
                    rpTO.setOtherExpenseAmount(dprTO.getOtherExpenseAmount());
                    rpTO.setVehicleDriverSalary(dprTO.getDriverSalary() + dprTO.getCleanerSalary());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setTripNos("0");
                rpTO.setFreightAmount("0.00");
                rpTO.setOtherExpenseAmount("0.00");
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr = new DprTO();
            double driverSalary = 0;
            //driverSalary = reportDAO.getVehicleDriverSalary(reportTO, userId);
            //reportTO.setVehicleDriverSalary(driverSalary);
            vehicleFixedExpensesList = reportDAO.getVehicleFixedExpenseList(reportTO, userId);
            Iterator itr3 = vehicleFixedExpensesList.iterator();
            while (itr3.hasNext()) {
                dpr = (DprTO) itr3.next();
                rpTO.setInsuranceAmount(dpr.getInsuranceAmount());
                rpTO.setFcAmount(dpr.getFcAmount());
                rpTO.setRoadTaxAmount(dpr.getRoadTaxAmount());
                rpTO.setPermitAmount(dpr.getPermitAmount());
                rpTO.setEmiAmount(dpr.getEmiAmount());
                rpTO.setFixedExpensePerDay(dpr.getFixedExpensePerDay());
                rpTO.setTotlalFixedExpense(dpr.getTotlalFixedExpense());
                //rpTO.setVehicleDriverSalary(dpr.getVehicleDriverSalary());
                //driverSalary = dpr.getVehicleDriverSalary();
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr1 = new DprTO();

            vehicleOperationExpensesList = reportDAO.getVehicleOperationExpenseList(reportTO, userId);
            if (vehicleOperationExpensesList.size() > 0) {
                Iterator itr4 = vehicleOperationExpensesList.iterator();
                while (itr4.hasNext()) {
                    dpr1 = (DprTO) itr4.next();
                    rpTO.setTollAmount(dpr1.getTollAmount());
                    rpTO.setFuelAmount(dpr1.getFuelAmount());
                    rpTO.setDriverIncentive(dpr1.getDriverIncentive());
                    rpTO.setDriverBata(dpr1.getDriverBata());
                    rpTO.setMiscAmount(dpr1.getMiscAmount());
                    rpTO.setDriverExpense(dpr1.getDriverBata() + dpr1.getDriverIncentive() + dpr1.getMiscAmount());
                    rpTO.setRouteExpenses(dpr1.getRouteExpenses());
                    rpTO.setTripOtherExpense(dpr1.getTripOtherExpense());
                    System.out.println("dpr1.getTollAmount():::" + dpr1.getTollAmount());
                    System.out.println("dpr1.getFuelAmount():::" + dpr1.getFuelAmount());
                    System.out.println("dpr1.getDriverIncentive():::" + dpr1.getDriverIncentive());
                    System.out.println("dpr1.getMiscAmount():::" + dpr1.getMiscAmount());
                    System.out.println("dpr1.getDriverBata():::" + dpr1.getDriverBata());
                    System.out.println("dpr1.getTripOtherExpense():::" + dpr1.getTripOtherExpense());
                    System.out.println("dpr1.driverSalary():::" + driverSalary);
                    System.out.println("dpr1.getOtherExpenseAmount():::" + rpTO.getOtherExpenseAmount());
                    System.out.println("dpr1.getVehicleDriverSalary():::" + rpTO.getVehicleDriverSalary());
                    rpTO.setTotlalOperationExpense(dpr1.getTollAmount() + dpr1.getFuelAmount() + rpTO.getDriverExpense() + dpr1.getTripOtherExpense()
                            + driverSalary + Double.parseDouble(rpTO.getOtherExpenseAmount()) + rpTO.getVehicleDriverSalary());
//                    rpTO.setTotlalOperationExpense(dpr1.getTollAmount() + dpr1.getFuelAmount() + dpr1.getDriverIncentive() + dpr1.getMiscAmount() + dpr1.getDriverBata() + dpr1.getTripOtherExpense() + driverSalary);
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setTollAmount(0.00);
                rpTO.setFuelAmount(0.00);
                rpTO.setDriverIncentive(0.00);
                rpTO.setDriverBata(0.00);
                rpTO.setDriverExpense(0.00);
                rpTO.setRouteExpenses(0.00);
                rpTO.setTripOtherExpense(0.00);
                rpTO.setTotlalOperationExpense(0.00);
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr2 = new DprTO();
            vehicleMaintainExpensesList = reportDAO.getVehicleMaintainExpenses(reportTO, userId);
            if (vehicleMaintainExpensesList.size() > 0) {
                Iterator itr5 = vehicleMaintainExpensesList.iterator();
                while (itr5.hasNext()) {
                    dpr2 = (DprTO) itr5.next();
                    //rpTO.setMaintainExpense(dpr2.getMaintainExpense());
                    rpTO.setMaintainExpense(dpr2.getItemValue() + dpr2.getLaborValue());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setVehicleId(repTO1.getVehicleId());
                rpTO.setMaintainExpense(0.00);
                //vehicleEarningsList.add(rpTO);
            }
            rpTO.setNetExpense(rpTO.getTotlalFixedExpense() + rpTO.getTotlalOperationExpense() + rpTO.getMaintainExpense());
            freightAmount = Double.parseDouble(rpTO.getFreightAmount());
            rpTO.setNetProfit(freightAmount - rpTO.getNetExpense());
            vehicleEarningsList.add(rpTO);
        }
        return vehicleEarningsList;
    }

    public ArrayList getVehicleUtilizationList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleUtilizationList = new ArrayList();
        vehicleUtilizationList = reportDAO.getVehicleUtilizationList(reportTO, userId);
        return vehicleUtilizationList;

    }

    public ArrayList getVehicleWeeklyProductivityReport(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList resultList = new ArrayList();

        resultList = reportDAO.getVehicleWeeklyProductivityReport(reportTO, userId);
        return resultList;

    }

    public ArrayList getDayWiseWeeklyProductivity(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList resultList = new ArrayList();
        resultList = reportDAO.getDayWiseWeeklyProductivity(reportTO, userId);
        return resultList;
    }

    public ArrayList getPopupCustomerProfitReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList popupCustomerProfitReport = new ArrayList();
        popupCustomerProfitReport = reportDAO.getPopupCustomerProfitReportDetails(reportTO);
        return popupCustomerProfitReport;

    }
    public ArrayList getPopupCustomerProfitReportDetailsNew(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList popupCustomerProfitReport = new ArrayList();
        popupCustomerProfitReport = reportDAO.getPopupCustomerProfitReportDetailsNew(reportTO);
        return popupCustomerProfitReport;

    }

    public ArrayList getGPSLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList logDetails = new ArrayList();
        logDetails = reportDAO.getGPSLogDetails(reportTO, userId);
        return logDetails;

    }

    public ArrayList getDriverSettlementDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList driverSettlementDetails = new ArrayList();
        driverSettlementDetails = reportDAO.getDriverSettlementDetails(reportTO, userId);
        return driverSettlementDetails;

    }

    public ArrayList getTripLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripLogDetails = new ArrayList();
        tripLogDetails = reportDAO.getTripLogDetails(reportTO, userId);
        return tripLogDetails;

    }

    public ArrayList getBPCLTransactionDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList BPCLTransactionDetails = new ArrayList();
        BPCLTransactionDetails = reportDAO.getBPCLTransactionDetails(reportTO, userId);
        return BPCLTransactionDetails;

    }

    public ArrayList getTripGpsStatusDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripGpsStatusDetails = new ArrayList();
        tripGpsStatusDetails = reportDAO.getTripGpsStatusDetails(reportTO);
        return tripGpsStatusDetails;

    }

    public ArrayList getTripCodeList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripCodeList = new ArrayList();
        tripCodeList = reportDAO.getTripCodeList(reportTO);
        return tripCodeList;
    }

    public ArrayList getTripDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getWflList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList wflList = new ArrayList();
        wflList = reportDAO.getWflList(reportTO);
        return wflList;
    }

    public ArrayList getWfuList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList wfuList = new ArrayList();
        wfuList = reportDAO.getWfuList(reportTO);
        return wfuList;
    }

    public ArrayList getTripInProgressList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripInProgressList = new ArrayList();
        tripInProgressList = reportDAO.getTripInProgressList(reportTO);
        return tripInProgressList;
    }
    public ArrayList getTripInProgressHireList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripInProgressList = new ArrayList();
        tripInProgressList = reportDAO.getTripInProgressHireList(reportTO);
        return tripInProgressList;
    }

    public ArrayList getTripNotStartedList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripNotStartedList = new ArrayList();
        tripNotStartedList = reportDAO.getTripNotStartedList(reportTO);
        return tripNotStartedList;
    }

    public ArrayList getJobCardList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList jobCardList = new ArrayList();
        jobCardList = reportDAO.getJobCardList(reportTO);
        return jobCardList;
    }

    public ArrayList getFutureTripList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList futureTripList = new ArrayList();
        futureTripList = reportDAO.getFutureTripList(reportTO);
        return futureTripList;
    }

    public int getWflWfuStatus(String currentDate) {
        int wflWfuStatus = 0;
        wflWfuStatus = reportDAO.getWflWfuStatus(currentDate);
        return wflWfuStatus;
    }

    public int getWflWfuLogId(String currentDate) {
        int wflwfuLogId = 0;
        wflwfuLogId = reportDAO.getWflWfuLogId(currentDate);
        return wflwfuLogId;
    }

    public String getTotalTripSummaryDetails(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getTotalTripSummaryDetails(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getTripSheetList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetList = new ArrayList();
        tripSheetList = reportDAO.getTripSheetList(reportTO);
        return tripSheetList;
    }

    public String getMarginWiseTripSummary(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getMarginWiseTripSummary(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getMonthWiseEmptyRunSummary(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList monthWiseEmptyRunSummary = new ArrayList();
        monthWiseEmptyRunSummary = reportDAO.getMonthWiseEmptyRunSummary(reportTO);
        return monthWiseEmptyRunSummary;
    }

    public ArrayList getJobcardSummary(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList jobcardSummary = new ArrayList();
        jobcardSummary = reportDAO.getJobcardSummary(reportTO);
        return jobcardSummary;
    }

    public ArrayList getTripMergingDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList TripMergingDetails = new ArrayList();
        TripMergingDetails = reportDAO.getTripMergingDetails(reportTO, userId);
        return TripMergingDetails;

    }

    public ArrayList getVehicleReadingDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleReadingDetails = new ArrayList();
        vehicleReadingDetails = reportDAO.getVehicleReadingDetails(reportTO, userId);
        return vehicleReadingDetails;

    }

    public ArrayList getTripMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripMergingList = new ArrayList();
        tripMergingList = reportDAO.getTripMergingList(reportTO, userId);
        return tripMergingList;
    }

    public ArrayList getTripNotMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripNotMergingList = new ArrayList();
        tripNotMergingList = reportDAO.getTripNotMergingList(reportTO, userId);
        return tripNotMergingList;
    }

    public ArrayList getCustomerMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerTripMergingList = new ArrayList();
        customerTripMergingList = reportDAO.getCustomerMergingList(reportTO, userId);
        return customerTripMergingList;
    }

    public ArrayList getCustomerTripNotMergingList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerTripNotMergingList = new ArrayList();
        customerTripNotMergingList = reportDAO.getCustomerTripNotMergingList(reportTO, userId);
        return customerTripNotMergingList;
    }

    public ArrayList getCustomerLists(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = new ArrayList();
        customerList = reportDAO.getCustomerLists(reportTO, userId);
        return customerList;
    }

    public ArrayList getPopupCustomerMergingProfitReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList popupCustomerMergingProfitReport = new ArrayList();
        popupCustomerMergingProfitReport = reportDAO.getPopupCustomerMergingProfitReportDetails(reportTO);
        return popupCustomerMergingProfitReport;

    }

    public ArrayList getTripExtraExpenseDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList BPCLTransactionDetails = new ArrayList();
        BPCLTransactionDetails = reportDAO.getTripExtraExpenseDetails(reportTO, userId);
        return BPCLTransactionDetails;

    }

    public ArrayList getVehicleDriverAdvanceDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDriverAdvanceDetails = new ArrayList();
        vehicleDriverAdvanceDetails = reportDAO.getVehicleDriverAdvanceDetails(reportTO, userId);
        return vehicleDriverAdvanceDetails;

    }

    public ArrayList getVehicleList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleList(reportTO);
        return vehicleList;
    }

    public ArrayList getTripVehicleList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getTripVehicleList(reportTO);
        return vehicleList;
    }

    public ArrayList getTripSheetListWfl(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripSheetList = new ArrayList();
        tripSheetList = reportDAO.getTripSheetListWfl(reportTO);
        return tripSheetList;
    }

    public String getWflHours(ReportTO reportTO) {
        String totalTripSummaryDetails = "";
        totalTripSummaryDetails = reportDAO.getWflHours(reportTO);
        return totalTripSummaryDetails;
    }

    public ArrayList getLatestUpdates(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList latestUpdates = new ArrayList();
        latestUpdates = reportDAO.getLatestUpdates(reportTO);
        return latestUpdates;
    }

    public ArrayList getToPayCustomerTripDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList ToPayCustomerTripDetails = new ArrayList();
        ToPayCustomerTripDetails = reportDAO.getToPayCustomerTripDetails(reportTO, userId);
        return ToPayCustomerTripDetails;

    }

    public ArrayList getTripVmrDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList TripVmrDetails = new ArrayList();
        TripVmrDetails = reportDAO.getTripVmrDetails(reportTO, userId);
        return TripVmrDetails;

    }

    public ArrayList getFcWiseTripBudgetDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList budgetDetails = new ArrayList();
        budgetDetails = reportDAO.getFcWiseTripBudgetDetails(reportTO, userId);
        return budgetDetails;

    }

    public ArrayList getAccountMgrPerformanceReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList budgetDetails = new ArrayList();
        budgetDetails = reportDAO.getAccountMgrPerformanceReportDetails(reportTO, userId);
        return budgetDetails;

    }

    public ArrayList getRNMExpenseReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList RnMExpenseDetails = new ArrayList();
        RnMExpenseDetails = reportDAO.getRNMExpenseReportDetails(reportTO, userId);
        return RnMExpenseDetails;

    }

    public ArrayList getTyreExpenseReportDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList RnMExpenseDetails = new ArrayList();
        RnMExpenseDetails = reportDAO.getRNMExpenseReportDetails(reportTO, userId);
        return RnMExpenseDetails;

    }

    public ArrayList getRNMVehicleList(String type) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getRNMVehicleList(type);
        return vehicleList;

    }

    public ArrayList getVehicleTyreList(String type) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = new ArrayList();
        vehicleList = reportDAO.getVehicleTyreList(type);
        return vehicleList;

    }

    public ArrayList processReportList(ReportTO reportTO) throws FPBusinessException, FPRuntimeException {
        ArrayList reportList = new ArrayList();
        reportList = reportDAO.getReportList(reportTO);
        //////System.out.println("reportList bp size"+reportList.size());

        return reportList;
    }

    public ArrayList getVehicleNoforReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleNoList = new ArrayList();
        vehicleNoList = reportDAO.getVehicleNoforReport(reportTO);
        return vehicleNoList;
    }

    public ArrayList getOrderExpenseRevenue(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList orderExpenseRevenue = new ArrayList();
        orderExpenseRevenue = reportDAO.getOrderExpenseRevenue(reportTO);
        return orderExpenseRevenue;
    }

    public ArrayList getTrailerMovementList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList trailerMovementList = new ArrayList();
        trailerMovementList = reportDAO.getTrailerMovementList(reportTO);
        return trailerMovementList;
    }

    public ArrayList getTripTrailerProfitDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripTrailerProfitDetails = new ArrayList();
        tripTrailerProfitDetails = reportDAO.getTripTrailerProfitDetails(reportTO, userId);
        return tripTrailerProfitDetails;
    }

    public ArrayList getTrailerWiseProfitList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        ArrayList vehicleEarnings = new ArrayList();
        ArrayList vehicleFixedExpensesList = new ArrayList();
        ArrayList vehicleOperationExpensesList = new ArrayList();
        ArrayList vehicleMaintainExpensesList = new ArrayList();
        ArrayList vehicleEarningsList = new ArrayList();
        ArrayList EarningsList = new ArrayList();
        vehicleDetailsList = reportDAO.getTrailerDetailsList(reportTO, userId);
        double freightAmount = 0.0;
        DprTO dprTO = new DprTO();
        ReportTO rpTO = new ReportTO();
        Iterator itr1 = vehicleDetailsList.iterator();
        ReportTO repTO1 = new ReportTO();
        while (itr1.hasNext()) {
            rpTO = new ReportTO();
            repTO1 = (ReportTO) itr1.next();
            reportTO.setTrailerId(repTO1.getTrailerId());
            vehicleEarnings = reportDAO.getTrailerEarningsList(reportTO, userId);
            if (vehicleEarnings.size() > 0) {
                Iterator itr2 = vehicleEarnings.iterator();
                while (itr2.hasNext()) {
                    dprTO = (DprTO) itr2.next();
                    //////System.out.println("repTO2.getTripNos() = " + dprTO.getTripNos());
                    //////System.out.println("repTO2.getFreightAmount() = " + dprTO.getFreightAmount());
                    rpTO.setTrailerId(dprTO.getTrailerId());
                    rpTO.setTripId(dprTO.getTripId());
                    rpTO.setTripNos(dprTO.getTripNos());
                    rpTO.setFreightAmount(dprTO.getFreightAmount());
                    rpTO.setOtherExpenseAmount(dprTO.getOtherExpenseAmount());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setTrailerId(repTO1.getTrailerId());
                rpTO.setTripNos("0");
                rpTO.setFreightAmount("0.00");
                rpTO.setOtherExpenseAmount("0.00");
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr = new DprTO();
            double driverSalary = 0;
            driverSalary = reportDAO.getVehicleDriverSalary(reportTO, userId);
            reportTO.setVehicleDriverSalary(driverSalary);
            vehicleFixedExpensesList = reportDAO.getTrailerFixedExpenseList(reportTO, userId);
            Iterator itr3 = vehicleFixedExpensesList.iterator();
            while (itr3.hasNext()) {
                dpr = (DprTO) itr3.next();
                rpTO.setInsuranceAmount(dpr.getInsuranceAmount());
                rpTO.setFcAmount(dpr.getFcAmount());
                rpTO.setRoadTaxAmount(dpr.getRoadTaxAmount());
                rpTO.setPermitAmount(dpr.getPermitAmount());
                rpTO.setEmiAmount(dpr.getEmiAmount());
                rpTO.setFixedExpensePerDay(dpr.getFixedExpensePerDay());
                rpTO.setTotlalFixedExpense(dpr.getTotlalFixedExpense());
                rpTO.setVehicleDriverSalary(dpr.getVehicleDriverSalary());
                driverSalary = dpr.getVehicleDriverSalary();
                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr1 = new DprTO();
            double totalOperationExpense = 0;
            vehicleOperationExpensesList = reportDAO.getTrailerOperationExpenseList(reportTO, userId);
            if (vehicleOperationExpensesList.size() > 0) {

                Iterator itr4 = vehicleOperationExpensesList.iterator();
                while (itr4.hasNext()) {
                    dpr1 = (DprTO) itr4.next();
                    rpTO.setTrailerExpence(dpr1.getTrailerExpence());
                    rpTO.setTrailerRevenue(dpr1.getTrailerRevenue());
                    totalOperationExpense = Double.parseDouble(dpr1.getTrailerExpence());
                    rpTO.setTotlalOperationExpense(totalOperationExpense);
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setTrailerId(repTO1.getTrailerId());
                rpTO.setTrailerExpence("0.00");
                rpTO.setTrailerRevenue("0.00");
                rpTO.setTotlalOperationExpense(0.00);

                //vehicleEarningsList.add(rpTO);
            }
            DprTO dpr2 = new DprTO();
            vehicleMaintainExpensesList = reportDAO.getTrailerMaintainExpenses(reportTO, userId);
            if (vehicleMaintainExpensesList.size() > 0) {

                Iterator itr5 = vehicleMaintainExpensesList.iterator();
                while (itr5.hasNext()) {
                    dpr2 = (DprTO) itr5.next();
                    rpTO.setMaintainExpense(dpr2.getMaintainExpense());
                    //vehicleEarningsList.add(rpTO);
                }
            } else {
                rpTO.setTrailerId(repTO1.getTrailerId());
                rpTO.setMaintainExpense(0.00);
                //vehicleEarningsList.add(rpTO);
            }
            rpTO.setNetExpense(rpTO.getTotlalFixedExpense() + rpTO.getTotlalOperationExpense() + rpTO.getMaintainExpense());
            freightAmount = Double.parseDouble(rpTO.getFreightAmount());
            rpTO.setNetProfit(freightAmount - rpTO.getNetExpense());
            vehicleEarningsList.add(rpTO);
        }
        return vehicleEarningsList;
    }

    public ArrayList getTrailerDetailsList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleDetailsList = new ArrayList();
        vehicleDetailsList = reportDAO.getTrailerDetailsList(reportTO, userId);
        return vehicleDetailsList;

    }

    public ArrayList getDashboardopsTruckNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTruckNos = new ArrayList();
        getTruckNos = reportDAO.getDashboardopsTruckNos(reportTO);
        return getTruckNos;
    }

    public ArrayList getDashboardWorkShop(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDashboardWorkShop = new ArrayList();
        getDashboardWorkShop = reportDAO.getDashboardWorkShop(reportTO);
        return getDashboardWorkShop;
    }

    public ArrayList getVehicleUT(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleMake = new ArrayList();
        vehicleMake = reportDAO.getVehicleUT(reportTO);
        return vehicleMake;
    }

    public ArrayList getVehicleUTChart() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleUTchart = new ArrayList();
        vehicleUTchart = reportDAO.getVehicleUTChart();
        return vehicleUTchart;
    }

    public ArrayList dbTrailerMake() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerMake = new ArrayList();
        trailerMake = reportDAO.dbTrailerMake();
        return trailerMake;
    }

    public ArrayList dbTruckType(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList truckType = new ArrayList();
        truckType = reportDAO.dbTruckType(reportTO);
        return truckType;
    }

    public ArrayList getOwnHireVehicleUT() throws FPRuntimeException, FPBusinessException {
        ArrayList ownHireUT = new ArrayList();
        ownHireUT = reportDAO.getOwnHireVehicleUT();
        return ownHireUT;
    }

    public ArrayList getOwnHireVehicleUT1() throws FPRuntimeException, FPBusinessException {
        ArrayList ownHireUT = new ArrayList();
        ownHireUT = reportDAO.getOwnHireVehicleUT1();
        return ownHireUT;
    }

    public ArrayList dbTrailerType() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerType = new ArrayList();
        trailerType = reportDAO.dbTrailerType();
        return trailerType;
    }

    public ArrayList getTrailerJobCardMTD() throws FPRuntimeException, FPBusinessException {
        ArrayList jobcardMTD = new ArrayList();
        jobcardMTD = reportDAO.getTrailerJobCardMTD();
        return jobcardMTD;
    }

    public ArrayList getTruckJobCardMTD() throws FPRuntimeException, FPBusinessException {
        ArrayList truckjobcardMTD = new ArrayList();
        truckjobcardMTD = reportDAO.getTruckJobCardMTD();
        return truckjobcardMTD;
    }

    public ArrayList getMofussilOrderdMTD() throws FPRuntimeException, FPBusinessException {
        ArrayList mofussilOrderdMTD = new ArrayList();
        mofussilOrderdMTD = reportDAO.getMofussilOrderdMTD();
        return mofussilOrderdMTD;
    }

    public ArrayList processMechPerformanceList(ReportTO repTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList mechPerformanceList = new ArrayList();
        mechPerformanceList = reportDAO.processMechPerformanceList(repTO, fromDate, toDate);

        if (mechPerformanceList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return mechPerformanceList;
    }

    public ArrayList processMechPerformanceData(ReportTO repTO, String fromDate, String toDate) throws FPRuntimeException, FPBusinessException {

        ArrayList mechPerformanceList = new ArrayList();
        mechPerformanceList = reportDAO.processMechPerformanceData(repTO, fromDate, toDate);

        if (mechPerformanceList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return mechPerformanceList;
    }

    public ArrayList ViewQBEntityList() throws FPRuntimeException, FPBusinessException {
        ArrayList ViewQBEntityList = new ArrayList();
        ViewQBEntityList = reportDAO.ViewQBEntityList();
        return ViewQBEntityList;
    }

    public ArrayList viewQueryBuilder() throws FPRuntimeException, FPBusinessException {
        String getPanNo = "";

        ArrayList viewTabels = new ArrayList();
        try {
            viewTabels = reportDAO.viewQueryBuilder();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return viewTabels;
    }

    public int saveQueryBuilderValues(ReportTO reportTO) {
        int status = 0;
        status = reportDAO.saveQueryBuilderValues(reportTO);
        return status;
    }

    public int editQBEntityDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = reportDAO.editQBEntityDetails(reportTO);
        return status;

    }

    public ArrayList ViewQBEntityListFilter(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ViewQBEntityListFilter = new ArrayList();
        ViewQBEntityListFilter = reportDAO.ViewQBEntityListFilter(reportTO);
        return ViewQBEntityListFilter;
    }

    public ArrayList viewQBReportList() throws FPRuntimeException, FPBusinessException {
        ArrayList viewQBReportList = new ArrayList();
        viewQBReportList = reportDAO.viewQBReportList();
        return viewQBReportList;

    }

    public ArrayList getQBEntityList() throws FPRuntimeException, FPBusinessException {
        ArrayList entitylist = new ArrayList();
        entitylist = reportDAO.getQBEntityList();
        return entitylist;
    }

    public ArrayList viewQBReportHeader(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewQBReportHeader = new ArrayList();
        viewQBReportHeader = reportDAO.viewQBReportHeader(reportTO);
        return viewQBReportHeader;
    }

    public String getReportName(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        String reportName = "";
        reportName = reportDAO.getReportName(reportTO);
        return reportName;
    }

    public String viewQBIndividualReportList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {

        String viewQBIndividualReportList = "";
        viewQBIndividualReportList = reportDAO.viewQBIndividualReportList(reportTO);
        return viewQBIndividualReportList;

    }

    public ArrayList getAvailableQBColumn(int entityId) throws FPRuntimeException, FPBusinessException {
        ArrayList getfuncrole = new ArrayList();
        getfuncrole = reportDAO.getAvailableQBColumn(entityId);
        return getfuncrole;
    }

    public ArrayList reportQBFilterValue(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList reportQBFilterValue = new ArrayList();
        reportQBFilterValue = reportDAO.reportQBFilterValue(reportTO);
        return reportQBFilterValue;
    }

    public ArrayList clientQBReportFilter(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList clientQBReportFilter = new ArrayList();
        clientQBReportFilter = reportDAO.clientQBReportFilter(reportTO);
        return clientQBReportFilter;
    }

    public ArrayList selectedReportQBColumn(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList selectedReportQBColumn = new ArrayList();
        selectedReportQBColumn = reportDAO.selectedReportQBColumn(reportTO);
        return selectedReportQBColumn;
    }

    public ArrayList editQBOnDemandReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList editQBOnDemandReport = new ArrayList();
        editQBOnDemandReport = reportDAO.editQBOnDemandReport(reportTO);
        return editQBOnDemandReport;
    }

    public ArrayList viewQBEntityDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewQBEntityDetails = new ArrayList();
        viewQBEntityDetails = reportDAO.viewQBEntityDetails(reportTO);
        return viewQBEntityDetails;
    }

    public ArrayList viewQBEntity(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList viewQBEntity = new ArrayList();
        viewQBEntity = reportDAO.viewQBEntity(reportTO);
        return viewQBEntity;
    }

    // QuertBuilder Ends Here
    public ArrayList getDailyTripPlaningDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getDailyTripPlaningDetails(reportTO, userId);
        return tripDetails;

    }

    public ArrayList getUserLoginList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userLoginList = new ArrayList();
        userLoginList = reportDAO.getUserLoginList(reportTO, userId);
        return userLoginList;

    }

    public ArrayList getUserLoginActivityDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList userLoginActivityList = new ArrayList();
        userLoginActivityList = reportDAO.getUserLoginActivityDetails(reportTO, userId);
        return userLoginActivityList;
    }

    public ArrayList getOrderWiseProfitDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList orderWiseProfitDetails = new ArrayList();
        orderWiseProfitDetails = reportDAO.getOrderWiseProfitDetails(reportTO);
        return orderWiseProfitDetails;
    }

    public ArrayList getEndedTripDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getEndedTripDetails(reportTO);
        return tripDetails;
    }

    public ArrayList driverSalaryReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList driverSalaryDetails = new ArrayList();
        driverSalaryDetails = reportDAO.driverSalaryReportDetails(reportTO);
        return driverSalaryDetails;
    }

    public ArrayList getTripExpensePrintDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripExpensePrintDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getTripExpenseSummaryDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripExpenseSummaryDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getTripReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripReportDetails(reportTO);
        return tripDetails;
    }

    public ArrayList getTripDetailsForOrder(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = reportDAO.getTripDetailsForOrder(reportTO);
        return tripDetails;
    }

    public ArrayList getShipperName(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShipperList = new ArrayList();
        getShipperList = reportDAO.getShipperName(reportTO);
        return getShipperList;
    }

    public ArrayList processGRSummaryList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShipperList = new ArrayList();
        getShipperList = reportDAO.processGRSummaryList(reportTO);
        return getShipperList;
    }

    public ArrayList dailyVehicleStatus(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getShipperList = new ArrayList();
        getShipperList = reportDAO.dailyVehicleStatus(reportTO);
        return getShipperList;
    }

    public ArrayList getVendorList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vendorList = new ArrayList();
        vendorList = reportDAO.getVendorList(reportTO);
        return vendorList;
    }

    public String getTripDieselCost(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        String dieselCost = "";
        dieselCost = reportDAO.getTripDieselCost(reportTO);
        return dieselCost;
    }

    public ArrayList getVehicleVendorwiseList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList getVehicleVendorwiseList = new ArrayList();
        getVehicleVendorwiseList = reportDAO.getVehicleVendorwiseList(reportTO, userId);
        return getVehicleVendorwiseList;
    }

    public ArrayList getDailyCashDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList dailyCashDetails = new ArrayList();
        dailyCashDetails = reportDAO.getDailyCashDetails(reportTO, userId);
        return dailyCashDetails;
    }

    public String getCashReceiveDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        String cashReceived = "";
        cashReceived = reportDAO.getCashReceiveDetails(reportTO, userId);
        return cashReceived;
    }

    public ArrayList processGRprofitabilityList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList processGRprofitabilityList = new ArrayList();
        processGRprofitabilityList = reportDAO.processGRprofitabilityList(reportTO);
        return processGRprofitabilityList;
    }

    public String getOrdersDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        String Orderdetails = "";
        Orderdetails = reportDAO.getOrdersDetails(reportTO, userId);
        return Orderdetails;
    }

    public ArrayList getContainerMovementList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList containerEmptyMovementList = new ArrayList();
        containerEmptyMovementList = reportDAO.getContainerMovementList(reportTO, userId);
        return containerEmptyMovementList;
    }

    public ArrayList getcontainerExportMovementList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList containerEmptyMovementList = new ArrayList();
        containerEmptyMovementList = reportDAO.getcontainerExportMovementList(reportTO, userId);
        return containerEmptyMovementList;
    }

    public ArrayList getcontainerImportMovementList(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList containerEmptyMovementList = new ArrayList();
        containerEmptyMovementList = reportDAO.getcontainerImportMovementList(reportTO, userId);
        return containerEmptyMovementList;
    }

    public ArrayList getVehicleDetainDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList VehicleDetainDetails = new ArrayList();
        VehicleDetainDetails = reportDAO.getVehicleDetainDetails(reportTO, userId);
        return VehicleDetainDetails;
    }

    public ArrayList getInvoiceEditLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceEditLogDetails = new ArrayList();
        invoiceEditLogDetails = reportDAO.getInvoiceEditLogDetails(reportTO, userId);
        return invoiceEditLogDetails;
    }

    public ArrayList tripStatusList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripStatusList = new ArrayList();
        tripStatusList = reportDAO.tripStatusList(reportTO);
        return tripStatusList;
    }

    public ArrayList getGpsVehiclelist(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList gpsVehiclelist = new ArrayList();
        gpsVehiclelist = reportDAO.getGpsVehiclelist(reportTO);
        return gpsVehiclelist;

    }

    public ArrayList invoiceDetailsReport(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceDetailsReport = new ArrayList();
        invoiceDetailsReport = reportDAO.invoiceDetailsReport(reportTO, userId);
        return invoiceDetailsReport;
    }

    public ArrayList getContractRateEditLogDetails(ReportTO reportTO, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList contractRateEditLogDetails = new ArrayList();
        contractRateEditLogDetails = reportDAO.getContractRateEditLogDetails(reportTO, userId);
        return contractRateEditLogDetails;
    }

    public ArrayList getBlockedGrSummaryDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList grSummaryList = new ArrayList();
        grSummaryList = reportDAO.getBlockedGrSummaryDetails(reportTO);
        return grSummaryList;
    }

    public ArrayList getInvoiceReport(ReportTO reportTO, String status, int userId) throws FPRuntimeException, FPBusinessException {
        ArrayList invoiceReport = new ArrayList();
        invoiceReport = reportDAO.getInvoiceReport(reportTO, status, userId);
        return invoiceReport;

    }

    public ArrayList getDispatchReport(ReportTO reportTO, int userId, String param) throws FPRuntimeException, FPBusinessException {
        ArrayList dispatchReport = new ArrayList();
        dispatchReport = reportDAO.getDispatchReport(reportTO, userId, param);
        return dispatchReport;

    }

    public ArrayList getExpenseTypeList() throws FPRuntimeException, FPBusinessException {
        ArrayList expenseList = new ArrayList();
        expenseList = reportDAO.getExpenseTypeList();
        return expenseList;
    }

    public ArrayList getGrExpenseDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList grExpenseDetails = new ArrayList();
        grExpenseDetails = reportDAO.getGrExpenseDetails(reportTO);
        return grExpenseDetails;
    }

    public ArrayList getEmailTemplate() throws FPRuntimeException, FPBusinessException {
        ArrayList emailTemplate = new ArrayList();
        emailTemplate = reportDAO.getEmailTemplate();
        return emailTemplate;
    }

    public int updateEmailTemplate(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int updateEmailTemplate = 0;
        updateEmailTemplate = reportDAO.updateEmailTemplate(reportTO);
        return updateEmailTemplate;
    }

    public String viewTemplateContent(ReportTO reportTO) throws JSONException, FPRuntimeException, FPBusinessException {
        String mailContent = "";
        mailContent = reportDAO.viewTemplateContent(reportTO);
        return mailContent;
    }

    public ArrayList getBasicReportList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList basicReportList = new ArrayList();
        basicReportList = reportDAO.getBasicReportList(reportTO);
        return basicReportList;
    }

    public ArrayList getRoleUserList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList roleUserList = new ArrayList();
        roleUserList = reportDAO.getRoleUserList(reportTO);
        return roleUserList;
    }

    public ArrayList getTotalPNRList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        System.out.println("im in");
        ArrayList totalCollectionList = new ArrayList();
        ArrayList pnrCollectionList = new ArrayList();
        ReportTO repTO = new ReportTO();
        Iterator itr = null;
        try {
            pnrCollectionList = new ArrayList();
            repTO.setParameterType("1");
            pnrCollectionList = reportDAO.getPNRList(repTO);
            if (pnrCollectionList.size() > 0) {
                repTO.setTodayList(pnrCollectionList);
            } else {
                repTO.setTodayList(null);
            }

            repTO.setParameterType("2");
            pnrCollectionList = reportDAO.getPNRList(repTO);
            if (pnrCollectionList.size() > 0) {
                repTO.setCurrentMonthList(pnrCollectionList);
            } else {
                repTO.setCurrentMonthList(null);
            }

            repTO.setParameterType("3");
            pnrCollectionList = reportDAO.getPNRList(repTO);
            if (pnrCollectionList.size() > 0) {
                repTO.setCurrentYearList(pnrCollectionList);
            } else {
                repTO.setCurrentYearList(null);
            }

            totalCollectionList.add(repTO);

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            pnrCollectionList = null;
            repTO = null;
            itr = null;
        }
        return totalCollectionList;
    }

    public String getPNRSummaryAlert(ArrayList nodList) throws FPRuntimeException, FPBusinessException {
        System.out.println("testformail" + nodList);
        String emailFormat2 = "";
        String emailFormat3 = "";
        String emailFormat4 = "";
        String emailFormat1 = "";
        String emailFormat = "";
        Iterator itr2 = null;
        Iterator itr1 = null;
        ReportTO repTO = null;
        ReportTO repTodayTO = null;
        try {
            if (nodList.size() > 0) {
                emailFormat1 = "<html>"
                        + "<body>"
                        + "<p style='font-style: italic;'><b>Dear Sir/Madam, <br><br>Please find below PNR Upload Summary Report <br><br/>"
                        + "<table align=\"center\" border=\"0\" id=\"table\" class=\"sortable\" style=\"width: 1100px\">\n"
                        + "                     <thead>\n"
                        + "                    <tr style=\" background-color: #666\">\n"
                        + "                            <td colspan=\"2\">&nbsp;</td>\n"
                        + "                            <td colspan=\"4\" style=\"text-align: center;color: white\"><b>Today Upload</b></td>\n"
                        + "                            <td colspan=\"4\" style=\"text-align: center;color: white\"><b>Monthly Upload</b></td>\n"
                        + "                            <td colspan=\"4\" style=\"text-align: center;color: white\"><b>Yearly Upload</b></td>\n"
                        + "                        </tr>\n"
                        + "                        <tr style=\"height:30px\" >\n"
                        + "                            <th><h3>PNR UPLOAD(Nos)</h3></th>\n"
                        + "                            <th><h3>20F Container(Nos)</h3></th>\n"
                        + "                            <th><h3>40F Container(Nos)</h3></th>\n"
                        + "                            <th><h3>PNR UPLOAD(Nos)</h3></th>\n"
                        + "                            <th><h3>20F Container(Nos)</h3></th>\n"
                        + "                            <th><h3>40F Container(Nos)</h3></th>\n"
                        + "                            <th><h3>PNR UPLOAD(Nos)</h3></th>\n"
                        + "                            <th><h3>20F Container(Nos)</h3></th>\n"
                        + "                            <th><h3>40F Container(Nos)</h3></th>\n"
                        + "                        </tr>\n"
                        + "                    </thead>\n"
                        + "                    <tbody>";
                itr1 = nodList.iterator();
                repTO = null;
                repTodayTO = null;
                int i = 1;
                while (itr1.hasNext()) {
                    repTO = new ReportTO();
                    repTO = (ReportTO) itr1.next();
                    emailFormat2 = emailFormat2 + "<tr style=\"height:30px\">";
                    if (repTO.getTodayList() != null) {
                        itr2 = repTO.getTodayList().iterator();
                        while (itr2.hasNext()) {
                            repTodayTO = new ReportTO();
                            repTodayTO = (ReportTO) itr2.next();
                            emailFormat2 = emailFormat2 + "<td style=\"text-align: right;background-color: #FCF7F7\">" + repTodayTO.getPNRNO() + "</td>" + "<td style=\"text-align: right;background-color: #FCF7F7\">" + repTodayTO.getC20F() + "</td>" + "<td style=\"text-align: right;background-color: #FCF7F7\">" + repTodayTO.getC40F() + "</td>";
                        }

                    } else if (repTO.getTodayList() == null) {
                        emailFormat2 = emailFormat2 + "<td style=\"text-align: right;background-color: #FCF7F7\">NA</td>" + "<td style=\"text-align: right;background-color: #FCF7F7\">NA</td>" + "<td style=\"text-align: right;background-color: #FCF7F7\">NA</td>" + "<td style=\"text-align: right;background-color: #FCF7F7\">NA</td>";

                    }
                    if (repTO.getCurrentMonthList() != null) {
                        itr2 = repTO.getCurrentMonthList().iterator();
                        while (itr2.hasNext()) {
                            repTodayTO = new ReportTO();
                            repTodayTO = (ReportTO) itr2.next();
                            emailFormat2 = emailFormat2 + "<td style=\"text-align: right;background-color: #FDEDED\">" + repTodayTO.getPNRNO() + "</td>" + "<td style=\"text-align: right;background-color: #FDEDED\">" + repTodayTO.getC20F() + "</td>" + "<td style=\"text-align: right;background-color: #FDEDED\">" + repTodayTO.getC40F() + "</td>";
                        }

                    } else if (repTO.getCurrentMonthList() == null) {
                        emailFormat2 = emailFormat2 + "<td style=\"text-align: right;background-color: #FDEDED\">NA</td>" + "<td style=\"text-align: right;background-color: #FDEDED\">NA</td>" + "<td style=\"text-align: right;background-color: #FDEDED\">NA</td>" + "<td style=\"text-align: right;background-color: #FDEDED\">NA</td>";

                    }
                    if (repTO.getCurrentYearList() != null) {
                        itr2 = repTO.getCurrentYearList().iterator();
                        while (itr2.hasNext()) {
                            repTodayTO = new ReportTO();
                            repTodayTO = (ReportTO) itr2.next();
                            emailFormat2 = emailFormat2 + "<td style=\"text-align: right;background-color: #F1E4E4\">" + repTodayTO.getPNRNO() + "</td>" + "<td style=\"text-align: right;background-color: #F1E4E4\">" + repTodayTO.getC20F() + "</td>" + "<td style=\"text-align: right;background-color: #F1E4E4\">" + repTodayTO.getC40F() + "</td>";
                        }

                    } else if (repTO.getCurrentYearList() == null) {
                        emailFormat2 = emailFormat2 + "<td style=\"text-align: right;background-color: #F1E4E4\">NA</td>" + "<td style=\"text-align: right;background-color: #F1E4E4\">NA</td>" + "<td style=\"text-align: right;background-color: #F1E4E4\">NA</td>" + "<td style=\"text-align: right;background-color: #F1E4E4\">NA</td>";

                    }
                    emailFormat2 = emailFormat2 + "</tr>";
                    i = i + 1;
                }
                emailFormat2 = emailFormat2 + "</body></html>";
                emailFormat3 = "<br><br><br>"
                        + "<html>"
                        + "<body>"
                        + "<table>"
                        + "<tr>"
                        + "<br><br><br>Regards,"
                        + "<br><br/>Route Logistics India Pvt.Ltd.<br/><br/>This is an Auto Generated Email Please Do Not Reply"
                        + " .</p>"
                        + "</tr>"
                        + "</table>"
                        + "</body></html>";
                emailFormat = emailFormat1 + "" + emailFormat2 + "" + emailFormat3 + "";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            emailFormat2 = null;
            emailFormat3 = null;
            emailFormat4 = null;
            emailFormat1 = null;
            itr2 = null;
            itr1 = null;
            repTO = null;
            repTodayTO = null;
        }
        return emailFormat;
    }

    public ArrayList getEmailList() throws FPRuntimeException, FPBusinessException {
        ArrayList pendingSMSList = new ArrayList();
        pendingSMSList = reportDAO.getEmailList();
        return pendingSMSList;
    }

    public ArrayList getImportLCLmovement(ReportTO reoprtTO) throws FPRuntimeException, FPBusinessException {
        ArrayList importLCLmovement = new ArrayList();
        importLCLmovement = reportDAO.getImportLCLmovement(reoprtTO);
        return importLCLmovement;
    }

    public ArrayList getImportICDmovement(ReportTO reoprtTO) throws FPRuntimeException, FPBusinessException {
        ArrayList importICDmovement = new ArrayList();
        importICDmovement = reportDAO.getImportICDmovement(reoprtTO);
        return importICDmovement;
    }

    public ArrayList getImportFCLmovement(ReportTO reoprtTO) throws FPRuntimeException, FPBusinessException {
        ArrayList importFCLmovement = new ArrayList();
        importFCLmovement = reportDAO.getImportFCLmovement(reoprtTO);
        return importFCLmovement;
    }

    public ArrayList getExportFCLmovement(ReportTO reoprtTO) throws FPRuntimeException, FPBusinessException {
        ArrayList exportFCLmovement = new ArrayList();
        exportFCLmovement = reportDAO.getExportFCLmovement(reoprtTO);
        return exportFCLmovement;
    }

    public ArrayList getPendingVesselList(ReportTO reoprtTO) throws FPRuntimeException, FPBusinessException {
        ArrayList pendingVesselList = new ArrayList();
        pendingVesselList = reportDAO.getPendingVesselList(reoprtTO);
        return pendingVesselList;
    }

    public ArrayList getExpiredVesselList(ReportTO reoprtTO) throws FPRuntimeException, FPBusinessException {
        ArrayList expiredVesselList = new ArrayList();
        expiredVesselList = reportDAO.getExpiredVesselList(reoprtTO);
        return expiredVesselList;
    }

    public ArrayList getMovementWiseOrderNos(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getStatusWiseTripNos = new ArrayList();
        getStatusWiseTripNos = reportDAO.getMovementWiseOrderNos(reportTO);
        return getStatusWiseTripNos;
    }

    public ArrayList nodReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodList = new ArrayList();
        nodList = reportDAO.nodReport(reportTO);
        return nodList;
    }

    public ArrayList nodReports(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLists = new ArrayList();
        nodLists = reportDAO.nodReports(reportTO);
        return nodLists;
    }

    public ArrayList nodReportLevel2(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLevel2List = new ArrayList();
        nodLevel2List = reportDAO.nodReportLevel2(reportTO);
        return nodLevel2List;
    }

    public ArrayList nodLevel3List(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList nodLevel3List = new ArrayList();
        nodLevel3List = reportDAO.nodReportLevel3(reportTO);
        return nodLevel3List;
    }

    public ArrayList processVendorPriceLists(ReportTO reportTO) throws FPBusinessException, FPRuntimeException {
        ArrayList vendorPriceList = new ArrayList();
        vendorPriceList = reportDAO.processVendorPriceLists(reportTO);
        return vendorPriceList;
    }

    public ArrayList dayWiseCollection(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList dayWiseCollection = new ArrayList();
        dayWiseCollection = reportDAO.dayWiseCollection(reportTO);
        return dayWiseCollection;
    }

    public ArrayList getListOfTripSHeet() throws FPRuntimeException, FPBusinessException {
        ArrayList listOfTripSheet = new ArrayList();
        listOfTripSheet = reportDAO.getListOfTripSHeet();
        return listOfTripSheet;
    }

    public ArrayList getTripContainerDetails() throws FPRuntimeException, FPBusinessException {
        ArrayList tripContainerDetails = new ArrayList();
        tripContainerDetails = reportDAO.getTripContainerDetails();
        return tripContainerDetails;
    }

    public ArrayList getTripVehicleListForTally() throws FPRuntimeException, FPBusinessException {
        ArrayList tripVehicleList = new ArrayList();
        tripVehicleList = reportDAO.getTripVehicleListForTally();
        return tripVehicleList;
    }

    public ArrayList getTripNameMappingList() throws FPRuntimeException, FPBusinessException {
        ArrayList tripNameMapping = new ArrayList();
        tripNameMapping = reportDAO.getTripNameMappingList();
        return tripNameMapping;
    }

    public ArrayList getAccountTransationList() throws FPRuntimeException, FPBusinessException {
        ArrayList accountTransation = new ArrayList();
        accountTransation = reportDAO.getAccountTransationList();
        return accountTransation;
    }

    public ArrayList getTransationDetailsList() throws FPRuntimeException, FPBusinessException {
        ArrayList ransationDetails = new ArrayList();
        ransationDetails = reportDAO.getTransationDetailsList();
        return ransationDetails;
    }

    public ArrayList getInventoryDetailsList() throws FPRuntimeException, FPBusinessException {
        ArrayList inventoryDetails = new ArrayList();
        inventoryDetails = reportDAO.getInventoryDetailsList();
        return inventoryDetails;
    }

    public ArrayList getTallyHeadList() throws FPBusinessException, FPRuntimeException {
        ArrayList tallyHeadList = new ArrayList();
        tallyHeadList = reportDAO.getTallyHeadList();

        return tallyHeadList;
    }

    public ArrayList getTripsMovingTowardsClearance() throws FPBusinessException, FPRuntimeException {
        ArrayList tripList = new ArrayList();
        tripList = reportDAO.getTripsMovingTowardsClearance();
        System.out.println("tripList-------" + tripList.size());
        return tripList;
    }

    public int saveTallyHeads(String[] expenseId, String[] tallyName, int userId) {
        int status = 0;
        for (int i = 0; i < expenseId.length; i++) {
            try {
                if (!"0".equals(expenseId[i]) && !"".equals(tallyName[i]) && tallyName[i] != null) {
                    status = reportDAO.saveTallyHeads(expenseId[i], tallyName[i], userId);
                }
            } catch (NullPointerException e) {
            }
        }
        return status;
    }

    public int saveTallyDriverHeads(String[] expenseId, String[] tallyName, int userId) {
        int status = 0;
        for (int i = 0; i < expenseId.length; i++) {
            try {
                if (!"0".equals(expenseId[i]) && !"".equals(tallyName[i]) && tallyName[i] != null) {
                    status = reportDAO.saveTallyDriverHeads(expenseId[i], tallyName[i], userId);
                }
            } catch (NullPointerException e) {
            }
        }
        return status;
    }

    public int saveTallyTruckHeads(String[] expenseId, String[] tallyName, int userId) {
        int status = 0;
        for (int i = 0; i < expenseId.length; i++) {
            try {
                if (!"0".equals(expenseId[i]) && !"".equals(tallyName[i]) && tallyName[i] != null) {
                    status = reportDAO.saveTallyTruckHeads(expenseId[i], tallyName[i], userId);
                }
            } catch (NullPointerException e) {
            }
        }
        return status;
    }

    public ArrayList getTallySalesList() throws FPBusinessException, FPRuntimeException {
        ArrayList tallySalesList = new ArrayList();
        tallySalesList = reportDAO.getTallySalesList();

        return tallySalesList;
    }

    public int saveTallySalesConversion(String tripNo, String tranNo, int userId) {
        int status = 0;
        SqlMapClient session = reportDAO.getSqlMapClient();
        Exception excp = null;
        try {
            session.startTransaction();
            status = reportDAO.saveTallySalesConversion(tripNo, tranNo, userId, session);
            session.commitTransaction();
        } catch (Exception e) {
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public ArrayList getTallyPurchaceList() throws FPBusinessException, FPRuntimeException {
        ArrayList tallypurchaceList = new ArrayList();
        tallypurchaceList = reportDAO.getTallyPurchaceList();
        return tallypurchaceList;
    }

    public ArrayList getTallyDriverHead() throws FPBusinessException, FPRuntimeException {
        ArrayList tallyDriverHead = new ArrayList();
        tallyDriverHead = reportDAO.getTallyDriverHead();
        return tallyDriverHead;
    }

    public ArrayList getTallyTruckHead() throws FPBusinessException, FPRuntimeException {
        ArrayList tallyTruckHead = new ArrayList();
        tallyTruckHead = reportDAO.getTallyTruckHead();
        return tallyTruckHead;
    }

    public ArrayList fleetStrengthChn() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleMake = new ArrayList();
        vehicleMake = reportDAO.fleetStrengthChn();
        return vehicleMake;
    }

    public ArrayList fleetStrengthVisaz() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleMake = new ArrayList();
        vehicleMake = reportDAO.fleetStrengthVisaz();
        return vehicleMake;
    }

    public ArrayList fleetStrengthKrsptn() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleMake = new ArrayList();
        vehicleMake = reportDAO.fleetStrengthKrsptn();
        return vehicleMake;
    }

    public ArrayList getFleetStrengthList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fleetCenterList = new ArrayList();
        ArrayList vehicleTypeList = new ArrayList();
        ArrayList vehicleModelList = new ArrayList();
        ArrayList fleetVehicleCount = new ArrayList();
        String vehicleCount = "0";
        fleetCenterList = reportDAO.getFleetCenterList(reportTO);
        vehicleTypeList = reportDAO.getVehicleTypeList(reportTO);
        vehicleModelList = reportDAO.getVehicleModeleList(reportTO);

        Iterator itr1 = fleetCenterList.iterator();
        while (itr1.hasNext()) {
            ReportTO repTO1 = new ReportTO();
            repTO1 = (ReportTO) itr1.next();

            Iterator itr2 = vehicleTypeList.iterator();
            while (itr2.hasNext()) {
                ReportTO dprTO = new ReportTO();
                dprTO = (ReportTO) itr2.next();

                System.out.println("dprTO.getVehicleTypeId()========" + dprTO.getVehicleTypeId());

                Iterator itr3 = vehicleModelList.iterator();
                while (itr3.hasNext()) {
                    ReportTO dpr = new ReportTO();
                    dpr = (ReportTO) itr3.next();

                    ReportTO reportTO2 = new ReportTO();
                    reportTO2.setFleetCenterId(repTO1.getCompId());
                    System.out.println("repTO1.getCompId()========" + repTO1.getCompId());
                    reportTO2.setModelId(dpr.getModelName());
                    System.out.println("dpr.getModelName()========" + dpr.getModelName());
                    reportTO2.setVehicletype(dprTO.getVehicleTypeId());
                    reportTO2.setVehicleTypeName(dprTO.getVehicleTypeName());

                    vehicleCount = reportDAO.getVehicleCount(reportTO2);
                    System.out.println("vehicleCount1=" + vehicleCount);
                    if (vehicleCount == null) {
                        vehicleCount = "0";
                    }
                    System.out.println("vehicleCount2=" + vehicleCount);
                    reportTO2.setVehCount(vehicleCount);

                    fleetVehicleCount.add(reportTO2);
                }
            }

        }
        return fleetVehicleCount;
    }

    public ArrayList getFleetCenterList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList fleetCenterList = new ArrayList();
        fleetCenterList = reportDAO.getFleetCenterList(reportTO);
        return fleetCenterList;
    }

    public ArrayList getVehicleTypeList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleTypeList = new ArrayList();
        vehicleTypeList = reportDAO.getVehicleTypeList(reportTO);
        return vehicleTypeList;
    }

    public ArrayList getVehicleModeleList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleModelList = new ArrayList();
        vehicleModelList = reportDAO.getVehicleModeleList(reportTO);
        return vehicleModelList;
    }

    public String getVehicleStatusReport(String mailTemplate) throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("system date = " + systemTime);
        String emailFormat = "";
        String fromDate = "";
        String endDate = "";

        String startDate = "";

        try {

            String name = "CALOG_Vehicle-Status-Report_" + systemTime + ".xls";
            String filename = name;
            String sheetName = "";
            HSSFWorkbook my_workbook = new HSSFWorkbook();

            boolean check = false;
            String filepath = "";

            File theDir = new File(ThrottleConstants.alertReportPath);
            if (!theDir.exists()) {
                boolean result = theDir.mkdir();
                if (result) {
                    System.out.println(ThrottleConstants.alertReportPath + " Created");
                }
            }

            filepath = ThrottleConstants.alertReportPath + name;
            System.out.println("filename = " + filepath);

            ReportTO reportTO = new ReportTO();
            reportTO.setCompanyId("1461");
            ArrayList wflList = new ArrayList();
            wflList = getWflList(reportTO);

            ArrayList wfuList = new ArrayList();
            wfuList = getWfuList(reportTO);

            ArrayList tripInProgressList = new ArrayList();
            tripInProgressList = getTripInProgressList(reportTO);
            ArrayList tripInProgressHireList = new ArrayList();
            tripInProgressHireList = getTripInProgressHireList(reportTO);
            ArrayList jobCardList = new ArrayList();
            jobCardList = getJobCardList(reportTO);

            ArrayList tripNotStartedList = new ArrayList();
            tripNotStartedList = reportDAO.getTripNotStartedList(reportTO);
            System.out.println("tripNotStartedList::" + tripNotStartedList.size());
            Iterator itr = wflList.iterator();
            ReportTO repTO = new ReportTO();

            Iterator itr1 = wfuList.iterator();
            ReportTO repTO1 = new ReportTO();

            Iterator itr2 = tripInProgressList.iterator();
            ReportTO repTO2 = new ReportTO();

            Iterator itr4 = jobCardList.iterator();
            ReportTO repTO4 = new ReportTO();

            Iterator itr3 = tripNotStartedList.iterator();
            ReportTO repTO3 = new ReportTO();

            Iterator itr5 = tripInProgressHireList.iterator();
            ReportTO repTO5 = new ReportTO();
            
            int cntr = 1;
            int wfl_North_West_Corridor = 0;
            int wfl_North_South_Corridor = 0;
            int wfl_Dedicate = 0;
            int wfl_General = 0;

            int wfu_North_West_Corridor = 0;
            int wfu_North_South_Corridor = 0;
            int wfu_Dedicate = 0;
            int wfu_General = 0;

            int notStart_North_West_Corridor = 0;
            int notStart_North_South_Corridor = 0;
            int notStart_Dedicate = 0;
            int notStart_General = 0;

            int progress_North_West_Corridor = 0;
            int progress_North_South_Corridor = 0;
            int progress_Dedicate = 0;
            int progress_General = 0;

            int job_North_West_Corridor = 0;
            int job_North_South_Corridor = 0;
            int job_Dedicate = 0;
            int job_General = 0;
            String mailContent = "";
            int chennaiTotal = 0;
            int tutiTotal = 0;
            int vizagTotal = 0;
            int kpctTotal = 0;

            while (itr.hasNext()) {
                repTO = (ReportTO) itr.next();
                if ("CA LOG - CHENNAI".equals(repTO.getOperationPoint())) {
                    wfl_North_West_Corridor++;
                    chennaiTotal++;
                } else if ("CA LOG - TUTICORIN".equals(repTO.getOperationPoint())) {
                    wfl_North_South_Corridor++;
                    tutiTotal++;
                } else if ("CA LOG - VIZAG".equals(repTO.getOperationPoint())) {
                    vizagTotal++;
//                } else if ("CA LOG - KPCT".equals(repTO.getOperationPoint())) {
//                    wfl_General++;
//                    kpctTotal++;
                }
            }

            while (itr1.hasNext()) {
                repTO = (ReportTO) itr1.next();
                if ("CA LOG - CHENNAI".equals(repTO.getOperationPoint())) {
                    wfu_North_West_Corridor++;
                    chennaiTotal++;
                } else if ("CA LOG - TUTICORIN".equals(repTO.getOperationPoint())) {
                    wfu_North_South_Corridor++;
                    tutiTotal++;
                } else if ("CA LOG - VIZAG".equals(repTO.getOperationPoint())) {
                    vizagTotal++;
//                } else if ("CA LOG - KPCT".equals(repTO.getOperationPoint())) {
//                    wfu_General++;
//                    kpctTotal++;
                }
            }
            while (itr2.hasNext()) {
                repTO2 = (ReportTO) itr2.next();
                ////System.out.println("repTO.getOperationPoint() 3 = " + repTO.getOperationPoint());
                if ("CA LOG - CHENNAI".equals(repTO2.getOperationPoint())) {
                    progress_North_West_Corridor++;
                    chennaiTotal++;
                } else if ("CA LOG - TUTICORIN".equals(repTO2.getOperationPoint())) {
                    progress_North_South_Corridor++;
                    tutiTotal++;
                } else if ("CA LOG - VIZAG".equals(repTO2.getOperationPoint())) {
                    progress_Dedicate++;
                    vizagTotal++;
//                } else if ("CA LOG - KPCT".equals(repTO2.getOperationPoint())) {
//                    progress_General++;
//                    kpctTotal++;
                }
            }
            while (itr3.hasNext()) {
                repTO3 = (ReportTO) itr3.next();
                ////System.out.println("repTO.getOperationPoint()4  = " + repTO.getOperationPoint());
                if ("CA LOG - CHENNAI".equals(repTO3.getOperationPoint())) {
                    notStart_North_West_Corridor++;
                    chennaiTotal++;
                } else if ("CA LOG - TUTICORIN".equals(repTO3.getOperationPoint())) {
                    notStart_North_South_Corridor++;
                    tutiTotal++;
                } else if ("CA LOG - VIZAG".equals(repTO3.getOperationPoint())) {
                    notStart_Dedicate++;
                    vizagTotal++;
//                } else if ("CA LOG - KPCT".equals(repTO3.getOperationPoint())) {
//                    notStart_General++;
//                    kpctTotal++;
                }
            }

            while (itr4.hasNext()) {
                repTO4 = (ReportTO) itr4.next();
                ////System.out.println("repTO.getOperationPoint() 5 = " + repTO.getOperationPoint());
                if ("CA LOG - CHENNAI".equals(repTO4.getOperationPoint())) {
                    job_North_West_Corridor++;
                    chennaiTotal++;
                } else if ("CA LOG - TUTICORIN".equals(repTO4.getOperationPoint())) {
                    job_North_South_Corridor++;
                    tutiTotal++;
                } else if ("CA LOG - VIZAG".equals(repTO4.getOperationPoint())) {
                    job_Dedicate++;
                    vizagTotal++;
//                } else if ("CA LOG - KPCT".equals(repTO4.getOperationPoint())) {
//                    job_General++;
//                    kpctTotal++;
                }
            }

            for (int k = 0; k <= 6; k++) {
                System.out.println("KKKKKKKKKK----"+k);
                if (k == 0) {
                    sheetName = "Summary";
                    HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                    HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                    HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                    style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                    style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                    HSSFRow header = my_sheet.createRow(0);
                    header.setHeightInPoints(20); // row hight

                    HSSFCell cell1 = header.createCell((short) 0);
                    style.setWrapText(true);
                    cell1.setCellValue("Vehicle Status");
                    cell1.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 0, (short) 4000);

                    HSSFCell cell2 = header.createCell((short) 1);
                    cell2.setCellValue("Chennai");
                    cell2.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 1, (short) 6000);

//                    HSSFCell cell3 = header.createCell((short) 2);
//                    cell3.setCellValue("KPCT");
//                    cell3.setCellStyle(style);
//                    my_sheet.setColumnWidth((short) 2, (short) 6000);
                    HSSFCell cell4 = header.createCell((short) 2);
                    cell4.setCellValue("Total");
                    cell4.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 2, (short) 6000);

                    cntr = 1;
                    HSSFRow row = my_sheet.createRow(cntr);

                    row = my_sheet.createRow((short) cntr);
                    row.createCell((short) 0).setCellValue("WFL");
                    row.getCell((short) 0).setCellStyle(style1);
                    row.createCell((short) 1).setCellValue(notStart_North_West_Corridor);
                    row.getCell((short) 1).setCellStyle(style1);
//                    row.createCell((short) 2).setCellValue(notStart_General);
//                    row.getCell((short) 2).setCellStyle(style1);
                    row.createCell((short) 2).setCellValue((notStart_General + notStart_North_West_Corridor));
                    row.getCell((short) 2).setCellStyle(style1);
                    cntr++;
                    System.out.println("mailContent befor::" + mailContent);
                    mailContent = "<tr><td>WFL</td><td align='right'>" + notStart_North_West_Corridor + "</td><td align='right'>" + (notStart_General + notStart_North_West_Corridor) + "</td></tr>";
                    System.out.println("mailContent afteer::" + mailContent);

                    row = my_sheet.createRow(cntr);
                    row = my_sheet.createRow((short) cntr);
                    row.createCell((short) 0).setCellValue("Idle - No Load");
                    row.getCell((short) 0).setCellStyle(style1);
                    row.createCell((short) 1).setCellValue(wfl_North_West_Corridor);
                    row.getCell((short) 1).setCellStyle(style1);
//                    row.createCell((short) 2).setCellValue(wfl_General);
//                    row.getCell((short) 2).setCellStyle(style1);
                    row.createCell((short) 2).setCellValue((wfl_General + wfl_North_West_Corridor));
                    row.getCell((short) 2).setCellStyle(style1);
                    cntr++;
                    mailContent = mailContent + "<tr><td>Idle - No Load</td><td align='right'>" + wfl_North_West_Corridor + "</td><td align='right'>" + (wfl_General + wfl_North_West_Corridor) + "</td></tr>";

                    row = my_sheet.createRow(cntr);
                    row = my_sheet.createRow((short) cntr);
                    row.createCell((short) 0).setCellValue("Idle - No Driver");
                    row.getCell((short) 0).setCellStyle(style1);
                    row.createCell((short) 1).setCellValue(wfu_North_West_Corridor);
                    row.getCell((short) 1).setCellStyle(style1);
//                    row.createCell((short) 2).setCellValue(wfu_General);
//                    row.getCell((short) 2).setCellStyle(style1);
                    row.createCell((short) 2).setCellValue((wfu_General + wfu_North_West_Corridor));
                    row.getCell((short) 2).setCellStyle(style1);
                    cntr++;
                    mailContent = mailContent + "<tr><td>Idle - No Driver</td><td align='right'>" + wfu_North_West_Corridor + "</td><td align='right'>" + (wfu_General + wfu_North_West_Corridor) + "</td></tr>";

                    row = my_sheet.createRow(cntr);
                    row = my_sheet.createRow((short) cntr);
                    row.createCell((short) 0).setCellValue("Trip In Progress");
                    row.getCell((short) 0).setCellStyle(style1);
                    row.createCell((short) 1).setCellValue(progress_North_West_Corridor);
                    row.getCell((short) 1).setCellStyle(style1);
//                    row.createCell((short) 2).setCellValue(progress_General);
//                    row.getCell((short) 2).setCellStyle(style1);
                    row.createCell((short) 2).setCellValue((progress_North_West_Corridor + progress_General));
                    row.getCell((short) 2).setCellStyle(style1);
                    cntr++;
                    mailContent = mailContent + "<tr><td>Trip In Progress</td><td align='right'>" + progress_North_West_Corridor + "</td><td align='right'>" + (progress_North_West_Corridor + progress_General) + "</td></tr>";

                    row = my_sheet.createRow(cntr);
                    row = my_sheet.createRow((short) cntr);
                    row.createCell((short) 0).setCellValue("Workshop");
                    row.getCell((short) 0).setCellStyle(style1);
                    row.createCell((short) 1).setCellValue(job_North_West_Corridor);
                    row.getCell((short) 1).setCellStyle(style1);
//                    row.createCell((short) 2).setCellValue(job_General);
//                    row.getCell((short) 2).setCellStyle(style1);
                    row.createCell((short) 2).setCellValue((job_General + job_North_West_Corridor));
                    row.getCell((short) 2).setCellStyle(style1);
                    cntr++;
                    mailContent = mailContent + "<tr><td>Workshop</td><td align='right'>" + job_North_West_Corridor + "</td><td align='right'>" + (job_North_West_Corridor + job_General) + "</td></tr>";

                    row = my_sheet.createRow(cntr);
                    row = my_sheet.createRow((short) cntr);
                    row.createCell((short) 0).setCellValue("Total");
                    row.getCell((short) 0).setCellStyle(style1);
                    row.createCell((short) 1).setCellValue(chennaiTotal);
                    row.getCell((short) 1).setCellStyle(style1);
//                    row.createCell((short) 2).setCellValue(kpctTotal);
//                    row.getCell((short) 2).setCellStyle(style1);
                    row.createCell((short) 2).setCellValue((chennaiTotal + kpctTotal));
                    row.getCell((short) 2).setCellStyle(style1);
                    mailContent = mailContent + "<tr><td align='right'><b>Total</b></td><td align='right'><b>" + chennaiTotal + "</b></td><td align='right'><b>" + (chennaiTotal + kpctTotal) + "</b></td></tr>";

                    cntr++;
                } else if (k == 1) {
                    sheetName = "WFL";

                    HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                    HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                    HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                    style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                    //                            style.setBorderTop((short) 1);
                    style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                    HSSFRow header = my_sheet.createRow(0);
                    header.setHeightInPoints(20); // row hight

                    HSSFCell cell1 = header.createCell((short) 0);
                    style.setWrapText(true);
                    cell1.setCellValue("SNo");
                    cell1.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 0, (short) 4000);

                    HSSFCell cell2 = header.createCell((short) 1);
                    cell2.setCellValue("TruckNo");
                    cell2.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 1, (short) 6000);

//                    HSSFCell cell3 = header.createCell((short) 2);
//                    cell3.setCellValue("Branch");
//                    cell3.setCellStyle(style);
//                    my_sheet.setColumnWidth((short) 2, (short) 6000);

                    HSSFCell cell31 = header.createCell((short) 2);
                    cell31.setCellValue("VehicleType");
                    cell31.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 2, (short) 6000);
                    
                    HSSFCell cell4 = header.createCell((short) 3);
                    cell4.setCellValue("Driver Name");
                    cell4.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 3, (short) 6000);
                    
                    HSSFCell cell5 = header.createCell((short) 4);
                    cell5.setCellValue("Driver Mobile");
                    cell5.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 4, (short) 6000);
                    
                    HSSFCell cell6 = header.createCell((short) 5);
                    cell6.setCellValue("Period");
                    cell6.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 5, (short) 6000);

                    HSSFCell cell7 = header.createCell((short) 6);
                    cell7.setCellValue("No of Days in Idle");
                    cell7.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 6, (short) 6000);

                    cntr = 1;
                    itr = tripNotStartedList.iterator();
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        HSSFRow row = my_sheet.createRow(cntr);
                        row = my_sheet.createRow((short) cntr);
                        row.createCell((short) 0).setCellValue(cntr);
                        row.getCell((short) 0).setCellStyle(style1);
                        row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                        row.getCell((short) 1).setCellStyle(style1);
//                        row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                        row.getCell((short) 2).setCellStyle(style1);
                        row.createCell((short) 2).setCellValue(repTO.getTonnage());
                        row.getCell((short) 2).setCellStyle(style1);
                        row.createCell((short) 3).setCellValue(repTO.getDriverName());
                        row.getCell((short) 3).setCellStyle(style1);
                        row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                        row.getCell((short) 4).setCellStyle(style1);
                        row.createCell((short) 5).setCellValue(repTO.getPeriod());
                        row.getCell((short) 5).setCellStyle(style1);
                        row.createCell((short) 6).setCellValue(repTO.getAge());
                        row.getCell((short) 6).setCellStyle(style1);
                        cntr++;
                    }

                } else if (k == 2) {
                    sheetName = "Idle - No Load";

                    HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                    HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                    HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                    style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                    style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                    HSSFRow header = my_sheet.createRow(0);
                    header.setHeightInPoints(20); // row hight

                    HSSFCell cell1 = header.createCell((short) 0);
                    style.setWrapText(true);
                    cell1.setCellValue("SNo");
                    cell1.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 0, (short) 4000);

                    HSSFCell cell2 = header.createCell((short) 1);
                    cell2.setCellValue("TruckNo");
                    cell2.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 1, (short) 6000);

//                    HSSFCell cell3 = header.createCell((short) 2);
//                    cell3.setCellValue("Branch");
//                    cell3.setCellStyle(style);
//                    my_sheet.setColumnWidth((short) 2, (short) 6000);

                    HSSFCell cell31 = header.createCell((short) 2);
                    cell31.setCellValue("VehicleType");
                    cell31.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 2, (short) 6000);
                    
                    HSSFCell cell4 = header.createCell((short) 3);
                    cell4.setCellValue("Driver Name");
                    cell4.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 3, (short) 6000);
                    
                    HSSFCell cell5 = header.createCell((short) 4);
                    cell5.setCellValue("Driver Mobile");
                    cell5.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 4, (short) 6000);
                    
                    HSSFCell cell6 = header.createCell((short) 5);
                    cell6.setCellValue("Period");
                    cell6.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 5, (short) 6000);

                    HSSFCell cell7 = header.createCell((short) 6);
                    cell7.setCellValue("No of Days in Load");
                    cell7.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 6, (short) 6000);

                    cntr = 1;
                    itr = wflList.iterator();
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        HSSFRow row = my_sheet.createRow(cntr);
                        row = my_sheet.createRow((short) cntr);
                        row.createCell((short) 0).setCellValue(cntr);
                        row.getCell((short) 0).setCellStyle(style1);
                        row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                        row.getCell((short) 1).setCellStyle(style1);
//                        row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                        row.getCell((short) 2).setCellStyle(style1);
                        row.createCell((short) 2).setCellValue(repTO.getTonnage());
                        row.getCell((short) 2).setCellStyle(style1);
                        row.createCell((short) 3).setCellValue(repTO.getDriverName());
                        row.getCell((short) 3).setCellStyle(style1);
                        row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                        row.getCell((short) 4).setCellStyle(style1);
                        row.createCell((short) 5).setCellValue(repTO.getPeriod());
                        row.getCell((short) 5).setCellStyle(style1);
                        row.createCell((short) 6).setCellValue(repTO.getAge());
                        row.getCell((short) 6).setCellStyle(style1);
                        cntr++;
                    }

                } else if (k == 3) {
                    sheetName = "Idle - No Driver";

                    HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                    HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                    HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                    style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                    style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                    HSSFRow header = my_sheet.createRow(0);
                    header.setHeightInPoints(20); // row hight

                    HSSFCell cell1 = header.createCell((short) 0);
                    style.setWrapText(true);
                    cell1.setCellValue("SNo");
                    cell1.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 0, (short) 4000);

                    HSSFCell cell2 = header.createCell((short) 1);
                    cell2.setCellValue("TruckNo");
                    cell2.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 1, (short) 6000);

//                    HSSFCell cell3 = header.createCell((short) 2);
//                    cell3.setCellValue("Branch");
//                    cell3.setCellStyle(style);
//                    my_sheet.setColumnWidth((short) 2, (short) 6000);

                    HSSFCell cell31 = header.createCell((short) 2);
                    cell31.setCellValue("VehicleType");
                    cell31.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 2, (short) 6000);
                    
                    HSSFCell cell4 = header.createCell((short) 3);
                    cell4.setCellValue("DriverName");
                    cell4.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 3, (short) 6000);
                    
                    HSSFCell cell5 = header.createCell((short) 4);
                    cell5.setCellValue("DriverMobile");
                    cell5.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 4, (short) 6000);
                    
                    HSSFCell cell6 = header.createCell((short) 5);
                    cell6.setCellValue("Period");
                    cell6.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 5, (short) 6000);

                    HSSFCell cell7 = header.createCell((short) 6);
                    cell7.setCellValue("No of Days in Idle");
                    cell7.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 6, (short) 6000);

                    cntr = 1;
                    itr = wfuList.iterator();
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        HSSFRow row = my_sheet.createRow(cntr);
                        row = my_sheet.createRow((short) cntr);
                        row.createCell((short) 0).setCellValue(cntr);
                        row.getCell((short) 0).setCellStyle(style1);
                        row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                        row.getCell((short) 1).setCellStyle(style1);
//                        row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                        row.getCell((short) 2).setCellStyle(style1);
                        row.createCell((short) 2).setCellValue(repTO.getTonnage());
                        row.getCell((short) 2).setCellStyle(style1);
                        row.createCell((short) 3).setCellValue(repTO.getDriverName());
                        row.getCell((short) 3).setCellStyle(style1);
                        row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                        row.getCell((short) 4).setCellStyle(style1);
                        row.createCell((short) 5).setCellValue(repTO.getPeriod());
                        row.getCell((short) 5).setCellStyle(style1);
                        row.createCell((short) 4).setCellValue(repTO.getAge());
                        row.getCell((short) 4).setCellStyle(style1);
                        cntr++;
                    }

                } else if (k == 4) {
                    sheetName = "Trip-InProgress";
                    HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                    HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                    HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                    style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                    style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                    HSSFRow header = my_sheet.createRow(0);
                    header.setHeightInPoints(20); // row hight

                    HSSFCell cell1 = header.createCell((short) 0);
                    style.setWrapText(true);
                    cell1.setCellValue("SNo");
                    cell1.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 0, (short) 4000);

                    HSSFCell cell2 = header.createCell((short) 1);
                    cell2.setCellValue("TruckNo");
                    cell2.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 1, (short) 6000);

                    HSSFCell cell3 = header.createCell((short) 2);
                    cell3.setCellValue("TripNo");
                    cell3.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 2, (short) 6000);

//                    HSSFCell cell4 = header.createCell((short) 3);
//                    cell4.setCellValue("Branch");
//                    cell4.setCellStyle(style);
//                    my_sheet.setColumnWidth((short) 3, (short) 6000);

                    HSSFCell cell4 = header.createCell((short) 3);
                    cell4.setCellValue("DriverName");
                    cell4.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 3, (short) 6000);
                    
                    HSSFCell cell5 = header.createCell((short) 4);
                    cell5.setCellValue("DriverMobile");
                    cell5.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 4, (short) 6000);
                  
                      
                    HSSFCell cell6 = header.createCell((short) 5);
                    cell6.setCellValue("Route Interim");
                    cell6.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 5, (short) 6000);

                    HSSFCell cell7 = header.createCell((short) 6);
                    cell7.setCellValue("CurrentLocation");
                    cell7.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 6, (short) 6000);

                    HSSFCell cell8 = header.createCell((short) 7);
                    cell8.setCellValue("Status");
                    cell8.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 7, (short) 6000);

                    HSSFCell cell9 = header.createCell((short) 8);
                    cell9.setCellValue("CustomerName");
                    cell9.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 8, (short) 6000);

                    HSSFCell cell10 = header.createCell((short) 9);
                    cell10.setCellValue("TripStartedOn");
                    cell10.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 8, (short) 8000);

                    HSSFCell cell11 = header.createCell((short) 10);
                    cell11.setCellValue("No of Days in Trip");
                    cell11.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 10, (short) 6000);
                    
                    HSSFCell cell12 = header.createCell((short) 10);
                    cell12.setCellValue("Duration time hrs");
                    cell12.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 10, (short) 6000);

                    cntr = 1;
                    itr = tripInProgressList.iterator();
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        HSSFRow row = my_sheet.createRow(cntr);
                        row = my_sheet.createRow((short) cntr);
                        row.createCell((short) 0).setCellValue(cntr);
                        row.getCell((short) 0).setCellStyle(style1);
                        row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                        row.getCell((short) 1).setCellStyle(style1);
                        row.createCell((short) 2).setCellValue(repTO.getTripCode());
                        row.getCell((short) 2).setCellStyle(style1);
//                        row.createCell((short) 3).setCellValue(repTO.getOperationPoint());
//                        row.getCell((short) 3).setCellStyle(style1);
                        row.createCell((short) 3).setCellValue(repTO.getDriverName());
                        row.getCell((short) 3).setCellStyle(style1);
                        row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                        row.getCell((short) 4).setCellStyle(style1);
                        
                        row.createCell((short) 5).setCellValue(repTO.getRouteInfo());
                        row.getCell((short) 5).setCellStyle(style1);
                        row.createCell((short) 6).setCellValue(repTO.getLocation());
                        row.getCell((short) 6).setCellStyle(style1);
                        row.createCell((short) 7).setCellValue(repTO.getStatusName());
                        row.getCell((short) 7).setCellStyle(style1);
                        row.createCell((short) 8).setCellValue(repTO.getCustomerName());
                        row.getCell((short) 8).setCellStyle(style1);
                        row.createCell((short) 9).setCellValue(repTO.getTripstarttime());
                        row.getCell((short) 9).setCellStyle(style1);
                        row.createCell((short) 10).setCellValue(repTO.getAge());
                        row.getCell((short) 10).setCellStyle(style1);
                        row.createCell((short) 11).setCellValue(repTO.getDurationHours());
                        row.getCell((short) 11).setCellStyle(style1);
                        cntr++;
                    }
                } else if (k == 5) {
                    sheetName = "Trip-InProgress Hire";
                    HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                    HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                    HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                    style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                    style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                    HSSFRow header = my_sheet.createRow(0);
                    header.setHeightInPoints(20); // row hight

                    HSSFCell cell1 = header.createCell((short) 0);
                    style.setWrapText(true);
                    cell1.setCellValue("SNo");
                    cell1.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 0, (short) 4000);

                    HSSFCell cell2 = header.createCell((short) 1);
                    cell2.setCellValue("TruckNo");
                    cell2.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 1, (short) 6000);

                    HSSFCell cell3 = header.createCell((short) 2);
                    cell3.setCellValue("TripNo");
                    cell3.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 2, (short) 6000);

//                    HSSFCell cell4 = header.createCell((short) 3);
//                    cell4.setCellValue("Branch");
//                    cell4.setCellStyle(style);
//                    my_sheet.setColumnWidth((short) 3, (short) 6000);

                    HSSFCell cell4 = header.createCell((short) 3);
                    cell4.setCellValue("DriverName");
                    cell4.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 3, (short) 6000);
                    
                    HSSFCell cell5 = header.createCell((short) 4);
                    cell5.setCellValue("DriverMobile");
                    cell5.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 4, (short) 6000);
                  
                      
                    HSSFCell cell6 = header.createCell((short) 5);
                    cell6.setCellValue("Route Interim");
                    cell6.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 5, (short) 6000);

                    HSSFCell cell7 = header.createCell((short) 6);
                    cell7.setCellValue("Transporter");
                    cell7.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 6, (short) 6000);

                    HSSFCell cell8 = header.createCell((short) 7);
                    cell8.setCellValue("Status");
                    cell8.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 7, (short) 6000);

                    HSSFCell cell9 = header.createCell((short) 8);
                    cell9.setCellValue("CustomerName");
                    cell9.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 8, (short) 6000);

                    HSSFCell cell10 = header.createCell((short) 9);
                    cell10.setCellValue("TripStartedOn");
                    cell10.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 8, (short) 8000);

                    HSSFCell cell11 = header.createCell((short) 10);
                    cell11.setCellValue("No of Days in Trip");
                    cell11.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 10, (short) 6000);

                    HSSFCell cell12 = header.createCell((short) 10);
                    cell12.setCellValue("Duration time hrs");
                    cell12.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 10, (short) 6000);
                    
                    cntr = 1;
                    itr = tripInProgressHireList.iterator();
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        HSSFRow row = my_sheet.createRow(cntr);
                        row = my_sheet.createRow((short) cntr);
                        row.createCell((short) 0).setCellValue(cntr);
                        row.getCell((short) 0).setCellStyle(style1);
                        row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                        row.getCell((short) 1).setCellStyle(style1);
                        row.createCell((short) 2).setCellValue(repTO.getTripCode());
                        row.getCell((short) 2).setCellStyle(style1);
//                        row.createCell((short) 3).setCellValue(repTO.getOperationPoint());
//                        row.getCell((short) 3).setCellStyle(style1);
                        row.createCell((short) 3).setCellValue(repTO.getDriverName());
                        row.getCell((short) 3).setCellStyle(style1);
                        row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                        row.getCell((short) 4).setCellStyle(style1);
                        
                        row.createCell((short) 5).setCellValue(repTO.getRouteInfo());
                        row.getCell((short) 5).setCellStyle(style1);
                        row.createCell((short) 6).setCellValue(repTO.getLocation());
                        row.getCell((short) 6).setCellStyle(style1);
                        row.createCell((short) 7).setCellValue(repTO.getStatusName());
                        row.getCell((short) 7).setCellStyle(style1);
                        row.createCell((short) 8).setCellValue(repTO.getCustomerName());
                        row.getCell((short) 8).setCellStyle(style1);
                        row.createCell((short) 9).setCellValue(repTO.getTripstarttime());
                        row.getCell((short) 9).setCellStyle(style1);
                        row.createCell((short) 10).setCellValue(repTO.getAge());
                        row.getCell((short) 10).setCellStyle(style1);
                        row.createCell((short) 11).setCellValue(repTO.getDurationHours());
                        row.getCell((short) 11).setCellStyle(style1);
                        cntr++;
                    }
                } else if (k == 6) {
                    sheetName = "Workshop";
                    HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                    HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                    HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                    style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                    style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                    style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                    style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                    style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                    HSSFRow header = my_sheet.createRow(0);
                    header.setHeightInPoints(20); // row hight

                    HSSFCell cell1 = header.createCell((short) 0);
                    style.setWrapText(true);
                    cell1.setCellValue("SNo");
                    cell1.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 0, (short) 4000);

                    HSSFCell cell2 = header.createCell((short) 1);
                    cell2.setCellValue("TruckNo");
                    cell2.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 1, (short) 6000);

//                    HSSFCell cell3 = header.createCell((short) 2);
//                    cell3.setCellValue("Branch");
//                    cell3.setCellStyle(style);
//                    my_sheet.setColumnWidth((short) 2, (short) 6000);

                    HSSFCell cell4 = header.createCell((short) 2);
                    cell4.setCellValue("Status");
                    cell4.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 2, (short) 6000);

                    HSSFCell cell5 = header.createCell((short) 3);
                    cell5.setCellValue("ServiceType");
                    cell5.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 3, (short) 6000);
                    
                    HSSFCell cell6 = header.createCell((short) 4);
                    cell6.setCellValue("Period");
                    cell6.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 4, (short) 6000);

                    HSSFCell cell7 = header.createCell((short) 5);
                    cell7.setCellValue("How Many Days in Workshop");
                    cell7.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 5, (short) 8000);
                    
                    HSSFCell cell8 = header.createCell((short) 6);
                    cell8.setCellValue("Expected Date of Delivery");
                    cell8.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 6, (short) 8000);
                    
                    HSSFCell cell9 = header.createCell((short) 7);
                    cell9.setCellValue("Remarks");
                    cell9.setCellStyle(style);
                    my_sheet.setColumnWidth((short) 7, (short) 6000);

                    cntr = 1;
                    itr = jobCardList.iterator();
                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        HSSFRow row = my_sheet.createRow(cntr);
                        row = my_sheet.createRow((short) cntr);
                        row.createCell((short) 0).setCellValue(cntr);
                        row.getCell((short) 0).setCellStyle(style1);
                        row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                        row.getCell((short) 1).setCellStyle(style1);
//                        row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                        row.getCell((short) 2).setCellStyle(style1);
                        row.createCell((short) 2).setCellValue(repTO.getStatusName());
                        row.getCell((short) 2).setCellStyle(style1);
                        row.createCell((short) 3).setCellValue(repTO.getServiceTypeName());
                        row.getCell((short) 3).setCellStyle(style1);
                        row.createCell((short) 4).setCellValue(repTO.getPeriod());
                        row.getCell((short) 4).setCellStyle(style1);
                        row.createCell((short) 5).setCellValue(repTO.getAge());
                        row.getCell((short) 5).setCellStyle(style1);
                        row.createCell((short) 6).setCellValue(repTO.getDeliveryDate());
                        row.getCell((short) 6).setCellStyle(style1);
                        row.createCell((short) 7).setCellValue(repTO.getRemarks());
                        row.getCell((short) 7).setCellStyle(style1);
                        cntr++;
                    }
                }
            }

            System.out.println("Your Mail excel Sheet  created");

            try {
                FileOutputStream fileOut = new FileOutputStream(filepath);
                my_workbook.write(fileOut);
                fileOut.close();
                System.out.println("Mail Excel written successfully..");

                System.out.println("mailContent=" + mailContent);
                mailTemplate = mailTemplate.replaceAll("vehicleStatusList", mailContent);
                emailFormat = mailTemplate + "~" + filename + "~" + filepath;

            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        }

        return emailFormat;
    }

    public ArrayList completedJobcards(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList completedJobcards = new ArrayList();
        completedJobcards = reportDAO.completedJobcards(reportTO);
        return completedJobcards;
    }

    public ArrayList activeJobcards(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList activeJobcards = new ArrayList();
        activeJobcards = reportDAO.activeJobcards(reportTO);
        return activeJobcards;
    }

    public ArrayList dktList() throws FPRuntimeException, FPBusinessException {
        ArrayList dktList = new ArrayList();
        dktList = reportDAO.dktList();
        return dktList;
    }

    public ArrayList conList() throws FPRuntimeException, FPBusinessException {
        ArrayList conList = new ArrayList();
        conList = reportDAO.conList();
        return conList;
    }

    public ArrayList txnList() throws FPRuntimeException, FPBusinessException {
        ArrayList txnList = new ArrayList();
        txnList = reportDAO.txnList();
        return txnList;
    }

    public int insertVehicleStatusDetailsList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        ArrayList vehicleDetailsList = new ArrayList();
        int vehicleNoDriverStatus = 0;
//        ArrayList vehicleNoDriverStatus = new ArrayList();
        ArrayList vehicleNoTripStatus = new ArrayList();
        ArrayList vehicleNoWorkShopStatus = new ArrayList();
        vehicleDetailsList = reportDAO.getAllVehicleList();
        Iterator itr1 = vehicleDetailsList.iterator();
        ReportTO repTO1 = new ReportTO();
        System.out.println("vehicleDetailsList:::" + vehicleDetailsList.size());
        while (itr1.hasNext()) {
            repTO1 = (ReportTO) itr1.next();
            reportTO.setVehicleId(repTO1.getVehicleId());

            Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, -1);
            dNow = cal.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String yesterday = ft.format(dNow);
            System.out.println("the yesterday date is " + yesterday);
            reportTO.setYesterday(yesterday);
            // check the vehicle No-Load status

            vehicleNoDriverStatus = reportDAO.getVehicleNoDriverStatus(reportTO);
            System.out.println("vehicleNoDriverStatus:::" + vehicleNoDriverStatus);

            vehicleNoTripStatus = reportDAO.getVehicleNoTripStatus(reportTO);
            System.out.println("vehicleNoTripStatus:::" + vehicleNoTripStatus.size());

            vehicleNoWorkShopStatus = reportDAO.getVehicleNoWorkshopStatus(reportTO);
            System.out.println("vehicleNoWorkShopStatus:::" + vehicleNoWorkShopStatus.size());

            if (vehicleNoTripStatus.size() > 0) {
                //Trip-In progress
                System.out.println("one...");
                reportTO.setStatusId("2");

                DprTO dprTO = new DprTO();
                Iterator itr2 = vehicleNoTripStatus.iterator();
                while (itr2.hasNext()) {
                    dprTO = (DprTO) itr2.next();
                    reportTO.setTripId(dprTO.getTripId());
                    reportTO.setTripStatus(dprTO.getTripStatus());
                    reportTO.setEstimatedRevenue(dprTO.getEstimatedRevenue());
                    reportTO.setJobCardId("0");
                    reportTO.setJobCardStatus("");
                    System.out.println("tripId::" + reportTO.getTripId());
                    System.out.println("tripStatus::" + reportTO.getTripStatus());
                    System.out.println("tripRevenue::" + reportTO.getEstimatedRevenue());
                }

            } else if (vehicleNoDriverStatus > 0) {
                //No Driver
                System.out.println("two...");
                reportTO.setStatusId("4");

                reportTO.setJobCardId("0");
                reportTO.setJobCardStatus("");
                reportTO.setTripId("0");
                reportTO.setTripStatus("0");
                reportTO.setEstimatedRevenue("0");

            } else if (vehicleNoWorkShopStatus.size() > 0) {
                //Workshop
                System.out.println("three...");
                reportTO.setStatusId("3");

                DprTO dprTO2 = new DprTO();
                Iterator itr3 = vehicleNoWorkShopStatus.iterator();
                while (itr3.hasNext()) {
                    dprTO2 = (DprTO) itr3.next();
                    reportTO.setJobCardId(dprTO2.getJobCardId());
                    reportTO.setJobCardStatus(dprTO2.getJobCardStatus());
                    reportTO.setTripId("0");
                    reportTO.setTripStatus("0");
                    reportTO.setEstimatedRevenue("0");
                    System.out.println("tripId::" + reportTO.getTripId());
                    System.out.println("tripStatus::" + reportTO.getTripStatus());
                    System.out.println("tripRevenue::" + reportTO.getEstimatedRevenue());
                }

            } else {
                //Idle No-Load / WFL
                System.out.println("four...");
                reportTO.setStatusId("1");
                reportTO.setJobCardId("0");
                reportTO.setJobCardStatus("");
                reportTO.setTripId("0");
                reportTO.setTripStatus("0");
                reportTO.setEstimatedRevenue("0");
            }
            // get Vehicle permit amounts
            String fcAmount = "";
            String insuranceAmount = "";
            String roadTaxAmount = "";
            String permitAmount = "";
            fcAmount = reportDAO.getVehicleFcAmount(reportTO);
            if ("".equals(fcAmount) || fcAmount == null) {
                reportTO.setFcAmount(Double.parseDouble("0"));
            } else {
                reportTO.setFcAmount(Double.parseDouble(fcAmount));
            }
            insuranceAmount = reportDAO.getVehicleInsuranceAmount(reportTO);
            if ("".equals(insuranceAmount) || insuranceAmount == null) {
                reportTO.setInsuranceAmount(Double.parseDouble("0"));
            } else {
                reportTO.setInsuranceAmount(Double.parseDouble(insuranceAmount));
            }
            roadTaxAmount = reportDAO.getVehicleRoadTaxAmount(reportTO);
            if ("".equals(roadTaxAmount) || roadTaxAmount == null) {
                reportTO.setRoadTaxAmount(Double.parseDouble("0"));
            } else {
                reportTO.setRoadTaxAmount(Double.parseDouble(roadTaxAmount));
            }
            permitAmount = reportDAO.getVehiclePermitAmount(reportTO);
            if ("".equals(permitAmount) || permitAmount == null) {
                reportTO.setPermitAmount(Double.parseDouble("0"));
            } else {
                reportTO.setPermitAmount(Double.parseDouble(permitAmount));
            }

            System.out.println("fcAmount::" + fcAmount);
            System.out.println("insuranceAmount::" + insuranceAmount);
            System.out.println("roadTaxAmount::" + roadTaxAmount);
            System.out.println("permitAmount::" + permitAmount);
            //end here...
            System.out.println("vehicleId::" + reportTO.getVehicleId());
            System.out.println("statusId::" + reportTO.getStatusId());
            status = reportDAO.insertDailyVehicleStatusDetails(reportTO);
            System.out.println("status:::" + status);
        }
        return status;
    }

    // fastTag mail inbox reading
   public int readFastTagMailInbox(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        try {
            String pop3Host = "imap.googlemail.com";
            String user = "calog.fastag@gmail.com";
            String password = "calog$123";

            System.out.println("inner.user---" + user);
            System.out.println("inner.password---" + password);
            Session session = Session.getDefaultInstance(new Properties());
            Store store = session.getStore("imaps");
            store.connect(pop3Host, 993, user, password);

            //create the folder object and open it
            Folder emailFolder = store.getFolder("INBOX");

            emailFolder.open(Folder.READ_WRITE);

            // retrieve the messages from the folder in an array and print it
            // Message[] messages = emailFolder.getMessages();
            // Fetch unseen messages from inbox folder
            Message[] messages = emailFolder.search(
                    new FlagTerm(new Flags(Flags.Flag.SEEN), false));

            System.out.println("messages.length---" + messages.length);

            for (int i = 0; i < messages.length; i++) {
                

                Message message = messages[i];

                System.out.println("---------------------------------");

//                System.out.println("Email Number " + (i + 1));
//                System.out.println("Subject: " + message.getSubject());
//                System.out.println("send Date: " + message.getSentDate());
//                System.out.println("From: " + message.getFrom()[0]);
//                System.out.println("Flag: " + message.getFlags());
//                System.out.println("Text: " + message.getContent().toString());
                String result = "";
                if (message.isMimeType("text/plain")) {
                    result = message.getContent().toString();
                } else if (message.isMimeType("multipart/*")) {
                    MimeMultipart mimeMultipart = (MimeMultipart) message.getContent();
                    result = getTextFromMimeMultipart(mimeMultipart);
                }
                System.out.println("result------" + result);

                //                  result = "Dear Customer, Greetings from ICICI Bank. Your ICICI Bank FASTag linked to vehicle no. TN69J8871 has been debited with 460.00 on date 13-02-2020 13:57:17 for Toll Charges at Pallikonda Toll Plaza toll plaza on 13-02-2020 at 13:53:21. Your available Balance is 7908.64 In case you have not done this transaction or for any dispute or any other query, you may contact our customer care @ 1800-210-0104 (Toll Free) / 1860-267-0104 (Chargeable). For more details on FASTag , please visit www.icicibank.com/fastag . You can recharge using UPI by sending following details- netc.ib10053103@icici for CUG recharges and netc.<vehicle number>@icici for tag level recharges. To get your ICICI Bank FASTag balance\n" +
                //"   \n" +
                //"on Mobile, SMS ETCBAL <Vehicle Regn No> to 5676766 or give a missed call on 8010928888 from your registered mobile number. NEVER SHARE your User ID or Password, even if the caller claims to be a bank employee. Sharing these details can lead to unauthorized access to your account. Looking forward to more opportunities to be of service to you. Sincerely, ICICI Bank Limited Terms and Conditions of ICICI Bank and third parties apply. ICICI Bank is not responsible for third party products, goods, services and offers. If you do not wish to receive further marketing e-mails, please register under ' Do Not Call ' registry on www.icicibank.com.";
                String vehicleNo = "";
                String amount = "";
                String date = "";
                String tolldate = "";
                String remarks = "";
                String balance = "";

                try {

                    String regex = "(?<=no. )(.*)(?= has)";
                    Pattern p = Pattern.compile(regex);
                    Matcher m = p.matcher(result);

                    while (m.find()) {
                        vehicleNo = m.group().toString();
                        System.out.println("vehicleNo==" + vehicleNo);
                    }

                    String regex1 = "(?<=debited with )(.*)(?= on date)";
                    Pattern p1 = Pattern.compile(regex1);
                    Matcher m1 = p1.matcher(result);
                    while (m1.find()) {
                        amount = m1.group().toString();
                        System.out.println("amount===" + amount);
                    }

//                    String regex2 = "(?<=on date )(.*)(?= for Toll)";
//                    Pattern p2 = Pattern.compile(regex2);
//                    Matcher m2 = p2.matcher(result);
//                    while (m2.find()) {
//                        date = m2.group().toString();
//                        System.out.println("date===" + date);
//                    }
                    
                    String regex6 = "(?<=toll plaza on )(.*)(?=. Your available Balance)";
                    Pattern p6 = Pattern.compile(regex6);
                    Matcher m6 = p6.matcher(result);
                    while (m6.find()) {
                        tolldate = m6.group().toString();
                        System.out.println("tolldate===" + tolldate);
                    }
                    tolldate.trim();
                    System.out.println("trim tolldate===" + tolldate);
                    String[] tolldates = tolldate.split("at");
                    String actdate = tolldates[0].trim();
                    System.out.println("act tolldate===" + actdate);
                    String acttime = tolldates[1].trim();
                    System.out.println("act tolltime===" + acttime);
                    
                    String[] temp1 = tolldates[0].split("-");
                    String dat1 = temp1[0] + "-" + temp1[1] + "-" + temp1[2];
                    System.out.println("dat1===" + dat1);
                    String[] temp2 = tolldates[1].split(":");
                    String time = temp2[0] + ":" + temp2[1] + ":" + temp2[2];
                    System.out.println("time===" + time);
                    
//                    
//                    String[] temp = date.split(" ");
//                    String dat = temp[0] + " " + temp[1];
//                    String[] temp1 = temp[0].split("-");
//                    String dat1 = temp1[0] + "-" + temp1[1] + "-" + temp1[2];
//                    System.out.println("dat1===" + dat1);
//                    String[] temp2 = temp[1].split(":");
//                    String time = temp2[0] + ":" + temp2[1] + ":" + temp2[2];
//                    System.out.println("time===" + time);

                    String regex3 = "(?<=Charges at )(.*)(?=. Your)";
                    Pattern p3 = Pattern.compile(regex3);
                    Matcher m3 = p3.matcher(result);
                    while (m3.find()) {
                        remarks = m3.group().toString();
                        System.out.println("remarks===" + remarks);
                    }

                    String regex4 = "(?<=Balance is )(.*)(?= In case)";
                    Pattern p4 = Pattern.compile(regex4);
                    Matcher m4 = p4.matcher(result);
                    while (m4.find()) {
                        balance = m4.group().toString();
                        System.out.println("balance===" + balance);
                    }
                    reportTO.setVehicleNo(vehicleNo);
                    reportTO.setTotalamount(amount);
                    reportTO.setDate(dat1);
                    reportTO.setLogTime(time);
                    reportTO.setRemarks(remarks);
                    reportTO.setBalanceamount(balance);
                    if (amount != null) {
                        if (!amount.equals("")) {
                            status = reportDAO.updateHappayDetails(reportTO);
                            if(status>0){
                            message.setFlag(Flags.Flag.SEEN, true);
                            }
                        }
                    }

                    System.out.println("status " + status);


                } catch (Exception e) {

                }

            }

            //close the store and folder objects
            emailFolder.close(false);

            store.close();

        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        } catch (MessagingException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return status;
    }

    private String getTextFromMimeMultipart(
            MimeMultipart mimeMultipart) throws MessagingException, IOException {
        String result = "";
        int count = mimeMultipart.getCount();
        for (int i = 0; i < count; i++) {
            BodyPart bodyPart = mimeMultipart.getBodyPart(i);
            if (bodyPart.isMimeType("text/plain")) {
                result = result + "\n" + bodyPart.getContent();
                break; // without break same text appears twice in my tests
            } else if (bodyPart.isMimeType("text/html")) {
                String html = (String) bodyPart.getContent();
                result = result + "\n" + Jsoup.parse(html).text();
            } else if (bodyPart.getContent() instanceof MimeMultipart) {
                result = result + getTextFromMimeMultipart((MimeMultipart) bodyPart.getContent());
            }
        }
        return result;
    }

    public ArrayList customerSalesReportList() throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleUTchart = new ArrayList();
        vehicleUTchart = reportDAO.customerSalesReportList();
        return vehicleUTchart;
    }

    public ArrayList getTripAutomationList() throws FPRuntimeException, FPBusinessException {
        ArrayList tripAutomationList = new ArrayList();
        tripAutomationList = reportDAO.getTripAutomationList();
        return tripAutomationList;
    }

    public int insertFastTagExpense(ReportTO reportTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = reportDAO.insertFastTagExpense(reportTo);
        return status;
    }

    public ArrayList getFactoryInOutList() throws FPRuntimeException, FPBusinessException {
        ArrayList factoryInOutList = new ArrayList();
        factoryInOutList = reportDAO.getFactoryInOutList();
        return factoryInOutList;
    }

    public int updateFactoryInOutList(ReportTO reportTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = reportDAO.updateFactoryInOutList(reportTo);
        return status;
    }

    public ArrayList orderStatusReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList orderStatusReportDetails = new ArrayList();
        orderStatusReportDetails = reportDAO.orderStatusReportDetails(reportTO);
        return orderStatusReportDetails;
    }

    public ArrayList getMovementListWithoutDocument(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList movementList = new ArrayList();
        movementList = reportDAO.getMovementListWithoutDocument(fromDate);
        return movementList;
    }

    public ArrayList getMovementListWithDocument(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList movementList = new ArrayList();
        movementList = reportDAO.getMovementListWithDocument(fromDate);
        return movementList;
    }

    public ArrayList getTripStartedDetils(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList tripStartedDetils = new ArrayList();
        tripStartedDetils = reportDAO.getTripStartedDetils(fromDate);
        return tripStartedDetils;
    }

    public ArrayList getTripStartReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripStartReportDetails = new ArrayList();
        tripStartReportDetails = reportDAO.getTripStartReportDetails(reportTO);
        return tripStartReportDetails;
    }

    public ArrayList getEmptyPlotInList(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList getEmptyPlotInList = new ArrayList();
        getEmptyPlotInList = reportDAO.getEmptyPlotInList(fromDate);
        return getEmptyPlotInList;
    }

    public ArrayList getEmptyPlotTimeDiff(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList getEmptyPlotTimeDiff = new ArrayList();
        getEmptyPlotTimeDiff = reportDAO.getEmptyPlotTimeDiff(fromDate);
        return getEmptyPlotTimeDiff;
    }

    public ArrayList getCFSPortInList(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList getCFSPortInList = new ArrayList();
        getCFSPortInList = reportDAO.getCFSPortInList(fromDate);
        return getCFSPortInList;
    }

    public ArrayList getCFSPortInTimeDiff(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList getEmptyPlotTimeDiff = new ArrayList();
        getEmptyPlotTimeDiff = reportDAO.getCFSPortInTimeDiff(fromDate);
        return getEmptyPlotTimeDiff;
    }

    public ArrayList getFactoryInList(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList getFactoryInList = new ArrayList();
        getFactoryInList = reportDAO.getFactoryInList(fromDate);
        return getFactoryInList;
    }

    public ArrayList getFactoryInTimeDiff(String fromDate) throws FPRuntimeException, FPBusinessException {
        ArrayList getFactoryInTimeDiff = new ArrayList();
        getFactoryInTimeDiff = reportDAO.getFactoryInTimeDiff(fromDate);
        return getFactoryInTimeDiff;
    }

    public ArrayList getMovementStatusReportDetails(ReportTO reportTO, String statusId) throws FPRuntimeException, FPBusinessException {
        ArrayList movementStatusReportDetails = new ArrayList();
        movementStatusReportDetails = reportDAO.getMovementStatusReportDetails(reportTO, statusId);
        return movementStatusReportDetails;
    }

//    public String getDailyBookingReportAlert(ReportTO reportTO1) throws FPRuntimeException, FPBusinessException {
//
//        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//        Calendar c = Calendar.getInstance();
//        Date date = new Date();
//        c.setTime(date);
//        String systemTime = sdf.format(c.getTime()).toString();
//        System.out.println("system date = " + systemTime);
//        String emailFormat = "";
//        String fromDate = "";
//        String endDate = "";
//        DecimalFormat df = new DecimalFormat("#.##");
//        String name = "DailyBookingReport_" + systemTime + ".xls";
//        String filename = name;
//        String filepath = ThrottleConstants.dailyBookingAlertsPath + name;
//        System.out.println("filename = " + name);
//
//        try {
//
//            File theDir = new File(ThrottleConstants.dailyBookingAlertsPath);
//            if (!theDir.exists()) {
//                boolean result = theDir.mkdir();
//                if (result) {
//                    System.out.println(ThrottleConstants.dailyBookingAlertsPath + " Created");
//                }
//            }
//
//            //String customerName = reportTO1.getCustomerName();
//            //String name = customerName + "_(" + startDate + ") TO (" + endDate + ")" + ".xls";
//            ArrayList dailyBookingCountList = new ArrayList();
//            dailyBookingCountList = reportDAO.dailyBookingCountList(reportTO1);
//            Iterator itr = dailyBookingCountList.iterator();
//
//            System.out.println("dailyBookingCountList : " + dailyBookingCountList.size());
//
//            HSSFWorkbook my_workbook = new HSSFWorkbook();
//
//            String sheetName = "Daily Booking Report";
//            String sheetName1 = "Started Trips";
//            String sheetName2 = "Trip in Progress";
//            String sheetName3 = "Ended Trips";
//            String sheetName4 = "Closure Trips";
//            String sheetName5 = "Moffusil - Own";
//            String sheetName6 = "Local - Own";
//            String sheetName7 = "Cancelled Trips";
//
//            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
//            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
//            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style.setBorderTop((short) 1);
//
//            HSSFRow header = my_sheet.createRow(0);
//            header.setHeightInPoints(20); // row hight
//
//            HSSFCell cell1 = header.createCell((short) 0);
//            style.setWrapText(true);
//            cell1.setCellValue("Bookings Status");
//            cell1.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 0, (short) 6000);
//
//            HSSFCell cell2 = header.createCell((short) 1);
//            cell2.setCellValue("20 ft");
//            cell2.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 1, (short) 4000);
//
//            HSSFCell cell3 = header.createCell((short) 2);
//            cell3.setCellValue("40 ft");
//            cell3.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 2, (short) 4000);
//
//            HSSFCell cell4 = header.createCell((short) 3);
//            cell4.setCellValue("Total");
//            cell4.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 3, (short) 4000);
//
//            ReportTO reportTO = new ReportTO();
//            int cntr = 1;
//            String countType = "";
//            boolean check = false;
//            while (itr.hasNext()) {
//                reportTO = (ReportTO) itr.next();
//                check = true;
//                HSSFRow row = my_sheet.createRow(cntr);
//                row = my_sheet.createRow((short) cntr);
////                row.createCell((short) 0).setCellValue(cntr);
//                if ("1".equals(reportTO.getCountType())) {
//                    countType = "Bookings Received";
//                } else if ("2".equals(reportTO.getCountType())) {
//                    countType = "Started Trips ";
//                } else if ("3".equals(reportTO.getCountType())) {
//                    countType = "Ended Trips ";
//                } else if ("4".equals(reportTO.getCountType())) {
//                    countType = "Closure Trips ";
//                } else if ("5".equals(reportTO.getCountType())) {
//                    countType = "Pending Bookings";
//                } else if ("6".equals(reportTO.getCountType())) {
//                    countType = "Cancelled Bookings";
//                }
//
//                row.createCell((short) 0).setCellValue(countType);
//                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
//                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
//                row.createCell((short) 3).setCellValue(reportTO.getTotCount());
//
//                cntr++;
//            }
//
////            table 2
//            HSSFCellStyle book = (HSSFCellStyle) my_workbook.createCellStyle();
//            book.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            book.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            book.setBorderTop((short) 1);
//
//            header = my_sheet.createRow(9);
//            header.setHeightInPoints(20); // row hight
//
//            HSSFCell cell11 = header.createCell((short) 0);
//            style.setWrapText(true);
//            cell11.setCellValue("Bookings Assigned");
//            cell11.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 0, (short) 6000);
//
//            HSSFCell cells2 = header.createCell((short) 1);
//            cells2.setCellValue("Own");
//            cells2.setCellStyle(book);
//            my_sheet.setColumnWidth((short) 1, (short) 4000);
//
//            HSSFCell cells3 = header.createCell((short) 2);
//            cells3.setCellValue("Hire");
//            cells3.setCellStyle(book);
//            my_sheet.setColumnWidth((short) 2, (short) 4000);
//
////            HSSFCell cells4 = header.createCell((short) 3);
////            cells4.setCellValue("Total");
////            cells4.setCellStyle(book);
////            my_sheet.setColumnWidth((short) 3, (short) 4000);
//
//            int cntrs = 10;
//            int totalOwn = 0;
//            int totalHire = 0;
////            String countType = "";
//            ReportTO reportTOs = null;
//            boolean checks = false;
//            ArrayList movementAllocationList = new ArrayList();
//            movementAllocationList = reportDAO.movementAllocationList(reportTO1);
//            Iterator itrs = movementAllocationList.iterator();
//            while (itrs.hasNext()) {
//                reportTOs = (ReportTO) itrs.next();
//                checks = true;
//                System.out.println("second table---" + cntrs);
//                HSSFRow rows = my_sheet.createRow(cntrs);
//                rows = my_sheet.createRow((short) cntrs);
////                row.createCell((short) 0).setCellValue(cntr);
//                if ("1".equals(reportTOs.getMovementType())) {
//                    countType = "Moffusil";
//                    totalOwn = totalOwn + reportTOs.getOwnCount();
//                } else if ("2".equals(reportTOs.getMovementType())) {
//                    countType = "Local";
//                    totalHire = totalHire + reportTOs.getHireCount();
//                } else if ("3".equals(reportTOs.getMovementType())) {
//                    countType = "Cancelled Trips";
//                }
//                int total = totalOwn + totalHire;
//                rows.createCell((short) 0).setCellValue(countType);
//                rows.createCell((short) 1).setCellValue(reportTOs.getOwnCount());
//                rows.createCell((short) 2).setCellValue(reportTOs.getHireCount());
////                rows.createCell((short) 3).setCellValue(total);
//
//                cntrs++;
//            }
////            HSSFRow rows = my_sheet.createRow(15);
////            rows = my_sheet.createRow((short) 10);
////            countType = "Total";
////            rows.createCell((short) 0).setCellValue(countType);
////            rows.createCell((short) 1).setCellValue(totalOwn);
////            rows.createCell((short) 2).setCellValue(totalHire);
//
//            //            table 3
//            HSSFCellStyle own = (HSSFCellStyle) my_workbook.createCellStyle();
//            own.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            own.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            own.setBorderTop((short) 1);
//
//            HSSFRow headerss = my_sheet.createRow(16);
//            headerss.setHeightInPoints(20); // row hight
//
//            HSSFCell cells11 = headerss.createCell((short) 0);
//            own.setWrapText(true);
//            cells11.setCellValue("Own Vehicle Status");
//            cells11.setCellStyle(own);
//            my_sheet.setColumnWidth((short) 0, (short) 6000);
//
//            HSSFCell cells12 = headerss.createCell((short) 1);
//            cells12.setCellValue("20 ft");
//            cells12.setCellStyle(own);
//            my_sheet.setColumnWidth((short) 1, (short) 4000);
//
//            HSSFCell cells13 = headerss.createCell((short) 2);
//            cells13.setCellValue("40 ft");
//            cells13.setCellStyle(own);
//            my_sheet.setColumnWidth((short) 2, (short) 4000);
//
//            HSSFCell cell14 = headerss.createCell((short) 3);
//            cell14.setCellValue("Total");
//            cell14.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 3, (short) 4000);
//
//            totalOwn = 0;
//            totalHire = 0;
//
//            ArrayList OwndailyBookingCountList = new ArrayList();
//            OwndailyBookingCountList = reportDAO.OwndailyBookingCountList(reportTO1);
//            Iterator itr1 = OwndailyBookingCountList.iterator();
//
//            reportTO = new ReportTO();
//            cntr = 17;
//            countType = "";
//            check = false;
//            while (itr1.hasNext()) {
//                reportTO = (ReportTO) itr1.next();
//                check = true;
//                HSSFRow row = my_sheet.createRow(cntr);
//                row = my_sheet.createRow((short) cntr);
//
//                if ("1".equals(reportTO.getCountType())) {
//                    countType = "Started Trips ";
//                } else if ("2".equals(reportTO.getCountType())) {
//                    countType = "Trips in Progress ";
//                } else if ("3".equals(reportTO.getCountType())) {
//                    countType = "Ended Trips ";
//                } else if ("4".equals(reportTO.getCountType())) {
//                    countType = "Closure Trips";
//                } else if ("5".equals(reportTO.getCountType())) {
//                    countType = "WorkShop";
//                } else if ("6".equals(reportTO.getCountType())) {
//                    countType = "Idle - No Driver";
//                }
//
//                row.createCell((short) 0).setCellValue(countType);
//                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
//                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
//                row.createCell((short) 3).setCellValue(reportTO.getTotCount());
//
//                cntr++;
//            }
//
//            //            table 4
//            HSSFCellStyle own1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            own1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            own1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            own1.setBorderTop((short) 1);
//
//            HSSFRow headerss1 = my_sheet.createRow(26);
//            headerss1.setHeightInPoints(26); // row hight
//
//            HSSFCell cells21 = headerss1.createCell((short) 0);
//            own1.setWrapText(true);
//            cells21.setCellValue("Own Vehicle Availability");
//            cells21.setCellStyle(own1);
//            my_sheet.setColumnWidth((short) 0, (short) 6000);
//
//            HSSFCell cells22 = headerss1.createCell((short) 1);
//            cells22.setCellValue("20 ft");
//            cells22.setCellStyle(own1);
//            my_sheet.setColumnWidth((short) 1, (short) 4000);
//
//            HSSFCell cells23 = headerss1.createCell((short) 2);
//            cells23.setCellValue("40 ft");
//            cells23.setCellStyle(own1);
//            my_sheet.setColumnWidth((short) 2, (short) 4000);
//
//            HSSFCell cell24 = headerss1.createCell((short) 3);
//            cell24.setCellValue("Total");
//            cell24.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 3, (short) 4000);
//
//            totalOwn = 0;
//            totalHire = 0;
//
//            ArrayList OwnVehicleAvail = new ArrayList();
//            OwnVehicleAvail = reportDAO.OwnVehicleAvail(reportTO1);
//            Iterator itr11 = OwnVehicleAvail.iterator();
//
//            reportTO = new ReportTO();
//            cntr = 27;
//            countType = "";
//            check = false;
//            while (itr11.hasNext()) {
//                reportTO = (ReportTO) itr11.next();
//                check = true;
//                HSSFRow row = my_sheet.createRow(cntr);
//                row = my_sheet.createRow((short) cntr);
//
//                if ("1".equals(reportTO.getCountType())) {
//                    countType = "WFL ";
//                } else if ("2".equals(reportTO.getCountType())) {
//                    countType = "Vehicles can plan for next trip ";
//                }
//
//                row.createCell((short) 0).setCellValue(countType);
//                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
//                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
//                row.createCell((short) 3).setCellValue(reportTO.getTotCount());
//
//                cntr++;
//            }
//
//            //            table 5
//            HSSFCellStyle hire = (HSSFCellStyle) my_workbook.createCellStyle();
//            hire.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            hire.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            hire.setBorderTop((short) 1);
//
//            HSSFRow headerH = my_sheet.createRow(30);
//            headerH.setHeightInPoints(20); // row hight
//
//            HSSFCell cells31 = headerH.createCell((short) 0);
//            own.setWrapText(true);
//            cells31.setCellValue("Hire Vehicle Status");
//            cells31.setCellStyle(hire);
//            my_sheet.setColumnWidth((short) 0, (short) 6000);
//
//            HSSFCell cells32 = headerH.createCell((short) 1);
//            cells32.setCellValue("20 ft");
//            cells32.setCellStyle(hire);
//            my_sheet.setColumnWidth((short) 1, (short) 4000);
//
//            HSSFCell cells33 = headerH.createCell((short) 2);
//            cells33.setCellValue("40 ft");
//            cells33.setCellStyle(hire);
//            my_sheet.setColumnWidth((short) 2, (short) 4000);
//
//            HSSFCell cell34 = headerH.createCell((short) 3);
//            cell34.setCellValue("Total");
//            cell34.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 3, (short) 4000);
//
//            totalOwn = 0;
//            totalHire = 0;
//
//            ArrayList HiredailyBookingCountList = new ArrayList();
//            HiredailyBookingCountList = reportDAO.HiredailyBookingCountList(reportTO1);
//            Iterator itr22 = HiredailyBookingCountList.iterator();
//
//            reportTO = new ReportTO();
//            cntr = 31;
//            countType = "";
//            check = false;
//            while (itr22.hasNext()) {
//                reportTO = (ReportTO) itr22.next();
//                check = true;
//                HSSFRow row = my_sheet.createRow(cntr);
//                row = my_sheet.createRow((short) cntr);
//
//                if ("1".equals(reportTO.getCountType())) {
//                    countType = "Started Trips ";
//                } else if ("2".equals(reportTO.getCountType())) {
//                    countType = "Trips in Progress ";
//                } else if ("3".equals(reportTO.getCountType())) {
//                    countType = "Ended Trips ";
//                } else if ("4".equals(reportTO.getCountType())) {
//                    countType = "Closure Trips";
//                }
//
//                row.createCell((short) 0).setCellValue(countType);
//                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
//                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
//                row.createCell((short) 3).setCellValue(reportTO.getTotCount());
//
//                cntr++;
//            }
//
////            sheet one  - start trips
//            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName1);
//            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style1.setBorderTop((short) 1);
//
//            HSSFRow header1 = my_sheet1.createRow(0);
//            header1.setHeightInPoints(20); // row hight
//
//            HSSFCell cellOne1 = header1.createCell((short) 0);
//            style1.setWrapText(true);
//            cellOne1.setCellValue("SNo");
//            cellOne1.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 0, (short) 2000);
//
//            HSSFCell cellOne2 = header1.createCell((short) 1);
//            style1.setWrapText(true);
//            cellOne2.setCellValue("CustRefNo");
//            cellOne2.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 1, (short) 6000);
//
//            HSSFCell cellOne3 = header1.createCell((short) 2);
//            cellOne3.setCellValue("Customer");
//            cellOne3.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 2, (short) 4000);
//
//            HSSFCell cellOne4 = header1.createCell((short) 3);
//            cellOne4.setCellValue("Goods");
//            cellOne4.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 3, (short) 5000);
//
//            HSSFCell cellOne5 = header1.createCell((short) 4);
//            cellOne5.setCellValue("Vehicle No");
//            cellOne5.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 4, (short) 4000);
//
//            HSSFCell cellOne6 = header1.createCell((short) 5);
//            cellOne6.setCellValue("TripCode");
//            cellOne6.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 5, (short) 7000);
//
//            HSSFCell cellOne7 = header1.createCell((short) 6);
//            cellOne7.setCellValue("Container No");
//            cellOne7.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 6, (short) 4000);
//
//            HSSFCell cellOne8 = header1.createCell((short) 7);
//            cellOne8.setCellValue("LR Nos");
//            cellOne8.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 7, (short) 4000);
//
//            HSSFCell cellOne9 = header1.createCell((short) 8);
//            cellOne9.setCellValue("Pkg/Wgt/Cbm");
//            cellOne9.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 8, (short) 4000);
//
//            HSSFCell cellOne10 = header1.createCell((short) 9);
//            cellOne10.setCellValue("Trip Date");
//            cellOne10.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 9, (short) 4000);
//
//            HSSFCell cellOne11 = header1.createCell((short) 10);
//            cellOne11.setCellValue("Order Type");
//            cellOne11.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 10, (short) 4000);
//
//            HSSFCell cellOne12 = header1.createCell((short) 11);
//            cellOne12.setCellValue("Transporter");
//            cellOne12.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 11, (short) 4000);
//
//            HSSFCell cellOne13 = header1.createCell((short) 12);
//            cellOne13.setCellValue("Route");
//            cellOne13.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 12, (short) 4000);
//
//            HSSFCell cellOne14 = header1.createCell((short) 13);
//            cellOne14.setCellValue("Driver");
//            cellOne14.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 13, (short) 4000);
//
//            HSSFCell cellOne15 = header1.createCell((short) 14);
//            cellOne15.setCellValue("Vehicle Type");
//            cellOne15.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 14, (short) 4000);
//
//            HSSFCell cellOne16 = header1.createCell((short) 15);
//            cellOne16.setCellValue("Status");
//            cellOne16.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 15, (short) 4000);
//
//            ReportTO reportToOne = null;
//            int cntrOne = 1;
//            int countOne = 1;
//            boolean checkOne = false;
//            ArrayList tripStartContainerDetails = new ArrayList();
//            reportTO1.setTripType("1");
//            tripStartContainerDetails = reportDAO.tripStartContainerDetails(reportTO1);
//            Iterator itrOne = tripStartContainerDetails.iterator();
//            System.out.println("tripStartContainerDetails............." + tripStartContainerDetails.size());
//            while (itrOne.hasNext()) {
//                reportToOne = (ReportTO) itrOne.next();
//                checkOne = true;
//                HSSFRow row1 = my_sheet1.createRow(cntrOne);
//                row1 = my_sheet1.createRow((short) cntrOne);
//
//
//
//                    row1.createCell((short) 0).setCellValue(cntrOne);
//                    row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
//                    row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
//                    row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
//                    row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
//                    row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
//                    row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
//                    row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
//                    row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
//                    row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
//                    row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
//                    row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
//                    row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
//                    row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
//                    row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
//                    row1.createCell((short) 15).setCellValue(reportToOne.getStatus());
//
//                    cntrOne++;
//            }
//
//            //            sheet one  - trip in progress
//            my_sheet1 = my_workbook.createSheet(sheetName2);
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style1.setBorderTop((short) 1);
//
//            header1 = my_sheet1.createRow(0);
//            header1.setHeightInPoints(20); // row hight
//
//            cellOne1 = header1.createCell((short) 0);
//            style1.setWrapText(true);
//            cellOne1.setCellValue("SNo");
//            cellOne1.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 0, (short) 2000);
//
//            cellOne2 = header1.createCell((short) 1);
//            style1.setWrapText(true);
//            cellOne2.setCellValue("CustRefNo");
//            cellOne2.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 1, (short) 6000);
//
//            cellOne3 = header1.createCell((short) 2);
//            cellOne3.setCellValue("Customer");
//            cellOne3.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 2, (short) 4000);
//
//            cellOne4 = header1.createCell((short) 3);
//            cellOne4.setCellValue("Goods");
//            cellOne4.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 3, (short) 5000);
//
//            cellOne5 = header1.createCell((short) 4);
//            cellOne5.setCellValue("Vehicle No");
//            cellOne5.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 4, (short) 4000);
//
//            cellOne6 = header1.createCell((short) 5);
//            cellOne6.setCellValue("TripCode");
//            cellOne6.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 5, (short) 7000);
//
//            cellOne7 = header1.createCell((short) 6);
//            cellOne7.setCellValue("Container No");
//            cellOne7.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 6, (short) 4000);
//
//            cellOne8 = header1.createCell((short) 7);
//            cellOne8.setCellValue("LR Nos");
//            cellOne8.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 7, (short) 4000);
//
//            cellOne9 = header1.createCell((short) 8);
//            cellOne9.setCellValue("Pkg/Wgt/Cbm");
//            cellOne9.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 8, (short) 4000);
//
//            cellOne10 = header1.createCell((short) 9);
//            cellOne10.setCellValue("Trip Date");
//            cellOne10.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 9, (short) 4000);
//
//            cellOne11 = header1.createCell((short) 10);
//            cellOne11.setCellValue("Order Type");
//            cellOne11.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 10, (short) 4000);
//
//            cellOne12 = header1.createCell((short) 11);
//            cellOne12.setCellValue("Transporter");
//            cellOne12.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 11, (short) 4000);
//
//            cellOne13 = header1.createCell((short) 12);
//            cellOne13.setCellValue("Route");
//            cellOne13.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 12, (short) 4000);
//
//            cellOne14 = header1.createCell((short) 13);
//            cellOne14.setCellValue("Driver");
//            cellOne14.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 13, (short) 4000);
//
//            cellOne15 = header1.createCell((short) 14);
//            cellOne15.setCellValue("Vehicle Type");
//            cellOne15.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 14, (short) 4000);
//
//            cellOne16 = header1.createCell((short) 15);
//            cellOne16.setCellValue("Status");
//            cellOne16.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 15, (short) 4000);
//
//            reportToOne = null;
//            cntrOne = 1;
//            countOne = 1;
//            checkOne = false;
//            ArrayList tripInProgressDetails = new ArrayList();
//            reportTO1.setTripType("2");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
//            itrOne = tripInProgressDetails.iterator();
//            System.out.println("tripInProgressDetails............." + tripInProgressDetails.size());
//            while (itrOne.hasNext()) {
//                reportToOne = (ReportTO) itrOne.next();
//                checkOne = true;
//                HSSFRow row1 = my_sheet1.createRow(cntrOne);
//                row1 = my_sheet1.createRow((short) cntrOne);
//
//                    row1.createCell((short) 0).setCellValue(cntrOne);
//                    row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
//                    row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
//                    row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
//                    row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
//                    row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
//                    row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
//                    row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
//                    row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
//                    row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
//                    row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
//                    row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
//                    row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
//                    row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
//                    row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
//                    row1.createCell((short) 15).setCellValue(reportToOne.getStatus());
//
//                    cntrOne++;
//            }
//
//            //            sheet one  - trip end
//            my_sheet1 = my_workbook.createSheet(sheetName3);
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style1.setBorderTop((short) 1);
//
//            header1 = my_sheet1.createRow(0);
//            header1.setHeightInPoints(20); // row hight
//
//            cellOne1 = header1.createCell((short) 0);
//            style1.setWrapText(true);
//            cellOne1.setCellValue("SNo");
//            cellOne1.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 0, (short) 2000);
//
//            cellOne2 = header1.createCell((short) 1);
//            style1.setWrapText(true);
//            cellOne2.setCellValue("CustRefNo");
//            cellOne2.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 1, (short) 6000);
//
//            cellOne3 = header1.createCell((short) 2);
//            cellOne3.setCellValue("Customer");
//            cellOne3.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 2, (short) 4000);
//
//            cellOne4 = header1.createCell((short) 3);
//            cellOne4.setCellValue("Goods");
//            cellOne4.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 3, (short) 5000);
//
//            cellOne5 = header1.createCell((short) 4);
//            cellOne5.setCellValue("Vehicle No");
//            cellOne5.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 4, (short) 4000);
//
//            cellOne6 = header1.createCell((short) 5);
//            cellOne6.setCellValue("TripCode");
//            cellOne6.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 5, (short) 7000);
//
//            cellOne7 = header1.createCell((short) 6);
//            cellOne7.setCellValue("Container No");
//            cellOne7.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 6, (short) 4000);
//
//            cellOne8 = header1.createCell((short) 7);
//            cellOne8.setCellValue("LR Nos");
//            cellOne8.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 7, (short) 4000);
//
//            cellOne9 = header1.createCell((short) 8);
//            cellOne9.setCellValue("Pkg/Wgt/Cbm");
//            cellOne9.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 8, (short) 4000);
//
//            cellOne10 = header1.createCell((short) 9);
//            cellOne10.setCellValue("Trip Date");
//            cellOne10.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 9, (short) 4000);
//
//            cellOne11 = header1.createCell((short) 10);
//            cellOne11.setCellValue("Order Type");
//            cellOne11.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 10, (short) 4000);
//
//            cellOne12 = header1.createCell((short) 11);
//            cellOne12.setCellValue("Transporter");
//            cellOne12.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 11, (short) 4000);
//
//            cellOne13 = header1.createCell((short) 12);
//            cellOne13.setCellValue("Route");
//            cellOne13.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 12, (short) 4000);
//
//            cellOne14 = header1.createCell((short) 13);
//            cellOne14.setCellValue("Driver");
//            cellOne14.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 13, (short) 4000);
//
//            cellOne15 = header1.createCell((short) 14);
//            cellOne15.setCellValue("Vehicle Type");
//            cellOne15.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 14, (short) 4000);
//
//            cellOne16 = header1.createCell((short) 15);
//            cellOne16.setCellValue("Status");
//            cellOne16.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 15, (short) 4000);
//
//            reportToOne = null;
//            cntrOne = 1;
//            countOne = 1;
//            checkOne = false;
//
//            reportTO1.setTripType("3");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
//            itrOne = tripInProgressDetails.iterator();
//            System.out.println("tripEndDetails............." + tripInProgressDetails.size());
//            while (itrOne.hasNext()) {
//                reportToOne = (ReportTO) itrOne.next();
//                checkOne = true;
//                HSSFRow row1 = my_sheet1.createRow(cntrOne);
//                row1 = my_sheet1.createRow((short) cntrOne);
//
//                    row1.createCell((short) 0).setCellValue(cntrOne);
//                    row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
//                    row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
//                    row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
//                    row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
//                    row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
//                    row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
//                    row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
//                    row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
//                    row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
//                    row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
//                    row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
//                    row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
//                    row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
//                    row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
//                    row1.createCell((short) 15).setCellValue(reportToOne.getStatus());
//
//                    cntrOne++;
//
//            }
//
//            //            sheet one  - trip closure
//            my_sheet1 = my_workbook.createSheet(sheetName4);
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style1.setBorderTop((short) 1);
//
//            header1 = my_sheet1.createRow(0);
//            header1.setHeightInPoints(20); // row hight
//
//            cellOne1 = header1.createCell((short) 0);
//            style1.setWrapText(true);
//            cellOne1.setCellValue("SNo");
//            cellOne1.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 0, (short) 2000);
//
//            cellOne2 = header1.createCell((short) 1);
//            style1.setWrapText(true);
//            cellOne2.setCellValue("CustRefNo");
//            cellOne2.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 1, (short) 6000);
//
//            cellOne3 = header1.createCell((short) 2);
//            cellOne3.setCellValue("Customer");
//            cellOne3.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 2, (short) 4000);
//
//            cellOne4 = header1.createCell((short) 3);
//            cellOne4.setCellValue("Goods");
//            cellOne4.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 3, (short) 5000);
//
//            cellOne5 = header1.createCell((short) 4);
//            cellOne5.setCellValue("Vehicle No");
//            cellOne5.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 4, (short) 4000);
//
//            cellOne6 = header1.createCell((short) 5);
//            cellOne6.setCellValue("TripCode");
//            cellOne6.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 5, (short) 7000);
//
//            cellOne7 = header1.createCell((short) 6);
//            cellOne7.setCellValue("Container No");
//            cellOne7.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 6, (short) 4000);
//
//            cellOne8 = header1.createCell((short) 7);
//            cellOne8.setCellValue("LR Nos");
//            cellOne8.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 7, (short) 4000);
//
//            cellOne9 = header1.createCell((short) 8);
//            cellOne9.setCellValue("Pkg/Wgt/Cbm");
//            cellOne9.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 8, (short) 4000);
//
//            cellOne10 = header1.createCell((short) 9);
//            cellOne10.setCellValue("Trip Date");
//            cellOne10.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 9, (short) 4000);
//
//            cellOne11 = header1.createCell((short) 10);
//            cellOne11.setCellValue("Order Type");
//            cellOne11.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 10, (short) 4000);
//
//            cellOne12 = header1.createCell((short) 11);
//            cellOne12.setCellValue("Transporter");
//            cellOne12.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 11, (short) 4000);
//
//            cellOne13 = header1.createCell((short) 12);
//            cellOne13.setCellValue("Route");
//            cellOne13.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 12, (short) 4000);
//
//            cellOne14 = header1.createCell((short) 13);
//            cellOne14.setCellValue("Driver");
//            cellOne14.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 13, (short) 4000);
//
//            cellOne15 = header1.createCell((short) 14);
//            cellOne15.setCellValue("Vehicle Type");
//            cellOne15.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 14, (short) 4000);
//
//            cellOne16 = header1.createCell((short) 15);
//            cellOne16.setCellValue("Status");
//            cellOne16.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 15, (short) 4000);
//
//            reportToOne = null;
//            cntrOne = 1;
//            countOne = 1;
//            checkOne = false;
//
//            reportTO1.setTripType("4");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
//            itrOne = tripInProgressDetails.iterator();
//            System.out.println("closuredetails............." + tripInProgressDetails.size());
//            
//            while (itrOne.hasNext()) {
//                reportToOne = (ReportTO) itrOne.next();
//                checkOne = true;
//                HSSFRow row1 = my_sheet1.createRow(cntrOne);
//                row1 = my_sheet1.createRow((short) cntrOne);
//
//                    row1.createCell((short) 0).setCellValue(cntrOne);
//                    row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
//                    row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
//                    row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
//                    row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
//                    row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
//                    row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
//                    row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
//                    row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
//                    row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
//                    row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
//                    row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
//                    row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
//                    row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
//                    row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
//                    row1.createCell((short) 15).setCellValue(reportToOne.getStatus());
//
//                    cntrOne++;
//
//            }
//
//            //            sheet one  - trip Moffusil own
//            my_sheet1 = my_workbook.createSheet(sheetName5);
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style1.setBorderTop((short) 1);
//
//            header1 = my_sheet1.createRow(0);
//            header1.setHeightInPoints(20); // row hight
//
//            cellOne1 = header1.createCell((short) 0);
//            style1.setWrapText(true);
//            cellOne1.setCellValue("SNo");
//            cellOne1.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 0, (short) 2000);
//
//            cellOne2 = header1.createCell((short) 1);
//            style1.setWrapText(true);
//            cellOne2.setCellValue("CustRefNo");
//            cellOne2.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 1, (short) 6000);
//
//            cellOne3 = header1.createCell((short) 2);
//            cellOne3.setCellValue("Customer");
//            cellOne3.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 2, (short) 4000);
//
//            cellOne4 = header1.createCell((short) 3);
//            cellOne4.setCellValue("Goods");
//            cellOne4.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 3, (short) 5000);
//
//            cellOne5 = header1.createCell((short) 4);
//            cellOne5.setCellValue("Vehicle No");
//            cellOne5.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 4, (short) 4000);
//
//            cellOne6 = header1.createCell((short) 5);
//            cellOne6.setCellValue("TripCode");
//            cellOne6.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 5, (short) 7000);
//
//            cellOne7 = header1.createCell((short) 6);
//            cellOne7.setCellValue("Container No");
//            cellOne7.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 6, (short) 4000);
//
//            cellOne8 = header1.createCell((short) 7);
//            cellOne8.setCellValue("LR Nos");
//            cellOne8.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 7, (short) 4000);
//
//            cellOne9 = header1.createCell((short) 8);
//            cellOne9.setCellValue("Pkg/Wgt/Cbm");
//            cellOne9.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 8, (short) 4000);
//
//            cellOne10 = header1.createCell((short) 9);
//            cellOne10.setCellValue("Trip Date");
//            cellOne10.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 9, (short) 4000);
//
//            cellOne11 = header1.createCell((short) 10);
//            cellOne11.setCellValue("Order Type");
//            cellOne11.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 10, (short) 4000);
//
//            cellOne12 = header1.createCell((short) 11);
//            cellOne12.setCellValue("Transporter");
//            cellOne12.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 11, (short) 4000);
//
//            cellOne13 = header1.createCell((short) 12);
//            cellOne13.setCellValue("Route");
//            cellOne13.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 12, (short) 4000);
//
//            cellOne14 = header1.createCell((short) 13);
//            cellOne14.setCellValue("Driver");
//            cellOne14.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 13, (short) 4000);
//
//            cellOne15 = header1.createCell((short) 14);
//            cellOne15.setCellValue("Vehicle Type");
//            cellOne15.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 14, (short) 4000);
//
//            cellOne16 = header1.createCell((short) 15);
//            cellOne16.setCellValue("Status");
//            cellOne16.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 15, (short) 4000);
//
//            reportToOne = null;
//            cntrOne = 1;
//            countOne = 1;
//            checkOne = false;
//
//            reportTO1.setTripType("5");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
//            itrOne = tripInProgressDetails.iterator();
//            System.out.println("moffusil own............." + tripInProgressDetails.size());
//            
//            while (itrOne.hasNext()) {
//                reportToOne = (ReportTO) itrOne.next();
//                checkOne = true;
//                HSSFRow row1 = my_sheet1.createRow(cntrOne);
//                row1 = my_sheet1.createRow((short) cntrOne);
//
//                    row1.createCell((short) 0).setCellValue(cntrOne);
//                    row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
//                    row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
//                    row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
//                    row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
//                    row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
//                    row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
//                    row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
//                    row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
//                    row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
//                    row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
//                    row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
//                    row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
//                    row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
//                    row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
//                    row1.createCell((short) 15).setCellValue(reportToOne.getStatus());
//
//                    cntrOne++;
//            }
//
//            //   sheet one  - trip Local own
//            my_sheet1 = my_workbook.createSheet(sheetName6);
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style1.setBorderTop((short) 1);
//
//            header1 = my_sheet1.createRow(0);
//            header1.setHeightInPoints(20); // row hight
//
//            cellOne1 = header1.createCell((short) 0);
//            style1.setWrapText(true);
//            cellOne1.setCellValue("SNo");
//            cellOne1.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 0, (short) 2000);
//
//            cellOne2 = header1.createCell((short) 1);
//            style1.setWrapText(true);
//            cellOne2.setCellValue("CustRefNo");
//            cellOne2.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 1, (short) 6000);
//
//            cellOne3 = header1.createCell((short) 2);
//            cellOne3.setCellValue("Customer");
//            cellOne3.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 2, (short) 4000);
//
//            cellOne4 = header1.createCell((short) 3);
//            cellOne4.setCellValue("Goods");
//            cellOne4.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 3, (short) 5000);
//
//            cellOne5 = header1.createCell((short) 4);
//            cellOne5.setCellValue("Vehicle No");
//            cellOne5.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 4, (short) 4000);
//
//            cellOne6 = header1.createCell((short) 5);
//            cellOne6.setCellValue("TripCode");
//            cellOne6.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 5, (short) 7000);
//
//            cellOne7 = header1.createCell((short) 6);
//            cellOne7.setCellValue("Container No");
//            cellOne7.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 6, (short) 4000);
//
//            cellOne8 = header1.createCell((short) 7);
//            cellOne8.setCellValue("LR Nos");
//            cellOne8.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 7, (short) 4000);
//
//            cellOne9 = header1.createCell((short) 8);
//            cellOne9.setCellValue("Pkg/Wgt/Cbm");
//            cellOne9.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 8, (short) 4000);
//
//            cellOne10 = header1.createCell((short) 9);
//            cellOne10.setCellValue("Trip Date");
//            cellOne10.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 9, (short) 4000);
//
//            cellOne11 = header1.createCell((short) 10);
//            cellOne11.setCellValue("Order Type");
//            cellOne11.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 10, (short) 4000);
//
//            cellOne12 = header1.createCell((short) 11);
//            cellOne12.setCellValue("Transporter");
//            cellOne12.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 11, (short) 4000);
//
//            cellOne13 = header1.createCell((short) 12);
//            cellOne13.setCellValue("Route");
//            cellOne13.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 12, (short) 4000);
//
//            cellOne14 = header1.createCell((short) 13);
//            cellOne14.setCellValue("Driver");
//            cellOne14.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 13, (short) 4000);
//
//            cellOne15 = header1.createCell((short) 14);
//            cellOne15.setCellValue("Vehicle Type");
//            cellOne15.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 14, (short) 4000);
//
//            cellOne16 = header1.createCell((short) 15);
//            cellOne16.setCellValue("Status");
//            cellOne16.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 15, (short) 4000);
//
//            reportToOne = null;
//            cntrOne = 1;
//            countOne = 1;
//            checkOne = false;
//
//            reportTO1.setTripType("6");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
//            itrOne = tripInProgressDetails.iterator();
//            System.out.println("local own............." + tripInProgressDetails.size());
//            while (itrOne.hasNext()) {
//                reportToOne = (ReportTO) itrOne.next();
//                checkOne = true;
//                HSSFRow row1 = my_sheet1.createRow(cntrOne);
//                row1 = my_sheet1.createRow((short) cntrOne);
//
//                    row1.createCell((short) 0).setCellValue(cntrOne);
//                    row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
//                    row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
//                    row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
//                    row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
//                    row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
//                    row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
//                    row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
//                    row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
//                    row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
//                    row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
//                    row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
//                    row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
//                    row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
//                    row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
//                    row1.createCell((short) 15).setCellValue(reportToOne.getStatus());
//
//                    cntrOne++;
//
//            }
//
//            //            sheet one  - trip cancell
//            my_sheet1 = my_workbook.createSheet(sheetName7);
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style1.setBorderTop((short) 1);
//
//            header1 = my_sheet1.createRow(0);
//            header1.setHeightInPoints(20); // row hight
//
//            cellOne1 = header1.createCell((short) 0);
//            style1.setWrapText(true);
//            cellOne1.setCellValue("SNo");
//            cellOne1.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 0, (short) 2000);
//
//            cellOne2 = header1.createCell((short) 1);
//            style1.setWrapText(true);
//            cellOne2.setCellValue("CustRefNo");
//            cellOne2.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 1, (short) 6000);
//
//            cellOne3 = header1.createCell((short) 2);
//            cellOne3.setCellValue("Customer");
//            cellOne3.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 2, (short) 4000);
//
//            cellOne4 = header1.createCell((short) 3);
//            cellOne4.setCellValue("Goods");
//            cellOne4.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 3, (short) 5000);
//
//            cellOne5 = header1.createCell((short) 4);
//            cellOne5.setCellValue("Vehicle No");
//            cellOne5.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 4, (short) 4000);
//
//            cellOne6 = header1.createCell((short) 5);
//            cellOne6.setCellValue("TripCode");
//            cellOne6.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 5, (short) 7000);
//
//            cellOne7 = header1.createCell((short) 6);
//            cellOne7.setCellValue("Container No");
//            cellOne7.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 6, (short) 4000);
//
//            cellOne8 = header1.createCell((short) 7);
//            cellOne8.setCellValue("LR Nos");
//            cellOne8.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 7, (short) 4000);
//
//            cellOne9 = header1.createCell((short) 8);
//            cellOne9.setCellValue("Pkg/Wgt/Cbm");
//            cellOne9.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 8, (short) 4000);
//
//            cellOne10 = header1.createCell((short) 9);
//            cellOne10.setCellValue("Trip Date");
//            cellOne10.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 9, (short) 4000);
//
//            cellOne11 = header1.createCell((short) 10);
//            cellOne11.setCellValue("Order Type");
//            cellOne11.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 10, (short) 4000);
//
//            cellOne12 = header1.createCell((short) 11);
//            cellOne12.setCellValue("Transporter");
//            cellOne12.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 11, (short) 4000);
//
//            cellOne13 = header1.createCell((short) 12);
//            cellOne13.setCellValue("Route");
//            cellOne13.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 12, (short) 4000);
//
//            cellOne14 = header1.createCell((short) 13);
//            cellOne14.setCellValue("Driver");
//            cellOne14.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 13, (short) 4000);
//
//            cellOne15 = header1.createCell((short) 14);
//            cellOne15.setCellValue("Vehicle Type");
//            cellOne15.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 14, (short) 4000);
//
//            cellOne16 = header1.createCell((short) 15);
//            cellOne16.setCellValue("Status");
//            cellOne16.setCellStyle(style1);
//            my_sheet1.setColumnWidth((short) 15, (short) 4000);
//
//            reportToOne = null;
//            cntrOne = 1;
//            countOne = 1;
//            checkOne = false;
//
//            reportTO1.setTripType("7");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
//            itrOne = tripInProgressDetails.iterator();
//            System.out.println("trip cancell............." + tripInProgressDetails.size());
//            while (itrOne.hasNext()) {
//                reportToOne = (ReportTO) itrOne.next();
//                checkOne = true;
//                HSSFRow row1 = my_sheet1.createRow(cntrOne);
//                row1 = my_sheet1.createRow((short) cntrOne);
//
//                    row1.createCell((short) 0).setCellValue(cntrOne);
//                    row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
//                    row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
//                    row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
//                    row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
//                    row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
//                    row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
//                    row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
//                    row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
//                    row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
//                    row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
//                    row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
//                    row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
//                    row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
//                    row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
//                    row1.createCell((short) 15).setCellValue(reportToOne.getStatus());
//
//                    cntrOne++;
//
//            }
//
//            ArrayList jobCardList = new ArrayList();
//            ReportTO reportTO11 = new ReportTO();
//            ReportTO repTO = new ReportTO();
//
//            reportTO11.setCompanyId("1461");
//            jobCardList = getJobCardList(reportTO11);
//
//            sheetName = "Workshop";
//            my_sheet = my_workbook.createSheet(sheetName);
//            style = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
//            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
//            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//            //                            style.setBorderTop((short) 1);
//            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//
//            header = my_sheet.createRow(0);
//            header.setHeightInPoints(20); // row hight
//
//            cell1 = header.createCell((short) 0);
//            style.setWrapText(true);
//            cell1.setCellValue("SNo");
//            cell1.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 0, (short) 4000);
//
//            cell2 = header.createCell((short) 1);
//            cell2.setCellValue("TruckNo");
//            cell2.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 1, (short) 6000);
//
//            cell3 = header.createCell((short) 2);
//            cell3.setCellValue("Branch");
//            cell3.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 2, (short) 6000);
//
//            cell4 = header.createCell((short) 3);
//            cell4.setCellValue("Status");
//            cell4.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 3, (short) 6000);
//
//            HSSFCell cell5 = header.createCell((short) 4);
//            cell5.setCellValue("ServiceType");
//            cell5.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 4, (short) 6000);
//
//            HSSFCell cell6 = header.createCell((short) 5);
//            cell6.setCellValue("How Many Days in Workshop");
//            cell6.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 5, (short) 6000);
//
//            cntr = 1;
//            itr = jobCardList.iterator();
//            while (itr.hasNext()) {
//                repTO = (ReportTO) itr.next();
//                HSSFRow row = my_sheet.createRow(cntr);
//                row = my_sheet.createRow((short) cntr);
//                row.createCell((short) 0).setCellValue(cntr);
//                row.getCell((short) 0).setCellStyle(style1);
//                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
//                row.getCell((short) 1).setCellStyle(style1);
//                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                row.getCell((short) 2).setCellStyle(style1);
//                row.createCell((short) 3).setCellValue(repTO.getStatusName());
//                row.getCell((short) 3).setCellStyle(style1);
//                row.createCell((short) 4).setCellValue(repTO.getServiceTypeName());
//                row.getCell((short) 4).setCellStyle(style1);
//                row.createCell((short) 5).setCellValue(repTO.getAge());
//                row.getCell((short) 5).setCellStyle(style1);
//                cntr++;
//            }
//
//            sheetName = "Idle - No Driver";
//
//            my_sheet = my_workbook.createSheet(sheetName);
//            style = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
//            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
//            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//
//            header = my_sheet.createRow(0);
//            header.setHeightInPoints(20); // row hight
//
//            cell1 = header.createCell((short) 0);
//            style.setWrapText(true);
//            cell1.setCellValue("SNo");
//            cell1.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 0, (short) 4000);
//
//            cell2 = header.createCell((short) 1);
//            cell2.setCellValue("TruckNo");
//            cell2.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 1, (short) 6000);
//
//            cell3 = header.createCell((short) 2);
//            cell3.setCellValue("Branch");
//            cell3.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 2, (short) 6000);
//
//            HSSFCell cell31 = header.createCell((short) 3);
//            cell31.setCellValue("VehicleType");
//            cell31.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 3, (short) 6000);
//
//            cell4 = header.createCell((short) 4);
//            cell4.setCellValue("How Many Days in Idle?");
//            cell4.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 4, (short) 6000);
//
//            ArrayList wfuList = new ArrayList();
//            wfuList = getWfuList(reportTO);
//            cntr = 1;
//            itr = wfuList.iterator();
//            while (itr.hasNext()) {
//                repTO = (ReportTO) itr.next();
//                HSSFRow row = my_sheet.createRow(cntr);
//                row = my_sheet.createRow((short) cntr);
//                row.createCell((short) 0).setCellValue(cntr);
//                row.getCell((short) 0).setCellStyle(style1);
//                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
//                row.getCell((short) 1).setCellStyle(style1);
//                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                row.getCell((short) 2).setCellStyle(style1);
//                row.createCell((short) 3).setCellValue(repTO.getTonnage());
//                row.getCell((short) 3).setCellStyle(style1);
//                row.createCell((short) 4).setCellValue(repTO.getAge());
//                row.getCell((short) 4).setCellStyle(style1);
//                cntr++;
//            }
//
//            sheetName = "WFL";
//
//            my_sheet = my_workbook.createSheet(sheetName);
//            style = (HSSFCellStyle) my_workbook.createCellStyle();
//            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
//            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
//            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
//            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
//            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//            //                            style.setBorderTop((short) 1);
//            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
//            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//
//            header = my_sheet.createRow(0);
//            header.setHeightInPoints(20); // row hight
//
//            cell1 = header.createCell((short) 0);
//            style.setWrapText(true);
//            cell1.setCellValue("SNo");
//            cell1.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 0, (short) 4000);
//
//            cell2 = header.createCell((short) 1);
//            cell2.setCellValue("TruckNo");
//            cell2.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 1, (short) 6000);
//
//            cell3 = header.createCell((short) 2);
//            cell3.setCellValue("Branch");
//            cell3.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 2, (short) 6000);
//
//            cell31 = header.createCell((short) 3);
//            cell31.setCellValue("VehicleType");
//            cell31.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 3, (short) 6000);
//
//            cell4 = header.createCell((short) 4);
//            cell4.setCellValue("How Many Days in Idle?");
//            cell4.setCellStyle(style);
//            my_sheet.setColumnWidth((short) 4, (short) 6000);
//
//            ArrayList tripNotStartedList = new ArrayList();
//            tripNotStartedList = reportDAO.getTripNotStartedList(reportTO);
//            System.out.println("tripNotStartedList::" + tripNotStartedList.size());
//            cntr = 1;
//            itr = tripNotStartedList.iterator();
//            while (itr.hasNext()) {
//                repTO = (ReportTO) itr.next();
//                HSSFRow row = my_sheet.createRow(cntr);
//                row = my_sheet.createRow((short) cntr);
//                row.createCell((short) 0).setCellValue(cntr);
//                row.getCell((short) 0).setCellStyle(style1);
//                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
//                row.getCell((short) 1).setCellStyle(style1);
//                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                row.getCell((short) 2).setCellStyle(style1);
//                row.createCell((short) 3).setCellValue(repTO.getTonnage());
//                row.getCell((short) 3).setCellStyle(style1);
//                row.createCell((short) 4).setCellValue(repTO.getAge());
//                row.getCell((short) 4).setCellStyle(style1);
//                cntr++;
//            }
//
//            System.out.println("Your Mail excel Sheet created");
//            System.out.println("Your Mail excel Sheet filepath : " + filepath);
//
//            try {
//                FileOutputStream fileOut = new FileOutputStream(filepath);
//                my_workbook.write(fileOut);
//                fileOut.close();
//                System.out.println("Mail Excel written successfully..");
//
//                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
//                Calendar c1 = Calendar.getInstance();
//
//                Calendar c2 = Calendar.getInstance();
//                c2.add(Calendar.DATE, -1);
//
//                String curDate = formatter.format(c1.getTime());
//                String yestDate = formatter.format(c2.getTime());
//                System.out.println("curDate--------" + curDate);
//                System.out.println("yestDate--------" + yestDate);
//
//                String emailFormat1 = "";
//                String emailFormat2 = "";
//                String rowContent = "";
//                itr = dailyBookingCountList.iterator();
//                while (itr.hasNext()) {
//                    reportTO = (ReportTO) itr.next();
//                    rowContent = rowContent + "<tr>";
//                    if ("1".equals(reportTO.getCountType())) {
//                        countType = "Bookings Received";
//                    } else if ("2".equals(reportTO.getCountType())) {
//                        countType = "Started Trips ";
//                    } else if ("3".equals(reportTO.getCountType())) {
//                        countType = "Ended Trips ";
//                    } else if ("4".equals(reportTO.getCountType())) {
//                        countType = "Closure Trips ";
//                    } else if ("5".equals(reportTO.getCountType())) {
//                        countType = "Pending Bookings";
//                    } else if ("6".equals(reportTO.getCountType())) {
//                        countType = "Cancelled Bookings";
//                    }
//
//                    rowContent = rowContent + "<td>" + countType + "</td>";
//                    rowContent = rowContent + "<td>" + reportTO.getTwentyFtCount() + "</td>";
//                    rowContent = rowContent + "<td>" + reportTO.getFourtyFtCount() + "</td>";
//                    rowContent = rowContent + "<td>" + reportTO.getTotCount() + "</td>";
//                    rowContent = rowContent + "</tr>";
//                    System.out.println("rowContent===" + rowContent);
//
//                }
//
//                itrs = movementAllocationList.iterator();
//                String rowContent1 = "";
//
//                while (itrs.hasNext()) {
//                    reportTOs = (ReportTO) itrs.next();
//                    rowContent1 = rowContent1 + "<tr>";
//                    if ("1".equals(reportTOs.getMovementType())) {
//                        countType = "Moffusil";
//                    } else if ("2".equals(reportTOs.getMovementType())) {
//                        countType = "Local";
//                    } else if ("3".equals(reportTOs.getMovementType())) {
//                        countType = "Cancelled Trips";
//                    }
//
//                    totalOwn = totalOwn + reportTOs.getOwnCount();
//                    totalHire = totalHire + reportTOs.getHireCount();
//                    rowContent1 = rowContent1 + "<td>" + countType + "</td>";
//                    rowContent1 = rowContent1 + "<td>" + reportTOs.getOwnCount() + "</td>";
//                    rowContent1 = rowContent1 + "<td>" + reportTOs.getHireCount() + "</td>";
//                    rowContent1 = rowContent1 + "</tr>";
//                    System.out.println("rowContent1===" + rowContent1);
//                }
//
//                rowContent1 = rowContent1 + "<tr><td>Total</td>";
//                rowContent1 = rowContent1 + "<td>" + totalOwn + "</td>";
//                rowContent1 = rowContent1 + "<td>" + totalHire + "</td>";
//                rowContent1 = rowContent1 + "</tr>";
//
//                /////////////// own vehicle status ////////////////////
//                itr1 = OwndailyBookingCountList.iterator();
//                String rowContent2 = "";
//                reportTO = null;
//                while (itr1.hasNext()) {
//                    reportTO = (ReportTO) itr1.next();
//                    rowContent2 = rowContent2 + "<tr>";
//                    if ("1".equals(reportTO.getCountType())) {
//                        countType = "Started Trips ";
//                    } else if ("2".equals(reportTO.getCountType())) {
//                        countType = "Trips in Progress ";
//                    } else if ("3".equals(reportTO.getCountType())) {
//                        countType = "Ended Trips ";
//                    } else if ("4".equals(reportTO.getCountType())) {
//                        countType = "Closure Trips";
//                    } else if ("5".equals(reportTO.getCountType())) {
//                        countType = "WorkShop";
//                    } else if ("6".equals(reportTO.getCountType())) {
//                        countType = "Idle - No Driver";
//                    }
//
//                    rowContent2 = rowContent2 + "<td>" + countType + "</td>";
//                    rowContent2 = rowContent2 + "<td>" + reportTO.getTwentyFtCount() + "</td>";
//                    rowContent2 = rowContent2 + "<td>" + reportTO.getFourtyFtCount() + "</td>";
//                    rowContent2 = rowContent2 + "<td>" + reportTO.getTotCount() + "</td>";
//                    rowContent2 = rowContent2 + "</tr>";
//                    System.out.println("rowContent2===" + rowContent2);
//                }
//
//                /////////////// Own vehicle Availability  ////////////////////
//                itr11 = OwnVehicleAvail.iterator();
//                String rowContent3 = "";
//                reportTO = new ReportTO();
//
//                while (itr11.hasNext()) {
//                    reportTO = (ReportTO) itr11.next();
//                    rowContent3 = rowContent3 + "<tr>";
//                    if ("1".equals(reportTO.getCountType())) {
//                        countType = "WFL ";
//                    } else if ("2".equals(reportTO.getCountType())) {
//                        countType = "Vehicles can plan for next trip ";
//                    }
//
//                    rowContent3 = rowContent3 + "<td>" + countType + "</td>";
//                    rowContent3 = rowContent3 + "<td>" + reportTO.getTwentyFtCount() + "</td>";
//                    rowContent3 = rowContent3 + "<td>" + reportTO.getFourtyFtCount() + "</td>";
//                    rowContent3 = rowContent3 + "<td>" + reportTO.getTotCount() + "</td>";
//                    rowContent3 = rowContent3 + "</tr>";
//                    System.out.println("rowContent3===" + rowContent3);
//
//                }
//
//                /////////////// Hire vehicle Availability  ////////////////////
//                itr22 = HiredailyBookingCountList.iterator();
//                String rowContent4 = "";
//                reportTO = new ReportTO();
//
//                while (itr22.hasNext()) {
//                    reportTO = (ReportTO) itr22.next();
//                    rowContent4 = rowContent4 + "<tr>";
//                    if ("1".equals(reportTO.getCountType())) {
//                        countType = "Started Trips ";
//                    } else if ("2".equals(reportTO.getCountType())) {
//                        countType = "Trips in Progress ";
//                    } else if ("3".equals(reportTO.getCountType())) {
//                        countType = "Ended Trips ";
//                    } else if ("4".equals(reportTO.getCountType())) {
//                        countType = "Closure Trips";
//                    }
//
//                    rowContent4 = rowContent4 + "<td>" + countType + "</td>";
//                    rowContent4 = rowContent4 + "<td>" + reportTO.getTwentyFtCount() + "</td>";
//                    rowContent4 = rowContent4 + "<td>" + reportTO.getFourtyFtCount() + "</td>";
//                    rowContent4 = rowContent4 + "<td>" + reportTO.getTotCount() + "</td>";
//                    rowContent4 = rowContent4 + "</tr>";
//                    System.out.println("rowContent4===" + rowContent4);
//
//                }
//
//                System.out.println("rowContent==" + rowContent);
//                System.out.println("rowContent1==" + rowContent1);
//                System.out.println("rowContent2==" + rowContent2);
//                System.out.println("rowContent3==" + rowContent3);
//                System.out.println("rowContent4==" + rowContent4);
//
//                emailFormat1 = "<html>"
//                        + "<body>"
//                        + "<p><b>Dear Team, <br><br><b>GOOD MORNING"
//                        + "<br><br>Please find the Bookings which is received on " + yestDate + " (6.00 A.M.) to " + curDate + "(6.00 A.M.) and find the abstract in the attachement, <br><br/>";
//                emailFormat2 = "</body></html><br><br><br>"
//                        + "<html>"
//                        + "<body>"
//                        
//                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1' >"
//                        + "<tr bgcolor='lightblue'><th>Bookings Status</th><th>20 ft</th>"
//                        + "<th>40 ft</th><th>total</th></tr>"
//                        + rowContent
//                        + "</table><br><br>"
//                        
//                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1'>"
//                        + "<tr bgcolor='lightblue'><th>Bookings Assigned</th>"
//                        + "<th>Own</th><th>Hire</th></tr>"
//                        + rowContent1
//                        + "</table><br><br>"
//                        
//                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1'>"
//                        + "<tr bgcolor='lightblue'><th>Own Vehicle Status</th>"
//                        + "<th>20 ft</th><th>40 ft</th><th>Total</th></tr>"
//                        + rowContent2
//                        + "</table><br><br>"
//                        
//                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1'>"
//                        + "<tr bgcolor='lightblue'><th>Own Vehicle Availability</th>"
//                        + "<th>20 ft</th><th>40 ft</th><th>Total</th></tr>"
//                        + rowContent3
//                        + "</table><br><br>"
//                        
//                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1'>"
//                        + "<tr bgcolor='lightblue'><th>Hire Vehicle Status</th>"
//                        + "<th>20 ft</th><th>40 ft</th><th>Total</th></tr>"
//                        + rowContent4
//                        + "</table><br><br>"
//                        
//                        + "<table>"
//                        + "<tr>"
//                        + "<br><br><br>Regards,"
//                        + "<br><br/>CA Log<br/><br/>This is an Auto Generated Email Please Do Not Reply"
//                        + " .</p>"
//                        + "</tr>"
//                        + "</table>"
//                        + "</body></html>";
//                emailFormat = emailFormat1 + "" + emailFormat2 + "";
//                emailFormat = emailFormat + "~" + filename + "~" + filepath;
//                System.out.println("filepath " + filepath);
//                System.out.println("emailFormat : 1  " + emailFormat);
//                if (!check) {
//                    emailFormat = "";
//                }
//                System.out.println("emailFormat : 2  " + emailFormat);
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//        } catch (Exception e) {
//            System.out.println("Error in FileWriter !!!");
//            e.printStackTrace();
//        }
//
//        //  System.out.println("emailFormat : 3  "+emailFormat);
//        return emailFormat;
//    }

    public String dailyBookingReportAlert2(ReportTO reportTO1) throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("system date = " + systemTime);
        String emailFormat = "";
        String fromDate = "";
        String endDate = "";
        DecimalFormat df = new DecimalFormat("#.##");
        String name = "Customer_Wise_Trip_List_" + systemTime + ".xls";
        String filename = name;
        String filepath = ThrottleConstants.dailyBookingAlertsPath + name;
        System.out.println("filename = " + name);

        try {

            File theDir = new File(ThrottleConstants.dailyBookingAlertsPath);
            if (!theDir.exists()) {
                boolean result = theDir.mkdir();
                if (result) {
                    System.out.println(ThrottleConstants.dailyBookingAlertsPath + " Created");
                }
            }

            //String customerName = reportTO1.getCustomerName();
            //String name = customerName + "_(" + startDate + ") TO (" + endDate + ")" + ".xls";
            ArrayList dailyBookingCountList = new ArrayList();
            dailyBookingCountList = reportDAO.dailyBookingCountList(reportTO1);
            Iterator itr = dailyBookingCountList.iterator();

            System.out.println("dailyBookingCountList : " + dailyBookingCountList.size());

            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "Customer Wise Trip List";
            String sheetName1 = "Trips In Progress Customer Wise";
            String sheetName2 = "Customer Wise Completed Trip";

            //            Customer Wise Trip List
            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderTop((short) 1);

            HSSFCellStyle stylet = (HSSFCellStyle) my_workbook.createCellStyle();
            stylet.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            stylet.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            stylet.setBorderTop((short) 1);

            HSSFRow headert = my_sheet.createRow(0);
            headert.setHeightInPoints(20); // row hight

            HSSFCell cellt1 = headert.createCell((short) 0);
            stylet.setWrapText(true);
            cellt1.setCellValue("SNo");
            cellt1.setCellStyle(stylet);
            my_sheet.setColumnWidth((short) 0, (short) 6000);

            HSSFCell cellt2 = headert.createCell((short) 1);
            cellt2.setCellValue("Customer Name");
            cellt2.setCellStyle(stylet);
            my_sheet.setColumnWidth((short) 1, (short) 5000);

            HSSFCell cellt3 = headert.createCell((short) 2);
            cellt3.setCellValue("Mov-Type");
            cellt3.setCellStyle(stylet);
            my_sheet.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cellt4 = headert.createCell((short) 3);
            cellt4.setCellValue("20 ft");
            cellt4.setCellStyle(stylet);
            my_sheet.setColumnWidth((short) 3, (short) 4000);

            HSSFCell cellt5 = headert.createCell((short) 4);
            cellt5.setCellValue("40 ft");
            cellt5.setCellStyle(stylet);
            my_sheet.setColumnWidth((short) 4, (short) 4000);

            HSSFCell cellt6 = headert.createCell((short) 5);
            cellt6.setCellValue("Total ft");
            cellt6.setCellStyle(stylet);
            my_sheet.setColumnWidth((short) 5, (short) 4000);

            ReportTO reportTOt = null;
            int cntrt = 15;
            int countT = 1;
            int totalFt = 0;
            boolean checkt = false;
            ArrayList customerAndTripDetails = new ArrayList();
            customerAndTripDetails = reportDAO.customerAndTripDetails(reportTO1);
            Iterator itrt = customerAndTripDetails.iterator();
            while (itrt.hasNext()) {
                reportTOt = (ReportTO) itrt.next();
                checkt = true;
                HSSFRow rowt = my_sheet.createRow(cntrt);
                rowt = my_sheet.createRow((short) cntrt);
                if (reportTOt.getTwentyFtCount() != null) {
                    totalFt = Integer.parseInt(reportTOt.getTwentyFtCount()) + Integer.parseInt(reportTOt.getFourtyFtCount());
                    rowt.createCell((short) 0).setCellValue(countT);
                    rowt.createCell((short) 1).setCellValue(reportTOt.getCustName());
                    rowt.createCell((short) 2).setCellValue(reportTOt.getMovementType());
                    rowt.createCell((short) 3).setCellValue(reportTOt.getTwentyFtCount());
                    rowt.createCell((short) 4).setCellValue(reportTOt.getFourtyFtCount());
                    rowt.createCell((short) 5).setCellValue(totalFt);

                    cntrt++;
                    countT++;
                }
            }

            //            Trips In Progress Customer Wise
            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName1);
            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            HSSFRow header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            HSSFCell cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            HSSFCell cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("BOOKING DATE");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            HSSFCell cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("CUST REF NO");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("CUSTOMER");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            HSSFCell cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("MOVE TYPE");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);

            HSSFCell cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("ROUTE INTERIM");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            HSSFCell cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("20 ft");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            HSSFCell cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("40 ft");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            HSSFCell cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Total ft");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);

            HSSFCell cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("CONTAINER NO");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            ReportTO reportToOne = null;
            int cntrOne = 1;
            int countOne = 1;
            boolean checkOne = false;
            ArrayList tripStartContainerDetails = new ArrayList();
            tripStartContainerDetails = reportDAO.tripStartContainerDetails(reportTO1);
            Iterator itrOne = tripStartContainerDetails.iterator();
            System.out.println("tripStartContainerDetails............." + tripStartContainerDetails.size());
            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);
                if (reportToOne.getTwentyFtCount() != null) {
                    totalFt = Integer.parseInt(reportToOne.getTwentyFtCount()) + Integer.parseInt(reportToOne.getFourtyFtCount());
                    row1.createCell((short) 0).setCellValue(cntrOne);
                    row1.createCell((short) 1).setCellValue(reportToOne.getConsignmentDate());
                    row1.createCell((short) 2).setCellValue(reportToOne.getOrderReferenceNo());
                    row1.createCell((short) 3).setCellValue(reportToOne.getCustName());
                    row1.createCell((short) 4).setCellValue(reportToOne.getMovementType());
                    row1.createCell((short) 5).setCellValue(reportToOne.getRouteInfo());
                    row1.createCell((short) 6).setCellValue(reportToOne.getTwentyFtCount());
                    row1.createCell((short) 7).setCellValue(reportToOne.getFourtyFtCount());
                    row1.createCell((short) 8).setCellValue(totalFt);
                    row1.createCell((short) 9).setCellValue(reportToOne.getContainerNo());

                    cntrOne++;
                }
            }

            //            Customer Wise Completed Trip
            HSSFSheet my_sheet2 = my_workbook.createSheet(sheetName2);
            HSSFCellStyle style2 = (HSSFCellStyle) my_workbook.createCellStyle();
            style2.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style2.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style2.setBorderTop((short) 1);

            HSSFRow header2 = my_sheet2.createRow(0);
            header2.setHeightInPoints(20); // row hight

            HSSFCell cellTwo1 = header2.createCell((short) 0);
            style2.setWrapText(true);
            cellTwo1.setCellValue("SNo");
            cellTwo1.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 0, (short) 2000);

            HSSFCell cellTwo2 = header2.createCell((short) 1);
            style2.setWrapText(true);
            cellTwo2.setCellValue("BOOKING DATE");
            cellTwo2.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 1, (short) 6000);

            HSSFCell cellTwo3 = header2.createCell((short) 2);
            cellTwo3.setCellValue("CUST REF NO");
            cellTwo3.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cellTwo4 = header2.createCell((short) 3);
            cellTwo4.setCellValue("CUSTOMER");
            cellTwo4.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 3, (short) 5000);

            HSSFCell cellTwo5 = header2.createCell((short) 4);
            cellTwo5.setCellValue("MOVE TYPE");
            cellTwo5.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 4, (short) 4000);

            HSSFCell cellTwo6 = header2.createCell((short) 5);
            cellTwo6.setCellValue("ROUTE INTERIM");
            cellTwo6.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 5, (short) 7000);

            HSSFCell cellTwo7 = header2.createCell((short) 6);
            cellTwo7.setCellValue("20 ft");
            cellTwo7.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 6, (short) 4000);

            HSSFCell cellTwo8 = header2.createCell((short) 7);
            cellTwo8.setCellValue("40 ft");
            cellTwo8.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 7, (short) 4000);

            HSSFCell cellTwo9 = header2.createCell((short) 8);
            cellTwo9.setCellValue("Total ft");
            cellTwo9.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 8, (short) 4000);

            HSSFCell cellTwo10 = header2.createCell((short) 9);
            cellTwo10.setCellValue("CONTAINER NO");
            cellTwo10.setCellStyle(style2);
            my_sheet2.setColumnWidth((short) 9, (short) 4000);

            ReportTO reportToTwo = null;
            int cntrTwo = 1;
            int countTwo = 1;
            boolean checkTwo = false;
            ArrayList tripEndDetails = new ArrayList();
            tripEndDetails = reportDAO.tripEndDetails(reportTO1);
            Iterator itrTwo = tripEndDetails.iterator();
            System.out.println("tripPendingContainerDetails............." + tripEndDetails.size());
            while (itrTwo.hasNext()) {
                reportToTwo = (ReportTO) itrTwo.next();
                checkOne = true;
                HSSFRow row2 = my_sheet2.createRow(cntrTwo);
                row2 = my_sheet2.createRow((short) cntrTwo);
                //                row.createCell((short) 0).setCellValue(cntr);
                if (reportToTwo.getTwentyFtCount() != null) {
                    totalFt = Integer.parseInt(reportToTwo.getTwentyFtCount()) + Integer.parseInt(reportToTwo.getFourtyFtCount());
                    row2.createCell((short) 0).setCellValue(cntrTwo);
                    row2.createCell((short) 1).setCellValue(reportToTwo.getConsignmentDate());
                    row2.createCell((short) 2).setCellValue(reportToTwo.getOrderReferenceNo());
                    row2.createCell((short) 3).setCellValue(reportToTwo.getCustName());
                    row2.createCell((short) 4).setCellValue(reportToTwo.getMovementType());
                    row2.createCell((short) 5).setCellValue(reportToTwo.getRouteInfo());
                    row2.createCell((short) 6).setCellValue(reportToTwo.getTwentyFtCount());
                    row2.createCell((short) 7).setCellValue(reportToTwo.getFourtyFtCount());
                    row2.createCell((short) 8).setCellValue(totalFt);
                    row2.createCell((short) 9).setCellValue(reportToTwo.getContainerNo());

                    cntrTwo++;
                }
            }

            System.out.println("Your Mail excel Sheet created");
            System.out.println("Your Mail excel Sheet filepath : " + filepath);

            try {
                FileOutputStream fileOut = new FileOutputStream(filepath);
                my_workbook.write(fileOut);
                fileOut.close();
                System.out.println("Mail Excel written successfully..");

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Calendar c1 = Calendar.getInstance();

                Calendar c2 = Calendar.getInstance();
                c2.add(Calendar.DATE, -1);

                String curDate = formatter.format(c1.getTime());
                String yestDate = formatter.format(c2.getTime());
                System.out.println("curDate--------" + curDate);
                System.out.println("yestDate--------" + yestDate);

                String emailFormat1 = "";
                String emailFormat2 = "";
                emailFormat1 = "<html>"
                        + "<body>"
                        + "<p><b>Dear Team, <br><br><b>GOOD MORNING"
                        + "<br><br>Please find the Bookings which is received on " + yestDate + " (6.00 A.M.) to " + curDate + "(6.00 A.M.) and find the abstract in the attachement, <br><br/>";
                emailFormat2 = "</body></html><br><br><br>"
                        + "<html>"
                        + "<body>"
                        + "<table>"
                        + "<tr>"
                        + "<br><br><br>Regards,"
                        + "<br><br/>CA Log<br/><br/>This is an Auto Generated Email Please Do Not Reply"
                        + " .</p>"
                        + "</tr>"
                        + "</table>"
                        + "</body></html>";
                emailFormat = emailFormat1 + "" + emailFormat2 + "";
                emailFormat = emailFormat + "~" + filename + "~" + filepath;
                System.out.println("filepath " + filepath);
                System.out.println("emailFormat : 1  " + emailFormat);
                System.out.println("emailFormat : 2  " + emailFormat);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        }

        //  System.out.println("emailFormat : 3  "+emailFormat);
        return emailFormat;
    }

    public ArrayList getCompanyLists() throws FPRuntimeException, FPBusinessException {
        ArrayList companyList = new ArrayList();
        companyList = reportDAO.getCompanyLists();
        return companyList;
    }

    public ArrayList getTripClosureList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getTripClosureList = new ArrayList();
        getTripClosureList = reportDAO.getTripClosureList(reportTO);
        return getTripClosureList;
    }

    public int updateTripDktStatus(ReportTO reportTo) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = reportDAO.updateTripDktStatus(reportTo);
        return status;
    }

    
    public String getDailyBookingReportAlert(ReportTO reportTO1) throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("system date = " + systemTime);
        String emailFormat = "";

        DecimalFormat df = new DecimalFormat("#.##");
        String name = "DailyBookingReport_" + systemTime + ".xls";
        String filename = name;
        String filepath = ThrottleConstants.dailyBookingAlertsPath + name;
        System.out.println("filename = " + name);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        String endDate = dateFormat.format(cal.getTime());
        cal.add(Calendar.DATE, -1);
        String fromDate = dateFormat.format(cal.getTime());

        try {

            File theDir = new File(ThrottleConstants.dailyBookingAlertsPath);
            if (!theDir.exists()) {
                boolean result = theDir.mkdir();
                if (result) {
                    System.out.println(ThrottleConstants.dailyBookingAlertsPath + " Created");
                }
            }

            //String customerName = reportTO1.getCustomerName();
            //String name = customerName + "_(" + startDate + ") TO (" + endDate + ")" + ".xls";
            HSSFWorkbook my_workbook = new HSSFWorkbook();

            String sheetName = "Daily Booking Report";
            String sheetName1 = "Started Trips";
            String sheetName2 = "Trip in Progress";
            String sheetName3 = "Ended Trips";
            String sheetName4 = "Closure Trips";
            String sheetName5 = "Moffusil - Own";
            String sheetName6 = "Local - Own";
            String sheetName7 = "Cancelled Trips";

            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderTop((short) 1);

            HSSFRow header = my_sheet.createRow(0);
            header.setHeightInPoints(20); // row hight
            
            HSSFCell cell1 = header.createCell((short) 0);
            style.setWrapText(true);
            cell1.setCellValue("Sno");
            cell1.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 4000);

            HSSFCell cell2 = header.createCell((short) 1);
            style.setWrapText(true);
            cell2.setCellValue("Customer Name");
            cell2.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 4000);

            HSSFCell cell3 = header.createCell((short) 2);
            cell3.setCellValue("Route Interim");
            cell3.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cell4 = header.createCell((short) 3);
            cell4.setCellValue("Qty");
            cell4.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 4000);

            HSSFCell cell5 = header.createCell((short) 4);
            cell5.setCellValue("");
            cell5.setCellStyle(style);
            my_sheet.setColumnWidth((short) 4, (short) 4000);

            HSSFCell cell6 = header.createCell((short) 5);
            cell6.setCellValue("Own");
            cell6.setCellStyle(style);
            my_sheet.setColumnWidth((short) 5, (short) 4000);

            HSSFCell cell7 = header.createCell((short) 6);
            cell7.setCellValue("");
            cell7.setCellStyle(style);
            my_sheet.setColumnWidth((short) 6, (short) 4000);

            HSSFCell cell8 = header.createCell((short) 7);
            cell8.setCellValue("Hire");
            cell8.setCellStyle(style);
            my_sheet.setColumnWidth((short) 7, (short) 4000);

            HSSFCell cell9 = header.createCell((short) 8);
            cell9.setCellValue("");
            cell9.setCellStyle(style);
            my_sheet.setColumnWidth((short) 8, (short) 4000);

            header = my_sheet.createRow(1);
            header.setHeightInPoints(20);

            HSSFCell cell11 = header.createCell((short) 0);
            cell11.setCellValue("");
            cell11.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 4000);
            
            HSSFCell cell12 = header.createCell((short) 1);
            cell12.setCellValue("");
            cell12.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 4000);

            HSSFCell cell13 = header.createCell((short) 2);
            cell13.setCellValue("");
            cell13.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cell14 = header.createCell((short) 3);
            cell14.setCellValue("20Ft");
            cell14.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 4000);

            HSSFCell cell15 = header.createCell((short) 4);
            cell15.setCellValue("40Ft");
            cell15.setCellStyle(style);
            my_sheet.setColumnWidth((short) 4, (short) 4000);

            HSSFCell cell17 = header.createCell((short) 5);
            cell17.setCellValue("20Ft");
            cell17.setCellStyle(style);
            my_sheet.setColumnWidth((short) 5, (short) 4000);

            HSSFCell cell18 = header.createCell((short) 6);
            cell18.setCellValue("40Ft");
            cell18.setCellStyle(style);
            my_sheet.setColumnWidth((short) 6, (short) 4000);

            HSSFCell cell117 = header.createCell((short) 7);
            cell117.setCellValue("20Ft");
            cell117.setCellStyle(style);
            my_sheet.setColumnWidth((short) 7, (short) 4000);

            HSSFCell cell118 = header.createCell((short) 8);
            cell118.setCellValue("40Ft");
            cell118.setCellStyle(style);
            my_sheet.setColumnWidth((short) 8, (short) 4000);

            ReportTO reportTO = new ReportTO();
            int cntr = 2;
            int rr = 1;
            String countType = "";
            boolean check = false;

            reportTO1.setFromDate(fromDate);
            reportTO1.setToDate(endDate);

            ArrayList BookingList = new ArrayList();
            BookingList = reportDAO.getBookingList(reportTO1);
            Iterator itrs = BookingList.iterator();
            while (itrs.hasNext()) {
                reportTO = (ReportTO) itrs.next();
                check = true;
                System.out.println("second table---" + cntr);
                HSSFRow rows = my_sheet.createRow(cntr);
                rows = my_sheet.createRow((short) cntr);

                rows.createCell((short) 0).setCellValue(rr);
                rows.createCell((short) 1).setCellValue(reportTO.getCustomerName());
                rows.createCell((short) 2).setCellValue(reportTO.getRouteInfo());
                rows.createCell((short) 3).setCellValue(reportTO.getOwn20Count());
                rows.createCell((short) 4).setCellValue(reportTO.getOwn40Count());
                rows.createCell((short) 5).setCellValue(reportTO.getHire20Count());
                rows.createCell((short) 6).setCellValue(reportTO.getHire40Count());
                rows.createCell((short) 7).setCellValue(reportTO.getTotal20Count());
                rows.createCell((short) 8).setCellValue(reportTO.getTotal40Count());

                cntr++;
                rr++;
            }

            cntr = cntr + 2;
            int totalOwn = 0;
            int totalHire = 0;
            ReportTO reportTOs = null;

            System.out.println("Bookings Status count before  -  " + cntr);

            style.setBorderTop((short) 1);
            header = my_sheet.createRow(cntr);
            header.setHeightInPoints(20);

            HSSFCell cells101 = header.createCell((short) 0);
            style.setWrapText(true);
            cells101.setCellValue("Bookings Status");
            cells101.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 6000);

            HSSFCell cells102 = header.createCell((short) 1);
            cells102.setCellValue("20 ft");
            cells102.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 4000);

            HSSFCell cells103 = header.createCell((short) 2);
            cells103.setCellValue("40 ft");
            cells103.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cells104 = header.createCell((short) 3);
            cells104.setCellValue("Total");
            cells104.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 4000);

            ArrayList dailyBookingCountList = new ArrayList();
//            dailyBookingCountList = reportDAO.dailyBookingCountList(reportTO1);
            dailyBookingCountList = reportDAO.getBookingReportDetails(reportTO1);
            Iterator itr = dailyBookingCountList.iterator();

            System.out.println("dailyBookingCountList : " + dailyBookingCountList.size());

            while (itr.hasNext()) {
                reportTO = (ReportTO) itr.next();
                check = true;
                ++cntr;
                HSSFRow row = my_sheet.createRow(cntr);
                row = my_sheet.createRow((short) cntr);
//                row.createCell((short) 0).setCellValue(cntr);
                if ("1".equals(reportTO.getCountType())) {
                    countType = "Bookings Received";
                } else if ("2".equals(reportTO.getCountType())) {
                    countType = "Started Trips ";
                } else if ("3".equals(reportTO.getCountType())) {
                    countType = "Ended Trips ";
                } else if ("4".equals(reportTO.getCountType())) {
                    countType = "Closure Trips ";
                } else if ("5".equals(reportTO.getCountType())) {
                    countType = "Pending Bookings";
                } else if ("6".equals(reportTO.getCountType())) {
                    countType = "Cancelled Bookings";
                }

                row.createCell((short) 0).setCellValue(countType);
                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
                row.createCell((short) 3).setCellValue(reportTO.getTotCount());
            }

            cntr = cntr + 2;

            style.setBorderTop((short) 1);
            header = my_sheet.createRow(cntr);
            header.setHeightInPoints(20);

            HSSFCell cells11 = header.createCell((short) 0);
            style.setWrapText(true);
            cells11.setCellValue("Bookings Assigned");
            cells11.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 6000);

            HSSFCell cells12 = header.createCell((short) 1);
            cells12.setCellValue("Own");
            cells12.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 4000);

            HSSFCell cells13 = header.createCell((short) 2);
            cells13.setCellValue("Hire");
            cells13.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cells4 = header.createCell((short) 3);
            cells4.setCellValue("Total");
            cells4.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 4000);

            ArrayList movementAllocationList = new ArrayList();
//            movementAllocationList = reportDAO.movementAllocationList(reportTO1);
            movementAllocationList = reportDAO.getMovementAllocationList(reportTO1);
            Iterator itrs1 = movementAllocationList.iterator();

            int totMoff = 0;
            int totLoff = 0;
            int totCan = 0;

            while (itrs1.hasNext()) {
                reportTOs = (ReportTO) itrs1.next();
                ++cntr;
                System.out.println("second table---" + cntr);
                HSSFRow rows = my_sheet.createRow(cntr);
                rows = my_sheet.createRow((short) cntr);
//                row.createCell((short) 0).setCellValue(cntr);
                if ("1".equals(reportTOs.getMovementType())) {
                    countType = "Moffusil";
                    totMoff += reportTOs.getOwnCount() + reportTOs.getHireCount();
                } else if ("2".equals(reportTOs.getMovementType())) {
                    countType = "Local";
                    totLoff += reportTOs.getOwnCount() + reportTOs.getHireCount();
                } else if ("3".equals(reportTOs.getMovementType())) {
                    countType = "Cancelled Trips";
                    totCan += reportTOs.getOwnCount() + reportTOs.getHireCount();
                }

                int total = totalOwn + totalHire;
                rows.createCell((short) 0).setCellValue(countType);
                rows.createCell((short) 1).setCellValue(reportTOs.getOwnCount());
                rows.createCell((short) 2).setCellValue(reportTOs.getHireCount());

                if ("1".equals(reportTOs.getMovementType())) {
                    rows.createCell((short) 3).setCellValue(totMoff);
                } else if ("2".equals(reportTOs.getMovementType())) {
                    rows.createCell((short) 3).setCellValue(totLoff);
                } else if ("3".equals(reportTOs.getMovementType())) {
                    rows.createCell((short) 3).setCellValue(totCan);
                }
            }
            
            cntr = cntr + 2;

            style.setBorderTop((short) 1);
            header = my_sheet.createRow(cntr);
            header.setHeightInPoints(20);

            cells101 = header.createCell((short) 0);
            style.setWrapText(true);
            cells101.setCellValue("Own Vehicle Status");
            cells101.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 6000);

            cells102 = header.createCell((short) 1);
            cells102.setCellValue("20 ft");
            cells102.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 4000);

            cells103 = header.createCell((short) 2);
            cells103.setCellValue("40 ft");
            cells103.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 4000);

            cells104 = header.createCell((short) 3);
            cells104.setCellValue("Total");
            cells104.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 4000);

            totalOwn = 0;
            totalHire = 0;

            ArrayList OwndailyBookingCountList = new ArrayList();
            //        OwndailyBookingCountList = reportDAO.OwndailyBookingCountList(reportTO1);
            OwndailyBookingCountList = reportDAO.getOwnBookingCountList(reportTO1);
            Iterator itr1 = OwndailyBookingCountList.iterator();

            ++cntr;

            reportTO = new ReportTO();
            countType = "";
            check = false;
            while (itr1.hasNext()) {
                reportTO = (ReportTO) itr1.next();
                check = true;
                HSSFRow row = my_sheet.createRow(cntr);
                row = my_sheet.createRow((short) cntr);

                if ("1".equals(reportTO.getCountType())) {
                    countType = "Started Trips ";
                } else if ("2".equals(reportTO.getCountType())) {
                    countType = "Trips in Progress ";
                } else if ("3".equals(reportTO.getCountType())) {
                    countType = "Ended Trips ";
                } else if ("4".equals(reportTO.getCountType())) {
                    countType = "Closure Trips";
                } else if ("5".equals(reportTO.getCountType())) {
                    countType = "WorkShop";
                } else if ("6".equals(reportTO.getCountType())) {
                    countType = "Idle - No Driver";
                }

                row.createCell((short) 0).setCellValue(countType);
                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
                row.createCell((short) 3).setCellValue(reportTO.getTotCount());

                cntr++;
            }

            cntr = cntr + 2;
            
            style.setBorderTop((short) 1);

            header = my_sheet.createRow(cntr);
            header.setHeightInPoints(20); // row hight

            HSSFCell cells21 = header.createCell((short) 0);
            style.setWrapText(true);
            cells21.setCellValue("Own Vehicle Availability");
            cells21.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 6000);

            HSSFCell cells22 = header.createCell((short) 1);
            cells22.setCellValue("20 ft");
            cells22.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 4000);

            HSSFCell cells23 = header.createCell((short) 2);
            cells23.setCellValue("40 ft");
            cells23.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cell24 = header.createCell((short) 3);
            cell24.setCellValue("Total");
            cell24.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 4000);

            totalOwn = 0;
            totalHire = 0;

            ArrayList OwnVehicleAvail = new ArrayList();
//            OwnVehicleAvail = reportDAO.OwnVehicleAvail(reportTO1);
            OwnVehicleAvail = reportDAO.getOwnVehicleAvail(reportTO1);
            Iterator itr11 = OwnVehicleAvail.iterator();

            reportTO = new ReportTO();
            countType = "";
            check = false;
            while (itr11.hasNext()) {
                reportTO = (ReportTO) itr11.next();
                check = true;
                ++cntr;
                HSSFRow row = my_sheet.createRow(cntr);
                row = my_sheet.createRow((short) cntr);

                if ("1".equals(reportTO.getCountType())) {
                    countType = "WFL ";
                } else if ("2".equals(reportTO.getCountType())) {
                    countType = "Vehicles can plan for next trip ";
                }

                row.createCell((short) 0).setCellValue(countType);
                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
                row.createCell((short) 3).setCellValue(reportTO.getTotCount());

            }

            cntr = cntr + 2;

            style.setBorderTop((short) 1);

            header = my_sheet.createRow(cntr);
            header.setHeightInPoints(20);

            HSSFCell cells31 = header.createCell((short) 0);
            style.setWrapText(true);
            cells31.setCellValue("Hire Vehicle Status");
            cells31.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 6000);

            HSSFCell cells32 = header.createCell((short) 1);
            cells32.setCellValue("20 ft");
            cells32.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 4000);

            HSSFCell cells33 = header.createCell((short) 2);
            cells33.setCellValue("40 ft");
            cells33.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cell34 = header.createCell((short) 3);
            cell34.setCellValue("Total");
            cell34.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 4000);

            totalOwn = 0;
            totalHire = 0;

            ArrayList HiredailyBookingCountList = new ArrayList();
            //HiredailyBookingCountList = reportDAO.HiredailyBookingCountList(reportTO1);
            HiredailyBookingCountList = reportDAO.getHireBookingCountList(reportTO1);
            Iterator itr22 = HiredailyBookingCountList.iterator();

            reportTO = new ReportTO();
            countType = "";
            check = false;
            while (itr22.hasNext()) {
                reportTO = (ReportTO) itr22.next();
                check = true;
                ++cntr;
                HSSFRow row = my_sheet.createRow(cntr);
                row = my_sheet.createRow((short) cntr);

                if ("1".equals(reportTO.getCountType())) {
                    countType = "Started Trips ";
                } else if ("2".equals(reportTO.getCountType())) {
                    countType = "Trips in Progress ";
                } else if ("3".equals(reportTO.getCountType())) {
                    countType = "Ended Trips ";
                } else if ("4".equals(reportTO.getCountType())) {
                    countType = "Closure Trips";
                }

                row.createCell((short) 0).setCellValue(countType);
                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
                row.createCell((short) 3).setCellValue(reportTO.getTotCount());
            }

//            sheet one  - start trips
            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName1);
            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            HSSFRow header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            HSSFCell cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            HSSFCell cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("CustRefNo");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            HSSFCell cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("Customer");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("Goods");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            HSSFCell cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("Vehicle No");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);

            HSSFCell cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("TripCode");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            HSSFCell cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("Container No");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            HSSFCell cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("LR Nos");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            HSSFCell cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Pkg/Wgt/Cbm");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);

            HSSFCell cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Trip Date");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            HSSFCell cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("Order Type");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 4000);

            HSSFCell cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Transporter");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 4000);

            HSSFCell cellOne13 = header1.createCell((short) 12);
            cellOne13.setCellValue("Route");
            cellOne13.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 12, (short) 4000);

            HSSFCell cellOne14 = header1.createCell((short) 13);
            cellOne14.setCellValue("Driver");
            cellOne14.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 13, (short) 4000);

            HSSFCell cellOne15 = header1.createCell((short) 14);
            cellOne15.setCellValue("Vehicle Type");
            cellOne15.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 14, (short) 4000);

            HSSFCell cellOne16 = header1.createCell((short) 15);
            cellOne16.setCellValue("Status");
            cellOne16.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 15, (short) 4000);

            ReportTO reportToOne = null;
            int cntrOne = 1;
            int countOne = 1;
            boolean checkOne = false;
            ArrayList tripStartContainerDetails = new ArrayList();
            reportTO1.setTripType("1");
            tripStartContainerDetails = reportDAO.getTripStartContainerDetails(reportTO1);
            //tripStartContainerDetails = reportDAO.tripStartContainerDetails(reportTO1);
            Iterator itrOne = tripStartContainerDetails.iterator();
            System.out.println("tripStartContainerDetails............." + tripStartContainerDetails.size());
            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);

                row1.createCell((short) 0).setCellValue(cntrOne);
                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                cntrOne++;
            }

            //            sheet one  - trip in progress
            my_sheet1 = my_workbook.createSheet(sheetName2);
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("CustRefNo");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("Customer");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("Goods");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("Vehicle No");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);

            cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("TripCode");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("Container No");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("LR Nos");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Pkg/Wgt/Cbm");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);

            cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Trip Date");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("Order Type");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 4000);

            cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Transporter");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 4000);

            cellOne13 = header1.createCell((short) 12);
            cellOne13.setCellValue("Route");
            cellOne13.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 12, (short) 4000);

            cellOne14 = header1.createCell((short) 13);
            cellOne14.setCellValue("Driver");
            cellOne14.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 13, (short) 4000);

            cellOne15 = header1.createCell((short) 14);
            cellOne15.setCellValue("Vehicle Type");
            cellOne15.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 14, (short) 4000);

            cellOne16 = header1.createCell((short) 15);
            cellOne16.setCellValue("Status");
            cellOne16.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 15, (short) 4000);

            reportToOne = null;
            cntrOne = 1;
            countOne = 1;
            checkOne = false;
            ArrayList tripInProgressDetails = new ArrayList();
            reportTO1.setTripType("2");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
            tripInProgressDetails = reportDAO.getTripStartContainerDetails(reportTO1);
            itrOne = tripInProgressDetails.iterator();
            System.out.println("tripInProgressDetails............." + tripInProgressDetails.size());
            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);

                row1.createCell((short) 0).setCellValue(cntrOne);
                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                cntrOne++;
            }

            //            sheet one  - trip end
            my_sheet1 = my_workbook.createSheet(sheetName3);
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("CustRefNo");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("Customer");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("Goods");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("Vehicle No");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);

            cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("TripCode");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("Container No");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("LR Nos");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Pkg/Wgt/Cbm");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);

            cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Trip Date");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("Order Type");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 4000);

            cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Transporter");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 4000);

            cellOne13 = header1.createCell((short) 12);
            cellOne13.setCellValue("Route");
            cellOne13.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 12, (short) 4000);

            cellOne14 = header1.createCell((short) 13);
            cellOne14.setCellValue("Driver");
            cellOne14.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 13, (short) 4000);

            cellOne15 = header1.createCell((short) 14);
            cellOne15.setCellValue("Vehicle Type");
            cellOne15.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 14, (short) 4000);

            cellOne16 = header1.createCell((short) 15);
            cellOne16.setCellValue("Status");
            cellOne16.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 15, (short) 4000);

            reportToOne = null;
            cntrOne = 1;
            countOne = 1;
            checkOne = false;

            reportTO1.setTripType("3");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
            tripInProgressDetails = reportDAO.getTripStartContainerDetails(reportTO1);
            itrOne = tripInProgressDetails.iterator();
            System.out.println("tripEndDetails............." + tripInProgressDetails.size());
            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);

                row1.createCell((short) 0).setCellValue(cntrOne);
                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                cntrOne++;

            }

            //            sheet one  - trip closure
            my_sheet1 = my_workbook.createSheet(sheetName4);
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("CustRefNo");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("Customer");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("Goods");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("Vehicle No");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);

            cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("TripCode");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("Container No");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("LR Nos");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Pkg/Wgt/Cbm");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);

            cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Trip Date");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("Order Type");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 4000);

            cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Transporter");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 4000);

            cellOne13 = header1.createCell((short) 12);
            cellOne13.setCellValue("Route");
            cellOne13.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 12, (short) 4000);

            cellOne14 = header1.createCell((short) 13);
            cellOne14.setCellValue("Driver");
            cellOne14.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 13, (short) 4000);

            cellOne15 = header1.createCell((short) 14);
            cellOne15.setCellValue("Vehicle Type");
            cellOne15.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 14, (short) 4000);

            cellOne16 = header1.createCell((short) 15);
            cellOne16.setCellValue("Status");
            cellOne16.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 15, (short) 4000);

            reportToOne = null;
            cntrOne = 1;
            countOne = 1;
            checkOne = false;

            reportTO1.setTripType("4");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
            tripInProgressDetails = reportDAO.getTripStartContainerDetails(reportTO1);
            itrOne = tripInProgressDetails.iterator();
            System.out.println("closuredetails............." + tripInProgressDetails.size());

            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);

                row1.createCell((short) 0).setCellValue(cntrOne);
                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                cntrOne++;

            }

            //            sheet one  - trip Moffusil own
            my_sheet1 = my_workbook.createSheet(sheetName5);
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("CustRefNo");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("Customer");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("Goods");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("Vehicle No");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);

            cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("TripCode");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("Container No");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("LR Nos");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Pkg/Wgt/Cbm");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);

            cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Trip Date");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("Order Type");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 4000);

            cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Transporter");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 4000);

            cellOne13 = header1.createCell((short) 12);
            cellOne13.setCellValue("Route");
            cellOne13.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 12, (short) 4000);

            cellOne14 = header1.createCell((short) 13);
            cellOne14.setCellValue("Driver");
            cellOne14.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 13, (short) 4000);

            cellOne15 = header1.createCell((short) 14);
            cellOne15.setCellValue("Vehicle Type");
            cellOne15.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 14, (short) 4000);

            cellOne16 = header1.createCell((short) 15);
            cellOne16.setCellValue("Status");
            cellOne16.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 15, (short) 4000);

            reportToOne = null;
            cntrOne = 1;
            countOne = 1;
            checkOne = false;

            reportTO1.setTripType("5");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
            tripInProgressDetails = reportDAO.getTripStartContainerDetails(reportTO1);
            itrOne = tripInProgressDetails.iterator();
            System.out.println("moffusil own............." + tripInProgressDetails.size());

            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);

                row1.createCell((short) 0).setCellValue(cntrOne);
                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                cntrOne++;
            }

            //   sheet one  - trip Local own
            my_sheet1 = my_workbook.createSheet(sheetName6);
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("CustRefNo");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("Customer");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("Goods");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("Vehicle No");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);

            cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("TripCode");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("Container No");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("LR Nos");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Pkg/Wgt/Cbm");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);

            cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Trip Date");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("Order Type");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 4000);

            cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Transporter");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 4000);

            cellOne13 = header1.createCell((short) 12);
            cellOne13.setCellValue("Route");
            cellOne13.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 12, (short) 4000);

            cellOne14 = header1.createCell((short) 13);
            cellOne14.setCellValue("Driver");
            cellOne14.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 13, (short) 4000);

            cellOne15 = header1.createCell((short) 14);
            cellOne15.setCellValue("Vehicle Type");
            cellOne15.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 14, (short) 4000);

            cellOne16 = header1.createCell((short) 15);
            cellOne16.setCellValue("Status");
            cellOne16.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 15, (short) 4000);

            reportToOne = null;
            cntrOne = 1;
            countOne = 1;
            checkOne = false;

            reportTO1.setTripType("6");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
            tripInProgressDetails = reportDAO.getTripStartContainerDetails(reportTO1);
            itrOne = tripInProgressDetails.iterator();
            System.out.println("local own............." + tripInProgressDetails.size());
            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);

                row1.createCell((short) 0).setCellValue(cntrOne);
                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                cntrOne++;

            }

            //            sheet one  - trip cancell
            my_sheet1 = my_workbook.createSheet(sheetName7);
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("CustRefNo");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("Customer");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("Goods");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("Vehicle No");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);

            cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("TripCode");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("Container No");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("LR Nos");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Pkg/Wgt/Cbm");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);

            cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Trip Date");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("Order Type");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 4000);

            cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Transporter");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 4000);

            cellOne13 = header1.createCell((short) 12);
            cellOne13.setCellValue("Route");
            cellOne13.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 12, (short) 4000);

            cellOne14 = header1.createCell((short) 13);
            cellOne14.setCellValue("Driver");
            cellOne14.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 13, (short) 4000);

            cellOne15 = header1.createCell((short) 14);
            cellOne15.setCellValue("Vehicle Type");
            cellOne15.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 14, (short) 4000);

            cellOne16 = header1.createCell((short) 15);
            cellOne16.setCellValue("Status");
            cellOne16.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 15, (short) 4000);

            reportToOne = null;
            cntrOne = 1;
            countOne = 1;
            checkOne = false;

            reportTO1.setTripType("7");
//            tripInProgressDetails = reportDAO.tripStartContainerDetails(reportTO1);
            tripInProgressDetails = reportDAO.getTripStartContainerDetails(reportTO1);
            itrOne = tripInProgressDetails.iterator();
            System.out.println("trip cancell............." + tripInProgressDetails.size());
            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);

                row1.createCell((short) 0).setCellValue(cntrOne);
                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                cntrOne++;

            }

            ArrayList jobCardList = new ArrayList();
            ReportTO reportTO11 = new ReportTO();
            ReportTO repTO = new ReportTO();

            reportTO11.setCompanyId("1461");
            jobCardList = getJobCardList(reportTO11);

            sheetName = "Workshop";
            my_sheet = my_workbook.createSheet(sheetName);
            style = (HSSFCellStyle) my_workbook.createCellStyle();
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);            
            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

            header = my_sheet.createRow(0);
            header.setHeightInPoints(20); // row hight

            cell1 = header.createCell((short) 0);
            style.setWrapText(true);
            cell1.setCellValue("SNo");
            cell1.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 4000);

            cell2 = header.createCell((short) 1);
            cell2.setCellValue("TruckNo");
            cell2.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 6000);

            cell3 = header.createCell((short) 2);
            cell3.setCellValue("Branch");
            cell3.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 6000);

            cell4 = header.createCell((short) 3);
            cell4.setCellValue("Status");
            cell4.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 6000);

            HSSFCell cellx5 = header.createCell((short) 4);
            cellx5.setCellValue("ServiceType");
            cellx5.setCellStyle(style);
            my_sheet.setColumnWidth((short) 4, (short) 6000);

            HSSFCell cellx6 = header.createCell((short) 5);
            cellx6.setCellValue("How Many Days in Workshop");
            cellx6.setCellStyle(style);
            my_sheet.setColumnWidth((short) 5, (short) 6000);

            cntr = 1;
            itr = jobCardList.iterator();
            while (itr.hasNext()) {
                repTO = (ReportTO) itr.next();
                HSSFRow row = my_sheet.createRow(cntr);
                row = my_sheet.createRow((short) cntr);
                row.createCell((short) 0).setCellValue(cntr);
                row.getCell((short) 0).setCellStyle(style1);
                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                row.getCell((short) 1).setCellStyle(style1);
                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
                row.getCell((short) 2).setCellStyle(style1);
                row.createCell((short) 3).setCellValue(repTO.getStatusName());
                row.getCell((short) 3).setCellStyle(style1);
                row.createCell((short) 4).setCellValue(repTO.getServiceTypeName());
                row.getCell((short) 4).setCellStyle(style1);
                row.createCell((short) 5).setCellValue(repTO.getAge());
                row.getCell((short) 5).setCellStyle(style1);
                cntr++;
            }

            sheetName = "Idle - No Driver";

            my_sheet = my_workbook.createSheet(sheetName);
            style = (HSSFCellStyle) my_workbook.createCellStyle();
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

            header = my_sheet.createRow(0);
            header.setHeightInPoints(20); // row hight

            cell1 = header.createCell((short) 0);
            style.setWrapText(true);
            cell1.setCellValue("SNo");
            cell1.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 4000);

            cell2 = header.createCell((short) 1);
            cell2.setCellValue("TruckNo");
            cell2.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 6000);

            cell3 = header.createCell((short) 2);
            cell3.setCellValue("Branch");
            cell3.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 6000);

            HSSFCell cell31 = header.createCell((short) 3);
            cell31.setCellValue("VehicleType");
            cell31.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 6000);

            cell4 = header.createCell((short) 4);
            cell4.setCellValue("How Many Days in Idle?");
            cell4.setCellStyle(style);
            my_sheet.setColumnWidth((short) 4, (short) 6000);

            ArrayList wfuList = new ArrayList();
            wfuList = getWfuList(reportTO);
            cntr = 1;
            itr = wfuList.iterator();
            while (itr.hasNext()) {
                repTO = (ReportTO) itr.next();
                HSSFRow row = my_sheet.createRow(cntr);
                row = my_sheet.createRow((short) cntr);
                row.createCell((short) 0).setCellValue(cntr);
                row.getCell((short) 0).setCellStyle(style1);
                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                row.getCell((short) 1).setCellStyle(style1);
                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
                row.getCell((short) 2).setCellStyle(style1);
                row.createCell((short) 3).setCellValue(repTO.getTonnage());
                row.getCell((short) 3).setCellStyle(style1);
                row.createCell((short) 4).setCellValue(repTO.getAge());
                row.getCell((short) 4).setCellStyle(style1);
                cntr++;
            }

            sheetName = "WFL";

            my_sheet = my_workbook.createSheet(sheetName);
            style = (HSSFCellStyle) my_workbook.createCellStyle();
            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            //                            style.setBorderTop((short) 1);
            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

            header = my_sheet.createRow(0);
            header.setHeightInPoints(20); // row hight

            cell1 = header.createCell((short) 0);
            style.setWrapText(true);
            cell1.setCellValue("SNo");
            cell1.setCellStyle(style);
            my_sheet.setColumnWidth((short) 0, (short) 4000);

            cell2 = header.createCell((short) 1);
            cell2.setCellValue("TruckNo");
            cell2.setCellStyle(style);
            my_sheet.setColumnWidth((short) 1, (short) 6000);

            cell3 = header.createCell((short) 2);
            cell3.setCellValue("Branch");
            cell3.setCellStyle(style);
            my_sheet.setColumnWidth((short) 2, (short) 6000);

            cell31 = header.createCell((short) 3);
            cell31.setCellValue("VehicleType");
            cell31.setCellStyle(style);
            my_sheet.setColumnWidth((short) 3, (short) 6000);

            cell4 = header.createCell((short) 4);
            cell4.setCellValue("How Many Days in Idle?");
            cell4.setCellStyle(style);
            my_sheet.setColumnWidth((short) 4, (short) 6000);

            ArrayList tripNotStartedList = new ArrayList();
            tripNotStartedList = reportDAO.getTripNotStartedList(reportTO);
            System.out.println("tripNotStartedList::" + tripNotStartedList.size());
            cntr = 1;
            itr = tripNotStartedList.iterator();
            while (itr.hasNext()) {
                repTO = (ReportTO) itr.next();
                HSSFRow row = my_sheet.createRow(cntr);
                row = my_sheet.createRow((short) cntr);
                row.createCell((short) 0).setCellValue(cntr);
                row.getCell((short) 0).setCellStyle(style1);
                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                row.getCell((short) 1).setCellStyle(style1);
                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
                row.getCell((short) 2).setCellStyle(style1);
                row.createCell((short) 3).setCellValue(repTO.getTonnage());
                row.getCell((short) 3).setCellStyle(style1);
                row.createCell((short) 4).setCellValue(repTO.getAge());
                row.getCell((short) 4).setCellStyle(style1);
                cntr++;
            }

            System.out.println("Your Mail excel Sheet created");
            System.out.println("Your Mail excel Sheet filepath : " + filepath);

            try {
                FileOutputStream fileOut = new FileOutputStream(filepath);
                my_workbook.write(fileOut);
                fileOut.close();
                System.out.println("Mail Excel written successfully..");

                SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Calendar c1 = Calendar.getInstance();

                Calendar c2 = Calendar.getInstance();
                c2.add(Calendar.DATE, -1);

                String curDate = formatter.format(c1.getTime());
                String yestDate = formatter.format(c2.getTime());
                System.out.println("curDate--------" + curDate);
                System.out.println("yestDate--------" + yestDate);

                String emailFormat1 = "";
                String emailFormat2 = "";
                String rowContent11 = "";
                cntr = 1;

                itrs = BookingList.iterator();
                while (itrs.hasNext()) {
                    reportTO = (ReportTO) itrs.next();
                    check = true;
                    System.out.println("second table---" + cntr);

                    rowContent11 = rowContent11 + "<tr>";
                    rowContent11 = rowContent11 + "<td>" + cntr + "</td>";
                    rowContent11 = rowContent11 + "<td>" + reportTO.getCustomerName() + "</td>";
                    rowContent11 = rowContent11 + "<td>" + reportTO.getRouteInfo() + "</td>";
                    rowContent11 = rowContent11 + "<td>" + reportTO.getTotal20Count() + "</td>";
                    rowContent11 = rowContent11 + "<td>" + reportTO.getTotal40Count() + "</td>";
                    rowContent11 = rowContent11 + "<td>" + reportTO.getOwn20Count() + "</td>";
                    rowContent11 = rowContent11 + "<td>" + reportTO.getOwn40Count() + "</td>";
                    rowContent11 = rowContent11 + "<td>" + reportTO.getHire20Count() + "</td>";
                    rowContent11 = rowContent11 + "<td>" + reportTO.getHire40Count() + "</td>";
                    rowContent11 = rowContent11 + "</tr>";
                    cntr++;

                }

                String rowContent5 = "";
                itr = dailyBookingCountList.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    rowContent5 = rowContent5 + "<tr>";
                    if ("1".equals(reportTO.getCountType())) {
                        countType = "Bookings Received";
                    } else if ("2".equals(reportTO.getCountType())) {
                        countType = "Started Trips ";
                    } else if ("3".equals(reportTO.getCountType())) {
                        countType = "Ended Trips ";
                    } else if ("4".equals(reportTO.getCountType())) {
                        countType = "Closure Trips ";
                    } else if ("5".equals(reportTO.getCountType())) {
                        countType = "Pending Bookings";
                    } else if ("6".equals(reportTO.getCountType())) {
                        countType = "Cancelled Bookings";
                    }

                    rowContent5 = rowContent5 + "<td>" + countType + "</td>";
                    rowContent5 = rowContent5 + "<td>" + reportTO.getTwentyFtCount() + "</td>";
                    rowContent5 = rowContent5 + "<td>" + reportTO.getFourtyFtCount() + "</td>";
                    rowContent5 = rowContent5 + "<td>" + reportTO.getTotCount() + "</td>";
                    rowContent5 = rowContent5 + "</tr>";
                    System.out.println("rowContent===" + rowContent5);

                }

                itrs = movementAllocationList.iterator();
                String rowContent1 = "";

                while (itrs.hasNext()) {
                    reportTOs = (ReportTO) itrs.next();
                    rowContent1 = rowContent1 + "<tr>";
                    if ("1".equals(reportTOs.getMovementType())) {
                        countType = "Moffusil";
                    } else if ("2".equals(reportTOs.getMovementType())) {
                        countType = "Local";
                    } else if ("3".equals(reportTOs.getMovementType())) {
                        countType = "Cancelled Trips";
                    }

                    totalOwn = totalOwn + reportTOs.getOwnCount();
                    totalHire = totalHire + reportTOs.getHireCount();
                    rowContent1 = rowContent1 + "<td>" + countType + "</td>";
                    rowContent1 = rowContent1 + "<td>" + reportTOs.getOwnCount() + "</td>";
                    rowContent1 = rowContent1 + "<td>" + reportTOs.getHireCount() + "</td>";
                    rowContent1 = rowContent1 + "</tr>";
                    System.out.println("rowContent1===" + rowContent1);
                }

                /////////////// own vehicle status ////////////////////
                itr1 = OwndailyBookingCountList.iterator();
                String rowContent2 = "";
                reportTO = null;
                while (itr1.hasNext()) {
                    reportTO = (ReportTO) itr1.next();
                    rowContent2 = rowContent2 + "<tr>";
                    if ("1".equals(reportTO.getCountType())) {
                        countType = "Started Trips ";
                    } else if ("2".equals(reportTO.getCountType())) {
                        countType = "Trips in Progress ";
                    } else if ("3".equals(reportTO.getCountType())) {
                        countType = "Ended Trips ";
                    } else if ("4".equals(reportTO.getCountType())) {
                        countType = "Closure Trips";
                    } else if ("5".equals(reportTO.getCountType())) {
                        countType = "WorkShop";
                    } else if ("6".equals(reportTO.getCountType())) {
                        countType = "Idle - No Driver";
                    }

                    rowContent2 = rowContent2 + "<td>" + countType + "</td>";
                    rowContent2 = rowContent2 + "<td>" + reportTO.getTwentyFtCount() + "</td>";
                    rowContent2 = rowContent2 + "<td>" + reportTO.getFourtyFtCount() + "</td>";
                    rowContent2 = rowContent2 + "<td>" + reportTO.getTotCount() + "</td>";
                    rowContent2 = rowContent2 + "</tr>";
                    System.out.println("rowContent2===" + rowContent2);
                }

                /////////////// Own vehicle Availability  ////////////////////
                itr11 = OwnVehicleAvail.iterator();
                String rowContent3 = "";
                reportTO = new ReportTO();

                while (itr11.hasNext()) {
                    reportTO = (ReportTO) itr11.next();
                    rowContent3 = rowContent3 + "<tr>";
                    if ("1".equals(reportTO.getCountType())) {
                        countType = "WFL ";
                    } else if ("2".equals(reportTO.getCountType())) {
                        countType = "Vehicles can plan for next trip ";
                    }

                    rowContent3 = rowContent3 + "<td>" + countType + "</td>";
                    rowContent3 = rowContent3 + "<td>" + reportTO.getTwentyFtCount() + "</td>";
                    rowContent3 = rowContent3 + "<td>" + reportTO.getFourtyFtCount() + "</td>";
                    rowContent3 = rowContent3 + "<td>" + reportTO.getTotCount() + "</td>";
                    rowContent3 = rowContent3 + "</tr>";
                    System.out.println("rowContent3===" + rowContent3);

                }

                /////////////// Hire vehicle Availability  ////////////////////
                itr22 = HiredailyBookingCountList.iterator();
                String rowContent4 = "";
                reportTO = new ReportTO();

                while (itr22.hasNext()) {
                    reportTO = (ReportTO) itr22.next();
                    rowContent4 = rowContent4 + "<tr>";
                    if ("1".equals(reportTO.getCountType())) {
                        countType = "Started Trips ";
                    } else if ("2".equals(reportTO.getCountType())) {
                        countType = "Trips in Progress ";
                    } else if ("3".equals(reportTO.getCountType())) {
                        countType = "Ended Trips ";
                    } else if ("4".equals(reportTO.getCountType())) {
                        countType = "Closure Trips";
                    }

                    rowContent4 = rowContent4 + "<td>" + countType + "</td>";
                    rowContent4 = rowContent4 + "<td>" + reportTO.getTwentyFtCount() + "</td>";
                    rowContent4 = rowContent4 + "<td>" + reportTO.getFourtyFtCount() + "</td>";
                    rowContent4 = rowContent4 + "<td>" + reportTO.getTotCount() + "</td>";
                    rowContent4 = rowContent4 + "</tr>";
                    System.out.println("rowContent4===" + rowContent4);

                }

                System.out.println("rowContent==" + rowContent5);

                emailFormat1 = "<html>"
                        + "<body>"
                        + "<p><b>Dear Team, <br><br><b>GOOD MORNING"
                        + "<br><br>Please find the Bookings which is received on " + yestDate + " (6.00 A.M.) to " + curDate + "(6.00 A.M.) and find the abstract in the attachement, <br><br/>";
                emailFormat2 = "</body></html><br><br><br>"
                        + "<html>"
                        + "<body>"
                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1' >"
                        + "<tr bgcolor='lightblue'>"
                        + "<th rowspan=2>Sno</th>"
                        + "<th rowspan=2>Customer Name</th>"
                        + "<th rowspan=2>Route</th>"
                        + "<th colspan=2>Qty</th>"
                        + "<th colspan=2>Own</th>"
                        + "<th colspan=2>Hire</th>"
                        + "</tr>"
                        + "<tr>"
                        + "<th align=center>20 ft</th><th align=center>40 ft</th><th align=center>20 ft</th><th align=center>40 ft</th>"
                        + "<th align=center>20 ft</th><th align=center>40 ft</th>"
                        + "</tr>"
                        + rowContent11
                        + "</table><br><br>"
                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1' >"
                        + "<tr bgcolor='lightblue'><th>Bookings Status</th><th>20 ft</th>"
                        + "<th>40 ft</th><th>total</th></tr>"
                        + rowContent5
                        + "</table><br><br>"
                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1'>"
                        + "<tr bgcolor='lightblue'><th>Bookings Assigned</th>"
                        + "<th>Own</th><th>Hire</th></tr>"
                        + rowContent1
                        + "</table><br><br>"
                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1'>"
                        + "<tr bgcolor='lightblue'><th>Own Vehicle Status</th>"
                        + "<th>20 ft</th><th>40 ft</th><th>Total</th></tr>"
                        + rowContent2
                        + "</table><br><br>"
                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1'>"
                        + "<tr bgcolor='lightblue'><th>Own Vehicle Availability</th>"
                        + "<th>20 ft</th><th>40 ft</th><th>Total</th></tr>"
                        + rowContent3
                        + "</table><br><br>"
                        + "<table width='90%'  cellspacing='0' cellpadding='0' border='1'>"
                        + "<tr bgcolor='lightblue'><th>Hire Vehicle Status</th>"
                        + "<th>20 ft</th><th>40 ft</th><th>Total</th></tr>"
                        + rowContent4
                        + "</table><br><br>"
                        + "<table>"
                        + "<tr>"
                        + "<br><br><br>Regards,"
                        + "<br><br/>CA Log<br/><br/>This is an Auto Generated Email Please Do Not Reply"
                        + " .</p>"
                        + "</tr>"
                        + "</table>"
                        + "</body></html>";
                emailFormat = emailFormat1 + "" + emailFormat2 + "";
                emailFormat = emailFormat + "~" + filename + "~" + filepath;
                System.out.println("filepath " + filepath);
                System.out.println("emailFormat : 1  " + emailFormat);
                if (!check) {
                    emailFormat = "";
                }
                System.out.println("emailFormat : 2  " + emailFormat);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        }

        //  System.out.println("emailFormat : 3  "+emailFormat);
        return emailFormat;
    }
    
    
    
    public ArrayList getBookingList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList bookingList = new ArrayList();
        bookingList = reportDAO.getBookingList(reportTO);
        return bookingList;
    }

    public ArrayList getBookingReportDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList bookingReportList = new ArrayList();
        bookingReportList = reportDAO.getBookingReportDetails(reportTO);
        return bookingReportList;
    }

    public ArrayList getMovementAllocationList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList movementAllocationList = new ArrayList();
        movementAllocationList = reportDAO.getMovementAllocationList(reportTO);
        return movementAllocationList;
    }

    public ArrayList getOwnBookingCountList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ownBookingCountList = new ArrayList();
        ownBookingCountList = reportDAO.getOwnBookingCountList(reportTO);
        return ownBookingCountList;
    }

    public ArrayList getOwnVehicleAvail(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList ownVehicleAvail = new ArrayList();
        ownVehicleAvail = reportDAO.getOwnVehicleAvail(reportTO);
        return ownVehicleAvail;
    }

    public ArrayList getHireBookingCountList(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList hireBookingCountList = new ArrayList();
        hireBookingCountList = reportDAO.getHireBookingCountList(reportTO);
        return hireBookingCountList;
    }

    public ArrayList getTripStartContainerDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripStartContainerDetails = new ArrayList();
        tripStartContainerDetails = reportDAO.getTripStartContainerDetails(reportTO);
        return tripStartContainerDetails;
    }
    
    
    public ArrayList getFATransactionsDetails(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getFATransactionsDetails = new ArrayList();
        getFATransactionsDetails = reportDAO.getFATransactionsDetails(reportTO);
        return getFATransactionsDetails;
    }
    public ArrayList tripStatusReport(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList tripStatusReport = new ArrayList();
        tripStatusReport = reportDAO.tripStatusReport(reportTO);
        return tripStatusReport;
    }
    
    
     public String getVehicleUtilReport() throws FPRuntimeException, FPBusinessException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("system date = " + systemTime);
        String emailFormat = "";
        String fromDate = "";
        String endDate = "";
        DecimalFormat df = new DecimalFormat("#.##");
        String name = "";
        String fileName = "";
        ReportTO reportTO = new ReportTO(); 
        
        try {

            File theDir = new File(ThrottleConstants.vehicleUtilReportPath);
            if (!theDir.exists()) {
                boolean result = theDir.mkdir();
                if (result) {
                    System.out.println(ThrottleConstants.vehicleUtilReportPath + " Created");
                }
            }

            //String customerName = reportTO1.getCustomerName();
            //String name = customerName + "_(" + startDate + ") TO (" + endDate + ")" + ".xls";
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c1 = Calendar.getInstance();

            Calendar c2 = Calendar.getInstance();
            c2.add(Calendar.MONTH, -1);
            
            String curDate = formatter.format(c1.getTime());
            String yestDate = formatter.format(c2.getTime());
            System.out.println("curDate--------" + curDate);
            System.out.println("yestMonthCurDate--------" + yestDate);

            reportTO.setFromDate(yestDate); 
            reportTO.setToDate(curDate);
            
            fileName = "vehicleUtilReport_(" + yestDate + ") TO (" + curDate + ")" + ".xls";
            String filepath = ThrottleConstants.vehicleUtilReportPath + fileName;
            System.out.println("filename = " + fileName);

            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "VehicleUtilisationReport";

            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);
            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            HSSFRow header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            HSSFCell cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("SNo");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            HSSFCell cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("Vehicle No");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            HSSFCell cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("Vehicle Type");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("Fleet Center");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            HSSFCell cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("Local Trips");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 4000);
            
            HSSFCell cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("Moff Trips");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 4000);
            
            HSSFCell cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("Total Trips	");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);
            
            HSSFCell cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("Total Utilised Days");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 5000);

            HSSFCell cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("Total KM Run");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 7000);

            HSSFCell cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Billed Km");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 4000);

            HSSFCell cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("Reefer Hrs Run");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 4000);

            HSSFCell cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Vehicle Utilization(%)");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 5000);

            ReportTO reportToOne = null;
            int cntrOne = 1;
            int countOne = 1;
            double percent = 0.00;
            int userId = 1;
            boolean checkOne = false;
            ArrayList getVehicleUtilizationList = new ArrayList();
            getVehicleUtilizationList = reportDAO.getVehicleUtilizationList(reportTO,userId);
            Iterator itrOne = getVehicleUtilizationList.iterator();
            System.out.println("getVehicleUtilizationList............." + getVehicleUtilizationList.size());
            while (itrOne.hasNext()) {
                reportToOne = (ReportTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);
//                if (reportToOne.getTwentyFtCount() != null) {
                    percent = reportToOne.getUtilDays() * 100 / Double.parseDouble(reportToOne.getTotDays());
                    row1.createCell((short) 0).setCellValue(cntrOne);
                    row1.createCell((short) 1).setCellValue(reportToOne.getRegNo());
                    row1.createCell((short) 2).setCellValue(reportToOne.getVehicleType());
                    row1.createCell((short) 3).setCellValue(reportToOne.getCompanyName());
                    row1.createCell((short) 4).setCellValue(reportToOne.getNoOfTripsLocal());
                    row1.createCell((short) 5).setCellValue(reportToOne.getNoOfTripsMoff());
                    row1.createCell((short) 6).setCellValue(reportToOne.getNoOfTripsLocal() + reportToOne.getNoOfTripsMoff());
                    row1.createCell((short) 7).setCellValue(reportToOne.getUtilDays());
                    row1.createCell((short) 8).setCellValue(reportToOne.getRunKm());
                    row1.createCell((short) 9).setCellValue(reportToOne.getBillableKM());
                    row1.createCell((short) 10).setCellValue(reportToOne.getRunHm());
                    DecimalFormat dfm = new DecimalFormat("#.##");
                    row1.createCell((short) 11).setCellValue(Double.valueOf(dfm.format(percent)));

                    cntrOne++;
//                }
            }

            System.out.println("Your Mail excel Sheet created");
            System.out.println("Your Mail excel Sheet filepath : " + filepath);

            try {
                FileOutputStream fileOut = new FileOutputStream(filepath);
                my_workbook.write(fileOut);
                fileOut.close();
                System.out.println("Mail Excel written successfully..");

                

                String emailFormat1 = "";
                String emailFormat2 = "";
                emailFormat1 = "<html>"
                        + "<body>"
                        + "<p><b>Dear Team, <br><br><b>GOOD MORNING"
                        + "<br><br>Please find the VehicleUtilisation Report from "+yestDate+" to "+curDate+". <br><br/>";
                emailFormat2 = "</body></html><br><br><br>"
                        + "<html>"
                        + "<body>"
                        + "<table>"
                        + "<tr>"
                        + "<br><br><br>Regards,"
                        + "<br><br/>CA Log<br/><br/>This is an Auto Generated Email Please Do Not Reply"
                        + " .</p>"
                        + "</tr>"
                        + "</table>"
                        + "</body></html>";
                emailFormat = emailFormat1 + "" + emailFormat2 + "";
                emailFormat = emailFormat + "~" + fileName + "~" + filepath;
                System.out.println("filepath " + filepath);
                System.out.println("emailFormat : 1  " + emailFormat);
                System.out.println("emailFormat : 2  " + emailFormat);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        }

        //  System.out.println("emailFormat : 3  "+emailFormat);
        return emailFormat;
    }
     
     public String getVehicleWeeklyProductivityAlerts() throws FPRuntimeException, FPBusinessException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();         
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        System.out.println("system date = " + systemTime);
        String emailFormat = "";
        String fromDate = "";
        String endDate = "";
        DecimalFormat df = new DecimalFormat("#.##");
        String name = "";
        String fileName = "";
        ReportTO reportTO = new ReportTO(); 
        VwpTO VwpTo = new VwpTO(); 
        
        try {

            File theDir = new File(ThrottleConstants.weeklyProdReportPath);
            if (!theDir.exists()) {
                boolean result = theDir.mkdir();
                if (result) {
                    System.out.println(ThrottleConstants.weeklyProdReportPath + " Created");
                }
            }

            //String customerName = reportTO1.getCustomerName();
            //String name = customerName + "_(" + startDate + ") TO (" + endDate + ")" + ".xls";
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c1 = Calendar.getInstance();

            Calendar c2 = Calendar.getInstance();
            c2.add(Calendar.MONTH, -1);

            String curDate = formatter.format(c1.getTime());
            String yestDate = formatter.format(c2.getTime());
            System.out.println("curDate--------" + curDate);
            System.out.println("yestMonthCurDate--------" + yestDate);

            reportTO.setFromDate(yestDate); 
            reportTO.setToDate(curDate);
            
            fileName = "WeeklyProdReport_(" + yestDate + ") TO (" + curDate + ")" + ".xls";
            String filepath = ThrottleConstants.weeklyProdReportPath + fileName;
            System.out.println("filename = " + fileName);

            HSSFWorkbook my_workbook = new HSSFWorkbook();
            String sheetName = "WeeklyProdReport";

            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);
            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
            style1.setBorderTop((short) 1);

            HSSFRow header1 = my_sheet1.createRow(0);
            header1.setHeightInPoints(20); // row hight

            HSSFCell cellOne1 = header1.createCell((short) 0);
            style1.setWrapText(true);
            cellOne1.setCellValue("Week");
            cellOne1.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 0, (short) 2000);

            HSSFCell cellOne2 = header1.createCell((short) 1);
            style1.setWrapText(true);
            cellOne2.setCellValue("WeekDateRange");
            cellOne2.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 1, (short) 6000);

            HSSFCell cellOne3 = header1.createCell((short) 2);
            cellOne3.setCellValue("NoOfDays");
            cellOne3.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 2, (short) 4000);

            HSSFCell cellOne4 = header1.createCell((short) 3);
            cellOne4.setCellValue("40' TripleAxle Mof");
            cellOne4.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 3, (short) 5000);

            HSSFCell cellOne5 = header1.createCell((short) 4);
            cellOne5.setCellValue("40' TripleAxle Loc");
            cellOne5.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 4, (short) 5000);

            HSSFCell cellOne6 = header1.createCell((short) 5);
            cellOne6.setCellValue("40' DoubleAxle Mof");
            cellOne6.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 5, (short) 7000);

            HSSFCell cellOne7 = header1.createCell((short) 6);
            cellOne7.setCellValue("40' DoubleAxle Loc");
            cellOne7.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 6, (short) 4000);

            HSSFCell cellOne8 = header1.createCell((short) 7);
            cellOne8.setCellValue("TotalMofussil");
            cellOne8.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 7, (short) 4000);

            HSSFCell cellOne9 = header1.createCell((short) 8);
            cellOne9.setCellValue("TotalLocal");
            cellOne9.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 8, (short) 4000);
            
            HSSFCell cellOne10 = header1.createCell((short) 9);
            cellOne10.setCellValue("Total Movements");
            cellOne10.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 9, (short) 5000);
            
            HSSFCell cellOne11 = header1.createCell((short) 10);
            cellOne11.setCellValue("TotalMoff Movements");
            cellOne11.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 10, (short) 5000);
            
            HSSFCell cellOne12 = header1.createCell((short) 11);
            cellOne12.setCellValue("Ave.Per.Vehicle");
            cellOne12.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 11, (short) 4000);
            
            HSSFCell cellOne13 = header1.createCell((short) 12);
            cellOne13.setCellValue("Ave.Pro.Per Vehicle/Month");
            cellOne13.setCellStyle(style1);
            my_sheet1.setColumnWidth((short) 12, (short) 5000);

            VwpTO reportToOne = null;
            int cntrOne = 1;
            int totDays = 0;
            int mof403 = 0;
            int loc403 = 0;
            int mof402 = 0;
            int loc402 = 0;
            int totMof = 0;
            int totLoc = 0;
            int nettMof = 0;
            int nettTotal = 0;
            double nettAve1 = 0.00;
            double nettAve2 = 0.00;
            double percent = 0.00;
            int userId = 1;
            boolean checkOne = false;
            ArrayList getVehicleWeeklyProductivityReport = new ArrayList();
            getVehicleWeeklyProductivityReport = reportDAO.getVehicleWeeklyProductivityReport(reportTO,userId);
            Iterator itrOne = getVehicleWeeklyProductivityReport.iterator();
            System.out.println("getVehicleWeeklyProductivityReport............." + getVehicleWeeklyProductivityReport.size());
            while (itrOne.hasNext()) {
                reportToOne = (VwpTO) itrOne.next();
                checkOne = true;
                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                row1 = my_sheet1.createRow((short) cntrOne);
//                if (reportToOne.getTwentyFtCount() != null) {
                    totDays = totDays + Integer.parseInt(reportToOne.getNoOfDays());
                    mof403 = mof403 + Integer.parseInt(reportToOne.getFortyTripleAxleMof());
                    loc403 = loc403 + Integer.parseInt(reportToOne.getFortyTripleAxleLoc());
                    mof402 = mof402 + Integer.parseInt(reportToOne.getFortyDoubleAxleMof());
                    loc402 = loc402 + Integer.parseInt(reportToOne.getFortyDoubleAxleLoc());
                    totMof = totMof + Integer.parseInt(reportToOne.getTotalMof());
                    totLoc = totLoc + Integer.parseInt(reportToOne.getTotalLoc());
                    nettTotal = nettTotal + Integer.parseInt(reportToOne.getNettTotal());
                    nettMof = nettMof + Integer.parseInt(reportToOne.getNettMof());
                    nettAve1 = nettAve1 + Double.parseDouble(reportToOne.getAvePerVehicleWeek());
                    nettAve2 = nettAve2 + Double.parseDouble(reportToOne.getAvePerVehicleMonth());
                    row1.createCell((short) 0).setCellValue(reportToOne.getWeekName());
                    row1.createCell((short) 1).setCellValue(reportToOne.getDateRange());
                    row1.createCell((short) 2).setCellValue(reportToOne.getNoOfDays());
                    row1.createCell((short) 3).setCellValue(reportToOne.getFortyTripleAxleMof());
                    row1.createCell((short) 4).setCellValue(reportToOne.getFortyTripleAxleLoc());
                    row1.createCell((short) 5).setCellValue(reportToOne.getFortyDoubleAxleMof());
                    row1.createCell((short) 6).setCellValue(reportToOne.getFortyDoubleAxleLoc());
                    row1.createCell((short) 7).setCellValue(reportToOne.getTotalMof());
                    row1.createCell((short) 8).setCellValue(reportToOne.getTotalLoc());
                    row1.createCell((short) 9).setCellValue(reportToOne.getNettTotal());
                    row1.createCell((short) 10).setCellValue(reportToOne.getNettMof());
                    row1.createCell((short) 11).setCellValue(reportToOne.getAvePerVehicleWeek());
                    row1.createCell((short) 12).setCellValue(reportToOne.getAvePerVehicleMonth());

                    cntrOne++;
//                }
            }
            
            HSSFRow row2 = my_sheet1.createRow(cntrOne);
            row2 = my_sheet1.createRow((short) cntrOne);
            row2.createCell((short) 0).setCellValue("");
            row2.createCell((short) 1).setCellValue("Total");
            row2.createCell((short) 2).setCellValue(totDays);
            row2.createCell((short) 3).setCellValue(mof403);
            row2.createCell((short) 4).setCellValue(loc403);
            row2.createCell((short) 5).setCellValue(mof402);
            row2.createCell((short) 6).setCellValue(loc402);
            row2.createCell((short) 7).setCellValue(totMof);
            row2.createCell((short) 8).setCellValue(totLoc);
            row2.createCell((short) 9).setCellValue(totMof+totLoc);
            row2.createCell((short) 10).setCellValue(nettMof);
            row2.createCell((short) 11).setCellValue(nettAve1);
            row2.createCell((short) 12).setCellValue(nettAve2);

            System.out.println("Your Mail excel Sheet created");
            System.out.println("Your Mail excel Sheet filepath : " + filepath);

            try {
                FileOutputStream fileOut = new FileOutputStream(filepath);
                my_workbook.write(fileOut);
                fileOut.close();
                System.out.println("Mail Excel written successfully..");

                String emailFormat1 = "";
                String emailFormat2 = "";
                emailFormat1 = "<html>"
                        + "<body>"
                        + "<p><b>Dear Team, <br><br><b>GOOD MORNING"
                        + "<br><br>Please find the Weekly Productivity Report from "+yestDate+" to "+curDate+". <br><br/>";
                emailFormat2 = "</body></html><br><br><br>"
                        + "<html>"
                        + "<body>"
                        + "<table>"
                        + "<tr>"
                        + "<br><br><br>Regards,"
                        + "<br><br/>CA Log<br/><br/>This is an Auto Generated Email Please Do Not Reply"
                        + " .</p>"
                        + "</tr>"
                        + "</table>"
                        + "</body></html>";
                emailFormat = emailFormat1 + "" + emailFormat2 + "";
                emailFormat = emailFormat + "~" + fileName + "~" + filepath;
                System.out.println("filepath " + filepath);
                System.out.println("emailFormat : 1  " + emailFormat);
                System.out.println("emailFormat : 2  " + emailFormat);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            System.out.println("Error in FileWriter !!!");
            e.printStackTrace();
        }

        //  System.out.println("emailFormat : 3  "+emailFormat);
        return emailFormat;
    }
     
     public ArrayList getDriverStatusCount(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDriverStatusCount = new ArrayList();
        getDriverStatusCount = reportDAO.getDriverStatusCount(reportTO);
        return getDriverStatusCount;
    }
     public ArrayList getOndutyDrivers(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getOndutyDrivers = new ArrayList();
        getOndutyDrivers = reportDAO.getOndutyDrivers(reportTO);
        return getOndutyDrivers;
    }
     public ArrayList getRestDrivers(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getRestDrivers = new ArrayList();
        getRestDrivers = reportDAO.getRestDrivers(reportTO);
        return getRestDrivers;
    }
     public ArrayList getDLRenewalDrivers(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getDLRenewalDrivers = new ArrayList();
        getDLRenewalDrivers = reportDAO.getDLRenewalDrivers(reportTO);
        return getDLRenewalDrivers;
    }
     public ArrayList getReleivedDrivers(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getReleivedDrivers = new ArrayList();
        getReleivedDrivers = reportDAO.getReleivedDrivers(reportTO);
        return getReleivedDrivers;
    }
     public ArrayList getNoLoadDrivers(ReportTO reportTO) throws FPRuntimeException, FPBusinessException {
        ArrayList getNoLoadDrivers = new ArrayList();
        getNoLoadDrivers = reportDAO.getNoLoadDrivers(reportTO);
        return getNoLoadDrivers;
    }
}
