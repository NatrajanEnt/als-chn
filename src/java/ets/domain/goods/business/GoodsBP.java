package ets.domain.goods.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.goods.data.GoodsDAO;
 
import java.util.ArrayList;
import java.util.Iterator;

public class GoodsBP {
     public GoodsBP() {
    }
    GoodsDAO goodsDAO;

    public GoodsDAO getGoodsDAO() {
        return goodsDAO;
    }

    public void setGoodsDAO(GoodsDAO goodsDAO) {
        this.goodsDAO = goodsDAO;
    }
 //goodsList via search
      /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises -p
     */
    public ArrayList processGoodsList(GoodsTO goodsTO) throws FPRuntimeException, FPBusinessException {
        
        ArrayList searchGoodsList = new ArrayList();
        //////System.out.println("via search");
       searchGoodsList = goodsDAO.getGoodsList(goodsTO);
          
        return  searchGoodsList;
    }
    //goodsList
      /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises -p
     */
    public ArrayList processGoodsList() throws FPRuntimeException, FPBusinessException {
        
        ArrayList goodsList = new ArrayList();
        //////System.out.println("i am in bp");
       goodsList = goodsDAO.getGoodsList();
          
        return  goodsList;
    }
    //processGdTypeList
    /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGdTypeList() throws FPRuntimeException, FPBusinessException {
        
        ArrayList outList = new ArrayList();
        //////System.out.println("i am in bp");
       outList = goodsDAO.getGdTypeList();
         
        return  outList;
    }
    
    //processInList
     /**
     * This method used to Get Vendor Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processInList() throws FPRuntimeException, FPBusinessException {
        
        ArrayList inList = new ArrayList();
        //////System.out.println("i am in bp");
       inList = goodsDAO.getGdInList();
         
        return  inList;
    }
    //processSaveGoods
     /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises -p
     */
    public int processSaveGoods(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        
        int insertStatus = 0;
        insertStatus = goodsDAO.doProcessSaveGoods(List, UserId);
          if (insertStatus  == 0) {
            throw new FPBusinessException("EM-GOODS-01");
        }
        return insertStatus;
    }
    
    // processSaveVehicleOutTime
     /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processSaveGoodsOut(ArrayList List, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = goodsDAO.doProcessGoodsOut(List, UserId);
         
         if (insertStatus  == 0) {
            throw new FPBusinessException("EM-GOODS-02");
        }
       return insertStatus;
    }
    
    //processInsertGoodsDetails
     /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int processInsertGoodsDetails(GoodsTO goodsTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = goodsDAO.doGoodsDetails(goodsTO, UserId);
        if (insertStatus == 0) {
            throw new FPBusinessException("EM-GOODS-03");
        }
        return insertStatus;
    }
}  
    

   

