<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
    <form name="tripSheet" method="post">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripSheet-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

        <c:if test="${tripDetails != null}">
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="70">
                        <th>Sno</th>
                        <th width="120">Trip Code</th>
                        <th width="240">Movement Type</th>
                        <th width="240">Trip Status</th>
                        <th width="120">Consignment No</th>
                        <th width="120">Vehicle No </th>
                        <th width="60">Run Km </th>
                        <th width="280">Fleet Center</th>
                        <th width="120">Customer Name </th>
                        <th width="120">Route </th>
                        <th width="120">Driver Name</th>
                        <th width="120">Vehicle Type </th>
                        <th width="120">Trip Scheduled</th>
                        <th width="120">Start Date Time</th>
                        <th width="120">Planned End Date Time</th>
                        <th width="120">End Date Time</th>
                        <th width="60">Estimated Revenue</th>
                        <th>Total Expense</th>
                        <th width="60">Trip Days</th>
                        <!--                        <th>Trip Created By</th>
                                                <th>Trip Ended By</th>
                                                <th>Trip Closed By</th>
                                                <th>Trip Settled By</th>-->

                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td  width="40" align="left"><%=index%></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.tripCode}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.movementType}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.status}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.cNotes}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.vehicleNo}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.tripRunKm}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.fleetCenterName}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.customerName}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.routeInfo}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.driverName}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.vehicleTypeName}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.plannedStartDateTime}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.actualStartDateTime}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.plannedEndDateTime}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.actualEndDateTime}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.orderRevenue}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.totalExpense}"/></td>
                            <td  width="40" align="left"><c:out value="${tripDetails.tripDays}"/></td>
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
