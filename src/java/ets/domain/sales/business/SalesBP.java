package ets.domain.sales.business;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.domain.sales.data.SalesDAO;
import ets.domain.sales.business.SalesTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.data.OperationDAO;
import ets.domain.util.ThrottleConstants;
import java.util.ArrayList;
import java.util.Iterator;

public class SalesBP {

    private SalesDAO salesDAO;
    private OperationDAO operationDAO;

    public OperationDAO getOperationDAO() {
        return operationDAO;
    }

    public void setOperationDAO(OperationDAO operationDAO) {
        this.operationDAO = operationDAO;
    }

    public SalesDAO getSalesDAO() {
        return salesDAO;
    }

    public void setSalesDAO(SalesDAO salesDAO) {
        this.salesDAO = salesDAO;
    }

    public int processPnrDatFile(SalesTO salesTO, String pnrNo, String portNo, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        SqlMapClient session = salesDAO.getSqlMapClient();
        try {
            session.startTransaction();
            status = salesDAO.processPnrDatFile(salesTO, userId, session);
            System.out.println("status:" + status);
            session.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
        }
        return status;
    }

    public int getPNRCount(String pnrNo) throws FPRuntimeException, FPBusinessException {
        int pnrCount = 0;
        pnrCount = salesDAO.getPNRCount(pnrNo);
        return pnrCount;
    }

    public int saveConsignmentNote(String pnrNo, String portNo, String reportDate, int userId) throws FPRuntimeException, FPBusinessException {
        int consignmentOrderId = 0;
        String orderReferenceNo = pnrNo;
        String billingPartyId = ThrottleConstants.pnrBillingPartyId;
        String consignorId = ThrottleConstants.pnrBillingPartyId;
        String consigneeId = ThrottleConstants.pnrBillingPartyId;
        String portRefNo = "portPnrLocationId" + portNo;
        String originId = ThrottleConstants.triwayCFSLocationId;

        String portId = "";
        String contractId = "";
        if ("1".equals(portNo)) {
            portId = ThrottleConstants.portPnrLocationId1;
        } else if ("2".equals(portNo)) {
            portId = ThrottleConstants.portPnrLocationId2;
        } else if ("3".equals(portNo)) {
            portId = ThrottleConstants.portPnrLocationId3;
        } else if ("4".equals(portNo)) {
            portId = ThrottleConstants.portPnrLocationId4;
        }
        String destinationId = ThrottleConstants.triwayCFSLocationId;

        String twentyFeetContainerQty = "0";
        String twentyFeetContainerNos = "";
        String twentyFeetContainerRate = "";
        String fortyFeetContainerQty = "0";
        String fortyFeetContainerNos = "";
        String fortyFeetContainerRate = "";
        int totalContainers = 0;
        String vesselName = "";
        String portName = "";
        String pnrDate = "";
        float totalFreightRates = 0.00F;
        boolean twentyFeetContractExistStatus = false;
        boolean fortyFeetContractExistStatus = false;
        boolean executionStatus = false;

        //take contract status
        ArrayList contractDetails = salesDAO.getContractDetails(billingPartyId, originId, portId, destinationId);
        Iterator itr = contractDetails.iterator();
        SalesTO salesTO = null;
        SalesTO consignmentNoteTO = new SalesTO();

        while (itr.hasNext()) {
            salesTO = new SalesTO();
            salesTO = (SalesTO) itr.next();
            contractId = salesTO.getContractId();
            if (ThrottleConstants.TFVehicleTypeId.equals(salesTO.getVehicleTypeId())) {
                twentyFeetContainerRate = salesTO.getRateWithoutReefer();
                twentyFeetContractExistStatus = true;
            }
            if (ThrottleConstants.FFVehicleTypeId.equals(salesTO.getVehicleTypeId())) {
                fortyFeetContainerRate = salesTO.getRateWithoutReefer();
                fortyFeetContractExistStatus = true;
            }
            consignmentNoteTO.setTotalKM(salesTO.getTotalKM());
            consignmentNoteTO.setTotalHours(salesTO.getTotalHours());
            consignmentNoteTO.setTotalMinutes(salesTO.getTotalMinutes());
        }
        ArrayList pnrDetails = salesDAO.getPNRData(pnrNo);
        itr = pnrDetails.iterator();
        salesTO = null;
        while (itr.hasNext()) {
            salesTO = new SalesTO();
            salesTO = (SalesTO) itr.next();
            vesselName = salesTO.getLinerName();
            portName = salesTO.getPortName();
            pnrDate = salesTO.getCreated();
            if (ThrottleConstants.pnrTFContainerType.equals(salesTO.getContainerType())) {
                twentyFeetContainerNos = salesTO.getContainerNos();
                twentyFeetContainerQty = salesTO.getContainerQty();
            }
            if (ThrottleConstants.pnrFFContainerType.equals(salesTO.getContainerType())) {
                fortyFeetContainerNos = salesTO.getContainerNos();
                fortyFeetContainerQty = salesTO.getContainerQty();
            }
        }

        totalContainers = Integer.parseInt(twentyFeetContainerQty) + Integer.parseInt(fortyFeetContainerQty);
        if (twentyFeetContainerRate != null && !"".equals(twentyFeetContainerRate)
                && twentyFeetContainerQty != null && !"".equals(twentyFeetContainerQty)) {
            totalFreightRates = Integer.parseInt(twentyFeetContainerQty) * Float.parseFloat(twentyFeetContainerRate);
        }
        if (fortyFeetContainerRate != null && !"".equals(fortyFeetContainerRate)
                && fortyFeetContainerQty != null && !"".equals(fortyFeetContainerQty)) {
            totalFreightRates = totalFreightRates + Integer.parseInt(fortyFeetContainerQty) * Float.parseFloat(fortyFeetContainerRate);
        }

        if (twentyFeetContainerQty != null && !"".equals(twentyFeetContainerQty)
                && Integer.parseInt(twentyFeetContainerQty) > 0) {
            if (twentyFeetContractExistStatus) {
                executionStatus = true;
            }
        }
        System.out.println("executionStatus1:" + executionStatus);
        if (fortyFeetContainerQty != null && !"".equals(fortyFeetContainerQty)
                && Integer.parseInt(fortyFeetContainerQty) > 0) {
            if (fortyFeetContractExistStatus) {
                executionStatus = true;
            }
        }
        System.out.println("executionStatus2:" + executionStatus);
        System.out.println("twentyFeetContainerQty:" + twentyFeetContainerQty);
        System.out.println("fortyFeetContainerQty:" + fortyFeetContainerQty);
        System.out.println("twentyFeetContractExistStatus:" + twentyFeetContractExistStatus);
        System.out.println("fortyFeetContractExistStatus:" + fortyFeetContractExistStatus);
        if (executionStatus) {
            //fetch customer details
            ArrayList customerDetails = salesDAO.getCustomerDetails(billingPartyId);
            itr = customerDetails.iterator();
            salesTO = null;
            while (itr.hasNext()) {
                salesTO = new SalesTO();
                salesTO = (SalesTO) itr.next();
                consignmentNoteTO.setCustomerName(salesTO.getCustomerName());
                consignmentNoteTO.setCustomerAddress(salesTO.getCustomerAddress());
                consignmentNoteTO.setMobile(salesTO.getMobile());
                consignmentNoteTO.setPincode(salesTO.getPincode());
                consignmentNoteTO.setPhone(salesTO.getPhone());
                consignmentNoteTO.setEmail(salesTO.getEmail());
            }
            //fetch consignor details        
            ArrayList consignorDetails = salesDAO.getCustomerDetails(consignorId);
            itr = consignorDetails.iterator();
            salesTO = null;
            while (itr.hasNext()) {
                salesTO = new SalesTO();
                salesTO = (SalesTO) itr.next();
                consignmentNoteTO.setConsignorName(salesTO.getCustomerName());
                consignmentNoteTO.setConsignorAddress(salesTO.getCustomerAddress());
                consignmentNoteTO.setConsignorMobile(salesTO.getMobile());
                consignmentNoteTO.setConsignorPincode(salesTO.getPincode());
                consignmentNoteTO.setConsignorPhone(salesTO.getPhone());
                consignmentNoteTO.setConsignorEmail(salesTO.getEmail());
            }
            //fetch consignee details        
            ArrayList consigneeDetails = salesDAO.getCustomerDetails(consignorId);
            itr = consigneeDetails.iterator();
            salesTO = null;
            while (itr.hasNext()) {
                salesTO = new SalesTO();
                salesTO = (SalesTO) itr.next();
                consignmentNoteTO.setConsigneeName(salesTO.getCustomerName());
                consignmentNoteTO.setConsigneeAddress(salesTO.getCustomerAddress());
                consignmentNoteTO.setConsigneeMobile(salesTO.getMobile());
                consignmentNoteTO.setConsigneePincode(salesTO.getPincode());
                consignmentNoteTO.setConsigneePhone(salesTO.getPhone());
                consignmentNoteTO.setConsigneeEmail(salesTO.getEmail());
            }

            consignmentNoteTO.setCustomerId(billingPartyId);
            consignmentNoteTO.setOriginId(originId);
            consignmentNoteTO.setDestinationId(portId);
            consignmentNoteTO.setCustomerOrderRefNo(pnrNo);
            consignmentNoteTO.setRouteContractId(contractId);
            consignmentNoteTO.setFreightRate(totalFreightRates + "");
            consignmentNoteTO.setUserId(userId);
            consignmentNoteTO.setConsignorId(consignorId);
            consignmentNoteTO.setConsigneeId(consigneeId);
            consignmentNoteTO.setCreated(reportDate);
            System.out.println("pnrDates==" + reportDate);
            SqlMapClient session = salesDAO.getSqlMapClient();
            try {
                session.startTransaction();
                String cNoteCode = "CO/17-18/";
//                String cNoteCodeSequence = operationDAO.getCnoteCodeSequence(session);
//                cNoteCode = cNoteCode + cNoteCodeSequence;
                consignmentNoteTO.setConsignmentNoteNo(cNoteCode);

                consignmentOrderId = salesDAO.saveConsignmentNote(consignmentNoteTO, session);

                consignmentNoteTO.setConsignmentOrderId(String.valueOf(consignmentOrderId));
                if (consignmentOrderId != 0) {
                    itr = pnrDetails.iterator();
                    salesTO = null;
                    int containerInsertStatus = 0;
                    while (itr.hasNext()) {
                        salesTO = new SalesTO();
                        salesTO = (SalesTO) itr.next();
                        contractId = salesTO.getContractId();
                        if (ThrottleConstants.pnrTFContainerType.equals(salesTO.getContainerType())) {
                            twentyFeetContainerNos = salesTO.getContainerNos();
                            twentyFeetContainerQty = salesTO.getContainerQty();
                            containerInsertStatus = salesDAO.insertContainer(consignmentOrderId, ThrottleConstants.TFVehicleTypeId,
                                    1, salesTO.getContainerQty(), twentyFeetContainerRate,
                                    salesTO.getContainerNos(), salesTO.getLinerName(), userId, session);
                        }
                        if (ThrottleConstants.pnrFFContainerType.equals(salesTO.getContainerType())) {
                            fortyFeetContainerNos = salesTO.getContainerNos();
                            fortyFeetContainerQty = salesTO.getContainerQty();
                            containerInsertStatus = salesDAO.insertContainer(consignmentOrderId, ThrottleConstants.FFVehicleTypeId,
                                    2, salesTO.getContainerQty(), fortyFeetContainerRate,
                                    salesTO.getContainerNos(), salesTO.getLinerName(), userId, session);
                        }
                    }
//            int insertStatus1 = operationDAO.insertConsignmentArticle(operationTO, userId, session);            
                    System.out.println("containerInsertStatus = " + containerInsertStatus);
                    consignmentNoteTO.setOriginId(originId);
                    consignmentNoteTO.setPoint1Id(portId);
                    consignmentNoteTO.setDestinationId(destinationId);
                    int routeCourseDetails = salesDAO.insertCNoteRouteCourse(consignmentNoteTO, userId, session);

                    //start email
                    String mailTemplate = "";
                    mailTemplate = salesDAO.getMailTemplate("1", session);
                    mailTemplate = mailTemplate.replaceAll("PNRNO", pnrNo);
                    mailTemplate = mailTemplate.replaceAll("PNRDATE", pnrDate);
                    mailTemplate = mailTemplate.replaceAll("PORTNAME", portName);
                    int indx = vesselName.length();
                    if(vesselName.indexOf(",") > 0){
                            indx = vesselName.indexOf(",");
                    }
                    vesselName = vesselName.substring(0, indx);                    
                    mailTemplate = mailTemplate.replaceAll("VESSELNAME", vesselName);
                    mailTemplate = mailTemplate.replaceAll("TWENTYTOTAL", twentyFeetContainerQty + " container(s)");
                    mailTemplate = mailTemplate.replaceAll("FORTYTOTAL", fortyFeetContainerQty + " container(s)");
                    mailTemplate = mailTemplate.replaceAll("NETTTOTAL", totalContainers + " container(s)");

                    SalesTO salesTO2 = new SalesTO();
                    salesTO2.setMailTypeId("3");
                    salesTO2.setMailSubjectTo("Created. PNR " + pnrNo + " Vessel " + vesselName + " Port " + portName);
                    salesTO2.setMailSubjectCc("Created. PNR " + pnrNo + " Vessel " + vesselName + " Port " + portName);
                    salesTO2.setMailSubjectBcc("Created. PNR " + pnrNo + " Vessel " + vesselName + " Port " + portName);
                    salesTO2.setMailContentTo(mailTemplate);
                    salesTO2.setMailContentCc(mailTemplate);
                    salesTO2.setMailContentBcc(mailTemplate);

                    salesTO2.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                    salesTO2.setMailIdCc(ThrottleConstants.pnrUploadCcMailId);
                    salesTO2.setMailIdBcc(ThrottleConstants.pnrUploadBccMailId);

                    int insertStatus = salesDAO.insertMailDetails(salesTO2, userId, session);
                }
                //end email

                session.commitTransaction();
            } catch (Exception e) {
                consignmentOrderId = 0;
                System.out.println("am here.....11");
                int removeStatus = salesDAO.removePnrDatFile(pnrNo);
                System.out.println("am here.....22");
                e.printStackTrace();
            } finally {
                //System.out.println("am here 7");
                try {
                    session.endTransaction();
                } catch (Exception ex) {
                    ex.printStackTrace();
                    //System.out.println("am here 8" + ex.getMessage());
                } finally {
                    session.getSession().close();
                }
            }
        }

//        if (consignmentOrderId == 0) {
//            System.out.println("am here.....1");
//            int removeStatus = salesDAO.removePnrDatFile(pnrNo);
//            System.out.println("am here.....2");
//        }
        return consignmentOrderId;
    }

    public ArrayList getPNRprojection(SalesTO salesTO) throws FPRuntimeException, FPBusinessException {
        ArrayList PNRprojection = new ArrayList();
        PNRprojection = salesDAO.getPNRprojection(salesTO);
        return PNRprojection;
    }

    public ArrayList getVehicleAvailability(SalesTO salesTO) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleAvailability = new ArrayList();
        vehicleAvailability = salesDAO.getVehicleAvailability(salesTO);
        return vehicleAvailability;
    }

    public String getMailTemplate(String eventId, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        String mailTemplate = "";
        mailTemplate = salesDAO.getMailTemplate(eventId, session);
        return mailTemplate;
    }

    public int insertMailDetails(SalesTO salesTO, int userId, SqlMapClient session) {
        int insertMailDetails = 0;
        insertMailDetails = salesDAO.insertMailDetails(salesTO, userId, session);
        return insertMailDetails;
    }
    
    public String getDayOfWeek() throws FPRuntimeException, FPBusinessException {
        String dayOfWeek = "";
        dayOfWeek = salesDAO.getMailTemplate();
        return dayOfWeek;
    }

}
