
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateEmpCode {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
//        String url = "jdbc:mysql://203.124.105.244:3306/";
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "throttlebrattle";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "admin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement updateCustomerRank = null;
                PreparedStatement customerRank = null;
                statement = con.createStatement();

// updating records
                String sql = " select customer_Id from ts_customer_outstanding ";
                System.out.println("Updated successfully");
                updateCustomerRank = con.prepareStatement(sql);
                String sql1 = "select customer_Id from ts_customer_outstanding order by customer_Id";
                rs = statement.executeQuery(sql1);
                System.out.println("----------------------");
                int i=1;
                int updateStatus = 0;
                while (rs.next()) {
                    int customerId = rs.getInt(1);
                    System.out.println("customerId = " + customerId);

                    updateCustomerRank.setInt(2,customerId);
                    updateStatus = updateCustomerRank.executeUpdate();
                }

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
