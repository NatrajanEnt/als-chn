<%-- 
    Document   : SlabMaster
    Created on : Dec 10, 2015, 5:17:31 PM
    Author     : Gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<script type="text/javascript">
    function slabsubmit()
    {
//        alert("save")
        var errStr = "";
        var nameCheckStatus = $("#slabStatus").text();
        if (document.getElementById("slabName").value == "") {
            errStr = "Please enter Slab Name.\n";
            alert(errStr);
            document.getElementById("slabName").focus();
        }
        else if (nameCheckStatus != "") {
            errStr = "Slab Name Already Exists.\n";
            alert(errStr);
            document.getElementById("slabName").focus();
        }
        if (errStr == "") {
            document.slabMaster.action = "/throttle/saveSlabMaster.do";
            document.slabMaster.method = "post";
            document.slabMaster.submit();
        }
    }

    function setValues(sno, slabName,maxTonnage, slabId, status) {
        //alert ("set value")
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("slabName").value = slabName;
        document.getElementById("slabId").value = slabId;
        document.getElementById("status").value = status;
        document.getElementById("maxTonnage").value = maxTonnage;
    }

    function checkslabName() {
        

        var slabName = document.getElementById('slabName').value;

        var url = '/throttle/checkslabName.do?slabName=' + slabName;
        if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function() {
            processRequest();
        };
        httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#slabStatus").text('Please Check City Name  :' + val + ' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#slabStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Slab Master Brattle Food</title>
    </head>
    <body onload="">
        <form name="slabMaster"  method="POST">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="contenthead" colspan="4" >Slab Master</td>
                    </tr>
                    <tr>
                        <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="slabStatus" style="color: red"></label></td>
                    </tr>
                    <tr><input type="hidden" name="slabId" id="slabId" value=""/>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Slab Name</td>
                    <td class="text1"><input type="text" name="slabName" id="slabName" class="textbox" onchange="checkslabName();" autocomplete="off" maxlength="50"></td>

                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Max.Tonnage</td>
                    <td class="text1"><input type="text" name="maxTonnage" id="maxTonnage" class="textbox"  autocomplete="off" maxlength="50"></td>
                    </tr>

                    <tr>
                        <td class="text2">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                        <td class="text2">
                            <select  align="center" class="textbox" name="status" id="status" >
                                <option value='Y'>Active</option>
                                <option value='N' id="inActive" >In-Active</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="slabsubmit()">
                        </center>
                    </td>
                </tr>
                <br>
                <br>

                <h2 align="center">Slab List</h2>

                <table width="815" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="30">
                            <th><h3>S.No</h3></th>
                            <th><h3>Slab Name </h3></th>
                            <th><h3>Max.Tonnage </h3></th>
                            <th><h3>Status</h3></th>
                            <th><h3>Select</h3></th>
                        </tr>
                    </thead>

                    <% int sno = 0;%>
                    <c:if test = "${Slablist!= null}">
                        <c:forEach items="${Slablist}" var="slab">
                            <%
                                sno++;
                                String className = "text1";
                                if ((sno % 1) == 0) {
                                    className = "text1";
                                } else {
                                    className = "text2";
                                }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${slab.slabName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${slab.maxTonnage}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${slab.status}"/></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${slab.slabName}" />', '<c:out value="${slab.maxTonnage}" />','<c:out value="${slab.slabId}" />','<c:out value="${slab.status}" />');" /></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    </c:if>
                </table>
                <br>
                <br>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
