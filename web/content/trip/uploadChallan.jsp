<%--
    Document   : viewTripSheet
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">
        function mailTriggerPage(tripId) {
            window.open('/throttle/viewTripDetailsMail.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewCurrentLocation(vehicleRegNo) {
            var url = 'http://ivts.noviretechnologies.com/IVTS/reportEngServ.do?username=serviceuser&password=7C53C003126C10E1091C73F4F945FEB4&companyId=759&reportRef=ref_currentStatusMapServ&param0=759&param1=ref_currentStatusMapServ&param2=&param3=&param4=&param5=&param6=&param7=&param8=&param9=&param10=&param11=' + vehicleRegNo;
            //alert(url);
            window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

        function viewTripDetails(tripId, vehicleId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId + '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewVehicleDetails(vehicleId) {
            window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues() {
            if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
            }
            if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
            }
            if ('<%=request.getAttribute("toDate")%>' != 'null') {
                document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
            }
            if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
                document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
            }
            if ('<%=request.getAttribute("cityFromId")%>' != 'null') {
                document.getElementById('cityFromId').value = '<%=request.getAttribute("cityFromId")%>';
            }
            if ('<%=request.getAttribute("cityFrom")%>' != 'null') {
                document.getElementById('cityFrom').value = '<%=request.getAttribute("cityFrom")%>';
            }
            if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
                document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
            }
            if ('<%=request.getAttribute("zoneId")%>' != 'null') {
                document.getElementById('zoneId').value = '<%=request.getAttribute("zoneId")%>';
            }

            if ('<%=request.getAttribute("tripSheetId")%>' != 'null') {
                document.getElementById('tripSheetId').value = '<%=request.getAttribute("tripSheetId")%>';
            }
            if ('<%=request.getAttribute("customerId")%>' != 'null') {
                document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
            }
            if ('<%=request.getAttribute("tripStatusId")%>' != 'null') {
                document.getElementById('tripStatusId').value = '<%=request.getAttribute("tripStatusId")%>';
            }
            if ('<%=request.getAttribute("podStatus")%>' != 'null') {
                document.getElementById('podStatus').value = '<%=request.getAttribute("podStatus")%>';
            }
            if ('<%=request.getAttribute("statusId")%>' != 'null') {
                document.getElementById('stsId').value = '<%=request.getAttribute("statusId")%>';
            }
        }

        function submitPage() {
            document.tripSheet.action = '/throttle/viewTripSheetsForChallan.do';
            document.tripSheet.submit();
        }


    </script>

    <script type="text/javascript">
        var httpRequest;
        function getLocation() {
            var zoneid = document.getElementById("zoneId").value;
            if (zoneid != '') {

                // Use the .autocomplete() method to compile the list based on input from user
                $('#cityFrom').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getLocationName.do",
                            dataType: "json",
                            data: {
                                cityId: request.term,
                                zoneId: document.getElementById('zoneId').value
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityFromId').val(tmp[0]);
                        $('#cityFrom').val(tmp[1]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
                };

            }
        }


        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user

            $('#primaryDriver').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getPrimaryDriver.do",
                        dataType: "json",
                        data: {
                            driverName: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            var primaryDriver = $('#primaryDriver').val();
                            if (items == '' && primaryDriver != '') {
                                alert("Invalid Primary Driver Name");
                                $('#primaryDriver').val('');
                                $('#primaryDriverId').val('');
                                $('#primaryDriver').focus();
                            } else {
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var id = ui.item.Id;
                    $('#primaryDriver').val(value);
                    $('#primaryDriverId').val(id);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
        });


    </script>
    <style>
        .ui-autocomplete { cursor:pointer; height:120px; overflow-y:scroll }
    </style>

   

</head>
<body onload="sorter.size(20);
          setValues();">
    <form name="tripSheet" method="post">
        <%@ include file="/content/common/path.jsp" %>
        <%@include file="/content/common/message.jsp" %>
        <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                    </h2></td>
                <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
            </tr>
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:900;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1">View Expense Challan Details</li>
                        </ul>
                        <div id="first">
                            <table width="100%" cellpadding="0" cellspacing="5" border="0" align="center" class="table4">
                                <tr>
                                    <td width="80" >Driver Name</td>
                                    <td  width="80">
                                        <input name="primaryDriver" id="primaryDriver" class="textbox" value="<c:out value="${primaryDriver}"/>" />
                                        <input type="hidden" name="primaryDriverId" id="primaryDriverId" class="textbox" value="<c:out value="${primaryDriverId}"/>" /></td>
                                    <td  width="80" >Vehicle No</td>
                                    <td  width="80"> <select name="vehicleId" id="vehicleId" class="textbox" style="height:20px; width:122px;"" >
                                     <c:if test="${vehicleList != null}">
                                         <option value="" selected>--Select--</option>
                                         <c:forEach items="${vehicleList}" var="vehicleList">
                                             <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                         </c:forEach>
                                     </c:if>
                                        </select>
                                    </td> 
                                    <script>
                                        document.getElementById("vehicleId").value='<c:out value="${vehicleId}"/>';
                                    </script>

                                </tr>
                                <tr>

                                    <td width="80" ><font color="red">*</font>From Date</td>
                                    <td width="80"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="height:15px; width:120px;" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>

                                    <td width="80" ><font color="red">*</font>To Date</td>
                                    <td width="80"><input name="toDate" id="toDate" type="text" class="datepicker" style="height:15px; width:120px;" onClick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>


                                    <td width="80">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td width="80" >Bill Status </td>
                                    <td  width="80"> <select name="documentRequired" id="documentRequired" class="textbox" style="height:20px; width:122px;" >
                                            <option value="Y" >Bill Uploaded</option>
                                            <option value="N" >Bill Pending</option>
                                        </select></td>
                                <script>
                                            document.getElementById("documentRequired").value='<c:out value="${documentRequired}"/>';
                                </script>
                                <td>&nbsp; </td>
                                <td><input type="button"   value="Search" class="button" name="Search" onclick="submitPage();" > </td>
                                </tr>


                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <br>
        <br>
        <table border="1" bgcolor="#97caff">
            <tr>
                <td width="80"> &nbsp;</td>
                <td width="80">  Challan  </td>
                <td width="80"> Total  Amount</td>

            </tr>
            <tr>
                <td>Uploaded:</td>
                <td width="80">&nbsp;&nbsp;&nbsp;<c:out value="${uploadedChallan}"/></td>
                <td width="80"> <c:out value="${uploadedChallanAmount}"/></td>

            </tr>
            <tr>
                <td>Pending:</td>
                <td width="80">&nbsp;&nbsp;&nbsp;<c:out value="${pendingChallan}"/></td>
                <td width="80"> <c:out value="${pendingChallanAmount}"/></td>

            </tr>
            <tr>
                <td>Total:</td>
                <td width="80">&nbsp;&nbsp;&nbsp;<c:out value="${uploadedChallan+pendingChallan}"/></td>
                <td width="80"> <c:out value="${uploadedChallanAmount+pendingChallanAmount}"/></td>

            </tr>
        </table>
        <c:if test="${tripDetails != null}">
            <table align="center" border="0" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="50">
                        <th><h3>Sno</h3></th>
                        <th><h3>Vehicle No </h3></th>
                        <th><h3>Trip Code</h3></th>
                        <th><h3>Fleet Center with Contact</h3></th>
                        <th><h3>Route </h3></th>
                        <th><h3>Driver Name with Contact </h3></th>
                        <th><h3>Vehicle Type </h3></th>
                        <th><h3>Trip Status</h3></th>
                        <th><h3>No of Uploaded Expense</h3></th>
                        <th><h3>Uploaded Expense Amount</h3></th>
                        <th><h3>No of pending Expense</h3></th>
                        <th><h3>Pending Expense Amount</h3></th>
                        <th><h3> Total Challan</h3></th>
                        <th><h3> Total Expense</h3></th>
                        <th><h3>select</h3></th>

                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripDetails}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td class="<%=className%>" width="40" align="left">
                                <%=index%>
                            </td>



                            <td class="<%=className%>" width="120">
                                <a href="#" onclick="viewVehicleDetails('<c:out value="${tripDetails.vehicleId}"/>')"><c:out value="${tripDetails.vehicleNo}"/></a><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font></td>
                            <td class="<%=className%>" width="120" >
                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>');"><c:out value="${tripDetails.tripCode}"/></a>
                            </td>

                            <td class="<%=className%>" width="150"><c:out value="${tripDetails.fleetCenterName}"/><br/><c:out value="${tripDetails.fleetCenterNo}"/></td>



                            <td class="<%=className%>" >                                
                                <c:if test="${tripDetails.routeNameStatus > '2'}">
                                    <img src="images/Black_star1.png" alt="MultiPoint"  title=" MultiPoint"/><c:out value="${tripDetails.routeInfo}"/>
                                </c:if>
                                <c:if test="${tripDetails.routeNameStatus <= '2'}">
                                    <c:out value="${tripDetails.routeInfo}"/>
                                </c:if>
                            </td>

                            <td class="<%=className%>" ><c:out value="${tripDetails.driverName}"/><br/><c:out value="${tripDetails.mobileNo}"/><br><font color="red"><c:out value="${tripDetails.oldDriverName}"/></font></td>
                            <td class="<%=className%>" ><c:out value="${tripDetails.vehicleTypeName}"/></td>                



                            <td class="<%=className%>" >
                                <c:if test="${tripDetails.extraExpenseStatus == 0}">
                                    <c:out value="${tripDetails.status}"/>
                                </c:if>
                                <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                    <c:if test="${tripDetails.statusId == 14}">
                                        <c:out value="${tripDetails.status}"/>
                                    </c:if>
                                    <c:if test="${tripDetails.statusId != 14}">
                                        FC Closure
                                    </c:if>
                                </c:if>
                            </td>
                            <td class="<%=className%>" ><c:out value="${tripDetails.uploadedChallan}"/></td>                
                            <td class="<%=className%>" ><c:out value="${tripDetails.uploadedChallanAmount}"/></td>                
                            <td class="<%=className%>" ><c:out value="${tripDetails.pendingChallan}"/></td>                
                            <td class="<%=className%>" ><c:out value="${tripDetails.pendingChallanAmount}"/></td>                
                            <td class="<%=className%>" ><c:out value="${tripDetails.documentRequiredStatus}"/></td>                
                            <td class="<%=className%>" ><c:out value="${tripDetails.totalDocumentAmount}"/></td>                
                            <td class="<%=className%>" >
                                <a href="viewExpensePODDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>">upload Bill</a>
                            </td>                
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" >5</option>
                    <option value="10">10</option>
                    <option value="20" selected="selected">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
