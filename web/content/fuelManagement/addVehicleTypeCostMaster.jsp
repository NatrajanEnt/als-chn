<%-- 
    Document   : addVehicleTypeCostMaster
    Created on : Apr 17, 2015, 3:21:47 PM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>


        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




        <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
<html>     
      <title>Add VehicleType Cost Master</title>
    </head>
    <body>
        <form name="addVehicleTypeCost"  method="post" >
            <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
            <div id="fixme" style="overflow:auto; background-color:#FFFFFF; " >
                    <div align="center"  style="position:fixed; table-layout:fixed; background-color:#FFFFFF; width:875px; height:40px;">
                        <!-- pointer table -->
                        <table width="700" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                            <tr>
                                <td >
                                    <%@ include file="/content/common/path.jsp" %>
                                    <%@ include file="/content/common/message.jsp" %>
                                </td></tr></table>
                        <!-- pointer table -->

                    </div>
                </div>
                <br><br><br><br><br><br><br>
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="400" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30"><div class="contenthead">Vehicle Type Cost Master</div></td>
                </tr>
                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Select Country</td>
                    <td class="text2">
                        <select name="countryId" id="countryId" class='form-control'>
                            <option value="">--Country---</option>
                            <c:if test = "${countryList != null}" >
                                <c:forEach items="${countryList}" var="countryList">
                                <option value="<c:out value='${countryList.countryId}'/>"><c:out value='${countryList.countryName}'/></option>
                                </c:forEach>
                            </c:if>
                        </select>
                        <script>
                            var countryId='<c:out value='${countryId}'/>';
                            if(countryId != null & countryId != ""){
                            document.getElementById("countryId").value=countryId;
                            }
                        </script>
                        &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<input align="center" type="button" class="button" value="Go"  onClick="fetchData();"></td>
                </tr>                
            </table>
            <br><br>
            <input type="hidden" name="fetchStatus" id="fetchStatus" value="Y"/>

           <c:if test = "${fuelConfigVehicleTypeList != null}" >
            <table width="90%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>                        
                        <th><h3>VehicleType Name</h3></th>
                        <th><h3>Fuel cost/Km</h3></th>
                        <th><h3>Toll cost/Km</h3></th>
                        <th><h3>Misc Cost</h3></th>
                        <th><h3>Driver Incentive</h3></th>
                        <th><h3>Etc Cost</h3></th>
                    </tr>
                </thead>
                <tbody>
                   
                         <% int sno = 0;%>
                    <c:forEach items="${fuelConfigVehicleTypeList}" var="fuelConfigVehicleTypeList">
                        <%
                        sno++;
                        String className = "text1";
                        if ((sno % 1) == 0) {
                            className = "text1";
                        } else {
                            className = "text2";
                        }
                        %>
                        <tr height="30">
                        <td class="<%=className%>"  height="30"> <%= sno++%></td>
                        <td class="<%=className%>" align="left" >
                            <select name="vehicleTypeIds" id="vehicleTypeId<%=sno%>" class="form-control">
                                <option value='<c:out value="${fuelConfigVehicleTypeList.vehicleTypeId}"/>'><c:out value="${fuelConfigVehicleTypeList.vehicleTypeName}"/></option>
                            </select>
                                <c:if test = "${fuelConfigVehicleTypeList.vehicleTYpeCostId != null}" >
                                    <input type="hidden" name="vehicleTypeCostId" id="vehicleTypeCostId<%=sno%>"  value='<c:out value="${fuelConfigVehicleTypeList.vehicleTYpeCostId}"/>'/>
                                </c:if>
                        </td>
                        <td class="<%=className%>" align="left" >
                            <input type="text" name="costPerKm" id="costPerKm<%=sno%>" readonly value='<c:out value="${fuelConfigVehicleTypeList.fuelCostPerKm}"/>' class="form-control"/>
                        </td>
                        <td class="<%=className%>" align="left" >
                            <input type="text" name="tollCostPerKm" id="tollCostPerKm<%=sno%>"   onkeypress=" return onKeyPressBlockCharacters(event)" maxlength="10" value='<c:out value="${fuelConfigVehicleTypeList.tollCostPerKm}"/>'  class="form-control"/>
                        </td>
                        <td class="<%=className%>" align="left" >
                            <input type="text" name="miscAmt" id="miscAmt<%=sno%>"   onkeypress=" return onKeyPressBlockCharacters(event)" maxlength="10" class="form-control" value='<c:out value="${fuelConfigVehicleTypeList.miscCostPerKm}"/>' />
                        </td>
                        <td class="<%=className%>" align="left" >
                            <input type="text" name="driverIncentive" id="driverIncentive<%=sno%>"   onkeypress=" return onKeyPressBlockCharacters(event)" maxlength="10" class="form-control" value='<c:out value="${fuelConfigVehicleTypeList.driverIncentive}"/>'/>
                        </td>
                        <td class="<%=className%>" align="left" >
                            <input type="text" name="etcAmount" id="etcAmount<%=sno%>"   onkeypress=" return onKeyPressBlockCharacters(event)" maxlength="10" class="form-control" value='<c:out value="${fuelConfigVehicleTypeList.etcCost}"/>'/>                             

                        </td>                                                                     
                        </tr>
                    </c:forEach>
                </tbody>
                    </table>
                    
                    <center>
                    <input type="button" class="button"  name="ADD" value="ADD" onclick="savevehicleTypeCost()"  />
                    <br>
                    </center>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>

                    
                </c:if>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <script type="text/javascript">
            function fetchData(){
                      var countryId=document.getElementById("countryId").value;
                      alert(countryId);
                      if(countryId==""){
                          alert("Please Select Country");
                          document.getElementById("countryId").focus();
                          return;
                      }
                      document.addVehicleTypeCost.action = '/throttle/addvehicleTypeOperatingCost.do?countryId='+countryId;
                      document.addVehicleTypeCost.submit();
            }
            function savevehicleTypeCost(){
                var count=0;
                var vehicleTypeId=document.getElementsByName("vehicleTypeIds");
                var tollCostPerKm=document.getElementsByName("tollCostPerKm");
                var miscAmt=document.getElementsByName("miscAmt");
                var driverIncentive=document.getElementsByName("driverIncentive");
                if(vehicleTypeId.length>0){
                for(var i=0;i<vehicleTypeId.length;i++){
                if(tollCostPerKm[i].value==""){
                count=1;
                alert("Please Enter TollCost ");
                tollCostPerKm[i].focus();
                break;
                }else if(miscAmt[i].value==""){
                 count=1;
                alert("Please Enter Miscellaneous Amount " );
                miscAmt[i].focus();
                break;
                }else if(driverIncentive[i].value==""){
                count=1;
                alert("Please Enter Driver Incentive ");
                driverIncentive[i].focus();
                break;
                }                 
                   
                }
                if(parseInt(count)==parseInt(0)){
                document.addVehicleTypeCost.action = '/throttle/savevehicleTypeOperatingCost.do?countryId='+document.getElementById("countryId").value;
                document.addVehicleTypeCost.submit();
                }                    
                }
           }
        </script>
    </body>
</html>
