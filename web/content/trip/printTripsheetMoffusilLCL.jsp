<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : RAM
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <title>PrintTripSheet</title>
    </head>
    <body>
        <style type="text/css">
            @media print {
                #printbtn {
                    display :  none;
                }
            }
        </style>
        <style type="text/css" media="print">
            table { page-break-inside:avoid }
            @media print
            {
                @page {
                    margin-top: 0;
                    margin-bottom: 0;
                }
                .page-break  { display: block; page-break-before: always; }
            } 
        </style>
        <style>
        </style>

        <script>

//            window.onunload = function() {
//                window.opener.location.reload();
//            }
        </script>
        <form name="enter" method="post">
            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            String billingPartyId = (String)request.getAttribute("billingPartyId");
            String companyId = (String)request.getAttribute("companyId");
            %>
            <div id="printContent">

                <c:forEach items="${tripDetailsListLCL}" var="tripStatus">
                    <div class="page-break">
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    <table align="center"  style="border-collapse: collapse;width:400px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                           background:url(images/dict-logoWatermark.png);
                           background-repeat:no-repeat;
                           background-position:center;
                           border:1px solid;
                           /*border-color:#CD853F;width:800px;*/
                           border:1px solid;
                           /*opacity:0.8;*/
                           /*font-weight:bold;*/
                           ">
                        <tr>
                            <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                                <table align="left" >
                                    <tr>
                                        <% if ("2479".equals(billingPartyId)) {%>
                                        <% // if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                        <td align="left"><img src="images/triway_forward.jpg" width="165" height="85"/></td>
                                            <%}else {%>
                                        <td align="left"><img src="images/Route Logo.JPG" width="140" height="70"/></td>
                                            <%}%>

                                        <td style="padding-left: 180px; padding-right:42px;">
                                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;




                                    <center>
                                        <% if ("2479".equals(billingPartyId)) {%>
                                        <font face="verdana" size="4"><b>Triway Forwarder Pvt Ltd.</b></font></center><br>
                                        <%}else {%>
                                    <font face="verdana" size="4"><b>Route Logistics India Pvt.Ltd.</b></font></center>
                                    <%}%>

                                    <center>
                                        <% if ("2479".equals(billingPartyId) && !"1464".equals(companyId) ) {%>
                                        <font face="verdana" size="3" >14,Jaffer Street, Chennai,Tamilnadu.GSTIN:33AAACT2874J2Z8
                                        </font><br><br>
                                        <%}else if ("1464".equals(companyId)) {%>
                                        <font face="verdana" size="3" >Industrial Subrub, Yeshwanthpur, Bangalore 560022  </font>
                                        <%}else {%>
                                        <font face="verdana" size="3" >14,Jaffer Street, Chennai,Tamilnadu.GSTIN:33AACCR6979E1Z6
                                        </font><br><br>
                                        <%}%>
                                    </center>
                            </td>
                            <td align="right">
                                <br>
                                <div id="externalbox" style="width:3in">
                                    <div id="inputdata" ></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    </td>
                    </tr>

                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <tbody>
                                <tr>
                                    <td valign="top" width="55%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Consignor<br></b></font><br> <font face="verdana" size="3"> <c:out value="${tripStatus.consignorName}"/>,<br><font face="verdana" weight="50" ><c:out value='${tripStatus.consignorAddress}'/>,</font ><br>GST IN :<c:out value="${tripStatus.consignorGST}" /></font>  <br>
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                    <td width="45%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <font face="verdana" size="2">

                                        <br>
                                        <font face="verdana" size="2">&nbsp;&nbsp;<b>LR NO :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</b></font><font size="3"> 
                                        <% if ("1464".equals(companyId)) {%>
                                            <c:out value="${tripStatus.manualLrNumber}"/><br>
                                        <% }else{%>
                                            MAA IM / 
                                        <c:if test="${tripId <= 289}">
                                            <c:out value="${tripStatus.consignmentId}"/>
                                        </c:if>
                                        <c:if test="${tripId >= 289}">
                                            <c:out value="${tripStatus.lrNumber}"/>
                                        </c:if>
                                        / 2018-19&nbsp; </font><br>
                                         <% }%>

                                        <!--                                                <c:if test="${orderType == 'Moffusil-Export-FCL'}">
                                                                                          EX 
                                        </c:if>
                                        <c:if test="${orderType == 'Moffusil-Import-FCL'|| 'Moffusil-Import-LCL'}">
                                          IM 
                                        </c:if>
                                        <c:if test="${orderType == 'Moffusil-ICD'}">
                                          IC
                                        </c:if> 
                                        /<c:out value="${tripStatus.lrNumber}"/> /2017-18&nbsp; </font><br>-->
                                        <font face="verdana" size="2">
                                        <font face="verdana" size="2"><br>
                                        <font face="verdana" size="2">&nbsp;&nbsp;<b>LR Date : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</b><font face="verdana" size="3"><c:out value="${preStartDate}"/>&nbsp; </font><br>
                                        <font size="2">
                                        <br>
                                        &nbsp;&nbsp;<b>Booking No :</b></font><font face="verdana" size="3">&nbsp;&nbsp;&nbsp; <b>&nbsp;</b><c:out value="${tripStatus.tripCode}"/></font>
                                        <font size="2"><br>
                                        <br>
                                        &nbsp;&nbsp;<b>Job No :</b></font><font face="verdana" size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;</b><c:out value="${tripStatus.customerOrderReferenceNo}"/></font>
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>  <font face="verdana" size="3"><b>&nbsp;Consignee</b></font>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<br>
                                        <br><font face="verdana" size="3"><c:out value="${tripStatus.consigneeName}"/>,<br><c:out value="${tripStatus.consigneeAddress}"/>,<br>GST IN :<c:out value="${tripStatus.consigneeGST}"/></font><br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">


                                        <!--<br><font face="verdana" size="2">&emsp;<b>Form KK :</b></font>&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;</b><font face="verdana" size="3"></font><br><br>-->
                                        <font face="verdana" size="2">&emsp;<b>Value Declared :  </b></font>&emsp;&emsp;&nbsp;<font face="verdana" size="3"><b>&nbsp;</b>As Per Bill</font><br><br>

                                        <font face="verdana" size="2">&emsp;<b>No of Packages :</b></font><font face="verdana" size="3">&emsp;&emsp;<b>&nbsp;</b><c:out value="${tripStatus.packageNos}"/> 
                                        <c:if test="${ tripStatus.uom == '1'}">Box.</c:if>
                                        <c:if test="${ tripStatus.uom == '2'}">Bag.</c:if>
                                        <c:if test="${ tripStatus.uom == '3'}">Pallet.</c:if>
                                        <c:if test="${ tripStatus.uom == '4'}">Each.</c:if>
                                        <c:if test="${ tripStatus.uom == '5'}">Packages.</c:if>
                                        </font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Volume :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b><c:out value="${tripStatus.volumeDocNo}"/> CBM.</font><br><br>

                                        <font face="verdana" size="2">&emsp;<b>Gross Weight : </b></font><font face="verdana" size="3">&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b><c:out value="${tripStatus.weight}"/> kgs.&emsp;&emsp;</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>E-Way Bill No :</b></font><font face="verdana" size="3">&emsp;&emsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;</b><c:out value="${tripStatus.ewayBillNo}"/></font><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="3"><b> &nbsp;Delivery At</b></font><br>
                                        <c:if test="${tripStatus.movementTypeId == '13'|| tripStatus.movementTypeId == '14' || tripStatus.movementTypeId == '15'  }">
                                            <br><font face="verdana" size="3"><c:out value="${tripStatus.consigneeName}"/>,<br><c:out value="${tripStatus.consigneeAddress}"/>,<br>GST IN :<c:out value="${tripStatus.consigneeGST}"/>.</font><br>
                                        </c:if>
                                        <c:if test="${ tripStatus.movementTypeId == '16'}">
                                            <br><font face="verdana" size="3"><c:out value="${tripStatus.deliveryAddress}"/>.</font><br>
                                        </c:if>
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">

                                        <% if ("2479".equals(billingPartyId)) {%>
                                        <br><font face="verdana" size="2">&emsp;<b>Contact Person :</b></font><font face="verdana" size="3">&emsp;&emsp;<b></b>Mr.Sanjay</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Mobile :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b>+91 9945536141</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Email :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;<b>&nbsp;</b>sanjay@routelogistics.net </font><br><br>
                                        <%}else if("1464".equals(companyId)) {%>
                                        <br><font face="verdana" size="2">&emsp;<b>Contact Person :</b></font><font face="verdana" size="3">&emsp;&emsp;<b></b>Mr.Sanjay</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Mobile :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b>+91 9945536141</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Email :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;<b>&nbsp;</b>sanjay@routelogistics.net </font><br><br>
                                        <%}else {%>
                                        <br><font face="verdana" size="2">&emsp;<b>Contact Person :</b></font><font face="verdana" size="3">&emsp;&emsp;<b></b>Mr.Saran</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Mobile :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b>+91 89398 33084 /85 /45</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Email :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;<b>&nbsp;</b> </font><br><br>
                                        <%}%>
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br> <font face="verdana" size="3"><b>&nbsp;Booking Office</b></font><br>
                                        <% if ("2479".equals(billingPartyId)) {%>
                                        <% // if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                        <font face="verdana" size="2"><br>&nbsp;&nbsp;&nbsp;&nbsp;CHENNAI &emsp;&emsp; TRIWAY FORWARDERS (I) P LTD</font><br>
                                        <% }else if ("1464".equals(companyId)) {%> 
                                        <font face="verdana" size="2"><br>&nbsp;&nbsp;&nbsp;&nbsp;BANGALORE &emsp;&emsp; ROUTE LOGISTICS (I) P LTD</font><br>
                                        <%}else {%>
                                        <font face="verdana" size="2"><br>&nbsp;&nbsp;&nbsp;&nbsp;CHENNAI &emsp;&emsp; ROUTE LOGISTICS (I) P LTD</font><br>
                                        <%}%> 
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="2">&emsp;<b>Containers :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;<b>&nbsp;</b>&nbsp;<c:out value='${containerNo}'/>&emsp;<c:out value='${containerNo1}'/></font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Driver Details :</b></font><font face="verdana" size="3">&emsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;</b><c:out value='${driverName}'/> <br>&nbsp; &nbsp;+91 <c:out value='${driverMobile}'/> </font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Truck No :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;</b><c:out value='${vehicleNo}'/>&nbsp;&nbsp;(<c:out value='${vehicleType}'/>)</font><br>
                                        <br><font face="verdana" size="2">&emsp;<b>Route :&emsp; </b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;</b><c:out value='${tripStatus.from}'/>&nbsp; To &nbsp; <c:out value='${tripStatus.to}'/></font><br><br>
                                        <!--<br><font size="2">&emsp;<b>Route &emsp; :</b></font><font size="2">&emsp;<c:out value='${routeInfo}'/></font><br><br>-->
                                        <font face="verdana" size="2">&emsp;<b>Terms </b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;</b> Collect</font>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<br>
                                        <br> <font face="verdana" size="2">&emsp;<b>Type of Delivery :</b></font><font face="verdana" size="3">&nbsp;<b>&nbsp;&nbsp;</b> 
                                          <% if ("1464".equals(companyId)) {%> 
                                          Urgent
                                          <%}else{%>
                                          Normal
                                          <% }%> 
                                          </font><br><br>

                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <font face="verdana" size="3"><br>&nbsp;&nbsp;<b>List of Document</b></font><br><font face="verdana" size="3"><br>&nbsp;&nbsp;Invoice,&emsp; Packing List,&emsp; Bill of Entry.</font><br>
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="2">&emsp; <b>Remarks<br></b></font><font face="verdana" size="3">&emsp;<c:out value='${tripStatus.vehicleRemarks}'/></font><br>
                                        <br><br><br><br><br>
                                    </td>
                                </tr>

                                <tr>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="3">&nbsp;&nbsp;<b>Owners Risk</b></font><br><font face="verdana" size="3">
                                        &nbsp;&nbsp;Goods to be insured by party at Owners Risk.<br>
                                        &nbsp;&nbsp;Not Responsible for Breakage,Damages,etc.<br>
                                        &nbsp;&nbsp;Subject to Chennai Jurisdiction Only.<br>
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <% if ("1464".equals(companyId)) {%> 
                                        <font face="verdana" size="3">&emsp;<b>Prepared By : </b></font><font face="verdana" size="2"><b>ROUTE LOGISTICS (I)P LTD BANGALORE</b></font><br>
                                        <font face="verdana" size="3"><br>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;bangalore@routelogistics.net</font><br><br>
                                        <%}else{%>
                                        <font face="verdana" size="3">&emsp;<b>Prepared By : </b></font><font face="verdana" size="2"><b>ROUTE LOGISTICS (I)P LTD CHENNAI</b></font><br>
                                        <font face="verdana" size="3"><br>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;chennai@routelogistics.net</font><br><br>
                                        <%}%>
                                        <font face="verdana" size="2">&emsp;<b>Prepared On :</b></font><font face="verdana" size="3">&emsp;<c:out value="${grDate}"/></font><br>

                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse"style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="3">&nbsp;&nbsp;<b>Description :</b></font>
                                        <% if ("1464".equals(companyId)) {%> 
                                           <br> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <font face="verdana" size="3"><c:out value="${tripStatus.description}"/>  <br> </font>
                                       <%}else{%>
                                        <br><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${orderType}"/><br>
                                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;BOE NO :&emsp;<c:out value="${tripStatus.billOfEntry}"/><br>
                                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;BOE DATE :&emsp;<c:out value="${tripStatus.challanDate}"/>
                                        <%}%>
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </table>
                                       
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>                                                            
                        <br>                                                            
                        <br>                                                            
                        <br>   </div>                                                          
                        <!--Muthu....-:) -->
                </div>
            </c:forEach>
            <br>
            <center>
                <input type="button" id="printbtn" class="button"  value="Back" onClick="viewTripDetails();" />
                <input type="button" class="button" id="printbtn" value="Print" onClick="print('printContent');" />
                &nbsp;&nbsp;&nbsp;
                <c:if test="${tripDetailsList != null}">
                    <c:forEach items="${tripDetailsList}" var="tripStatus">
                        <c:if test="${tripStatus.tripStatusId < 8}">
                            <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripId}"/>&tripType=1&statusId=8"><input type="button" value="Start Trip"></a>
                            </c:if>
                        </c:forEach>
                    </c:if>

            </center>
        </form>
        <script type="text/javascript">
            function viewTripDetails() {
                document.enter.action = '/throttle/viewTripSheets.do?&statusId=10&tripType=1&admin=No';
                document.enter.submit();
            }
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            function get_object(id) {
                var object = null;
                if (document.layers) {
                    object = document.layers[id];
                } else if (document.all) {
                    object = document.all[id];
                } else if (document.getElementById) {
                    object = document.getElementById(id);
                }
                return object;
            }
            //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
            get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
            /* ]]> */
        </script>
    </body>
</html>