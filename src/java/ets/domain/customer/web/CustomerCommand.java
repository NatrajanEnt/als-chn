package ets.domain.customer.web;

/**
 *
 * @author vidya
 */
public class CustomerCommand {

    private String tallyBillingType = "";
    private String billingPartyAddress = "";
    private String billingState = "";
    private String billingCustName = "";
    private String billingCustFlag = "";
    private String billingCustId = "";
    private String consignorId = "";
    private String consigneeName = "";
    private String consignorName = "";
    private String contactPerson = "";
    private String phoneNo = "";
    private String address1 = "";
    private String address2 = "";
    private String cityId = "";
    private String email = "";
    private String pinCode = "";
    private String consigneeId = "";
    private String activeInd = "";
    private String pinCodeAdd = "";
    private String erpId = "";
    private String customerTypeId = "";
    private String secondaryBillingTypeId;
    private String secondaryBillingTypeName;
    private String customerGroup;
    private String customerGroupId;
    private String groupName;
    private String creditLimit;
    private String name;
    private String empName;
    private String creditDays;
    private String customerName;
    private String customerId;

    private String paymentType;
    private int custId;
    private int compId;
    private String custName;
    private String custContactPerson;
    private String custAddress;
    private String custCity;
    private String custState;
    private String custPhone;
    private String custMobile;
    private String custEmail;
    private String custStatus;
    private String[] custIdList;
    private String[] custNameList;
    private String[] custTypeList;
    private String[] custContactPersonList;
    private String[] custAddressList;
    private String[] custCityList;
    private String[] custStateList;
    private String[] custPhoneList;
    private String[] custMobileList;
    private String[] custEmailList;
    private String[] custStatusList;
    private String[] selectedIndex = null;
    private String contManufacturerName;
    private String contModelName;
    private String contPercentAgeUpSpares;
    private String contPercentAgeUpLabour;
    private String contStatus;
    private String contFromDate;
    private String contToDate;
    private String customerType;

    private String BunkName;
    private String fuelType;
    private String currRate;
    private String location;
    private String state;
    private String activeStatus;
    private String remarks;
    private String tancem;
    private double currentrate;
    private String custCode = "";
    private String accountManagerId = "";
    private String billingTypeId = "";
    private String organizationId = "";

//    12/05/16
    private String custAddresstwo = "";
    private String custAddressthree = "";
private String custTypeid="";

    public String getCustTypeid() {
        return custTypeid;
    }

    public void setCustTypeid(String custTypeid) {
        this.custTypeid = custTypeid;
    }

  

 

    //gst
    private String gstNo = "";
    private String stateId = "";
   
    public String getCustAddresstwo() {
        return custAddresstwo;
    }

    public void setCustAddresstwo(String custAddresstwo) {
        this.custAddresstwo = custAddresstwo;
    }

    public String getCustAddressthree() {
        return custAddressthree;
    }

    public void setCustAddressthree(String custAddressthree) {
        this.custAddressthree = custAddressthree;
    }

    public String getBunkName() {
        return BunkName;
    }

    public void setBunkName(String BunkName) {
        this.BunkName = BunkName;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getCurrRate() {
        return currRate;
    }

    public void setCurrRate(String currRate) {
        this.currRate = currRate;
    }

    public double getCurrentrate() {
        return currentrate;
    }

    public void setCurrentrate(double currentrate) {
        this.currentrate = currentrate;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustContactPerson() {
        return custContactPerson;
    }

    public void setCustContactPerson(String custContactPerson) {
        this.custContactPerson = custContactPerson;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getCustStatus() {
        return custStatus;
    }

    public void setCustStatus(String custStatus) {
        this.custStatus = custStatus;
    }

    public String[] getCustAddressList() {
        return custAddressList;
    }

    public void setCustAddressList(String[] custAddressList) {
        this.custAddressList = custAddressList;
    }

    public String[] getCustContactPersonList() {
        return custContactPersonList;
    }

    public void setCustContactPersonList(String[] custContactPersonList) {
        this.custContactPersonList = custContactPersonList;
    }

    public String[] getCustEmailList() {
        return custEmailList;
    }

    public void setCustEmailList(String[] custEmailList) {
        this.custEmailList = custEmailList;
    }

    public String[] getCustIdList() {
        return custIdList;
    }

    public void setCustIdList(String[] custIdList) {
        this.custIdList = custIdList;
    }

    public String[] getCustCityList() {
        return custCityList;
    }

    public void setCustCityList(String[] custCityList) {
        this.custCityList = custCityList;
    }

    public String[] getCustMobileList() {
        return custMobileList;
    }

    public void setCustMobileList(String[] custMobileList) {
        this.custMobileList = custMobileList;
    }

    public String[] getCustNameList() {
        return custNameList;
    }

    public void setCustNameList(String[] custNameList) {
        this.custNameList = custNameList;
    }

    public String[] getCustPhoneList() {
        return custPhoneList;
    }

    public void setCustPhoneList(String[] custPhoneList) {
        this.custPhoneList = custPhoneList;
    }

    public String[] getCustStateList() {
        return custStateList;
    }

    public void setCustStateList(String[] custStateList) {
        this.custStateList = custStateList;
    }

    public String[] getCustStatusList() {
        return custStatusList;
    }

    public void setCustStatusList(String[] custStatusList) {
        this.custStatusList = custStatusList;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String getContFromDate() {
        return contFromDate;
    }

    public void setContFromDate(String contFromDate) {
        this.contFromDate = contFromDate;
    }

    public String getContManufacturerName() {
        return contManufacturerName;
    }

    public void setContManufacturerName(String contManufacturerName) {
        this.contManufacturerName = contManufacturerName;
    }

    public String getContModelName() {
        return contModelName;
    }

    public void setContModelName(String contModelName) {
        this.contModelName = contModelName;
    }

    public String getContPercentAgeUpLabour() {
        return contPercentAgeUpLabour;
    }

    public void setContPercentAgeUpLabour(String contPercentAgeUpLabour) {
        this.contPercentAgeUpLabour = contPercentAgeUpLabour;
    }

    public String getContPercentAgeUpSpares() {
        return contPercentAgeUpSpares;
    }

    public void setContPercentAgeUpSpares(String contPercentAgeUpSpares) {
        this.contPercentAgeUpSpares = contPercentAgeUpSpares;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getContToDate() {
        return contToDate;
    }

    public void setContToDate(String contToDate) {
        this.contToDate = contToDate;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String[] getCustTypeList() {
        return custTypeList;
    }

    public void setCustTypeList(String[] custTypeList) {
        this.custTypeList = custTypeList;
    }

    public String getTancem() {
        return tancem;
    }

    public void setTancem(String tancem) {
        this.tancem = tancem;
    }

    public String getCustCode() {
        return custCode;
    }

    public void setCustCode(String custCode) {
        this.custCode = custCode;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSecondaryBillingTypeId() {
        return secondaryBillingTypeId;
    }

    public void setSecondaryBillingTypeId(String secondaryBillingTypeId) {
        this.secondaryBillingTypeId = secondaryBillingTypeId;
    }

    public String getSecondaryBillingTypeName() {
        return secondaryBillingTypeName;
    }

    public void setSecondaryBillingTypeName(String secondaryBillingTypeName) {
        this.secondaryBillingTypeName = secondaryBillingTypeName;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getErpId() {
        return erpId;
    }

    public void setErpId(String erpId) {
        this.erpId = erpId;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getPinCodeAdd() {
        return pinCodeAdd;
    }

    public void setPinCodeAdd(String pinCodeAdd) {
        this.pinCodeAdd = pinCodeAdd;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public int getCompId() {
        return compId;
    }

    public void setCompId(int compId) {
        this.compId = compId;
    }

    public String getBillingCustFlag() {
        return billingCustFlag;
    }

    public void setBillingCustFlag(String billingCustFlag) {
        this.billingCustFlag = billingCustFlag;
    }

    public String getBillingCustId() {
        return billingCustId;
    }

    public void setBillingCustId(String billingCustId) {
        this.billingCustId = billingCustId;
    }

    public String getBillingCustName() {
        return billingCustName;
    }

    public void setBillingCustName(String billingCustName) {
        this.billingCustName = billingCustName;
    }

    public String getTallyBillingType() {
        return tallyBillingType;
    }

    public void setTallyBillingType(String tallyBillingType) {
        this.tallyBillingType = tallyBillingType;
    }

    public String getBillingPartyAddress() {
        return billingPartyAddress;
    }

    public void setBillingPartyAddress(String billingPartyAddress) {
        this.billingPartyAddress = billingPartyAddress;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }
    
    

}
