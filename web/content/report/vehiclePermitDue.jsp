
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
</head>
<script language="javascript">

    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }

    function submitPage() {
        var chek = validation();
        if (chek == 'true') {

            document.fcReport.action = '/throttle/vehiclePermitReport.do';
            document.fcReport.submit();
        }
    }
    function validation() {
        if (document.fcReport.companyId.value == 0) {
            alert("Please Select Data for Company Name");
            document.fcReport.companyId.focus();
            return 'false';
        }/*
         else  if(textValidation(document.fcReport.dueIn,'Due Days')){
         return 'false';
         }
         else  if(textValidation(document.fcReport.toDate,'TO Date')){
         return'false';
         }*/
        return 'true';
    }
    function setValues() {
            if ('<%=request.getAttribute("companyId")%>' != 'null') {
                document.fcReport.companyId.value = '<%=request.getAttribute("companyId")%>';
                document.fcReport.dueIn.value = '<%=request.getAttribute("dueIn")%>';
                //document.fcReport.toDate.value='<%=request.getAttribute("toDate")%>';
        }
        /*
         if('<%=request.getAttribute("vendorId")%>'!='null'){
         document.fcReport.vendorId.value='<%=request.getAttribute("vendorId")%>';
         }
         */
            if ('<%=request.getAttribute("regNo")%>' != 'null') {
                document.fcReport.regNo.value = '<%=request.getAttribute("regNo")%>';
        }


    }
    function getVehicleNos() {

        var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Permit Due Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Reports</a></li>
            <li class="active">Permit  Due Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="getVehicleNos();setValues();">
                <form name="fcReport">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover"  id="report">
                        <thead>
                                  <th>
                                      Permit Due Report
                                  </th>
                              </thead>
                        <table class="table table-info mb30 table-hover" >
                                            <tr>
                                                <td align="left" height="30"><font color="red">*</font>Operation Point</td>
                                                <td><select class="form-control" name="companyId" style="width:240px">
                                                        <option value="0">-select-</option>
                                                        <c:if test = "${LocationLists != null}" >
                                                            <c:forEach items="${LocationLists}" var="company">
                                                                <c:choose>
                                                                    <c:when test="${company.companyTypeId==1011}" >
                                                                        <option value="<c:out value='${company.cmpId}'/>"><c:out value="${company.name}"/></option>
                                                                    </c:when>
                                                                </c:choose>
                                                            </c:forEach>
                                                        </c:if>

                                                    </select> </td>
                                                <td align="left" height="30">Vehicle No</td>
                                                <td height="30"><input name="regNo" id="regno" type="text" class="form-control"style="width:240px"  value=""> </td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>Due in (days)</td>
                                                <td><input name="dueIn" type="text" class="form-control" value="30" size="20" style="width:240px"/> &nbsp; </td>
                                            <input name="fromDate" type="hidden" class="form-control" value="" size="15" />
                                            <td>&nbsp;</td>

                                            <input name="toDate" type="hidden" class="form-control" value="" size="13">
                                            <td><input type="button" value="Search" class="btn btn-info" name="search" onClick="submitPage()">
                                                <input type="hidden" value="" name="reqfor"></td>
                                            </tr>
                                        </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <% int index = 0;%>
                    <%int oddEven = 0;%>
                    <%  String classText = "";%>
                    <c:if test = "${DueList != null}" >
                        <table  id="bg"  class="table table-info mb30 table-hover">

                            <tr><thead>
                                <th width="35" >Sno</th>
                                <th width="132" height="30" >Operation Point </th>
                                <th width="127" height="30" >Vehicle No </th>
                                <th width="121" >Expiry Date </th>
                                <th width="121" >Permit Type </th>
                                <th width="174" height="30" >Due In Days </th>
                           </thead> </tr>
                            <c:forEach items="${DueList}" var="fc">
                                <%
                    oddEven = index % 2;
                    if (oddEven > 0) {
                        classText = "text2";
                    } else {
                        classText = "text1";
                    }
                                %>

                                <tr>
                                    <td ><%=index + 1%></td>
                                    <td  height="30"><c:out value="${fc.companyName}" /></td>
                                    <td  height="30"><c:out value="${fc.regNo}" /> </td>
                                    <td  height="30"><c:out value="${fc.nextFc}" /> </td>
                                    <td  height="30"><c:out value="${fc.permitType}" /> </td>
                                    <td  height="30"><c:out value="${fc.due}" /></td>
                                </tr>
                                <% index++;%>
                            </c:forEach>
                        </table>
                    </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>

        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
