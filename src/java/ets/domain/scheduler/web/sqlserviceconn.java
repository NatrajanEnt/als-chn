package ets.domain.scheduler.web;

import ets.domain.report.business.ReportTO;
import ets.domain.report.business.TallyTO;
import ets.domain.thread.web.SendEmail;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

class sqlserviceconn extends Thread {

    String JobStartDate = "";
    String JobEndDate = "";
    String CityFromCode = "";
    String CityToCode = "";
    String Distance = "";
    String OwnedHired = "";
    String HiredVehicleNo = "";
    String HireCharges = "";
    String Partyname = "";
    String AccCode = "";
    String DieselSupplies = "";
    String DieselConsumed = "";
    String OpeningKM = "";
    String ClosingKM = "";
    String TotalKMRun = "";
    String Size_20 = "";
    String Size_40 = "";
    String Size_40HC = "";
    String Size_FT = "";
    String Size_OT = "";
    String NetWt = "";
    String Closed = "";
    String CompanyCode = "";
    String BrnCd = "";
    String RUNDATE = "";
    String JobReachedDate = "";
    String JobDate = "";
    String DieselRecovered = "";
    String IsExport = "";
    String TrlJobId = "";
    String InvDt = "";
    String size_20Taurus = "";
    String size_40SngAxle = "";
    String size_24DblAxle = "";
    String size_40TplAxle = "";
    String size_20DblAxle = "";

    public sqlserviceconn(TallyTO reportTO) {

        JobStartDate = reportTO.getJobStartDate();
        JobEndDate = reportTO.getJobEndDate();
        CityFromCode = reportTO.getCityFromCode();
        CityToCode = reportTO.getCityToCode();
        Distance = reportTO.getDistance();
        OwnedHired = reportTO.getOwnedHired();
        HiredVehicleNo = reportTO.getHiredVehicleNo();
        HireCharges = reportTO.getHireCharges();
        Partyname = reportTO.getPartyname();
        AccCode = reportTO.getAccCode();
        DieselSupplies = reportTO.getDieselSupplies();
        DieselConsumed = reportTO.getDieselConsumed();
        OpeningKM = reportTO.getOpeningKM();
        ClosingKM = reportTO.getClosingKM();
        TotalKMRun = reportTO.getTotalKMRun();
        Size_20 = reportTO.getSize_20();
        Size_40 = reportTO.getSize_40();
        Size_40HC = reportTO.getSize_40HC();
        Size_FT = reportTO.getSize_FT();
        Size_OT = reportTO.getSize_OT();
        NetWt = reportTO.getNetWt();
        Closed = reportTO.getClosed();
        CompanyCode = reportTO.getCompanyCode();
        BrnCd = reportTO.getBrnCd();
        RUNDATE = reportTO.getRunDate();
        JobReachedDate = reportTO.getJobReachedDate();
        JobDate = reportTO.getJobDate();
        DieselRecovered = reportTO.getDieselRecovered();
        IsExport = reportTO.getIsExport();
        TrlJobId = reportTO.getTrlJobId();
        InvDt = reportTO.getInvDt();
        size_20Taurus = reportTO.getSize_20Taurus();
        size_40SngAxle = reportTO.getSize_40SngAxle();
        size_24DblAxle = reportTO.getSize_24DblAxle();
        size_40TplAxle = reportTO.getSize_40TplAxle();
        size_20DblAxle = reportTO.getSize_20DblAxle();

    }

    public void run() {
        int updateStatus = 0;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sqlQuery = "";
        String tripNo = "";
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conn = DriverManager.getConnection("jdbc:sqlserver://188.188.188.80:1433;TrsptFA", "Trt", "TrtFA@1234%");

            if (conn != null) {
                System.out.println("DKT Txn Database Successfully connected");

                sqlQuery = "INSERT INTO [TrsptFA].[dbo].[Dkt] (JobStartDate,JobEndDate,CityFromCode,CityToCode,Distance,Owned_Hired,HiredVehicleNo,HireCharges,Partyname,AccCode,"
                        + "DieselSupplies,DieselConsumed,OpeningKM,ClosingKM,TotalKMRun,Size_20,Size_40,Size_40HC,Size_FT,Size_OT,"
                        + "NetWt,Closed,CompanyCode,BrnCd,RUNDATE,JobReachedDate,JobDate,DieselRecovered,IsExport,TrlJobId"
                        + "InvDt,size_20Taurus,size_40SngAxle,size_24DblAxle,size_40TplAxle,size_20DblAxle) VALUES "
                        + "(?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?,?,?,?,?,"
                        + "?,?,?,?,?,?)";

                stmt = conn.prepareStatement(sqlQuery);

                stmt.setString(1, JobStartDate);
                stmt.setString(2, JobEndDate);
                stmt.setString(3, CityFromCode);
                stmt.setString(4, CityToCode);
                stmt.setString(5, Distance);
                stmt.setString(6, OwnedHired);
                stmt.setString(7, HiredVehicleNo);
                stmt.setString(8, HireCharges);
                stmt.setString(9, Partyname);
                stmt.setString(10, AccCode);
                stmt.setString(11, DieselSupplies);
                stmt.setString(12, DieselConsumed);
                stmt.setString(13, OpeningKM);
                stmt.setString(14, ClosingKM);
                stmt.setString(15, TotalKMRun);
                stmt.setString(16, Size_20);
                stmt.setString(17, Size_40);
                stmt.setString(18, Size_40HC);
                stmt.setString(19, Size_FT);
                stmt.setString(20, Size_OT);
                stmt.setString(21, NetWt);
                stmt.setString(22, Closed);
                stmt.setString(23, CompanyCode);
                stmt.setString(24, BrnCd);
                stmt.setString(25, RUNDATE);
                stmt.setString(26, JobReachedDate);
                stmt.setString(27, JobDate);
                stmt.setString(28, DieselRecovered);
                stmt.setString(29, IsExport);
                stmt.setString(30, TrlJobId);
                stmt.setString(31, InvDt);
                stmt.setString(32, size_20Taurus);
                stmt.setString(33, size_40SngAxle);
                stmt.setString(34, size_24DblAxle);
                stmt.setString(35, size_40TplAxle);
                stmt.setString(36, size_20DblAxle);

                updateStatus = stmt.executeUpdate();
                System.out.println("UPDATE = " + stmt);

//                while (rs.next()) {
//                    tripNo = rs.getString("TripNo");
//                }
//                System.out.println("tripNo==========" + tripNo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if (updateStatus != 0) {
//        if (!"".equals(updateStatus) && "0".equals(updateStatus)) {

            String fileNameNew = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/" + fileNameNew);
            Connection con = null;
            try {
                dbProps.load(is);//this may throw IOException
            } catch (IOException ex) {
                Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
            }
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            int status = 0;

            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try {
                    PreparedStatement updateMailDetails = null;
                    String sql = "UPDATE Dkt SET \n"
                            + " post_ind = 1"
                            + " WHERE  TrlJobId = ?";
                    updateMailDetails = con.prepareStatement(sql);
                    updateMailDetails.setString(1, TrlJobId);
                    status = updateMailDetails.executeUpdate();
                    System.out.println("NEW CHANGES = " + status);

                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("TripNo doesn't exist.");
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}
