<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>


<!--<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>-->

<!--<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->

<!-- Data from www.netmarketshare.com. Select Browsers => Desktop share by version. Download as tsv. -->
<pre id="tsv" style="display:none"></pre>




<div class="pageheader">
    <h2><i class="fa fa-home"></i> Dashboard
        <!--<span>Operations</span>-->
    </h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Dashboard</li>
            <!--<li class="active">Operations</li>-->
        </ol>
    </div>
</div>

<div class="contentpanel">

    <div class="row">

<!--        <div class="col-sm-6 col-md-3">
            <div class="panel panel-success panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">

                                <i class="ion ion-android-bus"></i>
                            </div>
                            <div class="col-xs-8">
                                                    <small class="stat-label">Truck Utilisation</small>
                                <h4> <spring:message code="dashboard.label.Trucks"  text="Default"/></h4>

                                <h1><span id="totalTruckNosSpan"></span></h1>
                            </div>
                        </div> row 

                        <div class="mb15"></div>

                    </div> stat 

                </div> panel-heading 
            </div> panel 
        </div> col-sm-6 -->

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-primary panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-android-subway"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Export"  text="Export"/></h4>
                                <h1><span id="totalExportMovementSpan"></span></h1>
                            </div>
                        </div>  

                        <div class="mb15"></div>



                    </div>  

                </div>  
            </div>  
        </div> 

        <div class="col-sm-6 col-md-3">
            <div class="panel panel-danger panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-android-subway"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Import"  text="Import"/></h4>
                                <h1><span id="totalImportMovementSpan"></span></h1>
                            </div>
                        </div>  

                        <div class="mb15"></div>

                    </div>  
                </div> 
            </div>   
        </div>  

<!--        <div class="col-sm-6 col-md-3">
            <div class="panel panel-warning panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-happy"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4> <spring:message code="dashboard.label.Customers"  text="Default"/></h4>
                                <h1><span id="totalMonthlyLeaseCustomerNosSpan"></span></h1>
                            </div>
                        </div> row 

                        <div class="mb15"></div>



                    </div> stat 

                </div> panel-heading 
            </div> panel 
        </div> col-sm-6 -->

        <%--
        <div class="col-sm-6 col-md-3">
            <div class="panel panel-warning panel-stat">
                <div class="panel-heading">

                    <div class="stat">
                        <div class="row">
                            <div class="col-xs-4">
                                <i class="ion ion-alert-circled"></i>
                            </div>
                            <div class="col-xs-8">
                                <h4>DailyLeaseCustomer</h4>
                                <h1><span id="totalDailyLeaseCustomerNosSpan"></span></h1>
                            </div>
                        </div><!-- row -->

                        <div class="mb15"></div>



                    </div><!-- stat -->

                </div><!-- panel-heading -->
            </div><!-- panel -->
        </div><!-- col-sm-6 -->

        --%>
    </div><!-- row -->


    <table>
        <tr>
            <!--<td>-->
                <!--<div class="row" >-->
<!--                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns 
                                <h5  class="panel-title"> <spring:message code="dashboard.label.TruckMake"  text="Default"/></h5>
                                <div  id="truckMake" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>
                                        <div class="panel-body">


                                            <div class="col-sm-8">


                                                  <div id="containerFleetUtilisation" style="min-width: 600px; height: 400px; margin: 0 auto"></div>

                                            </div>



                                      </div>
                        </div>
                    </div>-->
            <!--</td>-->
<!--            <td>
                <div class="col-sm-8 col-md-9">
                    <div class="panel panel-default">

                        <div class="outer1">
                            <div class="panel-btns">
                                <a href="" class="panel-close">&times;</a>
                                <a href="" class="minimize">&minus;</a>
                            </div> panel-btns 
                            <h5 class="panel-title"><spring:message code="dashboard.label.TrailerMake"  text="Trailer"/></h5>
                            <div id="trailerMake" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                        </div>
                                    <div class="panel-body">


                                        <div class="col-sm-8">

                                        </div>
                                  </div>
                    </div>
                </div>
            </td>-->
<!--        </tr>
        <tr>-->
            <td>
                <div class="row" >
                    <div class="col-sm-8 col-md-9">
                        <div class="panel panel-default">

                            <div class="outer2">
                                <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div><!-- panel-btns -->
                                <h5 class="panel-title"><spring:message code="dashboard.label.Export"  text="Export"/></h5>
                                <div id="truckType" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                            </div>
                            <!--            <div class="panel-body">


                                            <div class="col-sm-8">




                                            </div>



                                      </div>-->
                        </div>
                    </div>
            </td>
            <td>
                <div class="col-sm-8 col-md-9">
                    <div class="panel panel-default">

                        <div class="outer">
                            <div class="panel-btns">
                                <a href="" class="panel-close">&times;</a>
                                <a href="" class="minimize">&minus;</a>
                            </div> 
                            <h5 class="panel-title"><spring:message code="dashboard.label.Import"  text="Import"/></h5>
                            <div id="truckType1" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                        </div>
<!--                                    <div class="panel-body">


                                        <div class="col-sm-8">

                                        </div>
                                  </div>-->
                    </div>
                </div>
            </td>
        </tr>



    </table>
    <!--<table>-->

        <!--<div class="col-sm-8 col-md-9">-->
        <!--<div class="panel panel-default">-->

        <!--<div class="outer4">-->
            <!--                    <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns -->
            <!--<h5 class="panel-title">Fleet Utilisation</h5>-->
<!--            <div id="barchart" style="min-width: 1200px; height: 400px; margin: 0 auto"></div>
        </div>-->
        <!--            <div class="panel-body">


                        <div class="col-sm-8">

                        </div>
                  </div>-->
        <!--</div>-->
        <!--</div>-->
    <!--</table>-->



    <style>
        .outer {
            width: 600px;
            color: navy;
            background-color: #1caf9a;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer1 {
            width: 600px;
            color: navy;
            background-color: #428bca;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer2 {
            width: 600px;
            color: navy;
            background-color: #a94442;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer3 {
            width: 600px;
            color: navy;
            background-color: #f0ad4e;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer4 {
            width: 1210px;
            color: navy;
            background-color: pink;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .ben{
            width: 600px;
        }
    </style>
    <!-- /.box -->




</div><!-- contentpanel -->





</div><!-- mainpanel -->



<%@ include file="/content/common/NewDesign/settings.jsp" %>


<script>
    loadMonthlyTripStatus();
    function loadMonthlyTripStatus() {
        var vehicleMakeArray = [];
        var vehicleCountArray = [];
        $.ajax({
            url: '/throttle/ownVehicleMTD.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        vehicleMakeArray.push(value.tripDate);
//                        vehicleCountArray.push(value.ownership);
                        vehicleCountArray.push([parseInt(value.ownership)]);
                });
                $('#barchart').highcharts({
                    chart: {
                        type: 'column'
                    },
                    title: {
                        text: 'Own Vehicles MTD'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category',
                        categories: vehicleMakeArray,
                        labels: {
                            rotation: -45,
                            style: {
                                fontSize: '13px',
                                fontFamily: 'Verdana, sans-serif'
                            }
                        }
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'No Of Trips'
                        }
                    },
                    legend: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: 'Truck : <b>{point.y:f} Nos</b>'
                    },
                    series: [{
                            name: 'Own Vehicles MTD',
                            data: vehicleCountArray,
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:f}', // one decimal
                                y: 10, // 10 pixels down from the top
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }
                        }]
                });
            }
        });
    }
</script>


<script>
    loadTrucknos();
    function loadTrucknos() {
        $.ajax({
            url: '/throttle/getDashBoardTruckNos.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                    if(value.Name == 'Make'){
                      $("#totalTruckNosSpan").text(value.Count);
                    }else if(value.Name == 'TrailerMaster'){
                      $("#totalTrailersNosSpan").text(value.Count);
                    }else if(value.Name == 'EmpMaster'){
                      $("#totalDriversNosSpan").text(value.Count);
                    }
                    else if(value.Name == 'CustomerMaster'){
                      $("#totalMonthlyLeaseCustomerNosSpan").text(value.Count);
                    } else if(value.Name == 'Export'){
                      $("#totalExportMovementSpan").text(value.Count);
                    } else if(value.Name == 'Import'){
                      $("#totalImportMovementSpan").text(value.Count);
                    }



                });
//                        vehicleMakeArray.push(value.Make);
//                        vehicleCountArray.push([parseInt(value.Count)]);
                }
            });
        }
        </script>


  <script>

    truckMake();
    function truckMake() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/vehicleMake.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.Make;
                        x_values_sub['y'] = parseInt(value.Count);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#truckMake').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
</script>
  <script>

    trailerMake();
    function trailerMake() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/dbTrailerMake.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.Make;
                        x_values_sub['y'] = parseInt(value.Count);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#trailerMake').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
</script>
  <script>

    truckType();
    function truckType() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/ownHireVehicleUT.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.ownVehCount;
                        x_values_sub['y'] = parseInt(value.hireVehCount);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#truckType').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
</script>
  <script>

    truckType1();
    function truckType1() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/ownHireVehicleUT1.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.tripStatusName;
                        x_values_sub['y'] = parseInt(value.count);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#truckType1').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
</script>
  <script>

    trailerType();
    function trailerType() {
        var x_values = [];
        var x_values_sub = {};
        var cntr = 0;
        $.ajax({
            url: '/throttle/ownHireVehicleUT.do',
            dataType: 'json',
            success: function (data) {
                $.each(data, function (key, value) {
                        x_values_sub['name'] = value.ownVehCount;
                        x_values_sub['y'] = parseInt(value.hireVehCount);
                        x_values.push(x_values_sub);
                        x_values_sub = {};
//                    }
                });
//                alert(vehicleMakeArray[1])
                $('#trailerType').highcharts({
                    chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },

                    title: {
                text: ''
            },

                    tooltip: {
                pointFormat: '{series.name}: <b>{point.y}</b>'
            },

             plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.y} ',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
                }
            },


                    series: [{
                    type: 'pie',
                    name: 'Nos',
                    data: x_values
                }]
                });
            }
        });
    }
</script>





<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>




