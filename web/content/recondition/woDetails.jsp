
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.recondition.business.ReconditionTO" %>
        <%@ page import="ets.domain.mrs.business.MrsTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <title>WO Details</title>
    </head>

    <script>
        function print(val)
        {
            var DocumentContainer = document.getElementById(val);
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }


    </script>


    <body>

        <%

//            NumberFormat df2 = new DecimalFormat("#0.00");
            int itemNameLength = 35, uomLength = 10;
            ReconditionTO reconditionTO = null;
            ProblemActivitiesTO activityTO = null;
            int pageSize = 24;
            String[] activity = null;
        //ArrayList billRecord = (ArrayList) request.getAttribute("billRecord");
            //ArrayList billItems = new ArrayList();
            //billItems = (ArrayList)request.getAttribute("billItems");

            String[] companyAddress = null;
//            float discountVal = 0.00f;
//            float finalAmount = 0.00f;
            ArrayList billHeaderInfo = (ArrayList) request.getAttribute("billHeaderInfo");
            Iterator itr = billHeaderInfo.iterator();
            ReconditionTO rcto = null;
            while (itr.hasNext()) {
                rcto = (ReconditionTO) itr.next();
//                finalAmount = Float.parseFloat(rcto.getPrice());
//                System.out.println("finalAmount:" + finalAmount);
                companyAddress = rcto.getAddressSplit();
            }

            int totalRecords = billHeaderInfo.size();
            int itemSize = billHeaderInfo.size();
        //int laborSize = (Integer) request.getAttribute("laborSize") ;
            //int taxSize = (Integer) request.getAttribute("taxSize") ;
//            float laborTotal = 0;
//            float spareTotal = 0;
//            float taxTotal = 0;
//            double total = 0;
            int index = 0;
            int loopCntr = 0;

            for (int i = 0; i < itemSize; i = i + pageSize) {
                loopCntr = i;
        %>

        <div id="print<%= i%>" >

            <style>


                .text1 {
                    font-family:Tahoma;
                    font-size:14px;
                    color:#333333;
                    background-color:#E9FBFF;
                    padding-left:10px;
                    line-height:15px;
                }
                .text2 {
                    font-family:Tahoma;
                    font-size:12px;
                    color:#333333;
                    background-color:#FFFFFF;
                    padding-left:10px;
                    line-height:15px;
                }


            </style>



            <c:if test= "${billHeaderInfo != null}"  >
                <c:forEach items="${billHeaderInfo}" var="wo" >
                    <table width="725" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
                        <tr>
                            <td height="27" colspan="6" class="text2"  align="center"  scope="row" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                <b>RC WorkOrder </b>
                            </td>
                        </tr>
                        <c:set var="workOrderNo" value="${wo.woId}" />
                        <c:set var="receivedDate" value="${wo.createdOn}" />
                        <c:set var="vendorName" value="${wo.vendorName}"/>                     
                        <c:set var="tyreNo" value="${wo.tyreNo}"/>
                        <c:set var="vendorGst" value="${wo.vendorGst}"/>
                        <c:set var="GSTNo" value="${wo.GSTNo}"/>
                        <% //float grandTotal = (Float)pageContext.getAttribute("nettAmount");
                            //System.out.println("Page Context Variable out-->"+grandTotal);
                        %>
                        <tr>
                            <td class="text2" colspan="4" width="427"  align="left" rowspan="2" scope="col" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                                <!--<img src="images/throttle_logo_header.png" style="height:30px;width:100px;margin-left:-01px;">-->
                                <p> <b>CA Log Pvt Ltd. </b><br>
                                    <c:out value="${wo.companyAddress}" /> <br>
                                    <c:out value="${wo.companyCity}" /> - <c:out value="${wo.companyPincode}" /> <br>
                                    E-mail :<c:out value="${wo.companyEmail}" /> 
                                    GST No :37AADCK9309G1ZE

                                </p>
                            </td>
                            <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                                <p>Rc Workorder No<br>
                                    <c:if test= "${wo.woId != ''}"  >
                                        <b> <c:out value="${wo.woId}" /> </b>
                                    </c:if>

                                </p>
                            </td>
                            <td  class="text2"  align="left"  width="148" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                                <p>Received Date<br>
                                    <b> <c:out value="${wo.createdOn}" /> </b> </p>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2"  align="left" scope="col"  style=" padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                                <p>Vendor Name<br>
                                    <b> <c:out value="${vendorName}"/>  </b> </p>
                            </td>
                            <td  class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                                <p>Tyre No<br>
                                    <b><c:out value="${wo.tyreNo}" /></b>
                                </p>
                            </td>
                        </tr>

                    </table>
                </c:forEach>
            </c:if>



            <table width="725" height="99"  border="0" cellpadding="0"  style="border:1px; border-left-color:#E8E8E8; border-left-style:solid; border-right-color:#E8E8E8; border-right-style:solid;"  cellspacing="0">
                <tr>
                    <th width="30" class="text2"  height="30" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; " scope="col" >S.No</th>

                    <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Company Name</th>
                    <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Vendor Name</th>
                    <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Item Name</th>
                    <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">PAPL Code</th>
                    <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Tyre No</th>
                </tr>

                <% if (loopCntr <= itemSize) { %>
                <%

                    for (int j = loopCntr; (j < (itemSize)) && (j < i + pageSize); j++) {

                        ++loopCntr;
                       ReconditionTO itemTO = new ReconditionTO();
                        itemTO = (ReconditionTO) billHeaderInfo.get(j);
                        String itemName = "";
                        itemName = itemTO.getItemName();
                        if (itemName.length() > itemNameLength) {
                            itemName = itemName.substring(0, itemNameLength - 1);
                        }
                %>



                <tr>
                    <td  class="text2"  align="left"  style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                        <% index++;%> <%= index%>
                    </td>

                    <td  class="text2" style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                        <%= itemTO.getCompanyName()%>
                    </td>

                    <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; "  align="right" >
                        <%= itemTO.getVendorName()%>
                    </td>
                    <td  class="text2"   align="left"   style="border:1px; border-color:#FFFFFF; border-left-color:#CCCCCC; border-left-style:solid; "  scope="row" >
                        <%= itemName%>
                    </td>
                    <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                        <%= itemTO.getPaplCode()%>
                    </td>
                    <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-left-color:#CCCCCC; border-left-style:solid; "  align="center" >
                        <%= itemTO.getTyreNo()%>
                    </td>

                    <!--                    <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; "  align="right" >
                    <%--<%= itemTO.getLaborAmount()%>--%>
                </td>
                <td  class="text2"   style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; "  align="right" >
                    <%--<%= itemTO.getPrice()%>--%>
                </td>-->

                    <% } %>

                    <% } %>
                </tr>
                <br>

                <% 
                    if (loopCntr == (totalRecords)) { %>



                <!--                <tr>
                                    <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                                        &nbsp;
                                    </td>
                                    <td class="text2" colspan="6" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                                        Tax Amount &nbsp;
                                    </td>
                                    <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <%--<c:set var="nettTaxValue" value="${nettTaxValue + taxDetails.spareTaxAmount}" />
                <fmt:formatNumber pattern="##.00" value="${taxDetails.spareTaxAmount}"/> --%>
                <fmt:formatNumber pattern="##.00" value="${tax}"/>


            </td>
        </tr>


        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="6" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Round Off
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <%--<%=df2.format(roundOff)%> <c:out value="${spareRetAmount}" /> --%>
                <fmt:formatNumber pattern="##.00" value="0"/>
            </td>
        </tr>






        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC; " scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="6"  align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;"  scope="row" >
                <b> Grand Total </b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <b>
                <%--<%=df2.format(java.lang.Math.round(grandTotal1))%> --%>
                <fmt:formatNumber pattern="##.00" value="${finalAmount}"/>
            </b>






        </td>
    </tr>-->
                <%--<jsp:useBean id="numberWords2"   class="ets.domain.report.business.NumberWords" >
                    <% numberWords2.setRoundedValue(String.valueOf(total));
                    NumberFormat formatter = new DecimalFormat("#0.00");
                    %>
                    </tr> <jsp:getProperty name="numberWords2" property="roundedValue" />
                    <b><%=formatter.format(java.lang.Math.ceil(total)) %> </b>
                </jsp:useBean>--%>




                <% } %>

                <% if (loopCntr != (totalRecords)) { %>

                <tr>
                    <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;  border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                        &nbsp;
                    </td>
                    <td class="text2" colspan="5"  align="center" style="border:1px; border-color:#FFFFFF;    border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                        <b>Contnd..</b>
                    </td>
                    <td  class="text2" style="border:1px; border-color:#FFFFFF;  border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;"  align="right" >
                        &nbsp;
                    </td>
                </tr>






                <% } %>

            </table>





            <table width="725" height="79" border="0" style="border:1px; border-color:#E8E8E8; border-style:solid;"  cellpadding="0" cellspacing="0">
                <tr>
                    <td width="720">
                        <div class="text2" >
                            <% if (loopCntr == (totalRecords)) { %>
                            <!--Amount Chargeable (in words)-->
                            <%--<jsp:useBean id="numberWords1"   class="ets.domain.report.business.NumberWords" >--%>


                            <%-- numberWords1.setNumberInWords(numberWords1.getRoundedValue()); %>

                                <!--<b>  Rupees <
                                jsp:getProperty name="numberWords1" property="numberInWords" /> Only</b>-->
                            <%--</jsp:useBean>--%>

                            <% } %>

                            <br>

                            Remarks:


                            <br>
                            <table height="10" align="left" cellpadding="0" cellspacing="0" border="0">



                                <tr>
                                    <td width="200" class="text2">
                                        <b> AS PER <c:out value="${servicePtName}" />&nbsp;WorkOrder No</b>
                                    </td>
                                    <td class="text2">
                                        :&nbsp;<b><c:out value="${workOrderNo}" /></b>
                                    </td>
                                </tr>

                                <tr>
                                    <td width="200" class="text2">
                                        <b>Buyer's GST No</b>
                                    </td>
                                    <td class="text2">
                                        :&nbsp;<c:out value="${GSTNo}" />
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200" class="text2" >
                                        <b> Corporate Office Address</b>
                                    </td>
                                    <td class="text2">
                                       &nbsp;<p> <b>CA Log Pvt Ltd. </b><br>
                                            Old No: 40, New No: 46, 6th Floor,<br>
                                            Rajaji Salai, Parrys <br>
                                            Chennai - 600001</p> 
                                            <!--                                            Plot No 1-124,<br>
                                                                                        Survey No. 329,
                                                                                        Nizampet,
                                                                                        <br>Hyderabad-500072.<br>
                                                                                         GST: 37AADCK9309G1ZE<br>
                                                                                        E-mail : info@kesineni.com</p>-->
                                    </td>
                                </tr>
                                <tr>
                                    <td width="200" class="text2"  >
                                        <b> Helpline NO (24X7)</b>
                                    </td>
                                    <td class="text2">
                                        :&nbsp;<b> 9676767676</b>
                                    </td>
                                </tr>
                                <%--</table>
                                </div>

<table width="720" border="0" cellpadding="0" cellspacing="0">
                                --%>    <tr>
                                    <td class="text2" width="380" align="left"><b><br>Declaration</b> </td>
                                    <td class="text2" width="315" align="right"><b> for CA Log Pvt Ltd. </b> </td>
                                </tr>
                                <tr>
                                    <td class="text2" width="380" align="left">We declare that this invoice shows the actual price of the</td>
                                    <td class="text2" width="325" align="right"> &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text2" width="380" align="left">goods described and that all particulars are true and correct</td>
                                    <td class="text2" width="325" align="right">&nbsp; </td>
                                </tr>
                                <tr>
                                    <td class="text2" width="380" align="left">&nbsp; </td>
                                    <td class="text2" width="380" align="left">&nbsp; </td>
                                </tr>
                                <tr>
                                    <td class="text2" width="380" align="left">&nbsp; </td>
                                    <td class="text2" width="380" align="left">&nbsp; </td>
                                </tr>
                                <tr>
                                    <td class="text2" width="380" align="left"><b>Customer Signature </b></td>
                                    <td class="text2" width="315" align="right"><b> Authorized Signatory </b></td>
                                </tr>

                            </table>

                        </div>

                    </td>
                </tr>
            </table>



            <br>
            <div ALIGN="left" class="text2">
                SUBJECT TO CHENNAI JURISDICTION &nbsp; This is a Computer Generated Invoice
            </div>
        </div>
        <br>
    <center>
        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i%>');" > &nbsp;
    </center>
    <br>
    <br>
    <% }%>


</body>
</html>
