<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : RAM
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <title>PrintTripSheet</title>
    </head>
    <body>
        <style type="text/css">
            #watermark {
                color: #d0d0d0;
                font-size: 50pt;
                -webkit-transform: rotate(-45deg);
                -moz-transform: rotate(-45deg);
                position: absolute;
                width: 100%;
                height: 100%;
                margin: 0;
                z-index: -1;
                left:200px;
                top:200px;
            }
        </style>
        <script>

            window.onunload = function() {
                window.opener.location.reload();
            }
        </script>
        <form name="enter" method="post">
            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            String billingPartyId = (String)request.getAttribute("billingPartyId");
            %>
            <div id="printContent">
                <br>
                <center>
                    <font size="4"> <b> <c:out value='${orderType}'/> </b></font>
                    <c:if test="${menuId == 1}">
                        <div id="watermark">
                            <p>DUPLICATE COPY</p>
                        </div>
                    </c:if>
                </center>
                <table align="center"  style="border-collapse: collapse;width:400px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                       background:url(images/dict-logoWatermark.png);
                       background-repeat:no-repeat;
                       background-position:center;
                       border:1px solid;
                       /*border-color:#CD853F;width:800px;*/
                       border:1px solid;
                       /*opacity:0.8;*/
                       /*font-weight:bold;*/
                       ">
                    <tr>
                        <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                            <table align="left" >
                                <tr>
                                    <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                    <td align="left"><img src="images/Triway_CFS_Logo.jpg" width="165" height="70"/></td>
                                        <%}else {%>
                                    <td align="left"><img src="images/Route Logo.JPG" width="140" height="70"/></td>
                                        <%}%>

                                    <td style="padding-left: 180px; padding-right:42px;">
                                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;




                                <center>
                                    <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                    <font size="4"><b>Triway CFS Pvt Ltd.</b></font></center><br>
                                    <%}else {%>
                                <font size="4"><b>Route Logistics India Pvt.Ltd.</b></font></center><br>
                                <%}%>

                                <center>
                                    <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                    <font size="2" >148, Ponneri High Road, Edayanchavadi, New Nappalayam, Chennai,Tamilnadu.
                                    </font>
                                    <%}else {%>
                                    <font size="2" >14,Jaffer Street, Chennai,Tamilnadu.
                                    </font>
                                    <%}%>
                                </center>
                        </td>
                        <td align="right">
                            <br>
                            <div id="externalbox" style="width:3in">
                                <div id="inputdata" ></div>
                            </div>
                        </td>
                    </tr>
                </table>
                </td>
                </tr>

                <tr>
                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <tr>
                                <td  style="border-right:solid #888 1px;">
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>PNR/Booking No </b></font> <font size="2"> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${customerOrderReferenceNo}"/></font> 
                                    <br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>Route Info </b></font><font size="2">&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${routeInfo}"/>.</font><br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>Trip Code </b> </font><font size="2">&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${tCode}"/></font><br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>Trip Start Time </b> </font><font size="2">&emsp;&emsp;&emsp;<c:out value="${startDate}"/> &emsp;&emsp;<c:out value="${startTime}"/>  </font><br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>Trip End Time </b> </font><font size="2">&nbsp;&emsp;&emsp;&emsp;<c:out value="${endDate}"/> &emsp;&emsp;<c:out value="${endTime}"/> </font><br>
                                    <br>

                                </td>
                                <td  style="border-right:solid #888 1px;">
                                    <font size="2">&nbsp;&nbsp;<b>Transporter Name</b></font> <font size="2"> &emsp;&emsp;<c:out value="${vendor}"/></font> 
                                    <br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>Vehicle No</b></font><font size="2">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${vehicleNo}"/> &nbsp;(<c:out value='${vehicleType}'/>)</font><br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>Driver Name </b> </font><font size="2">&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${driverName}"/></font><br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>Mobile </b> </font><font size="2">&nbsp;&nbsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${driverMobile}"/>   </font><br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>DL No </b> </font><font size="2">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${licenceNo}"/> </font><br>
                                    <br>
                                    <br>
                                    <font size="2">&nbsp;&nbsp;<b>E-Way Bill No </b> </font><font size="2">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${eWayBillNo}"/> </font><br>
                                    <br>

                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr height='100'>
                    <td style="border: solid #888 1px;">

                        <table width='100%' height='80'>
                            <tr>
                                <td>
                                    <table  width='100%' height='80'>
                                        <tr>
                                            <td>
                                                <table  style="border-collapse: collapse;" width='100%' height='100'>
                                                    <thead height="100">
                                                        <tr style="height:50px;white-space: pre; " >
                                                            <th style="border: solid #888 1px;"><h1><font size="2">Container No</font></h1></th>
                                                    <th style="border: solid #888 1px;"><h1><font size="2">Vessel Name</font></h1></th>
                                                    <th style="border: solid #888 1px;"><h1><font size="2">&nbsp;Container Size&nbsp;</font></h1></th>
                                                    <th style="border: solid #888 1px; "><h1><font size="2">&nbsp;&nbsp;Container Qty&nbsp;&nbsp;</font> </h1></th>
                                        </tr>
                                        </thead>
                                        <tbody height="80">

                                            <tr style="height:50px;padding:0px;margin:0px; " >
                                                <td style="border: solid #888 1px;">

                                                    &nbsp;&nbsp;&nbsp;&nbsp; <c:out value='${containerNo}'/>
                                                    <br>&nbsp;&nbsp;&nbsp;&nbsp;  <c:out value='${containerNo1}'/>
                                                </td>
                                                <td style="border: solid #888 1px;  ">&nbsp;&nbsp;&nbsp;&nbsp;  <c:out value='${linerName}'/>  <br>&nbsp;&nbsp;&nbsp;&nbsp; <c:out value='${linerName1}'/></td>
                                                <td style="border: solid #888 1px;  "> &nbsp;&nbsp;&nbsp;&nbsp;<c:out value='${containerTypeId}'/>   <br>&nbsp;&nbsp;&nbsp;&nbsp;<c:out value='${containerTypeId1}'/></td>
                                                <td style="border: solid #888 1px;  ">&nbsp;&nbsp;&nbsp;&nbsp; 
                                                    <c:if test = "${containerQuantity == 1}" >
                                                        1 * 40 
                                                    </c:if> 
                                                    <c:if test = "${containerQuantity == 2}" >
                                                        1 * 20 
                                                    </c:if> 
                                                    <c:if test = "${containerQuantity == 3}" >
                                                        2 * 20 
                                                    </c:if> 
                                                </td>
                                            </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <table  width='100%' height='80'>
                    <tr>
                        <td>
                            <table  style="border-collapse: collapse;" width='100%' height='100'>
                                <thead height="100">
                                    <tr style="height:50px;white-space: pre; " >
                                        <th style="border: solid #888 1px;"><h1><font size="2">S.No</font></h1></th>
                                <th style="border: solid #888 1px;"><h1><font size="2">Description</font></h1></th>
                                <th style="border: solid #888 1px;"><h1><font size="2">Amount</font></h1></th>
                    </tr>
                    </thead>
                    <tbody height="80">
                        <tr style="height:50px;padding:0px;margin:0px; " >
                            <td align="center" style="border: solid #888 1px;  "> 1</td>
                            <td style="border: solid #888 1px;  "> </td>
                            <td style="border: solid #888 1px;  "> </td>
                        </tr>
                        <tr style="height:50px;padding:0px;margin:0px; " >
                            <td align="center" style="border: solid #888 1px;  ">2</td>
                            <td style="border: solid #888 1px;  "> </td>
                            <td style="border: solid #888 1px;  "> </td>
                        </tr>
                        <tr style="height:50px;padding:0px;margin:0px; " >
                            <td align="center" style="border: solid #888 1px;  ">3</td>
                            <td style="border: solid #888 1px;  "> </td>
                            <td style="border: solid #888 1px;  "> </td>
                        </tr>
                        <tr style="height:50px;padding:0px;margin:0px; " >
                            <td align="center" style="border: solid #888 1px;  ">4</td>
                            <td style="border: solid #888 1px;  "> </td>
                            <td style="border: solid #888 1px;  "> </td>
                        </tr>
                        <tr style="height:50px;padding:0px;margin:0px; " >
                            <td align="center" style="border: solid #888 1px;  ">5</td>
                            <td style="border: solid #888 1px;  "> </td>
                            <td style="border: solid #888 1px;  "> </td>
                        </tr>
                        <tr style="height:50px;padding:0px;margin:0px; " >
                            <td align="center" style="border: solid #888 1px;  ">6</td>
                            <td style="border: solid #888 1px;  "> </td>
                            <td style="border: solid #888 1px;  "> </td>
                        </tr>
                        <tr style="height:50px;padding:0px;margin:0px; " >
                            <td align="center" style="border: solid #888 1px;  ">7</td>
                            <td style="border: solid #888 1px;  "> </td>
                            <td style="border: solid #888 1px;  "> </td>
                        </tr>
                        <tr style="height:50px;padding:0px;margin:0px; " >
                            <td align="center" style="border: solid #888 1px;  ">8</td>
                            <td style="border: solid #888 1px;  "> </td>
                            <td style="border: solid #888 1px;  "> </td>
                        </tr>
                </table>
                </td>
                </tr>
                <tr height="80">
                    <td >
                        <table   >
                            <tr>
                            <br>
                            <br>
                            <br>
                            <td colspan="3"><font size="2">&nbsp;&nbsp;&nbsp;&nbsp;<b>Driver Signature</b> <br>&nbsp;&nbsp;&nbsp;&nbsp;&emsp; (<c:out value='${driverName}'/> )<br>
                                </font>
                            </td><td>
                                &emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;
                                &emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;
                                &emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;&emsp; &emsp;
                            </td>
                            <td colspan="3" ><font size="2" >&nbsp;&nbsp;&nbsp;&nbsp;<b>Trip Inward</b>  <br>&nbsp;&nbsp;&nbsp;&nbsp; (Signature)<br>
                                </font>
                            </td>
                </tr>
                </table>
                </td>
                </tr>
                </table>
                <br>

                </table>
            </div>
            <br>
            <center>

                <input type="button" class="button"  value="Back" onClick="viewTripDetails();" />
                <input type="button" class="button"  value="Print" onClick="print('printContent');" />
                &nbsp;&nbsp;&nbsp;
                <c:if test="${tripDetailsList != null}">
                    <c:forEach items="${tripDetailsList}" var="tripStatus">
                        <c:if test="${tripStatus.tripStatusId < 8}">
                            <a href="viewTripSheets.do?tripSheetId=<c:out value="${tripId}"/>&tripType=1&statusId=8"><input type="button" value="Start Trip"></a>
                            </c:if>
                        </c:forEach>
                    </c:if>

            </center>                 
        </form>
        <script type="text/javascript">
            function viewTripDetails() {
                document.enter.action = '/throttle/viewTripSheets.do?&statusId=10&tripType=1&admin=No';
                document.enter.submit();
            }
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            function get_object(id) {
                var object = null;
                if (document.layers) {
                    object = document.layers[id];
                } else if (document.all) {
                    object = document.all[id];
                } else if (document.getElementById) {
                    object = document.getElementById(id);
                }
                return object;
            }
            //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
            get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
            /* ]]> */
        </script>
    </body>
</html>