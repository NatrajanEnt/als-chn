<%-- 
    Document   : searchVehicleRegistration
    Created on : 16 Jun, 2016, 10:16:05 AM
    Author     : pavithra
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">

    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }

    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }

    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<style>
    #index td {
        color:white;
    }
</style>
<!--setImages(1,0,0,0,0,0);-->
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.VehiclePermit"  text="Vehicle State Permit"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="stores.label.VehiclePermit"  text="Vehicle State Permit"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body onLoad="getVehicleNos();
                    setImages(0, 0, 0, 0, 0, 0);
                    setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
                <form name="viewVehicleDetails"  method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>


                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-bordered" >
                        <tr height="30" id="index">
                            <td colspan="4"  style="background-color:#5BC0DE;"><b>View Vehicle Permit</b></td>&nbsp;&nbsp;&nbsp;                    
                        </tr>
                        <tr>
                            <td align="center" style="border-color:#5BC0DE;padding:16px;">
                                <table>
                                    <tr>
                                        <td>Vehicle Number</td>
                                        <td>
                                            <input type="textbox" id="regno" name="regno" value="<c:out value="${regNo}" />" class="form-control" style="width:260px;height: 38px;">
                                        </td>
                                        <td></td>
                                        <td>

                                        </td>  
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>" type="textbox" class="form-control datepicker" style="width:260px;height: 38px;" onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" value="<c:out value="${toDate}"/>" type="textbox" class="form-control datepicker" style="width:260px;height: 38px;" onclick="ressetDate(this);"></td>
                                    </tr>
                                    <tr>
                                        <td></td> 
                                        <td></td> 
                                        <td><input type="button" class="btn btn-info" name="Search" value="Search" onclick="submitPage(this.name);" style="width:100px;height:35px;">
                                            <input type="button" class="btn btn-info" name="ExportExcel"  value="<spring:message code="trucks.label.ExportExcel"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;"></td>
                                        </td>  
                                        <td></td>  
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>


                    <br>

                    <%
                                int index = 1;

                    %>


                    <c:if test="${vehiclePermitDetails == null }" >
                        <br>
                        <center><font color="red" size="2"> no records found </font></center>
                        </c:if>
                        <c:if test="${vehiclePermitDetails != null }" >
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead><tr>
                                    <th>S.No</th>
                                    <th>Vehicle Number</th>
                                    <th>Permit Amount</th>
                                    <th>Expiry Date</th>
                                    <th>Elapsed Days</th>
                                    <th>Remarks</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>

                                <c:forEach items="${vehiclePermitDetails}" var="veh" >

                                    <tr>

                                        <td  height="30" style="padding-left:30px; "> <%= index++%> </td>
                                        <td  height="30" style="padding-left:30px; "><c:out value="${veh.regNo}" />&ensp;</td>

                                        <c:if test="${veh.vehiclePermit1Details != null }" >
                                            <c:forEach items="${veh.vehiclePermit1Details}" var="permit1" >
                                                <td  height="30" style="padding-left:30px; "><c:out value="${permit1.permitAmt}" />&ensp;</td>
                                            </c:forEach>
                                        </c:if>
                                        <td align="left">
                                            <c:if test="${veh.vehiclePermit1Details != null }" >
                                                <c:forEach items="${veh.vehiclePermit1Details}" var="permit1" >
                                                    <c:if test="${permit1.elapsedDays < 30}">
                                                        <font color="red"><b><c:out value="${permit1.permitExpiryDate}" /></font>
                                                        </c:if>
                                                        <c:if test="${permit1.elapsedDays >= 30}">
                                                            <font color="green"><b><c:out value="${permit1.permitExpiryDate}" />
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                    </td>
                                                    <td align="left">
                                                        <c:if test="${veh.vehiclePermit1Details != null }" >
                                                            <c:forEach items="${veh.vehiclePermit1Details}" var="permit1" >
                                                                <c:if test="${permit1.elapsedDays < 30}">
                                                                    <font color="red"><b><c:out value="${permit1.elapsedDays}" /></font>
                                                                    </c:if>
                                                                    <c:if test="${permit1.elapsedDays >= 30}">
                                                                        <font color="green"><b><c:out value="${permit1.elapsedDays}" />
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:if>
                                                                </td>
                                                                <c:forEach items="${veh.vehiclePermit1Details}" var="permit1" >
                                                                    <td  height="30" style="padding-left:30px; "><c:out value="${permit1.remarks}" />&ensp;</td>
                                                                </c:forEach>

                                                                <td>
                                                                    <c:if test="${veh.vehiclePermit1Details != null }" >
                                                                        <c:forEach items="${veh.vehiclePermit1Details}" var="permit1" >
                                                                            <c:if test="${permit1.permitType == '' || permit1.elapsedDays <= 30 || veh.permitType == '102' || veh.permitType == '103' || permit1.permitType == '101'}">
                                                                                <a href='/throttle/vehicleDetail.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&permitId=<c:out value="${veh.permitid}" />&permitType=<c:out value="${permit1.permitType}" />&type=1&listId=7&fleetTypeId=1'> <img src="/throttle/images/addlogos.png" height="20px;"/></a>
                                                                                    <c:if test="${permit1.permitType == '101' && permit1.elapsedDays <= 30}">
                                                                                    &ensp;<a href='/throttle/viewVehiclePermit.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&permitId=<c:out value="${veh.permitid}" />&permitType=<c:out value="${permit1.permitType}" />&type=1&value=1&listId=7&fleetTypeId=1'><img src="/throttle/images/icon_view.jpg" height="20px;"/></a>
                                                                                    &ensp;<a href='/throttle/viewVehiclePermit.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&permitId=<c:out value="${veh.permitid}" />&permitType=<c:out value="${permit1.permitType}" />&type=1&value=2&listId=7&fleetTypeId=1'> <img src="/throttle/images/edit.png" height="20px;"/></a>
                                                                                    </c:if>
                                                                                </c:if>
                                                                                <c:if test="${permit1.permitType == '101' && permit1.elapsedDays >= 30}">
                                                                                &ensp;<a href='/throttle/viewVehiclePermit.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&permitId=<c:out value="${veh.permitid}" />&permitType=<c:out value="${permit1.permitType}" />&type=1&value=1&listId=7&fleetTypeId=1'><img src="/throttle/images/icon_view.jpg" height="20px;"/></a>
                                                                                &ensp;<a href='/throttle/viewVehiclePermit.do?vehicleId=<c:out value="${veh.vehicleId}" />&regNo=<c:out value="${veh.regNo}" />&permitId=<c:out value="${veh.permitid}" />&permitType=<c:out value="${permit1.permitType}" />&type=1&value=2&listId=7&fleetTypeId=1'> <img src="/throttle/images/edit.png" height="20px;"/></a>
                                                                                </c:if>
                                                                            </c:forEach>
                                                                        </c:if>
                                                                </td>

                                                                </tr>
                                                            </c:forEach>
                                                            </tbody>
                                                            </table>
                                                        </c:if>
                                                        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
                                                        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
                                                        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
                                                        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
                                                        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>




                                                        <script language="javascript" type="text/javascript">
                                                setFilterGrid("table");
                                                        </script>
                                                        <div id="controls">
                                                            <div id="perpage">
                                                                <select onchange="sorter.size(this.value)">
                                                                    <option value="5"  selected="selected">5</option>
                                                                    <option value="10">10</option>
                                                                    <option value="20">20</option>
                                                                    <option value="50">50</option>
                                                                    <option value="100">100</option>
                                                                </select>
                                                                <span>Entries Per Page</span>
                                                            </div>
                                                            <div id="navigation">
                                                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                                            </div>
                                                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                                                        </div>
                                                        <script type="text/javascript">
                                                            var sorter = new TINY.table.sorter("sorter");
                                                            sorter.head = "head";
                                                            sorter.asc = "asc";
                                                            sorter.even = "evenrow";
                                                            sorter.odd = "oddrow";
                                                            sorter.evensel = "evenselected";
                                                            sorter.oddsel = "oddselected";
                                                            sorter.paginate = true;
                                                            sorter.currentid = "currentpage";
                                                            sorter.limitid = "pagelimit";
                                                            sorter.init("table", 5);
                                                        </script>


                                                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                                                        </body>
                                                        <script type="text/javascript">
                                                            function submitPage(value) {

                                                                if (value == 'add') {
                                                                    document.viewVehicleDetails.action = '/throttle/addVehiclePage.do?fleetTypeId=1';
                                                                    document.viewVehicleDetails.submit();
                                                                } else {
                                                                    if (value == 'ExportExcel') {

                                                                        document.viewVehicleDetails.action = '/throttle/searchVehiclePage.do?listId=7&param=ExportExcel';
                                                                        //                            document.viewVehicleDetails.action = '/throttle/vehicleList.do?param=ExportExcel';
                                                                        document.viewVehicleDetails.submit();
                                                                    } else {
                                                                        document.viewVehicleDetails.action = '/throttle/searchVehiclePage.do?listId=7';
                                                                        document.viewVehicleDetails.submit();
                                                                    }
                                                                }
                                                            }


                                                            function setDefaultVals(regNo, typeId, mfrId, usageId, groupId) {

                                                                if (regNo != 'null') {
                                                                    document.viewVehicleDetails.regNo.value = regNo;
                                                                }
                                                                if (typeId != 'null') {
                                                                    document.viewVehicleDetails.typeId.value = typeId;
                                                                }
                                                                if (mfrId != 'null') {
                                                                    document.viewVehicleDetails.mfrId.value = mfrId;
                                                                }
                                                                if (usageId != 'null') {
                                                                    document.viewVehicleDetails.usageId.value = usageId;
                                                                }
                                                                if (groupId != 'null') {
                                                                    document.viewVehicleDetails.groupId.value = groupId;
                                                                }
                                                            }

                                                            function getVehicleNos() {
                                                                //onkeypress='getList(sno,this.id)'
                                                                var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
                                                            }

                                                        </script>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <%@ include file="/content/common/NewDesign/settings.jsp" %>



<!--<td  height="30" style="padding-left:30px; "><c:out value="${veh.permitType}" /> </td>-->
                            <!--<td  height="30" style="padding-left:30px; "><c:out value="${veh.permitNo}" /></td>-->
                            <!--<td  height="30" style="padding-left:30px; "><c:out value="${veh.permitAmount}" /></td>-->
                            <!--<td  height="30" style="padding-left:30px; "><c:out value="${veh.permitPaidDate}" /></td>-->
                            <!--<td  height="30" style="padding-left:30px; "><c:out value="${veh.permitExpiryDate}" /></td>-->
                            <!--<td  height="30" style="padding-left:30px; "><c:out value="${veh.elapsedDays}" /></td>-->   