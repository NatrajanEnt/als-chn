

<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->
<body>
    <script>

        var billNo = '<%= request.getAttribute("billNo") %>';
        //        alert(counterId);
        if (billNo != '' && billNo != 'null' && billNo != null) {
            window.open('printBillDetails.do?billNo=' + billNo, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

        function printBill(billNo) {
            window.open('printBillDetails.do?billNo=' + billNo, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }


        var woId = '<%= request.getAttribute("woId") %>';
        //        alert(counterId);
        if (woId != '' && woId != 'null' && woId != null) {
            window.open('printWoDetails.do?woId=' + woId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }

        function printWo(woId) {
            window.open('viewRCWO.do?supplyId=' + woId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }


    </script>


    <form name="rcList" method="post">                    
        <%--<%@ include file="/content/common/path.jsp" %>--%>            
        <!-- pointer table -->

        <!-- message table -->           
        <%@ include file="/content/common/message.jsp" %>    

        <div class="pageheader">
            <h2><i class="fa fa-edit"></i>  <spring:message code="recondition.label.RCList"  text="RCList"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="recondition.label.Recondition"  text="Recondition"/></a></li>
                    <li class="active"><spring:message code="recondition.label.RCList"  text="RCList"/></li>
                </ol>
            </div>
        </div>
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <c:set var="mont" value="(Months)" />
                    <c:if test = "${rcList != null}" >
                        <!--<table align="center" border="0" cellpadding="0" cellspacing="0" width="750" id="bg" class="border">-->              
                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                                <!--<tr>
                                    <th colspan="8"><spring:message code="recondition.label.RCList"  text="RCList"/> </th>
                                </tr>  -->

                                <tr>
                                    <th  height="30"><spring:message code="recondition.label.Sl.No"  text="Sl.No"/></th>
                                    <th  height="30"><spring:message code="recondition.label.CompanyName"  text="default text"/></th>
                                    <th  height="30"><spring:message code="recondition.label.ItemName"  text="default text"/></th>
                                    <th  height="30"><spring:message code="recondition.label.Tyre No"  text="Tyre No"/></th>                       
                                    <th  height="30"><spring:message code="recondition.label.Workorder No"  text="Workorder No"/></th>                       
                                    <th  height="30"><spring:message code="recondition.label.CreatedDate"  text="default text"/></th>
                                    <th  height="30"><spring:message code="recondition.label.Status"  text="Status"/></th>
                                    <th  height="30"><spring:message code="recondition.label.Action"  text="Action "/></th>
                                </tr>     
                            </thead>


                            <% int index = 0;%>                

                            <%index = 0;
                                        String classText = "";
                                        int oddEven = 0;
                                        int sno =1;
                            %>
                            <c:forEach items="${rcList}" var="rc"> 

                                <%

                                            oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                %>
                                <tr>
                                    <td  height="30"><%=sno%></td>
                                    <td  height="30"><c:out value="${rc.companyName}"/></td>
                                    <td  height="30"><c:out value="${rc.itemName}"/></td>
                                    <td  height="30"><c:out value="${rc.tyreNo}"/></td>
                                    <td  height="30"><c:out value="${rc.woId}"/></td>
                                    <td  height="30"><c:out value="${rc.createdDate}"/></td>  
                                    <td  height="30">
                                        <c:if test = "${rc.rcQueueId!=0 && rc.woId==null && rc.billNo==null}" >
                                            RC Created        
                                        </c:if>
                                        <c:if test = "${rc.rcQueueId!=0 && rc.woId!=null && rc.billNo==null}" >
                                            Work Order Created        
                                        <td ><a href='#' onclick='printWo(<c:out value="${rc.woId}" />)' >PrintWO</a></td>
                                    </c:if>
                                    <c:if test = "${rc.activeInd=='N' && rc.rcstatus=='Y' && rc.billNo!=null}" >
                                        Completed        
                                        <td ><a href='#' onclick='printBill(<c:out value="${rc.billNo}" />)' >PrintBill</a></td>
                                    </c:if>

                                </tr>
                                <%
                                            index++;
                                            sno++;
                                %>
                            </c:forEach>                 
                        </table>     

                    </c:if>             
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>


                    </form>
                    </body>
                </div>
            </div>
        </div>
        <%@ include file="../common/NewDesign/settings.jsp" %>
