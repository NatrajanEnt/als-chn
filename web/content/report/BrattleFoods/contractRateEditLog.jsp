<%--
    Document   : invoiceEditLogDetails
    Created on : Jun 8, 2016, 4:37:58 PM
    Author     : hp
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<script  type="text/javascript" src="js/jq-ac-script.js"></script>
        
    <html>
    <head>
        
    <script type="text/javascript">    
      // new customer ajax start

      // Use the .autocomplete() method to compile the list based on input from user    
       $(document).ready(function() {
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                         $('#customerName').val('');   
                         $('#customerId').val('');   
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
               
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
            
        function submitPage(){
        document.log.action = '/throttle/searchContractRateLog.do';
        document.log.submit();
        }
            </script>
    </head>
    <body onload="setValue();
            sorter.size(10);">
        <form name="log" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <br>
            <table width="1000" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="10" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Contract Rate Log Details</li>
                            </ul>
                            <div id="first">
                                <table width="950px" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >

                                    <tr colspan="2">
                                        <td  height="30"><font color="red">*</font>Customer Name </td>
                                         <td>
                                        <input type="hidden" class="textbox" name="customerId" id="customerId" style="width: 110px" value="<c:out value="${customerId}"/>"/>
                                        <input type="text" class="textbox" name="customerName" style="height:15px; width:110;"  id="customerName" value="<c:out value="${customerName}"/>"/>
                                    </td>
                                    <!--<td><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>-->
                                    <td><input type="button" class="button" name="Search"   value="Search" onclick="submitPage();"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

            <br>
            <br>

              <table align="center" border="0" id="table"  class="sortable" width="90%">
                <thead>
                    <tr  height="80" width="180" >
                        <th><h4>S.No</h4></th>
                        <th><h4>Customer Name</h4></th>
                        <th><h4>Route</h4></th>
                        <th><h4>Vehicle Type</h4></th>
                        <th><h4>Load Type</h4></th>
                        <th><h4>Container Type</h4></th>
                        <th><h4>Container QTY</h4></th>
                        <th><h4>Rate With Reefer Old</h4></th>
                        <th><h4>Rate With Reefer New</h4></th>
                        <th><h4>Rate Without Reefer Old</h4></th>
                        <th><h4>Rate Without Reefer New</h4></th>
                        <th><h4>Active Ind Old</h4></th>
                        <th><h4>Active Ind New</h4></th>
                        <th><h4>Created By</h4></th>
                        <th><h4>Created_on</h4></th>
                       
                        

                    </tr>
                </thead>
                <tbody>


                    <% int index = 1,sno = 1;%>
                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>

                    <c:forEach items="${contractRateEditLogDetails}" var="log">


                        <tr height="50" width="150px;">
                            <td align="center" class="<%=classText%>"><%=sno%></td>
                            <td align="center" class="<%=classText%>">&nbsp;<c:out value="${log.custName}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;<c:out value="${log.routeName}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;<c:out value="${log.vehicleType}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;<c:out value="${log.loadTypeName}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;<c:out value="${log.containerType}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;<c:out value="${log.containerQty}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.rateWithReeferOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.rateWithReeferNew}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.rateWithoutReeferOld}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.rateWithoutReeferNew}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;&nbsp;&nbsp;<c:out value="${log.validStatusOld}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;&nbsp;&nbsp;<c:out value="${log.validStatusNew}"/></td>
                            <td align="center" class="<%=classText%>">&nbsp;&nbsp;&nbsp;<c:out value="${log.createdBy}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${log.createdOn}"/></td>


                        </tr>
                   <%
                   index++;
                     sno++;
                        %>
                    </c:forEach>

                </tbody>


            </table>
            <br/>
            <br/>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <br/>
            <br/>
            <br/>
            <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table",0);
        </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>