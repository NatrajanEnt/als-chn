<%-- 
    Document   : driverSettlementReportExcel
    Created on : M 13, 2019, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "orderStatusReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
                        <c:if test="${orderStatusReportDetails != null}">
                         <table align="center" id="table" class="table table-info mb30 table-hover"style="width:100%;" >
                            <thead height="30">
                                <tr>
                                    <th align="center" rowspan="2">Sno</th>
                                    <th align="center" rowspan="2">Date</th>
                                    <th align="center" rowspan="2">Day</th>
                                    <th align="center" colspan="3">Movement Start Within</th>
                                    <th align="center" rowspan="2">No Of Export Movements using CA Log Vehicles</th>
                                   

                                </tr> <tr>
                                    <th align="center">Less than 1 hr</th>
                                    <th align="center">1 to 2 hrs</th>
                                    <th align="center">abv 2 hrs</th>
                                </tr>
                            </thead>
                            <tbody>
                                <%int i=0;%>
                                <c:forEach items="${orderStatusReportDetails}" var="os">
                                    <%i++;%>
                                    <tr>
                                        <td><%=i%></td>
                                        <td><c:out value="${os.date}"/></td>
                                        <td><c:out value="${os.day}"/></td>
                                        <td><c:out value="${os.timeSlot1}"/></td>
                                        <td><c:out value="${os.timeSlot2}"/></td>
                                        <td><c:out value="${os.timeSlot3}"/></td>
                                        <td><c:out value="${os.totalCount}"/></td>
                                    </tr>

                                </c:forEach>
                            </tbody>
                </table>
            </c:if>
</form>
    </body>
</html>