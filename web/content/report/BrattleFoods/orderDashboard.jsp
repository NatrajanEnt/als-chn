<%@ include file="/content/common/NewDesign/header.jsp" %>
	<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
	

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value) {

                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
//                } else if (document.getElementById('toDate').value == '') {
//                    alert("Please select to Date");
//                    document.getElementById('toDate').focus();
                }
                else {
                    if (value == "ExportExcel") {
                        document.BPCLTransaction.action = '/throttle/handleOrderDashBoardReport.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else {
                        document.BPCLTransaction.action = '/throttle/handleOrderDashBoardReport.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>
<%
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            Date date = new Date();
            String fDate = "";
            String tDate = "";
            if (request.getAttribute("fromDate") != null) {
                fDate = (String) request.getAttribute("fromDate");
            } else {
                fDate = dateFormat.format(date);
            }
            if (request.getAttribute("toDate") != null) {
                tDate = (String) request.getAttribute("toDate");
            } else {
                tDate = dateFormat.format(date);
            }
        %>


    </head>
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Dashboard" text="Dashboard"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Dashboard" text="Dashboard"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Dashboard" text="Dashboard"/></li>
		
		                </ol>
		            </div>
        </div>
        
        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                
    <body>
        <form name="BPCLTransaction" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
<table class="table table-info mb30 table-hover" id="report" >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >Order DashBoard</th>
		</tr>
               </thead>
            
                               <table class="table table-info mb30 table-hover"  >
                                   <thead>

                                    <tr>
                                        <th><font color="red">*</font>Date</th>
                                        <th height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:240px;height:40px" value="<c:out value="${fromDate}"/>" ></th>
                                        <th><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="Search"></th>
                                    </tr>
                                   </thead>
                                    <tr>
                                        <!--<td><input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>-->
                                        <td></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            
            
            <div id="printContent">
                <%--<c:if test="${Orderdetails != null}">--%>
                 <table class="table table-info mb30 table-hover sortable" id="table" >

                    <thead>
                        <tr height="50">
                            <th align="center">S.No</th>
                    <th align="center">No Of Orders</th>
                    <th align="center">No Of Open Trips</th>
                    <th align="center">No Of Closed Trips</th>
                    <th align="center">No Of Invoice Generated</th>
                    </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <%--<c:forEach items="${Orderdetails}" var="BPCLTD">--%>
                            <%
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                            %>

                            <tr>
                                <td align="center" width="30"><%=index++%></td>
                                <td  width="30" align="center" ><c:out value="${ordersPlaced}"/></td>
                                <td  width="30" align="center" ><c:out value="${openTrip}"/></td>
                                <td width="30" align="center" ><c:out value="${closedTrip}"/></td>
                                <td width="30" align="center"  ><c:out value="${invoiceGenerated}"/></td>
                            </tr>
                        <%--</c:forEach>--%>
                    </tbody>
                </table>
                <%--</c:if>--%>
            </div>
            
            
            <div id="avgMonth">
                 <table class="table table-info mb30 table-hover sortable" id="table" >
                    <thead>
                     <tr height="50">

                    <th align="center">Avg No Of Orders</th>
                    <th align="center">Avg No Of Open Trips</th>
                    <th align="center">Avg No Of Closed Trips</th>
                    <th align="center">Avg No Of Invoice Generated</th>
                    </tr>
                    </thead>
                    <tbody>
                        <% int index1 = 1;%>
                            <%
                                String classText1 = "";
                                int oddEven1 = index1 % 2;
                                if (oddEven1 > 0) {
                                    classText1 = "text2";
                                } else {
                                    classText1 = "text1";
                                }
                            %>

                            <tr>

                                <td  width="30" align="center" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="0" value="${totalOrderInCurrentMonth/totalDays}" /></td>
                                <td  width="30" align="center" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="0" value="${totalOpenTripInCurrentMonth/totalDays}" /></td>
                                <td width="30" align="center" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="0" value="${totalClosedTripInCurrentMonth/totalDays}" /></td>
                                <td width="30" align="center"  ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="0" value="${totalInvoiceInCurrentMonth/totalDays}" /></td>
                            </tr>
                    </tbody>
                </table>
            </div>
                            
                            
            <div id="month1">
                 <table class="table table-info mb30 table-hover sortable" id="table" >
                    <thead>
                    <tr height="50">
                    <th align="center">Report For</th>
                    <th align="center">No Of Orders</th>
                    <th align="center">No Of Open Trips</th>
                    <th align="center">No Of Closed Trips</th>
                    <th align="center">No Of Invoice Generated</th>
                    </tr>
                    </thead>
                    <tbody>
                        <% int index13 = 1;%>
                            <%
                                String classText13 = "";
                                int oddEven13 = index13 % 2;
                                if (oddEven13 > 0) {
                                    classText13 = "text2";
                                } else {
                                    classText13 = "text1";
                                }
                            %>

                            <tr>
                                <td align="center" width="30"><c:out value="${curMonth}"/></td>
                                <td  width="30" align="center" ><c:out value="${totalOrderInCurrentMonth}"/></td>
                                <td  width="30" align="center" ><c:out value="${totalOpenTripInCurrentMonth}"/></td>
                                <td width="30" align="center" ><c:out value="${totalClosedTripInCurrentMonth}"/></td>
                                <td width="30" align="center"  ><c:out value="${totalInvoiceInCurrentMonth}"/></td>
                            </tr>
                    </tbody>
                </table>
            </div> 
            
            
            <div id="month2">
                 <table class="table table-info mb30 table-hover sortable" id="table" >
                
                    <thead>
                        <tr height="50">
                            <th align="center">Report For</th>
                    <th align="center">No Of Orders</th>
                    <th align="center">No Of Open Trips</th>
                    <th align="center">No Of Closed Trips</th>
                    <th align="center">No Of Invoice Generated</th>
                    </tr>
                    </thead>
                    <tbody>
                        <% int index2 = 1;%>
                            <%
                                String classText2 = "";
                                int oddEven2 = index2 % 2;
                                if (oddEven2 > 0) {
                                    classText2 = "text2";
                                } else {
                                    classText2 = "text1";
                                }
                            %>

                            <tr>
                                <td align="center" width="30"><c:out value="${prevMonth}"/></td>
                                <td  width="30" align="center" ><c:out value="${totalOrderInPreviousMonth}"/></td>
                                <td  width="30" align="center" ><c:out value="${totalOpenTripInPreviousMonth}"/></td>
                                <td width="30" align="center" ><c:out value="${totalClosedTripInPreviousMonth}"/></td>
                                <td width="30" align="center"  ><c:out value="${totalInvoiceInPreviousMonth}"/></td>
                            </tr>
                    </tbody>
                </table>
            </div> 
            
            
                      
        </form>
    </body>

</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>



