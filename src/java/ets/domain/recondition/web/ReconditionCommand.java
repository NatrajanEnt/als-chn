/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
package ets.domain.recondition.web;

public class ReconditionCommand {

    String itemId = "";
    String itemName = "";
    String mfrCode = "";
    String paplCode = "";
    String rcNumber = "";
    String remarks = "";
    String gdType = "";
    String createdDate = "";
    String elapsedDays = "";
    String jobCardId = "";
    String vendorId = "";
    String vendorName = "";
    String rcstatus = "";
    String billAmount = "";
    String receivedDate = "";
    String woId = "";
    String[] rcNumbers = null;
    String[] rcItemIds = null;
    String[] itemAmounts = null;
    String[] rcStatuses = null;
    String[] itemIds = null;
    String[] vendorIds = null;
    String[] selectedIndex = null;
    String[] rcQueueIds = null;
    String[] rcNo = null;
    String[] categoryIds = null;
    String[] sparesAmounts = null;
    String[] vats = null;
    String[] cgst = null;
    String[] sgst = null;
    String[] igst = null;
    String[] lcgst = null;
    String[] lsgst = null;
    String[] ligst = null;
    String[] laborwithTax = null;
    String[] sparesWithTaxs = null;
    String[] laborAmounts = null;
    String[] tyreIds = null;
    String[] positionIds = null;
    String[] regNo = null;
    
    //Hari
    String rcWorkId=null;
    String mrsId=null;
    String[] sparesAmountsExternal = null;
    String customerTypeId=null;
    String customerName=null;
    String customerAddress=null;
    String mobileNo=null;

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }
    

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }


    public String[] getSparesAmountsExternal() {
        return sparesAmountsExternal;
    }

    public void setSparesAmountsExternal(String[] sparesAmountsExternal) {
        this.sparesAmountsExternal = sparesAmountsExternal;
    }

    

    public String getMrsId() {
        return mrsId;

    }

    public void setMrsId(String mrsId) {
        this.mrsId = mrsId;
    }


    public String getRcWorkId() {
        return rcWorkId;
    }

    public void setRcWorkId(String rcWorkId) {
        this.rcWorkId = rcWorkId;
    }


    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getElapsedDays() {
        return elapsedDays;
    }

    public void setElapsedDays(String elapsedDays) {
        this.elapsedDays = elapsedDays;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getRcNumber() {
        return rcNumber;
    }

    public void setRcNumber(String rcNumber) {
        this.rcNumber = rcNumber;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public String[] getRcNumbers() {
        return rcNumbers;
    }

    public void setRcNumbers(String[] rcNumbers) {
        this.rcNumbers = rcNumbers;
    }

    public String[] getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(String[] vendorIds) {
        this.vendorIds = vendorIds;
    }

    public String[] getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(String[] selectedIndex) {
        this.selectedIndex = selectedIndex;
    }

    public String[] getRcQueueIds() {
        return rcQueueIds;
    }

    public void setRcQueueIds(String[] rcQueueIds) {
        this.rcQueueIds = rcQueueIds;
    }

    public String[] getItemAmounts() {
        return itemAmounts;
    }

    public void setItemAmounts(String[] itemAmounts) {
        this.itemAmounts = itemAmounts;
    }

    public String[] getRcItemIds() {
        return rcItemIds;
    }

    public void setRcItemIds(String[] rcItemIds) {
        this.rcItemIds = rcItemIds;
    }

    public String[] getRcStatuses() {
        return rcStatuses;
    }

    public void setRcStatuses(String[] rcStatuses) {
        this.rcStatuses = rcStatuses;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String getRcstatus() {
        return rcstatus;
    }

    public void setRcstatus(String rcstatus) {
        this.rcstatus = rcstatus;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getGdType() {
        return gdType;
    }

    public void setGdType(String gdType) {
        this.gdType = gdType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String[] getRcNo() {
        return rcNo;
    }

    public void setRcNo(String[] rcNo) {
        this.rcNo = rcNo;
    }

    public String[] getCategoryIds() {
        return categoryIds;
    }

    public void setCategoryIds(String[] categoryIds) {
        this.categoryIds = categoryIds;
    }

    public String[] getLaborAmounts() {
        return laborAmounts;
    }

    public void setLaborAmounts(String[] laborAmounts) {
        this.laborAmounts = laborAmounts;
    }

    public String[] getSparesAmounts() {
        return sparesAmounts;
    }

    public void setSparesAmounts(String[] sparesAmounts) {
        this.sparesAmounts = sparesAmounts;
    }

    public String[] getSparesWithTaxs() {
        return sparesWithTaxs;
    }

    public void setSparesWithTaxs(String[] sparesWithTaxs) {
        this.sparesWithTaxs = sparesWithTaxs;
    }

    public String[] getVats() {
        return vats;
    }

    public void setVats(String[] vats) {
        this.vats = vats;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public String[] getPositionIds() {
        return positionIds;
    }

    public void setPositionIds(String[] positionIds) {
        this.positionIds = positionIds;
    }

    public String[] getTyreIds() {
        return tyreIds;
    }

    public void setTyreIds(String[] tyreIds) {
        this.tyreIds = tyreIds;
    }

    public String[] getRegNo() {
        return regNo;
    }

    public void setRegNo(String[] regNo) {
        this.regNo = regNo;
    }

    public String[] getCgst() {
        return cgst;
    }

    public void setCgst(String[] cgst) {
        this.cgst = cgst;
    }

    public String[] getSgst() {
        return sgst;
    }

    public void setSgst(String[] sgst) {
        this.sgst = sgst;
    }

    public String[] getIgst() {
        return igst;
    }

    public void setIgst(String[] igst) {
        this.igst = igst;
    }

    public String[] getLcgst() {
        return lcgst;
    }

    public void setLcgst(String[] lcgst) {
        this.lcgst = lcgst;
    }

    public String[] getLsgst() {
        return lsgst;
    }

    public void setLsgst(String[] lsgst) {
        this.lsgst = lsgst;
    }

    public String[] getLigst() {
        return ligst;
    }

    public void setLigst(String[] ligst) {
        this.ligst = ligst;
    }

    public String[] getLaborwithTax() {
        return laborwithTax;
    }

    public void setLaborwithTax(String[] laborwithTax) {
        this.laborwithTax = laborwithTax;
    }


       
}    
    