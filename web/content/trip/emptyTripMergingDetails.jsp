<%--
    Document   : CompanyWiseProfitability
    Created on : Oct 31, 2013, 11:09:54 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">    
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">


            function viewConsignmentDetails(orderId) {
            window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }

    $(document).ready(function() {
    $("#datepicker").datepicker({
    showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

    });
    });
            $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
            });
            });</script>

</head>


<script type="text/javascript">
            $(document).ready(function() {
    // Use the .autocomplete() method to compile the list based on input from user
    $('#customerName').autocomplete({
    source: function(request, response) {
    $.ajax({
    url: "/throttle/getCustomerDetails.do",
            dataType: "json",
            data: {
            customerName: request.term
            },
            success: function(data, textStatus, jqXHR) {
            var items = data;
                    response(items);
            },
            error: function(data, type) {
            console.log(type);
            }
    });
    },
            minLength: 1,
            select: function(event, ui) {
            $("#customerName").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('~');
                    $('#customerId').val(tmp[0]);
                    $('#customerName').val(tmp[1]);
                    return false;
            }
    }).data("autocomplete")._renderItem = function(ul, item) {
    var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
            .data("item.autocomplete", item)
            .append("<a>" + itemVal + "</a>")
            .appendTo(ul);
    };
    });</script>

<script>


            function submitPageForTripMerging() {
            if (document.getElementById("tripMergingRemarks").value == ""){
                    alert("Please Enter the remarks");
                    document.getElementById("tripMergingRemarks").focus();
            } else{
                $("#saveTripMerging").hide();
              document.CNoteSearch.action = "/throttle/saveTripMergingDetails.do";
                document.CNoteSearch.submit();  
            }
            }

    


    



    function viewTripDetails(tripId) {
    window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
    window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewConsignmentDetails(orderId) {
    window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }



    function searchPage() {
    document.CNoteSearch.action = "/throttle/emptyTripMergingDetails.do";
            document.CNoteSearch.submit();
    }
</script>

<body>
    <%
                String menuPath = "Operation >> Empty Trip Merging";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="CNoteSearch" method="post" enctype="multipart">
        <%@ include file="/content/common/path.jsp" %>
        <br>
        <br>


        <br>
        <c:if test = "${emptyTripList != null}" >
            <table width="100%" align="center" border="0" id="table" class="sortable"  style="width:1150px;" >
                <thead>
                    <tr height="40">
                        <th><h3>Sno</h3></th>
                        <th><h3>Consignment No</h3></th>
                        <th><h3>Trip Code</h3></th>
                        <th><h3>Vehicle No</h3></th>
                        <th><h3>Route </h3></th>
                        <th><h3>Trip Start Date</h3> </th>
                        <th><h3>Trip Start Time</h3> </th>
                        <th><h3>Trip End Date</h3></th>
                        <th><h3>Trip End Time</h3></th>
                        <th><h3>Trip Type</h3></th>
                        <th><h3>Trip Sequence</h3></th>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${emptyTripList}" var="cnl">

                        <tr height="30">
                            <td align="left" ><%=sno%></td>

                            <td align="left" > <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentId}"/>');"><c:out value="${cnl.consignmentNote}"/></a></td>
                            <td align="left" >
                                <input type="hidden" name="tripId" id="tripId<%=index%>" value="<c:out value="${cnl.tripId}"/>"/>
                                <a href="#" onclick="viewTripDetails('<c:out value="${cnl.tripSheetId}"/>');"><c:out value="${cnl.tripCode}"/></a>
                            </td>
                            <td align="left" >
                                <a href="#" onclick="viewVehicleDetails('<c:out value="${cnl.vehicleId}"/>')"><c:out value="${cnl.regNo}"/></a>
                            </td>

                            <td align="left" ><c:out value="${cnl.routeInfo}"/></td>
                            <td align="left" ><c:out value="${cnl.tripStartDate}"/></td>
                            <td align="left" ><c:out value="${cnl.tripStartTime}"/></td>
                            <td align="left" ><c:out value="${cnl.tripEndDate}"/></td>
                            <td align="left" ><c:out value="${cnl.tripEndTime}"/></td>
                            <td align="left" >
                                <c:if test="${cnl.tripType == 'E'}">
                                    Empty Trip
                                </c:if>
                                <c:if test="${cnl.tripType == 'L'}">
                                    Loaded Trip
                                </c:if>
                            </td>
                            <td align="left" >
                                <input type="hidden" name="tripType" id="tripType<%=index%>" value="<c:out value="${cnl.tripType}"/>" />
                                <input type="text" name="tripSequence" id="tripSequence<%=index%>" value="<%=sno%>" onchange="setChequeSequence('<%=index%>');" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                            </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>

        <center>
            Trip Merging Remarks :<textarea name="tripMergingRemarks" id="tripMergingRemarks" cols="30" rows="5"></textarea><br>    
            &nbsp;&nbsp;<input type="button" class="button" name="trip" id="saveTripMerging"  value="save" onclick="submitPageForTripMerging()"/>
        </center>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
