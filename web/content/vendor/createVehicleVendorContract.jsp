<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>

<script>
    $(document).ready(function() {
        // Get the table object to use for adding a row at the end of the table
        var $itemsTable = $('#itemsTable6');

        // Create an Array to for the table row. ** Just to make things a bit easier to read.
        var count = 0;
        var rowTemp = [
            '<tr class="item-row">',
            '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
            '<td><input name="localRouteContractCode" id="localRouteContractCode" value="" class="form-control" style="width:120px"/><input type="hidden" name="loadTypeId" id="loadTypeId" value="2"/><input type="hidden" name="containerTypeId" id="containerTypeId" value="0"/><input type="hidden" name="containerQty" id="containerQty" value="0"/><input type="hidden" name="localVehicleTypeId" id="localVehicleTypeId" value="0"/>',
            '<input  type="hidden" name="contractCategoryId" id="contractCategoryId" value="4" class="form-control" style="width:150px" ></td>',
            '<td><input type="hidden" name="localPickupPointId" id="localPickupPointId" value="" class="form-control"  style="width: 120px;"/><input name="localPickupPoint" id="localPickupPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" /></td>',
            '<td><input name="localDropPoint" id="localDropPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/><input type="hidden" name="localDropPointId" id="localDropPointId" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="localPointRouteId" id="localPointRouteId" value="" class="form-control"  style="width: 120px;"/></td>',
            '<td><input name="localTotalKm" id="localTotalKm" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);" readonly/><input type="hidden" name="localTotalHrs" id="localTotalHrs" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="localTotalMinutes" id="localTotalMinutes" value="" class="form-control"  style="width: 120px;"/></td>',
            '<td><input name="fromCBM" id="fromCBM" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>',
            '<td><input name="toCBM" id="toCBM" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
            '<td><input name="flatRate" id="flatRate" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
            '</tr>'
        ].join('');

        // Add row to list and allow user to use autocomplete to find items.
        $("#addRow6").bind('click', function() {
            var $row = $(rowTemp);
            count++;
//                alert("Add Row"+count);
            // save reference to inputs within row
            var $localRouteContractCode = $row.find('#localRouteContractCode');
            var $localVehicleTypeId = $row.find('#localVehicleTypeId');
            var $localPickupPointId = $row.find('#localPickupPointId');
            var $localPickupPoint = $row.find('#localPickupPoint');
            var $localDropPoint = $row.find('#localDropPoint');
            var $localDropPointId = $row.find('#localDropPointId');
            var $localPointRouteId = $row.find('#localPointRouteId');
            var $localTotalKm = $row.find('#localTotalKm');
            var $localTotalHrs = $row.find('#localTotalHrs');
            var $localTotalMinutes = $row.find('#localTotalMinutes');
            var $fromCBM = $row.find('#fromCBM');
            var $toCBM = $row.find('#toCBM');
            var $flatRate = $row.find('#flatRate');

            if ($('#localRouteContractCode:last').val() !== '') {
                // apply autocomplete widget to newly created row
                $row.find('#localPickupPoint').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCCPointToPointWeightRouteName.do",
                            dataType: "json",
                            data: {
                                elementName: $(this.element).prop("id"),
                                elementValue: request.term,
                                rowCount: count,
                                ptpwDropPoint: $row.find('#localPointRouteId').val()
                            },
                            success: function(data, textStatus, jqXHR) {
                                //console.log( data);
                                var items = data;
                                if (items == '') {

                                    $row.find("#localPickupPoint").val('');
                                    $row.find("#localPickupPointId").val('');
                                    $row.find('#localDropPoint').val('');
                                    $row.find('#localDropPointId').val('');
                                    $row.find('#localTotalKm').val('');
                                    $row.find('#localTotalHrs').val('');
                                    $row.find('#localTotalMinutes').val('');
                                    $row.find('#localPointRouteId').val('');
                                    $row.find('#fromCBM').val('');
                                    $row.find('#toCBM').val('');
                                    $row.find('#flatRate').val('');
                                } else {
                                    //alert(items);
                                    response(items);
                                }
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var $itemrow = $(this).closest('tr');
                        // Populate the input fields from the returned values
                        var value = ui.item.Name;
                        //                                alert(value);
                        var tmp = value.split('-');
                        $itemrow.find('#localPickupPoint').val(tmp[0]);
                        $itemrow.find('#localPickupPointId').val(tmp[1]);

                        $itemrow.find('#localDropPoint').val('');
                        $itemrow.find('#localDropPointId').val('');
                        $itemrow.find('#localTotalKm').val('');
                        $itemrow.find('#localTotalHrs').val('');
                        $itemrow.find('#localTotalMinutes').val('');
                        $itemrow.find('#localPointRouteId').val('');
                        $itemrow.find('#fromCBM').val('');
                        $itemrow.find('#toCBM').val('');
                        $itemrow.find('#flatRate').val('');
                        $itemrow.find('#localDropPoint').focus();
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[0] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
                $row.find('#localDropPoint').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCCPointToPointWeightRouteName.do",
                            dataType: "json",
                            data: {
                                elementName: $(this.element).prop("id"),
                                elementValue: request.term,
                                rowCount: count,
                                ptpwPickupPoint: $row.find('#localPickupPointId').val()
                            },
                            success: function(data, textStatus, jqXHR) {
                                //console.log( data);
                                var items = data;
                                if (items == '') {

                                    $row.find("#localDropPoint").val('');
                                    $row.find("#localDropPointId").val('');
                                    $row.find("#localPointRouteId").val('');
                                    $row.find("#localTotalKm").val('');
                                    $row.find("#localTotalHrs").val('');
                                    $row.find("#localTotalMinutes").val('');
                                    $row.find("#fromCBM").val('');
                                    $row.find("#toCBM").val('');
                                    $row.find("#flatRate").val('');
                                    $row.find('#localDropPoint').focus();
                                } else {
                                    //alert(items);
                                    response(items);
                                }
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 0,
                    select: function(event, ui) {
                        var $itemrow = $(this).closest('tr');
                        // Populate the input fields from the returned values
                        var value = ui.item.Name;
                        //                                alert(value);
                        var tmp = value.split('-');
                        $itemrow.find('#localDropPoint').val(tmp[0]);
                        $itemrow.find('#localDropPointId').val(tmp[1]);
                        $itemrow.find('#localTotalKm').val(tmp[2]);
                        $itemrow.find('#localTotalHrs').val(tmp[3]);
                        $itemrow.find('#localTotalMinutes').val(tmp[4]);
                        $itemrow.find('#localPointRouteId').val(tmp[5]);
                        $itemrow.find('#flatRate').focus();
                        return false;
                    }
                    // Format the list menu output of the autocomplete
                }).data("ui-autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[0] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            //.append( "<a>"+ item.Name + "</a>" )
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

                // Add row after the first row in table
                $('.item-row:last', $itemsTable).after($row);
                $(localRouteContractCode).focus();

            } // End if last itemCode input is empty
            return false;
        });

    });// End DOM

    // Remove row when clicked
    $("#deleteRow").live('click', function() {
        $(this).parents('.item-row').remove();
        // Hide delete Icon if we only have one row in the list.
        if ($(".item-row").length < 2)
            $("#deleteRow").hide();
    });




    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#localPickupPoint').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCCPointToPointWeightRouteName.do",
                    dataType: "json",
                    data: {
                        elementName: $(this.element).prop("id"),
                        elementValue: request.term,
                        rowCount: 1,
                        ptpwDropPoint: document.getElementById("localDropPoint").value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        //alert(items);
                        if (items == '') {
                            alert("Invalid Route Point");
                            $("#localPickupPoint").val('');
                            $("#localPickupPointId").val('');
                            $('#localDropPoint').val('');
                            $('#localDropPointId').val('');
                            $('#localTotalKm').val('');
                            $('#localTotalHrs').val('');
                            $('#localTotalMinutes').val('');
                            $('#localPointRouteId').val('');
                            $('#fromCBM').val('');
                            $('#toCBM').val('');
                            $('#flatRate').val('');
                        } else {
                            //alert(items);
                            response(items);
                        }
                    },
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#localPickupPoint").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#localPickupPoint').val(tmp[0]);
                $itemrow.find('#localPickupPointId').val(tmp[1]);
                if ($itemrow.find('#localDropPoint').val() != '') {
                    $itemrow.find('#localDropPoint').val('');
                    $itemrow.find('#localDropPointId').val('');
                    $itemrow.find('#localTotalKm').val('');
                    $itemrow.find('#localTotalHrs').val('');
                    $itemrow.find('#localTotalMinutes').val('');
                    $itemrow.find('#localPointRouteId').val('');
                    $itemrow.find('#fromCBM').val('');
                    $itemrow.find('#toCBM').val('');
                    $itemrow.find('#flatRate').val('');
                }
                $itemrow.find('#localDropPoint').focus();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#localDropPoint').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCCPointToPointWeightRouteName.do",
                    dataType: "json",
                    data: {
                        elementName: $(this.element).prop("id"),
                        elementValue: request.term,
                        rowCount: 1,
                        ptpwPickupPoint: document.getElementById("localPickupPointId").value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            alert("Invalid Route Point");
                            $("#localDropPoint").val('');
                            $("#localDropPointId").val('');
                            $("#localPointRouteId").val('');
                            $("#localTotalKm").val('');
                            $("#localTotalHrs").val('');
                            $("#localTotalMinutes").val('');
                            $("#fromCBM").val('');
                            $("#toCBM").val('');
                            $("#flatRate").val('');
                            $('#localDropPoint').focus();
                        } else {
                            //alert(items);
                            response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 0,
            select: function(event, ui) {
                $("#localDropPoint").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#localDropPoint').val(tmp[0]);
                $itemrow.find('#localDropPointId').val(tmp[1]);
                $itemrow.find('#localTotalKm').val(tmp[2]);
                $itemrow.find('#localTotalHrs').val(tmp[3]);
                $itemrow.find('#localTotalMinutes').val(tmp[4]);
                $itemrow.find('#localPointRouteId').val(tmp[5]);
                $itemrow.find('#flatRate').focus();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[0] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>

<script type="text/javascript" language="javascript">
    function setContainerTypeListDistance(val, sno) {
        if (val == '1058') {
            // 20FT vehicle
            $('#containerTypeIdDistance' + sno).empty();
            $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(1).html('20'))
            $('#containerQtyDistance' + sno).empty();
            $('#containerQtyDistance' + sno).append($('<option ></option>').val(1).html('1'))
        } else if (val == '1059' || val == '1060') {
            // 40FT vehicle
            $('#containerTypeIdDistance' + sno).empty();
            $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(0).html('--Select--'))
            $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(1).html('20'))
            $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(2).html('40'))
            $('#containerQtyDistance' + sno).empty();

        }
    }
    function setContainerQtyListDistance(val, sno) {
        if (val == '1') {
            // 20FT vehicle
            $('#containerQtyDistance' + sno).empty();
            $('#containerQtyDistance' + sno).append($('<option ></option>').val(2).html('2'))
        } else if (val == '2') {
            // 40FT vehicle
            $('#containerQtyDistance' + sno).empty();
            $('#containerQtyDistance' + sno).append($('<option ></option>').val(1).html('1'))

        }
    }
</script>
<script type="text/javascript" language="javascript">
    function getDistanceMarketHireRateOld(sno, tableRow) {
        var vehicleTypeId = $("#vehicleTypeIdDistance" + sno).val();
        var loadTypeId = $("#loadTypeIdDistance" + sno).val();
        var containerTypeId = $("#containerTypeIdDistance" + sno).val();
        var containerQty = $("#containerQtyDistance" + sno).val();
        var fromDistance = $("#fromDistance" + sno).val();
        var toDistance = $("#toDistance" + sno).val();
        if (vehicleTypeId != '0' && containerTypeId != '0' && containerQty != '0'
                && fromDistance != '' && toDistance != '' && loadTypeId != '0') {
            $.ajax({
                url: "/throttle/getDistanceMarketHireRate.do",
                dataType: "json",
                data: {
                    vehicleTypeId: vehicleTypeId,
                    loadTypeId: loadTypeId,
                    containerTypeId: containerTypeId,
                    containerQty: containerQty,
                    fromDistance: fromDistance,
                    toDistance: toDistance
                },
                success: function(data, textStatus, jqXHR) {
                    if (data.MarketHireRate == '0') {
                        $("#distanceStatusSpan").text("Route not defined in organisation");
                        $("#distanceMarketRate" + sno).text(0);
//                                                $("#distanceMarketRateSpan"+sno).text(0);
                        $("#vehicleTypeIdDistance" + sno).val(0);
                        $("#containerTypeIdDistance" + sno).val(0);
                        $("#containerQtyDistance" + sno).val(0);
                        checkDistanceMarketRate(sno, data.MarketHireRate);
                    } else if (data.MarketHireRate != '') {
                        $("#distanceStatusSpan").text("");
                        $("#distanceMarketRate" + sno).text(data.MarketHireRate);
//                                                $("#distanceMarketRate"+sno).val(data.MarketHireRate);
                        checkDistanceMarketRate(sno, data.MarketHireRate);
                    }
                },
                error: function(data, type) {
                }
            });
        }

    }

    function checkDistanceMarketRate(sno, value) {
        var additionalCost = $("#rateWithoutReeferDistance" + sno).val();
        var marketRate = value;
        var sts = false;
        if (parseFloat(additionalCost) > parseFloat(marketRate)) {
            sts = true;
        }
        if (sts) {
            $("#distanceStatusSpan").text("Needs Approval");
            $("#distanceStatusSpan").removeClass("noErrorClass");
            $("#distanceStatusSpan").addClass("errorClass");
        } else {
            $("#distanceStatusSpan").text("Auto Approval");
            $("#distanceStatusSpan").removeClass("errorClass");
            $("#distanceStatusSpan").addClass("noErrorClass");
        }
    }


</script>
<script>
    function submitPage() {
        var contractTypeId = $("#contractTypeId").val();
        var count = 0;
        if (contractTypeId == 2 || contractTypeId == 3) {
            count = checkOnDemandContract();
        }
        if (count == 0) {
            document.vehicleVendorContract.action = '/throttle/saveVehicleVendorContract.do';
            document.vehicleVendorContract.submit();
        }
    }

    function checkOnDemandContract() {
        var spot = $("[name=spotCost]");
        var count = 0;
        for (var i = 0; i < spot.length; i++) {
//        if(spot[i].value == '0' || spot[i].value == '0.00' ){
//          alert("contract amount cannot be 0")
//          spot[i].focus();
//          count = 1;
//          return count;
//        }
            if (spot[i].value == '') {
                alert("please enter the contract amount")
                spot[i].focus();
                count = 1;
                return count;
            }
        }
        return count;
    }
</script>

<script>
    function setVehicleTypeDetails(sel, sno) {
        var vehicleTypeName = sel.options[sel.selectedIndex].text;
        if (vehicleTypeName == "20'") {
            $("#containerTypeId" + sno + " option[value='2']").remove();
            document.getElementById("containerTypeId" + sno).value = 1;
            $('#containerQty' + sno).val('1');
        } else {
            var exist = 0;
            $('#containerTypeId' + sno + ' option').each(function() {
                if (this.value == '2') {
                    exist = 1;
                }
            });
            if (exist == 1) {
            } else {
                $('#containerTypeId' + sno).append(new Option("40'", '2'));
            }
            document.getElementById("containerTypeId" + sno).value = 0;
            $('#containerQty' + sno).val('');
        }
    }
    function setContainerValue(cnt, containerType) {
        var containers = containerType;
        var vehicleType = $('#vehicleTypeId' + cnt).val();
        if (vehicleType == "1059~40'" || vehicleType == "1059" || vehicleType == "1060"  ) {
            if (containers == "1") {
                $('#containerQty' + cnt).val('2');
            } else if (containers == "2") {
                $('#containerQty' + cnt).val('1');
            } else {
                $('#containerQty' + cnt).val('0');
            }
        }

    }

    var count = 1;
//      $(document).ready(function() {  alert("hiiii");
//            // Get the table object to use for adding a row at the end of the table
//
//            var $itemsTable = $('#distanceTable');
//
//            // Create an Array to for the table row. ** Just to make things a bit easier to read.
//            var rowTemp = "";
//            <c:if test="${display == 'none'}">
//                rowTemp = [
//                '<tr class="item-row">',
//                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
//
//                    '<td><span id="distanceOrgValues" style="font-size: 14px"></span><input type="hidden" name="distanceApprovalStatus" id="distanceApprovalStatus" value="1" /></td>',
//                '<td><input name="referenceName" value="" class="form-control" id="referenceName"  style="width: 150px;" /></td>',
//                '<td><c:if test="${vehicleTypeList != null}"><select name="vehicleTypeIdDistance" id="vehicleTypeIdDistance"  class="form-control" style="width:150px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
//
//                '<td><select name="containerTypeIdDistance" id="containerTypeIdDistance"  class="form-control" style="width:150px" ></td>',
//                '<td><select name="containerQtyDistance" id="containerQtyDistance"  class="form-control" style="width:150px" ></td>',
//                '<td><select name="loadTypeIdDistance" id="loadTypeIdDistance"  class="form-control" style="width:150px" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></td>',
//                '<td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width: 150px;"  /></td>',
//
//                '<td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width: 150px;"  /></td>',
//
//                '<td><input name="rateWithReeferDistance" value="" class="form-control" id="rateWithReeferDistance" onchange="contractDistanceCheck();" style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
//                '<td><input name="rateWithoutReeferDistance" value="" class="form-control" id="rateWithoutReeferDistance" onchange="contractDistanceCheck();" style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="fuelVehicle" value="" class="form-control" id="fuelVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
//                  '<td><span id="distanceOrgRateWithReefer" style="font-size: 12px; color:green"></span></td>',
//                '<td><span id="distanceOrgRateWithoutReefer" style="font-size: 12px;color:green"></span></td>',
//                '</tr>'
//            ].join('');
//            </c:if>
//            <c:if test="${display == 'block'}">
//                rowTemp = [
//                '<tr class="item-row">',
//                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
//                 '<td><span id="distanceOrgValues" style="font-size: 14px"></span><input type="hidden" name="distanceApprovalStatus" id="distanceApprovalStatus" value="1" /></td>',
//                '<td><input name="referenceName" value="" class="form-control" id="referenceName"  style="width: 150px;" /></td>',
//                '<td><c:if test="${vehicleTypeList != null}"><select name="vehicleTypeIdDistance" id="vehicleTypeIdDistance"  class="form-control" style="width:150px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
//                         '<td><select name="containerTypeIdDistance" id="containerTypeIdDistance"  class="form-control" style="width:150px" ></td>',
//                '<td><select name="containerQtyDistance" id="containerQtyDistance"  class="form-control" style="width:150px" ></td>',
//               '<td><select name="loadTypeIdDistance" id="loadTypeIdDistance"  class="form-control" style="width:150px" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></td>',
//               '<td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width: 150px;" /></td>',
//
//                '<td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width: 150px;"/></td>',
//
//                '<td><input name="rateWithReeferDistance" value="0" class="form-control" id="rateWithReeferDistance"  onchange="contractDistanceCheck();" style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
//                '<td><input name="rateWithoutReeferDistance" value="0" class="form-control" id="rateWithoutReeferDistance" onchange="contractDistanceCheck();" style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
//                '<td><span id="distanceOrgRateWithReefer" style="font-size: 12px; color:green"></span></td>',
//                '<td><span id="distanceOrgRateWithoutReefer" style="font-size: 12px;color:green"></span></td>',
//
//                '</tr>'
//            ].join('');
//            </c:if>
//
//            // Add row to list and allow user to use autocomplete to find items.
//
//
//
//            $("#addRowDistance").bind('click', function() {
//
//              //  count++;
////             alert("hiiii");
//
////              if( document.getElementById("distanceTable").style.visibility == "hidden"){
////                 document.getElementById("distanceTable").style.visibility = "visible";
////
////                } else{
//                var $row = $(rowTemp);
//
//                // save reference to inputs within row
//
//                var $referenceName = $row.find('#referenceName');
//                var $ptpVehicleTypeId = $row.find('#vehicleTypeIdDistance');
//
//                var $containerTypeId = $row.find('#containerTypeIdDistance');
//                var $containerQty = $row.find('#containerQtyDistance');
//                var $ptpPickupPoint = $row.find('#fromDistance');
//
//
//                var $ptpDropPoint = $row.find('#toDistance');
//
//                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
//                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');
//                 $row.find('#vehicleTypeIdDistance').change(function(){
////                        var $itemrow = $(this).closest('tr');
//                        var val = $row.find('#vehicleTypeIdDistance').val();
//                        if(val == '1058'){
//                        // 20FT vehicle
//                        $row.find('#containerTypeIdDistance').empty();
//                        $row.find('#containerTypeIdDistance').append($('<option ></option>').val(1).html('20'))
//                        $row.find('#containerQtyDistance').empty();
//                        $row.find('#containerQtyDistance').append($('<option ></option>').val(1).html('1'))
//                        }else if(val == '1059'){
//                        // 40FT vehicle
//                        $row.find('#containerTypeIdDistance').empty();
//                        $row.find('#containerTypeIdDistance').append($('<option ></option>').val(0).html('--Select--'))
//                        $row.find('#containerTypeIdDistance').append($('<option ></option>').val(1).html('20'))
//                        $row.find('#containerTypeIdDistance').append($('<option ></option>').val(2).html('40'))
//                        $row.find('#containerQtyDistance').empty();
//
//                        }
//                    });
//                    $row.find('#containerTypeIdDistance').change(function(){
////                        var $itemrow = $(this).closest('tr');
//                        var val = $row.find('#containerTypeIdDistance').val();
//                        if(val == '1'){
//                        // 20FT Container
//                        $row.find('#containerQtyDistance').empty();
//                        $row.find('#containerQtyDistance').append($('<option ></option>').val(2).html('2'))
//                        }else if(val == '2'){
//                        // 40FT Container
//                        $row.find('#containerQtyDistance').empty();
//                        $row.find('#containerQtyDistance').append($('<option ></option>').val(1).html('1'))
//
//                        }
//                    });
//                $('.item-row:last', $itemsTable).after($row);
////                if ($('#ptpRouteContractCode:last').val() !== '') {}else{ // End if last itemCode input is empty
////                    alert("please fill route code");
////                }
//                return false;
//           // }
//            });
//
//
//     });// End DOM

    </script>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Vendor</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Create Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%--  <%try{%>--%>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>
                <form name="vehicleVendorContract" method="post" >
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover" style="width:70%">
                        <thead><tr >
                                <th  colspan="4" >Vendor Contract Info</th>
                            </tr></thead>
                        <tr >
                            <td>Vendor Name</td>
                            <td><input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="textbox">
                                <input type="hidden" name="vendorName" id="vendorName" value="<c:out value="${vendorName}"/>" class="textbox">
                                <c:out value="${vendorName}"/></td>
                            <td>Contract Type</td>
                            <td>
                                <select name="contractTypeId" id="contractTypeId" class="textbox" onchange="checkContractType(this.value);" style="height:22px;width:150px;">
                                    <!--<option value="0" selected>--Select--</option>-->
                                    <option value="1" >Dedicated</option>
                                    <option value="2" >On Demand</option>
                                    <option value="3" selected>Both</option>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td class="text2">Contract From</td>
                            <td class="text2"><input type="text" name="startDate" id="startDate" value="<c:out value="${startDate}" />" class="datepicker"  style="height:22px;width:150px;color:black">
                            </td>

                            <td  class="text2">Contract To</td>
                            <td class="text2"><input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker"  style="height:22px;width:150px;color:black"></td>
                        </tr>
                        <tr >
                            <td  class="text1">Payment Type</td>
                            <td>
                                <select name="paymentType" id="paymentType" class="textbox"  style="height:22px;width:150px;">
                                    <option value="1" >Monthly</option>
                                    <option value="2" >Trip Wise</option>
                                    <option value="3" >FortNight</option>
                                </select>
                               <input type="hidden"  name="contractFlag" id="contractFlag" value="<c:out value="${contractFlag}" />">
                            </td>
                            <td  colspan="2"></td>
                        </tr>
                    </table>
                    <br>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <!--<li class="active" data-toggle="tab"><a href="#deD"><span>DEDICATED</span></a></li>-->
                            <li data-toggle="tab"><a href="#fullTruck"><span>Point to Point - Rate Details</span></a></li>
                            <!--<li data-toggle="tab"><a href="#weightBaseRate"><span>Weight - Based - Rate Details</span></a></li>-->
                            <!--<li data-toggle="tab"><a href="#distanceRate"><span>Weight - Based - Rate Details</span></a></li>-->
                            <!--<li><a href="#weightBreak"><span>Weight Break </span></a></li>-->
                        </ul>
                        <div id=distanceRate class="tab-pane active" style="padding-left:1px;overflow: auto;display: none" >
                            <div class="inpad" style="padding-left:1px;">

                                <br>
                                <br>

                                <table class="table table-info mb30 table-hover sortable" width="98%" id="distanceTable" >
                                    <thead>
                                        <tr height="40" >
                                            <th>Sno</th>
                                            <th>Approval Required?</th>
                                            <th  height="80" style="width: 10px;"><font color="red">*</font>Reference name</th>


                                            <th  height="30" style="width: 10px;"><font color="red">*</font>Vehicle Type</th>
                                            <th  height="30" style="width: 10px;"><font color="red">*</font>Container Type</th>
                                            <th  height="30" style="width: 10px;"><font color="red">*</font>Container Qty</th>
                                            <th  height="30" style="width: 10px;"><font color="red">*</font>Load Type</th>
                                            <th  height="30" style="width: 10px;"><font color="red">*</font>From Distance</th>

                                            <th  height="30" ><font color="red">*</font>To Distance</th>

                                            <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate With Reefer</th>
                                            <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate Without Reefer</th>
                                            <th  height="30" style="width: 90px;" ><font color="red">*</font>Org Rate With Reefer</th>
                                            <th  height="30" style="width: 90px;" ><font color="red">*</font>Org Rate Without Reefer</th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="item-row">
                                            <td>1</td>


                                            <td align="left" ><span id="distanceStatusSpan" style="font-size: 14px"></span>  </td>

                                            <td><input type="text" name="referenceName" id="referenceName" value="" class="form-control"  style="width:150px;height:44px;"/></td>


                                            <td><c:if test="${vehicleTypeList != null}"><select onchange='setContainerTypeListDistance(this.value, "1")' name="vehicleTypeIdDistance" id="vehicleTypeIdDistance1"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>

                                                    <td><select onchange='setContainerQtyListDistance(this.value, "1")' name="containerTypeIdDistance" id="containerTypeIdDistance1"  class="form-control" style="width:150px;height:44px;"></select></td>
                                                    <td><select name="containerQtyDistance" id="containerQtyDistance1" onchange="getDistanceMarketHireRate('1', '');" class="form-control" style="width:150px;height:44px;"></select></td>
                                                    <td><select name="loadTypeIdDistance" id="loadTypeIdDistance1"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></select></td>
                                                    <td><input name="fromDistance" value="" class="form-control" id="fromDistance1"  style="width:150px;height:44px;"  /></td>

                                                    <td><input name="toDistance" value="" class="form-control" id="toDistance1"  style="width:150px;height:44px;" /></td>

                                                    <td><input name="rateWithReeferDistance" value="0" class="form-control" id="rateWithReeferDistance1"  style="width:150px;height:44px;" 
                                                               onKeyPress="return onKeyPressBlockCharacters(event);" onchange="getDistanceMarketHireRate('1', '');"/></td>
                                                    <td><input name="rateWithoutReeferDistance" value="0" class="form-control" id="rateWithoutReeferDistance1"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="getDistanceMarketHireRate('1', '');" /></td>
                                                    <td align="left" ><span id="distanceMarketRate1" style="font-size: 14px;color:green;"></span></td>
                                                    <!--<td align="left" ><span id="distanceMarketRateSpan" style="font-size: 14px;color:green;"></span></td>-->
                                                </tr> 
                                                </tr> 
                                            </tbody>
                                        </table>


                                        <a href="#" id="addRowDistance" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /><b> Add Route Code</b></span></a>
                                        <br>
                                        <br>
                                        <center>
                                            <!--<input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitDistancePage(this.value)"/>-->
                                        </center>
                                        <br>
                                        <br>
                                    </div></div>



                                <div id="fullTruck" style="padding-left: 1px;">
                                    <span id="statusSpan" style="color: red"></span>
                                    <div id="mainFullTruck"  style="width: 72%;">
                                        <input class="btn btn-info"  type="button" id="btAdd" value="Add Route" style="width:95px;height:30px;font-weight: bold;padding:1px;font-size: 12px;"/>
                                        <input class="btn btn-info" type="button" id="btRemove" value="Remove Route" style="width:95px;height:30px;font-weight: bold;padding:1px;font-size: 12px;" />
                                    </div>
                                    <style>
                                        .errorClass { 
                                            background-color: red;
                                        }
                                        .noErrorClass { 
                                            background-color: greenyellow;
                                        }
                                    </style>
                                    <script>
                                        function setContainerQtyList(val, sno) {
                                            if (val == '1') {
                                                // 20FT vehicle
                                                $('#containerQty' + sno).empty();
                                                $('#containerQty' + sno).append($('<option ></option>').val(2).html('2'))
                                                $('#containerQtyDistance' + sno).empty();
                                                $('#containerQtyDistance' + sno).append($('<option ></option>').val(2).html('2'))
                                            } else if (val == '2') {
                                                // 40FT vehicle
                                                $('#containerQty' + sno).empty();
                                                $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))
                                                $('#containerQtyDistance' + sno).empty();
                                                $('#containerQtyDistance' + sno).append($('<option ></option>').val(1).html('1'))

                                            } else {
                                                $('#containerQty' + sno).empty();
                                                $('#containerQty' + sno).append($('<option ></option>').val(0).html('0'))
                                            }
                                        }
                                        function setContainerTypeList(val, sno) {
                                            if (val == '1058') {
                                                // 20FT vehicle
                                                $('#containerTypeId' + sno).empty();
                                                $('#containerTypeId' + sno).append($('<option ></option>').val(1).html('20'))
                                                $('#containerQty' + sno).empty();
                                                $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))
                                                $('#containerTypeIdDistance' + sno).empty();
                                                $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(1).html('20'))
                                                $('#containerQtyDistance' + sno).empty();
                                                $('#containerQtyDistance' + sno).append($('<option ></option>').val(1).html('1'))
                                            } else if (val == '1059' || val == '1060') {
                                                // 40FT vehicle
                                                $('#containerTypeId' + sno).empty();
                                                $('#containerTypeId' + sno).append($('<option ></option>').val(0).html('--Select--'))
                                                $('#containerTypeId' + sno).append($('<option ></option>').val(1).html('20'))
                                                $('#containerTypeId' + sno).append($('<option ></option>').val(2).html('40'))
                                                $('#containerQty' + sno).empty();
                                                $('#containerTypeIdDistance' + sno).empty();
                                                $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(0).html('--Select--'))
                                                $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(1).html('20'))
                                                $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(2).html('40'))
                                                $('#containerQtyDistance' + sno).empty();

                                            } else {
                                                $('#containerTypeId' + sno).empty();
                                                $('#containerTypeId' + sno).append($('<option ></option>').val(4).html('0'))
                                                $('#containerQty' + sno).empty();
                                                $('#containerQty' + sno).append($('<option ></option>').val(0).html('0'))
                                                $('#containerTypeIdDistance' + sno).empty();
                                                $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(0).html('0'))
                                                $('#containerQtyDistance' + sno).empty();
                                                $('#containerQtyDistance' + sno).append($('<option ></option>').val(0).html('0'))
                                            }
                                        }
                                        function checkKey(obj, e, id, cnt) {
                                            var orgName = 'originNameFullTruck' + cnt;
                                            var point1Name = 'pointName1' + cnt;
                                            var point2Name = 'pointName2' + cnt;
                                            var point3Name = 'pointName3' + cnt;
                                            var point4Name = 'pointName4' + cnt;
                                            var destinationName = 'destinationNameFullTruck' + cnt;
                                            var pointId = "";
                                            if (id == orgName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }
                                            if (id == point1Name) {
                                                pointId = 'pointId1' + cnt;
                                            }
                                            if (id == point2Name) {
                                                pointId = 'pointId2' + cnt;
                                            }
                                            if (id == point3Name) {
                                                pointId = 'pointId3' + cnt;
                                            }
                                            if (id == point4Name) {
                                                pointId = 'pointId4' + cnt;
                                            }
                                            if (id == destinationName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }
                                            var idVal = $('#' + pointId).val();
                                            if (e.keyCode == 46 || e.keyCode == 8) {
                                                $('#' + id).val('');
                                                $('#' + pointId).val('');
                                            } else if (idVal != '' && e.keyCode != 13) {
                                                //                                        $('#' + id).val('');
                                                //                                        $('#' + pointId).val('');
                                            }
                                        }

                                        function checkPointNames(id, cnt) {
                                            var orgName = 'originNameFullTruck' + cnt;
                                            var point1Name = 'pointName1' + cnt;
                                            var point2Name = 'pointName2' + cnt;
                                            var point3Name = 'pointName3' + cnt;
                                            var point4Name = 'pointName4' + cnt;
                                            var destinationName = 'destinationNameFullTruck' + cnt;
                                            var pointId = "";
                                            if (id == orgName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }
                                            if (id == point1Name) {
                                                pointId = 'pointId1' + cnt;
                                            }
                                            if (id == point2Name) {
                                                pointId = 'pointId2' + cnt;
                                            }
                                            if (id == point3Name) {
                                                pointId = 'pointId3' + cnt;
                                            }
                                            if (id == point4Name) {
                                                pointId = 'pointId4' + cnt;
                                            }
                                            if (id == destinationName) {
                                                pointId = 'destinationIdFullTruck' + cnt;
                                            }
                                            var idValue = $('#' + id).val();
                                            var idValue1 = $('#' + pointId).val();
                                            if (idValue != '' && idValue1 == '') {
                                                alert('Invalid Point Name');
                                                $('#' + id).focus();
                                                //                                        $('#'+id).val('');
                                                //                                        $('#'+pointId).val('');
                                            }
                                            getMarketHireRate(cnt, cnt);
                                        }

                                        function getMarketHireRateOld(sno, tableRow) {
                                            var vehicleTypeId = $("#vehicleTypeId" + sno).val();
                                            var loadTypeId = $("#loadTypeId" + sno).val();
                                            var containerTypeId = $("#containerTypeId" + sno).val();
                                            var containerQty = $("#containerQty" + sno).val();
                                            var originIdFullTruck = $("#originIdFullTruck" + tableRow).val();
                                            var pointId1 = $("#pointId1" + tableRow).val();
                                            var pointId2 = $("#pointId2" + tableRow).val();
                                            var pointId3 = $("#pointId3" + tableRow).val();
                                            var pointId4 = $("#pointId4" + tableRow).val();
                                            var destinationIdFullTruck = $("#destinationIdFullTruck" + tableRow).val();
                                            if (vehicleTypeId != '0' && containerTypeId != '0'
                                                    && originIdFullTruck != '' && destinationIdFullTruck != '' && loadTypeId != '0') {
                                                $.ajax({
                                                    url: "/throttle/getMarketHireRate.do",
                                                    dataType: "json",
                                                    data: {
                                                        vehicleTypeId: vehicleTypeId,
                                                        loadTypeId: loadTypeId,
                                                        containerTypeId: containerTypeId,
                                                        containerQty: containerQty,
                                                        originIdFullTruck: originIdFullTruck,
                                                        pointId1: pointId1,
                                                        pointId2: pointId2,
                                                        pointId3: pointId3,
                                                        pointId4: pointId4,
                                                        destinationIdFullTruck: destinationIdFullTruck
                                                    },
                                                    success: function(data, textStatus, jqXHR) {
                                                        if (data.MarketHireRate == '0') {
                                                            $("#statusSpan").text("Route not defined in organisation");
                                                            $("#marketRate" + sno).val(0);
                                                            $("#marketRateSpan" + sno).text(0);
                                                            $("#vehicleTypeId" + sno).val(0);
                                                            $("#containerTypeId" + sno).val(0);
                                                            $("#containerQty" + sno).val(0);
                                                        } else if (data.MarketHireRate != '') {
                                                            $("#statusSpan").text("");
                                                            $("#marketRate" + sno).val(data.MarketHireRate);
                                                            $("#marketRateSpan" + sno).text(data.MarketHireRate);
                                                        }
                                                    },
                                                    error: function(data, type) {
                                                    }
                                                });
                                            }

                                        }

                                        function checkMarketRate(val, sno) {
                                            var spotCost = $("#spotCost" + sno).val();
                                            var additionalCost = $("#additionalCost" + sno).val();
                                            var marketRate = $("#marketRate" + sno).val();
                                            var sts = false;
                                            if (parseFloat(spotCost) > parseFloat(marketRate)) {
                                                sts = true;
                                            }
                                            if (parseFloat(additionalCost) > parseFloat(marketRate)) {
                                                sts = true;
                                            }
                                            if (sts) {
                                                $("#approvalStatusSpan" + sno).text("Needs Approval");
                                                $("#approvalStatusSpan" + sno).removeClass("noErrorClass");
                                                $("#approvalStatusSpan" + sno).addClass("errorClass");
                                            } else {
                                                $("#approvalStatusSpan" + sno).text("Auto Approval");
                                                $("#approvalStatusSpan" + sno).removeClass("errorClass");
                                                $("#approvalStatusSpan" + sno).addClass("noErrorClass");
                                            }
                                        }

                                        var container = "";
                                        $(document).ready(function() {
                                            var iCnt = 1;
                                            var rowCnt = 1;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            container = $($("#routeFullTruck")).css({
                                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            $(container).last().after('<table id="mainTableFullTruck" width="80%"><tr></td>\
                                            <table  id="routeDetails' + iCnt + '"  border="1">\n\
                                            <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                            <td>Sno</td>\n\
                                            <td>Origin</td>\n\
                                            <td>Interim Point 1</td>\n\
                                            <td>Interim Point 2</td>\n\
                                            <td>Interim Point 3</td>\n\
                                            <td>Interim Point 4</td>\n\
                                            <td>Destination</td>\n\
                                            <tr style="height:22px;"><td>Route&nbsp; ' + iCnt + '</td>\n\
                                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                            <td><input type="hidden" name="pointId1" id="pointId1' + iCnt + '" value="0" />\n\
                                            <input type="text" name="pointName1" id="pointName1' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);"  style="height:22px;width:160px;"/>\n\</td>\n\
                                            <td><input type="hidden" name="pointId2" id="pointId2' + iCnt + '" value="0" />\n\
                                            <input type="text" name="pointName2" id="pointName2' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                            <td><input type="hidden" name="pointId3" id="pointId3' + iCnt + '" value="0" />\n\
                                            <input type="text" name="pointName3" id="pointName3' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                            <td><input type="hidden" name="pointId4" id="pointId4' + iCnt + '" value="0" />\n\
                                            <input type="text" name="pointName4" id="pointName4' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                            <input type="hidden" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/></td>\n\
                                            </tr>\n\
                                            </table>\n\
                                            <table  id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1" width="60%" >\n\
                                            <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                            <td>Status</td><td>Sno</td><td>FromDate</td><td>ToDate</td><td>Vehicle Type</td><td>Load Type</td><td>Container Type</td><td>Container Qty</td>\n\
 <c:if test="${contractFlag == '2'}"><td>From(Tonnage)</td><td>To(Tonnage)</td></c:if>\n\
<td>Rate With Reefer</td><td>Rate Without Reefer</td><td>Advance Type</td><td>Advance Value</td><td>Initial Advance</td><td>Remaining Amount</td><td>Agreed No.of Vehicle</td></tr>\n\
                                            <td>Market Rate</td></tr>\n\
                                            <tr style="height:22px;">\n\
                                            <td><span id="approvalStatusSpan' + iCnt + '">Not Filled</span></td><td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                            <td><input type="text" name="fromDate1" style="width:110px;"  id="fromDate1' + iCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                            <td><input type="text" name="toDate1"  style="width:110px;" id="toDate1' + iCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                            <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="1" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><select type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + iCnt + ');setContainerTypeList(this.value,' + iCnt + ');getMarketHireRate(' + iCnt + ',' + iCnt + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                            <td><select name="loadTypeId" id="loadTypeId' + iCnt + '"  class="textbox" onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');" style="height:22px;width:160px;"><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></select></td>\n\
                                            <td> <c:if test="${containerTypeList != null}"><select name="containerTypeId" id="containerTypeId' + iCnt + '"  class="textbox" style="height:22px;width:110px;" onChange="setContainerValue(' + iCnt + ',this.value);setContainerQtyList(this.value,' + iCnt + ',' + iCnt + ');getMarketHireRate(' + iCnt + ',' + iCnt + ')"><option value="0">-Select-</option><c:forEach items="${containerTypeList}" var="conType"><option value="<c:out value="${conType.containerId}"/>"><c:out value="${conType.containerName}"/></option></c:forEach></c:if></select></td>\n\
                                            <td><select name="containerQty" id="containerQty' + iCnt + '"  class="textbox" style="height:22px;width:110px;" onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');" ><option value="0">-Select-</option><option value="1">1</option><option value="2">2</option></select></td>\n\
\n\ <c:if test="${contractFlag == '1'}"><input type="hidden" name="fromCBM" id="fromCBM" value="0" ><input type="hidden" name="toCBM" id="toCBM" value="0" > </c:if><c:if test="${contractFlag == '2'}"><td><input type="text" name="fromCBM" id="fromCBM" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"></td>\n\
<td><input type="text" name="toCBM" id="toCBM" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"></td></c:if>\n\
                                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\\n\
                                            <td><select name="advanceMode" id="advanceMode' + iCnt + '"  onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;" >\n\
                                                <option value=0>-Select-</option>\n\
                                                <option value=1>PERCENTAGE</option>\n\
                                                <option value=2>FLAT</option>\n\
                                                <option value=3>CREDIT</option>\n\
                                            </select>\n\
                                            </td>\n\
                                            <td><input type="text" name="modeRate" id="modeRate' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="initialAdvance" id="initialAdvance' + iCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="endAdvance" id="endAdvance' + iCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="lHCNO" id="lHCNO' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="hidden" name="marketRate" id="marketRate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><span id="marketRateSpan' + iCnt + '"></span></td>\n\
                                            </tr></table>\n\
                                            <br><table border="0"><tr>\n\
                                            <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ',' + iCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                            <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                            </tr></table></td></tr></table><br><br>');
                                            callOriginAjaxFullTruck(iCnt);
                                            callPointId1AjaxFullTruck(iCnt);
                                            callPointId2AjaxFullTruck(iCnt);
                                            callPointId3AjaxFullTruck(iCnt);
                                            callPointId4AjaxFullTruck(iCnt);
                                            callDestinationAjaxFullTruck(iCnt);
                                            $('.datepicker').datepicker();
                                            $('#btAdd').click(function() {
                                                iCnt = iCnt + 1;
                                                $(container).last().after('<table id="mainTableFullTruck" width="100%"><tr><td>\
                                            <table  id="routeDetailsFullTruck' + iCnt + '" border="1" >\n\
                                            <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px">\n\
                                            <td>Sno</td>\n\
                                            <td>Origin</td>\n\
                                            <td>Interim Point 1</td>\n\
                                            <td>Interim Point 2</td>\n\
                                            <td>Interim Point 3</td>\n\
                                            <td>Interim Point 4</td>\n\
                                            <td>Destination</td>\n\
                                            <tr style="height:22px;">\n\
                                            <td>Route&nbsp; ' + iCnt + '</td>\n\
                                            <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/>\n\
                                            <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="hidden" name="pointId1" id="pointId1' + iCnt + '" value="0" />\n\
                                            <input type="text" name="pointName1" id="pointName1' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);"  style="height:22px;width:160px;"/>\n\</td>\n\
                                            <td><input type="hidden" name="pointId2" id="pointId2' + iCnt + '" value="0" />\n\
                                            <input type="text" name="pointName2" id="pointName2' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                            <td><input type="hidden" name="pointId3" id="pointId3' + iCnt + '" value="0" />\n\
                                            <input type="text" name="pointName3" id="pointName3' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                            <td><input type="hidden" name="pointId4" id="pointId4' + iCnt + '" value="0" />\n\
                                            <input type="text" name="pointName4" id="pointName4' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                            <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/>\n\
                                            <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id, ' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\
                                            <input type="hidden" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="travelHourFullTruck" id="travelHourFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="travelKmFullTruck" id="travelKmFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/><input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + iCnt + '" value="" style="height:22px;width:160px;"/></td>\n\
                                            </tr>\n\
                                            </table>\n\
                                            <table  id="routeInfoDetailsFullTruck' + iCnt + rowCnt + '" border="1">\n\
                                            <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                            <td>Status</td><td>Sno</td><td>FromDate</td><td>ToDate</td><td>Vehicle Type</td><td>Load Type</td><td>Container Type</td><td>Container Qty</td><td>Rate With Reefer</td><td>Rate Without Reefer</td><td>Advance Type</td><td>Advance Value</td><td>Initial Advance</td><td>Remaining Amount</td><td>Agreed No.of Vehicle</td></tr>\n\
                                            <td>Market Rate</td></tr>\n\
                                            <tr style="height:22px;">\n\
                                            <td><span id="approvalStatusSpan' + iCnt + '">Not Filled</span></td><td>' + rowCnt + '<input type="hidden" name="iCnt1" id="iCnt1' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                            <td><input type="text" name="fromDate1" style="width:110px;"  id="fromDate1' + iCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                            <td><input type="text" name="toDate1" style="width:110px;"  id="toDate1' + iCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                            <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + iCnt + '" value="1" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><select type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + iCnt + ');setContainerTypeList(this.value,' + iCnt + ');getMarketHireRate(' + iCnt + ',' + iCnt + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                            <td><select name="loadTypeId" id="loadTypeId' + iCnt + '"  class="textbox" onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');" style="height:22px;width:160px;" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></select></td>\n\
                                            <td><c:if test="${containerTypeList != null}"><select name="containerTypeId" id="containerTypeId' + iCnt + '"  class="textbox" style="height:22px;width:110px;" onChange="setContainerValue(' + iCnt + ',this.value);setContainerQtyList(this.value,' + iCnt + ');getMarketHireRate(' + iCnt + ',' + iCnt + ')"><option value="0">-Select-</option><c:forEach items="${containerTypeList}" var="conType"><option value="<c:out value="${conType.containerId}"/>"><c:out value="${conType.containerName}"/></option></c:forEach></c:if></select></td>\n\
                                            <td><select name="containerQty" id="containerQty' + iCnt + '"  class="textbox" style="height:22px;width:110px;" onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');"><option value="0">-Select-</option><option value="1">1</option><option value="2">2</option></select></td>\n\
                                            <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;" /></td>\n\
                                            <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onChange="findAdvanceAmount(' + iCnt + ')"  onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            <td><select name="advanceMode" id="advanceMode' + iCnt + '"  onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;" >\n\
                                                <option value=0>-Select-</option>\n\
                                                <option value=1>PERCENTAGE</option>\n\
                                                <option value=2>FLAT</option>\n\
                                                <option value=3>CREDIT</option>\n\
                                            </select>\n\
                                            </td>\n\
                                            <td><input type="text" name="modeRate" id="modeRate' + iCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="initialAdvance" id="initialAdvance' + iCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="endAdvance" id="endAdvance' + iCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\
                                            <td><input type="text" name="lHCNO" id="lHCNO' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                            </tr></table>\n\
                                            <td><input type="hidden" name="marketRate" id="marketRate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><span id="marketRateSpan' + iCnt + '"></span></td>\n\
                                            </tr></table>\n\
                                            <br><table border="0"><tr>\n\
                                            <td><input class="btn btn-info"  type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ',' + iCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                            <input class="btn btn-info"  type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt + rowCnt + '" value="Remove" onclick="deleteRow(' + iCnt + rowCnt + ')"  style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                           </tr></table></td></tr></table><br><br>');
                                                callOriginAjaxFullTruck(iCnt);
                                                callPointId1AjaxFullTruck(iCnt);
                                                callPointId2AjaxFullTruck(iCnt);
                                                callPointId3AjaxFullTruck(iCnt);
                                                callPointId4AjaxFullTruck(iCnt);
                                                callDestinationAjaxFullTruck(iCnt);
                                                $('.datepicker').datepicker();
                                                $('#mainFullTruck').after(container);
                                            });
                                            $('#btRemove').click(function() {
                                                //                                alert($('#mainTableFullTruck tr').size());
                                                if ($(container).size() > 1) {
                                                    $(container).last().remove();
                                                    iCnt = iCnt - 1;
                                                }
                                            });
                                        });

                                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                        var divValue, values = '';
                                        function GetTextValue() {
                                            $(divValue).empty();
                                            $(divValue).remove();
                                            values = '';
                                            $('.input').each(function() {
                                                divValue = $(document.createElement('div')).css({
                                                    padding: '5px', width: '200px'
                                                });
                                                values += this.value + '<br />'
                                            });
                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                            $('body').append(divValue);
                                        }

                                        function addRow(val, v1) {
                                            alert("tets");
                                            var loadCnt2 = val;
                                            var loadCnt1 = v1;
                                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt2 + ' tr').size();
                                            alert("tets22" + routeInfoSize);
                                            var concatVar = v1 + "" + (routeInfoSize - 1);
                                            var loadCnt = parseInt(concatVar);
                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            $('#routeInfoDetailsFullTruck' + val + ' tr').last().after(
                                                    '<tr>\n\
                <td><span id="approvalStatusSpan' + loadCnt + '">Not Filled</span><td>' + routeInfoSize + '<input type="hidden" name="iCnt1" id="iCnt1' + loadCnt + '" value="' + loadCnt1 + '"/></td>\n\
                <td><input type="text" name="fromDate1" style="width:110px;"  id="fromDate1' + loadCnt + '" class="datepicker form-control"  value=""/></td>\n\
                <td><input type="text" name="toDate1" style="width:110px;"  id="toDate1' + loadCnt + '" class="datepicker form-control"  value=""/></td>\n\
                <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + loadCnt + '" value="1" style="height:22px;width:160px;"/><select type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + loadCnt + ');setContainerTypeList(this.value,' + loadCnt + ');getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                <td><select name="loadTypeId" id="loadTypeId' + loadCnt + '" onchange="getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ');" class="textbox" style="height:22px;width:160px;" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></select></td>\n\
                <td><c:if test="${containerTypeList != null}"><select name="containerTypeId" id="containerTypeId' + loadCnt + '"  class="textbox" style="height:22px;width:110px;" onChange="setContainerValue(' + loadCnt + ',this.value);setContainerQtyList(this.value,' + loadCnt + ');getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ')"><option value="0">-Select-</option><c:forEach items="${containerTypeList}" var="conType"><option value="<c:out value="${conType.containerId}"/>"><c:out value="${conType.containerName}"/></option></c:forEach></c:if></select></td>\n\
                <td><select name="containerQty" id="containerQty' + loadCnt + '"  class="textbox" style="height:22px;width:110px;" onchange="getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ');"><option value="0">-Select-</option><option value="1">1</option><option value="2">2</option></select></td>\n\
                <td><input type="text" name="spotCost" id="spotCost' + loadCnt + '" value="0" onkeyup="checkMarketRate(this.value,' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
                <td><input type="text" name="additionalCost" id="additionalCost' + loadCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
                <td><select name="advanceMode" id="advanceMode' + loadCnt + '"  onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;" >\n\
            <option value=0>-Select-</option>\n\
            <option value=1>PERCENTAGE</option>\n\
            <option value=2>FLAT</option>\n\
            <option value=3>CREDIT</option>\n\
        </select>\n\
        </td>\n\
        <td><input type="text" name="modeRate" id="modeRate' + loadCnt + '" value=""  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
        <td><input type="text" name="initialAdvance" id="initialAdvance' + loadCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
        <td><input type="text" name="endAdvance" id="endAdvance' + loadCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\\n\
        <td><input type="text" name="lHCNO" id="lHCNO' + loadCnt + '" value=""  style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                <td><input type="hidden" name="marketRate" id="marketRate' + loadCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><span id="marketRateSpan' + loadCnt + '"></span></td></tr>');
                                            $('.datepicker').datepicker();
                                            loadCnt++;
                                        }
                                        function deleteRow(val) {
                                            var loadCnt = val;

                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }
                                    </script>


                                    <script>
                                        function findAdvanceAmount(sno) {

                                            var additionalCost = $("#additionalCost" + sno).val();
                                            var advanceMode = $("#advanceMode" + sno).val();
                                            var modeRate = $("#modeRate" + sno).val();

                                            var endAdvance = "";
                                            var initialAdvance = "";

                                            if (advanceMode != "") {

                                                if (advanceMode == 1) {

                                                    var percentValue = "";
                                                    if (modeRate == 0) {
                                                        percentValue = 0;
                                                    } else {
                                                        percentValue = parseFloat(additionalCost) * (parseFloat(modeRate) / 100)
                                                    }

                                                    initialAdvance = percentValue;
                                                    endAdvance = parseFloat(additionalCost) - parseFloat(percentValue);

                                                } else if (advanceMode == 2) {
                                                    var flatValue = parseFloat(additionalCost) - parseFloat(modeRate);
                                                    initialAdvance = parseFloat(modeRate);
                                                    endAdvance = parseFloat(flatValue);
                                                }
                                                else if (advanceMode == 3) {
                                                    initialAdvance = 0;
                                                    endAdvance = parseFloat(additionalCost);
                                                    document.getElementById("modeRate" + sno).value = 0;
                                                }
                                                else {
                                                    //alert("Please Select advance type");
                                                    document.getElementById("initialAdvance" + sno).value = 0;
                                                    document.getElementById("endAdvance" + sno).value = 0;
                                                    document.getElementById("modeRate" + sno).value = 0;
                                                    return false;
                                                }

                                                document.getElementById("initialAdvance" + sno).value = initialAdvance;
                                                document.getElementById("endAdvance" + sno).value = endAdvance;
                                            }
                                        }

                                    </script>




                                    <script>
                                        function callOriginAjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'originNameFullTruck' + val;
                                            var pointId = 'originIdFullTruck' + val;
                                            var desPointName = 'destinationNameFullTruck' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                        function callPointId1AjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'pointName1' + val;
                                            var pointId = 'pointId1' + val;
                                            var point2Name = 'pointName2' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + point2Name).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                        function callPointId2AjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'pointName2' + val;
                                            var pointId = 'pointId2' + val;
                                            var point3Name = 'pointName3' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + point3Name).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                        function callPointId3AjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'pointName3' + val;
                                            var pointId = 'pointId3' + val;
                                            var point4Name = 'pointName4' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + point4Name).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }
                                        function callPointId4AjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //       var pointNameId = 'originNameFullTruck' + val;
                                            var pointNameId = 'pointName4' + val;
                                            var pointId = 'pointId4' + val;
                                            var desPointName = 'destinationNameFullTruck' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityFromList.do",
                                                        dataType: "json",
                                                        data: {
                                                            //                                                    cityName: request.term,
                                                            cityName: $("#" + pointNameId).val(),
                                                            //                                                    cityName: $("#" + pointNameId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                            //alert("asdfasdf")
                                                        },
                                                        error: function(data, type) {
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Value;
                                                    //alert("value ="+value);
                                                    var temp = value.split("-");
                                                    var textId = temp[0];
                                                    var textName = temp[1];
                                                    var id = ui.item.Id;
                                                    //alert("id ="+id);
                                                    //alert(id+" : "+value);
                                                    $('#' + pointId).val(textId);
                                                    $('#' + pointNameId).val(textName);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                //var temp [] = "";
                                                var itemVal = item.Value;
                                                var temp = itemVal.split("-");
                                                var textId = temp[0];
                                                var t2 = temp[1];
                                                //                                        alert("t1 = "+t1)
                                                //                                        alert("t2 = "+t2)
                                                //alert("itemVal = "+itemVal)
                                                t2 = '<font color="green">' + t2 + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + t2 + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }


                                        //
                                        //
                                        //                                }


                                        function callDestinationAjaxFullTruck(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'destinationNameFullTruck' + val;
                                            var pointIdId = 'destinationIdFullTruck' + val;
                                            var originPointId = 'originIdFullTruck' + val;
                                            var pointId1 = 'pointId1' + val;
                                            var pointId2 = 'pointId2' + val;
                                            var pointId3 = 'pointId3' + val;
                                            var pointId4 = 'pointId4' + val;
                                            var truckRouteId = 'routeIdFullTruck' + val;
                                            var travelKm = 'travelKmFullTruck' + val;
                                            var travelHour = 'travelHourFullTruck' + val;
                                            var travelMinute = 'travelMinuteFullTruck' + val;


                                            //                                    if($("#" + originPointId).val() == ""){
                                            //                                        alert("id inside condition ==")
                                            //                                    }else{
                                            //                                        alert("else inside condition ==")
                                            //                                    }
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getCityToList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: $("#" + pointNameId).val(),
                                                            originCityId: $("#" + originPointId).val(),
                                                            pointId1: $("#" + pointId1).val(),
                                                            pointId2: $("#" + pointId2).val(),
                                                            pointId3: $("#" + pointId3).val(),
                                                            pointId4: $("#" + pointId4).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.cityName;
                                                    var id = ui.item.cityId;
                                                    //                                            alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + travelKm).val(ui.item.Distance);
                                                    $('#' + travelHour).val(ui.item.TravelHour);
                                                    $('#' + travelMinute).val(ui.item.TravelMinute);
                                                    $('#' + truckRouteId).val(ui.item.RouteId);
                                                    $('#vehicleTypeId' + val).val(0);
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.cityName;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };
                                        }
                                    </script>

                                    <div id="routeFullTruck">

                                    </div>
                                    <br><br>

                                    <center>
                                        <a><input type="button" class="btn btn-info btnPrevious" value="<spring:message code="vendors.label.PREVIOUS" text="default text"/>" name="Previous" style="width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                            <br><br>

                        </div>
                        <div id="weightBaseRate" style="overflow: auto;display: none">

                            <div class="inpad">

                                <table class="table table-info mb30 table-hover" width="90%" id="itemsTable6">
                                    <thead>
                                        <tr >
                                            <th></td>
                                            <th  height="30" style="width:120px" ><font color="red">*</font> Contract Code</td>                                                
                                            <th  height="30" style="width:120px"><font color="red">*</font>First Pick Up</td>
                                            <th  height="30" style="width:120px"><font color="red">*</font>Final Drop Point</td>
                                            <th  height="30" style="width:120px"><font color="red">*</font>Total Km</td>
                                            <th  height="30" style="width:120px"><font color="red">*</font>From (Tonnage)</td>
                                            <th  height="30" style="width:120px"><font color="red">*</font>To (Tonnage)</td>
                                            <th  height="30" style="width:120px"><font color="red">*</font> Rate</td>
                                                <!--<td  height="30" style="width:120px" ><font color="red">*</font>Contract Type</td>-->                                                
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="item-row">
                                            <td>&nbsp;</td>
                                        </tr>
                                        <%--
                                        <tr class="item-row">
                                            <td></td>
                                            <td><input name="localRouteContractCode" id="localRouteContractCode" value="" class="form-control" style="width:120px"/>
                                                <input type="hidden" name="contractCategoryId" id="contractCategoryId"  class="form-control" style="width:150px;height:44px;" value="4"></td>
                                            <td>
                                                <input type="hidden" name="loadTypeId" id="loadTypeId" value="2"/>   
                                                <input type="hidden" name="containerTypeId" id="containerTypeId" value="0"/>   
                                                <input type="hidden" name="containerQty" id="containerQty" value="0"/>   
                                                <input type="hidden" name="localVehicleTypeId" id="localVehicleTypeId" value="0"/>
                                                <input type="hidden" name="localPickupPointId" id="localPickupPointId" value="" class="form-control"  style="width: 120px;"/>
                                                <input name="localPickupPoint" id="localPickupPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/>
                                            </td>
                                            <td><input name="localDropPoint" id="localDropPoint" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockNumbers(event);"/>
                                                <input type="hidden" name="localDropPointId" id="localDropPointId" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="localPointRouteId" id="localPointRouteId" value="" class="form-control"  style="width: 120px;"/>
                                            </td>
                                            <td><input name="localTotalKm" id="localTotalKm" value="" class="form-control"  style="width: 120px;" readonly/><input type="hidden" name="localTotalHrs" id="localTotalHrs" value="" class="form-control"  style="width: 120px;"/><input type="hidden" name="localTotalMinutes" id="localTotalMinutes" value="" class="form-control"  style="width: 120px;"/></td>
                                            <td><input name="fromCBM" id="fromCBM" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td><input name="toCBM" id="toCBM" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                            <td><input name="flatRate" id="flatRate" value="" class="form-control"  style="width: 120px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>
                                        </tr>
                                        --%>
                                    </tbody>
                                </table>
                                <a href="#" id="addRow6" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route Code</span></a>
                                <br>
                                <br>


                            </div>
                        </div>

                        <!--<script>
                            function saveVendorContract(){
                                document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                                document.customerContract.submit();
                            }
                         </script>-->
                        <div id="deD"  class="tab-pane active" style="padding-left: 1px;display:none">
                            <script>
                                var contain = "";
                                $(document).ready(function() {
                                    var iCnt = 1;
                                    var rowCnt = 1;
                                    //alert(rowCnt)
                                    // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                    contain = $($("#routedeD")).css({
                                        padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                        borderTopColor: '#999', borderBottomColor: '#999',
                                        borderLeftColor: '#999', borderRightColor: '#999'
                                    });
                                    $(contain).last().after('<table id="mainTableFullTruck" width="100%"><tr></td>\n\
               <table style="display:none;border-spacing: 40px 10px;color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:25px;" cellspacing="10" id="routeDetails' + iCnt + '" border="1">\n\
                   <tr><td style="padding-left:10px;padding-right:10px;">Agreed Fuel Price</td>\n\
                    <td style="margin-left:-200px;">\n\
                    <input type="text" class="textbox" style="width:120px;height:25px;" name="agreedFuelPrice" id="agreedFuelPrice"  value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                       <td style="width:3px;">\n\
                          <select type="text" name="uom" id="uom" class="textbox"style="width:90px;height:25px;">\n\
                                      <option value="0">--Select--</option>\n\
                                      <option value="1">Litre</option>\n\
                                      <option value="2">Gallon</option>\n\
                                      <option value="3">Kilogram</option>\n\
                                      </select></td>\n\
                                        <td style="padding-left:10px;padding-right:10px;">Fuel Hike % for Revising Cost</td>\n\
                                 <td><input style="width:120px;height:25px;" class="textbox" type="text" name="hikeFuelPrice" id="hikeFuelPrice"  onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                  </tr></table><br><br>\n\
                                    <table class="table table-info mb30 " border="1"   id="routeDetails1' + iCnt + '"  style="width:100%;">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                    <td>Sno</td>\n\
                                    <td><center>Vehicle Type</center></td>\n\
                                    <td><center>Vehicle Units</center></td>\n\
                                    <td><center>Contract Category</center></td>\n\
                                   <td><center>Max Allowable KM Per Vehicle Per Month</center></td>\n\
                                    <td><center>Fixed Cost Per Vehicle per Month</center></td>\n\
                                    <td><center>Extra KM (Rate/KM)</center></td>\n\
                                    <td colspan="2"><br><center>Fixed Duration <br>Per day</center><br>\n\
                                    <table  border="0" >\n\
                                    <tr style="color:white;font-weight: bold;font-size:14px;"><td width="235px;"><center>Hours</center></td><td width="206px;"><center>Minutes</center></td></tr></table></td>\n\
                                    <td><center>Total Fixed Cost Per Month</center></td>\n\
                                    <td><center>Rate Per KM</center></td>\n\
                                    <td colspan="2"><br><br><center>Over Time Cost Per HR</center><br><br>\n\
                                    <table border="0">\n\
                                    <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"><td width="245px;"><center>Work Days</center></td><td width="216px;height="20px;"><center>Holidays</center></td></tr></table></td>\n\
                                    <td><center>Additional Cost</center></td></tr>\n\
                                     <tr>\n\
                                   <td> ' + iCnt + ' </td>\n\
                                   <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');" style="height:22px;width:100px;"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:80px;"/></td>\n\
                                   <td><select ype="text" name="contractCategory" id="contractCategory' + iCnt + '" style="height:22px;width:80px;"><option value="0">--Select--</option><option value="1">Fixed</option><option value="2">Actual</option></select></td>\n\
                                   <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                    <td><input type="text" name="fixedCost" id="fixedCost' + iCnt + '" value="0" onchange="calculateTotalFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                   <td><input type="text" name="rateLimit" id="rateLimit' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                    <td width="10px;"><select type="text" name="fixedHrs" id="fixedHrs' + iCnt + '" class="textbox" style="height:22px;width:45px;">\n\
                                              <option value="00">00</option>\n\
                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                   </td>\n\
                                    <td width="260px;"><select type="text" name="fixedMin" id="fixedMin' + iCnt + '" class="textbox" style="height:22px;width:45px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                   </td>\n\
                                    <td><input type="text" name="totalFixedCost" id="totalFixedCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                    <td><input type="text" name="rateCost" id="rateCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                    <td><input type="text" name="workingDays" id="workingDays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                    <td><input type="text" name="holidays" id="holidays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                    <td><input type="text" name="addCostDedicate" id="addCostDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                     </tr>\n\
                                    </table>\n\
                                  <br> <table border="0" width=""><tr>\n\
                                    <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + iCnt + ')" style="width:90px;height:30px;font-weight: bold;padding: 1px;"/>\n\
                                    <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" style="width:90px;height:30px;font-weight: bold;padding: 1px;"/>\n\
                               </tr></table></td></tr></table><br><br>');
                                    callOriginAjaxdeD(iCnt);
                                    callDestinationAjaxdeD(iCnt);
                                    $('#btAdd').click(function() {
                                        iCnt = iCnt + 1;

                                        callOriginAjaxdeD(iCnt);
                                        callDestinationAjaxdeD(iCnt);
                                        $('#maindeD').after(contain);
                                    });
                                    $('#btRemove').click(function() {
//                                        alert($('#mainTabledeD tr').size());
                                        if ($(contain).size() > 2) {
                                            $(contain).last().remove();
                                            iCnt = iCnt - 1;
                                        }
                                    });
                                });

                                // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                var divValue, values = '';
                                function GetTextValue() {
                                    $(divValue).empty();
                                    $(divValue).remove();
                                    values = '';
                                    $('.input').each(function() {
                                        divValue = $(document.createElement('div')).css({
                                            padding: '5px', width: '200px'
                                        });
                                        values += this.value + '<br />'
                                    });
                                    $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                    $('body').append(divValue);
                                }

                                $(document).ready(function() {
                                    $("#mAllow").keyup(function() {
//                                        alert($(this).val());
                                    });
                                })

                                function addRows(val) {
                                    //alert(val);
                                    var loadCnt = val;
                                    //    alert(loadCnt)
                                    //alert("loadCnt");
                                    //var loadCnt1 = ve;
                                    var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                                    // alert(routeInfoSize);
                                    var routeInfoSizeSub = routeInfoSize - 2;
                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                    $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                                <%--   <td><input type="text" name="vehicleTypeIddeDTemp" id="vehicleTypeIddeDTemp' + loadCnt + '" /></td>--%>\n\
                                   <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');" style="height:22px;width:100px;"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                   <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:80px;"/></td>\n\
                                   <td><select type="text" name="contractCategory" id="contractCategory' + routeInfoSizeSub + '" style="height:22px;width:80px;"><option value="">--Select--</option><option value="1">Fixed</option><option value="2">Actual</option></select></td>\n\
                                   <td><input type="text" name="maxAllowableKM" id="maxAllowableKM"' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                    <td><input type="text" name="fixedCost" id="fixedCost' + routeInfoSizeSub + '" onchange="calculateTotalFixedCost(' + routeInfoSizeSub + ')" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                   <td><input type="text" name="rateLimit" id="rateLimit' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                    <td width="10px;"><select type="text" name="fixedHrs" id="fixedHrs' + routeInfoSizeSub + '" class="textbox" style="height:22px;width:45px;">\n\
                                              <option value="00">00</option>\n\
                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                   </td>\n\
                                    <td width="260px;"><select type="text" name="fixedMin" id="fixedMin' + routeInfoSizeSub + '" class="textbox" style="height:22px;width:45px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                   </td>\n\
                                    <td><input type="text" name="totalFixedCost" id="totalFixedCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                    <td><input type="text" name="rateCost" id="rateCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                    <td><input type="text" name="workingDays" id="workingDays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                    <td><input type="text" name="holidays" id="holidays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                    <td><input type="text" name="addCostDedicate" id="addCostDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:100px;"/></td>\n\
                                  </tr>');
                                    // alert("loadCnt = "+loadCnt)
                                    loadCnt++;
                                    //   alert("loadCnt = +"+loadCnt)
                                }

                                function onSelectVal(val, countVal) {
                                    //                            alert("vehicleTypeIdDedicate"+val);
                                    document.getElementById("vehicleTypeIddeDTemp" + countVal).value = val;
                                }


                                function deleteRows(val) {
                                    var loadCnt = val;
                                    //     alert(loadCnt);
                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                    // alert(routeInfoDetails);
                                    if ($('#routeDetails1' + loadCnt + ' tr').size() > 4) {
                                        $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                        loadCnt = loadCnt - 1;
                                    } else {
                                        alert('One row should be present in table');
                                    }
                                }

                                function calculateTotalFixedCost(sno) {
//                                    alert(sno);
                                    var vehicleUnits = document.getElementById("vehicleUnitsDedicate" + sno).value;
                                    var fixedCost = document.getElementById("fixedCost" + sno).value;
                                    var totalCost = parseInt(vehicleUnits) * parseInt(fixedCost);
                                    document.getElementById("totalFixedCost" + sno).value = totalCost;
                                }

                                    </script>

                                    <script>
                                        function callOriginAjaxdeD(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'originNamedeD' + val;
                                            var pointIdId = 'originIddeD' + val;
                                            var desPointName = 'destinationNamedeD' + val;


                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: request.term,
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Name;
                                                    var id = ui.item.Id;
                                                    //alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + desPointName).focus();
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }

                                        function callDestinationAjaxdeD(val) {
                                            // Use the .autocomplete() method to compile the list based on input from user
                                            //alert(val);
                                            var pointNameId = 'destinationNamedeD' + val;
                                            var pointIdId = 'destinationIddeD' + val;
                                            var originPointId = 'originIddeD' + val;
                                            var truckRouteId = 'routeIddeD' + val;
                                            var travelKm = 'travelKmdeD' + val;
                                            var travelHour = 'travelHourdeD' + val;
                                            var travelMinute = 'travelMinutedeD' + val;

                                            //alert(prevPointId);
                                            $('#' + pointNameId).autocomplete({
                                                source: function(request, response) {
                                                    $.ajax({
                                                        url: "/throttle/getTruckCityList.do",
                                                        dataType: "json",
                                                        data: {
                                                            cityName: request.term,
                                                            originCityId: $("#" + originPointId).val(),
                                                            textBox: 1
                                                        },
                                                        success: function(data, textStatus, jqXHR) {
                                                            var items = data;
                                                            response(items);
                                                        },
                                                        error: function(data, type) {

                                                            //console.log(type);
                                                        }
                                                    });
                                                },
                                                minLength: 1,
                                                select: function(event, ui) {
                                                    var value = ui.item.Name;
                                                    var id = ui.item.Id;
                                                    //alert(id+" : "+value);
                                                    $('#' + pointIdId).val(id);
                                                    $('#' + pointNameId).val(value);
                                                    $('#' + travelKm).val(ui.item.TravelKm);
                                                    $('#' + travelHour).val(ui.item.TravelHour);
                                                    $('#' + travelMinute).val(ui.item.TravelMinute);
                                                    $('#' + truckRouteId).val(ui.item.RouteId);
                                                    //validateRoute(val,value);

                                                    return false;
                                                }

                                                // Format the list menu output of the autocomplete
                                            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                //alert(item);
                                                var itemVal = item.Name;
                                                itemVal = '<font color="green">' + itemVal + '</font>';
                                                return $("<li></li>")
                                                        .data("item.autocomplete", item)
                                                        .append("<a>" + itemVal + "</a>")
                                                        .appendTo(ul);
                                            };


                                        }

                                    </script>

                                    <div id="routedeD">

                                    </div>
                                    <br>
                                    <br>
                                    <center>
                                        <a><input type="button" class="btn btn-info btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                    </center>
                                    <br>
                                    <br>
                                </div>

                                <script>
                                    $('.btnNext').click(function() {
                                        $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                    });
                                    $('.btnPrevious').click(function() {
                                        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                    });
                                </script>

                            </div>
                            <br>
                            <br>
                            <center>
                                <input type="button" class="btn btn-info" value="Save" onclick="submitPage();" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                            </center>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    <%--   <%}catch(Exception e)
                 {
                        out.println(e.toString());
                    }
               %>--%>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>