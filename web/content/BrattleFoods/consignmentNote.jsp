<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>



<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });


    function setEwayBillNo() {
//        alert("inside");
        var invoiceAmt = document.getElementById("invoiceValue").value;
        var eWayBillNo = document.getElementById("eWayBillNo").value;
        if (invoiceAmt >= 50000) {
            $('#eWayBillLabel').show();
            $('#eWayBill').show();
        } else {
            $('#eWayBillLabel').hide();
            $('#eWayBill').hide();
        }
    }
    function validateEwayBillNo() {
//        alert("inside");
        var invoiceAmt = document.getElementById("invoiceValue").value;
        var eWayBillNo = document.getElementById("eWayBillNo").value;
        if (invoiceAmt >= 50000 && eWayBillNo == "") {
            alert("Please Enter E-Way Bill No");
            $('#eWayBillNo').focus();
        }
    }

</script>

<style>

    .loading {

        border:10px solid red;

        display:none;

    }

    .button1{border: 1px black solid ; color:#000 !important;  }



    .spinner{

        position: fixed;

        top: 50%;

        left: 50%;

        margin-left: -50px; /* half width of the spinner gif */

        margin-top: -50px; /* half height of the spinner gif */

        text-align:center;

        z-index:1234;

        overflow: auto;

        width: 300px; /* width of the spinner gif */

        height: 300px; /*hight of the spinner gif +2px to fix IE8 issue */

    }

    .ui-loader-background {

        width:100%;

        height:100%;

        top:0;

        padding: 0;

        margin: 0;

        background: rgba(0, 0, 0, 0.3);

        display:none;

        position: fixed;

        z-index:100;

    }

    .ui-loader-background {

        display:block;



    }



</style>

<script>
    function setContainerTypeList(sno, val) {
        if (val == '1058') {
            // 20FT vehicle
            $('#containerType' + sno).empty();
            $('#containerType' + sno).append($('<option ></option>').val(1).html('20'))
        } else if (val == '1059' || val == '1060') {
            // 40FT vehicle
            $('#containerType' + sno).empty();
            $('#containerType' + sno).append($('<option ></option>').val(0).html('--Select--'))
            $('#containerType' + sno).append($('<option ></option>').val(1).html('20'))
            $('#containerType' + sno).append($('<option ></option>').val(2).html('40'))

        } else {
            $('#containerType' + sno).empty();
            $('#containerType' + sno).append($('<option ></option>').val(4).html('0'));
            $('#containerQty' + sno).val('1');
            setContainerQtyList(sno, 1);
        }
    }
    function setContainerQtyList(sno, val) {
        var containerType = document.getElementById("containerType" + sno).value;
        var vehicletype = document.getElementById("vehTypeIdTemp" + sno).value;
        //var consolName = document.getElementById("consolName" + sno).value;
        var value = parseInt(val) % 2;
        if (containerType == '1' && value != 0 && vehicletype == '1059') {
            alert("Quantity has not be odd numbers for the selected vehicle type and container type")
            document.getElementById("containerQty" + sno).value = "";
            deleteRowContainer();
            $('#containerQty' + sno).focus();
        } else {
            getContainerFreightRate(sno);
        }
    }
</script>

<script type="text/javascript">

    function checkKey(obj, e, id, id1) {
        var idVal = $('#' + id).val();
        if (e.keyCode == 46 || e.keyCode == 8) {
            $('#' + obj.id).val('');


            $('#' + id).val('');
            $('#' + id1).val('');

        } else if (idVal != '' && e.keyCode != 13) {
            $('#' + obj.id).val('');

            $('#' + id).val('');
            $('#' + id1).val('');

        }
    }
    function checkKey1(obj, e, id, id1, id2) {
        var idVal = $('#' + id).val();
        if (e.keyCode == 46 || e.keyCode == 8) {
            $('#' + obj.id).val('');


            $('#' + id).val('');
            $('#' + id1).val('');
            $('#' + id2).val('');

        } else if (idVal != '' && e.keyCode != 13) {
            $('#' + obj.id).val('');

            $('#' + id).val('');
            $('#' + id1).val('');
            $('#' + id2).val('');

        }
    }
    function checkKeyForCustomer(obj, e, id) {
        var idVal = $('#' + id).val();
        if (e.keyCode == 46 || e.keyCode == 8) {
            $('#' + obj.id).val('');


            $('#' + id).val('');
            $('#customerCode').val('');
            $('#paymentType').val('');
            $('#customerAddress').val('');
            $('#pincode').val('');
            $('#customerMobileNo').val('');
            $('#mailId').val('');
            $('#customerPhoneNo').val('');
            $('#billingTypeId').val('');
            if (document.getElementById("movementType").value == '9' || document.getElementById("movementType").value == '10') {
                $('#emptyDestinationTemp').empty();
                $('#consigneeNameTemp').val('');
                $('#custName').val('');
                checkKey('consigneeNameTemp', 46, 'consigneeCode', 'consigneeAddressCount');
                checkKey1('custName', 46, 'billingParty', 'billPartyCode', 'consignorAddressCount');
            }
        } else if (idVal != '' && e.keyCode != 13) {
            $('#' + obj.id).val('');

            $('#' + id).val('');
            $('#customerCode').val('');
            $('#paymentType').val('');
            $('#customerAddress').val('');
            $('#pincode').val('');
            $('#customerMobileNo').val('');
            $('#mailId').val('');
            $('#customerPhoneNo').val('');
            $('#billingTypeId').val('');
//            if (document.getElementById("movementType").value == '9' || document.getElementById("movementType").value == '10' ) {
            if (document.getElementById("movementType").value == '9' || document.getElementById("movementType").value == '10' || document.getElementById("movementType").value == '11') {
                $('#emptyDestinationTemp').empty();
                $('#consigneeNameTemp').val('');
                $('#custName').val('');
                checkKey('consigneeNameTemp', 46, 'consigneeCode', 'consigneeAddressCount');
                checkKey1('custName', 46, 'billingParty', 'billPartyCode', 'consignorAddressCount');
            }
        }
    }

    function currentTime() {
//        alert("1111111111");
        var date, curr_hour, curr_minute;
        date = new Date();
        curr_hour = date.getHours();
        curr_minute = date.getMinutes();
        if (parseInt(curr_hour) < 10) {
            curr_hour = "0" + curr_hour;
        }
        if (parseInt(curr_minute) < 10) {
            curr_minute = "0" + curr_minute;
        }
//        alert("34343");
        document.getElementById("consignmentHour").value = curr_hour;
//        alert("545454");
        document.getElementById("graReportMinute").value = curr_minute;
//        alert("hjhjhj");
        document.getElementById("pointPlanHour1").value = curr_hour;
        document.getElementById("pointPlanMinute1").value = curr_minute;
    }


    //this autocomplete is used for consignee and consignor names
    $(document).ready(function () {
        //                alert("POST")
        // Use the .autocomplete() method to compile the list based on input from user
        $('#consignorName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getConsignorName.do",
                    dataType: "json",
                    data: {
                        consignorName: request.term,
                        customerId: document.getElementById('customerId').value

                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }

                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                $('#consignorName').val(value);
                $('#consignorPhoneNo').val(ui.item.Mobile);
                $('#consignorAddress').val(ui.item.Address);
                $('#consignorId').val(ui.item.custId);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#consigneeName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getConsigneeName.do",
                    dataType: "json",
                    data: {
                        consigneeName: request.term,
                        customerId: document.getElementById('customerId').value
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                $('#consigneeName').val(value);
                $('#consigneePhoneNo').val(ui.item.Mobile);
                $('#consigneeAddress').val(ui.item.Address);
                $('#consigneeId').val(ui.item.custId);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        }

    });

</script>

<script type="text/javascript">

    function convertStringToDate(y, m, d, h, mi, ss) {
        //1
        //var dateString = "2013-08-09 20:02:03";
        var dateString = y + '-' + m + '-' + d + ' ' + h + ':' + mi + ':' + ss;
        //alert(dateString);
        var reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
        var dateArray = reggie.exec(dateString);
        var dateObject = new Date(
                (+dateArray[1]),
                (+dateArray[2]) - 1, // Careful, month starts at 0!
                (+dateArray[3]),
                (+dateArray[4]),
                (+dateArray[5]),
                (+dateArray[6])
                );
        //alert(dateObject);
        return dateObject;

    }

    function viewCustomerContract() {
        window.open('/throttle/viewCustomerContract.do?custId=' + $("#customerId").val() + '&billingTypeId=' + $("#billingTypeId").val(), 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }


    function setHubId(sno) {

        if (sno == 2) {
            var origin = document.getElementById("pointNameTemp" + sno).value;


            var temp = origin.split('~');
            // alert(temp[0]);
            //alert(temp[1]);

            document.getElementById("originHub").value = temp[0];
            document.getElementById("pointId2").value = temp[0];
            document.getElementById("pointName2").value = temp[1];

        } else if (sno == 3) {
            var destination = document.getElementById("pointNameTemp" + sno).value;
            var temp = destination.split('~');
            //alert(temp[0]);
            //
            //alert(temp[1]);
            document.getElementById("destinationHub").value = temp[0];
            document.getElementById("pointId3").value = temp[0];
            document.getElementById("pointName3").value = temp[1];
        }

    }



    function validateRoute(val) {
        //            alert(val);
        var contractType = document.cNote.billingTypeId.value;
        //alert(contractType);
        if (contractType != '1' && contractType != '2') {

            var pointName = document.getElementById("pointName" + val).value;
            var pointIdValue = document.getElementById("pointId" + val).value;
            var pointOrderValue = document.getElementById("pointOrder" + val).value;
            var pointOrder = document.getElementsByName("pointOrder");
            var pointNames = document.getElementsByName("pointName");
            var pointIdPrev = 0;
            var pointIdNext = 0;
            var pointNamePrev = '';
            var pointNameNext = '';
            //            alert(pointOrderValue);
            //            alert(pointOrder.length);
            var prevPointOrderVal = parseInt(pointOrderValue) - 1;
            var nextPointOrderVal = parseInt(pointOrderValue) + 1;
            for (var m = 1; m <= pointOrder.length; m++) {
                //                    alert("loop:"+m+" :"+document.getElementById("pointOrder"+m).value +"pointOrderValue:"+pointOrderValue+"prevPointOrderVal:"+prevPointOrderVal+"nextPointOrderVal:"+nextPointOrderVal);
                if (document.getElementById("pointOrder" + m).value == prevPointOrderVal) {
                    pointIdPrev = document.getElementById("pointId" + m).value
                    pointNamePrev = document.getElementById("pointName" + m).value
                    //                        alert("pointIdPrev:"+pointIdPrev);
                }
                if (document.getElementById("pointOrder" + m).value == nextPointOrderVal) {
                    //                        alert("am here..");
                    pointIdNext = document.getElementById("pointId" + m).value
                    pointNameNext = document.getElementById("pointName" + m).value
                    //                           alert("pointIdNext:"+pointIdNext);
                }
            }
            //            alert(pointIdValue);
            //            alert(pointIdPrev);
            //            alert(pointIdNext);
            if (document.getElementById("movementType").value != 8) {
                ajaxRouteCheck(pointIdValue, pointIdPrev, pointIdNext, val, pointName, pointNamePrev, pointNameNext);
            }
        }
        //            else{
        //                var pointName = document.getElementById("pointName"+val).value;
        //                var pointIdValue = document.getElementById("pointId"+val).value;
        //                //alert(pointIdValue)
        //                if(pointIdValue == ''){
        //                    alert('please enter valid interim point');
        //                    document.getElementById("pointName"+val).value = '';
        //                    document.getElementById("pointName"+val).focus();
        //                }
        //            }

    }



    var podRowCount1 = 1;
    var podSno1 = 0;
    function addRouteCourse1(id, name, order, type, routeId, routeKm, routeReeferHr, startDate) {
        if (parseInt(podRowCount1) % 2 == 0)
        {
            styl = "form-control";
        } else {
            styl = "form-control";
        }
        podSno1++;
        var tab = document.getElementById("routePlan");
        var newrow = tab.insertRow(podRowCount1);




        cell = newrow.insertCell(0);
        var cell0 = "<td class='form-control' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='textbox' value='" + order + "' ></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(1);
        var cell0 = "<td class='form-control' height='25' ><input type='text' readonly  id='pointName" + podSno1 + "' name='pointName' class='textbox' value='" + name + "' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='" + routeKm + "' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='" + routeReeferHr + "' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='" + routeId + "' ><input type='hidden' id='pointId" + podSno1 + "'  name='pointId' value='" + id + "' ></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        if (type == 'select') {
            cell = newrow.insertCell(2);
            var cell0 = "<td class='form-control' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='textbox'><option value='PickUp'>PickUp</option><option value='Drop'>Drop</option></select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
        } else {
            cell = newrow.insertCell(2);
            var cell0 = "<td class='form-control' height='25' ><input type='text' readonly  id='pointType" + podSno1 + "'  name='pointType' class='textbox' value='" + type + "' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
        }
        cell = newrow.insertCell(3);
        var cell0 = "<td class='form-control' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='textbox' ></textarea></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(4);
        var cell0 = "<td class='form-control' height='25' ><input type='text' onChange='validateTripSchedule();' value='" + startDate + "'  readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(5);
        var cell0 = "<td class='form-control' height='25' >HH:<select name='pointPlanHour'  id='pointPlanHour" + podSno1 + "' onChange='validateTripSchedule();validateTransitTime(" + podSno1 + ");'class='textbox'><option value='00' selected>00</option><option value='01'>01</option><option value='02'>02</option><option value='03' selected >03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='textbox'><option value='00' selected>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option></select></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(6);
        var cell0 = "<td class='form-control' height='25' >&nbsp;</td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        podRowCount1++;

        $(document).ready(function () {

            $("#datepicker").datepicker({minDate: 0,
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });



        });

        $(function () {
            $(".datepicker").datepicker({minDate: 0,
                changeMonth: true, changeYear: true,
                dateFormat: 'dd-mm-yy'
            });

        });

    }


    function addRouteCourse3(id, name, order, type, routeId, routeKm, routeReeferHr, startDate) {
        if (parseInt(podRowCount1) % 2 == 0)
        {
            styl = "form-control";
        } else {
            styl = "form-control";
        }
        podSno1++;
        var tab = document.getElementById("routePlan");
        var newrow = tab.insertRow(podRowCount1);


        cell = newrow.insertCell(0);
        var cell0 = "<td class='form-control' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='textbox' value='" + order + "' ></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(1);
        var cell0 = "<td class='form-control' height='25' ><input type='text' readonly  id='pointName" + podSno1 + "' name='pointName' class='textbox' value='" + name + "' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='" + routeKm + "' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='" + routeReeferHr + "' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='" + routeId + "' ><input type='hidden' id='pointId" + podSno1 + "'  name='pointId' value='" + id + "' ></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        if (type == 'select') {
            cell = newrow.insertCell(2);
            var cell0 = "<td class='form-control' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='textbox'><option value='PickUp'>PickUp</option><option value='Drop'>Drop</option></select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
        } else {
            cell = newrow.insertCell(2);
            var cell0 = "<td class='form-control' height='25' ><input type='text' readonly  id='pointType" + podSno1 + "'  name='pointType' class='textbox' value='" + type + "' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
        }
        cell = newrow.insertCell(3);
        var cell0 = "<td class='form-control' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='textbox' ></textarea></td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        if (type == 'PickUp') {
            cell = newrow.insertCell(4);
            var cell0 = "<td class='form-control' height='25' ><input type='text' onChange='validateTripSchedule();'  value='" + startDate + "'   readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
        } else {
            cell = newrow.insertCell(4);
            var cell0 = "<td class='form-control' height='25' ><input type='text' onChange='validateTransitTime(" + podSno1 + ");'  value='" + startDate + "'   readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

        }
        if (type == 'PickUp') {
            cell = newrow.insertCell(5);
            var cell0 = "<td class='form-control' height='25' >HH:<select name='pointPlanHour' onChange='validateTripSchedule();'   id='pointPlanHour" + podSno1 + "'   class='textbox'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='textbox'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
        } else {
            cell = newrow.insertCell(5);
            var cell0 = "<td class='form-control' height='25' >HH:<select name='pointPlanHour' onChange='validateTransitTime(" + podSno1 + ");'   id='pointPlanHour" + podSno1 + "'   class='textbox'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='textbox'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option></select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            $(".datepicker").datepicker({
                changeMonth: true, changeYear: true
            });

        }



        podRowCount1++;

        $(document).ready(function () {

            $("#datepicker").datepicker({minDate: 0,
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });



        });

        $(function () {
            $(".datepicker").datepicker({minDate: 0,
                changeMonth: true, changeYear: true,
                dateFormat: 'dd-mm-yy'
            });

        });



    }


    function addRouteCourse2() {
        if (parseInt(podRowCount1) % 2 == 0)
        {
            styl = "form-control";
        } else {
            styl = "form-control";
        }
        //var pointOrderValue = document.getElementById("pointOrder" + parseInt(podRowCount1 - 1)).value;
        var pointOrderValue = document.getElementById("pointOrder2").value;
        var prevRowRouteNameValue = document.getElementById("pointName" + parseInt(podRowCount1 - 1)).value;
        var prevRowPointIdValue = document.getElementById("pointId" + parseInt(podRowCount1 - 1)).value;
        var prevRowRouteOrderValue = document.getElementById("pointOrder" + parseInt(podRowCount1 - 1)).value;
        if (prevRowRouteNameValue.trim() == '' || prevRowPointIdValue == '') {
            alert('please enter the enroute name for route order ' + prevRowRouteNameValue);
            document.getElementById("pointName" + parseInt(podRowCount1 - 1)).focus();
        } else {

            podSno1++;
            var tab = document.getElementById("routePlan");
            var newrow = tab.insertRow(podRowCount1 - 1);

            //alert(pointOrderValue);
            document.getElementById("pointOrder2").value = pointOrderValue;
            pointOrderValue = parseInt(pointOrderValue) + 1;
            var pointOrderSeq = parseInt(pointOrderValue) + 1;
            //            alert(pointOrderSeq);
            document.getElementById("pointOrder3").value = pointOrderSeq;
            //document.getElementById("pointOrder" + parseInt(podRowCount1 - 1)).value = pointOrderValue;


            //            var cell = newrow.insertCell(0);
            //            var cell0 = "<td class='form-control' height='25' >" + podSno1 + "</td>";
            //            cell.setAttribute("className", styl);
            //            cell.innerHTML = cell0;

            //alert(podSno1);
            cell = newrow.insertCell(0);
            var cell0 = "<td class='form-control' height='25' ><input type='text' readonly  id='pointOrder" + podSno1 + "' name='pointOrder' class='textbox' value='" + pointOrderValue + "' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell0 = "<td class='form-control' height='25' ><input type='text' id='pointName" + podSno1 + "' onChange='validateRouteOld(" + podSno1 + ");'  name='pointName' class='textbox' value='' ><input type='hidden' id='pointTransitHrs" + podSno1 + "'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm" + podSno1 + "'  name='pointRouteKm' value='' ><input type='hidden' id='pointRouteReeferHr" + podSno1 + "'  name='pointRouteReeferHr' value='' ><input type='hidden' id='pointRouteId" + podSno1 + "'  name='pointRouteId' value='0' ><input type='hidden' id='pointId" + podSno1 + "'  readonly name='pointId' class='textbox' value='0' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            callAjax(podSno1);
            cell = newrow.insertCell(2);
            var cell0 = "<td class='form-control' height='25' ><select name='pointType' id='pointType" + podSno1 + "' class='textbox'>\n\
            <option value='Empty PickUp Point'>Empty PickUp Point</option>\n\
            <option value='Loading Point'>Loading Point</option>\n\
            <option value='Customs Clearance Point' selected>Customs Clearance Point</option>\n\
            <option value='Unloading Point'>Unloading Point</option>\n\
            <option value='Drop Point'>Drop Point</option>\n\
            </select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell0 = "<td class='form-control' height='25' ><textarea id='pointAddresss" + podSno1 + "' name='pointAddresss' rows='2' cols='20' class='textbox' ></textarea></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='form-control' height='25' ><input type='text'  value=''  onChange='validateTransitTime(" + podSno1 + ");'     readonly name='pointPlanDate'  id='pointPlanDate" + podSno1 + "'   class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='form-control' height='25' >HH:<select name='pointPlanHour'  onChange='validateTransitTime(" + podSno1 + ");'    id='pointPlanHour" + podSno1 + "'   class='textbox'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute" + podSno1 + "'   class='textbox'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            podRowCount1++;

            document.getElementById('pointName' + podSno1).focus();

            $(".datepicker").datepicker({
                changeMonth: true, changeYear: true
            });

            $(document).ready(function () {

                $("#datepicker").datepicker({minDate: 0,
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function () {
                $(".datepicker").datepicker({minDate: 0,
                    changeMonth: true, changeYear: true,
                    dateFormat: 'dd-mm-yy'
                });

            });

        }
    }





</script>


<script type="text/javascript">

    var httpReqRouteCheck;
    var temp = "";
    function ajaxRouteCheck(pointIdValue, pointIdPrev, pointIdNext, val, pointName, pointNamePrev, pointNameNext)
    {

        var url = "/throttle/checkForRoute.do?pointIdValue=" + pointIdValue + "&pointIdPrev=" + pointIdPrev + "&pointIdNext=" + pointIdNext;
        if (pointIdValue != '') {
            if (window.ActiveXObject)
            {
                httpReqRouteCheck = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpReqRouteCheck = new XMLHttpRequest();
            }
            httpReqRouteCheck.open("GET", url, true);
            httpReqRouteCheck.onreadystatechange = function () {
                processAjaxRouteCheck(val, pointName, pointNamePrev, pointNameNext);
            };
            httpReqRouteCheck.send(null);
        } else {
            alert("invalid interim point: " + pointName);
            document.getElementById("pointName" + val).value = '';
            document.getElementById("pointName" + val).focus();
        }
    }

    function processAjaxRouteCheck(val, pointName, pointNamePrev, pointNameNext)
    {
        if (httpReqRouteCheck.readyState == 4)
        {
            if (httpReqRouteCheck.status == 200)
            {
                temp = httpReqRouteCheck.responseText.valueOf();
                alert(temp);
                var tempVal = temp.split('~');
                if (tempVal[0] == 0) {
                    alert("valid route does not exists for the selected point: " + pointName);
                    document.getElementById("pointName" + val).value = '';
                    document.getElementById("pointName" + val).focus();
                }
                if (tempVal[0] == 1) {
                    alert("valid route does not exists between " + pointName + " and " + pointNameNext);
                    document.getElementById("pointName" + val).value = '';
                    document.getElementById("pointName" + val).focus();
                }
                if (tempVal[0] == 2) {
                    document.getElementById("pointOrder" + val).focus();
                    var tempValSplit = tempVal[1].split("-");
                    document.getElementById("pointRouteId" + (val - 1)).value = tempValSplit[0];
                    document.getElementById("pointRouteKm" + (val - 1)).value = tempValSplit[1];
                    document.getElementById("pointRouteReeferHr" + (val - 1)).value = tempValSplit[2];
                    document.getElementById("pointTransitHrs" + (val - 1)).value = tempValSplit[3];


                    tempValSplit = tempVal[2].split("-");
                    document.getElementById("pointRouteId" + (val + 1)).value = tempValSplit[0];
                    document.getElementById("pointRouteKm" + (val + 1)).value = tempValSplit[1];
                    document.getElementById("pointRouteReeferHr" + (val + 1)).value = tempValSplit[2];
                    document.getElementById("pointTransitHrs" + (val + 1)).value = tempValSplit[3];
                }
            } else
            {
                alert("Error loading page\n" + httpReqRouteCheck.status + ":" + httpReqRouteCheck.statusText);
            }
        }
    }




    $(document).ready(function () {

        $("#datepicker").datepicker({minDate: 0,
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        $(".datepicker").datepicker({minDate: 0,
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });

</script>
<script type="text/javascript" language="javascript">
    var poItems = 0;
    var rowCount = '';
    var snumber = '';


    function estimateFreight() {
        var billingTypeId = document.getElementById("billingTypeId").value;
        var customerTypeId = document.getElementById("customerTypeId").value;
        //customertypeid 1-> contract; 2-> walk in
        //billling type id 1 -> point to point; 2 -> point to point based on wt; 3 -> actual weight
        var totalKm = 0;
        var totalHr = 0;
        var rateWithReefer = 0;
        var rateWithoutReefer = 0;
        if (customerTypeId == 1 && billingTypeId == 1) {//point to point
            var temp = document.cNote.vehTypeIdContractTemp.value;
            if (temp == 0) {
                alert("Freight cannot be estimated as the vehicle type is not chosen");
            } else {
                var temp = document.cNote.vehTypeIdContractTemp.value;
                //  alert(temp);
                var tempSplit = temp.split("-");
                rateWithReefer = tempSplit[5];
                rateWithoutReefer = tempSplit[6];
                totalKm = tempSplit[2];
                totalHr = tempSplit[3];
                if (document.cNote.reeferRequired.value == 'Yes') {
                    document.cNote.totFreightAmount.value = rateWithReefer;
                } else {
                    document.cNote.totFreightAmount.value = rateWithoutReefer;
                }
                document.cNote.totalKm.value = totalKm;
                document.cNote.totalHours.value = totalHr;
            }
        } else if (customerTypeId == 1 && billingTypeId == 2) {//point to point weight
            var temp = document.cNote.vehTypeIdContractTemp.value;
            if (temp == 0) {
                alert("Freight cannot be estimated as the vehicle type is not chosen");
            } else {
                var tempSplit = temp.split("-");
                rateWithReefer = tempSplit[7];
                rateWithoutReefer = tempSplit[8];
                totalKm = tempSplit[2];
                totalHr = tempSplit[3];
                var totalWeight = document.cNote.totalWeightage.value;
                if (document.cNote.reeferRequired.value == 'Yes') {
                    document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                } else {
                    document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                }
                document.cNote.totalKm.value = totalKm;
                document.cNote.totalHours.value = totalHr;
            }
        } else if (customerTypeId == 1 && billingTypeId == 3) {//actual kms
            var pointRouteKm = document.getElementsByName("pointRouteKm");
            var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");

            //calculate total km and total hrs
            for (var i = 0; i < pointRouteKm.length; i++) {
                totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
            }

            var temp = document.cNote.vehTypeIdTemp.value;
            if (temp == 0) {
                alert("Freight cannot be estimated as the vehicle type is not chosen");
            } else {
                var tempSplit = temp.split("-");
                var vehicleRatePerKm = tempSplit[1];
                var reeferRatePerHour = tempSplit[2];


                var vehicleFreight = parseFloat(totalKm) * parseFloat(vehicleRatePerKm);
                var reeferFreight = parseFloat(totalHr) * parseFloat(reeferRatePerHour);

                if (document.cNote.reeferRequired.value == 'Yes') {
                    document.cNote.totFreightAmount.value = (parseFloat(vehicleFreight) + parseFloat(reeferFreight)).toFixed(2);
                } else {
                    document.cNote.totFreightAmount.value = parseFloat(vehicleFreight).toFixed(2);
                }
            }
            document.cNote.totalKm.value = totalKm;
            document.cNote.totalHours.value = totalHr;
        } else if (customerTypeId == 2) { //walk in

            var pointRouteKm = document.getElementsByName("pointRouteKm");
            var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
            var walkInBillingTypeId = document.cNote.walkInBillingTypeId.value;
            //calculate total km and total hrs
            for (var i = 0; i < pointRouteKm.length; i++) {
                totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
            }
            if (walkInBillingTypeId == 1) {//fixed rate
                rateWithReefer = document.cNote.walkinFreightWithReefer.value;
                rateWithoutReefer = document.cNote.walkinFreightWithoutReefer.value;
                if (document.cNote.reeferRequired.value == 'Yes') {
                    document.cNote.totFreightAmount.value = parseFloat(rateWithReefer).toFixed(2);
                } else {
                    document.cNote.totFreightAmount.value = parseFloat(rateWithoutReefer).toFixed(2);
                }
            } else if (walkInBillingTypeId == 2) {//rate per km
                rateWithReefer = document.cNote.walkinRateWithReeferPerKm.value;
                rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKm.value;

                if (document.cNote.reeferRequired.value == 'Yes') {
                    document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithReefer)).toFixed(2);
                } else {
                    document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithoutReefer)).toFixed(2);

                }
            } else if (walkInBillingTypeId == 3) {// rate per kg
                var totalWeight = document.cNote.totalWeightage.value;
                rateWithReefer = document.cNote.walkinRateWithReeferPerKg.value;
                rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKg.value;
                if (document.cNote.reeferRequired.value == 'Yes') {
                    document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                } else {
                    document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                }

            }
            document.cNote.totFreightAmount1.value = document.cNote.totFreightAmount.value;
            document.cNote.totalKm.value = totalKm;
            document.cNote.totalHours.value = totalHr;
        }
    }

</script>

</head>

<script type="text/javascript">
    var rowCount = 1;
    var sno = 0;
    var rowCount1 = 1;
    var sno1 = 0;
    var httpRequest;
    var httpReq;
    var styl = "";

    function addRow1() {
        if (parseInt(rowCount1) % 2 == 0)
        {
            styl = "form-control";
        } else {
            styl = "form-control";
        }
        sno1++;
        var tab = document.getElementById("addTyres1");
        //find current no of rows
        var rowCountNew = document.getElementById('addTyres1').rows.length;
        rowCountNew--;
        var newrow = tab.insertRow(rowCountNew);

        var cell = newrow.insertCell(0);
        var cell0 = "<td class='form-control' height='25' > " + sno1 + "</td>";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        // Positions
//        cell = newrow.insertCell(1);
//        var cell0 = "<td class='form-control' height='30' >";
//        cell.setAttribute("className", styl);
//        cell.innerHTML = cell0;
        // TyreIds
        var cell = newrow.insertCell(1);
        var cell0 = "<td class='form-control' height='30' ><input type='hidden' name='productCodes' class='textbox' style='width:150px;height:40px' value='p001' ><input type='text' name='productNames' class='textbox' style='width:150px;height:40px;' value='NA' >";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

//        var cell = newrow.insertCell(3);
//        var cell0 = "<td class='form-control' height='30' >";
//        cell.setAttribute("className", styl);
//        cell.innerHTML = cell0;

        cell = newrow.insertCell(2);
        var cell1 = "<td class='form-control' height='30' ><input type='hidden' name='batchCode' class='textbox' style='width:150px;height:40px;' value='B001' ><input type='text' name='packagesNos'  value='0' onKeyPress='return onKeyPressBlockCharacters(event);' id='packagesNos' class='textbox' style='width:150px;height:40px;' value='' onkeyup='calcTotalPacks(this.value)'>";

        cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
        cell.setAttribute("className", "form-control");
        cell.innerHTML = cell1;

        cell = newrow.insertCell(3);
        var cell1 = "<td class='form-control' height='30' ><select name='uom' id='uom' class='textbox' style='width:150px;height:40px;'><option value='1'  >Box</option> <option value='2'>Bag</option><option value='3'>Pallet</option><option value='4'>Each</option><option value='5' selected >Packages</option></select> </td>";

        cell.setAttribute("className", "form-control");
        cell.innerHTML = cell1;
        cell = newrow.insertCell(4);
        var cell0 = "<td class='form-control' height='30' ><input type='text' name='productVolume' onKeyPress='return onKeyPressBlockCharacters(event);' id='productVolume' class='textbox' style='width:150px;height:40px;' value='0' onkeyup='calcTotalVolumes(this.value);packageAddComplete();' >";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(5);
        var cell0 = "<td class='form-control' height='30' ><input type='text' name='weights'   onKeyPress='return onKeyPressBlockCharacters(event);' id='weights' class='textbox' style='width:150px;height:40px;' value='0' onkeyup='calcTotalWeights(this.value);packageAddComplete();' >";
        cell.setAttribute("className", styl);
        cell.innerHTML = cell0;
        rowCount1++;
    }
    var rowCounts = 1;
    var snos1 = 0;
    var rowCounts1 = 1;
    var snos1 = 0;
    var httpRequests;
    var httpReqs;
    var styls = "";
    function addRow2() {
        if (parseInt(rowCounts1) % 2 == 0)
        {
            styls = "form-control";
        } else {
            styls = "form-control";
        }
        snos1++;
        var tab = document.getElementById("addContainer");
        //find current no of rows
        var rowCountNew = document.getElementById('addContainer').rows.length;
        rowCountNew--;
        var newrow = tab.insertRow(rowCountNew);
        var rowCount12 = rowCountNew;
        //                    alert("rowCountNew"+rowCountNew);
        //                    alert("sno1"+sno1);
        var movementType = document.getElementById('movementType').value;
        var userId = document.getElementById('userId').value;
        // Positions
        cell = newrow.insertCell(0);

        if (movementType == '9' || movementType == '10' || movementType == '11') {

//            var cell0 = "<td class='form-control' height='30' >-NA-<input type='hidden'   name='vehTypeIdTemp'  id ='vehTypeIdTemp" + snos1 + "' class='textbox' />";
            var cell0 = "<td class='form-control' height='30' ><input type='hidden'   name='vehTypeIdTemp'  id ='vehTypeIdTemp" + snos1 + "' class='textbox' /><input type='text'   name='consolName'  id ='consolName" + snos1 + "' class='textbox' value='' style='width:150px;height:40px;' />";
        } else {
            var cell0 = "<td class='form-control' height='30' ><input type='hidden'   name='consolName'  id ='consolName" + snos1 + "' class='textbox' value='' style='width:150px;height:40px;' /><select   name='vehTypeIdTemp' onchange='setContainerTypeList(" + snos1 + ",this.value)' id ='vehTypeIdTemp" + snos1 + "' class='textbox' style='width:150px;height:40px;' ><option value='0'>--select---</option> <c:if test="${vehicleTypeList != null}" ><c:forEach items="${vehicleTypeList}" var="vehicleType"><option  value='<c:out value="${vehicleType.vehicleTypeId}" />'><c:out value="${vehicleType.vehicleTypeName}" /> </c:forEach > </c:if>  </select>";
        }

        cell.setAttribute("className", styls);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(1);
        var cell0 = "<td class='form-control' height='30' ><select   name='containerType'  id ='containerType" + snos1 + "' class='textbox' style='width:150px;height:40px;' onchange='resetContainerQuantity(" + snos1 + ")' ><option value='0'>--select---</option> <c:if test="${containerTypeList != null}" ><c:forEach items="${containerTypeList}" var="details"><option  value='<c:out value="${details.containerId}" />'><c:out value="${details.containerName}" /> </c:forEach > </c:if>  </select>";
        cell.setAttribute("className", styls);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(2);
        var cell0 = "<td class='form-control' height='30' ><select   name='containerCategory'  id ='containerCategory" + snos1 + "' class='textbox' style='width:150px;height:40px;' onchange='resetContainerQuantity(" + snos1 + ")' ><option value='0'>--select---</option> <c:if test="${containerCategoryList != null}" ><c:forEach items="${containerCategoryList}" var="details"><option  value='<c:out value="${details.categoryId}" />'><c:out value="${details.categoryName}" /> </c:forEach > </c:if>  </select>";
        cell.setAttribute("className", styls);
        cell.innerHTML = cell0;

        cell = newrow.insertCell(3);
        var cell0 = "<td class='form-control' height='30' ><select   name='linerName' id ='linerName" + snos1 + "' class='textbox' style='width:150px;height:40px;' onchange='setConatinerLiner(" + snos1 + ")' ><option value='0'>--select---</option> <c:if test="${linerList != null}" ><c:forEach items="${linerList}" var="details"><option  value='<c:out value="${details.linerName}" />'><c:out value="${details.linerName}" /> </c:forEach > </c:if>  </select>";
        cell.setAttribute("className", styls);
        cell.innerHTML = cell0;
        // TyreIds
        var cell = newrow.insertCell(4);
        var cell0 = "<td class='form-control' height='30' ><input type='hidden' name='rate' id='rate" + snos1 + "' class='textbox' style='width:150px;height:40px;' readOnly ><input type='hidden' name='containerQtyOld' id='containerQtyOld" + snos1 + "'  value='0' class='textbox' style='width:150px;height:30px;' readOnly ><input type='text'  name='containerQty' id='containerQty" + snos1 + "'  class='textbox' maxlength='3' style='width:150px;height:40px;' ><input type='hidden' name='containerFreightChargeSingle' id='containerFreightChargeSingle" + snos1 + "'  value='0' class='textbox' readOnly >";
        cell.setAttribute("className", styls);
        cell.innerHTML = cell0;

        var cell = newrow.insertCell(5);
        var cell0 = "<td class='form-control' height='30' ><input type='text'  name='goodsDes' id='goodsDes" + snos1 + "'  onKeyPress='return onKeyPressBlockCharacters1(" + snos1 + ",event);'  class='textbox'  style='width:150px;height:40px;' > </td>";
        cell.setAttribute("className", styls);
        cell.innerHTML = cell0;

        var cell = newrow.insertCell(6);
        var cell0 = "<td class='form-control' height='30' ><input type='text'  name='tonnage' id='tonnage" + snos1 + "'  onKeyPress='return onKeyPressBlockCharacters(event);' onChange='setContainerQtyList(" + snos1 + ",this.value);'  class='textbox' maxlength='2' style='width:150px;height:40px;' > </td>";
        cell.setAttribute("className", styls);
        cell.innerHTML = cell0;

        var cell = newrow.insertCell(7);
        var cell0 = "<td class='form-control' height='30' ><font color='red'><span id='approvalStatusSpan" + snos1 + "'></span></font></td>";
        cell.setAttribute("className", styls);
        cell.innerHTML = cell0;

//        var cell = newrow.insertCell(5);
//        if (userId == '1523' || userId == '1403' ) {
//        var cell0 =  "<td class='form-control' style='display:block;' height='30' ><input type='text' name='rate' id='rate" + snos1 + "' class='textbox' style='width:150px;height:40px;' readOnly ><input type='hidden' name='containerQtyOld' id='containerQtyOld" + snos1 + "'  value='0' class='textbox' style='width:150px;height:30px;' readOnly >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='baxiimages/min1.jpg' onclick='deleteRow(" + snos1 + ")'/>";
//        }   else{
//        var cell0 =  "<td class='form-control' style='display:block;' height='30' ><input type='hidden' name='rate' id='rate" + snos1 + "' class='textbox' style='width:150px;height:40px;' readOnly ><input type='hidden' name='containerQtyOld' id='containerQtyOld" + snos1 + "'  value='0' class='textbox' style='width:150px;height:30px;' readOnly >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='baxiimages/min1.jpg' onclick='deleteRow(" + snos1 + ")'/>";
//        }
//        cell.setAttribute("className", styls);
//        cell.innerHTML = cell0;

//        var cell = newrow.insertCell(5);
//        var cell0 = "<td class='form-control' height='30' ><input type='tex't name='rate' id='rate" + snos1 + "' class='textbox' style='width:150px;height:40px;' readOnly ><input type='hidden' name='containerQtyOld' id='containerQtyOld" + snos1 + "'  value='0' class='textbox' style='width:150px;height:30px;' readOnly >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src='baxiimages/min1.jpg' onclick='deleteRow(" + snos1 + ")'/>";
//        cell.setAttribute("className", styls);
//        cell.innerHTML = cell0;

        rowCounts1++;
    }


    function deleteRow(sno) {
        //                    alert("sno"+sno);
        if (sno != 1) {
            //                        sno=sno-1;
            document.getElementById("addContainer").deleteRow(sno);
            //                        sno1--;
        } else {
            alert("Can't Delete The Row");
        }

    }
    function resetContainerQuantity(sno) {
        document.getElementById("containerQty" + sno).value = "";

    }
    function getContainerFreightRate(sno) {

        var customerTypeId = document.getElementById("customerTypeId").value;
        if (customerTypeId == '2') {
            $("#tdFreightAmountLabel").show();
            $("#tdFreightAmount").show();
            document.getElementById('totFreightAmount').readOnly = false;
        }
        if (parseInt(document.getElementById("containerQtyOld" + sno).value) < parseInt(document.getElementById("containerQty" + sno).value)) {

            var vehTypeIdTemp = document.getElementById("vehTypeIdTemp" + sno).value;
            var vehTypeName = $('#vehTypeIdTemp' + sno).find('option:selected').text();
            var containerType = document.getElementById("containerType" + sno).value;
            var containerTypeName = $('#containerType' + sno).find('option:selected').text();
            var regex = new RegExp('[0-9]+');
            var vehicleTypeValue = vehTypeName.match(regex);
            var containerTypeValue = containerTypeName.match(regex);
            var multiplyOddEvenStatus = 0;
            if (parseInt(vehicleTypeValue) == parseInt(containerTypeValue)) {

                multiplyOddEvenStatus = 0;
            } else {
                multiplyOddEvenStatus = 1;
            }
            var containerQty = document.getElementById("containerQty" + sno).value;
            var tonnage = document.getElementById("tonnage" + sno).value;
            var productCategoryId = document.getElementById("productCategoryId").value;
            //                    alert("vehTypeIdTemp == "+vehTypeIdTemp);
            //                    alert("containerType == "+containerType);



            var pointId = "";
            var movementType = document.getElementById("movementType").value;
            var pointIds = document.getElementsByName("pointId");
            if (movementType != '8') {
                for (var i = 0; i < pointIds.length; i++) {
                    //  alert(pointIds[i].value);
                    j = i + 1;
                    if (i != 0 && i != pointIds.length) {
                        j = j + 1;
                    }
                    if (pointIds[i].value == '' || pointIds[i].value == 0) {
                        alert(pointIds[i].value);
                        alert('please enter valid interim point1');
                        //alert(document.getElementById("pointName"+j).value);
                        document.getElementById("pointName" + j).focus();
                        statusCheck = false;
                    }

                    if (i == 0) {
                        pointId = pointIds[i].value;
                    } else {
                        pointId = pointId + "~" + pointIds[i].value;
                    }
                }
            }
            var loadType = 0;
            if (movementType == '1' || movementType == '31' || movementType == '32' || movementType == '2' || movementType == '29' || movementType == '30' || movementType == '4' || movementType == '5' ||
                    movementType == '6' || movementType == '10' || movementType == '9'
                    || movementType == '11'
                    || movementType == '12'
                    || movementType == '13'
                    || movementType == '14'
                    || movementType == '15' 
                    || movementType == '19' || movementType == '17'
                    || movementType == '20'
                    || movementType == '24'
                    || movementType == '25' || movementType == '26' || movementType == '27' || movementType == '28' || movementType == '33' || movementType == '34') {
                loadType = 2;
            } else {
                loadType = 1;
            }
            if (movementType == '9' || movementType == '10' || movementType == '11') {
                if (containerType == 1) { //20
                    vehTypeIdTemp = 1058;
                    containerType = 1;
                } else if (containerType == 2
                        ) { //40
                    vehTypeIdTemp = 1059;
                    containerType = 2;
                }
                document.getElementById("vehTypeIdTemp" + sno).value = vehTypeIdTemp;
            }
            document.getElementById("rate" + sno).value = "";

            if (movementType != 8 && movementType != 19) {
                $.ajax({
                    url: '/throttle/getContractVehicleTypeForPointsList.do',
                    data: {
                        contractId: $('#contractId').val(),
                        billingTypeId: $('#billingTypeId').val(),
                        tonnage: tonnage,
                        pointId: pointId,
                        vehicleTypeId: vehTypeIdTemp,
                        loadType: loadType,
                        containerTypeIds: containerType,
                        containerQty: containerQty,
                        productCategoryId: $('#productCategoryId').val(),
                        movementTypeId: movementType
                    },
                    dataType: 'json',
                    success: function (data) {
                        document.cNote.contractApprovalStatus.value = data.contractApprovalStatus;
                        if (data == '' || data == null) {

                            if ($('#customerTypeId').val() == 2) {
                                document.getElementById("rate" + sno).value = "";
                                getGenerateContainer(sno);
                            } else {
 //                            alert('Freight Charge is not available for chosen vehicle Type. please check Contract and its validity.');
                               alert('Freight Charge is not available for chosen vehicle Type. please check Contract.');
                                document.getElementById("rate" + sno).value = "";
                            }
                        } else {
                            $.each(data, function (i, data) {
                                var rate = 0;
                                if (document.cNote.reeferRequired.value == 'Yes') {
                                    if (movementType == '6') {
                                        rate = Math.round(parseInt(data.rateWithReefer) / 2);
                                    } else {

                                        rate = data.rateWithReefer;
                                    }
                                } else {
                                    if (movementType == '6') {
                                        rate = Math.round(parseInt(data.rateWithoutReefer) / 2);
                                    } else {

                                        rate = data.rateWithoutReefer;
                                    }

                                }
                                var multiplyCount = 0;
                                var qty = parseInt(containerQty);
                                var contractContainerQty = data.containerQty;
                                var approvalStatus = data.approvalStatus;
                                var orgWithoutRefeerRate = data.orgWithoutRefeerRate;
                                //alert(approvalStatus);
                                $("#approvalStatusSpan" + sno).text('');
                                if (approvalStatus == null || approvalStatus == "2") {
                                    $("#approvalStatusSpan" + sno).text('Contract rate is not approved. Order may not be available for Trip planning until approval, Org rate is' + orgWithoutRefeerRate);
                                    alert('Contract rate is not approved. Order may not be available for Trip planning until approval, Org rate is' + orgWithoutRefeerRate);
                                    //document.getElementById("rate" + sno).value = "";
                                    return false;
                                }

                                var billableQty = 0;
                                if (vehTypeIdTemp == '1058' || vehTypeIdTemp == '1059') {
                                    billableQty = Math.round(parseInt(qty) / parseInt(contractContainerQty));
                                } else {
                                    billableQty = Math.round(parseInt(qty));
                                }
                                document.getElementById("containerFreightChargeSingle" + sno).value = parseFloat(rate);
                                rate = parseFloat(billableQty * rate);
                                document.cNote.totalKm.value = data.totalKm;
                                document.cNote.totalHours.value = data.totalHours;
                                document.cNote.totalMinutes.value = data.totalMinutes;
                                document.cNote.baseRate.value = data.baseRate;
                                document.getElementById("rate" + sno).value = rate;
                                var rateName = document.getElementsByName("rate");
                                document.getElementById("totFreightAmount").value = "";
                                for (var j = 1; j <= rateName.length; j++) {
                                    if (document.getElementById("totFreightAmount").value == "") {
                                        document.getElementById("totFreightAmount").value = parseFloat(document.getElementById("rate" + j).value);
                                    } else {
                                        document.getElementById("totFreightAmount").value = parseFloat(document.getElementById("totFreightAmount").value) + parseFloat(document.getElementById("rate" + j).value);
                                    }
                                }
                            });
                            getGenerateContainer(sno);
                        }

                    }
                });
            } else if (movementType == 8) {
                var tempData = document.getElementById("distanceContractType").value.split("~");
                var temp1 = tempData[1].split("-");
                var fromDistance = temp1[0];
                var toDistance = temp1[1];
                $.ajax({
                    url: '/throttle/getDistanceContractRateList.do',
                    data: {
                        contractId: $('#contractId').val(),
                        vehicleTypeId: vehTypeIdTemp,
                        loadType: 2,
                        containerTypeIds: containerType,
                        containerQty: containerQty,
                        fromDistance: fromDistance,
                        toDistance: toDistance
                    },
                    dataType: 'json',
                    success: function (data) {

                        if (data == '' || data == null) {
//alert('Freight Charge is not available for chosen vehicle Type. please check Contract and its validity.');                        
                            alert('Freight Charge is not available for chosen vehicle Type. please check Contract..');
                            document.getElementById("rate" + sno).value = "";
                        } else {
                            $.each(data, function (i, data) {
                                var rate = 0;
                                if (document.cNote.reeferRequired.value == 'Yes') {


                                    rate = data.rateWithReefer;

                                } else {

                                    rate = data.rateWithoutReefer;
                                }
                                var qty = parseInt(containerQty);
                                var contractContainerQty = data.containerQty;

                                var billableQty = Math.round(parseInt(qty) / parseInt(contractContainerQty));
                                document.getElementById("containerFreightChargeSingle" + sno).value = parseFloat(rate);
                                rate = parseFloat(billableQty * rate);
                                document.cNote.totalKm.value = 0;
                                document.cNote.totalHours.value = 0;
                                document.cNote.totalMinutes.value = 0;
                                document.getElementById("rate" + sno).value = rate;
                                var rateName = document.getElementsByName("rate");
                                document.getElementById("totFreightAmount").value = "";
                                for (var j = 1; j <= rateName.length; j++) {
                                    if (document.getElementById("totFreightAmount").value == "") {
                                        document.getElementById("totFreightAmount").value = parseFloat(document.getElementById("rate" + j).value);
                                    } else {
                                        document.getElementById("totFreightAmount").value = parseFloat(document.getElementById("totFreightAmount").value) + parseFloat(document.getElementById("rate" + j).value);
                                    }
                                }
                            });
                            getGenerateContainer(sno);
                        }

                    }
                });

            } else if (movementType == 19) {

                document.getElementById("containerFreightChargeSingle" + sno).value = parseFloat("0.00");
                document.cNote.totalKm.value = 0;
                document.cNote.totalHours.value = 0;
                document.cNote.totalMinutes.value = 0;
                document.getElementById("rate" + sno).value = "0.00";
                document.getElementById("totFreightAmount").value = parseFloat("0.00");
                $("#tdFreightAmountLabel").show();
                $("#tdFreightAmount").show();
                document.getElementById('totFreightAmount').readOnly = false;
                getGenerateContainer(sno);
            }

        } else {
            document.getElementById("containerQty" + sno).value = document.getElementById("containerQtyOld" + sno).value;
            alert("please reset the container");
        }


    }
    function getVehicleTypeFreight(vehicleTypeId) {
        //if (parseInt(document.getElementById("containerQtyOld" + sno).value) < parseInt(document.getElementById("containerQty" + sno).value)) {
        //  alert("vehicleTypeId:"+vehicleTypeId);
        var vehTypeIdTemp = vehicleTypeId;
//            var vehTypeName = $('#vehTypeIdTemp' + sno).find('option:selected').text();
//            var containerType = document.getElementById("containerType" + sno).value;
//            var containerTypeName = $('#containerType' + sno).find('option:selected').text();
//            var regex = new RegExp('[0-9]+');
//            var vehicleTypeValue = vehTypeName.match(regex);
//            var containerTypeValue = containerTypeName.match(regex);
//            var multiplyOddEvenStatus = 0;
//            if (parseInt(vehicleTypeValue) == parseInt(containerTypeValue)) {
//
//                multiplyOddEvenStatus = 0;
//            } else {
//                multiplyOddEvenStatus = 1;
//            }
        //  var containerQty = document.getElementById("containerQty" + sno).value;
        var productCategoryId = document.getElementById("productCategoryId").value;
        //                    alert("vehTypeIdTemp == "+vehTypeIdTemp);
        //                    alert("containerType == "+containerType);



        var pointId = "";
        var movementType = document.getElementById("movementType").value;
        var pointIds = document.getElementsByName("pointId");
        if (movementType != '8') {
            for (var i = 0; i < pointIds.length; i++) {
                //  alert(pointIds[i].value);
                j = i + 1;
                if (i != 0 && i != pointIds.length) {
                    j = j + 1;
                }
                if (pointIds[i].value == '' || pointIds[i].value == 0) {
                    alert(pointIds[i].value);
                    alert('please enter valid interim point1');
                    //alert(document.getElementById("pointName"+j).value);
                    document.getElementById("pointName" + j).focus();
                    statusCheck = false;
                }

                if (i == 0) {
                    pointId = pointIds[i].value;
                } else {
                    pointId = pointId + "~" + pointIds[i].value;
                }
            }
        }

        var loadType = 0;
        if (movementType == '1' || movementType == '31' || movementType == '32' || movementType == '2'
        || movementType == '29' || movementType == '30' || movementType == '4' || movementType == '5' ||
                movementType == '6' || movementType == '10' || movementType == '9'
                || movementType == '11'
                || movementType == '12'
                || movementType == '13'
                || movementType == '14'
                || movementType == '15' 
                || movementType == '25'
                || movementType == '26'
                || movementType == '27' || movementType == '28') {
            loadType = 2;
        } else {
            loadType = 1;
        }
//		if (movementType == '9' || movementType == '10' || movementType == '11' ) {
//                if(containerType == 1 || containerType == 3 || containerType == 4 || containerType == 5 || containerType == 6 || containerType == 7){ //20
//                    vehTypeIdTemp = 1058;
//                    
//                }else if(containerType == 2 || containerType == 8 || containerType == 9 || containerType == 10 || containerType == 11 
//                        || containerType == 12
//                        || containerType == 13
//                        ){ //40
//                    vehTypeIdTemp = 1059;
//                   
//                }
//		document.getElementById("vehTypeIdTemp" + sno).value=vehTypeIdTemp;
//            }
        // document.getElementById("rate" + sno).value = "";

        if (movementType != 8) {
            $.ajax({
                url: '/throttle/getContractVehicleTypeForPointsList.do',
                data: {
                    contractId: $('#contractId').val(),
                    pointId: pointId,
                    billingTypeId: $('#billingTypeId').val(),
                    tonnage: 0,
                    vehicleTypeId: vehTypeIdTemp,
                    loadType: loadType,
                    containerTypeIds: 4,
                    containerQty: 0
                },
                dataType: 'json',
                success: function (data) {
                    document.cNote.contractApprovalStatus.value = data.contractApprovalStatus;
                    if (data == '' || data == null) {
 //                       alert('Freight Charge is not available for chosen vehicle Type. please check Contract and its validity1.');
                        alert('Freight Charge is not available for chosen vehicle Type... please check Contract.');
                        document.getElementById("rate" + sno).value = "";
                    } else {
                        $.each(data, function (i, data) {
                            var rate = 0;
                            if (document.cNote.reeferRequired.value == 'Yes') {
                                if (movementType == '6') {
                                    rate = Math.round(parseInt(data.rateWithReefer) / 2);
                                } else {

                                    rate = data.rateWithReefer;
                                }
                            } else {
                                if (movementType == '6') {
                                    rate = Math.round(parseInt(data.rateWithoutReefer) / 2);
                                } else {

                                    rate = data.rateWithoutReefer;
                                }

                            }
                            var multiplyCount = 0;
                            var qty = parseInt(1);
                            var contractContainerQty = data.containerQty;
                            var approvalStatus = data.approvalStatus;
                            var orgWithoutRefeerRate = data.orgWithoutRefeerRate;
                            //alert(approvalStatus);
                            $("#approvalStatusSpan" + sno).text('');
                            if (approvalStatus == null || approvalStatus == "2") {
                                $("#approvalStatusSpan" + sno).text('Contract rate is not approved. Order may not be available for Trip planning until approval, Org rate is' + orgWithoutRefeerRate);
                                alert('Contract rate is not approved. Order may not be available for Trip planning until approval, Org rate is' + orgWithoutRefeerRate);
                                //document.getElementById("rate" + sno).value = "";
                            }

//                                var billableQty=0;
//                              if(vehTypeIdTemp=='1058' || vehTypeIdTemp=='1059'){
//                                 billableQty = Math.round(parseInt(qty) / parseInt(contractContainerQty));
//                            }else{
//                                 billableQty = Math.round(parseInt(qty));
//                             }alert(billableQty);
//                              //  document.getElementById("containerFreightChargeSingle" + sno).value = parseFloat(rate);
//                                rate = parseFloat(billableQty * rate);

                            document.cNote.totalKm.value = data.totalKm;
                            document.cNote.totalHours.value = data.totalHours;
                            document.cNote.totalMinutes.value = data.totalMinutes;
                            //  document.getElementById("rate" + sno).value = rate;
                            var rateName = document.getElementsByName("rate");
                            document.getElementById("totFreightAmount").value = "";
                            document.getElementById("totFreightAmount").value = parseFloat(rate);
//                                for (var j = 1; j <= rateName.length; j++) {
//                                    if (document.getElementById("totFreightAmount").value == "") {
//                                        document.getElementById("totFreightAmount").value = parseFloat(document.getElementById("rate" + j).value);
//                                    } else {
//                                        document.getElementById("totFreightAmount").value = parseFloat(document.getElementById("totFreightAmount").value) + parseFloat(document.getElementById("rate" + j).value);
//                                    }
//                                }
                        });
                    }

                }
            });
        }
        //  getGenerateContainer(sno);
//        } else {
//            document.getElementById("containerQty" + sno).value = document.getElementById("containerQtyOld" + sno).value;
//            alert("please reset the container");
//        }
    }
    function packageAddCompleteOLD() {

        if (document.getElementById("movementType").value == 14 || document.getElementById("movementType").value == 16) {
            //$('#packageAddButton').hide();
            $('#packageCompleteButton').hide();

            var originId = document.cNote.contractRouteOrigin1.value;
            ;
            var destinationId = document.cNote.contractRouteDestination1.value;

            //alert(originId+":"+destinationId);

            $.ajax({
                url: '/throttle/getLclContractRateList.do',
                data: {
                    contractId: $('#contractId').val(),
                    vehicleTypeId: 0,
                    loadType: 2,
                    fromId: originId,
                    toId: destinationId
                },
                dataType: 'json',
                success: function (data) {

                    if (data == '' || data == null) {
                        alert('Freight Charge is not available. please check Contract.');
                        document.getElementById("rate" + sno).value = "";
                    } else {
                        $.each(data, function (i, data) {
                            var ratePerKg = 0;
                            var ratePerCBM = 0;
                            ratePerKg = data.rateWithReefer;
                            ratePerCBM = data.rateWithoutReefer;

                            var totalFreightBasedOnRatePerKg = 0;
                            var totalFreightBasedOnRatePerCbm = 0;
                            var totalWeight = document.cNote.totalWeightage.value;
                            var totalCbm = document.cNote.totalVolumes.value;
                            totalFreightBasedOnRatePerKg = parseFloat(ratePerKg) * parseFloat(totalWeight);
                            totalFreightBasedOnRatePerCbm = parseFloat(ratePerCBM) * parseFloat(totalCbm);


                            document.getElementById("totFreightAmount").value = "";
                            if (totalFreightBasedOnRatePerKg > totalFreightBasedOnRatePerCbm) {
                                document.getElementById("totFreightAmount").value = parseFloat(totalFreightBasedOnRatePerKg);
                            } else {
                                document.getElementById("totFreightAmount").value = parseFloat(totalFreightBasedOnRatePerCbm);
                            }

                        });
                    }

                }
            });
        }
    }
    function calculateTravelHours(sno) {
        var endDates = document.getElementById('endDates' + sno).value;
        var endTimeIds = document.getElementById('endTimeIds' + sno).value;
        var tempDate1 = endDates.split("-");
        var tempTime1 = endTimeIds.split(":");
        var stDates = document.getElementById('stDates' + sno).value;
        var stTimeIds = document.getElementById('stTimeIds' + sno).value;
        var tempDate2 = stDates.split("-");
        var tempTime2 = stTimeIds.split(":");
        var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
        var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], tempTime1[0], tempTime1[1]);              // now
        var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
        var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
        document.getElementById('timeDifferences' + sno).value = hoursDifference;
    }

    function goToImportPage() {
        document.cNote.action = '/throttle/cnoteUploadPage.do';
        document.cNote.submit();
    }
        </script>
        &nbsp;
        <script>
            //endingPointIds
            function resetAddRow(sno) {
                if (document.getElementById('endingPointIds' + sno).value == document.getElementById('destination').value) {
                    //           addRow
                    document.getElementById('addRowDetails').style.display = 'none';
                }
            }
        </script>



        <script>

            function callAjax(val) {
                // Use the .autocomplete() method to compile the list based on input from user

                var pointNameId = 'pointName' + val;
                var pointIdId = 'pointId' + val;
                var prevPointId = 'pointId' + (parseInt(val) - 1);
                var pointOrder = 'pointOrder' + val;

                var pointOrderVal = $('#' + pointOrder).val();
                var prevPointOrderVal = parseInt(pointOrderVal) - 1;
                var prevPointId = 0;
                var pointIds = document.getElementsByName("pointId");
                var pointOrders = document.getElementsByName("pointOrder");
                for (var m = 0; m < pointIds.length; m++) {
                    //alert("loop value:"+pointOrders[m].value +" : "+pointIds[m].value );
                    if (pointOrders[m].value == prevPointOrderVal) {
                        //alert("am here:"+pointOrders[m].value +" : "+prevPointOrderVal);
                        prevPointId = pointIds[m].value;
                    }
                }

                $('#' + pointNameId).autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getCityList.do",
                            dataType: "json",
                            data: {
                                cityName: request.term,
                                prevPointId: prevPointId,
                                pointOrder: $('#' + pointOrder).val(),
                                billingTypeId: $('#billingTypeId').val(),
                                contractId: $('#contractId').val(),
                                textBox: 1
                            },
                            success: function (data, textStatus, jqXHR) {
                                var contractType = document.cNote.billingTypeId.value;
                                //alert(contractType);
                                if (contractType == '1' || contractType == '2') {
                                    if (data == '') {
                                        $('#' + pointIdId).val(0);
//                                        alert('please enter valid interim point');
                                        $('#' + pointNameId).val('');
                                        $('#' + pointNameId).focus();
                                    }
                                }

                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {

                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        var value = ui.item.Name;
                        var id = ui.item.Id;
                        //alert(id+" : "+value);
                        $('#' + pointIdId).val(id);
                        $('#' + pointNameId).val(value);
                        //validateRoute(val,value);

                        return false;
                    }

                    // Format the list menu output of the autocomplete
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };


            }
            //end ajax for vehicle Nos


            // Use the .autocomplete() method to compile the list based on input from user
            $(document).ready(function () {
                $('#custName').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getConsignorDetails.do",
//                            url: "/throttle/getCustomerDetailsWithoutContract.do",
                            dataType: "json",
                            data: {
                                customerName: request.term,
                                customerCode: document.getElementById('customerCode').value,
                                customerId: document.getElementById('customerId').value
                            },
                            success: function (data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        $("#custName").val(ui.item.Name);
                        var $itemrow = $(this).closest('tr');
                        var value = ui.item.Name;
                        var tmp = value.split('~');
                        $itemrow.find('#billingParty').val(tmp[0]);
                        $itemrow.find('#custName').val(tmp[1]);
                        $itemrow.find('#billPartyCode').val(tmp[2]);
                        $('#consignorId').val(tmp[0]);
                        $('#consignorName').val(tmp[1]);
                        $('#consignorPhoneNo').val(tmp[8]);
                        $('#consignorAddressCount').val(tmp[13]);

                        $('#consignorAddress').text(tmp[3]);
                        return false;


                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('~');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);


                };


            });
            // second Customer Ajax
            $(document).ready(function () {
                $('#consigneeNameTemp').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getConsigneeDetails.do",
//                            url: "/throttle/getCustomerDetailsWithoutContract.do",
                            dataType: "json",
                            data: {
                                customerName: request.term,
                                customerCode: document.getElementById('customerCode').value,
                                customerId: document.getElementById('customerId').value
                            },
                            success: function (data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        $("#consigneeNameTemp").val(ui.item.Name);
                        var $itemrow = $(this).closest('tr');
                        var value = ui.item.Name;
                        var tmp = value.split('~');

                        $itemrow.find('#consigneeCode').val(tmp[2]);
                        //                    $itemrow.find('#customerCode').val(tmp[2]);
                        $('#consigneeId').val(tmp[0]);
                        $('#consigneeNameTemp').val(tmp[1]);
                        $('#consigneeName').val(tmp[1]);
                        $('#consigneePhoneNo').val(tmp[8]);
                        $('#consigneeAddressCount').val(tmp[13]);
                        $('#consigneeAddress').text(tmp[3]);
                        return false;

                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('~');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });
            //
            $(document).ready(function () {
                $('#secCustName').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getCustomerDetailsWithoutContract.do",
                            dataType: "json",
                            data: {
                                customerName: request.term,
                                customerCode: document.getElementById('customerCode').value
                            },
                            success: function (data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        $("#secCustName").val(ui.item.Name);
                        var $itemrow = $(this).closest('tr');
                        var value = ui.item.Name;
                        var tmp = value.split('~');
                        $itemrow.find('#secCustId').val(tmp[0]);
                        $itemrow.find('#secCustName').val(tmp[1]);
                        $itemrow.find('#secCustCode').val(tmp[2]);
                        //                    $itemrow.find('#customerCode').val(tmp[2]);

                        return false;

                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('~');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });

            // new ajax end
            $(document).ready(function () {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#customerName').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getCustomerDetails.do",
                            dataType: "json",
                            data: {
                                customerName: request.term,
                                customerCode: document.getElementById('customerCode').value
                            },
                            success: function (data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        $("#customerName").val(ui.item.Name);
                        var value = ui.item.Name;
                        var tmp = value.split('~');
                        var panNo = tmp[21];
                        var organizationId = tmp[23];
                        var gstNo = tmp[22];
                        var checke = "1";
                        if (organizationId == '') {
                            checke = "0";
                            alert("Cnote cannot be create since Organization for customer is Empty");
                            return;
                        }
                        if (organizationId == '1' && panNo == '') {
                            checke = "0";
                            alert("Cnote cannot be create since Organization,Pan No,GST No for customer is Empty");
                            return;
                        }
                        if (organizationId == '2' && gstNo == '' && panNo == '') {
                            checke = "0";
                            alert("Cnote cannot be create since Organization is company,GST No for customer is Empty");
                            return;
                        }
                        if (panNo == '') {
                            checke = "0";
                            alert("Cnote cannot be create since PAN no for customer is Empty");
                            return;
                        }
                        if (checke == "1") {




                            $('#customerId').val(tmp[0]);
                            //                    $('#customerName').val(tmp[1]);
                            $('#customerCode').val(tmp[2]);
                            $('#customerAddress').text(tmp[3]);
                            $('#pincode').val(tmp[4]);
                            $('#customerMobileNo').val(tmp[6]);
                            $('#customerPhoneNo').val(tmp[5]);
                            $('#mailId').val(tmp[7]);
                            $('#billingTypeId').val(tmp[8]);
                            $('#billingTypeName').text(tmp[9]);
                            $('#billTypeName').text(tmp[9]);
                            $('#contractNo').text(tmp[10]);
                            $('#contractExpDateTemp').text(tmp[11]);
                            $('#contractExpDate').val(tmp[11]);
                            $('#contractId').val(tmp[12]);
                            $('#paymentType').val(tmp[13]);
                            $('#creditLimitTemp').text(tmp[14]);
                            $('#creditDaysTemp').text(tmp[15]);
                            $('#outStandingTemp').text(tmp[16]);
                            $('#customerRankTemp').text(tmp[17]);
                            if (tmp[18] == "0") {
                                $('#approvalStatusTemp').text("No");
                            } else {
                                $('#approvalStatusTemp').text("Yes");
                            }
                            $('#outStandingDateTemp').text(tmp[19]);

                            $('#creditLimit').val(tmp[14]);
                            $('#creditDays').val(tmp[15]);
                            $('#outStanding').val(tmp[16]);
                            $('#customerRank').val(tmp[17]);
                            $('#approvalStatus').val(tmp[18]);
                            $('#outStandingDate').val(tmp[19]);

                            $('#creditLimitTable').hide();


                            //$('#ahref').attr('href', '/throttle/viewCustomerContract.do?custId=' + tmp[0]);
                            $("#contractDetails").show();
                            document.getElementById('customerCode').readOnly = true;
                            var movementType = document.getElementById('movementType').value;
                            $('#origin').empty();
                            $('#origin').append(
                                    $('<option style="width:150px"></option>').val(0).html('--Select--')
                                    )


                            var movementType = document.getElementById("movementType").value;
//                            if (parseInt(movementType) == 9 || parseInt(movementType) == 10) {
                            if (parseInt(movementType) == 9 || parseInt(movementType) == 10 || parseInt(movementType) == 11) {
                                setEmptyDestination(tmp[0], tmp[12], movementType);
                                document.getElementById("custName").readOnly = true;
                                document.getElementById("consigneeNameTemp").readOnly = true;
                                $('#consigneeId').val(tmp[0]);
                                $('#consigneeNameTemp').val(tmp[1]);
                                $('#consigneeName').val(tmp[1]);
                                $('#consigneeCode').val(tmp[2]);
                                $('#consigneePhoneNo').val(tmp[5]);
                                $('#consigneeAddressCount').val('1');
                                $('#consigneeAddress').text(tmp[3]);
                                $('#billingParty').val(tmp[0]);
                                $('#custName').val(tmp[1]);
                                $('#billPartyCode').val(tmp[2]);
                                $('#consignorId').val(tmp[0]);
                                $('#consignorName').val(tmp[1]);
                                $('#consignorPhoneNo').val(tmp[5]);
                                $('#consignorAddressCount').val('1');
                                $('#consignorAddress').text(tmp[3]);

                            }


                            if (movementType != '8') {
                                if (tmp[8] == '3') {//actual kms contract
                                    setOriginList(tmp[12]);
                                    setVehicleTypeForActualKM(tmp[12]);
                                    // $('#vehicleTypeDiv').show();
                                    $('#vehicleTypeContractDiv').hide();
                                    //   $('#contractRouteDiv2').show();
                                    $('#contractRouteDiv1').hide();
                                } else {
                                    getContractRoutes(tmp[12]);
                                    getContractRoutesOrigin(tmp[12]);
                                    getContractRoutesOriginExp(tmp[12]);
                                    getContractRoutesOriginRepo(tmp[12]);


                                    //    $('#vehicleTypeDiv').hide();
                                    // $('#vehicleTypeContractDiv').show(); for DICT Change.show all possible vehicle type
                                    $('#vehicleTypeDiv').show();
                                    // $('#contractRouteDiv1').show();
                                    $('#contractRouteDiv2').hide();
                                }

                            } else if (movementType == '8') {

                                getDistanceContractType(tmp[0]);
                                $('#distanceContractTypeDiv').show();
                            }
                            getBillingPartyCheck();
                            fetchCustomerProducts(tmp[1]);
                            return false;
                        }
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('~');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

                $('#customerCode').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getCustomerDetails.do",
                            dataType: "json",
                            data: {
                                customerCode: request.term,
                                customerName: document.getElementById('customerName').value
                            },
                            success: function (data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        $("#customerCode").val(ui.item.Name);
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#customerId').val(tmp[0]);
                        $('#customerName').val(tmp[1]);
                        $('#customerCode').val(tmp[2]);
                        $('#customerAddress').val(tmp[3]);
                        $('#customerMobileNo').val(tmp[4]);
                        $('#customerPhoneNo').val(tmp[5]);
                        $('#mailId').val(tmp[6]);
                        $('#billingTypeId').val(tmp[7]);
                        $('#billingTypeName').val(tmp[8]);
                        $('#billTypeName').val(tmp[8]);
                        $("#contractDetails").show();
                        document.getElementById('customerName').readOnly = true;


                        return false;
                    }
                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[2] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

            });

            function setOriginList(val) {
                var movementType = document.getElementById("movementType").value;
                $.ajax({
                    url: '/throttle/getContractRouteList.do',
                    data: {contractId: val,
                        billingTypeId: $('#billingTypeId').val(),
                        customerTypeId: $('#customerTypeId').val(),
                        movementTypeId: movementType},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (i, data) {
                            $('#originTemp').append(
                                    $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                                    )
                        });
                    }
                });

            }
            function setEmptyDestination(customerId, contractId, movementType) {
                if (contractId != '') {
                    if (movementType == 9) {
                        $.ajax({
                            url: '/throttle/getEmptyContainerLoction.do',
                            data: {contractId: contractId, customerId: customerId, customerTypeId: $('#customerTypeId').val(), movementType: movementType},
                            dataType: 'json',
                            success: function (data) {
                                $('#emptyDestinationTemp').empty();
                                $('#emptyDestinationTemp').append(
                                        $('<option ></option>').val("0").html("--select--")
                                        )
                                $.each(data, function (i, data) {
                                    $('#emptyDestinationTemp').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                            }
                        });
                    } else if (movementType == 10) {
                        $.ajax({
                            url: '/throttle/getEmptyContainerLoction.do',
                            data: {contractId: contractId, customerId: customerId, customerTypeId: $('#customerTypeId').val(), movementType: movementType},
                            dataType: 'json',
                            success: function (data) {
                                $('#emptyDestinationTemp').empty();
                                $('#emptyDestinationTemp').append(
                                        $('<option ></option>').val("0").html("--select--")
                                        )
                                $.each(data, function (i, data) {

                                    $('#emptyDestinationTemp').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                            }
                        });
                    } else if (movementType == 11) {
                        $.ajax({
                            url: '/throttle/getEmptyContainerLoction.do',
                            data: {contractId: contractId, customerId: customerId, customerTypeId: $('#customerTypeId').val(), movementType: 9},
                            dataType: 'json',
                            success: function (data) {
                                $('#emptyDestinationTemp').empty();
                                $('#emptyDestinationTemp').append(
                                        $('<option ></option>').val("0").html("--select--")
                                        )
                                $.each(data, function (i, data) {
                                    $('#emptyDestinationTemp').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                            }
                        });
                    }

                }


            }

            function setRoute() {
                resetRouteInfo();
                var movementType = document.getElementById("movementType").value;
                var startDate = document.cNote.startDate.value;
                addRouteCourse3(document.cNote.emptyExportOrigin.value, document.cNote.emptyExportOrigin.options[document.cNote.emptyExportOrigin.selectedIndex].text, '1', 'PickUp', 0, 0, 0, startDate);

                addRouteCourse3(document.cNote.emptyDestinationTemp.value, document.cNote.emptyDestinationTemp.options[document.cNote.emptyDestinationTemp.selectedIndex].text, '2', 'Interam Point', 0, 0, 0, startDate);

                addRouteCourse3(document.cNote.emptyExportDestination.value, document.cNote.emptyExportDestination.options[document.cNote.emptyExportDestination.selectedIndex].text, '3', 'Drop', 0, 0, 0, startDate);
                document.cNote.origin.value = document.cNote.distanceOriginId.value;
                document.cNote.destination.value = document.cNote.distanceDestinationId.value;

            }
            function getDistanceContractType(val) {
                $.ajax({
                    url: '/throttle/getDistanceContractType.do',
                    data: {customerId: val},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (i, data) {
                            $('#distanceContractType').append(
                                    $('<option style="width:150px"></option>').val(data.Id + '~' + data.Name).html(data.Name)
                                    )
                        });
                    }
                });

            }
            function fetchCustomerProducts(val) {
                var custId = document.cNote.customerId.value;
                //alert(custId + ":::" +val );
                document.cNote.customerName.value = val;
                if (custId != '' && custId != '0') {
                    $.ajax({
                        url: '/throttle/fetchCustomerProducts.do',
                        data: {customerId: custId},
                        dataType: 'json',
                        success: function (data) {
                            if (data != '') {
                                $('#productCategoryIdTemp').empty();
                                $('#productCategoryIdTemp').append(
                                        $('<option ></option>').val(0).html('--select--')
                                        )
                                $.each(data, function (i, data) {

                                    $('#productCategoryIdTemp').append(
                                            $('<option></option>').val(data.Id).html(data.Name)
                                            )

                                });
                            }
                        }
                    });
                }

            }
            function setVehicleTypeForActualKM(val) {
                $('#vehTypeIdTemp').empty();
                $('#vehTypeIdTemp').append(
                        $('<option ></option>').val(0).html('--select--')
                        )
                $.ajax({
                    url: '/throttle/getVehicleTypeForActualKM.do',
                    data: {contractId: val},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (i, data) {
                            $('#vehTypeIdTemp').append(
                                    $('<option ></option>').val(data.Id).html(data.Name)
                                    )
                        });
                    }
                });

            }

            function getContractRoutes(val) {
                $.ajax({
                    url: '/throttle/getContractRoutes.do',
                    data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: $('#customerTypeId').val()},
                    dataType: 'json',
                    success: function (data) {
                        $.each(data, function (i, data) {
                            $('#contractRoute').append(
                                    $('<option ></option>').val(data.Id).html(data.Name)
                                    )
                        });
                    }
                });
            }
            // For Repo
            function getContractRoutesOriginRepo(val) {
                var movementType = document.cNote.movementType.value;
                $.ajax({
                    url: '/throttle/getContractRoutesOrigin.do',
                    data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: $('#customerTypeId').val(), movementTypeId: movementType, customerId: $('#customerId').val()},
                    dataType: 'json',
                    success: function (data) {
                        $('#contractRouteOrigin1').empty();
                        $('#contractRouteOrigin1').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        $.each(data, function (i, data) {
                            $('#contractRouteOrigin1').append(
                                    $('<option ></option>').val(data.Id).html(data.Name)
                                    )
                        });
                    }
                });
            }
            //For Import Orders
            function getContractRoutesOrigin(val) {
                var movementType = document.cNote.movementType.value;
                $.ajax({
                    url: '/throttle/getContractRoutesOrigin.do',
                    data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: $('#customerTypeId').val(), movementTypeId: movementType, customerId: $('#customerId').val()},
                    dataType: 'json',
                    success: function (data) {
                        $('#contractRouteOrigin').empty();
                        $('#contractRouteOrigin').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        $.each(data, function (i, data) {
                            $('#contractRouteOrigin').append(
                                    $('<option ></option>').val(data.Id).html(data.Name)
                                    )
                        });
                    }
                });
            }

            function getContractRoutesOriginOtherICD() {
                var val = $("#contractId").val();
                var emptyContainerLocation = $("#emptyContainerLocation").val();
                if (emptyContainerLocation == 2) {
                    $.ajax({
                        url: '/throttle/getContractRoutesOriginOtherICD.do',
                        data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                        dataType: 'json',
                        success: function (data) {
                            $('#expcontractRouteOrigin').empty();
                            $('#expcontractRouteOrigin').append(
                                    $('<option></option>').val(0).html('-select-')
                                    )
                            $.each(data, function (i, data) {
                                $('#expcontractRouteOrigin').append(
                                        $('<option ></option>').val(data.Id).html(data.Name)
                                        )
                            });
                        }
                    });
                } else if (emptyContainerLocation == 1) {
                    getContractRoutesOriginExp(val);
                }

            }


            //For Export Orders

            function getContractRoutesOriginExp(val) {
                var movementType = document.cNote.movementType.value;
                $.ajax({
                    url: '/throttle/getContractRoutesOrigin.do',
                    data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: $('#customerTypeId').val(), movementTypeId: movementType, customerId: $('#customerId').val()},
                    dataType: 'json',
                    success: function (data) {
                        $('#expcontractRouteOrigin').empty();
                        $('#expcontractRouteOrigin').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        $.each(data, function (i, data) {
                            $('#expcontractRouteOrigin').append(
                                    $('<option ></option>').val(data.Id).html(data.Name)
                                    )
                        });
                    }
                });
            }
            // for repo

            function setContractOriginDestinationRepo() {
                deleteRowContainer();
                var movementTypeVal = document.cNote.movementType.value;
                if (document.cNote.contractRouteOrigin1.value == '0') {
                    $('#contractRouteDestination1').empty();
                    $('#contractRouteDestination1').append(
                            $('<option></option>').val(0).html('-select-')
                            )
                    setContractRouteDetails();
                } else {


                    $.ajax({
                        url: '/throttle/getContractRoutesOriginDestination.do',
                        data: {
                            movementType: movementTypeVal,
                            contractId: $('#contractId').val(),
                            customerId: $('#customerId').val(),
                            contractRouteOrigin: $('#contractRouteOrigin1').val(),
                            contarctRouteInteream: 0,
                            customerTypeId: $('#customerTypeId').val()},
                        dataType: 'json',
                        success: function (data) {
                        
                            $('#contractRouteDestination1').empty();
                            $('#contractRouteDestination1').append(
                                    $('<option></option>').val(0).html('-select-')
                                    )
                            $.each(data, function (i, data) {
                                $('#contractRouteDestination1').append(
                                        $('<option ></option>').val(data.Id).html(data.Name)
                                        )
                            });
                            setContractRouteDetails();
                        }
                    });
                }
            }


            // for Import
            function setContractOriginDestination() {
                deleteRowContainer();
                var movementTypeVal = document.cNote.movementType.value;
                if (document.cNote.contractRouteOrigin.value == '0') {
                    $('#destinationTemp').empty();
                    $('#destinationTemp').append(
                            $('<option></option>').val(0).html('-select-')
                            )
                    setContractRouteDetails();
                } else {

                    $.ajax({
                        url: '/throttle/getContractRoutesOriginDestination.do',
                        data: {movementType: movementTypeVal, contractId: $('#contractId').val(), contractRouteOrigin: $('#contractRouteOrigin').val(), contarctRouteInteream: $('#contractRouteDestination').val(), customerTypeId: $('#customerTypeId').val(), customerId: $('#customerId').val()},
                        dataType: 'json',
                        success: function (data) {
                            $('#destinationTemp').empty();
                            $('#destinationTemp').append(
                                    $('<option></option>').val(0).html('-select-')
                                    )
                            $.each(data, function (i, data) {
                                $('#destinationTemp').append(
                                        $('<option ></option>').val(data.Id).html(data.Name)
                                        )
                            });
                            setContractRouteDetails();
                        }
                    });
                }
            }
            // for Export
            function setContractOriginDestinationExp() {
                // alert("i m thr");
                deleteRowContainer();
                var movementTypeVal = document.cNote.movementType.value;
                var emptyContainerLocation = $("#emptyContainerLocation").val();
                if (emptyContainerLocation == 1) {
                    if (document.cNote.expcontractRouteOrigin.value == '0') {
                        $('#expdestinationTemp').empty();
                        $('#expdestinationTemp').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        setContractRouteDetails();
                    } else {
                        // alert(document.getElementById("expcontractRouteDestination").value);
                        $.ajax({
                            url: '/throttle/getContractRoutesOriginDestination.do',
                            data: {movementType: movementTypeVal, contractId: $('#contractId').val(), contractRouteOrigin: $('#expcontractRouteOrigin').val(), contarctRouteInteream: $('#expcontractRouteDestination').val(), customerTypeId: $('#customerTypeId').val(), customerId: $('#customerId').val()},
                            dataType: 'json',
                            success: function (data) {
                                $('#expdestinationTemp').empty();
                                $('#expdestinationTemp').append(
                                        $('<option></option>').val(0).html('-select-')
                                        )
                                $.each(data, function (i, data) {
                                    $('#expdestinationTemp').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                                setContractRouteDetailsExp();
                            }
                        });
                    }
                } else if (emptyContainerLocation == 2) {
                    if (document.cNote.expcontractRouteOrigin.value == '0') {
                        $('#expdestinationTemp').empty();
                        $('#expdestinationTemp').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        setContractRouteDetails();
                    } else {
                        // alert(document.getElementById("expcontractRouteDestination").value);
                        $.ajax({
                            url: '/throttle/getContractRoutesOriginDestinationOtherICD.do',
                            data: {contractId: $('#contractId').val(), contractRouteOrigin: $('#expcontractRouteOrigin').val(), contarctRouteInteream: $('#expcontractRouteDestination').val(), customerTypeId: $('#customerTypeId').val()},
                            dataType: 'json',
                            success: function (data) {
                                $('#expdestinationTemp').empty();
                                $('#expdestinationTemp').append(
                                        $('<option></option>').val(0).html('-select-')
                                        )
                                $.each(data, function (i, data) {
                                    $('#expdestinationTemp').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                                setContractRouteDetailsExpOtherICD();
                            }
                        });
                    }
                }

            }

            //get import Interim point
            function setContractOriginInterem() {
                deleteRowContainer();
                if (document.cNote.contractRouteOrigin.value == '0') {
                    $('#contractRouteDestination').empty();
                    $('#contractRouteDestination').append(
                            $('<option></option>').val(0).html('-select-')
                            )
                    setContractRouteDetails();
                } else {

                    $.ajax({
                        url: '/throttle/getContractRoutesOriginInteream.do',
                        data: {contractId: $('#contractId').val(), contractRouteOrigin: $('#contractRouteOrigin').val(), customerTypeId: $('#customerTypeId').val(), movementTypeId: $('#movementType').val(), customerId: $('#customerId').val()},
                        dataType: 'json',
                        success: function (data) {
                            $('#contractRouteDestination').empty();
                            $('#contractRouteDestination').append(
                                    $('<option></option>').val(0).html('-select-')
                                    )
                            $.each(data, function (i, data) {
                                $('#contractRouteDestination').append(
                                        $('<option ></option>').val(data.Id).html(data.Name)
                                        )
                            });
                            setContractRouteDetails();
                        }
                    });
                }
            }
            function setContractOriginInteremExp() {
//                alert("dadsadsa"+$('#expcontractRouteOrigin').val());
                deleteRowContainer();
                var emptyContainerLocation = $("#emptyContainerLocation").val();
                if (emptyContainerLocation == 1) {
                    if (document.cNote.expcontractRouteOrigin.value == '0') {
                        $('#expcontractRouteDestination').empty();
                        $('#expcontractRouteDestination').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        setContractRouteDetails();
                    } else {
                        $.ajax({
                            url: '/throttle/getContractRoutesOriginInteream.do',
                            data: {contractId: $('#contractId').val(), contractRouteOrigin: $('#expcontractRouteOrigin').val(), customerTypeId: $('#customerTypeId').val(), movementTypeId: $('#movementType').val(), customerId: $('#customerId').val()},
                            dataType: 'json',
                            success: function (data) {
                                $('#expcontractRouteDestination').empty();
                                $('#expcontractRouteDestination').append(
                                        $('<option></option>').val(0).html('-select-')
                                        )
                                $.each(data, function (i, data) {
                                    $('#expcontractRouteDestination').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                                setContractRouteDetails();
                            }
                        });
                    }
                } else if (emptyContainerLocation == 2) {
                    if (document.cNote.expcontractRouteOrigin.value == '0') {
                        $('#expcontractRouteDestination').empty();
                        $('#expcontractRouteDestination').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        setContractRouteDetails();
                    } else {
                        $.ajax({
                            url: '/throttle/getContractRoutesOriginIntereamOtherICD.do',
                            data: {contractId: $('#contractId').val(), contractRouteOrigin: $('#expcontractRouteOrigin').val(), customerTypeId: $('#customerTypeId').val()},
                            dataType: 'json',
                            success: function (data) {
                                $('#expcontractRouteDestination').empty();
                                $('#expcontractRouteDestination').append(
                                        $('<option></option>').val(0).html('-select-')
                                        )
                                $.each(data, function (i, data) {
                                    $('#expcontractRouteDestination').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                                setContractRouteDetails();
                            }
                        });
                    }
                }

            }

            var httpReq;
            var temp = "";
            function setDestination()
            {
                document.cNote.origin.value = document.cNote.originTemp.value;
                var billingTypeId = 0;
                if (document.getElementById("movementType").value == 14 || document.getElementById("movementType").value == 16) {
                    billingTypeId = 2;
                } else {
                    billingTypeId = document.getElementById('billingTypeId').value;
                }
                var url = "/throttle/getDestinationList.do?originId=" + document.cNote.originTemp.value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&contractId=" + document.getElementById('contractId').value;
                if (window.ActiveXObject)
                {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest)
                {
                    httpReq = new XMLHttpRequest();
                }
                httpReq.open("GET", url, true);
                httpReq.onreadystatechange = function () {
                    processAjax();
                };
                httpReq.send(null);
            }

            function processAjax()
            {
                if (httpReq.readyState == 4)
                {
                    if (httpReq.status == 200)
                    {
                        temp = httpReq.responseText.valueOf();
                        setDestinationOptions(temp, document.cNote.destinationTemp);
                    } else
                    {
                        alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                    }
                }
            }

            function setDestinationOptions(text, variab) {
                variab.options.length = 0;
                option0 = new Option("--select--", '0');
                variab.options[0] = option0;
                if (text != "") {
                    var splt = text.split('~');
                    var temp1;
                    var id;
                    var name;
                    for (var i = 0; i < splt.length; i++) {
                        temp1 = splt[i].split('#');
                        //                    alert(temp1[0]);
                        //                    alert(temp1[1]);
                        /*
                         if (document.getElementById('customerTypeId').value == 2) {
                         id = temp1[0];
                         name = temp1[1];
                         //setVehicleType();
                         } else if (document.getElementById('billingTypeId').value == 3 && document.getElementById('customerTypeId').value == 1) {
                         id = temp1[0] + "-" + temp1[1];
                         name = temp1[1];
                         //setVehicleType();
                         } else if (document.getElementById('billingTypeId').value != 3 && document.getElementById('customerTypeId').value == 1) {
                         id = temp1[0];
                         name = temp1[1];
                         //setVehicleType();
                         }
                         */
                        //  alert(name);
                        //  alert(id);
                        id = temp1[0];
                        name = temp1[1];
                        option1 = new Option(name, id)
                        variab.options[i + 1] = option1;
                    }
                    setRouteDetails();
                }
            }

            function setVehicleType() {
                //alert();
                // alert("hiii");
                var url = "/throttle/getContainerVehicleTypeList.do?containerId=" + document.getElementById('containerType').value;
                if (window.ActiveXObject) {
                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest) {
                    httpReq = new XMLHttpRequest();
                }
                httpReq.open("GET", url, true);
                httpReq.onreadystatechange = function () {
                    processVehicleTypeAjax();
                };
                httpReq.send(null);
            }
            function getContractVehicleType() {
                if (document.getElementById("movementType").value == 1 || document.getElementById("movementType").value == 31 || document.getElementById("movementType").value == 32 || document.getElementById("movementType").value == 2 || document.getElementById("movementType").value == 29 || document.getElementById("movementType").value == 30
                        || document.getElementById("movementType").value == 25 || document.getElementById("movementType").value == 26 || document.getElementById("movementType").value == 27 || document.getElementById("movementType").value == 28) {
                    var pointPlanDate2 = document.getElementById("pointPlanDate2").value;
                    if (pointPlanDate2 == '') {
                        if (document.getElementById("movementType").value == 1 || document.getElementById("movementType").value == 31 || document.getElementById("movementType").value == 32 || document.getElementById("movementType").value == 27 || document.getElementById("movementType").value == 25 || document.getElementById("movementType").value == 26) {
                            alert("Please select Loading date");
                            document.getElementById("pointPlanDate2").focus();
                        } else if (document.getElementById("movementType").value == 2 || document.getElementById("movementType").value == 29 || document.getElementById("movementType").value == 30 || document.getElementById("movementType").value == 28) {
                            alert("Please select Unloading date");
                            document.getElementById("pointPlanDate2").focus();
                        }
                    } else {
                        $("#routePlanAddRow").hide();
                        $("#freezeRoute").hide();
                        $("#resetRoute").hide();
                        $("#unFreezetRoute").show();
                    }
                } else {
                    $("#routePlanAddRow").hide();
                    $("#freezeRoute").hide();
                    $("#resetRoute").hide();
                    $("#unFreezetRoute").show();
                }
            }
            function getGenerateContainer(sno) {
                $("#generateContainer1").show();
                var vehTypeIdTemp = document.getElementById("vehTypeIdTemp" + sno).value;
                var containerType = document.getElementById("containerType" + sno).value;
                var vehTypeIdTempValue = $('#vehTypeIdTemp' + sno).find('option:selected').text();
                var containerTypeValue = $('#containerType' + sno).find('option:selected').text();
                var linerValue = $('#linerName' + sno).find('option:selected').text();
                var linerId = document.getElementById("linerName" + sno).value;
                var tonnage = document.getElementById("tonnage" + sno).value;
                var goodsDes = document.getElementById("goodsDes" + sno).value;
                var containerQty = parseInt(document.getElementById("containerQty" + sno).value);
                var containerQtyOld = parseInt(document.getElementById("containerQtyOld" + sno).value);
                var containerFreightChargesTemp = document.getElementById("containerFreightChargeSingle" + sno).value;
                var consolName = document.getElementById("consolName" + sno).value;
                var containerCategory = $('#containerCategory' + sno).find('option:selected').text();
                var categoryId = document.getElementById("containerCategory" + sno).value;
                //            alert("containerQtyOld ==" + containerQtyOld + "containerQty " + containerQty +" containerFreightChargesTemp = "+containerFreightChargesTemp);
                if (containerQtyOld == 0) {
                    //                alert("add New ContainerQty");
                    for (var b = 0; b < containerQty; b++) {
                        addRowContainer(vehTypeIdTemp, containerType, vehTypeIdTempValue, containerTypeValue, containerFreightChargesTemp, sno, linerValue, linerId, containerCategory, categoryId, consolName, tonnage, goodsDes);
                    }
                    document.getElementById("containerQtyOld" + sno).value = containerQty;
                } else if (containerQtyOld < containerQty) {
                    //                alert("edit Old ContainerQty");
                    var containerQty1 = containerQty - containerQtyOld;
                    for (var b = 0; b < containerQty1; b++) {
                        addRowContainer(vehTypeIdTemp, containerType, vehTypeIdTempValue, containerTypeValue, containerFreightChargesTemp, sno, linerValue, linerId, containerCategory, categoryId, consolName, tonnage, goodsDes);
                    }
                    document.getElementById("containerQtyOld" + sno).value = containerQty;
                } else {
                    alert("Please Delete the Row")
                }
            }
            var rowCounts = 1;
            var rowCounts1 = 1;

            var httpRequests;
            var httpReqs;
            var styls = "";

            var sno5 = 0;
            function addRowContainer(vehTypeIdTemp, containerType, vehTypeIdTempValue, containerTypeValue, containerFreightChargesTemp, sno, linerValue, linerId, containerCategory, categoryId, consolName, tonnage, goodsDes) {
                if (parseInt(rowCount1) % 2 == 0)
                {
                    styl = "form-control";
                } else {
                    styl = "form-control";
                }
                sno5++;
                var tab = document.getElementById("generateContainer1");
                //find current no of rows
                var rowCountNew = document.getElementById('generateContainer1').rows.length;
//                            alert("generateContainer1  " + rowCountNew);
//                rowCountNew--;
                var newrow = tab.insertRow(rowCountNew);
                //            newrow.id = ;
                var rowCount12 = rowCountNew;

                // Positions
                var lenValue = document.getElementById('generateContainer1').rows.length;
//                alert("lenValue="+lenValue);
//                alert("sno5="+sno5);

                cell = newrow.insertCell(0);
                var cell0 = "<td   align='left' ><input type='hidden' name='containerTypeValue' id='containerTypeValue' value=" + containerType + " /><lable id='lableVehicleId' style='padding-left: 10px;'>" + vehTypeIdTempValue + "</lable><input type='hidden' name='vehTypeIdValue' id='vehTypeIdValue" + sno5 + "' value=" + vehTypeIdTemp + " /> <input type='hidden' name='consignmentContainerId' id='consignmentContainerId" + sno5 + "' value='' /><input type='hidden' name='containerFreightCharges' id='containerFreightCharges'  value='" + containerFreightChargesTemp + "' class='textbox' readOnly ><td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(1);
                var cell0 = "<td align='left'  ><lable  id='lablecontainerType' style='padding-left: 0px;' >" + containerTypeValue + "</lable>&emsp;&nbsp;&emsp;<td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                // TyreIds
                var cell = newrow.insertCell(2);
                var cell0 = "<td ><input type='text' name='containerName' id='containerName" + lenValue + "' value='' maxlength='11' onChange='validateContainerNo(" + lenValue + ");' onKeyPress='return onKeyPressBlockCharacters1(" + lenValue + ",event);'/><td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                var cell = newrow.insertCell(3);
                var cell0 = "<td align='left' ><lable style='padding-right: 10px;' id='lableContainerLinerName' >" + linerValue + "</lable><input type='hidden' name='containerLinerName' id='containerLinerName" + sno5 + "' value=" + linerId + " /> <td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                var cell = newrow.insertCell(4);
                var cell0 = "<td align='left' ><textarea  name='goodsDesc' id='goodsDesc" + lenValue + "' value=''  style='width:140px;height:30px'; />" + goodsDes + "</textarea><input type='hidden' name='containerCategory' id='containerCategory" + sno5 + "' value=" + categoryId + " /> <input type='hidden' name='consolName' id='consolName" + sno5 + "' value=" + consolName + " /><td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

//                var cell = newrow.insertCell(5);
//                var cell0 = "<td  align='center' ><input type='text' name='containerPkgs' id='containerPkgs" + lenValue + "' value='0' style='width:110px'; /><td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;

                var cell = newrow.insertCell(5);
                var cell0 = "<td  align='left' ><input type='hidden' name='containerVolume' id='containerVolume" + lenValue + "' value='0' style='width:110px'; /><input type='hidden' name='containerPkgs' id='containerPkgs" + lenValue + "' value='0' style='width:110px'; /><input type='text' name='containerWeight' readonly id='containerWeight" + lenValue + "' value='" + tonnage + "' style='width:110px'; /><td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

//                var cell = newrow.insertCell(7);
//                var cell0 = "<td  align='center'  ><input type='text' name='containerVolume' id='containerVolume" + lenValue + "' value='0' style='width:110px'; /><td>";
//                cell.setAttribute("className", styl);
//                cell.innerHTML = cell0;
//                
                if (document.getElementById("movementType").value == 1 || document.getElementById("movementType").value == 31 || document.getElementById("movementType").value == 32
                        || document.getElementById("movementType").value == 25 || document.getElementById("movementType").value == 26
                        || document.getElementById("movementType").value == 27) {
                    document.getElementById("containerPkgs1").readOnly = true;
                    document.getElementById("containerVolume1").readOnly = true;
                }

                var cell = newrow.insertCell(6);
                var cell0 = "<td  align='left'  ><input type='text' name='custRefNo' id='custRefNo" + lenValue + "' value='' style='width:110px'; /><td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

            }

            function deleteContainerRow(sno) {
                // alert("sno"+sno);
                var containerQtyLength = "";
                var vehTypeIdValue = document.getElementById("vehTypeIdValue" + sno).value;
                var taskArray = new Array();
                //                if (document.getElementById("consignmentContainerId" + sno).value != "") {
                //                    alert("1111");
                //                    $.ajax({
                //                        url: '/throttle/deleteconsignmentContainer.do',
                //                        data: {
                //                            contractId: $('#contractId').val(),
                //                            consignmentContainerId: document.getElementById("consignmentContainerId" + sno).value
                //                        },
                //                        dataType: 'json',
                //                        success: function (data) {
                //                            document.getElementById("generateContainer1").deleteRow(sno);
                //                        }
                //                    });
                //                } else {
                //                }
                //alert("vehTypeIdValue"+vehTypeIdValue);
                document.getElementById("generateContainer1").deleteRow(sno);
                sno5--;
                $(document).ready(function () {
                    sno = sno - 1;
                    $("input[name=vehTypeIdValue]").each(function () {
                        if ($(this).val() == vehTypeIdValue) {
                            taskArray.push($(this).val());
                        }
                    });
                });
                containerQtyLength = document.getElementById('addContainer').rows.length;
                containerQtyLength = containerQtyLength - 1;
                for (var i = 1; i < containerQtyLength; i++) {
                    if (vehTypeIdValue == document.getElementById("vehTypeIdTemp" + i).value) {
                        document.getElementById("containerQty" + i).value = taskArray.length - 1;
                        document.getElementById("containerQtyOld" + i).value = taskArray.length - 1;
                        getContainerFreightRate(i);
                    }
                }
            }
            function onKeyPressBlockCharacters1(sno3, e)
            {
                var fieldLength = document.getElementById('containerName' + sno3).value.length;
                if (fieldLength <= 3) {
                    var key = window.event ? e.keyCode : e.which;
                    var keychar = String.fromCharCode(key);
                    reg = /\d/;
                    return !reg.test(keychar);
                } else if (fieldLength <= 10) {
                    var key = window.event ? e.keyCode : e.which;
                    var keychar = String.fromCharCode(key);
                    reg = /[a-zA-Z]+$/;
                    return !reg.test(keychar);
                }
            }
            function validateContainerNo(sno) {
                var containerNos = document.cNote.containerName;
                for (var i = 1; i <= containerNos.length; i++) {
                    for (var j = i + 1; j < containerNos.length + 1; j++) {
                        if (document.getElementById("containerName" + i).value == document.getElementById("containerName" + j).value && document.getElementById("containerName" + i).value && document.getElementById("containerName" + j).value) {
                            alert("container No already exist..");
                            document.getElementById("containerName" + sno).value = "";
                            document.getElementById("containerName" + sno).focus();
                        }
                    }
                }
            }




            function deleteRowContainer() {

                var rowCountaddContainer = document.getElementById('addContainer').rows.length;
                //            containerRowId
                var rowCountNew = document.getElementById('generateContainer1').rows.length;
                for (var j = 0; j < rowCountaddContainer; j++) {
                    if (j > 0 && ((j + 1) < rowCountaddContainer)) {
                        document.getElementById("containerQtyOld" + j).value = "0";
                        $("#approvalStatusSpan" + j).text('');
                    }
                }
                var n = 0;
                for (var i = rowCountNew; i <= rowCountNew; i--) {
                    n = i - 1;
                    if (i == 0) {
                        var vehTypeIdTemp = document.getElementsByName('vehTypeIdTemp');
                        for (var k = 0; k < vehTypeIdTemp.length; k++) {
                            document.getElementsByName("vehTypeIdTemp")[k].value = 0;
                            document.getElementsByName("containerType")[k].value = 0;
                            document.getElementsByName("containerQty")[k].value = "";
                            document.getElementsByName("tonnage")[k].value = "";
                            document.getElementsByName("containerFreightChargeSingle")[k].value = 0;
                            document.getElementsByName("rate")[k].value = "";
                        }
                        document.getElementById("totFreightAmount").value = "";
                        break;
                    } else {
                        document.getElementById("generateContainer1").deleteRow(n);
                    }
                }

            }


            function unFreezetRouteFn() {
                $("#unFreezetRoute").hide();
                $("#routePlanAddRow").show();
                $("#freezeRoute").show();
                $("#resetRoute").show();
                var contractType = document.cNote.billingTypeId.value;
                //alert(contractType);
                if (contractType == '1' || contractType == '2') {
                    $('#vehTypeIdContractTemp').empty();
                    $('#vehTypeIdContractTemp').append(
                            $('<option></option>').val(0).html('-select-')
                            )
                }
            }
            function processVehicleTypeAjax() {
                if (httpReq.readyState == 4) {
                    if (httpReq.status == 200) {
                        temp = httpReq.responseText.valueOf();
                        alert(temp);
                        $('#vehTypeIdContractTemp').append(
                                $('<option style="width:150px"></option>').val(0).html('--Select--')
                                )
                        setVehicleTypeOptions(temp, document.cNote.vehTypeIdContractTemp);
                    } else {
                        alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                    }
                }
            }

            function setVehicleTypeOptions(text, variab) {
                variab.options.length = 0;
                option0 = new Option("--select--", '0');
                variab.options[0] = option0;
                if (text != "") {

                    var splt = text.split('~');
                    var temp1;
                    variab.options[0] = option0;
                    for (var i = 0; i < splt.length; i++) {
                        temp1 = splt[i].split('#');
                        //alert(temp1[1]+"-"+temp1[0]);
                        option0 = new Option(temp1[1], temp1[0])
                        variab.options[i + 1] = option0;
                    }
                }
            }


            function multiPickupShow() {
                var billingTypeId = document.getElementById('billingTypeId').value;
                if (document.getElementById('multiPickup').checked == true) {
                    //                document.getElementById('multiPickupCharge').readOnly = false;
                    document.getElementById('multiPickup').value = "Y";
                    //alert(document.getElementById('multiPickup').value);
                    //                if (billingTypeId == "3") {
                    //                    $("#routeDetail").show();
                    //                    $("#showRouteCourse").show();
                    //                }
                } else if (document.getElementById('multiPickup').checked == false) {
                    //                document.getElementById('multiPickupCharge').readOnly = true;
                    document.getElementById('multiPickup').value = "N";
                    //alert(document.getElementById('multiPickup').value);
                    //                if (billingTypeId == "3") {
                    //                    $("#routeDetail").hide();
                    //                    $("#showRouteCourse").hide();
                    //                }
                }
            }
            function multiDeliveryShow() {
                var billingTypeId = document.getElementById('billingTypeId').value;
                if (document.getElementById('multiDelivery').checked == true) {
                    document.getElementById('multiDeliveryCharge').readOnly = false;
                    document.getElementById('multiDelivery').value = "Y";
                    //                if (billingTypeId == "3") {
                    //                    $("#routeDetail").show();
                    //                    $("#showRouteCourse").show();
                    //                }
                } else if (document.getElementById('multiDelivery').checked == false) {
                    document.getElementById('multiDeliveryCharge').readOnly = true;
                    document.getElementById('multiDelivery').value = "N";
                    //                if (billingTypeId == "3") {
                    //                    $("#routeDetail").hide();
                    //                    $("#showRouteCourse").hide();
                    //                }
                }
            }

            function calcTotalPacks(val) {
                var totVal = 0;
                var packagesNos = document.getElementsByName('packagesNos');
                for (var i = 0; i < packagesNos.length; i++) {
                    if (packagesNos[i].value != '') {
                        totVal += parseInt(packagesNos[i].value);
                    }
                }
                $('#totalPackages').text(totVal);
                $('#totalPackage').val(totVal);
            }

            function calcTotalVolumes(val) {
                var totVal = 0;
                var totVol = 0;
                var volumes = document.getElementsByName('productVolume');
                for (var i = 0; i < volumes.length; i++) {
                    if (volumes[i].value != '') {
                        totVol += parseInt(volumes[i].value);
                    }
                }
                $('#totalvolume').text(totVol);
                $('#totalVolumes').val(totVol);
            }
            function calcTotalWeights(val) {
                var totVal = 0;
                var totVol = 0;
                var reeferRequired = document.getElementById('reeferRequired').value;
                var weights = document.getElementsByName('weights');
                var volumes = document.getElementsByName('productVolume');
                var billingTypeId = document.getElementById('billingTypeId').value;
                var rateWithReefer = document.getElementById('rateWithReefer').value;
                var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
                for (var i = 0; i < weights.length; i++) {
                    if (weights[i].value != '') {
                        totVal += parseInt(weights[i].value);
                    }
                }

                $('#totalWeight').text(totVal);
                $('#totalWeightage').val(totVal);
                var freigthTotal = 0;
                if (billingTypeId == 2 && reeferRequired == 'Yes') {
                    freigthTotal = parseInt(rateWithReefer) * parseInt(totVal);
                    $('#freightAmount').text(freigthTotal)
                    $('#totalCharges').val(freigthTotal)
                } else if (billingTypeId == 2 && reeferRequired == 'No') {
                    freigthTotal = parseInt(rateWithoutReefer) * parseInt(totVal);
                    $('#freightAmount').text(freigthTotal)
                    $('#totalCharges').val(freigthTotal)
                }
            }
            function resetRouteInfo() {
                var customerType = document.cNote.customerTypeId.value;
                var billingType = document.cNote.billingTypeId.value;
                if (customerType == '1' && (billingType == '1' || billingType == '2')) {
                    setContractRouteDetails();
                } else {
                    setRouteDetails();
                }
            }
            function validateTransitTime(val) {

                var transitHours = document.getElementById("pointTransitHrs" + val).value;
                if (transitHours == '') {
                    var pointIdValue = document.getElementById("pointId" + val).value;
                    var pointOrderValue = document.getElementById("pointOrder" + val).value;
                    var pointOrder = document.getElementsByName("pointOrder");
                    var pointNames = document.getElementsByName("pointName");
                    var pointIdPrev = 0;
                    var pointIdNext = 0;
                    var prevPointOrderVal = parseInt(pointOrderValue) - 1;
                    var nextPointOrderVal = parseInt(pointOrderValue) + 1;
                    for (var m = 1; m <= pointOrder.length; m++) {
                        if (document.getElementById("pointOrder" + m).value == prevPointOrderVal) {
                            pointIdPrev = document.getElementById("pointId" + m).value;
                        }
                        if (document.getElementById("pointOrder" + m).value == nextPointOrderVal) {
                            pointIdNext = document.getElementById("pointId" + m).value;
                        }
                    }
                    //fetch transit hours via ajax for prepointid and current point id and set transit hours value to hidden field

                }

            }

            function validateTripSchedule() {

                //alert(document.getElementById("pointPlanDate1").value);
                document.cNote.vehicleRequiredDate.value = document.getElementById("pointPlanDate1").value;
                document.cNote.vehicleRequiredHour.value = document.getElementById("pointPlanHour1").value;
                document.cNote.vehicleRequiredMinute.value = document.getElementById("pointPlanMinute1").value;
                var scheduleDate = document.cNote.vehicleRequiredDate.value;
                var scheduleHour = document.cNote.vehicleRequiredHour.value;
                var scheduleMinute = document.cNote.vehicleRequiredMinute.value;
                var temp = scheduleDate.split("-");
                var tripScheduleTime = convertStringToDate(temp[2], temp[1], temp[0], scheduleHour, scheduleMinute, '00');
                var time1 = new Date();
                var time1ms = time1.getTime(time1); //i get the time in ms
                var time2 = tripScheduleTime;
                var time2ms = time2.getTime(time2);
                var difference = time2ms - time1ms;
                var hours = Math.floor(difference / 36e5);
                //alert(hours);
                if (hours < 0) {
                    alert('vehicle required time is in the past. please check ');
                    //alert('Orders cannot be accepted as the cut off time has elapsed.');
                    return true;
                } else {
                    return true;
                }

            }
            function submitPage(value) {
		 	
//                if (isEmpty(document.cNote.billingParty.value)) {
//                    alert('please enter consignorName');
//                    document.cNote.billingParty.focus();
//                    return;
//                }
//                if (isEmpty(document.cNote.consigneeNameTemp.value)) {
//                    alert('please enter consigneeName');
//                    document.cNote.consigneeNameTemp.focus();
//                    return;
//                }
                if (isEmpty(document.cNote.customerId.value)) {
                    alert('please enter customer name');
                    document.cNote.customerId.focus();
                    return;
                }
                if (document.cNote.movementType.value == '0') {
                    alert('please choose the movement type');
                    document.cNote.movementType.focus();
                    return;
                }
//                if (document.cNote.movementType.value == '2' || document.cNote.movementType.value == '5') {
//                    if (document.cNote.vehicleInstruction.value == '') {
//                        alert('please enter the goods description');
//                        document.cNote.vehicleInstruction.focus();
//                        return;
//                    }
//                }

//                if (document.cNote.contractId.value == '0') {
//                    alert('please choose the route');
//                    document.cNote.contractId.focus();
//                    return;
//                }
                var conatinerNames = document.getElementsByName("containerName");
                var goodsDesc = document.getElementsByName("goodsDesc");
                var custRefNo = document.getElementsByName("custRefNo");
                for (var i = 0; i < conatinerNames.length; i++) {
                    var fieldLength = conatinerNames[i].value.length;
                    var goodsDescLength = goodsDesc[i].value.length;
                    var custRefNoLength = custRefNo[i].value.length;
                    //  alert(fieldLength);
                    if (fieldLength > 0 && fieldLength != 11) {
                        alert('container no length should be 11');
                        conatinerNames[i].focus();
                        return;
                    } else if (custRefNoLength == 0) {
                        alert('Please enter the customer reference no');
                        custRefNo[i].focus();
                        return;
                    }/* else if(fieldLength == 0 && document.getElementById("survayCon").value == 2 ){
                     alert('Please enter the container no');
                     conatinerNames[i].focus();
                     return;
                     } */ else if (goodsDescLength == 0) {
                        alert('Please enter the Goods Desc');
                        goodsDesc[i].focus();
                        return;
                    }
                }

                if (document.cNote.movementType.value == '3') {
                    if (document.cNote.contractRouteOrigin1.value == '0') {
                        alert('please select the origin');
                        document.cNote.contractRouteOrigin1.focus();
                        return;
                    }
                    if (document.cNote.contractRouteDestination1.value == '0') {
                        alert('please select the destinatiom');
                        document.cNote.contractRouteDestination1.focus();
                        return;
                    }

                } else if (document.cNote.movementType.value != '8' && (document.cNote.movementType.value == '1' || document.cNote.movementType.value == '4' || document.cNote.movementType.value == '6')) {
                    if (document.cNote.expcontractRouteOrigin.value == '0') {
                        alert('please select the origin');
                        document.cNote.expcontractRouteOrigin.focus();
                        return;
                    }
                    if (document.cNote.expcontractRouteDestination.value == '0') {
                        alert('please select the loading point');
                        document.cNote.expcontractRouteDestination.focus();
                        return;
                    }
                    if (document.cNote.expdestinationTemp.value == '0') {
                        alert('please select the destinatiom');
                        document.cNote.expdestinationTemp.focus();
                        return;
                    }
//                    alert("val:::"+document.cNote.clearanceType.value);
                    if (document.cNote.clearanceType.value == '0') {
                        alert('please select the clearance type');
                        document.cNote.clearanceType.focus();
                        return;
                    }
                } else if (document.cNote.movementType.value == 10) {
//                    alert("test");
                    if (document.cNote.vesselCutoffTime.value == '' || document.cNote.vesselCutoffTime.value == '0') {
                        alert('please enter the VesselCutoffTime');
                        document.cNote.vesselCutoffTime.focus();
                        return false;
                    }

                } else if (document.cNote.movementType.value == 18 || document.cNote.movementType.value == 21 || document.cNote.movementType.value == 33 || document.cNote.movementType.value == 34 || document.cNote.movementType.value == 35 || document.cNote.movementType.value == 36) {
                    if (document.cNote.emptyContDropTime1.value == '' || document.cNote.emptyContDropTime1.value == '0') {
                        alert('please enter the emptyContDropTime');
                        document.cNote.emptyContDropTime1.focus();
                        return false;
                    }
                }

                var containerType = document.getElementsByName("containerType");
                var vehicletype = document.getElementsByName("containerType");
                var containerQty = document.getElementsByName("containerQty");
                var check = 0;
                var message = "";
                for (var i = 0; i < vehicletype.length; i++) {
                    if (containerType[i] == '') {
                        check = check + 1;
                        message = message + "conatiner type is empty ";
                        document.cNote.containerType[i].focus();
                        return;
                    }
                    if (vehicletype[i] == '') {
                        check = check + 1;
                        message = message + "vehicle type is empty ";
                        document.cNote.vehicletype[i].focus();
                        return;
                    }
                    if (containerQty[i] == '') {
                        check = check + 1;
                        message = message + "conatiner quantity is empty ";
                        document.cNote.containerQty[i].focus();
                        return;
                    }
                }
                var movementType = document.getElementById("movementType").value;
                if (movementType != 12 && movementType != 13 && movementType != 14 && movementType != 15 && movementType != 16) {
                    if (document.cNote.freightAcceptedStatus.checked) {
                        var billingTypeId = $("#billingTypeId").val();
                        if (billingTypeId == 4) {
                            $("#orderButton").show();
                        } else {
                            if (parseFloat(document.cNote.totFreightAmount.value) >= 0) {
                                $("#orderButton").show();
                            } else {

                                document.cNote.freightAcceptedStatus.checked = false;
                                alert("Please check the customer contract  freight rate ");
                            }
                        }

                    } else {
                        alert("select freight agree for order creation");
                        return;
                    }
                } else {
                    document.cNote.totFreightAmount.value = 0;
                }
                if (document.getElementById("movementType").value == 1 || document.getElementById("movementType").value == 31 || document.getElementById("movementType").value == 32 || document.getElementById("movementType").value == 2 || document.getElementById("movementType").value == 29 || document.getElementById("movementType").value == 30 || document.getElementById("movementType").value == 25 || document.getElementById("movementType").value == 26) {
                    var pointPlanDate2 = document.getElementById("pointPlanDate2").value;
                    if (pointPlanDate2 == '') {
                        if (document.getElementById("movementType").value == 1 || document.getElementById("movementType").value == 31 || document.getElementById("movementType").value == 32 || document.getElementById("movementType").value == 25 || document.getElementById("movementType").value == 26) {
                            alert("Please select Loading date");
                            document.getElementById("pointPlanDate2").focus();
                            return;
                        } else if (document.getElementById("movementType").value == 2 || document.getElementById("movementType").value == 29 || document.getElementById("movementType").value == 30) {
                            alert("Please select Unloading date");
                            document.getElementById("pointPlanDate2").focus();
                            return;
                        }
                    }
                }
                var totFreightAmount = document.cNote.totFreightAmount.value;
                if ((totFreightAmount == '' || parseFloat(totFreightAmount) == null)) {
                    alert('Revenue cannot be estimated, please check the customer contract and proceed');
                    return;
                } else {
                    var origin = "";
                    var dest = "";
                    var drop = "";
                    var route = "";
                    var movementType = document.getElementById("movementType").value;
                    if (movementType == "1" || movementType == "31" || movementType == "32" || movementType == "20" || movementType == "25" || movementType == "26") {
                        if (movementType == "1" || movementType == "25" || movementType == "26") {
                            movementType = "Export"
                        } else if (movementType == "31" || movementType == "32") {
                            movementType = "Coastal Outbound"
                        } else if (movementType == "20") {
                            movementType = "Ex-Bonding"
                        }
                        origin = document.getElementById("expcontractRouteOrigin").options[document.getElementById("expcontractRouteOrigin").selectedIndex ].text;
                        dest = document.getElementById("expcontractRouteDestination").options[document.getElementById("expcontractRouteDestination").selectedIndex ].text;
                        drop = document.getElementById("expdestinationTemp").options[document.getElementById("expdestinationTemp").selectedIndex ].text;
                        route = origin + "-" + dest + "-" + drop;
                    } else if (movementType == "2" || movementType == "28" || movementType == "29" || movementType == "30") {
                        if (movementType == '29' || movementType == '30') {
                            movementType = "Coastal Inbound (Import)"
                        } else {
                            movementType = "Import"
                        }
                        origin = document.getElementById("contractRouteOrigin").options[document.getElementById("contractRouteOrigin").selectedIndex ].text;
                        dest = document.getElementById("contractRouteDestination").options[document.getElementById("contractRouteDestination").selectedIndex ].text;
                        drop = document.getElementById("destinationTemp").options[document.getElementById("destinationTemp").selectedIndex ].text;
                        route = origin + "-" + dest + "-" + drop;
                    } else if (movementType == '3') {
                        movementType = 'Repo'
                        origin = document.getElementById("contractRouteOrigin1").options[document.getElementById("contractRouteOrigin1").selectedIndex ].text;
                        dest = document.getElementById("contractRouteDestination1").options[document.getElementById("contractRouteDestination1").selectedIndex ].text;
                        route = origin + "-" + dest;

                    } else if (movementType == '4') {
                        movementType = 'Export DSO'
                        origin = document.getElementById("expcontractRouteOrigin").options[document.getElementById("expcontractRouteOrigin").selectedIndex ].text;
                        dest = document.getElementById("expcontractRouteDestination").options[document.getElementById("expcontractRouteDestination").selectedIndex ].text;
                        drop = document.getElementById("expdestinationTemp").options[document.getElementById("expdestinationTemp").selectedIndex ].text;
                        route = origin + "-" + dest + "-" + drop;
                    } else if (movementType == '5') {
                        movementType = 'Import DSO'
                        origin = document.getElementById("contractRouteOrigin").options[document.getElementById("contractRouteOrigin").selectedIndex ].text;
                        dest = document.getElementById("contractRouteDestination").options[document.getElementById("contractRouteDestination").selectedIndex ].text;
                        drop = document.getElementById("destinationTemp").options[document.getElementById("destinationTemp").selectedIndex ].text;
                        route = origin + "-" + dest + "-" + drop;

                    } else if (movementType == '6') {
                        movementType = 'Back To Town'
                        origin = document.getElementById("expcontractRouteOrigin").options[document.getElementById("expcontractRouteOrigin").selectedIndex ].text;
                        dest = document.getElementById("expcontractRouteDestination").options[document.getElementById("expcontractRouteDestination").selectedIndex ].text;
                        drop = document.getElementById("expdestinationTemp").options[document.getElementById("expdestinationTemp").selectedIndex ].text;
                        route = origin + "-" + dest + "-" + drop;
                    } else if (movementType == '9') {
                        movementType = 'Export-EmptyPickUp'
                        origin = document.getElementById("emptyExportOrigin").options[document.getElementById("emptyExportOrigin").selectedIndex ].text;
                        dest = document.getElementById("emptyExportDestination").options[document.getElementById("emptyExportDestination").selectedIndex ].text;
                        drop = document.getElementById("emptyDestinationTemp").options[document.getElementById("emptyDestinationTemp").selectedIndex ].text;
                        route = origin + "-" + dest + "-" + drop;
                    } else if (movementType == '10') {
                        movementType = 'Export-PortDrop'
                        origin = document.getElementById("emptyExportOrigin").options[document.getElementById("emptyExportOrigin").selectedIndex ].text;
                        dest = document.getElementById("emptyExportDestination").options[document.getElementById("emptyExportDestination").selectedIndex ].text;
                        drop = document.getElementById("emptyDestinationTemp").options[document.getElementById("emptyDestinationTemp").selectedIndex ].text;
                        route = origin + "-" + dest + "-" + drop;
                    } else if (movementType == '8') {
                        movementType = 'Export(Distance based)'
                        origin = document.getElementById("emptyExportOrigin").options[document.getElementById("emptyExportOrigin").selectedIndex ].text;
                        dest = document.getElementById("emptyExportDestination").options[document.getElementById("emptyExportDestination").selectedIndex ].text;
                        drop = document.getElementsByName("pointName").value;
                        route = origin + "-" + dest + "-" + drop;
                    } else if (movementType == '12' || movementType == '13' || movementType == '14' || movementType == '15' || movementType == '16' || movementType == '17' || movementType == '21' || movementType == '23' || movementType == '19' || movementType == '24' || movementType == '27' || movementType == '28' || movementType == '33' || movementType == '34' || movementType == '35' || movementType == '36') {
                        if (movementType == '12') {
                            movementType = 'Moffusil-Export-FCL'
                        } else if (movementType == '13') {
                            movementType = 'Moffusil-Import-FCL'
                        } else if (movementType == '14') {
                            movementType = 'Moffusil-Import-LCL'
                        } else if (movementType == '15') {
                            movementType = 'Moffusil-ICD'
                        } else if (movementType == '16') {
                            movementType = 'Moffusil-Export-LCL'
                        } else if (movementType == '17') {
                            movementType = 'Vessel Empty'
                        } else if (movementType == '18') {
                            movementType = 'Plot to Plot'
                        } else if (movementType == '19') {
                            movementType = 'Local PNR'
                        } else if (movementType == '23') {
                            movementType = 'PNR-MOFF'
                        } else if (movementType == '24') {
                            movementType = 'Offload - Mov'
                        } else if (movementType == '21') {
                            movementType = 'De-stuff empty'
                        } else if (movementType == '27') {
                            movementType = 'Local-Exp'
                        } else if (movementType == '28') {
                            movementType = 'Local-Imp'
                        } else if (movementType == '33' || movementType == '34') {
                            movementType = 'Repo'
                        } else if (movementType == '35' || movementType == '36') {
                            movementType = 'Lease Box'
                        } else {
                            movementType = ''
                        }
                        origin = document.getElementById("contractRouteOrigin1").options[document.getElementById("contractRouteOrigin1").selectedIndex ].text;
                        dest = document.getElementById("contractRouteDestination1").options[document.getElementById("contractRouteDestination1").selectedIndex ].text;
                        drop = document.getElementsByName("pointName").value;
                        route = origin + "-" + dest;
//                        route = origin + "-" + dest + "-" + drop;
                    }




                    var consignor = document.getElementById("custName").value;
                    var consignee = document.getElementById("consigneeNameTemp").value;
                    var billingParty = document.getElementById("customerName").value;

                    var confirms = confirm('movement type=' + movementType + '\n'
//                            + "Consignor=" + consignor + "\n"
//                            + "Consignee=" + consignee + "\n"
                            + "BillingParty=" + billingParty + "\n"
                            + "Route=" + route + "\n\n"
                            + "Do You want to Confirm Cnote Creation?");

                    if (confirms == true) {
                        $("#createOrder").hide();
                        //                    alert("You Pressed Yes")
                        document.getElementById("spinner").style.display = "block";
                        document.getElementById("loader").style.display = "block";
                        document.getElementById('loader').className += 'ui-loader-background';

                        document.cNote.action = "/throttle/saveConsignmentNote.do";
                        document.cNote.submit();
                    } else {
                        $("#createOrder").show();
                        //                 alert("You Pressed No")
                    }
                }

            }

            function setProductCategoryValues() {
                var temp = document.cNote.productCategoryIdTemp.value;
                //alert(temp);
                if (temp != 0) {
                    var tempSplit = temp.split('~');
                    document.getElementById("temperatureInfo").innerHTML = 'Reefer Temp (deg Celcius): Min ' + tempSplit[1] + ' Max ' + tempSplit[2];
                    document.getElementById("temperatureInfo").innerHTML = '';
                    document.cNote.productCategoryId.value = tempSplit[0];
                    var reeferRequired = tempSplit[3];
                    if (reeferRequired == 'Y') {
                        reeferRequired = 'Yes';
                    } else {
                        reeferRequired = 'No';
                    }
                    document.cNote.reeferRequired.value = reeferRequired;
                } else {
                    document.getElementById("temperatureInfo").innerHTML = '';
                    document.cNote.productCategoryId.value = 0;
                    document.cNote.reeferRequired.value = '';
                }
            }
            function checkFreightAcceptedStatus() {
                //alert("am here.."+document.cNote.freightAcceptedStatus.checked);
                if (document.cNote.freightAcceptedStatus.checked) {
                    //alert("am here..");
                    var billingTypeId = $("#billingTypeId").val();
                    if (billingTypeId == 4) {
                        $("#orderButton").show();
                    } else {
                        if (parseFloat(document.cNote.totFreightAmount.value) >= 0) {
                            $("#orderButton").show();
                        } else {
                            document.cNote.freightAcceptedStatus.checked = false;
                            alert("Invalid freight value");
                        }

                    }
                } else {
                    $("#orderButton").hide();
                }
            }
        </script>

        <script type="text/javascript">


            function showOrderType() {
                document.cNote.destinationTemp.value = '0';
                setRouteDetails();
            }

            function showMovTab() {
                $('#addModesDiv').hide();
                $('#consName').hide();
                $('#vehType').show();
                $('#productDetailsDiv').hide();
                deleteRowContainer();
                if (document.getElementById("movementType").value == 1 || document.getElementById("movementType").value == 27 || document.getElementById("movementType").value == 4 || document.getElementById("movementType").value == 6 || document.getElementById("movementType").value == 20
                        || document.getElementById("movementType").value == 25 || document.getElementById("movementType").value == 26
                        || document.getElementById("movementType").value == 31 || document.getElementById("movementType").value == 32
                        ) {
                    $('#ImportRouteDiv').hide();
                    $('#DistanceRouteDiv').hide();
                    $('#ExportRouteDiv').show();
                    $('#contractRouteDiv1').hide();
                    $('#EmptyExportRouteDiv').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').show();
                    $('#billOfEntry').val('');
                    $('#billOfEntryDate').val('');
                    $('#boE').hide();
                    $('#impNameLable').hide();
                    $('#impNameField').hide();
                    $('#mptyValidityLable').hide();
                    $('#emptyContDropTime').hide();
                    $('#boEdate').hide();
                    $("tr.extraRepoCust").hide();
                    $('#mainCust').show();
                    $('#repoCust').hide();
                    $('#addVehTypeDiv').show();
                    $('#addContainersDiv').show();
//                                $('#tdFreightAmountLabel').show();
//                                $('#tdFreightAmount').show();
                    //$('#tdFreightAmountCheckBox').show();
                    $('#emptyContDropTimes').hide();
                    $('#emptyContDropField').hide();

                } else if (document.getElementById("movementType").value == 2 || document.getElementById("movementType").value == 29 || document.getElementById("movementType").value == 30 || document.getElementById("movementType").value == 28 || document.getElementById("movementType").value == 5) {
                    $('#ImportRouteDiv').show();
                    $('#DistanceRouteDiv').hide();
                    $('#EmptyExportRouteDiv').hide();
                    $('#ExportRouteDiv').hide();
                    $('#contractRouteDiv1').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').hide();
                    $('#shipingLineNo').val('');
                    $('#boE').show();
                    $('#impNameLable').hide();
                    $('#impNameField').hide();
                    $('#mptyValidityLable').hide();
                    $('#emptyContDropTime').hide();
                    $('#boEdate').hide();
                    $("tr.extraRepoCust").hide();
                    // $('#repoCustomer').hide();
                    // $('#mainCustomer').show();
                    $('#mainCust').show();
                    $('#repoCust').hide();
                    $('#addVehTypeDiv').show();
                    $('#addContainersDiv').show();
//                                $('#tdFreightAmountLabel').show();
//                                $('#tdFreightAmount').show();
                    //$('#tdFreightAmountCheckBox').show();
                    $('#emptyContDropTimes').hide();
                    $('#emptyContDropField').hide();

                } else if (document.getElementById("movementType").value == 9 || document.getElementById("movementType").value == 10 ||
                        document.getElementById("movementType").value == 11) {
//                            } else if (document.getElementById("movementType").value == 9 || document.getElementById("movementType").value == 10) {
                    $('#consName').show();
                    $('#vehType').hide();
                    $('#addContainersDiv').show();
                    $('#addVehTypeDiv').show();
                    $('#DistanceRouteDiv').hide();
                    $('#EmptyExportRouteDiv').show();
                    $('#ExportRouteDiv').hide();
                    $('#contractRouteDiv1').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').hide();
                    $('#shipingLineNo').val('');
                    $('#boE').hide();
                    $('#impNameLable').hide();
                    $('#impNameField').hide();
                    $('#mptyValidityLable').hide();
                    $('#emptyContDropTime').hide();
                    $('#boEdate').hide();
                    $("tr.extraRepoCust").hide();
                    $('#mainCust').show();
                    $('#repoCust').hide();
                    $("#routeChart").show();
                    if (document.getElementById("movementType").value == 9 || document.getElementById("movementType").value == 11) {
//                                if (document.getElementById("movementType").value == 9) {

                        $('#emptyPickup').show();
                        $('#portPickup').hide();
                        $('#emptyContDropTimes').hide();
                        $('#emptyContDropTime').hide();
                        $('#vesselCutoffTimes').hide();
                        $('#vesselCutoffTime').hide();
                        if (document.getElementById("movementType").value == 11) {
                            $('#emptyContDropTimes').show();
                            $('#emptyContDropTime').show();
                            $('#vesselCutoffTimes').hide();
                            $('#vesselCutoffTime').hide();
                        }
                    } else {
                        $('#emptyPickup').hide();
                        $('#portPickup').show();
                        $('#vesselCutoffTimes').show();
                        $('#vesselCutoffTime').show();
                        $('#emptyContDropTimes').hide();
                        $('#emptyContDropTime').hide();

                    }
                    if (document.cNote.emptyExportOrigin.value == '0') {
                        $('#emptyExportOrigin').empty();
                        $('#emptyExportOrigin').append(
                                $('<option></option>').val(7615).html('Triway CFS')
                                )
                    }
                    if (document.cNote.emptyExportDestination.value == '0') {
                        $('#emptyExportDestination').empty();
                        $('#emptyExportDestination').append(
                                $('<option></option>').val(7615).html('Triway CFS')
                                )
                    }

                    //   $('#emptyDestinationTemp').empty();
                    $('#customerName').val('');
                    $('#consigneeNameTemp').val('');
                    $('#custName').val('');
                    $("#routePlanAddRow").show();
                    $("#freezeRoute").show();
                    $("#resetRoute").show();
                    checkKeyForCustomer('customerName', 46, 'customerId');
                    checkKey('consigneeNameTemp', 46, 'consigneeCode', 'consigneeAddressCount');
                    checkKey1('custName', 46, 'billingParty', 'billPartyCode', 'consignorAddressCount');
                } else if (document.getElementById("movementType").value == 8) {
                    $('#DistanceRouteDiv').show();
                    $('#ImportRouteDiv').hide();
                    $('#ExportRouteDiv').hide();
                    $('#EmptyExportRouteDiv').hide();
                    $('#contractRouteDiv1').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').show();
                    $('#billOfEntry').val('');
                    $('#billOfEntryDate').val('');
                    $('#boE').hide();
                    $('#boEdate').hide();
                    $('#impNameLable').hide();
                    $('#impNameField').hide();
                    $('#mptyValidityLable').hide();
                    $('#emptyContDropTime').hide();
                    $("tr.extraRepoCust").hide();
                    // $('#repoCustomer').hide();
                    // $('#mainCustomer').show();
                    $('#mainCust').show();
                    $('#repoCust').hide();
                    $("#routeChart").show();
                    if (document.cNote.distanceOriginId.value != '0' && document.cNote.distanceDestinationId.value != '0') {
                        //alert("am here...");
                        //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                        var startDate = document.cNote.startDate.value;
                        addRouteCourse3(document.cNote.distanceOriginId.value, document.cNote.distanceOrigin.value, '1', 'PickUp', 0, 0, 0, '');
                        addRouteCourse3(document.cNote.distanceDestinationId.value, document.cNote.distanceDestination.value, '2', 'Drop', 0, 0, 0, startDate);
                        document.cNote.origin.value = document.cNote.distanceOriginId.value;
                        document.cNote.destination.value = document.cNote.distanceDestinationId.value;
                        $("#routePlanAddRow").show();
                        $("#freezeRoute").show();
                        $("#resetRoute").show();
                    } else {
                        $("#routePlanAddRow").hide();
                        $("#freezeRoute").hide();
                        $("#resetRoute").hide();
                    }

                } else if (document.getElementById("movementType").value == 12
                        || document.getElementById("movementType").value == 13) {


//                                $('#productDetailsDiv').show();
                    //$('#addModesDiv').show();
                    $('#ImportRouteDiv').hide();
                    $('#ExportRouteDiv').hide();
                    $('#addContainersDiv').show();
                    $('#addVehTypeDiv').show();
                    $('#contractRouteDiv1').show();
                    $('#EmptyExportRouteDiv').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').hide();
                    if (document.getElementById("movementType").value == 13) {
                        $('#boE').show();
                        $('#boEdate').show();
                        $('#impNameLable').show();
                        $('#impNameField').show();
                        $('#mptyValidityLable').show();
                        $('#emptyContDropTime').show();
                    } else {
                        $('#boE').hide();
                        $('#boEdate').hide();
                        $('#billOfEntry').val('');
                        $('#billOfEntryDate').val('');
                        $('#impNameLable').hide();
                        $('#impNameField').hide();
                        $('#mptyValidityLable').hide();
                        $('#emptyContDropTime').hide();
                        $('#importerName').val('');
                    }
                    $('#shipingLineNo').val('');
                    $("tr.extraRepoCust").hide();
                    // $('#repoCustomer').show();
                    // $('#mainCustomer').hide();
                    $('#mainCust').show();
                    $('#repoCust').hide();
                    $("#routeChart").hide();
                    resetRouteInfo();


                } else if (document.getElementById("movementType").value == 15) {


                    $('#productDetailsDiv').show();
                    //$('#addModesDiv').show();
                    $('#ImportRouteDiv').hide();
                    $('#ExportRouteDiv').hide();
                    $('#addMofVehTypeDiv').show();

                    $('#addContainersDiv').hide();
                    $('#addVehTypeDiv').hide();
                    $('#contractRouteDiv1').show();
                    $('#EmptyExportRouteDiv').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').hide();
                    $('#boE').hide();
                    $('#boEdate').hide();
                    $('#billOfEntry').val('');
                    $('#impNameLable').hide();
                    $('#impNameField').hide();
                    $('#importerName').val('');
                    $('#shipingLineNo').val('');
                    $("tr.extraRepoCust").hide();
                    // $('#repoCustomer').show();
                    // $('#mainCustomer').hide();
                    $('#mainCust').show();
                    $('#repoCust').hide();
                    $("#routeChart").hide();
                    resetRouteInfo();


                } else if (document.getElementById("movementType").value == 14 || document.getElementById("movementType").value == 16) {


                    $('#productDetailsDiv').show();
                    //$('#addModesDiv').show();
                    $('#addContainersDiv').hide();
                    $('#addVehTypeDiv').hide();
                    $('#ImportRouteDiv').hide();
                    $('#ExportRouteDiv').hide();
                    $('#contractRouteDiv1').show();
                    $('#EmptyExportRouteDiv').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').hide();
                    $('#boE').show();
                    $('#boEdate').show();
                    $('#impNameLable').show();
                    $('#impNameField').show();
//                                $('#billOfEntry').val('');
                    $('#shipingLineNo').val('');
                    //$("tr.extraRepoCust").show();
                    // $('#repoCustomer').show();
                    // $('#mainCustomer').hide();
                    $('#mainCust').show();
                    $('#repoCust').hide();
                    $("#routeChart").hide();
                    resetRouteInfo();


                } else if (document.getElementById("movementType").value == 19 || document.getElementById("movementType").value == 17 || document.getElementById("movementType").value == 18 || document.cNote.movementType.value == 21 || document.getElementById("movementType").value == 23 || document.getElementById("movementType").value == 24 || document.cNote.movementType.value == 33 || document.cNote.movementType.value == 34 || document.cNote.movementType.value == 35 || document.cNote.movementType.value == 36) {
                    $('#ImportRouteDiv').hide();
                    $('#ExportRouteDiv').hide();
                    $('#contractRouteDiv1').show();
                    $('#EmptyExportRouteDiv').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').hide();
                    $('#boE').hide();
                    $('#boEdate').hide();
                    $('#billOfEntry').val('');
                    $('#impNameLable').hide();
                    $('#impNameField').hide();
                    $('#mptyValidityLable').hide();
                    $('#emptyContDropTime').hide();
                    $('#importerName').val('');
                    $('#shipingLineNo').val('');
                    $("tr.extraRepoCust").hide();

                    $('#repoCust').hide();
                    $('#mainCust').show();
                    $("#routeChart").hide();
                    $('#addVehTypeDiv').show();
                    $('#addContainersDiv').show();
//                                $('#tdFreightAmountLabel').show();
//                                $('#tdFreightAmount').show();
                    //$('#tdFreightAmountCheckBox').show();

                    if (document.getElementById("movementType").value == 18 || document.cNote.movementType.value == 21 || document.cNote.movementType.value == 33 || document.cNote.movementType.value == 34 || document.cNote.movementType.value == 35 || document.cNote.movementType.value == 36) {
                        $('#emptyContDropTimes').show();
                        $('#emptyContDropField').show();
                    } else {
                        $('#emptyContDropTimes').hide();
                        $('#emptyContDropField').hide();
                    }
                    resetRouteInfo();

                } else {
//                                alert("test");
                    $('#ImportRouteDiv').hide();
                    $('#ExportRouteDiv').hide();
                    $('#contractRouteDiv1').show();
                    $('#EmptyExportRouteDiv').hide();
                    $('#emptyContainerLocationTD').hide();
                    $('#surcontainerTypes').hide();
                    $('#slN').hide();
                    $('#boE').hide();
                    $('#boEdate').hide();
                    $('#billOfEntry').val('');
                    $('#impNameLable').hide();
                    $('#impNameField').hide();
                    $('#mptyValidityLable').hide();
                    $('#emptyContDropTime').hide();
                    $('#importerName').val('');
                    $('#shipingLineNo').val('');
                    $("tr.extraRepoCust").hide();
//                                $("tr.extraRepoCust").hide();
//                                
                    // $('#repoCustomer').show();
                    // $('#mainCustomer').hide();

                    $('#repoCust').hide();
                    $('#mainCust').show();
//                                $('#repoCust').show();
//                                $('#mainCust').hide();
                    $("#routeChart").hide();
                    $('#emptyContDropTimes').hide();
                    $('#emptyContDropField').hide();
                    resetRouteInfo();


                }
                if (document.getElementById("movementType").value == 12
                        || document.getElementById("movementType").value == 13
                        || document.getElementById("movementType").value == 14
                        || document.getElementById("movementType").value == 15
                        || document.getElementById("movementType").value == 16
                        ) {
                    $('#contractRouteDiv11').show();
                } else {
                    $('#contractRouteDiv11').hide();
                }
                var rowCountNew1 = document.getElementById('addContainer').rows.length;
                //alert(rowCountNew1);
                if (rowCountNew1 == 2) {
                    addRow2();
                }

//                           the walk-in new logic
                var customerTypeId = document.getElementById("customerTypeId").value;
                if (customerTypeId == '2') {
//                                alert("customerTypeId::"+customerTypeId);
                    $('#customerId').val('2980');
                    $('#billingTypeId').val('1');
                    $('#contractId').val('683');
                    getContractRoutes('683');
                    getContractRoutesOrigin('683');
                    getContractRoutesOriginExp('683');
                    getContractRoutesOriginRepo('683');

                    $('#contract').hide();
                    $('#walkIn').show();

                } else {
                    $('#contract').show();
                    $('#walkIn').hide();
                }
//                             document.getElementById("movementType").disabled=true;
            }
            function showVehicleTypeList() {
                var truckType = document.cNote.truckType.value;
                if (truckType == 2) {
                    $('#addContainersDiv').show();
                    $('#addVehTypeDiv').show();
                    $('#mofVehType1').hide();
                    $('#mofVehType2').hide();
                } else {
                    $('#addContainersDiv').hide();
                    $('#addVehTypeDiv').hide();
                    $('#mofVehType1').show();
                    $('#mofVehType2').show();

                }
            }
            function viewConsignorDetails() {


                var billingParty = document.getElementById("billingParty").value;
                var addressCount = document.getElementById("consignorAddressCount").value;
                if (addressCount == 1) {

                    window.open('/throttle/getConsignorAddress.do?CustId=' + billingParty + "&param=Consignor", 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                }
            }

            function viewConsigneeDetails() {
                var billingParty = document.getElementById("consigneeId").value;
                var addressCount = document.getElementById("consigneeAddressCount").value;
                //  alert(addressCount);
                if (addressCount == 1) {

                    window.open('/throttle/getConsignorAddress.do?CustId=' + billingParty + "&param=Consignee", 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                }
            }


            //         var httpRequest;

            function getBillingPartyCheck() {
                var customerId = document.cNote.customerId.value;
                if (customerId != '') {
                    var url = '/throttle/checkBillingPartyId.do?customerId=' + document.cNote.customerId.value;
                    if (window.ActiveXObject)
                    {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    } else if (window.XMLHttpRequest)
                    {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("POST", url, true);
                    httpRequest.onreadystatechange = function () {
                        go1();
                    };
                    httpRequest.send(null);
                }
            }


            function go1() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var response = httpRequest.responseText;
                        var temp = response.split('-');
                        if (response != "") {
                            document.getElementById("Status").innerHTML = "Today You Have Already Created CNote For " + httpRequest.responseText.valueOf() + " Billing Party";
                            document.cNote.customerId.focus();
                            document.cNote.customerId.select();
                        } else
                        {
                            document.getElementById("Status").innerHTML = "";
                        }
                    }
                }
            }

        </script>
        <script>


            function fnValidatePAN(Obj) {
                if (Obj == null)
                    Obj = window.event.srcElement;
                if (Obj.value != "") {
                    ObjVal = Obj.value;
                    var panPat = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
                    var code = /([C,P,H,F,A,T,B,L,J,G])/;
                    var code_chk = ObjVal.substring(3, 4);
                    if (ObjVal.search(panPat) == -1) {
                        alert("Invalid Pan No");
                        Obj.focus();
                        return false;
                    }
                    if (code.test(code_chk) == false) {
                        alert("Invaild PAN Card No.");
                        return false;
                    }
                }
            }

        </script>

        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> Consignment Note</h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Sales</a></li>
            <li class="active">Consignment Note</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="
                    addRow1();
//                    addRow2();
                    multiPickupShow();
//                    multiDeliveryShow();
                    currentTime();">
                <% String menuPath = "Operations >>  Create New Consignment Note";
                            request.setAttribute("menuPath", menuPath);
                %>
                <form name="cNote" method="post">

                    <%@include file="/content/common/message.jsp" %>
                    <%
                                Date today = new Date();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                String startDate = sdf.format(today);
                    %>
                    <div id="spinner" class="spinner" style="display:none;" >
                        <img id="img-spinner" src="images/page-loader2.gif" alt="Loading" />
                        <div style="margin-top: 10px; color: white">
                            <b>Please wait...</b>
                        </div>
                    </div>
                    <input type="hidden" name="contractApprovalStatus" value='' />
                    <input type="hidden" name="startDate" value='<%=startDate%>' />

                    <table class="table table-default mb30 table-hover"   style="width:100%">
                        <!--                <tr  ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Consignment Note</td></tr>
                                        </tr>
                                        <tr>
                                            <td>Entry Option</td>
                                            <td><input type="radio" name="entryType" value="1" checked="">Manual</td>
                                            <td><input type="radio"  name="entryType" value="2" onclick="goToImportPage();">Import</td>
                                            <td >&nbsp;</td>
                                            <input id="txtF" type="text" m onKeyPress="return onKeyPressBlockCharacters(event);"  />
                                        </tr>-->

                        <tr>
                            <td><font color="red">*</font>Customer Type</td>
                            <td>
                                <select name="customerTypeId" id="customerTypeId" style="width:240px;"  class="form-control" onchange="setWalkin()">
                                    <option value="1">Contract</option>
                                    <option value="2">Walk-In</option>
                                </select>
                            </td>
                            <td>Consignment Date </td>
                            <td ><input type="text"  name="consignmentDate" style="width:240px;"  id="consignmentDate" value="<%=startDate%>" class="form-control datepicker"  /></td>
                            <td  align="left"  >HH: <select name="consignmentHour"  id="consignmentHour"  autocomplete='off' class="textbox" style="width:60px;height:40px">
                                    <option value='00'>00</option>
                                    <option value='01'>01</option>
                                    <option value='02'>02</option>
                                    <option value='03'>03</option>
                                    <option value='04'>04</option>
                                    <option value='05'>05</option>
                                    <option value='06'>06</option>
                                    <option value='07'>07</option>
                                    <option value='08'>08</option>
                                    <option value='09'>09</option>
                                    <option value='10'>10</option>
                                    <option value='11'>11</option>
                                    <option value='12'>12</option>
                                    <option value='13'>13</option>
                                    <option value='14'>14</option>
                                    <option value='15'>15</option>
                                    <option value='16'>16</option>
                                    <option value='17'>17</option>
                                    <option value='18'>18</option>
                                    <option value='19'>19</option>
                                    <option value='20'>20</option>
                                    <option value='21'>21</option>
                                    <option value='22'>22</option>
                                    <option value='23'>23</option>
                                </select>
                                MI: <select name="consignmentMinute"  id="graReportMinute" class="textbox" style="width:60px;height:40px" >
                                    <option value='00'>00</option>
                                    <option value='01'>01</option>
                                    <option value='02'>02</option>
                                    <option value='03'>03</option>
                                    <option value='04'>04</option>
                                    <option value='05'>05</option>
                                    <option value='06'>06</option>
                                    <option value='07'>07</option>
                                    <option value='08'>08</option>
                                    <option value='09'>09</option>
                                    <option value='10'>10</option>
                                    <option value='11'>11</option>
                                    <option value='12'>12</option>
                                    <option value='13'>13</option>
                                    <option value='14'>14</option>
                                    <option value='15'>15</option>
                                    <option value='16'>16</option>
                                    <option value='17'>17</option>
                                    <option value='18'>18</option>
                                    <option value='19'>19</option>
                                    <option value='20'>20</option>
                                    <option value='21'>21</option>
                                    <option value='22'>22</option>
                                    <option value='23'>23</option>
                                    <option value='24'>24</option>
                                    <option value='25'>25</option>
                                    <option value='26'>26</option>
                                    <option value='27'>27</option>
                                    <option value='28'>28</option>
                                    <option value='29'>29</option>
                                    <option value='30'>30</option>
                                    <option value='31'>31</option>
                                    <option value='32'>32</option>
                                    <option value='33'>33</option>
                                    <option value='34'>34</option>
                                    <option value='35'>35</option>
                                    <option value='36'>36</option>
                                    <option value='37'>37</option>
                                    <option value='38'>38</option>
                                    <option value='39'>39</option>
                                    <option value='40'>40</option>
                                    <option value='41'>41</option>
                                    <option value='42'>42</option>
                                    <option value='43'>43</option>
                                    <option value='44'>44</option>
                                    <option value='45'>45</option>
                                    <option value='46'>46</option>
                                    <option value='47'>47</option>
                                    <option value='48'>48</option>
                                    <option value='49'>49</option>
                                    <option value='50'>50</option>
                                    <option value='51'>51</option>
                                    <option value='52'>52</option>
                                    <option value='53'>53</option>
                                    <option value='54'>54</option>
                                    <option value='55'>55</option>
                                    <option value='56'>56</option>
                                    <option value='57'>57</option>
                                    <option value='58'>58</option>
                                    <option value='59'>59</option>

                                </select>

                            </td>
                        <tr>
                        <tr style="display:none;">
                            <td><font color="red"></font>Customer Reference No</td>
                            <td><input type="text" readonly style="width:240px;" name="orderReferenceNo" id="orderReferenceNo" maxlength="100" value="" class="form-control" /></td>
                        </tr>
                        <input type="hidden" name="shippingLineTwo" id="shippingLineTwo" maxlength="45" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" class="form-control" />

                        <tr>
                            <td><font color="red">*</font>Movement Type  &nbsp;
                            </td>
                            <td>
                                <select name="movementType" id="movementType" style="width:240px;" onChange="showMovTab();"  class="form-control" >
                                    <option value="0">--Select---</option>
                                    <c:forEach items="${movementTypeList}" var="proList">
                                        <c:forEach items="${userMovementList}" var="userMovementList">
                                            <c:if test="${proList.movementTypeId == userMovementList.movementTypeId}">
                                                <option value="<c:out value="${proList.movementTypeId}"/>"><c:out value="${proList.movementType}"/></option>
                                            </c:if>
                                        </c:forEach>
                                    </c:forEach>

                                </select>
                            </td>


                            <td><font color="red">*</font>Select Product Type &nbsp;&nbsp;
                            </td>
                            <td>
                                <c:if test="${productCategoryList != null}">
                                    <input type="hidden" name="productCategoryId" id="productCategoryId" class="form-control" />
                                    <select name="productCategoryIdTemp" style="width:240px;" id="productCategoryIdTemp" onchange="setProductCategoryValues();"   class="form-control"  >
                                        <c:forEach items="${productCategoryList}" var="proList">
                                            <option value="<c:out value="${proList.productCategoryId}"/>"><c:out value="${proList.customerTypeName}"/></option>
                                        </c:forEach>
                                    </select>
                                </c:if>
                            </td>
                            
                        </tr>
                        <!--<table class="table table-info mb30 table-hover" border="1" style="width:100%">-->
                        <tr>
                        <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                        <div align="center" id="Status">&nbsp;&nbsp;</div>
                        </font>
                        <td><font color="red">*</font>Customer Name</td>
                        <td id="contract">
                            <input type="text" name="customerName" style="width:240px;"   id="customerName" class="form-control"   onKeyPress="return onKeyPressBlockNumbers(event);" onkeyup="return checkKeyForCustomer(this, event, 'customerId')"/>
                            <input type="hidden" name="customerId" id="customerId" class="form-control"  />
                        </td>
                        <td id="walkIn" style="display:none;">
                            <input type="text" name="walkinCustomerName" style="width:240px;"    id="walkinCustomerName" class="form-control"  />
                            <input type="hidden" name="walkInBillingTypeId"   id="walkInBillingTypeId" value="1" class="form-control"  />
                        </td>
                        <td><font color="red"></font>Customer Code</td>
                        <td><input type="text"  style="width:240px;" name="customerCode" id="customerCode" class="form-control"  readonly  /></td>
                        <input type="hidden" name="paymentType" id="paymentType" class="form-control" />
                        
                        
                        </tr>
                        <tr style="display:none;">
                            <td id="mainCustomer" class="form-control"><font color="red">*</font><span id="repoCust" style="display: none"> Consignor Name(1)</span><span id="mainCust">Consignor Name</span></td>
                            <td><input type="hidden" name="billingParty" id="billingParty" value="0" class="form-control" />
                                <input type="hidden" id="consignorAddressCount" name="consignorAddressCount" value=""/>
                                <input type="text" name="custName" onKeyPress="return onKeyPressBlockNumbers(event);"
                                       onkeyup="return checkKey1(this, event, 'billingParty', 'billPartyCode', 'consignorAddressCount')" id="custName" class="form-control" />
                            </td>
                            <td><font color="red"></font>Consignor Code</td>
                            <td><input type="text" name="billPartyCode" id="billPartyCode" class="form-control"  readonly  /></td>
                        <input type="hidden" name="custPaymentType" id="custPaymentType" class="form-control" />
                        </tr>

                        <tr style="display:none;"  >
                            <td><font color="red">*</font>Consignee Name</td>
                            <td>
                                <input type="text" onkeyup="return checkKey(this, event, 'consigneeCode', 'consigneeAddressCount')" name="consigneeNameTemp" onKeyPress="return onKeyPressBlockNumbers(event);"  id="consigneeNameTemp" class="form-control"  /> <!-- -->
                                <input type="hidden" id="consigneeAddressCount" name="consigneeAddressCount" value="0"/>
                            </td>
                            <td><font color="red"></font>Consignee Code</td>
                            <td><input type="text" name="consigneeCode" id="consigneeCode" class="form-control"  readonly  /></td>

                        </tr>
                        <tr >
                            <td id="impNameLable" style="display:none" ><font color="red">*</font>Importer Name </td>
                            <td id="impNameField" style="display:none" >  <input type="text"  name="importerName" value="" onKeyPress="return onKeyPressBlockNumbers(event);"  id="importerName" class="form-control"  /> <!-- -->
                            </td>

                        </tr>

                        <tr class="extraRepoCust" style="display: none">
                            <td><font color="red"></font>Consignor Name(2)</td>
                            <td><input type="hidden" name="secCustId" id="secCustId" value="0" class="form-control"  />
                                <input type="text" name="secCustName" onKeyPress="return onKeyPressBlockNumbers(event);"
                                       onkeyup="return checkKey(this, event, 'secCustId', 'secCustCode')"  id="secCustName" class="form-control"  /></td>
                            <td><font color="red"></font>Consignor Code(2)</td>
                            <td><input type="text" name="secCustCode" id="secCustCode" class="form-control"  readonly  /></td>

                        </tr>
                        <!--Invoice Table-->

                        <tr style="display: none" ><td colSpan="9" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Invoice Details</td></tr>
                        <tr style="display: none" >
                            <td><font color="red">*</font>Invoice No</td>
                            <td><input type="text"  name="invoiceNo"   id="invoiceNo" class="form-control" value="0" /></td>

                            <td><font color="red">*</font>Invoice Date</td>
                            <td>
                                <input type="text" id="invoiceDates"  name="invoiceDates" value="<%=startDate%>" readonly class="form-control datepicker"  /></td>

                        </tr>
                        <tr style="display: none" >
                            <td><font color="red">*</font>Invoice Value</td>
                            <td><input type="text"  name="invoiceValue" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="setEwayBillNo();" id="invoiceValue" class="form-control" value="0" /></td>

                            <td id="eWayBillLabel" style="display: none"><font color="red">*</font>E-Way Bill No</td>
                            <td id="eWayBill" style="display: none" ><input type="text" maxlength="12" name="eWayBillNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  id="eWayBillNo" class="form-control" value="0" onblur="validateEwayBillNo();"  /></td>

                        </tr>

                        <!--</table>-->
                        <div id="contractDetails" style="display: none">
                            <!--<table>-->
                            <!--                            <tr>
                                                            <td>-->
                            Contract No :
                            <!--                                </td> 
                                                            <td>-->
                            <b><input type="hidden" name="contractId" id="contractId" class="form-control" /><label id="contractNo"></label></b>
                            <!--</td>-->
                            <!--<td>-->  
                            <!--Contract Expiry Date :-->
				<!--</td>-->
				<!--<td>-->
				<font color="green" style="display:none"><b><label id="contractExpDateTemp"></label></b></font>
				<input type="hidden" id="contractExpDate" name="contractExpDate" value="<c:out value="${contractExpDate}"/>"/>
				<!--                                </td>
								<td>&nbsp;&nbsp;&nbsp;</td>
				    <td>-->
                            <a href="#" onclick="viewCustomerContract();">view</a>
                            <!--                                </td>
                                                        </tr>-->
                            <!--</table>-->
                        </div>            

                        <!--<table class="table table-info mb30 table-hover"  border="1" style="width:100%">-->

                        <tr id="boE"  style="display: none">
                            <td > <font color="red">*</font>BL No  &nbsp;</td>
                            <td>  <input type="text" id="billOfEntry" maxlength="45" name="billOfEntry" value="" onKeyPress="return onKeyPressBlockCharacters(event);" class="form-control" style="width:240px;"  />
                            </td>
                        </tr>     
                        <tr  id="slN"  style="display: none">     
                            <td> <font color="red">*</font>
                                <!--SLN No-->  SB No
                                &nbsp;</td>
                            <td><input type="text" id="shipingLineNo" maxlength="45" name="shipingLineNo" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" class="form-control" style="width:240px;" /></td>
                            <td> <font color="red">*</font>Clearance Type</td>
                            <td><select id="clearanceType" name="clearanceType" class="form-control" style="width:240px;" />
                                    <option value="0" selected>--Select--</option>
                                    <option value="1" >Factory</option>
                                    <option value="2">CFS (With Dummy Seal)</option>
                                    <option value="3">CFS (With SB)</option>
                                    <option value="4">Port</option>
                                </select> 
                            </td>
                        </tr>
                        <tr>
                             <td><font color="red"></font>Job No</td>
                        <td><input type="text"  style="width:240px;" name="jobNo" id="jobNo" class="form-control"   /></td>
                        </tr>

                        <tr>
                            <td   colspan="1" align="left" >&nbsp;<label id="temperatureInfo"></label></td>


                            <td id="boEdate"  style="display: none"> <font color="red">*</font>BOE Date  &nbsp;
                                <input type="text" id="billOfEntryDate"  name="billOfEntryDate" value=""  class="form-control datepicker" readonly style="width:180px;height:40px;" /></td>

                            <td id="emptyContainerLocationTD"  style="display:none;"><font color="red">*</font>Empty Container Location &nbsp;
                                <select name="emptyContainerLocation" id="emptyContainerLocation" onchange="getContractRoutesOriginOtherICD();" 
                                        class="form-control" style="width:180px;height:40px;" >
                                    <option value="1" selected>Chennai</option>
                                    <c:forEach items="${emptyPickupList}" var="emptyPickupList">
                                        <option value="<c:out value="${emptyPickupList.firstPickupName}"/>"><c:out value="${emptyPickupList.firstPickupName}"/></option>
                                    </c:forEach>

                                </select>

                            </td>
                            <td id="surcontainerTypes"  style="display:none;"><font color="red">*</font>ContainerType &nbsp;
                                <select name="survayCon" id="survayCon" class="form-control" style="width:180px;height:40px;" >
                                    <option value="1" selected>Any One</option>
                                    <option value="2" >Survey</option>
                                </select>

                            </td>
                        </tr>

                    </table>    

                    <div style="display: none">
                        <tr>
                            <td><font color="red"></font>Address</td>
                            <td><textarea rows="1" cols="16" id="customerAddress" name="customerAddress" class="form-control" ></textarea></td>
                            <td><font color="red"></font>Pincode</td>
                            <td><input type="text"  name="pincode" id="pincode"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="form-control"  /></td>
                        </tr>
                        <tr>
                            <td><font color="red"></font>Mobile No</td>
                            <td><input type="text"  name="customerMobileNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  id="customerMobileNo" class="form-control"   /></td>
                            <td><font color="red"></font>E-Mail Id</td>
                            <td><input type="text"  name="mailId" id="mailId" class="form-control"   /></td>
                        </tr>
                        <tr>
                            <td>Phone No</td>
                            <td><input type="text" name="customerPhoneNo" id="customerPhoneNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="form-control"  maxlength="10"   /></td>

                            <td>Billing Type</td>
                            <td id="billingType"><input type="hidden" name="billingTypeId" id="billingTypeId" class="form-control"  /><label id="billingTypeName"></label></td>

                        </tr>
                    </div>








                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" style="display: none">

                        <tr>
                            <td class="contenthead" colspan="6" >Order  Type </td>
                        </tr>
                        <tr>

                            <td><font color="red">*</font>Select Order Type &nbsp;&nbsp;

                                <select name="orderType" id="orderType" onchange="showOrderType();" >
                                    <option value="0">--Select---</option>
                                    <option value="1">LCL Order</option>
                                    <option value="2" selected >FCL Order</option>
                                </select>

                            </td>


                        </tr>
                    </table>
                    <br>

                    <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr    ><td colSpan="9" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Consignment / Route Details</td></tr>


                        <tr id="contractRouteDiv11" style="display:none;">
                            <td style="display:none"><font color="red">*</font>TripType(Single/Return)</td>
                            <td style="display:none" >
                                <select id="tripType" name="tripType" class="form-control" style="width:180px;height:40px;display: none">
                                    <option value="0">-Select-</option>
                                    <option value="1" selected>Single</option>
                                    <option value="2">Return</option>
                                </select>
                            </td>

                            <td id="mptyValidityLable"  style="display: none"><font color="red">*</font>Empty Bond Validity Time</td>
                            <td id="mptyValidityField"  style="display: none" ><input type="text" name="emptyContDropTime" id="emptyContDropTime" style="width:180px;height:40px;" value="" class="form-control datepicker" /></td>


                        </tr>
                        <tr id="contractRouteDiv1" style="display:none;">
                            <td align="right" ><font color="red">*</font>Origin</td>
                            <td>
                                <select id="contractRouteOrigin1" name="contractRouteOrigin1" onchange='setContractOriginDestinationRepo();' class="form-control" style="width:180px;height:40px;">
                                    <option value="0">-Select-</option>
                                </select>
                            </td>
                            <td align="right"><font color="red">*</font>Destination</td>
                            <td>
                                <select id="contractRouteDestination1" name="contractRouteDestination1" onchange='setContractRouteDetailsRepo();' class="form-control" style="width:180px;height:40px;">
                                    <option value="0">-Select-</option>
                                </select>
                            </td>
                            <td colspan="2" >&nbsp;</td>
                            <td id="emptyContDropTimes"  style="display: none"><font color="red">*</font>Empty Validity Time</td>
                            <td id="emptyContDropField" style="display: none" ><input type="text" name="emptyContDropTime1" id="emptyContDropTime1" readonly  value="" class="form-control datepicker" /></td>

                        </tr>
                        <input type="hidden" name="origin" value="" />
                        <input type="hidden" name="destination" value="" />
                        <input type="hidden" name="destinationHub" id="destinationHub" value="" />
                        <input type="hidden" name="originHub" id="originHub"value="" />

                        <tr  id="contractRouteDiv2" style="display:none;">
                            <td align="right"><font color="red">*</font>Origin</td>
                            <td>
                                <select id="originTemp" name="originTemp" onchange='setDestination();' class="form-control" >
                                    <option value="0">-Select-</option>
                                </select>
                            </td>
                            <td align="right"><font color="red">*</font>Destination</td>
                            <td><select id="destinationTemp1" name="destinationTemp1"  onchange="setRouteDetails();" class="form-control" >
                                    <option value="0">-Select-</option>
                                </select></td>
                            <td colspan="2" >&nbsp;</td>
                        </tr>
                        <tr  id="ExportRouteDiv" style="display:none;">
                            <td style="width:90px;" align="right"><font color="red">*</font>Origin</td>
                            <td style="width:90px;">
                                <select id="expcontractRouteOrigin" name="expcontractRouteOrigin" onchange='setContractOriginInteremExp();' class="form-control" >
                                    <option value="0">-Select-</option>
                                </select>
                            </td>
                            <td style="width:150px;" align="right"><font color="red">*</font>Loading </td>
                            <td  style="width:150px;" ><select id="expcontractRouteDestination" name="expcontractRouteDestination"  onchange="setContractOriginDestinationExp();" class="form-control" >
                                    <option value="0">-Select-</option>
                                </select></td>
                            <td style="width:150px;margin-left:-150px;" align="right"><font color="red">*</font>Container Drop</td>
                            <td style="width:150px;"><select id="expdestinationTemp" name="expdestinationTemp"  onchange="setContractRouteDetailsNewExp();" class="form-control" >
                                    <option value="0">-Select-</option>
                                </select>
                            </td>

                        </tr>
                        <tr  id="DistanceRouteDiv" style="display:none;">
                            <td style="width:90px;" align="right"><font color="red">*</font>Origin</td>
                            <td style="width:90px;">
                                <input type="text" id="distanceOrigin" name="distanceOrigin" value="Triway CFS" readonly>
                                <input type="hidden" id="distanceOriginId" name="distanceOriginId" value="7615">
                            </td>

                            <td style="width:150px;margin-left:-150px;" align="right"><font color="red">*</font> Drop</td>
                            <td style="width:150px;"> <input type="text" id="distanceDestination" name="Triway CFS" value="triwayDestination" readonly>
                                <input type="hidden" id="distanceDestinationId" name="distanceDestinationId" value="7615">
                            </td>

                        </tr>
                        <tr  id="ImportRouteDiv" style="display:none;">
                            <td align="right"><font color="red">*</font>Origin</td>
                            <td>
                                <select id="contractRouteOrigin" name="contractRouteOrigin" onchange='setContractOriginInterem();' class="form-control" >
                                    <option value="0">-Select-</option>
                                </select>
                            </td>
                            <td><font color="red" align="right">*</font>Unloading </td>
                            <td><select id="contractRouteDestination" name="contractRouteDestination"  onchange="setContractOriginDestination();" class="form-control" >
                                    <option value="0">-Select-</option>
                                </select></td>
                            <td><font color="red">*</font>Empty Container Drop</td>
                            <td><select id="destinationTemp" name="destinationTemp"  onchange="setContractRouteDetailsNew();" class="form-control" >
                                    <option value="0">-Select-</option>
                                </select></td>
                        </tr>
                        <tr  id="EmptyExportRouteDiv" style="display:none;">
                            <td align="right"><font color="red">*</font>Origin</td>
                            <td>
                                <select id="emptyExportOrigin" name="emptyExportOrigin" onchange='setContractOriginInterem();' class="form-control" >
                                    <option value="0">-Select-</option>
                                </select>
                            </td>
                            <td align="right"><font color="red">*</font>Destination </td>
                            <td><select id="emptyExportDestination" name="emptyExportDestination"  onchange="setContractOriginDestination();" class="form-control" >
                                    <option value="0">-Select-</option>
                                </select></td>
                            <td id="emptyPickup"  style="display: none"><font color="red">*</font>Pickup / Drop Location</td>
                            <td id="portPickup"  style="display: none"><font color="red">*</font>Port Location</td>
                            <td><select id="emptyDestinationTemp" name="emptyDestinationTemp" class="select2 input-default form-control" onchange="setEmptyDestination();
                                    setRoute();" class="form-control" >
                                    <option value="0">-Select-</option>
                                </select>
                                <script>
                                    $('#emptyDestinationTemp').select2({placeholder: 'Fill Location'});
                                </script>
                            </td>
                            <td id="vesselCutoffTimes"  style="display: none"><font color="red">*</font>Vessel CutOff Time</td>
                            <td><input type="text" name="vesselCutoffTime" id="vesselCutoffTime" style="display: none" value="" class="form-control datepicker" /></td>

                            <td id="emptyContDropTimes"  style="display: none"><font color="red">*</font>Empty Bond Validity Time</td>
                            <td id="emptyContDropField" style="display: none" ><input type="text" name="emptyContDropTime" id="emptyContDropTime"  value="" class="form-control datepicker" /></td>
                        </tr>


                        <tr id="routeChart" style="display:none;" >
                            <td colspan="9" align="left" >
                                <table class="table table-info mb30 table-hover" style="width:100%" id="routePlan"  >
                                    <thead >
                                        <tr id="tableDesingTH" >
                                            <th   ><font color="red">*</font>Order Sequence</th>
                                            <th   ><font color="red">*</font>Point Name</th>
                                            <th   ><font color="red">*</font>Point Type</th>
                                            <th   ><font color="red">*</font>Address</th>
                                            <th   ><font color="red">*</font>Required Date</th>
                                            <th   ><font color="red"></font>Required Time</th>
                                        </tr>

                                </table>
                                <table class="border" align="left" width="900" cellpadding="0" cellspacing="0" >
                                    <tr>
                                        <td>
                                            <input type="button" class="btn btn-info" id="routePlanAddRow" style="display:none;" value="Interim Point" name="save" onClick="addRouteCourse2();">
                                            &nbsp;&nbsp;<input type="button" class="btn btn-info" id="freezeRoute" style="display:none;"  value="Freeze Route" name="save" onClick="getContractVehicleType();">
                                            &nbsp;&nbsp;<input type="button" class="btn btn-info" id="resetRoute" style="display:none;"  value="Reset" name="save" onClick="resetRouteInfo();">
                                            &nbsp;&nbsp;<input type="button" class="btn btn-info" id="unFreezetRoute" style="display:none;"  value="UnFreeze Route" name="save" onClick="unFreezetRouteFn();">
                                        </td>
                                    </tr>
                                </table>
                                </center>
                                <br>


                            </td>
                        </tr>

                        <tr  id="distanceContractTypeDiv" style="display:none;">
                            <td><font color="red">*</font>Available Type</td>
                            <td>
                                <select id="distanceContractType" name="distanceContractType" onchange='setDistanceContractId(this.value);' class="form-control" >
                                    <option value="0~0-0">-Select-</option>
                                </select>
                            </td>
                        <input type="hidden" id="distanceContractId" name="distanceContractId" value="0"/>
                        <!--                            <td colspan="2" >&nbsp;</td>
                                                    <td colspan="2" >&nbsp;</td>-->
                        </tr>
                        <input type="hidden" name="businessType" value="1" />
                        <tr style="display: none">
                            <td >Goods Description &nbsp;&nbsp;&nbsp;&nbsp;
                                <input type="text" name="consignmentOrderInstruction" 
                                       id="consignmentOrderInstruction" value ="" class="form-control" ></td>
                            <!--                            <td>Multi Pickup</td>-->
                            <td><input type="hidden" class="form-control" name="multiPickup" id="multiPickup" onclick="multiPickupShow()" value="N"></td>
                            <!--                            <td>Multi Delivery</td>-->
                            <td colspan="4" ><input type="hidden" class="form-control" name="multiDelivery" id="multiDelivery" onclick="multiDeliveryShow()" value="N"></td>
                        </tr>
                    </table>
                    <div id="productDetailsDiv" style="display:none;">
                        <table class="table table-info mb30 table-hover" style="width:100%" id="bg"  >
                            <!--<table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" >-->
                            <tr>
                                <td colspan="4" >
                                    <table class="table table-info mb30 table-hover" style="width:100%" id="addTyres1"  >
                                        <!--<table border="0" class="border" align="left" width="90%" cellpadding="0" cellspacing="0" id="addTyres1">-->
                                        <thead >
                                            <tr >
                                                <th  align="center"  >Sno</th>
                                                <!--<th   >Product/Article Code</th>-->
                                                <th   >Product/Article Name </th>
                                                <!--<th   >Batch </th>-->
                                                <th   ><font color='red'></font>Packages</th>
                                                <th   ><font color='red'></font>Uom</th>
                                                <th   ><font color='red'></font>Volume(cbm)</th>
                                                <th   ><font color='red'></font>Weight(in Kg)</th>
                                            </tr>
                                        </thead>
                                        <br>

                                        <tr>
                                            <td colspan="1" align="right">
                                                <!--&nbsp;&nbsp;&nbsp;<input type="reset" class="button" value="Clear">-->
                                                <div id="packageAddButton" style=display:block;">
                                                    <input type="button" class="btn btn-success" value="Add Row" name="save" onClick="addRow1()">
                                                </div> &nbsp;&nbsp;&nbsp;&nbsp;

                                            </td>
                                            <td colspan="4" align="left">

                                                <div id="packageCompleteButton" style=display:none;">
                                                    <input type="button" class="btn btn-success" value="Complete" name="Complete" onClick="packageAddComplete()">
                                                </div>


                                            </td>
                                        </tr>
                                    </table>

                                </td>
                                <td colspan="2" align="left">
                                    <table class="table table-info mb30 table-hover" style="width:100%" id="table"  >
                                        <!--<table align="center" border="0" id="table" class="sortable" style="width:100px;" >-->
                                        <thead >
                                            <tr id="tableDesingTH" >


                                                <th>
                                                    <label >Total No Packages</label>
                                                    <label id="totalPackages">0</label>
                                                    <input type="hidden" id="totalPackage" name="totalPackage" />
                                                </th>
                                                <th>
                                                    <label >Total Weight (Kg)</label>
                                                    <label id="totalWeight">0</label>
                                                    <input type="hidden" id="totalWeightage" name="totalWeightage" />
                                                </th>
                                                <th>
                                                    <label >Total volume (cbm)</label>
                                                    <label id="totalvolume">0</label>
                                                    <input type="hidden" id="totalVolumes" name="totalVolumes" />
                                                </th>
                                            </tr>
                                        </thead>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div id="addMofVehTypeDiv"  style="display: none;">
                        <table>
                            <tr>
                                <td>Truck Type</td>
                                <td>

                                    <select name="truckType" onChange="showVehicleTypeList();">
                                        <option value="0">-select-</option>
                                        <option value="1">Truck</option>
                                        <option value="2">Trailer</option>
                                    </select>
                                </td>

                                <td id="mofVehType1" style="display: none">Vehicle Type</td>
                                <td id="mofVehType2" style="display: none">

                                    <select   name='mofVehTypeId'  class='textbox' style='width:150px;height:40px;' >
                                        <option value='0'>--select---</option> 
                                        <c:if test="${vehicleTypeList != null}" >
                                            <c:forEach items="${vehicleTypeList}" var="vehicleType">
                                                <c:if test="${vehicleType.vehicleTypeId != 1058}" >
                                                    <c:if test="${vehicleType.vehicleTypeId != 1059}" >
                                                        <option  value='<c:out value="${vehicleType.vehicleTypeId}" />'><c:out value="${vehicleType.vehicleTypeName}" /> 
                                                        </c:if>
                                                    </c:if>
                                                </c:forEach > 
                                            </c:if>  
                                    </select>
                                </td>

                            </tr>
                        </table>
                    </div>
                    <div id="addVehTypeDiv"  style="display: none;">
                        <table class="table table-info mb30 table-hover"  id="addContainer" style="width:100%;" >
                            <thead >
                                <tr id="tableDesingTH" >
                                    <th   ><font color='red'>*</font><span id="consName" style="display: none"> Consol Name</span><span id="vehType" style="display: none"> Vehicle Type</span></th>
                                    <!--                                <th   ><font color='red'>*</font>Vehicle Type</th>-->
                                    <th   ><font color='red'>*</font>Container Size </th>
                                    <th   ><font color='red'>*</font>Category </th>
                                    <th   ><font color='red'>*</font>Liner </th>
                                    <th   ><font color='red'>*</font>Quantity</th>
                                    <th   >Goods Desc</th>
                                    <th   >Tonnage</th>
                                </tr>
                            </thead>
                            <tr>
                                <td colspan="5" align="left">
                            <center>
                                <input type="button" class="btn btn-info" value="Add Row" name="save" onClick="addRow2()" >
                                <input type="button" class="btn btn-info" value="Reset Container" name="resetContainer" onClick="deleteRowContainer();">
                            </center>
                            </td>
                            </tr>
                        </table>
                    </div>    
                    <div id="addModesDiv"  style="display: none;">
                        <table class="table table-info mb30 table-hover"  id="addModes" style="width:100%;" >
                            <tr >
                                <td class="form-control">PickUp Mode</td>
                                <td height="20" >
                                    <select name="pickupMode" class="form-control" id="pickupMode"  >
                                        <option value="">-select-</option>
                                        <option  value="1">Door Collection</option>
                                        <option  value="2">GodOwn Collection</option>
                                    </select>
                                </td>
                                <td class="form-control">Delivery Mode</td>
                                <td height="20" >
                                    <select name="deliveryMode" class="form-control" id="deliveryMode"  >
                                        <option value="">-select-</option>
                                        <option  value="1">Door Delivery</option>
                                        <option  value="2">Godown Delivery</option>


                                    </select>
                                </td>
                            </tr>

                        </table>
                    </div>
                    <div id="addContainersDiv"  style="display: none;">
                        <table class="table table-info mb30 table-hover"  id="addContainer" style="width:100%;" id="generateContainer" >
                            <thead >
                                <tr  >
                                    <th align="left" >Veh Type</th>                                
                                    <th align="left" >Con Type </th>
                                    <th align="left" >Container No</th>
                                    <th align="left" >&emsp;Liner Name</th>
                                    <th align="center">&nbsp;Goods Desc</th>
                                    <!--<th align="right" >&emsp;&emsp;Pkgs</th>-->
                                    <th align="right" >&nbsp;Wgt(Tonnage)</th>
                                    <!--<th align="right" >Volume(CBM)</th>-->
                                    <th align="center" >
                                        <!--Cust.Ref.No-->
                                        Booking No / BOE
                                    </th>
                                </tr>
                            </thead>
                            <tr >   <td colspan="7">                                    
                                    <table class="table table-info mb30 table-hover" style="width:100%;" id="generateContainer1" >    
                                    </table></td>
                            </tr>
                        </table>
                    </div>

                    <table class="table table-info mb30 table-hover"  id="addContainer" style="width:100%;"  >
                        <input type="hidden" name="vehTypeId" id="vehTypeId" value="0" />

                        <tr id="vehicleTypeContractDiv" style="display:none;">
                            <td>
                                Vehicle Type &nbsp;&nbsp;

                            </td>
                            <td  colspan="5" > <select name="vehTypeIdContractTemp" id="vehTypeIdContractTemp"  onchange="getFrieght();" class="form-control"  style="width:120px;">
                                    <option value="0" selected>--Select--</option>
                                </select></td>
                        </tr>
                        <input type="hidden" readonly  name="reeferRequired" id="reeferRequired"  class="form-control"  value="" />
                        <script>
                            function setDistanceContractId(val) {
                                var temp = val.split("~");
                                document.getElementById("distanceContractId").value = temp[0];
                            }
                            function setRouteDetails() {
                                $("#routeChart").show();
                                var temp = document.getElementById('destinationTemp').value;
                                // alert(temp);
                                var destinationSelect = document.getElementById('destinationTemp');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                var originSelect = document.getElementById('originTemp');
                                var originName = originSelect.options[originSelect.selectedIndex].text;
                                var tempVal = temp.split('-');
                                document.cNote.destination.value = tempVal[1];
                                //alert(document.cNote.destination.value);
                                document.cNote.routeId.value = tempVal[0];
                                document.cNote.routeBased.value = 'Y';
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if (podRowCount1 > 1) {
                                    for (var x = 1; x < podRowCount1; x++) {
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                //alert(document.cNote.destinationTemp.value);
                                if ((document.cNote.destinationTemp.value != '0') && (document.cNote.destinationTemp.value != '')) {
                                    //alert("am here...");
                                    var startDate = document.cNote.startDate.value;
                                    if (document.getElementById("orderType").value == "2") {
                                        addRouteCourse1(document.cNote.origin.value, originName, '1', 'PickUp', 0, 0, 0, startDate);
                                        // addRouteCourseNew(document.cNote.origin.value,originName,'2','Origin Hub',0,0,0,startDate);
                                        //addRouteCourseNew(document.cNote.origin.value,originName,'3','Destination Hub',0,0,0,startDate);
                                        addRouteCourse1(document.cNote.destination.value, destinationName, '2', 'Drop', tempVal[0], tempVal[1], tempVal[2], '');
                                    } else {

                                        addRouteCourse1(document.cNote.origin.value, originName, '1', 'PickUp', 0, 0, 0, startDate);
                                        addRouteCourse1(document.cNote.destination.value, destinationName, '2', 'Drop', tempVal[0], tempVal[1], tempVal[2], '');
                                    }
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                } else {
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }
                            }
                            // For Import
                            function setContractRouteDetails() {

                                $("#routeChart").show();
                                var origin = document.getElementById('contractRouteOrigin').value;
                                var destination = document.getElementById('contractRouteDestination').value;
                                //alert(origin + " ::: " + destination);
                                var destinationSelect = document.getElementById('contractRouteDestination');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                var originSelect = document.getElementById('contractRouteOrigin');
                                var originName = originSelect.options[originSelect.selectedIndex].text;
                                document.cNote.origin.value = origin;
                                document.cNote.destination.value = destination;
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if (podRowCount1 > 1) {
                                    for (var x = 1; x < podRowCount1; x++) {
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                if (document.cNote.origin.value != '0' && document.cNote.destination.value != '0') {
                                    //alert("am here...");
                                    //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                    var startDate = document.cNote.startDate.value;
                                    addRouteCourse3(document.cNote.origin.value, originName, '1', 'PickUp', 0, 0, 0, startDate);
                                    addRouteCourse3(document.cNote.destination.value, destinationName, '2', 'Drop', 0, 0, 0, '');
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                } else {
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }


                            }
                            // For Repo
                            function setContractRouteDetailsRepo() {
                                deleteRowContainer();
                                $("#routeChart").show();
                                var origin = document.getElementById('contractRouteOrigin1').value;
                                var destination = document.getElementById('contractRouteDestination1').value;
                                //alert(origin + " ::: " + destination);
                                var destinationSelect = document.getElementById('contractRouteDestination1');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                var originSelect = document.getElementById('contractRouteOrigin1');
                                var originName = originSelect.options[originSelect.selectedIndex].text;
                                document.cNote.origin.value = origin;
                                document.cNote.destination.value = destination;
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if (podRowCount1 > 1) {
                                    for (var x = 1; x < podRowCount1; x++) {
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                if (document.cNote.origin.value != '0' && document.cNote.destination.value != '0') {
                                    //alert("am here...");
                                    //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                    var startDate = document.cNote.startDate.value;
                                    addRouteCourse3(document.cNote.origin.value, originName, '1', 'PickUp', 0, 0, 0, startDate);
                                    var tripType = document.cNote.tripType.value;
                                    if (tripType == 1) {
                                        addRouteCourse3(document.cNote.destination.value, destinationName, '2', 'Drop', 0, 0, 0, '');
                                    }
                                    if (tripType == 2) {
                                        addRouteCourse3(document.cNote.destination.value, destinationName, '2', 'Loading', 0, 0, 0, '');
                                        addRouteCourse3(document.cNote.origin.value, originName, '3', 'Drop', 0, 0, 0, '');
                                    }
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                } else {
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }


                            }
                            function setContractRouteDetailsNew() {
                                deleteRowContainer();
                                $("#routeChart").show();
                                var origin = document.getElementById('contractRouteOrigin').value;
                                var destination = document.getElementById('contractRouteDestination').value;
                                var emptyContainerdestination = document.getElementById('destinationTemp').value;
                                //alert(origin + " ::: " + destination);
                                var destinationSelect = document.getElementById('contractRouteDestination');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                var originSelect = document.getElementById('contractRouteOrigin');
                                var originName = originSelect.options[originSelect.selectedIndex].text;
                                var emptyContainerdestinationSelect = document.getElementById('destinationTemp');
                                var emptyContainerdestinationName = emptyContainerdestinationSelect.options[emptyContainerdestinationSelect.selectedIndex].text;
                                document.cNote.origin.value = origin;
                                document.cNote.contractRouteDestination.value = destination;
                                document.cNote.destinationTemp.value = emptyContainerdestination;
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if (podRowCount1 > 1) {
                                    for (var x = 1; x < podRowCount1; x++) {
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                if (document.cNote.origin.value != '0' && document.cNote.destination.value != '0') {
                                    //alert("am here...");
                                    //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                    var startDate = document.cNote.startDate.value;
                                    addRouteCourse3(document.cNote.origin.value, originName, '1', 'PickUp', 0, 0, 0, startDate);
                                    addRouteCourse3(document.cNote.contractRouteDestination.value, destinationName, '2', 'Unloading', 0, 0, 0, '');
                                    addRouteCourse3(document.cNote.destinationTemp.value, emptyContainerdestinationName, '3', 'Drop', 0, 0, 0, '');
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                } else {
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }
                                currentTime();

                            }
                            //For Export
                            function setContractRouteDetailsExp() {

                                $("#routeChart").show();
                                //alert(origin + " ::: " + destination);
                                var destinationSelect = document.getElementById('expcontractRouteDestination');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                var originSelect = document.getElementById('expcontractRouteOrigin');
                                var originName = originSelect.options[originSelect.selectedIndex].text;
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if (podRowCount1 > 1) {
                                    for (var x = 1; x < podRowCount1; x++) {
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                if (document.cNote.expcontractRouteOrigin.value != '0' && document.cNote.expcontractRouteDestination.value != '0') {
                                    //alert("am here...");
                                    //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                    var startDate = document.cNote.startDate.value;
                                    addRouteCourse3(document.cNote.origin.value, originName, '1', 'PickUp', 0, 0, 0, startDate);
                                    addRouteCourse3(document.cNote.destination.value, destinationName, '2', 'Drop', 0, 0, 0, '');
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                } else {
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }


                            }

                            function setContractRouteDetailsExpOtherICD() {

                                $("#routeChart").show();
                                //alert(origin + " ::: " + destination);
                                var destinationSelect = document.getElementById('expcontractRouteDestination');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                var originSelect = document.getElementById('expcontractRouteOrigin');
                                var originName = originSelect.options[originSelect.selectedIndex].text;
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if (podRowCount1 > 1) {
                                    for (var x = 1; x < podRowCount1; x++) {
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                if (document.cNote.expcontractRouteOrigin.value != '0' && document.cNote.expcontractRouteDestination.value != '0') {
                                    //alert("am here...");
                                    //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                    var startDate = document.cNote.startDate.value;
                                    addRouteCourse3(617, 'DICT Sonepat', '1', 'StartPoint', 0, 0, 0, startDate);
                                    addRouteCourse3(document.cNote.origin.value, originName, '2', 'PickUp', 0, 0, 0, '');
                                    addRouteCourse3(document.cNote.destination.value, destinationName, '3', 'Drop', 0, 0, 0, '');
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                } else {
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }


                            }

                            function setContractRouteDetailsNewExp() {
                                deleteRowContainer();
                                var emptyContainerLocation = $("#emptyContainerLocation").val();
                                if (emptyContainerLocation == 1) {
                                    $("#routeChart").show();
                                    var origin = document.getElementById('expcontractRouteOrigin').value;
                                    var destination = document.getElementById('expcontractRouteDestination').value;
                                    var emptyContainerdestination = document.getElementById('expdestinationTemp').value;
                                    // alert(origin + " ::: " + destination+"::"+emptyContainerdestination);
                                    var destinationSelect = document.getElementById('expcontractRouteDestination');
                                    var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                    var originSelect = document.getElementById('expcontractRouteOrigin');
                                    var originName = originSelect.options[originSelect.selectedIndex].text;
                                    var emptyContainerdestinationSelect = document.getElementById('expdestinationTemp');
                                    var emptyContainerdestinationName = emptyContainerdestinationSelect.options[emptyContainerdestinationSelect.selectedIndex].text;
                                    document.cNote.origin.value = origin;
                                    document.cNote.destination.value = destination;
                                    document.cNote.expdestinationTemp.value = emptyContainerdestination;
                                    //remove table rows and reset values
                                    var tab = document.getElementById("routePlan");
                                    //alert(tab.rows.length);
                                    //alert(podRowCount1);
                                    if (podRowCount1 > 1) {
                                        for (var x = 1; x < podRowCount1; x++) {
                                            document.getElementById("routePlan").deleteRow(1);
                                        }
                                    }
                                    podRowCount1 = 1;
                                    podSno1 = 0;
                                    if (document.cNote.expcontractRouteOrigin.value != '0' && document.cNote.expcontractRouteDestination.value != '0') {
                                        //alert("am here...");
                                        //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                        var startDate = document.cNote.startDate.value;
                                        addRouteCourse3(document.cNote.expcontractRouteOrigin.value, originName, '1', 'PickUp', 0, 0, 0, startDate);
                                        addRouteCourse3(document.cNote.expcontractRouteDestination.value, destinationName, '2', 'Loading', 0, 0, 0, '');
                                        addRouteCourse3(document.cNote.expdestinationTemp.value, emptyContainerdestinationName, '3', 'Drop', 0, 0, 0, '');
                                        $("#routePlanAddRow").show();
                                        $("#freezeRoute").show();
                                        $("#resetRoute").show();
                                    } else {
                                        $("#routePlanAddRow").hide();
                                        $("#freezeRoute").hide();
                                        $("#resetRoute").hide();
                                    }
                                } else if (emptyContainerLocation == 2) {
                                    $("#routeChart").show();
                                    var origin = document.getElementById('expcontractRouteOrigin').value;
                                    var destination = document.getElementById('expcontractRouteDestination').value;
                                    var emptyContainerdestination = document.getElementById('expdestinationTemp').value;
                                    //alert(origin + " ::: " + destination);
                                    var destinationSelect = document.getElementById('expcontractRouteDestination');
                                    var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;
                                    var originSelect = document.getElementById('expcontractRouteOrigin');
                                    var originName = originSelect.options[originSelect.selectedIndex].text;
                                    var emptyContainerdestinationSelect = document.getElementById('expdestinationTemp');
                                    var emptyContainerdestinationName = originSelect.options[emptyContainerdestinationSelect.selectedIndex].text;
                                    var finalDestinationPoint = $("#expdestinationTemp option:selected").text();
                                    document.cNote.origin.value = origin;
                                    document.cNote.destination.value = destination;
                                    document.cNote.expdestinationTemp.value = emptyContainerdestination;
                                    //remove table rows and reset values
                                    var tab = document.getElementById("routePlan");
                                    //alert(tab.rows.length);
                                    //alert(podRowCount1);
                                    if (podRowCount1 > 1) {
                                        for (var x = 1; x < podRowCount1; x++) {
                                            document.getElementById("routePlan").deleteRow(1);
                                        }
                                    }
                                    podRowCount1 = 1;
                                    podSno1 = 0;
                                    if (document.cNote.expcontractRouteOrigin.value != '0' && document.cNote.expcontractRouteDestination.value != '0') {
                                        //alert("am here...");
                                        //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                        var startDate = document.cNote.startDate.value;
                                        addRouteCourse3(617, 'DICT Sonepat', '1', 'StartPoint', 0, 0, 0, startDate);
                                        addRouteCourse3(document.cNote.expcontractRouteOrigin.value, originName, '2', 'PickUp', 0, 0, 0, '');
                                        addRouteCourse3(document.cNote.expcontractRouteDestination.value, destinationName, '3', 'Loading', 0, 0, 0, '');
                                        addRouteCourse3(document.cNote.expdestinationTemp.value, finalDestinationPoint, '4', 'Drop', 0, 0, 0, '');
                                        $("#routePlanAddRow").show();
                                        $("#freezeRoute").show();
                                        $("#resetRoute").show();
                                    } else {
                                        $("#routePlanAddRow").hide();
                                        $("#freezeRoute").hide();
                                        $("#resetRoute").hide();
                                    }

                                }
                                currentTime();
                            }

                            function setFreightRate() {
                                var temp = document.getElementById("vehTypeIdTemp").value;
                                var tempVal = temp.split("-");
                                document.cNote.vehTypeId.value = tempVal[0];
                                document.cNote.vehicleTypeId.value = tempVal[0];
                                //alert(document.cNote.vehTypeId.value);
                                //alert(document.cNote.vehicleTypeId.value);

                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;
                                estimateFreight();
                            }
                            function setFreightRate1() {
                                var temp = document.getElementById("vehTypeIdContractTemp").value;
                                // var tempVal = temp.split("-");
                                document.cNote.vehTypeId.value = temp;
                                document.cNote.vehicleTypeId.value = temp;
                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;
                                estimateFreight();
                            }

                            function calculateFreightTotal() {
                                var reeferRequired = document.getElementById('reeferRequired').value;
                                var billingTypeId = document.getElementById('billingTypeId').value;
                                var rateWithReefer = document.getElementById('rateWithReefer').value;
                                var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
                                var totalKm = document.getElementById('totalKm').value;
                                var totalWeight = $("#totalWeight").text();
                                var totalAmount = 0;
                                if (reeferRequired == 'Yes' && billingTypeId == 1) {
                                    $('#freightAmount').text(rateWithReefer)
                                    $('#totFreightAmount').val(rateWithReefer)
                                    $('#totalCharges').val(rateWithReefer)
                                } else if (reeferRequired == 'No' && billingTypeId == 1) {
                                    $('#freightAmount').text(rateWithoutReefer)
                                    $('#totFreightAmount').val(rateWithoutReefer)
                                    $('#totalCharges').val(rateWithoutReefer)
                                } else if (reeferRequired == 'Yes' && billingTypeId == 2) {
                                    totalAmount = totalWeight * rateWithReefer;
                                    alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'No' && billingTypeId == 2) {
                                    totalAmount = totalWeight * rateWithReefer;
                                    alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'Yes' && billingTypeId == 3) {
                                    totalAmount = totalKm * rateWithReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'No' && billingTypeId == 3) {
                                    totalAmount = totalKm * rateWithoutReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                }
                            }
                        </script>
                        <tr>
                        <input type="hidden" class="form-control" name="destinationId" id="destinationId" >
                        <input type="hidden" class="form-control" name="routeContractId" id="routeContractId" >
                        <input type="hidden" class="form-control" name="routeId" id="routeId" >
                        <input type="hidden" class="form-control" name="routeBased" id="routeBased" >
                        <input type="hidden" class="form-control" name="contractRateId" id="contractRateId" >
                        <input type="hidden" class="form-control" name="totalKm" id="totalKm" >
                        <input type="hidden" class="form-control" name="totalHours" id="totalHours" >
                        <input type="hidden" class="form-control" name="totalMinutes" id="totalMinutes" >
                        <input type="hidden" class="form-control" name="totalPoints" id="totalPoints" >
                        <input type="hidden" class="form-control" name="vehicleTypeId" id="vehicleTypeId" >
                        <input type="hidden" readonly class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" >

                        <input type="hidden"   name="vehicleRequiredHour" id="vehicleRequiredHour" >
                        <input type="hidden" name="vehicleRequiredMinute" id="vehicleRequiredMinute" >
                        <input type="hidden" name="containerTypeIds" id="containerTypeIds"value="" />
                        <input type="hidden" name="vehTypeIdTemp1" id="vehTypeIdTemp1"value="" />
                        <input type="hidden" name="containerQtys" id="containerQtys"value="" />
                        <input type="hidden" name="containerRow" id="containerRow"value="" />
                        <input type="hidden" class="form-control" name="rateWithReefer" id="rateWithReefer" >
                        <input type="hidden" class="form-control" name="rateWithoutReefer" id="rateWithoutReefer" >
                    </table>





                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" style="display:none;">

                        <tr>
                            <td class="contentsub"   colspan="6">Consignor Details</td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Consignor Name</td>
                        <input type="hidden" name="consignorId" id="consignorId" value=""/>
                        <td><input type="text" class="form-control" id="consignorName" onKeyPress="return onKeyPressBlockNumbers(event);" name="consignorName" ></td>
                        <td><font color="red">*</font>Mobile No</td>
                        <td><input type="text" class="form-control" id="consignorPhoneNo" maxlength="12" onKeyPress="return onKeyPressBlockCharacters(event);"  name="consignorPhoneNo" ></td>
                        <td><font color="red">*</font>Address</td>
                        <td><textarea rows="1" cols="16" name="consignorAddress" id="consignorAddress"></textarea> </td>
                        </tr>
                    </table>

                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg" style="display:none;">

                        <tr>
                            <td class="contentsub"   colspan="6">Consignee Details</td>
                        </tr>
                        <tr>
                            <td><font color="red">*</font>Consignee Name temp</td>
                        <input type="hidden" name="consigneeId" id="consigneeId" value=""/>
                        <td><input type="text" class="form-control" id="consigneeName" onKeyPress="return onKeyPressBlockNumbers(event);"  name="consigneeName" ></td>
                        <td><font color="red">*</font>Mobile No</td>
                        <td><input type="text" class="form-control" id="consigneePhoneNo" maxlength="12"  onKeyPress="return onKeyPressBlockCharacters(event);"   name="consigneePhoneNo" ></td>
                        <td><font color="red">*</font>Address</td>
                        <td><textarea rows="1" cols="16" name="consigneeAddress" id="consigneeAddress"></textarea> </td>
                        </tr>

                    </table>







                    <script>
                        $(".nexttab").click(function () {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                        $(".previoustab").click(function () {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected - 1);
                        });
                    </script>
                    <table   id="creditLimitTable" style="display:none;">
                        <tr>
                            <td style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"   colspan="6" >Credit Limit Details</td>
                        </tr>
                        <tr>
                            <td>Credit Days &nbsp; </td>
                            <td><label id="creditDaysTemp"></label><input type="hidden" name="creditDays" id="creditDays" /> </td>
                            <td align="left" >Credit Limit &nbsp; </td>
                            <td><label id="creditLimitTemp"></label><input type="hidden" name="creditLimit" id="creditLimit" /></td>
                        </tr>
                        <tr>
                            <td align="left" >Customer Rank &nbsp; </td>
                            <td><label id="customerRankTemp"></label><input type="hidden" name="customerRank" id="customerRank" /></td>
                            <td align="left" >Approval Status&nbsp; </td>
                            <td><label id="approvalStatusTemp"></label><input type="hidden" name="approvalStatus" id="approvalStatus" /></td>
                        </tr>
                        <tr>
                            <td align="left" >Out Standing &nbsp; </td>
                            <td><label id="outStandingTemp"></label><input type="hidden" name="outStanding" id="outStanding" /></td>
                            <td align="left" >Out Standing Date&nbsp; </td>
                            <td><label id="outStandingDateTemp"></label><input type="hidden" name="outStandingDate" id="outStandingDate" /></td>
                        </tr>

                    </table>
                    <table class="table table-info mb30 table-hover" style="width:100%" >


                        <tr>
                            <td colspan="2">Remarks
                            </td>
                            <td colspan="2"><textarea rows="1" cols="16" name="vehicleInstruction" id="vehicleInstruction" class="form-control" ></textarea></td>
                            <td><input type="hidden" name="userId" id="userId" value="<c:out value="${userId}"/>" /></td>
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <td></td> 
                            <!--<td colspan="2">Description</td>-->
                            <!--<td>Customer Reference  Remarks</td>-->
                            <!--<td colspan="2">-->
                            <!--<textarea name="orderReferenceRemarks" id="orderReferenceRemarks" maxlength="100" class="form-control" cols="16" rows="1"  ></textarea>-->
                        <input type="hidden" name="orderReferenceRemarks" id="orderReferenceRemarks" value="<c:out value="${userId}"/>" /></td>
                        <!--</td>-->



                        <td id="tdFreightAmountLabel" align="right"  style="display:none;"> 


                            &emsp;  Estimated Freight Charges &nbsp; </td>


                        <td id="tdFreightAmount" align="left" style="display:none"> 
                            <input type="HIDDEN" name="baseRate" id="baseRate" value="">
                            INR.
                            <input type="text" readonly  class="textbox" style="width:120px;height:40px;" onKeyPress='return onKeyPressBlockCharacters(event);' name="totFreightAmount" id="totFreightAmount" value=""/>
                            &nbsp;
                        </td>
                        <td id="tdFreightAmountCheckBox"  style="display:none;">
                            <font color='red'>*</font> Agree &nbsp;&nbsp;
                            <input type="checkbox" checked="checked" name="freightAcceptedStatus" id="freightAcceptedStatus" onclick="checkFreightAcceptedStatus();"  />
                        </td>
                        </tr>
                    </table>

                    <input type="hidden" class="form-control" id="subTotal" name="subTotal"  value="0" readonly="">
                    <script type="text/javascript">
                        function calculateSubTotal1() {
                            var customerTypeId = document.getElementById("customerTypeId").value;
                            if (customerTypeId == 2) {
                                var fixedRateWithReefer = $("#walkinFreightWithReefer").val();
                                var fixedRateWithoutReefer = $("#walkinFreightWithoutReefer").val();
                                var rateWithReeferPerKm = $("#walkinRateWithReeferPerKm").val();
                                var rateWithoutReeferPerKm = $("#walkinRateWithoutReeferPerKm").val();
                                var rateWithReeferPerKg = $("#walkinRateWithReeferPerKg").val();
                                var rateWithoutReeferPerKg = $("#walkinRateWithoutReeferPerKg").val();
                            }
                            var freightAmount = $('#freightAmount').text();
                            var docCharges = document.getElementById('docCharges').value;
                            var odaCharges = document.getElementById('odaCharges').value;
                            var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                            var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                            var handleCharges = document.getElementById('handleCharges').value;
                            var otherCharges = document.getElementById('otherCharges').value;
                            var unloadingCharges = document.getElementById('unloadingCharges').value;
                            var loadingCharges = document.getElementById('loadingCharges').value;
                            var total = 0;
                            var walkInTotal = 0;
                            var kmRate = 0;
                            var kgRate = 0;
                            if (customerTypeId == 2) {
                                var billingTypeId = $("#walkInBillingTypeId").val();
                                var totalKm = $("#totalKm").val();
                                var totalWeight = $("#totalWeight").text();
                                if (fixedRateWithReefer != '' && billingTypeId == 1) {
                                    walkInTotal += parseInt(fixedRateWithReefer);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (fixedRateWithoutReefer != '' && billingTypeId == 1) {
                                    walkInTotal += parseInt(fixedRateWithoutReefer);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithReeferPerKm != '' && billingTypeId == 3) {
                                    kmRate = parseInt(totalKm) * parseInt(rateWithReeferPerKm);
                                    walkInTotal += parseInt(kmRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithoutReeferPerKm != '' && billingTypeId == 3) {
                                    kmRate = parseInt(totalKm) * parseInt(rateWithoutReeferPerKm);
                                    walkInTotal += parseInt(kmRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithReeferPerKg != '' && billingTypeId == 2) {
                                    kgRate = parseInt(totalWeight) * parseInt(rateWithReeferPerKg);
                                    walkInTotal += parseInt(kgRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithoutReeferPerKg != '' && billingTypeId == 2) {
                                    kgRate = parseInt(totalWeight) * parseInt(rateWithoutReeferPerKg);
                                    walkInTotal += parseInt(kgRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                            }
                            if (docCharges != '') {
                                total += parseInt(docCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (odaCharges != '') {
                                total += parseInt(odaCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiPickupCharge != '') {
                                total += parseInt(multiPickupCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiDeliveryCharge != '') {
                                total += parseInt(multiDeliveryCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (handleCharges != '') {
                                total += parseInt(handleCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (otherCharges != '') {
                                total += parseInt(otherCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (unloadingCharges != '') {
                                total += parseInt(unloadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (loadingCharges != '') {
                                total += parseInt(loadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            document.getElementById('subTotal').value = total;
                            if (customerTypeId == 1) {
                                if (freightAmount != '') {
                                    total += parseInt(freightAmount);
                                } else {
                                    total += parseInt(0);
                                }
                            } else if (customerTypeId == 2) {
                                total += parseInt(walkInTotal);
                            }
                            document.getElementById('totalCharges').value = total;
                        }
                        function calculateSubTotal() {
                            estimateFreight();
                            var freightAmount = document.cNote.totFreightAmount.value;
                            var docCharges = document.getElementById('docCharges').value;
                            var odaCharges = document.getElementById('odaCharges').value;
                            var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                            var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                            var handleCharges = document.getElementById('handleCharges').value;
                            var otherCharges = document.getElementById('otherCharges').value;
                            var unloadingCharges = document.getElementById('unloadingCharges').value;
                            var loadingCharges = document.getElementById('loadingCharges').value;
                            var total = 0;
                            var walkInTotal = 0;
                            var kmRate = 0;
                            var kgRate = 0;
                            if (docCharges != '') {
                                total += parseInt(docCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (odaCharges != '') {
                                total += parseInt(odaCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiPickupCharge != '') {
                                total += parseInt(multiPickupCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiDeliveryCharge != '') {
                                total += parseInt(multiDeliveryCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (handleCharges != '') {
                                total += parseInt(handleCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (otherCharges != '') {
                                total += parseInt(otherCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (unloadingCharges != '') {
                                total += parseInt(unloadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (loadingCharges != '') {
                                total += parseInt(loadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            document.getElementById('subTotal').value = total.toFixed(2);
                            total = total + parseFloat(freightAmount);
                            document.getElementById('totalCharges').value = total.toFixed(2);
                        }
                    </script>
                    <input align="right" value="" type="hidden" readonly class="form-control" id="totalCharges" name="totalCharges" >
                    <br/>
                    <center>
                        <!--                        <input type="button" class="button" name="Save" value="Estimate Freight" onclick="estimateFreight();" >-->
                        <div id="orderButton" >
                            <input type="button" class="btn btn-info" name="Save" value="Create Order" id="createOrder" onclick="submitPage(this.value);" >
                        </div>
                    </center>
                    <!--                </div>-->
                    <!--- pop up div -->
                    <div class="modal fade" id="myModal" role="dialog" style="width: 100%;height: 100%">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()">&times;</button>
                                    <center><h4 class="modal-title" id="titleHead">Customer  Address Details</h4></center>
                                </div>
                                <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" id="bg" class="border">
                                    <tr align="center">
                                        <td colspan="4" align="center" class="contenthead" ><div class="contenthead">Select Customer Address</div></td>
                                    </tr>
                                    <c:forEach items="${ConsignorAddressDetails}" var="Address">
                                        <tr>
                                            <td width="350" >&nbsp;&nbsp;&nbsp;<font color="red">*</font>Customer Name</td>
                                            <td ><input name="custName" id="custName" type="text" class="form-control" value="<c:out value="${Address.customerName}"/>" readonly maxlength="50"></td>
                                        </tr>

                                        <tr>
                                            <td ><font color="red">*</font>Customer Address</td>
                                            <td >
                                                <textarea name="custAddress" id="custAddress" readonly ><c:out value="${Address.custAddress}"/></textarea>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="entryType1"  id="entryType1" value="1" >
                                            </td>
                                        </tr>

                                        <tr>
                                            <td >&nbsp;Customer Address 2</td>
                                            <td  >
                                                <textarea name="custAddresstwo" id="custAddresstwo" readonly ><c:out value="${Address.custAddresstwo}"/></textarea>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="entryType2"  id="entryType2" value="2"  >
                                            </td>
                                        </tr>

                                        <tr>
                                            <td >&nbsp;Customer Address 3</td>
                                            <td >
                                                <textarea name="custAddressthree" id="custAddressthree" readonly ><c:out value="${Address.custAddressthree}"/></textarea>
                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="entryType3"  id="entryType3" value="3" >
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </div>
                        </div>

                        <script>
                            setProductCategoryValues();
                        </script>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

                <div id="loader"></div>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
