<%-- 
    Document   : VehicleWeeklyProductivityReport
    Created on : sep 13, 2020, 01:31:16 PM
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="profit" action=""  method="post">
            <%
                       Date dNow = new Date();
                       SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                       //System.out.println("Current Date: " + ft.format(dNow));
                       String curDate = ft.format(dNow);
                       String expFile = "VehicleWeeklyProductivity-" + curDate + ".xls";

                       String fileName = "attachment;filename=" + expFile;
                       response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                       response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>

            <c:if test="${resultList != null}">
                <table class="table table-info mb30 table-hover" id="table" class="sortable" border="1" >
                    <thead>
                        <tr>
                            <th rowspan="2" align="center">Week</th>
                            <th rowspan="2" align="center">WeekDateRange</th>
                            <th rowspan="2" align="center">NoOfDays</th>
                            <th colspan="2" align="center">40'TripleAxle</th>
                            <th colspan="2" align="center">40'DoubleAxle</th>
                            <!--<th colspan="2" align="center">20'DoubleAxle</th>-->
                            <th rowspan="2" align="center">TotalMofussil</th>
                            <th rowspan="2" align="center">TotalLocal</th>
                            <th rowspan="2" align="center">Total Movements</th>
                            <th rowspan="2" align="center">TotalMoff Movements</th>
                            <th rowspan="2" align="center">Ave.Per.Vehicle</th>
                            <th rowspan="2" align="center">Ave.Pro.Per Vehicle/Month</th>

                        </tr> 
                        <tr>
                            <th align="center">Mof</th>
                            <th align="center">Loc</th>
                            <th align="center">Mof</th>
                            <th align="center">Loc</th>
<!--                            <th align="center">Mof</th>
                            <th align="center">Loc</th>-->

                        </tr> 
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:set var="days" value="0"/>
                        <c:set var="mof403" value="0"/>
                        <c:set var="loc403" value="0"/>
                        <c:set var="mof402" value="0"/>
                        <c:set var="loc402" value="0"/>
                        <c:set var="mof202" value="0"/>
                        <c:set var="loc202" value="0"/>
                        <c:set var="totMof" value="0"/>
                        <c:set var="totLoc" value="0"/>
                        <c:set var="nettTotal" value="0"/>
                        <c:set var="nettMof" value="0"/>
                        <c:set var="nettAve1" value="0"/>
                        <c:set var="nettAve2" value="0"/>

                        <c:forEach items="${resultList}" var="utilList">
                            <c:set var="days" value="${days + utilList.noOfDays}"/>
                            <c:set var="mof403" value="${mof403 + utilList.fortyTripleAxleMof}"/>
                            <c:set var="loc403" value="${loc403 + utilList.fortyTripleAxleLoc}"/>
                            <c:set var="mof402" value="${mof402 + utilList.fortyDoubleAxleMof}"/>
                            <c:set var="loc402" value="${loc402 + utilList.fortyDoubleAxleLoc}"/>
                            <c:set var="mof202" value="${mof202 + utilList.twentyDoubleAxleMof}"/>
                            <c:set var="loc202" value="${loc202 + utilList.twentyDoubleAxleLoc}"/>

                            <c:set var="totMof" value="${totMof + utilList.totalMof}"/>
                            <c:set var="totLoc" value="${totLoc + utilList.totalLoc}"/>
                            <c:set var="nettTotal" value="${nettTotal + utilList.nettTotal}"/>
                            <c:set var="nettMof" value="${nettMof + utilList.nettMof}"/>
                            <c:set var="nettAve1" value="${nettAve1 + utilList.avePerVehicleWeek}"/>
                            <c:set var="nettAve2" value="${nettAve2 + utilList.avePerVehicleMonth}"/>

                            <tr>
                                <td  align="center"    >
                                    <a  href="#" onclick="viewDayWiseDetails('<c:out value="${utilList.weekId}"/>');">
                                        <c:out value="${utilList.weekName}"/>
                                    </a>

                                </td>
                                <td  align="center"    >
                                    <a  href="#" onclick="viewDayWiseDetails('<c:out value="${utilList.weekId}"/>');">
                                        <c:out value="${utilList.dateRange}"/>
                                    </a>
                                </td>
                                <td  align="center"    ><c:out value="${utilList.noOfDays}"/></td>

                                <td  align="center"    >
                                    <a href="#" onclick="viewCustomerProfitDetails(1060, 1, ' <c:out value="${utilList.dateRange}"/>', '<c:out value="${companyId}"/>');"><font color="#050404">
                                            <c:out value="${utilList.fortyTripleAxleMof}"/>
                                        </font></a>
                                </td>
                                <td  align="center"    >
                                    <a href="#" onclick="viewCustomerProfitDetails(1060, 0, ' <c:out value="${utilList.dateRange}"/>', '<c:out value="${companyId}"/>');"><font color="#050404">
                                            <c:out value="${utilList.fortyTripleAxleLoc}"/>
                                        </font></a>
                                </td>
                                <td  align="center"    >
                                    <a href="#" onclick="viewCustomerProfitDetails(1059, 1, ' <c:out value="${utilList.dateRange}"/>', '<c:out value="${companyId}"/>');"><font color="#050404">
                                            <c:out value="${utilList.fortyDoubleAxleMof}"/>
                                        </font></a>
                                </td>
                                <td  align="center"    >
                                    <a href="#" onclick="viewCustomerProfitDetails(1059, 0, ' <c:out value="${utilList.dateRange}"/>', '<c:out value="${companyId}"/>');"><font color="#050404">
                                            <c:out value="${utilList.fortyDoubleAxleLoc}"/>
                                        </font></a>
                                </td>
<%--                                <td  align="center"    >
                                    <a href="#" onclick="viewCustomerProfitDetails(1058, 1, ' <c:out value="${utilList.dateRange}"/>', '<c:out value="${companyId}"/>');"><font color="#050404">
                                            <c:out value="${utilList.twentyDoubleAxleMof}"/>
                                        </font></a>
                                </td>
                                <td  align="center"    >
                                    <a href="#" onclick="viewCustomerProfitDetails(1058, 0, ' <c:out value="${utilList.dateRange}"/>', '<c:out value="${companyId}"/>');"><font color="#050404">
                                            <c:out value="${utilList.twentyDoubleAxleLoc}"/>
                                        </font></a>
                                </td>--%>

                                <td  align="center"    ><c:out value="${utilList.totalMof}"/></td>
                                <td  align="center"    ><c:out value="${utilList.totalLoc}"/></td>
                                <td  align="center"    ><c:out value="${utilList.nettTotal}"/></td>
                                <td  align="center"    ><c:out value="${utilList.nettMof}"/></td>
                                <td  align="center"  ><c:out value="${utilList.avePerVehicleWeek}"/></td>
                                <td  align="center"  ><c:out value="${utilList.avePerVehicleMonth}"/></td>
                            </tr>

                        </c:forEach>
                        <tr>
                            <td  align="center"    >&nbsp;</td>
                            <td  align="center"    ><font color="blue"><b>Total</b></font></td>
                            <td  align="center"    ><c:out value="${days}"/></td>

                            <td  align="center"    ><c:out value="${mof403}"/></td>
                            <td  align="center"    ><c:out value="${loc403}"/></td>
                            <td  align="center"    ><c:out value="${mof402}"/></td>
                            <td  align="center"    ><c:out value="${loc402}"/></td>
<%--                            <td  align="center"    ><c:out value="${mof202}"/></td>
                            <td  align="center"    ><c:out value="${loc202}"/></td>--%>
                            <td  align="center"    ><c:out value="${totMof}"/></td>
                            <td  align="center"    ><c:out value="${totLoc}"/></td>
                            <td  align="center"    ><c:out value="${totMof+totLoc}"/></td>
                            <td  align="center"    ><c:out value="${nettMof}"/></td>
                            <td  align="center"    >
                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${nettAve1}" />
                                <%--<c:out value="${nettAve1}"/>--%>
                            </td> 
                            <td  align="center"    >
                                <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${nettAve2}" />
                                <%--<c:out value="${nettAve2}"/>--%>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            
            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>