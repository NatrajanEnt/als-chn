<%-- 
    Document   : viewclosedtrip
    Created on : Dec 6, 2013, 4:14:16 PM
    Author     : srinientitle
--%>

<%@page import="ets.domain.billing.business.BillingTO"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
         <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });


            });

        </script>
        <script type="text/javascript">
               function setValues(){
             if('<%=request.getAttribute("type")%>' != 'null' ){
                document.getElementById('type').value= '<%=request.getAttribute("type")%>';
            }
              if('<%=request.getAttribute("shippingBillNo")%>' != 'null' ){
                document.getElementById('shippingBillNo').value= '<%=request.getAttribute("shippingBillNo")%>';
            }
            if('<%=request.getAttribute("billOfEntryNo")%>' != 'null' ){
                document.getElementById('billOfEntryNo').value= '<%=request.getAttribute("billOfEntryNo")%>';
            }
            if('<%=request.getAttribute("fromDate")%>' != 'null' ){
                document.getElementById('fromDate').value= '<%=request.getAttribute("fromDate")%>';
            }
            if('<%=request.getAttribute("toDate")%>' != 'null' ){
                document.getElementById('toDate').value= '<%=request.getAttribute("toDate")%>';
            }
            if('<%=request.getAttribute("customerName")%>' != 'null'){
                document.getElementById('customer').value='<%=request.getAttribute("customerName")%>';
            }
            if('<%=request.getAttribute("customerId")%>' != 'null'){
                document.getElementById('billingParty').value='<%=request.getAttribute("customerId")%>';
            }
               
            if('<%=request.getAttribute("movementType")%>' != 'null'){
                document.getElementById('movementType').value='<%=request.getAttribute("movementType")%>';
            }
            
        }

                function submitPage() {
                var customerId = document.getElementById("billingParty").value;
                document.enter.action = '/throttle/viewClosedRepoOrderForBilling.do';
                document.enter.submit();

            }
            function generateBill() {
                var temp="";
                var cntr = 0;
                var selectedConsignment = document.getElementsByName('consignmentId');
                  for (var i = 0; i < selectedConsignment.length; i++) {
               if (selectedConsignment[i].checked == true) {
                   if (cntr == 0) {
                       temp = selectedConsignment[i].value;
                   } else {
                       temp = temp + "," + selectedConsignment[i].value;
                   }
                   cntr++;
               }
        }
//                alert("temp"+temp);
                document.enter.action = '/throttle/gererateRepoOrderBill.do?tripIds='+temp;
                document.enter.submit();
            }
            function selectCheckCondition() {
                if (document.getElementById("select").checked = true) {
                    document.getElementById("select").value = "selected";
                } else {
                    document.getElementById("select").value = "";
                }



            }
            $(document).ready(function () {
                $('#Customer').autocomplete({
                    source: function (request, response) {
                        $.ajax({
                            url: "/throttle/getCustomerName.do",
                            dataType: "json",
                            data: {
                                customerCode: request.term,
                                customerName: document.getElementById('Customer').value
                            },
                            success: function (data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function (data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function (event, ui) {
                        $("#Customer").val(ui.item.customerName);
                        var $itemrow = $(this).closest('tr');
                        var value = ui.item.customerName;
                        var tmp = value.split('-');
                        $itemrow.find('#customerId').val(tmp[0]);
                        $itemrow.find('#Customer').val(tmp[1]);
                        $("#billingParty").val(tmp[0]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function (ul, item) {
                    var itemVal = item.customerName;

                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };


            });

            $(function () {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

            function printTripGrn(tripSheetId){
            window.open('/throttle/handleTripSheetPrint.do?tripSheetId='+tripSheetId, 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>
    <body onload="setValues();">
        <form name="enter" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Bill Generation</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td>Bill of Entry No</td>
                                        <td height="30"><input name="billOfEntryNo" id="billOfEntryNo" type="text" class="textbox" value="<c:out value="${billOfEntryNo}"/>" ></td>
                                        <td>Shiping Bill No</td>
                                        <td height="30"><input name="shipingBillNo" id="shipingBillNo" type="text" class="textbox" value="<c:out value="${shippingBillNo}"/>" ></td>
                                    <tr>
                                    <tr>                                       
                                        <td><font color="red">*</font>Billing Party</td>
                                        <td height="30">
                                            <input type="hidden" name="billingParty" id="billingParty" value="<c:out value="${customerId}"/>"/>
                                            <input type="text" name="Customer" id="Customer" class="textbox" value="<c:out value="${customerName}"/>" />
                                        </td>
                                        <td>Consignment Order No</td>
                                        <td height="30"><input name="orderNo" id="orderNo" type="text" class="textbox" value="<c:out value="${customerId}"/>" ></td> 
                                    </tr>
                                    <tr>
                                        <td>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>" ></td>
                                    </tr>
                                    <tr>
                                        <td>Priority</td>
                                        <td height="30"><select name="type" id="type"   >
                                                <option value="">All</option>
                                                <option value="1">urgent</option>
                                                <option value="2">Moderate</option>
                                                <option value="3">Normal</option>
                                            </select></td>
                                        <td>Order Type</td>
                                        <td height="30">
                                             <select name="movementType" id="movementType"  >
                                    <option value="">--Select---</option>
                                    <c:forEach items="${movementTypeList}" var="proList">
                                        <option value="<c:out value="${proList.movementTypeId}"/>"><c:out value="${proList.movementType}"/></option>
                                    </c:forEach>

                                </select>

                                        </td>
                                    </tr>
                                    <tr>`                                                                        
                                        <td colspan="4"><center><input type="button" class="button"   value="FETCH DATA" onclick="submitPage();"></center></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

            <br>
            <br>
            <%
            if((ArrayList)request.getAttribute("closedTrips")!=null){
                String fromDate=(String)request.getAttribute("fromDate");
                String toDate=(String)request.getAttribute("toDate");
            ArrayList list=(ArrayList)request.getAttribute("closedTrips");
            %>
            <%if(list.size()>0){%>
            <table style="width: 100%" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="80">
                        <th><h3>S No</h3></th>
                         <th><h3>Select</h3></th>
<%--                        <th><h3>Bill of Entry</h3></th>
                        <th><h3>Shipping Bill No</h3></th>--%>
                        <th><h3>Order Type</h3></th>
                        <th><h3>Billing Party</h3></th>
                        <th><h3>Customer Name</h3></th>
                    <%--    <th><h3>Revenue</h3></th>  --%>
                         <th><h3>Route Name</h3></th>
                        <th><h3>GR NO</h3></th>
                        <th><h3>C-Notes</h3></th>
                  <%--      <th><h3>Container</h3></th> --%>
                        <th><h3>Trip Code</h3></th>
                        <%--          <th><h3>POD Status</h3></th> --%>
                       
<%--                        <th><h3>Tonnage</h3></th> --%>
                        
                        <th><h3>Status</h3></th>
                <%--        <th><h3>Action</h3></th>  --%>
                       
                    </tr>
                </thead>
                <tbody>
                    <%int index=0;
                    int sno=1;
                    String classText ="";
                    %>              
                    <%
                         String customerName="";
                         String billingParty="";
                         String container="";
                         String cnoteNo="";
                         String consignmentOrderId="";
                         String tripId="";
                         String regNo="";
                         String vehicleType="";
                         String routeName="";
                         String vehicleTypeName="";
                         String tripStartDate="";
                         String tripEndDate="";
                         String tripstartkm="";
                         String tripendkm="";
                         String totalWeight="";
                         String destinationName="";
                         String freightAmt="";
                         String billingType="";
                         String customerId="";
                         String cnoteId="";
                         String checkTripId="";
                         String totalkmrun="";
                         String podStatus="";
                         String reeferRequired="";
                         String timeElapsedValue="";
                         String billOfEntry="";
                         String shippingBillNo="";
                         String movementType="";
                         for(int i=0;i<list.size();i++){
                         String tripCodes[] = null;
                         String tripIds[] = null;
                         String tripCode="";
                          String grNo="";
                         int oddEven = index % 2;
                          if (oddEven > 0) {
                                     classText = "text2";
                                 } else {
                                     classText = "text1";
                                 }
                             BillingTO opto=(BillingTO)list.get(i);
                             customerName=opto.getCustomerName();
                             cnoteNo=opto.getConsignmentNoteNo();
                             tripId=opto.getTripId();
                             if(opto.getTripId().contains(",")){
                             tripCodes = opto.getTripCode().split(",");
                             tripIds=opto.getTripId().split(",");
                                for(int k=0; k<tripCodes.length; k++){
                                 if(tripCode == ""){
                                 tripCode="<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>,<br>";
                                 }else{
                                 if(k == tripCodes.length-1){
                                 tripCode=tripCode+"<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>";
                                 }else{
                                 tripCode=tripCode+"<a href='#' onclick='viewTripDetails("+tripIds[k]+")'>"+tripCodes[k]+"</a>,<br>";
                                 }    
                                 }   
                                    
                                }
                             }else{
                             tripCode="<a href='#' onclick='viewTripDetails("+opto.getTripId()+")'>"+opto.getTripCode()+"</a>";
                             }
                             checkTripId=opto.getTripId();
                             destinationName=opto.getConsigmentDestination();
                             freightAmt=opto.getFreightCharges();
                             billingType=opto.getBillingTypeId();
                             customerId=opto.getCustomerId();
                             totalkmrun=opto.getTotalKmRun();                                               
                             totalWeight=opto.getTotalWeightage();                                               
                             podStatus=opto.getStatus();                                               
                             routeName=opto.getRouteInfo();                                               
                             reeferRequired=opto.getTempReeferRequired();                                               
                             consignmentOrderId=opto.getConsignmentOrderId();  
                             billingParty=opto.getBillingParty();
                             container=opto.getContainerTypeName();
                             timeElapsedValue=opto.getTimeElapsedValue();
                             billOfEntry=opto.getBillOfEntryNo();
                             shippingBillNo=opto.getShipingBillNo();
                             movementType=opto.getMovementType();
                             String[] grNos=null;
                           //  grNo=opto.getGrNo();
                             if(opto.getGrNo().contains(",")){
                             grNos = opto.getGrNo().split(",");
                             tripIds=opto.getTripId().split(",");
                                for(int k=0; k<grNos.length; k++){
                                 if(grNo == ""){
                                 grNo="<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>,<br>";
                                 }else{
                                 if(k == grNos.length-1){
                                 grNo=grNo+"<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>";
                                 }else{
                                 grNo=grNo+"<a href='#' onclick='printTripGrn("+tripIds[k]+")'>"+grNos[k]+"</a>,<br>";
                                 }
                                 }

                                }
                             }else{
                             grNo="<a href='#' onclick='printTripGrn("+opto.getTripId()+")'>"+opto.getGrNo()+"</a>";
                             }
                    %>

                    <tr>
                        <td  height="30">
                         <% if(Integer.parseInt(timeElapsedValue )> -2) {%>
                           <font color="green">   <%=sno%></font>
                             <% }else if (Integer.parseInt(timeElapsedValue )> -5){ %>
                               <font color="orange">    <%=sno%></font>
                              <%}else { %>
                               <font color="red">  <%=sno%></font>
                              <% }%>
                        </td>
                        <td  height="30">
                            <input type="hidden" name="tripId" id="tripId<%=sno%>" value="<%=tripId%>"/>
                            <input type="hidden" name="customerName<%=sno%>" value="<%=billingParty%>"/>
                            <input type="hidden" name="customerId<%=sno%>" id="customerId<%=sno%>"value="<%=customerId%>"/>
                            <input type="hidden" name="cnoteNo<%=sno%>" value="<%=cnoteNo%>"/>
                            <input type="hidden" name="destinationName<%=sno%>" value="<%=destinationName%>"/>
                            <input type="hidden" name="freightAmt<%=sno%>" value="<%=freightAmt%>"/>
                            <input type="hidden" name="billingType<%=sno%>" value="<%=billingType%>"/>
                            <input type="hidden" name="customerId<%=sno%>" value="<%=customerId%>"/>
                            <input type="hidden" name="totalweights<%=sno%>" value="<%=totalWeight%>"/>
                            <input type="hidden" name="tripDate<%=sno%>" value="<%=tripStartDate%>"/>
                            <input type="checkbox" name="consignmentId" id="consignmentId<%=sno%>" value="<%=tripId%>" onclick="setBillingOrder('<%=sno%>',this,this.value)"/>
                            <input type="hidden" name="consignmentOrderIdStatus" id="consignmentOrderIdStatus<%=sno%>" value="0" />
                            <input type="hidden" name="tripIdStatus" id="tripIdStatus<%=sno%>" value="0" />
                            <input type="hidden" name="routeName<%=sno%>" value="<%=routeName%>"/>
                            <input type="hidden" name="rowNo<%=sno%>" value="<%=sno%>"/>
                            <input type="hidden" name="totalkmrun<%=sno%>" value="<%=totalkmrun%>"/>
                        </td>
<%--                        <td  height="30" ><%=billOfEntry%></td>

                        <td  height="30" ><%=shippingBillNo%></td>--%>
                        <td  height="30" ><%=movementType%></td>
                        <td  height="30" ><%=billingParty%></td>
                        <td  height="30" ><%=customerName%></td>
                  <%--       <td  height="30" style="width:120px;text-align: right"><%=freightAmt%></td> --%>
                          <td  height="30" style="width:120px"><%=routeName%></td>
                           <td  height="30" style="width:120px"><%=grNo%></td>
                        <td  height="30" style="width:120px">
                            <% if(Integer.parseInt(timeElapsedValue )> -2) {%>

                                <font color="green">   <%=cnoteNo%></font>
                           <% }else if (Integer.parseInt(timeElapsedValue )> -5){ %>
                            <font color="orange">   <%=cnoteNo%></font>
                            <%}else { %>
                              <font color="red">   <%=cnoteNo%></font>
                            <% }%>
                        </td>
                  <%--      <td  height="30" style="width:120px"><%=container%></td> --%>
                       
                        <td  height="30" style="width:120px"><%=tripCode%></td>
                <%--        <td  height="30" >

                            <% if("0".equals(podStatus)){%>
                            <a href="viewTripPod.do?tripSheetId=<%=tripId%>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                <% } else if("1".equals(podStatus)){%>
                            <a href="viewTripPod.do?tripSheetId=<%=tripId%>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                <% }%>
                        </td>  --%>
                                             
<%--                        <td  height="30" style="width:120px;text-align: right"><%=totalWeight%></td>--%>
                       
                        <td  height="30"><img src="/throttle/images/icon_closed.png" alt=""/></td>
                   <%--     <td  height="30">Settled</td>  --%>
                        

                    </tr>
                    <%
                     cnoteId="500000";
                    sno++;
                    index++;
                    %>
                    <%
                }
            %>                

                </tbody>
            </table>
            <center>
                <input type="hidden" name="consignmentOrderId" id="consignmentOrderId" />
                <input type="hidden" name="custId" id="custId" value="<%=customerId%>" />
                <input type="hidden" name="tripSheetId" id="tripSheetId" value="" />
                <input type="button" class="button"   value="GENERATE BILL" onclick="generateBill();">
            </center>

            <input type="hidden" name="billingOrderId" id="billingOrderId" value=""/>
            <input type="hidden" name="invoicefromDate" value="<%=fromDate%>"/>
            <input type="hidden" name="invoicetoDate" value="<%=toDate%>"/>
            <!--          <br>
                    <center>
                        <input type="button" class="button" value="Generate Bill" name="Generate Bill" onClick="generateBill();">
                    </center>-->
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <%}}%>                
            <script type="text/javascript">
                var customerName = '<c:out value="${customerName}"/>';
                var customerId = '<c:out value="${customerId}"/>';

                if (customerId != null & customerId != "") {
                    document.getElementById("customerId").value = customerId;
                }
                if (customerName != null & customerName != "") {
                    document.getElementById("Customer").value = customerName;
                }

                function viewTripDetails(tripId) {
                    window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                }

                function setBillingOrder(sno,obj,value) {
                    var cnoteId = "";
                    if(obj.checked == true){
                   
                    document.getElementById("tripIdStatus"+sno).value = value;
                    document.getElementById("tripSheetId").value = value;
                    }else{
                    document.getElementById("tripIdStatus"+sno).value = 0;
                    document.getElementById("tripSheetId").value = 0;
                    }
                }
            </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>