<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>  

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        
         <script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script> 
    
    <script>
        function submitPage(){
            var chek=Validation();
            if(chek==1){            
            document.vehicleReport.action='/throttle/mechanicPerformanceDetails.do';
            document.vehicleReport.submit();
            }
        }
        function Validation(){              
            if(textValidation(document.vehicleReport.fromDate,'From Date')){
               return 0;              
                }
            if(textValidation(document.vehicleReport.toDate,'To Date')){
               return 0;              
                }
                                
                return 1;
            }

        function setValues(){

            if('<%=request.getAttribute("fromDate")%>'!='null'){
            document.vehicleReport.fromDate.value ='<%=request.getAttribute("fromDate")%>';
            document.vehicleReport.toDate.value ='<%=request.getAttribute("toDate")%>';
           
            }
            if('<%=request.getAttribute("technicianId")%>' != 'null'){
              document.vehicleReport.technicianId.value='<%=request.getAttribute("technicianId")%>';
              }
            if('<%=request.getAttribute("spId")%>' != 'null'){
              document.vehicleReport.spId.value='<%=request.getAttribute("spId")%>';
              }

            }
            
            
    
    function printPage()
    {       
        var DocumentContainer = document.getElementById("printPage");
        var WindowObject = window.open('', "TrackHistoryData", 
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        //WindowObject.close();   
    }            

     function newWindow(techId,fromDate,toDate ){
        //alert('/throttle/mechPerformanceData.do?technicianId='+techId+"&fromDate="+fromDate+"&toDate="+toDate);
        window.open('/throttle/mechPerformanceData.do?technicianId='+techId+"&fromDate="+fromDate+"&toDate="+toDate, 'PopupPage', 'height=450,width=600,scrollbars=yes,resizable=yes');
    }            
            
    </script>

     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> Mechanic Performance </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Reports</a></li>
          <li class="active">Mechanic Performance</li>
        </ol>
      </div>
      </div>

<div class="contentpanel">
<div class="panel panel-default">


      <div class="panel-body">
    <body onload="setValues();">
    <form name="vehicleReport">
    
        <%@ include file="/content/common/message.jsp" %>
        
        
<!--           <table class="table table-bordered">
               <thead>
          <tr>
              <th height="30"  colspan="8" >Mechanic Performance </th>
            </tr>
</thead>-->
<table class="table table-info mb30 table-hover" id="bg"   >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >Mechanic Performance</th>
		</tr>
                    </thead>
            </table>	
<table class="table table-info mb30 table-hover"  >
            <tr>
                 <td  align="left" height="30">&nbsp;&nbsp;Technician</td>
                 <td   ><select class="form-control" style="width:240px;height:40px" name="technicianId" >
                        <option value="0">---Select---</option>
                        <c:if test = "${TechnicianList != null}" >
                            <c:forEach items="${TechnicianList}" var="tec"> 
                                <option value='<c:out value="${tec.technicianId}" />'><c:out value="${tec.technician}" /></option>
                            </c:forEach >
                        </c:if>  	
                </select>
                </td>
<!--                    </tr>
                    <tr>-->
                <td  align="left" height="30">&nbsp;&nbsp;Location</td>
                 <td   ><select class="form-control" style="width:240px;height:40px" name="spId" >
                        <option value="0">---Select---</option>
                        <c:if test = "${servicePointList != null}" >
                            <c:forEach items="${servicePointList}" var="sp">
                                <option value='<c:out value="${sp.spId}" />'><c:out value="${sp.spName}" /></option>
                            </c:forEach >
                        </c:if>
                </select>
                </td>
                </tr>
                <tr>
                
               <td  align="left" height="30"><font color="red">*</font>From Date</td>               
                <td ><input name="fromDate" type="text" class="datepicker form-control" style="width:240px;height:40px"  size="20">
                    <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleReport.fromDate,'dd-mm-yyyy',this)"/></td>-->
                 <td  align="left" height="30"><font color="red">*</font>To Date </td> 
                <td ><input name="toDate" type="text" class="datepicker form-control" style="width:240px;height:40px"  size="20">
                    <!--<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.vehicleReport.toDate,'dd-mm-yyyy',this)"/></td>-->
            </tr>
      
        </table>
        <center>   
        <br>
             <input type="button"  class="btn btn-success"  value="Search" onclick="submitPage();">   
            </center>
        <br>
        <br>
        <c:if test = "${mechPerformanceList != null}" >
            <center> <input type="button"  class="btn btn-success"  name="Print" value="Print" onClick="printPage();" > </center>
        <div id="printPage" >
    <!--<table class="table table-bordered">-->
    <table class="table table-info mb30 table-hover" id="table" >
        <thead>
                <tr>
                    
                    <th>Technician</th>
                    <th>Location</th>
                    <th>Estimated Hrs</th>
                    <th>Actual Hrs</th>
                    <th>Productive Hours Available</th>
                    <th>Productivity</th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <% int index = 0;%> 
                <c:forEach items="${mechPerformanceList}" var="mech"> 
                    <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>	
                    

                        <td class="<%=classText %>" height="30" ><c:out value="${mech.technician}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${mech.companyName}" /></td>
                        <td class="<%=classText %>" height="30"><c:out value="${mech.estHrs}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${mech.actHrs}" /></td>                        
                        <td class="<%=classText %>" height="30"><c:out value="${noOfDays}" /></td>                        
                        <td class="<%=classText %>" height="30">
                            
                            <fmt:formatNumber type="percent" maxFractionDigits="2" value="${mech.actHrs/noOfDays}" />
                        </td>                        
                        <td class="<%=classText %>" height="30">
                            <a href=# onclick=newWindow('<c:out value="${mech.technicianId}" />','<c:out value="${fromDate}" />','<c:out value="${toDate}" />')>details</a>
                        </td>                        

                    </tr>
                    <% index++;%>
                </c:forEach>
                
            </table>
     
            </div>
        </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>	
    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
 </div>


        </div>
        </div>



<%@ include file="../common/NewDesign/settings.jsp" %>

