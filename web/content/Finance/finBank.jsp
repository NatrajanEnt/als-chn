
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->



   <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.ManageBankBranchMaster"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.ManageBankBranchMaster"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"> 
    <body>
        <form method="post" name="addbank" class="form-horizontal form-bordered">
                     <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
            <c:if test = "${primarybankList != null}" >
                 <table class="table table-info mb30 table-hover" id="table">
<thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th><spring:message code="finance.label.BankName"  text="default text"/></th>
                            <th><spring:message code="finance.label.BankCode"  text="default text"/></th>
                            <th><spring:message code="finance.label.ViewBranch"  text="default text"/></th>
                            </thead>
                  
                    <% int index = 0;%>
                    <c:forEach items="${primarybankList}" var="BL">
                       
                        <tr height="30">
                            <td  align="left"> <%= index + 1 %> </td>
                            <td  align="left"> <c:out value="${BL.primaryBankName}" /></td>
                            <td  align="left"> <c:out value="${BL.primaryBankCode}"/> </td>
                            <td  height="30"><input type="button" class="btn btn-success" style="width:60px;height:30px;" name="View" value="<spring:message code="finance.label.View"  text="default text"/>" onclick="loadBankBranch('<c:out value="${BL.primaryBankId}"/>','<c:out value="${BL.primaryBankName}"/>')"/></td>
                        </tr>
                        <% index++;%>
                    </c:forEach>
                </c:if>

            </table>
              
            
             <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
            <br>
            <script>
                function loadBankBranch(bankId, bankName){                                        
                    $.ajax({
                    url: '/throttle/addBankPage.do',
                    data: {bankId: bankId, bankName: bankName},
                    //dataType: 'json',
                    success: function (data) {
                    $("#branchDetalis").html(data); 
                    }
                    }); 
                } 
            </script>
            <div id="branchDetalis">
                <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                        <div align="center" id="bankNameStatus" height="20" >&nbsp;&nbsp;</div>
                    </font>
            </div>

            <!--            <center><input type="submit" class="button" value="Add" /></center>-->
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>

      </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
