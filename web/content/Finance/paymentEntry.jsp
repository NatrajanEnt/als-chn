
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
        <script type="text/javascript">
            $(document).ready(function () {
                var date2 = new Date();
                $(".datepicker").datepicker({
                    dateFormat: 'dd-mm-yy'
                }).datepicker('setDate', date2)

            });
            /*     $(document).ready(function () {
             $("#datepicker").datepicker({
             showOn: "button",
             buttonImage: "calendar.gif",
             buttonImageOnly: true
             
             });
             });
             
             $(function () {
             //alert("cv");
             $(".datepicker").datepicker({
             
             changeMonth: true, changeYear: true
             });
             });
             */
        </script>


    <script language="javascript">
        function checkLedger(sno)
        {
            var ledgerId = document.getElementById("ledgerId").value;
            var Lid = document.getElementById("bankId" + sno).value;
            var temp = Lid.split("~");
            if (temp[0] == ledgerId) {
                alert("Select Other then Karikali cash")
                document.getElementById("bankId" + sno).value = 0;
            }
        }
//        window.onload = function ()
//        {
//            var currentDate = new Date();
//            var day = currentDate.getDate();
//            var month = currentDate.getMonth() + 1;
//            var year = currentDate.getFullYear();
//            var myDate = day + "-" + month + "-" + year;
//            document.getElementById("date").value = myDate;
//        }


        function sumPaymentAmt() {
            var sumAmt = 0;
            var totAmt = 0;
            sumAmt = document.getElementsByName('amount');
            for (i = 0; i < sumAmt.length; i++) {
                if (sumAmt[i].value != "") {
                    totAmt += parseFloat(sumAmt[i].value);
//                totAmt += parseInt(sumAmt[i].value);
                    document.getElementById('totalCreditAmt').value = parseFloat(totAmt).toFixed(2);
//                document.getElementById('totalCreditAmt').value=parseInt(totAmt);
                } else {
                    document.getElementById('totalCreditAmt').value = 0;
                }
            }
        }

        function bankIdVal()
        {
            var bankid1 = document.getElementsByName("bankid1");
            var bankid2 = document.getElementsByName("bankid2");
            //            alert("bankid1=== "+textValidation(bankid1[i]);
            // alert("bankid2=== "+bankid2);
        }
        function setSelectbox(i)
        {
            var selected = document.getElementsByName("selectedIndex");
            alert("selected==" + selected[i]);
            selected[i].checked = true;

        }
        function setCheckBox(val) {
            //  alert(val);
            //  alert("in set cb");
            document.getElementById("selectedIndex" + val).checked = true;

        }
        function submitPage1(value) {
            if (value == "Save") {
                if (document.getElementById('fromDate').value == "") {
                    alert("please select cash payment date");
                    document.getElementById('fromDate').focus();
//                }else if(document.getElementById('totalCreditAmt').value == ""){
//                    alert("please enter credit amount");
//                    document.getElementById('totalCreditAmt').focus();
                } else {
                    var checValidate = selectedItemValidation();
                }
                if (checValidate == 'SubmitForm') {
                    document.manufacturer.action = '/throttle/insertPaymentEntry.do';
                    document.manufacturer.submit();
                }
            }
        }
        function submitPage(value)
        {
            if (value == 'Save') {
                if (document.getElementById('fromDate').value == "") {
                  alert("please select cash payment date");
                    document.getElementById('fromDate').focus();
                } else if (selectedItemValidation() == 'SubmitForm') {
                    //validattion
                        document.manufacturer.action = '/throttle/insertPaymentEntry.do';
                        document.manufacturer.submit();
                }
            }
        }
        function selectedItemValidation() {
            var bankId = document.getElementsByName("bankid1");
            var amount = document.getElementsByName("amount");
            var narration = document.getElementsByName("narration");
            var chec = 0;
            var index = 0;
            var mess = "SubmitForm";
            for (var i = 0; i < amount.length; i++) {
                index = parseInt(i) + parseInt(1);
                chec++;
                if (bankId[i].value == 0) {
                    alert("select Ledger Name for row " + index);
                    mess = 'NotSubmit';
                } else if (amount[i].value == "") {
                    alert("select amount for row " + index);
                    mess = 'NotSubmit';
                } else if (narration[i].value == "") {
                    alert("Give narration for row " + index);
                    mess = 'NotSubmit';
                } else {
                    mess = 'SubmitForm';

                }
            }
            if (mess == 'SubmitForm') {
                if (chec == 0) {
                    alert("Please click ADDROW And Then Proceed");
                    mess = 'NotSubmit';
                } else {
                    mess = 'SubmitForm';
                }
            }
            return mess;
        }

        function setValues(sno, accountEntryIds, accountsAmount, creditAmount, voucherCode, accountEntryDate, headerNarration, fullName, voucherId,vehicleId,costNarration,vehicleNo) {
//        if(accountEntryIds.contains(",")){
            var temp = accountEntryIds.split(",");
            var tempOne = fullName.split(",");
            var tempTwo = accountsAmount.split(",");
            var tempThree = document.getElementById('entryNarration' + sno).value.split("@");
            var index = 0;
            var i;
             DeleteRow();
            var chechkbox = document.getElementsByName("edit");
            for (i = 0; i < chechkbox.length; i++) {
                document.getElementById('edit' + i).checked = false;
            }
            document.getElementById('edit' + sno).checked = true;
            if (document.getElementById('edit' + sno).checked == true) {
                for (i = 0; i < temp.length; i++) {
                    index = parseInt(i) + parseInt(1);
                //alert(tempOne.length);

                  showRow();
                    document.getElementById('amount' + index).value = tempTwo[i];
                    document.getElementById('bankId' + index).value = tempOne[i];
                    document.getElementById('narration' + index).innerHTML = tempThree[i];
                    document.getElementById('selectedIndex' + index).checked = true;
                }
                document.getElementById('totalCreditAmt').value = "";
                document.getElementById('fromDate').value = "";
                document.getElementById('voucherCode').value = "";
                document.getElementById('voucherIdEdit').value = "";

                document.getElementById('totalCreditAmt').value = creditAmount;
                document.getElementById('fromDate').value = accountEntryDate;
                document.getElementById('voucherCode').value = voucherCode;
                document.getElementById('voucherIdEdit').value = voucherId;

            }
            if(vehicleId > 0){ 
                document.getElementById('costCenterTab').style.display='block';
                document.getElementById('vehicleId').value = vehicleId;
                document.getElementById('vehicleNo').value = vehicleNo;
                document.getElementById('costCenterNarration').value = costNarration;
                document.getElementById('activeInd').value = 1;
            }
        }

        var rowCount = 1;
        var sno = 0;
        var style = "text2";
        function showRow()
        {
            if (rowCount % 2 == 0) {
                style = "text2";
            } else {
                style = "text1";
            }
            sno++;
            var tab = document.getElementById("addRow");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell1 = "<td class='text1' height='25' >" + sno + " </td>";
//            <input type='text' name='serialNo' id='serialNo" + sno + "' class='form-control' />

            cell.setAttribute("className", style);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(1);
//            var cell2 = "<td class='text1' height='30'><select class='form-control' id='bankId"+sno+"' onChange='checkLedger("+sno+");setCheckBox("+sno+");'  style='width:225px'  name='bankid1'><option selected value=0>---Select---</option><c:if test = "${paymentLedgerList != null}" ><c:forEach items="${paymentLedgerList}" var="JLL"><option  value='<c:out value="${JLL.ledgerID}" />~<c:out value="${JLL.groupCode}" />~<c:out value="${JLL.levelID}" />~<c:out value="${JLL.ledgerCode}" />'><c:out value="${JLL.ledgerName}" /></option></c:forEach ></c:if></select></td>";
            var cell2 = "<td class='text1' height='30'><select class='form-control' id='bankId" + sno + "'   style='width:190px;height:40px;'  name='bankid1'><option selected value=0>---<spring:message code="finance.label.Select"  text="default text"/>---</option><c:if test = "${paymentLedgerList != null}" ><c:forEach items="${paymentLedgerList}" var="JLL"><c:if test = "${JLL.ledgerID != 2}" ><option  value='<c:out value="${JLL.ledgerID}" />~<c:out value="${JLL.groupCode}" />~<c:out value="${JLL.levelID}" />~<c:out value="${JLL.ledgerCode}" />'><c:out value="${JLL.ledgerName}" /></option></c:if></c:forEach ></c:if></select></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell2;

            cell = newrow.insertCell(2);
//            var cell3 = "<td class='text1' height='30'><input type='text' name='amount' id='amount"+sno+"' maxlength='13' onkeyup='sumPaymentAmt();' onclick='bankIdVal(sno-1)'  size='20' class='form-control' onkeypress='return onKeyPressBlockCharacters(event);' /></td>";
            var cell3 = "<td class='text1' height='30'><input type='text' name='amount' id='amount" + sno + "' maxlength='13' onkeyup='sumPaymentAmt();' onclick='bankIdVal(sno-1)'  style='width:190px;height:40px;' class='form-control'  /></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell3;

            cell = newrow.insertCell(3);
//            var cell4 = "<td height='30' class='tex1'> <div align='center'><select name='accType' id='accType"+sno+"'  class='form-control'><option value='DEBIT'>DEBIT</option></select></div> </td>";
            var cell4 = "<td height='30' class='tex1'> <div align='center'><select name='accType' id='accType" + sno + "'  class='form-control' style='width:190px;height:40px;'><option value='DEBIT'><spring:message code="finance.label.DEBIT"  text="default text"/></option></select></div> </td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell4;

            cell = newrow.insertCell(4);
            
    var cell5 = "<td class='text1' height='20'><textarea name='narration'  id='narration" + sno + "' style='width:190px;height:40px;' class='form-control' /></textarea></td>";
        cell.setAttribute("className", style);
            cell.innerHTML = cell5;

            cell = newrow.insertCell(5);
//            var cell6 = "<td class='text1' height='30'><input type='checkbox' name='selectedIndex' value='"+sno+"'/></td>";
            if(parseInt(sno)==1){
var cell6 = "<td class='text1' height='30'><input type='checkbox' id='costCenter" + sno + "' name='costCenter' value='" + sno + "' onclick='viewCostCenter(" + sno + ");' style='width:15px;'/></td>";
  }else{
var cell6 = "<td class='text1' height='30'></td>";
  }
        cell.setAttribute("className", style);
            cell.innerHTML = cell6;
            cell = newrow.insertCell(6);
//            var cell6 = "<td class='text1' height='30'><input type='checkbox' name='selectedIndex' value='"+sno+"'/></td>";
            var cell7 = "<td class='text1' height='30'><input type='checkbox' id='selectedIndex" + sno + "' name='selectedIndex' value='" + sno + "' style='width:15px;'/></td>";
            cell.setAttribute("className", style);
            cell.innerHTML = cell7;

            //            cell = newrow.insertCell(5);
            //            var cell6 = "<td alain='left' >3tretrt<input type='checkbox' name='deleteItem' value='"+snumber+"'/> </td>";
            //            cell.setAttribute("className",style);
            //            cell.innerHTML = cell6;

            var temp = sno - 1;

            rowCount++;
//            alert("im here");
            // document.getElementById('AddRow').style.display = 'none';

        }


        function DeleteRow() {
            document.getElementById('AddRow').style.display = 'block';
            try {
                var table = document.getElementById("addRow");
                rowCount = table.rows.length;
                // alert(rowCount);
                for (var i = 1; i < rowCount; i++) {
                    var row = table.rows[i];
                    var checkbox = row.cells[6].childNodes[0];
                    if (null != checkbox && true == checkbox.checked) {
                        if (rowCount <= 1) {
                            alert("Cannot delete all the rows");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                        sno--;
                        // snumber--;
                    }
                }
                sumPaymentAmt();
            } catch (e) {
                alert(e);
            }
        }
function viewCostCenter(sno){
    if(document.getElementById('costCenter' + sno).checked == true){
    document.getElementById('costCenterTab').style.display='block';
    document.getElementById('activeInd').value='1';
}else if(document.getElementById('costCenter' + sno).checked == false){
     document.getElementById('costCenterTab').style.display='none';
     document.getElementById('activeInd').value='0';
    }
    
    }
    

        function printCashPaymentEntry(val, voucherNo) {
            document.manufacturer.action = '/throttle/printCashPaymentEntry.do?voucherCode=' + val + '&voucherNo=' + voucherNo;
            document.manufacturer.submit();
        }
        function viewCostCenterCashPayment(accountEntryIds, debitAmount, creditAmount, voucherCode, creditLedgerName, debitLedgerName, Credit, Debit, entryDate) {

            document.manufacturer.action = '/throttle/viewCostCenterCashPayment.do?accountEntryIds=' + accountEntryIds + '&debitAmount=' + debitAmount + '&creditAmount=' + creditAmount + '&Entry=Cash Payment&voucherCode=' + voucherCode + '&creditLedgerName=' + creditLedgerName + '&debitLedgerName=' + debitLedgerName + '&Credit=' + Credit + '&Debit=' + Debit + '&entryDate=' + entryDate;
            document.manufacturer.submit();

        }
        function costCenterCashPaymentEditView(accountEntryIds, debitAmount, creditAmount, voucherCode, val, creditLedgerName, debitLedgerName, entryDate) {
            //alert(voucherCode);
            document.manufacturer.action = '/throttle/costCenterCashPaymentEditView.do?accountEntryIds=' + accountEntryIds + '&debitAmount=' + debitAmount + '&creditAmount=' + creditAmount + '&Entry=Cash Payment&voucherCode=' + voucherCode + '&Param=' + val + '&creditLedgerName=' + creditLedgerName + '&debitLedgerName=' + debitLedgerName + '&entryDate=' + entryDate;
            document.manufacturer.submit();
        }
        function editCashPaymentEntry(voucherCode) {
            //alert(voucherCode);
            document.manufacturer.action = '/throttle/cashPaymentEdit.do?voucherCode=' + voucherCode;
            document.manufacturer.submit();
        }
       function getVehicleNos(){
        // Use the .autocomplete() method to compile the list based on input from user
       // alert("$('#vehicleNo').value()");
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Vehicle No");
                        $('#vehicleNo').val('');
                        $('#vehicleId').val('');    
                        $('#vehicleNo').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#vehicleNo').val(value);
                $('#vehicleId').val(id);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    }



                </script>
            
            
            <style>
    #index td {
   color:white;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.PaymentEntryDate"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.PaymentEntryDate"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            
            
            
            
                <body onload="showRow();">
                    <form name="manufacturer" method="post" >
                       <%@ include file="/content/common/message.jsp" %>
            <input type="hidden" id="voucherCode" name="voucherCode" size="20" class="form-control" value="">
            <input type="hidden" id="voucherIdEdit" name="voucherIdEdit" size="20" class="form-control" value="">
            <table class="table table-info mb30 table-hover">
                <thead>
                <tr id="index">
                    <th colspan="6"><spring:message code="finance.label.PaymentEntryDate"  text="default text"/></th>
                </tr>
                </thead>
                <tr>
                    <td  height="30"><spring:message code="finance.label.Date"  text="default text"/> </td>
                    <td  height="30">
                        <input type="textbox" id="fromDate" name="fromDate" size="20" class="datepicker" value="" style="width:240px;height:40px;">
                    </td>
                    <td  height="30"><spring:message code="finance.label.CashHead"  text="default text"/></td>
                    <td  height="30">
                        <input type="hidden" id="ledgerId" name="ledgerId"  size="20" class="form-control" readonly value="<%=ThrottleConstants.CashOnHandLedgerId%>">
                        <input type="text" id="cashHeadName" name="cashHeadName" value="<%=ThrottleConstants.CashOnHandLedgerName%>"  style="width:240px;height:40px;" class="form-control" readonly>
                    </td>
                    <td  height="30"><spring:message code="finance.label.Credit"  text="default text"/></td>
                    <td><input type="text" id="totalCreditAmt" name="totalCreditAmt" value="" style="width:240px;height:40px;" class="form-control" onkeypress="return onKeyPressBlockCharacters(event);" readonly>
                        
                    </td>
                </tr>
                <script type="text/javascript">
                    function onKeyPressBlockCharacters(e)
                    {
                        var key = window.event ? e.keyCode : e.which;
                        var keychar = String.fromCharCode(key);
                        reg = /[a-zA-Z]+$/;

                        return !reg.test(keychar);

                    }
                </script>
            </table>
            <p>&nbsp;</p>
            <div align="center">
                 <table class="table table-info mb30 table-hover" id="addRow">
                    <tr id="index" >
                        <td style="background-color:#5BC0DE;"><spring:message code="finance.label.Sno"  text="default text"/></td>
                        <td style="background-color:#5BC0DE;"><spring:message code="finance.label.LedgerName"  text="default text"/></td>
                        <td style="background-color:#5BC0DE;"><spring:message code="finance.label.Amount"  text="default text"/></td>
                        <td style="background-color:#5BC0DE;"><spring:message code="finance.label.AccountType"  text="default text"/></td>
                        <td style="background-color:#5BC0DE;"><spring:message code="finance.label.Narration"  text="default text"/></td>
                        <td style="background-color:#5BC0DE;"><spring:message code="finance.label.CostCenter"  text="default text"/></td>
                        <td style="background-color:#5BC0DE;"><spring:message code="finance.label.Select"  text="default text"/></td>
                    </tr>
                </table>
            </div>
            <div align="center" id="costCenterTab" style="display: none">
                 <table class="table table-info mb30 table-hover" style="width:780px;" >
            <!--<table  class="border" border="3" class="table table-bordered">--> 
                   <tr id="index">
                        <td style="background-color:#5BC0DE;" align="center"><spring:message code="finance.label.VehicleNo"  text="default text"/></td>
                        <td style="background-color:#5BC0DE;" align="center"><spring:message code="finance.label.Narration"  text="default text"/></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <input type="text" id="vehicleNo" name="vehicleNo" style="width:190px;height:40px;" class="form-control" value="" onfocus="getVehicleNos();">
                        <input type="hidden" id="vehicleId" name="vehicleId" size="20" class="form-control" value="">
                        <input type="hidden" id="activeInd" name="activeInd" size="20" class="form-control" value="">
                        </td>
                        <td align="center">
                            <textarea type="text" id="costCenterNarration" name="costCenterNarration" style="width:190px;height:40px;" class="form-control" value=""></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <table align="center" width="100%" border="0">
                <tr >
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right"><input type="button" id="AddRow" value="<spring:message code="finance.label.AddRow"  text="default text"/>" class="btn btn-success" onClick="showRow();" style="width:100px;height:35px;">&emsp;</td>
                    <td colspan="2" align="left"> <input type="button" id="DeleteRow1" value="<spring:message code="finance.label.DeleteRow"  text="default text"/>" class="btn btn-success" onClick="DeleteRow();" style="width:100px;height:35px;">&emsp;
                        <input type="button" value="<spring:message code="finance.label.EntryDate"  text="default text"/>" class="btn btn-success" onClick="submitPage(this.value);" style="width:100px;height:35px;"></td>
                </tr>
            </table>
            <br>
            
            <!--            <center>
                            &nbsp;&nbsp;<br>
                            &nbsp;&nbsp;                
                        </center>
                        <br>-->

            <c:if test = "${cashPaymentList != null}" >

                <br>
                 <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                    <th><spring:message code="finance.label.EntryDate"  text="default text"/></th>
                    <th><spring:message code="finance.label.VoucherCode"  text="default text"/></th>
                    <th><spring:message code="finance.label.CreditLedger"  text="default text"/></th>
                    <th><spring:message code="finance.label.CreditAmount"  text="default text"/></th>
                    <th><spring:message code="finance.label.DebitLedger"  text="default text"/></th>
                    <th><spring:message code="finance.label.DebitAmount"  text="default text"/></th>
                    <th><spring:message code="finance.label.Narration"  text="default text"/></th>
                    <th><spring:message code="finance.label.Print"  text="default text"/></th>
                    <th><spring:message code="finance.label.Edit"  text="default text"/></th>
                    <!--<th><h3>Cost Center</th>-->
                    </thead>
                    <tbody>
                        <% int index = 0;%>
                        <c:forEach items="${cashPaymentList}" var="PL">
                           
                            <tr height="30">
                                <td   align="left"> <%= index + 1%> </td>
                                <td   align="left"> <c:out value="${PL.accountEntryDate}"/> </td>
                                <td  align="left"> 
                                    <c:out value="${PL.voucherNo}" />
                                </td>
                                <td   align="left"> <c:out value="${PL.creditLedgerName}"/> </td>
                                <td   align="left"> <c:out value="${PL.creditAmount}"/> </td>
                                <td   align="left"> <c:out value="${PL.debitLedgerName}"/> </td>
                                <td   align="left"> <c:out value="${PL.debitAmount}"/> </td>
                                <td   align="left"> <c:out value="${PL.headerNarration}"/> </td>
                                <td   align="left"><a href="" onclick="printCashPaymentEntry('<c:out value="${PL.voucherCode}" />', '<c:out value="${PL.voucherNo}"/>')">Print</a></td>
                                <td  align="center"> 
                                    <input type="hidden" id="entryNarration<%=index%>" name="entryNarration"  value="<c:out value="${PL.entryNarration}"/>">
                                    <input type="checkbox" align="center" id="edit<%=index%>" name="edit" onclick="setValues('<%=index%>', '<c:out value="${PL.accountEntryIds}" />', '<c:out value="${PL.accountsAmount}"/>', '<c:out value="${PL.creditAmount}"/>', '<c:out value="${PL.voucherCode}" />', '<c:out value="${PL.accountEntryDate}"/>', '0', '<c:out value="${PL.fullName}"/>', '<c:out value="${PL.voucherId}"/>','<c:out value="${PL.vehicleId}"/>','<c:out value="${PL.costNarration}"/>','<c:out value="${PL.vehicleNo}"/>');" />
                                    <!--<input type="checkbox" align="center" id="edit<%=index%>" onclick="setValuestest();setValues1('<%=index%>', '<c:out value="${PL.accountEntryIds}" />', '<c:out value="${PL.accountsAmount}"/>', '<c:out value="${PL.creditAmount}"/>', '<c:out value="${PL.voucherCode}" />', '<c:out value="${PL.accountEntryDate}"/>', '<c:out value="${PL.headerNarration}"/>', '<c:out value="${PL.fullName}"/>', '<c:out value="${PL.voucherId}"/>');" />-->
                                </td>


                               <%-- <td   align="left">
                                    <c:if test="${PL.creditCount > 0 || PL.debitCount > 0}" >
                                        <c:if test="${PL.status == 0}" >
                                            <a href="" onclick="viewCostCenterCashPayment('<c:out value="${PL.accountEntryIds}" />', '<c:out value="${PL.debitAmount}"/>', '<c:out value="${PL.creditAmount}"/>', '<c:out value="${PL.voucherCode}" />', '<c:out value="${PL.creditLedgerName}"/>', '<c:out value="${PL.debitLedgerName}"/>', '<c:out value="${PL.creditCount}"/>', '<c:out value="${PL.debitCount}"/>', '<c:out value="${PL.accountEntryDate}"/>')">Create</a>
                                        </c:if>

                                        <c:if test="${PL.status == 1}" >
                                            <a href="" onclick="costCenterCashPaymentEditView('<c:out value="${PL.accountEntryIds}" />', '<c:out value="${PL.debitAmount}"/>', '<c:out value="${PL.creditAmount}"/>', '<c:out value="${PL.voucherCode}" />', 'View', '<c:out value="${PL.creditLedgerName}"/>', '<c:out value="${PL.debitLedgerName}"/>', '<c:out value="${PL.accountEntryDate}"/>')">view</a>&nbsp;/
                                            <a href="" onclick="costCenterCashPaymentEditView('<c:out value="${PL.accountEntryIds}" />', '<c:out value="${PL.debitAmount}"/>', '<c:out value="${PL.creditAmount}"/>', '<c:out value="${PL.voucherCode}" />', 'Edit', '<c:out value="${PL.creditLedgerName}"/>', '<c:out value="${PL.debitLedgerName}"/>', '<c:out value="${PL.accountEntryDate}"/>')">Edit</a>
                                        </c:if>
                                    </c:if>
                                    <c:if test="${PL.creditCount == 0 && PL.debitCount == 0}" >
                                        &nbsp;
                                    </c:if>
                                </td>--%>
                            </tr>


                            <% index++;%>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <br>
            <br>
            <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>