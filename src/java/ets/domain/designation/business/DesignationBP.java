/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
package ets.domain.designation.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.designation.business.DesignationTO;
import ets.domain.designation.data.DesignationDAO;

import java.util.*;

/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
public class DesignationBP {

    /** Creates a new instance of __NAME__ */
    public DesignationBP() {
    }
    DesignationDAO designationDAO;

    public DesignationDAO getDesignationDAO() {
        return designationDAO;
    }

    public void setDesignationDAO(DesignationDAO designationDAO) {
        this.designationDAO = designationDAO;
    }

    /**
     * This method used to Get Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processActiveDesignaList() throws FPRuntimeException, FPBusinessException {

        ArrayList DesignaList = new ArrayList();
        ArrayList activeDesigList = new ArrayList();
        DesignationTO desigTO = null;
        DesignaList = designationDAO.getDesignaList();
        Iterator itr = DesignaList.iterator();
        while(itr.hasNext()){
            desigTO = new DesignationTO();
            desigTO = (DesignationTO)itr.next();            
            if(desigTO.getActiveInd().equalsIgnoreCase("y"))
            {
                activeDesigList.add(desigTO);
            }
        }        
        return activeDesigList;
    }
    
    /**
     * This method used to Get Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises -l
     */
    public ArrayList processGetDesignaList() throws FPRuntimeException, FPBusinessException {

        ArrayList DesignaList = new ArrayList();
        DesignaList = designationDAO.getDesignaList();
         if (DesignaList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return DesignaList;
    }    

    /**
     * This method used to Add Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    
    /*   public ArrayList addDesignation() throws FPRuntimeException, FPBusinessException {
    ArrayList DesignaList = new ArrayList();
    DesignaList = designationDAO.getDesignaList();
    return DesignaList;
    }
    
    /**
     * This method used to Insert Designation Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises  -l
     */
    public int processInsertDesigDetails(DesignationTO designationTO, int UserId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = designationDAO.doDesigDetails(designationTO, UserId);
        return insertStatus;
    }

    public String processCheckDesigName(String designationName) {
        String desigName = "";
        desigName = designationDAO.doCheckDesigName(designationName);
        if (desigName == null) {
            desigName = "";
        }
        return desigName;
    }

    /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises  -l
     */
    public int processModifyDesignaDetails(ArrayList List, int UserId) {
        int insertStatus = 0;
        insertStatus = designationDAO.doDesignaDetailsModify(List, UserId);
        return insertStatus;
    }

    /**
     * This method used to Get Desgintion Name.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String processGetDesigName(int desigId) throws FPRuntimeException, FPBusinessException {
        String desigName = null;
      
        desigName = designationDAO.getDesigName(desigId);
        return desigName;
    }

    /**
     * This method used to Get Grade Details According to Designations
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises  -l
     */
    public ArrayList processGetGradeList(int desigId) throws FPRuntimeException, FPBusinessException {
        ArrayList gradeList = new ArrayList();
        gradeList = designationDAO.getGradeList(desigId);
        if (gradeList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return gradeList;

    }
    
     /**
     * This method used to Insert Grade Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises   -l
     */
    public int processInsertgradeDetails(DesignationTO designationTO, int UserId)  throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = designationDAO.doGradeDetails(designationTO, UserId);
        return insertStatus;
    }
    
    /**
     * This method used to Get Grade Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processGetGradeList() throws FPRuntimeException, FPBusinessException {
      
        ArrayList GradeList = new ArrayList();
        GradeList = designationDAO.getGradeList();
        return GradeList;
    }
    
    /**
     * This method used to Get Grade Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList processActiveGradeList() throws FPRuntimeException, FPBusinessException {
      
        ArrayList GradeList = new ArrayList();
        ArrayList activeGradeList = new ArrayList();
        DesignationTO desigTO = null;
        GradeList = designationDAO.getGradeList();
        Iterator itr = GradeList.iterator();
        while(itr.hasNext()){
            desigTO = new DesignationTO();
            desigTO = (DesignationTO)itr.next();
            if(desigTO.getActiveInd().equalsIgnoreCase("y"))
            {
                activeGradeList.add(desigTO);
            }
        }   
        
        return activeGradeList;
    }    
    
      /**
     * This method used to Modify grade  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises   -l
     */
    public int processModifyGradeDetails(ArrayList List, int UserId) {
        int insertStatus = 0;
        insertStatus = designationDAO.doGradeDetailsModify(List, UserId);
        return insertStatus;
    }
    
    public ArrayList getActiveGradeList(int desigId) {
        ArrayList gradeList = new ArrayList();
        gradeList = designationDAO.getActiveGradeList(desigId);
        return gradeList;
    }    
    
}
