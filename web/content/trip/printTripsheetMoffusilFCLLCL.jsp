<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : RAM
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <title>PrintTripSheet</title>
    </head>
    <body>
        <style type="text/css">
            @media print {
                #printbtn {
                    display :  none;
                }
            }
        </style>
        <style type="text/css" media="print">
            @media print
            {
                @page {
                    margin-top: 0;
                    margin-bottom: 0;
                }
            } 
        </style>
        
        <script>

//            window.onunload = function() {
//                window.opener.location.reload();
//            }
        </script>
        <form name="enter" method="post">
            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            String billingPartyId = (String)request.getAttribute("billingPartyId");
            String companyId = (String)request.getAttribute("companyId");
            %>
            <div id="printContent">

                <br>
                <br>
                <br>
                <br>
                <br>
                <c:forEach items="${tripDetailsListFCL}" var="tripStatus">

                    <table align="center"  style="border-collapse: collapse;width:400px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                           background:url(images/dict-logoWatermark.png);
                           background-repeat:no-repeat;
                           background-position:center;
                           border:1px solid;
                           /*border-color:#CD853F;width:800px;*/
                           border:1px solid;
                           /*opacity:0.8;*/
                           /*font-weight:bold;*/
                           ">
                        <tr >
                            <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border" >
                                <table align="left" >
                                    <tr>
                                        
                                        <td align="left"/>
                                            <!--<img src="images/Chakiat Logo.png" width="190" height="70"/>-->
                                            <font size="2">&emsp;&emsp;&emsp;<b>GSTIN </b> 33AABFC5853E2ZS</font>                                           
                                        </td>
                                           

                                        <td style="padding-left: 180px; padding-right:42px;">
                                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;




                                    <center>
                                        
                                    <font face="verdana" size="4"><b>C.A Logistics India Pvt.Ltd.</b></font></center>

                                    <center>
                                        
                                        <font face="verdana" size="3" >40,Rajaji Salai, Chennai,Tamilnadu.
                                        </font><br><br>
                                       
                                    </center>
                            </td>
                            <td align="right">
                                <br>
                                <div id="externalbox" style="width:3in">
                                    <div id="inputdata" ></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    </td>
                    </tr>

                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse;border-right: solid #888 1px">
                            <tbody>
                                <tr>
                                    <td valign="top" width="55%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Consignor</b></font><br> <font face="verdana" size="3"> <br><c:out value="${tripStatus.consignorName}"/><br><font face="verdana" size="3" weight="50" ><c:out value='${tripStatus.consignorAddress}'/></font ><br><font face="verdana" size="3">GST IN:&nbsp;<c:out value="${tripStatus.consignorGST}" /></font>  <br>
                                        <br>
                                    </td>
                                    <td width="45%" style=";border: solid #666 1px;border-bottom: none;border-collapse: collapse ;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <font face="verdana" size="2">

                                        <br>
                                        <font face="verdana" size="2">&nbsp;&nbsp;<b>LR NO : &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;</b></font><font face="verdana" size="3"> 
                                        <% if ("1464".equals(companyId)) {%>
                                             <c:out value="${tripStatus.manualLrNumber}"/><br>
                                        <% }else{%>
                                            MAA
                                        
                                        <c:if test="${orderType == 'Moffusil-Export-FCL'}">
                                            EX 
                                        </c:if>
                                        <c:if test="${orderType == 'Moffusil-Import-FCL'|| 'Moffusil-Import-LCL'}">
                                            IM 
                                        </c:if>
                                        <c:if test="${orderType == 'Moffusil-ICD'}">
                                            IC
                                        </c:if> 
                                        /&nbsp;
                                        <%--<c:if test="${tripId <= '289'}">--%>
                                            <%--<c:out value="${tripStatus.consignmentId}"/>--%>
                                        <%--</c:if>--%>
                                        <%--<c:if test="${tripId >= '289'}">--%>
                                            <c:out value="${tripStatus.lrNumber}"/>
                                        <%--</c:if>--%>
                                        / 2018-19&nbsp; </font><br><% }%>
                                        <font face="verdana" size="2">
                                        <font face="verdana" size="2"><br>
                                        <font face="verdana" size="2">&nbsp;&nbsp;<b>LR Date :&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><font size="3"><c:out value="${preStartDate}"/>&nbsp; </font><br>
                                        <font face="verdana" size="2">
                                        <br>
                                        <!--&nbsp;&nbsp;<b>Booking No :</b></font><font face="verdana" size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;</b><c:out value="${tripStatus.tripCode}"/></font>-->
                                        <font size="2">
                                        &nbsp;&nbsp;<b>Cust Ref No :</b></font><font face="verdana" size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>&nbsp;</b><c:out value="${tripStatus.customerOrderReferenceNo}"/></font>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>  <font face="verdana" size="3"><b>&nbsp;Consignee</b></font>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<br>
                                        <br><font face="verdana" size="3"><c:out value="${tripStatus.consigneeName}"/><br><c:out value="${tripStatus.consigneeAddress}"/><br>GST IN :<c:out value="${tripStatus.consigneeGST}"/></font><br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">


                                        <br><font face="verdana" size="2">&emsp;<b>Goods Desc :</b></font>&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;</b><font face="verdana" size="3"><c:out value='${description}'/></font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Value Declared :</b></font>&emsp;&emsp;&nbsp;<font face="verdana" size="3"><b>&nbsp;&nbsp;</b>As per bill</font><br><br>

<!--                                        <font face="verdana" size="2.5">&emsp;<b>No.of Packages :</b></font><font face="verdana" size="3">&emsp;<b>&nbsp;&nbsp;&nbsp;</b><c:out value="${tripStatus.packageNos}"/> pks.</font><br><br>
                                        <font face="verdana" size="2.5">&emsp;<b>Volume :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><c:out value="${tripStatus.volumeDocNo}"/> CBM.</font><br><br>-->

                                        <font face="verdana" size="2">&emsp;<b>Gross Weight :</b></font><font face="verdana" size="3">&emsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;</b> <c:out value="${tripStatus.weight}"/> tonnage.&emsp;&emsp;</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>E-Way Bill No :</b></font><font face="verdana" size="3">&emsp;&emsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><c:out value="${tripStatus.ewayBillNo}"/></font><br><br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <%--
                                        <br><font face="verdana" size="3"><b>&nbsp;Delivery At</b></font><br>
                                        <c:if test="${tripStatus.movementTypeId == '13'|| tripStatus.movementTypeId == '15'}">
                                            <br><font face="verdana" size="3"><c:out value="${tripStatus.consigneeName}"/>,<br><c:out value="${tripStatus.consigneeAddress}"/>.</font><br>
                                        </c:if>
                                        <c:if test="${tripStatus.movementTypeId != '13'&& tripStatus.movementTypeId != '15'}">
                                            <br><font face="verdana" size="3"><c:out value="${tripStatus.deliveryAddress}"/>.</font><br>
                                        </c:if>
                                        <br>
                                        --%>
                                        <br> <font face="verdana" size="3"><b>&nbsp;Booking Office</b></font><br>
                                        
                                        <font face="verdana" size="2"><br>&nbsp;&nbsp;&nbsp;&nbsp;CHENNAI &emsp;&emsp; C.A LOGISTICS (I) Pvt Ltd</font><br>

                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        
                                        <!--<br><font face="verdana" size="2">&emsp;<b>Contact Person :</b></font><font face="verdana" size="3">&emsp;&emsp;<b></b>Mr.Siva</font><br><br>-->
                                        <font face="verdana" size="2">&emsp;<b>Mobile :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b>+91</font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Email :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;<b>&nbsp;</b> </font><br><br>
                                      
                                    </td>
                                </tr>

                                <tr>
                                    <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="2">&emsp;<b>Container No(s) :</b></font>
                                        <font face="verdana" size="3">&emsp;&emsp;&emsp;<b>&nbsp;&nbsp;&nbsp; </b><c:out value='${tripStatus.containerNumber}'/> &emsp; 
                                        <br>
                                        <br>
                                        <c:if test="${tripStatus.containerQuantity == 1}">
                                            <font face="verdana" size="2">&emsp;<b>Size :</b></font> 1*40
                                        </c:if>
                                        <c:if test="${tripStatus.containerQuantity == 2}">
                                           <font face="verdana" size="2">&emsp;<b>Size :</b></font> 1*20
                                        </c:if>
                                        <c:if test="${tripStatus.containerQuantity == 3}">
                                           <font face="verdana" size="2">&emsp;<b>Size :</b></font> 2*20 
                                        </c:if>
                                        
                                        </font>
                                        <br>
                                        <br>
                                        <font face="verdana" size="2">&emsp;<b>Seal No : </b></font><font face="verdana" size="3">&emsp;<c:out value='${sealNo}'/></font><br>
                                        <br>
                                        <br>
                                        <font face="verdana" size="2">&emsp;<b>Truck No : </b></font><font face="verdana" size="3">&emsp;<c:out value='${vehicleNo}'/>&nbsp;&nbsp;(<c:out value='${vehicleType}'/>)</font><br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        
                                        <!--<br><font size="2">&emsp;<b>Pkgs / Wgt / Cbm </b></font><font size="2">&emsp;&emsp;&emsp;<b>:&nbsp;</b><c:out value='${tripStatus.containerPkgs}'/>&nbsp;/<c:out value='${tripStatus.containerWgts}'/>&nbsp;/<c:out value='${tripStatus.containerVolume}'/></font><br><br>-->

                                        <font face="verdana" size="2">&emsp;<b>Driver Name :</b></font><font face="verdana" size="3">&emsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;</b>&nbsp;Mr.<c:out value='${driverName}'/> <%--<br>&nbsp; &nbsp;+91 <c:out value='${driverMobile}'/> --%></font><br><br>
                                        <font face="verdana" size="2">&emsp;<b>Driver Contact :</b></font><font face="verdana" size="3">&emsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;</b>+91 <c:out value='${driverMobile}'/> </font><br><br>
                                        <!--<br><font face="verdana" size="2">&emsp;<b>Route : &emsp; </b></font><font size="3"></b><c:out value='${routeInfo}'/> <%-- <c:out value='${tripStatus.from}'/>&nbsp; To &nbsp; <c:out value='${tripStatus.to}'/>--%></font><br><br>-->
                                        <br><font face="verdana" size="2">&emsp;<b>Movement Type : &emsp; </b></font><font size="3"></b><c:out value="${orderType}"/> <%-- <c:out value='${tripStatus.from}'/>&nbsp; To &nbsp; <c:out value='${tripStatus.to}'/>--%></font><br><br>
                                        <!--<br><font size="2">&emsp;<b>Route &emsp; :</b></font><font size="2">&emsp;<c:out value='${routeInfo}'/></font><br><br>-->
                                        <%--<font face="verdana" size="2">&emsp;<b>Terms :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>&nbsp;&nbsp;&nbsp;</b>Collect</font>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<br>
                                        <br> <font face="verdana" size="2">&emsp;<b>Type of Delivery :</b></font><font face="verdana" size="3">&nbsp;<b>&nbsp;&nbsp;&nbsp;</b> &nbsp;
                                         <% if ("1464".equals(companyId)) {%> 
                                          Urgent
                                          <%}else{%>
                                          Normal
                                          <% }%> 
                                       </font><br><br>
                                       --%>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2" valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                            <center><font face="verdana" size="5"><b><br>&nbsp;Route Interim :</b></font>&emsp;&emsp;<font face="verdana" size="5"><c:out value='${routeInfo}'/>
                            </center>
                            <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <font face="verdana" size="3"><b><br>&nbsp;List of Document</b></font><br><font face="verdana" size="3"><br>&nbsp;&nbsp;Invoice,&emsp; Packing List,&emsp; Bill of Entry.</font><br>
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="2.5">&emsp; <b>Remarks :<br></b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;<c:out value='${tripStatus.vehicleRemarks}'/></font><br>
                                        <br><br><br><br><br>
                                    </td>
                                </tr>

                                <tr>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="3">&nbsp;&nbsp;<b>Owners Risk<br></b></font><br><font face="verdana" size="3">
                                        &nbsp;&nbsp;&nbsp;&nbsp;Goods to be insured by party at Owners Risk.<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;Not Responsible for Breakage,Damages,etc.<br>
                                        &nbsp;&nbsp;&nbsp;&nbsp;Subject to Chennai Jurisdiction Only.<br>
                                        <br>
                                        <br>
                                        <br>
                                    </td>
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        
                                        <font face="verdana" size="3">&emsp;<b>Prepared By : &emsp;</b></font><font face="verdana" size="2"><b>C.A LOGISTICS (I)P LTD</b></font><br>
                                        <br>
                                        <br>
                                        <br>
                                        <font face="verdana" size="3"><br>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Authorised Signatory</font><br><br>
                                       
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td colspan="2" style=" border: solid #888 1px;border-bottom: solid #888 1px;border-collapse: collapse"style=" border: solid #888 1px;border-bottom: solid #888 1px;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br><font face="verdana" size="3">&nbsp;&nbsp;<b>Description :</b></font><br>
                                      <% if ("1464".equals(companyId)) {%> 
                                           
                                       <%}else{%>
                                        <br><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${orderType}"/><br></font>
                                        <%--&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;BOE NO :&emsp;<c:out value="${tripStatus.billOfEntry}"/><br>
                                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;BOE DATE :&emsp;<c:out value="${tripStatus.challanDate}"/>
                                        --%>
                                        <c:if test="${tripStatus.movementTypeId == '13'}">
                                            <br> &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;EMPTY VALIDITY :&emsp;<c:out value="${tripStatus.emptyDropTime}"/>
                                        </c:if>
                                        <%}%>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </table>
                        <!--Muthu....-:) -->
                </div>
            </c:forEach>
            <br>
            <center>
                
                <input type="button" id="printbtn" class="button"  value="Back" onClick="viewTripDetails();" />
                <input type="button" class="button" id="printbtn" value="Print" onClick="print('printContent');" />
                &nbsp;&nbsp;&nbsp;
                <c:if test="${tripDetailsList != null}">
                    <c:forEach items="${tripDetailsList}" var="tripStatus">
                        <c:if test="${tripStatus.tripStatusId < 8}">
                            <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripId}"/>&tripType=1&statusId=8"><input type="button" value="Start Trip"></a>
                            </c:if>
                        </c:forEach>
                    </c:if>

            </center>
            <br>
            <br>
        </form>
        <script type="text/javascript">
            function viewTripDetails() {
                document.enter.action = '/throttle/viewTripSheets.do?&statusId=10&tripType=1&admin=No';
                document.enter.submit();
            }
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            function get_object(id) {
                var object = null;
                if (document.layers) {
                    object = document.layers[id];
                } else if (document.all) {
                    object = document.all[id];
                } else if (document.getElementById) {
                    object = document.getElementById(id);
                }
                return object;
            }
            //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
            get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
            /* ]]> */
        </script>
    </body>
</html>