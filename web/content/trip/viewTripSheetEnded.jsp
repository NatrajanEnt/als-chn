
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>

<script type="text/javascript">
     function viewSettlementPrint(tripSheetId,vehicleId) {
        window.open('/throttle/viewDriverSettlementPrint.do?tripSheetId='+tripSheetId + '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function printTripAdvance(tripSheetId) {
        window.open('getExpensePrintDetails.do?tripId=' + tripSheetId + '&param=advance', 'PopupPage', 'height = 700, width = 900, scrollbars = yes, resizable = yes');
    }
    function printTripFuel(tripSheetId) {
        window.open('getExpensePrintDetails.do?tripId=' + tripSheetId + '&param=fuel', 'PopupPage', 'height = 700, width = 900, scrollbars = yes, resizable = yes');
    }
    function mailTriggerPage(tripId) {
        window.open('/throttle/viewTripDetailsMail.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewCurrentLocation(vehicleRegNo) {
        var url = ' http://dict.throttletms.com/api/vehicleonmap/map.php?uname=interem&group_id=5087&height=600&width=700&vehicle=' + vehicleRegNo;
        //alert(url);
        window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewTripDetails(tripId, vehicleId, tripType) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails1(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        document.tripSheet.action = '/throttle/viewVehicle.do?vehicleId=' + vehicleId;
        document.tripSheet.submit();
    }
    function viewTripInwardDetails(tripId, tripInwardPrint, menuId) {
        document.tripSheet.action = '/throttle/handleTripSheetPrint.do?tripSheetId=' + tripId + '&tripInwardPrint=' + tripInwardPrint + '&menuId=' + menuId;
        document.tripSheet.submit();
    }
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function setValues() {
        if ('<%=request.getAttribute("statusId")%>' != 'null') {
            document.getElementById('stsId').value = '<%=request.getAttribute("statusId")%>';
        }
        if ('<%=request.getAttribute("statusId")%>' != 'null') {
            document.getElementById('tripStatusId').value = '<%=request.getAttribute("statusId")%>';
        }
        if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
            document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
        }
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
            document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
        }
        if ('<%=request.getAttribute("cityFromId")%>' != 'null') {
            document.getElementById('cityFromId').value = '<%=request.getAttribute("cityFromId")%>';
        }
        if ('<%=request.getAttribute("cityFrom")%>' != 'null') {
            document.getElementById('cityFrom').value = '<%=request.getAttribute("cityFrom")%>';
        }
        if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
            document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
        }
        if ('<%=request.getAttribute("zoneId")%>' != 'null') {
            document.getElementById('zoneId').value = '<%=request.getAttribute("zoneId")%>';
        }

        if ('<%=request.getAttribute("tripSheetId")%>' != 'null') {
            document.getElementById('tripSheetId').value = '<%=request.getAttribute("tripSheetId")%>';
        }
        if ('<%=request.getAttribute("customerId")%>' != 'null') {
            document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
        }
//            if('<%=request.getAttribute("tripStatusId")%>' != 'null' ){
//                document.getElementById('tripStatusId').value= '<%=request.getAttribute("tripStatusId")%>';
//            }
        if ('<%=request.getAttribute("podStatus")%>' != 'null') {
            document.getElementById('podStatus').value = '<%=request.getAttribute("podStatus")%>';
        }

    }


    function submitPage() {
        //document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('stsId').value + "&param=search";
        document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('tripStatusId').value + "&param=search";
        document.tripSheet.submit();
    }

    function submitPage1() {
        document.tripSheet.action = '/throttle/viewTripSheets.do?param=exportExcel';
        document.tripSheet.submit();
    }
    function changeVehicleAfterTripStart(tripId, vehicleId, tripType) {
        document.tripSheet.action = '/throttle/changeVehicleAfterTripStart.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId;
        document.tripSheet.submit();
    }


</script>

<script type="text/javascript">
    var httpRequest;
    function getLocation() {
        var zoneid = document.getElementById("zoneId").value;
        if (zoneid != '') {

            // Use the .autocomplete() method to compile the list based on input from user
            $('#cityFrom').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getLocationName.do",
                        dataType: "json",
                        data: {
                            cityId: request.term,
                            zoneId: document.getElementById('zoneId').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#cityFromId').val(tmp[0]);
                    $('#cityFrom').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        }
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.PrimaryOperation22"  text="Trip Sheet"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.PrimaryOperation"  text="PrimaryOperation"/></a></li>
            <li class="active"><c:out value="${menuPath}"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(5);
                    setValues();">
                <form name="tripSheet" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>

                    <table class="table table-info mb30 table-hover"  align="left" style="width:70%;">

                        <tr style="display:none;" >
                            <td>Billing Type</td>
                            <td>
                                <select name="billingType" id="billingType" class="form-control"  >
                                    <option value="" selected>--Select--</option>
                                    <option value="1" >Primary</option>
                                    <option value="2" >Secondary</option>
                                    <option value="3" >All</option>
                                </select>
                            </td>
                            <td>Vehicle No</td>
                            <td>
                                <select name="vehicleId" id="vehicleId" class="form-control"  >
                                    <c:if test="${vehicleList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${vehicleList}" var="vehicleList">
                                            <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>Trip Sheet No</td>
                            <td>
                                <input name="tripSheetId" id="tripSheetId" type="text" class="form-control"   value="<c:out value="${tripSheetId}"/>"  >
                            </td>

                            <td>Vehicle Type</td>
                            <td>
                                <select name="vehicleTypeId" id="vehicleTypeId" class="form-control"  >
                                    <c:if test="${vehicleTypeList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${vehicleTypeList}" var="vehicleTypeList">
                                            <option value='<c:out value="${vehicleTypeList.typeId}"/>'><c:out value="${vehicleTypeList.typeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr style="display:none;" >
                            <td>Fleet Center</td>
                            <td>
                                <select name="fleetCenterId" id="fleetCenterId" class="form-control"   >
                                    <c:if test="${companyList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${companyList}" var="companyList">
                                            <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>Zone</td>
                            <td>
                                <select name="zoneId" id="zoneId" class="form-control"   onchange="getLocation();" >
                                    <c:if test="${zoneList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${zoneList}" var="zoneList">
                                            <option value='<c:out value="${zoneList.zoneId}"/>'><c:out value="${zoneList.zoneName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>Location</td>
                            <td>
                                <input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityName}"/>" class="form-control" >
                                <input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityId}"/>" class="form-control"  >

                            </td>
                            <td>Customer</td>
                            <td>
                                <select name="customerId" id="customerId" class="form-control"  >
                                    <c:if test="${customerList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${customerList}" var="customerList">
                                            <option value='<c:out value="${customerList.customerName}"/>'><c:out value="${customerList.customerName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr >
                            <td>From Date</td>
                            <td>
                                <input name="fromDate" id="fromDate" type="text" class="form-control datepicker" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>">
                            </td>

                            <td>To Date</td>
                            <td>
                                <input name="toDate" id="toDate" type="text" class="form-control datepicker"  onClick="ressetDate(this);" value="<c:out value="${toDate}"/>">
                            </td>
                            <td colspan="4" >&nbsp;</td>
                        </tr>
                        <tr style="display:none;" >
                            <td>Status</td>
                            <td>
                                <select name="tripStatusId" id="tripStatusId" class="form-control"  >
                                    <option value="0" selected>--Select--</option>
                                    <c:if test="${statusDetails != null}">                                        
                                        <c:forEach items="${statusDetails}" var="statusDetails">
                                            <option value='<c:out value="${statusDetails.statusId}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <td colspan="4" >&nbsp;</td>
                            <td>POD Status</td>
                            <td>
                                <select name="podStatus" id="podStatus" class="form-control"  >
                                    <option value="" selected>--Select--</option>
                                    <option value="0" >POD Uploaded</option>
                                    <option value="1" >POD not Uploaded</option>
                                </select>
                            </td>
                        </tr>

                        <tr >
                            <td colspan="8" align="center">
                                <input type="hidden"   value="<c:out value="${statusId}"/>" class="text" name="stsId" id="stsId">
                                <input type="hidden"   value="<c:out value="${tripType}"/>" class="text" name="tripType">
                                <input type="button"   value="Search" class="btn btn-success" name="Search" onclick="submitPage();" style="width:100px;height:30px;font-weight: bold;padding:1px;">

                                <input type="button" class="btn btn-success" name="ExportExcel" id="ExportExcel" onclick="submitPage1(this.name);" value="ExportExcel" style="width:100px;height:30px;font-weight: bold;padding:1px;">
                                <c:if test="${statusId != 100}">
                                    &nbsp;
                                </c:if>
                        </tr>
                    </table>
                    <% int index = 1;%>
                    <c:if test="${tripDetails == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                        <c:if test="${tripDetails != null}">

                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead >
                                <tr >
                                    <th>Sno</th>
                                    <th>Trip Code</th>
                                    <th>Order Type </th>
                                    <th>PNR No</th>
                                    <th>VesselName</th>
                                    <th>Port</th>
                                    <th>Transporter</th>
                                    <th>Vehicle No </th>
                                    <th>Vehicle Type </th>
                                    <th>Container Qty </th>
                                    <th>Container No(s) </th>
                                    <th>Driver Name</th>
                                    <th>Mobile</th>
                                    <th>TripEndDate&Time</th>
                                    <th>Print</th>
                                </tr>
                            </thead>
                            <tbody>

                                <c:forEach items="${tripDetails}" var="tripDetails">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td class="form-control" >
                                            <%=index%>
                                        </td>
                                        <td class="form-control"  >
                                            <c:if test="${tripDetails.oldVehicleNo != null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                            <c:if test="${tripDetails.oldVehicleNo == null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 0);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                        </td>
                                        <td class="form-control" align="center">

                                            <c:out value="${tripDetails.movementTypeName}"/>
                                        </td>
                                        <td class="form-control" >
                                            <c:out value="${tripDetails.customerOrderReferenceNo}"/>
                                        </td>
                                        <td class="form-control" >
                                            <c:out value="${tripDetails.linerId}"/>
                                        </td>
                                        <td class="form-control" >
                                            <c:if test="${tripDetails.routeNameStatus > '2'}">
                                                <c:out value="${tripDetails.portName}"/>
                                            </c:if>
                                            <c:if test="${tripDetails.routeNameStatus <= '2'}">
                                                <%--<c:out value="${tripDetails.routeInfo}"/>--%>
                                                <c:out value="${tripDetails.portName}"/>
                                            </c:if>
                                        </td>
                                        <td class="form-control" ><c:out value="${tripDetails.vehicleVendor}"/></td>
                                        <td class="form-control" >
                                            <a href="#" onclick="viewVehicleDetails(<c:out value="${tripDetails.vehicleId}"/>)"><c:out value="${tripDetails.vehicleNo}"/></a><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font>
                                        </td>
                                        <td class="form-control" ><c:out value="${tripDetails.vehicleTypeName}"/></td>

                                        <td class="form-control" >
                                            <c:if test = "${tripDetails.containerQuantity == 1}" >
                                                1 * 40 
                                            </c:if> 
                                            <c:if test = "${tripDetails.containerQuantity == 2}" >
                                                1 * 20 
                                            </c:if> 
                                            <c:if test = "${tripDetails.containerQuantity == 3}" >
                                                2 * 20 
                                            </c:if> 
                                        </td>
                                        <td class="form-control" >
                                            <c:if test="${tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">

                                                <c:out value="${tripDetails.containerNo}"/><br>
                                            </c:if>
                                        </td>
                                        <td class="form-control" ><c:out value="${tripDetails.driverName}"/><br><font color="red"><c:out value="${tripDetails.oldDriverName}"/></font></td>
                                        <td class="form-control" ><c:out value="${tripDetails.mobileNo}"/></font></td>
                                        <td class="form-control" ><c:out value="${tripDetails.expectedArrivalDate}"/> &nbsp;<br> <c:out value="${tripDetails.expectedArrivalTime}"/></td>
                                        <td class="form-control" >
                                             <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>','<c:out value="${tripDetails.vehicleId}" />')" >
                                                            <br>  Settlement Print
                                                        </a>
<!--                                            <a href="#" onclick="viewTripInwardDetails('<c:out value="${tripDetails.tripSheetId}"/>', 'Y', 1);">Print</a>-->
                                        </td>
                                    </tr>

                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="loader"></div>

                    <div id="controls"  >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>

                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $(".clsr").click(function() {

                                document.getElementById("spinner").style.display = "block";
                                document.getElementById("loader").style.display = "block";
                                document.getElementById('loader').className += 'ui-loader-background';
                            });
                        });
                        function GenTripGr(tripSheetId) {
                            $.ajax({
                                url: "/throttle/handleTripGRGeneration.do",
                                dataType: "text",
                                data: {
                                    tripId: tripSheetId

                                },
                                success: function(temp) {
                                    // alert(data);
                                    if (temp != '') {
                                        alert("GR Generated Successfully!!");

                                    } else {
                                        alert('Something Went wrong! Please check.');
                                    }
                                }
                            });

                        }
                        function printTripGrn(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripChallan(tripSheetId) {
                            window.open('/throttle/handleTripChallanPrint.do?tripSheetId=' + tripSheetId, 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                    </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

