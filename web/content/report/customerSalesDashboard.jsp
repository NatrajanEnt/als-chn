<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.SimpleDateFormat" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<script type="text/javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<pre id="tsv" style="display:none"></pre>
<div class="pageheader">
    <h2><i class="fa fa-home"></i> Tues Volume
        <!--<span>Operations</span>-->
    </h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Customer Sales Report</li>
            <!--<li class="active">Operations</li>-->
        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="trip" method="post">
                    <br>
                    <div>   
                        <table class="table table-info mb30 table-hover"  border="1" align= "center" style="width:80%;">
                            <thead>
                                <tr >
                                    <th rowspan="2" >Company Name</th>
                                    <th colspan="2" style="text-align: center">Export</th>
                                    <th colspan="2" style="text-align: center">Import</th>
                                    <th colspan="2" style="text-align: center">Total</th>
                                </tr>
                                <tr>
                                <th>20 Ft </th>
                                <th>40 Ft </th>
                                <th>20 Ft </th>
                                <th>40 Ft </th>
                                <th>20 Ft </th>
                                <th>40 Ft </th>
                            </thead>
                            </tr>
                            <c:set var="cust20Ft" value="${0 }"/>
                            <c:set var="cust40Ft" value="${0 }"/>
                            <c:set var="total20Ft" value="${0 }"/>
                            <c:set var="total40Ft" value="${0 }"/>
                                <c:if test="${customerSalesReport != null}">
                                <c:forEach items="${customerSalesReport}" var="custSale">
                                <tr>
                                    <td><c:out value="${custSale.customerName}"/></td>
                                    <td><c:out value="${custSale.importFCL}"/></td>
                                    <td><c:out value="${custSale.importLCL}"/></td>
                                    <td><c:out value="${custSale.c20F}"/></td>
                                    <td><c:out value="${custSale.c40F}"/></td>
                                     <c:set var="cust20Ft" value="${custSale.importFCL + custSale.c20F}"/>
                                     <c:set var="cust40Ft" value="${custSale.importLCL + custSale.c40F}"/>
                                    <td><c:out value="${cust20Ft}"/></td>
                                    <td><c:out value="${cust40Ft}"/></td>
                                       
                                    </tr>
                                         <c:set var="total20Ft" value="${total20Ft+cust20Ft }"/>
                                        <c:set var="total40Ft" value="${total40Ft+cust40Ft }"/>
                                </c:forEach>
                                         <tr>
                                        <td colspan="5" style="text-align: right"> <b>Total</b></td>
                                        <td>&emsp;&emsp;<c:out value="${total20Ft}"/></td>
                                        <td><c:out value="${total40Ft}"/></td>
                                    </tr>
                            </c:if>
                        </table>

                    </div>   

                </form>
            </body>
        </div>
    </div>
</div>


<%@ include file="/content/common/NewDesign/settings.jsp" %>




