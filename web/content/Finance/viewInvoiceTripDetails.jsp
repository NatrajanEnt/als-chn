<%-- 
    Document   : viewInvoiceTripDetails
    Created on : 17 Feb, 2017, 10:52:08 AM
    Author     : pavithra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
        <%@page import="java.util.Locale"%>
    </head>
    <body>
        <form name="tripDetails" method="post" >
                
            <c:if test = "${tripDetails != null}" >
                <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                    <th>Sno</th>
                    <th>TripNo</th>
                    <th>Trip Code</th>
                    <th>Route</th>
                    <th>Trip Start Date</th>
                    <th>Trip End Date</th>
                    <th>Total Km</th>
                    <th>Status</th>
                    </thead>
                    <tbody>
                        <%
                                    int index = 1;%>
                        <c:forEach items="${tripDetails}" var="list"> 

                            <tr  height="40" > 
                                <td  height="20"><%=index%></td>
                                <td  height="20"><c:out value="${list.tripId}"/></td>
                                <td  height="20"><c:out value="${list.tripCode}"/></td>
                                <td  height="20"><c:out value="${list.route}"/></td>
                                <td  height="20"><c:out value="${list.startDate}"/></td>
                                <td  height="20"><c:out value="${list.endDate}"/></td>
                                <td  height="20"><c:out value="${list.totRunKM}"/></td>
                                <td  height="20"><c:out value="${list.statusName}"/></td>
                            </tr>
                            <%
                                index++;
                            %>
                        </c:forEach>
                    </tbody>
                </table>

            </c:if>
        </form>
    </body>
</html>
