
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


    <script type="text/javascript">
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            // alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>
    <script type="text/javascript">
        function setValues() {
            if ('<%=request.getAttribute("vendorId")%>' != 'null') {
                document.getElementById('vendorId').value = '<%=request.getAttribute("vendorId")%>';
            }
            if ('<%=request.getAttribute("fromDate")%>' != 'null') {
                document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
            }
            if ('<%=request.getAttribute("toDate")%>' != 'null') {
                document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
            }
        }
    </script>
    <script language="javascript">

        function submitPage() {
            document.invoice.action = '/throttle/handlePaymentPaidReport.do';
            document.invoice.submit();
        }

    </script>

 <style>
    #index td {
   color:white;
}
</style>
 <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.PaymentPaidReport"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.PaymentPaidReport"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
<body >
    <form name="invoice" action=""  method="post" class="form-horizontal form-bordered">
        <br>
<!--         <table class="table table-bordered">
              <tr height="30" id="index">
                             <td colspan="4"  style="background-color:#5BC0DE;"><b>Payment Paid Report</b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
            <tr>
                <td align="left" style="border-color:#5BC0DE;padding:16px;">-->
                         <table class="table table-info mb30 table-hover">
                             <thead><tr><th colspan="6"><spring:message code="finance.label.PaymentPaidReport"  text="default text"/></th></tr></thead>

                            <tr>
                                <td><spring:message code="finance.label.Vendor"  text="default text"/></td>
                                <td height="30">
                                    <select name="vendorId" id="vendorId"  class="form-control" style="width:260px;height:40px;">
                                            <c:if test="${vendorList != null}">
                                                <option value="" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                                <c:forEach items="${vendorList}" var="vendor">
                                                    <option value='<c:out value="${vendor.vendorId}"/>'><c:out value="${vendor.vendorName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById('vendorId').value = '<c:out value="${vendorId}"/>';
                                        </script>
                                </td>
                                <td><spring:message code="finance.label.FromDate"  text="default text"/></td>
                                <td height="30"><input name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>" type="text" class="datepicker" style="width:260px;height:40px;"></td>
                                <td><spring:message code="finance.label.ToDate"  text="default text"/></td>
                                <td height="30"><input name="toDate" id="toDate" value="<c:out value="${toDate}"/>" type="text" class="datepicker" style="width:260px;height:40px;"></td>
                                
                            </tr>
                            
                            <tr>
                                <td></td>  <td></td>  
                                <td align="right"><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="finance.label.Search"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;">
                                </td>  
                                <td><input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="finance.label.ExportExcel"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;">
                                <td>
                                   
                                </td>
                                <td>
                                   
                                </td>
                            </tr>
                        </table>
                </td>
            </tr>
        </table>
        <br>
        
           <c:if test="${invoiceList != null}">
            <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                        <th><spring:message code="finance.label.Code"  text="default text"/></th>
                        <th><spring:message code="finance.label.GrandTotal"  text="default text"/> </th>
                        <th><spring:message code="finance.label.PaidAmount"  text="default text"/> </th>
                        <th><spring:message code="finance.label.PendingAmount"  text="default text"/></th>

                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${invoiceList}" var="invoice">
                        
                        <c:if test="${invoice.payAmount > '0' }">
                            <tr height="30">
                                <td height="30"><%=index%></td>

                                <td><c:out value="${invoice.invoiceCode}"/></td>
                                <td><c:out value="${invoice.grandTotal}"/> </td>
                                <td><c:out value="${invoice.payAmount}"/> </td>
                                <td><c:out value="${invoice.pendingAmount}"/></td>

                            </tr>
                        </c:if>
                    <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
        </c:if>
        <c:if test="${invoiceList == null}">
            <center>
                <font color="red"><spring:message code="finance.label.NoRecordsFound"  text="default text"/></font>
            </center>
        </c:if>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>    
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>