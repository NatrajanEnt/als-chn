<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!--<html>-->
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>

    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
    <script src="/throttle/js/jquery.ui.core.js"></script>

</head>
<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            format: "dd-mm-yyyy",
            autoclose: true,
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        //	alert("cv");
        $(".datepicker").datepicker({
            format: "dd-mm-yyyy",
            autoclose: true
        });

    });


</script>

<script language="javascript">


    var httpReq;
    var temp = "";
    function ajaxData()
    {
        if (document.IssueReport.mfrId.value != '0') {
            var url = "/throttle/getModels1.do?mfrId=" + document.IssueReport.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function () {
                processAjax();
            };
            httpReq.send(null);
        }
    }
    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                setOptions(temp, document.IssueReport.modelId);
                if ('<%= request.getAttribute("modelId")%>' != 'null') {
                    document.IssueReport.modelId.value = <%= request.getAttribute("modelId")%>;
                }
            } else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }



    function submitPage() {
        var Item = document.getElementById("Item").value;
        var chek = validation();
        if (chek == 'true') {
            document.IssueReport.action = '/throttle/stockIssueReport.do?value=0&item=' + Item;
            document.IssueReport.submit();
        }
    }
    function validation() {
        if (document.IssueReport.companyId.value == 0) {
            alert("Please Select Data for Company Name");
            document.IssueReport.companyId.focus();
            return 'false';
        } else if (textValidation(document.IssueReport.fromDate, 'From Date')) {
            return 'false';
        } else if (textValidation(document.IssueReport.toDate, 'TO Date')) {
            return'false';
        }
        return 'true';
    }

    function setValues() {
        var poId = '<%=request.getAttribute("poId")%>';
        if ('<%=request.getAttribute("companyId")%>' != 'null') {
            document.IssueReport.companyId.value = '<%=request.getAttribute("companyId")%>';
            document.IssueReport.fromDate.value = '<%=request.getAttribute("fromDate")%>';
            document.IssueReport.toDate.value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("categoryId")%>' != 'null') {
            document.IssueReport.categoryId.value = '<%=request.getAttribute("categoryId")%>';
        }
        if ('<%=request.getAttribute("mfrId")%>' != 'null') {
            document.IssueReport.mfrId.value = '<%=request.getAttribute("mfrId")%>';
        }
        if ('<%=request.getAttribute("modelId")%>' != 'null') {
            ajaxData();
            document.IssueReport.modelId.value = '<%=request.getAttribute("modelId")%>';
        }
        if ('<%=request.getAttribute("mfrCode")%>' != 'null') {
            document.IssueReport.mfrCode.value = '<%=request.getAttribute("mfrCode")%>';
        }
        if ('<%=request.getAttribute("paplCode")%>' != 'null') {
            document.IssueReport.paplCode.value = '<%=request.getAttribute("paplCode")%>';
        }
        if ('<%=request.getAttribute("itemName")%>' != 'null') {
            document.IssueReport.itemName.value = '<%=request.getAttribute("itemName")%>';
        }
        if ('<%=request.getAttribute("regNo")%>' != 'null') {
            document.IssueReport.regNo.value = '<%=request.getAttribute("regNo")%>';
        }
        if ('<%=request.getAttribute("rcWorkorderId")%>' != 'null') {
            document.IssueReport.rcWorkorderId.value = '<%=request.getAttribute("rcWorkorderId")%>';
        }
        if ('<%=request.getAttribute("counterId")%>' != 'null') {
            document.IssueReport.counterId.value = '<%=request.getAttribute("counterId")%>';
        }

    }
    function getItemNames() {
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"), new ListSuggestions("itemName", "/throttle/handleItemSuggestions.do?"));

    }
    function getVehicleNos() {
//onkeypress='getList(sno,this.id)'
        var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
    }

    function toexcel() {

        document.IssueReport.action = '/throttle/handleStockIssueExcelRepNew.do';
        document.IssueReport.submit();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsstores.label.Stock Issue Report" text="Stock Issue Report"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsstores.label.FMSStores" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsstores.label.Stock Issue Report" text="Stock Issue Report"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">    

            <body onload="setValues();
                    getItemNames();
                    getVehicleNos();">
                <form name="IssueReport" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="6">Material Issue Report</th></tr></thead>

                        <!--            <table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                            </h2></td>
                            <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                            </tr>
                            <tr id="exp_table" >
                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                    <li style="background:#76b3f1">Material Issue Report</li>
                            </ul>-->
                        <div id="first">
                            <!--<table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">-->
                            <tr><input type="hidden" name="Item" id="Item" value="<c:out value="${Item}"/>">
                            <td ><font color="red">*</font>Location</td>
                            <td ><select  class="form-control" style="width:240px;height:40px"  name="companyId" >
                                    <option value="">-select-</option>
                                    <c:if test = "${LocationLists != null}" >
                                        <c:forEach items="${LocationLists}" var="company">
                                            <c:if test = "${company.cmpId== companyId}" >
                                                <option selected value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>

                                </select></td>

                            <td >&nbsp;&nbsp;MFR</td>
                            <td ><select name="mfrId" class="form-control" style="width:240px;height:40px">
                                    <option value="">-select-</option>
                                    <c:if test = "${MfrLists != null}" >
                                        <c:forEach items="${MfrLists}" var="mfr">
                                            <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                            </tr>
                            <tr>
                                <td width="114" >&nbsp;&nbsp;Category</td>
                                <td width="172" ><select name="categoryId" class="form-control" style="width:240px;height:40px">
                                        <option value="">-select-</option>
                                        <c:if test = "${Item == 1}" >
                                            <option value="1011">TYRES</option>
                                            <option value="1038">TYRES SPARE ITEMS</option>
                                        </c:if>
                                        <c:if test = "${Item != 1}" >
                                            <c:if test = "${categoryList != null}" >
                                                <c:forEach items="${categoryList}" var="category">
                                                    <option value="<c:out value="${category.categoryId}"/>"><c:out value="${category.categoryName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </c:if>


                                    </select></td>

                                <td>&nbsp;&nbsp;Company Code</td>
                                <td ><input name="paplCode" type="text" class="form-control" value="" style="width:240px;height:40px" autocomplete="off"></td>
                            </tr>
                            <tr>

                                <td width="80" >Workorder No</td>
                                <td width="80" ><input type="text" name="rcWorkorderId" class="form-control" style="width:240px;height:40px" autocomplete="off"></td>
                                <td>&nbsp;&nbsp;Vehicle No</td>
                                <td ><input name="regNo" id="regno" type="text" class="form-control" value="" style="width:240px;height:40px" autocomplete="off"></td>
                            </tr>
                            <tr>

                                <td ><font color="red">*</font>From Date</td>
                                <td ><input name="fromDate" type="text"  class="datepicker form-control"  style="width:240px;height:40px"  value="" autocomplete="off">
                                </td>
                                <td ><font color="red">*</font>To Date</td>
                                <td  ><input name="toDate" type="text"  class="datepicker form-control" style="width:240px;height:40px"  value="" autocomplete="off">
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td width="114" style="display:none">&nbsp;&nbsp;Item Name</td>
                                <td width="172" style="display:none"><input name="itemName" id="itemName" type="text"  class="form-control" style="width:240px;height:40px" value=""></td>

                                <td width="80" style="display:none">Counter No</td>
                                <td width="182" style="display:none"><input name="counterId" type="text" class="form-control" value="" style="width:240px;height:40px">
                                </td>
                            </tr>
                            <tr>
                                <td width="148">&nbsp;</td>
                                <td width="172" >&nbsp;
                                </td>

                                <td width="80" colpson="4"><input type="button"  value="Fetch Data" class="btn btn-success" name="Fetch Data" onClick="submitPage()">
                                    <input type="hidden" value="" name="reqfor"></td>
                            </tr>
                    </table>
                    <!--</div></div>-->
                    </td>
                    </tr>
                    </table>
                    <br>
                    <c:set var="totalValue" value="0"/>
                    <c:set var="totalProfit" value="0"/>
                    <c:set var="totalNettProfit" value="0"/>
                    <c:set var="totalUnbilledValue" value="0"/>
                    <c:set var="totalUnbilledProfit" value="0"/>
                    <c:set var="totalUnbilledNettProfit" value="0"/>
                    <c:set var="totalQty" value="0"/>
                    <c:set var="issQty" value="0"/>
                    <c:set var="retQty" value="0"/>
                    <c:set var="buyPrice" value="0"/>
                    <c:set var="totalPrice" value="0"/>
                    <c:if test = "${stokIssueList != null}" >


                        <% int index = 0; String type="Billed";%>
                        <c:forEach items="${stokIssueList}" var="issue">

                            <c:if test = "${issue.type == 'unbilled'}" >
                                <% 
                                if(type.equals("Billed") ){
                                    index = 0; type="UnBilled";
                                %></table><%
                                }
                                %>

                            </c:if>
                            <%if (index == 0){%>
                            <table class="table table-info mb30 table-hover">
                                <thead>
                                    <!--<table width="1200" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">-->
                                    <!--<tr><td colspan="23" align="center" ><b><%=type%></b></td></tr>-->
                                    <tr >
                                        <th >Sno</td>
                                        <th  >MFR</td>
                                        <th  >Model</td>
                                        <th  >REG NO</td>
                                        <th  >JOBCARD NO</td>
                                        <th  >RCWO NO</td>
                                        <th  >MRSNo </td>
                                        <th  >MANUALMRSNo </td>
                                        <th  >MRSDATE</td>
                                        <th  >IssueDATE</td>
                                        <th >Company CODE</td>
                                        <th  >ITEM NAME</td>
                                        <th >Tech Name</td>
                                        <th >User</td>
                                        <th  >Iss Qty</td>
                                        <th  >Ret Qty</td>
                                        <th  >Net Qty</td>
                                        <th  >BuyPrice</td>
                                        <th  >TotalPrice</td>


                                    </tr>
                                </thead>

                                <%
                                }
                                String classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                    classText = "text2";
                                } else {
                                    classText = "text1";
                                }
                                %>

                                <tr>
                                    <%--//For Generate Excel--%>
                                <input type="hidden" name="sno" value="<%=index+1%>">
                                <input type="hidden" name="mfrName" value="<c:out value="${issue.mfrName}"/>">
                                <input type="hidden" name="regNos" value="<c:out value="${issue.regNo}" />">
                                <input type="hidden" name="jobCardId" value="<c:out value="${issue.jobCardId}"/>">
                                <input type="hidden" name="rcWorkorderIds" value="<c:out value="${issue.rcWorkorderId}"/>">
        <!--                        <input type="hidden" name="counterIds" value="<c:out value="${issue.counterId}"/>">-->
                                <input type="hidden" name="counterIds" value="-">
                                <input type="hidden" name="mrsId" value="<c:out value="${issue.mrsId}"/>">
                                <input type="hidden" name="manualMrsNo" value="<c:out value="${issue.manualMrsNo}"/>">
                                <input type="hidden" name="createdDate" value="<c:out value="${issue.createdDate}"/>">
                                <input type="hidden" name="manualMrsDate" value="<c:out value="${issue.manualMrsDate}"/>">
                                <input type="hidden" name="paplCodes" value="<c:out value="${issue.paplCode}" />">
                                <input type="hidden" name="itemNames" value="<c:out value="${issue.itemName}"/>">
                                <input type="hidden" name="categoryName" value="<c:out value="${issue.categoryName}"/>">
                                <input type="hidden" name="itemType" value="<c:out value="${issue.itemType}"/>">
                                <input type="hidden" name="itemPrice" value="<c:out value="${issue.itemPrice}"/>">
                                <input type="hidden" name="itemQty" value="<c:out value="${issue.itemQty}"/>">

                                <td  ><%=index+1%></td>
                                <td  ><c:out value="${issue.mfrName}"/></td>
                                <td  ><c:out value="${issue.modelName}"/></td>
                                <td ><c:out value="${issue.regNo}" /></td>
                                <td  ><c:out value="${issue.jobCardId}"/></td>
                                <td  ><c:out value="${issue.rcWorkorderId}"/></td>
    <!--                            <td  ><c:out value="${issue.counterId}"/></td>-->
                                <td  ><c:out value="${issue.mrsId}"/></td>
                                <td  ><c:out value="${issue.manualMrsNo}"/></td>
                                <td  ><c:out value="${issue.manualMrsDate}"/></td>
                                <td  ><c:out value="${issue.issueDate}"/></td>
                                <td ><c:out value="${issue.paplCode}" /></td>
                                <td  ><c:out value="${issue.itemName}"/></td>

                                <td  ><c:out value="${issue.empName}"/></td>
                                <td  ><c:out value="${issue.user}"/></td>
                                <td   align="right" ><c:out value="${issue.issueQty}"/></td>
                                <td   align="right" ><c:out value="${issue.retQty}"/></td>
                                <td   align="right" ><c:out value="${issue.itemQty}"/></td>
                                <td   align="right" ><c:out value="${issue.buyPrice}"/></td>
                                <td   align="right" ><fmt:formatNumber value="${issue.buyPrice*issue.itemQty}" pattern="##.00"/>
                                </td>



<!--                         <td  ><c:out value="${issue.categoryName}"/></td>
                            <td  ><c:out value="${issue.itemType}"/></td>
                            <td  ><c:out value="${issue.itemPrice}"/></td>
                            <td  ><c:out value="${issue.itemQty}"/></td>-->

                                <c:if test = "${issue.type == 'billed'}" >
                                    <c:set var="totalValue" value="${(issue.sellPrice * issue.itemQty) + totalValue}" />
                                    <c:set var="totalProfit" value="${issue.profit + totalProfit}" />
                                    <c:set var="totalNettProfit" value="${issue.nettProfit + totalNettProfit}" />
                                </c:if>
                                <c:if test = "${issue.type == 'unbilled'}" >
                                    <c:set var="totalUnbilledValue" value="${(issue.sellPrice * issue.itemQty) + totalUnbilledValue}" />
                                    <c:set var="totalUnbilledProfit" value="${issue.profit + totalUnbilledProfit}" />
                                    <c:set var="totalUnbilledNettProfit" value="${issue.nettProfit + totalUnbilledNettProfit}" />
                                </c:if>
                                <c:set var="totalQty" value="${issue.itemQty + totalQty}" />
                                <c:set var="issueQty" value="${issue.issueQty + issueQty}" />
                                <c:set var="retQty" value="${issue.retQty + retQty}" />
                                <c:set var="buyPrice" value="${issue.buyPrice + buyPrice}" />
                                <c:set var="totalPrice" value="${(issue.buyPrice*issue.itemQty) + totalPrice}" />
                                </tr>
                                <% index++;%>
                        </c:forEach>
                        <c:if test = "${totalQty != 0}" >
                            <tr >
                                <td colspan="13"></td>
                                <td >Total</td>
                                <td align="right"><fmt:formatNumber value="${issueQty}" pattern="##.00"/></td>
                            <td align="right"><fmt:formatNumber value="${retQty}" pattern="##.00"/></td>
                            <td align="right"><fmt:formatNumber value="${totalQty}" pattern="##.00"/></td>
                            <td align="right"><fmt:formatNumber value="${buyPrice}" pattern="##.00"/></td>
                            <td align="right"><fmt:formatNumber value="${totalPrice}" pattern="##.00"/></td>

                            </tr>
                        </c:if>
                        </table>
                        <table class="table table-info mb30 table-hover" id="bg" style="display:none"><thead>
                                <tr><th colspan="23" align="left" >Billed</td></tr>
                                <tr>
                                    <td  colspan="14" >&nbsp;</td>
                                    <td  >&nbsp;</td>
                                    <td >&nbsp;</td>
                                    <td  >&nbsp;</td>
                                    <td  ><b>Total Selling Amount</b> </td>
                                    <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalValue}" pattern="##.00"/></b></td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr>
                            <tr>
                                <td  colspan="14" >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td  ><b>Total Profit</b> </td>
                                <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalProfit}" pattern="##.00"/></b>  </td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr>
                            <tr>
                                <td  colspan="14" >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td  ><b>Total Nett Profit</b> </td>
                                <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalNettProfit}" pattern="##.00"/></b>  </td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr<thead>
                                <tr><th colspan="23" align="left" >UnBilled</td></tr>
                                <tr>
                                    <td  colspan="14" >&nbsp;</td>
                                    <td  >&nbsp;</td>
                                    <td >&nbsp;</td>
                                    <td  >&nbsp;</td>
                                    <td  ><b>Total Selling Amount</b> </td>
                                    <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalUnbilledValue}" pattern="##.00"/></b></td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr>
                            <tr>
                                <td  colspan="14" >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td  ><b>Total Profit</b> </td>
                                <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalUnbilledProfit}" pattern="##.00"/></b>  </td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr>
                            <tr>
                                <td  colspan="14" >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td  ><b>Total Nett Profit</b> </td>
                                <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalUnbilledNettProfit}" pattern="##.00"/></b>  </td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr>

                            <thead>
                                <tr><th colspan="23" align="left" >Summary</td></tr>
                                <tr>
                                    <td  colspan="14" >&nbsp;</td>
                                    <td  >&nbsp;</td>
                                    <td >&nbsp;</td>
                                    <td  >&nbsp;</td>
                                    <td  ><b>Nett Selling Amount</b> </td>
                                    <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalValue + totalUnbilledValue}" pattern="##.00"/></b></td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr>
                            <tr>
                                <td  colspan="14" >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td  ><b>Total Profit</b> </td>
                                <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalProfit + totalUnbilledProfit}" pattern="##.00"/></b>  </td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr>
                            <tr>
                                <td  colspan="14" >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td >&nbsp;</td>
                                <td  >&nbsp;</td>
                                <td  ><b>Total Nett Profit</b> </td>
                                <td   align="right" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber value="${totalNettProfit + totalUnbilledNettProfit}" pattern="##.00"/></b>  </td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>
                            <td >&nbsp;</td>

                            </tr>
                        </table>
                        <br>
                        <center>
                            <input class="btn btn-success" type="button" value="ExportToExcel" onClick="toexcel();">
                        </center>
                    </c:if>
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

