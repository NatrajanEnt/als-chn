

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
  
    <script language="javascript">

        
        var httpReq;
        var temp = "";
        function ajaxData()
        {

            var url = "/throttle/getModels1.do?mfrId=" + document.mileage.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setOptions(temp, document.mileage.modelId);
                    setTimeout("fun()", 1);
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }
        
        

        function fun() {
            document.mileage.modelId.value = '<%=request.getAttribute("modelId")%>';
        }



        function setValues() {
            if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                document.mileage.vehicleTypeId.value = '<%=request.getAttribute("vehicleTypeId")%>';
            }
            ajaxData();
        }

        
        
        function submitPage() {
            var checValidate = selectedItemValidation();
//                document.mileage.action = '/throttle/saveVehicleMilleage.do';
//                document.mileage.submit();
        }
        function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var chec=0;
            for(var i=0;(i<index.length && index.length!=0);i++){
                if(index[i].checked){
                chec++;
                }
            }
            if(chec == 0){
            alert("Please Select Any One And Then Proceed");
            }else{
            document.mileage.action = '/throttle/saveVehicleMilleage.do';
            document.mileage.submit();
            }
        }
    </script>
      <div class="pageheader">
    <h2><i class="fa fa-edit"></i>Mileage Configuration</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Fuel</a></li>
            <li class="active">Mileage Configuration</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    
    <body onload="setValues();">
        <form name="mileage" method="post">
            <br>   
            <c:if test = "${milleageDetails!= null}">
                <table align="center" width="950" class="table table-info mb30 table-hover" border="0" id="table" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr >
                        <th>S no</th>                              
                        <th>Vehicle No</th>                              
                        <th>Empty Mileage</th>                              
                        <th>Below 10 Ton</th>                              
                        <th>10-18 Ton</th>                              
                        <th>18-21 Ton</th>                              
                        <th>Above 21 Ton</th>
                    </tr>
                    </thead>
                    <% int index = 0;
                    int sno = 1;%>
                    <c:forEach items="${milleageDetails}" var="fuel"> 
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "form-control";
                            } else {
                                classText = "form-control";
                            }
                            
                        %>	
                        <tr height="30">
                            <td ><%=sno%></td>                                                         
                            <td ><input type="hidden" name="vehicleId"  value='<c:out value="${fuel.vehicleId}"/>'><c:out value="${fuel.regNo}" /></td>                                                         
                            <td ><c:out value="${fuel.emptyMileage}" /></td>                                                         
                            <td ><c:out value="${fuel.belowTen}" /></td>                                                         
                            <td ><c:out value="${fuel.tenToEgtn}" /></td>                                                         
                            <td ><c:out value="${fuel.egtnToTwnty}" /></td>                                                         
                            <td ><c:out value="${fuel.aboveTwnty}" /></td>
                        </tr>
                        <%
                        index++;
                        sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
            <br>
            <center><input type="button" class="btn btn-info" value="Update Mileage" name="update" onclick="submitPage();" >
            </center><br>
            <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
            </script>
            <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50" >50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
            <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
            <br>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
     </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>