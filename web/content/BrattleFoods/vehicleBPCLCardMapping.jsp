<%--
    Document   : vehicleBPCLCardMapping
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : Arul
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript">
   $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script>



    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNoBPCLCardNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Vehicle No");
                        $('#vehicleNo').val('');
                        $('#vehicleId').val('');    
                        $('#vehicleNo').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.vehicleNo;
                var id = ui.item.vehicleId;
                var bpclCardNo = ui.item.bpclTransactionId;
                $('#vehicleNo').val(value);
                $('#vehicleId').val(id);
                 $('#bpclTransactionId').val(bpclCardNo);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.vehicleNo;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });




    //Update Vehicle Planning
    function setValues(vehicleId, vehicleNo, bpclTransactionId,fastTagTransactionId,sno) {
        var count = parseInt(document.getElementById("count").value);
        //document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("vehicleId").value = vehicleId;
        document.getElementById("vehicleNo").value = vehicleNo;
        document.getElementById("vehicleNo").readOnly = true;
        document.getElementById("bpclTransactionId").value = bpclTransactionId;
        document.getElementById("fastTagTransactionId").value = fastTagTransactionId;
    }

function submitPage(value) {
        if(value == 'Save'){
        if (document.getElementById("vehicleNo").value == '' || document.getElementById("vehicleId").value == '') {
            alert("Please Select Valid Vehicle No");
            document.getElementById("vehicleNo").focus();
        } else if (document.getElementById("bpclTransactionId").value == '') {
            alert("Please Enter BPCL Card No");
            document.getElementById("bpclTransactionId").focus();
        }else if (document.getElementById("fastTagTransactionId").value == '') {
            alert("Please Enter FastTag Card No");
            document.getElementById("fastTagTransactionId").focus();
        } else {
            $("#save").hide();
            document.vehicleBPCLCardMapping.action = '/throttle/saveVehicleBPCLCardMapping.do';
            document.vehicleBPCLCardMapping.submit();
        }
        }if(value == 'ExportExcel'){
            document.vehicleBPCLCardMapping.action = '/throttle/vehicleBPCLCardMapping.do?param=ExportExcel';
            document.vehicleBPCLCardMapping.submit();
        }
    }
    //savefunction
    function submitPageOld() {
        if (document.getElementById("vehicleNo").value == '' || document.getElementById("vehicleId").value == '') {
            alert("Please Select Valid Vehicle No");
            document.getElementById("vehicleNo").focus();
        } else if (document.getElementById("bpclTransactionId").value == '') {
            alert("Please Enter BPCL Card No");
            document.getElementById("bpclTransactionId").focus();
        }else if (document.getElementById("fastTagTransactionId").value == '') {
            alert("Please Enter FastTag Card No");
            document.getElementById("fastTagTransactionId").focus();
        } else {
            $("#save").hide();
            document.vehicleBPCLCardMapping.action = '/throttle/saveVehicleBPCLCardMapping.do';
            document.vehicleBPCLCardMapping.submit();
        }
    }
</script>
    
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Card Mapping"  text="Card Mapping"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Card Mapping"  text="Card Mapping"/></a></li>
            <!--<li class="active"><c:out value="${menuPath}"/></li>-->
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="document.vehicleBPCLCardMapping.vehicleNo.focus()">
        <form name="vehicleBPCLCardMapping"  method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%--<%@ include file="/content/common/message.jsp" %>--%>
            <br>
            <br>

            <table class="table table-info mb30 table-bordered">
                <input type="hidden" name="stChargeId" id="stChargeId" value=""  />
                <tr><thead>
                        <th  colspan="6" >Vehicle Cash Card Mapping</th>
                    </thead>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Vehicle No</td>
                    <td ><input type="hidden" name="vehicleId" id="vehicleId" class="form-control"  ><input type="text" name="vehicleNo" id="vehicleNo" class="form-control" ></td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>Fleet Card No</td>
                    <td ><input type="text" name="bpclTransactionId" id="bpclTransactionId" class="form-control"></td>
                    <td >&nbsp;&nbsp;<font color="red">*</font>FastTag Card No</td>
                    <td ><input type="text" name="fastTagTransactionId" id="fastTagTransactionId" class="form-control"></td>
                </tr>
                <tr>
                    <td  colspan="6" align="center"><input type="button" class="btn btn-success" value="Save" id="save" onClick="submitPage();"   />
                        &emsp;<input type="button" class="btn btn-success" name="ExportExcel"  value="<spring:message code="trucks.label.ExportExcel"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;"></td>
                </tr>
            </table>
            <br>
            
            <table class="table table-info mb30 table-bordered" id="table"  >
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Vehicle No </h3></th>
                        <th><h3>Fleet Card No</h3></th>
                        <th><h3>FastTag Card No</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${vehicleBPCLCardMapping != null}">
                        <c:forEach items="${vehicleBPCLCardMapping}" var="vehicleCard">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                <td class="<%=className%>"  align="left"><input type="hidden" name="vehicleIds" id="vehicleIds" value="<c:out value="${vehicleCard.vehicleId}" />"/> <c:out value="${vehicleCard.regNo}" /></td>
                                <td class="<%=className%>"  align="left"><input type="hidden" name="bpclCardIds" id="bpclCardIds" value="<c:out value="${vehicleCard.bpclTransactionId}" />"/>  <c:out value="${vehicleCard.bpclTransactionId}" /></td>
                                <td class="<%=className%>"  align="left"><input type="hidden" name="fastTagCardIds" id="fastTagCardIds" value="<c:out value="${vehicleCard.fastTagTransactionId}" />"/>  <c:out value="${vehicleCard.fastTagTransactionId}" /></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues('<c:out value="${vehicleCard.vehicleId}" />', '<c:out value="${vehicleCard.regNo}" />', '<c:out value="${vehicleCard.bpclTransactionId}" />', '<c:out value="${vehicleCard.fastTagTransactionId}" />', '<%=sno%>');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>