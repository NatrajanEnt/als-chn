/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.billing.data;

/**
 *
 * @author vinoth
 */
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.billing.business.BillingTO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import com.ibatis.sqlmap.client.SqlMapClient;

public class BillingDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "LoginDAO";

    public BillingDAO() {
    }

    public ArrayList getOrdersForBilling(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("fromDate", billingTO.getFromDate());
            map.put("toDate", billingTO.getToDate());
            if (!"".equals(billingTO.getCustomerName())) {
                billingTO.setCustomerName(billingTO.getCustomerName() + "%");
            }
            map.put("custName", billingTO.getCustomerName());
            map.put("userId", billingTO.getUserId());
            map.put("customerId", billingTO.getCustomerId());
            map.put("companyId", billingTO.getCompanyId());
            map.put("billOfEntry", billingTO.getBillOfEntryNo());
            map.put("shippingBillNo", billingTO.getShipingBillNo());
            map.put("billingType", billingTO.getBillingType());
            map.put("movementType", billingTO.getMovementType());
            System.out.println("closed Trip map is::" + map);
//            if(billingTO.getCustomerId() != null){
//            }
            clisedTrips = new ArrayList();
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersForBilling", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public ArrayList getRepoOrdersForBilling(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList clisedTrips = null;

        try {
            map.put("fromDate", billingTO.getFromDate());
            map.put("toDate", billingTO.getToDate());
            if (!"".equals(billingTO.getCustomerName())) {
                billingTO.setCustomerName(billingTO.getCustomerName() + "%");
            }
            map.put("custName", billingTO.getCustomerName());
            map.put("userId", billingTO.getUserId());
            map.put("customerId", billingTO.getCustomerId());
            map.put("billOfEntry", billingTO.getBillOfEntryNo());
            map.put("shippingBillNo", billingTO.getShipingBillNo());
            map.put("billingType", billingTO.getBillingType());
            map.put("movementType", billingTO.getMovementType());
            System.out.println("closed Trip map is::" + map);
//            if(billingTO.getCustomerId() != null){
//            }
            clisedTrips = new ArrayList();
            clisedTrips = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getRepoOrdersForBilling", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedTrips Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosedTrips List", sqlException);
        }
        return clisedTrips;
    }

    public ArrayList getOrdersToBeBillingDetails(BillingTO billingTO) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        String movementType = (String) getSqlMapClientTemplate().queryForObject("billing.getOrdersMovementType", map);
        System.out.println("movementType="+movementType);
       
        if("12".equals(movementType) || "13".equals(movementType)){
             map.put("movementType","1");
        }else {
            map.put("movementType","0");
        }
        
        ArrayList orderBillingDetails = new ArrayList();
        try {
            orderBillingDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrdersToBeBillingDetails", map);
            System.out.println("tripDetails size=" + orderBillingDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return orderBillingDetails;
    }

    public ArrayList getOrdersToBeBillingDetails1(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        ArrayList orderBillingDetails = new ArrayList();
        try {
            orderBillingDetails = (ArrayList) session.queryForList("billing.getOrdersToBeBillingDetails1", map);
            System.out.println("tripDetails size=" + orderBillingDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return orderBillingDetails;
    }

    public String getBillingTotalRevenue(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        String totalRevenue = "";
        try {
            totalRevenue = (String) session.queryForObject("billing.getBillingTotalRevenue", map);
            System.out.println("tripDetails size=" + totalRevenue);
            if (totalRevenue == null) {
                totalRevenue = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return totalRevenue;
    }

    public String getBillingTotalExpense(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);
        String totalExpense = "";
        try {
            totalExpense = (String) session.queryForObject("billing.getBillingTotalExpense", map);
            totalExpense= String.valueOf(Double.parseDouble(totalExpense)*Double.parseDouble("1.3"));
            System.out.println("totalExpense==="+totalExpense);
            System.out.println("tripDetails size=" + totalExpense);
            if (totalExpense == null) {
                totalExpense = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return totalExpense;
    }

    public ArrayList getOrderOtherExpenseDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getOrderOtherExpenseDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getOrderOtherExpenseDetails1(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) session.queryForList("billing.getOrderOtherExpenseDetails1", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripDetails(BillingTO billingTO) {
        Map map = new HashMap();
        map.put("invoiceId", billingTO.getInvoiceId());
        ArrayList tripDetails = new ArrayList();
        System.out.println("map:---" + map);
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getTripDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public int saveBillHeader(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();
        map.put("fromDate", billingTO.getFromDate());
        map.put("billingPartyId", billingTO.getBillingPartyId());
        map.put("userId", billingTO.getUserId());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("customerId", billingTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", billingTO.getNoOfTrips());
        map.put("grandTotal", billingTO.getGrandTotal());
        map.put("totalRevenue", billingTO.getTotalRevenue());
        map.put("totalExpToBeBilled", billingTO.getTotalExpToBeBilled());
        map.put("orderCount", billingTO.getOrderCount());
        map.put("remarks", billingTO.getRemarks());
        map.put("totalTax", billingTO.getTotalTax());
        map.put("customerAddress", billingTO.getCustomerAddress());
        map.put("otherExpense", billingTO.getOtherExpense());
        map.put("totalProfitMarginPercent", billingTO.getTotalProfitMarginPercent());
        map.put("profitMarginAmount", billingTO.getProfitMarginAmount());
        
        map.put("hireChrg", billingTO.getHireChrg());
        map.put("haltingChrg", billingTO.getHaltingChrg());
        map.put("loadUnloadChrg", billingTO.getLoadUnloadChrg());
        map.put("addChrg", billingTO.getAddChrg());
        
        map.put("finalBillAmount", (Double.parseDouble(billingTO.getHireChrg()) + Double.parseDouble(billingTO.getHaltingChrg()) + Double.parseDouble(billingTO.getLoadUnloadChrg()) + Double.parseDouble(billingTO.getAddChrg()) + Double.parseDouble(billingTO.getProfitMarginAmount()) ) );
        
        System.out.println("the saveBillHeader:" + map);
        int status = 0;
        try {
            status = (Integer) session.insert("billing.saveBillHeader", map);
            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillHeader List", sqlException);
        }

        return status;
    }

    public int saveBillDetails(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("userId", billingTO.getUserId());
        map.put("invoiceNo", billingTO.getInvoiceNo());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceId", billingTO.getInvoiceId());
        map.put("tripId", billingTO.getTripId());
        map.put("customerId", billingTO.getCustomerId());
        map.put("totalRevenue", billingTO.getEstimatedRevenue());
        map.put("totalExpToBeBilled", billingTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", String.valueOf( Double.parseDouble(billingTO.getExpenseToBeBilledToCustomer())));
//        map.put("grandTotal", String.valueOf(Double.parseDouble(billingTO.getEstimatedRevenue()) + Double.parseDouble(billingTO.getExpenseToBeBilledToCustomer())));
        map.put("otherExpense", billingTO.getOtherExpense());

        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) session.insert("billing.saveBillDetails", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetails List", sqlException);
        }

        return status;
    }

    public int saveBillTax(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", billingTO.getUserId());
        map.put("invoiceId", billingTO.getInvoiceId());
        try {
            if ("1".equalsIgnoreCase(billingTO.getOrganizationId()) && "".equalsIgnoreCase(billingTO.getGstNo()) && "9".equalsIgnoreCase(billingTO.getBillingState())) {
                map.put("gstType", "CGST");
                map.put("taxAmount", billingTO.getCgstAmount());
                map.put("taxPercentage", billingTO.getCgstPercentage());
                System.out.println("the tax map ccgst:" + map);
                status = (Integer) session.update("billing.saveInvoiceTax", map);
                map.put("gstType", "SGST");
                map.put("taxAmount", billingTO.getSgstAmount());
                map.put("taxPercentage", billingTO.getSgstPercentage());
                System.out.println("the tax map sgst:" + map);
                status = (Integer) session.update("billing.saveInvoiceTax", map);
            } else if ("1".equalsIgnoreCase(billingTO.getOrganizationId()) && "".equalsIgnoreCase(billingTO.getGstNo()) && !"9".equalsIgnoreCase(billingTO.getBillingState())) {
                map.put("gstType", "IGST");
                map.put("taxAmount", billingTO.getIgstAmount());
                map.put("taxPercentage", billingTO.getIgstPercentage());
                System.out.println("the tax map:" + map);
                status = (Integer) session.update("billing.saveInvoiceTax", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetails List", sqlException);
        }

        return status;
    }

    public int saveBillDetailExpense(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();

        map.put("userId", billingTO.getUserId());
        map.put("invoiceCode", billingTO.getInvoiceCode());
        map.put("invoiceDetailId", billingTO.getInvoiceDetailId());
        map.put("tripId", billingTO.getTripId());
        map.put("expenseId", billingTO.getExpenseId());
        map.put("expenseName", billingTO.getExpenseName() + "; " + billingTO.getExpenseRemarks());
        map.put("marginValue", billingTO.getMarginValue());
        map.put("taxPercentage", billingTO.getTaxPercentage());
        map.put("expenseValue", billingTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (billingTO.getMarginValue() == null || "".equals(billingTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = billingTO.getMarginValue();
        }
        if (billingTO.getTaxPercentage() == null || "".equals(billingTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = billingTO.getTaxPercentage();
        }
        if (billingTO.getExpenseValue() == null || "".equals(billingTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = billingTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) session.update("billing.saveBillDetailExpense", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillDetailExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillDetailExpense List", sqlException);
        }

        return status;
    }

    public int updateStatus(BillingTO billingTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        map.put("tripSheetId", billingTO.getTripSheetId());
        map.put("tripId", billingTO.getTripSheetId());
        map.put("userId", userId);
        map.put("statusId", billingTO.getStatusId());
        System.out.println("the updateEndTripSheet" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        int tripClosureCount = 0;
        try {

            map.put("KM", 0);
            map.put("HM", 0);
            map.put("remarks", "system update");
            status = (Integer) session.update("billing.insertTripStatus", map);
            status = (Integer) session.update("billing.updateTripStatus", map);

//            String ConsignmentIdList = (String) session.queryForObject("billing.getTripConsignment", map);
            ArrayList ConsignmentIdList = new ArrayList();
            ConsignmentIdList = (ArrayList) session.queryForList("billing.getTripConsignment", map);
            System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.size());
//            System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
//            String[] ConsignmentId = ConsignmentIdList.split(",");
            for (int i = 0; i < ConsignmentIdList.size(); i++) {
                map.put("consignmentId", ConsignmentIdList.get(i));

//            for (int i = 0; i < ConsignmentId.length; i++) {
//                map.put("consignmentId", ConsignmentId[i]);
                map.put("consignmentStatus", "13");
                System.out.println("the updated map:" + map);
                status = (Integer) session.update("billing.updateConsignmentStatus", map);
                System.out.println("the status1:" + status);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList viewBillForSubmission(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo) {
        Map map = new HashMap();
        ArrayList closedBillList = null;
        try {
            map.put("customerId", customerId);
            if (fromDate == null) {
                fromDate = "";
            }
            if (toDate == null) {
                toDate = "";
            }
            map.put("fromDate", fromDate);
            map.put("toDate", toDate);
            map.put("tripType", tripType);
            map.put("submitStatus", submitStatus);
            if (billNo != null && !"".equals(billNo)) {
                map.put("billNo", "%" + billNo + "%");
            } else {
                map.put("billNo", "");
            }
            if (grNo != null && !"".equals(grNo)) {
                map.put("grNo", "%" + grNo + "%");
            } else {
                map.put("grNo", "");
            }
            System.out.println("getClosedbill Value map" + map);
            closedBillList = (ArrayList) getSqlMapClientTemplate().queryForList("billing.viewBillForSubmission", map);
            System.out.println("getbil details size is ::::" + closedBillList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return closedBillList;
    }

    public int submitBill(String invoiceId, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("invoiceId", invoiceId);
            map.put("userId", userId);
            System.out.println("map::::" + map);
            status = (Integer) getSqlMapClientTemplate().update("billing.updateTripStatusForBillingSubmission", map);
            ArrayList billingTripIds = new ArrayList();
            billingTripIds = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInvoiceTripIds", map);
            if (billingTripIds.size() > 0) {
                Iterator itr = billingTripIds.iterator();
                BillingTO billingTO = new BillingTO();
                while (itr.hasNext()) {
                    billingTO = (BillingTO) itr.next();
                    map.put("tripId", billingTO.getTripId());
                    status = (Integer) getSqlMapClientTemplate().update("billing.saveTripStatusDetailsForBillingSubmission", map);
                }
            }
            status = (Integer) getSqlMapClientTemplate().update("billing.updateBillSubmitStatusFinanceHeader", map);

            System.out.println("status = " + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("submitBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "submitBill List", sqlException);
        }
        return status;
    }

    public int updateCancel(String billingNo, int userId) {
        Map map = new HashMap();
        map.put("billingNo", billingNo);
        map.put("userId", userId);
        System.out.println("the updateCancel" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("billing.updateInvoiceHeader", map);
            System.out.println("statusInvoiceHeaderUpdate" + status);

            status = (Integer) getSqlMapClientTemplate().update("billing.updateInvoiceDetail", map);
            System.out.println("statusInvoiceDetailsUpdate" + status);

            System.out.println("updateStatus size=" + tripDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getPreviewHeader(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList getPreviewHeader = null;
        try {
            String[] tripId = billingTO.getTripSheetIds().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getPreviewHeader Value map" + map);
            getPreviewHeader = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getPreviewHeader", map);
            System.out.println("getPreviewHeader size is ::::" + getPreviewHeader.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return getPreviewHeader;
    }

    public ArrayList getBillingDetailsForPreview(BillingTO billingTO) {
        Map map = new HashMap();
        ArrayList getBillingDetailsForPreview = null;
        try {
            String[] tripId = billingTO.getTripSheetIds().split(",");
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getBillingDetailsForPreview map" + map);
            getBillingDetailsForPreview = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getBillingDetailsForPreview", map);
            System.out.println("getBillingDetailsForPreview size is ::::" + getBillingDetailsForPreview.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCustomerDetails", sqlException);
        }
        return getBillingDetailsForPreview;
    }

    public String getInvoiceCodeSequence(SqlMapClient session) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        try {
            codeSequence = (Integer) session.insert("trip.getInvoiceCodeSequence", map);
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }
            System.out.println("getInvoiceCodeSequence=" + codeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public String getInvoiceCodeSequence1718(SqlMapClient session, int gstCheck) {
        Map map = new HashMap();
        int codeSequence = 0;
        String invoiceCode = "";
        try {
            if (gstCheck == 2) {
                codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequence1718", map);
            } else {
                codeSequence = (Integer) session.insert("billing.getInvoiceCodeSequenceGST", map);
            }
            invoiceCode = codeSequence + "";
            if (invoiceCode.length() == 1) {
                invoiceCode = "000000" + invoiceCode;
                System.out.println("invoice no 1.." + invoiceCode);
            } else if (invoiceCode.length() == 2) {
                invoiceCode = "00000" + invoiceCode;
                System.out.println("invoice lenght 2.." + invoiceCode);
            } else if (invoiceCode.length() == 3) {
                invoiceCode = "0000" + invoiceCode;
                System.out.println("invoice lenght 3.." + invoiceCode);
            } else if (invoiceCode.length() == 4) {
                invoiceCode = "000" + invoiceCode;
            } else if (invoiceCode.length() == 5) {
                invoiceCode = "00" + invoiceCode;
            } else if (invoiceCode.length() == 6) {
                invoiceCode = "0" + invoiceCode;
            }

            System.out.println("getInvoiceCodeSequence=" + invoiceCode);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return invoiceCode;
    }

    public ArrayList getGSTTaxDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("tripId", billingTO.getTripId());

        String[] tripId = billingTO.getTripSheetId().split(",");
        List tripIds = new ArrayList(tripId.length);
        for (int i = 0; i < tripId.length; i++) {
            System.out.println("value:" + tripId[i]);
            tripIds.add(tripId[i]);
        }
        map.put("tripIds", tripIds);

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getGSTTaxDetails", map);
            System.out.println("gstDetails size=" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public ArrayList getInvoiceGSTTaxDetails(BillingTO billingTO) {
        Map map = new HashMap();

        map.put("invoiceId", billingTO.getInvoiceId());

        ArrayList gstDetails = new ArrayList();
        try {
            gstDetails = (ArrayList) getSqlMapClientTemplate().queryForList("billing.getInvoiceGSTTaxDetails", map);
            System.out.println("gstDetails size=" + gstDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return gstDetails;
    }

    public int updateTripGst(String invoiceId, String[] tripIds, String[] tripIdIGSTAmount, String[] tripIdCGSTAmount, String[] tripIdSGSTAmount) {
        Map map = new HashMap();
        map.put("invoiceId", invoiceId);

        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            for (int i = 0; i < tripIds.length; i++) {
                map.put("tripId", tripIds[i]);
                map.put("tripIdIGSTAmount", tripIdIGSTAmount[i]);
                map.put("tripIdCGSTAmount", tripIdCGSTAmount[i]);
                map.put("tripIdSGSTAmount", tripIdSGSTAmount[i]);
                System.out.println("map...." + map);
                status = (Integer) getSqlMapClientTemplate().update("billing.updateInvoiceTripGST", map);
                System.out.println("updateInvoiceTripGST.." + status);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }
//  Tally
    
    public int updateTripInvoiceTally(BillingTO billingTO, SqlMapClient session) {
        Map map = new HashMap();
        map.put("billingPartyId", billingTO.getBillingPartyId());
        map.put("userId", billingTO.getUserId());
        map.put("companyId", billingTO.getCompanyId());
        map.put("invoiceId", billingTO.getInvoiceId());

         ArrayList invoiceDetails = new ArrayList();
         ArrayList tripDetails = new ArrayList();
         ArrayList invoiceAmountDetails = new ArrayList();
        System.out.println("the Tally saveBillHeader:" + map);
        int status = 0;
        try {
            
            invoiceDetails = (ArrayList) session.queryForList("billing.getInvoiceDetailsListForTally", map);
            System.out.println("invoiceDetails="+invoiceDetails.size());
            
            Iterator itr = invoiceDetails.iterator();
            BillingTO billingTO1 = new BillingTO();
            while (itr.hasNext()) {
                billingTO1 = new BillingTO();
                billingTO1 = (BillingTO) itr.next();      
                //  set Invoice Details
                map.put("tripId", billingTO1.getTripId());
                map.put("movementType", billingTO1.getMovementType());
                map.put("invoiceCode", billingTO1.getInvoiceCode());
                map.put("invoiceDate", billingTO1.getInvoiceDate());
                map.put("custName", billingTO1.getCustName());
                map.put("address1", billingTO1.getAddress1());
                map.put("address2", billingTO1.getAddress2());
                map.put("address3",  "Contact: "+billingTO1.getContact()+ " " +billingTO1.getAddress3());
                map.put("city", billingTO1.getCity());
                map.put("state", billingTO1.getState());
                map.put("panNo", billingTO1.getPanNo());
                map.put("gstNo", billingTO1.getGstNo());
                map.put("hireAmt", billingTO1.getHireAmt());
                map.put("loadingAmt", billingTO1.getLoadingAmt());
                map.put("haltingAmt", billingTO1.getHaltingAmt());
                map.put("additionalAmt", billingTO1.getAdditionalAmt());
                map.put("billAmount", billingTO1.getBillAmount());
                System.out.println("map one is---"+map);
                System.out.println("billingTO1.getMovementType()===="+billingTO1.getMovementType());
                            //                get trip details
                if("12".equals(billingTO1.getMovementType()) || "13".equals(billingTO1.getMovementType())){
                    tripDetails = (ArrayList) session.queryForList("billing.getTripDetailsForTallyFCL", map);
                    System.out.println(" getTripDetailsForTallyFCL= " + tripDetails.size());
                }else{
                    tripDetails = (ArrayList) session.queryForList("billing.getTripDetailsForTallyLCL", map);
                    System.out.println(" getTripDetailsForTallyLCL= " + tripDetails.size());
                }
                
                Iterator itr1 = tripDetails.iterator();
                BillingTO billingTO2 = new BillingTO();
                while (itr1.hasNext()) {
                    billingTO2 = new BillingTO();
                    billingTO2 = (BillingTO) itr1.next();  
                    map.put("tripNo", billingTO2.getTripCode());
                    map.put("tripDate", billingTO2.getTripDate());
                    map.put("vehicleNo", billingTO2.getVehicleNo());
                    map.put("driverName", billingTO2.getDriverName());
                    map.put("licenseNo", billingTO2.getLicenseNo());
                    map.put("lrNumber", billingTO2.getLrNumber());
                    map.put("containerNo", billingTO2.getContainerNo());
                    map.put("containerPkgs", billingTO2.getContainerPkgs());
                    map.put("containerWgts", billingTO2.getContainerWgts());
                    map.put("containerVolume", billingTO2.getContainerVolume());
                    map.put("transactionType", "Hire Charges"); 
                    
                     if("1".equals(billingTO2.getContainerTypeId())){
                        map.put("containerType", "20"); 
                    }else if("2".equals(billingTO2.getContainerTypeId())){
                        map.put("containerType", "40"); 
                    }else{
                         map.put("containerType", "0"); 
                    }
                     
                    if("12".equals(billingTO1.getMovementType())){
                        map.put("transportType", "EXP");
                        map.put("cargoType", "FCL");
                    }
                    if("13".equals(billingTO1.getMovementType())){
                        map.put("transportType", "IMP");
                        map.put("cargoType", "FCL");
                    }
                    if("14".equals(billingTO1.getMovementType())){
                        map.put("transportType", "IMP");
                        map.put("cargoType", "LCL");
                    }
                    if("15".equals(billingTO1.getMovementType())){
                        map.put("transportType", "ICD");
                        map.put("cargoType", "ICD");
                    }
                    if("16".equals(billingTO1.getMovementType())){
                        map.put("transportType", "EXP");
                        map.put("cargoType", "LCL");
                    }
                    
                    System.out.println("map ieees rrr=="+map);
                    
                }
                 
                System.out.println("map for tally Master:" + map);
                int inserTally = 0;
//                inserTally = (Integer) session.insert("billing.insertTallyInvoiceList", map);
                System.out.println("inserTally=invoice="+inserTally);
                map.put("tripListId",inserTally);
                
//                status = (Integer) session.insert("billing.insertTallyInvoiceVehicle", map);
                System.out.println("invoice-1-"+status);
                
//                status = (Integer) session.insert("billing.insertTallyInvoiceContainer", map);
                System.out.println("invoice-2-"+status);
                
//                Seperate Charegese
                invoiceAmountDetails = (ArrayList) session.queryForList("billing.getInvoiceAmountsForTally", map);
//                invoiceAmountDetails = (ArrayList) session.queryForList("billing.getInvoiceAmountsForTallyAll", map);
                System.out.println(" start invoiceAmountDetails if= " + invoiceAmountDetails.size());          
                
                Iterator itr2 = invoiceAmountDetails.iterator();
                BillingTO billingTO3 = new BillingTO();
                while (itr2.hasNext()) {
                    billingTO3 = new BillingTO();
                    billingTO3 = (BillingTO) itr2.next();  
                    map.put("invoiceName", billingTO3.getInvoiceName() );
                    map.put("invoiceNameId", billingTO.getInvoiceId());
                    map.put("invoiceAmount", billingTO3.getInvoiceAmount());
                    
                    System.out.println("map is rrr=="+map);
                    
                    status = (Integer) session.insert("billing.insertTallyInvoiceNameMapping", map);
                    System.out.println("invoice-3-"+status);
                    
                    status = (Integer) session.insert("billing.insertTallyInvoiceAccDetails", map);
                    System.out.println("invoice-5-"+status);
                }
                   map.put("invoiceName", "Trip Invoice" );
                   String invoiceAmt = (String) session.queryForObject("billing.getInvoiceTotAmt", map);
                   System.out.println("invoiceAmt---"+invoiceAmt);
                   map.put("invoiceAmount", invoiceAmt);  
                   
                  status = (Integer) session.insert("billing.insertTallyInvoiceAccount", map);
                  System.out.println("invoice-4-"+status);
            }

            
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBillHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBillHeader List", sqlException);
        }

        return status;
    }

    

}
