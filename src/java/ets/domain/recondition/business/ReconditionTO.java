    /*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
-------------------------------------------------------------------------*/
/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/
package ets.domain.recondition.business;

import java.util.ArrayList;

public class ReconditionTO {
    private String[] addressSplit = null;
    String invoiceNo = "";
    String hsnCode = "";
    String cgst = "";
    String sgst = "";
    String igst = "";
    String lcgst = "";
    String lsgst = "";
    String ligst = "";
    String laborwithTax = "";
    String utgst = "";
    String gstName = "";
    String gstPercentage = "";
    String stateId = "";
    String vendorGst = "";
    String createdOn = "";
    String woNo = "";
   
    private ArrayList taxList = new ArrayList();
    
    
    
    
    
    String companyAddress = "";
    String companyCity = "";
    String companyPincode = "";
    String companyEmail = "";
    String GSTNo = "";
    int itemId = 0;
    String itemName = "";
    String activeInd = "";
    String billNo = "";
    String mfrCode = "";
    String paplCode = "";
    String rcNumber = "";
    String createdDate = "";
    String receiveDate = "";
    String materialCost = "";
    String materialCostExternal = "";
    String taxpercentage = "";
    String elapsedDays = "";
    String jobCardId = "";
    String vendorId = "";
    String vendorName = "";
    String remarks = "";
    String gdType = "";
    String rcQueueId = "";
    String companyId = "";
    String woId = "";
    String amount = "";
    String status = "";
    String rcItemId = "";
    String uomName = "";
    String rcstatus = "";
    String billAmount = "";
    String receivedDate = "";
    String[] rcNumbers = null;
    String[] rcItemIds = null;
    String[] itemAmounts = null;
    String[] rcStatuses = null;
    String[] itemIds = null;
    String[] vendorIds = null;
    String availability = "";
    String categoryId = "";
    String categoryName = "";
    String tyreNo = "";
    String tyreId = "";
    String vehicleId = "";
    String stkStatus = "";
    String rcDuration = "";
    String price = "";
    String positionId = "";   
    String billId = "";   
    String tax = "";   
    String sparesAmount = "";   
    String sparesWithTax = "";   
    String laborAmount = "";   
    String rcAmntWithOutTax = "";   
    String address = "";
    String phoneNo = "";
    String quantity = "";
    String regNo = "";
    String companyName = "";
    //Hari
    int rcWorkId=0;
    int empId= 0;
    int mrsId=0;
    String empName="";
    int userId=0;
    
    String sparesAmountExternal = "";
    String customerTypeId=null;
    String customerTypeName=null;
    ArrayList tyreNos=null;

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerTypeName() {
        return customerTypeName;
    }

    public void setCustomerTypeName(String customerTypeName) {
        this.customerTypeName = customerTypeName;
    }


    public String getSparesAmountExternal() {
        return sparesAmountExternal;
    }

    public void setSparesAmountExternal(String sparesAmountExternal) {
        this.sparesAmountExternal = sparesAmountExternal;
    }


    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }


    public int getMrsId() {
        return mrsId;
    }

    public void setMrsId(int mrsId) {
        this.mrsId = mrsId;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public int getRcWorkId() {
        return rcWorkId;
    }

    public void setRcWorkId(int rcWorkId) {
        this.rcWorkId = rcWorkId;
    }


    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getElapsedDays() {
        return elapsedDays;
    }

    public void setElapsedDays(String elapsedDays) {
        this.elapsedDays = elapsedDays;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getJobCardId() {
        return jobCardId;
    }

    public void setJobCardId(String jobCardId) {
        this.jobCardId = jobCardId;
    }

    public String getMfrCode() {
        return mfrCode;
    }

    public void setMfrCode(String mfrCode) {
        this.mfrCode = mfrCode;
    }

    public String getPaplCode() {
        return paplCode;
    }

    public void setPaplCode(String paplCode) {
        this.paplCode = paplCode;
    }

    public String getRcNumber() {
        return rcNumber;
    }

    public void setRcNumber(String rcNumber) {
        this.rcNumber = rcNumber;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }

    public String[] getItemIds() {
        return itemIds;
    }

    public void setItemIds(String[] itemIds) {
        this.itemIds = itemIds;
    }

    public String[] getRcNumbers() {
        return rcNumbers;
    }

    public void setRcNumbers(String[] rcNumbers) {
        this.rcNumbers = rcNumbers;
    }

    public String[] getVendorIds() {
        return vendorIds;
    }

    public void setVendorIds(String[] vendorIds) {
        this.vendorIds = vendorIds;
    }

    public String getRcQueueId() {
        return rcQueueId;
    }

    public void setRcQueueId(String rcQueueId) {
        this.rcQueueId = rcQueueId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getWoId() {
        return woId;
    }

    public void setWoId(String woId) {
        this.woId = woId;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRcItemId() {
        return rcItemId;
    }

    public void setRcItemId(String rcItemId) {
        this.rcItemId = rcItemId;
    }

    public String getUomName() {
        return uomName;
    }

    public void setUomName(String uomName) {
        this.uomName = uomName;
    }

    public String getBillAmount() {
        return billAmount;
    }

    public void setBillAmount(String billAmount) {
        this.billAmount = billAmount;
    }

    public String[] getItemAmounts() {
        return itemAmounts;
    }

    public void setItemAmounts(String[] itemAmounts) {
        this.itemAmounts = itemAmounts;
    }

    public String[] getRcItemIds() {
        return rcItemIds;
    }

    public void setRcItemIds(String[] rcItemIds) {
        this.rcItemIds = rcItemIds;
    }

    public String[] getRcStatuses() {
        return rcStatuses;
    }

    public void setRcStatuses(String[] rcStatuses) {
        this.rcStatuses = rcStatuses;
    }

    public String getRcstatus() {
        return rcstatus;
    }

    public void setRcstatus(String rcstatus) {
        this.rcstatus = rcstatus;
    }

    public String getReceivedDate() {
        return receivedDate;
    }

    public void setReceivedDate(String receivedDate) {
        this.receivedDate = receivedDate;
    }

    public String getGdType() {
        return gdType;
    }

    public void setGdType(String gdType) {
        this.gdType = gdType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTyreNo() {
        return tyreNo;
    }

    public void setTyreNo(String tyreNo) {
        this.tyreNo = tyreNo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getStkStatus() {
        return stkStatus;
    }

    public void setStkStatus(String stkStatus) {
        this.stkStatus = stkStatus;
    }

    public String getRcDuration() {
        return rcDuration;
    }

    public void setRcDuration(String rcDuration) {
        this.rcDuration = rcDuration;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

    public String getBillId() {
        return billId;
    }

    public void setBillId(String billId) {
        this.billId = billId;
    }

    public String getLaborAmount() {
        return laborAmount;
    }

    public void setLaborAmount(String laborAmount) {
        this.laborAmount = laborAmount;
    }

    public String getSparesAmount() {
        return sparesAmount;
    }

    public void setSparesAmount(String sparesAmount) {
        this.sparesAmount = sparesAmount;
    }

    public String getSparesWithTax() {
        return sparesWithTax;
    }

    public void setSparesWithTax(String sparesWithTax) {
        this.sparesWithTax = sparesWithTax;
    }

    public String getTax() {
        return tax;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getRcAmntWithOutTax() {
        return rcAmntWithOutTax;
    }

    public void setRcAmntWithOutTax(String rcAmntWithOutTax) {
        this.rcAmntWithOutTax = rcAmntWithOutTax;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTyreId() {
        return tyreId;
    }

    public void setTyreId(String tyreId) {
        this.tyreId = tyreId;
    }

    public ArrayList getTyreNos() {
        return tyreNos;
    }

    public void setTyreNos(ArrayList tyreNos) {
        this.tyreNos = tyreNos;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getMaterialCostExternal() {
        return materialCostExternal;
    }

    public void setMaterialCostExternal(String materialCostExternal) {
        this.materialCostExternal = materialCostExternal;
    }

    public String getTaxpercentage() {
        return taxpercentage;
    }

    public void setTaxpercentage(String taxpercentage) {
        this.taxpercentage = taxpercentage;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

   

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyCity() {
        return companyCity;
    }

    public void setCompanyCity(String companyCity) {
        this.companyCity = companyCity;
    }

    public String getCompanyPincode() {
        return companyPincode;
    }

    public void setCompanyPincode(String companyPincode) {
        this.companyPincode = companyPincode;
    }

    public String getCompanyEmail() {
        return companyEmail;
    }

    public void setCompanyEmail(String companyEmail) {
        this.companyEmail = companyEmail;
    }

    public String getGSTNo() {
        return GSTNo;
    }

    public void setGSTNo(String GSTNo) {
        this.GSTNo = GSTNo;
    }

    public String getMaterialCost() {
        return materialCost;
    }

    public void setMaterialCost(String materialCost) {
        this.materialCost = materialCost;
    }

    public String[] getAddressSplit() {
        return addressSplit;
    }

    public void setAddressSplit(String[] addressSplit) {
        this.addressSplit = addressSplit;
    }

    public String getHsnCode() {
        return hsnCode;
    }

    public void setHsnCode(String hsnCode) {
        this.hsnCode = hsnCode;
    }

    public String getCgst() {
        return cgst;
    }

    public void setCgst(String cgst) {
        this.cgst = cgst;
    }

    public String getSgst() {
        return sgst;
    }

    public void setSgst(String sgst) {
        this.sgst = sgst;
    }

    public String getIgst() {
        return igst;
    }

    public void setIgst(String igst) {
        this.igst = igst;
    }

    public String getUtgst() {
        return utgst;
    }

    public void setUtgst(String utgst) {
        this.utgst = utgst;
    }

    public String getGstName() {
        return gstName;
    }

    public void setGstName(String gstName) {
        this.gstName = gstName;
    }

    public String getGstPercentage() {
        return gstPercentage;
    }

    public void setGstPercentage(String gstPercentage) {
        this.gstPercentage = gstPercentage;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public ArrayList getTaxList() {
        return taxList;
    }

    public void setTaxList(ArrayList taxList) {
        this.taxList = taxList;
    }

    public String getVendorGst() {
        return vendorGst;
    }

    public void setVendorGst(String vendorGst) {
        this.vendorGst = vendorGst;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getWoNo() {
        return woNo;
    }

    public void setWoNo(String woNo) {
        this.woNo = woNo;
    }

    public String getLcgst() {
        return lcgst;
    }

    public void setLcgst(String lcgst) {
        this.lcgst = lcgst;
    }

    public String getLsgst() {
        return lsgst;
    }

    public void setLsgst(String lsgst) {
        this.lsgst = lsgst;
    }

    public String getLigst() {
        return ligst;
    }

    public void setLigst(String ligst) {
        this.ligst = ligst;
    }

    public String getLaborwithTax() {
        return laborwithTax;
    }

    public void setLaborwithTax(String laborwithTax) {
        this.laborwithTax = laborwithTax;
    }

   

   

      
}  
