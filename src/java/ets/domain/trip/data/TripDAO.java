/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.trip.business.TripTO;
import ets.domain.trip.business.PointTO;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;
import ets.domain.programmingfree.excelexamples.calculateDistance;
import ets.domain.users.business.LoginTO;
//
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;
import org.json.simple.parser.JSONParser;
import org.json.*;

//
import ets.domain.util.FPLogUtils;

import java.util.ArrayList;

import java.util.HashMap;

import java.util.Iterator;

import java.util.Map;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.text.DateFormat;

//import com.Service;
//import com.ServiceSoap;
//import com.UpdateOrderStatusResponse.UpdateOrderStatusResult;
//import com.CreateTMSAccountDetailResponse.CreateTMSAccountDetailResult;
//import com.ArrayOfTMSModel;
//import com.TMSModel;
import java.text.SimpleDateFormat;
import java.util.List;

//import com.efsapi.Service;
//import com.efsapi.ServiceSoap;
//import com.efsapi.UpdateOrderStatusResponse.UpdateOrderStatusResult;
import java.util.List;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripDAO extends SqlMapClientDaoSupport {

    /**
     * Creates a new instance of __NAME__
     */
    public TripDAO() {
    }
    private final static String CLASS = "TripDAO";

    /**
     * This method used to Get Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getConsignmentList(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        map.put("movementType", tripTO.getMovementType());

        ArrayList consignmentList = new ArrayList();
        try {
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentList", map);
            System.out.println("consignmentList size=" + consignmentList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return consignmentList;
    }

    public ArrayList getTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripDetails = new ArrayList();
        System.out.println("map:---" + map);
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getVehicleTypeContainerTypeRates(ArrayList consignmentList, ArrayList orderPointDetails) {
        Map map = new HashMap();
        String movementType = "";
        String productCategory = "";
        String contractId = "";
        Iterator itr = consignmentList.iterator();
        System.out.println(" consignmentList size iz" + consignmentList.size());
        TripTO tripTONew = new TripTO();
        int count = 0;
        while (itr.hasNext()) {
            tripTONew = new TripTO();
            tripTONew = (TripTO) itr.next();
            movementType = tripTONew.getMovementType();
            productCategory = tripTONew.getProductInfo();
            if (count == 0) {
                contractId = tripTONew.getRouteContractId() + "";
                count = count + 1;
            } else {
                contractId = contractId + "," + tripTONew.getRouteContractId() + "";
            }
        }
        System.out.println("contractId " + contractId);
        map.put("movementType", movementType);
        map.put("productCategory", productCategory);

        String firstPointId = "";
        String finalPointId = "";
        String point1Id = "";
        String point2Id = "";
        String point3Id = "";
        String point4Id = "";
        itr = orderPointDetails.iterator();
        int cntr = 0;
        int size = orderPointDetails.size();
        size--;
        System.out.println("size:" + size);
        PointTO pointTO = new PointTO();
        while (itr.hasNext()) {
            System.out.println("size:" + size + "  cntr:" + cntr);
            pointTO = new PointTO();
            pointTO = (PointTO) itr.next();
            if (cntr == 0) {
                firstPointId = pointTO.getPointId();
            } else if (cntr == size) {
                finalPointId = pointTO.getPointId();
            } else {
                if (cntr == 1) {
                    point1Id = pointTO.getPointId();
                }
                if (cntr == 2) {
                    point2Id = pointTO.getPointId();
                }
                if (cntr == 3) {
                    point3Id = pointTO.getPointId();
                }
                if (cntr == 4) {
                    point4Id = pointTO.getPointId();
                }
            }
            cntr++;
        }
        map.put("firstPointId", firstPointId);
        map.put("point1Id", point1Id);
        map.put("point2Id", point2Id);
        map.put("point3Id", point3Id);
        map.put("point4Id", point4Id);
        map.put("finalPointId", finalPointId);
        ArrayList vehicleTypeContainerTypeRates = new ArrayList();

        System.out.println("map:---" + map);
        //vehicleTypeId~containerTypeId~containerQty~loadedType
        String inputType1 = "1058~1~1~1,1058~1~1~2";
        String inputType = "1058~1~1~1,1059~2~1~1,1059~1~2~1,1058~1~1~2,1059~2~1~2,1059~1~2~2";
        String[] temp2 = inputType1.split(",");
        String[] temp3 = null;
        String[] temp = inputType.split(",");
        String[] temp1 = null;
        String rate = "";
        Double totalRate = 0.00;
        try {
            if (contractId.contains(",")) {
                String tempVar[] = contractId.split(",");
                for (int i = 0; i < temp2.length; i++) {
                    tripTONew = new TripTO();
                    temp3 = temp2[i].split("~");
                    totalRate = 0.00;
                    for (int i1 = 0; i1 < tempVar.length; i1++) {
                        map.put("vehicleTypeId", temp3[0]);
                        map.put("containerTypeId", temp3[1]);
                        map.put("containerQty", temp3[2]);
                        map.put("loadTypeId", temp3[3]);
                        map.put("contractId", tempVar[i1]);
                        Object rateObj1 = getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeContainerTypeRates", map);
                        if (rateObj1 != null) {
                            rate = (String) rateObj1;
                            totalRate = totalRate + Double.parseDouble(rate);
                            System.out.println("rate:" + rate);
                        }
                    }
                    tripTONew.setVehicleTypeId("1059");
                    tripTONew.setContainerTypeId("1");
                    tripTONew.setContainerQty("2");
                    tripTONew.setLoadType(temp3[3]);
                    System.out.println("map:" + map);
                    System.out.println(" totalRate" + totalRate);
                    tripTONew.setCurrRate(totalRate + "");
                    vehicleTypeContainerTypeRates.add(tripTONew);
                }
            } else {
                map.put("contractId", contractId);
                for (int i = 0; i < temp.length; i++) {
                    tripTONew = new TripTO();
                    temp1 = temp[i].split("~");

                    map.put("vehicleTypeId", temp1[0]);
                    map.put("containerTypeId", temp1[1]);
                    map.put("containerQty", temp1[2]);
                    map.put("loadTypeId", temp1[3]);
                    tripTONew.setVehicleTypeId(temp1[0]);
                    tripTONew.setContainerTypeId(temp1[1]);
                    tripTONew.setContainerQty(temp1[2]);
                    tripTONew.setLoadType(temp1[3]);
                    System.out.println("map:" + map);
                    Object rateObj = getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeContainerTypeRates", map);
                    System.out.println("rateObj:" + rateObj);
                    if (rateObj != null) {
                        rate = (String) rateObj;
                        System.out.println("rate:" + rate);
                        tripTONew.setCurrRate(rate);
                        vehicleTypeContainerTypeRates.add(tripTONew);
                    }
                }
            }
            System.out.println("vehicleTypeContainerTypeRates size=" + vehicleTypeContainerTypeRates.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeContainerTypeRates Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleTypeContainerTypeRates List", sqlException);
        }

        return vehicleTypeContainerTypeRates;
    }

    public ArrayList getTripsToBeBilledDetails(TripTO tripTO) {
        Map map = new HashMap();

        String[] tripIds = tripTO.getTripIds();
        List tripSheetIds = new ArrayList(tripIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        map.put("tripSheetIds", tripSheetIds);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsToBeBilledDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getSecTripsToBeBilledDetails(TripTO tripTO) {
        Map map = new HashMap();

        String[] tripIds = tripTO.getTripIds();
        List tripSheetIds = new ArrayList(tripIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        map.put("tripSheetIds", tripSheetIds);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecTripsToBeBilledDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripsOtherExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();

        String[] tripIds = tripTO.getTripIds();
        List tripSheetIds = new ArrayList(tripIds.length);
        for (int i = 0; i < tripIds.length; i++) {
            System.out.println("value:" + tripIds[i]);
            tripSheetIds.add(tripIds[i]);
        }
        map.put("tripSheetIds", tripSheetIds);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripsOtherExpenseDetails", map);
            System.out.println("tripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripsOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripsOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getInvoiceHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceHeader", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceDetails", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetails List", sqlException);
        }

        return result;
    }

    public ArrayList getInvoiceDetailExpenses(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceDetailExpenses", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailExpenses Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceDetailExpenses List", sqlException);
        }

        return result;
    }

    public ArrayList getTripPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripPointDetails = new ArrayList();
//        try {
        System.out.println("map for point:" + map);
        try {
            String movementType = (String) getSqlMapClientTemplate().queryForObject("trip.getTripMovementType", map);

            if ("8".equals(movementType)) {
                tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointDetailsDistance", map);
                System.out.println("getTripPointDetails size=" + tripPointDetails.size());
            } else {
                tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointDetails", map);
                System.out.println("getTripPointDetails size=" + tripPointDetails.size());
            }

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getTripLableDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripLableDetails = new ArrayList();
//        try {
        System.out.println("map for point:" + map);
        try {

            tripLableDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripLableDetails", map);
            System.out.println("tripLableDetails size=" + tripLableDetails.size());

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripLableDetails;
    }

    public ArrayList getOrderPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        map.put("movementType", tripTO.getMovementType());
        map.put("collectionBranch", tripTO.getCollectionBranch());
        System.out.println("map getOrderPointDetailss" + map);
        ArrayList tripPointDetails = new ArrayList();
        try {

            if (!"8".equals(tripTO.getMovementType())) {
                tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderPointDetails", map);
                System.out.println("getOrderPointDetails size=" + tripPointDetails.size());
            } else if ("8".equals(tripTO.getMovementType())) {
                tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDistanceOrderPointDetails", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getHubPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);

        ArrayList tripPointDetails = new ArrayList();
        try {
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getHubPointDetails", map);
            System.out.println("gethubPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getDropPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);

        ArrayList tripPointDetails = new ArrayList();
        try {
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDropPointDetails", map);
            System.out.println("getDropPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDropPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }
        return tripPointDetails;
    }

    public ArrayList getwareHouseDetails(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList wareHouseDetail = new ArrayList();
        try {
            wareHouseDetail = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getwareHouseDetail", map);
            System.out.println("wareHouseDetail size=" + wareHouseDetail.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDropPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }
        return wareHouseDetail;
    }

    public ArrayList getPortDetails(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList portDetail = new ArrayList();
        try {
            portDetail = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPortDetail", map);
            System.out.println("wareHouseDetail size=" + portDetail.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDropPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderPointDetails List", sqlException);
        }
        return portDetail;
    }

    public ArrayList getVehicleRegNos(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo();
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("vehicleNo = " + vehicleNo);
        map.put("vehicleNo", vehicleNo + "%");
//        if(tripTO.getVehicleNo() != null && !"".equals(tripTO.getVehicleNo())){
//        }else{
//        map.put("vehicleNo", "");
//        }
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        if ("1".equals(tripTO.getVendorId())) {
            map.put("ownerShip", "1");
            map.put("vendorId", "");
        } else {
            map.put("vendorId", tripTO.getVendorId());
            map.put("ownerShip", "");
        }
        if (tripTO.getVehicleTypeId() != null && !"".equals(tripTO.getVehicleTypeId())) {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId().split("-")[0]);
        } else {
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        }

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map: in the dao" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNos", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getTrailerNos(TripTO tripTO) {
        Map map = new HashMap();
        // String vehicleNo = tripTO.getVehicleNo();
        ArrayList trailerNos = new ArrayList();
        try {
            //   System.out.println("map: in the dao" + map);
            trailerNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTrailerNos", map);
            System.out.println("trailerNos size=" + trailerNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return trailerNos;
    }

    public ArrayList getTrailerNos() {
        Map map = new HashMap();
        // String vehicleNo = tripTO.getVehicleNo();
        ArrayList trailerNos = new ArrayList();
        try {
            //   System.out.println("map: in the dao" + map);
            trailerNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTrailerNos", map);
            System.out.println("trailerNos size=" + trailerNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return trailerNos;
    }

    public ArrayList getVehicleRegNosForEmptyTrip(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo();
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("vehicleNo = " + vehicleNo);
        map.put("vehicleNo", vehicleNo + "%");
//        if(tripTO.getVehicleNo() != null && !"".equals(tripTO.getVehicleNo())){
//        }else{
//        map.put("vehicleNo", "");
//        }
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map: in the dao" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNosForEmptyTrip", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getVehicleRegNosForUpload(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo() + "%";
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("vehicleNo", vehicleNo);
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else if ("2".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 1);
        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());

        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map:" + map);
            String tripStatus = "";
            String jobcardStatus = "";

            tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleRegNosForUploadStatus", map);
            System.out.println("vehicleStatus = " + tripStatus);
            if (tripStatus == null) {
                map.put("tripStatus", 0);
                jobcardStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getJobcardForUploadStatus", map);
                if (jobcardStatus == null) {
                    map.put("tripStatus", 2);
                } else {
                    map.put("tripStatus", 0);
                }
            } else {
                map.put("tripStatus", 1);
            }

            System.out.println("map = " + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleRegNosForUpload", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNosForUpload Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNosForUpload List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getDrivers(TripTO tripTO) {
        Map map = new HashMap();
        String driverName = tripTO.getDriverName() + "%";
        map.put("driverName", driverName);

        ArrayList drivers = new ArrayList();
        try {
            drivers = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDrivers", map);
            System.out.println("drivers size=" + drivers.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return drivers;
    }

    public String getConsignmentOrderRevenue(String orderId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String revenue = "";
        try {
            map.put("orderId", orderId);
            System.out.println("map = " + map);
            revenue = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderRevenue", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("revenue=" + revenue);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderRevenue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrderRevenue", sqlException);
        }
        return revenue;
    }

    public String getVehicleMileageAndTollRate(String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String response = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map...:" + map);
            response = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleMileageAndTollRate", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("response=" + response);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleMileageAndTollRate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleMileageAndTollRate", sqlException);
        }
        return response;
    }

    public String getEstimatedTripEndDateTime(String tripId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String response = "";
        try {
            map.put("tripId", tripId);
            response = (String) getSqlMapClientTemplate().queryForObject("trip.getEstimatedTripEndDateTime", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("response=" + response);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEstimatedTripEndDateTime Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEstimatedTripEndDateTime", sqlException);
        }
        return response;
    }

    public String checkPreStartRoute(String preStartLocationId, String originId, String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String info = "";
        try {
            map.put("preStartLocationId", preStartLocationId);
            map.put("originId", originId);
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            info = (String) getSqlMapClientTemplate().queryForObject("trip.checkPreStartRoute", map);
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("info=" + info);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkPreStartRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkPreStartRoute", sqlException);
        }
        return info;
    }

    public String getConsignmentOrderExpense(String orderId) {
        Map map = new HashMap();
        String expense = "";
        try {
            map.put("orderId", orderId);
            System.out.println("map:" + map);
            expense = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentOrderExpense", map);
            System.out.println("expense=" + expense);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrderExpense", sqlException);
        }
        return expense;
    }

    public String getTripCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getTripCodeSequence", map);
            System.out.println("tripCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getTripCodeSequence(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        map.put("companyId", tripTO.getCompanyId());
        System.out.println("MAp--" + map);
        try {
            tripCodeSequence = (Integer) session.insert("trip.getTripCodeSequence", map);
            System.out.println("tripCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getGrNoSequence(SqlMapClient session) {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) session.insert("trip.getGrNoSequence", map);
            System.out.println("getGrNoSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getGrNoSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getGrNoSequence", map);
            System.out.println("getGrNoSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getRepoGrNoSequence(SqlMapClient session) {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) session.insert("trip.getRepoGrNoSequence", map);
            System.out.println("getGrNoSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getRepoGrNoSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getRepoGrNoSequence", map);
            System.out.println("getGrNoSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getTripChallanSequence(SqlMapClient session) {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) session.insert("trip.getChallanNoSequence", map);
            System.out.println("challan No=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getTripChallanSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getChallanNoSequence", map);
            System.out.println("challan No=" + tripCodeSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getCnoteCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getCnoteCodeSequence", map);
            System.out.println("getCnoteCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public String getInvoiceCodeSequence() {
        Map map = new HashMap();
        int tripCodeSequence = 0;
        try {
            tripCodeSequence = (Integer) getSqlMapClientTemplate().insert("trip.getInvoiceCodeSequence", map);
            System.out.println("getInvoiceCodeSequence=" + tripCodeSequence);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getInvoiceCodeSequence", sqlException);
        }
        return tripCodeSequence + "";
    }

    public int saveTripSheet(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int tripId = 0;
        try {
            map.put("singleTwentyStatus", tripTO.getSingleTwentyStatus());
            map.put("shipingLineNo", tripTO.getShipingLineNo());
            map.put("billOfEntry", tripTO.getBillOfEntry());
            map.put("tripCode", tripTO.getTripCode());
            map.put("companyId", tripTO.getCompanyId());
            map.put("customerId", tripTO.getCustomerId());
            map.put("productInfo", tripTO.getProductInfo());
            map.put("orderExpense", tripTO.getOrderExpense());
            map.put("orderRevenue", tripTO.getOrderRevenue());
            map.put("profitMargin", tripTO.getProfitMargin());
            map.put("hireCharge", tripTO.getHireCharges());
            map.put("movementType", tripTO.getMovementType());
            map.put("clearanceType", tripTO.getClearanceType());
            map.put("containerQuantity", tripTO.getContainerQuantity());
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            //map.put("pointPlanDate", pointPlanDate[0]);
            //map.put("pointPlanTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);

//            map.put("tripScheduleDate", tripTO.getTripScheduleDate());
//            map.put("tripScheduleTime", tripTO.getTripScheduleTime());
            if (pointPlanDate != null) {
                map.put("tripScheduleDate", pointPlanDate[0]);
                map.put("startDate", pointPlanDate[0]);
                map.put("vehicleactreportdate", pointPlanDate[0]);
                map.put("vehicleloadreportdate", pointPlanDate[0]);
            } else {
                map.put("tripScheduleDate", "00-00-0000");
                map.put("startDate", "00-00-0000");
                map.put("vehicleactreportdate", "00-00-0000");
                map.put("vehicleloadreportdate", "00-00-0000");

            }
            if (pointPlanTimeHrs != null) {
                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
                map.put("startTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
                map.put("vehicleactreporttime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
                map.put("vehicleloadreporttime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            } else {
                map.put("tripScheduleTime", "00" + ":" + "00");
                map.put("startTime", "00" + ":" + "00");
                map.put("vehicleactreporttime", "00" + ":" + "00");
                map.put("vehicleloadreporttime", "00" + ":" + "00");
            }

            //map.put("tripScheduleTime", tripTO.getTripScheduleTimeHrs()+":"+tripTO.getTripScheduleTimeMins());
            map.put("statusId", tripTO.getStatusId());
            map.put("transitHours", tripTO.getTripTransitHours());
            map.put("transitDay", tripTO.getTripTransitDays());
            map.put("advnaceToBePaidPerDay", tripTO.getAdvnaceToBePaidPerDay());
            map.put("userId", tripTO.getUserId());
            map.put("emptyTripPurpose", tripTO.getEmptyTripPurpose());
            map.put("vehicleTypeName", tripTO.getVehicleTypeName());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
            map.put("cNotes", tripTO.getcNotes());
            map.put("billingType", tripTO.getBillingType());
            map.put("customerName", tripTO.getCustomerName());
            map.put("customerType", tripTO.getCustomerType());
            map.put("routeInfo", tripTO.getRouteInfo());
            map.put("reeferRequired", tripTO.getReeferRequired());
            map.put("totalWeight", tripTO.getTotalWeight());
            map.put("preStartLocationStatus", tripTO.getPreStartLocationStatus());
            map.put("originId", tripTO.getOriginId());
            map.put("destinationId", tripTO.getDestinationId());
            System.out.println("tripTO.getDestinationId()=" + tripTO.getDestinationId());
            map.put("plannedEndDate", tripTO.getTripPlanEndDate());
            map.put("tripPlanEndDate", tripTO.getTripPlanEndDate());
            map.put("plannedEndTime", tripTO.getTripPlanEndTime());
            map.put("tripPlanEndTime", tripTO.getTripPlanEndTime());
            map.put("vehicleVendorId", tripTO.getVendorId());

            if (!"".equals(tripTO.getEstimatedKM())) {
                map.put("estimatedKm", tripTO.getEstimatedKM());
            } else {
                map.put("estimatedKm", "0");
            }

            String tripType = tripTO.getTripType();
            if (tripType != null && !"".equals(tripType)) {
                if ("secondary".equalsIgnoreCase(tripType)) {
                    map.put("tripType", "2");
                } else {
                    map.put("tripType", "1");
                }
            } else {
                map.put("tripType", "1");
            }
            System.out.println("map value is:" + map);
            if ("1040".equals(tripTO.getCustomerId())) {
                map.put("emptyTripStatusId", "1");
            } else {
                map.put("emptyTripStatusId", "0");
            }
            System.out.println("map = " + map);
            tripId = (Integer) session.insert("trip.saveTripSheet", map);
            System.out.println("tripId=" + tripId + " Movement Type :::" + tripTO.getMovementType());
            if ("19".equals(tripTO.getMovementType())) {
                map.put("tripSheetId", tripId);
                map.put("tripId", tripId);
                map.put("startOdometerReading", "0");
                map.put("vehicleloadtemperature", "0");
                map.put("startHM", "0");
                map.put("statusId", "10");
                map.put("lrNumber", "0");

                int status = (Integer) session.update("trip.updateStartTripSheet", map);
                System.out.println("status for Local PNR::::" + status);
            }
            //int vehicleAvailUpdate= (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForFreeze", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripSheet", sqlException);
        }
        return tripId;
    }

    public int saveTripSheet(TripTO tripTO) {
        Map map = new HashMap();
        int tripId = 0;
        try {
            map.put("shipingLineNo", tripTO.getShipingLineNo());
            map.put("billOfEntry", tripTO.getBillOfEntry());
            map.put("tripCode", tripTO.getTripCode());
            map.put("companyId", tripTO.getCompanyId());
            map.put("customerId", tripTO.getCustomerId());
            map.put("productInfo", tripTO.getProductInfo());
            map.put("orderExpense", tripTO.getOrderExpense());
            map.put("orderRevenue", tripTO.getOrderRevenue());
            map.put("profitMargin", tripTO.getProfitMargin());
            map.put("hireCharge", tripTO.getHireCharges());
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            //map.put("pointPlanDate", pointPlanDate[0]);
            //map.put("pointPlanTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);

//            map.put("tripScheduleDate", tripTO.getTripScheduleDate());
//            map.put("tripScheduleTime", tripTO.getTripScheduleTime());
            if (pointPlanDate != null) {
                map.put("tripScheduleDate", pointPlanDate[0]);
            } else {
                map.put("tripScheduleDate", "00-00-0000");
            }
            if (pointPlanTimeHrs != null) {
                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            } else {
                map.put("tripScheduleTime", "00" + ":" + "00");
            }

            //map.put("tripScheduleTime", tripTO.getTripScheduleTimeHrs()+":"+tripTO.getTripScheduleTimeMins());
            map.put("statusId", tripTO.getStatusId());
            map.put("transitHours", tripTO.getTripTransitHours());
            map.put("transitDay", tripTO.getTripTransitDays());
            map.put("advnaceToBePaidPerDay", tripTO.getAdvnaceToBePaidPerDay());
            map.put("userId", tripTO.getUserId());
            map.put("emptyTripPurpose", tripTO.getEmptyTripPurpose());
            map.put("vehicleTypeName", tripTO.getVehicleTypeName());
            map.put("cNotes", tripTO.getcNotes());
            map.put("billingType", tripTO.getBillingType());
            map.put("customerName", tripTO.getCustomerName());
            map.put("customerType", tripTO.getCustomerType());
            map.put("routeInfo", tripTO.getRouteInfo());
            map.put("reeferRequired", tripTO.getReeferRequired());
            map.put("totalWeight", tripTO.getTotalWeight());
            map.put("preStartLocationStatus", tripTO.getPreStartLocationStatus());
            map.put("originId", tripTO.getOriginId());
            map.put("destinationId", tripTO.getDestinationId());
            map.put("plannedEndDate", tripTO.getTripPlanEndDate());
            map.put("plannedEndTime", tripTO.getTripPlanEndTime());
            map.put("vehicleVendorId", tripTO.getVendorId());

            if (!"".equals(tripTO.getEstimatedKM())) {
                map.put("estimatedKm", tripTO.getEstimatedKM());
            } else {
                map.put("estimatedKm", "0");
            }

            String tripType = tripTO.getTripType();
            if (tripType != null && !"".equals(tripType)) {
                if ("secondary".equalsIgnoreCase(tripType)) {
                    map.put("tripType", "2");
                } else {
                    map.put("tripType", "1");
                }
            } else {
                map.put("tripType", "1");
            }
            System.out.println("map value is:" + map);
            if ("1040".equals(tripTO.getCustomerId())) {
                map.put("emptyTripStatusId", "1");
            } else {
                map.put("emptyTripStatusId", "0");
            }
            System.out.println("map = " + map);
            tripId = (Integer) getSqlMapClientTemplate().insert("trip.saveTripSheet", map);
            System.out.println("tripId=" + tripId);

            //int vehicleAvailUpdate= (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForFreeze", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripSheet", sqlException);
        }
        return tripId;
    }

    public int updatePreStartDetails(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("preStartLocationId", tripTO.getPreStartLocationId());
            map.put("preStartLocationPlanDate", tripTO.getPreStartLocationPlanDate());
            map.put("preStartLocationPlanTimeHrs", tripTO.getPreStartLocationPlanTimeHrs());
            map.put("preStartLocationPlanTimeMins", tripTO.getPreStartLocationPlanTimeMins());
            map.put("preStartLocationDistance", tripTO.getPreStartLocationDistance());
            map.put("preStartLocationDurationHrs", tripTO.getPreStartLocationDurationHrs());
            map.put("preStartLocationDurationMins", tripTO.getPreStartLocationDurationMins());
            map.put("preStartLocationVehicleMileage", tripTO.getPreStartLocationVehicleMileage());
            map.put("preStartLocationTollRate", tripTO.getPreStartLocationTollRate());
            map.put("preStartLocationRouteExpense", tripTO.getPreStartLocationRouteExpense());

            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.updatePreStartDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int updatePreStartDetails(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("preStartLocationId", tripTO.getPreStartLocationId());
            map.put("preStartLocationPlanDate", tripTO.getPreStartLocationPlanDate());
            map.put("preStartLocationPlanTimeHrs", tripTO.getPreStartLocationPlanTimeHrs());
            map.put("preStartLocationPlanTimeMins", tripTO.getPreStartLocationPlanTimeMins());
            map.put("preStartLocationDistance", tripTO.getPreStartLocationDistance());
            map.put("preStartLocationDurationHrs", tripTO.getPreStartLocationDurationHrs());
            map.put("preStartLocationDurationMins", tripTO.getPreStartLocationDurationMins());
            map.put("preStartLocationVehicleMileage", tripTO.getPreStartLocationVehicleMileage());
            map.put("preStartLocationTollRate", tripTO.getPreStartLocationTollRate());
            map.put("preStartLocationRouteExpense", tripTO.getPreStartLocationRouteExpense());

            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updatePreStartDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int saveTripStatusDetails(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("statusId", tripTO.getStatusId());
            map.put("userId", tripTO.getUserId());
            map.put("durationDate", tripTO.getDurationDate());
            map.put("durationTime", tripTO.getDurationTime());
            map.put("remarks", "System Update");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripStatusDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int saveTripStatusDetails(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("statusId", tripTO.getStatusId());
            map.put("userId", tripTO.getUserId());
            map.put("remarks", "System Update");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripStatusDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }

    public int saveTripVehicle(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleVendorId", tripTO.getVendorId());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());

//            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
//            map.put("trailerId", tripTO.getTrailerId());
//            map.put("seatCapacity", tripTO.getSeatCapacity());
            if (tripTO.getVehicleCapUtil() != "") {
                map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            } else {
                map.put("vehicleCapUtil", "0");
            }
            if (tripTO.getTrailerId() != "") {
                map.put("trailerId", tripTO.getTrailerId());
            } else {
                map.put("trailerId", "0");
            }
            if (tripTO.getSeatCapacity() != "") {
                map.put("seatCapacity", tripTO.getSeatCapacity());
            } else {
                map.put("seatCapacity", "0");
            }
            if ("0".equals(tripTO.getVehicleId())) {
                map.put("hireVehilceNo", tripTO.getHireVehicleNo());
            }

            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripVehicle", map);
            System.out.println("saveTripVehicle=" + status);

            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();

            if (pointPlanDate != null) {
                map.put("tripScheduleDate", pointPlanDate[0]);
            } else {
                map.put("tripScheduleDate", "00-00-0000");
            }
            if (pointPlanTimeHrs != null) {
                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            } else {
                map.put("tripScheduleTime", "00" + ":" + "00");
            }
            map.put("tripPlannedEndDate", tripTO.getTripPlanEndDate());
            map.put("tripPlannedEndTime", tripTO.getTripPlanEndTime());
            map.put("originId", tripTO.getOriginId());
            System.out.println("map for Avail:");
            int vehicleAvailUpdate = (Integer) session.update("trip.updateVehicleAvailabilityForFreeze", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTripVehicle(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());

//            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
//            map.put("trailerId", tripTO.getTrailerId());
//            map.put("seatCapacity", tripTO.getSeatCapacity());
            if (tripTO.getVehicleCapUtil() != "") {
                map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            } else {
                map.put("vehicleCapUtil", "0");
            }
            if (tripTO.getTrailerId() != "") {
                map.put("trailerId", tripTO.getTrailerId());
            } else {
                map.put("trailerId", "0");
            }
            if (tripTO.getSeatCapacity() != "") {
                map.put("seatCapacity", tripTO.getSeatCapacity());
            } else {
                map.put("seatCapacity", "0");
            }
            if ("0".equals(tripTO.getVehicleId())) {
                map.put("hireVehilceNo", tripTO.getHireVehicleNo());
            }

            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripVehicle", map);
            System.out.println("saveTripVehicle=" + status);

            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();

            if (pointPlanDate != null) {
                map.put("tripScheduleDate", pointPlanDate[0]);
            } else {
                map.put("tripScheduleDate", "00-00-0000");
            }
            if (pointPlanTimeHrs != null) {
                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            } else {
                map.put("tripScheduleTime", "00" + ":" + "00");
            }
            map.put("tripPlannedEndDate", tripTO.getTripPlanEndDate());
            map.put("tripPlannedEndTime", tripTO.getTripPlanEndTime());
            map.put("originId", tripTO.getOriginId());
            System.out.println("map for Avail:");
            int vehicleAvailUpdate = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForFreeze", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTripVehicleForVehicleChange(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date curDate = new Date();
        System.out.println(dateFormat.format(curDate));
        String updateDate = dateFormat.format(curDate);
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("startKM", tripTO.getVehicleStartKm());
            map.put("endKM", tripTO.getLastVehicleEndKm());
            map.put("startHM", tripTO.getVehicleStartOdometer());
            map.put("endHM", tripTO.getLastVehicleEndOdometer());
            map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            map.put("remarks", tripTO.getVehicleRemarks());
            map.put("cityId", tripTO.getCityId());

            map.put("tripSheetId", tripTO.getTripId());
            map.put("statusId", "22");
            map.put("updateDate", updateDate);
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripVehicleForVehicleChange", map);
            if (status > 0) {
                if (tripTO.getReasonForChange() == 2) {
                    map.put("accVehicleNo", tripTO.getAccVehicleId());
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateAccidentDetail", map);
                }
            }
            int maxVehicleSequence = 0;
            int updateVehicleSequence = 0;
            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            map.put("vehicleSequence", maxVehicleSequence);
            updateVehicleSequence = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            System.out.println("saveTripVehicle=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public ArrayList getVehicleChangeTripAdvanceDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleChangeTripAdvanceDetails", map);
            System.out.println("getVehicleChangeTripAdvanceDetails.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return tripAdvanceDetails;
    }

    public int clearVehicleAndDriverMapping(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            if ("".equals(tripTO.getLastVehicleEndKm()) || tripTO.getLastVehicleEndKm() == null) {
                map.put("endKM", 0);
            } else {
                map.put("endKM", tripTO.getLastVehicleEndKm());
            }
            if ("".equals(tripTO.getLastVehicleEndOdometer()) || tripTO.getLastVehicleEndOdometer() == null) {
                map.put("endHM", 0);
            } else {
                map.put("endHM", tripTO.getLastVehicleEndOdometer());
            }
            if (!"".equals(tripTO.getVehicleChangeDate())) {
                map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            }
            if (!"".equals(tripTO.getVehicleChangeHour()) && !"".equals(tripTO.getVehicleChangeMinute())) {
                map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            }
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.removeTripVehicle", map);
            System.out.println("removeTripVehicle=" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.removeTripDriver", map);
            System.out.println("removeTripDriver=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "clearVehicleAndDriverMapping", sqlException);
        }
        return status;
    }

    public int saveTripDriver(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("driverId", tripTO.getPrimaryDriverId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("vehicleVendorId", tripTO.getVendorId());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
            if (!"".equals(tripTO.getPrimaryDriverId())) {
                System.out.println("tripTO.getPrimaryDriverId()" + tripTO.getPrimaryDriverId());
                map.put("driverId", tripTO.getPrimaryDriverId());
            } else {
                map.put("driverId", 0);
            }
//            if (!"".equals(tripTO.getPrimaryDriverName())) {
            System.out.println("tripTO.getPrimaryDriverName()" + tripTO.getPrimaryDriverName());
            map.put("primaryDriverName", tripTO.getPrimaryDriverName());
            map.put("secondaryDriverId", tripTO.getSecondaryDriver1Id());
//            }
//            else {
//                map.put("primaryDriverName", "NA");
//            }
//               map.put("primaryDriverName", tripTO.getPrimaryDriverName());

            map.put("type", "P");
//            if (!"".equals(tripTO.getMobileNo())) {
            System.out.println("tripTO.getPrimaryDriverName()" + tripTO.getPrimaryDriverName());
            map.put("driver1mobile", tripTO.getMobileNo());
//            }
//            else {
//                map.put("driver1mobile", 9);
//            }

//            map.put("driver1mobile", tripTO.getMobileNo());
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripDriver", map);
            System.out.println("status==statusstatus=" + status);

            String driverSalaryPercentage = (String) session.queryForObject("trip.getDriverSalaryPercentage", map);
            System.out.println("driverSalary==" + driverSalaryPercentage);
            if (driverSalaryPercentage != null) {
                System.out.println("iffff");
                String[] temp = driverSalaryPercentage.split("~");
                map.put("baseFreightPercent", temp[0]);
                map.put("driverPercent", temp[1]);
                map.put("cleanerPercent", temp[2]);
            } else {
                System.out.println("elseeee");
                map.put("baseFreightPercent", 0.93);
                map.put("driverPercent", 0.08);
                map.put("cleanerPercent", 0.03);
            }

            String baseFreight = (String) session.queryForObject("trip.getBaseFreightForDriverSalary", map);
            System.out.println("getBaseFreightForDriverSalary==" + baseFreight);
            map.put("baseFreight", baseFreight);
            String driverSalary = (String) session.queryForObject("trip.getTripDriverSalary", map);
            System.out.println("driverSalary==" + driverSalary);

            if (!"".equals(driverSalary)) {
                String[] temp = driverSalary.split("~");
                map.put("driverSalary", Double.parseDouble(temp[0]));
                map.put("cleanerSalary", Double.parseDouble(temp[1]));
                status = (Integer) session.update("trip.saveTripDriverSalary", map);
                System.out.println("statusstatusstatusstatusstatus=" + status);
            }
//            if (!"0".equals(tripTO.getSecondaryDriver1Id())) {
//                map.put("driverId", tripTO.getSecondaryDriver1Id());
//                map.put("type", "S");
//                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            }
//            if (!"0".equals(tripTO.getSecondaryDriver2Id())) {
//                map.put("driverId", tripTO.getSecondaryDriver2Id());
//                map.put("type", "S");
//                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            }

            System.out.println("saveTripDriver=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripDriver", sqlException);
        }
        return status;
    }

    public int saveTripDriver(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("driverId", tripTO.getPrimaryDriverId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("primaryDriverName", tripTO.getPrimaryDriverName());
            map.put("type", "P");
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            if (!"0".equals(tripTO.getSecondaryDriver1Id())) {
//                map.put("driverId", tripTO.getSecondaryDriver1Id());
//                map.put("type", "S");
//                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            }
//            if (!"0".equals(tripTO.getSecondaryDriver2Id())) {
//                map.put("driverId", tripTO.getSecondaryDriver2Id());
//                map.put("type", "S");
//                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
//            }

            System.out.println("saveTripDriver=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripDriver", sqlException);
        }
        return status;
    }

    public int saveTripConsignments(TripTO tripTO, String remainWeight, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        String statusID = "";
        String repoId = "";
        int newRepoId = 0;
        int plannedstatus = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            map.put("vehicleId", tripTO.getVehicleId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            int j = 1;
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("orderSequence", j);
                j++;
                System.out.println("map value is:" + map);
                statusID = (String) session.queryForObject("trip.getConsignmentStatus", map);
                repoId = (String) session.queryForObject("trip.getConsignmentRepoId", map);
                plannedstatus = (Integer) session.queryForObject("trip.getPlannedContainer", map);
                System.out.println("statusID:" + statusID + " order id:" + tripTO.getOrderType());
                if ("1".equals(tripTO.getRepoId()) || "1".equals(repoId)) {
                    if (plannedstatus > 0) {
                        // newRepoId= Integer.parseInt(repoId)+ Integer.parseInt(tripTO.getRepoId());
                        newRepoId = 1;
                    } else {
                        newRepoId = 2;
                    }
                } else {
                    newRepoId = Integer.parseInt(tripTO.getRepoId());
                }
//                if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("5")) {
//                    System.out.println("statusID:1");
//                    map.put("consignmentStatusId", "23");
//                } else if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("25")) {
//
//                    map.put("consignmentStatusId", "26");
//                } else if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("28")) {
//                    map.put("consignmentStatusId", "29");
//                } else {
                System.out.println("plannedstatus==" + plannedstatus);
                if (plannedstatus > 0) {
                    map.put("consignmentStatusId", "5");
                } else {
                    map.put("consignmentStatusId", "8");
                }
                map.put("remainWeight", remainWeight);
                map.put("repoId", newRepoId);
                System.out.println("statusID:2");
//                }
                System.out.println("map2 value is:" + map);
                status = (Integer) session.update("trip.saveTripConsignments", map);
                status = (Integer) session.update("trip.updateConsignmentOrderStatus", map);
                if ("14".equalsIgnoreCase(tripTO.getMovementType()) || "16".equalsIgnoreCase(tripTO.getMovementType())) {
                    status = (Integer) session.update("trip.getLRNumberSequence", map);
                }
            }
            if ("15".equalsIgnoreCase(tripTO.getMovementType())) {
                status = (Integer) session.update("trip.getLRNumberSequence", map);
                System.out.println("LCL ICD haaaa........???" + status);
            }
            System.out.println("saveTripConsignments=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripConsignments Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripConsignments", sqlException);
        }
        return status;
    }

    public int saveTripConsignments(TripTO tripTO, String remainWeight) {
        Map map = new HashMap();
        int status = 0;
        String statusID = "";
        String repoId = "";
        int newRepoId = 0;
        int plannedstatus = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            map.put("vehicleId", tripTO.getVehicleId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            int j = 1;
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("orderSequence", j);
                j++;
                System.out.println("map value is:" + map);
                statusID = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentStatus", map);
                repoId = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentRepoId", map);
                plannedstatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.getPlannedContainer", map);
                System.out.println("statusID:" + statusID + " order id:" + tripTO.getOrderType());
                if ("1".equals(tripTO.getRepoId()) || "1".equals(repoId)) {
                    if (plannedstatus > 0) {
                        // newRepoId= Integer.parseInt(repoId)+ Integer.parseInt(tripTO.getRepoId());
                        newRepoId = 1;
                    } else {
                        newRepoId = 2;
                    }
                } else {
                    newRepoId = Integer.parseInt(tripTO.getRepoId());
                }
//                if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("5")) {
//                    System.out.println("statusID:1");
//                    map.put("consignmentStatusId", "23");
//                } else if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("25")) {
//
//                    map.put("consignmentStatusId", "26");
//                } else if (Integer.parseInt(tripTO.getOrderType()) == 2 && statusID.equalsIgnoreCase("28")) {
//                    map.put("consignmentStatusId", "29");
//                } else {
                map.put("consignmentStatusId", "8");
                map.put("remainWeight", remainWeight);
                map.put("repoId", newRepoId);
                System.out.println("statusID:2");
//                }
                System.out.println("map2 value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripConsignments", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderStatus", map);
            }
            System.out.println("saveTripConsignments=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripConsignments Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripConsignments", sqlException);
        }
        return status;
    }

    public int saveAction(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("remarks", tripTO.getActionName() + " : " + tripTO.getActionRemarks());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                System.out.println("map value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveAction", map);
                System.out.println("saveAction=" + status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveAction Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAction", sqlException);
        }
        return status;
    }

    public int saveClosureApprovalRequest(String tripId, String requestType, String remarks, int userId, String rcmExp, String nettExp) {
        Map map = new HashMap();
        int status = 0;
        try {
            if (nettExp == null || "".equals(nettExp)) {
                nettExp = "0";
            }
            if (rcmExp == null || "".equals(rcmExp)) {
                rcmExp = "0";
            }
            map.put("tripId", tripId);
            map.put("requestType", requestType);
            map.put("remarks", remarks);
            map.put("userId", userId);
            map.put("rcmExp", rcmExp);
            map.put("nettExp", nettExp);
            String kmHmInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getTripKmHm", map);
            //String rcmSystemExpInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getRcmSystemExpense", map);
            String[] temp = kmHmInfo.split("-");
            map.put("kmRun", temp[0]);
            map.put("hmRun", temp[1]);
            map.put("aprovedStatus", "1");
            System.out.println("map in the trip dao = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveClosureApprovalRequest", map);
            System.out.println("saveClosureApprovalRequest=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveClosureApprovalRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveClosureApprovalRequest", sqlException);
        }
        return status;
    }

    public int saveBillingRevenueChangeApprovalRequest(String customerId, String requestType, String remarks, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("userId", userId);
            map.put("customerId", customerId);
            map.put("requestType", requestType);
            map.put("remarks", remarks);

            System.out.println("map in the trip dao = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillingRevenueApprovalRequest", map);
            System.out.println("saveBillingRevenueApprovalRequest=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveClosureApprovalRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveClosureApprovalRequest", sqlException);
        }
        return status;
    }

    public int saveTripUpdate(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("remarks", tripTO.getActionName() + " : " + tripTO.getActionRemarks());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            map.put("containerStatus", tripTO.getContainerStatus());

            map.put("tripId", tripTO.getTripId());
            map.put("tripSheetId", tripTO.getTripId());
            System.out.println("map value:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripStatusDetails", map);

            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsContStatusUnfreeze", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.deleteTripContainerUnfreeze", map);
            //update trip master
            System.out.println("map value:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripStatus", map);
            System.out.println("status ::::" + status);
//            updateOrderStatusToEFS(tripTO.getTripId(), tripTO.getStatusId());
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            if ("6".equals(tripTO.getStatusId())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentNoteStatus", map);
            }
            //       updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            System.out.println("saveAction=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveAction Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAction", sqlException);
        }
        return status;
    }

    public int saveTripRoutePlan(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String[] orderId = tripTO.getOrderIds();
            String[] pointId = tripTO.getPointId();
            String[] pointType = tripTO.getPointType();
            String[] pointOrder = tripTO.getPointOrder();
            String[] pointAddresss = tripTO.getPointAddresss();
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            String[] pointName = tripTO.getPointName();
            String[] actualpointId = tripTO.getActualPointId();
            String[] actualpointname = tripTO.getActualPointName();
            String[] actualLatitude = tripTO.getActualLatitude();
            String[] actualLongtitude = tripTO.getActualLongtitude();

            System.out.println("orderId.length:" + orderId.length);
            System.out.println("pointName.length:" + pointName.length);
            System.out.println("pointId.length:" + pointId.length);
            System.out.println("pointType.length:" + pointType.length);
            System.out.println("pointOrder.length:" + pointOrder.length);
            System.out.println("pointAddresss.length:" + pointAddresss.length);
            System.out.println("pointPlanDate.length:" + pointPlanDate.length);
            System.out.println("pointPlanTimeHrs.length:" + pointPlanTimeHrs.length);
            System.out.println("pointPlanTimeMins.length:" + pointPlanTimeMins.length);
            for (int i = 0; i < pointId.length; i++) {
                map.put("orderId", orderId[i]);
                map.put("pointName", pointName[i]);
                map.put("pointId", pointId[i]);
                map.put("pointType", pointType[i]);
                map.put("pointOrder", pointOrder[i]);
                map.put("actualpointId", actualpointId[i]);
                map.put("actualLatitude", actualLatitude[i]);
                map.put("actualLongtitude", actualLongtitude[i]);

//                map.put("pointAddresss", pointAddresss[i]);
                map.put("pointAddresss", actualpointname[i]);
                map.put("pointPlanDate", pointPlanDate[i]);
                map.put("pointPlanTime", pointPlanTimeHrs[i] + ":" + pointPlanTimeMins[i]);
                System.out.println("Route map value is:" + map);
                status = (Integer) session.update("trip.saveTripRoutePlan", map);
                System.out.println("saveTripRoutePlan=" + status);
            }
            System.out.println("");
            if (tripTO.getActionName() != null && "1".equals(tripTO.getActionName()) && !"".equals(tripTO.getPreStartLocationId())) {
                if ("0".equals(tripTO.getPreStartLocationStatus())) {
                    map.put("orderId", "0");
                    map.put("pointName", "NA");
                    map.put("pointId", tripTO.getPreStartLocationId());
                    map.put("pointType", "Pre Start");
                    map.put("pointOrder", "0");
                    map.put("pointAddresss", "na");
                    map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                    map.put("pointPlanTime", tripTO.getPreStartLocationPlanTimeHrs() + ":" + tripTO.getPreStartLocationPlanTimeMins());
                    System.out.println("route map value is:" + map);
                    status = (Integer) session.update("trip.saveTripRoutePlan", map);
                    System.out.println("saveTripRoutePlan=" + status);
                }
            }
//                 updateOrderStatusToEFS(tripTO.getTripId(), tripTO.getStatusId());
//            status = (Integer) session.update("trip.updateContainerConsignmentId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int saveTripRoutePlan(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String[] orderId = tripTO.getOrderIds();
            String[] pointId = tripTO.getPointId();
            String[] pointType = tripTO.getPointType();
            String[] pointOrder = tripTO.getPointOrder();
            String[] pointAddresss = tripTO.getPointAddresss();
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            System.out.println("orderId.length:" + orderId.length);
            System.out.println("pointId.length:" + pointId.length);
            System.out.println("pointType.length:" + pointType.length);
            System.out.println("pointOrder.length:" + pointOrder.length);
            System.out.println("pointAddresss.length:" + pointAddresss.length);
            System.out.println("pointPlanDate.length:" + pointPlanDate.length);
            System.out.println("pointPlanTimeHrs.length:" + pointPlanTimeHrs.length);
            System.out.println("pointPlanTimeMins.length:" + pointPlanTimeMins.length);
            for (int i = 0; i < pointId.length; i++) {
                map.put("orderId", orderId[i]);
                map.put("pointId", pointId[i]);
                map.put("pointType", pointType[i]);
                map.put("pointOrder", pointOrder[i]);
                map.put("pointAddresss", pointAddresss[i]);
                map.put("pointPlanDate", pointPlanDate[i]);
                map.put("pointPlanTime", pointPlanTimeHrs[i] + ":" + pointPlanTimeMins[i]);
                System.out.println("Route map value is:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                System.out.println("saveTripRoutePlan=" + status);
            }
            System.out.println("");
            if (tripTO.getActionName() != null && "1".equals(tripTO.getActionName()) && !"".equals(tripTO.getPreStartLocationId())) {
                if ("0".equals(tripTO.getPreStartLocationStatus())) {
                    map.put("orderId", "0");
                    map.put("pointId", tripTO.getPreStartLocationId());
                    map.put("pointType", "Pre Start");
                    map.put("pointOrder", "0");
                    map.put("pointAddresss", "na");
                    map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                    map.put("pointPlanTime", tripTO.getPreStartLocationPlanTimeHrs() + ":" + tripTO.getPreStartLocationPlanTimeMins());
                    System.out.println("route map value is:" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                    System.out.println("saveTripRoutePlan=" + status);
                }
            }
//                 updateOrderStatusToEFS(tripTO.getTripId(), tripTO.getStatusId());
            status = (Integer) getSqlMapClientTemplate().update("trip.updateContainerConsignmentId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int saveTripRoutePlanWhenTripUpdate(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
                if ("0".equals(tripTO.getPreStartLocationStatus())) {
                    map.put("orderId", "0");
                    map.put("pointId", tripTO.getPreStartLocationId());
                    map.put("pointType", "Pre Start");
                    map.put("pointOrder", "0");
                    map.put("pointAddresss", "na");
                    map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                    map.put("pointPlanTime", tripTO.getPreStartLocationPlanTimeHrs() + ":" + tripTO.getPreStartLocationPlanTimeMins());
                    System.out.println("map value is:" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
                    System.out.println("saveTripRoutePlan=" + status);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("documentRequired", tripTO.getDocumentRequired());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        if ("1".equals(tripTO.getMovementType()) || "2".equals(tripTO.getMovementType())) {
            map.put("fromDate", "");
            map.put("toDate", "");
        } else {
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
        }

        map.put("customerName", tripTO.getCustomerId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        System.out.println("tripTO.getTripSheetId()==" + tripTO.getTripSheetId());
        map.put("statusId", tripTO.getStatusId());
        map.put("tripStatusId", tripTO.getTripStatusId());
//        map.put("tripStatusId", tripTO.getStatusId());
        map.put("roleId", tripTO.getRoleId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("tripCode", tripTO.getTripCode());
        map.put("zoneId", tripTO.getZoneId());
        map.put("fleetCenterId", tripTO.getFleetCenterId());
        map.put("cityFromId", tripTO.getCityFromId());
        map.put("podStatus", tripTO.getPodStatus());
        map.put("tripType", tripTO.getTripType());
        map.put("extraExpenseStatus", tripTO.getExtraExpenseStatus());
        System.out.println("tripTO.getMovementType()::::::::::" + tripTO.getMovementType());
        if ("1,25,26,27,31,32".equals(tripTO.getMovementType())) {
            System.out.println("For Export");
            map.put("movement", "1");
        } else if ("2,28,29,30".equals(tripTO.getMovementType())) {
            System.out.println("For Import");
            map.put("movement", "2");
        } else {
            System.out.println("Else..");
            map.put("movement", tripTO.getMovementType());
        }

        map.put("subStatusId", tripTO.getSubStatusId());
        map.put("requestType", tripTO.getRequestType());
        map.put("customerId", tripTO.getCustomerId());

        map.put("userId", tripTO.getUserId());
        System.out.println("map ...." + map);
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);
        List userMovementList = new ArrayList(tripTO.getUserMovementList().size());

        Iterator umItr = tripTO.getUserMovementList().iterator();
        LoginTO umOpTO = new LoginTO();
        while (umItr.hasNext()) {
            umOpTO = new LoginTO();
            umOpTO = (LoginTO) umItr.next();
            userMovementList.add(umOpTO.getMovementTypeId());
        }
        map.put("userMovementList", userMovementList);
        System.out.println("mapaaaaaaaa = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSheetDetails", map);
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getTripSheetExcelDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("documentRequired", tripTO.getDocumentRequired());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        if ("1".equals(tripTO.getMovementType()) || "2".equals(tripTO.getMovementType())) {
            map.put("fromDate", "");
            map.put("toDate", "");
        } else {
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
        }

        map.put("customerName", tripTO.getCustomerId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        System.out.println("tripTO.getTripSheetId()==" + tripTO.getTripSheetId());
        map.put("statusId", tripTO.getStatusId());
        map.put("tripStatusId", tripTO.getTripStatusId());
//        map.put("tripStatusId", tripTO.getStatusId());
        map.put("roleId", tripTO.getRoleId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("tripCode", tripTO.getTripCode());
        map.put("zoneId", tripTO.getZoneId());
        map.put("fleetCenterId", tripTO.getFleetCenterId());
        map.put("cityFromId", tripTO.getCityFromId());
        map.put("podStatus", tripTO.getPodStatus());
        map.put("tripType", tripTO.getTripType());
        map.put("extraExpenseStatus", tripTO.getExtraExpenseStatus());
        System.out.println("tripTO.getMovementType()::::::::::" + tripTO.getMovementType());
        if ("1,25,26,27,31,32".equals(tripTO.getMovementType())) {
            System.out.println("For Export");
            map.put("movement", "1");
        } else if ("2,28,29,30".equals(tripTO.getMovementType())) {
            System.out.println("For Import");
            map.put("movement", "2");
        } else {
            System.out.println("Else..");
            map.put("movement", tripTO.getMovementType());
        }

        map.put("subStatusId", tripTO.getSubStatusId());
        map.put("requestType", tripTO.getRequestType());
        map.put("customerId", tripTO.getCustomerId());

        map.put("userId", tripTO.getUserId());
        System.out.println("map ...." + map);
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);
        List userMovementList = new ArrayList(tripTO.getUserMovementList().size());

        Iterator umItr = tripTO.getUserMovementList().iterator();
        LoginTO umOpTO = new LoginTO();
        while (umItr.hasNext()) {
            umOpTO = new LoginTO();
            umOpTO = (LoginTO) umItr.next();
            userMovementList.add(umOpTO.getMovementTypeId());
        }
        map.put("userMovementList", userMovementList);
        System.out.println("mapaaWWWWWWWWWWWWWaaaaaa = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSheetExcelDetails", map);
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getStatusDetails() {
        Map map = new HashMap();
        System.out.println("map = " + map);
        ArrayList response = new ArrayList();
        try {
            response = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStatusMaster", map);
            System.out.println("response size=" + response.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStatusDetails List", sqlException);
        }

        return response;
    }

    //       Arul Starts Here
    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripDetails(String tripSheetId) {
        Map map = new HashMap();
        String getTripDetails = "";
        try {
            map.put("tripSheetId", tripSheetId);
            getTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripDetail", map);
            System.out.println("getTripDetails=" + getTripDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails", sqlException);
        }
        return getTripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripInformation(String tripSheetId) {
        Map map = new HashMap();
        String getTripDetails = "";
        try {
            map.put("tripSheetId", tripSheetId);
            getTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripInformation", map);
            System.out.println("getTripDetails=" + getTripDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails", sqlException);
        }
        return getTripDetails;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getDriverCount(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String driverCount = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            driverCount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverCount", map);
            System.out.println("driverCount=" + driverCount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverCount", sqlException);
        }
        return driverCount;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripAdvance(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripAdvance = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            tripAdvance = (String) getSqlMapClientTemplate().queryForObject("trip.getTripAdvance", map);
            System.out.println("tripAdvance=" + tripAdvance);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvance", sqlException);
        }
        return tripAdvance;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public String getTripExpense(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripExpense = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            tripExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpense", map);
            System.out.println("tripExpense=" + tripExpense);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpense", sqlException);
        }
        return tripExpense;
    }

    /**
     * This method is used to View Trip Details For Driver Settlement.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int insertSettlement(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) {
        Map map = new HashMap();
        int insertSettlement = 0;
        int updateSetttlementMaster = 0;
        int updateSettlementDetail = 0;
        map.put("tripSheetId", tripSheetId);
        map.put("estimatedExpense", estimatedExpense);
        map.put("bpclTransactionAmount", bpclTransactionAmount);
        map.put("rcmExpense", rcmExpense);
        map.put("vehicleDieselConsume", vehicleDieselConsume);
        map.put("reeferDieselConsume", reeferDieselConsume);
        map.put("tripStartingBalance", tripStartingBalance);
        if (!"".equals(gpsHm) && gpsHm != null) {
            map.put("gpsHm", gpsHm);
        } else {
            map.put("gpsHm", "0");
        }
        if (!"".equals(gpsKm)) {
            map.put("gpsKm", gpsKm);
        } else {
            map.put("gpsKm", "0");
        }
        if (!"".equals(totalRunKm)) {
            map.put("totalRunKm", totalRunKm);
        } else {
            map.put("totalRunKm", "0");
        }
        if (!"".equals(totalDays)) {
            map.put("totalDays", totalDays);
        } else {
            map.put("totalDays", "0");
        }
        if (!"".equals(totalRefeerHours)) {
            map.put("totalRefeerHours", totalRefeerHours);
        } else {
            map.put("totalRefeerHours", "0");
        }
        if (!"".equals(milleage)) {
            map.put("milleage", milleage);
        } else {
            map.put("milleage", "0");
        }
        if (!"".equals(fuelPrice)) {
            map.put("fuelPrice", fuelPrice);
        } else {
            map.put("fuelPrice", "0");
        }
        if (!"".equals(tripDieselConsume)) {
            map.put("tripDieselConsume", tripDieselConsume);
        } else {
            map.put("tripDieselConsume", "0");
        }
        if (!"".equals(tripRcmAllocation)) {
            map.put("tripRcmAllocation", tripRcmAllocation);
        } else {
            map.put("tripRcmAllocation", "0");
        }
        if (!"".equals(totalTripAdvance)) {
            map.put("totalTripAdvance", totalTripAdvance);
        } else {
            map.put("totalTripAdvance", "0");

        }
        if (!"".equals(totalTripMisc)) {
            map.put("totalTripMisc", totalTripMisc);
        } else {
            map.put("totalTripMisc", "0");
        }
        if (!"".equals(totalTripBhatta)) {
            map.put("totalTripBhatta", totalTripBhatta);
        } else {
            map.put("totalTripBhatta", "0");
        }
        if (!"".equals(tripExtraExpense)) {
            map.put("tripExtraExpense", tripExtraExpense);
        } else {
            map.put("tripExtraExpense", "0");
        }
        if (!"".equals(totalTripExpense)) {
            map.put("totalTripExpense", totalTripExpense);
        } else {
            map.put("totalTripExpense", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripEndBalance)) {
            map.put("tripEndBalance", tripEndBalance);
        } else {
            map.put("tripEndBalance", "0");
        }

        map.put("settlementRemarks", settlementRemarks);
        map.put("paymentMode", paymentMode);
        map.put("userId", userId);
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("the insertSettlement" + map);
        int closureSize = 0;
        int settlementSize = 0;
        try {
            insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertSettlement", map);
            closureSize = (Integer) getSqlMapClientTemplate().queryForObject("trip.getClosureSize", map);
            settlementSize = (Integer) getSqlMapClientTemplate().queryForObject("trip.getSettlementSize", map);
            if (insertSettlement > 0 && closureSize == settlementSize) {
                updateSetttlementMaster = (Integer) getSqlMapClientTemplate().update("trip.updateSetttlementMaster", map);
                System.out.println("updateSetttlementMaster---" + updateSetttlementMaster);
                // updateOrderStatusToEFS(tripSheetId, "14");
                updateSettlementDetail = (Integer) getSqlMapClientTemplate().update("trip.updateSettlementDetail", map);
                System.out.println("updateSettlementDetail--" + updateSettlementDetail);
                int status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
                System.out.println("+status----" + status);
                //      updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return insertSettlement;
    }

    public int insertTripDriverSettlementDetails(String tripSheetId, String estimatedExpense, String bpclTransactionAmount, String rcmExpense, String vehicleDieselConsume, String reeferDieselConsume, String tripStartingBalance, String gpsKm, String gpsHm, String totalRunKm, String totalDays, String totalRefeerHours, String milleage, String tripDieselConsume, String tripRcmAllocation, String totalTripAdvance, String totalTripMisc, String totalTripBhatta, String totalTripExpense, String tripBalance, String tripEndBalance, String settlementRemarks, String paymentMode, String fuelPrice, String tripExtraExpense, int userId, TripTO tripTO) {
        Map map = new HashMap();
        int insertTripDriverSettlementDetails = 0;
        int updateSetttlementMaster = 0;
        int updateSettlementDetail = 0;
        map.put("tripSheetId", tripSheetId);
        map.put("estimatedExpense", estimatedExpense);
        map.put("bpclTransactionAmount", bpclTransactionAmount);
        map.put("rcmExpense", rcmExpense);
        map.put("vehicleDieselConsume", vehicleDieselConsume);
        map.put("reeferDieselConsume", reeferDieselConsume);
        map.put("tripStartingBalance", tripStartingBalance);
        if (!"".equals(gpsHm) && gpsHm != null) {
            map.put("gpsHm", gpsHm);
        } else {
            map.put("gpsHm", "0");
        }
        if (!"".equals(gpsKm)) {
            map.put("gpsKm", gpsKm);
        } else {
            map.put("gpsKm", "0");
        }
        if (!"".equals(totalRunKm)) {
            map.put("totalRunKm", totalRunKm);
        } else {
            map.put("totalRunKm", "0");
        }
        if (!"".equals(totalDays)) {
            map.put("totalDays", totalDays);
        } else {
            map.put("totalDays", "0");
        }
        if (!"".equals(totalRefeerHours)) {
            map.put("totalRefeerHours", totalRefeerHours);
        } else {
            map.put("totalRefeerHours", "0");
        }
        if (!"".equals(milleage)) {
            map.put("milleage", milleage);
        } else {
            map.put("milleage", "0");
        }
        if (!"".equals(fuelPrice)) {
            map.put("fuelPrice", fuelPrice);
        } else {
            map.put("fuelPrice", "0");
        }
        if (!"".equals(tripDieselConsume)) {
            map.put("tripDieselConsume", tripDieselConsume);
        } else {
            map.put("tripDieselConsume", "0");
        }
        if (!"".equals(tripRcmAllocation)) {
            map.put("tripRcmAllocation", tripRcmAllocation);
        } else {
            map.put("tripRcmAllocation", "0");
        }
        if (!"".equals(totalTripAdvance)) {
            map.put("totalTripAdvance", totalTripAdvance);
        } else {
            map.put("totalTripAdvance", "0");

        }
        if (!"".equals(totalTripMisc)) {
            map.put("totalTripMisc", totalTripMisc);
        } else {
            map.put("totalTripMisc", "0");
        }
        if (!"".equals(totalTripBhatta)) {
            map.put("totalTripBhatta", totalTripBhatta);
        } else {
            map.put("totalTripBhatta", "0");
        }
        if (!"".equals(tripExtraExpense)) {
            map.put("tripExtraExpense", tripExtraExpense);
        } else {
            map.put("tripExtraExpense", "0");
        }
        if (!"".equals(totalTripExpense)) {
            map.put("totalTripExpense", totalTripExpense);
        } else {
            map.put("totalTripExpense", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripBalance)) {
            map.put("tripBalance", tripBalance);
        } else {
            map.put("tripBalance", "0");
        }
        if (!"".equals(tripEndBalance)) {
            map.put("tripEndBalance", tripEndBalance);
        } else {
            map.put("tripEndBalance", "0");
        }

        map.put("settlementRemarks", settlementRemarks);
        map.put("paymentMode", paymentMode);
        map.put("userId", userId);
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("the insertSettlement" + map);
        int closureSize = 0;
        int settlementSize = 0;
        int driverChangeStatus = 0;
        ArrayList inActiveVehicleDriverlist = new ArrayList();
        ArrayList driverClosureList = new ArrayList();
        try {
            driverChangeStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkDriverChangeStatus", map);
            System.out.println("driverChangeStatus = " + driverChangeStatus);
            Float totalKm = 0.0f;
            Float totalHm = 0.0f;
            Float advancePaid = 0.0f;
            int totalTripDays = 0;
            String driverInTrip = "";
            String[] tempDriverInTrip = null;
            int driverCount = 0;
            if (driverChangeStatus > 0) {
                // Driver Cnage Case
                inActiveVehicleDriverlist = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInactiveVehicleDriver", map);
                Iterator itr1 = inActiveVehicleDriverlist.iterator();
                TripTO tpTO = new TripTO();
                while (itr1.hasNext()) {
                    tpTO = new TripTO();
                    tpTO = (TripTO) itr1.next();
                    if (tpTO.getDriverInTrip().contains(",")) {
                        tempDriverInTrip = driverInTrip.split(",");
                        for (int i = 0; i < tempDriverInTrip.length; i++) {
                            map.put("driverId", tempDriverInTrip[i]);
                            driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                            Iterator itr = driverClosureList.iterator();
                            TripTO trpTO = new TripTO();
                            while (itr.hasNext()) {
                                trpTO = new TripTO();
                                trpTO = (TripTO) itr.next();
                                map.put("tripId", tripSheetId);
                                map.put("tripSheetId", tripSheetId);
                                map.put("driverId", trpTO.getDriverId());
                                map.put("estimatedExpense", trpTO.getEstimatedExpense());
                                map.put("bpclTransactionAmount", 0);
                                map.put("rcmExpense", trpTO.getEstimatedExpense());
                                map.put("vehicleDieselConsume", Float.parseFloat(trpTO.getRunKm()) / Float.parseFloat(trpTO.getVehicleMileage()));
                                map.put("reeferDieselConsume", Float.parseFloat(trpTO.getRunHm()) * Float.parseFloat(trpTO.getReeferMileage()));
                                map.put("tripStartingBalance", 0);
                                if (!"".equals(gpsHm) && gpsHm != null) {
                                    map.put("gpsHm", gpsHm);
                                } else {
                                    map.put("gpsHm", "0");
                                }
                                if (!"".equals(gpsKm)) {
                                    map.put("gpsKm", gpsKm);
                                } else {
                                    map.put("gpsKm", "0");
                                }
                                if (!"".equals(trpTO.getRunKm())) {
                                    map.put("totalRunKm", trpTO.getRunKm());
                                } else {
                                    map.put("totalRunKm", "0");
                                }
                                if (!"".equals(trpTO.getTripTransitDays())) {
                                    map.put("totalDays", trpTO.getTripTransitDays());
                                } else {
                                    map.put("totalDays", "0");
                                }
                                if (!"".equals(trpTO.getRunHm())) {
                                    map.put("totalRefeerHours", trpTO.getRunHm());
                                } else {
                                    map.put("totalRefeerHours", "0");
                                }
                                if (!"".equals(trpTO.getVehicleMileage())) {
                                    map.put("milleage", trpTO.getVehicleMileage());
                                } else {
                                    map.put("milleage", "0");
                                }
                                if (!"".equals(trpTO.getFuelCost())) {
                                    map.put("fuelPrice", trpTO.getFuelCost());
                                } else {
                                    map.put("fuelPrice", "0");
                                }
                                if (!"".equals(trpTO.getFuelConsumption())) {
                                    map.put("tripDieselConsume", trpTO.getFuelConsumption());
                                } else {
                                    map.put("tripDieselConsume", "0");
                                }
                                if (!"".equals(trpTO.getEstimatedExpense())) {
                                    map.put("tripRcmAllocation", trpTO.getEstimatedExpense());
                                } else {
                                    map.put("tripRcmAllocation", "0");
                                }
                                if (!"".equals(tpTO.getAdvancePaid())) {
                                    map.put("totalTripAdvance", Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length);
                                } else {
                                    map.put("totalTripAdvance", "0");

                                }
                                if (!"".equals(trpTO.getMiscValue())) {
                                    map.put("totalTripMisc", trpTO.getMiscValue());
                                } else {
                                    map.put("totalTripMisc", "0");
                                }
                                if (!"".equals(trpTO.getDriverBhatta())) {
                                    map.put("totalTripBhatta", trpTO.getDriverBhatta());
                                } else {
                                    map.put("totalTripBhatta", "0");
                                }
                                String startDate = tpTO.getStartDate();
                                String endDate = tpTO.getEndDate();
                                map.put("tripStartDate", startDate);
                                map.put("tripEndDate", endDate);
                                String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
                                if (!"".equals(expenseValue)) {
                                    map.put("tripExtraExpense", Float.parseFloat(expenseValue) / tempDriverInTrip.length);
                                } else {
                                    map.put("tripExtraExpense", "0");
                                }
                                if (!"".equals(trpTO.getEstimatedExpense())) {
                                    map.put("totalTripExpense", Float.parseFloat(trpTO.getEstimatedExpense()));
                                } else {
                                    map.put("totalTripExpense", "0");
                                }
                                if (!"".equals(tripBalance)) {
                                    map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripBalance", "0");
                                }
                                if (!"".equals(tripBalance)) {
                                    map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripBalance", "0");
                                }
                                if (!"".equals(tripEndBalance)) {
                                    map.put("tripEndBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid()) / tempDriverInTrip.length));
                                } else {
                                    map.put("tripEndBalance", "0");
                                }

                                map.put("settlementRemarks", settlementRemarks);
                                map.put("paymentMode", paymentMode);
                                map.put("userId", userId);
                                map.put("vehicleId", tripTO.getVehicleId());
                                System.out.println("the insertSettlement" + map);
                                insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                                System.out.println("insert status = " + insertTripDriverSettlementDetails);
                            }
                        }
                    } else {
                        driverInTrip = tpTO.getDriverInTrip();
                        map.put("driverId", driverInTrip);
                        driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                        Iterator itr = driverClosureList.iterator();
                        TripTO trpTO = new TripTO();
                        while (itr.hasNext()) {
                            trpTO = new TripTO();
                            trpTO = (TripTO) itr.next();
                            map.put("tripId", tripSheetId);
                            map.put("tripSheetId", tripSheetId);
                            map.put("driverId", trpTO.getDriverId());
                            map.put("estimatedExpense", trpTO.getEstimatedExpense());
                            map.put("bpclTransactionAmount", 0);
                            map.put("rcmExpense", trpTO.getEstimatedExpense());
                            map.put("vehicleDieselConsume", Float.parseFloat(trpTO.getRunKm()) / Float.parseFloat(trpTO.getVehicleMileage()));
                            map.put("reeferDieselConsume", Float.parseFloat(trpTO.getRunHm()) * Float.parseFloat(trpTO.getReeferMileage()));
                            map.put("tripStartingBalance", 0);
                            if (!"".equals(gpsHm) && gpsHm != null) {
                                map.put("gpsHm", gpsHm);
                            } else {
                                map.put("gpsHm", "0");
                            }
                            if (!"".equals(gpsKm)) {
                                map.put("gpsKm", gpsKm);
                            } else {
                                map.put("gpsKm", "0");
                            }
                            if (!"".equals(trpTO.getRunKm())) {
                                map.put("totalRunKm", trpTO.getRunKm());
                            } else {
                                map.put("totalRunKm", "0");
                            }
                            if (!"".equals(trpTO.getTripTransitDays())) {
                                map.put("totalDays", trpTO.getTripTransitDays());
                            } else {
                                map.put("totalDays", "0");
                            }
                            if (!"".equals(trpTO.getRunHm())) {
                                map.put("totalRefeerHours", trpTO.getRunHm());
                            } else {
                                map.put("totalRefeerHours", "0");
                            }
                            if (!"".equals(trpTO.getVehicleMileage())) {
                                map.put("milleage", trpTO.getVehicleMileage());
                            } else {
                                map.put("milleage", "0");
                            }
                            if (!"".equals(trpTO.getFuelCost())) {
                                map.put("fuelPrice", trpTO.getFuelCost());
                            } else {
                                map.put("fuelPrice", "0");
                            }
                            if (!"".equals(trpTO.getFuelConsumption())) {
                                map.put("tripDieselConsume", trpTO.getFuelConsumption());
                            } else {
                                map.put("tripDieselConsume", "0");
                            }
                            if (!"".equals(trpTO.getEstimatedExpense())) {
                                map.put("tripRcmAllocation", trpTO.getEstimatedExpense());
                            } else {
                                map.put("tripRcmAllocation", "0");
                            }
                            if (!"".equals(tpTO.getAdvancePaid())) {
                                map.put("totalTripAdvance", Float.parseFloat(tpTO.getAdvancePaid()));
                            } else {
                                map.put("totalTripAdvance", "0");

                            }
                            if (!"".equals(trpTO.getMiscValue())) {
                                map.put("totalTripMisc", trpTO.getMiscValue());
                            } else {
                                map.put("totalTripMisc", "0");
                            }
                            if (!"".equals(trpTO.getDriverBhatta())) {
                                map.put("totalTripBhatta", trpTO.getDriverBhatta());
                            } else {
                                map.put("totalTripBhatta", "0");
                            }
                            String startDate = tpTO.getStartDate();
                            String endDate = tpTO.getEndDate();
                            map.put("tripStartDate", startDate);
                            map.put("tripEndDate", endDate);
                            String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpense", map);
                            if (!"".equals(expenseValue)) {
                                map.put("tripExtraExpense", Float.parseFloat(expenseValue));
                            } else {
                                map.put("tripExtraExpense", "0");
                            }
                            if (!"".equals(trpTO.getEstimatedExpense())) {
                                map.put("totalTripExpense", Float.parseFloat(trpTO.getEstimatedExpense()));
                            } else {
                                map.put("totalTripExpense", "0");
                            }
                            if (!"".equals(tripBalance)) {
                                map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripBalance", "0");
                            }
                            if (!"".equals(tripBalance)) {
                                map.put("tripBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripBalance", "0");
                            }
                            if (!"".equals(tripEndBalance)) {
                                map.put("tripEndBalance", (Float.parseFloat(trpTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())));
                            } else {
                                map.put("tripEndBalance", "0");
                            }

                            map.put("settlementRemarks", settlementRemarks);
                            map.put("paymentMode", paymentMode);
                            map.put("userId", userId);
                            map.put("vehicleId", tripTO.getVehicleId());
                            System.out.println("the insertSettlement" + map);
                            insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                            System.out.println("insert status = " + insertTripDriverSettlementDetails);
                        }
                    }

                }

                int vehicleDriverCount = 0;
                String driverId = "";
                vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                if (vehicleDriverCount > 0) {
                    map.put("driverId", "");
                    driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                    System.out.println("driverClosureList.size() = " + driverClosureList.size());
                    Iterator itr = driverClosureList.iterator();
                    TripTO tTO = new TripTO();
                    while (itr.hasNext()) {
                        tTO = new TripTO();
                        tTO = (TripTO) itr.next();
                        map.put("tripSheetId", tripSheetId);
                        map.put("driverId", tTO.getDriverId());
                        map.put("estimatedExpense", tTO.getEstimatedExpense());
                        map.put("bpclTransactionAmount", 0);
                        map.put("rcmExpense", tTO.getEstimatedExpense());
                        map.put("vehicleDieselConsume", Float.parseFloat(tTO.getRunKm()) / Float.parseFloat(tTO.getVehicleMileage()));
                        map.put("reeferDieselConsume", Float.parseFloat(tTO.getRunHm()) * Float.parseFloat(tTO.getReeferMileage()));
                        map.put("tripStartingBalance", 0);
                        if (!"".equals(gpsHm) && gpsHm != null) {
                            map.put("gpsHm", gpsHm);
                        } else {
                            map.put("gpsHm", "0");
                        }
                        if (!"".equals(gpsKm)) {
                            map.put("gpsKm", gpsKm);
                        } else {
                            map.put("gpsKm", "0");
                        }
                        if (!"".equals(tTO.getRunKm())) {
                            map.put("totalRunKm", tTO.getRunKm());
                        } else {
                            map.put("totalRunKm", "0");
                        }
                        if (!"".equals(tTO.getTripTransitDays())) {
                            map.put("totalDays", tTO.getTripTransitDays());
                        } else {
                            map.put("totalDays", "0");
                        }
                        if (!"".equals(tTO.getRunHm())) {
                            map.put("totalRefeerHours", tTO.getRunHm());
                        } else {
                            map.put("totalRefeerHours", "0");
                        }
                        if (!"".equals(tTO.getVehicleMileage())) {
                            map.put("milleage", tTO.getVehicleMileage());
                        } else {
                            map.put("milleage", "0");
                        }
                        if (!"".equals(tTO.getFuelCost())) {
                            map.put("fuelPrice", tTO.getFuelCost());
                        } else {
                            map.put("fuelPrice", "0");
                        }
                        if (!"".equals(tTO.getFuelConsumption())) {
                            map.put("tripDieselConsume", tTO.getFuelConsumption());
                        } else {
                            map.put("tripDieselConsume", "0");
                        }
                        if (!"".equals(tTO.getEstimatedExpense())) {
                            map.put("tripRcmAllocation", tTO.getEstimatedExpense());
                        } else {
                            map.put("tripRcmAllocation", "0");
                        }
                        if (!"".equals(totalTripAdvance)) {
                            map.put("totalTripAdvance", Float.parseFloat(totalTripAdvance) - Float.parseFloat(tpTO.getAdvancePaid()) / vehicleDriverCount);
                        } else {
                            map.put("totalTripAdvance", "0");

                        }
                        if (!"".equals(tTO.getMiscValue())) {
                            map.put("totalTripMisc", tTO.getMiscValue());
                        } else {
                            map.put("totalTripMisc", "0");
                        }
                        if (!"".equals(tTO.getDriverBhatta())) {
                            map.put("totalTripBhatta", tTO.getDriverBhatta());
                        } else {
                            map.put("totalTripBhatta", "0");
                        }
                        String startDate = tpTO.getStartDate();
                        String endDate = tpTO.getEndDate();
                        map.put("tripStartDate", startDate);
                        map.put("tripEndDate", endDate);
                        String expenseValue = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleExtraExpenseEnd", map);
                        if (!"".equals(expenseValue)) {
                            map.put("tripExtraExpense", Float.parseFloat(expenseValue) / vehicleDriverCount);
                        } else {
                            map.put("tripExtraExpense", "0");
                        }
                        if (!"".equals(totalTripExpense)) {
                            map.put("totalTripExpense", Float.parseFloat(tTO.getEstimatedExpense()) / vehicleDriverCount);
                        } else {
                            map.put("totalTripExpense", "0");
                        }

                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripEndBalance)) {
                            map.put("tripEndBalance", (Float.parseFloat(tTO.getEstimatedExpense())) - (Float.parseFloat(tpTO.getAdvancePaid())) / vehicleDriverCount);
                        } else {
                            map.put("tripEndBalance", "0");
                        }

                        map.put("settlementRemarks", settlementRemarks);
                        map.put("paymentMode", paymentMode);
                        map.put("userId", userId);
                        map.put("vehicleId", tripTO.getVehicleId());
                        System.out.println("the insertSettlement" + map);
                        insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                        System.out.println("insert status = " + insertTripDriverSettlementDetails);
                    }

                }

            } else {
                //Driver Not Change Case
                int vehicleDriverCount = 0;
                String driverId = "";
                vehicleDriverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleDriverCount", map);
                System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                if (vehicleDriverCount > 0) {
                    driverClosureList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverClosureList", map);
                    System.out.println("driverClosureList.size() = " + driverClosureList.size());
                    Iterator itr = driverClosureList.iterator();
                    TripTO tpTO = new TripTO();
                    while (itr.hasNext()) {
                        tpTO = new TripTO();
                        tpTO = (TripTO) itr.next();
                        map.put("tripSheetId", tripSheetId);
                        map.put("driverId", tpTO.getDriverId());
                        map.put("estimatedExpense", tpTO.getEstimatedExpense());
                        map.put("bpclTransactionAmount", Float.parseFloat(bpclTransactionAmount) / vehicleDriverCount);
                        map.put("rcmExpense", tpTO.getEstimatedExpense());
                        map.put("vehicleDieselConsume", Float.parseFloat(vehicleDieselConsume) / vehicleDriverCount);
                        map.put("reeferDieselConsume", Float.parseFloat(reeferDieselConsume) / vehicleDriverCount);
                        map.put("tripStartingBalance", tripStartingBalance);
                        if (!"".equals(gpsHm) && gpsHm != null) {
                            map.put("gpsHm", gpsHm);
                        } else {
                            map.put("gpsHm", "0");
                        }
                        if (!"".equals(gpsKm)) {
                            map.put("gpsKm", gpsKm);
                        } else {
                            map.put("gpsKm", "0");
                        }
                        if (!"".equals(tpTO.getRunKm())) {
                            map.put("totalRunKm", tpTO.getRunKm());
                        } else {
                            map.put("totalRunKm", "0");
                        }
                        if (!"".equals(totalDays)) {
                            map.put("totalDays", totalDays);
                        } else {
                            map.put("totalDays", "0");
                        }
                        if (!"".equals(tpTO.getRunHm())) {
                            map.put("totalRefeerHours", tpTO.getRunHm());
                        } else {
                            map.put("totalRefeerHours", "0");
                        }
                        if (!"".equals(tpTO.getVehicleMileage())) {
                            map.put("milleage", tpTO.getVehicleMileage());
                        } else {
                            map.put("milleage", "0");
                        }
                        if (!"".equals(fuelPrice)) {
                            map.put("fuelPrice", fuelPrice);
                        } else {
                            map.put("fuelPrice", "0");
                        }
                        if (!"".equals(tpTO.getFuelConsumption())) {
                            map.put("tripDieselConsume", tpTO.getFuelConsumption());
                        } else {
                            map.put("tripDieselConsume", "0");
                        }
                        if (!"".equals(tpTO.getEstimatedExpense())) {
                            map.put("tripRcmAllocation", tpTO.getEstimatedExpense());
                        } else {
                            map.put("tripRcmAllocation", "0");
                        }
                        if (!"".equals(totalTripAdvance)) {
                            map.put("totalTripAdvance", Float.parseFloat(totalTripAdvance) / vehicleDriverCount);
                        } else {
                            map.put("totalTripAdvance", "0");

                        }
                        if (!"".equals(tpTO.getMiscValue())) {
                            map.put("totalTripMisc", tpTO.getMiscValue());
                        } else {
                            map.put("totalTripMisc", "0");
                        }
                        if (!"".equals(tpTO.getDriverBhatta())) {
                            map.put("totalTripBhatta", tpTO.getDriverBhatta());
                        } else {
                            map.put("totalTripBhatta", "0");
                        }
                        if (!"".equals(tripExtraExpense)) {
                            map.put("tripExtraExpense", Float.parseFloat(tripExtraExpense) / vehicleDriverCount);
                        } else {
                            map.put("tripExtraExpense", "0");
                        }
                        if (!"".equals(totalTripExpense)) {
                            map.put("totalTripExpense", Float.parseFloat(totalTripExpense) / vehicleDriverCount);
                        } else {
                            map.put("totalTripExpense", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", Float.parseFloat(tripBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripBalance)) {
                            map.put("tripBalance", Float.parseFloat(tripBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripBalance", "0");
                        }
                        if (!"".equals(tripEndBalance)) {
                            map.put("tripEndBalance", Float.parseFloat(tripEndBalance) / vehicleDriverCount);
                        } else {
                            map.put("tripEndBalance", "0");
                        }

                        map.put("settlementRemarks", settlementRemarks);
                        map.put("paymentMode", paymentMode);
                        map.put("userId", userId);
                        map.put("vehicleId", tripTO.getVehicleId());
                        System.out.println("the insertSettlement" + map);
                        insertTripDriverSettlementDetails = (Integer) getSqlMapClientTemplate().update("trip.insertTripDriverSettlementDetails", map);
                        System.out.println("insert status = " + insertTripDriverSettlementDetails);
                    }

                }

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return insertTripDriverSettlementDetails;
    }

    // Arul Starts 12-12-2013
    public ArrayList getEndTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        if (tripTO.getActiveStatus() == null || "".equals(tripTO.getActiveStatus())) {
            map.put("activeStatus", "Y");
            System.out.println("tripTO.getActiveStatus()-EEEEE--");
        } else {
            map.put("activeStatus", tripTO.getActiveStatus());
            System.out.println("tripTO.getActiveStatus()-SSSS--" + tripTO.getActiveStatus());
        }
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerName());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("podStatus", tripTO.getPodStatus());
        map.put("driverId", tripTO.getDriverId());

        System.out.println("map in the end trip sheet details= " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            int driverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getDriverCountPerTrip", map);
            System.out.println("driverCount==" + driverCount);
            map.put("driverCount", driverCount);
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripSheetDetails", map);
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getClosureEndTripSheetDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerName());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("podStatus", tripTO.getPodStatus());
        map.put("activeVehicle", tripTO.getActiveVehicle());
        int tripVehicleCount = 0;
        System.out.println("map in the end trip sheet details= " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            System.out.println("tripVehicleCount = " + tripVehicleCount);
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureEndTripSheetDetails", map);
            //hididng vehicle Change logic for DICT as it is not applicable for now 02/04/2016
//            if (tripVehicleCount == 1) {
//            } else {
//                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureEndTripSheetDetailsForVehicleChange", map);
//            }
            System.out.println("getTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getClosureApprovalProcessHistory(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        System.out.println("map = " + map);
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getClosureApprovalProcessHistory", map);
            System.out.println("getClosureApprovalProcessHistory result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosureApprovalProcessHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosureApprovalProcessHistory List", sqlException);
        }

        return result;
    }

    public String getGpsKm(String tripSheetId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String gpsKm = "";
        try {
            map.put("tripSheetId", tripSheetId);
            gpsKm = (String) getSqlMapClientTemplate().queryForObject("trip.getGpsKm", map);
            System.out.println("gpsKm in the dao test by arul = " + gpsKm);
            if (gpsKm == null) {
                gpsKm = "0-0";
            }
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("gpsKm in dao =" + gpsKm);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGpsKm Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGpsKm", sqlException);
        }
        return gpsKm;
    }

    public String getClosureApprovalStatus(String tripSheetId, String requestType) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("requestType", requestType);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getClosureApprovalStatus", map);
            System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosureApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosureApprovalStatus", sqlException);
        }
        return result;
    }

    public String getRevenueApprovalStatus(String customerId, String requestType) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("customerId", customerId);
            map.put("requestType", requestType);
            System.out.println("map for approve status:" + map);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getRevenueApprovalStatus", map);
            System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosureApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getClosureApprovalStatus", sqlException);
        }
        return result;
    }

    public String getMiscValue(String vehicleTypeId) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String getMiscValue = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map = " + map);
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue3118", map);
            } else {
                getMiscValue = (String) getSqlMapClientTemplate().queryForObject("trip.getMiscValue", map);
            }
            //resultList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentOrderRevenue", map);
            System.out.println("revenue=" + getMiscValue);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMiscValue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMiscValue", sqlException);
        }
        return getMiscValue;
    }

    public String getBookedExpense(String tripSheetId, TripTO tripTO) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getBookedExpense", map);
            System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBookedExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBookedExpense", sqlException);
        }
        return result;
    }
//Nithya Start 7 Dec

    public int savePreTripSheetDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("preStartDate", tripTO.getPreStartDate());
        map.put("preStarttime", tripTO.getTripPreStartHour() + ":" + tripTO.getTripPreStartMinute() + ":00");
        map.put("preOdometerReading", tripTO.getPreOdometerReading());
        map.put("preTripRemarks", tripTO.getPreTripRemarks());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("preStartHM", tripTO.getPreStartHM());
        map.put("statusId", tripTO.getStatusId());
        map.put("userId", userId);
        System.out.println("the pretripsheetdetails" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertPreTripSheetDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updatePreTripSheetDetails", map);
//            updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            System.out.println("savePreTripSheetDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public int setAdvanceAdvice(TripTO tripTO) {
        Map map = new HashMap();
        map.put("estimatedAdvancePerDay", tripTO.getEstimatedAdvancePerDay());
        map.put("userId", tripTO.getUserId());
        map.put("tripId", tripTO.getTripSheetId());
        System.out.println("the setAdvanceAdvice" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripAdvance", map);
            System.out.println("setAdvanceAdvice size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillHeader(TripTO tripTO) {
        Map map = new HashMap();

        map.put("billingPartyId", tripTO.getCustomerId());
        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("billType", "1");
        map.put("noOfTrips", tripTO.getNoOfTrips());
        map.put("grandTotal", tripTO.getGrandTotal());
        map.put("totalRevenue", tripTO.getTotalRevenue());
        map.put("totalExpToBeBilled", tripTO.getTotalExpToBeBilled());
        System.out.println("the saveBillHeader" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillHeader", map);
            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetails(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("tripId", tripTO.getTripId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("totalRevenue", tripTO.getEstimatedRevenue());
        map.put("totalExpToBeBilled", tripTO.getExpenseToBeBilledToCustomer());
        Float grandTotal = Float.parseFloat(tripTO.getEstimatedRevenue()) + Float.parseFloat(tripTO.getExpenseToBeBilledToCustomer());
        map.put("grandTotal", grandTotal);
        System.out.println("the saveBillDetails:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().insert("trip.saveBillDetails", map);
            System.out.println("saveBillDetails size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveBillDetailExpense(TripTO tripTO) {
        Map map = new HashMap();

        map.put("userId", tripTO.getUserId());
        map.put("invoiceCode", tripTO.getInvoiceCode());
        map.put("invoiceDetailId", tripTO.getInvoiceDetailId());
        map.put("tripId", tripTO.getTripId());
        map.put("expenseId", tripTO.getExpenseId());
        map.put("expenseName", tripTO.getExpenseName() + "; " + tripTO.getExpenseRemarks());
        map.put("marginValue", tripTO.getMarginValue());
        map.put("taxPercentage", tripTO.getTaxPercentage());
        map.put("expenseValue", tripTO.getExpenseValue());
        Float totalValue = 0.00F;
        Float taxValue = 0.00F;
        Float nettValue = 0.00F;
        String marginValue = "0";
        String expenseValue = "0";
        String taxPercentage = "0";
        if (tripTO.getMarginValue() == null || "".equals(tripTO.getMarginValue())) {
            marginValue = "0";
        } else {
            marginValue = tripTO.getMarginValue();
        }
        if (tripTO.getTaxPercentage() == null || "".equals(tripTO.getTaxPercentage())) {
            taxPercentage = "0";
        } else {
            taxPercentage = tripTO.getTaxPercentage();
        }
        if (tripTO.getExpenseValue() == null || "".equals(tripTO.getExpenseValue())) {
            expenseValue = "0";
        } else {
            expenseValue = tripTO.getExpenseValue();
        }
        totalValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue));
        taxValue = ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        nettValue = (Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                + ((Float.parseFloat(expenseValue) + Float.parseFloat(marginValue))
                * Float.parseFloat(taxPercentage) / 100);
        map.put("taxValue", taxValue);
        map.put("totalValue", totalValue);
        map.put("nettValue", nettValue);
        System.out.println("the saveBillDetailExpense:" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveBillDetailExpense", map);
            System.out.println("saveBillDetailExpense size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

//   public int updateStartTripSheet(String[] containerTypeId,String[] containerNo,TripTO tripTO, int userId) {
//        Map map = new HashMap();
//        map.put("userId", userId);
//        map.put("planStartTime", tripTO.getPlanStartHour() + ":" + tripTO.getPlanStartMinute() + ":00");
//        map.put("planStartDate", tripTO.getPlanStartDate());
//        map.put("startDate", tripTO.getStartDate());
//        map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
//        map.put("startOdometerReading", tripTO.getStartOdometerReading());
//        map.put("startHM", tripTO.getStartHM());
//        map.put("tripSheetId", tripTO.getTripSheetId());
//        map.put("tripId", tripTO.getTripSheetId());
//        map.put("startTripRemarks", tripTO.getStartTripRemarks());
//        map.put("statusId", tripTO.getStatusId());
//        map.put("tripPlanEndDate", tripTO.getTripPlanEndDate());
//        map.put("tripPlanEndTime", tripTO.getTripPlanEndTime());
//        map.put("lrNumber", tripTO.getLrNumber());
//        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
//        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
//        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
//        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
//        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
//        map.put("startTrailerKM", tripTO.getStartTrailerKM());
//
//        System.out.println("the updatetripsheetdetails" + map);
//        int status = 0;
//        int update = 0;
//        String ConsignmentIdList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignment", map);
//        System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
//        String[] ConsignmentId = ConsignmentIdList.split(",");
//        for (int i = 0; i < ConsignmentId.length; i++) {
//            map.put("consignmentId", ConsignmentId[i]);
//            String ConsignmentDetailsList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentOrderType", map);
//
//            String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
////            if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("23")) {
////                map.put("consignmentStatus", "24");
////            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("26")) {
////                map.put("consignmentStatus", "27");
////            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("29")) {
////                map.put("consignmentStatus", "30");
////            } else {
////                map.put("consignmentStatus", "10");
////            }
//            map.put("consignmentStatus", "10");
//            System.out.println("the updated map:" + map);
//
//            update = (Integer) getSqlMapClientTemplate().update("trip.updateStartconsignmentSheet", map);
//            System.out.println("the update:" + update);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
//            System.out.println("the status1:" + status);
//        }
//        ArrayList tripDetails = new ArrayList();
//        try {
//            status = (Integer) getSqlMapClientTemplate().update("trip.insertStartTripSheetDetails", map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripSheet", map);
//            updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
//            if(containerTypeId != null){
//                 for(int i=1;i< containerTypeId.length;i++){
//                 map.put("containerTypeId",containerTypeId[i]) ;
//                 map.put("containerNo",containerNo[i]) ;
//                 status = (Integer) getSqlMapClientTemplate().update("trip.insertContainerDetails", map);
//                }
//            }
//
////            int maxVehicleSequence = 0;
////            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
////            map.put("vehicleSequence", maxVehicleSequence);
////            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
//
//            int insertConsignmentArticle = 0;
//            map.put("consignmentId", tripTO.getConsignmentId());
//            System.out.println("the inner" + map);
//            String[] productCodes = null;
//            String[] productNames = null;
//            String[] packageNos = null;
//            String[] weights = null;
//            String[] productbatch = null;
//            String[] productuom = null;
//            String[] loadedpackages = null;
//            productCodes = tripTO.getProductCodes();
//            productNames = tripTO.getProductNames();
//            packageNos = tripTO.getPackagesNos();
//            weights = tripTO.getWeights();
//            productbatch = tripTO.getProductbatch();
//            productuom = tripTO.getProductuom();
//            loadedpackages = tripTO.getLoadedpackages();
//            for (int i = 0; i < productCodes.length; i++) {
//                map.put("productCode", productCodes[i]);
//                map.put("producName", productNames[i]);
//                map.put("packageNos", packageNos[i]);
//                map.put("weights", weights[i]);
//                map.put("productbatch", productbatch[i]);
//                map.put("productuom", productuom[i]);
//                map.put("loadedpackages", loadedpackages[i]);
//                System.out.println("the full" + map);
//                if (productCodes[i] != null && !"".equals(productCodes[i]) && !"0".equals(productuom[i])) {
//                    insertConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentArticle", map);
//                }
//
//            }
//            String tripRevenueDetails = "";
//            String tripTemp[] = null;
//            String customerId = "";
//            String tripType = "";
//            String estimatedRevenue = "";
//            String emptyTrip = "";
//            tripRevenueDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripRevenueDetails", map);
//            System.out.println("tripRevenueDetails = " + tripRevenueDetails);
//            if (!"".equals(tripRevenueDetails)) {
//                tripTemp = tripRevenueDetails.split("~");
//                customerId = tripTemp[0];
//                tripType = tripTemp[1];
//                estimatedRevenue = tripTemp[2];
//                emptyTrip = tripTemp[3];
//            }
//            /*    if ("1".equals(tripType) && "0".equals(emptyTrip)) {
//            String code2 = "";
//            String[] temp = null;
//            int insertStatus = 0;
//            map.put("userId", userId);
//            map.put("DetailCode", "1");
//            map.put("voucherType", "%SALES%");
//            code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
//            temp = code2.split("-");
//            int codeval2 = Integer.parseInt(temp[1]);
//            int codev2 = codeval2 + 1;
//            String voucherCode = "SALES-" + codev2;
//            System.out.println("voucherCode = " + voucherCode);
//            map.put("voucherCode", voucherCode);
//            map.put("mainEntryType", "VOUCHER");
//            map.put("entryType", "SALES");
//
//            //get ledger info
//            map.put("customer", customerId);
//            String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerLedgerInfo", map);
//            System.out.println("ledgerInfo:" + ledgerInfo);
//            temp = ledgerInfo.split("~");
//            String ledgerId = temp[0];
//            String particularsId = temp[1];
//            map.put("ledgerId", ledgerId);
//            map.put("particularsId", particularsId);
//
//            map.put("amount", estimatedRevenue);//hidde for crossing amount edit option by madhavan
//            //  map.put("amount", crossingAmount);
//            map.put("Accounts_Type", "DEBIT");
//            map.put("Remark", "Freight Charges");
//            map.put("Reference", "Trip");
//            //                System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
//            //                getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
//            System.out.println("tripId = " + tripTO.getTripSheetId());
//            map.put("SearchCode", tripTO.getTripSheetId());
//            System.out.println("map1 =---------------------> " + map);
//            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
//            System.out.println("status1 = " + insertStatus);
//            //--------------------------------- acc 2nd row start --------------------------
//            if (insertStatus > 0) {
//            map.put("DetailCode", "2");
//            map.put("ledgerId", "51");
//            map.put("particularsId", "LEDGER-39");
//            map.put("Accounts_Type", "CREDIT");
//            System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + estimatedRevenue);
//            System.out.println("map2 =---------------------> " + map);
//            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
//            System.out.println("status2 = " + insertStatus);
//            }
//
//            }
//             */
//
//            System.out.println("updateStartTripSheet size=" + tripDetails.size());
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
//        }
//
//        return status;
//    }
    public ArrayList getTripPODDetails(String tripSheetId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripSheetId);

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPODDetails", map);
            System.out.println("getTripPODDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPODDetails List", sqlException);
        }

        return tripDetails;
    }

    public int saveTripPodDetails(String actualFilePath, String cityId1, String tripSheetId1, String podRemarks1, String lrNumber1, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            if (tripSheetId1 != null && tripSheetId1 != "") {
                tripId = Integer.parseInt(tripSheetId1);
                map.put("tripSheetId", tripId);
            }

            if (cityId1 != null && cityId1 != "") {
                cityId = Integer.parseInt(cityId1);
                map.put("cityId", cityId);
            }
            map.put("podRemarks", podRemarks1);
            map.put("fileName", fileSaved);
            map.put("lrNumber", lrNumber1);
            map.put("userId", userId);
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] podFile = new byte[(int) file.length()];
            System.out.println("podFile = " + podFile);
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            System.out.println("the saveTripPodDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripPodDetails", map);
            map.put("consignorName", tripTO.getConsignorName());
            map.put("consigneeName", tripTO.getConsigneeName());
            map.put("consignorMobileNo", tripTO.getConsignorMobileNo());
            map.put("consigneeMobileNo", tripTO.getConsigneeMobileNo());
            map.put("consignorAddress", tripTO.getConsignorAddress());
            map.put("consigneeAddress", tripTO.getConsigneeAddress());
            map.put("consignmentNote", tripTO.getConsignmentNote());
            System.out.println("the consignorDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignorDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripPodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripPodDetails List", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleList(String companyId) throws FPRuntimeException, FPBusinessException {
        ArrayList vehicleList = null;
        Map map = new HashMap();
        map.put("companyId",companyId);
        System.out.println("MAP-----" + map);
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return vehicleList;
    }

    public ArrayList getDriverSettlementDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList settlementDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            settlementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverSettlementDetails", map);
            System.out.println("getDriverSettlementDetails.size() = " + settlementDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverSettlementDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverSettlementDetails List", sqlException);
        }
        return settlementDetails;
    }

    public int createGPSLogForTripStart(String tripId) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripId", tripId);
            System.out.println("the createGPSLogForTripStart" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.createGPSLogForTripStart", map);
            System.out.println("createGPSLogForTripStart size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("createGPSLogForTripStart Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "createGPSLogForTripStart List", sqlException);
        }

        return status;
    }

    public int createGPSLogForTripEnd(String tripId) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripId", tripId);
            System.out.println("the createGPSLogForTripEnd" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.createGPSLogForTripEnd", map);
            System.out.println("createGPSLogForTripEnd =" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("createGPSLogForTripEnd Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "createGPSLogForTripEnd List", sqlException);
        }

        return status;
    }

    public ArrayList getStartTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        try {
//            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
//            if(tripVehicleCount == 1){
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartTripDetails", map);
//            }else{
//            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartTripDetailsForVehicleChange", map);
//            }
            System.out.println("getStartTripDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartTripDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getPreStartTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        System.out.println("getPreStartTripDetails" + tripTO.getTripId());
        ArrayList tripPreStartDetails = new ArrayList();
        try {
            tripPreStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPreStartTripDetails", map);
            System.out.println("getPreStartTripDetails size=" + tripPreStartDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreStartTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPreStartTripDetails List", sqlException);
        }

        return tripPreStartDetails;
    }

    public ArrayList getStartedTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        String vehicleId = "";
        ArrayList tripStartDetails = new ArrayList();
        int tripVehicleCount = 0;
        System.out.println("map in trip start details = " + map);
        try {
            tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetails", map);
            System.out.println("tripVehicleCount in DAO if size = " + tripStartDetails.size());
            System.out.println("getStartedTripDetails size=" + tripStartDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartedTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartedTripDetails List", sqlException);
        }

        return tripStartDetails;
    }

    public ArrayList getVehicleChangeTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        String vehicleId = "";
        ArrayList tripStartDetails = new ArrayList();
        int tripVehicleCount = 0;
        System.out.println("map in trip start details = " + map);
        try {
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            System.out.println("tripVehicleCount in DAO = " + tripVehicleCount);
            if (tripVehicleCount == 1) {
                System.out.println("tripVehicleCount in DAO if = " + tripVehicleCount);
                tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetails", map);
                System.out.println("tripVehicleCount in DAO if size = " + tripStartDetails.size());
            } else {
                System.out.println("tripVehicleCount in DAO else = " + tripVehicleCount);
                vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
                map.put("vehicleId", vehicleId);
                tripStartDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStartedTripDetailsForVehicleChange", map);
                System.out.println("tripVehicleCount in DAO else size = " + tripStartDetails.size());
            }
            System.out.println("getStartedTripDetails size=" + tripStartDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStartedTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStartedTripDetails List", sqlException);
        }

        return tripStartDetails;
    }

    public ArrayList getEndTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripEndDetails = new ArrayList();
        int tripVehicleCount = 0;
        try {
            tripVehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleCount", map);
            if (tripVehicleCount == 1) {
                System.out.println(" 2222222222");
                tripEndDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripDetailsForVehicleChange", map);
            } else {
                tripEndDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEndTripDetails", map);
                System.out.println(" 111111111111");
            }
            System.out.println("getEndTripDetails size=" + tripEndDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEndTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEndTripDetails List", sqlException);
        }

        return tripEndDetails;
    }

    public ArrayList getGPSDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList gpsDetails = new ArrayList();
        try {
            gpsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getGPSDetails", map);
            System.out.println("getGPSDetails size=" + gpsDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGPSDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getGPSDetails List", sqlException);
        }

        return gpsDetails;
    }

    public ArrayList getTotalExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        ArrayList expenseDetails = new ArrayList();
        String vehicleId = "";
        String modelId = "";
        String vehicleTypeId = "";
        vehicleId = tripTO.getVehicleId();
        try {

            if ("".equals(vehicleId) && vehicleId == null && "0".equals(vehicleId)) {
                vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleId", map);
            }
            map.put("vehicleId", vehicleId);
            modelId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleModelId", map);
            map.put("modelId", modelId);
            if (!"0".equals(vehicleId)) {
                vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeId", map);
            } else {
                vehicleTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeIdFromTrip", map);
            }
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map expenseDetails" + map);
            expenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTotalExpenseDetails", map);
            System.out.println("getTotalExpenseDetails size=" + expenseDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getTotalExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalExpenseDetails List", sqlException);
        }

        return expenseDetails;
    }

    public String getTollAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        map.put("vehicleTypeId", vehicleTypeId);
        System.out.println("map = " + map);
        try {
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount3118", map);
            } else {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTollAmount", map);
            }
            System.out.println("getTollAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tollAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tollAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getSecondaryRouteExpense(TripTO tripTO) {
        Map map = new HashMap();
        String result = "";
        try {
            map.put("tripSheetId", tripTO.getTripId());
            System.out.println("map = " + map);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryRouteExpense", map);
            System.out.println("getSecondaryRouteExpense size=" + result);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryRouteExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryRouteExpense List", sqlException);
        }

        return result;
    }

    public String getDriverIncentiveAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        map.put("vehicleTypeId", vehicleTypeId);
        try {
            if (vehicleTypeId.equals("1021") || vehicleTypeId.equals("1022")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            } else if (vehicleTypeId.equals("1023") || vehicleTypeId.equals("1024")) {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            } else {
                tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverIncentive", map);
            }
            System.out.println("getDriverIncentive size=" + tollAmount);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverIncentive Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIncentive List", sqlException);
        }

        return tollAmount;
    }

    public String getDriverBataAmount(String vehicleTypeId) {
        Map map = new HashMap();
        String tollAmount = "";
        try {
            tollAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverBatta", map);
            System.out.println("getDriverBataAmount size=" + tollAmount);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverBataAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverBataAmount List", sqlException);
        }

        return tollAmount;
    }

    public String getFuelPrice(String vehicleTypeId) {
        Map map = new HashMap();
        String fuelPrice = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            fuelPrice = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelPrice", map);
            System.out.println("getFuelPrice size=" + fuelPrice);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelPrice;
    }

    public String getFuelType(String vehicleTypeId) {
        Map map = new HashMap();
        String fuelType = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            System.out.println("map:" + map);
            fuelType = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelType", map);
            System.out.println("getFuelType size=" + fuelType);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelType List", sqlException);
        }

        return fuelType;
    }

    public String getSecondaryFuelPrice(String vehicleTypeId, TripTO tripTo) {
        Map map = new HashMap();
        String fuelPrice = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("tripId", tripTo.getTripId());
            System.out.println("map:" + map);
            fuelPrice = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFuelPrice", map);
            System.out.println("getFuelPrice size=" + fuelPrice);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelPrice;
    }

    public String getSecondaryFuelType(String vehicleTypeId, TripTO tripTo) {
        Map map = new HashMap();
        String secondaryFuelType = "";
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("tripId", tripTo.getTripId());
            System.out.println("map:" + map);
            secondaryFuelType = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFuelType", map);
            System.out.println("getSecondaryFuelType size=" + secondaryFuelType);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryFuelType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryFuelType List", sqlException);
        }

        return secondaryFuelType;
    }

    public String getTripEmails(String tripId, String statusId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        map.put("statusId", statusId);
        String result = "";
        try {
            System.out.println("map:" + map);
            result = (String) getSqlMapClientTemplate().queryForObject("trip.getTripEmails", map);
            System.out.println("getTripEmails value=" + result);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripEmails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripEmails List", sqlException);
        }

        return result;
    }

    public int saveTripClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("estimatedExpense", tripTO.getEstimatedExpense());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("runKM", tripTO.getTotalRumKM());
        map.put("reeferHours", tripTO.getTotalRefeerHours());
        map.put("reeferMinutes", tripTO.getTotalRefeerMinutes());
        map.put("mileage", tripTO.getMilleage());
        map.put("reeferConsumption", tripTO.getReeferConsumption());
        map.put("fuelConsumed", tripTO.getFuelConsumed());
        map.put("fuelAmount", tripTO.getFuelAmount());
        map.put("fuelCost", tripTO.getFuelCost());
        map.put("tollAmount", tripTO.getTollAmount());
        map.put("tollCost", tripTO.getTollCost());
        map.put("incentiveAmount", tripTO.getIncentiveAmount());
        map.put("driverIncentive", tripTO.getDriverIncentive());
        map.put("battaAmount", tripTO.getBattaAmount());
        map.put("driverBatta", tripTO.getDriverBatta());
        map.put("routeExpense", tripTO.getRouteExpense());
        map.put("totalExpense", tripTO.getTotalExpenses());
        map.put("userId", userId);
        System.out.println("the saveTripClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripClosure", map);
            System.out.println("saveTripClosure size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosure List", sqlException);
        }

        return status;
    }

    public int saveTripClosureDetails(TripTO tripTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        System.out.println("close trip start");
        map.put("tripSheetId", tripTO.getTripId());
//        map.put("vehicleId", tripTO.getVehicleId());
        if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", tripTO.getVehicleId());
        }
        map.put("vendorId", tripTO.getVendorId());
        if ("1".equals(tripTO.getVendorId())) {
            map.put("hireVehicleNo", "0");
        } else {
            map.put("hireVehicleNo", tripTO.getHireVehicleNo());
        }
        int offloadStatus = 0;
        int statusId = 0;
        map.put("statusId", tripTO.getStatusId());
        map.put("estimatedExpense", tripTO.getEstimatedExpense());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("runKM", tripTO.getRunKm());
        map.put("reeferHours", tripTO.getRunHm());
        map.put("gpsKm", tripTO.getGpsKm());
        map.put("gpsHm", tripTO.getGpsHm());
        map.put("reeferMinutes", "0");
        map.put("mileage", tripTO.getMilleage());
        System.out.println("close trip start MAP--" + map);
        if (tripTO.getReeferMileage().equals("")) {
            map.put("reeferConsumption", "0");
        } else {
            map.put("reeferConsumption", tripTO.getReeferMileage());
        }
        map.put("haltCntrAmt", tripTO.getHaltCntrAmt());

        map.put("haltStartDate", tripTO.getHaltStartDate());
        map.put("haltStartHour", tripTO.getHaltStartHour());
        map.put("haltStartMinute", tripTO.getHaltStartMinute());

        map.put("haltEndDate", tripTO.getHaltEndDate());
        map.put("haltEndHour", tripTO.getHaltEndHour());
        map.put("haltEndMinute", tripTO.getHaltEndMinute());
        map.put("activeVehicle", tripTO.getActiveInd());
        System.out.println("close trip start MAP-1-" + map);
        System.out.println("tripTO.getTripDay()--" + tripTO.getTripDay());
        if (tripTO.getTripDay() == null || "".equals(tripTO.getTripDay()) || "0".equals(tripTO.getTripDay())) {
            map.put("haltDays", "0");
        } else {
            map.put("haltDays", tripTO.getTripDay());
        }
        System.out.println("MAP" + map);
        System.out.println("tripTO.getDieselUsed()--------" + tripTO.getDieselUsed());
        System.out.println("tripTO.getDieselCost()--------" + tripTO.getDieselCost());
        System.out.println("tripTO.getVehicleId()--------" + tripTO.getVehicleId());

        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId()) && !"0".equals(tripTO.getVehicleId())) {
//            if (!tripTO.getDieselUsed().equals("") && !tripTO.getDieselUsed().equals("?") && tripTO.getDieselUsed() != null) {
            if (!tripTO.getDieselUsed().equals("?")) {
                map.put("fuelConsumed", tripTO.getDieselUsed());
                System.out.println("10-01-2020 - 1 : |" + tripTO.getDieselUsed() + "|");
            } else {
                map.put("fuelConsumed", "0");
                tripTO.setDieselUsed("0");
                System.out.println("10-01-2020 : " + tripTO.getDieselUsed());
            }

            if (!tripTO.getDieselCost().equals("?")) {
                map.put("fuelCost", tripTO.getDieselCost());
                System.out.println("10-01-2020 - 1 : |" + tripTO.getDieselCost() + "|");
            } else {
                tripTO.setDieselCost("0");
                map.put("fuelCost", "0");
                System.out.println("10-01-2020 : " + tripTO.getDieselCost());
            }
            System.out.println("tripTO.getSystemExpense()-----" + tripTO.getSystemExpense());
            if (!tripTO.getSystemExpense().equals("?")) {
                map.put("systemExpenses", tripTO.getSystemExpense());
                System.out.println("system exp 10-01-2020 - 1 : |" + tripTO.getDieselUsed() + "|");
            } else {
                map.put("systemExpenses", tripTO.getSystemExpense());
                tripTO.setSystemExpense("0");
                System.out.println("10-01-2020 system exp : " + tripTO.getDieselUsed());
            }
        } else {
//            if (tripTO.getDieselUsed().equals("?")) {
            map.put("fuelConsumed", "0");
            map.put("fuelCost", "0");
            tripTO.setDieselUsed("0");
            tripTO.setDieselCost("0");
        }

        map.put("fuelAmount", tripTO.getFuelPrice());
        map.put("tollAmount", tripTO.getTollRate());
        map.put("tollCost", tripTO.getTollCost());
        map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
        map.put("driverIncentive", tripTO.getDriverIncentive());
        map.put("miscValue", tripTO.getMiscValue());
        map.put("battaAmount", tripTO.getDriverBattaPerDay());
        map.put("driverBatta", tripTO.getDriverBatta());
        map.put("routeExpense", tripTO.getBookedExpense());
        map.put("totalExpense", tripTO.getTotalExpenses());
        map.put("hireCharges", tripTO.getHireCharges());
        map.put("dieselAmount", tripTO.getDieselAmount());
        map.put("userId", userId);

        if (tripTO.getSecondaryParkingAmount().equals("")) {
            map.put("secParkingAmount", "0");
        } else {
            map.put("secParkingAmount", tripTO.getSecondaryParkingAmount());
        }
        if (tripTO.getPreColingAmount().equals("")) {
            map.put("preColingAmount", "0");
        } else {
            map.put("preColingAmount", tripTO.getPreColingAmount());
        }
        if (tripTO.getSecAdditionalTollCost().equals("")) {
            map.put("secAddlTollCost", "0");
        } else {
            map.put("secAddlTollCost", tripTO.getSecAdditionalTollCost());
        }
        System.out.println("the saveTripClosure" + map);
        int status = 0;
        int driverChangeStatus = 0;
        int driverSettelment = 0;
        ArrayList tripDetails = new ArrayList();
        ArrayList vehicleDriverlist = new ArrayList();
        ArrayList inActiveVehicleDriverlist = new ArrayList();
        ArrayList vehicleChangeStatus = new ArrayList();
        try {
            if ("13".equals(tripTO.getStatusId())) {
                status = (Integer) session.insert("trip.saveTripClosure", map);
                System.out.println("saveTripDriverClosure---" + status);
            } else if ("14".equals(tripTO.getStatusId())) {
                status = (Integer) session.update("trip.updateTripClosure", map);
                System.out.println("saveTripDriverClosure-update--" + status);
            }
            System.out.println("status tripClosureId =" + status);
            if (status > 0) {
                map.put("tripClosureId", status);
                String driver = (String) session.queryForObject("trip.getTripVehicleDriverId", map);
                if (!"".equals(driver) && driver != null) {
                    map.put("driver", driver);
                } else {
                    map.put("driver", 0);
                }
                if (!"".equals(tripTO.getUnclearedAmount()) && tripTO.getUnclearedAmount() != null) {
                    map.put("unclearedAmount", tripTO.getUnclearedAmount());
                } else {
                    map.put("unclearedAmount", 0);
                }

                System.out.println(" map for Driver Settelment:" + map);
                driverSettelment = (Integer) session.update("trip.updateTripDriverSettelement", map);
                System.out.println("driverSettelment = " + driverSettelment);
                driverChangeStatus = (Integer) session.queryForObject("trip.checkDriverChangeStatus", map);
                System.out.println("driverChangeStatus = " + driverChangeStatus);
                int statuss = 0;
//                int statuss = (Integer) session.update("trip.updateTripHaltDays", map);
//                System.out.println("statuss---" + statuss);

                map.put("diversionId", tripTO.getDiversionId());
                map.put("driverName", tripTO.getDriverName());
                map.put("driverId", 0);
                map.put("driverCode", tripTO.getDriverCode());
                map.put("divertCityId", Integer.parseInt(tripTO.getDivertCityId()));
                map.put("divertCityCode", tripTO.getDivertCityCode());
                map.put("divertCharge", Double.parseDouble(tripTO.getDivertCharge()));
                map.put("divertRemarks", tripTO.getDivertRemarks());

                System.out.println("map----" + map);
//                if ("".equals(tripTO.getDiversionId()) && Double.parseDouble(tripTO.getDivertCharge()) > 0) {
//                    statuss = (Integer) session.update("trip.insertTripDiversion", map);
//                    System.out.println("statuss--diversion-" + statuss);
//                } else {
//                    if (!"".equals(tripTO.getDiversionId())) {
//                        statuss = (Integer) session.update("trip.updateTripDiversion", map);
//                        System.out.println("statuss---" + statuss);
//                    }
//                }

                map.put("contractTonnage", tripTO.getContractTonnage());
                map.put("OTId", tripTO.getOTId());
                map.put("netWt", Double.parseDouble(tripTO.getNetWt()));
                if (Double.parseDouble(tripTO.getActWt()) == 0) {
                    map.put("actWt", Double.parseDouble(tripTO.getNetWt()));
                } else {
                    map.put("actWt", Double.parseDouble(tripTO.getActWt()));
                }
                map.put("oTWt", Double.parseDouble(tripTO.getOTWt()));
                map.put("chargePerTon", Double.parseDouble(tripTO.getChargePerTon()));
                map.put("OTCharges", Double.parseDouble(tripTO.getOTCharges()));
                System.out.println("map-1---" + map);
                map.put("tripsheetId", tripTO.getTripSheetId());
                map.put("activeVehicle", tripTO.getActiveInd());
                vehicleChangeStatus = (ArrayList) session.queryForList("trip.checkVehicleChangeStatus", map);
                System.out.println("vehicleChangeStatus----" + vehicleChangeStatus.size());
                int checkTonnageEntry = (Integer) session.queryForObject("trip.checkTonnageEntry", map);
                System.out.println("checkTonnageEntry----" + checkTonnageEntry);
                System.out.println("tripTO.getTripId()----" + tripTO.getTripId());
                int cvcount = 0;
                String tripIds = "";
                Iterator itr = null;
                TripTO tpTO1 = null;
                itr = vehicleChangeStatus.iterator();
                while (itr.hasNext()) {
                    cvcount = vehicleChangeStatus.size();
                    tpTO1 = (TripTO) itr.next();
                    System.out.println("vehicleChangeStatus---" + tpTO1.getChangeVehicleStatus());
                    System.out.println("cvcount---" + cvcount);
                    if ("N".equals(tripTO.getActiveInd()) && cvcount >= 2) {
                        map.put("tripIds", tripTO.getTripSheetId());
                    } else if ("Y".equals(tripTO.getActiveInd()) && cvcount >= 2) {
                        tripIds = tripTO.getTripSheetId() + "A";
                        System.out.println("tripTO.getTripId()---+tripTO.getTripId()---" + tripTO.getTripSheetId());
                        map.put("tripIds", tripIds);
                    } else if ("Y".equals(tripTO.getActiveInd()) && cvcount == 1) {
                        map.put("tripIds", tripTO.getTripSheetId());
                    }
                }
                System.out.println("tripIdssssssss" + tripIds);

                //auto tonnage entry for PNR,empty movememnt
//                if (checkTonnageEntry == 0 && Double.parseDouble(tripTO.getOTCharges()) == 0 && "2".equals(tripTO.getIsExport())) {
//                    System.out.println("sss-auto tonnage entry");
//                    map.put("chargePerTon", 0.00);
//                    map.put("OTCharges", 0.00);
//                    statuss = (Integer) session.update("trip.insertTripOverTonnage", map);
//                    System.out.println("insertTripOverTonnage---" + statuss);
//                } else if (checkTonnageEntry == 0 && Double.parseDouble(tripTO.getOTCharges()) == 0 && "1".equals(tripTO.getIsExport())) {  //for auto entry for export if value = 0 
//                    statuss = (Integer) session.update("trip.insertTripOverTonnage", map);
//                    System.out.println("insertTripOverTonnage---" + statuss);
//                }
                //auto tonnage entry for PNR,empty movememnt                
//                if ("".equals(tripTO.getOTId()) && Double.parseDouble(tripTO.getOTCharges()) > 0) {
//                    statuss = (Integer) session.update("trip.insertTripOverTonnage", map);
//                    System.out.println("insertTripOverTonnage---" + statuss);
//                } else {
//                    if (!"".equals(tripTO.getOTId())) {
//                        statuss = (Integer) session.update("trip.updateTripOverTonnage", map);
//                        System.out.println("updateTripOverTonnage---" + statuss);
//                    }
//                }
                if (driverChangeStatus > 0) {
                    // Driver Cnage Case
                    inActiveVehicleDriverlist = (ArrayList) session.queryForList("trip.getInactiveVehicleDriver", map);
                    Iterator itr1 = inActiveVehicleDriverlist.iterator();
                    TripTO tpTO = new TripTO();
                    Float runKm = 0.0f;
                    Float totalRunKm = 0.0f;
                    Float runHm = 0.0f;
                    Float totalRunHm = 0.0f;
                    Float milleage = 0.0f;
                    Float reeferMileage = 0.0f;
                    Float dieselUsed = 0.0f;
                    Float driverBatta = 0.0f;
                    Float miscRate = 0.0f;
                    Float extraExpenseValue = 0.0f;
                    Float fuelPrice = 0.0f;
                    Float tollRate = 0.0f;
                    Float driverIncentivePerKm = 0.0f;
                    Float nettExpense = 0.0f;
                    Float bookedExpense = 0.0f;
                    Float tempEstimatedExpense = 0.0f;
                    Float estimatedExpense = 0.0f;
                    Float driverBattaPerDay = 100.0f;
                    String startDate = "";
                    String endDate = "";
                    String expenseValue = "";
                    int totalDays = 0;
                    int totalDaysSum = 0;
                    int driverCount = 0;
                    int driverCountSum = 0;
                    while (itr1.hasNext()) {
                        tpTO = new TripTO();
                        tpTO = (TripTO) itr1.next();
                        String[] tempDriverId = null;
                        driverCount = Integer.parseInt(tpTO.getDriverCount());
                        driverCountSum += Integer.parseInt(tpTO.getDriverCount());
                        totalDays = Integer.parseInt(tpTO.getTotalDays());
                        totalDaysSum += Integer.parseInt(tpTO.getTotalDays());
                        tempDriverId = tpTO.getDriverInTrip().split(",");
                        runKm = Float.parseFloat((String) tpTO.getTotalKm());
                        totalRunKm += Float.parseFloat((String) tpTO.getTotalKm());
                        runHm = Float.parseFloat((String) tpTO.getTotalHm());
                        totalRunHm += Float.parseFloat((String) tpTO.getTotalHm());
                        milleage = Float.parseFloat((String) tripTO.getMilleage());
                        reeferMileage = Float.parseFloat((String) tripTO.getReeferMileage());
                        dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                        driverBatta = totalDays * driverBattaPerDay * driverCount;
                        miscRate = Float.parseFloat((String) tripTO.getMiscRate());
                        startDate = tpTO.getStartDate();
                        endDate = tpTO.getEndDate();
                        map.put("tripStartDate", startDate);
                        map.put("tripEndDate", endDate);
                        expenseValue = (String) session.queryForObject("trip.getTripVehicleExtraExpense", map);
                        extraExpenseValue = Float.parseFloat((String) expenseValue);
                        fuelPrice = Float.parseFloat((String) tripTO.getFuelPrice());
                        tollRate = Float.parseFloat((String) tripTO.getTollRate());
                        driverIncentivePerKm = Float.parseFloat((String) tripTO.getDriverIncentivePerKm());
                        bookedExpense = Float.parseFloat((String) tripTO.getBookedExpense());
                        Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                        Float dieselCost = dieselUsed * fuelPrice;
                        Float tollCost = runKm * tollRate;
                        Float driverIncentive = runKm * driverIncentivePerKm;
                        Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                        nettExpense = systemExpense + bookedExpense;
                        tempEstimatedExpense = Float.parseFloat(tripTO.getEstimatedExpense()) / runKm;
                        estimatedExpense = tempEstimatedExpense * runKm;

                        if (tempDriverId.length > 0) {
                            for (int i = 0; i < tempDriverId.length; i++) {
                                map.put("driverId", tempDriverId[i]);
                                map.put("estimatedExpense", estimatedExpense / driverCount);
                                map.put("totalDays", totalDays);
                                map.put("runKM", runKm / driverCount);
                                map.put("reeferHours", runHm / driverCount);
                                map.put("gpsKm", 0);
                                map.put("gpsHm", 0);
                                map.put("reeferMinutes", "0");
                                map.put("mileage", milleage);
                                map.put("reeferConsumption", reeferMileage);
                                map.put("fuelConsumed", dieselUsed / driverCount);
                                map.put("fuelAmount", tripTO.getFuelPrice());
                                map.put("fuelCost", dieselCost / driverCount);
                                map.put("tollAmount", tripTO.getTollRate());
                                map.put("tollCost", tollCost / driverCount);
                                map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                                map.put("driverIncentive", driverIncentive / driverCount);
                                map.put("miscValue", miscValue);
                                map.put("battaAmount", tripTO.getDriverBattaPerDay());
                                map.put("driverBatta", driverBatta / driverCount);
                                map.put("systemExpenses", systemExpense / driverCount);
                                map.put("routeExpense", bookedExpense / driverCount);
                                map.put("totalExpense", nettExpense / driverCount);
                                System.out.println("final Map = " + map);
//                                status = (Integer) session.update("trip.saveTripDriverClosure", map);
                                if ("13".equals(tripTO.getStatusId())) {
                                    status = (Integer) session.update("trip.saveTripDriverClosure", map);
                                    System.out.println("saveTripDriverClosure---" + status);
                                } else if ("14".equals(tripTO.getStatusId())) {
                                    status = (Integer) session.update("trip.updateTripDriverClosure", map);
                                    System.out.println("saveTripDriverClosure---" + status);
                                }
                            }
                        }
                    }

                    int vehicleDriverCount = 0;
                    String driverId = "";
                    vehicleDriverCount = (Integer) session.queryForObject("trip.getVehicleDriverCount", map);
                    System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                    if (vehicleDriverCount > 0) {
                        vehicleDriverlist = (ArrayList) session.queryForList("trip.getTripVehicleDriver", map);
                        System.out.println("vehicleDriverlist.size() = " + vehicleDriverlist.size());
                        Iterator itr2 = vehicleDriverlist.iterator();
                        TripTO trpTO = new TripTO();
                        vehicleDriverCount = vehicleDriverCount - driverCountSum;
                        totalDays = totalDays - totalDaysSum;
                        while (itr2.hasNext()) {
                            trpTO = new TripTO();
                            trpTO = (TripTO) itr2.next();
                            driverId = trpTO.getDriverId();
                            System.out.println("driverId = " + driverId);
                            map.put("driverId", driverId);
                            runKm = (Float.parseFloat((String) tripTO.getRunKm()) - totalRunKm) / vehicleDriverCount;
                            runHm = (Float.parseFloat((String) tripTO.getRunHm()) - totalRunHm) / vehicleDriverCount;
                            milleage = Float.parseFloat((String) tripTO.getMilleage());
                            reeferMileage = Float.parseFloat((String) tripTO.getReeferMileage());
                            dieselUsed = (runKm / milleage) + (runHm * reeferMileage);
                            driverBatta = totalDays * driverBattaPerDay * vehicleDriverCount;
                            miscRate = Float.parseFloat((String) tripTO.getMiscRate());
                            startDate = tpTO.getStartDate();
                            endDate = tpTO.getEndDate();
                            map.put("tripStartDate", startDate);
                            map.put("tripEndDate", endDate);
                            expenseValue = (String) session.queryForObject("trip.getTripVehicleExtraExpenseEnd", map);
                            extraExpenseValue = Float.parseFloat((String) expenseValue);
                            fuelPrice = Float.parseFloat((String) tripTO.getFuelPrice());
                            tollRate = Float.parseFloat((String) tripTO.getTollRate());
                            driverIncentivePerKm = Float.parseFloat((String) tripTO.getDriverIncentivePerKm());
                            bookedExpense = Float.parseFloat((String) tripTO.getBookedExpense());
                            Float miscValue = ((runKm * miscRate) + extraExpenseValue + driverBatta) * 5 / 100;
                            Float dieselCost = dieselUsed * fuelPrice;
                            Float tollCost = runKm * tollRate;
                            Float driverIncentive = runKm * driverIncentivePerKm;
                            Float systemExpense = dieselCost + tollCost + driverIncentive + driverBatta + miscValue;
                            nettExpense = systemExpense + bookedExpense;
                            tempEstimatedExpense = Float.parseFloat(tripTO.getEstimatedExpense()) / runKm;
                            estimatedExpense = tempEstimatedExpense * runKm;
                            map.put("estimatedExpense", Float.parseFloat(tripTO.getEstimatedExpense()) / vehicleDriverCount);
                            map.put("totalDays", tripTO.getTotalDays());
                            map.put("runKM", Float.parseFloat(tripTO.getRunKm()) / vehicleDriverCount);
                            map.put("reeferHours", Float.parseFloat(tripTO.getRunHm()) / vehicleDriverCount);
                            map.put("gpsKm", Float.parseFloat(tripTO.getGpsKm()) / vehicleDriverCount);
                            map.put("gpsHm", Float.parseFloat(tripTO.getGpsHm()) / vehicleDriverCount);
                            map.put("reeferMinutes", "0");
                            map.put("mileage", tripTO.getMilleage());
                            map.put("reeferConsumption", tripTO.getReeferMileage());
                            map.put("fuelConsumed", Float.parseFloat(tripTO.getDieselUsed()) / vehicleDriverCount);
                            map.put("fuelAmount", tripTO.getFuelPrice());
                            map.put("fuelCost", Float.parseFloat(tripTO.getDieselCost()) / vehicleDriverCount);
                            map.put("tollAmount", tripTO.getTollRate());
                            map.put("tollCost", Float.parseFloat(tripTO.getTollCost()) / vehicleDriverCount);
                            map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                            map.put("driverIncentive", Float.parseFloat(tripTO.getDriverIncentive()) / vehicleDriverCount);
                            map.put("miscValue", tripTO.getMiscValue());
                            map.put("battaAmount", tripTO.getDriverBattaPerDay());
                            map.put("driverBatta", Float.parseFloat(tripTO.getDriverBatta()) / vehicleDriverCount);
                            map.put("systemExpenses", Float.parseFloat(tripTO.getSystemExpense()) / vehicleDriverCount);
                            map.put("routeExpense", Float.parseFloat(tripTO.getBookedExpense()) / vehicleDriverCount);
                            map.put("totalExpense", Float.parseFloat(tripTO.getTotalExpenses()) / vehicleDriverCount);
                            System.out.println("final Map = " + map);
                            if ("13".equals(tripTO.getStatusId())) {
                                status = (Integer) session.update("trip.saveTripDriverClosure", map);
                                System.out.println("saveTripDriverClosure---" + status);
                            } else if ("14".equals(tripTO.getStatusId())) {
                                status = (Integer) session.update("trip.updateTripDriverClosure", map);
                                System.out.println("saveTripDriverClosure---" + status);
                            }
                        }
                    }

                } else {
                    // Driver not Change Case
                    int vehicleDriverCount = 0;
                    String driverId = "";
                    vehicleDriverCount = (Integer) session.queryForObject("trip.getVehicleDriverCount", map);
                    System.out.println("vehicleDriverCount = " + vehicleDriverCount);
                    if (vehicleDriverCount > 0) {
                        vehicleDriverlist = (ArrayList) session.queryForList("trip.getTripVehicleDriver", map);
                        System.out.println("vehicleDriverlist.size() = " + vehicleDriverlist.size());
                        Iterator itr3 = vehicleDriverlist.iterator();
                        TripTO tpTO = new TripTO();
                        while (itr3.hasNext()) {
                            tpTO = new TripTO();
                            tpTO = (TripTO) itr3.next();
                            driverId = tpTO.getDriverId();
                            map.put("driverId", driverId);
                            map.put("estimatedExpense", Float.parseFloat(tripTO.getEstimatedExpense()) / vehicleDriverCount);
                            map.put("totalDays", tripTO.getTotalDays());
                            map.put("runKM", Float.parseFloat(tripTO.getRunKm()) / vehicleDriverCount);
                            map.put("reeferHours", Float.parseFloat(tripTO.getRunHm()) / vehicleDriverCount);
                            map.put("gpsKm", Float.parseFloat(tripTO.getGpsKm()) / vehicleDriverCount);
                            map.put("gpsHm", Float.parseFloat(tripTO.getGpsHm()) / vehicleDriverCount);
                            map.put("reeferMinutes", "0");
                            map.put("mileage", tripTO.getMilleage());
                            map.put("reeferConsumption", tripTO.getReeferMileage());
                            map.put("fuelConsumed", Float.parseFloat(tripTO.getDieselUsed()) / vehicleDriverCount);
                            map.put("fuelAmount", tripTO.getFuelPrice());
                            map.put("fuelCost", Float.parseFloat(tripTO.getDieselCost()) / vehicleDriverCount);
                            map.put("tollAmount", tripTO.getTollRate());
                            map.put("tollCost", Float.parseFloat(tripTO.getTollCost()) / vehicleDriverCount);
                            map.put("incentiveAmount", tripTO.getDriverIncentivePerKm());
                            map.put("driverIncentive", Float.parseFloat(tripTO.getDriverIncentive()) / vehicleDriverCount);
                            map.put("miscValue", tripTO.getMiscValue());
                            map.put("battaAmount", tripTO.getDriverBattaPerDay());
                            map.put("driverBatta", Float.parseFloat(tripTO.getDriverBatta()) / vehicleDriverCount);
                            System.out.println("finaEEEEEEEEsl Map = " + map);
                            System.out.println("tripTO.getSystemExpense() Map = " + tripTO.getSystemExpense());
                            System.out.println("tripTO.getBookedExpense() Map = " + tripTO.getBookedExpense());
                            System.out.println("tripTO.getTotalExpenses() Map = " + tripTO.getTotalExpenses());
                            
                            if ((!tripTO.getSystemExpense().equals("?")) && tripTO.getSystemExpense() != null) {
                                System.out.println("tripTO.getSystemExpense() if " + tripTO.getSystemExpense());
                                map.put("systemExpenses", Float.parseFloat(tripTO.getSystemExpense()) / vehicleDriverCount);
                            } else {
                                System.out.println("tripTO.getSystemExpense() else " + tripTO.getSystemExpense());
                                map.put("systemExpenses", 0);
                            }
                            if ((!tripTO.getBookedExpense().equals("?")) && tripTO.getBookedExpense() != null) {
                                map.put("routeExpense", Float.parseFloat(tripTO.getBookedExpense()) / vehicleDriverCount);
                            } else {
                                map.put("routeExpense", 0);
                            }
                            if ((!tripTO.getTotalExpenses().equals("?")) && tripTO.getTotalExpenses() != null) {
                                map.put("totalExpense", Float.parseFloat(tripTO.getTotalExpenses()) / vehicleDriverCount);
                            } else {
                                map.put("totalExpense", 0);
                            }
                            System.out.println("finaEEEEEEEEsl Map = " + map);

                            if ("13".equals(tripTO.getStatusId())) {
                                status = (Integer) session.update("trip.saveTripDriverClosure", map);
                                System.out.println("saveTripDriverClosure---" + status);
                            } else if ("14".equals(tripTO.getStatusId())) {
                                status = (Integer) session.update("trip.updateTripDriverClosure", map);
                                System.out.println("saveTripDriverClosure---" + status);
                            }
                        }
                    }

                }
                //account entry details start

                //fuel cost
                String code2 = "";
                String[] temp = null;
                int insertStatus = 0;
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%PAYMENT%");
                code2 = (String) session.queryForObject("trip.getTripVoucherCode", map);
                temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);

                if ("3".equals(tripTO.getOwnerShip())) {
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 1582);
                    map.put("particularsId", "LEDGER-1562");

                    map.put("amount", tripTO.getDieselCost());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Hire Charge");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                } else {
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 37);
                    map.put("particularsId", "LEDGER-29");

                    map.put("amount", tripTO.getDieselCost());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Fuel Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                    //toll cost
                    codev2++;
                    voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 41);
                    map.put("particularsId", "LEDGER-33");

                    map.put("amount", tripTO.getTollCost());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Toll Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                    //driver Bata cost
                    codev2++;
                    voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 34);
                    map.put("particularsId", "LEDGER-27");

                    map.put("amount", tripTO.getDriverBatta());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Driver Bhatta Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                    //driver Incentive cost
                    codev2++;
                    voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 34);
                    map.put("particularsId", "LEDGER-27");

                    map.put("amount", tripTO.getDriverIncentive());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Driver Incentive Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }
                    //Misc cost

                    codev2++;
                    voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    map.put("ledgerId", 40);
                    map.put("particularsId", "LEDGER-32");

                    map.put("amount", tripTO.getMiscValue());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Misc Charges");
                    map.put("Reference", "Trip");

                    System.out.println("tripId = " + tripTO.getTripId());
                    map.put("SearchCode", tripTO.getTripId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) session.update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }

                }
                //updateTripCloserToEFS(tripTO.getTripId());
                //account entry details end
                int updateTripStatus = 0;
                if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
                    map.put("vehicleId", "0");
                } else {
                    map.put("vehicleId", tripTO.getVehicleId());
                }
                map.put("vendorId", tripTO.getVendorId());
                if ("1".equals(tripTO.getVendorId())) {
                    map.put("hireVehicleNo", "0");
                } else {
                    map.put("hireVehicleNo", tripTO.getHireVehicleNo());
                }
                map.put("activeVehicle", tripTO.getActiveInd());
                offloadStatus = (Integer) session.queryForObject("trip.checkOffloadStatus", map);
                System.out.println("offloadStatus closetrip-----" + offloadStatus);
                System.out.println("tripTO.getClosureStatus() closetrip-----" + tripTO.getClosureStatus());

                if ("13".equals(tripTO.getStatusId()) && offloadStatus == 0) {   // trip closure normal
                    map.put("closeStatus", "1");
                } else if ("13".equals(tripTO.getStatusId()) && offloadStatus == 1) {    // trip closure tripEnd 
                    map.put("closeStatus", "1");
                } else if ("13".equals(tripTO.getStatusId()) && offloadStatus == 2) {    // trip closure offload trip end
                    map.put("closeStatus", "1");
                    map.put("subStatus", "48");
                    System.out.println("tripTO.getStatusId()--" + tripTO.getStatusId());
                    map.put("tripStatus", tripTO.getStatusId());
                    updateTripStatus = (Integer) session.update("trip.updateTrpStatus", map);
                    System.out.println("updateTripStatus-AAAAA----" + updateTripStatus);

                } else if ("14".equals(tripTO.getStatusId()) && offloadStatus == 0) {    // trip settlement
                    map.put("closeStatus", "2");
                } else if ("14".equals(tripTO.getStatusId()) && offloadStatus == 1) {    // trip settlement tripEnd 
                    map.put("closeStatus", "2");
                } else if ("14".equals(tripTO.getStatusId()) && offloadStatus == 2) {    // trip settlement offload trip end
                    map.put("closeStatus", "2");
                    map.put("subStatus", "48");
                    map.put("tripStatus", tripTO.getStatusId());
                    System.out.println("map0-----" + map);
                    updateTripStatus = (Integer) session.update("trip.updateTrpStatus", map);
                    System.out.println("updateTripStatus-BBBBB----" + updateTripStatus);
                }

                map.put("tripStatus", tripTO.getStatusId());
                map.put("activeVehicle", tripTO.getActiveInd());
                System.out.println("activeVehicle-close update---" + tripTO.getActiveInd());
                System.out.println("updateClosureStatus map --" + map);
                int updateClosureStatus = (Integer) session.update("trip.updateClosureStatus", map);
                System.out.println("updateClosureStatus-AAAAA----" + updateClosureStatus);
                int checkDktStatus = (Integer) session.queryForObject("trip.checkDktStatus", map);
                System.out.println("checkDktStatus---" + checkDktStatus);
                if (checkDktStatus > 0) {
                    int updateDktStatus = (Integer) session.update("trip.updateDktStatus", map);
                    System.out.println("updateDktStatus----" + updateDktStatus);
                }
            }

            System.out.println("STATUSID DAO--" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripClosure List", sqlException);
        }

        return status;
    }

    public int updateEndTripSheet(String[] containerTypeId, String[] containerNo, TripTO tripTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        map.put("endDate", tripTO.getEndDate());
        map.put("planEndDate", tripTO.getPlanEndDate());
        map.put("companyId", tripTO.getCompanyId());
        String plannedEndMinute = "00";
        String plannedEndHour = "00";
        String tripEndMinute = "00";
        String tripEndHour = "00";
        if (tripTO.getPlanEndHour() != null && !"".equals(tripTO.getPlanEndHour())) {
            plannedEndHour = tripTO.getPlanEndHour();
        }
        if (tripTO.getPlanEndMinute() != null && !"".equals(tripTO.getPlanEndMinute())) {
            plannedEndMinute = tripTO.getPlanEndMinute();
        }
        if (tripTO.getTripEndHour() != null && !"".equals(tripTO.getTripEndHour())) {
            tripEndHour = tripTO.getTripEndHour();
        }
        if (tripTO.getTripEndMinute() != null && !"".equals(tripTO.getTripEndMinute())) {
            tripEndMinute = tripTO.getTripEndMinute();
        }
        map.put("planEndTime", plannedEndHour + ":" + plannedEndMinute + ":00");
        map.put("endTime", tripEndHour + ":" + tripEndMinute + ":00");
        map.put("durationTime", tripEndHour + ":" + tripEndMinute);
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("endHM", tripTO.getEndHM());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", tripTO.getTotalKM());
        map.put("totalHrs", tripTO.getTotalHrs());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("durationDay1", tripTO.getDurationDay1());
        map.put("durationDay2", tripTO.getDurationDay2());
        map.put("endTrailerKM", tripTO.getEndTrailerKM());
        System.out.println("the updateEndTripSheet" + map);

        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        //map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");

        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
        map.put("destinationId", tripTO.getDestinationId());
        map.put("originId", tripTO.getOriginId());
        map.put("vehicleId", tripTO.getVehicleId());

        if (tripTO.getEndReportingTime() != null && !"".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getEndReportingTime());
        }
        if (tripTO.getEndReportingTime() == null && "".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
        }

        int status = 0;
        int update = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            System.out.println("map for End:" + map);
            status = (Integer) session.update("trip.insertEndTripSheetDetails", map);
            status = (Integer) session.update("trip.updateEndTripSheet", map);
            int vehicleAvailUpdate = (Integer) session.update("trip.updateVehicleAvailabilityForEnd", map);
            // updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            String trailerIds = tripTO.getTrailerId();
            String trailer[] = trailerIds.split(",");
            for (int k = 0; k < trailer.length; k++) {
                map.put("trailerId", trailer[k]);
                System.out.println("trailer availability map:" + map);
                int traileAvailUpdate = (Integer) session.update("trip.updateTrailerAvailabiltyForEnd", map);
                System.out.println("traileAvailUpdate = " + traileAvailUpdate);
            }

            // update consignment status for pick up trip end
            String ConsignmentIdList = (String) session.queryForObject("trip.getTripConsignment", map);
            System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
            String[] ConsignmentId = ConsignmentIdList.split(",");
            for (int j = 0; j < ConsignmentId.length; j++) {
                map.put("consignmentId", ConsignmentId[j]);
                map.put("consignmentOrderId", ConsignmentId[j]);
                String ConsignmentDetailsList = (String) session.queryForObject("trip.getTripConsignmentOrderType", map);
                System.out.println("the ConsignmentDetailsList size:" + ConsignmentDetailsList.length());
                String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
                if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("24")) {
                    map.put("consignmentStatus", "25");
                } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("27")) {
                    map.put("consignmentStatus", "28");
                } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("30")) {
                    map.put("consignmentStatus", "31");
                } else {
                    map.put("consignmentStatus", "12");
                }

                String orderTripDetails = null;
                orderTripDetails = (String) session.queryForObject("trip.getOrderTripDetails", map);
                System.out.println("details length is" + ConsignmentDetails[2]);
//                if(orderTripDetails!=null || Integer.parseInt(ConsignmentDetails[2])>0){
//                  map.put("consignmentStatus", "25");
//                  }
//                if(Integer.parseInt(ConsignmentDetails[2])==0 && orderTripDetails==null){
//                map.put("consignmentStatus", "26");
//                }
                System.out.println("the updated map:" + map);
                update = (Integer) session.update("trip.updateEndconsignmentSheet", map);
                System.out.println("the update:" + update);

                //   status = (Integer) session.update("trip.insertEndTripSheetDetails", map);
                map.put("collectionBranch", tripTO.getCollectionBranch());
                if ("1".equals(tripTO.getCollectionBranch())) {
                    map.put("consignmentStatus", "5");
                }
                int plannedstatus = (Integer) session.queryForObject("trip.getPlannedContainer", map);
                System.out.println("plannedstatus==" + plannedstatus);
                if (plannedstatus > 0) {
                    map.put("consignmentStatus", "5");
                    status = (Integer) session.update("trip.updateConsignmentStatus", map);
                }
//                    if (plannedstatus == 0) {

//                    }
                if (containerNo != null) {
                    for (int k = 0; k < containerNo.length; k++) {
                        //map.put("consignmentId", ConsignmentId[j]);
                        map.put("containerNo", containerNo[k]);
                        System.out.println("srini check:" + map);
                        status = (Integer) session.update("trip.updateConsignmentContainerStatus", map);
                    }
                }

                //pick up trip end
//                  updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
                int updateConsignmentArticle = 0;
                map.put("consignmentId", tripTO.getConsignmentId());
                String[] productCodes = null;
                String[] unloadedpackages = null;
                String[] shortage = null;
                String[] articleId = null;
                String[] articleName = null;
                articleName = tripTO.getProductNames();
                productCodes = tripTO.getProductCodes();
                unloadedpackages = tripTO.getUnloadedpackages();
                articleId = tripTO.getTripArticleId();
                shortage = tripTO.getShortage();
                if (tripTO.getProductCodes() != null && !"".equals(tripTO.getProductCodes()) && !"".equals(tripTO.getConsignmentId()) && tripTO.getConsignmentId() != null) {
                    for (int i = 0; i < articleName.length; i++) {
                        map.put("productCode", productCodes[i]);
                        map.put("loadedpackages", unloadedpackages[i]);
                        map.put("shortage", shortage[i]);
                        map.put("tripArticleId", articleId[i]);
                        map.put("articleName", articleName[i]);
                        System.out.println("the inner product update" + map);
                        updateConsignmentArticle = (Integer) session.update("trip.updateConsignmentArticle", map);
                    }
                }

                System.out.println("updateEndTripSheet size=" + tripDetails.size());
            }

            String tripDays = (String) session.queryForObject("trip.getTotalTripDays", map);
            String tripDriver = (String) session.queryForObject("trip.getTripDriver", map);
            map.put("tripDriver", tripDriver);
            String tripCleaner = (String) session.queryForObject("trip.getTripCleaner", map);
            map.put("tripCleaner", tripCleaner);
            System.out.println("tripDays=" + tripDays);
            System.out.println("tripDriver-tripCleaner--" + map);
            if (!"".equals(tripDays) && tripDays != null) {
                String[] tripDay = tripDays.split("~");
                System.out.println("dates=" + tripDay[0] + " to " + tripDay[1]);
                String[] dateList = getDateList(tripDay[0], tripDay[1]);
                int dailyBata = 0;
                int driverPadi = 0;
                int cleanerPadi = 0;
                for (int j = 0; j < dateList.length; j++) {
                    map.put("effDate", dateList[j]);
                    int check = 0;
                    check = (Integer) session.queryForObject("trip.getTripDriverBataExist", map);
                    // Driver salary config Master data
                    String bhata = (String) session.queryForObject("trip.getBataConfigMaster", map);
                    System.out.println("bhata-from driver salary config-" + bhata);
                    System.out.println("check=" + check);
                    if (check == 0) {
                        if (bhata != null) {
                            String[] temp = bhata.split("~");
                            int result = Integer.parseInt(temp[0]);
                            int result1 = Integer.parseInt(temp[1]);
                            driverPadi = driverPadi + result;
                            System.out.println("cleanerPadicleanerPadicleanerPadicleanerPadi--" + driverPadi);
                            cleanerPadi = cleanerPadi + result1;
                            System.out.println("cleanerPadicleanerPadicleanerPadi--" + cleanerPadi);

                            map.put("dpadi", Double.parseDouble(temp[0]));
                            map.put("cpadi", Double.parseDouble(temp[1]));
                            dailyBata = (Integer) session.update("trip.insertDailyBata", map);
                            System.out.println("dailyBata==" + dailyBata);
                        }

                    }
                }
                String totalBata = (String) session.queryForObject("trip.getTotalTripDriverBata", map);
                System.out.println("totalBata==" + totalBata + "dailyBata=" + dailyBata);
                System.out.println("driverPadi==" + driverPadi);
                System.out.println("cleanerPadi==" + cleanerPadi);
                if (driverPadi != 0 && cleanerPadi != 0 && dailyBata != 0) {
//                    String[] temp = totalBata.split("~");
                    map.put("driverBata", driverPadi);
                    map.put("cleanerBata", cleanerPadi);
                    map.put("expenseBy", tripDriver);
                    //            Driver Bata
                    map.put("netExpense", driverPadi);
                    map.put("expenses", driverPadi);
                    map.put("expenseName", "1024");
                    status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
                    System.out.println("driver padi=" + status);
                    //            Cleaner Bata
                    map.put("expenseBy", tripCleaner);
                    map.put("netExpense", cleanerPadi);
                    map.put("expenses", cleanerPadi);
                    map.put("expenseName", "1031");
                    status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
                    System.out.println("cleaner padi=" + status);
                }

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheet List", sqlException);
        }

        return status;
    }

    public int updateEndConsignmentSheet(TripTO tripTO, int userId) {
        Map map = new HashMap();
        //map.put("consignmentStatus", tripTO.getConsignmentStatusId());
        map.put("endDate", tripTO.getEndDate());
        map.put("planEndDate", tripTO.getPlanEndDate());
        String plannedEndMinute = "00";
        String plannedEndHour = "00";
        String tripEndMinute = "00";
        String tripEndHour = "00";
        if (tripTO.getPlanEndHour() != null && !"".equals(tripTO.getPlanEndHour())) {
            plannedEndHour = tripTO.getPlanEndHour();
        }
        if (tripTO.getPlanEndMinute() != null && !"".equals(tripTO.getPlanEndMinute())) {
            plannedEndMinute = tripTO.getPlanEndMinute();
        }
        if (tripTO.getTripEndHour() == null && !"".equals(tripTO.getTripEndHour())) {
            tripEndHour = tripTO.getTripEndHour();
        }
        if (tripTO.getTripEndMinute() == null && !"".equals(tripTO.getTripEndMinute())) {
            tripEndMinute = tripTO.getTripEndMinute();
        }
        map.put("planEndTime", plannedEndHour + ":" + plannedEndMinute + ":00");
        map.put("endTime", tripEndHour + ":" + tripEndMinute + ":00");
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("endHM", tripTO.getEndHM());
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", tripTO.getTotalKM());
        map.put("totalHrs", tripTO.getTotalHrs());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("durationDay1", tripTO.getDurationDay1());
        map.put("durationDay2", tripTO.getDurationDay2());
        System.out.println("the updateEndTripSheet" + map);

        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        //map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");

        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());

        if (tripTO.getEndReportingTime() != null && !"".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getEndReportingTime());
        }
        if (tripTO.getEndReportingTime() == null && !"".equals(tripTO.getEndReportingTime())) {
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
        }

        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            int update = 0;

            String ConsignmentDetailsList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentOrderType", map);
            System.out.println("the ConsignmentDetailsList size:" + ConsignmentDetailsList.length());
            String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
            if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("24")) {
                map.put("consignmentStatus", "25");
            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("27")) {
                map.put("consignmentStatus", "28");
            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("30")) {
                map.put("consignmentStatus", "31");
            } else {
                map.put("consignmentStatus", "12");
            }
            System.out.println("the updated map:" + map);
            update = (Integer) getSqlMapClientTemplate().update("trip.updateEndconsignmentSheet", map);
            System.out.println("the update:" + update);

            //   status = (Integer) getSqlMapClientTemplate().update("trip.insertEndTripSheetDetails", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);

            int updateConsignmentArticle = 0;
            String[] productCodes = null;
            String[] unloadedpackages = null;
            String[] shortage = null;
            String[] articleId = null;
            productCodes = tripTO.getProductCodes();
            unloadedpackages = tripTO.getUnloadedpackages();
            articleId = tripTO.getTripArticleId();
            shortage = tripTO.getShortage();
            if (tripTO.getProductCodes() != null && !"".equals(tripTO.getProductCodes()) && !"".equals(tripTO.getConsignmentId()) && tripTO.getConsignmentId() != null) {
                for (int i = 0; i < productCodes.length; i++) {
                    map.put("productCode", productCodes[i]);
                    map.put("loadedpackages", unloadedpackages[i]);
                    map.put("shortage", shortage[i]);
                    map.put("tripArticleId", articleId[i]);
                    System.out.println("the inner" + map);
                    //         updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentArticle", map);
                }
            }

            System.out.println("updateEndTripSheet size=" + tripDetails.size());
            System.out.println("status=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheet List", sqlException);
        }

        return status;
    }

    public ArrayList getEmptyTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList emptyTripList = new ArrayList();
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("tripId", tripTO.getTripId());
        try {
            emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripDetails", map);
            System.out.println("getEmptyTripDetails.size() = " + emptyTripList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripDetails List", sqlException);
        }
        return emptyTripList;
    }

    public int saveManualEmptyTripApproval(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", tripTO.getUserId());
        System.out.println("the saveEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int updateStartTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("startKm", tripTO.getStartKm());
        map.put("startHm", tripTO.getStartHM());
        map.put("tripStartLoadTemp", tripTO.getLoadingTemperature());
        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        map.put("userId", userId);
        System.out.println("the updateStartTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripSheetDuringClosure", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripVehicleDuringClosure", map);
            System.out.println("updateStartTripSheetDuringClosure =" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateEndTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();
//            nilesh 17 10 2015 Start
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
//            nilesh 17 10 2015 End
        map.put("endTime", tripTO.getTripEndHour() + ":" + tripTO.getTripEndMinute() + ":00");
        map.put("endDate", tripTO.getEndDate());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("tripTransitHours", tripTO.getTripTransitHours());
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("endHM", tripTO.getEndHM());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        //map.put("endTripRemarks", tripTO.getEndTripRemarks());
        map.put("totalKM", tripTO.getTotalKM());
        map.put("totalHrs", tripTO.getTotalHrs());
        map.put("userId", userId);
        map.put("statusId", 12);
        map.put("tripDetainHours", tripTO.getTripDetainHours());
        if (!"NaN".equals(tripTO.getTripFactoryToIcdHour()) || (tripTO.getTripFactoryToIcdHour()) != null) {
            map.put("tripFactoryToIcdHours", tripTO.getTripFactoryToIcdHour());
        } else {
            map.put("tripFactoryToIcdHours", 0.0);

        }
        if (!"".equals(tripTO.getSetteledKm()) && (tripTO.getSetteledKm()) != null) {
            map.put("setteledKm", tripTO.getSetteledKm());
        }
        if (!"".equals(tripTO.getSetteledHm()) && (tripTO.getSetteledHm()) != null) {
            map.put("setteledHm", tripTO.getSetteledHm());
        }
        if (!"".equals(tripTO.getSetteltotalKM()) && (tripTO.getSetteltotalKM()) != null) {
            map.put("setteltotalKM", tripTO.getSetteltotalKM());
        }
        if (!"".equals(tripTO.getSetteltotalHrs()) && (tripTO.getSetteltotalHrs()) != null) {
            map.put("setteltotalHrs", tripTO.getSetteltotalHrs());
        }
        if (!"".equals(tripTO.getBillOfEntry())) {
            map.put("billOfEntry", tripTO.getBillOfEntry());
        }
        if (!"".equals(tripTO.getShipingLineNo())) {
            map.put("shipingLineNo", tripTO.getShipingLineNo());
        }
        System.out.println("the updateEndTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripSheetDuringClosure", map);
            if (!"".equals(tripTO.getBillOfEntry())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updatConsignmentForBoE", map);
                System.out.println("BoE updated .." + status);
            }
            if (!"".equals(tripTO.getShipingLineNo())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updatConsignmentForSlN", map);
                System.out.println("sln updated .." + status);
            }
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTripVehicleDuringClosure", map);
            System.out.println("updateEndTripSheetDuringClosure size=" + tripDetails.size());
            // update product
            int updateConsignmentArticle = 0;
            map.put("consignmentId", tripTO.getConsignmentId());
            String[] productCodes = null;
            String[] unloadedpackages = null;
            String[] shortage = null;
            String[] articleId = null;
            String[] articleName = null;
            articleName = tripTO.getProductNames();
            productCodes = tripTO.getProductCodes();
            unloadedpackages = tripTO.getUnloadedpackages();
            articleId = tripTO.getTripArticleId();
            shortage = tripTO.getShortage();
            map.put("consignmentId", 0);
            map.put("productCode", tripTO.getCommodityCategory());
            map.put("productbatch", 0);
            map.put("packageNos", 0);
            map.put("weights", 0);
            map.put("productuom", 1);
            map.put("loadedpackages", 0);
            map.put("shortage", 0);
            map.put("tripArticleId", 0);
            map.put("producName", tripTO.getCommodityName());
            System.out.println("map for product..." + map);
            int article = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripArticleDeatils", map);
            if (article == 0) {
                updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentArticle", map);
            } else {
                updateConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.updateTripArticle", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int overrideEndTripSheetDuringClosure(TripTO tripTO, int userId) {
        Map map = new HashMap();

        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("overrideTripRemarks", tripTO.getOverrideTripRemarks());
        map.put("userId", userId);
        if (!"".equals(tripTO.getSetteledKm()) && (tripTO.getSetteledKm()) != null) {
            map.put("setteledKm", tripTO.getSetteledKm());
        }
        if (!"".equals(tripTO.getSetteledHm()) && (tripTO.getSetteledHm()) != null) {
            map.put("setteledHm", tripTO.getSetteledHm());
        }
        if (!"".equals(tripTO.getSetteltotalKM()) && (tripTO.getSetteltotalKM()) != null) {
            map.put("setteltotalKM", tripTO.getSetteltotalKM());
        }
        if (!"".equals(tripTO.getSetteltotalHrs()) && (tripTO.getSetteltotalHrs()) != null) {
            map.put("setteltotalHrs", tripTO.getSetteltotalHrs());
        }
        System.out.println("the overrideEndTripSheetDuringClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.overrideEndTripSheetDuringClosure", map);
            System.out.println("overrideEndTripSheetDuringClosure size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("overrideEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "overrideEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateStatus(TripTO tripTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripId", tripTO.getTripSheetId());

        map.put("vendorId", tripTO.getVendorId());
        map.put("userId", userId);
        map.put("statusId", tripTO.getStatusId());
        map.put("eWayBillNo", tripTO.geteWayBillNo());
        map.put("invoiceNo", tripTO.getInvoiceNo());
        map.put("invoiceValue", tripTO.getInvoiceValue());
        map.put("invoiceDate", tripTO.getInvoiceDate());
        map.put("invoiceWeight", tripTO.getInvoiceWeight());
        map.put("expiryDate", tripTO.getExpiryDate());
        map.put("expiryHour", tripTO.getExpiryHour());
        map.put("expiryMin", tripTO.getExpiryMin());
        map.put("expiryTime", tripTO.getExpiryHour() + ":" + tripTO.getExpiryMin() + ":00");

        if ("".equals(tripTO.getVehicleId()) || tripTO.getVehicleId() == null) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", tripTO.getVehicleId());
        }

        if ("1".equals(tripTO.getVendorId())) {
            map.put("hireVehicleNo", "0");
        } else {
            map.put("hireVehicleNo", tripTO.getHireVehicleNo());
        }

        System.out.println("the updateEndTripSheet 09-01-2019" + map);
        int status = 0;
        int statusInsert = 0;
        ArrayList tripDetails = new ArrayList();
        int tripVehicleCount = 0;
        int tripClosureCount = 0;
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date curDate = new Date();
        System.out.println("closureDate----" + dateFormat.format(curDate));

        String updateDate = dateFormat.format(curDate);
        System.out.println("closureDate--QQ--" + updateDate);
        int update = 0;
        try {
            map.put("KM", 0);
            map.put("HM", 0);
            map.put("remarks", "system update");
            map.put("updateDate", updateDate);
            System.out.println("map-AAA-" + map);
            //values(#tripSheetId#, #statusId#, #KM#, #HM#, #remarks#, #userId#, now())
            statusInsert = (Integer) session.update("trip.insertTripStatus", map);
            System.out.println("11-------" + statusInsert);
            System.out.println("map value: 09-01-2019" + map);

//            status = (Integer) session.update("trip.updateTripStatus", map);
//            System.out.println("statusInsert==333333=="+statusInsert);
//            tripVehicleCount = (Integer) session.queryForObject("trip.getTripVehicleCount", map);
//            tripClosureCount = (Integer) session.queryForObject("trip.getTripClosureCount", map);
//            if (tripClosureCount > 0) {
//                if (tripVehicleCount == tripClosureCount) {
//                    map.put("KM", 0);
//                    map.put("HM", 0);
//                    map.put("remarks", "system update");
//
//
//                    System.out.println("updateStatus size=" + tripDetails.size());
//                }
//            } else {
//                map.put("KM", 0);
//                map.put("HM", 0);
//                map.put("remarks", "system update");
//                status = (Integer) session.update("trip.insertTripStatus", map);
//                status = (Integer) session.update("trip.updateTripStatus", map);
//
//                System.out.println("updateStatus size=" + tripDetails.size());
//            }
            int updateInvoiceDetail = (Integer) session.update("trip.updateTripForInvoiceDetail", map);
            System.out.println("updateInvoiceDetail---09-01-2019" + updateInvoiceDetail);

            System.out.println("statusInsert: 09-01-2019" + statusInsert + "status:" + status);
            int insertTripExpenseCount = 0;
            if (status != 0) {
                if (!"0".equals(tripTO.getVehicleId())) {
                    insertTripExpenseCount = (Integer) session.update("trip.insertTripExpenseReportCount", map);
                }
            }
            System.out.println("insertTripExpenseCount: 09-01-2019" + insertTripExpenseCount);
            String ConsignmentIdList = (String) session.queryForObject("trip.getTripConsignment", map);
            System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
            String[] ConsignmentId = ConsignmentIdList.split(",");
            for (int i = 0; i < ConsignmentId.length; i++) {
                map.put("consignmentId", ConsignmentId[i]);
                map.put("consignmentOrderId", ConsignmentId[i]);
                map.put("consignmentStatus", "13");
                System.out.println("the updated map:" + map);
                int plannedstatus = (Integer) session.queryForObject("trip.getPlannedContainer", map);
                System.out.println("plannedstatus==09-01-2019 " + plannedstatus);
                if (plannedstatus > 0) {
                    map.put("consignmentStatus", "5");
                    status = (Integer) session.update("trip.updateConsignmentStatus", map);
                    System.out.println("the status1: 09-01-2019 " + status);
                }
            }

            System.out.println("vendorId:: 09-01-2019 " + tripTO.getVendorId());
            System.out.println("statusId:: 09-01-2019 " + tripTO.getStatusId());
            String checkeclosureCount = (String) session.queryForObject("trip.checkTripClosureCount", map);
            System.out.println("checkeclosureCount:: 09-01-2019  " + checkeclosureCount);

            if (checkeclosureCount != null && !"".equals(checkeclosureCount)) {
                String[] tem = checkeclosureCount.split("~");
                System.out.println("tem0::" + tem[0]);
                System.out.println("tem1::" + tem[1]);
//
//                if (Integer.parseInt(tem[0]) == Integer.parseInt(tem[1])) {
//                    update = (Integer) session.update("trip.updateClosureStatusInTrip", map);
//                    System.out.println("update-updateClosureStatusInTrip--" + update);
//                } else {
//                    if (Integer.parseInt(tem[0]) == 2 && !"0".equals(tripTO.getVendorId())) {
//                        update = (Integer) session.update("trip.updateClosureStatusInTrip", map);
//                        System.out.println("update-updateClosureStatusInTrip--" + update);
//                    }
//                }
                if (Integer.parseInt(tem[0]) == Integer.parseInt(tem[1])) {
                    update = (Integer) session.update("trip.updateClosureStatusInTrip", map);
                    System.out.println("update-updateClosureStatusInTrip--" + update);
                }

            }
//             updateOrderStatusToEFS(tripTO.getTripId(), tripTO.getStatusId());
            //           updateTripCloserToEFS(tripTO.getTripId());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return update;
    }

    public ArrayList getFuelDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());

        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getFuelDetails", map);
            System.out.println("getFuelDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails List", sqlException);
        }

        return tripDetails;
    }

    public String getFuelPriceDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());

        String tripDetails = "";
        try {
            System.out.println("fuel price map : " + map);
            tripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelPriceDetails", map);
            System.out.println("getFuelDetails :" + tripDetails);
            if (tripDetails == null || "".equals(tripDetails)) {
                tripDetails = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelDetails List", sqlException);
        }

        return tripDetails;
    }

    public ArrayList getOtherExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());

        if (tripTO.getVendorId() == null || "".equals(tripTO.getVendorId())) {
            map.put("vendorId", "1");
        } else {
            map.put("vendorId", tripTO.getVendorId());
        }
        if (tripTO.getHireVehicleNo() == null || "".equals(tripTO.getHireVehicleNo())) {
            map.put("hireVehicleNo", "0");
        } else {
            map.put("hireVehicleNo", tripTO.getHireVehicleNo());
        }

        if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
            map.put("vehicleId", "0");
        }

        ArrayList tripDetails = new ArrayList();
        try {
            System.out.println(" mapiss" + map);
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOtherExpenseDetails", map);
            System.out.println("getOtherExpenseDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOtherExpenseDetails List", sqlException);
        }

        return tripDetails;
    }

    public int insertTripFuelExpense(String tripSheetId, String location, String fillDate, String fuelLitres, String fuelAmount, String fuelRemarks, String fuelPricePerLitre, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
        map.put("fillDate", fillDate);
        map.put("location", location);
        map.put("fuelPricePerLitre", fuelPricePerLitre);
        map.put("fuelLitres", fuelLitres);
        map.put("fuelAmount", fuelAmount);
        map.put("fuelRemarks", fuelRemarks);
        map.put("userId", userId);
        System.out.println("the insertTripFuel" + map);
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripFuel", map);
            System.out.println("insertTripFuel size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripFuel List", sqlException);
        }

        return status;
    }

    public ArrayList getExpenseDetails() {
        Map map = new HashMap();

        ArrayList expenseDetails = new ArrayList();
        try {
            expenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getExpenseDetails", map);
            System.out.println("getExpenseDetails size=" + expenseDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpenseDetails List", sqlException);
        }

        return expenseDetails;
    }

    public ArrayList getExpenseDriver() {
        Map map = new HashMap();

        ArrayList expenseDriver = new ArrayList();
        try {
            expenseDriver = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getExpenseDriver", map);
            System.out.println("getExpenseDriver size=" + expenseDriver.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpenseDetails List", sqlException);
        }

        return expenseDriver;
    }

    public ArrayList getDriverName(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList driverNameDetails = new ArrayList();
        System.out.println("map getDriverName = " + map);
        try {
            driverNameDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverName", map);
            System.out.println("getDriverName size=" + driverNameDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverName List", sqlException);
        }

        return driverNameDetails;
    }

    public int insertTripOtherExpense(String tripSheetId, String vehicleId, String vendorId, String hireVehicleNo, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String activeValue, int userId, String borneBys, String expenseId, String approval, String customerHalt) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
        if (vehicleId == null || "".equals(vehicleId)) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", vehicleId);
        }
        if (vendorId == null || "".equals(vendorId)) {
            map.put("vendorId", "1");
        } else {
            map.put("vendorId", vendorId);
        }
        if (hireVehicleNo == null || "".equals(hireVehicleNo)) {
            map.put("hireVehicleNo", "0");
        } else {
            map.put("hireVehicleNo", hireVehicleNo);
        }
//        map.put("hireVehicleNo", hireVehicleNo);
//        map.put("vehicleId", vehicleId);
        map.put("expenseType", expenseType);
        map.put("billMode", billMode);
        map.put("expenseName", expenseName);
        map.put("employeeName", employeeName);
        map.put("activeValue", activeValue);

        String tempApproval = "";
        System.out.println("customerHalt--------" + tempApproval);
        if (Integer.parseInt(expenseName) == 1021) {
            if (Double.parseDouble(expenses) == 0) {
                tempApproval = "1";
            } else {
                tempApproval = customerHalt;
            }
            map.put("approval", tempApproval);
        } else if (Integer.parseInt(expenseName) == 1013) {
            map.put("approval", approval);
        } else {
            map.put("approval", 0);
        }
        String[] temp = null;
        System.out.println("expenseDate---" + expenseDate);
        temp = expenseDate.split("-");
        System.out.println(" UUUUUUUU" + temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":" + "00");
        map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":" + "00");
        if (!"".equals(taxPercentage)) {
            map.put("taxPercentage", taxPercentage);
        } else {
            map.put("taxPercentage", "0");
        }
        map.put("expenseRemarks", expenseRemarks);
        if (!"".equals(expenses)) {
            map.put("expenses", expenses);
        } else {
            map.put("expenses", "0");
        }
        if (!"".equals(borneBys)) {
            map.put("borneBys", borneBys);
        } else {
            map.put("borneBys", "0");
        }
        if (!"".equals(currency)) {
            map.put("currency", currency);
        } else {
            map.put("currency", "0");
        }
        if (!"".equals(netExpense)) {
            map.put("netExpense", netExpense);
        } else {
            map.put("netExpense", "0");
        }
        if (!"".equals(marginValue)) {
            map.put("marginValue", marginValue);
        } else {
            map.put("marginValue", "0");
        }
        if (!"".equals(expenseId)) {
            map.put("expenseId", expenseId);
        } else {
            map.put("expenseId", "0");
        }
        map.put("userId", userId);
        System.out.println("the insertTripOtherExpense" + map);
        try {
            int check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseExistWithVehicle", map);
            //int check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseExist", map);
            System.out.println("my check point for trip expense duplication==" + check);
            int secondCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseIdExistWithVehicle", map);
            // int secondCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseIdExist", map);
//            int thirdCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseIdExistWithVehicle", map);
            System.out.println("my check point for trip expense 2222==" + secondCheck);

            if ("1023".equals(expenseName)) {
                System.out.println("check:" + check);
                System.out.println("second check" + secondCheck);
            }

            if (check == 0 && secondCheck == 0) {
                status = (Integer) getSqlMapClientTemplate().update("trip.insertTripOtherExpense", map);
                System.out.println("insertTripOtherExpense size=");

                //Acct Entry
                String code2 = "";
                temp = null;
                int insertStatus = 0;
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%PAYMENT%");

                code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
                temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                //select ledger details for the expense;
                map.put("expenseId", expenseName);
                String ledgerDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseLedgerInfo", map);

                String[] tempVar = ledgerDetails.split("~");
                map.put("ledgerId", tempVar[0]);
                map.put("particularsId", tempVar[1]);

                map.put("amount", expenses);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Other Expenses");
                map.put("Reference", "Trip");

                System.out.println("tripId = " + tripSheetId);
                map.put("SearchCode", tripSheetId);
                System.out.println("map1 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                System.out.println("status1 = " + insertStatus);
                //--------------------------------- acc 2nd row start --------------------------
                if (insertStatus > 0) {
                    map.put("DetailCode", "2");
                    map.put("ledgerId", "52");
                    map.put("particularsId", "LEDGER-40");
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status2 = " + insertStatus);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripFuel List", sqlException);
        }

        return status;
    }

    public ArrayList getExpiryDateDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList expiryDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            expiryDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getExpiryDateDetails", map);
            System.out.println("getExpiryDateDetails.size() = " + expiryDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpiryDateDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpiryDateDetails List", sqlException);
        }
        return expiryDetails;
    }

    public int updateTripOtherExpense(String tripSheetId, String vehicleId, String vendorId, String hireVehicleNo, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String expenseId, String activeValue, int userId, String borneBys, String approval, String customerHalt) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int expenseType1 = 0;
        int billMode1 = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
//        map.put("vehicleId", vehicleId);
        if (vehicleId == null || "".equals(vehicleId)) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", vehicleId);
        }
        if (vendorId == null || "".equals(vendorId)) {
            map.put("vendorId", "1");
        } else {
            map.put("vendorId", vendorId);
        }
        if (hireVehicleNo == null || "".equals(hireVehicleNo)) {
            map.put("hireVehicleNo", "0");
        } else {
            map.put("hireVehicleNo", hireVehicleNo);
        }

        if (billMode != null && billMode != "") {
            billMode1 = Integer.parseInt(billMode);
            map.put("billMode", billMode1);
        }
        if (expenseType != null && expenseType != "") {
            expenseType1 = Integer.parseInt(expenseType);
            map.put("expenseType", expenseType1);
        }
        if (!"".equals(taxPercentage)) {
            map.put("taxPercentage", taxPercentage);
        } else {
            map.put("taxPercentage", "0");
        }
        if (!"".equals(borneBys)) {
            map.put("borneBys", borneBys);
        } else {
            map.put("borneBys", "0");
        }
        map.put("expenseRemarks", expenseRemarks);
        if (!"".equals(expenses)) {
            map.put("expenses", expenses);
        } else {
            map.put("expenses", "0");
        }
        if (!"".equals(currency)) {
            map.put("currency", currency);
        } else {
            map.put("currency", "0");
        }
        if (!"".equals(netExpense)) {
            map.put("netExpense", netExpense);
        } else {
            map.put("netExpense", "0");
        }
        if (!"".equals(marginValue)) {
            map.put("marginValue", marginValue);
        } else {
            map.put("marginValue", "0");
        }
        String tempApproval = "";
        System.out.println("customerHalt--------" + tempApproval);
        System.out.println("expenses--------" + expenses);
        if (Integer.parseInt(expenseName) == 1021) {
            if (Double.parseDouble(expenses) == 0.00) {
                tempApproval = "1";
            } else {
                tempApproval = customerHalt;
            }
            map.put("approval", tempApproval);
        } else if (Integer.parseInt(expenseName) == 1013) {
            map.put("approval", approval);
        } else {
            map.put("approval", 0);
        }
        map.put("expenseType", expenseType);
        map.put("billMode", billMode);
        map.put("expenseName", expenseName);
        map.put("expenseId", expenseId);
        map.put("employeeName", employeeName);
        String[] temp = null;
        temp = expenseDate.split("-");
        map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":00");
        map.put("expenseRemarks", expenseRemarks);
        map.put("activeValue", activeValue);
        map.put("userId", userId);
        System.out.println("the updateTripOtherExpense" + map);
        try {
//            status = (Integer) getSqlMapClientTemplate().update("trip.deleteOtherExpense", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpense", map);
            System.out.println("updateTripOtherExpense size=");
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripOtherExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripOtherExpense List", sqlException);
        }

        return status;
    }

    public int deleteTripOtherExpense(String tripSheetId, String vehicleId, String expenseName, String employeeName, String expenseType, String expenseDate, String expenseHour, String expenseMinute, String taxPercentage, String expenseRemarks, String expenses, String currency, String netExpense, String billMode, String marginValue, String expenseId, String activeValue, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int expenseType1 = 0;
        int billMode1 = 0;
        if (tripSheetId != null && tripSheetId != "") {
            tripId = Integer.parseInt(tripSheetId);
            map.put("tripSheetId", tripId);
        }
        map.put("vehicleId", vehicleId);
        if (billMode != null && billMode != "") {
            billMode1 = Integer.parseInt(billMode);
            map.put("billMode", billMode1);
        }
        if (expenseType != null && expenseType != "") {
            expenseType1 = Integer.parseInt(expenseType);
            map.put("expenseType", expenseType1);
        }
        if (!"".equals(taxPercentage)) {
            map.put("taxPercentage", taxPercentage);
        } else {
            map.put("taxPercentage", "0");
        }
        map.put("expenseRemarks", expenseRemarks);
        if (!"".equals(expenses)) {
            map.put("expenses", expenses);
        } else {
            map.put("expenses", "0");
        }
        if (!"".equals(currency)) {
            map.put("currency", currency);
        } else {
            map.put("currency", "0");
        }
        if (!"".equals(netExpense)) {
            map.put("netExpense", netExpense);
        } else {
            map.put("netExpense", "0");
        }
        if (!"".equals(marginValue)) {
            map.put("marginValue", marginValue);
        } else {
            map.put("marginValue", "0");
        }
        map.put("expenseType", expenseType);
        map.put("billMode", billMode);
        map.put("expenseName", expenseName);
        map.put("expenseId", expenseId);
        map.put("employeeName", employeeName);
        String[] temp = null;
        temp = expenseDate.split("-");
        map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " " + expenseHour + ":" + expenseMinute + ":00");
        map.put("expenseRemarks", expenseRemarks);
        map.put("activeValue", activeValue);
        map.put("userId", userId);
        System.out.println("the deleteTripOtherExpense" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.deleteOtherExpense", map);

            System.out.println("deleteTripOtherExpense size=");
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripOtherExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripOtherExpense List", sqlException);
        }

        return status;
    }

    public int saveTripExpensePodDetailsOLD(String actualFilePath, String tripExpenseId1, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripExpenseId = 0;
        try {
            if (tripExpenseId1 != null && tripExpenseId1 != "") {
                tripExpenseId = Integer.parseInt(tripExpenseId1);
                map.put("tripExpenseId", tripExpenseId);
            }
            map.put("userId", userId);
            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] podFile = new byte[(int) file.length()];
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            System.out.println("the saveTripExpensePodDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpensePodDetailsOLD", map);
            System.out.println("saveTripExpensePodDetails size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpensePodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpensePodDetails List", sqlException);
        }

        return status;
    }

    public int saveTripExpensePodDetails(String actualFilePath, String fileSave, String tripExpenseId1, int userId) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        FileInputStream fis = null;
        int tripExpenseId = 0;
        try {
            if (tripExpenseId1 != null && tripExpenseId1 != "") {
                tripExpenseId = Integer.parseInt(tripExpenseId1);
                map.put("tripExpenseId", tripExpenseId);
            }
            map.put("userId", userId);
            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] podFile = new byte[(int) file.length()];
            fis.read(podFile);
            fis.close();
            map.put("podFile", podFile);
            map.put("fileSave", fileSave);
            System.out.println("the saveTripExpensePodDetails" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpensePodDetails", map);
            status1 = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpenseDoumentUploadedDetails", map);
            System.out.println("saveTripExpensePodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpensePodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpensePodDetails List", sqlException);
        }

        return status;
    }
//Nithya End 7 Dec
//Arul Start Here 07-12-2013

    /**
     * This method used to get Trip Advance Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripAdvanceDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetails", map);
            if (tripAdvanceDetails.size() == 0) {
                tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsOld", map);
            }
            System.out.println("tripAdvanceDetails.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvanceDetails List", sqlException);
        }
        return tripAdvanceDetails;
    }

    /**
     * This method used to get Trip Fuel Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripDieselDetails(TripTO tripTo) {
        Map map = new HashMap();
        ArrayList tripFuelDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTo.getTripSheetId());
            map.put("vehicleId", tripTo.getVehicleId());
            System.out.println(" Trip  diesel map is::" + map);
            tripFuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDieselDetails", map);
            System.out.println("tripFuelDetails.size() = " + tripFuelDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDieselDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDieselDetails List", sqlException);
        }
        return tripFuelDetails;
    }

    public ArrayList getTripGrDetails(TripTO tripTo) {
        Map map = new HashMap();
        ArrayList tripFuelDetails = new ArrayList();
        try {
            map.put("tripId", tripTo.getTripId());
            map.put("grNumber", tripTo.getGrNumber());

            System.out.println(" Trip  gr map is::" + map);

            tripFuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripGrDetails", map);
            System.out.println("tripGrDetails.size() = " + tripFuelDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDieselDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDieselDetails List", sqlException);
        }
        return tripFuelDetails;
    }

    /**
     * This method used to get Trip Fuel Details.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getTripExpenseDetails(String tripSheetId, String vehicleId) {
        Map map = new HashMap();
        ArrayList tripExpenseDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", vehicleId);
            System.out.println("closed Trip map is::" + map);
            tripExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripExpenseDetails", map);
            System.out.println("tripExpenseDetails.size() = " + tripExpenseDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpenseDetails List", sqlException);
        }
        return tripExpenseDetails;
    }

    public ArrayList getTripStausDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println("closed Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripStatusDetails", map);
            System.out.println("getTripStausDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripStausDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripStausDetails List", sqlException);
        }
        return statusDetails;
    }

    public ArrayList getPODDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPODDetails", map);
            System.out.println("getPODDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPODDetails List", sqlException);
        }
        return statusDetails;
    }

    public ArrayList getTripPackDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripPackDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            tripPackDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPackDetails", map);
            System.out.println("getTripPackDetails.size() = " + tripPackDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPackDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPackDetails List", sqlException);
        }
        return tripPackDetails;
    }

    public ArrayList getTripUnPackDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripUnPackDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            tripUnPackDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripUnPackDetails", map);
            System.out.println("tripUnPackDetails.size() = " + tripUnPackDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripUnPackDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripUnPackDetails List", sqlException);
        }
        return tripUnPackDetails;
    }

    public ArrayList viewApproveDetails(String tripid, int tripclosureid) {
        Map map = new HashMap();
        ArrayList viewApproveDetails = new ArrayList();
        map.put("tripid", tripid);
        map.put("tripclosureid", tripclosureid);

        try {
            viewApproveDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.viewApproveDetails", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewApproveDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewApproveDetails", sqlException);
        }
        return viewApproveDetails;
    }

    public ArrayList viewRevenueApproveDetails(String customerId, int revenueApproveId, String tripId) {
        Map map = new HashMap();
        ArrayList viewApproveDetails = new ArrayList();
        map.put("customerId", customerId);
        map.put("revenueApproveId", revenueApproveId);
        map.put("tripId", tripId);
        System.out.println("map for Email content:" + map);
        try {
            viewApproveDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.viewRevenueApproveDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewApproveDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewApproveDetails", sqlException);
        }
        return viewApproveDetails;
    }

    public ArrayList getEmailDetails(String activitycode) {
        ArrayList EmailDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actcode", activitycode);
            EmailDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailDetails", map);
            System.out.println("EmailDetails size=" + EmailDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("EmailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "EmailDetails List", sqlException);
        }

        return EmailDetails;
    }

    public ArrayList getTripCityList(TripTO tripTO) {
        ArrayList cityList = new ArrayList();
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripCityList", map);
            System.out.println("getTripCityList size=" + cityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCityList List", sqlException);
        }

        return cityList;
    }

    public ArrayList getCityList() {
        ArrayList cityList = new ArrayList();
        Map map = new HashMap();
        try {

            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCityList", map);
            System.out.println("getCityList size=" + cityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCityList List", sqlException);
        }

        return cityList;
    }

    public ArrayList getmailcontactDetails(String tripid) {
        ArrayList getmailcontactDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("tripid", tripid);
            getmailcontactDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getmailContactDetails", map);
            System.out.println("getmailcontactDetails size=" + getmailcontactDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getmailcontactDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getmailcontactDetails List", sqlException);
        }

        return getmailcontactDetails;
    }

    public ArrayList getTripStakeHoldersList() throws FPRuntimeException, FPBusinessException {
        ArrayList holdersList = new ArrayList();
        Map map = new HashMap();
        try {

            holdersList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripStakeHolders", map);
            System.out.println("Size in holdersList " + holdersList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return holdersList;
    }

    public ArrayList getstatusList() throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        try {

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getstatusList", map);
            System.out.println("Size in statusList " + statusList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return statusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getEmailFunction(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList setStatusList = new ArrayList();
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("stakeHolderId", tripTO.getStakeHolderId());
            map.put("holderName", tripTO.getHolderName());
            map.put("statusId", tripTO.getStatusId());
            map.put("statusName", tripTO.getStatusName());
            System.out.print("map----" + map);
            setStatusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSetStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return setStatusList;
    }

    /**
     * This method used to Get Available Functions .
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFunc(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunc = null;
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("statusId", stackHolderId);

            assignedFunc = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSetStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return assignedFunc;
    }

    /**
     * This method used to Get Assigned Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public ArrayList getAssignedFucntions(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList assignedFunctions = new ArrayList();
        Map map = new HashMap();
        //String sqlQuery = "select Function_Id from Role_function where Role_Id="+roleId+"";

        try {
            map.put("statusId", tripTO.getStatusId());

            assignedFunctions = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAssignedFunctions", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return assignedFunctions;
    }

    /**
     * This method used to Get Assigned Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int insertEmailSettings(String[] assignedFunc, String stackHolderId, int userId) throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        int updateStatus = 0;
        int insertStatus = 0;
        try {
            map.put("statusId", stackHolderId);
            System.out.println("stake holder List" + map + "length assigned func" + assignedFunc.length);
            if ((Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map) != null) {

                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map);
            }
            if (assignedFunc != null) {
                for (int i = 0; i < assignedFunc.length; i++) {
                    map.put("statusId", stackHolderId);
                    map.put("userId", userId);
                    map.put("assignedFuncId", assignedFunc[i]);
                    System.out.println("insert map" + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().insert("trip.insertEmailFunction", map);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return insertStatus;
    }

    /**
     * This method used to Modify Functions.
     *
     * @param request - Http request object.
     *
     * @throws FPBusinessException - Throws when a business Exception araises
     *
     * @throws FPRuntimeException - Throws when a Runtime Exception araises
     */
    public int deleteStakeholder(String[] assignedFunc, String stakeHolderId, int userId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int updateStatus = 0;
        try {

            map.put("statusId", stakeHolderId);
            if ((Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map) != null) {

                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteStakeholder", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deletefunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deletefunctions", sqlException);
        }
        return updateStatus;
    }

    public ArrayList getEditStatuslist(String stackHolderId) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        //        int statusId;
        try {
            map.put("statusId", stackHolderId);

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEditStatusList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return statusList;
    }

    public int insertVehicleDriverAdvance(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int insertVehicleDriverAdvance = 0;
        int index = 0;
//        map.put("userId", userId);
        try {

            map.put("vehicleId", tripTO.getVehicleId());
            map.put("advanceAmount", tripTO.getAdvanceAmount());
            map.put("advanceDate", tripTO.getAdvanceDate());
            map.put("advanceRemarks", tripTO.getAdvanceRemarks());
            map.put("primaryDriverId", tripTO.getPrimaryDriverId());
            map.put("secondaryDriverIdOne", tripTO.getSecondaryDriverIdOne());
            map.put("secondaryDriverIdTwo", tripTO.getSecondaryDriverIdTwo());
            map.put("expenseType", tripTO.getExpenseType());
            map.put("userId", userId);
            System.out.println("map value is:" + map);
            insertVehicleDriverAdvance = (Integer) getSqlMapClientTemplate().insert("trip.insertVehicleDriverAdvance", map);
            System.out.println("insertVehicleDriverAdvance=" + insertVehicleDriverAdvance);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertVehicleDriverAdvance", sqlException);
        }
        return insertVehicleDriverAdvance;
    }

    public String checkVehicleDriverAdvance(String vehicleId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("vehicleId", vehicleId);
        String checkVehicleDriverAdvance = "";
        System.out.println("map = " + map);
        try {
            checkVehicleDriverAdvance = (String) getSqlMapClientTemplate().queryForObject("trip.checkVehicleDriverAdvance", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkVehicleDriverAdvance", sqlException);
        }
        return checkVehicleDriverAdvance;

    }

    public ArrayList getVehicleDriverAdvanceList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList statusList = null;
        Map map = new HashMap();
        map.put("usageTypeId", tripTO.getUsageTypeId());
        System.out.println("map = " + map);
        try {

            statusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleDriverAdvanceList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverAdvanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleDriverAdvanceList", sqlException);
        }
        return statusList;
    }

    public ArrayList getCustomerList(String companyId) throws FPRuntimeException, FPBusinessException {
        ArrayList customerList = null;
        Map map = new HashMap();
        map.put("companyId", companyId);
        System.out.println("companyId--" + companyId);
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerList", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerList", sqlException);
        }
        return customerList;
    }

    public ArrayList getZoneList() throws FPRuntimeException, FPBusinessException {
        ArrayList zoneList = null;
        Map map = new HashMap();
        try {
            zoneList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getZoneList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerList", sqlException);
        }
        return zoneList;
    }

    public ArrayList getLocation(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("city", tripTO.getCityId() + "%");
        map.put("zoneId", tripTO.getZoneId());
        System.out.println("map = " + map);

        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getLocation", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCity Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCity", sqlException);
        }
        return cityList;
    }

    public int insertBPCLTransactionHistory(ArrayList bpclTransactionList, TripTO tripTO1, int userId) {
        Map map = new HashMap();
        int insertVehicleDriverAdvance = 0;
        int index = 0;
        TripTO tripTO = new TripTO();
        try {
            map.put("userId", userId);
            map.put("transactionHistoryId", tripTO1.getTransactionHistoryId());
            Iterator itr = bpclTransactionList.iterator();
            while (itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                map.put("tripId", tripTO.getTripId());
                map.put("vehicleId", tripTO.getVehicleId());
                map.put("vehicleNo", tripTO.getVehicleNo());
                map.put("accountId", tripTO.getAccountId());
                map.put("dealerName", tripTO.getDealerName());
                map.put("dealerCity", tripTO.getDealerCity());
                String[] temp = null;
                String date = "";
                String time = "";
                temp = tripTO.getTransactionDate().split(" ");
                String[] temp1 = temp[0].split("-");
                date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
                time = temp[1];
                String transactionDateTime = date + " " + time;
                map.put("transactionDate", transactionDateTime);

                String[] accTemp = null;
                String accDate = "";
                String accTime = "";
                accTemp = tripTO.getAccountingDate().split(" ");
                String[] accDateTemp = accTemp[0].split("-");
                accDate = accDateTemp[2] + "-" + accDateTemp[1] + "-" + accDateTemp[0];
                accTime = accTemp[1];
                String accDateTime = accDate + " " + accTime;
                map.put("accountingDate", accDateTime);

                map.put("transactionType", tripTO.getTransactionType());
                map.put("currency", tripTO.getCurrency());
                map.put("amount", tripTO.getAmount());
                map.put("volumeDocNo", tripTO.getVolumeDocNo());
                map.put("amountBalance", tripTO.getAmoutBalance());
                map.put("petromilesEarned", tripTO.getPetromilesEarned());
                map.put("odometerReading", tripTO.getOdometerReading());
                System.out.println("map value is:" + map);
                insertVehicleDriverAdvance += (Integer) getSqlMapClientTemplate().update("trip.insertBPCLTransactionHistory", map);
            }
            System.out.println("insertVehicleDriverAdvance=" + insertVehicleDriverAdvance);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBPCLTransactionHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertBPCLTransactionHistory", sqlException);
        }
        return insertVehicleDriverAdvance;
    }

    public String checkTransactionHistoryId(TripTO tripTO) {
        Map map = new HashMap();
        map.put("transactionHistoryId", tripTO.getTransactionHistoryId());
        map.put("vehicleNo", tripTO.getVehicleNo());
        map.put("accountId", tripTO.getAccountId());
        map.put("dealerName", tripTO.getDealerName());
        map.put("dealerCity", tripTO.getDealerCity());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);

        String[] accTemp = null;
        String accDate = "";
        String accTime = "";
        accTemp = tripTO.getAccountingDate().split(" ");
        String[] accDateTemp = accTemp[0].split("-");
        accDate = accDateTemp[2] + "-" + accDateTemp[1] + "-" + accDateTemp[0];
        accTime = accTemp[1];
        String accDateTime = accDate + " " + accTime;
        map.put("accountingDate", accDateTime);
        String status = "";
        System.out.println("map = " + map);
        try {
            status = (String) getSqlMapClientTemplate().queryForObject("trip.checkTransactionHistoryId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTransactionHistoryId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkTransactionHistoryId", sqlException);
        }
        return status;

    }

    public String getTripCodeForBpcl(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        String tripId = "";
        System.out.println("map = " + map);
        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripIdForBpcl", map);
                if (tripId != null) {
                    tripId = tripId + "~" + vehicleId;
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripIdForBpcl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripIdForBpcl", sqlException);
        }
        return tripId;

    }

    public int getTripCodeCountForBpcl(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        int tripCount = 0;
        System.out.println("map = " + map);
        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripCodeCountForBpcl", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeCountForBpcl Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripCodeCountForBpcl", sqlException);
        }
        return tripCount;

    }

    public ArrayList getTripWrongDataList(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList tripWrongDataList = new ArrayList();
        map.put("vehicleNo", tripTO.getVehicleNo());
        String[] temp = null;
        String date = "";
        String time = "";
        temp = tripTO.getTransactionDate().split(" ");
        String[] temp1 = temp[0].split("-");
        date = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
        time = temp[1];
        String transactionDateTime = date + " " + time;
        map.put("transactionDate", transactionDateTime);
        String vehicleId = "";
        int tripCount = 0;
        System.out.println("map = " + map);

        try {
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForBpcl", map);
            if (vehicleId != null && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                System.out.println("map vehicleId = " + map);
                tripWrongDataList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripWrongDataList", map);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripWrongDataList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripWrongDataList", sqlException);
        }
        return tripWrongDataList;
    }

    public ArrayList getBPCLTransactionHistory(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList bpclTransactionHistory = new ArrayList();
        ArrayList bpclTransactionHistoryStartDate = new ArrayList();
        ArrayList bpclTransactionHistoryEndDate = new ArrayList();
        /*
         * set the parameters in the map for sending to ORM
         */
        String vehicleId = "";
        map.put("tripId", tripTO.getTripId());
        if (tripTO.getVehicleId() != null && !"".equals(tripTO.getVehicleId())) {
            map.put("vehicleId", tripTO.getVehicleId());
        } else {
            map.put("tripSheetId", tripTO.getTripId());
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
            map.put("vehicleId", vehicleId);
        }
        System.out.println("map = " + map);
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        String startDate = "";
        String currentTripStartDate = "";
        String endDate = "";
        try {
            bpclTransactionHistoryStartDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryDate", map);
            Iterator itr = bpclTransactionHistoryStartDate.iterator();
            if (itr.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr.next();
                startDate = tripTO1.getStartDate();
                currentTripStartDate = tripTO1.getCurrentTripStartDate();
                endDate = tripTO1.getEndDate();
            }
//            bpclTransactionHistoryStartDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryStartDate", map);
//            Iterator itr = bpclTransactionHistoryStartDate.iterator();
//
//            if(itr.hasNext()){
//                tripTO1 = new TripTO();
//                tripTO1 = (TripTO) itr.next();
//                startDate = tripTO1.getStartDate();
//            }
//            bpclTransactionHistoryEndDate = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistoryEndDate", map);
//            Iterator itr1 = bpclTransactionHistoryEndDate.iterator();
//            if(itr1.hasNext()){
//                tripTO2 = new TripTO();
//                tripTO2 = (TripTO) itr1.next();
//                endDate = tripTO2.getEndDate();
//            }
            map.put("startDate", startDate);
            map.put("currentTripStartDate", currentTripStartDate);
            map.put("endDate", endDate);
            System.out.println("map = " + map);
            bpclTransactionHistory = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBPCLTransactionHistory", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBPCLTransactionHistory Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBPCLTransactionHistory", sqlException);
        }
        return bpclTransactionHistory;
    }

    public ArrayList getConsignmentListForUpdate(String consignmentOrderId) {
        Map map = new HashMap();
        map.put("consignmentOrderId", consignmentOrderId);

        ArrayList consignmentList = new ArrayList();
        try {
            consignmentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentListForUpdate", map);
            System.out.println("consignmentList size=" + consignmentList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return consignmentList;
    }

    public int saveTripRoutePlanForEmptyTrip(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String consignmentId = tripTO.getConsignmentId();
            String originId = tripTO.getOriginId();
            String destinationId = tripTO.getDestinationId();
            String origin = tripTO.getOrigin();
            String destination = tripTO.getDestination();
            String plannedDate = tripTO.getPreStartLocationPlanDate();
            String plannedHour = tripTO.getPreStartLocationPlanTimeHrs();
            String plannedMinutes = tripTO.getPreStartLocationPlanTimeMins();
            String routeId = "0";
            map.put("orderId", consignmentId);
            map.put("pointId", originId);
            map.put("pointType", "Start Point");
            map.put("pointOrder", "1");
            map.put("pointAddresss", origin);
            map.put("pointPlanDate", plannedDate);
            map.put("pointPlanTime", plannedHour + ":" + plannedMinutes);
            map.put("routeId", routeId);
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
            System.out.println("saveTripRoutePlan=" + status);
            routeId = tripTO.getRouteId();
            map.put("orderId", consignmentId);
            map.put("pointId", destinationId);
            map.put("pointType", "End Point");
            map.put("pointOrder", "2");
            map.put("pointAddresss", destination);
            map.put("pointPlanDate", "00-00-0000");
            map.put("pointPlanTime", "00" + ":" + "00");
            map.put("routeId", routeId);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getTripAdvanceDetailsStatus(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println("closed Trip map is::" + map);
            tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsStatus", map);
            if (tripAdvanceDetails.size() > 0) {
                tripAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAdvanceDetailsStatusOld", map);
            }
            System.out.println("getTripAdvanceDetailsStatus.size() = " + tripAdvanceDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripAdvanceDetailsStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripAdvanceDetailsStatus List", sqlException);
        }
        return tripAdvanceDetails;
    }

    public int saveWFUTripSheet(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("wfuRemarks", tripTO.getWfuRemarks());
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");

        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveWFUTripSheet", map);
//            updateOrderStatusToEFS(tripTO.getTripSheetId(), "18");
            System.out.println("updateEndTripSheet size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheet List", sqlException);
        }

        return status;
    }

    public ArrayList getWfuDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList wfuDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            wfuDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getWfuDetails", map);
            System.out.println("getWfuDetails.size() = " + wfuDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWfuDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWfuDetails List", sqlException);
        }
        return wfuDetails;
    }

    public ArrayList getEmptyTripMergingList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList emptyTripList = new ArrayList();
        map.put("customerId", tripTO.getCustomerId());
        map.put("tripCode", tripTO.getTripCode());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        if (tripTO.getVehicleId().equals("")) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", tripTO.getVehicleId());
        }
        map.put("tripId", tripTO.getTripId());
        System.out.println("tripId=" + tripTO.getTripId() + "");
        try {
            if (tripTO.getTripId() != null && !"".equals(tripTO.getTripId())) {
                String[] tripIdNos = tripTO.getTripId().split(",");
                List tripIds = new ArrayList(tripIdNos.length);
                for (int i = 0; i < tripIdNos.length; i++) {
                    System.out.println("value:" + tripIdNos[i]);
                    tripIds.add(tripIdNos[i]);
                }
                map.put("tripIds", tripIds);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingList", map);
            } else if (tripTO.getTripId() == null) {
                System.out.println("map = " + map);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingLists", map);
            } else if ("".equals(tripTO.getTripId())) {
                System.out.println("map = " + map);
                emptyTripList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyTripMergingLists", map);
            }
            System.out.println("getEmptyTripMergingList.size() = " + emptyTripList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripMergingList List", sqlException);
        }
        return emptyTripList;
    }

    public int insertEmptyTripMerging(String[] tripId, String[] tripSequence, String tripMergingRemarks, int userId) {
        Map map = new HashMap();
        int mergeId = 0;
        int mergeCount = 0;
        try {
            map.put("userId", userId);
            map.put("tripMergingRemarks", tripMergingRemarks);
            mergeId = (Integer) getSqlMapClientTemplate().insert("trip.saveTripMergeMaster", map);
            if (mergeId > 0) {
                map.put("mergeId", mergeId);
                for (int i = 0; i < tripId.length; i++) {
                    map.put("tripId", tripId[i]);
                    if ("".equals(tripSequence[i])) {
                        map.put("tripSequence", 0);
                    } else {
                        map.put("tripSequence", tripSequence[i]);
                    }
                    mergeCount += (Integer) getSqlMapClientTemplate().update("trip.saveTripMergeDetails", map);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripMergeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripMergeDetails", sqlException);
        }
        return mergeId;
    }

    public String getTripCode(int tripId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("tripId", tripId);
        String tripCode = "";
        System.out.println("map = " + map);
        try {
            tripCode = (String) getSqlMapClientTemplate().queryForObject("trip.getTripCode", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripCode", sqlException);
        }
        return tripCode;

    }

    public int saveEmptyTripApproval(String tripId, String approvalStatus, String userId, String mailId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        map.put("mailId", mailId);
        System.out.println("the saveEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int saveTripRoutePlanForSecondaryTrip(TripTO tripTO, ArrayList orderPointDetails) {
        Map map = new HashMap();
        int status = 0;
        try {
            SecondaryOperationTO operationTO = null;
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String consignmentId = tripTO.getConsignmentId();
            String originId = tripTO.getOriginId();
            String destinationId = tripTO.getDestinationId();
            String origin = tripTO.getOrigin();
            String destination = tripTO.getDestination();
            String plannedDate = tripTO.getPreStartLocationPlanDate();
            String plannedHour = tripTO.getPreStartLocationPlanTimeHrs();
            String plannedMinutes = tripTO.getPreStartLocationPlanTimeMins();

            String routeId = "0";
            map.put("orderId", consignmentId);
            Iterator itr = orderPointDetails.iterator();
            while (itr.hasNext()) {
                operationTO = new SecondaryOperationTO();
                operationTO = (SecondaryOperationTO) itr.next();
                map.put("pointId", operationTO.getPointId());
                map.put("pointType", operationTO.getPointType());
                map.put("pointOrder", operationTO.getPointSequence());
                map.put("pointAddresss", operationTO.getPointAddresss());
                map.put("pointPlanDate", tripTO.getPreStartLocationPlanDate());
                map.put("pointPlanTime", "00:00");
                map.put("routeId", 0);
                System.out.println("map value is dsdssdsdsdsdsdsdsdsdsdsds:" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.saveTripRoutePlan", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int checkeEmptyTripApproval(String tripId, String approvalStatus, String userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkeEmptyTripApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkeEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public ArrayList getTempLogDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList viewTempLogDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            viewTempLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTempLogDetails", map);
            System.out.println("viewTempLogDetails.size() = " + viewTempLogDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyTripMergingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmptyTripMergingList List", sqlException);
        }
        return viewTempLogDetails;
    }

    public int saveTempLogDetails(String actualFilePath, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            if (tripTO.getTripId() != null && tripTO.getTripId() != "") {
                tripId = Integer.parseInt(tripTO.getTripId());
                map.put("tripSheetId", tripId);
            }

            map.put("fileName", fileSaved);
            map.put("userId", userId);

            File file = new File(actualFilePath);
            fis = new FileInputStream(file);
            byte[] tempFile = new byte[(int) file.length()];
            fis.read(tempFile);
            fis.close();
            map.put("tempFile", tempFile);
            System.out.println("the saveTripTempLogDetails123455" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTempLogDetails", map);
            System.out.println("saveTripTempLogDetails123455 size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripPodDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripPodDetails List", sqlException);
        }

        return status;
    }

    public int saveTemperatureLogApproval(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTemperatureLogApproval", map);
            System.out.println("saveTemperatureLogApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTemperatureLogApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public int checkTemperatureLogApproval(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", userId);
        System.out.println("the checkeTemperatureLogApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTemperatureLogApproval", map);
            System.out.println("checkeTemperatureLogApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkeTemperatureLogApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public String checkConsignmentCreditLimitStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        String status = "";
        map.put("consignmentId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the checkeTemperatureLogApproval" + map);
        try {
            status = (String) getSqlMapClientTemplate().queryForObject("trip.checkConsignmentCreditLimitStatus", map);
            System.out.println("ConsignmentCreditLimitStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("ConsignmentCreditLimitStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ConsignmentCreditLimitStatus List", sqlException);
        }

        return status;
    }

    public int updateConsignmentOrderCreditLimitStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignmentOrderId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the updateConsignmentOrderCreditLimitStatus" + map);
        try {
            if (tripTO.getApprovalStatus().equals("1")) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderCreditLimitApprovalStatus", map);
            } else if (tripTO.getApprovalStatus().equals("2")) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderCreditLimitCancelStatus", map);
            }
            System.out.println("updateConsignmentOrderCreditLimitStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignmentOrderCreditLimitStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateConsignmentOrderCreditLimitStatus", sqlException);
        }

        return status;
    }

    public int updateConsignmentOrderApprovalStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignmentOrderId", tripTO.getConsignmentId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("mailId", tripTO.getMailId());
        map.put("statusId", tripTO.getStatusId());
        map.put("approvalStatusId", tripTO.getApprovalStatusId());
        map.put("userId", userId);
        System.out.println("the updateConsignmentOrderApprovalStatus" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentOrderApprovalStatus", map);
            map.put("updateType", "System Update");
            map.put("remarks", "Credit Limit Update Status");
            status = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentStatus", map);
            System.out.println("updateConsignmentOrderApprovalStatus size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateConsignmentOrderApprovalStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateConsignmentOrderApprovalStatus List", sqlException);
        }

        return status;
    }

    public int insertLoadingUnloadingDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            SecondaryOperationTO operationTO = null;
            map.put("userId", userId);
            map.put("subStatus", tripTO.getSubStatusId());
            map.put("tripId", tripTO.getTripSheetId());
            map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
            map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
            map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
            map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
            map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
            map.put("tripRouteCourseId", tripTO.getRouteId());
            System.out.println("map value is" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveLoadingUnloadingDetails", map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.saveSubStatusDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public ArrayList getStatusList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statuList = new ArrayList();
        try {
            statuList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getStatusList", map);
            System.out.println("getStatusList.size() = " + statuList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getStatusList List", sqlException);
        }
        return statuList;
    }

    public ArrayList getTripVehicleNo(TripTO tripTO) {
        Map map = new HashMap();
        if (tripTO.getStatusId().toString().equals("222")) {
            System.out.println("i am in if " + tripTO.getStatusId().toString());
            map.put("statusId", "10");

        } else {
            System.out.println("i am in else " + tripTO.getStatusId().toString());
            map.put("statusId", tripTO.getStatusId());

        }
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        if ("1".equals(tripTO.getTripType())) {
            map.put("usageTypeId", 2);
        } else {
            map.put("usageTypeId", 1);
        }
        if ("2".equals(tripTO.getTripType())) {
            map.put("customerName", tripTO.getCustomerName());
        }
        ArrayList vehicleNos = new ArrayList();
        try {
            System.out.println("map:" + map);
            vehicleNos = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripVehicleNo", map);
            System.out.println("vehicleNos size=" + vehicleNos.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return vehicleNos;
    }

    public ArrayList getRunningTripDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println(tripTO.getConsignmentStatusId().toString());
        ArrayList tripDetails = new ArrayList();
        try {
            if (tripTO.getConsignmentStatusId().toString().equalsIgnoreCase("5")) {
                map.put("orderStatus", "24");
                System.out.println("my map is : " + map);
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getFirstLeginProgress", map);
            } else if (tripTO.getConsignmentStatusId().toString().equalsIgnoreCase("28")) {
                map.put("orderStatus", "30");
                System.out.println("my map is : " + map);
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getThirdLeginProgress", map);
            } else if (tripTO.getConsignmentStatusId().toString().equalsIgnoreCase("25")) {
                map.put("orderStatus", "27");
                System.out.println("my map is : " + map);
                tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecondLeginProgress", map);
            }
            System.out.println("trip details size=" + tripDetails.size());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }
        return tripDetails;
    }

    public ArrayList getConsignmentPaymentDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList consignmentPaymentList = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripId());
            map.put("paymentType", tripTO.getPaymentType());
            System.out.println(" Trip map is::" + map);
            consignmentPaymentList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentPaymentDetails", map);
            System.out.println("consignmentPaymentList.size() = " + consignmentPaymentList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentPaymentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentPaymentDetails List", sqlException);
        }
        return consignmentPaymentList;
    }

    public ArrayList getEmailList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getEmailList = new ArrayList();
        try {
            map.put("mailId", tripTO.getEmailId());
            System.out.println(" Trip map is::" + map);
            getEmailList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailList", map);
            System.out.println("getEmailList.size() = " + getEmailList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmailList List", sqlException);
        }
        return getEmailList;
    }

    public int saveVehicleDriverAdvanceApproval(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", tripTO.getVehicleDriverAdvanceId());
        map.put("approvalStatus", tripTO.getApprovalStatus());
        map.put("userId", tripTO.getUserId());
        map.put("mailId", tripTO.getMailId());
        System.out.println("the saveVehicleDriverAdvanceApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveVehicleDriverAdvanceApproval", map);
            System.out.println("saveVehicleDriverAdvanceApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleDriverAdvanceApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveVehicleDriverAdvanceApproval List", sqlException);
        }

        return status;
    }

    public int checkVehicleDriverAdvanceApproval(String vehicleDriverAdvanceId, String approvalStatus, String userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", vehicleDriverAdvanceId);
        map.put("approvalStatus", approvalStatus);
        map.put("userId", userId);
        System.out.println("the checkeEmptyTripApproval" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkVehicleDriverAdvanceApproval", map);
            System.out.println("saveEmptyTripApproval size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate ttro the calling class
             */
            FPLogUtils.fpDebugLog("checkeEmptyTripApproval Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkeEmptyTripApproval List", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleAdvanceRequest(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList wfuDetails = new ArrayList();
        try {
            map.put("fromdate", tripTO.getFromDate());
            map.put("todate", tripTO.getToDate());
            map.put("vehicleAdvanceId", tripTO.getVehicleDriverAdvanceId());
            System.out.println(" Trip map is::" + map);
            wfuDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleAdvanceRequest", map);
            System.out.println("getVehicleAdvanceRequest.size() = " + wfuDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleAdvanceRequest Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleAdvanceRequest List", sqlException);
        }
        return wfuDetails;
    }

    public ArrayList getViewVehicleDriverAdvanceList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList advanceDetails = new ArrayList();
        try {
            map.put("vehicleId", tripTO.getVehicleId());
            System.out.println(" Trip map is::" + map);
            String tripId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTripId", map);
            if (tripId != null) {
                map.put("tripId", tripId);
                System.out.println(" Trip map is::" + map);
                advanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleAdvanceDetailsList", map);
                System.out.println("getVehicleAdvanceDetailsList.size() = " + advanceDetails.size());
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getViewVehicleDriverAdvanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getViewVehicleDriverAdvanceList List", sqlException);
        }
        return advanceDetails;
    }

    public int saveVehicleDriverAdvancePay(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("vehicleDriverAdvanceId", tripTO.getVehicleDriverAdvanceId());
        map.put("paidAdvance", tripTO.getPaidAdvance());
        map.put("paidStatus", tripTO.getPaidStatus());
        map.put("userId", tripTO.getUserId());
        System.out.println("the saveVehicleDriverAdvancePay" + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.saveVehicleDriverAdvancePay", map);
            System.out.println("saveVehicleDriverAdvancePay size=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveVehicleDriverAdvancePay Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveVehicleDriverAdvancePay List", sqlException);
        }

        return status;
    }

    public String getLastBpclTransactionDate() {
        Map map = new HashMap();
        String lastBpclTxDate = "";
        System.out.println("map = " + map);
        try {
            lastBpclTxDate = (String) getSqlMapClientTemplate().queryForObject("trip.getLastBpclTransactionDate", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLastBpclTransactionDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getLastBpclTransactionDate", sqlException);
        }
        return lastBpclTxDate;

    }

    public ArrayList getApprovalValueDetails(double actualval) {
        ArrayList getapprovalValueDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actualval", actualval);
            System.out.println("map:" + map);
            getapprovalValueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getApprovalValueDetails", map);
            System.out.println("getapprovalValueDetails size=" + getapprovalValueDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getApprovalValueDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getApprovalValueDetails List", sqlException);
        }

        return getapprovalValueDetails;
    }

    public ArrayList getRepairMaintenence(int configId) {
        ArrayList getapprovalValueDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("configId", configId);
            System.out.println("map:" + map);
            getapprovalValueDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRepairMaintenence", map);
            System.out.println("getapprovalValueDetails size=" + getapprovalValueDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRepairMaintenence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRepairMaintenence List", sqlException);
        }

        return getapprovalValueDetails;
    }

    public String getFCLeadMailId(String vehicleId) {
        Map map = new HashMap();
        String fcLeadMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            fcLeadMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getFCLeadMailId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFCLeadMailId", sqlException);
        }
        return fcLeadMailId;

    }

    public String getSecondaryFCLeadMailId(String vehicleId) {
        Map map = new HashMap();
        String fcLeadMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            fcLeadMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryFCLeadMailId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSecondaryFCLeadMailId", sqlException);
        }
        return fcLeadMailId;

    }

    public String getSecondaryApprovalPerson(String vehicleId) {
        Map map = new HashMap();
        String approvalPersonMailId = "";
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        try {
            approvalPersonMailId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryApprovalPerson", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFCLeadMailId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getFCLeadMailId", sqlException);
        }
        return approvalPersonMailId;

    }

    public String getPreviousTripsOdometerReading(String tripSheetId) {
        Map map = new HashMap();
        String previousTripsOdometerReading = "";
        String tripVehicleId = "";
        String previousTripDetails = "";
        String previousTripEndKm = null;
        String previousTripEndHm = null;
        String previousTripType = "";
        String reeferRequired = "";
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        String tripEndDetails = "";
        try {
            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
            System.out.println("tripVehicleId = " + tripVehicleId);
            if (tripVehicleId != null) {
                map.put("tripVehicleId", tripVehicleId);
                previousTripDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndKm", map);
                System.out.println("previousTripDetails = " + previousTripDetails);
                if (previousTripDetails != null) {
                    temp = previousTripDetails.split("~");
                    previousTripEndKm = temp[0];
                    previousTripType = temp[2];
                    reeferRequired = temp[3];
                    if (previousTripType.equals("0") && reeferRequired.equals("Yes")) {
                        previousTripEndHm = temp[1];
                    } else {
                        previousTripEndHm = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndHm", map);
                        System.out.println("previousTripEndHm = " + previousTripEndHm);
                    }
                }
                if (previousTripEndKm != null && previousTripEndHm != null) {
                    tripEndDetails = previousTripEndKm + "-" + previousTripEndHm;
                } else {
                    tripEndDetails = 0 + "-" + 0;
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return tripEndDetails;

    }

    public int insertBillingGraph(String tripSheetId, String graphPath, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        int insertStatus = 0;
        int updateStatus = 0;
        FileInputStream fis = null;
        File file = new File(graphPath);
        System.out.println("file = " + file);
        fis = new FileInputStream(file);
        System.out.println("fis = " + fis);
        byte[] graphFile = new byte[(int) file.length()];
        System.out.println("podFile = " + graphFile);
        fis.read(graphFile);
        fis.close();
        map.put("graphFile", graphFile);
        map.put("fileName", "dataLogExcel" + tripSheetId + ".xls");
        map.put("userId", userId);
        try {
            String status = (String) getSqlMapClientTemplate().queryForObject("trip.checkGraphStatus", map);
            if (status == null) {
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertBillingGraph", map);
            } else {
                updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateStatusBillingGraph", map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertBillingGraph", map);

            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBillingGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertBillingGraph", sqlException);
        }
        return insertStatus;

    }

    public String getTripCount(TripTO tripTO) {
        Map map = new HashMap();
        String tripVehicleId = "";
        String tripCount = "";
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("tripStartDate", tripTO.getTripScheduleDate());
        map.put("tripEndDate", tripTO.getTripEndDate());
        System.out.println("map = " + map);
        try {
            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryTripVehicleId", map);
            if (tripVehicleId != null) {
                map.put("tripVehicleId", tripVehicleId);
                tripCount = (String) getSqlMapClientTemplate().queryForObject("trip.getSecondaryTripCount", map);

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return tripCount;

    }

    public int updateInvoiceAmount(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("editMode", tripTO.getEditMode());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("userId", userId);
        try {

            map.put("billedAmount", tripTO.getBilledAmount());
            if (tripTO.getWeight() != null && !"".equals(tripTO.getWeight())) {
                map.put("weight", tripTO.getWeight());
            } else {
                map.put("weight", 0);
            }
            if (tripTO.getRatePerKg() != null && !"".equals(tripTO.getRatePerKg())) {
                map.put("ratePerKg", tripTO.getRatePerKg());
            } else {
                map.put("ratePerKg", 0);
            }
            if (tripTO.getDiscountAmount() != null && !"".equals(tripTO.getDiscountAmount())) {
                map.put("discountAmount", tripTO.getDiscountAmount());
            } else {
                map.put("discountAmount", 0);
            }
            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceAmountHeader", map);
            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceAmountDetail", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertBillingGraph Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertBillingGraph", sqlException);
        }
        return updateStatus;

    }

    public int insertCourierDetails(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int insertCourierDetails = 0;
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("courierNo", tripTO.getCourierNo());
        map.put("courierRemarks", tripTO.getCourierRemarks());
        map.put("userId", userId);
        try {
            insertCourierDetails = (Integer) getSqlMapClientTemplate().update("trip.insertCourierDetails", map);
            if (insertCourierDetails > 0) {
                insertCourierDetails = (Integer) getSqlMapClientTemplate().update("trip.updateCourierDetails", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCourierDetails", sqlException);
        }
        return insertCourierDetails;

    }

    public ArrayList getCourierDetails(TripTO tripTO) {
        ArrayList invoiceDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            System.out.println("map:" + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCourierDetails", map);
            System.out.println("getCourierDetails size=" + invoiceDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCourierDetails List", sqlException);
        }

        return invoiceDetails;
    }

    public int checkTempGraphApprovedStatus(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int checkTempGraphApprovedStatus = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("userId", userId);
        try {
            checkTempGraphApprovedStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTempGraphApprovedStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTempGraphApprovedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkTempGraphApprovedStatus", sqlException);
        }
        return checkTempGraphApprovedStatus;

    }

    public int checkBillSubmittedStatus(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int checkBillSubmittedStatus = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("invoiceId", tripTO.getInvoiceId());
        map.put("userId", userId);
        try {
            checkBillSubmittedStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkBillSubmittedStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkBillSubmittedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkBillSubmittedStatus", sqlException);
        }
        return checkBillSubmittedStatus;

    }

    public ArrayList getVehicleLogDetails(TripTO tripTO) {
        ArrayList vehicleLogDetails = new ArrayList();
        ArrayList vehicleLogDetailss = new ArrayList();
        Map map = new HashMap();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        try {
            String tripId = tripTO.getTripId();
            System.out.println("map:" + map);
            map.put("tripId", tripId);
            String startDate = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleLogDate", map);
            System.out.println("tripIdtripIdtripId:" + tripId);
            System.out.println("startDatestartDate:" + startDate);
            int transitHours = Integer.parseInt(tripTO.getTripTransitHours());
            System.out.println("transitHourstransitHours:" + transitHours);
            int totalHrs = Integer.parseInt(tripTO.getTotalHrs());
            System.out.println("totalHrstotalHrs:" + totalHrs);
            int j = 0;
            int k = transitHours / totalHrs;
            for (int i = 0; i < k; i++) {
                tripTO2 = new TripTO();
                j = j + k;
                map.put("time", j);
                map.put("startDate", startDate);
                map.put("tripId", tripId);
                System.out.println("map:" + map);

                vehicleLogDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleLogDetails", map);
                System.out.println("tripTO1.vehicleLogDetails()" + vehicleLogDetails.size());
                if (vehicleLogDetails.size() > 0) {
                    Iterator itr4 = vehicleLogDetails.iterator();
                    while (itr4.hasNext()) {
                        tripTO1 = (TripTO) itr4.next();
                        tripTO2.setLogDateTime(tripTO1.getLogDateTime());
                        System.out.println("tripTO1.getLogDateTime()" + tripTO.getLogDateTime());
                        tripTO2.setLogDate(tripTO1.getLogDate());
                        tripTO2.setLogTime(tripTO1.getLogTime());
                        System.out.println("tripTO1.getLogTime()" + tripTO.getLogTime());
                        tripTO2.setLocation(tripTO1.getLocation());
                        tripTO2.setTemperature(tripTO1.getTemperature());
                        tripTO2.setRegNo(tripTO1.getRegNo());
                        tripTO2.setDistance(tripTO1.getDistance());
                        System.out.println("tripTO1.getDistance()" + tripTO.getDistance());
                    }

                    vehicleLogDetailss.add(tripTO2);
                }
            }

            System.out.println("vehicleLogDetailss size=" + vehicleLogDetailss.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleLogDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleLogDetails List", sqlException);
        }

        return vehicleLogDetailss;
    }

    public ArrayList getEmployeeTripDetails(TripTO tripTO) {
        ArrayList invoiceDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("tripId", tripTO.getTripSheetId());
            map.put("employeeId", tripTO.getEmployeeId());
            System.out.println("map:" + map);
            invoiceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmployeeTripDetails", map);
            System.out.println("getEmployeeTripDetails size=" + invoiceDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmployeeTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getEmployeeTripDetails List", sqlException);
        }

        return invoiceDetails;
    }

    public int updateEmployeeInTrip(TripTO tripTO, int userId) throws FileNotFoundException, IOException {
        Map map = new HashMap();
        int status = 0;
        map.put("startDate", tripTO.getStartDate());
        map.put("endDate", tripTO.getEndDate());
        String driverChangeHour = "00";
        String driverChangeMinute = "00";
        if (tripTO.getDriverChangeHour() != null && !"".equals(tripTO.getDriverChangeHour())) {
            driverChangeHour = "00";
        } else {
            driverChangeHour = tripTO.getDriverChangeHour();
        }
        if (tripTO.getDriverChangeMinute() != null && !"".equals(tripTO.getDriverChangeMinute())) {
            driverChangeMinute = "00";
        } else {
            driverChangeMinute = tripTO.getDriverChangeMinute();
        }
        map.put("endTime", driverChangeHour + ":" + driverChangeMinute + ":00");
        map.put("startOdometerReading", tripTO.getStartOdometerReading());
        map.put("endOdometerReading", tripTO.getEndOdometerReading());
        map.put("totalKm", tripTO.getTotalKM());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("balanceAmount", tripTO.getBalanceAmount());
        map.put("returnAmount", tripTO.getReturnAmount());
        map.put("advancePaid", tripTO.getActualAdvancePaid());
        map.put("staffId", tripTO.getEmployeeId());
        map.put("remarks", tripTO.getActionRemarks());
        map.put("activeInd", tripTO.getStatus());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("driverId", tripTO.getPrimaryDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("startHm", tripTO.getStartHM());
        map.put("endHm", tripTO.getEndHM());
        map.put("totalHm", tripTO.getTotalHm());
        map.put("remarks", tripTO.getRemarks());
        map.put("cityId", tripTO.getCityId());
        map.put("driverCount", tripTO.getDriverCount());
        map.put("driverInTrip", tripTO.getDriverInTrip());
        map.put("userId", userId);
        String driverType = "";
        System.out.println("map update driver on a trip = " + map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEmployeeInTrip", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.inactiveTripDriver", map);
            driverType = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverType", map);
            if (driverType.equals("P")) {
                map.put("type", "P");
            } else if (driverType.equals("S")) {
                map.put("type", "S");
            } else if (driverType.equals("T")) {
                map.put("type", "S");
            }
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripDriver", map);
            status = (Integer) getSqlMapClientTemplate().update("employee.saveEmployeeStatus", map);
            System.out.println("updateNewDriverVehicleDriverMapping" + map);
            String vehicleid = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleIdForNewDriver", map);
            if (vehicleid != null) {
                map.put("oldVehicleId", vehicleid);
                map.put("oldDriverId", "0");
                if (driverType.equals("P")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingPrimary", map);
                } else if (driverType.equals("S")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingSecondary", map);
                } else if (driverType.equals("T")) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateNewDriverVehicleDriverMappingTeritory", map);
                }
                System.out.println("updateOldDriverVehicleDriverMapping" + map);
//                  status = (Integer) getSqlMapClientTemplate().update("trip.updateOldDriverVehicleDriverMapping", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertCourierDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertCourierDetails", sqlException);
        }
        return status;

    }

    public String checkEmployeeInTrip(String driverId) {
        Map map = new HashMap();
        TripTO tripTO = null;
        map.put("staffId", driverId);
        String checkEmployeeInTrip = "";
        System.out.println("map = " + map);
        try {
            checkEmployeeInTrip = (String) getSqlMapClientTemplate().queryForObject("employee.checkDriverStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkEmployeeInTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkEmployeeInTrip", sqlException);
        }
        return checkEmployeeInTrip;

    }

    public int insertMailDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int insertMailDetails = 0;
        map.put("mailTypeId", tripTO.getMailTypeId());
        map.put("mailSubjectTo", tripTO.getMailSubjectTo());
        map.put("mailSubjectCc", tripTO.getMailSubjectCc());
        map.put("mailSubjectBcc", tripTO.getMailSubjectBcc());
        map.put("mailContentTo", tripTO.getMailContentTo());
        map.put("mailContentCc", tripTO.getMailContentCc());
        map.put("mailContentBcc", tripTO.getMailContentBcc());
        map.put("mailTo", tripTO.getMailIdTo());
        map.put("mailCc", tripTO.getMailIdCc());
        map.put("mailBcc", tripTO.getMailIdBcc());
        map.put("filepath", tripTO.getFilePath());
        map.put("userId", userId);
        System.out.println("map####### = " + map);
        try {
            insertMailDetails = (Integer) getSqlMapClientTemplate().insert("trip.insertMailDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertMailDetails", sqlException);
        }
        return insertMailDetails;

    }

    public ArrayList getMailNotDeliveredList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList mailNotDeliveredList = new ArrayList();
        map.put("mailSendingId", tripTO.getMailSendingId());
        map.put("mailDeliveredStatusId", tripTO.getMailDeliveredStatusId());
        try {
            System.out.println("closed Trip map is::" + map);
            mailNotDeliveredList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getMailNotDeliveredList", map);
            System.out.println("getMailNotDeliveredList.size() = " + mailNotDeliveredList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMailNotDeliveredList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMailNotDeliveredList List", sqlException);
        }
        return mailNotDeliveredList;
    }

    public int updateTripOtherExpenseDoneStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int updateOtherExpenseDoneStatus = 0;
        map.put("tripId", tripTO.getTripSheetId());
        map.put("userId", userId);
        System.out.println("map = " + map);
        try {
            updateOtherExpenseDoneStatus = (Integer) getSqlMapClientTemplate().update("trip.updateOtherExpenseDoneStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateOtherExpenseDoneStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateOtherExpenseDoneStatus", sqlException);
        }
        return updateOtherExpenseDoneStatus;

    }

    public String getOtherExpenseDoneStatus(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripSheetId());
        String otherExpenseDoneStatus = "";
        System.out.println("map = " + map);
        try {
            otherExpenseDoneStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseDoneStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOtherExpenseDoneStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getOtherExpenseDoneStatus", sqlException);
        }
        return otherExpenseDoneStatus;

    }

    public String getLastUploadCustomerOutstandingDate() {
        Map map = new HashMap();
        String lastCustomerOutstandingDate = "";
        System.out.println("map = " + map);
        try {
            lastCustomerOutstandingDate = (String) getSqlMapClientTemplate().queryForObject("trip.getLastUploadCustomerOutstandingDate", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLastUploadCustomerOutstandingDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getLastUploadCustomerOutstandingDate", sqlException);
        }
        return lastCustomerOutstandingDate;

    }

    public String getPreviousOutStandingAmount(TripTO tripTO) {
        Map map = new HashMap();
        map.put("customerCode", tripTO.getCustomerCode());
        map.put("customerName", tripTO.getCustomerName());
        map.put("userId", tripTO.getUserId());
        System.out.println("map = " + map);
        String previousOutStandingAmount = "";
        int updateCustomerOutStanding = 0;
        try {
            previousOutStandingAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousOutStandingAmount", map);
            System.out.println("previousOutStandingAmount = " + previousOutStandingAmount);
            if (previousOutStandingAmount == null) {
                updateCustomerOutStanding = (Integer) getSqlMapClientTemplate().update("trip.insertCustomerOutStandingAmount", map);
                System.out.println("updateCustomerOutStanding = " + updateCustomerOutStanding);
                previousOutStandingAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousOutStandingAmount", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousOutStandingAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousOutStandingAmount", sqlException);
        }
        return previousOutStandingAmount;

    }

    public int updateCustomerOutStandingAmount(String[] customerId, String[] outStandingAmount, int userId) {
        Map map = new HashMap();
        int updateCustomerOutStandingAmount = 0;
        double creditLimit = 0.0;
        map.put("userId", userId);
        try {
            for (int i = 0; i < customerId.length; i++) {
                map.put("customerId", customerId[i]);
                map.put("amount", outStandingAmount[i]);
                creditLimit = (Double) getSqlMapClientTemplate().queryForObject("trip.getCustomerCreditLimit", map);
                int retVal = Double.compare(creditLimit, Double.parseDouble(outStandingAmount[i]));
                if (retVal >= 0) {
                    map.put("cnoteCount", "0");
                } else if (retVal < 0) {
                    map.put("cnoteCount", "1");
                }
                System.out.println("map = " + map);
                updateCustomerOutStandingAmount += (Integer) getSqlMapClientTemplate().update("trip.updateCustomerOutStandingAmount", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCustomerOutStandingAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateCustomerOutStandingAmount", sqlException);
        }
        return updateCustomerOutStandingAmount;

    }

    public ArrayList getCustomerOutStandingList() {
        Map map = new HashMap();
        ArrayList customerOutStandingList = new ArrayList();
        try {
            System.out.println("closed Trip map is::" + map);
            customerOutStandingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerOutStandingList", map);
            System.out.println("customerOutStandingList.size() = " + customerOutStandingList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerOutStandingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerOutStandingList List", sqlException);
        }
        return customerOutStandingList;
    }

    public ArrayList getSecondaryCustomerApprovalList() {
        Map map = new HashMap();
        ArrayList secondaryCustomerApprovalList = new ArrayList();
        try {
            System.out.println("closed Trip map is::" + map);
            secondaryCustomerApprovalList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecondaryCustomerApprovalList", map);
            System.out.println("getSecondaryCustomerApprovalList.size() = " + secondaryCustomerApprovalList.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSecondaryCustomerApprovalList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSecondaryCustomerApprovalList List", sqlException);
        }
        return secondaryCustomerApprovalList;
    }

    public int updateSecondaryCustomerMail(TripTO tripTO) {
        Map map = new HashMap();
        int updateSecondaryCustomerMail = 0;
        try {
            map.put("customerId", tripTO.getCustomerId());
            map.put("emailId", tripTO.getEmailId());
            System.out.println("map = " + map);
            updateSecondaryCustomerMail = (Integer) getSqlMapClientTemplate().update("trip.updateSecondaryCustomerMail", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateSecondaryCustomerMail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateSecondaryCustomerMail", sqlException);
        }
        return updateSecondaryCustomerMail;

    }

    public int updateEmptyTripRoute(TripTO tripTO) {
        Map map = new HashMap();
        int updateEmptyTripRoute = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("originId", tripTO.getOrigin());
            map.put("destinationId", tripTO.getDestination());
            map.put("cityFrom", tripTO.getCityFrom());
            map.put("cityTo", tripTO.getCityTo());
            map.put("routeInfo", tripTO.getCityFrom() + "-" + tripTO.getCityTo());
            map.put("expense", tripTO.getExpense());
            map.put("totalKm", tripTO.getTotalKm());
            map.put("totalHrs", tripTO.getTotalHours());
            map.put("totalMinutes", tripTO.getTotalMinutes());
            map.put("emptyTripRemarks", tripTO.getEmptyTripRemarks());
            map.put("routeId", tripTO.getRouteId());
            //set transit days
            float transitHours = 0.00F;
            if (tripTO.getTotalHours() != null && !"".equals(tripTO.getTotalHours())) {
                transitHours = Float.parseFloat(tripTO.getTotalHours());
            }
            double transitDay = (double) transitHours / 24;
            tripTO.setTripTransitDays(transitDay + "");
            //set advance to be paid per day
            float totalExpense = 0.00f;
            if (tripTO.getExpense() != null && !"".equals(tripTO.getExpense())) {
                totalExpense = Float.parseFloat(tripTO.getExpense());
            }

            int transitDayValue = (int) transitDay;
            if (transitDay > transitDayValue) {
                transitDay = transitDayValue + 1;
            }
            double advnaceToBePaidPerDay = 0;

            if (totalExpense > 0 && transitHours > 0) {
                advnaceToBePaidPerDay = totalExpense / transitDay;
            }
            map.put("transitDay", transitDay);
            map.put("transitHours", transitHours);
            map.put("advnaceToBePaidPerDay", advnaceToBePaidPerDay);

            System.out.println("map = " + map);
            updateEmptyTripRoute = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripMaster", map);
            System.out.println("updateEmptyTripRoute = " + updateEmptyTripRoute);
            int updateEmptyTripRouteCourse = 0;
            int updateEmptyTripConsignment = 0;
            if (updateEmptyTripRoute > 0) {
                updateEmptyTripRouteCourse = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripRouteCourseOrigin", map);
                System.out.println("updateEmptyTripRouteCourse one= " + updateEmptyTripRouteCourse);
                updateEmptyTripRouteCourse = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripRouteCourseDestination", map);
                System.out.println("updateEmptyTripRouteCourse two= " + updateEmptyTripRouteCourse);
                updateEmptyTripConsignment = (Integer) getSqlMapClientTemplate().update("trip.updateEmptyTripConsignment", map);
                System.out.println("updateEmptyTripConsignment two= " + updateEmptyTripConsignment);

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEmptyTripRoute Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateEmptyTripRoute", sqlException);
        }
        return updateEmptyTripRoute;

    }

    public String getVehicleUsageTypeId(String vehicleId) {
        Map map = new HashMap();
        map.put("vehicleId", vehicleId);
        System.out.println("map = " + map);
        String usageTypeId = "";
        try {
            usageTypeId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleUsageTypeId", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleUsageTypeId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleUsageTypeId", sqlException);
        }
        return usageTypeId;

    }

    public String getVehicleCurrentStatus(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        System.out.println("map = " + map);
        String tripStatusId = "";
        try {
            tripStatusId = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleCurrentStatus", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleCurrentStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleCurrentStatus", sqlException);
        }
        return tripStatusId;

    }

    public int updateDeleteTrip(TripTO tripTO) {
        Map map = new HashMap();
        int updatedeletetrip = 0;
        int updateGr = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("tripStatusId", tripTO.getStatusId());
            map.put("remarks", tripTO.getRemarks());
            String vehicleCurrentStatus = "";
            System.out.println("map = " + map);
            if (tripTO.getStatusId().equals("3")) {
                System.out.println("i m in cancel");
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTrip", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripVehicle", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripStatusDetails", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripAdvance", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateDeleteTripFuel", map);
                updateGr = (Integer) getSqlMapClientTemplate().update("trip.cancelTripGR", map);
            } else if (tripTO.getStatusId().equals("8")) {
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTrip", map);
//                updateOrderStatusToEFS(tripTO.getTripId(), "8");
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTripVehicle", map);
                updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateFreezeTripStatusDetails", map);
            } else if (tripTO.getStatusId().equals("10")) {
                vehicleCurrentStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleCurrentStatus", map);
                if (vehicleCurrentStatus == null) {
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripEnd", map);
//                    updateOrderStatusToEFS(tripTO.getTripId(), "10");
                    //updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripWfu", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripVehicle", map);
                    updatedeletetrip = (Integer) getSqlMapClientTemplate().update("trip.updateInprogressTripStatusDetails", map);
                }
            }
//            else if (tripTO.getStatusId().equals("3")) {
//                     updateGr= (Integer) getSqlMapClientTemplate().update("trip.cancelTripGR", map);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatedeletetrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updatedeletetrip", sqlException);
        }
        return updatedeletetrip;

    }

    public int updateTripAdvance(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("tripAdvanceId", tripTO.getTripAdvanceId());
        map.put("reqAmount", tripTO.getAdvanceAmount());

        map.put("userId", userId);
        System.out.println("the updateTripAdance" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripAdvance", map);
            System.out.println("updateTripAdvance=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripAdvance" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripAdvance", sqlException);
        }

        return status;
    }

    public int updateTripEnd(TripTO tripTO, int userId) {
        Map map = new HashMap();
        // map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
        //  map.put("startDate", tripTO.getStartDate());
        map.put("endTime", tripTO.getTripEndHour() + ":" + tripTO.getTripEndMinute() + ":00");
        map.put("endDate", tripTO.getEndDate());
        map.put("totalDays", tripTO.getTotalDays());
        map.put("tripTransitHours", tripTO.getTripTransitHours());
        //map.put("endOdometerReading", tripTO.getEndOdometerReading());
        //map.put("endHM", tripTO.getEndHM());
        map.put("tripId", tripTO.getTripId());
        // map.put("tripSheetId", tripTO.getTripSheetId());
        //map.put("endTripRemarks", tripTO.getEndTripRemarks());
        // map.put("totalKM", tripTO.getTotalKM());
        // map.put("totalHrs", tripTO.getTotalHrs());
        // map.put("userId", userId);
        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        System.out.println("the updateEndTrip Details in the DAO " + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEndTrip", map);
            System.out.println("updateEndTrip size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTripStartDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();

        map.put("tripId", tripTO.getTripId());

        map.put("tripStartDate", tripTO.getStartDate());
        map.put("tripStartTime", tripTO.getStartTime());
        System.out.println("the updateStartTrip Details in the DAO" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripDetails", map);
            System.out.println("updateEndTrip size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEndTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEndTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public ArrayList getTripClosureVehicleList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tripClosureVehicle = new ArrayList();
        ArrayList tripClosureVehicleList = new ArrayList();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        int serialNo = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("statusId", tripTO.getStatusId());
            System.out.println("closed Trip map is::" + map);
            tripClosureVehicle = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripClosureVehicleList", map);
//            if (tripClosureVehicle.size() > 0) {
//                Iterator itr = tripClosureVehicle.iterator();
//                while (itr.hasNext()) {
//                    tripTO2 = new TripTO();
//                    tripTO1 = (TripTO) itr.next();
//                    serialNo = Integer.parseInt(tripTO1.getSerialNumber());
//                    tripTO2.setSerialNumber(tripTO1.getSerialNumber());
//                    if (serialNo == 1) {
//                        tripTO2.setTripCode(tripTO1.getTripCode());
//                        tripTO2.setRegNo(tripTO1.getRegNo());
//                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
//                        tripTO2.setOriginCityName(tripTO1.getOriginCityName());
//                        tripTO2.setStartDate(tripTO1.getStartDate());
//                        tripTO2.setEndDate(tripTO1.getVehicleChangeDate());
//                        tripTO2.setStartKm(tripTO1.getStartKm());
//                        tripTO2.setEndKm(tripTO1.getEndKm());
//                        tripTO2.setTotalKm(tripTO1.getTotalKm());
//                        tripTO2.setStartHm(tripTO1.getStartHm());
//                        tripTO2.setEndHm(tripTO1.getEndHm());
//                        tripTO2.setTotalHm(tripTO1.getTotalHm());
//                        tripTO2.setTransitDays1(tripTO1.getTransitDays1());
//                        tripTO2.setVehicleId(tripTO1.getVehicleId());
//                    } else if (serialNo > 1 && serialNo != tripClosureVehicle.size()) {
//                        tripTO2.setTripCode(tripTO1.getTripCode());
//                        tripTO2.setRegNo(tripTO1.getRegNo());
//                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
//                        tripTO2.setOriginCityName(tripTO1.getVehicleChangeCityName());
//                        tripTO2.setStartDate(tripTO1.getVehicleChangeDate());
//                        tripTO2.setEndDate(tripTO1.getVehicleChangeDate());
//                        tripTO2.setStartKm(tripTO1.getStartKm());
//                        tripTO2.setEndKm(tripTO1.getEndKm());
//                        tripTO2.setTotalKm(tripTO1.getTotalKm());
//                        tripTO2.setStartHm(tripTO1.getStartHm());
//                        tripTO2.setEndHm(tripTO1.getEndHm());
//                        tripTO2.setTotalHm(tripTO1.getTotalHm());
//                        tripTO2.setTransitDays1(tripTO1.getTransitDays2());
//                        tripTO2.setVehicleId(tripTO1.getVehicleId());
//                    } else if (serialNo > 1 && serialNo == tripClosureVehicle.size()) {
//                        tripTO2.setTripCode(tripTO1.getTripCode());
//                        tripTO2.setRegNo(tripTO1.getRegNo());
//                        tripTO2.setRouteInfo(tripTO1.getRouteInfo());
//                        tripTO2.setOriginCityName(tripTO1.getVehicleChangeCityName());
//                        tripTO2.setStartDate(tripTO1.getVehicleChangeDate());
//                        tripTO2.setEndDate(tripTO1.getEndDate());
//                        tripTO2.setStartKm(tripTO1.getStartKm());
//                        tripTO2.setEndKm(tripTO1.getEndKm());
//                        tripTO2.setTotalKm(tripTO1.getTotalKm());
//                        tripTO2.setStartHm(tripTO1.getStartHm());
//                        tripTO2.setEndHm(tripTO1.getEndHm());
//                        tripTO2.setTotalHm(tripTO1.getTotalHm());
//                        tripTO2.setTransitDays1(tripTO1.getTransitDays2());
//                        tripTO2.setVehicleId(tripTO1.getVehicleId());
//                    }
//                    tripClosureVehicleList.add(tripTO2);
//                }
//            }
            System.out.println("getTripClosureVehicleList.size() = " + tripClosureVehicleList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripClosureVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripClosureVehicleList List", sqlException);
        }
        return tripClosureVehicle;
//        return tripClosureVehicleList;
    }

    public ArrayList getTripSettlementVehicleList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        System.out.println("map = " + map);
        ArrayList customer = new ArrayList();

        try {
            customer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSettlementVehicleList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSettlementVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTripSettlementVehicleList", sqlException);
        }
        return customer;

    }

    public ArrayList getEmptyCustomerList(TripTO tripTO) {
        Map map = new HashMap();

        System.out.println("map = " + map);
        ArrayList customer = new ArrayList();

        try {
            customer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmptyCustomerList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmptyCustomerList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getEmptyCustomerList", sqlException);
        }
        return customer;

    }

    public ArrayList getTicketingStatusList(TripTO tripTO) {
        Map map = new HashMap();

        System.out.println("map = " + map);
        ArrayList ticketingStatusList = new ArrayList();

        try {
            ticketingStatusList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingStatusList", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingStatusList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingStatusList", sqlException);
        }
        return ticketingStatusList;

    }

    public ArrayList getTicketingList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        map.put("roleId", tripTO.getRoleId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketingDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingDetailsList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingDetailsList", sqlException);
        }
        return ticketingList;
    }

    public ArrayList getTicketingStatusDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("userId", tripTO.getUserId());
        map.put("ticketId", tripTO.getTicketId());
        System.out.println("map = " + map);
        ArrayList ticketingList = new ArrayList();
        try {
            ticketingList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTicketingStatusDetailsList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTicketingStatusDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getTicketingStatusDetailsList", sqlException);
        }
        return ticketingList;
    }

    public int saveTicketFile(String actualFilePath, String fileSaved, TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ticketId = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("fileName", fileSaved);
            map.put("userId", userId);
            System.out.println("actualFilePath = " + actualFilePath);
            File file = new File(actualFilePath);
            System.out.println("file = " + file);
            fis = new FileInputStream(file);
            System.out.println("fis = " + fis);
            byte[] ticketFile = new byte[(int) file.length()];
            System.out.println("ticketFile = " + ticketFile);
            fis.read(ticketFile);
            fis.close();
            map.put("ticketFile", ticketFile);
            map.put("ticketId", tripTO.getTicketId());
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTicketDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTicket Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTicket List", sqlException);
        }

        return ticketId;
    }

    public int saveTicket(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int ticketId = 0;
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("userId", userId);
            map.put("status", tripTO.getStatus());
            map.put("type", tripTO.getType());
            map.put("from", tripTO.getFrom());
            map.put("to", tripTO.getTo());
            map.put("cc", tripTO.getCc());
            map.put("title", tripTO.getTitle());
            map.put("message", tripTO.getMessage());
            map.put("priority", tripTO.getPriority());
            System.out.println("the consignorDetails" + map);
            ticketId = (Integer) getSqlMapClientTemplate().insert("trip.saveTicket", map);
            map.put("ticketId", ticketId);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTicketStatusDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTicket Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTicket List", sqlException);
        }

        return ticketId;
    }

    public int updateTicketStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int ticketId = 0;
        int status = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("userId", userId);
            map.put("ticketId", tripTO.getTicketId());
            map.put("statusId", tripTO.getStatusId());
            map.put("message", tripTO.getMessage());
            System.out.println("the consignorDetails" + map);
            ticketId = (Integer) getSqlMapClientTemplate().update("trip.updateTicketMaster", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTicketStatusDetails", map);
            System.out.println("saveTripPodDetails size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTicketMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTicketMaster List", sqlException);
        }

        return ticketId;
    }

    public ArrayList gettripsettelmentDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        ArrayList tripsettelmentDetails = new ArrayList();
        System.out.println(" gettripsettelmentDetails map:" + map);
        try {

            tripsettelmentDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.gettripsettelmentDetails", map);

            System.out.println(" gettripsettelmentDetails size=" + tripsettelmentDetails.size());
            System.out.println(" gettripsettelmentDetails elements=" + tripsettelmentDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog(" gettripsettelmentDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripsettelmentDetails List", sqlException);
        }

        return tripsettelmentDetails;
    }

    public String getPreviousTripsEndTime(String tripSheetId) {
        Map map = new HashMap();

        String tripVehicleId = "";
        String tripVehicleId1 = "";
        String previousTripEndDetails = "";
        String previousTripType = "";
        String[] temp = null;
        map.put("tripSheetId", tripSheetId);
        System.out.println("map = " + map);
        String tripEndDetails = "";
        String previousTripEndDate = null;
        String previousTripEndTime = null;
        ArrayList tripClosureVehicle = new ArrayList();
        TripTO tripTO1 = new TripTO();
        TripTO tripTO2 = new TripTO();
        int serialNo = 0;
        String PreviousTripId = "";
        try {

            tripVehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);

            System.out.println("tripVehicleId = " + tripVehicleId);
            if (tripVehicleId != null && tripVehicleId != "") {
                map.put("tripVehicleId", tripVehicleId);

                PreviousTripId = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripId", map);
                System.out.println("tripVehicleId = " + PreviousTripId);
                if (PreviousTripId != null && PreviousTripId != "") {
                    map.put("tripId", PreviousTripId);
                    previousTripEndDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getPreviousTripEndTime", map);
                    System.out.println("previousTripEndTimeDetails = " + previousTripEndDetails);
                }
            }
            if (previousTripEndDetails != null && previousTripEndDetails != "") {
                System.out.println("previousTripEndDetails = " + previousTripEndDetails);
                temp = previousTripEndDetails.split("~");
                previousTripEndDate = temp[0];
                previousTripEndTime = temp[1];
            }
            System.out.println("previousTripEndDate = " + previousTripEndDate);
            System.out.println("previousTripEndTime = " + previousTripEndTime);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPreviousTripsOdometerReading Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPreviousTripsOdometerReading", sqlException);
        }
        return previousTripEndDetails;

    }

    public String getTripVehicleStatus(String tripId) {
        Map map = new HashMap();
        map.put("tripId", tripId);
        String tripVehicleStatus = "";
        try {

            tripVehicleStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleStatus", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripVehicleStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripVehicleStatus List", sqlException);
        }

        return tripVehicleStatus;
    }

    public ArrayList getSecFCmailcontactDetails(String customerName) {
        ArrayList getSecFCmailcontactDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("customerName", customerName);
            getSecFCmailcontactDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getSecFCmailContactDetails", map);
            System.out.println("getSecFCmailcontactDetails size=" + getSecFCmailcontactDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getmailcontactDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getmailcontactDetails List", sqlException);
        }

        return getSecFCmailcontactDetails;
    }

    public int updateNextTrip(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ticketId = 0;
        FileInputStream fis = null;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("nextTrip", tripTO.getNextTrip());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateNextTrip", map);
            System.out.println("updateNextTrip =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateNextTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateNextTrip List", sqlException);
        }

        return ticketId;
    }

    public ArrayList getPrimaryDriverSettlementTrip(TripTO tripTO) {
        ArrayList primaryDriverSettlementTrip = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            System.out.println("map getPrimaryDriverSettlementTrip = " + map);
            primaryDriverSettlementTrip = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrimaryDriverSettlementTrip", map);
            System.out.println("getPrimaryDriverSettlementTrip size=" + primaryDriverSettlementTrip.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrimaryDriverSettlementTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrimaryDriverSettlementTrip List", sqlException);
        }

        return primaryDriverSettlementTrip;
    }

    public ArrayList getVehicleDriverAdvance(TripTO tripTO) {
        ArrayList vehicleDriverAdvance = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            vehicleDriverAdvance = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleDriverAdvance", map);
            System.out.println("getSecFCmailcontactDetails size=" + vehicleDriverAdvance.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDriverAdvance List", sqlException);
        }

        return vehicleDriverAdvance;
    }

    public ArrayList getDriverIdleBhatta(TripTO tripTO) {
        ArrayList driverIdleBhatta = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            driverIdleBhatta = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverIdleBhatta", map);
            System.out.println("getDriverIdleBhatta size=" + driverIdleBhatta.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverIdleBhatta Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverIdleBhatta List", sqlException);
        }

        return driverIdleBhatta;
    }

    public String getDriverLastBalanceAmount(TripTO tripTO) {
        String driverLastBalanceAmount = "";
        Map map = new HashMap();
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            driverLastBalanceAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getDriverLastBalanceAmount", map);
            if (driverLastBalanceAmount == null) {
                driverLastBalanceAmount = "0";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDriverLastBalanceAmount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDriverLastBalanceAmount List", sqlException);
        }

        return driverLastBalanceAmount;
    }

    public int insertPrimaryDriverSettlement(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("driverId", tripTO.getDriverId());
            map.put("fromDate", tripTO.getFromDate());
            map.put("toDate", tripTO.getToDate());
            map.put("driverLastBalance", tripTO.getDriverLastBalance());
            map.put("tripAmount", tripTO.getTripAmount());
            map.put("advanceAmount", tripTO.getAdvanceAmount());
            map.put("settleAmount", tripTO.getSettleAmount());
            map.put("payAmount", tripTO.getPayAmount());
            map.put("paymentMode", tripTO.getPaymentMode());
            map.put("balanceAmount", tripTO.getBalanceAmount());
            map.put("settlementTripSize", tripTO.getSettlementTripsSize());
            map.put("bhattaAmount", tripTO.getBhattaAmount());
            map.put("idleDays", tripTO.getIdleDays());
            map.put("settlementRemarks", tripTO.getRemarks());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().insert("trip.insertPrimaryDriverSettlement", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPrimaryDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertPrimaryDriverSettlement List", sqlException);
        }

        return status;
    }

    public int insertPrimaryDriverSettlementDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("driverSettlementId", tripTO.getStatus());
            map.put("transactionType", tripTO.getTransactionType());
            map.put("transactionId", tripTO.getTransactionId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("driverId", tripTO.getDriverId());
            map.put("transactionAmount", tripTO.getTransactionAmount());
            map.put("driverCount", tripTO.getDriverCount());
            map.put("settlementAmount", tripTO.getAmount());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertPrimaryDriverSettlementDetails", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertPrimaryDriverSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertPrimaryDriverSettlement List", sqlException);
        }

        return status;
    }

    public int updateIdleBhattaStatus(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int tripId = 0;
        int cityId = 0;
        try {
            map.put("idleBhattaId", tripTO.getIdleBhattaId());
            map.put("userId", userId);
            System.out.println("update Next Trip Status = " + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateIdleBhattaStatus", map);
            System.out.println("insertPrimaryDriverSettlement =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateIdleBhattaStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateIdleBhattaStatus List", sqlException);
        }

        return status;
    }

    public ArrayList getTripOtherExpenseDocumentRequired(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripSheetId());
        System.out.println("map in other expense document:=" + map);
        ArrayList getDocumentRequiredDetails = new ArrayList();
        try {

            getDocumentRequiredDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripOtherExpenseDocumentRequired", map);
            System.out.println("getDocumentRequiredDetails.size() = " + getDocumentRequiredDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDocumentRequiredDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDocumentRequiredDetails", sqlException);
        }

        return getDocumentRequiredDetails;
    }

    public ArrayList getOtherExpenseFileDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOtherExpenseFile", map);
            System.out.println("getPODDetails.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPODDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPODDetails List", sqlException);
        }
        return statusDetails;
    }

    public int deleteExpenaseBillCopy(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int updateStatus = 0;
        int updateStatus1 = 0;
        try {

            map.put("expenseId", tripTO.getExpenseId());
            System.out.println("map for expense delete:" + map);

            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.deleteExpenseBillCopy", map);
            updateStatus1 = (Integer) getSqlMapClientTemplate().update("trip.updateTripExpenseForBillDelete", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("deletefunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "deletefunctions", sqlException);
        }
        return updateStatus;
    }

    public String getTripUnclearedBalance(TripTO tripTO) {
        String driverLastBalanceAmount = "";
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        try {
            driverLastBalanceAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getTripUnclearedBalance", map);
            if (driverLastBalanceAmount == null) {
                driverLastBalanceAmount = "0";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripUnclearedBalance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripUnclearedBalance List", sqlException);
        }

        return driverLastBalanceAmount;
    }

    public ArrayList getTripSheetDetailsForChallan(TripTO tripTO) {
        Map map = new HashMap();
        map.put("driverId", tripTO.getDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("documentRequiredStatus", tripTO.getDocumentRequiredStatus());
        System.out.println("map = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSheetDetailsForChallan", map);
            System.out.println("getTripSheetDetails for challan size=" + tripDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetailsforchallan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetailsforchallan List", sqlException);
        }

        return tripDetails;
    }

    public String getTotalBillExpenseForDriver(TripTO tripTO) {
        Map map = new HashMap();
        map.put("driverId", tripTO.getDriverId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("documentRequiredStatus", tripTO.getDocumentRequiredStatus());
        String billExpense = "";
        try {

            billExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getTotalBillExpenseForDriver", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalBillExpenseForDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalBillExpenseForDriver List", sqlException);
        }

        return billExpense;
    }

    public ArrayList getOrderDetailsForEnd(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        System.out.println("map = " + map);
        ArrayList orderDetails = new ArrayList();
        try {
            orderDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderDetailsForEnd", map);
            System.out.println("getTripSheetDetails for challan size=" + orderDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderDetailsForEnd Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderDetailsForEnd", sqlException);
        }

        return orderDetails;
    }

    public ArrayList getVehicleForGpsCurrentLocation(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList vehicleDetails = new ArrayList();
        try {
            vehicleDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleForGpsCurrentLocation", map);
            System.out.println("getVehicleForGpsCurrentLocation for challan size=" + vehicleDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleForGpsCurrentLocation Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleForGpsCurrentLocation", sqlException);
        }

        return vehicleDetails;
    }

    public int getRouteExpenseDetails(String val, String vehicleTypeId, TripTO tripTO) {
        Map map = new HashMap();
        //  ArrayList tripAdvanceDetails = new ArrayList();

        int routeExpense = 0;
        int update = 0;
        //  int OriginHubId=0;
        // int DestinationHubId=0;
        List ConsignmentNos = new ArrayList();
        List hubId = new ArrayList();
        int Expense = 0;
        map.put("vehicleTypeId", vehicleTypeId);
        try {
            String[] pointIds = val.split(":");
            System.out.println(pointIds.length);
            for (int i = 1; i <= pointIds.length - 1; i++) {
                String[] consignmentList = pointIds[i].split("~");
                System.out.println(consignmentList.length);
                for (int k = 0; k < (consignmentList.length - 1); k++) {
                    map.put("consignmentId", consignmentList[k]);
                    map.put("sequenceId", consignmentList[k + 1]);
                    System.out.println("C-note id map:" + map);
                    update = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentExecutionSequence", map);
                    System.out.println("updated:" + update);
                    ConsignmentNos.add(consignmentList[k]);
                }

                System.out.println("outer loop:");
            }
            map.put("ConsignmentNos", ConsignmentNos);
            System.out.println("C-note id map with:" + ConsignmentNos);
            hubId = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getHubId", map);
            System.out.println("hubId with:" + hubId.size());
            for (int j = 0; j < hubId.size() - 1; j++) {
                TripTO OriginHubId = null;
                TripTO DestinationHubId = null;
                OriginHubId = (TripTO) hubId.get(j);
                if (hubId.get(j + 1) != hubId.get(j)) {
                    DestinationHubId = (TripTO) hubId.get(j + 1);
                }
                map.put("OriginHubId", OriginHubId.getHubId());
                map.put("DestinationHubId", DestinationHubId.getHubId());
                System.out.println("last map with:" + map);
                Expense = Expense + (Integer) getSqlMapClientTemplate().queryForObject("trip.getRouteExpense", map);
            }

            System.out.println("routeExpense = " + Expense);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return Expense;
    }

    public double getSecondaryRouteExpenseDetails(String totalkm, String vehicleTypeId, String originId) {
        Map map = new HashMap();
        //  ArrayList tripAdvanceDetails = new ArrayList();
        int routeExpense = 0;
        Double expense = 0.0;
        try {
            String vehmilegfuil;
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("originId", originId);
            System.out.println("fuel query map" + map);
            //System.out.println("closed Trip map is::" + map);
            vehmilegfuil = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleMileg", map);
            System.out.println(vehmilegfuil);
            String Fuel = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelRate", map);
            System.out.println(Fuel);
            expense = ((Integer.parseInt(totalkm) / Double.parseDouble(vehmilegfuil)) * Double.parseDouble(Fuel));

            System.out.println(totalkm + "routeExpense = " + expense);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return expense;
    }

    public String getPointKm(TripTO tripTO) {
        Map map = new HashMap();
        String vehicleNo = tripTO.getVehicleNo();
        map.put("originId", tripTO.getOrigin());
        map.put("destinationId", tripTO.getDestination());
        map.put("vehicleId", tripTO.getVehicleId());
        System.out.println("vehicleNo = " + vehicleNo);

        String pointkm = "";
        ArrayList pointkm1 = new ArrayList();
        try {
            System.out.println("map: in the dao" + map);
            pointkm = (String) getSqlMapClientTemplate().queryForObject("trip.getPointKM", map);
            System.out.println("pointkm size=" + pointkm);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return pointkm;
    }

    public ArrayList getConfigurationMaster(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList configurationMaster = new ArrayList();

        try {
            System.out.println("map: in the dao" + map);
            configurationMaster = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConfigurationMaster", map);
            System.out.println("getConfigurationMaster size=" + configurationMaster.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return configurationMaster;
    }
    //get google data

    public ArrayList getRouteDistanceCity(String origin, String destination) throws MalformedURLException, IOException {
        int status = 0;
        if (origin.contains(",")) {
            origin = origin.replace(",", "");
        }
        if (destination.contains(",")) {
            destination = destination.replace(",", "");
        }
        URL url = new URL("https://maps.googleapis.com/maps/api/distancematrix/json?origins=" + origin.replace(" ", "") + "&destinations=" + destination.replace(" ", ""));
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        String line, outputString = "";
        BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()));
        while ((line = reader.readLine()) != null) {
            outputString += line;
        }
        System.out.println(outputString);
        String jsonText = outputString;
//String status[]=null;
        String error = "";
        ArrayList finalList = new ArrayList();
        try {
            JSONParser parser = new JSONParser();
            String str1 = "";
            ArrayList ar = new ArrayList();
            ArrayList ar1 = new ArrayList();

            KeyFinder finder = new KeyFinder();
            finder.setMatchKey("status");
            while (!finder.isEnd()) {
                parser.parse(jsonText, finder, true);
                if (finder.isFound()) {
                    finder.setFound(false);
                    System.out.println("RND " + finder.getValue());
                    ar.add(finder.getValue().toString());
                }
            }
            String str[] = new String[ar.size()];
            String splitkm[] = new String[5];
            String traveltime[] = new String[5];
            int indexArr = 0;
            Iterator itr = ar.iterator();
            while (itr.hasNext()) {
                str[indexArr] = itr.next().toString();
                ++indexArr;
                System.out.println(itr.next());
            }
            System.out.println("my 0 position " + str[0]);
            if (str[0].equalsIgnoreCase("OK")) {
                error = "ok";
                JSONParser parser1 = new JSONParser();
                KeyFinder finder1 = new KeyFinder();
                finder1.setMatchKey("text");
                while (!finder1.isEnd()) {
                    parser1.parse(jsonText, finder1, true);
                    if (finder1.isFound()) {
                        finder1.setFound(false);
                        System.out.println("RND " + finder1.getValue());
                        ar1.add(finder1.getValue().toString());
                    }
                }
                System.out.println("size" + ar1.size());
                Iterator it1 = ar1.iterator();
                String forkm[] = new String[ar1.size()];
                int index1 = 0;
                while (it1.hasNext()) {
                    forkm[index1] = it1.next().toString();
                    ++index1;
                }
                System.out.println("fddd" + forkm[0] + "  dfsdf" + forkm[1]);
                splitkm = forkm[0].split(" ");
                traveltime = forkm[1].split(" ");
                System.out.println("finalList" + finalList.size());
                finalList.add(splitkm[0].replace(",", ""));
                System.out.println("lenth of travel time" + traveltime.length);
                for (int j = 0; j < traveltime.length; j++) {
                    System.out.println("data" + traveltime[j]);
                    finalList.add(traveltime[j]);
                    System.out.println("finalList" + finalList.size());
                }
            } else {
                error = "not";
                System.out.println("wrong origin destination" + error);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return finalList;
    }

    public int createRoute(ArrayList googleData, ArrayList country, String vehicleType, String origin, String destination) throws FPRuntimeException, FPBusinessException {

        Map map = new HashMap();
        ArrayList configurationMaster = new ArrayList();
        int routeId = 0;
        try {
            String param[] = new String[6];
            String countryI[] = new String[5];
            int i = 0;
            i = 0;
            map.put("countryId", "2");
            Iterator itr = googleData.iterator();
            while (itr.hasNext()) {
                param[i] = itr.next().toString();
                i++;
            }
            System.out.println("param lenth is " + param.length);
            String val = "", val1 = "";
            int prlength = param.length;
            int totalHrs = 0, totalmint = 0;
            double travelkm = 0.0;
            for (int j = 1; j < param.length; j++) {
                System.out.println("j " + j);
                if (param[j + 1].equalsIgnoreCase("day")) {
                    totalHrs = Integer.parseInt(param[j]) * 24;
                    totalHrs = totalHrs + Integer.parseInt(param[j + 2]);
                    totalmint = 0;
                    //Integer.parseInt(param[j+2])*60;
                } else if (param[j + 1].equalsIgnoreCase("hours")) {
                    totalHrs = Integer.parseInt(param[j]);
                    totalmint = Integer.parseInt(param[j + 2]);
                } else if (param[j + 1].equalsIgnoreCase("mins")) {
                    totalHrs = 0;
                    totalmint = Integer.parseInt(param[j]);
                }
                break;
            }
            travelkm = Double.parseDouble(param[0]);
            System.out.println("totalhrs" + totalHrs + "total minuts" + totalmint + "travel km" + travelkm);

            System.out.println("For source and destination ID" + origin + " " + destination);
            String nextRouteCode = (String) getSqlMapClientTemplate().queryForObject("trip.getNextRouteCode", map);
            System.out.println("next Route Code" + nextRouteCode);
            if (nextRouteCode != null) {
                if (nextRouteCode.length() == 3) {
                    nextRouteCode = "RC" + nextRouteCode;
                } else if (nextRouteCode.length() == 2) {
                    nextRouteCode = "RC0" + nextRouteCode;
                } else {
                    nextRouteCode = "RC00" + nextRouteCode;
                }
            } else {
                nextRouteCode = "RC001";
            }
            System.out.println("next Route Code after concat" + nextRouteCode);
            System.out.println("map for fuel:" + map);
            String fuelprice = (String) getSqlMapClientTemplate().queryForObject("trip.getCurrentFuelPrice", map);
            int flag = 0;
            System.out.println("map for getConfigurationMaster: " + map);
            configurationMaster = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConfigurationMaster", map);
            Iterator itr1 = configurationMaster.iterator();
            TripTO tpTO = new TripTO();
            while (itr1.hasNext()) {
                tpTO = new TripTO();
                tpTO = (TripTO) itr1.next();
                String vehicleExpesne = String.valueOf(Double.parseDouble(tpTO.getFuelCostPerKm()) * travelkm);
                map.put("vehicleTypeId", tpTO.getVehicleTypeId());
                map.put("fuelCostPerKm", tpTO.getFuelCostPerKm());
                map.put("tollCostPerKm", tpTO.getTollCostPerKm());
                map.put("miscCostKm", tpTO.getMiscCostKm());
                map.put("driverIncentive", tpTO.getDriverIncentive());
                map.put("routeCode", nextRouteCode);
                map.put("totalHours", totalHrs);
                map.put("totalMins", totalmint);
                map.put("travelKm", travelkm);
                map.put("roadType", "NH");
                map.put("routeToId", destination);
                map.put("routeFromId", origin);
                map.put("fuelPrice", fuelprice);
                map.put("vehicleExpesne", vehicleExpesne);
                System.out.println("map for route Master:" + map);
                if (flag == 0) {

                    routeId = (Integer) getSqlMapClientTemplate().insert("trip.inserInMaster", map);
                }
                flag = 1;
                int maxid = (Integer) getSqlMapClientTemplate().queryForObject("trip.getlastRouteId", map);
                map.put("routeId", maxid);

                System.out.println("map for route cost:" + map);
                int insertcost = (Integer) getSqlMapClientTemplate().insert("trip.inserInCostMaster", map);
                System.out.println("insertcost" + insertcost);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return routeId;
    }

    public ArrayList getDistanceList(String[] consignmentOrderIds, String[] originId, String pickupPoint) {
        Map map = new HashMap();
        ArrayList distanceList = new ArrayList();
        try {
            calculateDistance cd = new calculateDistance();
            TripTO tripTO = new TripTO();
            String originLatLong = "";
            map.put("consignmentOrderIds", consignmentOrderIds);
            String origins = (String) getSqlMapClientTemplate().queryForObject("trip.getDistinctOrigins", map);
            String[] distinctOrigins = origins.split(",");
            System.out.println("consignmentOrderIds.length...:" + consignmentOrderIds.length);

            System.out.println("originId.length " + originId.length);
            System.out.println("distict originid length" + distinctOrigins.length);

            for (int j = 0; j < distinctOrigins.length; j++) {

                System.out.println("i m in first loop");
                System.out.println("distinctOrigins[j] before loop:" + distinctOrigins[j]);
                System.out.println("pickupPoint before loop:" + pickupPoint);
                if (!distinctOrigins[j].equals(pickupPoint)) {
                    System.out.println("pickupPoint in main loop " + pickupPoint);
                    System.out.println("distict originId in main loop " + distinctOrigins[j]);
                    double lat1 = 0.00;
                    double long1 = 0.00;
                    double lat2 = 0.00;
                    double long2 = 0.00;
                    double distance = 0.00;
                    int cntr = 0;
                    String temp1 = "";
                    String temp2 = "";
                    tripTO = new TripTO();
                    map.put("origin", pickupPoint);
                    originLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
                    if (!"".equals(originLatLong) && originLatLong != null) {
                        String temp[] = originLatLong.split("~");
                        lat1 = Double.parseDouble(temp[0]);
                        long1 = Double.parseDouble(temp[1]);
                    }
                    for (int i = 0; i < distinctOrigins.length; i++) {
                        System.out.println("i m in second loop");
                        String orgin = "";
                        if (!distinctOrigins[i].equals(pickupPoint)) {
                            orgin = distinctOrigins[i];
                            System.out.println("orgin " + orgin);
                            System.out.println("originId[i] " + distinctOrigins[i]);
                            System.out.println("pickupPoint " + pickupPoint);
                            map.put("origin", distinctOrigins[i]);
                            System.out.println("inside ids map" + map);
                            String destinationLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
                            if (!"".equals(destinationLatLong) && destinationLatLong != null) {
                                String temp[] = destinationLatLong.split("~");
                                lat2 = Double.parseDouble(temp[0]);
                                long2 = Double.parseDouble(temp[1]);
                            }
                            distance = cd.distance(lat1, long1, lat2, long2, 'K');
                            if (cntr == 0) {
                                temp1 = String.valueOf(distance);
                                temp2 = distinctOrigins[i];
                                System.out.println("distance 111..:" + distance);
                            } else {
                                temp1 = temp1 + "~" + String.valueOf(distance);
                                temp2 = temp2 + "~" + distinctOrigins[i];
                                System.out.println("distance array :" + temp1);
                            }
                            cntr++;
                        }

                    }
                    System.out.println("distinctOrigins 111..lat2:" + lat2);
                    System.out.println("distinctOrigins 111..lat1:" + lat1);
                    System.out.println("distinctOrigins 111..long2:" + long2);
                    System.out.println("distinctOrigins 111..long1:" + long1);

                    String[] tempVal = null;
                    tempVal = temp1.split("~");
                    String[] tempVal2 = temp2.split("~");
                    double smallValue = Double.parseDouble(tempVal[0]);
                    System.out.println("tempVal.length..:" + tempVal.length);
                    System.out.println("tempVal2.length..:" + tempVal2.length);
                    for (int i = 0; i < tempVal.length; i++) {
                        if (tempVal.length > 1) {
                            if (Double.parseDouble(tempVal[i]) < smallValue) {
                                smallValue = Double.parseDouble(tempVal[i]);
                                pickupPoint = tempVal2[i];

                                System.out.println("Shortest Distance:" + smallValue);
                                System.out.println("nearest Point:" + pickupPoint);
                                System.out.println("new pickup point :" + pickupPoint);
                            }
                        } else if (Double.parseDouble(tempVal[i]) == smallValue) {
                            smallValue = Double.parseDouble(tempVal[i]);
                            pickupPoint = tempVal2[i];
                            System.out.println("nearest Point else:" + pickupPoint);
                            System.out.println("shortest lenght else:" + smallValue);
                            System.out.println("new pickup point else:" + pickupPoint);
                        }
                    }
                    map.put("origin", pickupPoint);
                    System.out.println("map of nearest Point:" + map);
                    originLatLong = (String) getSqlMapClientTemplate().queryForObject("operation.getOriginLatLong", map);
                    if (!"".equals(originLatLong) && originLatLong != null) {
                        String temp[] = originLatLong.split("~");
                        lat1 = Double.parseDouble(temp[0]);
                        long1 = Double.parseDouble(temp[1]);
                    }
                    tripTO.setOriginId(pickupPoint);
                    tripTO.setDistance(String.valueOf(smallValue));
                    tripTO.setLatitude(String.valueOf(lat1));
                    tripTO.setLongitude(String.valueOf(long1));
                    distanceList.add(tripTO);
                    System.out.println("tripTO.pickupPoint" + pickupPoint);
                    System.out.println("tripTO.setOriginId" + tripTO.getOriginId());
                    System.out.println("tripTO.setDistance" + tripTO.getDistance());
                    System.out.println("tripTO.setLatitude" + tripTO.getLatitude());
                    System.out.println("tripTO.setLongitude" + tripTO.getLongitude());

                }

            }

            System.out.println("map: in the dao" + map);
            System.out.println("distanceList size=" + distanceList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDistanceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDistanceList List", sqlException);
        }

        return distanceList;
    }

    public String getRouteExpenseDetailsForVehicleType(String totalkm, String vehicleTypeId, String vendorId,
            String sourceId, String destinationId, String ownership, String noOfContianer, String movementType,
            String point1Id, String point2Id,
            String vehicleCategory, String productInfo,
            String containerTypeId, String companyId, String vehicleNo, String routeContractId, String orderTripType, String billingType, String containerTonnage) {
        Map map = new HashMap();
        String returnValue = "";
        try {
            String expense1 = "";
            map.put("countryId", "2");
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("billingType", billingType);
            map.put("containerWeight", containerTonnage);
            map.put("totalkm", totalkm);
            map.put("vendorId", vendorId);
            map.put("sourceId", sourceId);
            map.put("point1Id", point1Id);
            map.put("point2Id", point2Id);
            map.put("point3Id", "");
            map.put("point4Id", "");
            map.put("firstPointId", point1Id);
            map.put("destinationId", destinationId);
            map.put("ownership", ownership);
            map.put("noOfConatiner", noOfContianer);
            map.put("movementType", movementType);
            map.put("productInfo", productInfo);
            map.put("companyId", companyId);
            map.put("vehicleNo", vehicleNo);

            map.put("firstPointId", sourceId);
            map.put("finalPointId", destinationId);
            map.put("containerQty", noOfContianer);
            map.put("containerTypeId", containerTypeId);
            map.put("contractId", routeContractId);
            map.put("orderTripType", orderTripType);
            System.out.println("map iss :::" + map);
            String totalFuel = "";

            if (movementType.equals("1") || movementType.equals("2") || movementType.equals("17") || movementType.equals("18")
                    || movementType.equals("19") || movementType.equals("20") || movementType.equals("21") || movementType.equals("23")
                    || movementType.equals("24") || movementType.equals("25") || movementType.equals("26")
                    || movementType.equals("27") || movementType.equals("28")
                    || movementType.equals("29") || movementType.equals("30")
                    || movementType.equals("31") || movementType.equals("32")
                    || movementType.equals("33") || movementType.equals("34")
                    || movementType.equals("35") || movementType.equals("36")) { //if mofussil
//            if (movementType.equals("12") || movementType.equals("13") || movementType.equals("14") || movementType.equals("15") || movementType.equals("16")) { //if mofussil
                if (!"3".equals(ownership) || "1".equals(vehicleCategory)) {
                    if ("2".equals(orderTripType)) {//round trip
                        map.put("destinationId", point1Id);
                    }
                    System.out.println("map:" + map);
                    returnValue = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleExpenseForRoute", map);
                    System.out.println("returnValue:" + returnValue);
                    if (returnValue != null) {
                        String[] tempRetValue = returnValue.split("~");
                        returnValue = tempRetValue[10] + "~" + tempRetValue[9] + "~" + tempRetValue[0] + "~0.00" + "~" + tempRetValue[2]
                                + "~" + tempRetValue[3] + "~" + tempRetValue[4] + "~" + tempRetValue[5] + "~" + tempRetValue[6];
                        totalFuel = tempRetValue[0] + "~" + tempRetValue[1];
                        returnValue = "1#" + totalFuel + "@" + returnValue;
                    } else {
                        returnValue = "1#" + "0" + "~0.00@" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0";
                    }
                } else {
                    System.out.println("else....");
                    System.out.println(" map is::" + map);
                    if ("1".equals(containerTypeId) && "1059".equals(vehicleTypeId) && "1".equals(noOfContianer)) {
                        map.put("containerTypeId", "2");
                    } else if ("1".equals(containerTypeId) && "1060".equals(vehicleTypeId) && "1".equals(noOfContianer)) {
                        map.put("containerTypeId", "2");
                    }
                    int loadType = Integer.parseInt(movementType);
                    if (loadType == 3) {
                        //            if (loadType == 3 || loadType == 9 || loadType == 11) {
                        //            if (loadType == 3 || loadType == 9) {
                        map.put("movementType", "1");
                        map.put("loadTypeId", "1");
                    } else {
                        map.put("movementType", "2");
                        map.put("loadTypeId", "2");
                    }

                    System.out.println("expense1 map is =" + expense1);
                    System.out.println("expense1 map is dsdsds=" + map);
                    expense1 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseForVendorVehicle", map);
                    System.out.println("expense1 " + expense1);
                    if (expense1 == null) {
                        expense1 = "0~0~1~0~0~0~0~0";
                    }
                    System.out.println("returnValue---" + returnValue);
                    if ("".equals(returnValue)) {
                        returnValue = "1#0";
                    }
                    returnValue = returnValue + "-" + expense1;
                    System.out.println("returnVasasasasasalue---" + returnValue);
                }

            } else { //other than moffusil

                int loadType = Integer.parseInt(movementType);
                if ("1".equals(containerTypeId) && "1059".equals(vehicleTypeId) && "1".equals(noOfContianer)) {
                    map.put("containerTypeId", "2");
                }
                if (loadType == 3) {
                    //            if (loadType == 3 || loadType == 9 || loadType == 11) {
                    //            if (loadType == 3 || loadType == 9) {
                    map.put("movementType", "1");
                    map.put("loadTypeId", "1");
                } else {
                    map.put("movementType", "2");
                    map.put("loadTypeId", "2");
                }

                System.out.println(" map is::" + map);
                returnValue = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleExpense", map);
                if (returnValue == null) {
                    returnValue = "0";
                }
                System.out.println("expenseexpense " + returnValue);
                String approvalStatus = "";
                System.out.println("routeContractId:" + routeContractId);
                if (routeContractId.contains(",")) {
                    if ("1".equals(containerTypeId) && "1059".equals(vehicleTypeId) && "1".equals(noOfContianer)) {
                        map.put("containerTypeId", "1");
                        map.put("noOfContianer", "1");
                        map.put("vehicleTypeId", "1058");
                    }

                    Double rateWithReefer = 0.00;
                    Double rateWithoutReefer = 0.00;
                    String routeContractIdTemp[] = routeContractId.split(",");
                    for (int i = 0; i < routeContractIdTemp.length; i++) {
                        map.put("contractId", routeContractIdTemp[i]);
                        //                    map.put("containerQty", "1");
                        //                    map.put("vehicleTypeId", "1058");
                        System.out.println(" map is::" + map);

                        approvalStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeContainerTypeRates1", map);
                        System.out.println("approvalStatus:" + approvalStatus);
                        if (approvalStatus != null) {

                            String temp[] = approvalStatus.split("@");
                            approvalStatus = temp[1];
                            String temp1[] = temp[0].split("~");
                            rateWithReefer = rateWithReefer + Double.parseDouble(temp1[0]);
                            rateWithoutReefer = rateWithoutReefer + Double.parseDouble(temp1[1]);
                            approvalStatus = "1";
                            System.out.println("returnValueo" + temp[0]);
                            System.out.println("returnValue1" + temp[1]);
                        }
                        if (approvalStatus == null) {
                            returnValue = returnValue;
                            approvalStatus = null;
                            break;
                        }
                    }
                    if (approvalStatus == null) {
                        returnValue = returnValue;
                    } else {
                        returnValue = String.valueOf(rateWithReefer) + "~" + String.valueOf(rateWithoutReefer) + "~" + returnValue;
                        System.out.println(" Revenue" + returnValue);
                    }
                    System.out.println("returnValue111:" + returnValue);
                } else {
                    map.put("contractId", routeContractId);
                    map.put("containerQty", noOfContianer);
                    map.put("vehicleTypeId", vehicleTypeId);
                    if ("1".equals(containerTypeId) && "1059".equals(vehicleTypeId) && "1".equals(noOfContianer)) {
                        map.put("containerTypeId", "1");
                        map.put("noOfContianer", "1");
                        map.put("vehicleTypeId", "1058");
                    }

                    approvalStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeContainerTypeRates1", map);
                    if (approvalStatus != null) {
                        String temp[] = approvalStatus.split("@");
                        approvalStatus = temp[1];
                        returnValue = temp[0] + "~" + returnValue;
                        System.out.println("returnValueo" + temp[0]);
                        System.out.println("returnValue1" + temp[1]);
                    }
                }

                if ("1".equals(containerTypeId) && "1059".equals(vehicleTypeId) && "1".equals(noOfContianer)) {
                    map.put("vehicleTypeId", "1059");
                }

                map.put("containerQty", noOfContianer);
                map.put("vehicleTypeId", vehicleTypeId);

                System.out.println("returnValue" + returnValue);

                totalFuel = (String) getSqlMapClientTemplate().queryForObject("trip.getTripFuelForVehicle", map);
                System.out.println("totalFuel " + totalFuel);
                if (totalFuel == null) {
                    totalFuel = "0.00~0.00";
                }
                System.out.println("totalFuel = " + totalFuel);

                returnValue = approvalStatus + "#" + totalFuel + "@" + returnValue;

            }

//            if (!"3".equals(ownership) || "1".equals(vehicleCategory)) {
//
//            } else {
//                System.out.println("else....");
//                System.out.println(" map is::" + map);
//                if ("1".equals(containerTypeId) && "1059".equals(vehicleTypeId) && "1".equals(noOfContianer)) {
//                    map.put("containerTypeId", "2");
//                }
//                System.out.println("expense1 map is =" + expense1);
//                expense1 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseForVendorVehicle", map);
//                System.out.println("expense1 " + expense1);
//                if (expense1 == null) {
//                    expense1 = "0.00";
//                }
//
//                returnValue = returnValue + "-" + expense1;
//
//            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return returnValue;
    }

    public String getDistanceRouteExpenseDetailsForVehicleType(String vehicleTypeId, String vendorId,
            String fromDistance, String toDistance, String ownership, String noOfContianer, String movementType,
            String vehicleCategory,
            String containerTypeId, String companyId, String vehicleId, String contractId) {
        Map map = new HashMap();
        String returnValue = "";
        try {
            String expense1 = "";
            map.put("countryId", "2");
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("fromDistance", fromDistance);
            map.put("toDistance", toDistance);

            map.put("vendorId", vendorId);

            map.put("ownership", ownership);
            map.put("noOfConatiner", noOfContianer);
            map.put("movementType", movementType);
            // map.put("productInfo", productInfo);
            map.put("companyId", companyId);
            map.put("vehicleId", vehicleId);

            map.put("containerQty", noOfContianer);
            map.put("containerTypeId", containerTypeId);
            map.put("contractId", contractId);

            int loadType = Integer.parseInt(movementType);

            if (loadType == 3) {
                map.put("movementType", "1");
                map.put("loadTypeId", "1");
            } else {
                map.put("movementType", "2");
                map.put("loadTypeId", "2");
            }

            System.out.println(" map is::" + map);
            returnValue = (String) getSqlMapClientTemplate().queryForObject("trip.getDistanceVehicleExpense", map);
            if (returnValue == null) {
                returnValue = "0";
            }
            String approvalStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getDistanceVehicleTypeContainerTypeRates1", map);
            if (approvalStatus != null) {
                String temp[] = approvalStatus.split("@");
                approvalStatus = temp[1];
                returnValue = temp[0] + "~" + returnValue;
                System.out.println("returnValueo" + temp[0]);
                System.out.println("returnValue1" + temp[1]);
            }

            System.out.println("returnValue" + returnValue);

            String totalFuel = (String) getSqlMapClientTemplate().queryForObject("trip.getDistanceTripFuelForVehicle", map);
            System.out.println("totalFuel " + totalFuel);
            if (totalFuel == null) {
                totalFuel = "0.00~0.00";
            }
            System.out.println("totalFuel = " + totalFuel);

            returnValue = approvalStatus + "#" + totalFuel + "@" + returnValue;

            if (!"3".equals(ownership) || "1".equals(vehicleCategory)) {

            } else {
                System.out.println("else....");
                System.out.println(" map is::" + map);
                expense1 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseForDistanceVendorVehicle", map);
                System.out.println("expense1 " + expense1);
                if (expense1 == null) {
                    expense1 = "0.00";
                }

                returnValue = returnValue + "-" + expense1;

            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return returnValue;
    }

    public int saveTripVehicleForVehicleDeattach(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date curDate = new Date();
        System.out.println(dateFormat.format(curDate));
        String updateDate = dateFormat.format(curDate);

        try {
            map.put("tripId", tripTO.getTripId());
            map.put("startKM", tripTO.getVehicleStartKm());
            map.put("endKM", tripTO.getLastVehicleEndKm());
            map.put("startHM", tripTO.getVehicleStartOdometer());
            map.put("endHM", tripTO.getLastVehicleEndOdometer());
            map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            map.put("remarks", tripTO.getVehicleRemarks());
            map.put("cityId", tripTO.getCityId());

            map.put("tripSheetId", tripTO.getTripId());
            map.put("statusId", "23");
            map.put("updateDate", updateDate);
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripSheetForVehicleDeattach", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripVehicleForVehicleDeattach", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForVehicleDeattach", map);
            int maxVehicleSequence = 0;
            int updateVehicleSequence = 0;
            //  maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            //  map.put("vehicleSequence", maxVehicleSequence);
            //   updateVehicleSequence = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            System.out.println("saveTripVehicle=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTripVehicleForVehicleAttach(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date curDate = new Date();
        System.out.println(dateFormat.format(curDate));
        String updateDate = dateFormat.format(curDate);
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("startKM", tripTO.getVehicleStartKm());
            map.put("endKM", tripTO.getLastVehicleEndKm());
            map.put("startHM", tripTO.getVehicleStartOdometer());
            map.put("endHM", tripTO.getLastVehicleEndOdometer());
            map.put("vehicleChangeDate", tripTO.getVehicleChangeDate());
            map.put("vehicleChangeTime", tripTO.getVehicleChangeHour() + ":" + tripTO.getVehicleChangeMinute() + ":00");
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
            map.put("remarks", tripTO.getVehicleRemarks());
            map.put("cityId", tripTO.getCityId());
            map.put("destinationId", tripTO.getDestinationId());
            map.put("originId", tripTO.getOriginId());
            map.put("expectedArrivalDate", tripTO.getTripPlanEndDate());
            map.put("expectedArrivalTime", tripTO.getTripPlanEndTime());

            map.put("tripSheetId", tripTO.getTripId());
            map.put("statusId", "10");
            map.put("updateDate", updateDate);
            System.out.println("map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripSheetForVehicleAttach", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripVehicleForVehicleAttach", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityForVehicleAttach", map);
            int maxVehicleSequence = 0;
            int updateVehicleSequence = 0;
            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            map.put("vehicleSequence", maxVehicleSequence);
            updateVehicleSequence = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            System.out.println("saveTripVehicle=" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTrailerDetails(String[] trailerId, String[] trailerRemarks, String[] containerTypeId, String[] containerNo, String[] uniqueId, String orderId[], String[] tonnage, String[] sealNo, TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("originId", tripTO.getOriginId());
        //update trailer availability
        String[] pointPlanDate = tripTO.getPointPlanDate();
        String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
        String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
        if (pointPlanDate != null) {
            map.put("tripScheduleDate", pointPlanDate[0]);
        } else {
            map.put("tripScheduleDate", "00-00-0000");
        }
        if (pointPlanTimeHrs != null) {
            map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0] + ":" + "00");
        } else {
            map.put("tripScheduleTime", "00" + ":" + "00" + ":" + "00");
        }
        map.put("tripPlannedEndDate", tripTO.getTripPlanEndDate());
        map.put("tripPlannedEndTime", tripTO.getTripPlanEndTime());
        //

        System.out.println("the pretripsheetdetails" + map);
        int status = 0;
        try {
            if (containerTypeId != null) {
                for (int i = 0; i < containerTypeId.length; i++) {
                    map.put("containerTypeId", containerTypeId[i]);
                    if (tonnage[i] == null || "".equals(tonnage[i])) {
                        map.put("tonnage", 0);
                    } else {
                        map.put("tonnage", tonnage[i]);
                    }
                    if (sealNo[i] == null || "".equals(sealNo[i])) {
                        map.put("sealNo", "NA");
                    } else {
                        map.put("sealNo", sealNo[i]);
                    }
                    System.out.println("containerTypeId[i]" + containerTypeId[i]);
                    map.put("containerNo", containerNo[i]);
                    if ("1".equals(containerTypeId[i]) || "2".equals(containerTypeId[i])) {
                        map.put("consignmentContainerId", uniqueId[i]);
                        map.put("consignmentOrderId", orderId[i]);
                    } else {
                        map.put("consignmentContainerId", "0");
                        map.put("consignmentOrderId", "0");
                    }

                    System.out.println("the container map" + map);
                    status = (Integer) session.insert("trip.insertContainerDetails", map);

                    System.out.println("container Status..." + status);

                }
                if ("1".equalsIgnoreCase(tripTO.getMovementType()) || "2".equalsIgnoreCase(tripTO.getMovementType()) || "17".equalsIgnoreCase(tripTO.getMovementType())
                        || "18".equalsIgnoreCase(tripTO.getMovementType()) || "19".equalsIgnoreCase(tripTO.getMovementType())
                        || "27".equalsIgnoreCase(tripTO.getMovementType()) || "28".equalsIgnoreCase(tripTO.getMovementType())
                        || "20".equalsIgnoreCase(tripTO.getMovementType())) {
                    map.put("consignmentOrderId", status);
                    status = (Integer) session.update("trip.getLRNumberSequence", map);
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public int saveTripConsignmentsNew(String[] trailerId, String[] trailerRemarks, String[] containerTypeId,
            String[] containerNo, String[] uniqueId, String orderId[], TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("orderSequence", 1);
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("userId", tripTO.getUserId());
        map.put("consignmentStatusId", 10);

        int status = 0;
        try {

            for (int i = 0; i < containerTypeId.length; i++) {
                map.put("consignmentOrderId", orderId[i]);
                System.out.println("the saveTripConsignmentsNew map" + map);
                status = (Integer) session.update("trip.saveTripConsignments", map);
                System.out.println("saveTripConsignments new Status..." + status);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public int saveTrailerDetails(String[] trailerId, String[] trailerRemarks, String[] containerTypeId, String[] containerNo, String[] uniqueId, TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("originId", tripTO.getOriginId());
        //update trailer availability
        String[] pointPlanDate = tripTO.getPointPlanDate();
        String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
        String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
        //map.put("pointPlanDate", pointPlanDate[0]);
        //map.put("pointPlanTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);

//            map.put("tripScheduleDate", tripTO.getTripScheduleDate());
//            map.put("tripScheduleTime", tripTO.getTripScheduleTime());
        if (pointPlanDate != null) {
            map.put("tripScheduleDate", pointPlanDate[0]);
        } else {
            map.put("tripScheduleDate", "00-00-0000");
        }
        if (pointPlanTimeHrs != null) {
            map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0] + ":" + "00");
        } else {
            map.put("tripScheduleTime", "00" + ":" + "00" + ":" + "00");
        }
        map.put("tripPlannedEndDate", tripTO.getTripPlanEndDate());
        map.put("tripPlannedEndTime", tripTO.getTripPlanEndTime());
        //

        System.out.println("the pretripsheetdetails" + map);
        int status = 0;
        try {

//                System.out.println("trailerId.length:"+trailerId.length);
//                for(int i = 0; i<trailerId.length;i++ ){
//                   map.put("trailerId",trailerId[i]) ;
//                   map.put("trailerRemarks",trailerRemarks[i]) ;
//                    System.out.println("trailerInsert map:"+map);
//                   status = (Integer) getSqlMapClientTemplate().update("trip.insertTrailerDetails", map);
//                    int traileAvailUpdate=(Integer) getSqlMapClientTemplate().update("trip.updateTrailerAvailabiltyForFreeze", map);
//                   System.out.println("traileAvailUpdate = " + traileAvailUpdate);
//                }
            for (int i = 0; i < containerTypeId.length; i++) {
                map.put("containerTypeId", containerTypeId[i]);
                map.put("containerNo", containerNo[i]);
                map.put("consignmentContainerId", uniqueId[i]);
                System.out.println("the container map" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertContainerDetails", map);
                System.out.println("container Status..." + status);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public ArrayList getcontainerType(TripTO tripTO) {
        Map map = new HashMap();
        // String vehicleNo = tripTO.getVehicleNo();
        ArrayList container = new ArrayList();
        try {
            //   System.out.println("map: in the dao" + map);
            container = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getcontainerType", map);
            System.out.println("container size=" + container.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("containerType Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return container;
    }

    public ArrayList getTrailerList() throws FPRuntimeException, FPBusinessException {
        ArrayList trailerList = null;
        Map map = new HashMap();
        try {
            trailerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTrailerList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return trailerList;
    }

    public ArrayList getContainerList() throws FPRuntimeException, FPBusinessException {
        ArrayList containerList = null;
        Map map = new HashMap();
        try {
            containerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerList", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return containerList;
    }

    public ArrayList getContainerTypeDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList containerTypeDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            containerTypeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerTypeDetails", map);
            System.out.println("getContainerTypeDetails.size() = " + containerTypeDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return containerTypeDetails;
    }

//  update order efs
//public void updateOrderStatusToEFS(String tripId, String tripStatusId) {
//
//        Map map = new HashMap();
//        try {
//            String orderStatus = "", driverName = "", orderGPSTrackingLocation = "", orderNo = "", pickupDate = "", tripCode = "", tripPlannedStartDateTime = "", tripStartDateTime = "", tripPlannedEndDateTime = "",
//                    tripEndDateTime = "", vehicleNo = "",wayBillNo="";
//            map.put("tripId", tripId);
//            map.put("statusId", tripStatusId);
//            System.out.println("map:::::"+map);
//            String  orderReferenceNoTemp[]=null;
//            String orderReferenceNo = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderReferenceNoForTripId", map);
//            if(!"".equals(orderReferenceNo)){
//             orderReferenceNoTemp=orderReferenceNo.split(",");
//            }
//            System.out.println("orderReferenceNoTemp.length"+orderReferenceNoTemp.length);
//            for(int i=0;i<orderReferenceNoTemp.length;i++){
//            map.put("orderReferenceNo", orderReferenceNoTemp[i]);
//
//
//            // String tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getStausName", map);
//            System.out.println("map::::" + map);
//            ArrayList orderDeatils = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderDetails", map);
//            Iterator itr1 = orderDeatils.iterator();
//            TripTO tpTO = new TripTO();
//            while (itr1.hasNext()) {
//                tpTO = new TripTO();
//                tpTO = (TripTO) itr1.next();
//                orderStatus = tpTO.getStatusName();
//                driverName = tpTO.getDriverName();
//                orderGPSTrackingLocation = tpTO.getOrderGPSTrackingLocation();
//                orderNo = tpTO.getCustomerOrderReferenceNo();
//                pickupDate = tpTO.getLoadingDate();
//                tripCode = tpTO.getTripCode();
//                tripPlannedStartDateTime = tpTO.getPlannedPickupDate();
//                tripStartDateTime = tpTO.getPickupDate();
//                tripPlannedEndDateTime = tpTO.getDeliveryDate();
//                tripEndDateTime = tpTO.getDeliveryDate();
//                vehicleNo = tpTO.getVehicleNo();
//                wayBillNo=tpTO.getWayBillNo();
//                System.out.println("orderStatus" + orderStatus + "driverName" + driverName + "orderGPSTrackingLocation" + orderGPSTrackingLocation);
//                System.out.println("orderNo" + orderNo + "pickupDate" + pickupDate + "tripCode" + tripCode + "tripPlannedStartDateTime" + tripPlannedStartDateTime);
//                System.out.println("tripStartDateTime" + tripStartDateTime + "tripPlannedEndDateTime" + tripPlannedEndDateTime + "tripEndDateTime" + tripEndDateTime + "vehicleNo" + vehicleNo+"wayBillNo:"+wayBillNo);
//            }
//            // TODO code application logic here
//
//            Service srv = new Service();
//            ServiceSoap srvs = srv.getServiceSoap();
//            Date dNow = new Date();
//            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy kk:mm:ss");
//            String today = ft.format(dNow);
//
//            //UpdateOrderStatusResult result = srvs.updateOrderStatus(orderReferenceNo, tripStatusId, "04-05-2015 21:12:12");
//
//
//        //    String[] temp = orderReferenceNo.split(",");
//            UpdateOrderStatusResult result = null;
//        //    for (int i = 0; i < temp.length; i++) {
//             //   System.out.println(temp[i]);
//                result = srvs.updateOrderStatus(orderStatus, driverName, orderGPSTrackingLocation, orderNo, pickupDate, tripCode, tripPlannedStartDateTime, tripStartDateTime, tripPlannedEndDateTime, tripEndDateTime, vehicleNo,wayBillNo);
//                System.out.println("result.getContent() = " + result.getContent());
//                List resultList = result.getContent();
//                for (int k = 0; k < resultList.size(); k++) {
//                    System.out.println(resultList.get(k));
//                }
//            }
//          //  }
//
//
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            sqlException.printStackTrace();
//            FPLogUtils.fpDebugLog("updateOrderStatusToEFS Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            //throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOrderStatusToEFS List", sqlException);
//        }
//    }
//// update trip closer data
// public void updateTripCloserToEFS(String tripId) {
//
//        Map map = new HashMap();
//        try {
//            String orderno[] = null, accountentrydate[] = null, partycode[] = null, partytype[] = null, chargecode[] = null, accountstype[] = null, chargeamount[] = null, narration[] = null, jobcardtype[] = null;
//            String orderReferenceNoTemp[]=null;
//
//            map.put("tripId", tripId);
//
//            String orderReferenceNo = (String) getSqlMapClientTemplate().queryForObject("trip.getOrderReferenceNoForTripId", map);
//            // String tripStatus = (String) getSqlMapClientTemplate().queryForObject("trip.getStausName", map);
//            if(!"".equals(orderReferenceNo)){
//             orderReferenceNoTemp=orderReferenceNo.split(",");
//            }
//            System.out.println("orderReferenceNoTemp.length"+orderReferenceNoTemp.length);
//            int weightFactor=orderReferenceNoTemp.length;
//            for(int i=0;i<orderReferenceNoTemp.length;i++){
//            map.put("orderReferenceNo", orderReferenceNoTemp[i]);
//            map.put("weightFactor", weightFactor);
//           // map.put("orderReferenceNo", orderReferenceNo);
//            System.out.println("map::::" + map);
//            ArrayList tripAccountDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripAccountDetails", map);
//            ArrayOfTMSModel ar = new ArrayOfTMSModel();
//         //   ar.getTMSModel().removeAll(tripAccountDetails);
//            ar.getTMSModel().addAll(tripAccountDetails);
//            System.out.println("tmsmodel Size:"+ar.getTMSModel().size()) ;
//              Iterator itr1 = tripAccountDetails.iterator();
//            TMSModel tpTO = new TMSModel();
//            while (itr1.hasNext()) {
//                tpTO = new TMSModel();
//                tpTO = (TMSModel) itr1.next();
//               System.out.println("tpTO.getOrder()"+tpTO.getOrderno());
//               System.out.println("tpTO.getAccountentrydate()"+tpTO.getAccountentrydate());
//               System.out.println("tpTO.getAccountstype()"+tpTO.getAccountstype());
//               System.out.println("tpTO.getPartycode()"+tpTO.getPartycode());
//               System.out.println("tpTO.getPartytype()"+tpTO.getPartytype());
//               System.out.println("tpTO.getNarration()"+tpTO.getNarration());
//               System.out.println("tpTO.getChargecode()"+tpTO.getChargecode());
//               System.out.println("tpTO.getChargeamount()"+tpTO.getChargeamount());
//               System.out.println("tpTO.getJobcardtype()"+tpTO.getJobcardtype());
//               System.out.println("tpTO.getExpensetype()"+tpTO.getExpensetype());
//
//          }
//            //int i = 0;
//
////          ArrayList Acc=new ArrayList();
////
////                partycode[i]=tpTO.getEmpCode();
////                partytype[i]="E";
////                chargecode[i]=tpTO.getChargeCode();
////                accountstype[i]=tpTO.getAccountsType();
////                chargeamount[i]=tpTO.getAccountsAmount();
////                narration[i]=tpTO.getNarration();
////                jobcardtype[i]="";
////                System.out.println("accountentrydate"+accountentrydate+"partycode"+partycode+"chargecode"+chargecode+"accountstype"+accountstype+"chargeamount"+chargeamount+"narration"+narration);
////              i++;
////            }
////             // TODO code application logic here
//            Service srv = new Service();
//            ServiceSoap srvs = srv.getServiceSoap();
//            //UpdateOrderStatusResult result = srvs.updateOrderStatus(orderReferenceNo, tripStatusId, "04-05-2015 21:12:12");
//            String[] temp = orderReferenceNo.split(",");
//            CreateTMSAccountDetailResult result = null;
//           // for (int j = 0; j < temp.length; j++) {
//             //   System.out.println(temp[j]);
//                result = srvs.createTMSAccountDetail(ar);
//                System.out.println("result.getContent() = " + result.getContent());
//                List resultList = result.getContent();
//                for (int k = 0; k < resultList.size(); k++) {
//                    System.out.println(resultList.get(k));
//                }
//            }
//           // }
//
//
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            sqlException.printStackTrace();
//            FPLogUtils.fpDebugLog("updateOrderStatusToEFS Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            //throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOrderStatusToEFS List", sqlException);
//        }
//    }
//
    public ArrayList getVendorList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList result = new ArrayList();
        map.put("companyId", tripTO.getCompanyId());
        try {
            try {
                System.out.println("i am here ");
                map.put("companyId", tripTO.getCompanyId());
                result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVendorList", map);
                System.out.println("result=" + result.size());
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVendorList", sqlException);
        }
        return result;
    }

    public ArrayList getVendorVehicleType(TripTO tripTO) {
        Map map = new HashMap();
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("vendorId", tripTO.getVendorId());
        if ("1".equals(tripTO.getVendorId())) {
            map.put("ownerShip", 1);
        } else {
            map.put("ownerShip", "");
        }
        System.out.println("map = " + map);
        ArrayList vehicleTypeList = new ArrayList();
        try {
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVendorVehicleType", map);
            System.out.println("vehicleTypeList size=" + vehicleTypeList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleTypeList List", sqlException);
        }

        return vehicleTypeList;
    }

    public String getVehicleOwnerType(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", tripTO.getVehicleId());
        }
        System.out.println("map---------" + map);
        // ArrayList tripDetails = new ArrayList();
        String vehicleOwnerType = "";
        try {
            if (!"0".equals(tripTO.getVehicleId()) && !"".equals(tripTO.getVehicleId()) && tripTO.getVehicleId() != null) {
                vehicleOwnerType = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleOwnerType", map);
                System.out.println("getOtherExpenseDetails size=" + vehicleOwnerType.length());
            } else {
                vehicleOwnerType = "3";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOtherExpenseDetails List", sqlException);
        }

        return vehicleOwnerType;
    }

    public int updateStartTripSheet(TripTO tripTO, int userId) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        Map map2 = new HashMap();

        map.put("userId", userId);
        map.put("planStartTime", tripTO.getPlanStartHour() + ":" + tripTO.getPlanStartMinute() + ":00");
        map.put("planStartDate", tripTO.getPlanStartDate());
        map.put("startDate", tripTO.getStartDate());
        map.put("startTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute() + ":00");
        map.put("durationTime", tripTO.getTripStartHour() + ":" + tripTO.getTripStartMinute());
        map.put("startOdometerReading", tripTO.getStartOdometerReading());
        map.put("startHM", tripTO.getStartHM());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("startTripRemarks", tripTO.getStartTripRemarks());
        map.put("statusId", tripTO.getStatusId());
        map.put("tripPlanEndDate", tripTO.getTripPlanEndDate());
        map.put("tripPlanEndTime", tripTO.getTripPlanEndTime());
        map.put("lrNumber", tripTO.getLrNumber());
        map.put("vehicleactreportdate", tripTO.getVehicleactreportdate());
        map.put("vehicleactreporttime", tripTO.getVehicleactreporthour() + ":" + tripTO.getVehicleactreportmin() + ":00");
        map.put("vehicleloadreportdate", tripTO.getVehicleloadreportdate());
        map.put("vehicleloadreporttime", tripTO.getVehicleloadreporthour() + ":" + tripTO.getVehicleloadreportmin() + ":00");
        map.put("vehicleloadtemperature", tripTO.getVehicleloadtemperature());
        map.put("startTrailerKM", tripTO.getStartTrailerKM());

        map.put("regNo", tripTO.getRegNo());
        map1.put("tripId", tripTO.getTripId());
        map2.put("userId", userId);
        map2.put("tripId", tripTO.getTripId());
        map2.put("gpsVendor", tripTO.getGpsVendor());
        map2.put("gpsDeviceId", tripTO.getGpsDeviceId());

        System.out.println("the updatetripsheetdetails" + map);
        int status = 0;
        int update = 0;
        String ConsignmentIdList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignment", map);
        System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
        String[] ConsignmentId = ConsignmentIdList.split(",");
        for (int i = 0; i < ConsignmentId.length; i++) {
            map.put("consignmentId", ConsignmentId[i]);
            map.put("consignmentOrderId", ConsignmentId[i]);
            String ConsignmentDetailsList = (String) getSqlMapClientTemplate().queryForObject("trip.getTripConsignmentOrderType", map);

            String[] ConsignmentDetails = ConsignmentDetailsList.split("~");
            if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("23")) {
                map.put("consignmentStatus", "24");
            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("26")) {
                map.put("consignmentStatus", "27");
            } else if (ConsignmentDetails[0].equals("2") && ConsignmentDetails[1].equals("29")) {
                map.put("consignmentStatus", "30");
            } else {
                map.put("consignmentStatus", "10");
            }
            System.out.println("the updated map:" + map);

            update = (Integer) getSqlMapClientTemplate().update("trip.updateStartconsignmentSheet", map);
            System.out.println("the update:" + update);
            int plannedstatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.getPlannedContainer", map);
            System.out.println("plannedstatus==" + plannedstatus);

            if (plannedstatus > 0) {
                map.put("consignmentStatus", "5");
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentStatus", map);
            }

//            if (plannedstatus == 0) {
//            }
            System.out.println("the status1:" + status);
        }
        ArrayList tripDetails = new ArrayList();
        try {
            //status history entry
            status = (Integer) getSqlMapClientTemplate().update("trip.insertStartTripSheetDetails", map);

            System.out.println(" start trip status= " + status);
//            if (tripTO.getRegNo() != null) {
//
//                status = (Integer) getSqlMapClientTemplate().update("trip.updateStartVehicle", map);
//            }
            System.out.println("vehicle regNo status= " + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertStartTripGpsdetails", map2);

            System.out.println("vehicle gps status= " + status);

            //trip master status update
            status = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripSheet", map);

//             updateOrderStatusToEFS(tripTO.getTripSheetId(), tripTO.getStatusId());
            int maxVehicleSequence = 0;
            //            maxVehicleSequence = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaximumVehicleSequence", map);
            map.put("vehicleSequence", maxVehicleSequence);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleSequence", map);
            System.out.println("status---" + status);
            int insertConsignmentArticle = 0;
            map.put("consignmentId", tripTO.getConsignmentId());
            System.out.println("the inner" + map);
            String[] productCodes = null;
            String[] productNames = null;
            String[] packageNos = null;
            String[] weights = null;
            String[] productbatch = null;
            String[] productuom = null;
            String[] loadedpackages = null;
            String[] consArticleids = null;
            productCodes = tripTO.getProductCodes();
            productNames = tripTO.getProductNames();
            packageNos = tripTO.getPackagesNos();
            weights = tripTO.getWeights();
            productbatch = tripTO.getProductbatch();
            productuom = tripTO.getProductuom();
            consArticleids = tripTO.getConsArticleids();
            loadedpackages = tripTO.getLoadedpackages();
            if (productNames != null) {
                for (int i = 0; i < productNames.length; i++) {
                    map.put("productCode", productCodes[i]);
                    map.put("producName", productNames[i]);
                    map.put("packageNos", packageNos[i]);
                    map.put("weights", weights[i]);
                    map.put("productbatch", productbatch[i]);
                    map.put("productuom", productuom[i]);
                    map.put("loadedpackages", loadedpackages[i]);
                    map.put("consArticleid", consArticleids[i]);
                    System.out.println("The product Details:" + map);
                    if (productCodes[i] != null && !"".equals(productCodes[i]) && !"0".equals(productuom[i])) {
                        insertConsignmentArticle = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentArticle", map);
                    }

                }
            }
            String tripRevenueDetails = "";
            String tripTemp[] = null;
            String customerId = "";
            String tripType = "";
            String estimatedRevenue = "";
            String emptyTrip = "";
            tripRevenueDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTripRevenueDetails", map);
            System.out.println("tripRevenueDetails = " + tripRevenueDetails);
            if (!"".equals(tripRevenueDetails)) {
                tripTemp = tripRevenueDetails.split("~");
                customerId = tripTemp[0];
                tripType = tripTemp[1];
                estimatedRevenue = tripTemp[2];
                emptyTrip = tripTemp[3];
            }

            ///////////////////// new part //////////////////////////////////////
            String[] actualpointId = tripTO.getActualPointId();
            String[] actualpointname = tripTO.getActualPointName();
            String[] actualLatitude = tripTO.getActualLatitude();
            String[] actualLongtitude = tripTO.getActualLongtitude();
            String[] routeCourseId = tripTO.getRouteCourseId();

            System.out.println("actualpointId.length:" + actualpointId.length);
            System.out.println("actualpointname.length:" + actualpointname.length);
            System.out.println("actualLatitude.length:" + actualLatitude.length);
            System.out.println("actualLongtitude.length:" + actualLongtitude.length);
            int status1 = 0;
            for (int i = 0; i < actualpointId.length; i++) {
                map1.put("actualpointId", actualpointId[i]);
                map1.put("actualLatitude", actualLatitude[i]);
                map1.put("actualLongtitude", actualLongtitude[i]);
                map1.put("pointAddresss", actualpointname[i]);
                map1.put("movementUpdate", "0");
                map1.put("tripRouteCourseId", routeCourseId[i]);

                if (!actualpointId[i].equals("0")) {
                    System.out.println("Route map value is:" + map1);
                    status1 = (Integer) getSqlMapClientTemplate().update("trip.updateTripRoutePlan", map1);
                }

                System.out.println("saveTripRoutePlan=" + status1);
            }

            ///////////////////// new part //////////////////////////////////////
            /*    if ("1".equals(tripType) && "0".equals(emptyTrip)) {
             String code2 = "";
             String[] temp = null;
             int insertStatus = 0;
             map.put("userId", userId);
             map.put("DetailCode", "1");
             map.put("voucherType", "%SALES%");
             code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
             temp = code2.split("-");
             int codeval2 = Integer.parseInt(temp[1]);
             int codev2 = codeval2 + 1;
             String voucherCode = "SALES-" + codev2;
             System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "SALES");

             //get ledger info
             map.put("customer", customerId);
             String ledgerInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getCustomerLedgerInfo", map);
             System.out.println("ledgerInfo:" + ledgerInfo);
             temp = ledgerInfo.split("~");
             String ledgerId = temp[0];
             String particularsId = temp[1];
             map.put("ledgerId", ledgerId);
             map.put("particularsId", particularsId);

             map.put("amount", estimatedRevenue);//hidde for crossing amount edit option by madhavan
             //  map.put("amount", crossingAmount);
             map.put("Accounts_Type", "DEBIT");
             map.put("Remark", "Freight Charges");
             map.put("Reference", "Trip");
             //                System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + tripRevenue);
             //                getTripNo = (String) getSqlMapClientTemplate().queryForObject("operation.getTripNo", map);
             System.out.println("tripId = " + tripTO.getTripSheetId());
             map.put("SearchCode", tripTO.getTripSheetId());
             System.out.println("map1 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             System.out.println("status1 = " + insertStatus);
             //--------------------------------- acc 2nd row start --------------------------
             if (insertStatus > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", "51");
             map.put("particularsId", "LEDGER-39");
             map.put("Accounts_Type", "CREDIT");
             System.out.println("tripRevenueNew " + map.get("particularsId") + " = " + estimatedRevenue);
             System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             System.out.println("status2 = " + insertStatus);
             }

             }
             */
            System.out.println("updateStartTripSheet size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }

        return status;
    }

    public ArrayList getVehicleSchedule(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList vehicleSchedule = new ArrayList();
        try {
            map.put("vehicleId", tripTO.getVehicleId());
            System.out.println(" Trip map is::" + map);
            vehicleSchedule = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleSchedule", map);
            System.out.println("getVehicleSchedule.size() = " + vehicleSchedule.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getExpiryDateDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getExpiryDateDetails List", sqlException);
        }
        return vehicleSchedule;
    }

    public String getRoutePoints(TripTO tripTO) {
        Map map = new HashMap();
        String routePoints = "";
        try {
            map.put("consignmentOrderId", tripTO.getConsignmentOrderNos());
            routePoints = (String) getSqlMapClientTemplate().queryForObject("trip.getConsignmentRoutepointsForGoogleMap", map);
            System.out.println("routePoints=" + routePoints);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails", sqlException);
        }
        return routePoints;
    }

    public ArrayList getContainerDetails(String ConsignmentOrderIds) {
        Map map = new HashMap();
        ArrayList ContainerDetails = new ArrayList();
        map.put("ConsignmentOrderIds", ConsignmentOrderIds);
        try {
            System.out.println("map====ContainerDetailst" + map);
            ContainerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerDetails", map);
            System.out.println("ContainerDetails===" + ContainerDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return ContainerDetails;
    }

    public ArrayList getconsgncontainer(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consgncontainer = null;
        Map map = new HashMap();
        if ("2".equals(tripTO.getPageType())) {
            map.put("allocationFor", 1);
        } else if ("3".equals(tripTO.getPageType())) {
            map.put("allocationFor", 2);
        } else {
            map.put("allocationFor", 0);
        }

        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        map.put("movementType", tripTO.getMovementType());
        System.out.println("getconsgnContainerDetails map=" + map);
        try {
            String orderAllocation = (String) getSqlMapClient().queryForObject("trip.getOrderAllocation", map);
            System.out.println("orderAllocation::" + orderAllocation);
            map.put("orderAllocation", orderAllocation);
            consgncontainer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getconsgnContainerDetails", map);
            System.out.println("getconsgnContainerDetails.size() = " + consgncontainer.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return consgncontainer;
    }

    public ArrayList getrepoconsgncontainer(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        ArrayList consgncontainer = null;
        Map map = new HashMap();
        String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("consignmentOrderNos", consignmentOrderNos);
        try {
            consgncontainer = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getrepoconsgnContainerDetails", map);
            System.out.println("getrepoconsgnContainerDetails.size() = " + consgncontainer.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return consgncontainer;
    }

    public int updatePlannedStatus(TripTO tripTO, String[] containerTypeId, String[] containerNo, String[] uniqueId, String isRepo, String sealNo[], String grStatus[], String orderId[]) {
        Map map = new HashMap();
        int status = 0;
        int lastStatus = 0;
        try {
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            map.put("movementType", tripTO.getMovementType());
            if ("1".equals(tripTO.getRepoId())) {
                map.put("repoId", 1);
            } else if ("2".equals(tripTO.getRepoId())) {
                map.put("repoId", 2);

            } else {
                map.put("repoId", 0);
            }
            map.put("movementType", tripTO.getMovementType());
            if (containerNo != null) {
                for (int i = 0; i < containerTypeId.length; i++) {

                    map.put("containerTypeId", containerTypeId[i]);
                    map.put("containerNo", containerNo[i]);
                    map.put("uniqueId", uniqueId[i]);
                    if ("".equals(sealNo[i]) || sealNo[i] == null) {

                        map.put("sealNo", "NA");
                    } else {
                        map.put("sealNo", sealNo[i]);
                    }

                    map.put("grStatus", grStatus[i]);
                    map.put("consignmentOrderIds", orderId[i]);
                    System.out.println("updatePlannedStatus" + map);
                    if ("Y".equals(isRepo)) {
                        lastStatus = (Integer) getSqlMapClientTemplate().update("trip.updateLastPlannedStatus", map);
                        System.out.println("lastStatus:" + lastStatus);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.updatePlannedStatus", map);
                        System.out.println("updatePlannedStatus1111111111:" + status);
                    }
                }
                if ("2".equals(tripTO.getRepoId())) {
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatePlannedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updatePlannedStatus List", sqlException);
        }
        return status;
    }

    public int updateTempUnplannedStatus(TripTO tripTO, String[] uniqueId, String[] oldSelectedContainer) {
        Map map = new HashMap();
        int status = 0;
        int lastStatus = 0;
        try {
            if (uniqueId != null) {
                for (int i = 0; i < uniqueId.length; i++) {

                    map.put("uniqueId", uniqueId[i]);
                    status = (Integer) getSqlMapClientTemplate().queryForObject("trip.getAlreadyPlannedStatus", map);
                    System.out.println("status:" + status);
                }
            }
            if (status == 0) {
                if (oldSelectedContainer != null) {
                    for (int i = 0; i < oldSelectedContainer.length; i++) {

                        map.put("oldSelectedContainer", oldSelectedContainer[i]);
                        System.out.println("oldSelectedContainer map = " + map);
                        lastStatus = (Integer) getSqlMapClientTemplate().update("trip.updateTempUnplannedStatus", map);
                        System.out.println("lastStatus:" + lastStatus);
                    }
                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatePlannedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updatePlannedStatus List", sqlException);
        }
        return status;
    }

    public int updatePlannedStatus(TripTO tripTO, String[] containerTypeId, String[] containerNo, String[] uniqueId, String isRepo, String sealNo[], String grStatus[], String orderId[], SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int lastStatus = 0;
        try {
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            if ("1".equals(tripTO.getRepoId())) {
                map.put("repoId", 1);
            } else if ("2".equals(tripTO.getRepoId())) {
                map.put("repoId", 2);

            } else {
                map.put("repoId", 0);
            }
//            if (!"".equals(tripTO.getConsignorId()) && !"0".equals(tripTO.getConsignorId())) {
            map.put("consignorId", tripTO.getConsignorId());
//            }
//            if (!"".equals(tripTO.getConsigneeId()) && !"0".equals(tripTO.getConsigneeId())) {
            map.put("consigeeId", tripTO.getConsigneeId());
//            }

            System.out.println("containerTypeId.length" + containerTypeId.length);
            if (containerNo != null) {
                for (int i = 0; i < containerTypeId.length; i++) {

                    map.put("containerTypeId", containerTypeId[i]);

                    if ("".equals(containerNo[i]) || containerNo[i] == null) {

                        map.put("containerNo", "NA");
                    } else {
                        map.put("containerNo", containerNo[i]);
                    }

                    map.put("uniqueId", uniqueId[i]);

                    if ("".equals(sealNo[i]) || sealNo[i] == null) {

                        map.put("sealNo", "NA");
                    } else {
                        map.put("sealNo", sealNo[i]);
                    }

                    System.out.println("i value is :::" + i);
//                    if ("".equals(goodsDesc[i]) || goodsDesc[i] == null) {
//                       map.put("goodsDesc", "NA");
//                    } else {
//                       map.put("goodsDesc", goodsDesc[i]);
//                    }

                    map.put("grStatus", "To be Billed");

                    map.put("consignmentOrderIds", orderId[i]);
                    String transporterType = "0"; //0 indicates market
                    if (!"0".equals(tripTO.getVehicleId())) {
                        transporterType = "1";//own vehicle
                    }
//                        map.put("transporterType", consignmentOrderIds[0]);
                    map.put("transporterType", transporterType);
                    System.out.println("updatePlannedStatus" + map);
                    if ("Y".equals(isRepo)) {

                        lastStatus = (Integer) session.update("trip.updateLastPlannedStatus", map);
                        System.out.println("lastStatus:" + lastStatus);
                    } else {
                        System.out.println("rererere" + tripTO.getMovementType());
                        if (!"7".equalsIgnoreCase(tripTO.getMovementType())) {
                            status = (Integer) session.update("trip.updatePlannedStatus", map);
                        } else {
                            status = (Integer) session.update("trip.updateTempPlannedStatus", map);
                        }
                        System.out.println("updatePlannedStatus1111111111:" + status);
                    }
                }
                if ("2".equals(tripTO.getRepoId())) {
                }
            }
            //            update consignor, consignee
            if (!"".equals(tripTO.getConsignorId()) && !"".equals(tripTO.getConsigneeId()) && tripTO.getConsignorId() != null && tripTO.getConsigneeId() != null) {
                int alter = (Integer) session.update("trip.updateConsignorConsignee", map);
                System.out.println("lastStatus:" + alter);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatePlannedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updatePlannedStatus List", sqlException);
        }
        return status;
    }

    public String getPlannedConatinerSize(TripTO tripTO) {
        Map map = new HashMap();
        String plannedConatiner = "";
        try {
            map.put("orderId", tripTO.getConsignmentOrderNos());
            System.out.println("map for size:" + map);
            plannedConatiner = (String) getSqlMapClientTemplate().queryForObject("trip.getPlannedConatinerNos", map);
            System.out.println("plannedConatiner size=" + plannedConatiner);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return plannedConatiner;
    }

    public String getDistance(String origin, String destination) {
        Map map = new HashMap();
        String plannedConatiner = "";
        try {
            map.put("origin", origin);
            map.put("destination", destination);
            System.out.println("map for size:" + map);
            plannedConatiner = (String) getSqlMapClientTemplate().queryForObject("trip.getDistance", map);
            System.out.println("plannedConatiner size=" + plannedConatiner);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return plannedConatiner;
    }

    public ArrayList getPrintTripSheetDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripDetailsList = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            System.out.println("map for size:" + map);
            tripDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrintTripSheetDetails", map);
            System.out.println("tripDetailsList size=" + tripDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetails List", sqlException);
        }

        return tripDetailsList;
    }

    public ArrayList getPrintTripSheetDetailsForLCL(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripDetailsListLCL = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            System.out.println("map for size:" + map);
            tripDetailsListLCL = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrintTripSheetDetailsForLCL", map);
            System.out.println("tripDetailsListLCL size=" + tripDetailsListLCL.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetailsForLCL Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetailsForLCL List", sqlException);
        }

        return tripDetailsListLCL;
    }

    public ArrayList getPrintTripSheetDetailsForFCL(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripDetailsListLCL = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            System.out.println("map for size:" + map);
            tripDetailsListLCL = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrintTripSheetDetailsForFCL", map);
            System.out.println("tripDetailsListFCL size=" + tripDetailsListLCL.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetailsForFCL Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetailsForFCL List", sqlException);
        }

        return tripDetailsListLCL;
    }

    public ArrayList getPrintTripChallanDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList tripDetailsList = new ArrayList();
        try {

            map.put("tripSheetId", tripSheetId);
            System.out.println("map for size:" + map);
            tripDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getPrintTripChallanDetails", map);
            System.out.println("tripDetailsList size=" + tripDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPrintTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPrintTripSheetDetails List", sqlException);
        }

        return tripDetailsList;
    }

    public ArrayList getConsignmentNoteContainer(String consignmentNoteOredrId, String tripId) {
        Map map = new HashMap();
        ArrayList containerNo = new ArrayList();
        try {
            map.put("consignmentNoteOredrId", consignmentNoteOredrId);
            map.put("tripId", tripId);
            System.out.println("map for size:" + map);
            containerNo = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentNoteContainer", map);
            System.out.println("consignmentNoteOredrId  size=" + containerNo.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentNoteContainer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentNoteContainer List", sqlException);
        }

        return containerNo;
    }

    public ArrayList getConsignmentArticleList(String consignmentNoteOredrId) {
        Map map = new HashMap();
        ArrayList consignmentArticleList = new ArrayList();
        try {

            map.put("consignmentNoteOredrId", consignmentNoteOredrId);
            System.out.println("map for size:" + map);
            consignmentArticleList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentArticleList", map);
            System.out.println("consignmentArticleList size=" + consignmentArticleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentArticleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentArticleList List", sqlException);
        }

        return consignmentArticleList;
    }

    public ArrayList getBunkList(String bunkName, String companyId) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            System.out.println("bunkName.." + bunkName);
            if (bunkName != null && !"".equals(bunkName)) {
                map.put("bunkname", bunkName);
            }
            map.put("companyId", companyId);

            System.out.println("map.size().." + map.size());
            if (bunkName != null && !"".equals(bunkName)) {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getselBunk", map);
            } else {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBunkList", map);
            }
            System.out.println("bunkselList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doInsertBunk(TripTO tripTO, int userId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        int bunkid = 0;
        int id = 0;

        try {
            id = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMaxIDList", mapmaster);
            String FuelLocateId = tripTO.getBunkName().substring(0, 1) + id;

            System.out.println("FuelLocateId" + FuelLocateId);

            mapmaster.put("FuelLocateId", FuelLocateId);
            mapmaster.put("bunkname", tripTO.getBunkName());
            mapmaster.put("location", tripTO.getCurrlocation());
            mapmaster.put("state", tripTO.getBunkState());
            mapmaster.put("act_ind", tripTO.getBunkStatus());
            mapmaster.put("createdby", userId);
            mapmaster.put("companyId", tripTO.getCompanyId());

            System.out.println("test1" + status);
            int bunkId = (Integer) getSqlMapClientTemplate().insert("trip.insertBunkmaster", mapmaster);
            System.out.println("bunkId master is" + bunkId);

            //Ledger Code start
            if (bunkId != 0) {
                String code = (String) getSqlMapClientTemplate().queryForObject("trip.getLedgerCode", mapmaster);
                String[] temp = code.split("-");
                int codeval = Integer.parseInt(temp[1]);
                int codev = codeval + 1;
                String ledgercode = "LEDGER-" + codev;
                mapmaster.put("ledgercode", ledgercode);

                //current year and month start
                String accYear = (String) getSqlMapClientTemplate().queryForObject("trip.accYearVal", mapmaster);
                System.out.println("accYear:" + accYear);
                mapmaster.put("accYear", accYear);
                //current year end

                String bunkName = tripTO.getBunkName() + "-" + bunkId;
                System.out.println("bunkName =====> " + bunkName);

                int VendorLedgerId = (Integer) getSqlMapClientTemplate().insert("trip.insertVendorLedger", mapmaster);
                System.out.println("VendorLedgerId......." + VendorLedgerId);
                if (VendorLedgerId != 0) {
                    mapmaster.put("ledgerId", VendorLedgerId);
                    mapmaster.put("bunkId", bunkId);
                    System.out.println("map update ::::=> " + mapmaster);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateBunkInfo", mapmaster);
                }
            }
            //Ledger Code end

            //bunkid = (Integer) getSqlMapClientTemplate().queryForObject("customer.getBunkselectedList", mapmaster);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", tripTO.getFuelType());
            mapdetail.put("currentrate", tripTO.getCurrRate());
            mapdetail.put("bunkremarks", tripTO.getRemarks());
            mapdetail.put("act_ind", tripTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("test2" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getBunkalterList(String bunkId) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            if (bunkId != null) {
                map.put("bunkid", bunkId);
            }
            bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getalterBunk", map);

            System.out.println("bunkList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doUpdateBunk(TripTO tripTO, int userId, String bunkId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        mapmaster.put("bunkname", tripTO.getBunkName());
        mapmaster.put("location", tripTO.getCurrlocation());
        mapmaster.put("state", tripTO.getBunkState());
        mapmaster.put("act_ind", tripTO.getBunkStatus());
        mapmaster.put("createdby", userId);
        mapmaster.put("bunkid", bunkId);

        try {
            System.out.println("test1=" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateBunkmaster", mapmaster);
            System.out.println("status master is=" + status);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", tripTO.getFuelType());
            mapdetail.put("currentrate", tripTO.getCurrRate());
            mapdetail.put("bunkremarks", tripTO.getRemarks());
            mapdetail.put("act_ind", tripTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("\nbunkid.." + bunkId);
            System.out.println("\ncustomerTO.getFuelType()=" + tripTO.getFuelType());
            System.out.println("\ncustomerTO.getCurrRate()=" + tripTO.getCurrRate());
            System.out.println("\ncustomerTO.getRemarks()=" + tripTO.getRemarks());
            System.out.println("\ncustomerTO.getBunkStatus()=" + tripTO.getBunkStatus());
            System.out.println("\ncreatedby=" + userId);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getCustomerNameSuggestionList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            map.put("customerName", tripTO.getCustomerName() + "%");
            map.put("companyId", tripTO.getCompanyId());
            System.out.println("map values:" + map);
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerNameSuggestionList", map);

            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerNameSuggestionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerNameSuggestionList", sqlException);
        }
        return customerList;
    }

    public ArrayList getInvoiceDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceDetailsList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            invoiceDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceDetailsList", map);

            System.out.println("invoiceDetailsList size=" + invoiceDetailsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return invoiceDetailsList;
    }

    public ArrayList getInvoiceHeaderList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceHeaderList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            invoiceHeaderList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getInvoiceHeaderList", map);

            System.out.println("invoiceHeaderList size=" + invoiceHeaderList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceHeaderList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceHeaderList", sqlException);
        }
        return invoiceHeaderList;
    }

    public ArrayList getRtoMasterList() {
        Map map = new HashMap();
        ArrayList troMasterList = new ArrayList();
        try {
            troMasterList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRtoMasterList", map);

            System.out.println("troMasterList size=" + troMasterList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRtoMasterList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getRtoMasterList", sqlException);
        }
        return troMasterList;
    }

    public ArrayList getRtoMappedVehicleList(String rtoOfficeId) {
        Map map = new HashMap();
        ArrayList rtoMappedVehicleList = new ArrayList();
        try {
            if (rtoOfficeId != null && !"".equalsIgnoreCase(rtoOfficeId)) {
                map.put("rtoOfficeId", rtoOfficeId);
            }
            rtoMappedVehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRtoMappedVehicleList", map);

            System.out.println("rtoMappedVehicleList size=" + rtoMappedVehicleList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRtoMappedVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getRtoMappedVehicleList", sqlException);
        }
        return rtoMappedVehicleList;
    }

    public int insertRtoVehicleMapping(int userId, String[] assignedVehicleId, String rtoOfficeId) {
        Map map = new HashMap();
        int insertStatus = 0;
        try {
            if (assignedVehicleId != null && assignedVehicleId.length > 0) {
                map.put("rtoOfficeId", rtoOfficeId);
                insertStatus = (Integer) getSqlMapClientTemplate().delete("trip.deleteRtoMapping", map);
                for (int i = 0; i < assignedVehicleId.length; i++) {
                    map.put("userId", userId);
                    map.put("vehicleId", assignedVehicleId[i]);
                    insertStatus = (Integer) getSqlMapClientTemplate().insert("trip.insertRtoVehicleMapping", map);
                }
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertRtoVehicleMapping Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "insertRtoVehicleMapping", sqlException);
        }
        return insertStatus;
    }

    public int getRtoVehicleCount(TripTO tripTO) {
        Map map = new HashMap();
        int vehicleCount = 0;
        try {
            map.put("rtoOfficeId", tripTO.getRtoId());
            System.out.println("map===" + map);
            vehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getRtoVehicleCounttrip", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRtoVehicleCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getRtoVehicleCount", sqlException);
        }
        return vehicleCount;
    }

    public int getPlannedContainerCount(String consignmentOrderId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("consignmentOrderId", consignmentOrderId);
            System.out.println("map fro:" + map);
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.getPlannedContainerCount", map);
            System.out.println("count::" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return status;
    }

    public String getTwoTwentyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType) {
        Map map = new HashMap();
        String rate = "";
        try {
            map.put("customerId", customerId);
            map.put("firstPointId", firstPointId);
            map.put("secondPointId", secondPointId);
            map.put("productCategory", productCategory);
            map.put("movementType", movementType);
            rate = (String) getSqlMapClientTemplate().queryForObject("trip.getTwoTwentyRate", map);
            System.out.println("two twenty rate:" + rate);
            if (rate == null) {
                rate = "0.0";
            }
            System.out.println("two twenty rate After:" + rate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return rate;
    }

    public String getSingleFourtyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType) {
        Map map = new HashMap();
        String rate = "";
        try {
            map.put("customerId", customerId);
            map.put("firstPointId", firstPointId);
            map.put("secondPointId", secondPointId);
            map.put("productCategory", productCategory);
            map.put("movementType", movementType);
            System.out.println("map for Fourty:" + map);
            rate = (String) getSqlMapClientTemplate().queryForObject("trip.getSingleFourtyRate", map);
            System.out.println("single fourty rate:" + rate);
            if (rate == null) {
                rate = "0.0";
            }
            System.out.println("single fourty rate After:" + rate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return rate;
    }

    public String getSingleTwentyRate(String customerId, String firstPointId, String secondPointId, String productCategory, String movementType, String lastPointId, String thirdPoint) {
        Map map = new HashMap();
        String rate = "";
        String fourtyRate = "";
        String twoTwentyRate = "";
        try {
            map.put("customerId", customerId);
            map.put("firstPointId", firstPointId);
            map.put("secondPointId", secondPointId);
            map.put("lastPointId", lastPointId);
            map.put("thirdPoint", thirdPoint);
            map.put("productCategory", productCategory);
            map.put("movementType", movementType);
            System.out.println("map:" + map);
            if (!"1".equals(productCategory)) {
                System.out.println("i m in frozen/chiller");
                fourtyRate = (String) getSqlMapClientTemplate().queryForObject("trip.getSingleFourtyRateWithReefer", map);
            } else {
                System.out.println("i m in ambient");
                fourtyRate = (String) getSqlMapClientTemplate().queryForObject("trip.getSingleFourtyRateWithoutReefer", map);
            }
            System.out.println("single fourty rate:" + fourtyRate);
            if (fourtyRate == null) {
                fourtyRate = "0.0";
            }
            System.out.println("singleFourtyRate After:" + fourtyRate);
            rate = (String) getSqlMapClientTemplate().queryForObject("trip.getSingleTwentyRate", map);
            System.out.println("singleTwentyRate:" + rate);
            if (rate == null) {
                rate = "0.0";
            }
            System.out.println("singleTwentyRate After:" + rate);

            twoTwentyRate = (String) getSqlMapClientTemplate().queryForObject("trip.getTwoTwentyRate", map);
            System.out.println("two twenty rate:" + twoTwentyRate);
            if (twoTwentyRate == null) {
                twoTwentyRate = "0.0";
            }
            System.out.println("two twenty rate after:" + twoTwentyRate);

            rate = rate + "~" + fourtyRate + "~" + twoTwentyRate;
            System.out.println("final Rate tariff:" + rate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return rate;
    }

    public ArrayList getOrderInvoiceHeader(TripTO tripTO) {
        Map map = new HashMap();
        map.put("invoiceId", tripTO.getInvoiceId());
        ArrayList result = new ArrayList();
        System.out.println("map = " + map);
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceHeader", map);
            System.out.println("result size=" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrderInvoiceHeader Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOrderInvoiceHeader List", sqlException);
        }

        return result;
    }

    public ArrayList getOrderInvoiceDetailsList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList invoiceDetailsList = new ArrayList();
        try {
            map.put("invoiceId", tripTO.getInvoiceId());
            invoiceDetailsList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getOrderInvoiceDetailsList", map);

            System.out.println("invoiceDetailsList size=" + invoiceDetailsList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getInvoiceDetailsList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getInvoiceDetailsList", sqlException);
        }
        return invoiceDetailsList;
    }

    public ArrayList getTripFuelDetails(String tripSheetId) {
        Map map = new HashMap();
        ArrayList fuelDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripSheetId);
            fuelDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripFuelDetails", map);

            System.out.println("getTripFuelDeatils size=" + fuelDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return fuelDetails;
    }

    public String insertTripGr(String tripId, int userId, String tripGr, SqlMapClient session) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("tripGr", tripGr);
            tripGrNo = (Integer) session.insert("trip.insertTripGr", map);
            System.out.println("tripGrNo in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String insertTripGr(String tripId, int userId, String tripGr) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("tripGr", tripGr);
            tripGrNo = (Integer) getSqlMapClientTemplate().insert("trip.insertTripGr", map);
            System.out.println("tripGrNo in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String insertTripChallan(String tripId, int userId, String challanNo, SqlMapClient session) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("challanNo", challanNo);
            tripGrNo = (Integer) session.insert("trip.insertTripChallan", map);
            System.out.println("insertTripChallan in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String insertTripChallan(String tripId, int userId, String challanNo) {
        Map map = new HashMap();
        int tripGrNo = 0;
        try {
            map.put("tripId", tripId);
            map.put("userId", userId);
            map.put("challanNo", challanNo);
            tripGrNo = (Integer) getSqlMapClientTemplate().insert("trip.insertTripChallan", map);
            System.out.println("insertTripChallan in Dao=" + tripGrNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCnoteCodeSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCodeSequence", sqlException);
        }
        return tripGrNo + "";
    }

    public String getPlannedContainerNo(String orderId) {
        Map map = new HashMap();
        String rate = "";
        try {
            map.put("orderId", orderId);

            rate = (String) getSqlMapClientTemplate().queryForObject("trip.getPlannedContainerNo", map);
            System.out.println("getPlannedContainerNo:" + rate);
            if (rate == null) {
                rate = "";
            }
            System.out.println("getPlannedContainerNot:" + rate);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPlannedContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getPlannedContainerCount List", sqlException);
        }
        return rate;
    }

    public int updateTollAndDetaintionForBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId, String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId, String[] greenTax, String[] greenTaxOld, String[] shippingBillNo, String[] shippingBillNoOld, String[] billOfEntry, String[] billOfEntryOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId) {
        Map map = new HashMap();
        String tempContainer[] = null;
        String tempTripContainerId[] = null;
        String tempConsignmentContainerId[] = null;
        map.put("userId", userId);
        String dest = "";
        String billEntry = "";
        String shipNo = "";
        String containrNo = "";
        String grenTax = "";
        String tollTax = "";
        String otherExpenses = "";
        String weightmentCharge = "";
        String billParty = "";

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {

                map.put("tripId", tripId[i]);
                map.put("toll", toll[i]);
                map.put("detaintion", detaintion[i]);
                map.put("greenTax", greenTax[i]);
                map.put("grNo", grNo[i]);
                map.put("otherExpense", otherExpense[i]);
                map.put("weightment", weightment[i]);
                System.out.println("map@" + map);

                // check otherExpense
                int otherExpenseCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseForBilling", map);
                System.out.println("otherExpenseCheck" + otherExpenseCheck);
                if (!"0.00".equals(otherExpense[i])) {

                    if (otherExpenseCheck != 0) {
                        map.put("otherExpense", otherExpense[i]);
                        System.out.println("%%%%%%%%%%%%otherExpense[i]" + otherExpense[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateOtherExpenseForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertOtherExpenseForBilling", map);
                    }
                }

                // check detaiontion
                int detaintioncheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getDetaiontionForBilling", map);
                System.out.println("detaintioncheck" + detaintioncheck);
                if (!"0.00".equals(detaintion[i])) {

                    if (detaintioncheck != 0) {
                        map.put("detaintion", detaintion[i]);
                        System.out.println("%%%%%%%%%%%%detaintion[i]" + detaintion[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateDetaintionForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertDetaintionForBilling", map);
                    }
                }
                //check for Grenn Tax
                int greentax = (Integer) getSqlMapClientTemplate().queryForObject("trip.getGreenTaxForBilling", map);
                System.out.println("greentax" + greentax);
                if (!"0.00".equals(greenTax[i]) && !"0".equals(greenTax[i])) {

                    if (greentax != 0) {
                        map.put("greenTax", greenTax[i]);
                        System.out.println("%%%%%%%%%%%%greenTax[i]" + greenTax[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateGreenTaxForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertGreenTaxForBilling", map);
                    }
                }

                //check toll
                int tollcheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTollForBilling", map);
                System.out.println("tollcheck" + tollcheck);
                if (!"0.00".equals(toll[i])) {
                    if (tollcheck != 0) {
                        map.put("toll", toll[i]);
                        System.out.println("%%%%%%%%%%%toll[i]" + toll[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateTollForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertTollForBilling", map);
                    }
                }

                //check weightment
                int weightmentCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getWeightmentForBilling", map);
                System.out.println("weightmentCharge" + weightmentCheck);
                map.put("expenseType", expenseType[i]);
//         if( !"0.00".equals(weightment[i])){
                map.put("weightmentCharge", weightment[i]);
                if (weightmentCheck != 0) {
                    System.out.println("%%%%%%%%%%%toll[i]" + weightment[i]);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateWeightmentForBilling", map);
                } else if (!"0".equals(weightment[i])) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertWeightmentForBilling", map);
                }

                //update trip container and consignment container
                if (containerNoOld[i].equals(containerNo[i])) {
                    containrNo = "0";
                    System.out.println("enterecd action");
                    map.put("containerNo", "0");
                    map.put("containerNoOld", "0");
                } else {
                    containrNo = containerNo[i];
                    map.put("containerNoOld", containerNoOld[i]);
                    map.put("containerNo", containerNo[i]);
                }

                System.out.println("the update toll and detaintion" + map);
                // for double twenty  contanier,we need to  split the container for each trip id
                tempContainer = containerNo[i].split(",");
                tempTripContainerId = tripContainerId[i].split(",");
                tempConsignmentContainerId = consignmentConatinerId[i].split(",");
                for (int j = 0; j < tempContainer.length; j++) {
                    map.put("tempContainer", tempContainer[j]);
                    map.put("tempTripContainerId", tempTripContainerId[j]);
                    map.put("tempConsignmentContainerId", tempConsignmentContainerId[j]);
                    System.out.println("the container map111" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripContainer", map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentContainer", map);
                }

                //update shipping bill and BOE
                if ("1".equals(movementType)) {
                    if (!"0".equals(shippingBillNo[i]) && !"".equals(shippingBillNo[i]) && shippingBillNo[i] != null) {
                        map.put("shippingBillNo", shippingBillNo[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateShippingBillNoForTrip", map);
                        System.out.println("status for shipping billno" + status);
                    }
                } else if (!"0".equals(billOfEntry[i]) && !"".equals(billOfEntry[i]) && billOfEntry[i] != null) {
                    map.put("billOfEntry", billOfEntry[i]);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateBillOfEntryForTrip", map);
                    System.out.println("status for shipping billno" + status);
                }

                // insert charges for invoice edit log (BILLING)
                if (detaintionOld[i].equals(detaintion[i])) {
                    System.out.println("enterecd action");
                    dest = "0";
                    map.put("detaintion", "0");
                    map.put("detaintionOld", "0");
                } else {
                    dest = detaintion[i];
                    map.put("detaintionOld", detaintionOld[i]);
                    map.put("detaintion", detaintion[i]);
                }
                if (greenTaxOld[i].equals(greenTax[i])) {
                    grenTax = "0";
                    map.put("greenTax", "0");
                    map.put("greenTaxOld", "0");
                } else {
                    grenTax = greenTax[i];
                    map.put("greenTaxOld", greenTaxOld[i]);
                    map.put("greenTax", greenTax[i]);
                }
                if (tollOld[i].equals(toll[i])) {
                    tollTax = "0";
                    map.put("toll", "0");
                    map.put("tollOld", "0");
                } else {
                    tollTax = toll[i];
                    map.put("tollOld", tollOld[i]);
                    map.put("toll", toll[i]);
                }
                if (otherExpenseOld[i].equals(otherExpense[i])) {
                    otherExpenses = "0";
                    map.put("otherExpense", "0");
                    map.put("otherExpenseOld", "0");
                } else {
                    otherExpenses = otherExpense[i];
                    map.put("otherExpenseOld", otherExpenseOld[i]);
                    map.put("otherExpense", otherExpense[i]);
                }
                if (weightmentOld[i].equals(weightment[i])) {
                    weightmentCharge = "0";
                    map.put("weightment", "0");
                    map.put("weightmentOld", "0");
                } else {
                    weightmentCharge = toll[i];
                    map.put("weightmentOld", weightmentOld[i]);
                    map.put("weightment", weightment[i]);
                }
                System.out.println("##################" + map);

                if ("1".equals(movementType)) {
                    map.put("billOfEntry", "0");
                    map.put("billOfEntryOld", "0");
                    billEntry = "0";
                    if (shippingBillNoOld[i].equals(shippingBillNo[i])) {
                        System.out.println("shippingBillNo000000" + shippingBillNo[i] + "," + shippingBillNoOld[i]);
                        System.out.println("enterecd action");
                        shipNo = "0";
                        map.put("shippingBillNo", "0");
                        map.put("shippingBillNoOld", "0");
                    } else {
                        shipNo = shippingBillNo[i];

                        map.put("shippingBillNoOld", shippingBillNoOld[i]);
                        map.put("shippingBillNo", shippingBillNo[i]);

                        map.put("billOfEntry", "0");
                        map.put("billOfEntryOld", "0");
                    }
                } else {
                    map.put("shippingBillNo", "0");
                    map.put("shippingBillNoOld", "0");
                    shipNo = "0";
                    if (billOfEntry[i].equals(billOfEntryOld[i])) {
                        System.out.println("billOfEntry000000" + billOfEntry[i] + "," + billOfEntryOld[i]);
                        billEntry = "0";
                        map.put("billOfEntry", "0");
                        map.put("billOfEntryOld", "0");
                    } else {
                        billEntry = billOfEntry[i];
                        map.put("billOfEntryOld", billOfEntryOld[i]);
                        map.put("billOfEntry", billOfEntry[i]);

                        map.put("shippingBillNo", "0");
                        map.put("shippingBillNoOld", "0");
                    }
                }

                if (billingParty.equals(billingPartyOld)) {
                    System.out.println("billingParty" + billingParty + "," + billingParty);
                    billParty = "0";
                    map.put("billingParty", "0");
                    map.put("billingPartyOld", "0");
                } else {
                    billParty = billingParty;
                    map.put("billingPartyOld", billingPartyOld);
                    map.put("billingParty", billingParty);
                }

                System.out.println("map end=" + map);
                System.out.println("map containerNo[i]=" + containrNo);
                System.out.println("map shippingBillNo[i]=" + shipNo);
                System.out.println("map billOfEntry[i]=" + billEntry);
                System.out.println("map detaintion[i]=" + dest);
                System.out.println("map greenTax[i]=" + grenTax);
                System.out.println("map toll[i]=" + tollTax);
                System.out.println("map weightment[i]=" + weightment);
                System.out.println("map billList=" + billList);
                System.out.println("map billParty=" + billParty);

                if (("0".equals(shipNo)) && ("0".equals(billEntry)) && "0".equals(containrNo) && "0".equals(dest) && "0".equals(grenTax) && "0".equals(tollTax) && "0".equals(otherExpenses) && "0".equals(weightmentCharge) && "0".equals(billParty)) {
                    System.out.println("entered equal to Zero");
                } else {
                    System.out.println("BILLLING");
                    map.put("billType", "Billing");
                    map.put("movementType", movementType);
                    System.out.println("entered  equal to Zero");
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceEditLog", map);
                    System.out.println("the update toll and detaintion =" + status);
                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTollAndDetaintionForRepoBilling(String[] grNo, String[] tripId, String[] toll, String[] tollOld, String[] detaintion, String[] detaintionOld, String[] vehicleId, String[] driverId, String[] containerNo, String[] containerNoOld, String[] tripContainerId, String[] consignmentConatinerId, String[] greenTax, String[] greenTaxOld, String billList, String movementType, String billingPartyOld, String billingParty, String[] otherExpense, String[] otherExpenseOld, String[] weightment, String[] weightmentOld, String[] expenseType, int userId) {
        System.out.println("RAJDAO");
        Map map = new HashMap();
        String tempContainer[] = null;
        String tempTripContainerId[] = null;
        String tempConsignmentContainerId[] = null;
        map.put("userId", userId);
        String dest = "";
        String billEntry = "";
        String shipNo = "";
        String containrNo = "";
        String grenTax = "";
        String tollTax = "";
        String otherExpenses = "";
        String weightmentCharge = "";
        String billParty = "";

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {

                map.put("tripId", tripId[i]);
                map.put("toll", toll[i]);
                map.put("detaintion", detaintion[i]);
                map.put("greenTax", greenTax[i]);
                map.put("grNo", grNo[i]);
                map.put("otherExpense", otherExpense[i]);

                // check otherExpense
                int otherExpenseCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseForBilling", map);
                System.out.println("otherExpenseCheck" + otherExpenseCheck);
                if (!"0.00".equals(otherExpense[i])) {

                    if (otherExpenseCheck != 0) {
                        map.put("otherExpense", otherExpense[i]);
                        System.out.println("%%%%%%%%%%%%otherExpense[i]" + otherExpense[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateOtherExpenseForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertOtherExpenseForBilling", map);
                    }
                }

                //check toll
                int tollcheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTollForBilling", map);
                if (!"0.00".equals(toll[i])) {
                    if (tollcheck != 0) {
                        map.put("toll", toll[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateTollForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertTollForBilling", map);
                    }
                }
// check detaiontion
                int detaintioncheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getDetaiontionForBilling", map);
                if (!"0.00".equals(detaintion[i])) {

                    if (detaintioncheck != 0) {
                        map.put("detaintion", detaintion[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateDetaintionForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertDetaintionForBilling", map);
                    }
                }
                //check for Grenn Tax
                int greentax = (Integer) getSqlMapClientTemplate().queryForObject("trip.getGreenTaxForBilling", map);
                if (!"0.00".equals(greenTax[i])) {

                    if (greentax != 0) {
                        map.put("greenTax", greenTax[i]);
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateGreenTaxForBilling", map);
                    } else {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertGreenTaxForBilling", map);
                    }
                }

//check weightment
                int weightmentCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.getWeightmentForBilling", map);
                System.out.println("weightmentCharge" + weightmentCheck);
                map.put("expenseType", expenseType[i]);
//         if( !"0.00".equals(weightment[i])){
                map.put("weightmentCharge", weightment[i]);
                if (weightmentCheck != 0) {
                    System.out.println("%%%%%%%%%%%toll[i]" + weightment[i]);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateWeightmentForBilling", map);
                } else if (!"0".equals(weightment[i])) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertWeightmentForBilling", map);
                }

// update container
                if (containerNoOld[i].equals(containerNo[i])) {
                    containrNo = "0";
                    System.out.println("enterecd action");
                    map.put("containerNo", "0");
                    map.put("containerNoOld", "0");
                } else {
                    containrNo = containerNo[i];
                    map.put("containerNoOld", containerNoOld[i]);
                    map.put("containerNo", containerNo[i]);
                }

                // for double twenty  contanier,we need to  split the container for each trip id
                tempContainer = containerNo[i].split(",");
                tempTripContainerId = tripContainerId[i].split(",");
                tempConsignmentContainerId = consignmentConatinerId[i].split(",");
                for (int j = 0; j < tempContainer.length; j++) {
                    map.put("tempContainer", tempContainer[j]);
                    map.put("tempTripContainerId", tempTripContainerId[j]);
                    map.put("tempConsignmentContainerId", tempConsignmentContainerId[j]);
                    System.out.println("the container map111" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripContainer", map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentContainer", map);
                }

                //insert charges for invoice edit log (REPO BILLING)
                if (detaintionOld[i].equals(detaintion[i])) {
                    System.out.println("enterecd action");
                    dest = "0";
                    map.put("detaintion", "0");
                    map.put("detaintionOld", "0");
                } else {
                    dest = detaintion[i];
                    map.put("detaintionOld", detaintionOld[i]);
                    map.put("detaintion", detaintion[i]);
                }
                if (greenTaxOld[i].equals(greenTax[i])) {
                    grenTax = "0";
                    map.put("greenTax", "0");
                    map.put("greenTaxOld", "0");
                } else {
                    grenTax = greenTax[i];
                    map.put("greenTaxOld", greenTaxOld[i]);
                    map.put("greenTax", greenTax[i]);
                }
                if (tollOld[i].equals(toll[i])) {
                    tollTax = "0";
                    map.put("toll", "0");
                    map.put("tollOld", "0");
                } else {
                    tollTax = toll[i];
                    map.put("tollOld", tollOld[i]);
                    map.put("toll", toll[i]);
                }

                if ("RepoBilling".equals(billList)) {
                    shipNo = "0";
                    map.put("shippingBillNo", "0");
                    map.put("shippingBillNoOld", "0");
                    billEntry = "0";
                    map.put("billOfEntry", "0");
                    map.put("billOfEntryOld", "0");
                }
                if (billingParty.equals(billingPartyOld)) {
                    billParty = "0";
                    map.put("billingParty", "0");
                    map.put("billingPartyOld", "0");
                } else {
                    billParty = billingParty;
                    map.put("billingPartyOld", billingPartyOld);
                    map.put("billingParty", billingParty);
                }

                if (otherExpenseOld[i].equals(otherExpense[i])) {
                    otherExpenses = "0";
                    map.put("otherExpense", "0");
                    map.put("otherExpenseOld", "0");
                } else {
                    otherExpenses = otherExpense[i];
                    map.put("otherExpenseOld", otherExpenseOld[i]);
                    map.put("otherExpense", otherExpense[i]);
                }
                if (weightmentOld[i].equals(weightment[i])) {
                    weightmentCharge = "0";
                    map.put("weightment", "0");
                    map.put("weightmentOld", "0");
                } else {
                    weightmentCharge = weightment[i];
                    map.put("weightmentOld", weightmentOld[i]);
                    map.put("weightment", weightment[i]);
                }

                System.out.println("the update toll and detaintion" + map);

                if ("RepoBilling".equals(billList)) {

                    if (("0".equals(shipNo)) && ("0".equals(billEntry)) && "0".equals(containrNo) && "0".equals(dest) && "0".equals(grenTax) && "0".equals(otherExpenses) && "0".equals(weightmentCharge) && "0".equals(billParty)) {
                        System.out.println("entered equal to Zero");
                    } else {
                        System.out.println("REPOBILLLING");
                        map.put("billType", "RepoBilling");
                        map.put("movementType", movementType);
                        System.out.println("entered not equal to Zero");
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertInvoiceEditLog", map);
                        System.out.println("the update toll and detaintion =" + status);
                    }
                }

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateShippingBillOrBillOfEntryForBilling(String consignment_order_id, String billOfEntryNo, String billingPartyId, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);

        int status = 0;
        try {

            map.put("consignmentOrderId", consignment_order_id);
            map.put("billOfEntryNo", billOfEntryNo);
            //    map.put("shippingLineNo", shippingLineNo);
            map.put("billingPartyId", billingPartyId);

            System.out.println("the update bill of entry or shipping bill no" + map);

            if (!"0".equals(billOfEntryNo) && !"".equals(billOfEntryNo) && billOfEntryNo != null) {

                status = (Integer) getSqlMapClientTemplate().update("trip.updateBillOfEntryNo", map);
            }
//        if( !"0".equals(shippingLineNo) && !"".equals(shippingLineNo) && shippingLineNo!=null ){
//        status = (Integer) getSqlMapClientTemplate().update("trip.updateShippingBillNo", map);
//        }
            status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingParty", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTripOtherExpense(String tripId, String[] tripExpenseId, String[] expenses, String[] netexpenses, String[] expenseId, String[] expenseType, String totalRevenue, String[] oldExpenses, int userId, String borneBys) {
        Map map = new HashMap();

        map.put("userId", userId);
        map.put("tripSheetId", tripId);
        String totalRevenues = "";
        String oldExpense = "";
        String expense = "";
        int status = 0;
        try {
            for (int i = 0; i < tripExpenseId.length; i++) {
                map.put("tripExpenseId", tripExpenseId[i]);
                map.put("expenseId", expenseId[i]);
                map.put("oldExpense", oldExpenses[i]);
                map.put("expenses", expenses[i]);
                map.put("netexpenses", netexpenses[i]);

                System.out.println("the updateTripOtherExpense:" + map);
                // for double twenty  contanier,we need to  split the container for each trip id

                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpenseAfterClosure", map);
                System.out.println("updateTripOtherExpense" + status);

            }

            System.out.println("the expesne updated=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateTripAddexpense(String expenseId, String expenseDate, String expensesType, String marginValue, String taxExpenses, String expenseValue, String tripSheetId, String driverNameex, String remarks, String totalExpenses, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);

        int status = 0;
        try {

            map.put("expenseId", expenseId);
            map.put("expenseDate", expenseDate);
            map.put("expensesType", expensesType);
            map.put("marginValue", "0");
            map.put("taxExpenses", "0");
            map.put("expenseValue", expenseValue);
            map.put("driverNameex", "0");
            map.put("tripSheetId", tripSheetId);
            map.put("totalExpenses", totalExpenses);
            map.put("remarks", remarks);
            String[] temp = null;
            temp = expenseDate.split("-");
            map.put("expenseDate", temp[2] + "-" + temp[1] + "-" + temp[0] + " 00:00:00");
            System.out.println("the updateTripAddexpense:" + map);
            // for double twenty  contanier,we need to  split the container for each trip id
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripAddExpenseAfterClosure", map);

            System.out.println("the expesne updated=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripAddExpenseAfterClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int saveAdvanceGrNo(String noOfGr, String customerId, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);
        int loopcount = Integer.parseInt(noOfGr);

        int tempGrNo = 0;
        int insertTripGr = 0;
        String grNo = "";
        try {
            for (int i = 0; i < loopcount; i++) {

                System.out.println("the getGrNoSequence:" + map);

                tempGrNo = (Integer) getSqlMapClientTemplate().insert("trip.getGrNoSequence", map);
                System.out.println("tempGrNo" + tempGrNo);
                grNo = String.valueOf(tempGrNo);
                if (grNo.length() == 1) {
                    grNo = "000000" + grNo;
                    System.out.println("grno 1.." + grNo);
                } else if (grNo.length() == 2) {
                    grNo = "00000" + grNo;
                    System.out.println("grno lenght 2.." + grNo);
                } else if (grNo.length() == 3) {
                    grNo = "0000" + grNo;
                    System.out.println("grno lenght 3.." + grNo);
                } else if (grNo.length() == 4) {
                    grNo = "000" + grNo;
                } else if (grNo.length() == 5) {
                    grNo = "00" + grNo;
                } else if (grNo.length() == 6) {
                    grNo = "0" + grNo;
                }
                map.put("tripGr", grNo);
                map.put("customerId", customerId);
                map.put("blockStatus", "Y");
                map.put("used", "N");
                System.out.println("map:" + map);
                insertTripGr = (Integer) getSqlMapClientTemplate().update("trip.insertTripGr", map);

            }

            System.out.println("the getGrNoSequence inserted=" + insertTripGr);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return insertTripGr;
    }

    public int saveCancelledGrNo(String[] grIds, String[] activeInds, String customerId, String remarks, int userId) {
        Map map = new HashMap();
        int insertTripGr = 0;
        map.put("userId", userId);
        map.put("customerId", customerId);
        map.put("remarks", remarks);

        try {
            System.out.println(" activeInds.length" + activeInds.length);
            for (int i = 0; i < activeInds.length; i++) {
                if (activeInds[i].equals("Y")) {
                    map.put("grId", grIds[i]);
                    System.out.println("map valuei is " + map);
                    insertTripGr = (Integer) getSqlMapClientTemplate().update("trip.saveCancelledGrNo", map);
                }
            }

            System.out.println("the getGrNoSequence inserted=" + insertTripGr);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return insertTripGr;
    }

    public ArrayList getAdvanceGrDetails() {
        Map map = new HashMap();

        ArrayList advanceGrDetails = new ArrayList();
        try {

            advanceGrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAdvanceGrDetails", map);

            System.out.println("advanceGrDetails size=" + advanceGrDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return advanceGrDetails;
    }

    public ArrayList getBlockedAdvanceGrDetails(String customerId) {
        Map map = new HashMap();

        ArrayList advanceGrDetails = new ArrayList();
        try {
            map.put("customerId", customerId);
            System.out.println(" map is" + map);
            advanceGrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBlockedAdvanceGrDetails", map);

            System.out.println("advanceGrDetails size=" + advanceGrDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBlockedAdvanceGrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBlockedAdvanceGrDetails", sqlException);
        }
        return advanceGrDetails;
    }

    public ArrayList getCancelGrDetails() {
        Map map = new HashMap();

        ArrayList cancelGrDetails = new ArrayList();
        try {

            cancelGrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCancelGrDetails", map);

            System.out.println("getCancelGrDetails size=" + cancelGrDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return cancelGrDetails;
    }

    public ArrayList getAdvanceGrDetailsForPlanning(String customerId) {
        Map map = new HashMap();
        ArrayList advanceGrDetails = new ArrayList();
        try {
            map.put("customerId", customerId);
            String[] customerIds = customerId.split(",");
            List customerIdVal = new ArrayList(customerIds.length);
            for (int i = 0; i < customerIds.length; i++) {
                System.out.println("value:" + customerIds[i]);
                customerIdVal.add(customerIds[i]);
            }
            map.put("customerId", customerIdVal);

            System.out.println("map getAdvanceGrDetailsForPlanning---:" + map);
            advanceGrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAdvanceGrDetailsForPlanning", map);
            System.out.println("advanceGrDetails size=" + advanceGrDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return advanceGrDetails;
    }

    public int updatetripGR(String tripId, int userId, String grNo, SqlMapClient session) {
        Map map = new HashMap();
        int update = 0;
        map.put("userId", userId);
        map.put("tripId", tripId);
        map.put("grNo", grNo);
        System.out.println("map for advance GR:" + map);
        try {
            update = (Integer) session.update("trip.updateTripAdvanceGr", map);
            System.out.println("update" + update);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return update;
    }

    public int updatetripGR(String tripId, int userId, String grNo) {
        Map map = new HashMap();
        int update = 0;
        map.put("userId", userId);
        map.put("tripId", tripId);
        map.put("grNo", grNo);
        System.out.println("map for advance GR:" + map);
        try {
            update = (Integer) getSqlMapClientTemplate().update("trip.updateTripAdvanceGr", map);
            System.out.println("update" + update);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return update;
    }

    public String getTotalExpense(TripTO tripTO) {
        Map map = new HashMap();
        String totalExpense = "";
        ArrayList advanceGrDetails = new ArrayList();
        try {
            map.put("tripId", tripTO.getTripSheetId());
            totalExpense = (String) getSqlMapClientTemplate().queryForObject("trip.totalExpesne", map);

            System.out.println("totalExpense =" + totalExpense);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return totalExpense;
    }

    public String getKMreadings(TripTO tripTO, String activeVehicle) {
        Map map = new HashMap();
        String kmDetails = "";
        try {
            map.put("tripId", tripTO.getTripSheetId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("activeVehicle", activeVehicle);
            kmDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getKMreadings", map);
            System.out.println("kmDetails =" + kmDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return kmDetails;
    }

    public int saveTripCash(TripTO tripTo, String paidCash, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", userId);
            map.put("paidCash", paidCash);
            map.put("tripId", tripTo.getTripId());
            String GrNo = (String) getSqlMapClientTemplate().queryForObject("trip.getGrNo", map);
            map.put("grNo", GrNo);
            System.out.println("map for cash:" + map);

            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripCash", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateCashBalance", map);

            System.out.println("the saveTripCash inserted=" + status);

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int saveTripCash(TripTO tripTo, String paidCash, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", userId);
            map.put("paidCash", paidCash);
            map.put("tripId", tripTo.getTripId());
            String GrNo = (String) session.queryForObject("trip.getGrNo", map);
            map.put("grNo", GrNo);
            System.out.println("map for cash:" + map);

            status = (Integer) session.update("trip.insertTripCash", map);
            status = (Integer) session.update("trip.updateCashBalance", map);

            System.out.println("the saveTripCash inserted=" + status);

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateBillingPartyForBilling(String[] tripId, String[] consignmentOrderIds, String[] billOfEntryNo, String billingParty, String billingPartyId, String movementType, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("billingParty", billingParty);
                map.put("billingPartyId", billingPartyId);

                System.out.println("the update bill of entry or shipping bill no" + map);

                status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingParty", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingPartyForTrip", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public int updateBillingPartyForRepoBilling(String[] tripId, String[] consignmentOrderIds, String billingParty, String billingPartyId, String movementType, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);

        int status = 0;
        try {
            for (int i = 0; i < tripId.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("billingParty", billingParty);
                map.put("billingPartyId", billingPartyId);

                System.out.println("the update bill of entry or shipping bill no" + map);

                status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingParty", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateInvoiceBillingPartyForTrip", map);
                System.out.println("updateShippingBillOrBillOfEntryForBilling=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return status;
    }

    public ArrayList getEmailList() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList emailList = new ArrayList();
        try {
            emailList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getEmailListDetails", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getEmailList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getEmailList", sqlException);
        }
        return emailList;
    }

    public int updateMailStatus(TripTO tripTO) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int updateStatus = 0;
        map.put("mailSendingId", tripTO.getMailSendingId());
        map.put("mailDeliveredRespone", tripTO.getMailDeliveredResponse());
        map.put("mailDeliveredStatus", tripTO.getMailDeliveredStatusId());
        try {
            updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateMailStatus", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateMailStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "updateMailStatus", sqlException);
        }
        return updateStatus;
    }

    public int saveChangeVehicleAfterTripStart(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int updateTripMaster = 0;
        int updateDriverDetails = 0;
        int insertDriverDetails = 0;
        int updateTripVehicle = 0;

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date curDate = new Date();
        System.out.println(dateFormat.format(curDate));
        String updateDate = dateFormat.format(curDate);
        System.out.println("driverChangeDate---"+tripTO.getDriverChangeDate());
        map.put("driverChangeDate", tripTO.getDriverChangeDate());
        map.put("tripId", tripTO.getTripId());
        map.put("tripSheetId", tripTO.getTripId());
        map.put("tripIds", tripTO.getTripIdNew());
//        map.put("vehicleNo", tripTO.getVehicleNo());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("vehicleType", tripTO.getVehicleType());
        map.put("startKm", tripTO.getOdometerReading());
        map.put("startHm", 0.00);

        if ("".equals(tripTO.getVehicleCapUtil())) {
            map.put("vehicleCapUtil", 0.00);
        } else {
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
        }
        String vendId[] = tripTO.getVendorId().split("~");
        map.put("vehicleTonnage", tripTO.getVehicleTonnage());
        map.put("vendorId", vendId[0]);

        map.put("driverName", tripTO.getDriverName());
        map.put("driverMobile", tripTO.getDriverMobile());
        map.put("driverId", tripTO.getDriver1Id());
        map.put("userId", userId);
        System.out.println("vendorId----"+vendId[0]);
        try {

            String veh = tripTO.getVehicleNo();
            System.out.println("vehicleNO###########" + veh);
            if (tripTO.getVehicleNo().contains("~")) {
                map.put("vehicleNo", null);
                map.put("hireVehicleNo", null);
            } else {
                map.put("vehicleNo", tripTO.getVehicleNo());
                map.put("hireVehicleNo", tripTO.getVehicleNo());
            }
//            if(!tripTO.getOldVehicleId().equals(tripTO.getVehicleId())){

            updateTripVehicle = (Integer) getSqlMapClientTemplate().update("trip.updateTripVehicle", map);
            System.out.println("updateTripVehicle###==" + updateTripVehicle);
            int oldVehAvailUpdate = (Integer) getSqlMapClientTemplate().update("trip.oldVehAvailUpdate", map);
            System.out.println("oldVehAvailUpdate------"+oldVehAvailUpdate);
            
            map.put("activeVehicle", "N");
            
            if(tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
                map.put("vehicleId", "0");
            }else if(!"1".equals(vendId[0])){
                map.put("vehicleId", "0");
            }else{
                map.put("vehicleId", tripTO.getVehicleId());
            }
            
            System.out.println("map @@@###= " + map);
//            int offloadStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkVehChangeOffloadStatus", map); 
            map.put("offloadStatus", "2");
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripVehicleDetails", map);
            System.out.println("status###==" + status);
//            }

            String[] tempVendorId = tripTO.getVendorId().split("~");
            map.put("vendorId", tempVendorId[0]);

            updateTripMaster = (Integer) getSqlMapClientTemplate().update("trip.updateTripMaster", map);
            System.out.println("updateTripMaster###==" + updateTripMaster);

            map.put("remarks", "change vehicle");
            map.put("tripSheetId", tripTO.getTripId());
            map.put("statusId", "10");
            map.put("updateDate", tripTO.getDriverChangeDate());
            System.out.println("#$##map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatus", map);

//            map.put("vehicleId", tripTO.getVehicleId());
//            map.put("driverName", tripTO.getDriverName());
//            map.put("driverId", tripTO.getDriver1Id());
            System.out.println("updateDriverDetailsMAP@@@" + map);
            updateDriverDetails = (Integer) getSqlMapClientTemplate().update("trip.updateDriverDetails", map);
            System.out.println("updateDriverDetails###==" + updateDriverDetails);

            insertDriverDetails = (Integer) getSqlMapClientTemplate().update("trip.insertDriverDetails", map);
            System.out.println("insertDriverDetails###==" + insertDriverDetails);
            int updateFuel = 0;
            if ("Y".equals(tripTO.getNewSlip())) {
                updateFuel = (Integer) getSqlMapClientTemplate().update("trip.updateFuelDetails", map);
                updateFuel = (Integer) getSqlMapClientTemplate().update("trip.updateFuelSNo", map);
            }
            System.out.println("vehicleId-offload---"+tripTO.getVehicleId());
            System.out.println("vehicleId-Old vehicle - offload---"+tripTO.getVehicleId());
            
            
            int vehicleAvailUpdate = (Integer) getSqlMapClientTemplate().update("trip.updateChangeVehicleAvailabilityForFreeze", map);
            System.out.println("vehicleAvailUpdate------"+vehicleAvailUpdate);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("status Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "status", sqlException);
        }
        return status;

    }

    public int updateEstimateRevenue(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("estimateRevenue", tripTO.getEstimatedRevenue());

        map.put("userId", userId);
        System.out.println("the updateTripAdance" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateEstimateRevenue", map);
            System.out.println("updateEstimateRevenue=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateEstimateRevenue" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateEstimateRevenue", sqlException);
        }

        return status;
    }

    public Integer deleteExpenseIds(TripTO tripTO, int userId) {
        Map map = new HashMap();

        System.out.println("map@@@" + tripTO.getTripExpenseIds());
        String[] tripExpenses1 = tripTO.getTripExpensesId();
        String[] expenseType = tripTO.getExpenseTypes();
        String[] expenses = tripTO.getExpenses();

        String[] tripExpenses = tripTO.getTripExpenseIds().split(",");
        List tripExpensesList = new ArrayList(tripExpenses.length);
        for (int i = 0; i < tripExpenses.length; i++) {
            System.out.println("value:" + tripExpenses[i]);
            tripExpensesList.add(tripExpenses[i]);
        }
        map.put("tripExpensesList", tripExpensesList);
        Integer status = 0;
        try {
            // Delete invoiceDetail & invoice header For Bill Not To Customer
            status = (Integer) getSqlMapClientTemplate().update("trip.deleteExpenseIds", map);
            System.out.println("status@@@" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("status Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "status List", sqlException);
        }

        return status;
    }

    public int updateAdvanceGrNo(String noOfGr, String grNos, String grId, String customerId, int userId) {
        Map map = new HashMap();

        map.put("userId", userId);
        int loopcount = Integer.parseInt(noOfGr);
        System.out.println("loopcount@@@" + loopcount);
        int tempGrNo = 0;
        int insertTripGr = 0;
//        String grNo = "";
        try {
            for (int i = 0; i < loopcount; i++) {
                String[] grIds = grId.split(",");
                String[] grNo = grNos.split(",");

                map.put("grId", grIds[i]);
                map.put("tripGr", grNo[i]);
                map.put("customerId", customerId);
                map.put("blockStatus", "Y");
                map.put("used", "N");
                map.put("userId", userId);
                System.out.println("map:" + map);
                insertTripGr = (Integer) getSqlMapClientTemplate().update("trip.updateAdvanceTripGr", map);

            }

            System.out.println("the getGrNoSequence inserted=" + insertTripGr);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheetDuringClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheetDuringClosure List", sqlException);
        }

        return insertTripGr;
    }

    public ArrayList getAdvanceTripExpense(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getAdvanceTripExpense = new ArrayList();
        try {
            map.put("tripId", tripTO.getTripId());
            System.out.println("closed Trip map is::" + map);
            getAdvanceTripExpense = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAdvanceTripExpense", map);
            System.out.println("getAdvanceTripExpense.size() = " + getAdvanceTripExpense.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getAdvanceTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getAdvanceTripExpense List", sqlException);
        }
        return getAdvanceTripExpense;
    }

    public String getAdvanceVoucherNoSequence(TripTO tripTO) {
        Map map = new HashMap();
        int advanceVoucherNoSequence = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        try {
            advanceVoucherNoSequence = (Integer) getSqlMapClientTemplate().insert("trip.advanceVoucherNoSequence", map);
            System.out.println("advanceVoucherNoSequence=" + advanceVoucherNoSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("advanceVoucherNoSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "advanceVoucherNoSequence", sqlException);
        }
        return advanceVoucherNoSequence + "";
    }

    public String getAdvanceVoucherNoSequence(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int advanceVoucherNoSequence = 0;
        map.put("tripId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        try {
            advanceVoucherNoSequence = (Integer) session.insert("trip.advanceVoucherNoSequence", map);
            System.out.println("advanceVoucherNoSequence=" + advanceVoucherNoSequence);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("advanceVoucherNoSequence Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "advanceVoucherNoSequence", sqlException);
        }
        return advanceVoucherNoSequence + "";
    }

    public ArrayList getTripExpensePrintDetails(TripTO tripTO) {
        Map map = new HashMap();

        map.put("tripId", tripTO.getTripId());

        System.out.println("map for print Trip List = " + map);
        ArrayList tripDetails = new ArrayList();
        try {
            tripDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripExpensePrintDetails", map);
            System.out.println("getTripExpensePrintDetails size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpensePrintDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpensePrintDetails List", sqlException);
        }

        return tripDetails;
    }

    public int updateTransporter(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("vendorId", tripTO.getTransporter());

        map.put("userId", userId);
        System.out.println("the updateTransporter" + map);
        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTransporter", map);
            System.out.println("updateTransporter=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTransporter" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTransporter", sqlException);
        }

        return status;
    }

    public ArrayList getMapLocationList(String routeId) {
        Map map = new HashMap();

        ArrayList tripClosureDetails = new ArrayList();
        map.put("routeId", routeId);
        System.out.println("getMapLocationList" + map);
        try {
            tripClosureDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getMaplocation", map);
            System.out.println("getMapLocationList size=" + tripClosureDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMapLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMapLocationList List", sqlException);
        }

        return tripClosureDetails;
    }

    public int getTripExistInBilling(String[] tripId) {
        Map map = new HashMap();
        int status = 0;
        try {
            List tripIds = new ArrayList(tripId.length);
            for (int i = 0; i < tripId.length; i++) {
                System.out.println("value:" + tripId[i]);
                tripIds.add(tripId[i]);
            }

            map.put("tripSheetIds", tripIds);

            System.out.println("getTripExistInBilling map" + map);
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripExistInBilling", map);
            System.out.println("status size is ::::" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExistInBilling Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExistInBilling", sqlException);
        }
        return status;
    }

    public int insertTripFuel(int userId, String returnTripId, int tripId, String bunkName, String bunkPlace, String fuelDate, String fuelAmount,
            String fuelLtrs, String fuelRemarks, String driverId, String slipNo, String tripFuelId, String vehicleId, String hireVehicleNo, SqlMapClient session) {
        Map map = new HashMap();
        int lastInsertedId = 0;
        try {
            String[] tempBunk = bunkName.split("~");
//            System.out.println("tempBunk[0] = " + tempBunk[0]);
//            System.out.println("tempBunk[1] = " + tempBunk[1]);
//            System.out.println("tempBunk[2] = " + tempBunk[2]);
            map.put("tripId", tripId);
            map.put("returnTripId", returnTripId);
            map.put("tripSheetId", tripId);
            map.put("bunkName", tempBunk[0]);
            map.put("bunkPlace", bunkPlace);
            map.put("fuelDate", fuelDate);
            map.put("fuelAmount", fuelAmount);
            map.put("fuelLtrs", fuelLtrs);
            map.put("fuelRemarks", fuelRemarks);
            map.put("createdBy", userId);
            map.put("slipNo", slipNo);
            map.put("tripFuelId", tripFuelId);
            map.put("vehicleId", vehicleId);
            if ("0".equals(vehicleId)) {
                map.put("hireVehilceNo", hireVehicleNo);
            }
            System.out.println("map in tripFuel " + map);
            if (tripFuelId == null || "".equals(tripFuelId)) {
                lastInsertedId = (Integer) session.update("operation.insertTripFuel", map);
                System.out.println("insertTripFuel -->" + lastInsertedId);

                //  --------------------------------- acc 1st row start --------------------------
                map.put("userId", userId);
                map.put("DetailCode", "1");
                map.put("voucherType", "%PAYMENT%");
                String code2 = (String) session.queryForObject("operation.getTripVoucherCode", map);
                String[] temp = code2.split("-");
                int codeval2 = Integer.parseInt(temp[1]);
                int codev2 = codeval2 + 1;
                String voucherCode = "PAYMENT-" + codev2;
                System.out.println("voucherCode = " + voucherCode);
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "PAYMENT");
                map.put("ledgerId", "37");
                map.put("particularsId", "LEDGER-29");
                map.put("amount", fuelAmount);
                map.put("Accounts_Type", "DEBIT");
                map.put("Remark", "Diesel Expense");
                map.put("Reference", returnTripId);
                map.put("SearchCode", tripId);
                if ("".equals(driverId) || driverId == null) {
                    map.put("driverId", "0");
                } else {
                    map.put("driverId", driverId);
                }

                System.out.println("map1 =---------------------> " + map);
                int status1 = (Integer) session.update("operation.insertTripAccountEntry", map);
                System.out.println("status1 = " + status1);
                //--------------------------------- acc 2nd row start --------------------------
                if (status1 > 0) {
                    //identify debit account
                    //if own driver, debit account is driver account
                    //if contract driver, the debit account is contract vendor account
                    String ledgerId = tempBunk[1];
                    String particularsId = tempBunk[2];

                    map.put("DetailCode", "2");
                    map.put("ledgerId", ledgerId);
                    map.put("particularsId", particularsId);
                    map.put("Accounts_Type", "CREDIT");
                    System.out.println("map2 =---------------------> " + map);
                    int status2 = (Integer) session.update("operation.insertTripAccountEntry", map);
                    System.out.println("status2 = " + status2);
                }
            } else {
                lastInsertedId = (Integer) session.update("operation.updateTripFuel", map);
                System.out.println("update:" + lastInsertedId);
            }
            map.put("tripSheetId", tripId);
            map.put("allocatedFuel", fuelLtrs);
            map.put("consumedFuel", fuelLtrs);
            map.put("recoveryFuel", 0);
            map.put("recoveryAmount", 0);
            map.put("fuelSlipNo", slipNo);
            map.put("fuelRemarks", fuelRemarks);
            int fuelRecovery = (Integer) session.update("trip.saveTripFuelRecovery", map);
            System.out.println("fuelRecovery=" + fuelRecovery);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return lastInsertedId;
    }

    public int manualAdvanceRequest(TripTO tripTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("hire", tripTO.getHire());
            map.put("balance", tripTO.getBalance());
            map.put("mamul", tripTO.getMamul());

            map.put("advancerequestamt", tripTO.getAdvancerequestamt());
            map.put("requeston", tripTO.getRequeston());
            map.put("requestremarks", tripTO.getRequestremarks());
            map.put("advanceRemarks", tripTO.getAdvanceRemarks());
            map.put("advTrnType", tripTO.getAdvTrnType());
            map.put("requeststatus", tripTO.getRequeststatus());
            map.put("tripid", tripTO.getTripId());
            map.put("tobepaidtoday", tripTO.getTobepaidtoday());
            map.put("batchType", tripTO.getBatchType());
            map.put("tripday", tripTO.getTripday());
            map.put("currencyId", tripTO.getCurrencyid());
            map.put("userId", userId);
            System.out.println("map val" + map);

            manualAdvanceRequest = (Integer) session.insert("operation.manualAdvanceRequest", map);
            System.out.println("manualAdvanceRequest==" + manualAdvanceRequest);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public int saveTripExpense(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripSheetId", tripTO.getTripId());
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());
            System.out.println("tripTO.getPrimaryDriverId()-DAO---" + tripTO.getPrimaryDriverId());
            map.put("employeeName", tripTO.getPrimaryDriverId());
            map.put("expenseType", "2");
            map.put("billMode", "0");
            map.put("activeValue", "0");
            map.put("taxPercentage", "0");
            map.put("expenseRemarks", "system Update");
            map.put("currency", "1");
            map.put("marginValue", "0");

            //            Toll Update
            map.put("netExpense", "0.00");
            map.put("expenses", "0.00");
//            map.put("netExpense", tripTO.getTollAmount());
//            map.put("expenses", tripTO.getTollAmount());
            map.put("expenseName", "1011");
            System.out.println("map value is:" + map);
            if (!"0.00".equals(tripTO.getTollAmount())) {
                int tollcheck = (Integer) session.queryForObject("trip.getTollForTrip", map);
                if (tollcheck != 0) {
                    status = (Integer) session.update("trip.updateTollForTrip", map);
                } else {
                    status = (Integer) session.update("trip.saveTripExpense", map);
                }
            }

            //  Pooja Expenses
            map.put("netExpense", "15");
            map.put("expenses", "15");
            map.put("expenseName", "1030");
            System.out.println("map value for pooja is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
            System.out.println("staus=" + status);

            //  Container lift on/off  Expenses
            map.put("netExpense", "0");
            map.put("expenses", "0");
            map.put("expenseName", "1034");
            System.out.println("map value for cont is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
            System.out.println("staus=" + status);

            //  weightment  Expenses
            map.put("netExpense", "0");
            map.put("expenses", "0");
            map.put("expenseName", "1014");
            System.out.println("map value for wegnt is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
            System.out.println("staus=" + status);

            //  toll cash  Expenses
            map.put("netExpense", "0");
            map.put("expenses", "0");
            map.put("expenseName", "1027");
            System.out.println("map value for wegnt is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
            System.out.println("staus=" + status);

            //  loading & unloading cash  Expenses
            map.put("netExpense", "0");
            map.put("expenses", "0");
            map.put("expenseName", "1035");
            System.out.println("map value for wegnt is:" + map);
            status = (Integer) session.update("trip.saveTripExpense", map);
            System.out.println("staus=" + status);

            //            Fuel Update
            map.put("netExpense", tripTO.getFuelLtrsCost());
            map.put("expenses", tripTO.getFuelLtrsCost());
            map.put("expenseType", "2");
            map.put("expenseName", "1010");
            System.out.println("tripTO.getFuelAmounts()=" + tripTO.getFuelLtrsCost());
//            map.put("netExpense", tripTO.getOtherExpense());
//            map.put("expenses", tripTO.getOtherExpense());
//            map.put("expenseName", "1025");
            System.out.println("map value is:" + map);

            if (!"0.00".equals(tripTO.getFuelLtrsCost())) {
                int fuelCheck = (Integer) session.queryForObject("trip.getFuelForTrip", map);
                if (fuelCheck != 0) {
                    status = (Integer) session.update("trip.updateFuelForTrip", map);
                } else {
                    status = (Integer) session.update("trip.saveTripExpense", map);
                }
            }
//            if (!"0.00".equals(tripTO.getOtherExpense())) {
//                int dalaCheck = (Integer) session.queryForObject("trip.getDalaForTrip", map);
//                if (dalaCheck != 0) {
//                    status = (Integer) session.update("trip.updateDalaForTrip", map);
//                } else {
//                    status = (Integer) session.update("trip.saveTripExpense", map);
//                }
//            }

            /*       //            Driver Batta Update
//             map.put("netExpense", tripTO.getDriverBatta());
//             map.put("expenses", tripTO.getDriverBatta());
//             map.put("expenseName", "1024");
//             System.out.println("map value is:" + map);
//             if (!"0.00".equals(tripTO.getDriverBatta())) {
//             int driverBhattacheck = (Integer) session.queryForObject("trip.getDriverBhattaForTrip", map);
//             if (driverBhattacheck != 0) {
//             status = (Integer) session.update("trip.updateDriverBhattaForTrip", map);
//             } else {
//             status = (Integer) session.update("trip.saveTripExpense", map);
//             }
//             }

             //            Misc  Update
             map.put("netExpense", tripTO.getMiscValue());
             map.put("expenses", tripTO.getMiscValue());
             map.put("expenseName", "1018");
             System.out.println("map value is:" + map);
             if (!"0.00".equals(tripTO.getMiscValue())) {
             int otherExpCheck = (Integer) session.queryForObject("trip.getOtherExpForTrip", map);
             if (otherExpCheck != 0) {
             status = (Integer) session.update("trip.updateOtherExpForTrip", map);
             } else {
             status = (Integer) session.update("trip.saveTripExpense", map);
             }
             }


             //            Trip Expense(RTO)  Update
             map.put("netExpense", tripTO.getTripExpense());
             map.put("expenses", tripTO.getTripExpense());
             map.put("expenseName", "1017");
             System.out.println("map value is:" + map);
             if (!"0.00".equals(tripTO.getTripExpense())) {
             int tripExpCheck = (Integer) session.queryForObject("trip.getTripExpForTrip", map);
             if (tripExpCheck != 0) {
             status = (Integer) session.update("trip.updateTripExpForTrip", map);
             } else {
             status = (Integer) session.update("trip.saveTripExpense", map);
             }
             }
             //            PerDayCost PCD  Update
             map.put("netExpense", tripTO.getPerDayCOst());
             map.put("expenses", tripTO.getPerDayCOst());
             map.put("expenseName", "1026");
             System.out.println("map value is:" + map);
             if (!"0.00".equals(tripTO.getPerDayCOst())) {
             int perDayCostCheck = (Integer) session.queryForObject("trip.getPerDayCostForTrip", map);
             if (perDayCostCheck != 0) {
             status = (Integer) session.update("trip.updateTripPCDForTrip", map);
             } else {
             status = (Integer) session.update("trip.saveTripExpense", map);
             }
             }
             */
            System.out.println("saveTripExpense=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripExpense", sqlException);
        }
        return status;
    }

    public ArrayList getContainerAvailability(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList containerAvailibility = new ArrayList();
        map.put("containerNo", tripTO.getContainerNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        System.out.println("getMapLocationList" + map);
        try {
            if (tripTO.getContainerNo() != null && !"".equals(tripTO.getContainerNo())) {
                containerAvailibility = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerAvailabilitySearch", map);
            } else {
                containerAvailibility = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerAvailability", map);
            }
            System.out.println("getContainerAvailability size=" + containerAvailibility.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMapLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMapLocationList List", sqlException);
        }

        return containerAvailibility;
    }

    public ArrayList getContainerVisibility(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList containerVisibility = new ArrayList();
        map.put("containerNo", tripTO.getContainerNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        System.out.println("getMapLocationList" + map);
        try {

            containerVisibility = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getContainerVisibility", map);

            System.out.println("getContainerAvailability size=" + containerVisibility.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMapLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMapLocationList List", sqlException);
        }

        return containerVisibility;
    }

    public ArrayList getComodityDetails(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList comodityDetails = new ArrayList();

        try {

            comodityDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getComodityDetails", map);

            System.out.println("getComodityDetails size=" + comodityDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMapLocationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMapLocationList List", sqlException);
        }

        return comodityDetails;
    }

    public int updateTripRoute(TripTO tripTO, int userId, String tripRouteCourseId, String pointType, String pointSequence, String routeInfo, String newReveneue) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("tripId", tripTO.getTripId());
            map.put("tripRouteCourseId", tripRouteCourseId);
            map.put("pointId", tripTO.getTripPointId());
            map.put("pointType", pointType);
            map.put("pointOrder", pointSequence);
            map.put("pointAddresss", "");
            map.put("pointPlanDate", "00-00-0000");
            map.put("pointPlanTime", "00:00:00");
            map.put("routeInfo", routeInfo);
            map.put("newRevenue", newReveneue);

            map.put("userId", userId);
            System.out.println("map val" + map);
            if (!"".equalsIgnoreCase(tripRouteCourseId) && !"0".equalsIgnoreCase(tripRouteCourseId)) {
                manualAdvanceRequest = (Integer) getSqlMapClientTemplate().update("trip.updateTripRoute", map);
            } else if ("0".equalsIgnoreCase(tripRouteCourseId)) {
                int update = (Integer) getSqlMapClientTemplate().update("trip.updateTripRouteSequence", map);
                System.out.println("update" + update);
                if (update > 0) {
                    int insert = (Integer) getSqlMapClientTemplate().update("trip.insertTripRoute", map);
                }
            }
            int routeUpdate = (Integer) getSqlMapClientTemplate().update("trip.updateRouteInfo", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public int insertTripMasterLog(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("tripId", tripTO.getTripId());

            if (tripTO.getStatusId().equals("3")) {
                map.put("tripType", "3");
            } else {
                map.put("tripType", "1");
            }
            map.put("userId", userId);
            System.out.println("map val" + map);

            int routeUpdate = (Integer) getSqlMapClientTemplate().update("trip.insertTripMasterLog", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public ArrayList getTotalTripExpenseDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("driverId", tripTO.getDriverId());
        map.put("activeStatus", tripTO.getActiveStatus());
        ArrayList totalExpenseDetails = new ArrayList();
        try {
            System.out.println("map is ::" + map);

            int driverCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getDriverCountPerTrip", map);
            System.out.println("driverCount==" + driverCount);
            map.put("driverCount", driverCount);

            int vehicleCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getVehicleCountPerTrip", map);
            System.out.println("vehicleCount==" + vehicleCount);
            map.put("vehicleCount", vehicleCount);

            totalExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripOtherExpenseDetails", map);
            System.out.println("getGPSDetails size=" + totalExpenseDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalTripExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalTripExpenseDetails List", sqlException);
        }

        return totalExpenseDetails;
    }

    public String getFuelPrice(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripExpense = "";
        try {
            map.put("tripSheetId", tripSheetId);
            System.out.println(" map value is" + map);

            tripExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getDieselPrice", map);
            if ("".equals(tripExpense) || tripExpense == null) {
                String grDate = (String) getSqlMapClientTemplate().queryForObject("trip.getGrDateForDieselPrice", map);
                map.put("grDate", grDate);
                System.out.println(" map value is" + map);
                tripExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getDieselPriceNotAvailable", map);
            }
            System.out.println("getFuelPrice=" + tripExpense);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpense", sqlException);
        }
        return tripExpense;
    }

    public String getFuelExpense(String tripSheetId, TripTO tripTO) {
        Map map = new HashMap();
        String tripExpense = "";
        try {
            map.put("tripSheetId", tripSheetId);
            map.put("vehicleId", tripTO.getVehicleId());

            tripExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getFuelExpense", map);
            System.out.println("getFuelExpense=" + tripExpense);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripExpense", sqlException);
        }
        return tripExpense;
    }

    public int updateTripFuelAndRev(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;

        try {

            map.put("tripId", tripTO.getTripId());
            map.put("fuelAmount", tripTO.getTotalFuelValue());
            map.put("fuelLtrs", tripTO.getFuelLitres());
            map.put("orderExpense", tripTO.getEstimatedExpense());
            map.put("orderRevenue", tripTO.getEstimatedRevenue());

            System.out.println("map val" + map);

            status = (Integer) session.update("trip.updateTripFuel", map);
            status = (Integer) session.update("trip.updateTripMasterRevenue", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return status;
    }

    public int updateTripContainerDetails(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;

        String[] containerTypeId = null;
        String[] containerNo = null;
        String[] uniqueId = null;
        String sealNo[] = null;
        String grStatus[] = null;
        String orderId[] = null;

        try {

            containerTypeId = tripTO.getContainerTypeId().split(",");
            containerNo = tripTO.getContainerNo().split(",");
            uniqueId = tripTO.getUniqueId().split(",");
            sealNo = tripTO.getSealNo().split(",");
            grStatus = tripTO.getGrStatus().split(",");
            orderId = tripTO.getOrderId().split(",");

            for (int j = 0; j < containerNo.length; j++) {
                map.put("tempContainer", containerNo[j]);
                map.put("tempTripContainerId", uniqueId[j]);
                map.put("sealNo", sealNo[j]);
                map.put("tempConsignmentContainerId", orderId[j]);
                System.out.println("the updateTripContainerDetails map111" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripContainer", map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentContainer", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripContainerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripContainerDetails", sqlException);
        }
        return status;
    }

    public int deleteTripContainer(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;

        try {

            map.put("tripId", tripTO.getTripId());

            System.out.println("map val" + map);

            status = (Integer) session.update("trip.deleteTripContainer", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return status;
    }

    public int deleteTripConsignment(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;

        try {

            map.put("tripId", tripTO.getTripId());

            System.out.println("map val" + map);

            status = (Integer) session.update("trip.deleteTripConsignment", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return status;
    }

    public ArrayList getConsignmentContainerList(TripTO tripTO, String consignmentOrderId,
            String tripPointIds, String movementType, String containerQuantity) {
        Map map = new HashMap();
        ArrayList containerTypeDetails = new ArrayList();
        try {

            map.put("tripSheetId", tripTO.getTripSheetId());
            String[] consignmentNos = consignmentOrderId.split(",");
            List consignmentOrderNos = new ArrayList(consignmentNos.length);
            for (int i = 0; i < consignmentNos.length; i++) {
                System.out.println("value:" + consignmentNos[i]);
                consignmentOrderNos.add(consignmentNos[i]);
            }

//            List tripPointIds2 = new ArrayList(tripPointId2.length);
//            for (int i = 0; i < tripPointId2.length; i++) {
//                System.out.println("value:" + tripPointId2[2]);
//                tripPointIds2.add(tripPointId2[2]);
//            }
            System.out.println("containerQuantity" + containerQuantity);
            map.put("consignmentOrderNos", consignmentOrderNos);

            //String movementType="7";
            if ("7".equals(movementType)) {
                map.put("tripPointId2", tripPointIds);
                if ("1".equals(containerQuantity)) {
                    System.out.println("40 type Containers Only");
                    map.put("containerType", 2);
                } else {
                    System.out.println("20 type Containers Only");
                    map.put("containerType", 1);
                }
                System.out.println(" Trip map 1 is::" + map);
                containerTypeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAllConsignmentContainerList", map);
            } else {
                System.out.println(" Trip map 2 is::" + map);
                containerTypeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentContainerList", map);
            }
            System.out.println("getConsignmentContainerList.size() = " + containerTypeDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return containerTypeDetails;
    }

    public int updateHireVehicleDriverDetails(String tripId, String vehicleNo, String driverName, String driverMobile, String currRate, String activeVehicle) {
        Map map = new HashMap();
        int status = 0;

        try {

            map.put("tripId", tripId);
            map.put("vehicleNo", vehicleNo);
            map.put("driverName", driverName);
            map.put("driverMobile", driverMobile);
            map.put("currRate", currRate);
            map.put("activeVehicle", activeVehicle);
            System.out.println("map val" + map);

            status = (Integer) getSqlMapClientTemplate().update("trip.updateHireVehicle", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateHireDriver", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateHireLHC", map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripLHC", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return status;
    }

    public ArrayList getHireVehicleDriverDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("activeVehicle", tripTO.getActiveVehicle());
        map.put("hireVehicleNo", tripTO.getHireVehicleNo());
        System.out.println("SSSSSS" + map);
        ArrayList totalExpenseDetails = new ArrayList();
        try {
            totalExpenseDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getHireVehicleDriverDetails", map);
            System.out.println("getHireVehicleDriverDetails size=" + totalExpenseDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalTripExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalTripExpenseDetails List", sqlException);
        }

        return totalExpenseDetails;
    }

    public int updatePrint(String tripSheetId, String param) {

        Map map = new HashMap();
        int status = 0;
        map.put("tripId", tripSheetId);
        map.put("flag", param);

        try {
            System.out.println("tripSheetId=" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updatePrint", map);
            System.out.println("status master is=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getDriverNames(TripTO tripTO) {
        Map map = new HashMap();
        String driverName = tripTO.getDriverName() + "%";
        map.put("driverName", driverName);
        map.put("companyId", tripTO.getCompanyId());
        System.out.println("map==" + map);

        ArrayList drivers = new ArrayList();
        try {
            drivers = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverNames", map);
            System.out.println("drivers size=" + drivers.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("consignmentList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "consignmentList List", sqlException);
        }

        return drivers;
    }

    public ArrayList getVesselDetails(String ConsignmentOrderIds) {
        Map map = new HashMap();
        ArrayList VesselDetails = new ArrayList();

        //map.put("ConsignmentOrderIds", ConsignmentOrderIds);
        String[] consignmentNos = ConsignmentOrderIds.split(",");
        List consignmentOrderNos = new ArrayList(consignmentNos.length);
        for (int i = 0; i < consignmentNos.length; i++) {
            System.out.println("value:" + consignmentNos[i]);
            consignmentOrderNos.add(consignmentNos[i]);
        }
        map.put("ConsignmentOrderIds", consignmentOrderNos);
        System.out.println("map====ContainerDetailst" + map);

        try {
            System.out.println("map====ContainerDetailst" + map);
            VesselDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVesselDetails", map);
            System.out.println("VesselDetails===" + VesselDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return VesselDetails;
    }

    public int checkContainerPlannedStatus(TripTO tripTO) {
        Map map = new HashMap();
        int plannedstatus = 0;
        try {
            String[] consignmentNos = tripTO.getConsignmentOrderNos().split(",");
            List consignmentOrderNos = new ArrayList(consignmentNos.length);
            for (int i = 0; i < consignmentNos.length; i++) {
                System.out.println("value:" + consignmentNos[i]);
                consignmentOrderNos.add(consignmentNos[i]);
            }
            map.put("consignmentOrderNos", consignmentOrderNos);
            System.out.println("map====ContainerDetailst" + map);
            plannedstatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkContainerPlannedStatus", map);
            System.out.println("VesselDetails===" + plannedstatus);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "AjaxModelList", sqlException);
        }
        return plannedstatus;
    }

    public ArrayList getAllConsignmentContainerList(TripTO tripTO, String consignmentOrderId, String tripPointIds) {
        Map map = new HashMap();
        ArrayList containerTypeDetails = new ArrayList();
        try {

            map.put("tripSheetId", tripTO.getTripSheetId());
            String[] consignmentNos = consignmentOrderId.split(",");
            List consignmentOrderNos = new ArrayList(consignmentNos.length);
            for (int i = 0; i < consignmentNos.length; i++) {
                System.out.println("value:" + consignmentNos[i]);
                consignmentOrderNos.add(consignmentNos[i]);
            }
            String[] tripPointId2 = tripPointIds.split(",");
            List tripPointIds2 = new ArrayList(tripPointId2.length);
            for (int i = 0; i < tripPointId2.length; i++) {
                System.out.println("value:" + tripPointId2[2]);
                tripPointIds2.add(tripPointId2[2]);
            }
            map.put("tripPointIds2", tripPointIds2);
            map.put("consignmentOrderNos", consignmentOrderNos);
            System.out.println(" Trip map is::" + map);
            //containerTypeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentContainerList", map);
            containerTypeDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAllConsignmentContainerList", map);
            System.out.println("getConsignmentContainerList.size() = " + containerTypeDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContainerTypeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContainerTypeDetails List", sqlException);
        }
        return containerTypeDetails;
    }

    public String getPendingContainerCount(String orderId) {
        Map map = new HashMap();
        String retVal = null;
        try {
            map.put("orderId", Integer.parseInt(orderId));
            System.out.println("map====" + map);
            retVal = (String) getSqlMapClientTemplate().queryForObject("trip.getPendingContainerCount", map);
            System.out.println("getPendingContainerCount retVal===" + retVal);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPendingContainerCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getPendingContainerCount", sqlException);
        }
        return retVal;
    }

    public ArrayList getPNRData(String pnrNo) {
        Map map = new HashMap();
        ArrayList pnrDetails = new ArrayList();
        map.put("pnrNo", pnrNo);
        System.out.println("map = " + map);
        try {
            pnrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("sales.getPNRData", map);
            System.out.println("pnrDetails ------------ DAO --------- " + pnrDetails.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPNRData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPNRData", sqlException);
        }
        return pnrDetails;
    }

    public String getOtherVehicleTypeRates(String vehicleTypeId, String movementTypeId, String productCategory, String loadTypeId, String contractId, String sourceId, String point1Id, String point2Id, String destinationId) {
        Map map = new HashMap();
        String tripRevenue = "";
        String firstPointId = "";
        String finalPointId = "";

        String point3Id = "0";
        String point4Id = "0";

        map.put("firstPointId", sourceId);
        map.put("point1Id", point1Id);
        map.put("point2Id", point2Id);
        map.put("point3Id", point3Id);
        map.put("point4Id", point4Id);
        map.put("finalPointId", destinationId);
        map.put("contractId", contractId);
        map.put("movementType", movementTypeId);
        map.put("productCategory", productCategory);
        map.put("vehicleTypeId", vehicleTypeId);
        map.put("loadTypeId", loadTypeId);
        try {
            tripRevenue = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherVehicleTypeRates", map);
            System.out.println("tripRevenue value=" + tripRevenue);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDetails List", sqlException);
        }
        return tripRevenue;
    }

    public String getRouteExpenseDetailsForLCLVehicleType(String vehicleTypeId, String vendorId, String sourceId, String destinationId,
            String ownership, String noOfContianer, String containerTypeId, String movementType, String vehicleCategory, String orderTripType) {
        Map map = new HashMap();
        String returnValue = "1#" + "0" + "~0.00@" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0~0~0";
        try {
            String expense1 = "";
            map.put("countryId", "2");
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("vendorId", vendorId);
            map.put("sourceId", sourceId);
            map.put("point1Id", "");
            map.put("point2Id", "");
            map.put("point3Id", "");
            map.put("point4Id", "");
            map.put("destinationId", destinationId);
//                map.put("ownership", ownership);
            map.put("movementType", movementType);
            map.put("containerQty", "0");
            map.put("containerTypeId", "4");
            map.put("orderTripType", "1");

            String totalFuel = "";
            System.out.println("movementType" + movementType);
            if (movementType.equals("14") || movementType.equals("16")) { //if mofussil-LCL

                System.out.println("map=:" + map);
                returnValue = (String) getSqlMapClientTemplate().queryForObject("trip.getLCLVehicleExpenseForRoute", map);
                System.out.println("returnValue:" + returnValue);
                if (returnValue != null) {
                    System.out.println("ifff0000");
                    String[] tempRetValue = returnValue.split("~");
                    returnValue = tempRetValue[10] + "~" + tempRetValue[9] + "~" + tempRetValue[0] + "~0.00" + "~" + tempRetValue[2]
                            + "~" + tempRetValue[3] + "~" + tempRetValue[4] + "~" + tempRetValue[5] + "~" + tempRetValue[6];
                    totalFuel = tempRetValue[0] + "~0.00";
                    returnValue = "1#" + totalFuel + "@" + returnValue;
                } else {
                    System.out.println("else00000");
                    returnValue = "1#" + "0" + "~0.00@" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0" + "~" + "0~0~0";
                }
                System.out.println("returnValue in DAO=" + returnValue);

            }

            System.out.println("ownership===" + ownership + "vehicleCategory===" + vehicleCategory);
            if ("1".equals(vendorId)) {
                System.out.println("if--");
            } else {
                System.out.println("else....");
                System.out.println(" getTripExpenseForVendorLCLVehicle map is::" + map);

                if (movementType.equals("17")) {

                    expense1 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseForVendorLCLVehicleVessel", map);

                } else {
                    expense1 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseForVendorLCLVehicle", map);
                }

                System.out.println("expense1 " + expense1);
                if (expense1 == null) {
                    expense1 = "0~0~0";
                }

                returnValue = returnValue + "-" + expense1;
                System.out.println("returnValue====" + returnValue);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getVehicleChangeTripAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleChangeTripAdvanceDetails List", sqlException);
        }
        return returnValue;
    }

    //Tally
    public int updateStartTripSheetTally(TripTO tripTO, int userId, SqlMapClient session) throws SQLException {
        Map map = new HashMap();
        Map map1 = new HashMap();
        Map map2 = new HashMap();

        map.put("userId", userId);
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("companyId", tripTO.getCompanyId());

        System.out.println("the updatetripsheetdetailsTally" + map);
        int status = 0;
        int update = 0;
        String movementType = (String) session.queryForObject("trip.getConsignmentMovement", map);
        System.out.println("movementType-" + movementType);
        String ConsignmentIdList = (String) session.queryForObject("trip.getTripConsignment", map);
        System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
        String[] ConsignmentId = ConsignmentIdList.split(",");
        for (int i = 0; i < ConsignmentId.length; i++) {
            map.put("consignmentId", ConsignmentId[i]);
            System.out.println("the status1:" + status);
        }
        ArrayList tripDetails = new ArrayList();

        try {
            if ("12".equals(movementType) || "13".equals(movementType)) {
                tripDetails = (ArrayList) session.queryForList("trip.getTripDetailsListForTallyFCL", map);
                System.out.println(" start trip FCL= " + tripDetails.size());
            } else {
                tripDetails = (ArrayList) session.queryForList("trip.getTripDetailsListForTallyLCL", map);
                System.out.println(" start trip LCL= " + tripDetails.size());
            }
            int i = 0;
            int inserTally = 0;
            Iterator itr = tripDetails.iterator();
            TripTO tripTO1 = new TripTO();
            while (itr.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr.next();
                //  set Trip List
                map.put("tripNo", tripTO1.getTripCode());
                System.out.println("tripTO1.getTripCode()=" + tripTO1.getTripCode());
                map.put("tripDate", tripTO1.getTripDate());
                map.put("transactionDate", tripTO1.getTripDate());
                map.put("vehicleNo", tripTO1.getVehicleNo());
                map.put("driverName", tripTO1.getDriverName());
                map.put("advTrnType", tripTO1.getAdvTrnType());
                map.put("dlNo", "DL No " + tripTO1.getLicenseNo());
                map.put("lrNumber", tripTO1.getLrNumber());
                map.put("movementType", tripTO1.getMovementType());
                if ("12".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "EXP");
                    map.put("cargoType", "FCL");
                }
                if ("13".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "IMP");
                    map.put("cargoType", "FCL");
                }
                if ("14".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "IMP");
                    map.put("cargoType", "LCL");
                }
                if ("15".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "ICD");
                    map.put("cargoType", "ICD");
                }
                if ("16".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "EXP");
                    map.put("cargoType", "LCL");
                }
                map.put("containerTypeId", tripTO1.getContainerTypeId());
                if ("1".equals(tripTO1.getContainerTypeId())) {
                    map.put("containerType", "20");
                } else if ("2".equals(tripTO1.getContainerTypeId())) {
                    map.put("containerType", "40");
                } else {
                    map.put("containerType", "0");
                }
                map.put("containerNo", tripTO1.getContainerNo());
                map.put("expenseId", tripTO.getTripSheetId());
                map.put("conPackages", tripTO1.getContainerPkgs());
                map.put("conWeight", tripTO1.getContainerWgts());
                map.put("conVolume", tripTO1.getContainerVolume());
                map.put("advanceAmount", tripTO1.getAdvanceAmount());
                map.put("transactionId", tripTO1.getTransactionId());
                map.put("ownerShip", tripTO1.getOwnerShip());
                if ("Own".equals(tripTO1.getOwnerShip())) {
                    map.put("transactionType", "Suspense");
                    map.put("transactionName", tripTO1.getTallyName());
                } else {
                    map.put("transactionType", "Hire Advance");
                    map.put("transactionName", "Hire Advance on " + tripTO1.getOwnerShip());
                }

                if (i == 0) {
                    inserTally = (Integer) session.insert("trip.insertTallyAdvanceList", map);
                    System.out.println("inserTally==" + inserTally);
                }
                map.put("tripListId", inserTally);
                status = (Integer) session.insert("trip.insertTallyAdvanceContainer", map);
                System.out.println("push-2-" + status);

                i++;
                System.out.println("ii == " + i);
            }
            System.out.println("map for tally Master:" + map);

            status = (Integer) session.insert("trip.insertTallyAdvanceVehicle", map);
            System.out.println("push-1-" + status);
            status = (Integer) session.insert("trip.insertTallyAdvanceNameMapping", map);
            System.out.println("push-3-" + status);
            status = (Integer) session.insert("trip.insertTallyAdvanceAccount", map);
            System.out.println("push-4-" + status);
            status = (Integer) session.insert("trip.insertTallyAdvanceAccDetails", map);
            System.out.println("push-5-" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }

        return status;
    }

    public int updateTripClosureDetailsTally(TripTO tripTO, int userId, SqlMapClient session) throws SQLException {
        Map map = new HashMap();
        Map map1 = new HashMap();
        Map map2 = new HashMap();

        map.put("userId", userId);
        map.put("tripSheetId", tripTO.getTripId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("vehicleId", tripTO.getVehicleId());

        System.out.println("the update trip closure tally" + map);
        int status = 0;
        int update = 0;
        String movementType = (String) session.queryForObject("trip.getConsignmentMovement", map);
        System.out.println("movementType-" + movementType);
        String ConsignmentIdList = (String) session.queryForObject("trip.getTripConsignment", map);
        System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
        String[] ConsignmentId = ConsignmentIdList.split(",");
        for (int i = 0; i < ConsignmentId.length; i++) {
            map.put("consignmentId", ConsignmentId[i]);
            System.out.println("the status1:" + status);
        }
        ArrayList tripDetails = new ArrayList();
        ArrayList tripExpenseDetails = new ArrayList();

        try {
            if ("12".equals(movementType) || "13".equals(movementType)) {
                tripDetails = (ArrayList) session.queryForList("trip.getTripDetailsListForTallyAllfcl", map);
                System.out.println(" start trip FCL= " + tripDetails.size());
            } else {
                tripDetails = (ArrayList) session.queryForList("trip.getTripDetailsListForTallyAlllcl", map);
                System.out.println(" start trip LCL= " + tripDetails.size());
            }
            System.out.println("tripDetails size=" + tripDetails.size());
            Iterator itr = tripDetails.iterator();
            TripTO tripTO1 = new TripTO();
            while (itr.hasNext()) {
                tripTO1 = new TripTO();
                tripTO1 = (TripTO) itr.next();
                //  set Trip List
                map.put("tripNo", tripTO1.getTripCode());
                System.out.println("tripTO1.getTripCode()=" + tripTO1.getTripCode());
                map.put("tripDate", tripTO1.getTripDate());
                map.put("vehicleNo", tripTO1.getVehicleNo());
                map.put("driverName", tripTO1.getDriverName());
                map.put("advTrnType", tripTO1.getDriverName());
                map.put("dlNo", "DL No " + tripTO1.getLicenseNo());
                map.put("lrNumber", tripTO1.getLrNumber());
                map.put("movementType", tripTO1.getMovementType());
                if ("12".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "EXP");
                    map.put("cargoType", "FCL");
                }
                if ("13".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "IMP");
                    map.put("cargoType", "FCL");
                }
                if ("14".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "IMP");
                    map.put("cargoType", "LCL");
                }
                if ("15".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "ICD");
                    map.put("cargoType", "ICD");
                }
                if ("16".equals(tripTO1.getMovementType())) {
                    map.put("transportType", "EXP");
                    map.put("cargoType", "LCL");
                }
                map.put("containerTypeId", tripTO1.getContainerTypeId());
                if ("1".equals(tripTO1.getContainerTypeId())) {
                    map.put("containerType", "20");
                } else if ("2".equals(tripTO1.getContainerTypeId())) {
                    map.put("containerType", "40");
                } else {
                    map.put("containerType", "0");
                }
                map.put("containerNo", tripTO1.getContainerNo());
                map.put("conPackages", tripTO1.getContainerPkgs());
                map.put("conWeight", tripTO1.getContainerWgts());
                map.put("conVolume", tripTO1.getContainerVolume());
                map.put("ownerShip", tripTO1.getOwnerShip());
                map.put("transactionType", "Trip Expense");

                System.out.println("map for tally Master:" + map);
                int inserTally = 0;
//                inserTally = (Integer) session.insert("trip.insertTallyAdvanceList", map);
//                System.out.println("inserTally=="+inserTally);
                map.put("tripListId", inserTally);

//                status = (Integer) session.insert("trip.insertTallyAdvanceVehicle", map);
//                System.out.println("push-1-"+status);
//
//                status = (Integer) session.insert("trip.insertTallyAdvanceContainer", map);
//                System.out.println("push-2-"+status);
                tripExpenseDetails = (ArrayList) session.queryForList("trip.getTripExpenseDetailsListForTally", map);
                System.out.println(" start trip LCL= " + tripExpenseDetails.size());
                Iterator itr1 = tripExpenseDetails.iterator();
                TripTO tripTO2 = new TripTO();
                while (itr1.hasNext()) {
                    tripTO2 = new TripTO();
                    tripTO2 = (TripTO) itr1.next();
                    System.out.println("tripTO1.getTripCode()=" + tripTO1.getDriverName());
                    map.put("transactionId", tripTO.getTripId());
//                    map.put("transactionId", tripTO2.getTripExpenseId());
                    map.put("transactionName", tripTO2.getExpenseName() + " " + tripTO1.getVehTallyName());
                    map.put("expenseId", tripTO2.getExpenseId());
                    map.put("transactionDate", tripTO2.getExpenseDate());
                    map.put("advanceAmount", tripTO2.getExpenseValue());

                    System.out.println("map is ==" + map);

                    status = (Integer) session.insert("trip.insertTallyAdvanceNameMapping", map);
                    System.out.println("push-12-" + status);

                    status = (Integer) session.insert("trip.insertTallyAdvanceAccDetails", map);
                    System.out.println("push-14-" + status);

                }
            }
            if ("Own".equals(tripTO1.getOwnerShip())) {
                map.put("transactionName", tripTO1.getTallyName());
            }/*else{
             map.put("transactionName", "Hire Advance on "+tripTO1.getOwnerShip());
             }*/

            String totalExpAmt = (String) session.queryForObject("trip.getTotalTripExpAmt", map);
            System.out.println("totalExpAmt-watchhhhhh-" + totalExpAmt);
            map.put("advanceAmount", totalExpAmt);

            status = (Integer) session.insert("trip.insertTallyAdvanceAccount", map);
            System.out.println("push-13-" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStartTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStartTripSheet List", sqlException);
        }

        return status;
    }

    public int saveAddLrUpdate(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("userId", tripTO.getUserId());
            String ConsignmentOrderIds = tripTO.getConsignmentId();
            String[] consignmentNos = ConsignmentOrderIds.split(",");

            List consignmentOrderNos = new ArrayList(consignmentNos.length);
            for (int i = 0; i < consignmentNos.length; i++) {
                System.out.println("value:" + consignmentNos[i]);
                consignmentOrderNos.add(consignmentNos[i]);
            }
            String consignmentId = (String) consignmentOrderNos.get(0);
            System.out.println("consignmentId--" + consignmentId);
            map.put("consignmentOrderIds", consignmentOrderNos);
            map.put("consignmentOrderId", consignmentId);
            map.put("tripId", tripTO.getTripId());
            System.out.println("trip id ===" + tripTO.getTripId());
            // Trip Master Table
            String getOrderRoute = (String) session.queryForObject("trip.getOrderRoutesForLrAdd", map);
            System.out.println("getOrderRoute-" + getOrderRoute);
            String[] orderRoute = getOrderRoute.split("~");
            map.put("NewCnotes", orderRoute[0]);
            map.put("NewRoute", orderRoute[1]);
            map.put("originId", orderRoute[2]);
            map.put("destinationId", orderRoute[3]);

            String getTripRoute = (String) session.queryForObject("trip.getTripRoutesForLrAdd", map);
            System.out.println("getTripRoute-" + getTripRoute);
            String[] tripRoute = getTripRoute.split("~");
            map.put("tripCnotes", tripRoute[0]);
            map.put("tripRoute", tripRoute[1]);
            map.put("vehicleId", tripRoute[2]);
            map.put("consignmentStatusId", tripRoute[3]);

            String finalCnotes = tripRoute[0] + "," + orderRoute[0];
            String finalRoute = tripRoute[1] + "," + orderRoute[1];

            map.put("cNotes", finalCnotes);
            map.put("routeInfo", finalRoute);

            status = (Integer) session.update("trip.inserAddLrTripMaster", map);
            System.out.println("trip_master == " + status);
            // Trip LR Table
            status = (Integer) session.update("trip.getLRNumberSequence", map);
            System.out.println("trip_lr_number == " + status);

            // Trip route course
            String pointSequence = (String) session.queryForObject("trip.getLastPointSequence", map);

            map.put("pointId", orderRoute[2]);
            map.put("pointType", "PickUp");
            map.put("pointOrder", pointSequence);
            map.put("orderSequence", "3");
            map.put("pointAddresss", "");
            SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Calendar c = Calendar.getInstance();
            String systemTime = sdf.format(c.getTime()).toString();
            String[] dateTemp = systemTime.split(" ");
            String time = dateTemp[1];
            Date date = new Date();
            c.setTime(date);
            String fromDate = sdfNew.format(c.getTime()).toString();
            map.put("pointPlanDate", fromDate);
            map.put("pointPlanTime", time);
            status = (Integer) session.update("trip.insertTripRoute", map);
            System.out.println("trip_trip_route_course :: PickUp == " + status);

            pointSequence = (String) session.queryForObject("trip.getLastPointSequence", map);
            System.out.println("pointSequence :::" + pointSequence);
            map.put("pointId", orderRoute[2]);
            map.put("pointType", "Drop");
            map.put("pointOrder", pointSequence);
            map.put("pointAddresss", "");
            status = (Integer) session.update("trip.insertTripRoute", map);
            System.out.println("trip_trip_route_course :: Drop == " + status);

            // ts_trip_consignment
            status = (Integer) session.update("trip.saveTripConsignments", map);
            System.out.println("status - trip_consignment=" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveAction Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveAction", sqlException);
        }
        return status;
    }

    public int saveNewAdvance(TripTO tripTO) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("hire", tripTO.getHire());
            map.put("balance", tripTO.getBalance());
            map.put("mamul", tripTO.getMamul());

            map.put("advancerequestamt", tripTO.getAdvancerequestamt());
            map.put("requeston", tripTO.getRequeston());
            map.put("requestremarks", tripTO.getRequestremarks());
            map.put("advanceRemarks", tripTO.getAdvanceRemarks());
            map.put("advTrnType", tripTO.getAdvTrnType());
            map.put("requeststatus", "1");
            map.put("tripid", tripTO.getTripId());
            map.put("tobepaidtoday", tripTO.getTobepaidtoday());
            map.put("batchType", "M");
            map.put("tripday", "1");
            map.put("currencyId", "1");
            map.put("userId", tripTO.getUserId());
            System.out.println("map val" + map);

            manualAdvanceRequest = (Integer) getSqlMapClientTemplate().insert("operation.manualAdvanceRequest", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

//    public int saveTripStatusHistory(TripTO tripTO, String tripId, String penality, String pointName, String pointId, String startDate, String pcmremarks, String vehicleactreportmin, String cdate, SqlMapClient session) {
//        Map map = new HashMap();
//        int manualStatusUpdate = 0;
//        int status = 0;
//        String vehicleId = "";
//
//        try {
//
//            map.put("userId", tripTO.getUserId());
//            map.put("companyId", tripTO.getCompanyId());
//            map.put("tripSheetId", tripId);
//            map.put("tripId", tripId);
//            map.put("pointAddresss", tripTO.getPoint1Name());
//            map.put("tripRouteCourseId", tripTO.getRouteId());
//            map.put("penality", penality);
//
//            int tripStatusIdExists = (Integer) session.queryForObject("trip.getTripStatusIdExists", map);
//            System.out.println("tripStatusIdExists-----" + tripStatusIdExists);
//            if (tripStatusIdExists == 0) {
//                if (("36".equals(penality) || "37".equals(penality)) && !"0".equals(tripTO.getPoint1Id())) {
//                    map.put("pointId", tripTO.getPoint1Id());
//                    map.put("remarks", "Clearance @ " + tripTO.getPoint1Name() + " " + cdate);
//                } else {
//                    map.put("pointId", tripTO.getPoint1Id());
//                    map.put("remarks", tripTO.getPoint1Name() + " " + cdate);
//                }
//
//                map.put("actualLatitude", tripTO.getLatitude());
//                map.put("actualLongtitude", tripTO.getLongitude());
//
//                int multipleFactory = 0;
//                int statusCount = 0;
//                int factCount = 0;
//                if ("36".equals(penality) || "52".equals(penality) || "53".equals(penality) || "54".equals(penality) || "59".equals(penality)) {
//                    System.out.println("getMultipleFactoryCount is ==" + map);
//                    factCount = (Integer) session.queryForObject("trip.getMultipleFactoryCount", map);
//                    statusCount = (Integer) session.queryForObject("trip.getMultipleFactoryStatusCount", map);
//                    multipleFactory = factCount - statusCount;
//                }
//
//                String factorydet = "";
//                //           if ("35".equals(penality)) {
////                factorydet = (String) session.queryForObject("trip.getMultipleFactoryList", map);
////                String[] fdet = factorydet.split("~");
////                map.put("remarks", fdet[1]);
////                map.put("pointId", fdet[0]);
//                //     }
//
//                map.put("statusId", penality);
//                map.put("subStatus", penality);
//
//                if (multipleFactory > 0 && "36".equals(penality)) {
//                    map.put("statusId", penality);
//                    map.put("subStatus", "34");
//                }
//
//                map.put("pointPlanDate", startDate);
//                map.put("pointPlanHrs", pcmremarks);
//                map.put("pointPlanMin", vehicleactreportmin);
//
//                map.put("time", pcmremarks + ":" + vehicleactreportmin);
//                System.out.println("mappp is ==" + map);
//                manualStatusUpdate = (Integer) session.update("trip.insertTripStatusHistory", map);
//                System.out.println("manualStatusUpdate==" + manualStatusUpdate);
//
//                //map.put("subStatus", Integer.parseInt(penality));
//                status = (Integer) session.update("trip.saveSubStatusDetails", map);
//                System.out.println("penality$$$$$$--" + penality);
//                if (!"37".equals(penality) && !"38".equals(penality) && !"55".equals(penality) && !"56".equals(penality) && !"57".equals(penality) && !"58".equals(penality)) {
//                    map.put("movementUpdate", "1");
//                    map.put("tripRouteCourseId", tripTO.getRouteId());
//                    System.out.println("tripTO.getRouteId()$$$$----" + tripTO.getRouteId());
//                    status = (Integer) session.update("trip.updateRouteCoursePoint", map);
//                    System.out.println("updateRouteCoursePoint--update -----" + status);
//                }
//
//                if ("35".equals(penality)) {
//                    status = (Integer) session.update("trip.updateRouteCourseFactoryStatus", map);
//                }
//
//                map.put("cdate", cdate);
//                System.out.println("cdate------" + cdate);
//                System.out.println("penality------" + penality);
//                System.out.println("map-----" + map);
////            Factory IN
//                if ("35".equals(penality) || "43".equals(penality)) {
//                    status = (Integer) session.update("trip.updateTripFactoryInStatus", map);
//                    System.out.println("updateTripFactoryInStatus---" + status);
//                }
//
////            Factory OUT
//                if ("36".equals(penality) || "44".equals(penality) || "52".equals(penality) || "53".equals(penality) || "54".equals(penality) || "59".equals(penality)) {
//                    status = (Integer) session.update("trip.updateTripFactoryOutStatus", map);
//                    System.out.println("updateTripFactoryOutStatus---" + status);
//                }
//
////            Clerance IN
//                if ("37".equals(penality)) {
//                    status = (Integer) session.update("trip.updateTripCleranceInStatus", map);
//                    System.out.println("updateTripFactoryInStatus---" + status);
//                }
//
////            Clerance OUT
//                if ("38".equals(penality)) {
//                    status = (Integer) session.update("trip.updateTripCleranceOutStatus", map);
//                    System.out.println("updateTripFactoryOutStatus---" + status);
//                }
//
//                // statusId     Movement
//                //  38          Export
//                //  46          Import     Automatic TRIP END
//                if ("40".equals(penality) || "46".equals(penality)) {
//                    map.put("statusId", "12");
//                    manualStatusUpdate = (Integer) session.update("trip.insertTripStatusHistory", map);
//                    System.out.println("manualStatusUpdate::::" + manualStatusUpdate);
//                    map.put("endDate", startDate);
//                    map.put("endTime", vehicleactreportmin);
//                    map.put("vehicleactreportdate", startDate);
//                    map.put("vehicleactreporttime", vehicleactreportmin);
//                    map.put("vehicleloadreportdate", startDate);
//                    map.put("vehicleloadreporttime", vehicleactreportmin);
//                    map.put("vehicleloadtemperature", "0");
//                    map.put("endOdometerReading", "");
//                    map.put("totalKM", "");
//                    map.put("endHM", "0");
//                    map.put("totalHrs", "");
//                    map.put("totalDays", "");
//                    map.put("lrNumber", "0");
//                    map.put("durationDay1", "");
//                    map.put("originId", pointId);
//
//                    status = (Integer) session.update("trip.updateEndTripSheet", map);
//                    System.out.println("I am here in auto end::" + status);
//
//                    vehicleId = (String) session.queryForObject("trip.getTripVehicleId", map);
//                    if (!"0".equals(vehicleId) && !"".equals(vehicleId)) {
//                        map.put("vehicleId", vehicleId);
//                        int vehicleAvailUpdate = (Integer) session.update("trip.updateVehicleAvailabilityForEnd", map);
//                        System.out.println("vehicleAvailUpdate:::::" + vehicleAvailUpdate);
//                    }
//                    String ConsignmentIdList = (String) session.queryForObject("trip.getTripConsignment", map);
//                    System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
//                    String[] ConsignmentId = ConsignmentIdList.split(",");
//                    for (int j = 0; j < ConsignmentId.length; j++) {
//                        map.put("consignmentId", ConsignmentId[j]);
//                        map.put("consignmentOrderId", ConsignmentId[j]);
//                    }
//
//                    int plannedstatus = (Integer) session.queryForObject("trip.getPlannedContainer", map);
//                    System.out.println("plannedstatus==" + plannedstatus);
//
//                    if (plannedstatus == 0) {
//                        map.put("consignmentStatus", "12");
//                        status = (Integer) session.update("trip.updateConsignmentStatus", map);
//                    }
//
//                    String tripDays = (String) session.queryForObject("trip.getTotalTripDays", map);
//                    String tripDriver = (String) session.queryForObject("trip.getTripDriver", map);
//                    map.put("tripDriver", tripDriver);
//                    System.out.println("tripDays=" + tripDays);
//                    System.out.println("tripDriver---" + map);
//                    if (!"".equals(tripDays) && tripDays != null) {
//                        String[] tripDay = tripDays.split("~");
//                        System.out.println("dates=" + tripDay[0] + " to " + tripDay[1]);
//                        String[] dateList = getDateList(tripDay[0], tripDay[1]);
//                        int dailyBata = 0;
//                        int driverPadi = 0;
//                        int cleanerPadi = 0;
//                        for (int j = 0; j < dateList.length; j++) {
//                            map.put("effDate", dateList[j]);
//                            int check = 0;
//                            System.out.println("dateList[j]::" + dateList[j]);
//                            check = (Integer) session.queryForObject("trip.getTripDriverBataExist", map);
//                            // Driver salary config Master data
//                            String bhata = (String) session.queryForObject("trip.getBataConfigMaster", map);
//                            System.out.println("bhata-from driver salary config-" + bhata);
//                            System.out.println("check=" + check);
//                            if (check == 0) {
//                                System.out.println("check if inside over");
//                                if (bhata != null) {
//                                    System.out.println("bata if inside over");
//                                    String[] temp = bhata.split("~");
//                                    int result = Integer.parseInt(temp[0]);
//                                    int result1 = Integer.parseInt(temp[1]);
//                                    driverPadi = driverPadi + result;
//                                    System.out.println("cleanerPadicleanerPadicleanerPadicleanerPadi--" + driverPadi);
//                                    cleanerPadi = cleanerPadi + result1;
//                                    System.out.println("cleanerPadicleanerPadicleanerPadi--" + cleanerPadi);
//
//                                    map.put("dpadi", Double.parseDouble(temp[0]));
//                                    map.put("cpadi", Double.parseDouble(temp[1]));
//                                } else {
//                                    System.out.println("bata else over here");
//                                    driverPadi = driverPadi + 150;
//                                    cleanerPadi = cleanerPadi + 100;
//                                    map.put("dpadi", 150.00);
//                                    map.put("cpadi", 100.00);
//                                }
//
//                                dailyBata = (Integer) session.update("trip.insertDailyBata", map);
//                                System.out.println("dailyBata==" + dailyBata);
//
//                            }
//                        }
//
//                        String totalBata = (String) session.queryForObject("trip.getTotalTripDriverBata", map);
//                        System.out.println("totalBata==" + totalBata + "dailyBata=" + dailyBata);
//                        System.out.println("driverPadi==" + driverPadi);
//                        System.out.println("cleanerPadi==" + cleanerPadi);
//                        if (driverPadi != 0 && dailyBata != 0) {
////                if (driverPadi != 0 && cleanerPadi != 0 && dailyBata != 0) {
//                            System.out.println("final insert for driver/cleaner bata over here");
////                    String[] temp = totalBata.split("~");
//                            map.put("driverBata", driverPadi);
//                            map.put("cleanerBata", cleanerPadi);
//                            //            Driver Bata
//                            map.put("netExpense", driverPadi);
//                            map.put("expenses", driverPadi);
//                            map.put("expenseName", "1024");
//                            status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
//                            System.out.println("driver padi=" + status);
//                            //            Cleaner Bata
//                            map.put("netExpense", cleanerPadi);
//                            map.put("expenses", cleanerPadi);
//                            map.put("expenseName", "1031");
//                            status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
//                            System.out.println("cleaner padi=" + status);
//                        }
//
//                    }
//                }
//            }
//
//            // Hidden By Muthu
////            String pointOrder = (String) getSqlMapClientTemplate().queryForObject("trip.getLastPointSequence", map);
////             map.put("pointOrder", pointOrder);
////            map.put("lastPoint", Integer.parseInt(pointOrder) + 1);
////            System.out.println("map val" + map);
////            status = (Integer) getSqlMapClientTemplate().update("trip.updateStatusOrder", map);
////            if(status != 0){
////                manualStatusUpdate = (Integer) getSqlMapClientTemplate().update("trip.insertTripRoute", map);
////            }
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
//        }
//        return manualStatusUpdate;
//    }
    public int saveTripStatusHistory(TripTO tripTO, String tripId, String penality, String pointName, String pointId, String startDate, String pcmremarks, String vehicleactreportmin, String cdate, String type, SqlMapClient session) {
        Map map = new HashMap();
        int manualStatusUpdate = 0;
        int status = 0;
        String vehicleId = "";

        try {
            System.out.println("tripTO.getCompanyId()---" + tripTO.getCompanyId());
            map.put("userId", tripTO.getUserId());
            map.put("companyId", tripTO.getCompanyId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("vendorId", tripTO.getVendorId());
            map.put("hireVehicleNo", tripTO.getHireVehicleNo());
            map.put("tripSheetId", tripId);
            map.put("tripId", tripId);
            map.put("pointAddresss", tripTO.getPoint1Name());
            map.put("tripRouteCourseId", tripTO.getRouteId());
            map.put("penality", penality);
            map.put("type", type);

            int tripStatusIdExists = (Integer) session.queryForObject("trip.getTripStatusIdExists", map);
            System.out.println("tripStatusIdExists-----" + tripStatusIdExists);
            System.out.println("penality-----" + penality);
            if (tripStatusIdExists == 0) {
                if (("36".equals(penality) || "37".equals(penality)) && !"0".equals(tripTO.getPoint1Id())) {
                    System.out.println("tripTO.getPoint1Id()-if-----" + tripTO.getPoint1Id());
                    map.put("pointId", tripTO.getPoint1Id());
                    map.put("remarks", "Clearance @ " + tripTO.getPoint1Name() + " " + cdate);
                } else {
                    System.out.println("tripTO.getPoint1Id()-else-----" + tripTO.getPoint1Id());
                    map.put("pointId", tripTO.getPoint1Id());
                    map.put("remarks", tripTO.getPoint1Name() + " " + cdate);
                }

                map.put("actualLatitude", tripTO.getLatitude());
                map.put("actualLongtitude", tripTO.getLongitude());

                int multipleFactory = 0;
                int statusCount = 0;
                int factCount = 0;
                if ("36".equals(penality) || "52".equals(penality) || "53".equals(penality) || "54".equals(penality) || "59".equals(penality)) {
                    System.out.println("getMultipleFactoryCount is ==" + map);
                    factCount = (Integer) session.queryForObject("trip.getMultipleFactoryCount", map);
                    statusCount = (Integer) session.queryForObject("trip.getMultipleFactoryStatusCount", map);
                    multipleFactory = factCount - statusCount;
                }

                String factorydet = "";
                //           if ("35".equals(penality)) {
//                factorydet = (String) session.queryForObject("trip.getMultipleFactoryList", map);
//                String[] fdet = factorydet.split("~");
//                map.put("remarks", fdet[1]);
//                map.put("pointId", fdet[0]);
                //     }

                map.put("statusId", penality);
                map.put("subStatus", penality);

                if (multipleFactory > 0 && ("36".equals(penality) || "52".equals(penality) || "53".equals(penality) || "54".equals(penality) || "59".equals(penality))) {
//                if (multipleFactory > 0 && "36".equals(penality)) {
                    map.put("statusId", penality);
                    map.put("subStatus", "34");
                }

                map.put("pointPlanDate", startDate);
                map.put("pointPlanHrs", pcmremarks);
                map.put("pointPlanMin", vehicleactreportmin);

                map.put("time", pcmremarks + ":" + vehicleactreportmin);
                System.out.println("mappp isQQQQ ==" + map);
//                if(!"12".equals(penality) ){
                manualStatusUpdate = (Integer) session.update("trip.insertTripStatusHistory", map);
                System.out.println("manualStatusQQQQUpdate==" + manualStatusUpdate);
//                }

                //map.put("subStatus", Integer.parseInt(penality));
                map.put("activeVehicle", "Y");

                if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
                    map.put("vehicleId", "0");
                } else {
                    map.put("vehicleId", tripTO.getVehicleId());
                }
                map.put("vendorId", tripTO.getVendorId());

                if ("1".equals(tripTO.getVendorId())) {
                    map.put("hireVehicleNo", "0");
                } else {
                    map.put("hireVehicleNo", tripTO.getHireVehicleNo());
                }
                System.out.println("mappp WWWWW ==" + map);
                int offloadStatus = (Integer) session.queryForObject("trip.checkOffloadStatus", map);
                System.out.println("offloadStatus---" + offloadStatus);
                int checkVehCount = (Integer) session.queryForObject("trip.checkVehCount", map);
                System.out.println("checkVehCount---" + checkVehCount);
                if (offloadStatus == 2) {
                    map.put("subStatus", "48");
//                   if("40".equals(penality)){
                    map.put("vehstatusId", "12");
//                   }else{
//                       map.put("vehstatusId", "10");                           
//                   }

                    System.out.println("penality####" + penality);
                    if ("40".equals(penality) && checkVehCount == 2) {
                        status = (Integer) session.update("trip.updateOffloadTripEndVehicle1", map);
                        System.out.println("updateOffloadTripEndVehicle1--" + status);

                        status = (Integer) session.update("trip.updateOffloadTripEnd1", map);
                        System.out.println("updateOffloadTripEnd1--" + status);
                    } else if (!"40".equals(penality) && checkVehCount == 1) {
                        map.put("vehstatusId", "12");
                        status = (Integer) session.update("trip.updateOffloadTripEndVehicle", map);
                        System.out.println("updateOffloadTripEndVehicle--" + status);

                        status = (Integer) session.update("trip.updateOffloadTripEndDriver", map);
                        System.out.println("updateOffloadTripEndDriver--" + status);

                        status = (Integer) session.update("trip.updateOffloadTripEnd", map);
                        System.out.println("updateOffloadTripEnd--" + status);
                    }
                    int chckStatus = 0;
                    if (checkVehCount == 1) {
                        map.put("statusId", "37"); //clearance in 
                        chckStatus = (Integer) session.queryForObject("trip.checkStatusDetails", map);
                        System.out.println("chckStatus---" + chckStatus);
                        if (chckStatus == 0) {
                            manualStatusUpdate = (Integer) session.update("trip.insertTripStatusHistory", map);
                            System.out.println("manualStatus111Update==" + manualStatusUpdate);
                        }
                        map.put("statusId", "38"); //clearance out
                        manualStatusUpdate = (Integer) session.update("trip.insertTripStatusHistory", map);
                        System.out.println("manualStatus222Update==" + manualStatusUpdate);
//                        map.put("statusId", "12"); //clearance out
//                        manualStatusUpdate = (Integer) session.update("trip.insertTripStatusHistory", map);
//                        System.out.println("manualStatus111Update==" + manualStatusUpdate);
                    }

                } else if (offloadStatus == 1) {
                    map.put("subStatus", "48");
                    status = (Integer) session.update("trip.updateTripEnd", map);
                }
                status = (Integer) session.update("trip.saveSubStatusDetails", map);
                System.out.println("saveSubStatusDetails$$$$$$--" + status);
                System.out.println("penality$$$$$$--" + penality);
                if (!"37".equals(penality) && !"38".equals(penality) && !"55".equals(penality) && !"56".equals(penality) && !"57".equals(penality) && !"58".equals(penality)) {
                    map.put("movementUpdate", "1");
                    map.put("tripRouteCourseId", tripTO.getRouteId());
                    System.out.println("tripTO.getRouteId()$$$$----" + tripTO.getRouteId());
                    status = (Integer) session.update("trip.updateRouteCoursePoint", map);
                    System.out.println("updateRouteCoursePoint--update -----" + status);
                }

                if ("35".equals(penality)) {
                    status = (Integer) session.update("trip.updateRouteCourseFactoryStatus", map);
                }

                map.put("cdate", cdate);
                System.out.println("cdate------" + cdate);
                System.out.println("penality------" + penality);
                System.out.println("map-----" + map);
//            Factory IN
                if ("35".equals(penality) || "43".equals(penality)) {
                    status = (Integer) session.update("trip.updateTripFactoryInStatus", map);
                    System.out.println("updateTripFactoryInStatus---" + status);
                }

//            Factory OUT
                if ("36".equals(penality) || "44".equals(penality) || "52".equals(penality) || "53".equals(penality) || "54".equals(penality) || "59".equals(penality)) {
                    status = (Integer) session.update("trip.updateTripFactoryOutStatus", map);
                    System.out.println("updateTripFactoryOutStatus---" + status);
                }

//            Clerance IN
                if ("37".equals(penality)) {
                    status = (Integer) session.update("trip.updateTripCleranceInStatus", map);
                    System.out.println("updateTripFactoryInStatus---" + status);
                }

//            Clerance OUT
                if ("38".equals(penality)) {
                    status = (Integer) session.update("trip.updateTripCleranceOutStatus", map);
                    System.out.println("updateTripFactoryOutStatus---" + status);
                }

                // statusId     Movement
                //  38          Export
                //  46          Import     Automatic TRIP END
                if ("40".equals(penality) || "46".equals(penality)) {
                    map.put("statusId", "12");
                    manualStatusUpdate = (Integer) session.update("trip.insertTripStatusHistory", map);
                    System.out.println("manualStatusUpdate::::" + manualStatusUpdate);
                    map.put("endDate", startDate);
                    map.put("endTime", vehicleactreportmin);
                    map.put("vehicleactreportdate", startDate);
                    map.put("vehicleactreporttime", vehicleactreportmin);
                    map.put("vehicleloadreportdate", startDate);
                    map.put("vehicleloadreporttime", vehicleactreportmin);
                    map.put("vehicleloadtemperature", "0");
                    map.put("endOdometerReading", "");
                    map.put("totalKM", "");
                    map.put("endHM", "0");
                    map.put("totalHrs", "");
                    map.put("totalDays", "");
                    map.put("lrNumber", "0");
                    map.put("durationDay1", "");
                    map.put("originId", pointId);

                    status = (Integer) session.update("trip.updateEndTripSheet", map);
                    System.out.println("I am here in auto end::" + status);

                    vehicleId = (String) session.queryForObject("trip.getTripVehicleId", map);
                    if (!"0".equals(vehicleId) && !"".equals(vehicleId)) {
                        map.put("vehicleId", vehicleId);
                        int vehicleAvailUpdate = (Integer) session.update("trip.updateVehicleAvailabilityForEnd", map);
                        System.out.println("vehicleAvailUpdate:::::" + vehicleAvailUpdate);
                    }
                    String ConsignmentIdList = (String) session.queryForObject("trip.getTripConsignment", map);
                    System.out.println("the ConsignmentIdList size :" + ConsignmentIdList.length());
                    String[] ConsignmentId = ConsignmentIdList.split(",");
                    for (int j = 0; j < ConsignmentId.length; j++) {
                        map.put("consignmentId", ConsignmentId[j]);
                        map.put("consignmentOrderId", ConsignmentId[j]);
                    }

                    int plannedstatus = (Integer) session.queryForObject("trip.getPlannedContainer", map);
                    System.out.println("plannedstatus==" + plannedstatus);

                    if (plannedstatus == 0) {
                        map.put("consignmentStatus", "12");
                        status = (Integer) session.update("trip.updateConsignmentStatus", map);
                    }
                }
                int driverCount = 0;
                if ("40".equals(penality) || "46".equals(penality) || "12".equals(penality)) {

                    String tripDays = (String) session.queryForObject("trip.getTotalTripDays", map);
                    String tripDriver = (String) session.queryForObject("trip.getTripDriver", map);
                    String tripCleaner = (String) session.queryForObject("trip.getTripCleaner", map);
                    map.put("tripDriver", tripDriver);
                    map.put("tripCleaner", tripCleaner);
                    System.out.println("tripDays=QQQQ-" + tripDays);
                    System.out.println("tripDriver---" + map);
                    driverCount = (Integer) session.queryForObject("trip.getDriverCountPerTrip", map);
                    System.out.println("driverCount==" + driverCount);
                    System.out.println("tripDays==" + tripDays);
                    if (!"".equals(tripDays) && tripDays != null) {
                        if (driverCount == 1) {
                            String[] tripDay = tripDays.split("~");
                            System.out.println("dates=" + tripDay[0] + " to " + tripDay[1]);
                            String[] dateList = getDateList(tripDay[0], tripDay[1]);
                            int dailyBata = 0;
                            int driverPadi = 0;
                            int cleanerPadi = 0;
                            for (int j = 0; j < dateList.length; j++) {
                                map.put("effDate", dateList[j]);
                                int check = 0;
                                System.out.println("dateList[j]::" + dateList[j]);
                                check = (Integer) session.queryForObject("trip.getTripDriverBataExist", map);
                                // Driver salary config Master data
                                String bhata = (String) session.queryForObject("trip.getBataConfigMaster", map);
                                System.out.println("bhata-from driver salary config-" + bhata);
                                System.out.println("check=" + check);
                                if (check == 0) {
                                    System.out.println("check if inside over");
                                    if (bhata != null) {
                                        System.out.println("bata if inside over");
                                        String[] temp = bhata.split("~");
                                        int result = Integer.parseInt(temp[0]);
                                        int result1 = Integer.parseInt(temp[1]);
                                        driverPadi = driverPadi + result;
                                        System.out.println("cleanerPadicleanerPadicleanerPadicleanerPadi--" + driverPadi);
                                        cleanerPadi = cleanerPadi + result1;
                                        System.out.println("cleanerPadicleanerPadicleanerPadi--" + cleanerPadi);

                                        map.put("dpadi", Double.parseDouble(temp[0]));
                                        map.put("cpadi", Double.parseDouble(temp[1]));
                                    } else {
                                        System.out.println("bata else over here");
                                        driverPadi = driverPadi + 150;
                                        cleanerPadi = cleanerPadi + 100;
                                        map.put("dpadi", 150.00);
                                        map.put("cpadi", 100.00);
                                    }
                                    System.out.println("aaaa");
                                    dailyBata = (Integer) session.update("trip.insertDailyBata", map);
                                    System.out.println("dailyBata==" + dailyBata);
                                }
                            }
                            System.out.println("BEFORE");
                            String totalBata = (String) session.queryForObject("trip.getTotalTripDriverBata", map);
                            System.out.println("totalBata==" + totalBata + "dailyBata=" + dailyBata);
                            System.out.println("driverPadi==" + driverPadi);
                            System.out.println("cleanerPadi==" + cleanerPadi);
//                if (driverPadi != 0 && cleanerPadi != 0 && dailyBata != 0) {
                            System.out.println("final insert for driver/cleaner bata over here");
//                    String[] temp = totalBata.split("~");

//                        driverCount = (Integer) session.queryForObject("trip.getDriverCountPerTrip", map);
//                        System.out.println("driverCount==" + driverCount);
//                            map.put("dpadi", 150.00);
//                            map.put("cpadi", 100.00);
                            if (driverPadi != 0 && dailyBata != 0) {
                                map.put("driverBata", driverPadi);
                                map.put("cleanerBata", cleanerPadi);
                                map.put("expenseBy", tripDriver);
                                //            Driver Bata
                                map.put("netExpense", driverPadi);
                                map.put("expenses", driverPadi);
                                map.put("expenseName", "1024");
                                status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
                                System.out.println("driver padi=" + status);
                            }
                            if (cleanerPadi != 0 && dailyBata != 0) {

                                //            Cleaner Bata
                                map.put("driverId", 0);
                                map.put("netExpense", cleanerPadi);
                                map.put("expenses", cleanerPadi);
                                map.put("expenseBy", tripCleaner);
                                map.put("expenseName", "1031");
                                status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
                                System.out.println("cleaner padi=" + status);
                            }

                        } else if (driverCount > 1) {
                            int check = 0;

                            String tripDaysPerDriver1 = (String) session.queryForObject("trip.getTotalTripDaysPerDriver1", map);
                            System.out.println("tripDaysPerDriver1=" + tripDaysPerDriver1);
                            if (!"".equals(tripDaysPerDriver1) && tripDaysPerDriver1 != null) {
                                String[] tripDayperDriver1 = tripDaysPerDriver1.split("~");
                                System.out.println("dates=1=" + tripDayperDriver1[0] + " to " + tripDayperDriver1[1]);
                                String[] dateList1 = getDateList(tripDayperDriver1[0], tripDayperDriver1[1]);
                                System.out.println("tripDayperDriver1[2]---" + tripDayperDriver1[2]);
                                map.put("driverId", tripDayperDriver1[2]);
                                map.put("cleanerId", tripDayperDriver1[3]);
                                int driverPadi1 = 0;
                                int cleanerPadi1 = 0;
                                for (int k = 0; k < dateList1.length; k++) {
                                    System.out.println("dateList1[k]::" + dateList1[k]);
                                    // Driver salary config Master data
                                    map.put("effDate", dateList1[k]);
                                    map.put("tripDriver", tripDriver);
                                    System.out.println("dateList2[i]--" + dateList1[k]);
                                    System.out.println("tripDriver--" + tripDriver);
                                    check = (Integer) session.queryForObject("trip.getTripDriverBataExist", map);
                                    System.out.println("checkOld veh==" + check);
                                    if (check == 0) {
                                        String bhata = (String) session.queryForObject("trip.getBataConfigMaster", map);
                                        System.out.println("bhata-from driver salary config-" + bhata);
                                        if (bhata != null) {
                                            System.out.println("bata if inside over1");
                                            String[] temp1 = bhata.split("~");
                                            int result = Integer.parseInt(temp1[0]);
                                            int result1 = Integer.parseInt(temp1[1]);
                                            driverPadi1 = driverPadi1 + result;
                                            System.out.println("cleanerPadicleanerPadicleanerPadicleanerPadi--" + driverPadi1);
                                            cleanerPadi1 = cleanerPadi1 + result1;
                                            System.out.println("cleanerPadicleanerPadicleanerPadi--" + cleanerPadi1);

                                            map.put("dpadi", Double.parseDouble(temp1[0]));
                                            map.put("cpadi", Double.parseDouble(temp1[1]));

                                        } else {
                                            System.out.println("bata else over here1");
                                            driverPadi1 = driverPadi1 + 150;
                                            cleanerPadi1 = cleanerPadi1 + 100;
                                        }
                                    }

                                }
                                System.out.println("driverPadi1 -- " + driverPadi1);
                                System.out.println("cleanerPadi1 -- " + cleanerPadi1);
                                map.put("tripSheetId", tripId);
                                vehicleId = (String) session.queryForObject("trip.getTripOldVehicleId", map);
                                System.out.println("Old Vehicle ---" + vehicleId);
                                if (driverPadi1 != 0) {
                                    map.put("vehicleId", vehicleId);
                                    map.put("driverBata", driverPadi1);
                                    map.put("expenseBy", tripDayperDriver1[2]);
                                    //            Driver Bata
                                    map.put("netExpense", driverPadi1);
                                    map.put("expenses", driverPadi1);
                                    map.put("expenseName", "1024");
                                    status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
                                    System.out.println("driver padi=" + status);
                                }
                                if (cleanerPadi1 != 0) {
//                                    vehicleId = (String) session.queryForObject("trip.getTripOldVehicleId", map);
                                    map.put("vehicleId", vehicleId);
                                    map.put("driverBata", cleanerPadi1);
                                    map.put("expenseBy", tripDayperDriver1[3]);
                                    //            cleaner Bata
                                    map.put("netExpense", cleanerPadi1);
                                    map.put("expenses", cleanerPadi1);
                                    map.put("expenseName", "1031");
                                    status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
                                    System.out.println("driver padi=" + status);
                                }
                            }
                            String tripDaysPerDriver2 = (String) session.queryForObject("trip.getTotalTripDaysPerDriver2", map);
                            System.out.println("tripDaysPerDriver2=" + tripDaysPerDriver2);
                            if (!"".equals(tripDaysPerDriver2) && tripDaysPerDriver2 != null) {
                                String[] tripDayperDriver2 = tripDaysPerDriver2.split("~");
                                System.out.println("dates=2=" + tripDayperDriver2[0] + " to " + tripDayperDriver2[1]);
                                String[] dateList2 = getDateList(tripDayperDriver2[0], tripDayperDriver2[1]);
                                map.put("driverId", tripDayperDriver2[2]);
                                map.put("cleanerId", tripDayperDriver2[3]);
                                int driverPadi2 = 0;
                                int cleanerPadi2 = 0;
                                for (int i = 0; i < dateList2.length; i++) {
                                    System.out.println("dateList2[i]::" + dateList2[i]);
                                    // Driver salary config Master data
                                    map.put("effDate", dateList2[i]);
                                    map.put("tripDriver", tripDriver);
                                    System.out.println("dateList2[i]--" + dateList2[i]);
                                    System.out.println("tripDriver--" + tripDriver);
                                    check = (Integer) session.queryForObject("trip.getTripDriverBataExist", map);
                                    System.out.println("checkNew veh==" + check);
                                    if (check == 0) {
                                        String bhata = (String) session.queryForObject("trip.getBataConfigMaster", map);
                                        System.out.println("bhata-from driver salary config-" + bhata);
                                        if (bhata != null) {
                                            System.out.println("bata if inside over2");
                                            String[] temp1 = bhata.split("~");
                                            int result = Integer.parseInt(temp1[0]);
                                            int result1 = Integer.parseInt(temp1[1]);
                                            driverPadi2 = driverPadi2 + result;
                                            System.out.println("cleanerPadicleanerPadicleanerPadicleanerPadi2-" + driverPadi2);
                                            cleanerPadi2 = cleanerPadi2 + result1;
                                            System.out.println("cleanerPadicleanerPadicleanerPadi2--" + cleanerPadi2);

                                            map.put("dpadi", Double.parseDouble(temp1[0]));
                                            map.put("cpadi", Double.parseDouble(temp1[1]));

                                        } else {
                                            System.out.println("bata else over here2");
                                            driverPadi2 = driverPadi2 + 150;
                                            cleanerPadi2 = cleanerPadi2 + 100;
                                        }
                                    }

                                }
                                System.out.println("driverPadi2 -- " + driverPadi2);
                                System.out.println("cleanerPadi2 -- " + cleanerPadi2);
                                map.put("tripSheetId", tripId);
                                vehicleId = (String) session.queryForObject("trip.getTripVehicleId", map);
                                System.out.println("New Vehicle ---" + vehicleId);
                                if (driverPadi2 != 0) {
                                    map.put("vehicleId", vehicleId);
                                    map.put("driverBata", driverPadi2);
                                    map.put("expenseBy", tripDayperDriver2[2]);
                                    //            Driver Bata
                                    map.put("netExpense", driverPadi2);
                                    map.put("expenses", driverPadi2);
                                    map.put("expenseName", "1024");
                                    System.out.println("driverPadi2 map-" + map);
                                    status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
                                    System.out.println("driver padi=" + status);
                                }
                                if (cleanerPadi2 != 0) {
                                    map.put("vehicleId", vehicleId);
                                    map.put("cleanerBata", cleanerPadi2);
                                    map.put("expenseBy", tripDayperDriver2[3]);
                                    //            cleaner Bata
                                    map.put("netExpense", cleanerPadi2);
                                    map.put("expenses", cleanerPadi2);
                                    map.put("expenseName", "1031");
                                    System.out.println("cleanerPadi2 map-" + map);
                                    status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
                                    System.out.println("driver padi=" + status);
                                }
                            }
//                        } else {
//                            map.put("dpadi", 150.00);
//                            map.put("cpadi", 100.00);
//                            if (driverPadi != 0 && dailyBata != 0) {
//                                map.put("driverBata", driverPadi);
//                                map.put("cleanerBata", cleanerPadi);
//                                //            Driver Bata
//                                map.put("netExpense", driverPadi);
//                                map.put("expenses", driverPadi);
//                                map.put("expenseName", "1024");
//                                status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
//                                System.out.println("driver padi=" + status);
//                            }
                        }
//                        if (cleanerPadi != 0 && dailyBata != 0) {
//
//                            //            Cleaner Bata
//                            map.put("driverId", 0);
//                            map.put("netExpense", cleanerPadi);
//                            map.put("expenses", cleanerPadi);
//                            map.put("expenseName", "1031");
//                            status = (Integer) session.update("trip.saveTripExpenseDriverBata", map);
//                            System.out.println("cleaner padi=" + status);
//                        }

                    }
                }
            }
            System.out.println("manualStatusUpdate-----" + manualStatusUpdate);
            // Hidden By Muthu
//            String pointOrder = (String) getSqlMapClientTemplate().queryForObject("trip.getLastPointSequence", map);
//             map.put("pointOrder", pointOrder);
//            map.put("lastPoint", Integer.parseInt(pointOrder) + 1);
//            System.out.println("map val" + map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateStatusOrder", map);
//            if(status != 0){
//                manualStatusUpdate = (Integer) getSqlMapClientTemplate().update("trip.insertTripRoute", map);
//            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualStatusUpdate;
    }

    // Lorry Hire challan generation
    public int generateLorryHireChallan(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int lhcId = 0;
        int status = 0;

        try {

            Calendar calendar = Calendar.getInstance();
            int curyear = (int) calendar.get(Calendar.YEAR);
            String currentyear = curyear + "";
            currentyear = currentyear.substring(2);
            int nextyear = Integer.parseInt(currentyear) + 1;
            String accYear = currentyear + "" + nextyear;

            String lhcCodeSequence = "";
            lhcCodeSequence = (String) session.queryForObject("trip.getLHCNo", map);
            if (lhcCodeSequence == null) {
                lhcCodeSequence = "00001";
            } else if (lhcCodeSequence.length() == 1) {
                lhcCodeSequence = "0000" + lhcCodeSequence;
            } else if (lhcCodeSequence.length() == 2) {
                lhcCodeSequence = "000" + lhcCodeSequence;
            } else if (lhcCodeSequence.length() == 3) {
                lhcCodeSequence = "00" + lhcCodeSequence;
            } else if (lhcCodeSequence.length() == 4) {
                lhcCodeSequence = "0" + lhcCodeSequence;
            } else if (lhcCodeSequence.length() == 5) {
                lhcCodeSequence = "" + lhcCodeSequence;
            }
            String lhcCode = "LHC-" + accYear + "-" + lhcCodeSequence;

            map.put("lhcCode", lhcCode);
            map.put("contractHireId", tripTO.getContractHireId());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
            map.put("vehicleNo", tripTO.getHireVehicleNo());
            map.put("driverName", tripTO.getPrimaryDriverName());
            map.put("driverMobile", tripTO.getMobileNo());
            map.put("additionalCost", tripTO.getHire());
            map.put("tripid", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());

            System.out.println("tripTO.getHire() : " + tripTO.getHire());
            System.out.println("tripTO.tripTO.getContractHireRate() : " + tripTO.getContractHireRate());

            map.put("approveStatus", "1");

            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            System.out.println("consignmentOrderIds.length=" + consignmentOrderIds.length);
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                System.out.println("consignmentOrderIds[i]=" + consignmentOrderIds[i]);
                map.put("consignId", consignmentOrderIds[i]);
            }

            System.out.println("map val in LHC" + map);

            lhcId = (Integer) session.insert("trip.insertLHC", map);
            System.out.println("lhcId=" + lhcId);
            map.put("lhcId", lhcId);

            status = (Integer) session.update("trip.updateLhcinTrip", map);
            System.out.println("status=..." + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return lhcId;
    }

    public int updateSaveContainer(String tripId, String containerNo, String tonnage, String sealNo) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("tripId", tripId);
            String check = (String) getSqlMapClient().queryForObject("trip.getTripContainerCount", map);
            System.out.println("check==" + check);
            if ("1".equals(check) || check == "1") {
                map.put("containerNo", containerNo);
                map.put("tonnage", tonnage);
                map.put("sealNo", sealNo);
                System.out.println("check111==" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateSaveTripContainer", map);
                System.out.println("status=dao=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getDisplayLogoBlobData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "getDisplayLogoBlobData", sqlException);
        }
        return status;
    }

    public ArrayList getTripStatusHistory(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("movementType", tripTO.getMovementTypeId());
        ArrayList tripStatusHistory = new ArrayList();
//        try {
        System.out.println("map for point:" + map);
        try {
            tripStatusHistory = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripStatusHistory", map);
            System.out.println("tripStatusHistory size=" + tripStatusHistory.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripStatusHistory;
    }

    public int updateTrackingHist(String tripId, String pointId, String startDate, String startHour, String startMin, String selectedStatusId, String reportingDate, String reportingHour, String reportingMin, String routeCourseId) {
        Map map = new HashMap();
        int manualStatusUpdate = 0;
        int status = 0;

        try {

            map.put("tripId", tripId);
            map.put("statusId", selectedStatusId);
            map.put("pointAddresss", "");
            map.put("pointId", pointId);
            map.put("pointPlanDate", startDate);
            map.put("pointPlanHrs", startHour);
            map.put("pointPlanMin", startMin);
            map.put("remarks", "");
            map.put("time", startHour + ":" + startMin);

            map.put("vehicleactreportdate", reportingDate);
            map.put("vehicleactreporttime", reportingHour + ":" + reportingMin + ":00");
            map.put("vehicleloadreportdate", startDate);
            map.put("vehicleloadreporttime", startHour + ":" + startMin + ":00");
            map.put("vehicleloadtemperature", "0");
            map.put("pointOrder", routeCourseId);

            System.out.println("mappp is ==" + map);
            manualStatusUpdate = (Integer) getSqlMapClientTemplate().update("trip.insertTripStatusHistory", map);
            System.out.println("manualStatusUpdate==" + manualStatusUpdate);
            if (!"32".equals(selectedStatusId) || !"38".equals(selectedStatusId)) {
                manualStatusUpdate = (Integer) getSqlMapClientTemplate().update("trip.saveLoadingUnloadingTrackingDetails", map);
                System.out.println("dadadaeeecszcds==" + manualStatusUpdate);
            } else if ("38".equals(selectedStatusId)) {
                String pointOrder = (String) getSqlMapClientTemplate().queryForObject("trip.getLastPointSequence", map);
                map.put("pointOrder", pointOrder);
                map.put("lastPoint", Integer.parseInt(pointOrder) + 1);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateStatusOrder", map);
                if (status != 0) {
                    manualStatusUpdate = (Integer) getSqlMapClientTemplate().update("trip.insertTripRoute", map);
                }
            }
            map.put("subStatus", Integer.parseInt(selectedStatusId));
            status = (Integer) getSqlMapClientTemplate().update("trip.saveSubStatusDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualStatusUpdate;
    }

    public int updateDriverDetails(String tripId, String driver1Name, String driver1Id, String mobileNo) {
        Map map = new HashMap();
        int manualStatusUpdate = 0;
        int status = 0;

        try {

            map.put("tripId", tripId);
            map.put("driver1Name", driver1Name);
            map.put("driver1Id", driver1Id);
            map.put("mobileNo", mobileNo);

            System.out.println("mappp is ==" + map);
            manualStatusUpdate = (Integer) getSqlMapClientTemplate().update("trip.updateStartTripDriver", map);
            System.out.println("dadaada==" + manualStatusUpdate);

            // Hidden By Muthu
//            String pointOrder = (String) getSqlMapClientTemplate().queryForObject("trip.getLastPointSequence", map);
//             map.put("pointOrder", pointOrder);
//            map.put("lastPoint", Integer.parseInt(pointOrder) + 1);
//            System.out.println("map val" + map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateStatusOrder", map);
//            if(status != 0){
//                manualStatusUpdate = (Integer) getSqlMapClientTemplate().update("trip.insertTripRoute", map);
//            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualStatusUpdate;
    }

    public ArrayList getTripSheetMovementDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("documentRequired", tripTO.getDocumentRequired());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("consignmentNo", tripTO.getConsignmentNo());
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("customerName", tripTO.getCustomerId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("statusId", tripTO.getStatusId());
        map.put("tripStatusId", tripTO.getTripStatusId());
        map.put("roleId", tripTO.getRoleId());
        map.put("vehicleTypeId", tripTO.getVehicleTypeId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("tripSheetId", tripTO.getTripCode());
        map.put("zoneId", tripTO.getZoneId());
        map.put("fleetCenterId", tripTO.getFleetCenterId());
        map.put("cityFromId", tripTO.getCityFromId());
        map.put("podStatus", tripTO.getPodStatus());
        map.put("tripType", tripTO.getTripType());
        map.put("extraExpenseStatus", tripTO.getExtraExpenseStatus());
        System.out.println("tripTO.getMovementType()::::---" + tripTO.getMovementType());
        if ("1,25,26,27,31,32".equals(tripTO.getMovementType())) {
            map.put("movement", "1");
        } else if ("2,28,29,30".equals(tripTO.getMovementType())) {
            map.put("movement", "2");
        } else {
            map.put("movement", tripTO.getMovementType());
        }
        map.put("requestType", tripTO.getRequestType());
        map.put("subStatusId", tripTO.getSubStatusId());
        map.put("customerId", tripTO.getCustomerId());
        map.put("userId", tripTO.getUserId());
        System.out.println("map ...." + map);
        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);
        List userMovementList = new ArrayList(tripTO.getUserMovementList().size());

        Iterator umItr = tripTO.getUserMovementList().iterator();
        LoginTO umOpTO = new LoginTO();
        while (umItr.hasNext()) {
            umOpTO = new LoginTO();
            umOpTO = (LoginTO) umItr.next();
            userMovementList.add(umOpTO.getMovementTypeId());
        }
        map.put("userMovementList", userMovementList);
        System.out.println("map = " + map);
        ArrayList tripMovementDetails = new ArrayList();
        try {
            tripMovementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripSheetMovementDetails", map);
            System.out.println("tripMovementDetails size=" + tripMovementDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return tripMovementDetails;
    }

    public ArrayList getCustomerMovementDetails(TripTO tripTO) {
        Map map = new HashMap();
        System.out.println("movemoemvem" + tripTO.getMovementType());
        if ("1,25,26".equals(tripTO.getMovementType())) {
            map.put("movement", "1");
        } else {
            map.put("movement", tripTO.getMovementType());
        }

        map.put("requestType", tripTO.getRequestType());
        System.out.println("map = " + map);
        ArrayList customerMovementDetails = new ArrayList();
        try {
            customerMovementDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerMovementDetails", map);
            System.out.println("customerMovementDetails size=" + customerMovementDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripSheetDetails List", sqlException);
        }

        return customerMovementDetails;
    }

    public ArrayList viewVehicleLocation(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusDetails = new ArrayList();
        try {
            map.put("vehNo", tripTO.getVehicleNo());
            System.out.println(" Trip map is::" + map);
            statusDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getVehicleLocation", map);
            System.out.println("viewVehicleLocation.size() = " + statusDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("viewVehicleLocation Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "viewVehicleLocation List", sqlException);
        }
        return statusDetails;
    }

//    public ArrayList getStatusMaster(TripTO tripTO) {
//        Map map = new HashMap();
//        map.put("tripId", tripTO.getTripId());
//        map.put("movementType", tripTO.getMovementTypeId());
//        ArrayList statusMaster = new ArrayList();
//        try {
//            String clearanceType = (String) getSqlMapClientTemplate().queryForObject("trip.getTripCleranceType", map);
//            map.put("clearanceType", clearanceType);
//            System.out.println("map for getStatusMaster:" + map);
//
//            String penality = tripTO.getSubStatusId();
//
//            int multipleFactory = 0;
//            int statusCount = 0;
//            int factCount = 0;
//            System.out.println("getStatusMaster factory check is ==" + penality);
//            if ("35".equals(penality) || "36".equals(penality) || "52".equals(penality) || "53".equals(penality) || "54".equals(penality) || "55".equals(penality)
//                    || "56".equals(penality) || "57".equals(penality) || "58".equals(penality) || "59".equals(penality)) {
//                System.out.println("getMultipleFactoryCount is ==" + map);
//                factCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMultipleFactoryCount", map);
//                statusCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMultipleFactoryStatusCount", map);
//                multipleFactory = factCount - statusCount;
//            }
//
//            map.put("multiple", multipleFactory);
//            System.out.println("multiple factory check is ==" + multipleFactory);
//
//            statusMaster = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointStatus", map);
//            System.out.println("statusMaster size=" + statusMaster.size());
//
//        } catch (Exception sqlException) {
//
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
//        }
//
//        return statusMaster;
//    }
    public ArrayList getStatusMaster(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("tripSheetId", tripTO.getTripId());
        map.put("movementType", tripTO.getMovementTypeId());
        ArrayList statusMaster = new ArrayList();
        try {
            int factCount = (Integer) getSqlMapClientTemplate().queryForObject("trip.getMultipleFactoryCount", map);
            map.put("factCount", factCount);

            int subStatusId = (Integer) getSqlMapClientTemplate().queryForObject("trip.getSubStatusId", map);
            map.put("subStatusId", subStatusId);

            String clearanceType = (String) getSqlMapClientTemplate().queryForObject("trip.getTripCleranceType", map);
            map.put("clearanceType", clearanceType);
            System.out.println("map for getStatusMaster:" + map);
            if (factCount > 1 && (subStatusId == 34 || subStatusId == 35) && ("1".equals(tripTO.getMovementTypeId()))) {
                System.out.println("if");
                statusMaster = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointStatusForMultipleLoading", map);
            } else {
                System.out.println("else");
                statusMaster = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPointStatus", map);
            }
            System.out.println("statusMaster size=" + statusMaster.size());

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return statusMaster;
    }

    public ArrayList getIncompleteStatusMaster(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList statusMaster = new ArrayList();
        map.put("tripId", tripTO.getTripId());
        map.put("movementType", tripTO.getMovementTypeId());

        try {
            String clearanceType = (String) getSqlMapClientTemplate().queryForObject("trip.getTripCleranceType", map);
            map.put("clearanceType", clearanceType);
            System.out.println("map for IncompleteStatusMaster:" + map);
            statusMaster = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getIncompleteStatusMaster", map);
            System.out.println("statusMaster size=" + statusMaster.size());

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return statusMaster;
    }

    public String[] getDateList(String startDate, String endDate) {
        String[] dateList = new String[365];
        String[] datList = null;
        try {
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            Date startDt = df.parse(startDate);
            Date endDt = df.parse(endDate);
            Calendar startCal, endCal;
            startCal = Calendar.getInstance();
            startCal.setTime(startDt);
            endCal = Calendar.getInstance();
            endCal.setTime(endDt);
            System.out.println("startDate" + startDate);
            System.out.println("endDate" + endDate);
            //Just in case the dates were transposed this prevents infinite loop
            if (startCal.getTimeInMillis() > endCal.getTimeInMillis()) {
                startCal.setTime(endDt);
                endCal.setTime(startDt);
            }
            startCal.add(Calendar.DATE, -1);
            int i = 0;
            do {
                startCal.add(Calendar.DAY_OF_MONTH, 1);
                System.out.println("Day: " + df.format(startCal.getTime()));
                dateList[i] = df.format(startCal.getTime());
                System.out.println("Day++: " + dateList[i]);
                i++;
            } while (startCal.getTimeInMillis() < endCal.getTimeInMillis());
            datList = new String[i];
            for (int m = 0; m < datList.length; m++) {
                datList[m] = dateList[m];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return datList;
    }

    public int saveTripFuelRecovery(TripTO tripTO) {
        Map map = new HashMap();

        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("allocatedFuel", tripTO.getAllocatedFuel());
        map.put("consumedFuel", tripTO.getConsumedFuel());
        map.put("currentPrice", tripTO.getFuelPrice());
        map.put("recoveryFuel", tripTO.getRecoveryFuel());
        map.put("recoveryAmount", tripTO.getRecoveryAmount());
        map.put("fuelSlipNo", tripTO.getFuelSlipNo());
        map.put("fuelRemarks", tripTO.getFuelRemarks());
        map.put("compensationFuel", tripTO.getCompensationFuel());
        map.put("filledFuel", tripTO.getFilledFuel());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("driverId", tripTO.getDriverId());

        System.out.println("the saveTripFuelRecovery" + map);
        int status = 0;
        try {

            int check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripFuelRecovery", map);
            System.out.println("check--" + check);
            if (check == 0) {
                status = (Integer) getSqlMapClientTemplate().insert("trip.saveTripFuelRecovery", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripFuelRecovery", map);
            }

            System.out.println("saveTripFuelRecovery size=" + status);

            // Consumed fule litters * current fuel price::::
            check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripFuelExpense", map);
            double fuelExpense = 0;
            fuelExpense = Double.parseDouble(tripTO.getConsumedFuel()) * Double.parseDouble(tripTO.getFuelPrice());
            System.out.println("fuelExpense:::" + fuelExpense);
            map.put("fuelExpense", fuelExpense);
            if (check != 0 && fuelExpense != 0) {
//                int update = (Integer) getSqlMapClientTemplate().update("trip.updateTripFuelExpense", map);
//                System.out.println("update:::" + update);
            } else if (check == 0) {
//                map.put("vehicleId", tripTO.getVehicleId());
//                map.put("expenseType", "2");
//                map.put("billMode", "0");
//                map.put("employeeName", "0");
//                map.put("activeValue", "0");
//                map.put("taxPercentage", "0");
//                map.put("expenseRemarks", "system Update");
//                map.put("currency", "1");
//                map.put("marginValue", "0");
//                map.put("netExpense", fuelExpense);
//                map.put("expenses", fuelExpense);
//                map.put("expenseName", "1010");
//                System.out.println("map value is:" + map);
//
//                int update = (Integer) getSqlMapClientTemplate().update("trip.saveTripExpense", map);
//                System.out.println("update:::" + update);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public ArrayList getTripFuelRecovery(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());

        ArrayList tripFuelRecovery = new ArrayList();
        try {
            System.out.println(" mapiss" + map);
            tripFuelRecovery = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripFuelRecovery", map);
            System.out.println("tripFuelRecovery size=" + tripFuelRecovery.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOtherExpenseDetails List", sqlException);
        }

        return tripFuelRecovery;
    }

//    public int saveTripExtraFuelDetailes(TripTO tripTO) {
//        Map map = new HashMap();
//        int status = 0;
//        int expenseId = 0;
//        int county = 0;
//        int countx = 0;
//        int insertStatus = 0;
//        map.put("createdBy", tripTO.getUserId());
//        map.put("tripId", tripTO.getTripSheetId());
//        map.put("vehicleId", tripTO.getVehicleId());
//        map.put("hireVehilceNo", "");
//        map.put("returnTripId", "0");
//        map.put("tripSheetId", tripTO.getTripSheetId());
//        String[] fuelMode = tripTO.getFuelMode();
//        String[] bunkName = tripTO.getBunkNames();
//        String[] slipNo = tripTO.getSlipNo();
//        String[] fuelDate = tripTO.getFuelDates();
//        String[] fuelLtrs = tripTO.getFuelLtrs();
//        String[] fuelAmount = tripTO.getFuelAmounts();
//        String[] fuelFilledName = tripTO.getFuelFilledName();
//        String[] fuelRemarks = tripTO.getExtraFuelRemarks();
//        String[] fuelPriceRemarks = tripTO.getFuelPriceRemarks();
//        String[] fuelPriceRemarksEn = tripTO.getFuelPriceRemarksEn();
//        String[] tripAdvanceId = tripTO.getTripAdvanceIds();
//
//        String[] tripFuelId = tripTO.getUniqueIdE();
//        String[] bunkIdE = tripTO.getBunkIdE();
//        String[] slipNoE = tripTO.getSlipNoE();
//        String[] fuelDateE = tripTO.getFuelDateE();
//        String[] fuelLtrsE = tripTO.getFuelLtrsE();
//        String[] fuelAmountE = tripTO.getFuelAmountE();
//        String[] fuelRemarksE = tripTO.getFuelRemarksE();
//        String[] fuelPriceRemarksE = tripTO.getFuelPriceRemarksE();
//        Double fuelTripAmount = 0.00;
//        Double fuelTripCashAmount = 0.00;
//        try {
//            //            System.out.println("fuelMode.length--" + fuelMode.length);
//            if (fuelMode != null) {
//                for (int i = 0; i < fuelMode.length; i++) {
//                    map.put("fuelMode", fuelMode[i]);
//                    System.out.println("fuelMode[i]---" + fuelMode[i]);
////                        map.put("bunkName", bunkName[i]);
//                    System.out.println("bunkName[i]---" + bunkName[i]);
//                    map.put("slipNo", slipNo[i]);
//                    map.put("fuelDate", fuelDate[i]);
//                    map.put("fuelLtrs", fuelLtrs[i]);
//                    map.put("fuelAmount", fuelAmount[i]);
//                    map.put("fuelFilledName", fuelFilledName[i]);
//                    map.put("fuelRemarks", fuelRemarks[i]);
////                        if("2".equals(fuelMode[i])){
////                            map.put("fuelPriceRemarks", fuelPriceRemarksEn[i]);
////                        }else{
//                    map.put("fuelPriceRemarks", fuelPriceRemarks[i]);
////                        }
//                    if ("2".equals(fuelMode[i]) && "0".equals(bunkName[i])) {
//                        System.out.println("Bunk20");
//                        map.put("bunkName", "20"); // IOCL Fleet
//                    } else {
//                        map.put("bunkName", bunkName[i]);
//                    }
//                    map.put("bunkPlace", "");
//                    System.out.println("the saveBillHeader" + map);
//                    status = (Integer) getSqlMapClientTemplate().update("operation.insertTripFuel", map);
//                    System.out.println("status--" + status);
//                }
//
//                System.out.println("fuelTripCashAmount---" + fuelTripCashAmount);
//            }
//
//            //            System.out.println("tripFuelId.length----" + tripTO.getUniqueIdE().length);
//            if (tripTO.getUniqueIdE() != null) {
//                for (int j = 0; j < tripTO.getUniqueIdE().length; j++) {
//                    String[] tmp = bunkIdE[j].split("~");
//                    map.put("bunkIdE", tmp[0]);
//                    System.out.println("bunkIdE[j]---" + bunkIdE[j]);
//                    map.put("tripFuelId", tripFuelId[j]);
//                    map.put("slipNoE", slipNoE[j]);
//                    map.put("fuelDateE", fuelDateE[j]);
//                    map.put("fuelLtrsE", fuelLtrsE[j]);
//                    map.put("fuelAmountE", fuelAmountE[j]);
//                    map.put("fuelAmount", fuelAmountE[j]);
//                    //                  map.put("fuelFilledName", fuelFilledName[i]);
//                    map.put("fuelRemarksE", fuelRemarksE[j]);
//                    map.put("fuelPriceRemarksE", fuelPriceRemarksE[j]);
//                    System.out.println("the updateTripFuelDetails" + map);
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripFuelDetails", map);
//                    System.out.println("updateTripFuelDetails--" + status);
//                }
//            }
//            int updateStatus = 0;
//            String temp[];
//            ArrayList getFuelExpense = new ArrayList();
//            getFuelExpense = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getFuelEposExpense", map);
//            Iterator itr = getFuelExpense.iterator();
//            TripTO trpto = new TripTO();
//            while (itr.hasNext()) {
//                trpto = (TripTO) itr.next();
//                temp = trpto.getFuelExpense().split("~");
//                System.out.println("trpto.getFuelExpense()------------" + trpto.getFuelExpense());
//                map.put("tripId", tripTO.getTripSheetId());
//
//                if ("21".equals(temp[1])) {
//                    fuelTripCashAmount = Double.parseDouble(temp[0]);
//                    map.put("expenseId", 1047); // fuel cash expense
//                    map.put("expense", fuelTripCashAmount);
//
//                    System.out.println("expense--if--" + fuelTripAmount);
//                    updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateFuelCashExpense", map);
//                    System.out.println("IF 21 FUEL CASH EXP--" + updateStatus);
//                    if (updateStatus == 0) {
//                        map.put("fuelCashAmount", fuelTripCashAmount);
//                        map.put("remarks", "FUEL DRIVER CASH");
//                        System.out.println("fuelTripCashAmount----" + fuelTripCashAmount);
//                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertFuelCashExpense", map);
//                        System.out.println("IF 21 -insert--" + status);
//                    }
//                } else {
//                    System.out.println("ELSE----");
//                    fuelTripAmount = Double.parseDouble(temp[0]);
//                    map.put("expenseId", 1010);  // fuel expense
//                    map.put("expense", fuelTripAmount);
//                    System.out.println("expense--else--" + fuelTripAmount);
//                    updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateFuelCashExpense", map);
//                    System.out.println("ELSE 21  FUEL EXP--" + updateStatus);
//                    if (updateStatus == 0) {
//                        map.put("fuelCashAmount", fuelTripAmount);
//                        map.put("remarks", "FUEL Expense");
//                        System.out.println("fuelTripAmount----" + fuelTripAmount);
//                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertFuelCashExpense", map);
//                        System.out.println("ELSE 21 insert--" + status);
//                    }
//                }
//            }
//
//            System.out.println("saveBillHeader size=" + status);
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
//        }
//
//        return status;
//    }
    public String getTripHirevehicleNo(TripTO tripTO) {
        Map map = new HashMap();
        String vehicle = "";
        map.put("tripId", tripTO.getTripSheetId());
        map.put("tripSheetId", tripTO.getTripSheetId());

        try {

            System.out.println("fuelMode.length--" + map);
            vehicle = (String) getSqlMapClientTemplate().queryForObject("trip.getTripHirevehicleNo", map);
            System.out.println("vehicle no value=" + vehicle);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return vehicle;
    }

    public int updateTripHireVehicleNo(TripTO tripTO, String hireVehicleNo, String hireMobileNo) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("hireVehilceNo", hireVehicleNo);
        map.put("hireMobileNo", hireMobileNo);
        map.put("tripSheetId", tripTO.getTripSheetId());

        try {
            System.out.println("mapppp.length--" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripHireVehicleNo", map);
            System.out.println("sasas size=" + status);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripHireDriverNo", map);
            System.out.println("sasas size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int updateOffloadMovementinTrip(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        String vehicleId = "";
        map.put("tripId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("tripSheetId", tripTO.getTripSheetId());

        try {
            System.out.println("mapppp.length--" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateOffloadMovementStatusinTrip", map);
            System.out.println("sasas size=" + status);
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
            System.out.println("vehicle id=" + vehicleId);
            if (!"0".equals(vehicleId) && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripVehicleStatus", map);
                System.out.println("vehicle statau=" + status);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityStatus", map);
                System.out.println("vehicle statau=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int createOffloadMovementinTrip(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("tripSheetId", tripTO.getTripSheetId());

        try {

            System.out.println("mapppp.length--" + map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripHireVehicleNo", map);
            System.out.println("sasas size=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public ArrayList getoffloadContractRoutesOrigin(TripTO tripTO) {
        Map map = new HashMap();
        String temp = "";
        ArrayList contractRouteList = new ArrayList();
        map.put("tripId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("tripSheetId", tripTO.getTripSheetId());

        try {
            System.out.println("mapppp.ewewewew--" + map);
            temp = (String) getSqlMapClientTemplate().queryForObject("trip.getTripDetailsForOffloadMov", map);
            System.out.println("temp---" + temp);
            if (temp != null) {
                String[] temps = temp.split("~");
                map.put("customerId", temps[0]);
                map.put("vehicleTypeId", temps[1]);
                map.put("containerQty", temps[2]);
                map.put("movementTypeId", temps[3]);
                map.put("contractId", temps[4]);
                System.out.println("my map values :::" + map);
                contractRouteList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getContractRoutesOrigin", map);
            }

            System.out.println("sasas size=" + contractRouteList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return contractRouteList;
    }

    public String checkContainerExist(TripTO tripTO) {
        Map map = new HashMap();
        String containerNo = "";
        map.put("containerNo", tripTO.getContainerNo());

        try {
            System.out.println("mapppp.ewewewew--" + map);
            containerNo = (String) getSqlMapClientTemplate().queryForObject("trip.checkContainerExist", map);
            System.out.println("sasas size=" + containerNo);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return containerNo;
    }

    public int updateKMDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("activeInd", tripTO.getActiveInd());
        map.put("userId", userId);

        try {
            map.put("openKM", Double.parseDouble(tripTO.getOpenKM()));
            map.put("closingKM", Double.parseDouble(tripTO.getClosingKM()));
            map.put("totalKM", Double.parseDouble(tripTO.getTotalKM()));
            System.out.println("the updateEndTripSheet" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripClosingKm", map);
            System.out.println("sasas size=" + status);

            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripCloseVehicleKm", map);
            System.out.println("sasas size=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;
    }

    public String getOtherExpenses(String tripid, String vehicleId) {
        Map map = new HashMap();
        String otherExp = "";
        map.put("tripId", tripid);
        map.put("vehicleId", vehicleId);

        try {
            System.out.println("mapppp.getOtherExpenses--" + map);
            otherExp = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenses", map);
            System.out.println("sasas size=" + otherExp);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("otherExp Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "otherExp List", sqlException);
        }

        return otherExp;
    }

    public int updateTripContainers(String tripId, String[] tripConId, String[] conNo, String[] tonnage, String[] sealNo, String[] custRefNo, String freightAmount, String containerInsAmount, String vendorId, String hireVehicleNo, String type, int userId) {
        Map map = new HashMap();
        int status = 0;
        int consignmentContainerId = 0;
        map.put("tripId", tripId);

        try {
            if (type.equals("1")) {

                for (int i = 0; i < tripConId.length; i++) {
                    map.put("tripConId", tripConId[i]);
                    map.put("conNo", conNo[i]);
                    map.put("tonnage", tonnage[i]);
                    map.put("sealNo", sealNo[i]);
                    map.put("custRefNo", custRefNo[i]);
                    map.put("freightAmount", freightAmount);
                    System.out.println("map==========" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripContainers", map);
                    System.out.println("status ::" + status);
                    consignmentContainerId = (Integer) getSqlMapClientTemplate().queryForObject("trip.getConsignmentContainerId", map);
                    map.put("consignmentContainerId", consignmentContainerId);
                    System.out.println("consignmentContainerId:::" + consignmentContainerId);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateConsignmentContainerCustRefNo", map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateFreihtRevenue", map);
                    System.out.println("updateFreihtRevenue ::" + status);
//                status = (Integer) getSqlMapClientTemplate().update("trip.updateLhcFreihtRevenue", map);
//                System.out.println("updateLhcFreihtRevenue ::" + status);
                }
            } else {

                String expenseId = "";
                expenseId = "1050";
                map.put("expenseRemarks", "Container Insurance Payable");
                if ("".equals(containerInsAmount) || containerInsAmount == null) {
                    map.put("expenses", "0");
                    map.put("netExpense", "0");
                } else {
                    map.put("expenses", containerInsAmount);
                    map.put("netExpense", containerInsAmount);
                }
                if ("".equals(vendorId) || vendorId == null) {
                    map.put("vendorId", "0");
                } else {
                    map.put("vendorId", vendorId);
                }
                if ("".equals(hireVehicleNo) || hireVehicleNo == null) {
                    map.put("hireVehicleNo", "0");
                } else {
                    map.put("hireVehicleNo", hireVehicleNo);
                }

                map.put("borneBys", "0");
                map.put("currency", "1");
                map.put("marginValue", "0");
                map.put("expenseDate", "");
                map.put("expenseId", expenseId);
                map.put("expenseType", "3");
                map.put("userId", userId);

                int check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkContInsAmountExist", map);
                System.out.println("check---" + check);
                if (check > 0) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpenseDuringClosure", map);
                    System.out.println("my updateTripOtherExpenseDuringClosure==" + status);
                } else {
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertContInsAmount", map);
                    System.out.println("insertTripOtherExpense size=");
                }
            }

            System.out.println("updateTripContainer size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public String getRepaireMaintainStatus(String tripid) {
        Map map = new HashMap();
        String RandM = "";
        map.put("tripId", tripid);

        try {
            System.out.println("map getRepaireMaintainStatus " + map);
            RandM = (String) getSqlMapClientTemplate().queryForObject("trip.getRepaireMaintainStatus", map);
            System.out.println(" size : " + RandM);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getRepaireMaintainStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getRepaireMaintainStatus List", sqlException);
        }

        return RandM;
    }

    public ArrayList getRepairAndMaintanance(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList repairAndMaintanance = new ArrayList();
        try {
            map.put("companyId", tripTO.getCompanyId());
            System.out.println(" mapiss" + map);
            repairAndMaintanance = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getRepairAndMaintanance", map);
            System.out.println("repairAndMaintanance size=" + repairAndMaintanance.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOtherExpenseDetails List", sqlException);
        }

        return repairAndMaintanance;

    }

    public int updateRepairAndMaintanance(TripTO tripTO, String approvStatus, String tripExpenId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("approvStatus", approvStatus);
            map.put("tripExpenseId", tripExpenId);

            System.out.println("the update=======" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateRepairAndMaintanance", map);
            System.out.println("sasas size=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;

    }

    public String mileageDetails(TripTO tripTO) {
        Map map = new HashMap();
        String kmDetails = "";
        try {
            map.put("tripId", tripTO.getTripSheetId());
            map.put("vehicleId", tripTO.getVehicleId());
            kmDetails = (String) getSqlMapClientTemplate().queryForObject("trip.mileageDetails", map);
            System.out.println("kmDetails =" + kmDetails);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("mileageDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "mileageDetails", sqlException);
        }
        return kmDetails;
    }

    public int updatePlannedStatusVesselEmpty(TripTO tripTO, String containerTypeId, String containerNo, String uniqueId, String isRepo, String sealNo, String grStatus, String orderId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int lastStatus = 0;
        try {
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            if ("1".equals(tripTO.getRepoId())) {
                map.put("repoId", 1);
            } else if ("2".equals(tripTO.getRepoId())) {
                map.put("repoId", 2);

            } else {
                map.put("repoId", 0);
            }
//            System.out.println("containerTypeId.length" + containerTypeId.length);
            if (containerNo != null) {
//                for (int i = 0; i < containerTypeId.length; i++) {
                map.put("containerTypeId", containerTypeId);
                if ("".equals(containerNo) || containerNo == null) {
                    map.put("containerNo", "NA");
                } else {
                    map.put("containerNo", containerNo);
                }
                map.put("uniqueId", uniqueId);
                if ("".equals(sealNo) || sealNo == null) {
                    map.put("sealNo", "NA");
                } else {
                    map.put("sealNo", sealNo);
                }

                map.put("grStatus", "To be Billed");
                map.put("consignmentOrderIds", orderId);
                String transporterType = "0"; //0 indicates market
                if (!"0".equals(tripTO.getVehicleId())) {
                    transporterType = "1";//own vehicle
                }
                map.put("transporterType", transporterType);
                System.out.println("updatePlannedStatus" + map);
                status = (Integer) session.update("trip.updatePlannedStatus", map);
                System.out.println("updatePlannedStatus1111111111:" + status);
//                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updatePlannedStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updatePlannedStatus List", sqlException);
        }
        return status;
    }

    public int saveTripSheetEmptyVessel(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int tripId = 0;
        try {
            map.put("singleTwentyStatus", 0);
            map.put("shipingLineNo", "0");
            map.put("billOfEntry", "0");
            map.put("tripCode", tripTO.getTripCode());
            map.put("companyId", tripTO.getCompanyId());
            map.put("customerId", tripTO.getCustomerId());
            map.put("productInfo", tripTO.getProductInfo());
            map.put("orderExpense", "0");
            map.put("orderRevenue", "0");
            map.put("profitMargin", "0");
            map.put("hireCharge", "0");
            map.put("movementType", tripTO.getMovementType());
            map.put("containerQuantity", "0");

            map.put("tripScheduleDate", tripTO.getStartDate());
            map.put("startDate", tripTO.getStartDate());
            map.put("vehicleactreportdate", tripTO.getEndDate());
            map.put("vehicleloadreportdate", tripTO.getEndDate());
            map.put("endDate", tripTO.getEndDate());

            map.put("tripScheduleTime", "00" + ":" + "00");
            map.put("startTime", "00" + ":" + "00");
            map.put("endTime", "00" + ":" + "00");
            map.put("vehicleactreporttime", "00" + ":" + "00");
            map.put("vehicleloadreporttime", "00" + ":" + "00");

            map.put("plannedEndDate", tripTO.getEndDate());
            map.put("tripPlanEndDate", tripTO.getEndDate());
            map.put("plannedEndTime", "00:00");
            map.put("tripPlanEndTime", "00:00");

            //            String[] pointPlanDate = tripTO.getPointPlanDate();
            //            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            //            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            //            if (pointPlanDate != null) {
            //            } else {
            //                map.put("tripScheduleDate", "00-00-0000");
            //                map.put("startDate", "00-00-0000");
            //                map.put("endDate", "00-00-0000");
            //                map.put("vehicleactreportdate", "00-00-0000");
            //                map.put("vehicleloadreportdate", "00-00-0000");
            //
            //            }
            ////            if (pointPlanTimeHrs != null) {
            //                map.put("tripScheduleTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            //                map.put("startTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            //                map.put("endTime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            //                map.put("vehicleactreporttime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            //                map.put("vehicleloadreporttime", pointPlanTimeHrs[0] + ":" + pointPlanTimeMins[0]);
            //            } else {
            //            }
            map.put("consignmentOrderId", tripTO.getOrderId());
            map.put("statusId", tripTO.getStatusId());
            map.put("transitHours", tripTO.getTripTransitHours());
            map.put("transitDay", "1");
            map.put("advnaceToBePaidPerDay", "0");
            map.put("userId", tripTO.getUserId());
            map.put("emptyTripPurpose", "");
            map.put("vehicleTypeName", tripTO.getVehicleTypeName());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());
            map.put("cNotes", tripTO.getcNotes());
            map.put("billingType", tripTO.getBillingType());
            map.put("customerName", tripTO.getCustomerName());
            map.put("customerType", tripTO.getCustomerType());
            map.put("routeInfo", tripTO.getRouteInfo());
            map.put("reeferRequired", "No");
            map.put("totalWeight", "0");
            map.put("preStartLocationStatus", "1");
            map.put("originId", tripTO.getOriginId());
            map.put("destinationId", tripTO.getDestinationId());
            System.out.println("tripTO.getDestinationId()=" + tripTO.getDestinationId());

            map.put("vehicleVendorId", tripTO.getVendorId());

            if (!"".equals(tripTO.getEstimatedKM())) {
                map.put("estimatedKm", tripTO.getEstimatedKM());
            } else {
                map.put("estimatedKm", "0");
            }

            map.put("tripType", "1");
            System.out.println("map value is:" + map);
            if ("1040".equals(tripTO.getCustomerId())) {
                map.put("emptyTripStatusId", "1");
            } else {
                map.put("emptyTripStatusId", "0");
            }
            System.out.println("map = " + map);
            tripId = (Integer) session.insert("trip.saveTripSheet", map);
            System.out.println("tripId=" + tripId + " Movement Type :::" + tripTO.getMovementType());

            // Update closure
            map.put("tripSheetId", tripId);
            map.put("tripId", tripId);
            map.put("startOdometerReading", "0");
            map.put("vehicleloadtemperature", "0");
            map.put("startHM", "0");
            map.put("statusId", tripTO.getStatusId());
            map.put("lrNumber", "0");
            int status = (Integer) session.update("trip.insertTripFuelVesselEmpty", map);
            System.out.println("statusstatusstatus:::" + status);
            status = (Integer) session.update("trip.updateStartTripSheet", map);
            System.out.println("status for Muthu ::::" + status);
            map.put("endOdometerReading", "");
            map.put("totalKM", "");
            map.put("totalHrs", "");
            map.put("totalDays", "");
            map.put("durationDay1", "");
            map.put("endHM", "0");
            status = (Integer) session.update("trip.updateEndTripSheet", map);

            status = (Integer) session.update("trip.getLRNumberSequence", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripSheet", sqlException);
        }
        return tripId;
    }

    public int saveTripVehicleVesselEmpty(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleVendorId", tripTO.getVendorId());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());

            //hireVehilceNo
            map.put("vehicleCapUtil", "0");
            map.put("vehicleCapUtil", "0");
            map.put("trailerId", "0");
            map.put("seatCapacity", "0");
            map.put("hireVehilceNo", tripTO.getHireVehicleNo());

            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripVehicle", map);
            System.out.println("saveTripVehicle=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripVehicle Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripVehicle", sqlException);
        }
        return status;
    }

    public int saveTripConatinerDetailsVesselEmpty(String containerTypeId, String containerNo, String uniqueId, String orderId, String tonnage,
            String sealNo, TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("originId", tripTO.getOriginId());

        System.out.println("the pretripsheetdetails" + map);
        int status = 0;
        try {
            if (containerTypeId != null) {
//                for (int i = 0; i < containerTypeId.length; i++) {
                map.put("containerTypeId", containerTypeId);
                if (tonnage == null || "".equals(tonnage)) {
                    map.put("tonnage", 0);
                } else {
                    map.put("tonnage", tonnage);
                }
                if (sealNo == null || "".equals(sealNo)) {
                    map.put("sealNo", "NA");
                } else {
                    map.put("sealNo", sealNo);
                }
                System.out.println("containerTypeId[i]" + containerTypeId);
                map.put("containerNo", containerNo);
                if ("1".equals(containerTypeId) || "2".equals(containerTypeId)) {
                    map.put("consignmentContainerId", uniqueId);
                    map.put("consignmentOrderId", orderId);
                } else {
                    map.put("consignmentContainerId", "0");
                    map.put("consignmentOrderId", "0");
                }

                System.out.println("the container map" + map);
                status = (Integer) session.insert("trip.insertContainerDetails", map);

                System.out.println("container Status..." + status);

//                }
                map.put("consignmentOrderId", status);
                status = (Integer) session.update("trip.getLRNumberSequence", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("savePreTripSheetDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "savePreTripSheetDetails List", sqlException);
        }

        return status;
    }

    public int saveTripStatusDetailsForVesselEmpty(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("statusId", "8");
            map.put("userId", tripTO.getUserId());
            map.put("durationDate", tripTO.getDurationDate());
            map.put("durationTime", tripTO.getDurationTime());
            map.put("remarks", "System Update");
            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripStatusDetails", map);
            System.out.println("saveTripStatusDetails=" + status);
            map.put("statusId", tripTO.getStatusId());
            status = (Integer) session.update("trip.saveTripStatusDetails", map);
            System.out.println("sssss=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripStatusDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripStatusDetails", sqlException);
        }
        return status;
    }
//    

    public int saveTripConsignmentsVesselEmpty(TripTO tripTO, String remainWeight, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        String statusID = "";
        String repoId = "";
        int newRepoId = 0;
        int plannedstatus = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            map.put("statusId", tripTO.getStatusId());
            map.put("vehicleId", tripTO.getVehicleId());
            String[] consignmentOrderIds = tripTO.getConsignmentOrderId();
            int j = 1;
            for (int i = 0; i < consignmentOrderIds.length; i++) {
                map.put("consignmentOrderId", consignmentOrderIds[i]);
                map.put("orderSequence", j);
                j++;
                System.out.println("map value is:" + map);
                statusID = (String) session.queryForObject("trip.getConsignmentStatus", map);
//                repoId = (String) session.queryForObject("trip.getConsignmentRepoId", map);
                plannedstatus = (Integer) session.queryForObject("trip.getPlannedContainer", map);
//                System.out.println("statusID:" + statusID + " order id:" + tripTO.getOrderType());
//                if ("1".equals(tripTO.getRepoId()) || "1".equals(repoId)) {
//                    if (plannedstatus > 0) {
//                        newRepoId = 1;
//                    } else {
//                        newRepoId = 2;
//                    }
//                } else {
//                    newRepoId = Integer.parseInt(tripTO.getRepoId());
//                }

                System.out.println("plannedstatus==" + plannedstatus);
                if (plannedstatus > 0) {
                    map.put("consignmentStatusId", "5");
                } else {
                    map.put("consignmentStatusId", "13");
                    status = (Integer) session.update("trip.updateConsignmentOrderStatus", map);
                }
                map.put("remainWeight", "0");
                map.put("repoId", "0");
                System.out.println("statusID:2");
//                }
                System.out.println("map2 value is:" + map);
                status = (Integer) session.update("trip.saveTripConsignments", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripConsignments Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripConsignments", sqlException);
        }
        return status;
    }
//    

    public int saveTripDriverVessellEmpty(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;

        try {
            map.put("tripId", tripTO.getTripId());
            map.put("driverId", tripTO.getPrimaryDriverId());
            map.put("userId", tripTO.getUserId());
            map.put("vehicleId", tripTO.getVehicleId());

            map.put("vehicleVendorId", tripTO.getVendorId());
            map.put("vehicleTypeId", tripTO.getVehicleTypeId());

            if (!"".equals(tripTO.getPrimaryDriverId())) {
                System.out.println("tripTO.getPrimaryDriverId()" + tripTO.getPrimaryDriverId());
                map.put("driverId", tripTO.getPrimaryDriverId());
            } else {
                map.put("driverId", 0);
            }
            System.out.println("tripTO.getPrimaryDriverName()" + tripTO.getPrimaryDriverName());
            map.put("primaryDriverName", tripTO.getPrimaryDriverName());
            map.put("secondaryDriverId", tripTO.getPrimaryDriverId());

            map.put("type", "P");
            System.out.println("tripTO.getPrimaryDriverName()" + tripTO.getPrimaryDriverName());
            map.put("driver1mobile", "0");

            System.out.println("map value is:" + map);
            status = (Integer) session.update("trip.saveTripDriver", map);
            System.out.println("status==statusstatus=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripDriver Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripDriver", sqlException);
        }
        return status;
    }
//    

    public int saveTripRoutePlanVesselEmpty(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("tripId", tripTO.getTripId());
            map.put("userId", tripTO.getUserId());
            String[] orderId = tripTO.getOrderIds();
            String[] pointId = tripTO.getPointId();
            String[] pointType = tripTO.getPointType();
            String[] pointOrder = tripTO.getPointOrder();
            String[] pointAddresss = tripTO.getPointAddresss();
            String[] pointPlanDate = tripTO.getPointPlanDate();
            String[] pointPlanTimeHrs = tripTO.getPointPlanTimeHrs();
            String[] pointPlanTimeMins = tripTO.getPointPlanTimeMins();
            String[] pointName = tripTO.getPointName();
            System.out.println("orderId.length:" + orderId.length);
            System.out.println("pointName.length:" + pointName.length);
            System.out.println("pointId.length:" + pointId.length);
            System.out.println("pointType.length:" + pointType.length);
            System.out.println("pointOrder.length:" + pointOrder.length);
            System.out.println("pointAddresss.length:" + pointAddresss.length);
            System.out.println("pointPlanDate.length:" + pointPlanDate.length);
            System.out.println("pointPlanTimeHrs.length:" + pointPlanTimeHrs.length);
            System.out.println("pointPlanTimeMins.length:" + pointPlanTimeMins.length);
            for (int i = 0; i < pointId.length; i++) {
                map.put("orderId", orderId[i]);
                map.put("pointName", pointName[i]);
                map.put("pointId", pointId[i]);
                map.put("pointType", pointType[i]);
                map.put("pointOrder", pointOrder[i]);
                String temp[] = pointAddresss[i].split("~");
                map.put("actualpointId", temp[0]);
                map.put("actualLatitude", temp[1]);
                map.put("actualLongtitude", temp[2]);
                map.put("pointAddresss", temp[3]);
                map.put("pointPlanDate", pointPlanDate[i]);
                map.put("pointPlanTime", pointPlanTimeHrs[i] + ":" + pointPlanTimeMins[i]);
                System.out.println("Route map value is:" + map);
                status = (Integer) session.update("trip.saveTripRoutePlan", map);
                System.out.println("saveTripRoutePlan=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveTripRoutePlan Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripRoutePlan", sqlException);
        }
        return status;
    }

    public int manualAdvanceRequestVesselEmpty(TripTO tripTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int manualAdvanceRequest = 0;

        try {

            map.put("hire", "0");
            map.put("balance", "0");
            map.put("mamul", "0");

            map.put("advancerequestamt", "0");
            map.put("requeston", tripTO.getDurationDate());
            map.put("requestremarks", "System generated");
            map.put("advanceRemarks", "System generated");
            map.put("advTrnType", "Cash");
            map.put("requeststatus", "1");
            map.put("tripid", tripTO.getTripId());
            map.put("tobepaidtoday", "0");
            map.put("batchType", "M");
            map.put("tripday", "1");
            map.put("currencyId", "1");
            map.put("userId", userId);
            System.out.println("map val" + map);

            manualAdvanceRequest = (Integer) session.insert("operation.manualAdvanceRequest", map);
            System.out.println("manualAdvanceRequest==" + manualAdvanceRequest);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("Manual Approval Request Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "ManualApprovalRequest", sqlException);
        }
        return manualAdvanceRequest;
    }

    public String getTatHours(TripTO tripTO) {
        Map map = new HashMap();
        String tatDetails = "";
        try {
            map.put("tripId", tripTO.getTripSheetId());
            tatDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTatHours", map);
            System.out.println("tatDetails =" + tatDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripFuelDeatils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return tatDetails;
    }

    public String getAddRowFuelPrice(String bunkId, String fuelDate) {
        Map map = new HashMap();
        String fuelRate = "";
        try {
            map.put("bunkId", bunkId);
            map.put("fuelDate", fuelDate);
            System.out.println("map :" + map);
            fuelRate = (String) getSqlMapClientTemplate().queryForObject("trip.getAddRowFuelPrice", map);
            System.out.println("fuel rate is=" + fuelRate);
            if ("".equals(fuelRate) || fuelRate == null) {
                fuelRate = "0";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getFuelPrice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getFuelPrice List", sqlException);
        }

        return fuelRate;
    }

    public ArrayList getTripCurrentStatus(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("subStatus", tripTO.getSubStatusId());
        ArrayList getTripCurrentStatus = new ArrayList();
//        try {
        System.out.println("map for point:" + map);
        try {
            getTripCurrentStatus = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripCurrentStatus", map);
            System.out.println("getTripCurrentStatus size=" + getTripCurrentStatus.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripCurrentStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripCurrentStatus List", sqlException);
        }

        return getTripCurrentStatus;
    }

//    public int changeRouteDeviation(TripTO tripTO) {
//        Map map1 = new HashMap();
//        int status = 0;
//        try {
//
//            String[] actualpointId = tripTO.getActualPointId();
//            String[] routeCourseId = tripTO.getRouteCourseId();
//            String[] pointName = tripTO.getPointAddresss();
//            String[] revenue = tripTO.getOrderRevenues();
//
//            System.out.println("point length :  " + actualpointId.length);
//
//            for (int i = 0; i < actualpointId.length; i++) {
//
//                if (!actualpointId[i].equals("0")) {
//                    map1.put("pointId", actualpointId[i]);
//                    map1.put("pointAddresss", pointName[i]);
//                    map1.put("revenue", revenue[i]);
//                    map1.put("tripRouteCourseId", routeCourseId[i]);
//
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateRouteDeviation", map1);
//
//                    map1.put("tripId", tripTO.getTripId());
//                    map1.put("deviation", "1");
//
//                    String routeInfo = (String) getSqlMapClientTemplate().queryForObject("trip.getTripRouteInfo", map1);
//
//                    String[] route = routeInfo.split("-");
//
//                    String routeDetail = route[0] + "-" + pointName[i] + "-" + route[2];
//
//                    map1.put("routeInfo", routeDetail);
//
//                    System.out.println("changeRouteDeviation map:  " + map1);
//
//                    status = (Integer) getSqlMapClientTemplate().update("trip.updateRouteDeviationInTrip", map1);
//                }
//            }
//
//        } catch (Exception sqlException) {
//            sqlException.printStackTrace();
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            FPLogUtils.fpDebugLog("changeRouteDeviation Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            throw new FPRuntimeException("EM-MRS-01", CLASS, "changeRouteDeviation List", sqlException);
//        }
//
//        return status;
//    }
    public int changeRouteDeviation(TripTO tripTO) {
        Map map1 = new HashMap();
        int status = 0;
        try {

            String actualpointId = tripTO.getPointIds();
            String pointName = tripTO.getRoute();
            map1.put("routeInfo", pointName);
            String revenue = tripTO.getOrderRevenue();

            map1.put("userId", tripTO.getUserId());
            String[] pointIds = actualpointId.split("-");
            String[] pointNames = pointName.split("-");
            System.out.println("point length :  " + pointIds.length);
            map1.put("tripId", tripTO.getTripId());

            int updateRouteCourse = (Integer) getSqlMapClientTemplate().update("trip.updateRouteCourse", map1);
            for (int i = 0; i < pointIds.length; i++) {

                if (!pointIds[i].equals("0")) {
                    map1.put("pointId", pointIds[i]);
                    map1.put("pointAddresss", pointNames[i]);
                    map1.put("pointName", pointNames[i]);
                    map1.put("revenue", revenue);
                    map1.put("pointSequence", (i + 1));
                    map1.put("consignmentOrderId", 0);
                    if (i == 0) {
                        map1.put("pointType", "PickUp");
                    } else if (pointIds.length == (i + 1)) {
                        map1.put("pointType", "Droup");
                    } else {
                        map1.put("pointType", "Loading");
                    }

                    status = (Integer) getSqlMapClientTemplate().update("trip.insertTripRouteCourse", map1);

                    map1.put("deviation", "1");
                    System.out.println("changeRouteDeviation map:  " + map1);

                    status = (Integer) getSqlMapClientTemplate().update("trip.updateRouteDeviationInTrip", map1);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("changeRouteDeviation Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "changeRouteDeviation List", sqlException);
        }

        return status;
    }

    public ArrayList getCustomerContractPoint(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripPointDetails = new ArrayList();
        try {
            System.out.println("map for point:" + map);
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCustomerContractPoint", map);
            System.out.println("getCustomerContractPoint size=" + tripPointDetails.size());

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerContractPoint Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerContractPoint List", sqlException);
        }

        return tripPointDetails;
    }

    public int changeMovementType(TripTO tripTO) {
        Map map1 = new HashMap();
        int status = 0;
        try {

            map1.put("tripId", tripTO.getTripId());
            map1.put("clearanceType", tripTO.getType());
            System.out.println("tripTO.getType() : " + tripTO.getType());
            String actClearance = (String) getSqlMapClientTemplate().queryForObject("trip.getTripActClearanceInfo", map1);
            System.out.println("actClearance : " + actClearance);

            if (!actClearance.equalsIgnoreCase(tripTO.getType())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateClearanceStatus", map1);
                System.out.println("status-updateClearanceStatus---" + status);
                if (status > 0) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateCleranceTripStatus", map1);

                    status = (Integer) getSqlMapClientTemplate().update("trip.updateMovementStatus1", map1);

                    status = (Integer) getSqlMapClientTemplate().update("trip.updateMovementStatus2", map1);
                }
            }

            status = (Integer) getSqlMapClientTemplate().update("trip.updateClearanceInTrip", map1);

            System.out.println("changeRouteDetails=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("changeRouteDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "changeRouteDetails List", sqlException);
        }

        return status;
    }

    public ArrayList getHaltingApproval(TripTO tripTO) {
        Map map = new HashMap();

        ArrayList haltingApproval = new ArrayList();
        try {
            System.out.println(" mapiss" + map);
            map.put("pageType", tripTO.getPageType());
            map.put("roleId", tripTO.getRoleId());
            map.put("userId", tripTO.getUserId());
            map.put("compId", tripTO.getCompanyId());
            haltingApproval = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getHaltingApproval", map);
            System.out.println("haltingApproval size=" + haltingApproval.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getOtherExpenseDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getOtherExpenseDetails List", sqlException);
        }

        return haltingApproval;

    }

    public int updateCustomerHaltingApproval(TripTO tripTO, String approvStatus, String tripExpenId) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("approvStatus", approvStatus);
            map.put("tripExpenseId", tripExpenId);

            System.out.println("the update=======" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateCustomerHaltingApproval", map);
            System.out.println("sasas size=" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStatus List", sqlException);
        }

        return status;

    }

    public int updateDetentionClearanceCharge(TripTO tripTO, String typeId, int userId) {
        Map map = new HashMap();
        int status = 0;
        int statuss = 0;
        int tripId = 0;
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date curDate = new Date();
        System.out.println(dateFormat.format(curDate));

        String expenseDate = dateFormat.format(curDate);
        String expenseName = "";
        String expenseId = "";
        expenseName = "1021";
        expenseId = "1021";
        map.put("typeId", typeId);
        map.put("tripSheetId", tripTO.getTripSheetId());

        if (tripTO.getVendorId() == null || "".equals(tripTO.getVendorId())) {
            map.put("vendorId", "1");
        } else {
            map.put("vendorId", tripTO.getVendorId());
        }
        if (tripTO.getHireVehicleNo() == null || "".equals(tripTO.getHireVehicleNo())) {
            map.put("hireVehicleNo", "0");
        } else {
            map.put("hireVehicleNo", tripTO.getHireVehicleNo());
        }

        map.put("haltStartDate", tripTO.getHaltStartDate());
        map.put("haltStartHour", tripTO.getHaltStartHour());
        map.put("haltStartMinute", tripTO.getHaltStartMinute());

        map.put("haltEndDate", tripTO.getHaltEndDate());
        map.put("haltEndHour", tripTO.getHaltEndHour());
        map.put("haltEndMinute", tripTO.getHaltEndMinute());

        if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
            map.put("vehicleId", "0");
        } else {
            map.put("vehicleId", tripTO.getVehicleId());
        }
        if (tripTO.getVendorHaltingCharge() == null || "".equals(tripTO.getVendorHaltingCharge())) {
            map.put("vendorHaltingCharge", "0");
        } else {
            map.put("vendorHaltingCharge", tripTO.getVendorHaltingCharge());
        }
        map.put("billMode", "0");
        map.put("expenseName", expenseName);
        map.put("employeeName", "0");
        map.put("activeValue", "0");
        if ("0".equals(tripTO.getExpenseValue())) {
            map.put("approval", "1");
        } else {
            map.put("approval", "2");
        }
        map.put("taxPercentage", "0");

        map.put("haltDays", tripTO.getTripHaltDay());
//        map.put("haltCntrAmt", tripTO.getHaltCntrAmt());
        map.put("haltCntrAmt", tripTO.getExpenseValue());
        map.put("expenseRemarks", "Total days : " + tripTO.getTripDay());
        map.put("expenses", tripTO.getExpenseValue());
        map.put("netExpense", tripTO.getExpenseValue());

        map.put("borneBys", "0");
        map.put("currency", "1");
        map.put("marginValue", "0");
        map.put("expenseDate", expenseDate);
        map.put("expenseId", expenseId);
        map.put("expenseType", "1");
        map.put("contractRate", tripTO.getContractRate());

        map.put("userId", userId);

        System.out.println("the insertTripOtherExpense" + map);
        int check = 0;
        int insertStatus = 0;
        int codeval2 = 0;
        int codev2 = 0;
        try {
            System.out.println("typeId------" + typeId);
            if ("1".equals(typeId)) {
                check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseExist", map);
                System.out.println("my check point for trip expense duplication==" + check);
                int secondCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseIdExist", map);
                System.out.println("my check point for trip expense 2222==" + secondCheck);

                if (check == 0 && secondCheck == 0) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertTripOtherExpense", map);
                    System.out.println("insertTripOtherExpense size=");

                    //Acct Entry
                    String code2 = "";
                    String[] temp = null;
                    map.put("userId", userId);
                    map.put("DetailCode", "1");
                    map.put("voucherType", "%PAYMENT%");

                    code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
                    temp = code2.split("-");
                    codeval2 = Integer.parseInt(temp[1]);
                    codev2 = codeval2 + 1;
                    String voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    //select ledger details for the expense;
                    map.put("expenseId", expenseName);
                    String ledgerDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseLedgerInfo", map);

                    String[] tempVar = ledgerDetails.split("~");
                    map.put("ledgerId", tempVar[0]);
                    map.put("particularsId", tempVar[1]);

                    map.put("amount", tripTO.getExpenseValue());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Other Expenses");
                    map.put("Reference", "Trip");

                    map.put("SearchCode", tripTO.getTripSheetId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }
                } else {
                    int tripExpenseId = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseId", map);
                    map.put("tripExpenseId", tripExpenseId);
                    System.out.println("tripExpenseId=====" + tripExpenseId);
                    System.out.println("tripTO.getExpenseValue()--%%%%%%-"+tripTO.getExpenseValue());
                    if ("0".equals(tripTO.getExpenseValue()) || "0.00".equals(tripTO.getExpenseValue()) || "".equals(tripTO.getExpenseValue())) {
                        map.put("approval", "1"); // auto approved
                    } else {
                        map.put("approval", "2");  // waiting for approval
                    }
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpenseDuringClosure", map);
                    System.out.println("my updateTripOtherExpenseDuringClosure==" + status);

                }
                statuss = (Integer) getSqlMapClientTemplate().update("trip.updateTripHaltDays", map);
                System.out.println("statuss---" + statuss);
            } else {
                expenseName = "1049";
                expenseId = "1049";
                map.put("expenseName", expenseName);
                map.put("expenseId", expenseId);
                map.put("clrStartDate", tripTO.getClrStartDate());
                map.put("clrStartHour", tripTO.getClrStartHour());
                map.put("clrStartMinute", tripTO.getClrStartMinute());

                map.put("clrEndDate", tripTO.getClrEndDate());
                map.put("clrEndHour", tripTO.getClrEndHour());
                map.put("clrEndMinute", tripTO.getClrEndMinute());

                map.put("clrDays", tripTO.getTripClrDay());
                map.put("clrCntrAmt", tripTO.getClrCntrAmt());
                map.put("expenses", tripTO.getClearanceCharge());
                map.put("netExpense", tripTO.getClearanceCharge());

                check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseExist", map);
                System.out.println("my check point for trip expense duplication==" + check);
                int secondCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseIdExist", map);
                System.out.println("my check point for trip expense 2222==" + secondCheck);

                if (check == 0 && secondCheck == 0) {
                    status = (Integer) getSqlMapClientTemplate().update("trip.insertTripOtherExpense", map);
                    System.out.println("insertTripOtherExpense size=");

                    //Acct Entry
                    String code2 = "";
                    String[] temp = null;
                    map.put("userId", userId);
                    map.put("DetailCode", "1");
                    map.put("voucherType", "%PAYMENT%");

                    code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
                    temp = code2.split("-");
                    codeval2 = Integer.parseInt(temp[1]);
                    codev2 = codeval2 + 1;
                    String voucherCode = "PAYMENT-" + codev2;
                    System.out.println("voucherCode = " + voucherCode);
                    map.put("voucherCode", voucherCode);
                    map.put("mainEntryType", "VOUCHER");
                    map.put("entryType", "PAYMENT");
                    //select ledger details for the expense;
                    map.put("expenseId", expenseName);
                    String ledgerDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getOtherExpenseLedgerInfo", map);

                    String[] tempVar = ledgerDetails.split("~");
                    map.put("ledgerId", tempVar[0]);
                    map.put("particularsId", tempVar[1]);

                    map.put("amount", tripTO.getExpenseValue());
                    map.put("Accounts_Type", "DEBIT");
                    map.put("Remark", "Other Expenses");
                    map.put("Reference", "Trip");

                    map.put("SearchCode", tripTO.getTripSheetId());
                    System.out.println("map1 =---------------------> " + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                    System.out.println("status1 = " + insertStatus);
                    //--------------------------------- acc 2nd row start --------------------------
                    if (insertStatus > 0) {
                        map.put("DetailCode", "2");
                        map.put("ledgerId", "52");
                        map.put("particularsId", "LEDGER-40");
                        map.put("Accounts_Type", "CREDIT");
                        System.out.println("map2 =---------------------> " + map);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                        System.out.println("status2 = " + insertStatus);
                    }
                } else {
                    int tripExpenseId = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseId", map);
                    map.put("tripExpenseId", tripExpenseId);
                    System.out.println("tripExpenseId=====" + tripExpenseId);
                    if (Integer.parseInt(tripTO.getExpenseValue()) == 0 || "".equals(tripTO.getExpenseValue())) {
                        map.put("approval", "1"); // auto approved
                    } else {
                        map.put("approval", "2");  // waiting for approval
                    }
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpenseDuringClosure", map);
                    System.out.println("my updateTripOtherExpenseDuringClosure==" + status);

                }
                statuss = (Integer) getSqlMapClientTemplate().update("trip.updateTripClearanceDays", map);
                System.out.println("statuss---" + statuss);
            }

            if ("1".equals(typeId)) {
                if (tripTO.getVendorHaltingCharge() != null && !"".equals(tripTO.getVendorHaltingCharge())) {
                    System.out.println("enterrd vendor halt");
                    map.put("expenseId", "1052");
                    map.put("expenseName", "1052");
                    map.put("expenseType", "3");
                    map.put("expenses", tripTO.getVendorHaltingCharge());
                    map.put("netExpense", tripTO.getVendorHaltingCharge());
                    System.out.println("getVendorHaltingCharge map " + map);
                    check = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripExpenseExist", map);
                    System.out.println("my check point for trip expense duplication==" + check);
                    if (check == 0) {
                        status = (Integer) getSqlMapClientTemplate().update("trip.insertTripOtherExpense", map);
                        System.out.println("insertTripOtherExpense size=");

                    } else {
                        int tripExpenseId = (Integer) getSqlMapClientTemplate().queryForObject("trip.getTripExpenseId", map);
                        map.put("tripExpenseId", tripExpenseId);
                        System.out.println("tripExpenseId=====" + tripExpenseId);
                         if ("0".equals(tripTO.getExpenseValue()) || "0.00".equals(tripTO.getExpenseValue()) || "".equals(tripTO.getExpenseValue())) {
                            map.put("approval", "1"); // auto approved
                        } else {
                            map.put("approval", "2");  // waiting for approval
                        }
                        status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOtherExpenseDuringClosure", map);
                        System.out.println("my updateTripOtherExpenseDuringClosure==" + status);

                    }
                    statuss = (Integer) getSqlMapClientTemplate().update("trip.updateTripVendorHalt", map);
                    System.out.println("statuss---" + statuss);
                }
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertTripFuel Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTripFuel List", sqlException);
        }

        return status;
    }

    public ArrayList getConsignmentDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList consignmentDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println("getConsignmentDetails map " + map);
            consignmentDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getConsignmentDetails", map);
            System.out.println("consignmentDetails.size() = " + consignmentDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripUnPackDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripUnPackDetails List", sqlException);
        }
        return consignmentDetails;
    }

    public String getMovementType(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        String movementType = "";
        System.out.println("map = " + map);
        try {
            movementType = (String) getSqlMapClientTemplate().queryForObject("trip.getMovementType", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkVehicleDriverAdvance", sqlException);
        }
        return movementType;

    }

    public int insertConsignmentDetails(String consignmentOrderId, String tripSheetId, String shipingLineNo, String billOfEntry, String jobReferenceNo, String custOrderRefNo, String movementType) {
        Map map = new HashMap();
        int insertSettlement = 0;
        map.put("tripSheetId", tripSheetId);
        map.put("shipingLineNo", shipingLineNo);
        map.put("billOfEntry", billOfEntry);
        map.put("jobReferenceNo", jobReferenceNo);
        map.put("custOrderRefNo", custOrderRefNo);
        map.put("consignmentOrderId ", consignmentOrderId);
        System.out.println("insertConsignmentDetails map===" + map);
        try {
            if ("1".equals(movementType)) {
                System.out.println("movementType==" + movementType);
                insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertExprtConsignmentDetails", map);
            } else if ("2".equals(movementType)) {
                System.out.println("movementType==" + movementType);
                insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertImportConsignmentDetails", map);
            } else {
                System.out.println("movementType==" + movementType);
                insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentDetails", map);
            }

            System.out.println("insertSettlement==" + insertSettlement);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return insertSettlement;
    }

    public ArrayList getBookingNumberList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("fromDate", tripTO.getFromDate());
        map.put("toDate", tripTO.getToDate());
        map.put("movementType", tripTO.getMovementType());
        ArrayList bookingNumberList = new ArrayList();
        try {
            System.out.println("map for point:" + map);
            bookingNumberList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getBookingNumberList", map);
            System.out.println("getBookingNumberList size=" + bookingNumberList.size());

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBookingNumberList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBookingNumberList List", sqlException);
        }

        return bookingNumberList;
    }

    public int updateBookingNumber(String movementType, String customerOrderReferenceNo, String jobReferenceNo, String consignmentOrderId, String shipingLineNo, String billOfEntry) {
        Map map = new HashMap();
        int updateBookingNumber = 0;
        map.put("shipingLineNo", shipingLineNo);
        map.put("billOfEntry", billOfEntry);
        map.put("jobReferenceNo", jobReferenceNo);
        map.put("custOrderRefNo", customerOrderReferenceNo);
        map.put("consignmentOrderId", consignmentOrderId);
        System.out.println("updateBookingNumber map===" + map);
        System.out.println("movementType  ===" + movementType);
        try {
            if ("1".equals(movementType) || "27".equals(movementType) || "26".equals(movementType) || "32".equals(movementType) || "31".equals(movementType) || "25".equals(movementType) || "4".equals(movementType) || "6".equals(movementType) || "20".equals(movementType)) {
                System.out.println("movementType1==" + movementType);
                updateBookingNumber = (Integer) getSqlMapClientTemplate().update("trip.insertExprtConsignmentDetails", map);
            } else if ("2".equals(movementType) || "29".equals(movementType) || "30".equals(movementType) || "28".equals(movementType) || "5".equals(movementType)) {
                System.out.println("movementType2==" + movementType);
                updateBookingNumber = (Integer) getSqlMapClientTemplate().update("trip.insertImportConsignmentDetails", map);
            } else {
                System.out.println("movementType3==" + movementType);
                updateBookingNumber = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentDetails", map);
            }

            System.out.println("updateBookingNumber==" + updateBookingNumber);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return updateBookingNumber;
    }

    public String getMovementTypeName(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        String movementType = "";
        System.out.println("map = " + map);
        try {
            movementType = (String) getSqlMapClientTemplate().queryForObject("trip.getMovementTypeName", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkVehicleDriverAdvance Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "checkVehicleDriverAdvance", sqlException);
        }
        return movementType;

    }

    public int insertConsignmentDetails(String haltingDays, String consignmentOrderId, String tripSheetId, String shipingLineNo, String billOfEntry, String jobReferenceNo, String custOrderRefNo, String movementType) {
        Map map = new HashMap();
        int insertSettlement = 0;
        map.put("tripSheetId", tripSheetId);
        map.put("shipingLineNo", shipingLineNo);
        map.put("billOfEntry", billOfEntry);
        map.put("jobReferenceNo", jobReferenceNo);
        map.put("custOrderRefNo", custOrderRefNo);
        map.put("consignmentOrderId", consignmentOrderId);
        map.put("haltingDays", haltingDays);
        System.out.println("insertConsignmentDetails map===" + map);
        try {
            if ("1".equals(movementType) || "27".equals(movementType) || "26".equals(movementType) || "32".equals(movementType) || "31".equals(movementType) || "25".equals(movementType) || "4".equals(movementType) || "6".equals(movementType) || "20".equals(movementType)) {
                System.out.println("movementType==" + movementType);
                insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertExprtConsignmentDetails", map);
            } else if ("2".equals(movementType) || "29".equals(movementType) || "30".equals(movementType) || "28".equals(movementType) || "5".equals(movementType)) {
                System.out.println("movementType==" + movementType);
                insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertImportConsignmentDetails", map);
            } else {
                System.out.println("movementType==" + movementType);
                insertSettlement = (Integer) getSqlMapClientTemplate().update("trip.insertConsignmentDetails", map);
            }
            int insertHaltingDays = (Integer) getSqlMapClientTemplate().update("trip.insertHaltingDays", map);
            System.out.println("insertSettlement==" + insertSettlement);
            System.out.println("insertHaltingDays==" + insertHaltingDays);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSettlement Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSettlement List", sqlException);
        }

        return insertSettlement;
    }

    public int checkTallyInvoiceFlag(TripTO tripTO, int userId) {
        Map map = new HashMap();
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("userId", userId);
        System.out.println("the saveTripClosure" + map);
        int status = 0;
        ArrayList tripDetails = new ArrayList();
        try {
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTallyInvoiceFlag", map);
            System.out.println("checkTallyInvoiceFlag size=" + tripDetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkTallyInvoiceFlag Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkTallyInvoiceFlag List", sqlException);
        }

        return status;
    }

    public ArrayList getAddRowFuelPriceList(TripTO tripTO) {
        Map map = new HashMap();
        map.put("fuelDate", tripTO.getFuelDate());
        map.put("bunkId", tripTO.getBunkId());
        ArrayList fuelPriceList = new ArrayList();
        try {
            System.out.println("map for point:" + map);
            fuelPriceList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getAddRowFuelPriceList", map);
            System.out.println("fuelPriceList size=" + fuelPriceList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBookingNumberList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBookingNumberList List", sqlException);
        }

        return fuelPriceList;
    }

    public ArrayList getTripDiversionList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getTripDiversionList = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
                map.put("vehicleId", "0");
            } else {
                map.put("vehicleId", tripTO.getVehicleId());
            }
            System.out.println(" Trip map is::" + map);
            getTripDiversionList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDiversionList", map);
            System.out.println("getTripDiversionList.size() = " + getTripDiversionList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDiversionList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripDiversionList List", sqlException);
        }
        return getTripDiversionList;
    }

    public ArrayList getTripOverTonnageList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList getTripOverTonnageList = new ArrayList();
        try {
            if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
                map.put("vehicleId", "0");
            } else {
                map.put("vehicleId", tripTO.getVehicleId());
            }
            map.put("tripSheetId", tripTO.getTripSheetId());
            System.out.println(" Trip map is::" + map);
            getTripOverTonnageList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripOverTonnageList", map);
            System.out.println("getTripOverTonnageList.size() = " + getTripOverTonnageList.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripOverTonnageList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripOverTonnageList List", sqlException);
        }
        return getTripOverTonnageList;
    }

    public ArrayList getDivertCityList(TripTO trpTo) {
        ArrayList cityList = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("companyId", trpTo.getCompanyId());
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDivertCityList", map);
            System.out.println("getCityList size=" + cityList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCityList List", sqlException);
        }

        return cityList;
    }

    public int checkContainerNoExists(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("containerNo", tripTO.getContainerNumber());
            map.put("tripId", tripTO.getTripId());
            System.out.println("map::" + map);
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkContainerNoExists", map);
            System.out.println("status::" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkContainerNoExists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkContainerNoExists", sqlException);
        }

        return status;
    }

    public int saveDiversionOverTonnage(TripTO tripTO, String chargeType, String isExport) {
        Map map = new HashMap();
        int statuss = 0;
        try {

            map.put("eWayBillNo", tripTO.geteWayBillNo());
            map.put("invoiceNo", tripTO.getInvoiceNo());
            map.put("invoiceValue", tripTO.getInvoiceValue());
            map.put("invoiceDate", tripTO.getInvoiceDate());
            map.put("invoiceWeight", tripTO.getInvoiceWeight());
            map.put("expiryDate", tripTO.getExpiryDate());
            map.put("expiryHour", tripTO.getExpiryHour());
            map.put("expiryMin", tripTO.getExpiryMin());
            map.put("expiryTime", tripTO.getExpiryHour() + ":" + tripTO.getExpiryMin() + ":00");
            System.out.println("the updateEndTripSheet 06-06-2020" + map);

            map.put("diversionId", tripTO.getDiversionId());
            map.put("driverName", tripTO.getDriverName());
            map.put("driverId", 0);
            map.put("driverCode", tripTO.getDriverCode());
            map.put("divertCityId", Integer.parseInt(tripTO.getDivertCityId()));
            map.put("divertCityCode", tripTO.getDivertCityCode());
            map.put("divertCharge", Double.parseDouble(tripTO.getDivertCharge()));
            map.put("divertRemarks", tripTO.getDivertRemarks());
            map.put("tripSheetId", tripTO.getTripSheetId());
            String tripIds = "";
            System.out.println("map----" + map);
            System.out.println("chargeType---" + chargeType);
            System.out.println("tripTO.getDiversionId()---" + tripTO.getDiversionId());

            if ("3".equals(chargeType)) {
                int updateInvoiceDetail = (Integer) getSqlMapClientTemplate().update("trip.updateTripForInvoiceDetail", map);
                System.out.println("updateInvoiceDetail---06-06-2020" + updateInvoiceDetail);
            }

            ArrayList vehicleChangeStatus = new ArrayList();

            if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
                map.put("vehicleId", "0");
            } else {
                map.put("vehicleId", tripTO.getVehicleId());
            }
            map.put("activeVehicle", tripTO.getActiveInd());
            System.out.println("activeVehicle-1---" + tripTO.getActiveInd());
            System.out.println("map-1---" + map);
            vehicleChangeStatus = (ArrayList) getSqlMapClientTemplate().queryForList("trip.checkVehicleChangeStatus", map);
            System.out.println("vehicleChangeStatusCOUNT---" + vehicleChangeStatus.size());
            System.out.println("tripTO.getTripId()-QQQ--+-" + tripTO.getTripSheetId());

            int cvcount = 0;
            TripTO tpTO = null;
            Iterator itr = vehicleChangeStatus.iterator();
            while (itr.hasNext()) {
                cvcount = vehicleChangeStatus.size();
                tpTO = (TripTO) itr.next();
                System.out.println("vehicleChangeStatus---" + tpTO.getChangeVehicleStatus());
                System.out.println("cvcount---" + cvcount);
                if ("N".equals(tripTO.getActiveInd()) && cvcount >= 2) {
                    map.put("tripIds", tripTO.getTripSheetId());
                } else if ("Y".equals(tripTO.getActiveInd()) && cvcount >= 2) {
                    tripIds = tripTO.getTripSheetId() + "A";
                    System.out.println("tripTO.getTripId()---+tripTO.getTripId()---" + tripTO.getTripSheetId());
                    map.put("tripIds", tripIds);
                } else if ("Y".equals(tripTO.getActiveInd()) && cvcount == 1) {
                    map.put("tripIds", tripTO.getTripSheetId());
                }
            }
            System.out.println("tripIds---new---" + tripIds);
            System.out.println("MAP@@@@@@@@@---" + map);
            if ("1".equals(chargeType)) {    //diversion charge
                if ("".equals(tripTO.getDiversionId())) {
                    statuss = (Integer) getSqlMapClientTemplate().update("trip.insertTripDiversion", map);
                    System.out.println("statuss--diversion-" + statuss);
                } else {
                    if (!"".equals(tripTO.getDiversionId())) {
                        statuss = (Integer) getSqlMapClientTemplate().update("trip.updateTripDiversion", map);
                        System.out.println("statuss-diversion updtAE--" + statuss);
                    }
                }
            }
            map.put("contractTonnage", tripTO.getContractTonnage());
            map.put("OTId", tripTO.getOTId());
            map.put("netWt", Double.parseDouble(tripTO.getNetWt()));
            if (Double.parseDouble(tripTO.getActWt()) == 0) {
                map.put("actWt", Double.parseDouble(tripTO.getNetWt()));
            } else {
                map.put("actWt", Double.parseDouble(tripTO.getActWt()));
            }
            map.put("oTWt", Double.parseDouble(tripTO.getOTWt()));
            map.put("chargePerTon", Double.parseDouble(tripTO.getChargePerTon()));
            map.put("OTCharges", Double.parseDouble(tripTO.getOTCharges()));
            map.put("rtoCharges", Double.parseDouble(tripTO.getRtoCharges()));
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("expenseId", "1048");
            if (Double.parseDouble(tripTO.getOTCharges()) == 0.00) {
                map.put("approveStatus", "1");
            } else {
                map.put("approveStatus", "0");
            }
            System.out.println("map-1---" + map);

            if ("2".equals(chargeType)) {   //ovwer tonnage

                if ("".equals(tripTO.getOTId()) && Double.parseDouble(tripTO.getContractTonnage()) > 0) {
                    System.out.println("Double.parseDouble(tripTO.getNetWt())000-----------" + Double.parseDouble(tripTO.getNetWt()));
                    statuss = (Integer) getSqlMapClientTemplate().update("trip.insertTripOverTonnage", map);
                    System.out.println("insertTripOverTonnage---" + statuss);
                } else {
                    if (!"".equals(tripTO.getOTId()) && Double.parseDouble(tripTO.getContractTonnage()) > 0) {
                        statuss = (Integer) getSqlMapClientTemplate().update("trip.updateTripOverTonnage", map);
                        System.out.println("updateTripOverTonnage---" + statuss);
                    }
                }
                map.put("tripSheetId", tripTO.getTripSheetId());
                int checkeRTOExpenseId = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkeRTOExpenseId", map);
                System.out.println("checkeRTOExpenseId::" + checkeRTOExpenseId);
                if (checkeRTOExpenseId == 0) {
                    statuss = (Integer) getSqlMapClientTemplate().update("trip.insertRTOExpense", map);
                    System.out.println("insertRTOExpense---" + statuss);
                } else {
                    if (!"".equals(tripTO.getRtoCharges())) {
                        statuss = (Integer) getSqlMapClientTemplate().update("trip.updateRTOExpense", map);
                        System.out.println("updateRTOExpense---" + statuss);
                    }
                }
            }
            System.out.println("saveDiversionOverTonnage size=" + statuss);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return statuss;
    }

    public ArrayList getTatList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tatDetails = new ArrayList();
        try {
            map.put("tripId", tripTO.getTripSheetId());
            System.out.println("getTatList---MAP=====" + map);
            tatDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTatList", map);
            System.out.println("tatDetails size=" + tatDetails.size());

//            tatDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTatHours", map);
//            System.out.println("tatDetails =" + tatDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tatDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return tatDetails;
    }

    public ArrayList getTatClrList(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tatDetails = new ArrayList();
        try {
            map.put("tripId", tripTO.getTripSheetId());
            System.out.println("getTatClrList---MAP=====" + map);
            tatDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTatClrList", map);
            System.out.println("tatClrDetails size=" + tatDetails.size());

//            tatDetails = (String) getSqlMapClientTemplate().queryForObject("trip.getTatHours", map);
//            System.out.println("tatDetails =" + tatDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tatClrDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getTripFuelDeatils", sqlException);
        }
        return tatDetails;
    }

    public ArrayList getTripActualPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        ArrayList tripPointDetails = new ArrayList();
//        try {
        System.out.println("map for point:" + map);
        try {
            String movementType = (String) getSqlMapClientTemplate().queryForObject("trip.getTripMovementType", map);
            System.out.println("movementType----" + movementType);

            if ("8".equals(movementType)) {
                tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripActualPointDetailsDistance", map);
                System.out.println("getTripActualPointDetailsDistance size=" + tripPointDetails.size());
            } else {
                tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripActualPointDetails", map);
                System.out.println("getTripActualPointDetails size=" + tripPointDetails.size());
            }

        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getTripPickupPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("companyId", tripTO.getCompanyId());
        ArrayList tripPointDetails = new ArrayList();
        System.out.println("map for point:" + map);
        try {
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripPickupPointDetails", map);
            System.out.println("getTripPickupPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripPickupPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getTripLoadingPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("companyId", tripTO.getCompanyId());
        ArrayList tripPointDetails = new ArrayList();
        System.out.println("map for point:" + map);
        try {
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripLoadingPointDetails", map);
            System.out.println("getTripLoadingPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripLoadingPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public ArrayList getTripDropPointDetails(TripTO tripTO) {
        Map map = new HashMap();
        map.put("tripId", tripTO.getTripId());
        map.put("companyId", tripTO.getCompanyId());
        ArrayList tripPointDetails = new ArrayList();
        System.out.println("map for point:" + map);
        try {
            tripPointDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTripDropPointDetails", map);
            System.out.println("getTripDropPointDetails size=" + tripPointDetails.size());
        } catch (Exception sqlException) {

            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTripDropPointDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTripPointDetails List", sqlException);
        }

        return tripPointDetails;
    }

    public int updateTripActualPointDetails(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int update = 0;
        int tripId = 0;
        map.put("tripId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("statusId", tripTO.getStatusId());
        try {
            System.out.println("mappingId.length---" + map);
//            for(int i=0;i<mappingId.length;i++){
//                System.out.println("mappingId"+i+"="+mappingId[i]);
//            }
            ///////////////////// new part //////////////////////////////////////
            String[] actualpointId = tripTO.getActualPointId();
            String[] actualpointname = tripTO.getActualPointName();
            String[] actualLatitude = tripTO.getActualLatitude();
            String[] actualLongtitude = tripTO.getActualLongtitude();
            String[] routeCourseId = tripTO.getRouteCourseId();
            String[] mappingIds = tripTO.getMappingIds();

            System.out.println("actualpointId.length:" + actualpointId.length);
            System.out.println("actualpointname.length:" + actualpointname.length);
            System.out.println("actualLatitude.length:" + actualLatitude.length);
            System.out.println("actualLongtitude.length:" + actualLongtitude.length);
            System.out.println("mappingId.length:" + mappingIds.length);
            int status1 = 0;
            for (int i = 0; i < actualpointId.length; i++) {

                map.put("actualpointId", actualpointId[i]);
                map.put("actualLatitude", actualLatitude[i]);
                map.put("actualLongtitude", actualLongtitude[i]);
                map.put("pointAddresss", actualpointname[i]);
                map.put("movementUpdate", "0");
                map.put("tripRouteCourseId", routeCourseId[i]);
                map.put("mappingIds", mappingIds[i]);

                if (!actualpointId[i].equals("0")) {
                    System.out.println("Route map value is:" + map);
                    status1 = (Integer) getSqlMapClientTemplate().update("trip.updateTripRoutePlan", map);
                    System.out.println("status1---" + status1);
                }

//                update = (Integer) getSqlMapClientTemplate().update("trip.updateTripMappingId", map);
                System.out.println("update=" + update);
                System.out.println("saveTripRoutePlan=" + status1);
            }

            ///////////////////// new part //////////////////////////////////////
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripOtherExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripOtherExpense List", sqlException);
        }

        return status;
    }

    public String getContractToTonnage(TripTO tripTO) {
        Map map = new HashMap();
        String getContractToTonnage = "0.00";
        String values = "";
        String tmp = "";
        try {
            map.put("tripId", tripTO.getTripSheetId());
            map.put("point1Id", tripTO.getPoint1Id());
            map.put("point2Id", tripTO.getPoint2Id());
            map.put("point3Id", tripTO.getPoint3Id());
            map.put("point4Id", tripTO.getPoint4Id());
            map.put("firstPointId", tripTO.getFirstPointId());
            map.put("finalPointId", tripTO.getFinalPointId());
            System.out.println("FFFFF----" + map);
            values = (String) getSqlMapClientTemplate().queryForObject("trip.getVehicleTypeAnTonnage", map);
            System.out.println("values---" + values);
            if (values == null || "".equals(values)) {
                tmp = "0~0";
            } else {
                tmp = values;
            }
            String temp[] = tmp.split("~");
            String vehicleTypeId = temp[0];
            String netWts = temp[1];

            map.put("vehicleTypeId", vehicleTypeId);
            map.put("netWts", netWts);
            System.out.println(" TripWWWW map is::" + map);

            getContractToTonnage = (String) getSqlMapClientTemplate().queryForObject("trip.getContractToTonnage", map);
            System.out.println("getContractToTonnage = " + getContractToTonnage);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractToTonnage Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getContractToTonnage List", sqlException);
        }
        return getContractToTonnage;
    }

    public int checkInvoiceNo(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {
            map.put("invoiceNo", tripTO.getInvoiceNo());
            map.put("tripId", tripTO.getTripId());
            System.out.println("map::" + map);
            status = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkInvoiceNoExists", map);
            System.out.println("status::" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkInvoiceNoExists Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkInvoiceNoExists", sqlException);
        }

        return status;
    }

    public ArrayList getTollDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tollDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            map.put("vehicleId", tripTO.getVehicleId());
            System.out.println(" Trip map-----1 is::" + map);
            String tripStartEndDetai = (String) getSqlMapClientTemplate().queryForObject("trip.getTripStartEndDetail", map);
            String[] tripDates = tripStartEndDetai.split("~");
            String startDate = tripDates[0];
            String endDate = tripDates[1];
            System.out.println("startDate:" + startDate);
            System.out.println("endDate:" + endDate);
            map.put("fromDate", startDate);
            map.put("toDate", endDate);
            System.out.println(" Trip map-----2 is::" + map);
            tollDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getTollDetails", map);
            System.out.println("tollDetails.size() = " + tollDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTollDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTollDetails List", sqlException);
        }
        return tollDetails;
    }

    public int updateTripToll(String tripId, String vehicleId, String[] tollId, String[] tollAmount) {
        Map map = new HashMap();
        int status = 0;
        map.put("tripId", tripId);
        map.put("vehicleId", vehicleId);
        double totalTollAmt = 0.00;
        try {
            System.out.println("tollId.length======" + tollId.length);
            for (int i = 0; i < tollId.length; i++) {
                totalTollAmt = totalTollAmt + Double.parseDouble(tollAmount[i]);
                System.out.println("totalTollAmt==" + totalTollAmt);
                map.put("tollId", tollId[i]);
                map.put("tollAmount", tollAmount[i]);
                System.out.println("map==========" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripToll", map);
                System.out.println("status ::" + status);
            }
            map.put("expRemarks", "FastTag");
            map.put("expenseId", "1011");
            int tollExpenseCheck = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkTripTollExpense", map);
            System.out.println("tollExpenseCheck=---" + tollExpenseCheck);
            if (tollExpenseCheck > 0) {
                String tollExpense = (String) getSqlMapClientTemplate().queryForObject("trip.getTollExpense", map);
                System.out.println("tollExpense---" + tollExpense);
                totalTollAmt = totalTollAmt + Double.parseDouble(tollExpense);
                map.put("expenseValue", totalTollAmt);
                System.out.println("map===1=======" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTollExpense", map);
                System.out.println("status= if =" + status);
            } else {
                map.put("expenseValue", totalTollAmt);
                System.out.println("map===2=======" + map);
                status = (Integer) getSqlMapClientTemplate().update("trip.insertTollExpense", map);
                System.out.println("status= else =" + status);
            }
            if (status > 0) {
                System.out.println("Trip Master Update");
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripTagStatus", map);
            }
            System.out.println("updateTripTag =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /* 
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateTripToll Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateTripToll List", sqlException);
        }

        return status;
    }

    public ArrayList getMappedTollDetails(TripTO tripTO) {
        Map map = new HashMap();
        ArrayList tollDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTO.getTripSheetId());
            map.put("vehicleId", tripTO.getVehicleId());
            System.out.println(" Trip map-----1 is::" + map);
            tollDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getMappedTollDetails", map);
            System.out.println("tollDetails.size() = " + tollDetails.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getMappedTollDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getMappedTollDetails List", sqlException);
        }
        return tollDetails;
    }

    public int cancelTrip(String tripSheetId, String tripStatusId, String cancelRemarks, String consignmentOrderStatus, int userId) {
        Map map = new HashMap();
        int cancelTrip = 0;
        map.put("userId", userId);
        try {
            try {
                map.put("tripSheetId", tripSheetId);
                map.put("consignmentOrderStatus", consignmentOrderStatus);
                map.put("tripStatusId", tripStatusId);
                map.put("remarks", cancelRemarks);
                map.put("updateType", "System Update");
                map.put("userId", userId);
                System.out.println("mpa = " + map);
                cancelTrip = (Integer) getSqlMapClientTemplate().update("trip.cancelTrip", map);
                    int cancelConsignment = (Integer) getSqlMapClientTemplate().update("trip.cancelOrder", map);
                    System.out.println("cancelConsignment==" + cancelConsignment);

                    int updateVehicleAvailability = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailability", map);
                    System.out.println("updateVehicleAvailability==@@@@@@===" + updateVehicleAvailability);
//                if (cancelTrip > 0) {
//                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("cancelTrip Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "cancelTrip", sqlException);
        }

        return cancelTrip;
    }

    public int updateTripSheetEmptyVessel(TripTO tripTO, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripId", tripTO.getTripId());

            String value = (String) session.queryForObject("trip.getTripVesselEmptydata", map);
            String[] val = value.split("~");

            map.put("orderRevenue", val[0]);
            map.put("containerQuantity", val[1]);
            System.out.println(" updateTripSheetEmptyVessel map = " + map);
            status = (Integer) session.update("trip.updateTripSheetEmptyVessel", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripSheet Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripSheet", sqlException);
        }
        return status;
    }

    public int updateOffloadTripEnd(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        String vehicleId = "";
        map.put("tripId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("vendorId", tripTO.getVendorId());
        map.put("tripSheetId", tripTO.getTripSheetId());
        map.put("offloadStatus", "2");

        try {
            System.out.println("mapppp.length--" + map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateOffloadTripEnd", map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripEnd", map);
            System.out.println("sasas size=" + status);
//            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
//            System.out.println("vehicle id=" + vehicleId);
            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOffloadStatus", map);
            System.out.println("vehicle statau=" + status);

            if (!"0".equals(tripTO.getVehicleId()) && !"".equals(tripTO.getVehicleId())) {
                status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityStatus", map);
                System.out.println("vehicle avail=" + status);
            }

//                map.put("vehicleId", vehicleId);
//                 map.put("offloadStatus", "2");
////                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripVehicleStatus", map);
////                System.out.println("vehicle statau=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int updateTripEnd(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        String vehicleId = "";
        map.put("tripId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("tripSheetId", tripTO.getTripSheetId());

        try {
            System.out.println("mapppp.length--" + map);
//            status = (Integer) getSqlMapClientTemplate().update("trip.updateTripEnd", map);
//            System.out.println("sasas size=" + status);
            vehicleId = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVehicleId", map);
            System.out.println("vehicle id=" + vehicleId);
            if (!"0".equals(vehicleId) && !"".equals(vehicleId)) {
                map.put("vehicleId", vehicleId);
                map.put("offloadStatus", "1");
                status = (Integer) getSqlMapClientTemplate().update("trip.updateTripOffloadStatus", map);
                System.out.println("vehicle statau=" + status);
                status = (Integer) getSqlMapClientTemplate().update("trip.updateVehicleAvailabilityStatus", map);
                System.out.println("vehicle statau=" + status);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveChangeVehicleTripEnd(TripTO tripTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int updateTripMaster = 0;
        int updateDriverDetails = 0;
        int insertDriverDetails = 0;
        int updateTripVehicle = 0;

        map.put("driverChangeDate", tripTO.getDriverChangeDate());
        map.put("tripId", tripTO.getTripId());
        map.put("tripSheetId", tripTO.getTripId());
        map.put("tripIds", tripTO.getTripIdNew());
//        map.put("vehicleNo", tripTO.getVehicleNo());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("vehicleType", tripTO.getVehicleType());
        map.put("startKm", tripTO.getOdometerReading());
        map.put("startHm", 0.00);

        if ("".equals(tripTO.getVehicleCapUtil())) {
            map.put("vehicleCapUtil", 0.00);
        } else {
            map.put("vehicleCapUtil", tripTO.getVehicleCapUtil());
        }
        String vendId[] = tripTO.getVendorId().split("~");
        map.put("vehicleTonnage", tripTO.getVehicleTonnage());
        map.put("vendorId", vendId[0]);

        map.put("driverName", tripTO.getDriverName());
        map.put("driverMobile", tripTO.getDriverMobile());
        map.put("driverId", tripTO.getDriver1Id());
        map.put("userId", userId);

        try {

            String veh = tripTO.getVehicleNo();
            System.out.println("vehicleNO###########" + veh);
            if (tripTO.getVehicleNo().contains("~")) {
                map.put("vehicleNo", null);
            } else {
                map.put("vehicleNo", tripTO.getVehicleNo());
            }

            map.put("activeVehicle", "N");
            if (tripTO.getVehicleId() == null || "".equals(tripTO.getVehicleId())) {
                map.put("vehicleId", "0");
            } else {
                map.put("vehicleId", tripTO.getVehicleId());
            }
            System.out.println("map @@@###= " + map);

//            int offloadStatus = (Integer) getSqlMapClientTemplate().queryForObject("trip.checkVehChangeOffloadStatus", map); 
//            map.put("offloadStatus", offloadStatus);
//            status = (Integer) getSqlMapClientTemplate().update("trip.insertTripVehicleDetails", map);
//            System.out.println("status###==" + status);
//            }
            String[] tempVendorId = tripTO.getVendorId().split("~");
            map.put("vendorId", tempVendorId[0]);

            updateTripMaster = (Integer) getSqlMapClientTemplate().update("trip.insertTripHistory", map);
            System.out.println("updateTripMaster###==" + updateTripMaster);

            updateTripMaster = (Integer) getSqlMapClientTemplate().update("trip.updateChangeVehicleTripMaster", map);
            System.out.println("updateTripMaster###==" + updateTripMaster);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("status Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "status", sqlException);
        }
        return status;

    }

    public int saveTripExtraFuelDetailes(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        int expenseId = 0;
        int county = 0;
        int countx = 0;
        int insertStatus = 0;
        map.put("createdBy", tripTO.getUserId());
        map.put("tripId", tripTO.getTripSheetId());
        map.put("vehicleId", tripTO.getVehicleId());
        map.put("vendorId", tripTO.getVendorId());
        map.put("hireVehilceNo", "");
        map.put("returnTripId", "0");
        map.put("tripSheetId", tripTO.getTripSheetId());
        String[] fuelMode = tripTO.getFuelMode();
        String[] bunkName = tripTO.getBunkNames();
        String[] slipNo = tripTO.getSlipNo();
        String[] fuelDate = tripTO.getFuelDates();
        String[] fuelLtrs = tripTO.getFuelLtrs();
        String[] fuelAmount = tripTO.getFuelAmounts();
        String[] fuelFilledName = tripTO.getFuelFilledName();
        String[] fuelRemarks = tripTO.getExtraFuelRemarks();
        String[] fuelPriceRemarks = tripTO.getFuelPriceRemarks();
        String[] fuelPriceRemarksEn = tripTO.getFuelPriceRemarksEn();
        String[] tripAdvanceId = tripTO.getTripAdvanceIds();

        String[] tripFuelId = tripTO.getUniqueIdE();
        String[] bunkIdE = tripTO.getBunkIdE();
        String[] slipNoE = tripTO.getSlipNoE();
        String[] fuelDateE = tripTO.getFuelDateE();
        String[] fuelLtrsE = tripTO.getFuelLtrsE();
        String[] fuelAmountE = tripTO.getFuelAmountE();
        String[] fuelRemarksE = tripTO.getFuelRemarksE();
        String[] fuelPriceRemarksE = tripTO.getFuelPriceRemarksE();
        Double fuelTripAmount = 0.00;
        Double fuelTripCashAmount = 0.00;
        try {
            //            System.out.println("fuelMode.length--" + fuelMode.length);
            if (fuelMode != null) {
                for (int i = 0; i < fuelMode.length; i++) {
                    map.put("fuelMode", fuelMode[i]);
                    System.out.println("fuelMode[i]---" + fuelMode[i]);
//                        map.put("bunkName", bunkName[i]);
                    System.out.println("bunkName[i]---" + bunkName[i]);
                    map.put("slipNo", slipNo[i]);
                    map.put("fuelDate", fuelDate[i]);
                    map.put("fuelLtrs", fuelLtrs[i]);
                    map.put("fuelAmount", fuelAmount[i]);
                    System.out.println("fuelAmount[i]--------"+fuelAmount[i]);
                    map.put("fuelFilledName", fuelFilledName[i]);
                    map.put("fuelRemarks", fuelRemarks[i]);
//                        if("2".equals(fuelMode[i])){
//                            map.put("fuelPriceRemarks", fuelPriceRemarksEn[i]);
//                        }else{
                    map.put("fuelPriceRemarks", fuelPriceRemarks[i]);
//                        }
                    if ("2".equals(fuelMode[i]) && "0".equals(bunkName[i])) {
                        System.out.println("Bunk20");
                        map.put("bunkName", "20"); // IOCL Fleet
                    } else {
                        map.put("bunkName", bunkName[i]);
                    }
                    map.put("bunkPlace", "");
                    System.out.println("the saveBillHeader" + map);
                    status = (Integer) getSqlMapClientTemplate().update("operation.insertTripFuel", map);
                    System.out.println("status--" + status);
                }

                System.out.println("fuelTripCashAmount---" + fuelTripCashAmount);
            }

            //            System.out.println("tripFuelId.length----" + tripTO.getUniqueIdE().length);
            if (tripTO.getUniqueIdE() != null) {
                for (int j = 0; j < tripTO.getUniqueIdE().length; j++) {
                    String[] tmp = bunkIdE[j].split("~");
                    map.put("bunkIdE", tmp[0]);
                    System.out.println("bunkIdE[j]---" + bunkIdE[j]);
                    map.put("tripFuelId", tripFuelId[j]);
                    map.put("slipNoE", slipNoE[j]);
                    map.put("fuelDateE", fuelDateE[j]);
                    map.put("fuelLtrsE", fuelLtrsE[j]);
                    map.put("fuelAmountE", fuelAmountE[j]);
                    map.put("fuelAmount", fuelAmountE[j]);
                    //                  map.put("fuelFilledName", fuelFilledName[i]);
                    map.put("fuelRemarksE", fuelRemarksE[j]);
                    map.put("fuelPriceRemarksE", fuelPriceRemarksE[j]);
                    System.out.println("the updateTripFuelDetails" + map);
                    status = (Integer) getSqlMapClientTemplate().update("trip.updateTripFuelDetails", map);
                    System.out.println("updateTripFuelDetails--" + status);
                }
            }
            int updateStatus = 0;
            String temp[];
            ArrayList getFuelExpense = new ArrayList();
            getFuelExpense = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getFuelEposExpense", map);
            Iterator itr = getFuelExpense.iterator();
            TripTO trpto = new TripTO();
            while (itr.hasNext()) {
                trpto = (TripTO) itr.next();
                temp = trpto.getFuelExpense().split("~");
                System.out.println("trpto.getFuelExpense()------------" + trpto.getFuelExpense());
                map.put("tripId", tripTO.getTripSheetId());

                if ("21".equals(temp[1])) {
                    fuelTripCashAmount = Double.parseDouble(temp[0]);
                    map.put("expenseId", 1047); // fuel cash expense
                    map.put("expense", fuelTripCashAmount);

                    System.out.println("expense--if--" + fuelTripAmount);
                    updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateFuelCashExpense", map);
                    System.out.println("IF 21 FUEL CASH EXP--" + updateStatus);
                    if (updateStatus == 0) {
                        map.put("fuelCashAmount", fuelTripCashAmount);
                        map.put("remarks", "FUEL DRIVER CASH");
                        System.out.println("fuelTripCashAmount----" + fuelTripCashAmount);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertFuelCashExpense", map);
                        System.out.println("IF 21 -insert--" + status);
                    }
                } else {
                    System.out.println("ELSE----");
                    fuelTripAmount = Double.parseDouble(temp[0]);
                    map.put("expenseId", 1010);  // fuel expense
                    map.put("expense", fuelTripAmount);
                    System.out.println("expense--else--" + fuelTripAmount);
                    updateStatus = (Integer) getSqlMapClientTemplate().update("trip.updateFuelCashExpense", map);
                    System.out.println("ELSE 21  FUEL EXP--" + updateStatus);
                    if (updateStatus == 0) {
                        map.put("fuelCashAmount", fuelTripAmount);
                        map.put("remarks", "FUEL Expense");
                        System.out.println("fuelTripAmount----" + fuelTripAmount);
                        insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertFuelCashExpense", map);
                        System.out.println("ELSE 21 insert--" + status);
                    }
                }
            }

            System.out.println("saveBillHeader size=" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("setAdvanceAdvice Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "setAdvanceAdvice List", sqlException);
        }

        return status;
    }

    public int saveTripHireAdvanceDetails(TripTO tripTO) {
        Map map = new HashMap();
        int status = 0;
        try {

            map.put("tripId", tripTO.getTripSheetId());
            map.put("vehicleId", tripTO.getVehicleId());
            map.put("advanceAmount", tripTO.getAdvanceAmount());
            map.put("remarks", tripTO.getRemarks());
            map.put("advanceDate", tripTO.getAccountingDate());
            map.put("statusId", tripTO.getStatusId());
            map.put("type", tripTO.getType());
            System.out.println("Route map value is:" + map);
            status = (Integer) getSqlMapClientTemplate().update("trip.saveTripHireAdvanceDetails", map);
            System.out.println("status1---" + status);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveTripHireAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveTripHireAdvanceDetails List", sqlException);
        }

        return status;
    }

    public ArrayList tripCashAdvanceDetails(TripTO tripTo) {
        Map map = new HashMap();
        ArrayList tripCashAdvanceDetails = new ArrayList();
        try {
            map.put("tripSheetId", tripTo.getTripSheetId());
            map.put("vehicleId", tripTo.getVehicleId());
            System.out.println(" Trip  diesel map is::" + map);
            tripCashAdvanceDetails = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getCashAdvanceDetails", map);
            System.out.println("tripCashAdvanceDetails.size() = " + tripCashAdvanceDetails.size());

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("tripCashAdvanceDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "tripCashAdvanceDetails List", sqlException);
        }
        return tripCashAdvanceDetails;
    }
    
     public String getContInsAmount(TripTO tripTO) {
        ArrayList resultList = new ArrayList();
        Map map = new HashMap();
        String contInsAmount = "";
        try {
            map.put("tripId", tripTO.getTripId());
            if("".equals(tripTO.getVendorId()) || tripTO.getVendorId()==null){
               map.put("vendorId", "0");
            }else{
               map.put("vendorId", tripTO.getVendorId());
            }
            System.out.println("map = " + map);
            contInsAmount = (String) getSqlMapClientTemplate().queryForObject("trip.getContInsAmount", map);
            System.out.println("contInsAmount=" + contInsAmount);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignmentOrderRevenue Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getConsignmentOrderRevenue", sqlException);
        }
        return contInsAmount;
    }

     
    public ArrayList getDriverMappedVehicle(TripTO tripTO) {
        Map map = new HashMap();
        map.put("roleId", tripTO.getRoleId());
        map.put("companyId", tripTO.getCompanyId());
        map.put("driverId", tripTO.getDriverId());
        System.out.println("map getDriverMappedVehicle= " + map);
        ArrayList vehicleList = new ArrayList();
        try {
            vehicleList = (ArrayList) getSqlMapClientTemplate().queryForList("trip.getDriverMappedVehicle", map);
            System.out.println("vehicleList size=" + vehicleList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleList List", sqlException);
        }

        return vehicleList;
    } 
}
