<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

            <style type="text/css">
              option.red {font-color: red;}
              option.green {font-color: greenyellow;}
              </style>

<script type="text/javascript">


    function viewTripDetails(tripId) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function setValues() {
        if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
            document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
        }
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.getElementById('startDateFrom').value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.getElementById('startDateTo').value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("endDateFrom")%>' != 'null') {
            document.getElementById('endDateFrom').value = '<%=request.getAttribute("endDateFrom")%>';
        }
        if ('<%=request.getAttribute("endDateTo")%>' != 'null') {
            document.getElementById('endDateTo').value = '<%=request.getAttribute("endDateTo")%>';
        }
        if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
            document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
        }
        if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
            document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
        }
        if ('<%=request.getAttribute("customerId")%>' != 'null') {
            document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
        }
        if ('<%=request.getAttribute("tripStatusId")%>' != 'null') {
            document.getElementById('tripStatusId').value = '<%=request.getAttribute("tripStatusId")%>';
        }
        if ('<%=request.getAttribute("tripStatusIdTo")%>' != 'null') {
            document.getElementById('tripStatusIdTo').value = '<%=request.getAttribute("tripStatusIdTo")%>';
        }
        if ('<%=request.getAttribute("not")%>' != 'null' && '<%=request.getAttribute("not")%>' != '') {
            document.getElementById('not').checked = true;
        }
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }

    function submitPage(val) {
        if (val == 'ExportExcel') {
            document.tripSheet.action = '/throttle/tripStatusReport.do?param=ExportExcel';
            document.tripSheet.submit();
        } else {
            document.tripSheet.action = '/throttle/tripStatusReport.do?param=search';
            document.tripSheet.submit();
        }
    }
</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©</a>
              </span>-->

    <style type="text/css">





        .container {width: 960px; margin: 0 auto; overflow: hidden;}
        .content {width:800px; margin:0 auto; padding-top:50px;}
        .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

        /* STOP ANIMATION */



        /* Second Loadin Circle */

        .circle1 {
            background-color: rgba(0,0,0,0);
            border:5px solid rgba(100,183,229,0.9);
            opacity:.9;
            border-left:5px solid rgba(0,0,0,0);
            /*	border-right:5px solid rgba(0,0,0,0);*/
            border-radius:50px;
            /*box-shadow: 0 0 15px #2187e7; */
            /*	box-shadow: 0 0 15px blue;*/
            width:40px;
            height:40px;
            margin:0 auto;
            position:relative;
            top:-50px;
            -moz-animation:spinoffPulse 1s infinite linear;
            -webkit-animation:spinoffPulse 1s infinite linear;
            -ms-animation:spinoffPulse 1s infinite linear;
            -o-animation:spinoffPulse 1s infinite linear;
        }

        @-moz-keyframes spinoffPulse {
            0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
        }
        @-webkit-keyframes spinoffPulse {
            0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
        }
        @-ms-keyframes spinoffPulse {
            0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
        }
        @-o-keyframes spinoffPulse {
            0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
        }



    </style>
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.TripStatusReport" text="TripStatusReport"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="head.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.TripStatusReport" text="TripStatusReport"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setValues();
                        sorter.size(5);">
                    <form name="tripSheet" method="post">

                        <table class="table table-bordered" id="report" >
                            <!--            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                                                </h2></td>
                                            <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                                        </tr>-->
                            <!--            <tr height="30" id="index" >
                                                        <td colspan="4"  style="background-color:#5BC0DE;">
                                               
                                                        <b><spring:message code="operations.reports.label.ViewTripSheetReport" text="default text"/></b>
                                                        </td></tr>-->
                            <div id="first">
                                
                                <td style="border-color:#5BC0DE;padding:16px;">
                                    <table  class="table table-info mb30 table-hover" style="width:500px" align="left" >
                                        
                                        <tr>
                                            <td width="80" ><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                            <td  width="80" ><input type="textbox"  style="width:260px;height:40px;"  value="" class="datepicker" name="startDateFrom" id="startDateFrom" ></td>
                                            <td width="80" ><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                            <td  width="80" ><input type="textbox"  style="width:260px;height:40px;"  value="" class="datepicker" name="startDateTo" id="startDateTo" ></td>
                                        </tr>
                                          <tr>
                                              <td>Customer</td>
                                                <td>
                                                    <select name="customerId" id="customerId" class="form-control"  style="width:260px;height:40px;">
                                                        <c:if test="${customerList != null}">
                                                            <option value="" selected>--Select--</option>
                                                            <c:forEach items="${customerList}" var="customerList">
                                                                <option value='<c:out value="${customerList.customerId}"/>'><c:out value="${customerList.customerName}"/></option>
                                                            </c:forEach>
                                                        </c:if>
                                                    </select>
                                                </td>
                                                <script>
//                                                    alert('<c:out value="${customerId}"/>');
                                                      document.getElementById('customerId').value = '<c:out value="${customerId}"/>';
                                                </script>
<!--                                            <td width="80" ><spring:message code="operations.reports.label.FleetCenter" text="default text"/></td>
                                            <td width="80"> <select name="fleetCenterId" style="width:260px;height:40px;" id="fleetCenterId" class="form-control" style="height:20px; width:122px;"" >
                                                    <c:if test="${companyList != null}">
                                                        <option value="" selected>--<spring:message code="operations.reports.label.Select" text="default text"/>--</option>
                                                        <c:forEach items="${companyList}" var="companyList">
                                                            <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                                        </c:forEach>
                                                    </c:if>
                                                </select>
                                                <script>
                                                    document.getElementById("fleetCenterId").value = '1461';
                                                </script>
                                            </td>-->
                                            <!--<td width="80" ><spring:message code="operations.reports.label.Expense Type" text="Expense Type"/></td>-->
                                            <!--<td width="80"> <select name="expenseType" style="width:260px;height:40px;" id="expenseType" class="form-control" style="height:20px; width:122px;"" >-->
<!--                                                        <option value="0" selected>All</option>
                                                        <option value="1">Normal expense</option>
                                                        <option value="1010">Fuel</option>
                                                        <option value="1011">Fastag</option>
                                                        <option value="1036">Fuel Recovery</option>
                                                </select>
                                                <script>
                                                    document.getElementById("expenseType").value = '<c:out value="${expenseType}"/>';
                                                </script>
                                            </td>-->
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <input type="button"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" class="btn btn-success" name="Search" onclick="submitPage('Search');" >
                                                &nbsp;&nbsp;<input type="button"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" class="btn btn-success" name="ExportExcel" onclick="submitPage('ExportExcel');" >
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                </tr>
                        </table>
                        <br>

                        <%--<c:if test = "${tripDetails == null}" >--%>
                        <!--<center>-->
                            <!--<font color="blue"><spring:message code="operations.reports.label.PleaseWaitYourRequestisProcessing" text="default text"/></font>-->
                        <!--                                <div class="container">
                                                            <div class="content">
                                                                <div class="circle"></div>
                                                                <div class="circle1"></div>-->
                        <!--                                    </div>
                                                        </div>
                                                    </center>-->
                        <%--</c:if>--%>
                        <c:if test="${tripStatusReportSize != '0'}">
                            <table class="table table-info mb30 table-bordered" id="table" >
                                <thead>
                                    <tr ><th colspan="18"></th>
                                <th colspan="10" style="text-align:center;border-bottom-width: 2px;"><font size="4">Export</font></th>
                                <th colspan="6" style="text-align:center;border-bottom-width: 2px;"><font size="4">Import</font></th></tr>
                                    <tr >
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.axleType" text="axleType"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.mofforLoc" text="mofforLoc"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.moveDate" text="moveDate"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.stuffedDate" text="stuffedDate"/></th>
                                        <th style="border-left:1px solid white;width:100px;"><spring:message code="operations.reports.label.movementType" text="movementType"/></th>
                                        <th style="border-left:1px solid white;width:100px;"><spring:message code="operations.reports.label.consignmentOrderNo" text="consignmentOrderNo"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.customerName" text="customerName"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.tripCode" text="tripCode"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.containerNo" text="containerNo"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.containerSize" text="containerSize"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.wt" text="wt"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.routeinfo" text="routeinfo"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.transporterName" text="transporterName"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.liner" text="liner"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.vehicleNo" text="vehicleNo"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.tripStart" text="tripStart"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.tripEnd" text="tripEnd"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.emptyIn" text="emptyIn"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.emptyOut" text="emptyOut"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.factoryIn" text="factoryIn"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.factoryOut" text="factoryOut"/></th>
<%--                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.factoryOutwithClearance" text="factoryOutwithClearance"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.factoryOutwithoutClearanceSB" text="factoryOutwithoutClearanceSB"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.factoryOutwithRFIDSealBRCheckList" text="factoryOutwithRFIDSealBRCheckList"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.factoryOutwithDummySeal" text="factoryOutwithDummySeal"/></th>--%>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.waitingForSB" text="waitingForSB"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.waitingForBRandCheckList" text="waitingForBRandCheckList"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.clearanceIn" text="clearanceIn"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.clearanceOut" text="clearanceOut"/></th>
                                        <%--<th style="border-left:1px solid white"><spring:message code="operations.reports.label.clearanceatCfsCustomsSeal" text="clearanceatCfsCustomsSeal"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.clearanceatICDCustomsSeal" text="clearanceatICDCustomsSeal"/></th>--%>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.portIn" text="portIn"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.portOut" text="portOut"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.cFSPortIn" text="cFSPortIn"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.cFSPortOut" text="cFSPortOut"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.impFactoryIn" text="impFactoryIn"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.impFactoryOut" text="impFactoryOut"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.impEmptyIn" text="impEmptyIn"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.impEmptyOut" text="impEmptyOut"/></th>
                                        <!--                        <th><h3>Trip Created By</h3></th>
                                                                <th><h3>Trip Ended By</h3></th>
                                                                <th><h3>Trip Closed By</h3></th>
                                                                <th><h3>Trip Settled By</h3></th>-->

                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${tripStatusReport}" var="tripDetails">
                                        <%
                                                    String className = "text1";
                                                    if ((index % 2) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>
                                        <tr height="30">
                                            <td class="<%=className%>" width="40" align="left"><%=index%></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.axleType}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.mofforLoc}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.moveDate}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.stuffedDate}"/></td>
                                            <td class="<%=className%>" align="left" style="width:100px;"><c:out value="${tripDetails.movementType}"/></td>
                                            <td class="<%=className%>" align="left" style="width:100px;"><c:out value="${tripDetails.consignmentOrderNo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.customerName}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.tripCode}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.containerNo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.containerSize}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.wt}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.routeinfo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.transporterName}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.liner}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.vehicleNo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.tripStart}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.tripEnd}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.emptyIn}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.emptyOut}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.factoryIn}"/></td>
                                            <td class="<%=className%>" width="40" align="left">
                                                <c:if test="${tripDetails.factoryOut !=null }"> 
                                                    <c:out value="${tripDetails.factoryOut}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.factoryOutwithClearance !=null }"> 
                                                    <c:out value="${tripDetails.factoryOutwithClearance}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.factoryOutwithoutClearanceSB !=null }"> 
                                                    <c:out value="${tripDetails.factoryOutwithoutClearanceSB}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.factoryOutwithRFIDSealBRCheckList !=null }"> 
                                                    <c:out value="${tripDetails.factoryOutwithRFIDSealBRCheckList}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.factoryOutwithDummySeal !=null }"> 
                                                    <c:out value="${tripDetails.factoryOutwithDummySeal}"/>
                                                </c:if>
                                            </td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.waitingForSB}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.waitingForBRandCheckList}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.clearanceIn}"/></td>
                                            <td class="<%=className%>" width="40" align="left">
                                                <c:if test="${tripDetails.clearanceOut !=null }"> 
                                                    <c:out value="${tripDetails.clearanceOut}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.clearanceatCfsCustomsSeal !=null }"> 
                                                    <c:out value="${tripDetails.clearanceatCfsCustomsSeal}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.clearanceatICDCustomsSeal !=null }"> 
                                                    <c:out value="${tripDetails.clearanceatICDCustomsSeal}"/>
                                                </c:if>
                                            </td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.portIn}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.portOut}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.cFSPortIn}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.cFSPortOut}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.impFactoryIn}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.impFactoryOut}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.impEmptyIn}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.impEmptyOut}"/></td>
                                        </tr>

                                        <%index++;%>
                                    </c:forEach>
                                </tbody>
                            </table>

                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" >5</option>
                                        <option value="10">10</option>
                                        <option value="20" selected="selected">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />


                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 0);
                            </script>
                        </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

