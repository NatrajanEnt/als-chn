
<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
          
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


        <script type="text/javascript">
            function setInstValues() {
                var paymentMode = document.getElementById("paymentMode").value;
                var instName = "";
                if (paymentMode == 1) {
                    instName = "Cheque";
                } else if (paymentMode == 2) {
                    instName = "DD";
                } else if (paymentMode == 3) {
                    instName = "RTGS";
                }
                document.getElementById("instTag1").innerHTML = instName;
                document.getElementById("instTag2").innerHTML = instName;
                document.getElementById("instTag3").innerHTML = instName;
            }
            function setInvoiceDetails() {
                var invoiceName = document.getElementById('invoiceIdTemp').options[document.getElementById('invoiceIdTemp').selectedIndex].text;

                var invoiceNames = document.getElementsByName('invoiceNames');

                var errStatus = false;
                for (var i = 0; i < invoiceNames.length; i++) {
                    if (invoiceNames[i].value == invoiceName) {
                        alert("adjustment for this invoice no is already done. please select different invoice no");
                        errStatus = true;
                    }
                }
                if (!errStatus) {
                    var invoiceId = document.getElementById("invoiceIdTemp").value;
                    var temp = invoiceId.split("~");
                    document.getElementById("invoiceId").value = temp[0];
                    document.getElementById("grandTotal").value = temp[1];
                    document.getElementById("payAmount").value = 0;
                    document.getElementById("pendingAmount").value = temp[3];
                    document.getElementById("discountAmount").value = temp[4];
                }
            }
            function getInvoiceDetails() {
                var invoiceId = document.getElementById("invoiceId").value;
                if (invoiceId != "") {
                    $('#invoiceId').autocomplete({
                        source: function(request, response) {
                            $.ajax({
                                url: "/throttle/getInvoiceDetails.do",
                                dataType: "json",
                                data: {
                                    invoiceId: request.term
                                },
                                success: function(data, textStatus, jqXHR) {
                                    //alert("dgdfg");
                                    var items = data;
                                    response(items);
                                },
                                error: function(data, type) {
                                    console.log(type);
                                }
                            });
                        },
                        minLength: 1,
                        select: function(event, ui) {
                            var value = ui.item.Name;
                            var tmp = value.split('-');
                            $('#grandTotal').val(tmp[0]);
                            $('#pendingAmount').val(tmp[1]);
                            return false;
                        }
                    }).data("autocomplete")._renderItem = function(ul, item) {
                        var itemVal = item.Name;
                        var temp = itemVal.split('-');
                        itemVal = '<font color="green">' + temp[1] + '</font>';
                        return $("<li></li>")
                                .data("item.autocomplete", item)
                                .append("<a>" + itemVal + "</a>")
                                .appendTo(ul);
                    };
                }
            }


        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });
        </script>

    <script language="javascript">

        $(document).ready(function() {
            $("#chequeId").hide();
            $("#DDId").hide();
            $("#RTGSId").hide();
        });

        function submitPage1() {
            var selectedRowCount = $("#selectedRowCount").val();
            if($('#paymentMode').val() == 0){
                alert("Please select payment mode");
                $('#paymentMode').focus();
            }else if($('#bankBranchId').val() == ''){
                alert("Please select received bank");
                $('#bankBranchId').focus();
            }else if($('#bankId').val() == 0){
                alert("Please enter bank name");
                $('#bankId').focus();
            }else if($('#referenceNo').val() == ''){
                alert("Please enter reference no");
                $('#referenceNo').focus();
            }else if($('#receiptDate').val() == ''){
                alert("Please select receipt date");
                $('#receiptDate').focus();
            }else if($('#receiptAmount').val() == ''){
                alert("Please enter receipt amount");
                $('#receiptAmount').focus();
            }else if($('#invoiceIdTemp').val() == 0 && selectedRowCount <= 1){
                alert("Please select invoice");
                $('#invoiceIdTemp').focus();
            }else if(($('#payAmount').val() == '' || $('#payAmount').val() == 0 )  && (selectedRowCount <= 1)){
                alert("Please enter pay amount");
                $('#payAmount').focus();
            }else{
            document.invoice.action = '/throttle/insertInvoiceDetails.do';
            document.invoice.submit();
                
            }
        }
        
        
         function submitPage()
        {
            var invoiceReceiptformCode = document.getElementById("invoiceReceiptformCode").value;
            var paymentMode = document.getElementById("paymentMode").value;
            var referenceNo = document.getElementById("referenceNo").value;
//                alert(vendorNameId);
            var receiptDate = document.getElementById("receiptDate").value;
            var receiptAmount = document.getElementById("receiptAmount").value;
            var bankBranchId = document.getElementById("bankBranchId").value;
            var bankId = document.getElementById("bankId").value;
            var customerLedgerId = document.getElementById("customerLedgerId").value;
            var customerId = document.getElementById("customerId").value;
            var invoiceCode = [];
            var invoiceId = [];
            var payAmount = [];
            var narration = [];
            var invoiceCodes = document.getElementsByName("invoiceCode");
            var invoiceIds = document.getElementsByName("invoiceId");
            var payAmounts = document.getElementsByName("payAmount");
            var narrations = document.getElementsByName("narration");
            for (var i = 0; i < invoiceCodes.length; i++) {
                invoiceCode.push(invoiceCodes[i].value);
                invoiceId.push(invoiceIds[i].value);
                payAmount.push(payAmounts[i].value);
                narration.push(narrations[i].value);
            }
            var insertStatus = 0;
            var url = "";
            var inpId = 0;
            url = "./insertInvoiceDetails.do";
            $.ajax({
                url: url,
                data: {paymentMode: paymentMode, referenceNo: referenceNo, receiptDate: receiptDate, receiptAmount: receiptAmount,
                    bankBranchId: bankBranchId, bankId: bankId, customerLedgerId: customerLedgerId,customerId:customerId,invoiceCodeVal: invoiceCode,
                    invoiceIdVal: invoiceId, payAmountVal: payAmount, narrationVal: narration

                },
                type: "GET",
                success: function (response) {
                    inpId = response.toString().trim();
                    if (inpId == 0) {
                        insertStatus = 0;
                        var str = "Payment Failed";
                        $("#status1").text(str).css("color", "red");
                    } else {
                        insertStatus = inpId;
                        var str = "Paid Successfully" + " & Invoice Payment No is " + invoiceReceiptformCode + inpId;
                        $("#status1").text(str).css("color", "green");
                        $("#saveButton").hide();

                    }
                },
                error: function (xhr, status, error) {
                }
            });
            return insertStatus;
        }

    </script>
    <script>
        function showPayment() {
            var mode = document.getElementById("paymentMode").value;
            if (mode == "1") {
                $("#chequeId").show();
                $("#DDId").hide();
                $("#RTGSId").hide();
            } else if (mode == "2") {
                $("#chequeId").hide();
                $("#DDId").show();
                $("#RTGSId").hide();
            } else if (mode == "3") {
                $("#chequeId").hide();
                $("#DDId").hide();
                $("#RTGSId").show();
            } else {
                $("#chequeId").hide();
                $("#DDId").hide();
                $("#RTGSId").hide();
            }
        }
        
          window.onload = function()
        {
            var currentDate = new Date();
            var day = currentDate.getDate();
            var month = currentDate.getMonth() + 1;
            var year = currentDate.getFullYear();
            var myDate = day + "-" + month + "-" + year;
            document.getElementById("receiptDate").value = myDate;
        }
        function getBankBranch(bankId) {
                                    var temp = "";
                                        $.ajax({
                                            url: '/throttle/getBankBranchDetails.do',
                                            data: {bankId: bankId},
                                            dataType: 'json',
                                             success: function (temp) {
//                                         alert(temp);
                                        if (temp != '') {
                                            $('#bankBranchId').empty();
                                            $('#bankBranchId').append(
                                                    $('<option style="width:150px"></option>').val(0+"~"+0).html('---Select----')
                                                    )
                                            $.each(temp, function (i, data) {
                                                $('#bankBranchId').append(
                                                        $('<option value="'+data.branchId+"~"+data.bankLedgerId+'" style="width:150px"></option>').val(data.branchId+"~"+data.bankLedgerId).html(data.branchName)
                                                        )
                                            });
                                        } else {
                                            $('#bankBranchId').empty();
                                        }
                                    }
                                        });
                                    }
                                    var rowCount = 1;
        var sno = 0;
        var style = "text2";
        var align = "center";
        function addRow()
        {
            if (rowCount % 2 == 0) {
                style = "text2";
            } else {
                style = "text1";
            }


            var tab = document.getElementById("creditAddRow");
            var newrow = tab.insertRow(rowCount);

            var cell = newrow.insertCell(0);
            var cell1 = "<td class='text1' height='25' ><input type='hidden' name='invoiceId' id='invoiceId" + sno + "' value=''/><select class='form-control' id='invoiceIdTemp" + sno + "'  style='width:190px;height:40px;'  name='invoiceIdTemp' onchange='setInvoiceDetails(" + sno + ");'><option selected value=0>---<spring:message code="finance.label.Select"  text="default text"/>---</option><c:if test = "${invoiceList != null}" ><c:forEach items="${invoiceList}" var="invoiceList"><option  value='<c:out value="${invoiceList.invoiceId}"/>~<c:out value="${invoiceList.grandTotal}"/>~<c:out value="${invoiceList.payAmount}"/>~<c:out value="${invoiceList.pendingAmount}"/>~<c:out value="${invoiceList.invoiceCode}"/>'><c:out value="${invoiceList.invoiceCode}"/></option></c:forEach ></c:if></select></td>";

            cell.setAttribute("className", style);
            cell.setAttribute("align", align);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(1);
            var cell2 = "<td class='text1' height='30'><input type='hidden' name='invoiceCode' id='invoiceCode" + sno + "' value=''/><input type='textbox'  name='grandTotal' id='grandTotal" + sno + "' value=''  onKeyPress='return onKeyPressBlockCharacters(event);' style='width:190px;height:40px;'/></td>";
            cell.setAttribute("className", style);
            cell.setAttribute("align", align);
            cell.innerHTML = cell2;

            cell = newrow.insertCell(2);
            var cell3 = "<td class='text1' height='30'><input type='textbox'  name='pendingAmount' id='pendingAmount" + sno + "' value=''  onKeyPress='return onKeyPressBlockCharacters(event);' style='width:190px;height:40px;'/></td>";
            cell.setAttribute("className", style);
            cell.setAttribute("align", align);
            cell.innerHTML = cell3;

            cell = newrow.insertCell(3);
            var cell4 = "<td height='30' class='tex1'><input type='textbox'  name='payAmount' id='payAmount" + sno + "' value=''  onKeyPress='return onKeyPressBlockCharacters(event);' style='width:190px;height:40px;' onchange='calTotalAmount();'/></td>";
            cell.setAttribute("className", style);
            cell.setAttribute("align", align);
            cell.innerHTML = cell4;

            cell = newrow.insertCell(4);
            var cell5 = "<td class='text1' height='30'><textarea name='narration'   id='narration" + sno + "'  style='width:190px;height:40px;'/></textarea></td>";
            cell.setAttribute("className", style);
            cell.setAttribute("align", align);
            cell.innerHTML = cell5;

            cell = newrow.insertCell(5);
            var cell6 = "<td alain='left' ><input type='checkbox' id='selectedIndex" + sno + "' name='selectedIndex' value='" + sno + "' style='width:15px;'/></td>";
            cell.setAttribute("className", style);
            cell.setAttribute("align", align);
            cell.innerHTML = cell6;
            var temp = sno - 1;

            rowCount++;
            sno++;

        }
         function setInvoiceDetails(sno) {
//                 var invoiceNames = document.getElementsByName('invoiceNames');
//                 var invoiceName = document.getElementById('invoiceIdTemp'+sno).options[document.getElementById('invoiceIdTemp'+sno).selectedIndex].text;
            var invoiceName = document.getElementById('invoiceIdTemp' + sno).value;
            // alert(invoiceName);
            var invoiceNames = document.getElementsByName('invoiceIdTemp');
            var count = 0;
            var errStatus = false;
            for (var i = 0; i < invoiceNames.length; i++) {
                if (invoiceNames[i].value == invoiceName) {
                    count++;

                }
            }
            if (count > 1) {
                alert("This InvoiceCode is already done.please select different InvoiceCode!");
                document.getElementById('invoiceIdTemp' + sno).value = 0;
                errStatus = true;
            }
////                
            if (!errStatus) {
                var invoiceId = document.getElementById("invoiceIdTemp" + sno).value;
                // alert(invoiceId);
                var temp = invoiceId.split("~");
                if (temp[0] != 0) {
                    document.getElementById("invoiceId" + sno).value = temp[0];
                    document.getElementById("grandTotal" + sno).value = temp[1];
                    document.getElementById("payAmount" + sno).value = temp[2];
                    document.getElementById("pendingAmount" + sno).value = temp[3];
                    document.getElementById("invoiceCode" + sno).value = temp[4];
                } else {
                    document.getElementById("invoiceId" + sno).value = "";
                    document.getElementById("grandTotal" + sno).value = "";
                    document.getElementById("payAmount" + sno).value = "0";
                    document.getElementById("pendingAmount" + sno).value = "";
                    document.getElementById("invoiceCode" + sno).value = "";
                }
            }
        }
        function calTotalAmount() {
//            var totalAmountSpan
            var amounts = document.getElementsByName("payAmount");
            var totalAmount = 0;
            var amount = 0;
            var receiptAmount = 0;
            var grandTotal = 0;
            for (var i = 0; i < amounts.length; i++) {
                if (amounts[i].value == '') {
                    amount = 0;
                } else {
                    amount = parseFloat(amounts[i].value);
                }
                totalAmount += parseFloat(amount);
            }
            receiptAmount = document.getElementById("receiptAmount").value;
            grandTotal = parseFloat(receiptAmount).toFixed(2);
            if (grandTotal < totalAmount) {
                alert("The adjusted amount is greater than pay amount. please cross check");
            } else {
                document.getElementById("totalAmt").value = totalAmount.toFixed(2);
            }
        }
        function DeleteRow() {
            document.getElementById('AddRow').style.display = 'block';
            try {
                var table = document.getElementById("creditAddRow");
                rowCount = table.rows.length;
                // alert(rowCount);
                for (var i = 1; i < rowCount; i++) {
                    var row = table.rows[i];
                    var checkbox = row.cells[5].childNodes[0];
                    if (null != checkbox && true == checkbox.checked) {
                        if (rowCount <= 1) {
                            alert("Cannot delete all the rows");
                            break;
                        }
                        table.deleteRow(i);
                        rowCount--;
                        i--;
                        sno--;
                        // snumber--;
                    }
                }
                calTotalAmount();
            } catch (e) {
                alert(e);
            }
        }
    </script>
    
    
    <style>
    #index td {
   color:white;
}
</style>
             <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.RECEIPTDETAILS"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.RECEIPTDETAILS"  text="default text"/></li>
        </ol>
    </div>
</div>
    <div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    
    <body onload="addRow();">
                <form name="invoice" class="form-horizontal form-bordered">
            <%
       Date today = new Date();
       SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
       String todayDate = sdf.format(today);
            %>
            <br>
            <font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                    <div align="center" id="status1">&nbsp;&nbsp;
                    </div>
                    </font><br>
            <table class="table table-info mb30 table-hover">
                <thead>
                <tr id="index" style="background-color:#5BC0DE;">
                    <th colspan="6" >&nbsp;&nbsp;<spring:message code="finance.label.RECEIPTDETAILS"  text="default text"/></th>
                </tr>
                <tr  style="background-color:#5BC0DE;">
                    <th colspan="2" align="right" height="30">
                        &nbsp;&nbsp;&nbsp;<spring:message code="finance.label.CustomerName"  text="default text"/>:  &nbsp;&nbsp;&nbsp;<c:out value="${customerName}"/>
                        <input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" />
                <input type="hidden" name="customerLedgerId" id="customerLedgerId" value="<c:out value="${customerLedgerId}"/>" />
                <input type="hidden" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" />
                    <input type="hidden" name="invoiceReceiptformCode" id="invoiceReceiptformCode" value="<%=ThrottleConstants.invoiceReceiptformCode%>" />
                    </th>
                    <th colspan="4"  height="30">
                        &nbsp;&nbsp;&nbsp;Date: &nbsp;&nbsp;&nbsp;<input type="hidden" id="ddDate" name="ddDate" value="<%=todayDate%>"  size="20" class="datepicker" ><%=todayDate%>
                    </th>
                </tr>
                </thead>
                <tr height="40" >
                    <td ><font color=red>*</font><spring:message code="finance.label.PaymentMode"  text="default text"/></td>
                    <td >
                        <select name="paymentMode" id="paymentMode" onChange="setInstValues();" class="form-control" style="width:260px;height:40px;" >
                            <option value="0" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                            <option value="1" ><spring:message code="finance.label.Cheque"  text="default text"/></option>
                            <option value="2" ><spring:message code="finance.label.DD"  text="default text"/></option>
                            <option value="3" ><spring:message code="finance.label.RTGS"  text="default text"/></option>
                        </select>

                    </td>
                    <td ><font color=red>*</font><spring:message code="finance.label.BankName"  text="default text"/></td>
                    <td >
                        <select name="bankId" id="bankId"  onchange="getBankBranch(this.value)" class="form-control" style="width:260px;height:40px;">
                            <c:if test="${primarybankList != null}">
                                <option value="0" selected>--<spring:message code="finance.label.Select"  text="default text"/>--</option>
                                <c:forEach items="${primarybankList}" var="bankVar">
                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>
                                </c:forEach>
                            </c:if>          
                        </select>
                    </td >
                    <td  ><font color=red>*</font><spring:message code="finance.label.BankBranch"  text="default text"/></td>
                    <td   >
                        <select name="bankBranchId" id="bankBranchId" class="form-control" style="width:260px;height:40px;">
                        </select>
                    </td>
                </tr>
                <tr  height="40">
                    <td  ><div id="instTag1"></div> <spring:message code="finance.label.No"  text="default text"/></td>
                    <td  >
                        <input type="textbox"   name="referenceNo" id="referenceNo" value="" class="form-control" style="width:260px;height:40px;"/>
                    </td>
                    <td ><div id="instTag2"></div> <spring:message code="finance.label.Date"  text="default text"/></td>
                    <td  >
                        <input type="textbox" id="receiptDate" name="receiptDate" value=""  size="20" class="datepicker" class="form-control" style="width:260px;height:40px;">
                    </td>
                    <td   ><div id="instTag3"></div> <spring:message code="finance.label.Amount"  text="default text"/></td>
                    <td   >
                        <input type="textbox" id="receiptAmount"   name="receiptAmount" value="0"  onKeyPress='return onKeyPressBlockCharacters(event);' class="form-control" style="width:260px;height:40px;">
                    </td>
                </tr>
            </table>
            <br/>
             <table class="table table-info" id="creditAddRow" width="700">
                       <thead>
                    <th><spring:message code="finance.label.InvoiceCode"  text="default text"/></th>
                    <th><spring:message code="finance.label.TotalAmount"  text="default text"/></th>
                    <th><spring:message code="finance.label.PendingAmount"  text="default text"/></th>
                    <th><spring:message code="finance.label.ReadyToPay"  text="default text"/></th>
                    <th><spring:message code="finance.label.Narration"  text="default text"/></th>
                    <th><spring:message code="finance.label.Select"  text="default text"/></th>
             </thead>
                <tr id="index" style="background-color:#5BC0DE;">
                      <td>&nbsp;</td>
                    <td >&nbsp;</td>
                    <td align="right"><spring:message code="finance.label.Total"  text="default text"/>&nbsp;</td>
                    <td align="center"><input type="textbox" id="totalAmt" name="totalAmt" value="" class="form-control" style="width:190px;"></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <p><br>
            </p>
            <table align="center">
                <tr >
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                    <td align="right">&emsp;&emsp;<input type="button" id="AddRow" value="<spring:message code="finance.label.AddRow"  text="default text"/>" class="btn btn-success" onClick="addRow();" style="width:100px;height:35px;"></td>
                    <td colspan="2" align="left"> &emsp;&emsp;<input type="button" id="DeleteRow1" value="<spring:message code="finance.label.DeleteRow"  text="default text"/>" class="btn btn-success" onClick="DeleteRow();" style="width:100px;height:35px;">
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <br>
            <table align="center" >
                <tr>
                    <td colspan="2" align="center"> <input type="button" id="saveButton" name="saveButton" value="<spring:message code="finance.label.Save"  text="default text"/>" class="btn btn-success" onClick="submitPage();" style="width:100px;height:35px;"></td>
                </tr>
            </table>

</body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>