<%-- 
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                       Date dNow = new Date();
                       SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                       //System.out.println("Current Date: " + ft.format(dNow));
                       String curDate = ft.format(dNow);
                       String expFile = "TripSheet-" + curDate + ".xls";

                       String fileName = "attachment;filename=" + expFile;
                       response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                       response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test="${tripDetails != null}">

                <table class="table table-info mb30 table-bordered" id="table" >
                    <thead >
                        <c:if test = "${pnrUser == 1}" >
                            <tr >
                                <th>Sno</th>
                                <th>PNR No</th>
                                <th>VesselName</th>
                                <th>Vehicle No </th>
                                    <c:if test="${statusId >= 12}">
                                    <th>Container No(s) </th>
                                    </c:if>
                                <th>Trip Code</th>
                                <th>Order Type </th>
                                <th>Transporter</th>
                                <th>InterimDetails </th>
                                <th>Driver Name with Contact </th>
                                <th>Vehicle Type </th>
                                <th >Container Qty</th>
                                <!--                                        <th>LinkToMap</th>
                                                                        <th>Select</th>-->
                            </tr>
                        </c:if>
                        <c:if test = "${pnrUser != 1}" >
                            <tr >
                                <th>Sno</th>
                                <!--                                        <th >POD</th>-->
                                <th>CustRefNo</th>
                                <th>Customer</th>
                                <th>Goods</th>
                                <th>Vehicle No </th>
                                <th>Trip Code</th>
                                <th>Container No(s) </th>
                                <th>LR No(s)</th>
                                <th>pkg/wgt/cbm </th>
                                <th>Trip Planned Date </th>
                                <th>Trip Start Date </th>
                                <th>Trip End date</th>
                                <th>Trip Closure Date</th>
                                <th>Order Type </th>
                                <th>Transporter</th>
                                <th>Route </th>
                                <th>Driver Name </th>
                                <th>Driver Contact Number</th>
                                <th>Vehicle Type </th>                                    
                                <th>Status</th>                                    
                                <!--                                        <th>LinkToMap</th>
                                                                        <th>Select</th>-->
                            </tr>
                        </c:if>
                    </thead>
                    <tbody>

                        <% int index = 1;%>

                        <%
                               String classText = "";
                               int oddEven = index % 2;
                               if (oddEven > 0) {
                                   classText = "text2";
                               } else {
                                   classText = "text1";
                               }
                        %>
                        <c:forEach items="${tripDetails}" var="tripDetails">


                            <tr >
                                <td   ><%=index++%></td>
                                <td  align="right" >
                                    <c:out value="${tripDetails.customerOrderReferenceNo}"/>
                                </td>
                                <td   >
                                    <c:out value="${tripDetails.customerName}"/>
                                </td>
                                <td align="center"  >
                                    <c:out value="${tripDetails.description}"/>
                                </td>
                                <c:if test = "${pnrUser == 1}" >                                            
                                    <td>
                                        <c:out value="${tripDetails.linerId}"/>
                                    </td>
                                </c:if>

                                <c:if test="${tripDetails.vendorId !=1 || vendorType == 1}">
                                    <td class="form-control" >
                                        <c:if test="${tripDetails.activeInd == 'Y'}">
                                            <c:out value="${tripDetails.regNoHire}"/> 
                                        </c:if>
                                        <c:if test="${tripDetails.activeInd == 'N'}">
                                            <font color="red"> <c:out value="${tripDetails.regNoHireOld}"/> </font>
                                        </c:if>
                                    </td>
                                </c:if>
                                <c:if test="${tripDetails.vendorId == 1}">
                                    <td class="form-control" >
                                        <c:if test="${tripDetails.activeInd == 'Y'}">
                                            <c:out value="${tripDetails.vehicleNo}"/>
                                        </c:if>
                                        <c:if test="${tripDetails.activeInd == 'N'}">
                                            <font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font>
                                        </c:if>
                                    </td>
                                </c:if>     
                                <c:if test = "${pnrUser == 1}" >                                            
                                    <c:if test="${statusId >= 12}">
                                        <td   >
                                            <c:if test="${tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                                <c:out value="${tripDetails.containerNo}"/>
                                            </c:if>
                                        </td>
                                    </c:if>
                                </c:if>

                                <td>
                                    <c:if test="${tripDetails.oldVehicleNo != null}">
                                        <c:out value="${tripDetails.tripCode}"/> &nbsp /  &nbsp <c:out value="${tripDetails.tripSheetId}"/>
                                    </c:if>
                                    <c:if test="${tripDetails.oldVehicleNo == null}">
                                        <c:out value="${tripDetails.tripCode}"/> &nbsp /  &nbsp <c:out value="${tripDetails.tripSheetId}"/>
                                    </c:if>
                                </td>
                                <c:if test = "${pnrUser != 1}" >                                            
                                    <td   >
                                        <c:out value="${tripDetails.containerNo}"/>
                                    </td>
                                </c:if>
                                <c:if test = "${pnrUser != 1}" >                                            
                                    <td class="form-control" >
                                        <!--                                                MAA
                                        <c:if test="${tripDetails.movementType == 12 }">
                                            EX 
                                        </c:if>
                                        <c:if test="${tripDetails.movementType == 13 || tripDetails.movementType == 14 }">
                                            IM 
                                        </c:if>
                                        <c:if test="${tripDetails.movementType == 15 }">
                                            IC 
                                        </c:if>/-->
                                        <c:if test="${tripDetails.movementType != '16' }">
                                            <c:out value="${tripDetails.lrNumber}"/>
                                        </c:if>
                                        <c:if test="${tripDetails.movementType == '16' }">
                                            <c:out value="${tripDetails.manualLrNumber}"/>
                                        </c:if>
                                        <!--/ 2017-18&nbsp;-->
                                    </td>
                                </c:if>    

                                <c:if test = "${pnrUser != 1}" > 
                                    <c:if test="${tripDetails.movementType == 1  || tripDetails.movementType == 2 
                                                  || tripDetails.movementType == 17  || tripDetails.movementType == 18 
                                                  || tripDetails.movementType == 19  || tripDetails.movementType == 20  
                                                  || tripDetails.movementType == 21  || tripDetails.movementType == 23  
                                                  || tripDetails.movementType == 24 || tripDetails.movementType == 25 
                                                  || tripDetails.movementType == 26 || tripDetails.movementType == 27 
                                                  || tripDetails.movementType == 28 || tripDetails.movementType == 29 
                                                  || tripDetails.movementType == 30 || tripDetails.movementType == 31
                                                  || tripDetails.movementType == 32 || tripDetails.movementType == 33 
                                                  || tripDetails.movementType == 34 || tripDetails.movementType == 35 || tripDetails.movementType == 36
                                          }">
                                        <td   >
                                            <c:out value="${tripDetails.containerDetail}"/>
                                        </td> 
                                    </c:if>
                                    <c:if test="${tripDetails.movementType == 14 || tripDetails.movementType == 16 }">
                                        <td   >
                                            <c:out value="${tripDetails.consignmentArticles}"/>
                                        </td> 
                                    </c:if>
                                </c:if>

                                <td><c:out value="${tripDetails.tripPlannedDate}"/></td>  
                                <c:if test = "${pnrUser != 1}" >                                            
                                    <td>
                                        <c:if test="${tripDetails.activeInd == 'Y'}">
                                            <c:out value="${tripDetails.tripScheduleDate}"/> 
                                        </c:if>
                                        <c:if test="${tripDetails.activeInd == 'N'}">
                                            <font color="red"><c:out value="${tripDetails.oldStartDate}"/> </font>
                                        </c:if>
                                    </td>
                                </c:if>
                                <td  >
                                <c:if test="${tripDetails.activeInd == 'Y'}">
                                    <c:out value="${tripDetails.tripEndDate}"/> 
                                </c:if>
                                <c:if test="${tripDetails.activeInd == 'N'}">
                                    <font color="red"><c:out value="${tripDetails.oldEndDate}"/> </font>
                                </c:if>
                                </td> 
                                <td><c:out value="${tripDetails.tripCloseDate}"/></td>  
                                <td class="form-control" align="center">

                                    <c:out value="${tripDetails.movementTypeName}"/>
                                </td>    
                                <td ><c:if test="${tripDetails.activeInd == 'Y'}">
                                        <c:out value="${tripDetails.vehicleVendor}"/>
                                    </c:if>
                                    <c:if test="${tripDetails.activeInd == 'N'}">
                                        <font color="red"><c:out value="${tripDetails.oldVendor}"/></font>
                                    </c:if>
                                </td>
                                <td  >
                                    <c:if test = "${pnrUser == 1}" >
                                        <c:out value="${tripDetails.portName}"/>
                                    </c:if>
                                    <c:if test = "${pnrUser != 1}" >
                                        <c:out value="${tripDetails.routeInfo}"/>
                                    </c:if>
                                </td>

                                <td  ><c:out value="${tripDetails.driverName}"/><font ><c:out value="${tripDetails.oldDriverName}"/></font></td>
                                <td ><c:out value="${tripDetails.mobileNo}"/></td>
                                <td  ><c:out value="${tripDetails.vehicleTypeName}"/></td>
                                <c:if test = "${pnrUser == 1}" >
                                    <td  >
                                        <input type="hidden" name="containerQuantity" id="containerQuantity" value="<c:out value="${tripDetails.containerQuantity}" />">
                                        <c:if test = "${tripDetails.containerQuantity == 1}" >
                                            1 * 40 
                                        </c:if> 
                                        <c:if test = "${tripDetails.containerQuantity == 2}" >
                                            1 * 20 
                                        </c:if> 
                                        <c:if test = "${tripDetails.containerQuantity == 3}" >
                                            2 * 20 
                                        </c:if> 
                                    </td>
                                </c:if>

                                <td   align="left"> 
                                    <c:if test="${tripDetails.statusId == 10 }"> 
                                        <c:out value="${tripDetails.subStatusId}" /> 
                                    </c:if>
                                    <c:if test="${tripDetails.statusId != 10}"> 
                                        <c:if test="${tripDetails.statusId == 6}"> 
                                            <font color="red"> Cancelled  </font>
                                        </c:if>
                                        <c:if test="${tripDetails.statusId != 6}"> 
                                            <c:if test="${tripDetails.activeInd == 'Y'}">
                                                <c:out value="${tripDetails.status}" /> 
                                            </c:if>
                                            <c:if test="${tripDetails.activeInd == 'N'}">
                                                <c:if test="${tripDetails.changeVehStatus == 1}">
                                                <font color="red"> OffLoaded  </font>
                                                </c:if>
                                                <c:if test="${tripDetails.changeVehStatus == 0}">
                                                <font color="red"> OffLoaded  </font>
                                                </c:if>
                                            </c:if>
                                        </c:if>
                                    </c:if>

                                </td>   





                            </tr>

                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
