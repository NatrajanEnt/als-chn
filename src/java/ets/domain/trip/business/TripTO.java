/*-------------------------------------------------------------------------
 * __NAME__.java
 * __DATE__
 *
 * Copyright (c) Entitle.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * Entitle ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Entitle.
 -------------------------------------------------------------------------*/
package ets.domain.trip.business;

import java.util.ArrayList;

/**
 * ****************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ----------------------------------------------------------------------------
 * 1.0 __DATE__ Your_Name ,Entitle Created
 *
 *****************************************************************************
 */
public class TripTO {

    /**
     * Creates a new instance of __NAME__
     */
    public TripTO() {
    }

    private String contractRate = null;
    private String haltContractAmt = null;
    private String clrContractAmt = null;
    private String hireExpenses = null;
    private String movementId = null;
    private String containerInsAmount = null;
    private String vendorHaltingCharge = null;
    private String maxFuelPrice = null;
    private String tankCapacity = null;
    private String grosswt = null;
    private String bunkLiters = null;
    private String enrouteLiters = null;
    private String rtoRemarks = null;
    private String haltStatus = null;
    private String vehicleNos = null;
    private String driverLicenseNo = null;
    private String cleanerLicenseNo = null;
    private String vehStatusId = null;
    private String actStatus = null;
    private String customerErpId = null;
    private String closureStatus = null;
    private String offloadStatus = null;
    private String dktFAStatus = null;
    private String tollId = null;
    private String paidOn = null;                
    
    private String EWaybillNo = null;
    private String invoiceNo = null;
    private String invoiceValue = null;
    private String invoiceDate = null;
    private String invoiceWeight = null;
    private String EWaybillExpiryDate = null;
    private String EWaybillExpiryTime = null;
    
    private String activeVehicle = null;
    private String tripIdNew = null;
    private String changeVehStatus = null;
    private String oldStartDate = null;
    private String oldEndDate = null;
    private String oldVendor = null;
    private String vendorContractTypeId = null;
    private String vehicleContractTypeId = null;
    private String haltVehicleId = null;
    private String clrVehicleId = null;
    private String regNoHire = null;
    private String regNoHireOld = null;
    private String pointIds = null;
    private String vehicleCount = "0";
    private String driverChangeDate = "";
    private String oldDriverId = "";
    
    private String contractHireRate = null;
    private String changeVehicleStatus = null;
    private Double netWts = 0.00;
    private String contractTonnage = null;
    private String clearanceCharge = null;
    private String tripHaltDay = null;
    private String tripClrDay = null;
    private String clrStartDate = null;
    private String clrStartHour = null;
    private String clrStartMinute = null;
    private String clrEndDate = null;
    private String clrStartTime = null;
    private String clrEndTime = null;
    private String clrEndHour = null;
    private String clrEndMinute = null;
    private String clrCntrAmt = null;
    private String isExport = null;
    private String rtoCharges = null;
    private String[] mappingIds = null;
    private String custErpId = null;
    private String filePath = null;
    private String mappingId = null;
    private String driverMobNo = null;
    private String cleanerMobNo = null;
    private String tatValues = null;
    private String startEndTime = null;
    private String OTId = null;
    private String netWt = null;
    private String actWt = null;
    private String OTWt = null;
    private String chargePerTon = null;
    private String OTCharges = null;
    private String diversionId = null;
    private String divertCityId = null;
    private String divertCharge = null;
    private String divertRemarks = null;
    private String divertCityCode = null;
    private String divertCityName = null;
    private String driverCode = null;
    private String cleanerCode = null;
    private String haltStartDate = null;
    private String haltStartHour = null;
    private String haltStartMinute = null;
    private String haltStartTime = null;

    private String haltEndDate = null;
    private String haltEndHour = null;
    private String haltEndMinute = null;
    private String haltEndTime = null;

    private String fuelPriceRmrk = null;
    private String[] tripAdvanceIds = null;
    private String[] fuelPriceRemarksEn = null;
    private String[] fuelPriceRemarks = null;
    private String[] fuelPriceRemarksE = null;
    private String[] uniqueIdE = null;
    private String[] bunkIdE = null;
    private String[] slipNoE = null;
    private String[] fuelDateE = null;
    private String[] fuelLtrsE = null;
    private String[] fuelAmountE = null;
    private String[] fuelRemarksE = null;
    private String haltCntrAmt = "";
    private String tallyBillingType = "";
    private String routeDeviation = "";
    private String clearanceType = "";
    private String oldVehicleId = "";
    private String tripCloseDate = "";
    private String expiryDate = "";
    private String expiryHour = "";
    private String expiryMin = "";
    private String eWayBillNo = "";
    private String[] orderRevenues = null;
    private String[] uniquesId = null;
    private String[] sealNos = null;
    private String[] extraFuelRemarks = null;
    private String fuelRecoveryRemaks = "";
    private String approvStatus = "";
    private String tripExpenId = "";
    private String pageType = "";
    private String openKM = "";
    private String closingKM = "";
    private String borneBy = "";
    private String fuelingMode = "";
    private String allocatedFuel = "";
    private String consumedFuel = "";
    private String filledFuel = "";
    private String recoveryFuel = "";
    private String recoveryAmount = "";
    private String fuelSlipNo = "";
    private String fuelRemarks = "";
    private String durationDate = "";
    private String durationTime = "";
    private String haltingDays = "";

    private String billingTypeId = "";
    private String mobilNo = "";
    private String cleaner = "";
    private String dlExpiryDate = "";
    private String consignorId = "";
    private String consigneeId = "";
    private String logo = "";
    private String subStatusId = "";
    private String tripSubStatus = "";
    private String totalForty = "";
    private String emptyLocation = "";
    private String emptyLocName = "";
    private String contractHireId = "";
    private String tonnage = "";
    private String vehTallyName = "";
    private String tallyName = "";
    private String containerStatus = "";
    private String emptyDropTime = "";
    private String tripAdvance = "";
    private String fuelLtr = "";
    private String fuelBunk = "";
    private String hire = "";
    private String balance = "";
    private String mamul = "";
    private String vehicleCategory1 = "";
    private String consigOrderId = "";
    private String jobReferenceNo = "";

    private String containerDetail = "0";
    private String advTrnType = "";
    private String description = "";
    private String consignmentArticles = "0";
    private String perDayCOst = "0";
    private String fuelLtrsCost = "0";
    private String tripExpense = "0";
    private String deliveryAddress = "";
    private String movementTypeId = "";
    private String consignorGST = "";
    private String consigneeGST = "";
    private String billingPartyGST = "";
    private String ewayBillNo = "";
    private String containerPkgs = "";
    private String containerWgts = "";
    private String containerVolume = "";
    private String containerPackages = "";
    private String containerNumber = "";
    private String collectionBranch = "";
    private String singleTwentyStatus = "";
    private String lrNumber = "";
    private String manualLrNumber = "";
    private String printFlag = "";
    private String tripPointId = "";
    private String linerId = "";
    private String twtyTotal = "";
    private String frtyTotal = "";
    private String expenseCategory = "";
    private String commodityCategory = "";
    private String commodityName = "";
    private String gstApplicable = "";
    private String advancerequestamt = "";

    private String[] actualPointId = null;
    private String[] actualPointName = null;
    private String[] actualLatitude = null;
    private String[] actualLongtitude = null;
    private String[] routeCourseId = null;

    private String requeststatus = "";
    private String licenseNo = "";
    private String tobepaidtoday = "";
    private String tripday = "";
    private String batchType = "";
    private String currencyid = "";
    private String[] trailerIds = null, trailerRemarks = null, containerTypeIds = null, containerNos = null, wgtTonnage = null;
    private String[] fuelMode = null, fuelFilledName = null, bunkNames = null, bunkPlace = null, fuelDates = null, fuelAmounts = null, fuelLtrs = null, fuelFilledBy = null, fuelRemark = null, slipNo = null, fuelUniqueId = null;
    private String remainWeight = "", sealNo = "", grStatus = "", uniqueIds = "";
    private String driverNameId = "";
    private String isRepo = "";
    private String advanceGrNo = "";
    private String panNo = "";
    private String gstNo = "";
    private String transporter = "";
    private String tripExpenseIds = "";
    private String[] expenseTypes = null;
    private String[] expenses = null;
    private String[] tripExpensesId = null;

    private String activeInd = "";
    private String createdBy = "";
    private String usedStatus = "";
    private String grId = "";
    private String weightmentExpense = "";
    private String tollTax = "";
    private String loadType = "";
    private String mailDeliveredResponse = "";
    private String consignmentContainerId = "";
    private String movementTypeName = "";
    private String vehicleVendor = "";
    private String grHours = "";
    private String grMins = "";
    private String grNo = "";
    private String detaintion = "";
    private String containerTypeName = "";
    private String interamPoint = "";
    private String challanNo = "";
    private String challanDate = "";
    private String billOfEntry = "";
    private String shipingLineNo = "";
    private String vehicleCategory = "";
    private String movementType = "";
    private String hireVehicleNo = "";
    private String contractTypeId = "";
    private String tripFactoryToIcdHour = "";
    private String tripContainerId = "";
    private String grDate = "";

    private String dieselAmount = "";
    private String paidExpense = "";
    private String linerName = "";
    private String tripDetainHours = "";
    private String fuelDate = "";
    private String grNumber = "";
    private String billingPartyId = "";
    private String billingParty = "";
    private String lastPointId = "";
    private String rtoId = "";
    private String rtoName = "";
    private String containerQty = "";
    private String consignmentOrderDate = "";
    //new
    private String bunkName = "";
    private String state = "";
    private String currRate = "";
    private String activeStatus = "";
    private String bunkId = "";
    private String fuelType = "";
    private String bunkStatus = "";
    private String bunkState = "";
    private String currlocation = "";
    //new
    private String consignmentNoteId = "";
    private String fromCityName = "";
    private String toCityName = "";
    private String repoId = "";
    private String uniqueId = "";
    private String containerTypeId = "";
    private String containerCapacity = "";
    private String hireCharges = "";
    private String wayBillNo = "";
    private String vendorId = "";
    private String ownerShip = "";
    private String vendorTypeId = "";
    private String vendorName = "";
    private String originCity = "";
    private String destinationCity = "";
    private String plannedDeliveryDate = "";
    private String orderGPSTrackingLocation = "";
    private String plannedPickupDate = "";
    private String pickupDate = "";
    private String deliveryDate = "";
    private String remainOrderWeight = "";
    private String containerNo = "";
    private String containerName = "";
    private String containerId = "";
    private String deattchLoc = "";
    private String orderStatus = "";
    private String latitude = "";
    private String longitude = "";
    private String countryId = "";
    private String vehicleTypeId = "";
    private String fuelCostPerKm = "";
    private String tollCostPerKm = "";
    private String miscCostKm = "";
    private String driverIncentive = "";
    private String etcCost = "";
    private String costPerKm = "";
    private String creditLimit = "";
    private String availableLimit = "";
    private String[] orderSequenceNo = null;
    private String consginmentOrderStatus = "";
    private String trailerStartKm = "";
    private String hubId = "";
    private String currencyName = "";
    private String startTrailerKM = "";
    private String endTrailerKM = "";
    private String orderSize = "";
    private String trailerId = "";
    private String trailerNo = "";
    private String deattchLocId = "";
    private String seatCapacity = "";
    private String wareHouseName = "";
    private String wareHouseId = "";
    private String wareHouseAdd = "";
    private String portName = "";
    private String portId = "";
    private String portAddress = "";

    private String sourceWId = "";
    private String destinationWId = "";

    private String orderType = "";
    private String consignmentStatus = "";
    private String consignmentStatusId = "";
    private String totalDocumentAmount = "";
    private String uploadedChallan = "";
    private String uploadedChallanAmount = "";
    private String pendingChallan = "";
    private String pendingChallanAmount = "";
    private String preCoolingCost = "";
//    private String tollCostPerKm = "";
    private String advancePaid = "";
    private String expenseHour = "";
    private String expenseMinute = "";
    private String miscRate = "";
    private String advanceReturn = "";
    private String driverChangeMinute = "";
    private String driverChangeHour = "";
    private String driverInTrip = "";
    private String documentRequiredStatus = "";
    private String unclearedAmount = "";
    private String billCopyFile = "";
    private String billCopyName = "";
    private String documentRequired = "";
    private String transactionAmount = "";
    private String transactionId = "";
    private String idleDays = "";
    private String idleBhattaId = "";
    private String bhattaAmount = "";
    private String driverLastBalance = "";
    private String settleAmount = "";
    private String payAmount = "";
    private String settlementTripsSize = "";
    private String tripAmount = "";
    private String vehicleAdvanceId = "";
    private String bhattaDate = "";
    private String advanceCode = "";
    private String endingBalance = "";
    private String vehicleMileage = "";
    private String fuelConsumption = "";
    private String fuelExpense = "";
    private String tollExpense = "";
    private String driverBhatta = "";
    private String parkingCost = "";
    private String systemExpenses = "";
    private String nextTripCountStatus = "";
    private String currentTripStartDate = "";
    private String nextTrip = "";
    private String overrideTripRemarks = "";
    private String overRideBy = "";
    private String createdOn = "";
    private String overRideOn = "";
    private String setteledKm = "";
    private String setteledHm = "";
    private String setteltotalKM;
    private String setteltotalHrs;
    private String remarks = "";
    private String followedBy = "";
    private String followedOn = "";
    private String ticketingDetailId = "";
    private String fileName = "";
    private String ticketingFile = "";
    private String ticketNo = "";
    private String raisedBy = "";
    private String raisedOn = "";
    private String ticketId = "";
    private String priority = "";
    private String from = "";
    private String to = "";
    private String cc = "";
    private String title = "";
    private String message = "";
    private String type = "";
    private String emptyTripPurpose = "";
    private String transitDays1 = "";
    private String transitDays2 = "";
    private String serialNumber = "";
    private String originCityName = "";
    private String destinationCityName = "";
    private String vehicleChangeCityName = "";
    private String totalHm = "";
    private String tripAdvanceId = "";
    private String tripWfuDate = "";
    private String tripWfuTime = "";
    private String usageTypeId = "";
    private String secAdditionalTollCost = "";
    private String preColingAmount = "";
    private String secondaryParkingAmount = "";
    private String totalMinutes = "";
    private String totalKm = "";
    private String totalHours = "";
    private String expense = "";
    private String emptyTripRemarks = "";
    private String cityFrom = "";
    private String cityTo = "";
    private String mailDeliveredStatusId = "";
    private String mailSendingId = "";
    private String approvalStatusId = "";
    private String outStandingAmount = "";
    private String customerCode = "";
    private String extraExpenseStatus = "";
    private String startDate = "";
    private String endDate = "";
    private String startedBy = "";
    private String endedBy = "";
    private String mailDate = "";
    private String mailIdBcc = "";
    private String mailIdCc = "";
    private String mailIdTo = "";
    private String mailContentBcc = "";
    private String mailContentCc = "";
    private String mailContentTo = "";
    private String mailSubjectBcc = "";
    private String mailSubjectCc = "";
    private String mailSubjectTo = "";
    private String mailTypeId = "";
    private String mailId = "";
    private String jobcardId = "";
    private String businessType = "";
    private String emptyTrip = "";
    private String oldVehicleNo = "";
    private String oldDriverName = "";
    private String vehicleRemarks = "";
    private String vehicleChangeDate = "";
    private String vehicleChangeMinute = "";
    private String vehicleChangeHour = "";
    private String lastVehicleEndOdometer = "";
    private String vehicleStartOdometer = "";
    private String balanceAmount = "";
    private String lastVehicleEndKm = "";
    private String vehicleStartKm = "";
    private String returnAmount = "";
    private String licenceNo = "";
    private String licenceDate = "";
    private String employeeCode = "";
    private String joiningDate = "";
    private String fuelTypeId = "";
    private String logDateTime = "";
    private String logDate = "";
    private String logTime = "";
    private String temperature = "";
    private String location = "";
    private String distance = "";
    private String courierRemarks = "";
    private String courierNo = "";
    private String weight = "";
    private String ratePerKg = "";
    private String editMode = "";
    private String discountAmount = "";
    private String billedAmount = "";
    private String billingNameAddress = "";
    private String closureTotalExpense = "";
    private String vehicleDieselUsed = "";
    private String reeferDieselUsed = "";
    private String tempApprovalStatus = "";
    private String tripGraphId = "";
    private String ratePerKm = "";
    private String emailCc = "";
    private String approvalstatus = "";
    private String infoEmailTo = "";
    private String infoEmailCc = "";
    private String extraExpenseValue = "0";
    private String driverCount = "0";
    private String tripCountStatus = "";
    private String vehicleDriverAdvanceId = "";
    private String paymentTypeId = "";
    private String paidAmount = "";
    private String driverMobile = "";
    private String currentLocation = "";
    private String totalPackages = "";
    private String vehicleRequiredDateTime = "";
    private String vehicleRequiredDate = "";
    private String vehicleRequiredTime = "";
    private String consignmentDate = "";
    private String customerOrderReferenceNo = "";
    private String tempLogFile = "";
    private String routeNameStatus = "";
    private String route = "";
    private int routeContractId = 0;
    private String bpclTransactionId = "";
    private String secondaryTollAmount = "";
    private String secondaryAddlTollAmount = "";
    private String secondaryMiscAmount = "";
    private String transactionHistoryId = "", accountId = "", dealerName = "", dealerCity = "", transactionDate = "", accountingDate = "", transactionType = "", currency = "", amount = "", volumeDocNo = "", amoutBalance = "", petromilesEarned = "", odometerReading = "";
    private String consignorName = "";
    private String consigneeName = "";
    private String consignorAddress = "";
    private String consigneeAddress = "";
    private String consignorMobileNo = "";
    private String consigneeMobileNo = "";
    private String estimatedKM = "";
    private String emptyTripApprovalStatus = "";
    private String wfuRemarks = "";
    private String tripStartDate = "";
    private String tripStartTime = "";
    private String tripType = "";
    private String orderTripType = "";
    private String gpsKm = "";
    private String gpsHm = "";
    private String actualAdvancePaid = "";
    private String wfuUnloadingDate = "";
    private String wfuUnloadingTime = "";
    private String podStatus = "";
    private String podCount = "";
    private String paymentType = "";
    private String advanceToPayStatus = "";
    private String toPayStatus = "";
    private String currentTemperature = "";
    private String cityFromId = "";
    private String expectedArrivalDate = "";
    private String expectedArrivalTime = "";
    private String fleetCenterNo = "";
    private String fleetCenterName = "";
    private String mobileNo = "";
    private String gpsLocation = "";
    private String tripEndBalance = "";
    private String paymentMode = "";
    private String tripStartingBalance = "";
    private String tripBalance = "";
    private String zoneId = "";
    private String ZoneName = "";
    private String startReportingDate = "";
    private String startReportingTime = "";
    private String loadingTime = "";
    private String loadingDate = "";
    private String loadingTemperature = "";
    private String endReportingDate = "";
    private String endReportingTime = "";
    private String unLoadingTime = "";
    private String unLoadingDate = "";
    private String unLoadingTemperature = "";

    private String secondaryDriverIdOne = "";
    private String secondaryDriverIdTwo = "";
    private String secondaryDriverNameOne = "";
    private String secondaryDriverNameTwo = "";
    private String advanceAmount = "";
    private String advanceRemarks = "";
    private String cNotesEmail = "";
    private String vehicleNoEmail = "";
    private String tripCodeEmail = "";
    private String customerNameEmail = "";
    private String routeInfoEmail = "";
    private String stakeHolderId = "";
    private String holderName = "";
    private String fcHeadEmail = "";
    private String customerEmail = "";
    private String smtp = "";
    private String port = "";
    private String emailId = "";
    private String password = "";
    private String tomailId = "";
    private String uomId = "";
    private String routeName = "";
    private String miscValue = "";
    private String requeston = "";
    private String requestremarks = "";
    private String totalRumHM = "";
    private String consignmentId = "";
    private String articleCode = "";
    private String articleName = "";
    private String batch = "";
    private String packageNos = "";
    private String packageWeight = "";
    private String uom = "";
    private String tripArticleid = "";
    private String consArticleid = "";
    private String[] consArticleids = null;
    private String loadpackageNos = "";
    private String[] tripArticleId = null;
    private String[] productCodes = null;
    private String[] productNames = null;
    private String[] packagesNos = null;
    private String[] weights = null;
    private String[] productbatch = null;
    private String[] productuom = null;
    private String[] loadedpackages = null;
    private String[] unloadedpackages = null;
    private String[] shortage = null;
    private String vehicleactreportdate = "";
    private String vehicleactreporthour = "";
    private String vehicleactreportmin = "";
    private String vehicleloadreportdate = "";
    private String vehicleloadreporthour = "";
    private String vehicleloadreportmin = "";
    private String vehicleloadtemperature = "";
    private String approvalRequestBy = "";
    private String approvalRequestOn = "";
    private String approvalRequestRemarks = "";
    private String approvalRemarks = "";
    private String approvedBy = "";
    private String approvedOn = "";
    private String approvalStatus = "";
    private String requestType = "";
    private String systemExpense = "";
    private String rcmExpense = "";
    private String otherExpense = "";
    private String nettExpense = "";
    private String bookedExpense = "";
    private String driverBatta = "0";
    private String tollCost = "0";
    private String dieselCost = "0";
    private String dieselUsed = "0";
    private String totalDays = "0";
    private String runHm = "";
    private String runKm = "";
    private String reeferMileage = "";
    private String milleage = "";
    private String fuelPrice = "";
    private String driverIncentivePerKm = "";
    private String driverBattaPerDay = "";
    private String tollRate = "";
    private String estimatedExpense = "";
    private String permitExpiryDate = "";

    private String roleId = "";
    private String fleetCenterId = "";
    private String companyId = "";
    private String roadTaxExpiryDate = "";
    private String fcExpiryDate = "";
    private String insuranceExpiryDate = "";
    private String userName = "";
    private String productInfo = "";
    private String tripEndDate = "";
    private String expenseToBeBilledToCustomer = "";
    private String tripEndTime = "";
    private String startKm = "";
    private String startHm = "";
    private String endKm = "";
    private String endHm = "";
    private String freightAmount = "";
    private String tripPlanEndDate = "";
    private String tripPlanEndTime = "";
    private String tripclosureid = "";
    private String totalMins = "";
    private String tollAmount = "0";

//    private String driverIncentive = "";
    private String reeferConsumption = "";
    private String fuelConsumed = "";
    private String fuelCost = "0";
    private String routeExpense = "";
    private String totalExpenses = "";
    private String totalRumKM = "";
    private String totalRefeerHours = "";
    private String totalRefeerMinutes = "";
    private String battaAmount = "0";
    private String incentiveAmount = "0";
    private String routeExpenses = "";
    private String planStartDate = "";
    private String planStartHour = "";
    private String planStartMinute = "";
    private String planEndDate = "";
    private String planEndHour = "";
    private String planEndMinute = "";
    private String planStartTime = "";
    private String planEndTime = "";
    private String actionName = "";
    private String actionRemarks = "";
    private String preStartLocationId = "";
    private String preStartLocation = "";
    private String preStartLocationPlanDate = "";
    private String preStartLocationPlanTime = "";
    private String preStartLocationPlanTimeHrs = "";
    private String preStartLocationPlanTimeMins = "";
    private String preStartLocationDistance = "";
    private String preStartLocationDurationHrs = "";
    private String preStartLocationDurationMins = "";
    private String preStartLocationVehicleMileage = "";
    private String preStartLocationTollRate = "";
    private String preStartLocationRouteExpense = "";
    private String preStartLocationStatus = "";
    private String tripEndMinute = "";
    private String tripEndHour = "";
    private String tripStartMinute = "";
    private String tripStartHour = "";
    private String tripPreStartMinute = "";
    private String tripPreStartHour = "";
    private String advanceDate = "";
    private String tripDay = "";
    private String estimatedAdance = "";
    private String requestedAdvance = "";
    private String paidAdvance = "";
    private String totalFuelValue = "";
    private String empName = "";
    private String applicableTaxPercentage = "";
    private String totalExpenseValue = "";
    private String fuel = "";
    private String podPointId = "";
    private String startTime = "";
    private String startOdometerReading = "";
    private String startHM = "";
    //private String pointId = "";
    private String cityId = "";
    private String cityName = "";
    private String podRemarks = "";
    private String podFile = "";
    private String startTripRemarks = "";
    private String endTime = "";
    private String endHM = "";
    private String endOdometerReading = "";
    private String endTripRemarks = "";
    private String preStartHM = "";
    private String fuelLocation = "";
    private String fillDate = "";
    private String fuelLitres = "";
    private String fuelPricePerLitre = "";
    private String fuelAmount = "";
    private String fuelremarks = "";
    private String expenseId = "";
    private String expenseDate = "";
    private String expenseType = "";
    private String totalExpenseAmount = "";
    private String expenseRemarks = "";
    private String expenseName = "";
    private String driverNameex = "";
    private String employeeId = "";
    private String employeeName = "";
    private String taxPercentage = "";
    private double taxPercentageD = 0;
    private String totalValue = "";
    private String nettValue = "";
    private String taxValue = "";

    public String getDriverNameex() {
        return driverNameex;
    }

    public void setDriverNameex(String driverNameex) {
        this.driverNameex = driverNameex;
    }
    private String expenseValue = "";
    private String statusId = "";
    private String tripStatusId = "";
    private String tripExpenseId = "";
    private String tripPodId = "";
    private String durationHours = "";
    private String durationDay1 = "";
    private String durationDay2 = "";
    //Arul Start Here
    private String tripPlannedDate = "";
    private String tripPlannedStartTime = "";
    private String tripSheetDate = "";
    private String consignmentNote = "";
    private String routeInfo = "";
    private String vehicleTonnage = "";
    private String estimatedTonnage = "";
    private String vehicleUtilisation = "";
    private String tripRemarks = "";
    private String vehicleType = "";
    private String estimatedRevenue = "";
    private String totalRevenue = "";
    private String grandTotal = "";
    private String totalExpToBeBilled = "";
    private String estimatedTransitDays = "";
    private String estimatedTransitHours = "";
    private String estimatedAdvancePerDay = "";
    //Arul End Here
    private String primaryDriverId = "";
    private String secondaryDriver1Id = "";
    private String secondaryDriver2Id = "";
    private String primaryDriverName = "";
    private String secondaryDriver1Name = "";
    private String secondaryDriver2Name = "";
    private String tripDate = "";
    private String vehicleCapUtil = "";
    private String tripId = "";
    private String[] tripIds = null;
    private int userId = 0;
    private String driver1Id = "";
    private String statusName = "";
    private String profitMargin = "";
    private String passThroughStatus = "";
    private String marginValue = "";
    private String tripCode = "";
    private String invoiceCode = "";
    private String invoiceId = "";
    private String invoiceDetailId = "";
    private String tripTransitDays = "";
    private String tripTransitHours = "";
    private String advnaceToBePaidPerDay = "";
    private String[] consignmentOrderId = null;
    private String[] orderIds = null;
    private String[] pointId = null;
    private String[] pointName = null;
    private String[] pointType = null;
    private String[] pointOrder = null;
    private String[] pointAddresss = null;
    private String[] pointPlanDate = null;
    private String[] pointPlanTime = null;
    private String[] pointPlanTimeHrs = null;
    private String[] pointPlanTimeMins = null;
    private String cNotes = "";
    private String customerName = "";
    private String customerAddress = "";
    private String billDate = "";
    private String totalWeight = "";
    private String reeferRequired = "";
    private String consginmentRemarks = "";
    private String customerType = "";
    private String billingType = "";
//    private String vehicleTypeId = "";
    private String paidStatus = "";
    private String vehicleId = "";
    private String vehicleNo = "";
    private String driverId = "";
    private String driverName = "";
    private String cleanerId = "";
    private String cleanerName = "";
    private String vehicleTypeName = "";
    private String orderExpense = "";
    private String orderRevenue = "";
    private String tripScheduleDate = "";
    private String tripScheduleTime = "";
    private String tripScheduleTimeHrs = "";
    private String tripScheduleTimeMins = "";
    private String tripScheduleTimeDB = "";
    private String point1PlannedDate = "";
    private String point1PlannedTimeHrs = "";
    private String point1PlannedTimeMins = "";
    private String point2PlannedDate = "";
    private String point2PlannedTimeHrs = "";
    private String point2PlannedTimeMins = "";
    private String point3PlannedDate = "";
    private String point3PlannedTimeHrs = "";
    private String point3PlannedTimeMins = "";
    private String point4PlannedDate = "";
    private String point4PlannedTimeHrs = "";
    private String point4PlannedTimeMins = "";
    private String finalPlannedDate = "";
    private String finalPlannedTimeMins = "";
    private String finalPlannedTimeHrs = "";
    private String orderId = "";
    private String orderNo = "";
    private String routeId = "";
    private String origin = "";
    private String originId = "";
    private String destination = "";
    private String destinationId = "";
    private String firstPointId = "";
    private String finalPointId = "";
    private String point1Id = "";
    private String point1Name = "";
    private String point2Id = "";
    private String point2Name = "";
    private String point3Id = "";
    private String point3Name = "";
    private String point4Id = "";
    private String point4Name = "";
    private String totalKM = "";
    private String totalHrs = "";
    private String totalPoints = "";
    private String interimExists = "";
    private String consignmentOrderNos = "";
    private String vehicleExpense = "";
    private String reeferExpense = "";
    private String consignOrderId = "";
    private boolean doesConsignmentRouteCourseExists = false;
    private ArrayList consignmentRoutePoints = null;
    private ArrayList userMovementList = null;
//       Nithiya starts here
    private String preStartDate = "";
    private String preStartTime = "";
    private String preOdometerReading = "";
    private String preTripRemarks = "";
    private String regNo = "";
    private String tripSheetId = "";
    private String consignmentNo = "";
    private String customerId = "";
    private String noOfTrips = "";
    private String fromDate = "";
    private String toDate = "";
    private String status = "";
    private int accVehicleId = 0;
    private int reasonForChange = 0;
    private String newSlip = "";
    private String containerQuantity = "";
    private String gpsVendor = "";
    private String gpsDeviceId = "";

    private String compensationFuel = "";

    public String getCompensationFuel() {
        return compensationFuel;
    }

    public void setCompensationFuel(String compensationFuel) {
        this.compensationFuel = compensationFuel;
    }

//   Nithiya end here
    public String getConsignmentOrderNos() {
        return consignmentOrderNos;
    }

    public void setConsignmentOrderNos(String consignmentOrderNos) {
        this.consignmentOrderNos = consignmentOrderNos;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getFinalPointId() {
        return finalPointId;
    }

    public void setFinalPointId(String finalPointId) {
        this.finalPointId = finalPointId;
    }

    public String getFirstPointId() {
        return firstPointId;
    }

    public void setFirstPointId(String firstPointId) {
        this.firstPointId = firstPointId;
    }

    public String getInterimExists() {
        return interimExists;
    }

    public void setInterimExists(String interimExists) {
        this.interimExists = interimExists;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getPoint1Id() {
        return point1Id;
    }

    public void setPoint1Id(String point1Id) {
        this.point1Id = point1Id;
    }

    public String getPoint1Name() {
        return point1Name;
    }

    public void setPoint1Name(String point1Name) {
        this.point1Name = point1Name;
    }

    public String getPoint2Id() {
        return point2Id;
    }

    public void setPoint2Id(String point2Id) {
        this.point2Id = point2Id;
    }

    public String getPoint2Name() {
        return point2Name;
    }

    public void setPoint2Name(String point2Name) {
        this.point2Name = point2Name;
    }

    public String getPoint3Id() {
        return point3Id;
    }

    public void setPoint3Id(String point3Id) {
        this.point3Id = point3Id;
    }

    public String getPoint3Name() {
        return point3Name;
    }

    public void setPoint3Name(String point3Name) {
        this.point3Name = point3Name;
    }

    public String getPoint4Id() {
        return point4Id;
    }

    public void setPoint4Id(String point4Id) {
        this.point4Id = point4Id;
    }

    public String getPoint4Name() {
        return point4Name;
    }

    public void setPoint4Name(String point4Name) {
        this.point4Name = point4Name;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getTotalHrs() {
        return totalHrs;
    }

    public void setTotalHrs(String totalHrs) {
        this.totalHrs = totalHrs;
    }

    public String getTotalKM() {
        return totalKM;
    }

    public void setTotalKM(String totalKM) {
        this.totalKM = totalKM;
    }

    public String getFinalPlannedDate() {
        return finalPlannedDate;
    }

    public void setFinalPlannedDate(String finalPlannedDate) {
        this.finalPlannedDate = finalPlannedDate;
    }

    public String getPoint1PlannedDate() {
        return point1PlannedDate;
    }

    public void setPoint1PlannedDate(String point1PlannedDate) {
        this.point1PlannedDate = point1PlannedDate;
    }

    public String getPoint2PlannedDate() {
        return point2PlannedDate;
    }

    public void setPoint2PlannedDate(String point2PlannedDate) {
        this.point2PlannedDate = point2PlannedDate;
    }

    public String getPoint3PlannedDate() {
        return point3PlannedDate;
    }

    public void setPoint3PlannedDate(String point3PlannedDate) {
        this.point3PlannedDate = point3PlannedDate;
    }

    public String getPoint4PlannedDate() {
        return point4PlannedDate;
    }

    public void setPoint4PlannedDate(String point4PlannedDate) {
        this.point4PlannedDate = point4PlannedDate;
    }

    public String getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(String totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getTripScheduleDate() {
        return tripScheduleDate;
    }

    public void setTripScheduleDate(String tripScheduleDate) {
        this.tripScheduleDate = tripScheduleDate;
    }

    public String getTripScheduleTime() {
        return tripScheduleTime;
    }

    public void setTripScheduleTime(String tripScheduleTime) {
        this.tripScheduleTime = tripScheduleTime;
    }

    public String getOrderRevenue() {
        return orderRevenue;
    }

    public void setOrderRevenue(String orderRevenue) {
        this.orderRevenue = orderRevenue;
    }

    public String getReeferExpense() {
        return reeferExpense;
    }

    public void setReeferExpense(String reeferExpense) {
        this.reeferExpense = reeferExpense;
    }

    public String getVehicleExpense() {
        return vehicleExpense;
    }

    public void setVehicleExpense(String vehicleExpense) {
        this.vehicleExpense = vehicleExpense;
    }

    public String getOrderExpense() {
        return orderExpense;
    }

    public void setOrderExpense(String orderExpense) {
        this.orderExpense = orderExpense;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getVehicleNo() {
        return vehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        this.vehicleNo = vehicleNo;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getBillingType() {
        return billingType;
    }

    public void setBillingType(String billingType) {
        this.billingType = billingType;
    }

    public String getConsginmentRemarks() {
        return consginmentRemarks;
    }

    public void setConsginmentRemarks(String consginmentRemarks) {
        this.consginmentRemarks = consginmentRemarks;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getReeferRequired() {
        return reeferRequired;
    }

    public void setReeferRequired(String reeferRequired) {
        this.reeferRequired = reeferRequired;
    }

    public String getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(String totalWeight) {
        this.totalWeight = totalWeight;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public boolean isDoesConsignmentRouteCourseExists() {
        return doesConsignmentRouteCourseExists;
    }

    public void setDoesConsignmentRouteCourseExists(boolean doesConsignmentRouteCourseExists) {
        this.doesConsignmentRouteCourseExists = doesConsignmentRouteCourseExists;
    }

    public ArrayList getConsignmentRoutePoints() {
        return consignmentRoutePoints;
    }

    public void setConsignmentRoutePoints(ArrayList consignmentRoutePoints) {
        this.consignmentRoutePoints = consignmentRoutePoints;
    }

    public String getCleanerId() {
        return cleanerId;
    }

    public void setCleanerId(String cleanerId) {
        this.cleanerId = cleanerId;
    }

    public String getCleanerName() {
        return cleanerName;
    }

    public void setCleanerName(String cleanerName) {
        this.cleanerName = cleanerName;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String[] getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String[] consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getDriver1Id() {
        return driver1Id;
    }

    public void setDriver1Id(String driver1Id) {
        this.driver1Id = driver1Id;
    }

    public String[] getOrderIds() {
        return orderIds;
    }

    public void setOrderIds(String[] orderIds) {
        this.orderIds = orderIds;
    }

    public String[] getPointAddresss() {
        return pointAddresss;
    }

    public void setPointAddresss(String[] pointAddresss) {
        this.pointAddresss = pointAddresss;
    }

    public String[] getPointId() {
        return pointId;
    }

    public void setPointId(String[] pointId) {
        this.pointId = pointId;
    }

    public String[] getPointOrder() {
        return pointOrder;
    }

    public void setPointOrder(String[] pointOrder) {
        this.pointOrder = pointOrder;
    }

    public String[] getPointPlanDate() {
        return pointPlanDate;
    }

    public void setPointPlanDate(String[] pointPlanDate) {
        this.pointPlanDate = pointPlanDate;
    }

    public String[] getPointPlanTime() {
        return pointPlanTime;
    }

    public void setPointPlanTime(String[] pointPlanTime) {
        this.pointPlanTime = pointPlanTime;
    }

    public String[] getPointType() {
        return pointType;
    }

    public void setPointType(String[] pointType) {
        this.pointType = pointType;
    }

    public String getProfitMargin() {
        return profitMargin;
    }

    public void setProfitMargin(String profitMargin) {
        this.profitMargin = profitMargin;
    }

    public String getTripDate() {
        return tripDate;
    }

    public void setTripDate(String tripDate) {
        this.tripDate = tripDate;
    }

    public String getTripCode() {
        return tripCode;
    }

    public void setTripCode(String tripCode) {
        this.tripCode = tripCode;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getStatusName() {
        return statusName;
    }

    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }

    public String getTripTransitDays() {
        return tripTransitDays;
    }

    public void setTripTransitDays(String tripTransitDays) {
        this.tripTransitDays = tripTransitDays;
    }

    public String getTripTransitHours() {
        return tripTransitHours;
    }

    public void setTripTransitHours(String tripTransitHours) {
        this.tripTransitHours = tripTransitHours;
    }

    public String getAdvnaceToBePaidPerDay() {
        return advnaceToBePaidPerDay;
    }

    public void setAdvnaceToBePaidPerDay(String advnaceToBePaidPerDay) {
        this.advnaceToBePaidPerDay = advnaceToBePaidPerDay;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTripScheduleTimeDB() {
        return tripScheduleTimeDB;
    }

    public void setTripScheduleTimeDB(String tripScheduleTimeDB) {
        this.tripScheduleTimeDB = tripScheduleTimeDB;
    }

    public String getFinalPlannedTimeHrs() {
        return finalPlannedTimeHrs;
    }

    public void setFinalPlannedTimeHrs(String finalPlannedTimeHrs) {
        this.finalPlannedTimeHrs = finalPlannedTimeHrs;
    }

    public String getFinalPlannedTimeMins() {
        return finalPlannedTimeMins;
    }

    public void setFinalPlannedTimeMins(String finalPlannedTimeMins) {
        this.finalPlannedTimeMins = finalPlannedTimeMins;
    }

    public String getPoint1PlannedTimeHrs() {
        return point1PlannedTimeHrs;
    }

    public void setPoint1PlannedTimeHrs(String point1PlannedTimeHrs) {
        this.point1PlannedTimeHrs = point1PlannedTimeHrs;
    }

    public String getPoint1PlannedTimeMins() {
        return point1PlannedTimeMins;
    }

    public void setPoint1PlannedTimeMins(String point1PlannedTimeMins) {
        this.point1PlannedTimeMins = point1PlannedTimeMins;
    }

    public String getPoint2PlannedTimeHrs() {
        return point2PlannedTimeHrs;
    }

    public void setPoint2PlannedTimeHrs(String point2PlannedTimeHrs) {
        this.point2PlannedTimeHrs = point2PlannedTimeHrs;
    }

    public String getPoint2PlannedTimeMins() {
        return point2PlannedTimeMins;
    }

    public void setPoint2PlannedTimeMins(String point2PlannedTimeMins) {
        this.point2PlannedTimeMins = point2PlannedTimeMins;
    }

    public String getPoint3PlannedTimeHrs() {
        return point3PlannedTimeHrs;
    }

    public void setPoint3PlannedTimeHrs(String point3PlannedTimeHrs) {
        this.point3PlannedTimeHrs = point3PlannedTimeHrs;
    }

    public String getPoint3PlannedTimeMins() {
        return point3PlannedTimeMins;
    }

    public void setPoint3PlannedTimeMins(String point3PlannedTimeMins) {
        this.point3PlannedTimeMins = point3PlannedTimeMins;
    }

    public String getPoint4PlannedTimeHrs() {
        return point4PlannedTimeHrs;
    }

    public void setPoint4PlannedTimeHrs(String point4PlannedTimeHrs) {
        this.point4PlannedTimeHrs = point4PlannedTimeHrs;
    }

    public String getPoint4PlannedTimeMins() {
        return point4PlannedTimeMins;
    }

    public void setPoint4PlannedTimeMins(String point4PlannedTimeMins) {
        this.point4PlannedTimeMins = point4PlannedTimeMins;
    }

    public String getTripScheduleTimeHrs() {
        return tripScheduleTimeHrs;
    }

    public void setTripScheduleTimeHrs(String tripScheduleTimeHrs) {
        this.tripScheduleTimeHrs = tripScheduleTimeHrs;
    }

    public String getTripScheduleTimeMins() {
        return tripScheduleTimeMins;
    }

    public void setTripScheduleTimeMins(String tripScheduleTimeMins) {
        this.tripScheduleTimeMins = tripScheduleTimeMins;
    }

    public String[] getPointPlanTimeHrs() {
        return pointPlanTimeHrs;
    }

    public void setPointPlanTimeHrs(String[] pointPlanTimeHrs) {
        this.pointPlanTimeHrs = pointPlanTimeHrs;
    }

    public String[] getPointPlanTimeMins() {
        return pointPlanTimeMins;
    }

    public void setPointPlanTimeMins(String[] pointPlanTimeMins) {
        this.pointPlanTimeMins = pointPlanTimeMins;
    }

    public String getVehicleTonnage() {
        return vehicleTonnage;
    }

    public void setVehicleTonnage(String vehicleTonnage) {
        this.vehicleTonnage = vehicleTonnage;
    }

    public String getcNotes() {
        return cNotes;
    }

    public void setcNotes(String cNotes) {
        this.cNotes = cNotes;
    }

    public String getRouteInfo() {
        return routeInfo;
    }

    public void setRouteInfo(String routeInfo) {
        this.routeInfo = routeInfo;
    }

    public String getTripRemarks() {
        return tripRemarks;
    }

    public void setTripRemarks(String tripRemarks) {
        this.tripRemarks = tripRemarks;
    }

    public String getVehicleCapUtil() {
        return vehicleCapUtil;
    }

    public void setVehicleCapUtil(String vehicleCapUtil) {
        this.vehicleCapUtil = vehicleCapUtil;
    }

    public String getConsignmentNo() {
        return consignmentNo;
    }

    public void setConsignmentNo(String consignmentNo) {
        this.consignmentNo = consignmentNo;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getPreOdometerReading() {
        return preOdometerReading;
    }

    public void setPreOdometerReading(String preOdometerReading) {
        this.preOdometerReading = preOdometerReading;
    }

    public String getPreStartDate() {
        return preStartDate;
    }

    public void setPreStartDate(String preStartDate) {
        this.preStartDate = preStartDate;
    }

    public String getPreStartLocation() {
        return preStartLocation;
    }

    public void setPreStartLocation(String preStartLocation) {
        this.preStartLocation = preStartLocation;
    }

    public String getPreStartTime() {
        return preStartTime;
    }

    public void setPreStartTime(String preStartTime) {
        this.preStartTime = preStartTime;
    }

    public String getPreTripRemarks() {
        return preTripRemarks;
    }

    public void setPreTripRemarks(String preTripRemarks) {
        this.preTripRemarks = preTripRemarks;
    }

    public String getRegNo() {
        return regNo;
    }

    public void setRegNo(String regNo) {
        this.regNo = regNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getTripSheetId() {
        return tripSheetId;
    }

    public void setTripSheetId(String tripSheetId) {
        this.tripSheetId = tripSheetId;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionRemarks() {
        return actionRemarks;
    }

    public void setActionRemarks(String actionRemarks) {
        this.actionRemarks = actionRemarks;
    }

    public String getPrimaryDriverId() {
        return primaryDriverId;
    }

    public void setPrimaryDriverId(String primaryDriverId) {
        this.primaryDriverId = primaryDriverId;
    }

    public String getPrimaryDriverName() {
        return primaryDriverName;
    }

    public void setPrimaryDriverName(String primaryDriverName) {
        this.primaryDriverName = primaryDriverName;
    }

    public String getSecondaryDriver1Id() {
        return secondaryDriver1Id;
    }

    public void setSecondaryDriver1Id(String secondaryDriver1Id) {
        this.secondaryDriver1Id = secondaryDriver1Id;
    }

    public String getSecondaryDriver1Name() {
        return secondaryDriver1Name;
    }

    public void setSecondaryDriver1Name(String secondaryDriver1Name) {
        this.secondaryDriver1Name = secondaryDriver1Name;
    }

    public String getSecondaryDriver2Id() {
        return secondaryDriver2Id;
    }

    public void setSecondaryDriver2Id(String secondaryDriver2Id) {
        this.secondaryDriver2Id = secondaryDriver2Id;
    }

    public String getSecondaryDriver2Name() {
        return secondaryDriver2Name;
    }

    public void setSecondaryDriver2Name(String secondaryDriver2Name) {
        this.secondaryDriver2Name = secondaryDriver2Name;
    }

    public String getConsignmentNote() {
        return consignmentNote;
    }

    public void setConsignmentNote(String consignmentNote) {
        this.consignmentNote = consignmentNote;
    }

    public String getEstimatedAdvancePerDay() {
        return estimatedAdvancePerDay;
    }

    public void setEstimatedAdvancePerDay(String estimatedAdvancePerDay) {
        this.estimatedAdvancePerDay = estimatedAdvancePerDay;
    }

    public String getEstimatedRevenue() {
        return estimatedRevenue;
    }

    public void setEstimatedRevenue(String estimatedRevenue) {
        this.estimatedRevenue = estimatedRevenue;
    }

    public String getEstimatedTonnage() {
        return estimatedTonnage;
    }

    public void setEstimatedTonnage(String estimatedTonnage) {
        this.estimatedTonnage = estimatedTonnage;
    }

    public String getEstimatedTransitDays() {
        return estimatedTransitDays;
    }

    public void setEstimatedTransitDays(String estimatedTransitDays) {
        this.estimatedTransitDays = estimatedTransitDays;
    }

    public String getEstimatedTransitHours() {
        return estimatedTransitHours;
    }

    public void setEstimatedTransitHours(String estimatedTransitHours) {
        this.estimatedTransitHours = estimatedTransitHours;
    }

    public String getTripPlannedDate() {
        return tripPlannedDate;
    }

    public void setTripPlannedDate(String tripPlannedDate) {
        this.tripPlannedDate = tripPlannedDate;
    }

    public String getTripPlannedStartTime() {
        return tripPlannedStartTime;
    }

    public void setTripPlannedStartTime(String tripPlannedStartTime) {
        this.tripPlannedStartTime = tripPlannedStartTime;
    }

    public String getTripSheetDate() {
        return tripSheetDate;
    }

    public void setTripSheetDate(String tripSheetDate) {
        this.tripSheetDate = tripSheetDate;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleUtilisation() {
        return vehicleUtilisation;
    }

    public void setVehicleUtilisation(String vehicleUtilisation) {
        this.vehicleUtilisation = vehicleUtilisation;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDurationDay1() {
        return durationDay1;
    }

    public void setDurationDay1(String durationDay1) {
        this.durationDay1 = durationDay1;
    }

    public String getDurationDay2() {
        return durationDay2;
    }

    public void setDurationDay2(String durationDay2) {
        this.durationDay2 = durationDay2;
    }

    public String getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(String durationHours) {
        this.durationHours = durationHours;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getEndHM() {
        return endHM;
    }

    public void setEndHM(String endHM) {
        this.endHM = endHM;
    }

    public String getEndOdometerReading() {
        return endOdometerReading;
    }

    public void setEndOdometerReading(String endOdometerReading) {
        this.endOdometerReading = endOdometerReading;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEndTripRemarks() {
        return endTripRemarks;
    }

    public void setEndTripRemarks(String endTripRemarks) {
        this.endTripRemarks = endTripRemarks;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(String expenseId) {
        this.expenseId = expenseId;
    }

    public String getExpenseName() {
        return expenseName;
    }

    public void setExpenseName(String expenseName) {
        this.expenseName = expenseName;
    }

    public String getExpenseRemarks() {
        return expenseRemarks;
    }

    public void setExpenseRemarks(String expenseRemarks) {
        this.expenseRemarks = expenseRemarks;
    }

    public String getExpenseType() {
        return expenseType;
    }

    public void setExpenseType(String expenseType) {
        this.expenseType = expenseType;
    }

    public String getExpenseValue() {
        return expenseValue;
    }

    public void setExpenseValue(String expenseValue) {
        this.expenseValue = expenseValue;
    }

    public String getFillDate() {
        return fillDate;
    }

    public void setFillDate(String fillDate) {
        this.fillDate = fillDate;
    }

    public String getFuelAmount() {
        return fuelAmount;
    }

    public void setFuelAmount(String fuelAmount) {
        this.fuelAmount = fuelAmount;
    }

    public String getFuelLitres() {
        return fuelLitres;
    }

    public void setFuelLitres(String fuelLitres) {
        this.fuelLitres = fuelLitres;
    }

    public String getFuelLocation() {
        return fuelLocation;
    }

    public void setFuelLocation(String fuelLocation) {
        this.fuelLocation = fuelLocation;
    }

    public String getFuelPricePerLitre() {
        return fuelPricePerLitre;
    }

    public void setFuelPricePerLitre(String fuelPricePerLitre) {
        this.fuelPricePerLitre = fuelPricePerLitre;
    }

    public String getFuelremarks() {
        return fuelremarks;
    }

    public void setFuelremarks(String fuelremarks) {
        this.fuelremarks = fuelremarks;
    }

    public String getPodFile() {
        return podFile;
    }

    public void setPodFile(String podFile) {
        this.podFile = podFile;
    }

    public String getPodRemarks() {
        return podRemarks;
    }

    public void setPodRemarks(String podRemarks) {
        this.podRemarks = podRemarks;
    }

    public String getPreStartHM() {
        return preStartHM;
    }

    public void setPreStartHM(String preStartHM) {
        this.preStartHM = preStartHM;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getStartHM() {
        return startHM;
    }

    public void setStartHM(String startHM) {
        this.startHM = startHM;
    }

    public String getStartOdometerReading() {
        return startOdometerReading;
    }

    public void setStartOdometerReading(String startOdometerReading) {
        this.startOdometerReading = startOdometerReading;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getStartTripRemarks() {
        return startTripRemarks;
    }

    public void setStartTripRemarks(String startTripRemarks) {
        this.startTripRemarks = startTripRemarks;
    }

    public String getTaxPercentage() {
        return taxPercentage;
    }

    public void setTaxPercentage(String taxPercentage) {
        this.taxPercentage = taxPercentage;
    }

    public String getTotalDays() {
        return totalDays;
    }

    public void setTotalDays(String totalDays) {
        this.totalDays = totalDays;
    }

    public String getTotalExpenseAmount() {
        return totalExpenseAmount;
    }

    public void setTotalExpenseAmount(String totalExpenseAmount) {
        this.totalExpenseAmount = totalExpenseAmount;
    }

    public String getTripExpenseId() {
        return tripExpenseId;
    }

    public void setTripExpenseId(String tripExpenseId) {
        this.tripExpenseId = tripExpenseId;
    }

    public String getTripPodId() {
        return tripPodId;
    }

    public void setTripPodId(String tripPodId) {
        this.tripPodId = tripPodId;
    }

    public String getPodPointId() {
        return podPointId;
    }

    public void setPodPointId(String podPointId) {
        this.podPointId = podPointId;
    }

    public String getAdvanceDate() {
        return advanceDate;
    }

    public void setAdvanceDate(String advanceDate) {
        this.advanceDate = advanceDate;
    }

    public String getApplicableTaxPercentage() {
        return applicableTaxPercentage;
    }

    public void setApplicableTaxPercentage(String applicableTaxPercentage) {
        this.applicableTaxPercentage = applicableTaxPercentage;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEstimatedAdance() {
        return estimatedAdance;
    }

    public void setEstimatedAdance(String estimatedAdance) {
        this.estimatedAdance = estimatedAdance;
    }

    public String getFuelDate() {
        return fuelDate;
    }

    public void setFuelDate(String fuelDate) {
        this.fuelDate = fuelDate;
    }

    public String getFuelRemarks() {
        return fuelRemarks;
    }

    public void setFuelRemarks(String fuelRemarks) {
        this.fuelRemarks = fuelRemarks;
    }

    public String getPaidAdvance() {
        return paidAdvance;
    }

    public void setPaidAdvance(String paidAdvance) {
        this.paidAdvance = paidAdvance;
    }

    public String getRequestedAdvance() {
        return requestedAdvance;
    }

    public void setRequestedAdvance(String requestedAdvance) {
        this.requestedAdvance = requestedAdvance;
    }

    public String getTotalExpenseValue() {
        return totalExpenseValue;
    }

    public void setTotalExpenseValue(String totalExpenseValue) {
        this.totalExpenseValue = totalExpenseValue;
    }

    public String getTotalFuelValue() {
        return totalFuelValue;
    }

    public void setTotalFuelValue(String totalFuelValue) {
        this.totalFuelValue = totalFuelValue;
    }

    public String getTripDay() {
        return tripDay;
    }

    public void setTripDay(String tripDay) {
        this.tripDay = tripDay;
    }

    public String getTripEndHour() {
        return tripEndHour;
    }

    public void setTripEndHour(String tripEndHour) {
        this.tripEndHour = tripEndHour;
    }

    public String getTripEndMinute() {
        return tripEndMinute;
    }

    public void setTripEndMinute(String tripEndMinute) {
        this.tripEndMinute = tripEndMinute;
    }

    public String getTripPreStartHour() {
        return tripPreStartHour;
    }

    public void setTripPreStartHour(String tripPreStartHour) {
        this.tripPreStartHour = tripPreStartHour;
    }

    public String getTripPreStartMinute() {
        return tripPreStartMinute;
    }

    public void setTripPreStartMinute(String tripPreStartMinute) {
        this.tripPreStartMinute = tripPreStartMinute;
    }

    public String getTripStartHour() {
        return tripStartHour;
    }

    public void setTripStartHour(String tripStartHour) {
        this.tripStartHour = tripStartHour;
    }

    public String getTripStartMinute() {
        return tripStartMinute;
    }

    public void setTripStartMinute(String tripStartMinute) {
        this.tripStartMinute = tripStartMinute;
    }

    public String getPreStartLocationDistance() {
        return preStartLocationDistance;
    }

    public void setPreStartLocationDistance(String preStartLocationDistance) {
        this.preStartLocationDistance = preStartLocationDistance;
    }

    public String getPreStartLocationDurationHrs() {
        return preStartLocationDurationHrs;
    }

    public void setPreStartLocationDurationHrs(String preStartLocationDurationHrs) {
        this.preStartLocationDurationHrs = preStartLocationDurationHrs;
    }

    public String getPreStartLocationDurationMins() {
        return preStartLocationDurationMins;
    }

    public void setPreStartLocationDurationMins(String preStartLocationDurationMins) {
        this.preStartLocationDurationMins = preStartLocationDurationMins;
    }

    public String getPreStartLocationId() {
        return preStartLocationId;
    }

    public void setPreStartLocationId(String preStartLocationId) {
        this.preStartLocationId = preStartLocationId;
    }

    public String getPreStartLocationPlanDate() {
        return preStartLocationPlanDate;
    }

    public void setPreStartLocationPlanDate(String preStartLocationPlanDate) {
        this.preStartLocationPlanDate = preStartLocationPlanDate;
    }

    public String getPreStartLocationPlanTimeHrs() {
        return preStartLocationPlanTimeHrs;
    }

    public void setPreStartLocationPlanTimeHrs(String preStartLocationPlanTimeHrs) {
        this.preStartLocationPlanTimeHrs = preStartLocationPlanTimeHrs;
    }

    public String getPreStartLocationPlanTimeMins() {
        return preStartLocationPlanTimeMins;
    }

    public void setPreStartLocationPlanTimeMins(String preStartLocationPlanTimeMins) {
        this.preStartLocationPlanTimeMins = preStartLocationPlanTimeMins;
    }

    public String getPreStartLocationRouteExpense() {
        return preStartLocationRouteExpense;
    }

    public void setPreStartLocationRouteExpense(String preStartLocationRouteExpense) {
        this.preStartLocationRouteExpense = preStartLocationRouteExpense;
    }

    public String getPreStartLocationTollRate() {
        return preStartLocationTollRate;
    }

    public void setPreStartLocationTollRate(String preStartLocationTollRate) {
        this.preStartLocationTollRate = preStartLocationTollRate;
    }

    public String getPreStartLocationVehicleMileage() {
        return preStartLocationVehicleMileage;
    }

    public void setPreStartLocationVehicleMileage(String preStartLocationVehicleMileage) {
        this.preStartLocationVehicleMileage = preStartLocationVehicleMileage;
    }

    public String getPreStartLocationStatus() {
        return preStartLocationStatus;
    }

    public void setPreStartLocationStatus(String preStartLocationStatus) {
        this.preStartLocationStatus = preStartLocationStatus;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getPlanEndDate() {
        return planEndDate;
    }

    public void setPlanEndDate(String planEndDate) {
        this.planEndDate = planEndDate;
    }

    public String getPlanEndHour() {
        return planEndHour;
    }

    public void setPlanEndHour(String planEndHour) {
        this.planEndHour = planEndHour;
    }

    public String getPlanEndMinute() {
        return planEndMinute;
    }

    public void setPlanEndMinute(String planEndMinute) {
        this.planEndMinute = planEndMinute;
    }

    public String getPlanEndTime() {
        return planEndTime;
    }

    public void setPlanEndTime(String planEndTime) {
        this.planEndTime = planEndTime;
    }

    public String getPlanStartDate() {
        return planStartDate;
    }

    public void setPlanStartDate(String planStartDate) {
        this.planStartDate = planStartDate;
    }

    public String getPlanStartHour() {
        return planStartHour;
    }

    public void setPlanStartHour(String planStartHour) {
        this.planStartHour = planStartHour;
    }

    public String getPlanStartMinute() {
        return planStartMinute;
    }

    public void setPlanStartMinute(String planStartMinute) {
        this.planStartMinute = planStartMinute;
    }

    public String getPlanStartTime() {
        return planStartTime;
    }

    public void setPlanStartTime(String planStartTime) {
        this.planStartTime = planStartTime;
    }

    public String getBattaAmount() {
        return battaAmount;
    }

    public void setBattaAmount(String battaAmount) {
        this.battaAmount = battaAmount;
    }

    public String getDriverBatta() {
        return driverBatta;
    }

    public void setDriverBatta(String driverBatta) {
        this.driverBatta = driverBatta;
    }

    public String getDriverIncentive() {
        return driverIncentive;
    }

    public void setDriverIncentive(String driverIncentive) {
        this.driverIncentive = driverIncentive;
    }

    public String getEstimatedExpense() {
        return estimatedExpense;
    }

    public void setEstimatedExpense(String estimatedExpense) {
        this.estimatedExpense = estimatedExpense;
    }

    public String getFuelConsumed() {
        return fuelConsumed;
    }

    public void setFuelConsumed(String fuelConsumed) {
        this.fuelConsumed = fuelConsumed;
    }

    public String getFuelCost() {
        return fuelCost;
    }

    public void setFuelCost(String fuelCost) {
        this.fuelCost = fuelCost;
    }

    public String getFuelPrice() {
        return fuelPrice;
    }

    public void setFuelPrice(String fuelPrice) {
        this.fuelPrice = fuelPrice;
    }

    public String getIncentiveAmount() {
        return incentiveAmount;
    }

    public void setIncentiveAmount(String incentiveAmount) {
        this.incentiveAmount = incentiveAmount;
    }

    public String getMilleage() {
        return milleage;
    }

    public void setMilleage(String milleage) {
        this.milleage = milleage;
    }

    public String getReeferConsumption() {
        return reeferConsumption;
    }

    public void setReeferConsumption(String reeferConsumption) {
        this.reeferConsumption = reeferConsumption;
    }

    public String getRouteExpense() {
        return routeExpense;
    }

    public void setRouteExpense(String routeExpense) {
        this.routeExpense = routeExpense;
    }

    public String getRouteExpenses() {
        return routeExpenses;
    }

    public void setRouteExpenses(String routeExpenses) {
        this.routeExpenses = routeExpenses;
    }

    public String getTollAmount() {
        return tollAmount;
    }

    public void setTollAmount(String tollAmount) {
        this.tollAmount = tollAmount;
    }

    public String getTollCost() {
        return tollCost;
    }

    public void setTollCost(String tollCost) {
        this.tollCost = tollCost;
    }

    public String getTotalExpenses() {
        return totalExpenses;
    }

    public void setTotalExpenses(String totalExpenses) {
        this.totalExpenses = totalExpenses;
    }

    public String getTotalMins() {
        return totalMins;
    }

    public void setTotalMins(String totalMins) {
        this.totalMins = totalMins;
    }

    public String getTotalRefeerHours() {
        return totalRefeerHours;
    }

    public void setTotalRefeerHours(String totalRefeerHours) {
        this.totalRefeerHours = totalRefeerHours;
    }

    public String getTotalRefeerMinutes() {
        return totalRefeerMinutes;
    }

    public void setTotalRefeerMinutes(String totalRefeerMinutes) {
        this.totalRefeerMinutes = totalRefeerMinutes;
    }

    public String getTotalRumKM() {
        return totalRumKM;
    }

    public void setTotalRumKM(String totalRumKM) {
        this.totalRumKM = totalRumKM;
    }

    public String getTripclosureid() {
        return tripclosureid;
    }

    public void setTripclosureid(String tripclosureid) {
        this.tripclosureid = tripclosureid;
    }

    public String getPreStartLocationPlanTime() {
        return preStartLocationPlanTime;
    }

    public void setPreStartLocationPlanTime(String preStartLocationPlanTime) {
        this.preStartLocationPlanTime = preStartLocationPlanTime;
    }

    public String getTripPlanEndDate() {
        return tripPlanEndDate;
    }

    public void setTripPlanEndDate(String tripPlanEndDate) {
        this.tripPlanEndDate = tripPlanEndDate;
    }

    public String getTripPlanEndTime() {
        return tripPlanEndTime;
    }

    public void setTripPlanEndTime(String tripPlanEndTime) {
        this.tripPlanEndTime = tripPlanEndTime;
    }

    public String getEndHm() {
        return endHm;
    }

    public void setEndHm(String endHm) {
        this.endHm = endHm;
    }

    public String getEndKm() {
        return endKm;
    }

    public void setEndKm(String endKm) {
        this.endKm = endKm;
    }

    public String getRunHm() {
        return runHm;
    }

    public void setRunHm(String runHm) {
        this.runHm = runHm;
    }

    public String getRunKm() {
        return runKm;
    }

    public void setRunKm(String runKm) {
        this.runKm = runKm;
    }

    public String getStartHm() {
        return startHm;
    }

    public void setStartHm(String startHm) {
        this.startHm = startHm;
    }

    public String getStartKm() {
        return startKm;
    }

    public void setStartKm(String startKm) {
        this.startKm = startKm;
    }

    public String getTripEndDate() {
        return tripEndDate;
    }

    public void setTripEndDate(String tripEndDate) {
        this.tripEndDate = tripEndDate;
    }

    public String getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(String tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String[] getTripIds() {
        return tripIds;
    }

    public void setTripIds(String[] tripIds) {
        this.tripIds = tripIds;
    }

    public String getExpenseToBeBilledToCustomer() {
        return expenseToBeBilledToCustomer;
    }

    public void setExpenseToBeBilledToCustomer(String expenseToBeBilledToCustomer) {
        this.expenseToBeBilledToCustomer = expenseToBeBilledToCustomer;
    }

    public String getMarginValue() {
        return marginValue;
    }

    public void setMarginValue(String marginValue) {
        this.marginValue = marginValue;
    }

    public String getPassThroughStatus() {
        return passThroughStatus;
    }

    public void setPassThroughStatus(String passThroughStatus) {
        this.passThroughStatus = passThroughStatus;
    }

    public String getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getTotalRevenue() {
        return totalRevenue;
    }

    public void setTotalRevenue(String totalRevenue) {
        this.totalRevenue = totalRevenue;
    }

    public String getTotalExpToBeBilled() {
        return totalExpToBeBilled;
    }

    public void setTotalExpToBeBilled(String totalExpToBeBilled) {
        this.totalExpToBeBilled = totalExpToBeBilled;
    }

    public String getInvoiceCode() {
        return invoiceCode;
    }

    public void setInvoiceCode(String invoiceCode) {
        this.invoiceCode = invoiceCode;
    }

    public String getNoOfTrips() {
        return noOfTrips;
    }

    public void setNoOfTrips(String noOfTrips) {
        this.noOfTrips = noOfTrips;
    }

    public String getInvoiceDetailId() {
        return invoiceDetailId;
    }

    public void setInvoiceDetailId(String invoiceDetailId) {
        this.invoiceDetailId = invoiceDetailId;
    }

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBillDate() {
        return billDate;
    }

    public void setBillDate(String billDate) {
        this.billDate = billDate;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getFreightAmount() {
        return freightAmount;
    }

    public void setFreightAmount(String freightAmount) {
        this.freightAmount = freightAmount;
    }

    public String getNettValue() {
        return nettValue;
    }

    public void setNettValue(String nettValue) {
        this.nettValue = nettValue;
    }

    public String getTaxValue() {
        return taxValue;
    }

    public void setTaxValue(String taxValue) {
        this.taxValue = taxValue;
    }

    public String getTotalValue() {
        return totalValue;
    }

    public void setTotalValue(String totalValue) {
        this.totalValue = totalValue;
    }

    public double getTaxPercentageD() {
        return taxPercentageD;
    }

    public void setTaxPercentageD(double taxPercentageD) {
        this.taxPercentageD = taxPercentageD;
    }

    public String getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(String productInfo) {
        this.productInfo = productInfo;
    }

    public String getFcExpiryDate() {
        return fcExpiryDate;
    }

    public void setFcExpiryDate(String fcExpiryDate) {
        this.fcExpiryDate = fcExpiryDate;
    }

    public String getInsuranceExpiryDate() {
        return insuranceExpiryDate;
    }

    public void setInsuranceExpiryDate(String insuranceExpiryDate) {
        this.insuranceExpiryDate = insuranceExpiryDate;
    }

    public String getPermitExpiryDate() {
        return permitExpiryDate;
    }

    public void setPermitExpiryDate(String permitExpiryDate) {
        this.permitExpiryDate = permitExpiryDate;
    }

    public String getRoadTaxExpiryDate() {
        return roadTaxExpiryDate;
    }

    public void setRoadTaxExpiryDate(String roadTaxExpiryDate) {
        this.roadTaxExpiryDate = roadTaxExpiryDate;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getTripStatusId() {
        return tripStatusId;
    }

    public void setTripStatusId(String tripStatusId) {
        this.tripStatusId = tripStatusId;
    }

    public String getFleetCenterId() {
        return fleetCenterId;
    }

    public void setFleetCenterId(String fleetCenterId) {
        this.fleetCenterId = fleetCenterId;
    }

    public String getBookedExpense() {
        return bookedExpense;
    }

    public void setBookedExpense(String bookedExpense) {
        this.bookedExpense = bookedExpense;
    }

    public String getDieselCost() {
        return dieselCost;
    }

    public void setDieselCost(String dieselCost) {
        this.dieselCost = dieselCost;
    }

    public String getDieselUsed() {
        return dieselUsed;
    }

    public void setDieselUsed(String dieselUsed) {
        this.dieselUsed = dieselUsed;
    }

    public String getDriverBattaPerDay() {
        return driverBattaPerDay;
    }

    public void setDriverBattaPerDay(String driverBattaPerDay) {
        this.driverBattaPerDay = driverBattaPerDay;
    }

    public String getDriverIncentivePerKm() {
        return driverIncentivePerKm;
    }

    public void setDriverIncentivePerKm(String driverIncentivePerKm) {
        this.driverIncentivePerKm = driverIncentivePerKm;
    }

    public String getReeferMileage() {
        return reeferMileage;
    }

    public void setReeferMileage(String reeferMileage) {
        this.reeferMileage = reeferMileage;
    }

    public String getTollRate() {
        return tollRate;
    }

    public void setTollRate(String tollRate) {
        this.tollRate = tollRate;
    }

    public String getSystemExpense() {
        return systemExpense;
    }

    public void setSystemExpense(String systemExpense) {
        this.systemExpense = systemExpense;
    }

    public String getApprovalRemarks() {
        return approvalRemarks;
    }

    public void setApprovalRemarks(String approvalRemarks) {
        this.approvalRemarks = approvalRemarks;
    }

    public String getApprovalRequestBy() {
        return approvalRequestBy;
    }

    public void setApprovalRequestBy(String approvalRequestBy) {
        this.approvalRequestBy = approvalRequestBy;
    }

    public String getApprovalRequestOn() {
        return approvalRequestOn;
    }

    public void setApprovalRequestOn(String approvalRequestOn) {
        this.approvalRequestOn = approvalRequestOn;
    }

    public String getApprovalRequestRemarks() {
        return approvalRequestRemarks;
    }

    public void setApprovalRequestRemarks(String approvalRequestRemarks) {
        this.approvalRequestRemarks = approvalRequestRemarks;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(String approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getApprovedOn() {
        return approvedOn;
    }

    public void setApprovedOn(String approvedOn) {
        this.approvedOn = approvedOn;
    }

    public String getNettExpense() {
        return nettExpense;
    }

    public void setNettExpense(String nettExpense) {
        this.nettExpense = nettExpense;
    }

    public String getOtherExpense() {
        return otherExpense;
    }

    public void setOtherExpense(String otherExpense) {
        this.otherExpense = otherExpense;
    }

    public String getRcmExpense() {
        return rcmExpense;
    }

    public void setRcmExpense(String rcmExpense) {
        this.rcmExpense = rcmExpense;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getArticleCode() {
        return articleCode;
    }

    public void setArticleCode(String articleCode) {
        this.articleCode = articleCode;
    }

    public String getArticleName() {
        return articleName;
    }

    public void setArticleName(String articleName) {
        this.articleName = articleName;
    }

    public String getBatch() {
        return batch;
    }

    public void setBatch(String batch) {
        this.batch = batch;
    }

    public String getConsignmentId() {
        return consignmentId;
    }

    public void setConsignmentId(String consignmentId) {
        this.consignmentId = consignmentId;
    }

    public String[] getLoadedpackages() {
        return loadedpackages;
    }

    public void setLoadedpackages(String[] loadedpackages) {
        this.loadedpackages = loadedpackages;
    }

    public String getLoadpackageNos() {
        return loadpackageNos;
    }

    public void setLoadpackageNos(String loadpackageNos) {
        this.loadpackageNos = loadpackageNos;
    }

    public String getPackageNos() {
        return packageNos;
    }

    public void setPackageNos(String packageNos) {
        this.packageNos = packageNos;
    }

    public String getPackageWeight() {
        return packageWeight;
    }

    public void setPackageWeight(String packageWeight) {
        this.packageWeight = packageWeight;
    }

    public String[] getPackagesNos() {
        return packagesNos;
    }

    public void setPackagesNos(String[] packagesNos) {
        this.packagesNos = packagesNos;
    }

    public String[] getProductCodes() {
        return productCodes;
    }

    public void setProductCodes(String[] productCodes) {
        this.productCodes = productCodes;
    }

    public String[] getProductNames() {
        return productNames;
    }

    public void setProductNames(String[] productNames) {
        this.productNames = productNames;
    }

    public String[] getProductbatch() {
        return productbatch;
    }

    public void setProductbatch(String[] productbatch) {
        this.productbatch = productbatch;
    }

    public String[] getProductuom() {
        return productuom;
    }

    public void setProductuom(String[] productuom) {
        this.productuom = productuom;
    }

    public String[] getShortage() {
        return shortage;
    }

    public void setShortage(String[] shortage) {
        this.shortage = shortage;
    }

    public String[] getTripArticleId() {
        return tripArticleId;
    }

    public void setTripArticleId(String[] tripArticleId) {
        this.tripArticleId = tripArticleId;
    }

    public String getTripArticleid() {
        return tripArticleid;
    }

    public void setTripArticleid(String tripArticleid) {
        this.tripArticleid = tripArticleid;
    }

    public String[] getUnloadedpackages() {
        return unloadedpackages;
    }

    public void setUnloadedpackages(String[] unloadedpackages) {
        this.unloadedpackages = unloadedpackages;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getVehicleactreportdate() {
        return vehicleactreportdate;
    }

    public void setVehicleactreportdate(String vehicleactreportdate) {
        this.vehicleactreportdate = vehicleactreportdate;
    }

    public String getVehicleactreporthour() {
        return vehicleactreporthour;
    }

    public void setVehicleactreporthour(String vehicleactreporthour) {
        this.vehicleactreporthour = vehicleactreporthour;
    }

    public String getVehicleactreportmin() {
        return vehicleactreportmin;
    }

    public void setVehicleactreportmin(String vehicleactreportmin) {
        this.vehicleactreportmin = vehicleactreportmin;
    }

    public String getVehicleloadreportdate() {
        return vehicleloadreportdate;
    }

    public void setVehicleloadreportdate(String vehicleloadreportdate) {
        this.vehicleloadreportdate = vehicleloadreportdate;
    }

    public String getVehicleloadreporthour() {
        return vehicleloadreporthour;
    }

    public void setVehicleloadreporthour(String vehicleloadreporthour) {
        this.vehicleloadreporthour = vehicleloadreporthour;
    }

    public String getVehicleloadreportmin() {
        return vehicleloadreportmin;
    }

    public void setVehicleloadreportmin(String vehicleloadreportmin) {
        this.vehicleloadreportmin = vehicleloadreportmin;
    }

    public String getVehicleloadtemperature() {
        return vehicleloadtemperature;
    }

    public void setVehicleloadtemperature(String vehicleloadtemperature) {
        this.vehicleloadtemperature = vehicleloadtemperature;
    }

    public String[] getWeights() {
        return weights;
    }

    public void setWeights(String[] weights) {
        this.weights = weights;
    }

    public String getRequeston() {
        return requeston;
    }

    public void setRequeston(String requeston) {
        this.requeston = requeston;
    }

    public String getRequestremarks() {
        return requestremarks;
    }

    public void setRequestremarks(String requestremarks) {
        this.requestremarks = requestremarks;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getTotalRumHM() {
        return totalRumHM;
    }

    public void setTotalRumHM(String totalRumHM) {
        this.totalRumHM = totalRumHM;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getSmtp() {
        return smtp;
    }

    public void setSmtp(String smtp) {
        this.smtp = smtp;
    }

    public String getTomailId() {
        return tomailId;
    }

    public void setTomailId(String tomailId) {
        this.tomailId = tomailId;
    }

    public String getMiscValue() {
        return miscValue;
    }

    public void setMiscValue(String miscValue) {
        this.miscValue = miscValue;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getFcHeadEmail() {
        return fcHeadEmail;
    }

    public void setFcHeadEmail(String fcHeadEmail) {
        this.fcHeadEmail = fcHeadEmail;
    }

    public String getUomId() {
        return uomId;
    }

    public void setUomId(String uomId) {
        this.uomId = uomId;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public String getStakeHolderId() {
        return stakeHolderId;
    }

    public void setStakeHolderId(String stakeHolderId) {
        this.stakeHolderId = stakeHolderId;
    }

    public String getcNotesEmail() {
        return cNotesEmail;
    }

    public void setcNotesEmail(String cNotesEmail) {
        this.cNotesEmail = cNotesEmail;
    }

    public String getCustomerNameEmail() {
        return customerNameEmail;
    }

    public void setCustomerNameEmail(String customerNameEmail) {
        this.customerNameEmail = customerNameEmail;
    }

    public String getRouteInfoEmail() {
        return routeInfoEmail;
    }

    public void setRouteInfoEmail(String routeInfoEmail) {
        this.routeInfoEmail = routeInfoEmail;
    }

    public String getTripCodeEmail() {
        return tripCodeEmail;
    }

    public void setTripCodeEmail(String tripCodeEmail) {
        this.tripCodeEmail = tripCodeEmail;
    }

    public String getVehicleNoEmail() {
        return vehicleNoEmail;
    }

    public void setVehicleNoEmail(String vehicleNoEmail) {
        this.vehicleNoEmail = vehicleNoEmail;
    }

    public String getAdvanceAmount() {
        return advanceAmount;
    }

    public void setAdvanceAmount(String advanceAmount) {
        this.advanceAmount = advanceAmount;
    }

    public String getAdvanceRemarks() {
        return advanceRemarks;
    }

    public void setAdvanceRemarks(String advanceRemarks) {
        this.advanceRemarks = advanceRemarks;
    }

    public String getSecondaryDriverIdOne() {
        return secondaryDriverIdOne;
    }

    public void setSecondaryDriverIdOne(String secondaryDriverIdOne) {
        this.secondaryDriverIdOne = secondaryDriverIdOne;
    }

    public String getSecondaryDriverIdTwo() {
        return secondaryDriverIdTwo;
    }

    public void setSecondaryDriverIdTwo(String secondaryDriverIdTwo) {
        this.secondaryDriverIdTwo = secondaryDriverIdTwo;
    }

    public String getSecondaryDriverNameOne() {
        return secondaryDriverNameOne;
    }

    public void setSecondaryDriverNameOne(String secondaryDriverNameOne) {
        this.secondaryDriverNameOne = secondaryDriverNameOne;
    }

    public String getSecondaryDriverNameTwo() {
        return secondaryDriverNameTwo;
    }

    public void setSecondaryDriverNameTwo(String secondaryDriverNameTwo) {
        this.secondaryDriverNameTwo = secondaryDriverNameTwo;
    }

    public String getZoneName() {
        return ZoneName;
    }

    public void setZoneName(String ZoneName) {
        this.ZoneName = ZoneName;
    }

    public String getEndReportingDate() {
        return endReportingDate;
    }

    public void setEndReportingDate(String endReportingDate) {
        this.endReportingDate = endReportingDate;
    }

    public String getEndReportingTime() {
        return endReportingTime;
    }

    public void setEndReportingTime(String endReportingTime) {
        this.endReportingTime = endReportingTime;
    }

    public String getLoadingDate() {
        return loadingDate;
    }

    public void setLoadingDate(String loadingDate) {
        this.loadingDate = loadingDate;
    }

    public String getLoadingTemperature() {
        return loadingTemperature;
    }

    public void setLoadingTemperature(String loadingTemperature) {
        this.loadingTemperature = loadingTemperature;
    }

    public String getLoadingTime() {
        return loadingTime;
    }

    public void setLoadingTime(String loadingTime) {
        this.loadingTime = loadingTime;
    }

    public String getLrNumber() {
        return lrNumber;
    }

    public void setLrNumber(String lrNumber) {
        this.lrNumber = lrNumber;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getStartReportingDate() {
        return startReportingDate;
    }

    public void setStartReportingDate(String startReportingDate) {
        this.startReportingDate = startReportingDate;
    }

    public String getStartReportingTime() {
        return startReportingTime;
    }

    public void setStartReportingTime(String startReportingTime) {
        this.startReportingTime = startReportingTime;
    }

    public String getTripBalance() {
        return tripBalance;
    }

    public void setTripBalance(String tripBalance) {
        this.tripBalance = tripBalance;
    }

    public String getTripEndBalance() {
        return tripEndBalance;
    }

    public void setTripEndBalance(String tripEndBalance) {
        this.tripEndBalance = tripEndBalance;
    }

    public String getTripStartingBalance() {
        return tripStartingBalance;
    }

    public void setTripStartingBalance(String tripStartingBalance) {
        this.tripStartingBalance = tripStartingBalance;
    }

    public String getUnLoadingDate() {
        return unLoadingDate;
    }

    public void setUnLoadingDate(String unLoadingDate) {
        this.unLoadingDate = unLoadingDate;
    }

    public String getUnLoadingTemperature() {
        return unLoadingTemperature;
    }

    public void setUnLoadingTemperature(String unLoadingTemperature) {
        this.unLoadingTemperature = unLoadingTemperature;
    }

    public String getUnLoadingTime() {
        return unLoadingTime;
    }

    public void setUnLoadingTime(String unLoadingTime) {
        this.unLoadingTime = unLoadingTime;
    }

    public String getZoneId() {
        return zoneId;
    }

    public void setZoneId(String zoneId) {
        this.zoneId = zoneId;
    }

    public String getCityFromId() {
        return cityFromId;
    }

    public void setCityFromId(String cityFromId) {
        this.cityFromId = cityFromId;
    }

    public String getCurrentTemperature() {
        return currentTemperature;
    }

    public void setCurrentTemperature(String currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    public String getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(String expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public String getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(String expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public String getFleetCenterName() {
        return fleetCenterName;
    }

    public void setFleetCenterName(String fleetCenterName) {
        this.fleetCenterName = fleetCenterName;
    }

    public String getFleetCenterNo() {
        return fleetCenterNo;
    }

    public void setFleetCenterNo(String fleetCenterNo) {
        this.fleetCenterNo = fleetCenterNo;
    }

    public String getGpsLocation() {
        return gpsLocation;
    }

    public void setGpsLocation(String gpsLocation) {
        this.gpsLocation = gpsLocation;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(String accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getAmoutBalance() {
        return amoutBalance;
    }

    public void setAmoutBalance(String amoutBalance) {
        this.amoutBalance = amoutBalance;
    }

    public String getBpclTransactionId() {
        return bpclTransactionId;
    }

    public void setBpclTransactionId(String bpclTransactionId) {
        this.bpclTransactionId = bpclTransactionId;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDealerCity() {
        return dealerCity;
    }

    public void setDealerCity(String dealerCity) {
        this.dealerCity = dealerCity;
    }

    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    public String getOdometerReading() {
        return odometerReading;
    }

    public void setOdometerReading(String odometerReading) {
        this.odometerReading = odometerReading;
    }

    public String getPetromilesEarned() {
        return petromilesEarned;
    }

    public void setPetromilesEarned(String petromilesEarned) {
        this.petromilesEarned = petromilesEarned;
    }

    public String getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(String transactionDate) {
        this.transactionDate = transactionDate;
    }

    public String getTransactionHistoryId() {
        return transactionHistoryId;
    }

    public void setTransactionHistoryId(String transactionHistoryId) {
        this.transactionHistoryId = transactionHistoryId;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public String getVolumeDocNo() {
        return volumeDocNo;
    }

    public void setVolumeDocNo(String volumeDocNo) {
        this.volumeDocNo = volumeDocNo;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneeMobileNo() {
        return consigneeMobileNo;
    }

    public void setConsigneeMobileNo(String consigneeMobileNo) {
        this.consigneeMobileNo = consigneeMobileNo;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorMobileNo() {
        return consignorMobileNo;
    }

    public void setConsignorMobileNo(String consignorMobileNo) {
        this.consignorMobileNo = consignorMobileNo;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getPodCount() {
        return podCount;
    }

    public void setPodCount(String podCount) {
        this.podCount = podCount;
    }

    public String getPaidStatus() {
        return paidStatus;
    }

    public void setPaidStatus(String paidStatus) {
        this.paidStatus = paidStatus;
    }

    public String getEstimatedKM() {
        return estimatedKM;
    }

    public void setEstimatedKM(String estimatedKM) {
        this.estimatedKM = estimatedKM;
    }

    public String getCustomerOrderReferenceNo() {
        return customerOrderReferenceNo;
    }

    public void setCustomerOrderReferenceNo(String customerOrderReferenceNo) {
        this.customerOrderReferenceNo = customerOrderReferenceNo;
    }

    public String getAdvanceToPayStatus() {
        return advanceToPayStatus;
    }

    public void setAdvanceToPayStatus(String advanceToPayStatus) {
        this.advanceToPayStatus = advanceToPayStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getToPayStatus() {
        return toPayStatus;
    }

    public void setToPayStatus(String toPayStatus) {
        this.toPayStatus = toPayStatus;
    }

    public String getPodStatus() {
        return podStatus;
    }

    public void setPodStatus(String podStatus) {
        this.podStatus = podStatus;
    }

    public String getWfuRemarks() {
        return wfuRemarks;
    }

    public void setWfuRemarks(String wfuRemarks) {
        this.wfuRemarks = wfuRemarks;
    }

    public String getWfuUnloadingDate() {
        return wfuUnloadingDate;
    }

    public void setWfuUnloadingDate(String wfuUnloadingDate) {
        this.wfuUnloadingDate = wfuUnloadingDate;
    }

    public String getWfuUnloadingTime() {
        return wfuUnloadingTime;
    }

    public void setWfuUnloadingTime(String wfuUnloadingTime) {
        this.wfuUnloadingTime = wfuUnloadingTime;
    }

    public String getTripStartDate() {
        return tripStartDate;
    }

    public void setTripStartDate(String tripStartDate) {
        this.tripStartDate = tripStartDate;
    }

    public String getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(String tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getEmptyTripApprovalStatus() {
        return emptyTripApprovalStatus;
    }

    public void setEmptyTripApprovalStatus(String emptyTripApprovalStatus) {
        this.emptyTripApprovalStatus = emptyTripApprovalStatus;
    }

    public int getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(int routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getActualAdvancePaid() {
        return actualAdvancePaid;
    }

    public void setActualAdvancePaid(String actualAdvancePaid) {
        this.actualAdvancePaid = actualAdvancePaid;
    }

    public String getSecondaryAddlTollAmount() {
        return secondaryAddlTollAmount;
    }

    public void setSecondaryAddlTollAmount(String secondaryAddlTollAmount) {
        this.secondaryAddlTollAmount = secondaryAddlTollAmount;
    }

    public String getSecondaryMiscAmount() {
        return secondaryMiscAmount;
    }

    public void setSecondaryMiscAmount(String secondaryMiscAmount) {
        this.secondaryMiscAmount = secondaryMiscAmount;
    }

    public String getSecondaryTollAmount() {
        return secondaryTollAmount;
    }

    public void setSecondaryTollAmount(String secondaryTollAmount) {
        this.secondaryTollAmount = secondaryTollAmount;
    }

    public String getRouteNameStatus() {
        return routeNameStatus;
    }

    public void setRouteNameStatus(String routeNameStatus) {
        this.routeNameStatus = routeNameStatus;
    }

    public String getGpsKm() {
        return gpsKm;
    }

    public void setGpsKm(String gpsKm) {
        this.gpsKm = gpsKm;
    }

    public String getGpsHm() {
        return gpsHm;
    }

    public void setGpsHm(String gpsHm) {
        this.gpsHm = gpsHm;
    }

    public String getTempLogFile() {
        return tempLogFile;
    }

    public void setTempLogFile(String tempLogFile) {
        this.tempLogFile = tempLogFile;
    }

    public String getConsignmentDate() {
        return consignmentDate;
    }

    public void setConsignmentDate(String consignmentDate) {
        this.consignmentDate = consignmentDate;
    }

    public String getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(String currentLocation) {
        this.currentLocation = currentLocation;
    }

    public String getDriverMobile() {
        return driverMobile;
    }

    public void setDriverMobile(String driverMobile) {
        this.driverMobile = driverMobile;
    }

    public String getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(String paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public void setPaymentTypeId(String paymentTypeId) {
        this.paymentTypeId = paymentTypeId;
    }

    public String getTotalPackages() {
        return totalPackages;
    }

    public void setTotalPackages(String totalPackages) {
        this.totalPackages = totalPackages;
    }

    public String getVehicleRequiredDateTime() {
        return vehicleRequiredDateTime;
    }

    public void setVehicleRequiredDateTime(String vehicleRequiredDateTime) {
        this.vehicleRequiredDateTime = vehicleRequiredDateTime;
    }

    public String getVehicleDriverAdvanceId() {
        return vehicleDriverAdvanceId;
    }

    public void setVehicleDriverAdvanceId(String vehicleDriverAdvanceId) {
        this.vehicleDriverAdvanceId = vehicleDriverAdvanceId;
    }

    public String getTripCountStatus() {
        return tripCountStatus;
    }

    public void setTripCountStatus(String tripCountStatus) {
        this.tripCountStatus = tripCountStatus;
    }

    public String getDriverCount() {
        return driverCount;
    }

    public void setDriverCount(String driverCount) {
        this.driverCount = driverCount;
    }

    public String getExtraExpenseValue() {
        return extraExpenseValue;
    }

    public void setExtraExpenseValue(String extraExpenseValue) {
        this.extraExpenseValue = extraExpenseValue;
    }

    public String getEmailCc() {
        return emailCc;
    }

    public void setEmailCc(String emailCc) {
        this.emailCc = emailCc;
    }

    public String getInfoEmailTo() {
        return infoEmailTo;
    }

    public void setInfoEmailTo(String infoEmailTo) {
        this.infoEmailTo = infoEmailTo;
    }

    public String getInfoEmailCc() {
        return infoEmailCc;
    }

    public void setInfoEmailCc(String infoEmailCc) {
        this.infoEmailCc = infoEmailCc;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getRatePerKm() {
        return ratePerKm;
    }

    public void setRatePerKm(String ratePerKm) {
        this.ratePerKm = ratePerKm;
    }

    public String getTripGraphId() {
        return tripGraphId;
    }

    public void setTripGraphId(String tripGraphId) {
        this.tripGraphId = tripGraphId;
    }

    public String getTempApprovalStatus() {
        return tempApprovalStatus;
    }

    public void setTempApprovalStatus(String tempApprovalStatus) {
        this.tempApprovalStatus = tempApprovalStatus;
    }

    public String getVehicleDieselUsed() {
        return vehicleDieselUsed;
    }

    public void setVehicleDieselUsed(String vehicleDieselUsed) {
        this.vehicleDieselUsed = vehicleDieselUsed;
    }

    public String getReeferDieselUsed() {
        return reeferDieselUsed;
    }

    public void setReeferDieselUsed(String reeferDieselUsed) {
        this.reeferDieselUsed = reeferDieselUsed;
    }

    public String getClosureTotalExpense() {
        return closureTotalExpense;
    }

    public void setClosureTotalExpense(String closureTotalExpense) {
        this.closureTotalExpense = closureTotalExpense;
    }

    public String getBillingNameAddress() {
        return billingNameAddress;
    }

    public void setBillingNameAddress(String billingNameAddress) {
        this.billingNameAddress = billingNameAddress;
    }

    public String getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    public String getBilledAmount() {
        return billedAmount;
    }

    public void setBilledAmount(String billedAmount) {
        this.billedAmount = billedAmount;
    }

    public String getEditMode() {
        return editMode;
    }

    public void setEditMode(String editMode) {
        this.editMode = editMode;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRatePerKg() {
        return ratePerKg;
    }

    public void setRatePerKg(String ratePerKg) {
        this.ratePerKg = ratePerKg;
    }

    public String getCourierRemarks() {
        return courierRemarks;
    }

    public void setCourierRemarks(String courierRemarks) {
        this.courierRemarks = courierRemarks;
    }

    public String getCourierNo() {
        return courierNo;
    }

    public void setCourierNo(String courierNo) {
        this.courierNo = courierNo;
    }

    public String getLogDateTime() {
        return logDateTime;
    }

    public void setLogDateTime(String logDateTime) {
        this.logDateTime = logDateTime;
    }

    public String getLogDate() {
        return logDate;
    }

    public void setLogDate(String logDate) {
        this.logDate = logDate;
    }

    public String getLogTime() {
        return logTime;
    }

    public void setLogTime(String logTime) {
        this.logTime = logTime;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getFuelTypeId() {
        return fuelTypeId;
    }

    public void setFuelTypeId(String fuelTypeId) {
        this.fuelTypeId = fuelTypeId;
    }

    public String getReturnAmount() {
        return returnAmount;
    }

    public void setReturnAmount(String returnAmount) {
        this.returnAmount = returnAmount;
    }

    public String getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(String balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getLicenceNo() {
        return licenceNo;
    }

    public void setLicenceNo(String licenceNo) {
        this.licenceNo = licenceNo;
    }

    public String getLicenceDate() {
        return licenceDate;
    }

    public void setLicenceDate(String licenceDate) {
        this.licenceDate = licenceDate;
    }

    public String getEmployeeCode() {
        return employeeCode;
    }

    public void setEmployeeCode(String employeeCode) {
        this.employeeCode = employeeCode;
    }

    public String getJoiningDate() {
        return joiningDate;
    }

    public void setJoiningDate(String joiningDate) {
        this.joiningDate = joiningDate;
    }

    public String getLastVehicleEndKm() {
        return lastVehicleEndKm;
    }

    public void setLastVehicleEndKm(String lastVehicleEndKm) {
        this.lastVehicleEndKm = lastVehicleEndKm;
    }

    public String getVehicleStartKm() {
        return vehicleStartKm;
    }

    public void setVehicleStartKm(String vehicleStartKm) {
        this.vehicleStartKm = vehicleStartKm;
    }

    public String getOldVehicleNo() {
        return oldVehicleNo;
    }

    public void setOldVehicleNo(String oldVehicleNo) {
        this.oldVehicleNo = oldVehicleNo;
    }

    public String getOldDriverName() {
        return oldDriverName;
    }

    public void setOldDriverName(String oldDriverName) {
        this.oldDriverName = oldDriverName;
    }

    public String getVehicleRemarks() {
        return vehicleRemarks;
    }

    public void setVehicleRemarks(String vehicleRemarks) {
        this.vehicleRemarks = vehicleRemarks;
    }

    public String getVehicleChangeDate() {
        return vehicleChangeDate;
    }

    public void setVehicleChangeDate(String vehicleChangeDate) {
        this.vehicleChangeDate = vehicleChangeDate;
    }

    public String getVehicleChangeMinute() {
        return vehicleChangeMinute;
    }

    public void setVehicleChangeMinute(String vehicleChangeMinute) {
        this.vehicleChangeMinute = vehicleChangeMinute;
    }

    public String getVehicleChangeHour() {
        return vehicleChangeHour;
    }

    public void setVehicleChangeHour(String vehicleChangeHour) {
        this.vehicleChangeHour = vehicleChangeHour;
    }

    public String getLastVehicleEndOdometer() {
        return lastVehicleEndOdometer;
    }

    public void setLastVehicleEndOdometer(String lastVehicleEndOdometer) {
        this.lastVehicleEndOdometer = lastVehicleEndOdometer;
    }

    public String getVehicleStartOdometer() {
        return vehicleStartOdometer;
    }

    public void setVehicleStartOdometer(String vehicleStartOdometer) {
        this.vehicleStartOdometer = vehicleStartOdometer;
    }

    public String getEmptyTrip() {
        return emptyTrip;
    }

    public void setEmptyTrip(String emptyTrip) {
        this.emptyTrip = emptyTrip;
    }

    public String getBusinessType() {
        return businessType;
    }

    public void setBusinessType(String businessType) {
        this.businessType = businessType;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getMailIdBcc() {
        return mailIdBcc;
    }

    public void setMailIdBcc(String mailIdBcc) {
        this.mailIdBcc = mailIdBcc;
    }

    public String getMailIdCc() {
        return mailIdCc;
    }

    public void setMailIdCc(String mailIdCc) {
        this.mailIdCc = mailIdCc;
    }

    public String getMailIdTo() {
        return mailIdTo;
    }

    public void setMailIdTo(String mailIdTo) {
        this.mailIdTo = mailIdTo;
    }

    public String getMailContentBcc() {
        return mailContentBcc;
    }

    public void setMailContentBcc(String mailContentBcc) {
        this.mailContentBcc = mailContentBcc;
    }

    public String getMailContentCc() {
        return mailContentCc;
    }

    public void setMailContentCc(String mailContentCc) {
        this.mailContentCc = mailContentCc;
    }

    public String getMailContentTo() {
        return mailContentTo;
    }

    public void setMailContentTo(String mailContentTo) {
        this.mailContentTo = mailContentTo;
    }

    public String getMailSubjectBcc() {
        return mailSubjectBcc;
    }

    public void setMailSubjectBcc(String mailSubjectBcc) {
        this.mailSubjectBcc = mailSubjectBcc;
    }

    public String getMailSubjectCc() {
        return mailSubjectCc;
    }

    public void setMailSubjectCc(String mailSubjectCc) {
        this.mailSubjectCc = mailSubjectCc;
    }

    public String getMailSubjectTo() {
        return mailSubjectTo;
    }

    public void setMailSubjectTo(String mailSubjectTo) {
        this.mailSubjectTo = mailSubjectTo;
    }

    public String getMailTypeId() {
        return mailTypeId;
    }

    public void setMailTypeId(String mailTypeId) {
        this.mailTypeId = mailTypeId;
    }

    public String getJobcardId() {
        return jobcardId;
    }

    public void setJobcardId(String jobcardId) {
        this.jobcardId = jobcardId;
    }

    public String getMailDate() {
        return mailDate;
    }

    public void setMailDate(String mailDate) {
        this.mailDate = mailDate;
    }

    public String getStartedBy() {
        return startedBy;
    }

    public void setStartedBy(String startedBy) {
        this.startedBy = startedBy;
    }

    public String getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(String endedBy) {
        this.endedBy = endedBy;
    }

    public String getExtraExpenseStatus() {
        return extraExpenseStatus;
    }

    public void setExtraExpenseStatus(String extraExpenseStatus) {
        this.extraExpenseStatus = extraExpenseStatus;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getOutStandingAmount() {
        return outStandingAmount;
    }

    public void setOutStandingAmount(String outStandingAmount) {
        this.outStandingAmount = outStandingAmount;
    }

    public String getApprovalStatusId() {
        return approvalStatusId;
    }

    public void setApprovalStatusId(String approvalStatusId) {
        this.approvalStatusId = approvalStatusId;
    }

    public String getMailSendingId() {
        return mailSendingId;
    }

    public void setMailSendingId(String mailSendingId) {
        this.mailSendingId = mailSendingId;
    }

    public String getMailDeliveredStatusId() {
        return mailDeliveredStatusId;
    }

    public void setMailDeliveredStatusId(String mailDeliveredStatusId) {
        this.mailDeliveredStatusId = mailDeliveredStatusId;
    }

    public String getCityFrom() {
        return cityFrom;
    }

    public void setCityFrom(String cityFrom) {
        this.cityFrom = cityFrom;
    }

    public String getCityTo() {
        return cityTo;
    }

    public void setCityTo(String cityTo) {
        this.cityTo = cityTo;
    }

    public String getEmptyTripRemarks() {
        return emptyTripRemarks;
    }

    public void setEmptyTripRemarks(String emptyTripRemarks) {
        this.emptyTripRemarks = emptyTripRemarks;
    }

    public String getExpense() {
        return expense;
    }

    public void setExpense(String expense) {
        this.expense = expense;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getSecondaryParkingAmount() {
        return secondaryParkingAmount;
    }

    public void setSecondaryParkingAmount(String secondaryParkingAmount) {
        this.secondaryParkingAmount = secondaryParkingAmount;
    }

    public String getPreColingAmount() {
        return preColingAmount;
    }

    public void setPreColingAmount(String preColingAmount) {
        this.preColingAmount = preColingAmount;
    }

    public String getSecAdditionalTollCost() {
        return secAdditionalTollCost;
    }

    public void setSecAdditionalTollCost(String secAdditionalTollCost) {
        this.secAdditionalTollCost = secAdditionalTollCost;
    }

    public String getUsageTypeId() {
        return usageTypeId;
    }

    public void setUsageTypeId(String usageTypeId) {
        this.usageTypeId = usageTypeId;
    }

    public String getTripWfuDate() {
        return tripWfuDate;
    }

    public void setTripWfuDate(String tripWfuDate) {
        this.tripWfuDate = tripWfuDate;
    }

    public String getTripWfuTime() {
        return tripWfuTime;
    }

    public void setTripWfuTime(String tripWfuTime) {
        this.tripWfuTime = tripWfuTime;
    }

    public String getTripAdvanceId() {
        return tripAdvanceId;
    }

    public void setTripAdvanceId(String tripAdvanceId) {
        this.tripAdvanceId = tripAdvanceId;
    }

    public String getDestinationCityName() {
        return destinationCityName;
    }

    public void setDestinationCityName(String destinationCityName) {
        this.destinationCityName = destinationCityName;
    }

    public String getOriginCityName() {
        return originCityName;
    }

    public void setOriginCityName(String originCityName) {
        this.originCityName = originCityName;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getTotalHm() {
        return totalHm;
    }

    public void setTotalHm(String totalHm) {
        this.totalHm = totalHm;
    }

    public String getVehicleChangeCityName() {
        return vehicleChangeCityName;
    }

    public void setVehicleChangeCityName(String vehicleChangeCityName) {
        this.vehicleChangeCityName = vehicleChangeCityName;
    }

    public String getTransitDays1() {
        return transitDays1;
    }

    public void setTransitDays1(String transitDays1) {
        this.transitDays1 = transitDays1;
    }

    public String getTransitDays2() {
        return transitDays2;
    }

    public void setTransitDays2(String transitDays2) {
        this.transitDays2 = transitDays2;
    }

    public String getEmptyTripPurpose() {
        return emptyTripPurpose;
    }

    public void setEmptyTripPurpose(String emptyTripPurpose) {
        this.emptyTripPurpose = emptyTripPurpose;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getRaisedBy() {
        return raisedBy;
    }

    public void setRaisedBy(String raisedBy) {
        this.raisedBy = raisedBy;
    }

    public String getRaisedOn() {
        return raisedOn;
    }

    public void setRaisedOn(String raisedOn) {
        this.raisedOn = raisedOn;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTicketingDetailId() {
        return ticketingDetailId;
    }

    public void setTicketingDetailId(String ticketingDetailId) {
        this.ticketingDetailId = ticketingDetailId;
    }

    public String getTicketingFile() {
        return ticketingFile;
    }

    public void setTicketingFile(String ticketingFile) {
        this.ticketingFile = ticketingFile;
    }

    public String getFollowedBy() {
        return followedBy;
    }

    public void setFollowedBy(String followedBy) {
        this.followedBy = followedBy;
    }

    public String getFollowedOn() {
        return followedOn;
    }

    public void setFollowedOn(String followedOn) {
        this.followedOn = followedOn;
    }

    public String getSetteledHm() {
        return setteledHm;
    }

    public void setSetteledHm(String setteledHm) {
        this.setteledHm = setteledHm;
    }

    public String getSetteledKm() {
        return setteledKm;
    }

    public void setSetteledKm(String setteledKm) {
        this.setteledKm = setteledKm;
    }

    public String getSetteltotalHrs() {
        return setteltotalHrs;
    }

    public void setSetteltotalHrs(String setteltotalHrs) {
        this.setteltotalHrs = setteltotalHrs;
    }

    public String getSetteltotalKM() {
        return setteltotalKM;
    }

    public void setSetteltotalKM(String setteltotalKM) {
        this.setteltotalKM = setteltotalKM;
    }

    public String getOverRideBy() {
        return overRideBy;
    }

    public void setOverRideBy(String overRideBy) {
        this.overRideBy = overRideBy;
    }

    public String getOverRideOn() {
        return overRideOn;
    }

    public void setOverRideOn(String overRideOn) {
        this.overRideOn = overRideOn;
    }

    public String getOverrideTripRemarks() {
        return overrideTripRemarks;
    }

    public void setOverrideTripRemarks(String overrideTripRemarks) {
        this.overrideTripRemarks = overrideTripRemarks;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getNextTrip() {
        return nextTrip;
    }

    public void setNextTrip(String nextTrip) {
        this.nextTrip = nextTrip;
    }

    public String getCurrentTripStartDate() {
        return currentTripStartDate;
    }

    public void setCurrentTripStartDate(String currentTripStartDate) {
        this.currentTripStartDate = currentTripStartDate;
    }

    public String getNextTripCountStatus() {
        return nextTripCountStatus;
    }

    public void setNextTripCountStatus(String nextTripCountStatus) {
        this.nextTripCountStatus = nextTripCountStatus;
    }

    public String getDriverBhatta() {
        return driverBhatta;
    }

    public void setDriverBhatta(String driverBhatta) {
        this.driverBhatta = driverBhatta;
    }

    public String getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(String fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getFuelExpense() {
        return fuelExpense;
    }

    public void setFuelExpense(String fuelExpense) {
        this.fuelExpense = fuelExpense;
    }

    public String getParkingCost() {
        return parkingCost;
    }

    public void setParkingCost(String parkingCost) {
        this.parkingCost = parkingCost;
    }

    public String getSystemExpenses() {
        return systemExpenses;
    }

    public void setSystemExpenses(String systemExpenses) {
        this.systemExpenses = systemExpenses;
    }

    public String getTollExpense() {
        return tollExpense;
    }

    public void setTollExpense(String tollExpense) {
        this.tollExpense = tollExpense;
    }

    public String getVehicleMileage() {
        return vehicleMileage;
    }

    public void setVehicleMileage(String vehicleMileage) {
        this.vehicleMileage = vehicleMileage;
    }

    public String getEndingBalance() {
        return endingBalance;
    }

    public void setEndingBalance(String endingBalance) {
        this.endingBalance = endingBalance;
    }

    public String getAdvanceCode() {
        return advanceCode;
    }

    public void setAdvanceCode(String advanceCode) {
        this.advanceCode = advanceCode;
    }

    public String getBhattaDate() {
        return bhattaDate;
    }

    public void setBhattaDate(String bhattaDate) {
        this.bhattaDate = bhattaDate;
    }

    public String getIdleBhattaId() {
        return idleBhattaId;
    }

    public void setIdleBhattaId(String idleBhattaId) {
        this.idleBhattaId = idleBhattaId;
    }

    public String getVehicleAdvanceId() {
        return vehicleAdvanceId;
    }

    public void setVehicleAdvanceId(String vehicleAdvanceId) {
        this.vehicleAdvanceId = vehicleAdvanceId;
    }

    public String getDriverLastBalance() {
        return driverLastBalance;
    }

    public void setDriverLastBalance(String driverLastBalance) {
        this.driverLastBalance = driverLastBalance;
    }

    public String getTripAmount() {
        return tripAmount;
    }

    public void setTripAmount(String tripAmount) {
        this.tripAmount = tripAmount;
    }

    public String getSettleAmount() {
        return settleAmount;
    }

    public void setSettleAmount(String settleAmount) {
        this.settleAmount = settleAmount;
    }

    public String getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(String payAmount) {
        this.payAmount = payAmount;
    }

    public String getSettlementTripsSize() {
        return settlementTripsSize;
    }

    public void setSettlementTripsSize(String settlementTripsSize) {
        this.settlementTripsSize = settlementTripsSize;
    }

    public String getBhattaAmount() {
        return bhattaAmount;
    }

    public void setBhattaAmount(String bhattaAmount) {
        this.bhattaAmount = bhattaAmount;
    }

    public String getIdleDays() {
        return idleDays;
    }

    public void setIdleDays(String idleDays) {
        this.idleDays = idleDays;
    }

    public String getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(String transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getBillCopyName() {
        return billCopyName;
    }

    public void setBillCopyName(String billCopyName) {
        this.billCopyName = billCopyName;
    }

    public String getDocumentRequired() {
        return documentRequired;
    }

    public void setDocumentRequired(String documentRequired) {
        this.documentRequired = documentRequired;
    }

    public String getDocumentRequiredStatus() {
        return documentRequiredStatus;
    }

    public void setDocumentRequiredStatus(String documentRequiredStatus) {
        this.documentRequiredStatus = documentRequiredStatus;
    }

    public String getBillCopyFile() {
        return billCopyFile;
    }

    public void setBillCopyFile(String billCopyFile) {
        this.billCopyFile = billCopyFile;
    }

    public String getUnclearedAmount() {
        return unclearedAmount;
    }

    public void setUnclearedAmount(String unclearedAmount) {
        this.unclearedAmount = unclearedAmount;
    }

    public String getDriverChangeHour() {
        return driverChangeHour;
    }

    public void setDriverChangeHour(String driverChangeHour) {
        this.driverChangeHour = driverChangeHour;
    }

    public String getDriverChangeMinute() {
        return driverChangeMinute;
    }

    public void setDriverChangeMinute(String driverChangeMinute) {
        this.driverChangeMinute = driverChangeMinute;
    }

    public String getDriverInTrip() {
        return driverInTrip;
    }

    public void setDriverInTrip(String driverInTrip) {
        this.driverInTrip = driverInTrip;
    }

    public String getAdvancePaid() {
        return advancePaid;
    }

    public void setAdvancePaid(String advancePaid) {
        this.advancePaid = advancePaid;
    }

    public String getAdvanceReturn() {
        return advanceReturn;
    }

    public void setAdvanceReturn(String advanceReturn) {
        this.advanceReturn = advanceReturn;
    }

    public String getMiscRate() {
        return miscRate;
    }

    public void setMiscRate(String miscRate) {
        this.miscRate = miscRate;
    }

    public String getExpenseHour() {
        return expenseHour;
    }

    public void setExpenseHour(String expenseHour) {
        this.expenseHour = expenseHour;
    }

    public String getExpenseMinute() {
        return expenseMinute;
    }

    public void setExpenseMinute(String expenseMinute) {
        this.expenseMinute = expenseMinute;
    }

    public String getPreCoolingCost() {
        return preCoolingCost;
    }

    public void setPreCoolingCost(String preCoolingCost) {
        this.preCoolingCost = preCoolingCost;
    }

    public String getTollCostPerKm() {
        return tollCostPerKm;
    }

    public void setTollCostPerKm(String tollCostPerKm) {
        this.tollCostPerKm = tollCostPerKm;
    }

    public String getPendingChallan() {
        return pendingChallan;
    }

    public void setPendingChallan(String pendingChallan) {
        this.pendingChallan = pendingChallan;
    }

    public String getPendingChallanAmount() {
        return pendingChallanAmount;
    }

    public void setPendingChallanAmount(String pendingChallanAmount) {
        this.pendingChallanAmount = pendingChallanAmount;
    }

    public String getTotalDocumentAmount() {
        return totalDocumentAmount;
    }

    public void setTotalDocumentAmount(String totalDocumentAmount) {
        this.totalDocumentAmount = totalDocumentAmount;
    }

    public String getUploadedChallan() {
        return uploadedChallan;
    }

    public void setUploadedChallan(String uploadedChallan) {
        this.uploadedChallan = uploadedChallan;
    }

    public String getUploadedChallanAmount() {
        return uploadedChallanAmount;
    }

    public void setUploadedChallanAmount(String uploadedChallanAmount) {
        this.uploadedChallanAmount = uploadedChallanAmount;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getVehicleRequiredDate() {
        return vehicleRequiredDate;
    }

    public void setVehicleRequiredDate(String vehicleRequiredDate) {
        this.vehicleRequiredDate = vehicleRequiredDate;
    }

    public String getVehicleRequiredTime() {
        return vehicleRequiredTime;
    }

    public void setVehicleRequiredTime(String vehicleRequiredTime) {
        this.vehicleRequiredTime = vehicleRequiredTime;
    }

    public String getConsignmentStatusId() {
        return consignmentStatusId;
    }

    public void setConsignmentStatusId(String consignmentStatusId) {
        this.consignmentStatusId = consignmentStatusId;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    public void setConsignmentStatus(String consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    public String getOrderSize() {
        return orderSize;
    }

    public void setOrderSize(String orderSize) {
        this.orderSize = orderSize;
    }

    public String[] getOrderSequenceNo() {
        return orderSequenceNo;
    }

    public void setOrderSequenceNo(String[] orderSequenceNo) {
        this.orderSequenceNo = orderSequenceNo;
    }

    public String getHubId() {
        return hubId;
    }

    public void setHubId(String hubId) {
        this.hubId = hubId;
    }

    public String getConsginmentOrderStatus() {
        return consginmentOrderStatus;
    }

    public void setConsginmentOrderStatus(String consginmentOrderStatus) {
        this.consginmentOrderStatus = consginmentOrderStatus;
    }

    public String getSeatCapacity() {
        return seatCapacity;
    }

    public void setSeatCapacity(String seatCapacity) {
        this.seatCapacity = seatCapacity;
    }

    public String getTrailerId() {
        return trailerId;
    }

    public void setTrailerId(String trailerId) {
        this.trailerId = trailerId;
    }

    public String getTrailerNo() {
        return trailerNo;
    }

    public void setTrailerNo(String trailerNo) {
        this.trailerNo = trailerNo;
    }

    public String getWareHouseId() {
        return wareHouseId;
    }

    public void setWareHouseId(String wareHouseId) {
        this.wareHouseId = wareHouseId;
    }

    public String getWareHouseName() {
        return wareHouseName;
    }

    public void setWareHouseName(String wareHouseName) {
        this.wareHouseName = wareHouseName;
    }

    public String getDestinationWId() {
        return destinationWId;
    }

    public void setDestinationWId(String destinationWId) {
        this.destinationWId = destinationWId;
    }

    public String getSourceWId() {
        return sourceWId;
    }

    public void setSourceWId(String sourceWId) {
        this.sourceWId = sourceWId;
    }

    public String getEndTrailerKM() {
        return endTrailerKM;
    }

    public void setEndTrailerKM(String endTrailerKM) {
        this.endTrailerKM = endTrailerKM;
    }

    public String getStartTrailerKM() {
        return startTrailerKM;
    }

    public void setStartTrailerKM(String startTrailerKM) {
        this.startTrailerKM = startTrailerKM;
    }

    public String getCurrencyName() {
        return currencyName;
    }

    public void setCurrencyName(String currencyName) {
        this.currencyName = currencyName;
    }

    public String getPortId() {
        return portId;
    }

    public void setPortId(String portId) {
        this.portId = portId;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getTrailerStartKm() {
        return trailerStartKm;
    }

    public void setTrailerStartKm(String trailerStartKm) {
        this.trailerStartKm = trailerStartKm;
    }

    public String getWareHouseAdd() {
        return wareHouseAdd;
    }

    public void setWareHouseAdd(String wareHouseAdd) {
        this.wareHouseAdd = wareHouseAdd;
    }

    public String getPortAddress() {
        return portAddress;
    }

    public void setPortAddress(String portAddress) {
        this.portAddress = portAddress;
    }

    public String getAvailableLimit() {
        return availableLimit;
    }

    public void setAvailableLimit(String availableLimit) {
        this.availableLimit = availableLimit;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getCostPerKm() {
        return costPerKm;
    }

    public void setCostPerKm(String costPerKm) {
        this.costPerKm = costPerKm;
    }

    public String getCountryId() {
        return countryId;
    }

    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    public String getEtcCost() {
        return etcCost;
    }

    public void setEtcCost(String etcCost) {
        this.etcCost = etcCost;
    }

    public String getFuelCostPerKm() {
        return fuelCostPerKm;
    }

    public void setFuelCostPerKm(String fuelCostPerKm) {
        this.fuelCostPerKm = fuelCostPerKm;
    }

    public String getMiscCostKm() {
        return miscCostKm;
    }

    public void setMiscCostKm(String miscCostKm) {
        this.miscCostKm = miscCostKm;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getDeattchLoc() {
        return deattchLoc;
    }

    public void setDeattchLoc(String deattchLoc) {
        this.deattchLoc = deattchLoc;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getContainerNo() {
        return containerNo;
    }

    public void setContainerNo(String containerNo) {
        this.containerNo = containerNo;
    }

    public String getDeattchLocId() {
        return deattchLocId;
    }

    public void setDeattchLocId(String deattchLocId) {
        this.deattchLocId = deattchLocId;
    }

    public String getRemainOrderWeight() {
        return remainOrderWeight;
    }

    public void setRemainOrderWeight(String remainOrderWeight) {
        this.remainOrderWeight = remainOrderWeight;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getOrderGPSTrackingLocation() {
        return orderGPSTrackingLocation;
    }

    public void setOrderGPSTrackingLocation(String orderGPSTrackingLocation) {
        this.orderGPSTrackingLocation = orderGPSTrackingLocation;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getPlannedPickupDate() {
        return plannedPickupDate;
    }

    public void setPlannedPickupDate(String plannedPickupDate) {
        this.plannedPickupDate = plannedPickupDate;
    }

    public String getPlannedDeliveryDate() {
        return plannedDeliveryDate;
    }

    public void setPlannedDeliveryDate(String plannedDeliveryDate) {
        this.plannedDeliveryDate = plannedDeliveryDate;
    }

    public String getDestinationCity() {
        return destinationCity;
    }

    public void setDestinationCity(String destinationCity) {
        this.destinationCity = destinationCity;
    }

    public String getOriginCity() {
        return originCity;
    }

    public void setOriginCity(String originCity) {
        this.originCity = originCity;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVendorTypeId() {
        return vendorTypeId;
    }

    public void setVendorTypeId(String vendorTypeId) {
        this.vendorTypeId = vendorTypeId;
    }

    public String getOwnerShip() {
        return ownerShip;
    }

    public void setOwnerShip(String ownerShip) {
        this.ownerShip = ownerShip;
    }

    public String getHireCharges() {
        return hireCharges;
    }

    public void setHireCharges(String hireCharges) {
        this.hireCharges = hireCharges;
    }

    public String getWayBillNo() {
        return wayBillNo;
    }

    public void setWayBillNo(String wayBillNo) {
        this.wayBillNo = wayBillNo;
    }

    public String getGpsDeviceId() {
        return gpsDeviceId;
    }

    public void setGpsDeviceId(String gpsDeviceId) {
        this.gpsDeviceId = gpsDeviceId;
    }

    public String getGpsVendor() {
        return gpsVendor;
    }

    public void setGpsVendor(String gpsVendor) {
        this.gpsVendor = gpsVendor;
    }

    public String getContainerTypeId() {
        return containerTypeId;
    }

    public void setContainerTypeId(String containerTypeId) {
        this.containerTypeId = containerTypeId;
    }

    public String getContainerCapacity() {
        return containerCapacity;
    }

    public void setContainerCapacity(String containerCapacity) {
        this.containerCapacity = containerCapacity;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getRepoId() {
        return repoId;
    }

    public void setRepoId(String repoId) {
        this.repoId = repoId;
    }

    public String getConsignmentNoteId() {
        return consignmentNoteId;
    }

    public void setConsignmentNoteId(String consignmentNoteId) {
        this.consignmentNoteId = consignmentNoteId;
    }

    public String getFromCityName() {
        return fromCityName;
    }

    public void setFromCityName(String fromCityName) {
        this.fromCityName = fromCityName;
    }

    public String getToCityName() {
        return toCityName;
    }

    public void setToCityName(String toCityName) {
        this.toCityName = toCityName;
    }

    public String getActiveStatus() {
        return activeStatus;
    }

    public void setActiveStatus(String activeStatus) {
        this.activeStatus = activeStatus;
    }

    public String getBillingPartyId() {
        return billingPartyId;
    }

    public void setBillingPartyId(String billingPartyId) {
        this.billingPartyId = billingPartyId;
    }

    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getBunkState() {
        return bunkState;
    }

    public void setBunkState(String bunkState) {
        this.bunkState = bunkState;
    }

    public String getBunkStatus() {
        return bunkStatus;
    }

    public void setBunkStatus(String bunkStatus) {
        this.bunkStatus = bunkStatus;
    }

    public String getConsignmentOrderDate() {
        return consignmentOrderDate;
    }

    public void setConsignmentOrderDate(String consignmentOrderDate) {
        this.consignmentOrderDate = consignmentOrderDate;
    }

    public String getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String containerQty) {
        this.containerQty = containerQty;
    }

    public String getCurrRate() {
        return currRate;
    }

    public void setCurrRate(String currRate) {
        this.currRate = currRate;
    }

    public String getCurrlocation() {
        return currlocation;
    }

    public void setCurrlocation(String currlocation) {
        this.currlocation = currlocation;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getRtoId() {
        return rtoId;
    }

    public void setRtoId(String rtoId) {
        this.rtoId = rtoId;
    }

    public String getRtoName() {
        return rtoName;
    }

    public void setRtoName(String rtoName) {
        this.rtoName = rtoName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBillingParty() {
        return billingParty;
    }

    public void setBillingParty(String billingParty) {
        this.billingParty = billingParty;
    }

    public String getLastPointId() {
        return lastPointId;
    }

    public void setLastPointId(String lastPointId) {
        this.lastPointId = lastPointId;
    }

    public String getGrNumber() {
        return grNumber;
    }

    public void setGrNumber(String grNumber) {
        this.grNumber = grNumber;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getTripDetainHours() {
        return tripDetainHours;
    }

    public void setTripDetainHours(String tripDetainHours) {
        this.tripDetainHours = tripDetainHours;
    }

    public int getAccVehicleId() {
        return accVehicleId;
    }

    public void setAccVehicleId(int accVehicleId) {
        this.accVehicleId = accVehicleId;
    }

    public int getReasonForChange() {
        return reasonForChange;
    }

    public void setReasonForChange(int reasonForChange) {
        this.reasonForChange = reasonForChange;
    }

    public String getLinerName() {
        return linerName;
    }

    public void setLinerName(String linerName) {
        this.linerName = linerName;
    }

    public String getSealNo() {
        return sealNo;
    }

    public void setSealNo(String sealNo) {
        this.sealNo = sealNo;
    }

    public String getGrStatus() {
        return grStatus;
    }

    public void setGrStatus(String grStatus) {
        this.grStatus = grStatus;
    }

    public String getPaidExpense() {
        return paidExpense;
    }

    public void setPaidExpense(String paidExpense) {
        this.paidExpense = paidExpense;
    }

    public String getDieselAmount() {
        return dieselAmount;
    }

    public void setDieselAmount(String dieselAmount) {
        this.dieselAmount = dieselAmount;
    }

    public String getGrDate() {
        return grDate;
    }

    public void setGrDate(String grDate) {
        this.grDate = grDate;
    }

    public String getTripContainerId() {
        return tripContainerId;
    }

    public void setTripContainerId(String tripContainerId) {
        this.tripContainerId = tripContainerId;
    }

    public String getTripFactoryToIcdHour() {
        return tripFactoryToIcdHour;
    }

    public void setTripFactoryToIcdHour(String tripFactoryToIcdHour) {
        this.tripFactoryToIcdHour = tripFactoryToIcdHour;
    }

    public String getContractTypeId() {
        return contractTypeId;
    }

    public void setContractTypeId(String contractTypeId) {
        this.contractTypeId = contractTypeId;
    }

    public String getHireVehicleNo() {
        return hireVehicleNo;
    }

    public void setHireVehicleNo(String hireVehicleNo) {
        this.hireVehicleNo = hireVehicleNo;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getVehicleCategory() {
        return vehicleCategory;
    }

    public void setVehicleCategory(String vehicleCategory) {
        this.vehicleCategory = vehicleCategory;
    }

    public String getBillOfEntry() {
        return billOfEntry;
    }

    public void setBillOfEntry(String billOfEntry) {
        this.billOfEntry = billOfEntry;
    }

    public String getShipingLineNo() {
        return shipingLineNo;
    }

    public void setShipingLineNo(String shipingLineNo) {
        this.shipingLineNo = shipingLineNo;
    }

    public String getChallanDate() {
        return challanDate;
    }

    public void setChallanDate(String challanDate) {
        this.challanDate = challanDate;
    }

    public String getChallanNo() {
        return challanNo;
    }

    public void setChallanNo(String challanNo) {
        this.challanNo = challanNo;
    }

    public String getInteramPoint() {
        return interamPoint;
    }

    public void setInteramPoint(String interamPoint) {
        this.interamPoint = interamPoint;
    }

    public String getContainerTypeName() {
        return containerTypeName;
    }

    public void setContainerTypeName(String containerTypeName) {
        this.containerTypeName = containerTypeName;
    }

    public String getDetaintion() {
        return detaintion;
    }

    public void setDetaintion(String detaintion) {
        this.detaintion = detaintion;
    }

    public String getGrNo() {
        return grNo;
    }

    public void setGrNo(String grNo) {
        this.grNo = grNo;
    }

    public String getGrHours() {
        return grHours;
    }

    public void setGrHours(String grHours) {
        this.grHours = grHours;
    }

    public String getGrMins() {
        return grMins;
    }

    public void setGrMins(String grMins) {
        this.grMins = grMins;
    }

    public String getMovementTypeName() {
        return movementTypeName;
    }

    public void setMovementTypeName(String movementTypeName) {
        this.movementTypeName = movementTypeName;
    }

    public String getVehicleVendor() {
        return vehicleVendor;
    }

    public void setVehicleVendor(String vehicleVendor) {
        this.vehicleVendor = vehicleVendor;
    }

    public String getConsignmentContainerId() {
        return consignmentContainerId;
    }

    public void setConsignmentContainerId(String consignmentContainerId) {
        this.consignmentContainerId = consignmentContainerId;
    }

    public String getMailDeliveredResponse() {
        return mailDeliveredResponse;
    }

    public void setMailDeliveredResponse(String mailDeliveredResponse) {
        this.mailDeliveredResponse = mailDeliveredResponse;
    }

    public String getTollTax() {
        return tollTax;
    }

    public void setTollTax(String tollTax) {
        this.tollTax = tollTax;
    }

    public String getWeightmentExpense() {
        return weightmentExpense;
    }

    public void setWeightmentExpense(String weightmentExpense) {
        this.weightmentExpense = weightmentExpense;
    }

    public String getGrId() {
        return grId;
    }

    public void setGrId(String grId) {
        this.grId = grId;
    }

    public String getUsedStatus() {
        return usedStatus;
    }

    public void setUsedStatus(String usedStatus) {
        this.usedStatus = usedStatus;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String[] getExpenseTypes() {
        return expenseTypes;
    }

    public void setExpenseTypes(String[] expenseTypes) {
        this.expenseTypes = expenseTypes;
    }

    public String[] getExpenses() {
        return expenses;
    }

    public void setExpenses(String[] expenses) {
        this.expenses = expenses;
    }

    public String[] getTripExpensesId() {
        return tripExpensesId;
    }

    public void setTripExpensesId(String[] tripExpensesId) {
        this.tripExpensesId = tripExpensesId;
    }

    public String getTripExpenseIds() {
        return tripExpenseIds;
    }

    public void setTripExpenseIds(String tripExpenseIds) {
        this.tripExpenseIds = tripExpenseIds;
    }

    public String getTransporter() {
        return transporter;
    }

    public void setTransporter(String transporter) {
        this.transporter = transporter;
    }

    public String getNewSlip() {
        return newSlip;
    }

    public void setNewSlip(String newSlip) {
        this.newSlip = newSlip;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getLoadType() {
        return loadType;
    }

    public void setLoadType(String loadType) {
        this.loadType = loadType;
    }

    public String[] getTrailerRemarks() {
        return trailerRemarks;
    }

    public void setTrailerRemarks(String[] trailerRemarks) {
        this.trailerRemarks = trailerRemarks;
    }

    public String[] getTrailerIds() {
        return trailerIds;
    }

    public void setTrailerIds(String[] trailerIds) {
        this.trailerIds = trailerIds;
    }

    public String getAdvanceGrNo() {
        return advanceGrNo;
    }

    public void setAdvanceGrNo(String advanceGrNo) {
        this.advanceGrNo = advanceGrNo;
    }

    public String getRemainWeight() {
        return remainWeight;
    }

    public void setRemainWeight(String remainWeight) {
        this.remainWeight = remainWeight;
    }

    public String getIsRepo() {
        return isRepo;
    }

    public void setIsRepo(String isRepo) {
        this.isRepo = isRepo;
    }

    public String[] getContainerTypeIds() {
        return containerTypeIds;
    }

    public void setContainerTypeIds(String[] containerTypeIds) {
        this.containerTypeIds = containerTypeIds;
    }

    public String[] getContainerNos() {
        return containerNos;
    }

    public void setContainerNos(String[] containerNos) {
        this.containerNos = containerNos;
    }

    public String getUniqueIds() {
        return uniqueIds;
    }

    public void setUniqueIds(String uniqueIds) {
        this.uniqueIds = uniqueIds;
    }

    public String[] getBunkNames() {
        return bunkNames;
    }

    public void setBunkNames(String[] bunkNames) {
        this.bunkNames = bunkNames;
    }

    public String[] getBunkPlace() {
        return bunkPlace;
    }

    public void setBunkPlace(String[] bunkPlace) {
        this.bunkPlace = bunkPlace;
    }

    public String[] getFuelDates() {
        return fuelDates;
    }

    public void setFuelDates(String[] fuelDates) {
        this.fuelDates = fuelDates;
    }

    public String[] getFuelAmounts() {
        return fuelAmounts;
    }

    public void setFuelAmounts(String[] fuelAmounts) {
        this.fuelAmounts = fuelAmounts;
    }

    public String[] getFuelLtrs() {
        return fuelLtrs;
    }

    public void setFuelLtrs(String[] fuelLtrs) {
        this.fuelLtrs = fuelLtrs;
    }

    public String[] getFuelFilledBy() {
        return fuelFilledBy;
    }

    public void setFuelFilledBy(String[] fuelFilledBy) {
        this.fuelFilledBy = fuelFilledBy;
    }

    public String[] getFuelRemark() {
        return fuelRemark;
    }

    public void setFuelRemark(String[] fuelRemark) {
        this.fuelRemark = fuelRemark;
    }

    public String[] getSlipNo() {
        return slipNo;
    }

    public void setSlipNo(String[] slipNo) {
        this.slipNo = slipNo;
    }

    public String[] getFuelUniqueId() {
        return fuelUniqueId;
    }

    public void setFuelUniqueId(String[] fuelUniqueId) {
        this.fuelUniqueId = fuelUniqueId;
    }

    public String getDriverNameId() {
        return driverNameId;
    }

    public void setDriverNameId(String driverNameId) {
        this.driverNameId = driverNameId;
    }

    public String getAdvancerequestamt() {
        return advancerequestamt;
    }

    public void setAdvancerequestamt(String advancerequestamt) {
        this.advancerequestamt = advancerequestamt;
    }

    public String getRequeststatus() {
        return requeststatus;
    }

    public void setRequeststatus(String requeststatus) {
        this.requeststatus = requeststatus;
    }

    public String getTobepaidtoday() {
        return tobepaidtoday;
    }

    public void setTobepaidtoday(String tobepaidtoday) {
        this.tobepaidtoday = tobepaidtoday;
    }

    public String getTripday() {
        return tripday;
    }

    public void setTripday(String tripday) {
        this.tripday = tripday;
    }

    public String getBatchType() {
        return batchType;
    }

    public void setBatchType(String batchType) {
        this.batchType = batchType;
    }

    public String getCurrencyid() {
        return currencyid;
    }

    public void setCurrencyid(String currencyid) {
        this.currencyid = currencyid;
    }

    public String getExpenseCategory() {
        return expenseCategory;
    }

    public void setExpenseCategory(String expenseCategory) {
        this.expenseCategory = expenseCategory;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getCommodityName() {
        return commodityName;
    }

    public void setCommodityName(String commodityName) {
        this.commodityName = commodityName;
    }

    public String getGstApplicable() {
        return gstApplicable;
    }

    public void setGstApplicable(String gstApplicable) {
        this.gstApplicable = gstApplicable;
    }

    public String getCommodityCategory() {
        return commodityCategory;
    }

    public void setCommodityCategory(String commodityCategory) {
        this.commodityCategory = commodityCategory;
    }

    public String getLinerId() {
        return linerId;
    }

    public void setLinerId(String linerId) {
        this.linerId = linerId;
    }

    public String getTripPointId() {
        return tripPointId;
    }

    public void setTripPointId(String tripPointId) {
        this.tripPointId = tripPointId;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

    public String getPrintFlag() {
        return printFlag;
    }

    public void setPrintFlag(String printFlag) {
        this.printFlag = printFlag;
    }

    public String getSingleTwentyStatus() {
        return singleTwentyStatus;
    }

    public void setSingleTwentyStatus(String singleTwentyStatus) {
        this.singleTwentyStatus = singleTwentyStatus;
    }

    public String[] getPointName() {
        return pointName;
    }

    public void setPointName(String[] pointName) {
        this.pointName = pointName;
    }

    public String getContainerQuantity() {
        return containerQuantity;
    }

    public void setContainerQuantity(String containerQuantity) {
        this.containerQuantity = containerQuantity;
    }

    public String getTwtyTotal() {
        return twtyTotal;
    }

    public void setTwtyTotal(String twtyTotal) {
        this.twtyTotal = twtyTotal;
    }

    public String getFrtyTotal() {
        return frtyTotal;
    }

    public void setFrtyTotal(String frtyTotal) {
        this.frtyTotal = frtyTotal;
    }

    public ArrayList getUserMovementList() {
        return userMovementList;
    }

    public void setUserMovementList(ArrayList userMovementList) {
        this.userMovementList = userMovementList;
    }

    public String getConsArticleid() {
        return consArticleid;
    }

    public void setConsArticleid(String consArticleid) {
        this.consArticleid = consArticleid;
    }

    public String[] getConsArticleids() {
        return consArticleids;
    }

    public void setConsArticleids(String[] consArticleids) {
        this.consArticleids = consArticleids;
    }

    public String getCollectionBranch() {
        return collectionBranch;
    }

    public void setCollectionBranch(String collectionBranch) {
        this.collectionBranch = collectionBranch;
    }

    public String getOrderTripType() {
        return orderTripType;
    }

    public void setOrderTripType(String orderTripType) {
        this.orderTripType = orderTripType;
    }

    public String getConsignorGST() {
        return consignorGST;
    }

    public void setConsignorGST(String consignorGST) {
        this.consignorGST = consignorGST;
    }

    public String getConsigneeGST() {
        return consigneeGST;
    }

    public void setConsigneeGST(String consigneeGST) {
        this.consigneeGST = consigneeGST;
    }

    public String getBillingPartyGST() {
        return billingPartyGST;
    }

    public void setBillingPartyGST(String billingPartyGST) {
        this.billingPartyGST = billingPartyGST;
    }

    public String getEwayBillNo() {
        return ewayBillNo;
    }

    public void setEwayBillNo(String ewayBillNo) {
        this.ewayBillNo = ewayBillNo;
    }

    public String getContainerPkgs() {
        return containerPkgs;
    }

    public void setContainerPkgs(String containerPkgs) {
        this.containerPkgs = containerPkgs;
    }

    public String getContainerWgts() {
        return containerWgts;
    }

    public void setContainerWgts(String containerWgts) {
        this.containerWgts = containerWgts;
    }

    public String getContainerVolume() {
        return containerVolume;
    }

    public void setContainerVolume(String containerVolume) {
        this.containerVolume = containerVolume;
    }

    public String getContainerNumber() {
        return containerNumber;
    }

    public void setContainerNumber(String containerNumber) {
        this.containerNumber = containerNumber;
    }

    public String getPerDayCOst() {
        return perDayCOst;
    }

    public void setPerDayCOst(String perDayCOst) {
        this.perDayCOst = perDayCOst;
    }

    public String getTripExpense() {
        return tripExpense;
    }

    public void setTripExpense(String tripExpense) {
        this.tripExpense = tripExpense;
    }

    public String getFuelLtrsCost() {
        return fuelLtrsCost;
    }

    public void setFuelLtrsCost(String fuelLtrsCost) {
        this.fuelLtrsCost = fuelLtrsCost;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public String getMovementTypeId() {
        return movementTypeId;
    }

    public void setMovementTypeId(String movementTypeId) {
        this.movementTypeId = movementTypeId;
    }

    public String getContainerDetail() {
        return containerDetail;
    }

    public void setContainerDetail(String containerDetail) {
        this.containerDetail = containerDetail;
    }

    public String getConsignmentArticles() {
        return consignmentArticles;
    }

    public void setConsignmentArticles(String consignmentArticles) {
        this.consignmentArticles = consignmentArticles;
    }

    public String getContainerPackages() {
        return containerPackages;
    }

    public void setContainerPackages(String containerPackages) {
        this.containerPackages = containerPackages;
    }

    public String getManualLrNumber() {
        return manualLrNumber;
    }

    public void setManualLrNumber(String manualLrNumber) {
        this.manualLrNumber = manualLrNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAdvTrnType() {
        return advTrnType;
    }

    public void setAdvTrnType(String advTrnType) {
        this.advTrnType = advTrnType;
    }

    public String getHire() {
        return hire;
    }

    public void setHire(String hire) {
        this.hire = hire;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getMamul() {
        return mamul;
    }

    public void setMamul(String mamul) {
        this.mamul = mamul;
    }

    public String getTripAdvance() {
        return tripAdvance;
    }

    public void setTripAdvance(String tripAdvance) {
        this.tripAdvance = tripAdvance;
    }

    public String getFuelLtr() {
        return fuelLtr;
    }

    public void setFuelLtr(String fuelLtr) {
        this.fuelLtr = fuelLtr;
    }

    public String getFuelBunk() {
        return fuelBunk;
    }

    public void setFuelBunk(String fuelBunk) {
        this.fuelBunk = fuelBunk;
    }

    public String getEmptyDropTime() {
        return emptyDropTime;
    }

    public void setEmptyDropTime(String emptyDropTime) {
        this.emptyDropTime = emptyDropTime;
    }

    public String getContainerStatus() {
        return containerStatus;
    }

    public void setContainerStatus(String containerStatus) {
        this.containerStatus = containerStatus;
    }

    public String getTallyName() {
        return tallyName;
    }

    public void setTallyName(String tallyName) {
        this.tallyName = tallyName;
    }

    public String getVehTallyName() {
        return vehTallyName;
    }

    public void setVehTallyName(String vehTallyName) {
        this.vehTallyName = vehTallyName;
    }

    public String getTonnage() {
        return tonnage;
    }

    public void setTonnage(String tonnage) {
        this.tonnage = tonnage;
    }

    public String[] getWgtTonnage() {
        return wgtTonnage;
    }

    public void setWgtTonnage(String[] wgtTonnage) {
        this.wgtTonnage = wgtTonnage;
    }

    public String getVehicleCategory1() {
        return vehicleCategory1;
    }

    public void setVehicleCategory1(String vehicleCategory1) {
        this.vehicleCategory1 = vehicleCategory1;
    }

    public String getContractHireId() {
        return contractHireId;
    }

    public void setContractHireId(String contractHireId) {
        this.contractHireId = contractHireId;
    }

    public String getSubStatusId() {
        return subStatusId;
    }

    public void setSubStatusId(String subStatusId) {
        this.subStatusId = subStatusId;
    }

    public String getTotalForty() {
        return totalForty;
    }

    public void setTotalForty(String totalForty) {
        this.totalForty = totalForty;
    }

    public String getEmptyLocation() {
        return emptyLocation;
    }

    public void setEmptyLocation(String emptyLocation) {
        this.emptyLocation = emptyLocation;
    }

    public String getEmptyLocName() {
        return emptyLocName;
    }

    public void setEmptyLocName(String emptyLocName) {
        this.emptyLocName = emptyLocName;
    }

    public String getTripSubStatus() {
        return tripSubStatus;
    }

    public void setTripSubStatus(String tripSubStatus) {
        this.tripSubStatus = tripSubStatus;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getMobilNo() {
        return mobilNo;
    }

    public void setMobilNo(String mobilNo) {
        this.mobilNo = mobilNo;
    }

    public String getDlExpiryDate() {
        return dlExpiryDate;
    }

    public void setDlExpiryDate(String dlExpiryDate) {
        this.dlExpiryDate = dlExpiryDate;
    }

    public String getCleaner() {
        return cleaner;
    }

    public void setCleaner(String cleaner) {
        this.cleaner = cleaner;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getAllocatedFuel() {
        return allocatedFuel;
    }

    public void setAllocatedFuel(String allocatedFuel) {
        this.allocatedFuel = allocatedFuel;
    }

    public String getConsumedFuel() {
        return consumedFuel;
    }

    public void setConsumedFuel(String consumedFuel) {
        this.consumedFuel = consumedFuel;
    }

    public String getRecoveryFuel() {
        return recoveryFuel;
    }

    public void setRecoveryFuel(String recoveryFuel) {
        this.recoveryFuel = recoveryFuel;
    }

    public String getRecoveryAmount() {
        return recoveryAmount;
    }

    public void setRecoveryAmount(String recoveryAmount) {
        this.recoveryAmount = recoveryAmount;
    }

    public String getFuelSlipNo() {
        return fuelSlipNo;
    }

    public void setFuelSlipNo(String fuelSlipNo) {
        this.fuelSlipNo = fuelSlipNo;
    }

    public String[] getFuelMode() {
        return fuelMode;
    }

    public void setFuelMode(String[] fuelMode) {
        this.fuelMode = fuelMode;
    }

    public String[] getFuelFilledName() {
        return fuelFilledName;
    }

    public void setFuelFilledName(String[] fuelFilledName) {
        this.fuelFilledName = fuelFilledName;
    }

    public String[] getExtraFuelRemarks() {
        return extraFuelRemarks;
    }

    public void setExtraFuelRemarks(String[] extraFuelRemarks) {
        this.extraFuelRemarks = extraFuelRemarks;
    }

    public String getFuelingMode() {
        return fuelingMode;
    }

    public void setFuelingMode(String fuelingMode) {
        this.fuelingMode = fuelingMode;
    }

    public String getFuelRecoveryRemaks() {
        return fuelRecoveryRemaks;
    }

    public void setFuelRecoveryRemaks(String fuelRecoveryRemaks) {
        this.fuelRecoveryRemaks = fuelRecoveryRemaks;
    }

    public String getBorneBy() {
        return borneBy;
    }

    public void setBorneBy(String borneBy) {
        this.borneBy = borneBy;
    }

    public String getOpenKM() {
        return openKM;
    }

    public void setOpenKM(String openKM) {
        this.openKM = openKM;
    }

    public String getClosingKM() {
        return closingKM;
    }

    public void setClosingKM(String closingKM) {
        this.closingKM = closingKM;
    }

    public String getPageType() {
        return pageType;
    }

    public void setPageType(String pageType) {
        this.pageType = pageType;
    }

    public String getFilledFuel() {
        return filledFuel;
    }

    public void setFilledFuel(String filledFuel) {
        this.filledFuel = filledFuel;
    }

    public String getApprovStatus() {
        return approvStatus;
    }

    public void setApprovStatus(String approvStatus) {
        this.approvStatus = approvStatus;
    }

    public String getTripExpenId() {
        return tripExpenId;
    }

    public void setTripExpenId(String tripExpenId) {
        this.tripExpenId = tripExpenId;
    }

    public String getDurationDate() {
        return durationDate;
    }

    public void setDurationDate(String durationDate) {
        this.durationDate = durationDate;
    }

    public String getDurationTime() {
        return durationTime;
    }

    public void setDurationTime(String durationTime) {
        this.durationTime = durationTime;
    }

    public String[] getUniquesId() {
        return uniquesId;
    }

    public void setUniquesId(String[] uniquesId) {
        this.uniquesId = uniquesId;
    }

    public String[] getSealNos() {
        return sealNos;
    }

    public void setSealNos(String[] sealNos) {
        this.sealNos = sealNos;
    }

    public String geteWayBillNo() {
        return eWayBillNo;
    }

    public void seteWayBillNo(String eWayBillNo) {
        this.eWayBillNo = eWayBillNo;
    }

    public String getInvoiceValue() {
        return invoiceValue;
    }

    public void setInvoiceValue(String invoiceValue) {
        this.invoiceValue = invoiceValue;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getInvoiceWeight() {
        return invoiceWeight;
    }

    public void setInvoiceWeight(String invoiceWeight) {
        this.invoiceWeight = invoiceWeight;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getExpiryHour() {
        return expiryHour;
    }

    public void setExpiryHour(String expiryHour) {
        this.expiryHour = expiryHour;
    }

    public String getExpiryMin() {
        return expiryMin;
    }

    public void setExpiryMin(String expiryMin) {
        this.expiryMin = expiryMin;
    }

    public String getTripCloseDate() {
        return tripCloseDate;
    }

    public void setTripCloseDate(String tripCloseDate) {
        this.tripCloseDate = tripCloseDate;
    }

    public String getOldVehicleId() {
        return oldVehicleId;
    }

    public void setOldVehicleId(String oldVehicleId) {
        this.oldVehicleId = oldVehicleId;
    }

    public String[] getActualPointId() {
        return actualPointId;
    }

    public void setActualPointId(String[] actualPointId) {
        this.actualPointId = actualPointId;
    }

    public String[] getActualPointName() {
        return actualPointName;
    }

    public void setActualPointName(String[] actualPointName) {
        this.actualPointName = actualPointName;
    }

    public String[] getActualLatitude() {
        return actualLatitude;
    }

    public void setActualLatitude(String[] actualLatitude) {
        this.actualLatitude = actualLatitude;
    }

    public String[] getActualLongtitude() {
        return actualLongtitude;
    }

    public void setActualLongtitude(String[] actualLongtitude) {
        this.actualLongtitude = actualLongtitude;
    }

    public String[] getRouteCourseId() {
        return routeCourseId;
    }

    public void setRouteCourseId(String[] routeCourseId) {
        this.routeCourseId = routeCourseId;
    }

    public String getClearanceType() {
        return clearanceType;
    }

    public void setClearanceType(String clearanceType) {
        this.clearanceType = clearanceType;
    }

    public String[] getOrderRevenues() {
        return orderRevenues;
    }

    public void setOrderRevenues(String[] orderRevenues) {
        this.orderRevenues = orderRevenues;
    }

    public String getRouteDeviation() {
        return routeDeviation;
    }

    public void setRouteDeviation(String routeDeviation) {
        this.routeDeviation = routeDeviation;
    }

    public String[] getUniqueIdE() {
        return uniqueIdE;
    }

    public void setUniqueIdE(String[] uniqueIdE) {
        this.uniqueIdE = uniqueIdE;
    }

    public String[] getBunkIdE() {
        return bunkIdE;
    }

    public void setBunkIdE(String[] bunkIdE) {
        this.bunkIdE = bunkIdE;
    }

    public String[] getSlipNoE() {
        return slipNoE;
    }

    public void setSlipNoE(String[] slipNoE) {
        this.slipNoE = slipNoE;
    }

    public String[] getFuelDateE() {
        return fuelDateE;
    }

    public void setFuelDateE(String[] fuelDateE) {
        this.fuelDateE = fuelDateE;
    }

    public String[] getFuelLtrsE() {
        return fuelLtrsE;
    }

    public void setFuelLtrsE(String[] fuelLtrsE) {
        this.fuelLtrsE = fuelLtrsE;
    }

    public String[] getFuelAmountE() {
        return fuelAmountE;
    }

    public void setFuelAmountE(String[] fuelAmountE) {
        this.fuelAmountE = fuelAmountE;
    }

    public String[] getFuelRemarksE() {
        return fuelRemarksE;
    }

    public void setFuelRemarksE(String[] fuelRemarksE) {
        this.fuelRemarksE = fuelRemarksE;
    }

    public String getTallyBillingType() {
        return tallyBillingType;
    }

    public void setTallyBillingType(String tallyBillingType) {
        this.tallyBillingType = tallyBillingType;
    }

    public String getConsigOrderId() {
        return consigOrderId;
    }

    public void setConsigOrderId(String consigOrderId) {
        this.consigOrderId = consigOrderId;
    }

    public String getJobReferenceNo() {
        return jobReferenceNo;
    }

    public void setJobReferenceNo(String jobReferenceNo) {
        this.jobReferenceNo = jobReferenceNo;
    }

    public String getConsignOrderId() {
        return consignOrderId;
    }

    public void setConsignOrderId(String consignOrderId) {
        this.consignOrderId = consignOrderId;
    }

    public String getHaltingDays() {
        return haltingDays;
    }

    public void setHaltingDays(String haltingDays) {
        this.haltingDays = haltingDays;
    }

    public String getHaltCntrAmt() {
        return haltCntrAmt;
    }

    public void setHaltCntrAmt(String haltCntrAmt) {
        this.haltCntrAmt = haltCntrAmt;
    }

    public String[] getFuelPriceRemarks() {
        return fuelPriceRemarks;
    }

    public void setFuelPriceRemarks(String[] fuelPriceRemarks) {
        this.fuelPriceRemarks = fuelPriceRemarks;
    }

    public String[] getFuelPriceRemarksE() {
        return fuelPriceRemarksE;
    }

    public void setFuelPriceRemarksE(String[] fuelPriceRemarksE) {
        this.fuelPriceRemarksE = fuelPriceRemarksE;
    }

    public String getFuelPriceRmrk() {
        return fuelPriceRmrk;
    }

    public void setFuelPriceRmrk(String fuelPriceRmrk) {
        this.fuelPriceRmrk = fuelPriceRmrk;
    }

    public String[] getFuelPriceRemarksEn() {
        return fuelPriceRemarksEn;
    }

    public void setFuelPriceRemarksEn(String[] fuelPriceRemarksEn) {
        this.fuelPriceRemarksEn = fuelPriceRemarksEn;
    }

    public String[] getTripAdvanceIds() {
        return tripAdvanceIds;
    }

    public void setTripAdvanceIds(String[] tripAdvanceIds) {
        this.tripAdvanceIds = tripAdvanceIds;
    }

    public String getHaltStartDate() {
        return haltStartDate;
    }

    public void setHaltStartDate(String haltStartDate) {
        this.haltStartDate = haltStartDate;
    }

    public String getHaltStartHour() {
        return haltStartHour;
    }

    public void setHaltStartHour(String haltStartHour) {
        this.haltStartHour = haltStartHour;
    }

    public String getHaltStartMinute() {
        return haltStartMinute;
    }

    public void setHaltStartMinute(String haltStartMinute) {
        this.haltStartMinute = haltStartMinute;
    }

    public String getHaltEndDate() {
        return haltEndDate;
    }

    public void setHaltEndDate(String haltEndDate) {
        this.haltEndDate = haltEndDate;
    }

    public String getHaltEndHour() {
        return haltEndHour;
    }

    public void setHaltEndHour(String haltEndHour) {
        this.haltEndHour = haltEndHour;
    }

    public String getHaltEndMinute() {
        return haltEndMinute;
    }

    public void setHaltEndMinute(String haltEndMinute) {
        this.haltEndMinute = haltEndMinute;
    }

    public String getDriverCode() {
        return driverCode;
    }

    public void setDriverCode(String driverCode) {
        this.driverCode = driverCode;
    }

    public String getCleanerCode() {
        return cleanerCode;
    }

    public void setCleanerCode(String cleanerCode) {
        this.cleanerCode = cleanerCode;
    }

    public String getDivertCityId() {
        return divertCityId;
    }

    public void setDivertCityId(String divertCityId) {
        this.divertCityId = divertCityId;
    }

    public String getDivertCharge() {
        return divertCharge;
    }

    public void setDivertCharge(String divertCharge) {
        this.divertCharge = divertCharge;
    }

    public String getDivertRemarks() {
        return divertRemarks;
    }

    public void setDivertRemarks(String divertRemarks) {
        this.divertRemarks = divertRemarks;
    }

    public String getDivertCityCode() {
        return divertCityCode;
    }

    public void setDivertCityCode(String divertCityCode) {
        this.divertCityCode = divertCityCode;
    }

    public String getDivertCityName() {
        return divertCityName;
    }

    public void setDivertCityName(String divertCityName) {
        this.divertCityName = divertCityName;
    }

    public String getOTId() {
        return OTId;
    }

    public void setOTId(String OTId) {
        this.OTId = OTId;
    }

    public String getNetWt() {
        return netWt;
    }

    public void setNetWt(String netWt) {
        this.netWt = netWt;
    }

    public String getActWt() {
        return actWt;
    }

    public void setActWt(String actWt) {
        this.actWt = actWt;
    }

    public String getOTWt() {
        return OTWt;
    }

    public void setOTWt(String OTWt) {
        this.OTWt = OTWt;
    }

    public String getChargePerTon() {
        return chargePerTon;
    }

    public void setChargePerTon(String chargePerTon) {
        this.chargePerTon = chargePerTon;
    }

    public String getOTCharges() {
        return OTCharges;
    }

    public void setOTCharges(String OTCharges) {
        this.OTCharges = OTCharges;
    }

    public String getDiversionId() {
        return diversionId;
    }

    public void setDiversionId(String diversionId) {
        this.diversionId = diversionId;
    }

    public String getStartEndTime() {
        return startEndTime;
    }

    public void setStartEndTime(String startEndTime) {
        this.startEndTime = startEndTime;
    }

    public String getTatValues() {
        return tatValues;
    }

    public void setTatValues(String tatValues) {
        this.tatValues = tatValues;
    }

    public String getHaltStartTime() {
        return haltStartTime;
    }

    public void setHaltStartTime(String haltStartTime) {
        this.haltStartTime = haltStartTime;
    }

    public String getHaltEndTime() {
        return haltEndTime;
    }

    public void setHaltEndTime(String haltEndTime) {
        this.haltEndTime = haltEndTime;
    }

    public String getDriverMobNo() {
        return driverMobNo;
    }

    public void setDriverMobNo(String driverMobNo) {
        this.driverMobNo = driverMobNo;
    }

    public String getCleanerMobNo() {
        return cleanerMobNo;
    }

    public void setCleanerMobNo(String cleanerMobNo) {
        this.cleanerMobNo = cleanerMobNo;
    }

    public String getMappingId() {
        return mappingId;
    }

    public void setMappingId(String mappingId) {
        this.mappingId = mappingId;
    }

    public String[] getMappingIds() {
        return mappingIds;
    }

    public void setMappingIds(String[] mappingIds) {
        this.mappingIds = mappingIds;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getCustErpId() {
        return custErpId;
    }

    public void setCustErpId(String custErpId) {
        this.custErpId = custErpId;
    }

    public String getRtoCharges() {
        return rtoCharges;
    }

    public void setRtoCharges(String rtoCharges) {
        this.rtoCharges = rtoCharges;
    }

    public String getIsExport() {
        return isExport;
    }

    public void setIsExport(String isExport) {
        this.isExport = isExport;
    }

    public Double getNetWts() {
        return netWts;
    }

    public void setNetWts(Double netWts) {
        this.netWts = netWts;
    }

    public String getClrStartDate() {
        return clrStartDate;
    }

    public void setClrStartDate(String clrStartDate) {
        this.clrStartDate = clrStartDate;
    }

    public String getClrStartHour() {
        return clrStartHour;
    }

    public void setClrStartHour(String clrStartHour) {
        this.clrStartHour = clrStartHour;
    }

    public String getClrStartMinute() {
        return clrStartMinute;
    }

    public void setClrStartMinute(String clrStartMinute) {
        this.clrStartMinute = clrStartMinute;
    }

    public String getClrEndDate() {
        return clrEndDate;
    }

    public void setClrEndDate(String clrEndDate) {
        this.clrEndDate = clrEndDate;
    }

    public String getClrEndHour() {
        return clrEndHour;
    }

    public void setClrEndHour(String clrEndHour) {
        this.clrEndHour = clrEndHour;
    }

    public String getClrEndMinute() {
        return clrEndMinute;
    }

    public void setClrEndMinute(String clrEndMinute) {
        this.clrEndMinute = clrEndMinute;
    }

    public String getClrCntrAmt() {
        return clrCntrAmt;
    }

    public void setClrCntrAmt(String clrCntrAmt) {
        this.clrCntrAmt = clrCntrAmt;
    }

    public String getTripHaltDay() {
        return tripHaltDay;
    }

    public void setTripHaltDay(String tripHaltDay) {
        this.tripHaltDay = tripHaltDay;
    }

    public String getTripClrDay() {
        return tripClrDay;
    }

    public void setTripClrDay(String tripClrDay) {
        this.tripClrDay = tripClrDay;
    }

    public String getClearanceCharge() {
        return clearanceCharge;
    }

    public void setClearanceCharge(String clearanceCharge) {
        this.clearanceCharge = clearanceCharge;
    }

    public String getClrStartTime() {
        return clrStartTime;
    }

    public void setClrStartTime(String clrStartTime) {
        this.clrStartTime = clrStartTime;
    }

    public String getClrEndTime() {
        return clrEndTime;
    }

    public void setClrEndTime(String clrEndTime) {
        this.clrEndTime = clrEndTime;
    }

    public String getContractTonnage() {
        return contractTonnage;
    }

    public void setContractTonnage(String contractTonnage) {
        this.contractTonnage = contractTonnage;
    }

    public String getChangeVehicleStatus() {
        return changeVehicleStatus;
    }

    public void setChangeVehicleStatus(String changeVehicleStatus) {
        this.changeVehicleStatus = changeVehicleStatus;
    }

    public String getContractHireRate() {
        return contractHireRate;
    }

    public void setContractHireRate(String contractHireRate) {
        this.contractHireRate = contractHireRate;
    }

    public String getVehicleCount() {
        return vehicleCount;
    }

    public void setVehicleCount(String vehicleCount) {
        this.vehicleCount = vehicleCount;
    }

    public String getDriverChangeDate() {
        return driverChangeDate;
    }

    public void setDriverChangeDate(String driverChangeDate) {
        this.driverChangeDate = driverChangeDate;
    }

    public String getOldDriverId() {
        return oldDriverId;
    }

    public void setOldDriverId(String oldDriverId) {
        this.oldDriverId = oldDriverId;
    }

    public String getPointIds() {
        return pointIds;
    }

    public void setPointIds(String pointIds) {
        this.pointIds = pointIds;
    }

    public String getRegNoHire() {
        return regNoHire;
    }

    public void setRegNoHire(String regNoHire) {
        this.regNoHire = regNoHire;
    }

    public String getRegNoHireOld() {
        return regNoHireOld;
    }

    public void setRegNoHireOld(String regNoHireOld) {
        this.regNoHireOld = regNoHireOld;
    }

    public String getHaltVehicleId() {
        return haltVehicleId;
    }

    public void setHaltVehicleId(String haltVehicleId) {
        this.haltVehicleId = haltVehicleId;
    }

    public String getClrVehicleId() {
        return clrVehicleId;
    }

    public void setClrVehicleId(String clrVehicleId) {
        this.clrVehicleId = clrVehicleId;
    }

    public String getVendorContractTypeId() {
        return vendorContractTypeId;
    }

    public void setVendorContractTypeId(String vendorContractTypeId) {
        this.vendorContractTypeId = vendorContractTypeId;
    }

    public String getVehicleContractTypeId() {
        return vehicleContractTypeId;
    }

    public void setVehicleContractTypeId(String vehicleContractTypeId) {
        this.vehicleContractTypeId = vehicleContractTypeId;
    }

    public String getOldVendor() {
        return oldVendor;
    }

    public void setOldVendor(String oldVendor) {
        this.oldVendor = oldVendor;
    }

    public String getOldStartDate() {
        return oldStartDate;
    }

    public void setOldStartDate(String oldStartDate) {
        this.oldStartDate = oldStartDate;
    }

    public String getOldEndDate() {
        return oldEndDate;
    }

    public void setOldEndDate(String oldEndDate) {
        this.oldEndDate = oldEndDate;
    }

    public String getChangeVehStatus() {
        return changeVehStatus;
    }

    public void setChangeVehStatus(String changeVehStatus) {
        this.changeVehStatus = changeVehStatus;
    }

    public String getTripIdNew() {
        return tripIdNew;
    }

    public void setTripIdNew(String tripIdNew) {
        this.tripIdNew = tripIdNew;
    }

    public String getActiveVehicle() {
        return activeVehicle;
    }

    public void setActiveVehicle(String activeVehicle) {
        this.activeVehicle = activeVehicle;
    }

    public String getEWaybillNo() {
        return EWaybillNo;
    }

    public void setEWaybillNo(String EWaybillNo) {
        this.EWaybillNo = EWaybillNo;
    }

    public String getEWaybillExpiryDate() {
        return EWaybillExpiryDate;
    }

    public void setEWaybillExpiryDate(String EWaybillExpiryDate) {
        this.EWaybillExpiryDate = EWaybillExpiryDate;
    }

    public String getEWaybillExpiryTime() {
        return EWaybillExpiryTime;
    }

    public void setEWaybillExpiryTime(String EWaybillExpiryTime) {
        this.EWaybillExpiryTime = EWaybillExpiryTime;
    }

    public String getTollId() {
        return tollId;
    }

    public void setTollId(String tollId) {
        this.tollId = tollId;
    }

    public String getPaidOn() {
        return paidOn;
    }

    public void setPaidOn(String paidOn) {
        this.paidOn = paidOn;
    }

    public String getDktFAStatus() {
        return dktFAStatus;
    }

    public void setDktFAStatus(String dktFAStatus) {
        this.dktFAStatus = dktFAStatus;
    }

    public String getClosureStatus() {
        return closureStatus;
    }

    public void setClosureStatus(String closureStatus) {
        this.closureStatus = closureStatus;
    }

    public String getOffloadStatus() {
        return offloadStatus;
    }

    public void setOffloadStatus(String offloadStatus) {
        this.offloadStatus = offloadStatus;
    }

    public String getCustomerErpId() {
        return customerErpId;
    }

    public void setCustomerErpId(String customerErpId) {
        this.customerErpId = customerErpId;
    }

    public String getActStatus() {
        return actStatus;
    }

    public void setActStatus(String actStatus) {
        this.actStatus = actStatus;
    }

    public String getVehStatusId() {
        return vehStatusId;
    }

    public void setVehStatusId(String vehStatusId) {
        this.vehStatusId = vehStatusId;
    }

    public String getDriverLicenseNo() {
        return driverLicenseNo;
    }

    public void setDriverLicenseNo(String driverLicenseNo) {
        this.driverLicenseNo = driverLicenseNo;
    }

    public String getCleanerLicenseNo() {
        return cleanerLicenseNo;
    }

    public void setCleanerLicenseNo(String cleanerLicenseNo) {
        this.cleanerLicenseNo = cleanerLicenseNo;
    }

    public String getVehicleNos() {
        return vehicleNos;
    }

    public void setVehicleNos(String vehicleNos) {
        this.vehicleNos = vehicleNos;
    }

    public String getHaltStatus() {
        return haltStatus;
    }

    public void setHaltStatus(String haltStatus) {
        this.haltStatus = haltStatus;
    }

    public String getRtoRemarks() {
        return rtoRemarks;
    }

    public void setRtoRemarks(String rtoRemarks) {
        this.rtoRemarks = rtoRemarks;
    }

    public String getBunkLiters() {
        return bunkLiters;
    }

    public void setBunkLiters(String bunkLiters) {
        this.bunkLiters = bunkLiters;
    }

    public String getEnrouteLiters() {
        return enrouteLiters;
    }

    public void setEnrouteLiters(String enrouteLiters) {
        this.enrouteLiters = enrouteLiters;
    }

    public String getGrosswt() {
        return grosswt;
    }

    public void setGrosswt(String grosswt) {
        this.grosswt = grosswt;
    }

    public String getTankCapacity() {
        return tankCapacity;
    }

    public void setTankCapacity(String tankCapacity) {
        this.tankCapacity = tankCapacity;
    }

    public String getMaxFuelPrice() {
        return maxFuelPrice;
    }

    public void setMaxFuelPrice(String maxFuelPrice) {
        this.maxFuelPrice = maxFuelPrice;
    }

    public String getVendorHaltingCharge() {
        return vendorHaltingCharge;
    }

    public void setVendorHaltingCharge(String vendorHaltingCharge) {
        this.vendorHaltingCharge = vendorHaltingCharge;
    }

    public String getContainerInsAmount() {
        return containerInsAmount;
    }

    public void setContainerInsAmount(String containerInsAmount) {
        this.containerInsAmount = containerInsAmount;
    }

    public String getMovementId() {
        return movementId;
    }

    public void setMovementId(String movementId) {
        this.movementId = movementId;
    }

    public String getHireExpenses() {
        return hireExpenses;
    }

    public void setHireExpenses(String hireExpenses) {
        this.hireExpenses = hireExpenses;
    }

    public String getHaltContractAmt() {
        return haltContractAmt;
    }

    public void setHaltContractAmt(String haltContractAmt) {
        this.haltContractAmt = haltContractAmt;
    }

    public String getClrContractAmt() {
        return clrContractAmt;
    }

    public void setClrContractAmt(String clrContractAmt) {
        this.clrContractAmt = clrContractAmt;
    }

    public String getContractRate() {
        return contractRate;
    }

    public void setContractRate(String contractRate) {
        this.contractRate = contractRate;
    }
    
       
}
