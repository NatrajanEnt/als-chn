package ets.domain.sales.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.sales.business.SalesTO;

/**
 *
 * @author vidya
 */
public class SalesDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "SalesDAO";

    public int processPnrDatFile(SalesTO salesTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        map.put("pnrId", salesTO.getPnrId());
        map.put("pnrNo", salesTO.getPnrNo());
        map.put("created", salesTO.getCreated());
        map.put("linearName", salesTO.getLinearName());
        map.put("field1", salesTO.getField1());
        map.put("field2", salesTO.getField2());
        map.put("field3", salesTO.getField3());
        map.put("field4", salesTO.getField4());
        map.put("consignee", salesTO.getConsignee());
        map.put("field5", salesTO.getField5());
        map.put("field6", salesTO.getField6());
        map.put("pName", salesTO.getpName());
        map.put("field7", salesTO.getField7());
        map.put("field8", salesTO.getField8());
        map.put("field9", salesTO.getField9());
        map.put("conNo", salesTO.getConNo());
        map.put("field10", salesTO.getField10());
        map.put("field11", salesTO.getField11());
        map.put("loadCap", salesTO.getLoadCap());
        map.put("conType", salesTO.getConType());
        map.put("Tarewgt", salesTO.getTarewgt());
        map.put("field12", salesTO.getField12());
        map.put("field13", salesTO.getField13());
        map.put("userId", userId);
        System.out.println("map = " + map);
        try {
            //////System.out.println("this is DAT File");
            status = (Integer) session.update("sales.insertDatFile", map);
            System.out.println("sd status =" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("datFile Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "datFile List", sqlException);
        }

        return status;
    }

    public int getPNRCount(String pnrNo) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int pnrCount = 0;
        map.put("pnrNo", pnrNo);
        System.out.println("map = " + map);
        try {
            pnrCount = (Integer) getSqlMapClientTemplate().queryForObject("sales.getPNRCount", map);
            System.out.println("pnrCount ------------ DAO --------- " + pnrCount);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("pnrCount Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "pnrCount", sqlException);
        }
        return pnrCount;
    }

    public int saveConsignmentNote(SalesTO consignmentNoteTO, SqlMapClient session) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        int cnoteId = 0;
        map.put("consignmentNoteNo", consignmentNoteTO.getConsignmentNoteNo());
        map.put("customerOrderRefNo", consignmentNoteTO.getCustomerOrderRefNo());
        map.put("originId", consignmentNoteTO.getOriginId());
        map.put("destinationId", consignmentNoteTO.getDestinationId());
        map.put("routeContractId", consignmentNoteTO.getRouteContractId());
        map.put("freightRate", consignmentNoteTO.getFreightRate());

        map.put("customerId", consignmentNoteTO.getCustomerId());
        map.put("customerName", consignmentNoteTO.getCustomerName());
        map.put("customerAddress", consignmentNoteTO.getCustomerAddress());
        map.put("mobile", consignmentNoteTO.getMobile());
        map.put("pincode", consignmentNoteTO.getPincode());
        map.put("phone", consignmentNoteTO.getPhone());
        map.put("email", consignmentNoteTO.getEmail());

        map.put("consignorName", consignmentNoteTO.getConsignorName());
        map.put("consignorAddress", consignmentNoteTO.getConsignorAddress());
        map.put("consignorMobile", consignmentNoteTO.getConsignorMobile());
        map.put("consignorPincode", consignmentNoteTO.getConsignorPincode());
        map.put("consignorPhone", consignmentNoteTO.getConsignorPhone());
        map.put("consignorEmail", consignmentNoteTO.getConsignorEmail());

        map.put("consigneeName", consignmentNoteTO.getConsigneeName());
        map.put("consigneeAddress", consignmentNoteTO.getConsigneeAddress());
        map.put("consigneeMobile", consignmentNoteTO.getConsigneeMobile());
        map.put("consigneePincode", consignmentNoteTO.getConsigneePincode());
        map.put("consigneePhone", consignmentNoteTO.getConsigneePhone());
        map.put("consigneeEmail", consignmentNoteTO.getConsigneeEmail());

        map.put("consignmentStatusId", "5");
        map.put("totalKM", consignmentNoteTO.getTotalKM());
        map.put("totalHours", consignmentNoteTO.getTotalHours());
        map.put("totalMinutes", consignmentNoteTO.getTotalMinutes());
        map.put("userId", consignmentNoteTO.getUserId());
        map.put("consignorId", consignmentNoteTO.getConsignorId());
        map.put("consigneeId", consignmentNoteTO.getConsigneeId());
        map.put("reportDate", consignmentNoteTO.getCreated());

        System.out.println("map = " + map);
        try {
            cnoteId = (Integer) session.insert("sales.insertConsignmentNote", map);
            System.out.println("saveConsignmentNote ------------ DAO --------- " + cnoteId);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("saveConsignmentNote Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "saveConsignmentNote", sqlException);
        }
        return cnoteId;
    }

    public ArrayList getContractDetails(String billingPartyId, String originId, String portId, String destinationId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList contractDetails = new ArrayList();
        map.put("custId", billingPartyId);
        map.put("originId", originId);
        map.put("point1Id", portId);
        map.put("destinationId", destinationId);
        System.out.println("map = " + map);
        try {
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("sales.getCustomerContractData", map);
            System.out.println("contractDetails ------------ DAO --------- " + contractDetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractDetails", sqlException);
        }
        return contractDetails;
    }

    public ArrayList getCustomerDetails(String custId) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList customerDetails = new ArrayList();
        map.put("custId", custId);
        System.out.println("map = " + map);
        try {
            customerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("sales.getCustomerDetails", map);
            System.out.println("getCustomerDetails ------------ DAO --------- " + customerDetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCustomerDetails", sqlException);
        }
        return customerDetails;
    }

    public ArrayList getPNRData(String pnrNo) throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        ArrayList pnrDetails = new ArrayList();
        map.put("pnrNo", pnrNo);
        System.out.println("map = " + map);
        try {
            pnrDetails = (ArrayList) getSqlMapClientTemplate().queryForList("sales.getPNRData", map);
            System.out.println("pnrDetails ------------ DAO --------- " + pnrDetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getPNRData Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getPNRData", sqlException);
        }
        return pnrDetails;
    }

    public int insertContainer(int consignmentOrderId, String vehicleTypeId, int containerType, String containerQty,
            String containerFreightRate, String containerNos, String linerName, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        try {
            System.out.println("containerNos:"+containerNos);
            System.out.println("linerName:"+linerName);
            String[] containerNoTemp = containerNos.split(",");
            String[] containerLinerTemp = linerName.split(",");

            map.put("consignmentId", consignmentOrderId);
            map.put("userId", userId);
            System.out.println("containerNoTemp.length:" + containerNoTemp.length);
            if (containerNoTemp.length > 0) {
                for (int j = 0; j < containerNoTemp.length; j++) {
                    map.put("vehicleTypeId", vehicleTypeId);
                    map.put("containerType", containerType);
                    map.put("containerNo", containerNoTemp[j]);
                    map.put("containerLinerId", containerLinerTemp[j]);
                    map.put("containerFreightCharges", containerFreightRate);
                    map.put("quantity", 1);
                    System.out.println("map for insert Container:" + map);
                    status = (Integer) session.update("operation.insertContainer", map);

                }
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertContainer Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertContainer", sqlException);
        }

        return status;
    }

//    public int removePnrDatFile(String pnrNo, SqlMapClient session) {
    public int removePnrDatFile(String pnrNo) {
        Map map = new HashMap();
        int status = 0;
        map.put("pnrNo", pnrNo);
        try {
            System.out.println("map for removePnrDatFile:" + map);
//            status = (Integer) session.update("sales.removePnrDatFile", map);
            status = (Integer) getSqlMapClientTemplate().update("sales.removePnrDatFile", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("removePnrDatFile Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "removePnrDatFile", sqlException);
        }

        return status;
    }

    public int insertCNoteRouteCourse(SalesTO consignmentNoteTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        map.put("userId", userId);
        try {

            map.put("consignmentId", consignmentNoteTO.getConsignmentOrderId());
            map.put("pointId", consignmentNoteTO.getOriginId());
            map.put("pointType", "Origin");
            map.put("pointAddress", "Triway CFS");
            map.put("pointSequence", "1");
            System.out.println("map for insert insertCNoteRouteCourse:" + map);
            status = (Integer) session.update("sales.insertCNoteRouteCourse", map);

            map.put("pointId", consignmentNoteTO.getPoint1Id());
            map.put("pointType", "Loading");
            map.put("pointAddress", "Port");
            map.put("pointSequence", "2");
            System.out.println("map for insert insertCNoteRouteCourse:" + map);
            status = (Integer) session.update("sales.insertCNoteRouteCourse", map);

            map.put("pointId", consignmentNoteTO.getDestinationId());
            map.put("pointType", "Drop");
            map.put("pointAddress", "Triway CFS");
            map.put("pointSequence", "3");
            System.out.println("map for insert insertCNoteRouteCourse:" + map);
            status = (Integer) session.update("sales.insertCNoteRouteCourse", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertCNoteRouteCourse Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertCNoteRouteCourse", sqlException);
        }

        return status;
    }

    public ArrayList getPNRprojection(SalesTO salesTO) {
        Map map = new HashMap();
        ArrayList PNRprojection = new ArrayList();

        System.out.println("map = " + map);

        try {
            System.out.println("this is vehicle Availability");
            PNRprojection = (ArrayList) getSqlMapClientTemplate().queryForList("sales.getPNRprojection", map);
            System.out.println(" PNRprojection =" + PNRprojection.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("PNRprojection Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "PNRprojection ", sqlException);
        }

        return PNRprojection;
    }

    public ArrayList getVehicleAvailability(SalesTO salesTO) {
        Map map = new HashMap();
        ArrayList vehicleAvailability = new ArrayList();

        System.out.println("map = " + map);

        try {
            System.out.println("this is vehicle Availability");
            vehicleAvailability = (ArrayList) getSqlMapClientTemplate().queryForList("sales.getVehicleAvailability", map);
            System.out.println(" vehicleAvailability =" + vehicleAvailability.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("vehicleAvailability Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleAvailability ", sqlException);
        }

        return vehicleAvailability;
    }

    public String getMailTemplate(String eventId, SqlMapClient session) {
        Map map = new HashMap();
        String template = "";
        map.put("eventId", eventId);
        try {
            if(session != null) {
                template = (String) session.queryForObject("sales.getMailTemplate", map);
            }else{
                template = (String) getSqlMapClientTemplate().queryForObject("sales.getMailTemplate", map);
            }
            //System.out.println("checkRouteStage " + checkRouteStage);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSMSTemplate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSMSTemplate", sqlException);
        }

        return template;
    }

    public int insertMailDetails(SalesTO salesTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int insertMailDetails = 0;
        map.put("mailTypeId", salesTO.getMailTypeId());
        map.put("mailSubjectTo", salesTO.getMailSubjectTo());
        map.put("mailSubjectCc", salesTO.getMailSubjectCc());
        map.put("mailSubjectBcc", salesTO.getMailSubjectBcc());
        map.put("mailContentTo", salesTO.getMailContentTo());
        map.put("mailContentCc", salesTO.getMailContentCc());
        map.put("mailContentBcc", salesTO.getMailContentBcc());
        map.put("mailTo", salesTO.getMailIdTo());
        map.put("mailCc", salesTO.getMailIdCc());
        map.put("mailBcc", salesTO.getMailIdBcc());
        map.put("filePath", salesTO.getFilePath());
        map.put("userId", userId);
        map.put("mailDeliveredStatus", 0);
        //System.out.println("mapSSSS = " + map);
        try {
            System.out.println("session:" + session);
            if (session != null) {
                insertMailDetails = (Integer) session.update("sales.insertMailDetails", map);
            } else {
                insertMailDetails = (Integer) getSqlMapClientTemplate().update("sales.insertMailDetails", map);
            }
            System.out.println("insertMailDetails" + insertMailDetails);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertMailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "insertMailDetails", sqlException);
        }
        map = null;

        return insertMailDetails;

    }

    public int insertMailDetails1(SalesTO salesTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int insertMailDetails = 0;
        map.put("mailTypeId", salesTO.getMailTypeId());
        map.put("mailSubjectTo", salesTO.getMailSubjectTo());
        map.put("mailSubjectCc", salesTO.getMailSubjectCc());
        map.put("mailSubjectBcc", salesTO.getMailSubjectBcc());
        map.put("mailContentTo", salesTO.getMailContentTo());
        map.put("mailContentCc", salesTO.getMailContentCc());
        map.put("mailContentBcc", salesTO.getMailContentBcc());
        map.put("mailTo", salesTO.getMailIdTo());
        map.put("mailCc", salesTO.getMailIdCc());
        map.put("mailBcc", salesTO.getMailIdBcc());
        map.put("userId", userId);
        map.put("mailDeliveredStatus", 0);
        try {

            System.out.println("map for insert Container:" + map);
            //status = (Integer) session.update("operation.insertContainer", map);
            insertMailDetails = (Integer) session.insert("sales.insertMailDetails", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertMailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertMailDetails", sqlException);
        }

        return insertMailDetails;
    }
   
    public String getMailTemplate() {
        Map map = new HashMap();
        String dayOfWeek = "";
        try {
                dayOfWeek = (String) getSqlMapClientTemplate().queryForObject("sales.getDayOfWeek", map);
                System.out.println("dayOfWeek " + dayOfWeek);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSMSTemplate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getSMSTemplate", sqlException);
        }

        return dayOfWeek;
    }
    
}
