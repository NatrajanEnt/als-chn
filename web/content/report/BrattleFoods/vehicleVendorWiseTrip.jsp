<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("please select From Date");
                    document.getElementById('fromDate').focus();
                }
                if (document.getElementById('toDate').value == '') {
                    alert("please select To Date");
                    document.getElementById('toDate').focus();
                }
                else{
                if(value == 'ExportExcel'){
                document.accountReceivable.action = '/throttle/handleVehicleVendorwiseTrip.do?param=ExportExcel';
                document.accountReceivable.submit();
                }else{
                document.accountReceivable.action = '/throttle/handleVehicleVendorwiseTrip.do?param=Search';
                document.accountReceivable.submit();
                }
                }
            }
            
             function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
            }


            function viewCustomerProfitDetails(tripIds) {
            //alert(tripIds);
            window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId='+tripIds+"&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
        </script>

<style type="text/css">





.container {width: 960px; margin: 0 auto; overflow: hidden;}
.content {width:800px; margin:0 auto; padding-top:50px;}
.contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

/* STOP ANIMATION */



/* Second Loadin Circle */

.circle1 {
	background-color: rgba(0,0,0,0);
	border:5px solid rgba(100,183,229,0.9);
	opacity:.9;
	border-left:5px solid rgba(0,0,0,0);
/*	border-right:5px solid rgba(0,0,0,0);*/
	border-radius:50px;
	/*box-shadow: 0 0 15px #2187e7; */
/*	box-shadow: 0 0 15px blue;*/
	width:40px;
	height:40px;
	margin:0 auto;
	position:relative;
	top:-50px;
	-moz-animation:spinoffPulse 1s infinite linear;
	-webkit-animation:spinoffPulse 1s infinite linear;
	-ms-animation:spinoffPulse 1s infinite linear;
	-o-animation:spinoffPulse 1s infinite linear;
}

@-moz-keyframes spinoffPulse {
	0% { -moz-transform:rotate(0deg); }
	100% { -moz-transform:rotate(360deg);  }
}
@-webkit-keyframes spinoffPulse {
	0% { -webkit-transform:rotate(0deg); }
	100% { -webkit-transform:rotate(360deg);  }
}
@-ms-keyframes spinoffPulse {
	0% { -ms-transform:rotate(0deg); }
	100% { -ms-transform:rotate(360deg);  }
}
@-o-keyframes spinoffPulse {
	0% { -o-transform:rotate(0deg); }
	100% { -o-transform:rotate(360deg);  }
}



</style>
    </head>
    <body onload="setValue();sorter.size(10);">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">VehicleVendorWiseReport</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    
                                    <tr>
<!--                                        <td>Month</td>
                                        <td>
                                            <select class="" id="monthId" name="monthId">
                                                <option value="0" selected> -Select- </option>
                                                <option value="1">JAN</option>
                                                <option value="2">FEB</option>
                                                <option value="3">MAR</option>
                                                <option value="4">APR</option>
                                                <option value="5">MAY</option>
                                                <option value="6">JUN</option>
                                                <option value="7">JUL</option>
                                                <option value="8">AUG</option>
                                                <option value="9">SEP</option>
                                                <option value="10">OCT</option>
                                                <option value="11">NOV</option>
                                                <option value="12">DEC</option>
                                            </select>
                                            <input type="text" value="<c:out value="${monthId}"/>">
                                    <script>
                                        document.getElementById("monthId").value="<c:out value="${monthId}"/>";
                                    </script>
                                        </td>-->
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                        
                                          <td><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>
                                        <td><input type="button" class="button" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

<br>
            <br>
            <c:if test = "${vehicleVendorwiseList == null && vehicleVendorwiseListSize != null}" >
                <center>
                <font color="blue">Please Wait Your Request is Processing</font>
                <div class="container">
                 <div class="content">
                <div class="circle"></div>
                <div class="circle1"></div>
                </div>
            </div>
                </center>
            </c:if>
            <br>
            <br>
            <br>

            <c:if test = "${vehicleVendorwiseList != null}" >
                <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="80" >
                            <th><h3 align="center" >S.No</h3></th>
                            <th><h3 align="center">Trailer No.</h3></th>
                            <th><h3 align="center">Vehicle Type</h3></th>
                            <th><h3 align="center">TPT</h3></th>
                            <th><h3 align="center">No. Of Trips Empty</h3></th>
                            <th><h3 align="center">No. Of Trips Loaded</h3></th>
                            <th><h3 align="center">Total No. Of Trips <br>(Container Wise, Empty & Loaded,<br> Bothway also taken as single)</h3></th>
                            <th><h3 align="center">Total Leased<br> Monthly Charges</h3></th>
                            <th><h3 align="center">Fixed Cost Per Trip <br>Per Vehicle <br>(Without Diesel & Trip Advance)</h3></th>
                            <th><h3 align="center">Average Trip Per Vehicle</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:set var="profitPercent" value="${0}"/>
                         <c:set var="tranSporter" value=""/> 
                         <c:set var="emptyTripKM" value="${0}"/> 
                         <c:set var="loadedTrip" value="${0}"/> 
                         <c:set var="vehicleTypeName" value="${20}"/> 
                         <c:set var="vehicleTypeNames" value="${40}"/> 
                         <c:set var="totalTwentyFtVehicle" value="0"/>
                         <c:set var="totalFourtyFtVehicle" value="0"/>
                         <c:set var="totalTwentyFtTrip" value="0"/>
                         <c:set var="totalFourtyFtTrip" value="0"/>
                         <c:set var="totalVehicleForTransporter" value="0"/>
                        
                        
                        <c:forEach items="${vehicleVendorwiseList}" var="csList">
                            <c:set var="totalTripNos" value="${csList.loadedTrip + totalTripNos}"/>
                               <c:set var="totalEmptyTrip" value="${csList.emptyTripKM + totalEmptyTrip}"/>
                               <c:if test="${csList.vehicleTypeId == '1058'}">
                               <c:set var="totalTwentyFtVehicle" value="${totalTwentyFtVehicle + 1}"/>
                               <c:set var="totalTwentyFtTrip" value="${totalTwentyFtTrip +csList.loadedTrip +csList.emptyTripKM }"/>
                               </c:if>
                               <c:if test="${csList.vehicleTypeId == '1059'}">
                               <c:set var="totalFourtyFtVehicle" value="${totalFourtyFtVehicle + 1}"/>
                               <c:set var="totalFourtyFtTrip" value="${totalFourtyFtTrip+csList.loadedTrip +csList.emptyTripKM}"/>
                               </c:if>
                       <c:if test = "${tranSporter != ''}" >
                       <c:if test = "${tranSporter != csList.transporter}" >
                            
                             
                             <tr height="30">

                                <td align="center"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"><b><c:out value="${tranSporter}"/> Total()</b></td>
                                <td align="left"><b><c:out value="${emptyTripKM}"/></b></td>
                                <td align="left"><b><c:out value="${loadedTrip}"/></b></td>
                                <td align="left"><b><c:out value="${emptyTripKM +loadedTrip}"/></b></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                             <c:set var="emptyTripKM" value="${0}"/>
                             <c:set var="loadedTrip" value="${0}"/>
                            </tr>
                               </c:if> 
                               </c:if> 
                               <tr height="30">

                                <td align="center"><%=sno%></td>
                                <td align="left"><c:out value="${csList.regNo}"/></td>
                                <td align="left"><c:out value="${csList.vehicleTypeName}"/></td>
                                <td align="left">
                                <c:out value="${csList.transporter}"/>
                                </td>
                                <td align="left"><c:out value="${csList.emptyTripKM}"/></td>
                                <td align="left"><c:out value="${csList.loadedTrip}"/></td>
                                <td align="left"><c:out value="${csList.emptyTripKM+csList.loadedTrip}"/></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                               
                                 <c:set var="tranSporter" value="${csList.transporter}"/>  
                                 <c:set var="emptyTripKM" value="${emptyTripKM + csList.emptyTripKM}"/>  
                                 <c:set var="loadedTrip" value="${loadedTrip + csList.loadedTrip}"/>  
                                 
                            </tr>
                               
                               
                            <%
                                       index++;
                                       sno++;
                            %>
                             
                        </c:forEach>
                           <tr height="30">

                                <td align="center"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"><b><c:out value="${tranSporter}"/> Total()</b></td>
                                <td align="left"><b><c:out value="${emptyTripKM}"/></b></td>
                                <td align="left"><b><c:out value="${loadedTrip}"/></b></td>
                                <td align="left"><b><c:out value="${emptyTripKM +loadedTrip}"/></b></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                            </tr>
                    </tbody>
                </table>
                        <br/>
            <c:if test = "${vehicleVendorwiseList != null}" >
            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                <tr height="25" align="right">
                    <td style="background-color: #6374AB; color: #ffffff">Vehicle Type</td>
                    <td  style="background-color: #6374AB; color: #ffffff">No of Vehicles</td>
                    <td style="background-color: #6374AB; color: #ffffff">No.of Trips</td>
                    <td style="background-color: #6374AB; color: #ffffff">Avg. Trips Per Vehicle</td>
                </tr>
                
              
                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff"> 20"</td>
                    <td width="100" align="center"><c:out value="${totalTwentyFtVehicle}"/></td>
                    <td width="100" align="center"><fmt:formatNumber type="number"  value="${totalTwentyFtTrip}" /></td>
                    <td width="150" align="center"><fmt:formatNumber type="number" pattern="#" value="${totalTwentyFtTrip/totalTwentyFtVehicle}" /></td>
                </tr>
            
                
                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff"> 40"</td>
                    <td width="100" align="center"><c:out value="${totalFourtyFtVehicle}"/></td>
                    <td width="100" align="center"><fmt:formatNumber type="number"  value="${totalFourtyFtTrip}" /></td>
                    <td width="150" align="center"><fmt:formatNumber type="number" pattern="#" value="${totalFourtyFtTrip/totalFourtyFtVehicle}" /></td>
                </tr>
               
            </table>
            </c:if>
             </c:if>
           
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
<!--            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            
            <table>
                <tr>
                    <td></td>
                    <td></td>
                </tr>
                
            </table>
            <br/>-->
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>