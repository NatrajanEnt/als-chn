<%--
    Document   : BPCLTransactionReport
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>

<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>


        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{
                    if(value == "ExportExcel"){
                        document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else{   
                        document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>



    </head>
    <body onload="sorter.size(1);">
        <form name="BPCLTransaction" method="post">
             <br>
             <br>
             Latest Updates
             <br>
             <br>
             <br>

            <c:if test="${latestUpdates == null}">
                <center><font color="red">No Records Found</font></center>
            </c:if>
            <c:if test="${latestUpdates != null}">
                <table align="center" border="0" id="table" class="sortable" width="100%" >
                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                            <th align="center"><h3>Status Name</h3></th>
                            <th align="center"><h3>Module Name</h3></th>
                            <th align="center"><h3>Customer Name</h3></th>
                            <th align="center"><h3>Vehicle No</h3></th>
                            <th align="center"><h3>Request Amount</h3></th>
                            <th align="center"><h3>Paid Amount</h3></th>
                            <th align="center"><h3>Route Name</h3></th>
                            <th align="center"><h3>Updated By</h3></th>
                            <th align="center"><h3>Updated On</h3></th>
                        </tr>
                    </thead>
                            
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${latestUpdates}" var="update">
                            <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                            %>
                            <tr>
                                <td width="2" class="<%=classText%>"><%=index++%></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${update.statusName}"/>&nbsp;</td>
                                <td  width="30" class="<%=classText%>"><c:out value="${update.name}"/>&nbsp;</td>
                                <td  width="30" class="<%=classText%>"><c:out value="${update.customerName}"/>&nbsp;</td>
                                <td width="30" class="<%=classText%>"><c:out value="${update.vehicleNo}"/>&nbsp;</td>
                                <td width="30" class="<%=classText%>" ><c:out value="${update.requestAmount}"/>&nbsp;</td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${update.paidAmount}"/>&nbsp;</td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${update.routeName}"/>&nbsp;</td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${update.createdBy}"/>&nbsp;</td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${update.createdDate}"/>&nbsp;</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
           
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>

