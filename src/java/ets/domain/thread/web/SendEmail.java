/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.thread.web;

/**
 *
 * @author vinoth
 */
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.util.ThrottleConstants;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


//public class TestThread extends Thread {
public class SendEmail implements Runnable {

    int updateStatus = 0;
    TripBP tripBP;
    int MailSendingId = 0;
    String SMSContactNo = "";
    String SMSContent = "";
    int smsSendingStatus = 0;
    String tripId = "";
    String lastSmsDeliveredTime = "";
    String Toemailid = "";
    String CCemailid = "";
    String BCCemailid = "";
    String Frommailid = "";
    String EmailSubject = "";
    String BodyMessage = "";
    String SMTP = "";
    String Password = "";
    int updatedStatus = 0;
    int PORT = 0;

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }


    TripTO tripTO = new TripTO();
    String name;
    public SendEmail(String name) {
        this.name = name;
    }
    public SendEmail(TripBP tripBP, String name) {
        this.tripBP = tripBP;
        this.name = name;
    }

    public SendEmail() {
    }

    @Override
    public void run() {
        try {
            SMTP = ThrottleConstants.smtpServer;
            PORT = Integer.parseInt(ThrottleConstants.smtpPort);
            Frommailid = ThrottleConstants.fromMailId;
            Password = ThrottleConstants.fromMailPassword;
            ArrayList emailList = new ArrayList();
            emailList = tripBP.getEmailList();
            Iterator itr = emailList.iterator();
            while(itr.hasNext()) {
                tripTO = (TripTO) itr.next();
                MailSendingId = Integer.parseInt(tripTO.getMailSendingId());
                EmailSubject = tripTO.getMailSubjectTo();
                BodyMessage = tripTO.getMailContentTo();
                Toemailid = tripTO.getMailIdTo();
                CCemailid = tripTO.getMailIdCc();
                BCCemailid = tripTO.getMailIdBcc();
                tripId = tripTO.getTripId();
                Properties props = System.getProperties();
                props.put("mail.smtp.starttls.enable", "true"); // added this line
                props.put("mail.smtp.host", SMTP);
                props.put("mail.smtp.user", Frommailid);
                props.put("mail.smtp.password", Password);
                props.put("mail.smtp.port", PORT);
                props.put("mail.smtp.auth", "true");
                /*
                if ("smtp.gmail.com".equalsIgnoreCase(SMTP)) {
                props.put("mail.smtp.user", Frommailid);
                props.put("mail.smtp.password", Password);
                props.put("mail.smtp.port", PORT);
                props.put("mail.smtp.auth", "true");
                } else {//godaddy server
                props.put("mail.smtp.user", "");
                props.put("mail.smtp.password", "");
                props.put("mail.smtp.port", "25");
                //props.put("mail.smtp.auth", "true");
                }
                 */
                if (Toemailid.indexOf("") >= 0 ||
                        Toemailid.indexOf("") >= 0
                        ) {
                    if (!CCemailid.equals("") && !CCemailid.equals("-")) {
                        CCemailid = CCemailid + "," + "";
                    }else{
                        CCemailid = "";
                    }
                }

                String[] to = Toemailid.split(","); // added this line



                Session session = Session.getInstance(props, new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(Frommailid, Password);
                    }
                });
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(Frommailid));

                InternetAddress[] toAddress = new InternetAddress[to.length];

                //System.out.println("Toemailid:"+Toemailid);
                //System.out.println("CCemailid:"+CCemailid);

                if (!Toemailid.equals("") && !Toemailid.equals("-")) {
                    for (int i = 0; i < to.length; i++) { // changed from a while loop
                        toAddress[i] = new InternetAddress(to[i]);
                    }

                    for (int i = 0; i < toAddress.length; i++) { // changed from a while loop
                        message.addRecipient(Message.RecipientType.TO, toAddress[i]);
                    }
                }


                ////////////// cc part////////////////////



    //            CCemailid = "";
                //CCemailid = "nithyap@entitlesolutions.com";
                if (!CCemailid.equals("-")) {
                    String[] cc = CCemailid.split(","); // added this line
                    int ccLen = cc.length;
                    InternetAddress[] ccAddress = new InternetAddress[ccLen];
                    int j = 0;
                    for (j = 0; (j < cc.length && !CCemailid.equals("")); j++) { // changed from a while loop
                        ccAddress[j] = new InternetAddress(cc[j]);
                    }
                    for (j = 0; j < ccAddress.length; j++) { // changed from a while loop
                        message.addRecipient(Message.RecipientType.CC, ccAddress[j]);
                    }
                }

    //            BCCemailid = "";
                //BCCemailid = "nithyap@entitlesolutions.com";
//                if (!BCCemailid.equals("-")) {
//                    String[] bcc = BCCemailid.split(","); // added this line
//                    int bccLen = bcc.length;
//                    InternetAddress[] bccAddress = new InternetAddress[bccLen];
//                    int j = 0;
//                    for (j = 0; (j < bcc.length && !BCCemailid.equals("")); j++) { // changed from a while loop
//                        bccAddress[j] = new InternetAddress(bcc[j]);
//                    }
//                    for (j = 0; j < bccAddress.length; j++) { // changed from a while loop
//                        message.addRecipient(Message.RecipientType.BCC, bccAddress[j]);
//                    }
//                }
                //System.out.println("Toemailid1:"+Toemailid);
                //System.out.println("CCemailid1:"+CCemailid);
                message.setSubject(EmailSubject);
                //System.out.println("BodyMessage ::::::::::::"+BodyMessage);
                message.setContent(BodyMessage, "text/html");
                Transport transport = session.getTransport("smtp");
                System.out.println("Password = " + Password);
                System.out.println("Frommailid = " + Frommailid);
                System.out.println("SMTP = " + SMTP);
                System.out.println("PORT = " + PORT);
                transport.connect(SMTP, Frommailid, Password);
                transport.sendMessage(message, message.getAllRecipients());
                int updateStatus = 0;
                tripTO.setMailDeliveredStatusId("1");
                tripTO.setMailDeliveredResponse("Delivered Sucessfully");
                updateStatus = tripBP.updateMailStatus(tripTO);
                transport.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
            try {
                tripTO.setMailDeliveredStatusId("0");
                tripTO.setMailDeliveredResponse(e.toString());
                updateStatus = tripBP.updateMailStatus(tripTO);
            } catch (FPRuntimeException ex) {
                Logger.getLogger(SendSMS.class.getName()).log(Level.SEVERE, null, ex);
            } catch (FPBusinessException ex) {
                Logger.getLogger(SendSMS.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

     public void sendExcelMail(String mailFileName, String to, String cc, String bcc, String mailContent,String bodyMsg, HSSFWorkbook hssWorkBook) {
        try {
//          Properties props = System.getProperties();
//            props.loadProperties();
//            final Properties properties = new Properties();
            SMTP = ThrottleConstants.smtpServer;
            PORT = Integer.parseInt(ThrottleConstants.smtpPort);
            Frommailid = ThrottleConstants.fromMailId;
            Password = ThrottleConstants.fromMailPassword;


            Properties props = System.getProperties();
            props.put("mail.smtp.starttls.enable", "true"); // added this line
            props.put("mail.smtp.host", SMTP);
            props.put("mail.smtp.user", Frommailid);
            props.put("mail.smtp.password", Password);
            props.put("mail.smtp.port", PORT);
            props.put("mail.smtp.auth", "true");
//            Properties props = new Properties();
//            String host = props.getProperty("mail.smtp.host");
//            String port = props.getProperty("mail.smtp.port");
//            String userid = props.getProperty("mail.smtp.user.name");
//            String password1 = props.getProperty("mail.smtp.password");
//            String smtpsAuth = props.getProperty("mail.smtp.auth");

//            String SMTP_SERVER = host;
//            String SMTP_PORT = port;
            System.out.println("SMTP_SERVER host@@@"+SMTP);
            System.out.println("SMTP_PORT port@@@s"+PORT);
            System.out.println("userid@@@"+Frommailid);
            System.out.println("password1@@@"+Password);
           System.out.println("To@@@"+to);

            final String from = Frommailid;
            final String password = Password;
            //final String from = "cgsespl@gmail.com";
            //final String password = "cgsadmin123";
            String[] recipients = null;




            String mailSubject = mailContent;
            BodyMessage = bodyMsg;
            System.out.println("BodyMessage@@@"+BodyMessage);
            // Get system properties
            //  Properties props = System.getProperties();

            // Setup mail server
//            props.put("mail.smtp.host", SMTP_SERVER);
//            props.put("mail.smtp.port", SMTP_PORT);
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.auth", smtpsAuth);
//
//
//
//            props.put("mail.smtp.starttls.enable", "true");
//            props.put("mail.smtp.host", host);
//            props.put("mail.smtp.user", userid);
//            props.put("mail.smtp.password", password);
//            props.put("mail.smtp.port", port);
//            props.put("mail.smtps.auth", smtpsAuth);


            Session session = Session.getDefaultInstance(props, null);

            // Define message
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));




            recipients = to.split(",");

            InternetAddress[] addressTo = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressTo[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.TO, addressTo);
            recipients = null;
            recipients = cc.split(",");
            InternetAddress[] addressCc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressCc[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.CC, addressCc);

            recipients = null;
            recipients = bcc.split(",");

            InternetAddress[] addressBcc = new InternetAddress[recipients.length];
            for (int i = 0; i < recipients.length; i++) {
                addressBcc[i] = new InternetAddress(recipients[i]);
            }
            message.setRecipients(Message.RecipientType.BCC, addressBcc);





            System.out.println("mailSubject in email:" + mailSubject);
            message.setSubject(mailSubject);
            message.setContent(BodyMessage, "text/html");;


            // create the message part
            javax.mail.util.ByteArrayDataSource ds = null;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            System.out.println("01");
            hssWorkBook.write(baos);
            System.out.println("02");
            byte[] bytes = baos.toByteArray();
            try {
                ds = new ByteArrayDataSource(bytes, "application/excel");
            } catch (Exception err) {
                err.printStackTrace();
            }
            System.out.println("03");
            //fill message
//            messageBodyPart.setContent(mailContent, "text/html");
            DataHandler dh = new DataHandler(ds);
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            System.out.println("04");
            messageBodyPart.setDataHandler(dh);
            System.out.println("mailFileName in email:" + mailFileName);
            messageBodyPart.setFileName(mailFileName);
            System.out.println("2");
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);
            System.out.println("3");
            // Put parts in message
            message.setContent(multipart);
            System.out.println("4");
            // Send the message
            Transport transport = session.getTransport("smtp");
            System.out.println("host:" + SMTP);
            System.out.println("userid:" + Frommailid);
            System.out.println("password:" + Password);
            transport.connect(SMTP,PORT, Frommailid, Password);
            transport.sendMessage(message, message.getAllRecipients());
            transport.close();


            //Transport.send(message);
            System.out.println("after send");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("in excep:" + e.getMessage());
            System.out.println("in excep1:" + e.toString());
        }
    }


}
