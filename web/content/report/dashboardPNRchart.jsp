<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page import="java.text.SimpleDateFormat" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<script type="text/javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<style>

    table.round {
        font-family: arial, sans-serif;
        border-collapse: collapse;

        width: 50%;



    }

    td, th {

        border: 1px solid #dddddd;
        text-align: left;
        padding: 8px;
    }
    td.roundTr,tr.roundTr{
        text-align: left;
        padding: 8px;

    }
    table.round1{
        border-radius: 25px;
        border: 2px solid #73AD21;
        padding: 20px;
        width: 200px;
        height: 150px;
    }

    
    body{    background-color: rgba(136, 178, 201, 1); }
    
    
table {background-color: transparent; margin:30px auto; border-collapse: separate; border-spacing: 0; }

tr td {
    background-color: #fff;
    border: solid 1px #000;
    border-style: none solid solid none;
    padding: 20px;
}

tr:first-child td:first-child {
    -moz-border-radius-topleft: 7px;
    -webkit-border-top-left-radius: 7px;
    border-top-left-radius: 7px;
    box-shadow: -5px -5px 0px 5px rgba(255, 255, 255, 0.4), 0px 4px 20px rgba(0, 0, 0, 0.92);
}

tr:first-child td:last-child {
    -moz-border-radius-topright: 7px;
    -webkit-border-top-right-radius: 7px;
    border-top-right-radius: 7px;
     box-shadow: 5px -5px -0px 5px rgba(255, 255, 255, 0.4),  0px 4px 20px rgba(0, 0, 0, 0.92);
}

tr:last-child td:first-child {
    -moz-border-radius-bottomleft: 7px;
    -webkit-border-bottom-left-radius: 7px;
    border-bottom-left-radius: 7px;
    box-shadow: -5px 5px 0px 5px rgba(255, 255, 255, 0.4), 0px 4px 20px rgba(0, 0, 0, 0.92);
}

tr:last-child td:last-child {
    -moz-border-radius-bottomright: 7px;
    -webkit-border-bottom-right-radius: 7px;
    border-bottom-right-radius: 7px;
     box-shadow: 5px 5px 0px 5px rgba(255, 255, 255, 0.4), 0px 4px 20px rgba(0, 0, 0, 0.92);
}

tr:first-child td {
    border-top-style: solid;
}

tr td:first-child {
    border-left-style: solid;
}


</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">Loading & Unloading</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body >
<c:set var="totalVolume" value="${0 }"/>
<c:set var="finalTotalVolume" value="${0 }"/>
                <form name="trip" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <br>
                    <div>   
                        <table class="table table-info mb30 table-bordered" align= "center" style="width:80%;">
                            <tr    ><td align="center" colSpan="8" style="background-color:#AAE4F1"><font size="5" color="blue"><b><center>Fleet Strength</center></b></font></td></tr>
                                    <table align = "center" class="table table-info mb30 table-bordered" align= "center"  style="width:80%;">

                                        <tr > 
                                            <c:if test="${vehicleModelList != null}">
                                                <th style="color:slateblue;color:white;font-weight: bold;background-color:#5BC0DE;font-size:15px;"> Size and Axle Type </th>
                                                    <c:forEach items="${vehicleModelList}" var="product">
                                                    <th style="color:slateblue;color:white;font-weight: bold;background-color:#5BC0DE;font-size:15px;"> <c:out value="${product.modelName}"/> </th>
                                                    </c:forEach>
                                                    <th style="color:slateblue;color:white;font-weight: bold;background-color:#5BC0DE;font-size:15px;">Total</th>
                                                </c:if>
                                        </tr>

                                        <c:if test="${fleetCenterList != null}">
                                            <c:forEach items="${fleetCenterList}" var="comp">
                                                <tr>
                                                    <th colspan="10" align="center" style="color:DodgerBlue;"> <c:out value="${comp.companyName}"/></th>
                                                </tr>

                                                <c:if test="${vehicleTypeList != null}">
                                                    <c:forEach items="${vehicleTypeList}" var="vehType">  
                                                        <tr>
                                                            <td> <c:out value="${vehType.vehicleTypeName}"/></td>

                                                            <c:if test="${vehicleModelList != null}">
                                                                <c:if test="${fleetStrengthList != null}">
                                                                    <c:forEach items="${vehicleModelList}" var="vm">
                                                                        <c:forEach items="${fleetStrengthList}" var="fleetCnt">
                                                                            <c:if test="${fleetCnt.fleetCenterId== comp.compId 
                                                                                          && fleetCnt.modelId==vm.modelName
                                                                                          && fleetCnt.vehicletype== vehType.vehicleTypeId}">
                                                                                <td align="center">&emsp;&emsp; <c:out value="${fleetCnt.vehCount}"/></td>
                                                                                    <c:set var="totalVolume" value="${totalVolume + fleetCnt.vehCount }"/>
                                                                                    <c:set var="finalTotalVolume" value="${finalTotalVolume + fleetCnt.vehCount }"/>

                                                                            </c:if>        
                                                                        </c:forEach>
                                                                    </c:forEach>
                                                                        
                                                                </c:if> 
                                                            </c:if>
                                                            <td> &emsp;&emsp;<c:out value="${totalVolume}"/>  </td>
                                                            <c:set var="totalVolume" value="${0}"/>
                                                        </tr>

                                                    </c:forEach>
                                                </c:if>
                                                        <tr >
                                                            <td colspan='<c:out value="${vehicleModelListSize + 1}"/>' style="text-align:right;" color="green"><b>Total&emsp;&emsp;</b></td>
                                                            <td><font size="4" color="green"> &emsp;&emsp;<b><c:out value="${finalTotalVolume}"/></b></font></td>
                                                            <c:set var="finalTotalVolume" value="${0}"/>
                                                        </tr>


                                            </c:forEach>
                                        </c:if>

                                    </table>
                                    <BR><BR>
                        </table>

                    </div>   

                </form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>