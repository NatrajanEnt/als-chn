<%--
    Document   : CompanyWiseProfitability
    Created on : Oct 31, 2013, 11:09:54 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

    </head>
    <script>
        function submitPage() {
            document.CNoteSearch.action = "/throttle/BrattleFoods/SearchCNoteReport.jsp";
            document.CNoteSearch.submit();
        }
        function excelExport() {
            document.CNoteSearch.action = "/throttle/BrattleFoods/CNoteReportExcel.jsp";
            document.CNoteSearch.submit();
        }
    </script>
    <body>
        <%
            String menuPath = "Consignment Note >> View / Edit";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="CNoteSearch" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">C NOTE VIEW/EDIT</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td >Customer Name</td>
                                        <td><input type="text" class="form-control" name="CustomerName" id="CustomerName" style="width: 110px"/></td>
                                        <td>Service Type</td>
                                        <td>  <select name="Service"  id="Service" class="form-control" style="width:110px;" >
                                                <option value="o">--Select--</option>
                                                <option value="1">FLT</option>
                                                <option value="2">LTL</option>
                                            </select>
                                        </td>
                                        <td>Business Type</td>
                                        <td>
                                            <select name="Business"  id="Business" class="form-control" style="width:120px;" >
                                                <option value="o">--Select--</option>
                                                <option value="1">Primary </option>
                                                <option value="2">Secondary</option>
                                            </select>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td><select name="status"  id="status" class="form-control" style="width:120px;" >
                                                <option value="o">--Select--</option>
                                                <option value="1">Open</option>
                                                <option value="2">Secondary</option>
                                            </select>
                                        </td>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                    </tr>
                                    <tr align="center" >
                                        <td colspan="6"><input type="button" class="button"   value="Search" onclick="submitPage()"></td>
                                        <!--<td><input type="button" class="button"   value="Generate Excel" onclick="excelExport()"></td>-->
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
