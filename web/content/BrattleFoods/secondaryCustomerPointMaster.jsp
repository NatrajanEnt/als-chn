<%--
    Document   : cityMaster
    Created on : Dec 3, 2013, 10:43:13 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?&sensor=true"></script>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<script type="text/javascript">


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCity.do",
                    dataType: "json",
                    data: {
                        cityName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#cityId').val('');
                            $('#cityName').val('');
                        }
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                //var tmp = value.split('-');
                $('#cityId').val(id);
                $('#cityName').val(value);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var value = item.Name;
            var id = item.Id;
            var itemVal = '<font color="green">' + value + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });

    function citysubmit() {
        if ($("#pointName").val() == "") {
            alert("point name should not empty");
            $("#pointName").focus();
        } else if ($("#pointAddress").val() == "") {
            alert("point address should not empty");
            $("#pointAddress").focus();
        } else if ($("#cityId").val() == "" && $("#cityName").val() == "") {
            alert("city name should not empty");
            $("#cityName").focus();
        } else if ($("#cityId").val() == "" && $("#cityName").val() != "") {
            alert("city name should not empty");
            $("#cityName").focus();
        } else if ($("#latitudePosition").val() == "") {
            alert("lat position should not empty");
            $("#latitudePosition").focus();
        } else if ($("#longitudePosition").val() == "") {
            alert("long position should not empty");
            $("#longitudePosition").focus();
        } else {
            document.secondaryPointMaster.action = "/throttle/saveSecondaryCustomerPointMaster.do";
            document.secondaryPointMaster.submit();
        }
    }

    function setValues(sno, pointId, pointName, pointType, pointAddress, cityId, cityName, latitudePosition, longitudePosition, status) {
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("pointId").value = pointId;
        document.getElementById("pointName").value = pointName;
        document.getElementById("pointType").value = pointType;
        document.getElementById("pointAddress").value = pointAddress;
        document.getElementById("cityId").value = cityId;
        document.getElementById("cityName").value = cityName;
        document.getElementById("latitudePosition").value = latitudePosition;
        document.getElementById("longitudePosition").value = longitudePosition;
        document.getElementById("status").value = status;
    }





//
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }

    var httpRequest;
    function checkPointName() {

        var pointName = document.getElementById('pointName').value;
        var customerId = $("#customerId").val();
        var url = '/throttle/checkPointName.do?pointName=' + pointName + "&customerId=" + customerId;
        if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function() {
            processRequest();
        };
        httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#pointNameStatus").text(val);
                } else {
                    $("#nameStatus").hide();
                    $("#pointNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }







    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }


    function extractNumber(obj, decimalPlaces, allowNegative)
{
	var temp = obj.value;

	// avoid changing things if already formatted correctly
	var reg0Str = '[0-9]*';
	if (decimalPlaces > 0) {
		reg0Str += '\\.?[0-9]{0,' + decimalPlaces + '}';
	} else if (decimalPlaces < 0) {
		reg0Str += '\\.?[0-9]*';
	}
	reg0Str = allowNegative ? '^-?' + reg0Str : '^' + reg0Str;
	reg0Str = reg0Str + '$';
	var reg0 = new RegExp(reg0Str);
	if (reg0.test(temp)) return true;

	// first replace all non numbers
	var reg1Str = '[^0-9' + (decimalPlaces != 0 ? '.' : '') + (allowNegative ? '-' : '') + ']';
	var reg1 = new RegExp(reg1Str, 'g');
	temp = temp.replace(reg1, '');

	if (allowNegative) {
		// replace extra negative
		var hasNegative = temp.length > 0 && temp.charAt(0) == '-';
		var reg2 = /-/g;
		temp = temp.replace(reg2, '');
		if (hasNegative) temp = '-' + temp;
	}

	if (decimalPlaces != 0) {
		var reg3 = /\./g;
		var reg3Array = reg3.exec(temp);
		if (reg3Array != null) {
			// keep only first occurrence of .
			//  and the number of places specified by decimalPlaces or the entire string if decimalPlaces < 0
			var reg3Right = temp.substring(reg3Array.index + reg3Array[0].length);
			reg3Right = reg3Right.replace(reg3, '');
			reg3Right = decimalPlaces > 0 ? reg3Right.substring(0, decimalPlaces) : reg3Right;
			temp = temp.substring(0,reg3Array.index) + '.' + reg3Right;
		}
	}

	obj.value = temp;
}
function blockNonNumbers(obj, e, allowDecimal, allowNegative)
{
	var key;
	var isCtrl = false;
	var keychar;
	var reg;

	if(window.event) {
		key = e.keyCode;
		isCtrl = window.event.ctrlKey
	}
	else if(e.which) {
		key = e.which;
		isCtrl = e.ctrlKey;
	}

	if (isNaN(key)) return true;

	keychar = String.fromCharCode(key);

	// check for backspace or delete, or if Ctrl was pressed
	if (key == 8 || isCtrl)
	{
		return true;
	}

	reg = /\d/;
	var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
	var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

	return isFirstN || isFirstD || reg.test(keychar);
}


function codeAddress() {
    var address = document.getElementById('address').value;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        map.setCenter(results[0].geometry.location);
        var marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location
        });
        document.getElementById("latitudePosition").value = marker.getPosition().lat();
        document.getElementById("longitudePosition").value = marker.getPosition().lng();

        google.maps.event.addListener(marker, 'dragend', function (event) {
            document.getElementById("latitudePosition").value = this.getPosition().lat();
            document.getElementById("longitudePosition").value = this.getPosition().lng();
        });
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>City Master Brattle Food</title>
    </head>
    <!--<body onload="document.cityMaster.cityName.focus();">-->
    <body onload="document.secondaryPointMaster.pointName.focus();">

        <form name="secondaryPointMaster"  method="POST">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="pointId" id="pointId" value=""  />
                <tr>
                <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="contenthead" colspan="4" >Secondary Customer Points Master</td>
                    </tr>
                    <tr>
                        <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="pointNameStatus" style="color: red"></label></td>
                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Customer Name</td>
                        <td class="text1"><input type="hidden" name="customerId" id="customerId" class="textbox" value="<c:out value="${customerId}"/>"><input type="text" readonly name="customerName" id="customerName" class="textbox" value="<c:out value="${customerName}"/>"></td>
                        <td class="text1"></td>
                        <td class="text1"></td>
                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Point Name</td>
                        <td class="text1"><input type="text" name="pointName" id="pointName" class="textbox"  autocomplete="off" maxlength="100" onchange="checkPointName(this.value);"></td>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Point Type</td>
                        <td class="text1"><select name="pointType" id="pointType">
                                <option value="pickup">PickUp</option>
                                <option value="drop">Drop</option>
                            </select>
                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Point Address</td>
                        <td class="text1"><input type="text" name="pointAddress" id="pointAddress" class="textbox"   autocomplete="off" maxlength="200"></td>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>city Name</td>
                        <td class="text1"><input type="hidden" name="cityId" id="cityId" value="<c:out value="${cityId}"/>" class="textbox"><input type="text" name="cityName" id="cityName" value="<c:out value="${cityName}"/>" class="textbox" onchange="validateCityName(this.value);"></td>
                    <script>
                        function validateCityName(value) {
                            if (value == "") {
                                $("#cityId").val('');
                            }
                        }
                    </script>

                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Latitude Position</td>
                        <td class="text1"><input type="text" name="latitudePosition" id="latitudePosition" class="textbox"   autocomplete="off" maxlength="50" onblur="extractNumber(this,5,false);" onkeyup="extractNumber(this,5,false);" onkeypress="return blockNonNumbers(this, event, true, false);" ></td>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Longitude Position</td>
                        <td class="text1"><input type="text" name="longitudePosition" id="longitudePosition" value="" class="textbox" autocomplete="off" maxlength="50" onblur="extractNumber(this,5,false);" onkeyup="extractNumber(this,5,false);" onkeypress="return blockNonNumbers(this, event, true, false);"></td>
                    </tr>
                    <tr>

                        <td class="text2">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                        <td class="text2">
                            <select  align="center" class="textbox" name="status" id="status" >
                                <option value='Y'>Active</option>
                                <option value='N' id="inActive" style="display: none">In-Active</option>
                            </select>
                        </td>
                        <td class="text2">&nbsp;&nbsp;</td>
                        <td class="text2">&nbsp;&nbsp;</td>
                    </tr>


                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="citysubmit()">


                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>



            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>customer Name </h3></th>
                        <th><h3>Point Name </h3></th>
                        <th><h3>Point Type</h3></th>
                        <th><h3>Point Address</h3></th>
                        <th><h3>city Name</h3></th>
                        <th><h3>Latitude Position</h3></th>
                        <th><h3>Longitude Position</h3></th>
                        <th><h3>status</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${SecondaryCustomerPointsList != null}">
                        <c:forEach items="${SecondaryCustomerPointsList}" var="cml">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno + 1%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${cml.customerName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${cml.pointName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${cml.pointType}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${cml.pointAddress}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${cml.cityName}"/></td>
                                <td class="<%=className%>"  align="left"><c:out value="${cml.latitudePosition}"/></td>
                                <td class="<%=className%>"  align="left"><c:out value="${cml.longitudePosition}"/></td>
                                <td class="<%=className%>"  align="left"><c:out value="${cml.status}"/></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${cml.pointId}" />', '<c:out value="${cml.pointName}" />', '<c:out value="${cml.pointType}" />', '<c:out value="${cml.pointAddress}" />', '<c:out value="${cml.cityId}" />', '<c:out value="${cml.cityName}" />', '<c:out value="${cml.latitudePosition}" />', '<c:out value="${cml.longitudePosition}" />', '<c:out value="${cml.status}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
            </table>



            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>