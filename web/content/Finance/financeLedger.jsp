<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.LedgerMaster"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.LedgerMaster"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form method="post" action="/throttle/addLedgerPage.do">
                    <%@ include file="/content/common/message.jsp" %>
                    <!--<div style="height:360px;overflow:scroll;overflow-y:scroll;overflow-x:hidden;">-->
                    <table class="table table-info mb30 table-hover">
                        <c:if test = "${ledgerLists != null}" >
                            <thead>
                                <tr height="30">
                                    <th  align="left"  scope="col"><b><spring:message code="finance.label.Sno"  text="default text"/></b></th>
                                    <th  align="left"  scope="col"><b><spring:message code="finance.label.LedgerCode"  text="default text"/></b></th>
                                    <th  align="left"  scope="col"><b><spring:message code="finance.label.LedgerName"  text="default text"/></b></th>
                                    <th  align="left"  scope="col"><b><spring:message code="finance.label.GroupName"  text="default text"/></b></th>
                                    <th  align="left"  scope="col"><b><spring:message code="finance.label.OpeningBalance"  text="default text"/></b></th>
                                    <th  align="left"  scope="col"><b><spring:message code="finance.label.ActiveStatus"  text="default text"/></b></th>
                                    <th  align="left"  scope="col"><b><spring:message code="finance.label.Edit"  text="default text"/></b></th>
                                </tr>
                            </thead>
                            <% int index = 0;%>
                            <c:forEach items="${ledgerLists}" var="LL">
                                <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text1";
                                            } else {
                                                classText = "text2";
                                            }
                                %>
                                <tr height="30">
                                    <td   align="left"> <%= index + 1%> </td>
                                    <td  align="left"> <c:out value="${LL.ledgerCode}" /></td>
                                    <td   align="left"> <c:out value="${LL.ledgerName}"/> </td>
                                    <td   align="left"> <c:out value="${LL.groupname}"/> </td>
                                    <td   align="left"> <c:out value="${LL.openingBalance}"/> </td>
                                    <td   align="left"> <c:out value="${LL.active_ind}"/> </td>
                                    <td  align="left"> <a href="/throttle/alterLedgerDetail.do?ledgerID=<c:out value='${LL.ledgerID}' />" > <spring:message code="finance.label.Edit"  text="default text"/> </a> </td>
                                </tr>
                                <% index++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <!--</div>-->
                    <center><input type="submit" class="btn btn-success" value="<spring:message code="finance.label.Add"  text="default text"/>"/></center>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

