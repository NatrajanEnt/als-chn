<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if(value == 'ExportExcel'){
                document.accountReceivable.action = '/throttle/handleAccountsReceivable.do?param=ExportExcel';
                document.accountReceivable.submit();
                }else{
                document.accountReceivable.action = '/throttle/handleAccountsReceivable.do?param=Search';
                document.accountReceivable.submit();
                }
            }
            
            function collectBill(customerId){
                document.accountReceivable.action = '/throttle/handleCollectBillAmount.do?customerId='+customerId+'';
                document.accountReceivable.submit();
            }
            
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
            }
            
            function checkAmount(invoiceAmount,sno,invoiceCode){
//                alert(invoiceAmount);
//                alert(sno);
                  var payAmount = document.getElementById("payAmount"+sno).value;
                  if(payAmount > invoiceAmount){
                      alert("pay amount is greater than invoice amount for invoice no : "+invoiceCode);
                      document.getElementById("payAmount"+sno).value = "";
                  }else{
                      
                  }
            }
            
         function viewInvoiceDetails(customerId) {
         var fromDate = document.getElementById("fromDate").value;
         var toDate = document.getElementById("toDate").value;
         var invoiceType = document.getElementById("invoiceType").value;
            window.open('/throttle/handleAccountsReceivableDetails.do?customerId='+customerId+'&param=Search&fromDate='+fromDate+'&toDate='+toDate, 'PopupPage', 'height = 800, width = 1150, scrollbars = yes, resizable = yes');
        }
        
        function collectAmount(){
            document.accountReceivable.action = '/throttle/handleCollectBillAmount.do?customerId='+customerId+'';
            document.accountReceivable.submit();
        }
        </script>
<style type="text/css">





.container {width: 960px; margin: 0 auto; overflow: hidden;}
.content {width:800px; margin:0 auto; padding-top:50px;}
.contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

/* STOP ANIMATION */



/* Second Loadin Circle */

.circle1 {
	background-color: rgba(0,0,0,0);
	border:5px solid rgba(100,183,229,0.9);
	opacity:.9;
	border-left:5px solid rgba(0,0,0,0);
/*	border-right:5px solid rgba(0,0,0,0);*/
	border-radius:50px;
	/*box-shadow: 0 0 15px #2187e7; */
/*	box-shadow: 0 0 15px blue;*/
	width:40px;
	height:40px;
	margin:0 auto;
	position:relative;
	top:-50px;
	-moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
	-ms-animation:spinoffPulse 1s infinite linear;
	-o-animation:spinoffPulse 1s infinite linear;
}

@-moz-keyframes spinoffPulse {
	0% { -moz-transform:rotate(0deg); }
	100% { -moz-transform:rotate(360deg);  }
}
@-webkit-keyframes spinoffPulse {
	0% { -webkit-transform:rotate(0deg); }
	100% { -webkit-transform:rotate(360deg);  }
}
@-ms-keyframes spinoffPulse {
	0% { -ms-transform:rotate(0deg); }
	100% { -ms-transform:rotate(360deg);  }
}
@-o-keyframes spinoffPulse {
	0% { -o-transform:rotate(0deg); }
	100% { -o-transform:rotate(360deg);  }
}



</style>
    </head>
    <body onload="setValue();">
        <form name="accountReceivable" action=""  method="post">
            Customer Payments
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            
           
            <c:if test = "${customerPaymentDetails != null}" >
                <table style="width: 1100px" align="center" border="0" id="table1" class="sortable">
                    <thead>
                        <tr height="80">
                            <th><h3>S.No</h3></th>
                            <th><h3>Customer Name</h3></th>
                            <th><h3>Bill No</h3></th>
                            <th><h3>Invoice No</h3></th>
                            <th><h3>Invoice Date</h3></th>
                            <th><h3>Invoice Amount</h3></th>
                            <th><h3>Grand Total</h3></th>
                            <th><h3>Invoice Status</h3></th>
                            <th><h3>Pay Amount</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${customerPaymentDetails}" var="customer">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                             
                                    <tr height="30">
                                    <td align="left" class="text2"><%=sno%></td>
                                    <td align="left" class="text2"><c:out value="${customer.customerName}"/> </td>
                                    <td align="left" class="text2"><c:out value="${customer.invoiceCode}"/> </td>
                                    <td align="left" class="text2"><c:out value="${customer.invoiceNo}"/> </td>
                                    <td align="left" class="text2"><c:out value="${customer.invoiceDate}"/> </td>
                                    <td align="left" class="text2"><c:out value="${customer.invoiceAmount}"/> </td>
                                    <td align="left" class="text2"><c:out value="${customer.grandTotal}"/> </td>
                                    <td align="left" class="text2"><c:out value="${customer.invoiceStatus}"/> </td>
                                    <td align="left" class="text2"><input type="text" name="payAmount" id="payAmount<%=sno-1%>" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="checkAmount('<c:out value="${customer.invoiceAmount}"/>',<%=sno-1%>,'<c:out value="${customer.invoiceCode}"/>');"/></td>
                                   
                                    </tr>
                                     <%
                                       index++;
                                       sno++;
                                    %>
                                
                           
                        </c:forEach>

                    </tbody>
                </table>
                        <br>
                        <br>
                        <center><input type="button" class="button" name="Save" value="Save" onClick="collectAmount();" /> </center>
                        <br>
                        <br>
                        <br>
                        <br>
            
          <script language="javascript" type="text/javascript">
                setFilterGrid("table1");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
<!--                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>-->
<!--                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>-->
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </c:if>
    </body>    
    
    
</html>