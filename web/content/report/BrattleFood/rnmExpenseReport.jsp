<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">العربية</a>
              </span>-->
    <script type="text/javascript">
        function submitPage(value) {
            if (value == 'ExportExcel') {
                document.fcWiseTripSummary.action = '/throttle/handleRNMExpenseReportExcel.do?param=ExportExcel';
                document.fcWiseTripSummary.submit();
            } else {
                document.fcWiseTripSummary.action = '/throttle/handleRNMExpenseReportExcel.do?param=Search';
                document.fcWiseTripSummary.submit();
            }
        }
    </script>


    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.RNMSpentAnalysisReport" text="default text"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="operations.reports.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.RNMSpentAnalysisReport" text="default text"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body>
                    <form name="fcWiseTripSummary" method="post">
                        <%--<%@ include file="/content/common/path.jsp" %>--%>
                        <!--            <%@ include file="/content/common/message.jsp"%>
                         <table class="table table-bordered" id="report" >
                                                <tr height="30" id="index" >
                                                    <td colspan="4"  style="background-color:#5BC0DE;">
                                                        
                                                <div class="tabs" align="left" style="width:850;">
                                                        <b><spring:message code="operations.reports.label.RNMSpentAnalysisReport" text="default text"/></b>
                                                </td>
                                    </tr>
                                                    <div id="first">-->
                        <td style="border-color:#5BC0DE;padding:16px;">
                            <table  class="table table-info mb30 table-hover">
                                <thead><tr><th colspan="3"><spring:message code="operations.reports.label.RNMSpentAnalysisReport" text="default text"/></th></tr></thead>
                                <!--        <tr>
                                            <td><font color="red">*</font>From Date</td>
                                            <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                            <td><font color="red">*</font>To Date</td>
                                            <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                        </tr>   -->
                                <tr>
                                    <td><font color="red">*</font><spring:message code="operations.reports.label.Type" text="default text"/>
                                    </td>
                                    <td height="30">
                                        <select id="type"  style="width:260px;height:40px;" class="form-control" name="type">
                                            <option value="2"><spring:message code="operations.reports.label.Primary" text="default text"/>
                                            </option>
                                            <option value="1"><spring:message code="operations.reports.label.Secondary" text="default text"/>
                                            </option>
                                        </select>
                                    </td>
                                    <!--                                    </tr>
                                                                        <tr>-->
                                    <td ><input type="button" class="btn btn-success" name="search" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>
                                                ">
                                        <input type="button" class="btn btn-success" name="ExportExcel" onclick="submitPage(this.name);" value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>
                                               "></td>
                                </tr>

                            </table>
                            <!--                    </td>
                                        </table>
                            
                                                                
                                                 <br/>               
                            -->

                            <c:if test="${vehicleList != null }">
                                <table class="table table-info mb30 table-hover" id="table" class="sortable"  >
                                    <thead>
                                        <tr >
                                            <th ><spring:message code="operations.reports.label.SNo" text="default text"/>
                                            </th>
                                            <th ><spring:message code="operations.reports.label.VehicleNo" text="default text"/>
                                            </th>
                                            <th colspan="3" align="center" ><center><c:out value="${firstmonth}"/></center></th>
                                    <th colspan="3" align="center"  ><center><c:out value="${secondMonth}"/></center></th>
                                    <th colspan="3" align="center"  ><center><c:out value="${thirdMonth}"/></center></th>



                                    <!--                    <th colspan="3" >Second month</th>
                                                        <th colspan="3">Third month</th>-->
                                    </tr>
                                    <tr style="border-top:1px solid white">
                                        <th></th>
                                        <th></th>

                                        <th ><spring:message code="operations.reports.label.Container" text="default text"/>
                                        </th>
                                        <th ><spring:message code="operations.reports.label.Chassis" text="default text"/>
                                        </th>
                                        <th ><spring:message code="operations.reports.label.Reffer" text="default text"/>
                                        </th>

                                        <th ><spring:message code="operations.reports.label.Container" text="default text"/>
                                        </th>
                                        <th ><spring:message code="operations.reports.label.Chassis" text="default text"/>
                                        </th>
                                        <th ><spring:message code="operations.reports.label.Reffer" text="default text"/>
                                        </th>

                                        <th ><spring:message code="operations.reports.label.Container" text="default text"/>
                                        </th>
                                        <th ><spring:message code="operations.reports.label.Chassis" text="default text"/>
                                        </th>
                                        <th ><spring:message code="operations.reports.label.Reffer" text="default text"/>
                                        </th>
                                        <!--                    <th>Container</th>
                                                            <th>Chesis</th>
                                                            <th>Reffer</th>
                                                            <th>Container</th>
                                                            <th>Chesis</th>
                                                            <th>Reffer</th>-->
                                    </tr>
                                    </thead>
                                    <tbody>


                                        <% int index = 1;%>
                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                        %>


                                        <c:forEach items="${vehicleList}" var="vehicle">
                                            <tr>
                                                <td width="10" ><%=index++%></td>
                                                <td  width="80" ><c:out value="${vehicle.regNo}"/></td>
                                                <c:forEach items="${RnMExpenseList1}" var="expense1">
                                                    <c:if test="${vehicle.vehicleId == expense1.vehicleId  }">
                                                        <td  width="80" >
                                                <fmt:formatNumber pattern="##0.00" value="${expense1.chasisAmount1}"/>
                                        </td>
                                        <td width="80" >
                                        <fmt:formatNumber pattern="##0.00" value="${expense1.referAmount1}"/>
                                        </td>
                                        <td  width="80" >
                                        <fmt:formatNumber pattern="##0.00" value="${expense1.containerAmount1}"/>
                                        </td>
                                    </c:if>
                                </c:forEach>
                                <c:forEach items="${RnMExpenseList2}" var="expense2">
                                    <c:if test="${vehicle.vehicleId == expense2.vehicleId  }">
                                        <td  width="80" >
                                        <fmt:formatNumber pattern="##0.00" value="${expense2.chasisAmount2}"/>
                                        </td>
                                        <td  width="80" >
                                        <fmt:formatNumber pattern="##0.00" value="${expense2.referAmount2}"/>
                                        </td>
                                        <td  width="80" >
                                        <fmt:formatNumber pattern="##0.00" value="${expense2.containerAmount2}"/>
                                        </td>
                                    </c:if>
                                </c:forEach>
                                <c:forEach items="${RnMExpenseList3}" var="expense3">
                                    <c:if test="${vehicle.vehicleId == expense3.vehicleId  }">
                                        <td  width="80" >
                                        <fmt:formatNumber pattern="##0.00" value="${expense3.chasisAmount3}"/>
                                        </td>
                                        <td  width="80" >
                                        <fmt:formatNumber pattern="##0.00" value="${expense3.referAmount3}"/>
                                        </td>
                                        <td  width="80" >
                                        <fmt:formatNumber pattern="##0.00" value="${expense3.containerAmount3}"/>
                                        </td>
                                    </c:if>
                                </c:forEach>
                                </tr>
                            </c:forEach>
                            </tbody>
                            </table>
                        </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

