
<%@page import="java.text.SimpleDateFormat" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <script type="text/javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

    <!--        <script type="text/javascript" src="/throttle/js/suest"></script>
            <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
            <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
            <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
            <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

    <!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
            <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
            <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>






    <style type="text/css" title="currentStyle">
        @import "/throttle/css/layout-styles.css";
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->
    <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>


    <script  type="text/javascript" src="js/jq-ac-script.js"></script>





    <script type="text/javascript">
        function submitPage() {

            if (isEmpty(document.getElementById("vehicleloadtemperature").value)) {
                alert('please enter loading temperature ');
                document.getElementById("vehicleloadtemperature").focus();
            }

            else {
                document.endTripSheet.action = '/throttle/saveLoadingUnloadingDetails.do';
                document.endTripSheet.submit();

            }
        }
        
        
        $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

    </script>

</head>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">Unloading Details</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body  >

                <form name="endTripSheet" method="post">
                    <%
               Date today = new Date();
               SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
               String endDate = sdf.format(today);
                    %>
                    <%--<%@ include file="/content/common/path.jsp" %>--%>

                    
                  <table width="100%">
                        <% int loopCntr = 0;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <% if(loopCntr == 0) {%>
                                <tr bgcolor="skyblue" style="height:40px;">
                                    <td>&emsp;</td>
                                    <td  >Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                                    <td  >Driver: <c:out value="${trip.driverName}" /></td>
                                    <td  >Trip Code: <c:out value="${trip.tripCode}"/></td>
                                    <td  >Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                                    <td  >Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                                    <!--<td  >Status: <c:out value="${trip.status}"/></td>-->
                                <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                                <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>' />
                                <input type="hidden" name="statusId" value='<c:out value="${statusId}"/>' />
                                </tr>
                                <% }%>
                                <% loopCntr++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                        
                        <br>
                        <br>  <% int index10 = 1; int containerCount = 0; %>
                          <c:if test = "${containerDetails != null}" >
                                       <table class="table table-info mb30 table-border"  width="100%" >
                                           <tr  bgcolor="skyblue" id="index" height="30">
                                               <td   >S No</td>
                                               <td   >Container Type</td>
                                               <td   >Container No</td>
                                               <td   >Wgt(Tonnage)</td>
                                               <td   >Goods Desc</td>
                                               <td   >Seal No</td>
                                           </tr>
                                           <c:forEach items="${containerDetails}" var="container">
                                               <%
                                                      containerCount++;
                                                      String classText = "";
                                                      int oddEven1 = index10 % 2;
                                                      if (oddEven1 > 0) {
                                                          classText = "text1";
                                                      } else {
                                                          classText = "text2";
                                                      }
                                               %>
                                               <tr >
                                                   <td  > &emsp; &emsp;<%=index10++%></td>
                                                   <td  > &emsp; &emsp;<c:out value="${container.containerName}" /></td>
                                                   <td  >
                                                       <input type="text" name="containerNo" id="containerNo" class="form-control" value='<c:out value="${container.containerNo}" />' />
                                                   </td>
                                                   
                                                   <td  >
                                                       <input type="text" name="tonnage" id="tonnage" class="form-control" style="width:160px" value='<c:out value="${container.tonnage}" />' />
                                                   </td>
                                                   <td  > &emsp; <c:out value="${container.description}" /></td>
                                                   <td  >
                                                       <input type="text" name="sealNo" id="sealNo" class="form-control" style="width:160px"  value='<c:out value="${container.sealNo}" />' />
                                                   </td>
                                               </tr>
                                           </c:forEach >
                                       </table>
                                       <br/> 
       
                            </c:if>


                    <table   class="table table-info mb30 table-hover"  id="bg">
                        <thead><tr>
                                <th  colspan="4" >Vehicle Reporting Details</th>
                            </tr></thead>
                        <tr>
                            <td >Point Name</td>
                            <td ><c:out value="${pointName}" /></td>
                        <input type="hidden" name="routeCourseId" id="routeCourseId"  class="form-control" value="<c:out value="${tripRouteCourseId}"/>">
                        <input type="hidden" name="pointType" id="pointType"  class="form-control" value="<c:out value="${pointType}"/>"></td>
                        <td  height="25" colspan="2">
                            &nbsp;
                        </td>
                        </tr>
                        <tr>
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <td >Trip Planned Report Date</td>
                                    <td > <c:out value="${trip.tripScheduleDate}" /></td>

                                    <td  height="25" >Trip Planned Report Time </td>
                                    <td > <c:out value="${trip.tripScheduleTime}" /></td>
                                </c:forEach>
                            </c:if>

                        </tr>


                        <tr>
                            <td ><font color="red">*</font>Vehicle Actual Reporting Date</td>
                            <td ><input type="text" name="vehicleactreportdate" id="vehicleactreportdate" style="width:180px;height:40px;" class="datepicker form-control" value="<%=endDate%>"></td>
                            <td   ><font color="red">*</font>Vehicle Actual Reporting Time </td>
                            <td  colspan="3" align="left" height="25" >
                                HH:<select name="vehicleactreporthour" id="vehicleactreporthour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="vehicleactreportmin" id="vehicleactreportmin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                        </tr>
                        <thead><tr>
                                <th  colspan="4" >Vehicle UnLoading Details</th>
                            </tr></thead>
                        <tr>
                            <td ><font color="red">*</font>Vehicle Actual UnLoading Date</td>
                            <td ><input type="text" name="vehicleloadreportdate" id="vehicleloadreportdate" class="datepicker form-control" style="width:180px;height:40px;" value="<%=endDate%>"></td>
                            <td  height="25" ><font color="red">*</font>Vehicle Actual UnLoading Time </td>
                            <td  colspan="3" align="left" height="25" >HH:<select name="vehicleloadreporthour" id="vehicleloadreporthour" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                MI:<select name="vehicleloadreportmin" id="vehicleloadreportmin" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                        </tr>
                        <tr>
                            <td ><font color="red">*</font>Vehicle UnLoading Temperature</td>
                            <td ><input type="text" name="vehicleloadtemperature" id="vehicleloadtemperature" class="form-control" style="width:180px;height:40px;" value="0"></td>
                            <td  height="25" ></td>
                            <td  colspan="3" align="left" height="25" ></td>
                        <input type="hidden" name="tripSheetId" id="tripSheetId" value="<c:out value="${tripSheetId}"/>" />
                        <input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripSheetId}"/>" />
                        </tr>
                    </table>

                   
                    <br/>





                    <center>
                        <input type="button" class="btn btn-success" name="Save" value="Save" onclick="submitPage();" />
                    </center>
        </div>


        </form>
        </body>
    </div>
</div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>