<%--
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
           Date dNow = new Date();
           SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
           //System.out.println("Current Date: " + ft.format(dNow));
           String curDate = ft.format(dNow);
           String expFile = "Pending_Bookings"+curDate+".xls";

           String fileName = "attachment;filename=" + expFile;
           response.setContentType("application/vnd.ms-excel;charset=UTF-8");
           response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table class="table table-info mb30 table-bordered" id="table" width="100%" >
                <c:if test = "${consignmentListViewSummary != null}" >
                    <c:forEach items="${consignmentListViewSummary}" var="cls">
                        <tr></tr>
                        <tr>
                            <td align="center" colspan="2"><b>4oft:</b><c:out value="${cls.fortyFeetTotal}"/></td>
                            <td align="center"  colspan="2"><b>2oft:</b><c:out value="${cls.twentyFeetTotal}"/></td>
                            <td align="center"  colspan="2"><b>Count:</b><c:out value="${cls.fortyFeetTotal + cls.twentyFeetTotal}"/></td>
                            <td align="center"  colspan="3"><b>Booking:</b><c:out value="${cls.totalOrders}"/></td>
                        </tr>
                        <tr></tr>
                    </c:forEach>
                </c:if>
            </table>    
            <c:if test = "${consignmentListViewCustomerPreviousSummary != null}" >
                <table class="table table-info mb30 table-bordered" id="table" width="100%" >
                    <thead>
                        <tr >
                            <th align="center">S.No</th>
                            <th align="center">ConsigDate</th>
                            <th align="center">CustRefNo</th>
                            <th align="center">CustName</th>
                            <th align="center">MovementType</th>
                            <th align="center">SLN/BOE</th>
                            <th align="center">Point</th>
                            <th align="center">Veh.Req.On </th>
                            <th align="center">Loading/Unloading On</th>
                            <th align="center">40 FT</th>
                            <th align="center">20 FT</th>
                            <th align="center">Total</th>
                            <th align="center">Created By</th>
                            <th align="center">Age(in Days)</th>
                        </tr>
                    </thead>
                    <% int index = 0;
                                int sno = 1;
                    %>
                    <tbody>
                        <c:forEach items="${consignmentListViewCustomerPreviousSummary}" var="cnl">
                            <tr>
                            <td align="center" ><%= index%></td>
                            <td align="center" ><c:out value="${cls.consignmentOrderDate}"/></td>
                            <td align="center" ><c:out value="${cls.customerOrderReferenceNo}"/></td>
                            <td align="center" ><c:out value="${cls.customerName}"/></td>
                            <td align="center" ><c:out value="${cls.movementType}"/></td>
                            <td align="center" >
                                <c:if test="${cls.movementTypeId == 1 || cls.movementTypeId == 25 || cls.movementTypeId == 26  || cls.movementTypeId == 27 || cls.movementTypeId == 31 || cls.movementTypeId == 32}">
                                    <c:out value="${cls.shippingLineTwo}"/>
                                </c:if>
                                <c:if test="${cls.movementTypeId == 2 || cls.movementTypeId == 28 || cls.movementTypeId == 29 || cls.movementTypeId == 30}">
                                    <c:out value="${cls.billOfEntry}"/>
                                </c:if>

                            </td>
                            <td align="center" ><c:out value="${cls.finalPointName}"/></td>
                            <td align="center" ><c:out value="${cls.tripScheduleDate}"/><br><c:out value="${cls.tripScheduleTime}"/></td>
                            <td align="center" ><c:out value="${cls.loadUnloadTime}"/><br><c:out value="${cls.loadUnloadTime}"/></td>
                            <td align="center" ><c:out value="${cls.fortyFeetTotal}"/></td>
                            <td align="center" ><c:out value="${cls.twentyFeetTotal}"/></td>
                            <td align="center" ><font size='3'><c:out value="${cls.twentyFeetPending + cls.fortyFeetPending}"/></font>/ 
                                <font  size='3'> &nbsp;<c:out value="${cls.twentyFeetTotal + cls.fortyFeetTotal}"/></td>
                            <td align="center" ><c:out value="${cls.userName}"/></td>
                            <td align="center" ><c:out value="${cls.age}"/></td>
                        </tr>
                            <%index++;%>
                            <%sno++;%>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
