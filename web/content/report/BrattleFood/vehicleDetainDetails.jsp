<%-- 
    Document   : vehicleDetainDetails
    Created on : May 11, 2016, 4:45:06 PM
    Author     : Gulshan kumar
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("please select From Date");
                    document.getElementById('fromDate').focus();
                }
                if (document.getElementById('toDate').value == '') {
                    alert("please select To Date");
                    document.getElementById('toDate').focus();
                }
                else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/vehicleDetainDetailsReport.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/vehicleDetainDetailsReport.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }

            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }


            function viewCustomerProfitDetails(tripIds) {
                //alert(tripIds);
                window.open('/throttle/viewCustomerWiseProfitDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
            }
        </script>

        <style type="text/css">





            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */



            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }



        </style>
    </head>
    <body onload="setValue();
            sorter.size(10);">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">containerMovementReport</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >

                                    <tr>
                                        <td  height="30"><font color="red">*</font>From Date :</td>
                                        <td  height="30"><input type="text" name="fromDate" id="fromDate" class="datepicker" value="<c:out value="${fromDate}"/>" />  </td>
                                        <td  height="30"><font color="red">*</font>To Date : </td>
                                        <td  height="30"><input type="text" name="toDate" id="toDate" class="datepicker" value="<c:out value="${toDate}"/>" /></td>

                                    <script>
                                        document.getElementById("monthId").value = "<c:out value="${monthId}"/>";
                                    </script>
                                    </td>

                                    <td><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>
                                    <td><input type="button" class="button" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>

            <br>
            <br>

            <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40" >
                        <th><h3 align="center">Transporter</h3></th>
                        <th><h3 align="center">Vehicle No</h3></th>
                        <th><center><h3>No.of.Trips</h3></center></th>
                        <th><center><h3>BD</h3></center></th>
                        <th><center><h3>DTN</h3></center></th>
                        <th><center><h3>IDL</h3></center></th>
                        <th><h3 align="center">OT</h3></th>
                        <th><h3 align="center">Total Days</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <c:set var="OnTrip" value="0"/>
                    <c:set var="TotalTrips" value="0"/>
                    <c:set var="TotalBD" value="0"/>
                    <c:set var="TotalDTN" value="0"/>
                    <c:set var="TotalIDL" value="0"/>
                    <c:set var="TotalOT" value="0"/>

                    <% int index = 1;%>
                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:set var="vendorName" value=""/>  
                    <c:forEach items="${VehicleDetainDetails}" var="Vehicle">

                        <c:if test = "${vendorName != ''}" >
                            <c:if test = "${vendorName !=Vehicle.vendorName}" >

                                <tr height="30">
                                    <td align="center"><b><c:out value="${vendorName}"/> Total()</b></td>
                                    <td align="center"></td>
                                    <td align="center"><b><c:out value="${TotalTrips}"/></b></td>
                                    <td align="center"><b><c:out value="${TotalBD}"/></b></td>
                                    <td align="center"><b><c:out value="${TotalDTN}"/></b></td>
                                    <td align="center"><b><c:out value="${TotalIDL}"/></b></td>
                                    <td align="center"><b><c:out value="${TotalOT}"/></b></td>
                                    <td align="center"></td>



                                </tr>
                                <c:set var="TotalTrips" value="${0}"/>
                                <c:set var="TotalBD" value="${0}"/>
                                <c:set var="TotalDTN" value="${0}"/>
                                <c:set var="TotalIDL" value="${0}"/>
                                <c:set var="TotalOT" value="${0}"/>  
                            </c:if>
                        </c:if>
                        <tr height="30">
                            <c:set var="vendorName" value="${Vehicle.vendorName}"/>
                            <c:set var="OnTrip" value="${Vehicle.totalDays - Vehicle.tripDays}"/>
                            <c:set var="TotalTrips" value="${TotalTrips + Vehicle.noOfTrips}"/>
                            <c:set var="TotalBD" value="${TotalBD + 0}"/>
                            <c:set var="TotalDTN" value="${TotalDTN + 0}"/>
                            <c:set var="TotalIDL" value="${TotalIDL + OnTrip}"/>
                            <c:set var="TotalOT" value="${TotalOT + Vehicle.tripDays}"/>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.vendorName}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.regNo}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.noOfTrips}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${0}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${0}"/></td>
                                <td align="center" class="<%=classText%>"><c:out value="${OnTrip}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.tripDays}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.totalDays}"/></td>
                            
                        </tr>

                    </c:forEach>
                    <tr height="30">
                        <td align="center"><b><c:out value="${vendorName}"/> Total()</b></td>
                        <td align="center"></td>
                        <td align="center"><b><c:out value="${TotalTrips}"/></b></td>
                        <td align="center"><b><c:out value="${TotalBD}"/></b></td>
                        <td align="center"><b><c:out value="${TotalDTN}"/></b></td>
                        <td align="center"><b><c:out value="${TotalIDL}"/></b></td>
                        <td align="center"><b><c:out value="${TotalOT}"/></b></td>
                        <td align="center"></td>
                    </tr>
                </tbody>
            </table>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>
