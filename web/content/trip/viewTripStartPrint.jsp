<%-- 
    Document   : viewTripStartPrint
    Created on : mar 3, 2020, 10:36:08 PM
    Author     : HP
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.lang.Double" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="style.css" />
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <title>CA LOGISTICS</title>
    </head>

    <script>
        function generatePDF() {
            var doc = new jsPDF();
            $('#cmd').click(function() {
                doc.fromHTML($('#printDiv').html(), 15, 15, {
                    'width': 170,
                    'elementHandlers': specialElementHandlers
                });
                doc.save('sample-file.pdf');
            });
        }

        function viewTripDetails() {
            document.enter.action = '/throttle/viewTripSheets.do?menuClick=1&statusId=0&tripType=1&admin=No&movement=0&subStatusId=0&vendorType=0';
//                document.enter.action = '/throttle/viewTripSheets.do?&statusId=12&tripType=1&admin=No';
            document.enter.submit();
        }

        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>
    <%
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String voucherDate = dateFormat.format(date);;
        String billingPartyId = (String)request.getAttribute("billingPartyId");
    %>



    <body onload="calcExpenses();">
    <style type="text/css">
        #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
    </style>
    <form name="enter" method="post">
        <div id="printDiv">

            <table width="850" border="1" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">

                <tr>

                    <td height="30" style="text-align:center; text-transform:uppercase;" colspan="4">
                        <table align="left" style="height:80px;text-align:center;">
                            <tr>                           
                                <td colspna="4" style="padding-left: 280px; padding-right:42px;">
                                    <font size="3"> <center>
                                            <b>C.A Logistics Pvt.Ltd.</b></center><br>
                                        <center><u>SETTLEMENT PRINT</u></center></font>
                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr style="height:30px;">
                    <td align="left" ><font size="3">&emsp;JOB No</font></td>
                    <td align="left" ><font size="3">&emsp;<c:out value="${jobReferenceNo}"/></font></td>

                    <td align="left"><font size="3">&emsp;Vehicle No </font></td>
                    <td align="left"><font size="3">&emsp;<c:out value="${vehicleNo}"/> &nbsp; /<br> &nbsp; <c:out value="${vehicleTypeName}"/></font></td>
                </tr>
                <tr style="height:30px;">
                    <td align="left"><font size="3">&emsp;Trip Code</font></td>
                    <td align="left"><font size="3">&emsp;<c:out value="${tripCode}"/> </font></td>
                    <td align="left"><font size="3">&emsp;Customer Ref. No</font></td>
                    <td align="left"><font size="3">&emsp;<c:out value="${customerOrderReferenceNo}"/></font></td>
                </tr>
                <tr style="height:30px;">

                    <td align="left" ><font size="3">&emsp;Driver Name:</font></td>
                    <td align="left" ><font size="3">&emsp;<c:out value="${driverName}"/>&emsp; /<br> &emsp;<c:out value="${driverLicenseNo}"/></font></td>
                    <td align="left"><font size="3">&emsp;Cleaner Name</font></td>
                    <td align="left"><font size="3">&emsp;<c:out value="${cleanerName}"/>&emsp; /<br> &emsp;<c:out value="${cleanerLicenseNo}"/></font></td>
                </tr>
                <tr  style="height:150px;">
                    <td align="left" colspan="2">
                        <table align="left" style="height:150px;">
                            <tr style="height:10px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>Customer Name  </b></font></td>
                                <td align="left"><font size="2">&emsp;<c:out value="${customerName}"/></font></td>
                            </tr>
                            <tr style="height:10px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>Factory Name  </b><label>&emsp;</font></td>
                                <td align="left"><font size="2">&emsp;<label>&emsp;</font></td>
                            </tr>
                            <tr style="height:10px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>BKG No  </b></font></td>
                                <td align="left"><font size="2">&emsp;<c:out value="${billOfEntry}"/></font></td>
                            </tr>
                            <tr style="height:10px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>SB/BE NO  </b></font></td>
                                <td align="left"><font size="2">&emsp;<c:out value="${shipingLineNo}"/></font></td>
                            </tr>
                            <tr style="height:10px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>Movement Type </b> </font></td>
                                <td align="left"><font size="2">&emsp;<c:out value="${movementTypeName}"/></font></td>
                            </tr>
                        </table>

                    </td>
                    <td align="left" colspan="2">
                        <table align="left" style="height:150px;">
                            <tr style="height:20px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>Trip Start Date/Time  </b></font></td>
                                <td align="left"><font size="2">&emsp;<c:out value="${startDate}"/> &nbsp;<c:out value="${startTime}"/></font></td>
                            </tr>
                            <tr style="height:20px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>Trip End Date/Time  </b></font></td>
                                <td align="left"><font size="2">&emsp;<c:out value="${tripEndDate}"/> &nbsp;<c:out value="${tripEndTime}"/></font></td>
                            </tr>
                            <tr style="height:20px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>No Of Days  </b><label>&emsp; </font></td>
                                <td align="left"><font size="2"><label>&emsp; </font></td>
                            </tr>
                            <tr style="height:20px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>Halting Days  </b><label>&emsp; </font></td>
                                <td align="left"><font size="2"><label>&emsp; </font></td>
                            </tr>
                            <tr style="height:20px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>Halting Status  </b><label>&emsp;</font></td>
                                <td align="left"><font size="2"><label>&emsp;</font></td>
                            </tr>
                        </table>
                    </td> 
                </tr>           
                <tr style="height:30px;border-width: 0px;" >
                    <td align="center" colspan="4"><font size="3">&emsp;Route Interim &emsp;<c:out value="${routeInfo}"/></font></td>
                </tr>
                <tr style="height:30px;border-width: 0px;" >
                    <td colspan="4">
                        <table style="height:30px;" >
                            <tr style="height:20px;border:0px">
                                <td align="right">
                                 <font size="3">&emsp;&emsp;Odometer Start:<c:out value="${startKm}"/></label>&emsp;&emsp;&emsp;&emsp;&emsp;</font>
                                </td>
                                <td align="right">
                                    <font size="3">Odometer End:<c:out value="${endKm}"/></label>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</font>
                                </td>
                                <td align="right">
                                    <font size="3">Total Km :<c:out value="${runKm}"/></label>&emsp;</font>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr  style="height:150px;">
                    <td align="left" colspan="2" >
                        <table align="left" style="height:150px;">
                            <tr style="height:10px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>CONTAINER NO.  </b></font></td>
                                <td align="left"><font size="2">&emsp;<c:out value="${containerNo}"/></font></td>
                            </tr>
                            <tr style="height:10px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>CONTAINER SIZE</font></td>
                                <td align="left"><font size="2">&emsp;<c:out value="${containerTypeName}"/>&nbsp; * &nbsp;
                                        <c:if test="${containerQuantity==1}">1</c:if>
                                        <c:if test="${containerQuantity==2}">1</c:if>
                                        <c:if test="${containerQuantity==3}">2</c:if>
                                        </font></td>
                                </tr>
                                <tr style="height:10px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>GROSS WEIGHT  </b><label>&emsp;</font></td>
                                    <td align="left"><font size="2"><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:10px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>TANK CAPACITY  </b><label>&emsp;</font></td>
                                    <td align="left"><font size="2"><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:10px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>RECOVERY FUEL/RS  </b><label>&emsp;</font></td>
                                    <td align="left"><font size="2"><label>&emsp;</font></td>
                                </tr>
                            </table>

                        </td>
                        <td align="left" colspan="2">
                            <table align="left" style="height:150px;">
                                <tr style="height:20px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>FILLED DIESEL  </b><label>&emsp;</font></td>
                                    <td align="left"><font size="2"><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:20px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>BUNK & INDENT NO  </b><label>&emsp;</font></td>
                                    <td align="left"><font size="2"> </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:20px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>ENROUTE DIESEL  </b><label>&emsp; </font></td>
                                    <td align="left"><font size="2"><label>&emsp; </font></td>
                                </tr>
                                <tr style="height:20px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>DIESEL RATE  </b><label>&emsp; </font></td>
                                    <td align="left"><font size="2"><label>&emsp; </font></td>
                                </tr>
                                <tr style="height:20px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>ALLOCATED FUEL  </b><label>&emsp;</font></td>
                                    <td align="left"><font size="2"><label>&emsp;</font></td>
                                </tr>
                            </table>
                        </td> 
                    </tr> 

                    <tr style="height:30px;border-width: 0px;" >
                        <td align="center" colspan="4"><font size="3">&emsp;TRIP EXPENSES</font></td>
                    </tr>

                    <tr  style="height:130px;">
                        <td colspan="2" align="left" >
                            <table align="left" style="height:130px;">
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Driver Bhata  </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Cleaner Bhata </b> <label>&emsp;</font></td>
                                </tr>
                                
                                <tr style="height:30px;border:0px">
                                <td align="left"><font size="2">&emsp;<b>Pooja Expense: </b> </font></td>
                                <td align="left"><font size="2"><label>&emsp;<c:out value="${poojaExpense}"/></label></font></td>
                            </tr>
                                
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Lift Off	 </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Lift On :  </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Toll Encash  </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Weightment  </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Loading / Unloading  </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Phone  </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Po Charges </b><label>&emsp;</font></td>
                                </tr>
                            </table>

                        </td>

                        <td colspan="2" align="left">
                            <table align="left" style="height:130px;">
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>Maintenance Expenses</b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>1). </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>2). </b><label>&emsp; </font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>3). </b><label>&emsp; </font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b> Rto Expenses </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>1). </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>2). </b><label>&emsp; </font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>3). </b><label>&emsp; </font></td>
                                </tr>
                            </table>
                        </td> 
                    </tr>

                    <tr style="height:30px;border-width: 0px;" >
                        <td align="center" colspan="4"><font size="3">SETTLEMENT SUMMARY</font></td>
                    </tr>
                    <tr  style="height:100px;">
                        <td align="left" colspan="2">
                            <table align="left" style="height:100px;">
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>TRIP EXPENSES  </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>OTHER DEDUCTIONS  </b> <label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>NET PAYABLE :  </b><label>&emsp;</font></td>
                                </tr>

                            </table>

                        </td>
                        <td align="left" colspan="2">
                            <table align="left" style="height:100px;">
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>TRIP ADVANCE : </b><label>&emsp;</font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2"></font></td>
                                </tr>
                                <tr style="height:30px;border:0px">
                                    <td align="left"><font size="2">&emsp;<b>RUPEES IN WORDS. </b><label>&emsp; </font></td>
                                </tr>

                            </table>
                        </td> 
                    </tr> 

                    <tr>
                        <td height="50" colspan="2" valign="bottom"><b>&emsp;Prepared By</b></td>
                        <td height="50" colspan="2" valign="bottom"><b>&emsp;Passed By</b></td>
                    </tr>


                    <!--            <tr>
                <% String total = "";
                    if (request.getAttribute("amount") != null) {
                        total = (String) request.getAttribute("amount");
                    } else {
                        total = "00";
                    }
                %>
                <% if(total.contains("-")) {
                    total = total.replaceAll("-","");
                %>
                <td colspan="2" height="35" align="left">
                    <b>Return amount in rupees:</b><%float spareTotal = 0.00F;
                        spareTotal = Float.parseFloat(total);
                %>
                <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                    <% spareTotalRound.setRoundedValue(String.valueOf(spareTotal));%>
                    <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                    <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                    <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />Only</b>
                </jsp:useBean>
                </p> &nbsp;&nbsp;&nbsp;<c:out value="${remarks}"/>
            </td>
                <%}else{ %>
                <td colspan="2" height="35" align="left">
                    <b>&emsp;Balance amount in rupees:</b><%float spareTotal = 0.00F;
                        spareTotal = Float.parseFloat(total);
                %>
                <jsp:useBean id="spareTotalRound1"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                    <% spareTotalRound1.setRoundedValue(String.valueOf(spareTotal));%>
                    <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                    <% spareTotalRound1.setNumberInWords(spareTotalRound1.getRoundedValue());%>
                    <b><jsp:getProperty name="spareTotalRound1" property="numberInWords" />Only</b>
                </jsp:useBean>
                </p> &nbsp;&nbsp;&nbsp;<c:out value="${remarks}"/>
            </td>   
                <%}%>

            </tr>-->
                <!--            <tr>
                                <td height="90" colspan="2" valign="bottom">&emsp;Prepared By</td>
                                <td height="90" colspan="2" valign="bottom">&emsp;Passed By</td>
                            </tr>-->

            </table>
            <script type="text/javascript">
                /* <![CDATA[ */
                function get_object(id) {
                    var object = null;
                    if (document.layers) {
                        object = document.layers[id];
                    } else if (document.all) {
                        object = document.all[id];
                    } else if (document.getElementById) {
                        object = document.getElementById(id);
                    }
                    return object;
                }
    //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
                get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tCode}"/>', 0);
                /* ]]> */
            </script>
            <br>
            <!--                <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>-->
            <table width="850" border="1" cellpadding="0" cellspacing="0" align="center" border="0" style="display:none;font-family:Arial, Helvetica, sans-serif; font-size:13px; ">
                <tr>
                    <td align="left" >
                        <table align="left" >
                            <tr style="border:0px">
                                <td align="left"><font size="2"><b>Container In  </b><label>&emsp;</font></td>
                            </tr>
                        </table>

                    </td>

                    <td align="left">
                        <table align="left" >

                            <tr style="border:0px">
                                <td align="left"><font size="2"><b>Load In  </b> <label>&emsp;</font></td>
                            </tr>
                        </table>
                    </td> 
                    <td align="left">
                        <table align="left">

                            <tr style="border:0px">
                                <td align="left"><font size="2"><b>Drop On :  </b><label>&emsp;</font></td>
                            </tr>
                        </table>
                    </td> 
                </tr> 
                <tr style="height:130px;">
                    <td>
                        <table align="left" style="height:130px;">

                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Date :__________________________________</b> <label>&emsp;</font></td>
                            </tr>
                        </table>
                    </td>    
                    <td colspan="2">
                        <table align="left" style="height:130px;">

                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Plot In Time : ____________________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Plot Out Time : ___________________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Factory In Time : _________________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Factory out Time : ________________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Chennai In : ______________________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Concor / CWC (M) In Time : ________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Concor / CWC (M) Out Time : _______________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>Harbour O'Gate In : _______________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>CCTL/CITPL Gate In : ______________________________________</b> <label>&emsp;</font></td>
                            </tr>
                            <tr style="height:30px;border:0px">
                                <td align="left"><font size="2"><b>CCTL/CITPL Gate Out : _____________________________________</b> <label>&emsp;</font></td>
                            </tr>
                        </table>
                    </td>    
                </tr>  
            </table>
        </div><div id="editor"></div>
        <br>

        <center>  
            <input type="button" id="printbtn" class="button"  value="Back" onClick="viewTripDetails();" />
            &emsp; <input align="center" type="button" class="button"  onclick="print();" value = "Print"   />
            <!--&emsp;    <input align="center" type="button" class="button" id="cmd" onclick="generatePDF();" value = "Generate PDF"   />-->
        </center>
    </form>
</body>
</html>
