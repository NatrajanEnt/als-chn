package ets.domain.sales.business;

/**
 *
 * @author vidya
 */
public class SalesTO {

//    vehPro
    private String compId = "";
    private String roleId = "";
    private String portName = "";
    private String consolName = "";
    private String emptyBondValidity = "";
    private String mailSubjectTo = "";
    private String mailTypeId = "";
    private String mailSubjectCc = "";
    private String mailSubjectBcc = "";
    private String mailContentTo = "";
    private String fileName = "";
    private String filePath = "";
    
    private String mailContentCc = "";
    private String mailContentBcc = "";
    private String mailIdTo = "";
    private String mailIdCc = "";
    private String mailIdBcc = "";
    
    private String custOrderRefNo = "";
    private String date = "";
    private String elapsedDays = "";
    private String twentyTypeContainer = "";
    private String fortyTypeContainer = "";
    private String twentyPlanned = "";
    private String fortyPlanned = "";
    private String totaltwentyTypeContainer = "";
    private String totalfortyTypeContainer = "";
    private String vehicleId = "";
    private String vehicleTypeName = "";
    private String regno = "";
    private String expectedArrivalDate = "";
    private String expectedArrivalTime = "";
    private String activeInd = "";
    private String service = "";
    private String currdate = "";
    private String starttime = "";
    private String endtime = "";
    private String twentyAvailable = "";
    private String twentyNotAvail = "";
    private String fortyAvailable = "";
    private String fortyNotAvail = "";
    private String workSho20 = "";
    private String workShop40 = "";
    private String totalVehicletwenty = "";
    private String totalVehicleforty = "";
    private String vesselName = "";

    private String consignmentOrderId = "";
    private String linerName = "";
    private String totalKM = "";
    private String totalHours = "";
    private String totalMinutes = "";
    private String consignmentNoteNo = "";
    private String customerOrderRefNo = "";
    private String routeContractId = "";
    private String freightRate = "";
    private String originId = "";
    private String destinationId = "";
    private String point1Id = "";
    private String customerId = "";
    private String customerName = "";
    private String customerAddress = "";
    private String pincode = "";
    private String mobile = "";
    private String phone = "";
    private String email = "";

    private String twentyTypeContQtyPlanned = "";
    private String createdOn = "";
    private String twentyTypeContQty = "";
    private String fortyTypeContQty = "";
    private String twentyTypeContQtyCompleted = "";
    private String twentyTypeContQtyToBePlanned = "";
    private String fortyTypeContQtyPlanned = "";
    private String fortyTypeContQtyCompleted = "";
    private String fortyTypeContQtyToBePlanned = "";
    private String ownVeh = "";
    private String hiredVeh = "";
    private String delayAge = "";

    private int userId = 0;
    private String consignorId = "";
    private String consigneeId = "";
    private String consignorName = "";
    private String consignorAddress = "";
    private String consignorPincode = "";
    private String consignorMobile = "";
    private String consignorPhone = "";
    private String consignorEmail = "";

    private String consigneeName = "";
    private String consigneeAddress = "";
    private String consigneePincode = "";
    private String consigneeMobile = "";
    private String consigneePhone = "";
    private String consigneeEmail = "";

    private String containerTypeId = "";
    private String datFile = "";
    private String contractId = "";
    private String vehicleTypeId = "";
    private String rateWithReefer = "";
    private String rateWithoutReefer = "";
    private String approvalStatus = "";

    private String containerType = "";
    private String containerNos = "";
    private String containerQty = "";
    private String pnrId = "";
    private String pnrNo = "";
    private String created = "";
    private String linearName = "";
    private String field1 = "";
    private String field2 = "";
    private String field3 = "";
    private String field4 = "";
    private String consignee = "";
    private String field5 = "";
    private String field6 = "";
    private String pName = "";
    private String field7 = "";
    private String field8 = "";
    private String field9 = "";
    private String conNo = "";
    private String field10 = "";
    private String field11 = "";
    private String loadCap = "";
    private String conType = "";
    private String Tarewgt = "";
    private String field12 = "";
    private String field13 = "";
    
    private String consolNames = "";
    private String linernames = "";
    private String conTypeId = "";
    private String containerTypes = "";
    private String port = "";
    private String emptyValidity = "";
    private String conName = "";
    private String dAge = "";
    
    
    

    public String getDatFile() {
        return datFile;
    }

    public void setDatFile(String datFile) {
        this.datFile = datFile;
    }

    public String getPnrId() {
        return pnrId;
    }

    public void setPnrId(String pnrId) {
        this.pnrId = pnrId;
    }

    public String getPnrNo() {
        return pnrNo;
    }

    public void setPnrNo(String pnrNo) {
        this.pnrNo = pnrNo;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLinearName() {
        return linearName;
    }

    public void setLinearName(String linearName) {
        this.linearName = linearName;
    }

    public String getField1() {
        return field1;
    }

    public void setField1(String field1) {
        this.field1 = field1;
    }

    public String getField2() {
        return field2;
    }

    public void setField2(String field2) {
        this.field2 = field2;
    }

    public String getField3() {
        return field3;
    }

    public void setField3(String field3) {
        this.field3 = field3;
    }

    public String getField4() {
        return field4;
    }

    public void setField4(String field4) {
        this.field4 = field4;
    }

    public String getConsignee() {
        return consignee;
    }

    public void setConsignee(String consignee) {
        this.consignee = consignee;
    }

    public String getField5() {
        return field5;
    }

    public void setField5(String field5) {
        this.field5 = field5;
    }

    public String getField6() {
        return field6;
    }

    public void setField6(String field6) {
        this.field6 = field6;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public String getField7() {
        return field7;
    }

    public void setField7(String field7) {
        this.field7 = field7;
    }

    public String getField8() {
        return field8;
    }

    public void setField8(String field8) {
        this.field8 = field8;
    }

    public String getField9() {
        return field9;
    }

    public void setField9(String field9) {
        this.field9 = field9;
    }

    public String getConNo() {
        return conNo;
    }

    public void setConNo(String conNo) {
        this.conNo = conNo;
    }

    public String getField10() {
        return field10;
    }

    public void setField10(String field10) {
        this.field10 = field10;
    }

    public String getField11() {
        return field11;
    }

    public void setField11(String field11) {
        this.field11 = field11;
    }

    public String getLoadCap() {
        return loadCap;
    }

    public void setLoadCap(String loadCap) {
        this.loadCap = loadCap;
    }

    public String getConType() {
        return conType;
    }

    public void setConType(String conType) {
        this.conType = conType;
    }

    public String getTarewgt() {
        return Tarewgt;
    }

    public void setTarewgt(String Tarewgt) {
        this.Tarewgt = Tarewgt;
    }

    public String getField12() {
        return field12;
    }

    public void setField12(String field12) {
        this.field12 = field12;
    }

    public String getField13() {
        return field13;
    }

    public void setField13(String field13) {
        this.field13 = field13;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getContainerType() {
        return containerType;
    }

    public void setContainerType(String containerType) {
        this.containerType = containerType;
    }

    public String getContainerNos() {
        return containerNos;
    }

    public void setContainerNos(String containerNos) {
        this.containerNos = containerNos;
    }

    public String getContainerQty() {
        return containerQty;
    }

    public void setContainerQty(String containerQty) {
        this.containerQty = containerQty;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getRateWithReefer() {
        return rateWithReefer;
    }

    public void setRateWithReefer(String rateWithReefer) {
        this.rateWithReefer = rateWithReefer;
    }

    public String getRateWithoutReefer() {
        return rateWithoutReefer;
    }

    public void setRateWithoutReefer(String rateWithoutReefer) {
        this.rateWithoutReefer = rateWithoutReefer;
    }

    public String getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(String approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorPincode() {
        return consignorPincode;
    }

    public void setConsignorPincode(String consignorPincode) {
        this.consignorPincode = consignorPincode;
    }

    public String getConsignorMobile() {
        return consignorMobile;
    }

    public void setConsignorMobile(String consignorMobile) {
        this.consignorMobile = consignorMobile;
    }

    public String getConsignorPhone() {
        return consignorPhone;
    }

    public void setConsignorPhone(String consignorPhone) {
        this.consignorPhone = consignorPhone;
    }

    public String getConsignorEmail() {
        return consignorEmail;
    }

    public void setConsignorEmail(String consignorEmail) {
        this.consignorEmail = consignorEmail;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneePincode() {
        return consigneePincode;
    }

    public void setConsigneePincode(String consigneePincode) {
        this.consigneePincode = consigneePincode;
    }

    public String getConsigneeMobile() {
        return consigneeMobile;
    }

    public void setConsigneeMobile(String consigneeMobile) {
        this.consigneeMobile = consigneeMobile;
    }

    public String getConsigneePhone() {
        return consigneePhone;
    }

    public void setConsigneePhone(String consigneePhone) {
        this.consigneePhone = consigneePhone;
    }

    public String getConsigneeEmail() {
        return consigneeEmail;
    }

    public void setConsigneeEmail(String consigneeEmail) {
        this.consigneeEmail = consigneeEmail;
    }

    public String getCustomerOrderRefNo() {
        return customerOrderRefNo;
    }

    public void setCustomerOrderRefNo(String customerOrderRefNo) {
        this.customerOrderRefNo = customerOrderRefNo;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getFreightRate() {
        return freightRate;
    }

    public void setFreightRate(String freightRate) {
        this.freightRate = freightRate;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getPoint1Id() {
        return point1Id;
    }

    public void setPoint1Id(String point1Id) {
        this.point1Id = point1Id;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getConsignmentNoteNo() {
        return consignmentNoteNo;
    }

    public void setConsignmentNoteNo(String consignmentNoteNo) {
        this.consignmentNoteNo = consignmentNoteNo;
    }

    public String getTotalKM() {
        return totalKM;
    }

    public void setTotalKM(String totalKM) {
        this.totalKM = totalKM;
    }

    public String getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(String totalHours) {
        this.totalHours = totalHours;
    }

    public String getTotalMinutes() {
        return totalMinutes;
    }

    public void setTotalMinutes(String totalMinutes) {
        this.totalMinutes = totalMinutes;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getConsignmentOrderId() {
        return consignmentOrderId;
    }

    public void setConsignmentOrderId(String consignmentOrderId) {
        this.consignmentOrderId = consignmentOrderId;
    }

    public String getLinerName() {
        return linerName;
    }

    public void setLinerName(String linerName) {
        this.linerName = linerName;
    }

    public String getTwentyTypeContQtyPlanned() {
        return twentyTypeContQtyPlanned;
    }

    public void setTwentyTypeContQtyPlanned(String twentyTypeContQtyPlanned) {
        this.twentyTypeContQtyPlanned = twentyTypeContQtyPlanned;
    }

    public String getTwentyTypeContQty() {
        return twentyTypeContQty;
    }

    public void setTwentyTypeContQty(String twentyTypeContQty) {
        this.twentyTypeContQty = twentyTypeContQty;
    }

    public String getFortyTypeContQty() {
        return fortyTypeContQty;
    }

    public void setFortyTypeContQty(String fortyTypeContQty) {
        this.fortyTypeContQty = fortyTypeContQty;
    }

    public String getTwentyTypeContQtyCompleted() {
        return twentyTypeContQtyCompleted;
    }

    public void setTwentyTypeContQtyCompleted(String twentyTypeContQtyCompleted) {
        this.twentyTypeContQtyCompleted = twentyTypeContQtyCompleted;
    }

    public String getTwentyTypeContQtyToBePlanned() {
        return twentyTypeContQtyToBePlanned;
    }

    public void setTwentyTypeContQtyToBePlanned(String twentyTypeContQtyToBePlanned) {
        this.twentyTypeContQtyToBePlanned = twentyTypeContQtyToBePlanned;
    }

    public String getFortyTypeContQtyPlanned() {
        return fortyTypeContQtyPlanned;
    }

    public void setFortyTypeContQtyPlanned(String fortyTypeContQtyPlanned) {
        this.fortyTypeContQtyPlanned = fortyTypeContQtyPlanned;
    }

    public String getFortyTypeContQtyCompleted() {
        return fortyTypeContQtyCompleted;
    }

    public void setFortyTypeContQtyCompleted(String fortyTypeContQtyCompleted) {
        this.fortyTypeContQtyCompleted = fortyTypeContQtyCompleted;
    }

    public String getFortyTypeContQtyToBePlanned() {
        return fortyTypeContQtyToBePlanned;
    }

    public void setFortyTypeContQtyToBePlanned(String fortyTypeContQtyToBePlanned) {
        this.fortyTypeContQtyToBePlanned = fortyTypeContQtyToBePlanned;
    }

    public String getOwnVeh() {
        return ownVeh;
    }

    public void setOwnVeh(String ownVeh) {
        this.ownVeh = ownVeh;
    }

    public String getHiredVeh() {
        return hiredVeh;
    }

    public void setHiredVeh(String hiredVeh) {
        this.hiredVeh = hiredVeh;
    }

    public String getDelayAge() {
        return delayAge;
    }

    public void setDelayAge(String delayAge) {
        this.delayAge = delayAge;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getCustOrderRefNo() {
        return custOrderRefNo;
    }

    public void setCustOrderRefNo(String custOrderRefNo) {
        this.custOrderRefNo = custOrderRefNo;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTwentyTypeContainer() {
        return twentyTypeContainer;
    }

    public void setTwentyTypeContainer(String twentyTypeContainer) {
        this.twentyTypeContainer = twentyTypeContainer;
    }

    public String getFortyTypeContainer() {
        return fortyTypeContainer;
    }

    public void setFortyTypeContainer(String fortyTypeContainer) {
        this.fortyTypeContainer = fortyTypeContainer;
    }

    public String getTwentyPlanned() {
        return twentyPlanned;
    }

    public void setTwentyPlanned(String twentyPlanned) {
        this.twentyPlanned = twentyPlanned;
    }

    public String getFortyPlanned() {
        return fortyPlanned;
    }

    public void setFortyPlanned(String fortyPlanned) {
        this.fortyPlanned = fortyPlanned;
    }

    public String getTotaltwentyTypeContainer() {
        return totaltwentyTypeContainer;
    }

    public void setTotaltwentyTypeContainer(String totaltwentyTypeContainer) {
        this.totaltwentyTypeContainer = totaltwentyTypeContainer;
    }

    public String getTotalfortyTypeContainer() {
        return totalfortyTypeContainer;
    }

    public void setTotalfortyTypeContainer(String totalfortyTypeContainer) {
        this.totalfortyTypeContainer = totalfortyTypeContainer;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getExpectedArrivalDate() {
        return expectedArrivalDate;
    }

    public void setExpectedArrivalDate(String expectedArrivalDate) {
        this.expectedArrivalDate = expectedArrivalDate;
    }

    public String getExpectedArrivalTime() {
        return expectedArrivalTime;
    }

    public void setExpectedArrivalTime(String expectedArrivalTime) {
        this.expectedArrivalTime = expectedArrivalTime;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getCurrdate() {
        return currdate;
    }

    public void setCurrdate(String currdate) {
        this.currdate = currdate;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }

    public String getTwentyAvailable() {
        return twentyAvailable;
    }

    public void setTwentyAvailable(String twentyAvailable) {
        this.twentyAvailable = twentyAvailable;
    }

    public String getTwentyNotAvail() {
        return twentyNotAvail;
    }

    public void setTwentyNotAvail(String twentyNotAvail) {
        this.twentyNotAvail = twentyNotAvail;
    }

    public String getFortyAvailable() {
        return fortyAvailable;
    }

    public void setFortyAvailable(String fortyAvailable) {
        this.fortyAvailable = fortyAvailable;
    }

    public String getFortyNotAvail() {
        return fortyNotAvail;
    }

    public void setFortyNotAvail(String fortyNotAvail) {
        this.fortyNotAvail = fortyNotAvail;
    }

    public String getWorkSho20() {
        return workSho20;
    }

    public void setWorkSho20(String workSho20) {
        this.workSho20 = workSho20;
    }

    public String getWorkShop40() {
        return workShop40;
    }

    public void setWorkShop40(String workShop40) {
        this.workShop40 = workShop40;
    }

    public String getTotalVehicletwenty() {
        return totalVehicletwenty;
    }

    public void setTotalVehicletwenty(String totalVehicletwenty) {
        this.totalVehicletwenty = totalVehicletwenty;
    }

    public String getTotalVehicleforty() {
        return totalVehicleforty;
    }

    public void setTotalVehicleforty(String totalVehicleforty) {
        this.totalVehicleforty = totalVehicleforty;
    }

    public String getVesselName() {
        return vesselName;
    }

    public void setVesselName(String vesselName) {
        this.vesselName = vesselName;
    }

    public String getMailSubjectTo() {
        return mailSubjectTo;
    }

    public void setMailSubjectTo(String mailSubjectTo) {
        this.mailSubjectTo = mailSubjectTo;
    }

    public String getMailTypeId() {
        return mailTypeId;
    }

    public void setMailTypeId(String mailTypeId) {
        this.mailTypeId = mailTypeId;
    }

    public String getMailSubjectCc() {
        return mailSubjectCc;
    }

    public void setMailSubjectCc(String mailSubjectCc) {
        this.mailSubjectCc = mailSubjectCc;
    }

    public String getMailSubjectBcc() {
        return mailSubjectBcc;
    }

    public void setMailSubjectBcc(String mailSubjectBcc) {
        this.mailSubjectBcc = mailSubjectBcc;
    }

    public String getMailContentTo() {
        return mailContentTo;
    }

    public void setMailContentTo(String mailContentTo) {
        this.mailContentTo = mailContentTo;
    }

    public String getMailContentCc() {
        return mailContentCc;
    }

    public void setMailContentCc(String mailContentCc) {
        this.mailContentCc = mailContentCc;
    }

    public String getMailContentBcc() {
        return mailContentBcc;
    }

    public void setMailContentBcc(String mailContentBcc) {
        this.mailContentBcc = mailContentBcc;
    }

    public String getMailIdTo() {
        return mailIdTo;
    }

    public void setMailIdTo(String mailIdTo) {
        this.mailIdTo = mailIdTo;
    }

    public String getMailIdCc() {
        return mailIdCc;
    }

    public void setMailIdCc(String mailIdCc) {
        this.mailIdCc = mailIdCc;
    }

    public String getMailIdBcc() {
        return mailIdBcc;
    }

    public void setMailIdBcc(String mailIdBcc) {
        this.mailIdBcc = mailIdBcc;
    }

    public String getElapsedDays() {
        return elapsedDays;
    }

    public void setElapsedDays(String elapsedDays) {
        this.elapsedDays = elapsedDays;
    }

    public String getPortName() {
        return portName;
    }

    public void setPortName(String portName) {
        this.portName = portName;
    }

    public String getContainerTypeId() {
        return containerTypeId;
    }

    public void setContainerTypeId(String containerTypeId) {
        this.containerTypeId = containerTypeId;
    }

    public String getConsolName() {
        return consolName;
    }

    public void setConsolName(String consolName) {
        this.consolName = consolName;
    }

    public String getEmptyBondValidity() {
        return emptyBondValidity;
    }

    public void setEmptyBondValidity(String emptyBondValidity) {
        this.emptyBondValidity = emptyBondValidity;
    }

    public String getConsolNames() {
        return consolNames;
    }

    public void setConsolNames(String consolNames) {
        this.consolNames = consolNames;
    }

    public String getLinernames() {
        return linernames;
    }

    public void setLinernames(String linernames) {
        this.linernames = linernames;
    }

    public String getConTypeId() {
        return conTypeId;
    }

    public void setConTypeId(String conTypeId) {
        this.conTypeId = conTypeId;
    }

    public String getContainerTypes() {
        return containerTypes;
    }

    public void setContainerTypes(String containerTypes) {
        this.containerTypes = containerTypes;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getEmptyValidity() {
        return emptyValidity;
    }

    public void setEmptyValidity(String emptyValidity) {
        this.emptyValidity = emptyValidity;
    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getdAge() {
        return dAge;
    }

    public void setdAge(String dAge) {
        this.dAge = dAge;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
 
    
    
}
