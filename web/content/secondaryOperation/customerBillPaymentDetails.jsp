<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage(value) {
                if(value == 'ExportExcel'){
                document.accountReceivable.action = '/throttle/handleAccountsReceivable.do?param=ExportExcel';
                document.accountReceivable.submit();
                }else{
                document.accountReceivable.action = '/throttle/handleAccountsReceivable.do?param=Search';
                document.accountReceivable.submit();
                }
            }
            
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
            }
            
            
         function viewInvoiceDetails(customerId) {
         var fromDate = document.getElementById("fromDate").value;
         var toDate = document.getElementById("toDate").value;
         var invoiceType = document.getElementById("invoiceType").value;
            window.open('/throttle/handleAccountsReceivableDetails.do?customerId='+customerId+'&param=Search&fromDate='+fromDate+'&toDate='+toDate, 'PopupPage', 'height = 800, width = 1150, scrollbars = yes, resizable = yes');
        }
        </script>
<style type="text/css">





.container {width: 960px; margin: 0 auto; overflow: hidden;}
.content {width:800px; margin:0 auto; padding-top:50px;}
.contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

/* STOP ANIMATION */



/* Second Loadin Circle */

.circle1 {
	background-color: rgba(0,0,0,0);
	border:5px solid rgba(100,183,229,0.9);
	opacity:.9;
	border-left:5px solid rgba(0,0,0,0);
/*	border-right:5px solid rgba(0,0,0,0);*/
	border-radius:50px;
	/*box-shadow: 0 0 15px #2187e7; */
/*	box-shadow: 0 0 15px blue;*/
	width:40px;
	height:40px;
	margin:0 auto;
	position:relative;
	top:-50px;
	-moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
	-ms-animation:spinoffPulse 1s infinite linear;
	-o-animation:spinoffPulse 1s infinite linear;
}

@-moz-keyframes spinoffPulse {
	0% { -moz-transform:rotate(0deg); }
	100% { -moz-transform:rotate(360deg);  }
}
@-webkit-keyframes spinoffPulse {
	0% { -webkit-transform:rotate(0deg); }
	100% { -webkit-transform:rotate(360deg);  }
}
@-ms-keyframes spinoffPulse {
	0% { -ms-transform:rotate(0deg); }
	100% { -ms-transform:rotate(360deg);  }
}
@-o-keyframes spinoffPulse {
	0% { -o-transform:rotate(0deg); }
	100% { -o-transform:rotate(360deg);  }
}



</style>
    </head>
    <body onload="setValue();">
        <form name="accountReceivable" action=""  method="post">
            <br>
            <br>
            Customer Payment Details
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="border" >
                                        <tr>
                                        <td  class="text1" >Customer</td>
                                        <td  class="text1" height="30">
                                            Mateshwary Road Carriers
                                        </td>
                                       </tr>
                                        <tr>
                                        <td  class="text2" >Total Payment Due</td>
                                        <td class="text2" height="30">
                                            SAR.123,588.00
                                        </td>
                                       </tr>
                                        <tr>
                                        <td  class="text2" ><font color="red">*</font>Paid Amount SAR.</td>
                                        <td class="text2" height="30">
                                            <input type="text"  name="paidValue" value="0" />
                                        </td>
                                       </tr>
                                        <tr>
                                        <td  class="text1" ><font color="red">*</font>Pay Mode.</td>
                                        <td class="text1" height="30">
                                            <select name="payMode">
                                                <option value="Cheque">Cheque</option>
                                            </select>
                                        </td>
                                       </tr>
                                       <tr>
                                        <td  class="text1" ><font color="red">*</font>Cheque No</td>
                                        <td class="text1" height="30">
                                            <input type="text"  name="paidValue" value="" />
                                        </td>
                                       </tr>
                                       <tr>
                                        <td  class="text2" ><font color="red">*</font>Cheque Date</td>
                                        <td class="text2" height="30">
                                            <input type="text"  name="paidValue" value="" />
                                        </td>
                                       </tr>
                                       <tr>
                                        <td  class="text1" ><font color="red">*</font>Cheque Bank</td>
                                        <td class="text1" height="30">
                                            <input type="text"  name="paidValue" value="" />
                                        </td>
                                       </tr>
                                       <tr>
                                        <td  class="text2" ><font color="red">*</font>Pay Remarks</td>
                                        <td class="text2" height="30">
                                            <textarea rows="6" cols="30"></textarea>
                                        </td>
                                       </tr>
                                      

                                </table>
              
                                    
                        <br>
                        <center><input type="button" class="button" name="save" value="proceed" onClick="actionScript();" /> </center>
                        <br>
       
    </body>    
</html>