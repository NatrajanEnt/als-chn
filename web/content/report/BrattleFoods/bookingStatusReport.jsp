
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


<script type="text/javascript">
$(document).ready(function() {
$("#datepicker").datepicker({
showOn: "button",
buttonImage: "calendar.gif",
buttonImageOnly: true

});
});

$(function() {
//alert("cv");
$(".datepicker").datepicker({
/*altField: "#alternate",
altFormat: "DD, d MM, yy"*/
changeMonth: true, changeYear: true
});
});



</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
$(document).ready(function() {
$("#tabs").tabs();
});
</script>


<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == 'ExportExcel') {
                document.trip.action = '/throttle/bookingReport.do?param=ExportExcel';
                document.trip.submit();
            } else {
                document.trip.action = '/throttle/bookingReport.do?param=Search';
                document.trip.submit();
            }
        }
    }
   

</script>

<style>
#index td {
color:white;
font-weight: bold;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Booking Report</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>

                <form name="trip" method="post">
                    <br>
                    <%@ include file="/content/common/message.jsp" %>
                    <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                    </style>


                    <style>
                    #index td {
                        color:white;
                        font-weight: bold;
                    }
                    </style>
                    <table class="table table-info mb30 table-hover" style="width:100%;">
                        <thead>
                        <th colspan="4" style="background-color:#5BC0DE;width:100%;height:25px;color:white;font-size:13px;">Booking Details</th>
                        </thead>
                        <tbody>
                            <tr>
                                <td>From Date</td>
                                <td><input name="fromDate" id="fromDate" type="text" class="datepicker" value='<c:out value="${fromDate}" />' readonly style="width:180px;height:40px;" /></td>
                                <td>To Date</td>
                                <td><input name="toDate" id="toDate" type="text" class="datepicker" value='<c:out value="${toDate}" />' readonly style="width:180px;height:40px;" /></td>
                            </tr>
                            <tr height="40" align="center">
                                <td colspan="4" align="center"> 
                                    <input type="button" class="btn btn-info" name="Search"  id="Search"  value="<spring:message code="sales.label.customer.search"  text="Search"/>" onclick="submitPage(this.name);" >&nbsp;&nbsp;
                                    <input type="button" class="btn btn-info" name="ExportExcel" id="ExportExcel"  value="<spring:message code="sales.label.customer.Export Excel"  text="Export Excel"/>" onclick="submitPage(this.name);" >
                                </td>
                            </tr>
                        </tbody>
                    </table>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <li class="active" data-toggle="tab"><a href="#booking"><span>Daily Booking Report</span></a></li>
                            <li data-toggle="tab"><a href="#started"><span>Started Trips</span></a></li>
                            <li data-toggle="tab"><a href="#inProg"><span>InProgress</span></a></li>
                            <li data-toggle="tab"><a href="#ended"><span>Ended Trips</span></a></li>
                            <li data-toggle="tab"><a href="#closure"><span>Closure Trips</span></a></li>
                            <li data-toggle="tab"><a href="#moffusil"><span>Moffusil-Own</span></a></li>
                            <li data-toggle="tab"><a href="#local"><span>Local-Own</span></a></li>
                            <li data-toggle="tab"><a href="#cancel"><span>Cancelled Trips</span></a></li>
                            <li data-toggle="tab"><a href="#workshop"><span>Workshop</span></a></li>
                            <li data-toggle="tab"><a href="#idle"><span>Idle-No Driver</span></a></li>
                            <li data-toggle="tab"><a href="#wfl"><span>WFL</span></a></li>
                        </ul>

                        <div id="booking" class="tab-pane active">
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th align="center" rowspan="2">S No</th>
                                        <th align="center" rowspan="2">Customer Name</th>
                                        <th align="center" rowspan="2">Route Interim</th>
                                        <th align="center" colspan="2">&emsp;&emsp;Qty</th>
                                        <th align="center" colspan="2">&emsp;&emsp;Own</th>
                                        <th align="center" colspan="2">&emsp;&emsp;Hire</th>
                                    </tr>
                                    <tr>
                                        <th align="center">20 ft</th>
                                        <th align="center">40 ft</th>
                                        <th align="center">20 ft</th>
                                        <th align="center">40 ft</th>
                                        <th align="center">20 ft</th>
                                        <th align="center">40 ft</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${bookingList != null}" >
                                        <% int index0 = 0;%>
                                        <c:forEach items="${bookingList}" var="book">
                                            <%index0++;%>
                                            <%
                                                        String classText0 = "";
                                                        int oddEven0 = index0 % 2;
                                                        if (oddEven0 > 0) {
                                                            classText0 = "text1";
                                                        } else {
                                                            classText0 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index0%></td>
                                                <td ><c:out value="${book.customerName}" /></td>
                                                <td ><c:out value="${book.routeInfo}" /></td>
                                                <td ><c:out value="${book.total20Count}" /></td>
                                                <td ><c:out value="${book.total40Count}" /></td>
                                                <td ><c:out value="${book.own20Count}" /></td>
                                                <td ><c:out value="${book.own40Count}" /></td>
                                                <td ><c:out value="${book.hire20Count}" /></td>
                                                <td ><c:out value="${book.hire40Count}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th align="center">Booking Status</th>
                                        <th align="center">20 ft</th>
                                        <th align="center">40 ft</th>
                                        <th align="center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${bookingReportList != null}" >

                                    <% int index = 1;%>
                                    <c:forEach items="${bookingReportList}" var="booking">
                                        <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text1";
                                                    } else {
                                                        classText = "text2";
                                                    }
                                        %>
                                        <tr height="20">
                                            <td >
                                                <c:if test = "${booking.countType == 1}" >
                                                    Bookings Received
                                                </c:if>
                                                <c:if test = "${booking.countType == 2}" >
                                                    Started Trips
                                                </c:if>
                                                <c:if test = "${booking.countType == 3}" >
                                                    Ended Trips
                                                </c:if>
                                                <c:if test = "${booking.countType == 4}" >
                                                    Closure Trips
                                                </c:if>
                                                <c:if test = "${booking.countType == 5}" >
                                                    Pending Bookings
                                                </c:if>
                                                <c:if test = "${booking.countType == 6}" >
                                                    Cancelled Bookings
                                                </c:if>
                                            </td>
                                            <td ><c:out value="${booking.twentyFtCount}" /></td>
                                            <td ><c:out value="${booking.fourtyFtCount}" /></td>
                                            <td ><c:out value="${booking.totCount}" /></td>
                                        </tr>

                                    </c:forEach >
                                </c:if>
                                </tbody>
                            </table>
                            <br/>
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th align="center">Bookings Assigned</th>
                                        <th align="center">Own</th>
                                        <th align="center">Hire</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${movementAllocationList != null}" >

                                    <% int index1 = 1;%>
                                    <c:forEach items="${movementAllocationList}" var="allocat">
                                        <%
                                                    String classText1 = "";
                                                    int oddEven1 = index1 % 2;
                                                    if (oddEven1 > 0) {
                                                        classText1 = "text1";
                                                    } else {
                                                        classText1 = "text2";
                                                    }
                                        %>
                                        <tr height="20">
                                            <td >
                                                <c:if test = "${allocat.movementType == 1}" >
                                                    Moffusil
                                                </c:if>
                                                <c:if test = "${allocat.movementType == 2}" >
                                                    Local
                                                </c:if>
                                                <c:if test = "${allocat.movementType == 3}" >
                                                    Cancelled Trips
                                                </c:if>
                                            </td>
                                            <td ><c:out value="${allocat.ownCount}" /></td>
                                            <td ><c:out value="${allocat.hireCount}" /></td>
                                        </tr>

                                    </c:forEach >
                                </c:if>
                                </tbody>
                            </table>
                            <br/>
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th align="center">Own Vehicle Status</th>
                                        <th align="center">20 ft</th>
                                        <th align="center">40 ft</th>
                                        <th align="center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test = "${ownBookingCountList != null}" >
                                    <% int index2 = 1;%>
                                    <c:forEach items="${ownBookingCountList}" var="ownBooking">
                                        <%
                                                    String classText2 = "";
                                                    int oddEven2 = index2 % 2;
                                                    if (oddEven2 > 0) {
                                                        classText2 = "text1";
                                                    } else {
                                                        classText2 = "text2";
                                                    }
                                        %>
                                        <tr height="20">
                                            <td >
                                                <c:if test = "${ownBooking.countType == 1}" >
                                                    Started Trips
                                                </c:if>
                                                <c:if test = "${ownBooking.countType == 2}" >
                                                    Trips in Progress
                                                </c:if>
                                                <c:if test = "${ownBooking.countType == 3}" >
                                                    Ended Trips
                                                </c:if>
                                                <c:if test = "${ownBooking.countType == 4}" >
                                                    Closure Trips
                                                </c:if>
                                                <c:if test = "${ownBooking.countType == 5}" >
                                                    WorkShop
                                                </c:if>
                                                <c:if test = "${ownBooking.countType == 6}" >
                                                    Idle - No Driver
                                                </c:if>
                                            </td>
                                            <td ><c:out value="${ownBooking.twentyFtCount}" /></td>
                                            <td ><c:out value="${ownBooking.fourtyFtCount}" /></td>
                                            <td ><c:out value="${ownBooking.totCount}" /></td>
                                        </tr>

                                    </c:forEach >
                                </c:if>   
                                </tbody>
                            </table>
                            <br/>
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th align="center">Own Vehicle Availability</th>
                                        <th align="center">20 ft</th>
                                        <th align="center">40 ft</th>
                                        <th align="center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test = "${ownVehicleAvail != null}" >
                                    <% int index3 = 1;%>
                                    <c:forEach items="${ownVehicleAvail}" var="ownVehAvai">
                                        <%
                                                    String classText3 = "";
                                                    int oddEven3 = index3 % 2;
                                                    if (oddEven3 > 0) {
                                                        classText3 = "text1";
                                                    } else {
                                                        classText3 = "text2";
                                                    }
                                        %>
                                        <tr height="20">
                                            <td >
                                                <c:if test = "${ownVehAvai.countType == 1}" >
                                                    WFL
                                                </c:if>
                                                <c:if test = "${ownVehAvai.countType == 2}" >
                                                    Vehicles can plan for next trip
                                                </c:if>
                                            </td>
                                            <td ><c:out value="${ownVehAvai.twentyFtCount}" /></td>
                                            <td ><c:out value="${ownVehAvai.fourtyFtCount}" /></td>
                                            <td ><c:out value="${ownVehAvai.totCount}" /></td>
                                        </tr>

                                    </c:forEach >
                                </c:if>
                                </tbody>
                            </table>
                            <br/>
                            <table class="table table-info mb30 table-hover" style="width:100%">
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th align="center">Hire Vehicle Status</th>
                                        <th align="center">20 ft</th>
                                        <th align="center">40 ft</th>
                                        <th align="center">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:if test = "${hireBookingCountList != null}" >
                                    <% int index4 = 1;%>
                                    <c:forEach items="${hireBookingCountList}" var="hire">
                                        <%
                                                    String classText4 = "";
                                                    int oddEven4 = index4 % 2;
                                                    if (oddEven4 > 0) {
                                                        classText4 = "text1";
                                                    } else {
                                                        classText4 = "text2";
                                                    }
                                        %>
                                        <tr height="20">
                                            <td >
                                                <c:if test = "${hire.countType == 1}" >
                                                    Started Trips
                                                </c:if>
                                                <c:if test = "${hire.countType == 2}" >
                                                    Trips in Progress
                                                </c:if>
                                                <c:if test = "${hire.countType == 3}" >
                                                    Ended Trips
                                                </c:if>
                                                <c:if test = "${hire.countType == 4}" >
                                                    Closure Trips
                                                </c:if>
                                            </td>
                                            <td ><c:out value="${hire.twentyFtCount}" /></td>
                                            <td ><c:out value="${hire.fourtyFtCount}" /></td>
                                            <td ><c:out value="${hire.totCount}" /></td>
                                        </tr>

                                    </c:forEach >
                                </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                            <br>
                        </div>
                        <div id="started" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Goods</th>
                                        <th>Vehicle No</th>
                                        <th>TripCode</th>
                                        <th>Container No</th>
                                        <th>LR Nos</th>
                                        <th>Pkg/Wgt/Cbm</th>
                                        <th>Trip Date</th>
                                        <th>Order Type</th>
                                        <th>Transporter</th>
                                        <th>Route</th>
                                        <th>Driver</th>
                                        <th>Vehicle Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${tripStartContainerDetails != null}" >
                                        <% int index5 = 0;%>
                                        <c:forEach items="${tripStartContainerDetails}" var="start">
                                            <%index5++;%>
                                            <%
                                                        String classText5 = "";
                                                        int oddEven5 = index5 % 2;
                                                        if (oddEven5 > 0) {
                                                            classText5 = "text1";
                                                        } else {
                                                            classText5 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index5%></td>
                                                <td ><c:out value="${start.orderReferenceNo}" /></td>
                                                <td ><c:out value="${start.custName}" /></td>
                                                <td ><c:out value="${start.description}" /></td>
                                                <td ><c:out value="${start.regNo}" /></td>
                                                <td ><c:out value="${start.tripCode}" /></td>
                                                <td ><c:out value="${start.containerNo}" /></td>
                                                <td ><c:out value="${start.tripLrNo}" /></td>
                                                <td ><c:out value="${start.containerDetail}" /></td>
                                                <td ><c:out value="${start.tripStartDate}" /></td>
                                                <td ><c:out value="${start.movementType}" /></td>
                                                <td ><c:out value="${start.vendor}" /></td>
                                                <td ><c:out value="${start.routeInfo}" /></td>
                                                <td ><c:out value="${start.driverName}" /></td>
                                                <td ><c:out value="${start.vehicleType}" /></td>
                                                <td ><c:out value="${start.status}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="inProg" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Goods</th>
                                        <th>Vehicle No</th>
                                        <th>TripCode</th>
                                        <th>Container No</th>
                                        <th>LR Nos</th>
                                        <th>Pkg/Wgt/Cbm</th>
                                        <th>Trip Date</th>
                                        <th>Order Type</th>
                                        <th>Transporter</th>
                                        <th>Route</th>
                                        <th>Driver</th>
                                        <th>Vehicle Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${tripInProgressDetails != null}" >
                                        <% int index6 = 0;%>
                                        <c:forEach items="${tripInProgressDetails}" var="inProgress">
                                            <%index6++;%>
                                            <%
                                                        String classText6 = "";
                                                        int oddEven6 = index6 % 2;
                                                        if (oddEven6 > 0) {
                                                            classText6 = "text1";
                                                        } else {
                                                            classText6 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index6%></td>
                                                <td ><c:out value="${inProgress.orderReferenceNo}" /></td>
                                                <td ><c:out value="${inProgress.custName}" /></td>
                                                <td ><c:out value="${inProgress.description}" /></td>
                                                <td ><c:out value="${inProgress.regNo}" /></td>
                                                <td ><c:out value="${inProgress.tripCode}" /></td>
                                                <td ><c:out value="${inProgress.containerNo}" /></td>
                                                <td ><c:out value="${inProgress.tripLrNo}" /></td>
                                                <td ><c:out value="${inProgress.containerDetail}" /></td>
                                                <td ><c:out value="${inProgress.tripStartDate}" /></td>
                                                <td ><c:out value="${inProgress.movementType}" /></td>
                                                <td ><c:out value="${inProgress.vendor}" /></td>
                                                <td ><c:out value="${inProgress.routeInfo}" /></td>
                                                <td ><c:out value="${inProgress.driverName}" /></td>
                                                <td ><c:out value="${inProgress.vehicleType}" /></td>
                                                <td ><c:out value="${inProgress.status}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="ended" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Goods</th>
                                        <th>Vehicle No</th>
                                        <th>TripCode</th>
                                        <th>Container No</th>
                                        <th>LR Nos</th>
                                        <th>Pkg/Wgt/Cbm</th>
                                        <th>Trip Date</th>
                                        <th>Order Type</th>
                                        <th>Transporter</th>
                                        <th>Route</th>
                                        <th>Driver</th>
                                        <th>Vehicle Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${endedTrips != null}" >
                                        <% int index7 = 0;%>
                                        <c:forEach items="${endedTrips}" var="end">
                                            <%index7++;%>
                                            <%
                                                        String classText7 = "";
                                                        int oddEven7 = index7 % 2;
                                                        if (oddEven7 > 0) {
                                                            classText7 = "text1";
                                                        } else {
                                                            classText7 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index7%></td>
                                                <td ><c:out value="${end.orderReferenceNo}" /></td>
                                                <td ><c:out value="${end.custName}" /></td>
                                                <td ><c:out value="${end.description}" /></td>
                                                <td ><c:out value="${end.regNo}" /></td>
                                                <td ><c:out value="${end.tripCode}" /></td>
                                                <td ><c:out value="${end.containerNo}" /></td>
                                                <td ><c:out value="${end.tripLrNo}" /></td>
                                                <td ><c:out value="${end.containerDetail}" /></td>
                                                <td ><c:out value="${end.tripStartDate}" /></td>
                                                <td ><c:out value="${end.movementType}" /></td>
                                                <td ><c:out value="${end.vendor}" /></td>
                                                <td ><c:out value="${end.routeInfo}" /></td>
                                                <td ><c:out value="${end.driverName}" /></td>
                                                <td ><c:out value="${end.vehicleType}" /></td>
                                                <td ><c:out value="${end.status}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="closure" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Goods</th>
                                        <th>Vehicle No</th>
                                        <th>TripCode</th>
                                        <th>Container No</th>
                                        <th>LR Nos</th>
                                        <th>Pkg/Wgt/Cbm</th>
                                        <th>Trip Date</th>
                                        <th>Order Type</th>
                                        <th>Transporter</th>
                                        <th>Route</th>
                                        <th>Driver</th>
                                        <th>Vehicle Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${clsureTrips != null}" >
                                        <% int index8 = 0;%>
                                        <c:forEach items="${clsureTrips}" var="close">
                                            <%index8++;%>
                                            <%
                                                        String classText8 = "";
                                                        int oddEven8 = index8 % 2;
                                                        if (oddEven8 > 0) {
                                                            classText8 = "text1";
                                                        } else {
                                                            classText8 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index8%></td>
                                                <td ><c:out value="${close.orderReferenceNo}" /></td>
                                                <td ><c:out value="${close.custName}" /></td>
                                                <td ><c:out value="${close.description}" /></td>
                                                <td ><c:out value="${close.regNo}" /></td>
                                                <td ><c:out value="${close.tripCode}" /></td>
                                                <td ><c:out value="${close.containerNo}" /></td>
                                                <td ><c:out value="${close.tripLrNo}" /></td>
                                                <td ><c:out value="${close.containerDetail}" /></td>
                                                <td ><c:out value="${close.tripStartDate}" /></td>
                                                <td ><c:out value="${close.movementType}" /></td>
                                                <td ><c:out value="${close.vendor}" /></td>
                                                <td ><c:out value="${close.routeInfo}" /></td>
                                                <td ><c:out value="${close.driverName}" /></td>
                                                <td ><c:out value="${close.vehicleType}" /></td>
                                                <td ><c:out value="${close.status}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="moffusil" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Goods</th>
                                        <th>Vehicle No</th>
                                        <th>TripCode</th>
                                        <th>Container No</th>
                                        <th>LR Nos</th>
                                        <th>Pkg/Wgt/Cbm</th>
                                        <th>Trip Date</th>
                                        <th>Order Type</th>
                                        <th>Transporter</th>
                                        <th>Route</th>
                                        <th>Driver</th>
                                        <th>Vehicle Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${moffusilOwn != null}" >
                                        <% int index9 = 0;%>
                                        <c:forEach items="${moffusilOwn}" var="moff">
                                            <%index9++;%>
                                            <%
                                                        String classText9 = "";
                                                        int oddEven9 = index9 % 2;
                                                        if (oddEven9 > 0) {
                                                            classText9 = "text1";
                                                        } else {
                                                            classText9 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index9%></td>
                                                <td ><c:out value="${moff.orderReferenceNo}" /></td>
                                                <td ><c:out value="${moff.custName}" /></td>
                                                <td ><c:out value="${moff.description}" /></td>
                                                <td ><c:out value="${moff.regNo}" /></td>
                                                <td ><c:out value="${moff.tripCode}" /></td>
                                                <td ><c:out value="${moff.containerNo}" /></td>
                                                <td ><c:out value="${moff.tripLrNo}" /></td>
                                                <td ><c:out value="${moff.containerDetail}" /></td>
                                                <td ><c:out value="${moff.tripStartDate}" /></td>
                                                <td ><c:out value="${moff.movementType}" /></td>
                                                <td ><c:out value="${moff.vendor}" /></td>
                                                <td ><c:out value="${moff.routeInfo}" /></td>
                                                <td ><c:out value="${moff.driverName}" /></td>
                                                <td ><c:out value="${moff.vehicleType}" /></td>
                                                <td ><c:out value="${moff.status}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="local" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Goods</th>
                                        <th>Vehicle No</th>
                                        <th>TripCode</th>
                                        <th>Container No</th>
                                        <th>LR Nos</th>
                                        <th>Pkg/Wgt/Cbm</th>
                                        <th>Trip Date</th>
                                        <th>Order Type</th>
                                        <th>Transporter</th>
                                        <th>Route</th>
                                        <th>Driver</th>
                                        <th>Vehicle Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${localOwn != null}" >
                                        <% int index10 = 0;%>
                                        <c:forEach items="${localOwn}" var="loc">
                                            <%index10++;%>
                                            <%
                                                        String classText10 = "";
                                                        int oddEven10 = index10 % 2;
                                                        if (oddEven10 > 0) {
                                                            classText10 = "text1";
                                                        } else {
                                                            classText10 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index10%></td>
                                                <td ><c:out value="${loc.orderReferenceNo}" /></td>
                                                <td ><c:out value="${loc.custName}" /></td>
                                                <td ><c:out value="${loc.description}" /></td>
                                                <td ><c:out value="${loc.regNo}" /></td>
                                                <td ><c:out value="${loc.tripCode}" /></td>
                                                <td ><c:out value="${loc.containerNo}" /></td>
                                                <td ><c:out value="${loc.tripLrNo}" /></td>
                                                <td ><c:out value="${loc.containerDetail}" /></td>
                                                <td ><c:out value="${loc.tripStartDate}" /></td>
                                                <td ><c:out value="${loc.movementType}" /></td>
                                                <td ><c:out value="${loc.vendor}" /></td>
                                                <td ><c:out value="${loc.routeInfo}" /></td>
                                                <td ><c:out value="${loc.driverName}" /></td>
                                                <td ><c:out value="${loc.vehicleType}" /></td>
                                                <td ><c:out value="${loc.status}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="cancel" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Goods</th>
                                        <th>Vehicle No</th>
                                        <th>TripCode</th>
                                        <th>Container No</th>
                                        <th>LR Nos</th>
                                        <th>Pkg/Wgt/Cbm</th>
                                        <th>Trip Date</th>
                                        <th>Order Type</th>
                                        <th>Transporter</th>
                                        <th>Route</th>
                                        <th>Driver</th>
                                        <th>Vehicle Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${cancelledTrips != null}" >
                                        <% int index11 = 0;%>
                                        <c:forEach items="${cancelledTrips}" var="can">
                                            <%index11++;%>
                                            <%
                                                        String classText11 = "";
                                                        int oddEven11 = index11 % 2;
                                                        if (oddEven11 > 0) {
                                                            classText11 = "text1";
                                                        } else {
                                                            classText11 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index11%></td>
                                                <td ><c:out value="${can.orderReferenceNo}" /></td>
                                                <td ><c:out value="${can.custName}" /></td>
                                                <td ><c:out value="${can.description}" /></td>
                                                <td ><c:out value="${can.regNo}" /></td>
                                                <td ><c:out value="${can.tripCode}" /></td>
                                                <td ><c:out value="${can.containerNo}" /></td>
                                                <td ><c:out value="${can.tripLrNo}" /></td>
                                                <td ><c:out value="${can.containerDetail}" /></td>
                                                <td ><c:out value="${can.tripStartDate}" /></td>
                                                <td ><c:out value="${can.movementType}" /></td>
                                                <td ><c:out value="${can.vendor}" /></td>
                                                <td ><c:out value="${can.routeInfo}" /></td>
                                                <td ><c:out value="${can.driverName}" /></td>
                                                <td ><c:out value="${can.vehicleType}" /></td>
                                                <td ><c:out value="${can.status}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="workshop" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>TruckNo</th>
                                        <th>Branch</th>
                                        <th>Status</th>
                                        <th>ServiceType</th>
                                        <th>How Many Days in Workshop</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${workshopList != null}" >
                                        <% int index13 = 0;%>
                                        <c:forEach items="${workshopList}" var="wor">
                                            <%index13++;%>
                                            <%
                                                        String classText13 = "";
                                                        int oddEven13 = index13 % 2;
                                                        if (oddEven13 > 0) {
                                                            classText13 = "text1";
                                                        } else {
                                                            classText13 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index13%></td>
                                                <td ><c:out value="${wor.vehicleNo}" /></td>
                                                <td ><c:out value="${wor.operationPoint}" /></td>
                                                <td ><c:out value="${wor.statusName}" /></td>
                                                <td ><c:out value="${wor.serviceTypeName}" /></td>
                                                <td ><c:out value="${wor.age}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="idle" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>TruckNo</th>
                                        <th>Branch</th>
                                        <th>VehicleType</th>
                                        <th>How Many Days in Idle?</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${wfuList != null}" >
                                        <% int index15 = 0;%>
                                        <c:forEach items="${wfuList}" var="wfu">
                                            <%index15++;%>
                                            <%
                                                        String classText15 = "";
                                                        int oddEven15 = index15 % 2;
                                                        if (oddEven15 > 0) {
                                                            classText15 = "text1";
                                                        } else {
                                                            classText15 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index15%></td>
                                                <td ><c:out value="${wfu.vehicleNo}" /></td>
                                                <td ><c:out value="${wfu.operationPoint}" /></td>
                                                <td ><c:out value="${wfu.tonnage}" /></td>
                                                <td ><c:out value="${wfu.age}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>

                            </center>
                            <br>

                        </div>
                        <div id="wfl" >

                            <table class="table table-info mb30 table-hover" style="width:100%;"    id="table2" >
                                <thead style="background-color:#5BC0DE;width:100%;color:white;font-size:13px;">
                                    <tr height="30">
                                        <th>S No</th>
                                        <th>TruckNo</th>
                                        <th>Branch</th>
                                        <th>VehicleType</th>
                                        <th>How Many Days in Idle?</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:if test = "${wfuList != null}" >
                                        <% int index16 = 0;%>
                                        <c:forEach items="${tripNotStartedList}" var="notStart">
                                            <%index16++;%>
                                            <%
                                                        String classText16 = "";
                                                        int oddEven16 = index16 % 2;
                                                        if (oddEven16 > 0) {
                                                            classText16 = "text1";
                                                        } else {
                                                            classText16 = "text2";
                                                        }
                                            %>
                                            <tr height="20">
                                                <td><%=index16%></td>
                                                <td ><c:out value="${notStart.vehicleNo}" /></td>
                                                <td ><c:out value="${notStart.operationPoint}" /></td>
                                                <td ><c:out value="${notStart.tonnage}" /></td>
                                                <td ><c:out value="${notStart.age}" /></td>
                                            </tr>

                                        </c:forEach >
                                    </c:if>
                                </tbody>
                            </table>
                            <br>
                            <br>
                            <br>
                            <center>
                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                            </center>
                            <br>

                        </div>

                        <script>
                            $('.btnNext').click(function() {
                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                            });
                            $('.btnPrevious').click(function() {
                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                            });
                        </script>

                    </div>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>