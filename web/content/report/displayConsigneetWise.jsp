<%-- 
    Document   : displayConsigneetWise
    Created on : Jun 11, 2013, 11:50:38 PM
    Author     : Entitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <%@ page import="ets.domain.security.business.SecurityTO" %>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script src="/throttle/js/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script src="/throttle/js/TableSort.js" language="javascript" type="text/javascript"></script>
        <link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {

                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
        <script>
            function submitPage(value){
               if(value == 'Fetch Data'){
               var param = 'No';

               } else{
               var param = 'ExportExcel';
               }
               //alert('param is::;'+param);
                if(textValidation(document.consigneewise.fromDate,'From Date')){
                    return;
                }else if(textValidation(document.consigneewise.toDate,'To Date')){
                    return;
                }
                //alert(check);
                document.consigneewise.action="/throttle/searchConsigneewiseDetails.do?param="+param;
                document.consigneewise.submit();

            }
            function setFocus(){
                var consignmentName='<%=request.getAttribute("consignmentName")%>';
                var sdate='<%=request.getAttribute("fromDate")%>';
                var edate='<%=request.getAttribute("toDate")%>';
                if(consignmentName!='null'){
                document.consigneewise.consignmentName.value=consignmentName;
                }
                
                if(sdate!='null' && edate!='null'){
                    document.consigneewise.fromDate.value=sdate;
                    document.consigneewise.toDate.value=edate;
                }
            }
        </script>
    </head>
    <body onload="setFocus()" >
        <form name="consigneewise" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>
         <br/>
         <table cellpadding="0" cellspacing="2" align="center" border="0" width="700" id="report" bgcolor="#97caff" style="margin-top:0px;">
              <tr>
                    <td><b>Consignee Wise Report</b></td>
                    <td align="right"><span id="openClose" onclick="displayCollapse();" style="cursor: pointer;">Close</span>&nbsp;</td>
                </tr>
                <tr id="exp_table"  style="display: block;">
                    <td colspan="2" style="padding:15px;" align="right">
                        <div class="tabs" align="center" style="width:800px">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <td  height="30">Consignee Name:   </td>
                                    <td  height="30"><input type="text" name="consignmentName" id="consignmentName" onfocus="getConsigneeNames();" autocomplete="off" onPaste="return false" onDrag="return false" onDrop="return false"></td>
                                    <td  height="30"><font color="red">*</font>From Date :</td>
                                    <td  height="30"><input type="text" name="fromDate" id="fromDate" class="datepicker" value="" />  </td>
                                    <td  height="30"><font color="red">*</font>To Date : </td>
                                    <td  height="30"><input type="text" name="toDate" id="toDate" class="datepicker" value="" /></td>
                                </tr>
                               
                                 <tr>
                                     <td  height="30" colspan="5" align="center" >
                                            <input type="button"  value="Fetch Data"  class="button" name="Fetch Data" onClick="submitPage(this.value)">&nbsp;&nbsp;&nbsp;
                                            <input type="button"  value="Export to Excel"  class="button" name="excelExport" onClick="submitPage(this.value)">
                                    </td>

                                </tr>

                            </table>

                        </div>
                    </td>
                </tr>
         </table>
         <br>
          <%
                            DecimalFormat df = new DecimalFormat("0.00##");
                            String tripId = "",tripDate = "",destination = "",vehicleNumber = "",productName = "",crossing = "";
                            String invoiceNo = "",gpno = "",totalTonnage = "",distance = "",totalFreightAmount = "",consigneeName = "";
                            double totalFreight = 0,subTotalFreight = 0,totalSettlement = 0;
                            double grandTotalTonnage = 0,subTotalTonnage = 0;
                            String className = "text2",settlementAmount = "",existConsigneeName = "";
                            int index = 0;
                            int sno = 0;
%>
                            <table  border="0" class="border" align="center" width="850" cellpadding="0" cellspacing="0" id="bg">
                                <c:if test = "${consigneeWiseDetailsList != null}" >
                                    <%
                                    ArrayList consigneeWiseDetailsList = (ArrayList) request.getAttribute("consigneeWiseDetailsList");
                                        Iterator detailsItr = consigneeWiseDetailsList.iterator();
                                        ReportTO repTO = null;
                                        if (consigneeWiseDetailsList.size() != 0) {
                                            %>
                                <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30" width="100">Consignee Name</td>
                                <td class="contentsub" height="30" width="80">Trip Id</td>
                                <td class="contentsub" height="30" width="80">Trip Date</td>
                                <td class="contentsub" height="30" width="80">Destination</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">GP No</td>
                                <td class="contentsub" height="30" width="80">Invoice No</td>
                                <td class="contentsub" height="30">Total Tonnage</td>
                                <td class="contentsub" height="30">Freight Amount</td>
                                </tr>
                                <%
                                while (detailsItr.hasNext()) {
                                        index++;
                                        sno++;
                                       

                                        repTO = new ReportTO();
                                        repTO = (ReportTO) detailsItr.next();
                                        consigneeName  = repTO.getConsigneeName();
                                        if(consigneeName == null){
                                            consigneeName = "";
                                            }
                                        tripId  = repTO.getTripId();
                                        if(tripId == null){
                                            tripId = "";
                                            }
                                       
                                        tripDate  = repTO.getTripDate();
                                        if(tripDate == null){
                                            tripDate = "";
                                            }
                                        vehicleNumber  = repTO.getVehicleId();
                                        if(vehicleNumber == null){
                                            vehicleNumber = "";
                                            }
                                        gpno  = repTO.getGpno();
                                        if(gpno == null){
                                            gpno = "";
                                            }
                                        
                                        invoiceNo  = repTO.getInvoiceNo();
                                        if(invoiceNo == null){
                                            invoiceNo = "";
                                            }
                                        destination  = repTO.getDestination();
                                        if(destination == null){
                                            destination = "";
                                            }
                                        totalTonnage  = repTO.getTotalTonnage();
                                       
                                        if(totalTonnage == null){
                                            totalTonnage = "";
                                            }
                                        grandTotalTonnage += Double.parseDouble(totalTonnage);
                                        totalFreightAmount  = repTO.getTotalFreightAmount();
                                        if(totalFreightAmount == null){
                                            totalFreightAmount = "";
                                            }
                                        
                                        totalFreight += Double.parseDouble(totalFreightAmount);

                                    if(existConsigneeName !=""){
                                if(!existConsigneeName.equalsIgnoreCase(consigneeName)){
                                    if(className.equalsIgnoreCase("text1")){
                                        className = "text2";
                                        }else{
                                        className = "text1";
                                        }
                                        index++;
                                         
                        %>
                        <tr>
                            <td class="<%=className%>"  height="30" colspan="7" >&nbsp;</td>
                            <td class="<%=className%>" align="right" height="30"><b>Sub Total</b></td>
                            <td class="<%=className%>" align="right" height="30"><b><%=df.format(subTotalTonnage)%></b></td>
                            <td class="<%=className%>" align="right" height="30"><b><%=df.format(subTotalFreight)%></b></td>

                        </tr>
                        <%
                        subTotalTonnage = 0;
                        subTotalFreight = 0;

                    }
                    }
                                        subTotalTonnage += Double.parseDouble(totalTonnage);
                                        subTotalFreight += Double.parseDouble(totalFreightAmount);
                                        if(index%2 == 0){
                                        className = "text2";

                                        }else {
                                        className = "text1";
                                        }
                                         %>
                                        <tr>
                                              <td class="<%=className%>"  height="30"><%=sno%></td>
                                              <td class="<%=className%>"  height="30"><%=consigneeName%></td>
                                              <td class="<%=className%>"  height="30"><%=tripId%></td>
                                              <td class="<%=className%>"  height="30"><%=tripDate%></td>
                                              <td class="<%=className%>"  height="30"><%=destination%></td>
                                              <td class="<%=className%>"  height="30"><%=vehicleNumber%></td>
                                              <td class="<%=className%>"  height="30"><%=gpno%></td>
                                              <td class="<%=className%>" align="right" height="30"><%=invoiceNo%></td>
                                              <td class="<%=className%>" align="right" height="30"><%=totalTonnage%></td>
                                              <td class="<%=className%>" align="right" height="30"><%=totalFreightAmount%></td>
                                          </tr>

                                                <%
                                                existConsigneeName = consigneeName;

                                    } %>
                                           <tr>
                                    <td class="<%=className%>"  height="30" colspan="7" >&nbsp;</td>
                                    <td class="<%=className%>" align="right" height="30"><b>Sub Total</b></td>
                                    <td class="<%=className%>" align="right" height="30"><b><%=df.format(subTotalTonnage)%></b></td>
                                    <td class="<%=className%>" align="right" height="30"><b><%=df.format(subTotalFreight)%></b></td>
                        </tr>
                                    <tr>
                                        <td class="text2"  height="30" colspan="7" >&nbsp;</td>
                                        <td class="text2" align="right" height="30"><b>Grand Total</b></td>
                                        <td class="text2" align="right" height="30"><b><%=df.format(grandTotalTonnage)%></b></td>
                                        <td class="text2" align="right" height="30"><b><%=df.format(totalFreight)%></b></td>
                                    </tr>
                                          <%

                                            }
                                %>
                                </c:if>
                            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
<script type="text/javaScript">
        function getConsigneeNames(){
            var oTextbox = new AutoSuggestControl(document.getElementById("consignmentName"),new ListSuggestions("consignmentName","/throttle/consigneeNameSuggestions.do?"));
        }
        
    </script>
