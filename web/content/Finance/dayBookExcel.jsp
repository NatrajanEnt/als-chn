<%-- 
    Document   : dayBookExcel
    Created on : 12 Jan, 2016, 10:46:48 AM
    Author     : pavithra
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "Day_Book"+curDate+".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        
                 <c:if test = "${ledgerTransactionList != null}" >
                    <table width="700" class="border" border="1">
                        <tr class="contentsub">
                            <td class="contentsub" width="10%" ><div class="contentsub">Sno</div></td>
                            <td class="contentsub" width="10%" ><div class="contentsub">Date</div></td>
                            <td class="contentsub" width="70%"><div class="contentsub">Particulars</div></td>                            
                            <td class="contentsub" width="10%"><div class="contentsub">Debit</div></td>
                            <td class="contentsub" width="10%"><div class="contentsub">Credit</div></td>
                        </tr>
                        <c:set var="creditTotal" value="${0}" />
                        <c:set var="debitTotal" value="${0}" />
                        <%
                        String openingBalance = (String)request.getAttribute("openingBalance");
                        String amountType = (String)request.getAttribute("amountType");
                        String debitOpen = "";
                        String creditOpen = "";
                        int cntr = 0;
                        %>

                        <c:forEach items="${ledgerTransactionList}" var="txnlist">
                            <tr>
                                <% cntr++; %>
                                <td class="text1" ><%=cntr%></td>
                                <td class="text1" ><c:out value="${txnlist.accountEntryDate}" /></td>
                                <c:if test = "${txnlist.accountType == 'CREDIT'}" >

                                    <c:set var="creditTotal" value="${creditTotal + txnlist.accountAmount}"/>
                                    <td class="text1" ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Cr. <br>
                                        &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>
                                    <td class="text1" align ="right">&nbsp;</td>
                                    <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>                                    
                                </c:if>
                                <c:if test = "${txnlist.accountType == 'DEBIT'}" >
                                    <c:set var="debitTotal" value="${debitTotal + txnlist.accountAmount}"/>
                                    <td class="text1" ><c:out value="${txnlist.ledgerName}" />&nbsp; A/C &nbsp;&nbsp;&nbsp; Dr. <br>
                                        &nbsp;(Being <c:out value="${txnlist.accountNarration}" /> for Trip <c:out value="${txnlist.tripId}" />)</td>                                    
                                    <td class="text1" align ="right"><c:out value="${txnlist.accountAmount}" /></td>
                                    <td class="text1" align ="right">&nbsp;</td>
                                </c:if>
                            </tr>
                        </c:forEach>
                            <tr>
                                    <td colspan="3" align="right" class="text2"  height="30"><b>Total</b></td>
                                    <td  class="text2" align="right"  height="30"><b><c:out value="${debitTotal}" /></b></td>
                                    <td  class="text2" align="right"  height="30"><b><c:out value="${creditTotal}" /></b></td>
                              </tr>
                              
                    </table>
                </c:if>

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
