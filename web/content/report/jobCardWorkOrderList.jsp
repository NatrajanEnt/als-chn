<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="java.text.SimpleDateFormat" %>
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <!--    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>-->
    <!--<script language="javascript" src="/throttle/js/validate.js"></script>-->
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    <script language="javascript" src="/throttle/js/validate.js"></script></head>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script>


    function submitPage(value) {
//           alert("test")
//            var chek=validation();
//            if(chek=='true'){  
        if (value == 'ExportExcel') {

            document.receivedStock.action = '/throttle/handleJobCardWorkOrderList.do?param=ExportExcel';
            document.receivedStock.submit();
        } else {
            document.receivedStock.action = '/throttle/handleJobCardWorkOrderList.do';
            document.receivedStock.submit();
        }
    }
    function validation() {
        if (document.receivedStock.companyId.value == 0) {
            alert("Please Select Data for Company Name");
            document.receivedStock.companyId.focus();
            return 'false';
        } else if (textValidation(document.receivedStock.fromDate, 'From Date')) {
            return 'false';
        } else if (textValidation(document.receivedStock.toDate, 'TO Date')) {
            return'false';
        }
        return 'true';
    }



    function newWindows(woId) {
        window.open('/throttle/woDetail.do?woId=' + woId, 'PopupPage', 'height=450,width=700,scrollbars=yes,resizable=yes');
    }




    function setValues() {
        if ('<%=request.getAttribute("companyId")%>' != 'null') {
            document.receivedStock.companyId.value = '<%=request.getAttribute("companyId")%>';
        }
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.receivedStock.fromdate.value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.receivedStock.todate.value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("vendorId")%>' != 'null') {
            document.receivedStock.vendorId.value = '<%=request.getAttribute("vendorId")%>';
        }
        if ('<%=request.getAttribute("poId")%>' != 'null') {
            document.receivedStock.poId.value = '<%=request.getAttribute("poId")%>';
        }
    }
</script>

<body onload="setValues();">
    <form name="receivedStock" method="post">
        <%--<%@ include file="/content/common/path.jsp" %>--%>           
        <%@ include file="/content/common/message.jsp" %>

        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.WO LIST" text="WO LIST"/> </h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
                <ol class="breadcrumb">
                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                    <li><a href="general-forms.html"><spring:message code="hrms.label.Stores" text="Stores"/></a></li>
                    <li class=""><spring:message code="hrms.label.WO/PO LIST" text="WO/PO LIST"/></li>

                </ol>
            </div>
        </div>

        <div class="contentpanel">
            <div class="panel panel-default">
                <div class="panel-body">
                    <table class="table table-info mb30 table-hover" id="bg"   >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >WO LIST</th>
                            </tr>
                        </thead>
                    </table>

                    <table class="table table-info mb30 table-hover" id="report" >
                        <div id="first">
                            <!--    <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">-->
                            <table class="table table-info mb30 table-hover" id="report" >
                                <tr>
                                    <td height="30"><font color="red">*</font>Location</td>
                                    <td height="30"><select   name="companyId" class="form-control" style="width:240px;height:40px" >
                                            <option value="0">-select-</option>
                                            <c:if test = "${LocationLists != null}" > 
                                                <c:forEach items="${LocationLists}" var="company">   
                                                    <c:choose>
                                                        <c:when test="${company.companyTypeId==1012}" >
                                                            <option value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>                                                            
                                                        </c:when>
                                                    </c:choose>
                                                </c:forEach>
                                            </c:if>          

                                        </select></td>

                                    <td height="30">Vendor</td>
                                    <td height="30"><select name="vendorId" class="form-control" style="width:240px;height:40px" >
                                            <option value="0">-select-</option>
                                            <c:if test = "${VendorLists != null}" >
                                                <c:forEach items="${VendorLists}" var="vendor">
                                                    <c:if test = "${vendor.vendorTypeId == 1017}" >
                                                        <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                    <td height="30">Order No</td>
                                    <td height="30"><input name="poId" type="text" class="form-control" style="width:240px;height:40px"    height="30"></td>

                                </tr>

                                <tr>
                                    <!--                                    <td height="30">Order Type</td>
                                                                        <td height="30">
                                                                            <select name="orderType" class="form-control" style="width:240px;height:40px" >
                                                                                <option value='PO'> Purchase Order </option>
                                                                                <option value='WO'> Work Order </option>
                                                                            </select>
                                                                        </td>-->
                                    <td ><font color="red">*</font>From Date</td>
                                    <td ><input type="text" name="fromdate" id="fromdate" value='' autocomplete="off" class="datepicker , form-control" style="width:240px;height:40px;"/></td>
                                    <td ><font color="red">*</font>To Date</td>
                                    <td ><input type="text" name="todate" id="todate" value='' autocomplete="off" class="datepicker , form-control" style="width:240px;height:40px;"/></td>
                                    <td>
                                        <input type="button" name="Search" value="Search" class="btn btn-success" style="width:100px;height:35px;" onclick="submitPage(this.name);" >
                                    </td>
                                </tr>
                            </table>

                            </td>
                            </tr>
                    </table>
                    <!--<center>-->

<!--<input type="button" class="btn btn-success" name="ExportExcel"  value="<spring:message code="trucks.label.ExportExcel"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;">-->
                    <!--                    </center>
                                        <br>-->
                    <c:set var="laborAmt" value="0" />
                    <c:set var="spareAmt" value="0" />
                    <c:set var="laborTaxAmt" value="0" />
                    <c:set var="spareTaxAmt" value="0" />
                    <c:set var="Amount" value="0" />
                    <c:set var="totAmount" value="0" />
                    <c:if test = "${ordersList != null}" >

                        <table class="table table-info mb30 table-hover" id="table">
                            <thead>
                            <th  class="contentsub" height="30" >Sno</th>
                            <th  class="contentsub" height="30" >Date</th>
                            <th  class="contentsub" height="30" >Vehicle No</th>                                        
                            <th  class="contentsub" height="30" >JobCard No</th>                                        
                            <th  class="contentsub" height="30" >Vendor </th>                                                            
                            <th  class="contentsub" height="30" >WO No</th>                                        
                            <th  class="contentsub" height="30" >WorkDescription</th>                                        
                            <th  class="contentsub" height="30" >BillNo</th>                                                            
                            <th  class="contentsub" height="30" >BillDate</th>                                                            
                            <th  class="contentsub" height="30" >Spares Amt</th>                                                            
                            <th  class="contentsub" height="30" >Spares Tax Amt</th>                                                            
                            <th  class="contentsub" height="30" >Labour Amt</th>                                                            
                            <th  class="contentsub" height="30" >Labour Tax Amt</th>                                                            
                            <th  class="contentsub" height="30" >Total Amt</th>                                                            
                            <th  class="contentsub" height="30" >CreatedBy</th> 

                            </tr>  
                            <% int index = 0; %> 
                            <c:forEach items="${ordersList}" var="stock"> 
                                <%
                    String classText = "";
                    int oddEven = index % 2;
                    if (oddEven > 0) {
                        classText = "text2";
                    } else {
                        classText = "text1";
                    }
                                %>	

                                <tr>
                                    <td class="<%=classText%>" height="30" > <%= index+1 %> </td>
                                    <td class="<%=classText%>" height="30" ><c:out value="${stock.createdDate}"/></td>
                                <td class="<%=classText%>" height="30" ><c:out value="${stock.vehicleNo}"/></td>                                     
                                <td class="<%=classText%>" height="30" ><c:out value="${stock.jobCardNo}"/></td>                                     
                                <td class="<%=classText%>" height="30" ><c:out value="${stock.vendorName}"/></td>     

                                <td class="<%=classText%>" height="30" > <a href='/throttle/woDetail.do?woId=<c:out value="${stock.woId}"/>' onclick="return !window.open(this.href, 'somesite', 'width=800,height=500')" target="_blank" >
                                        <c:out value="${stock.woId}"/></a></td>
                                <td class="<%=classText%>" height="30" ><c:out value="${stock.problemName}"/></td>    


                                <c:if test = "${stock.billNo == '0'}" >
                                    <td class="<%=classText%>" height="30"  align='left' ><font color='red'>bill not received</font></td>                                     
                                    <td class="<%=classText%>" height="30"  align='left' ></td>                                     
                                    <td class="<%=classText%>" height="30"  align='left' ></td>                                     
                                    <td class="<%=classText%>" height="30"  align='left' ></td>                                     
                                    <td class="<%=classText%>" height="30"  align='left' ></td>                                     
                                    <td class="<%=classText%>" height="30"  align='left' ></td>                                     
                                    <td class="<%=classText%>" height="30"  align='left' ></td>                                     
                                </c:if>    
                                <c:if test = "${stock.billNo != '0'}" >                
                                    <td class="<%=classText%>" height="30" >
                                        <a href='/throttle/woBillDetail.do?woId=<c:out value="${stock.woId}"/>' onclick="return !window.open(this.href, 'somesite', 'width=800,height=500')" target="_blank" > 
                                            <c:out value="${stock.billNo}"/></a></td>    
                                    <td class="<%=classText%>" height="30" ><c:out value="${stock.billDate}"/></td>    
                                    <td class="<%=classText%>" height="30"  align='right'><c:out value="${stock.spareAmount}"/></td>                                     
                                    <td class="<%=classText%>" height="30" align='right' ><c:out value="${stock.itemTaxAmount}"/></td>                                     
                                    <td class="<%=classText%>" height="30"  align='right'><c:out value="${stock.labour}"/></td>                                     
                                    <td class="<%=classText%>" height="30"  align='right'><c:out value="${stock.serviceTaxAmnt}"/></td>                                     
                                    <td class="<%=classText%>" height="30"  align='right'><c:out value="${stock.spareAmount + stock.itemTaxAmount + stock.labour + stock.serviceTaxAmnt }"/></td>                                     
                                    <c:set var="spareAmt" value="${stock.spareAmount + spareAmt}" />
                                    <c:set var="spareTaxAmt" value="${stock.itemTaxAmount + spareTaxAmt}" />
                                    <c:set var="laborAmt" value="${stock.labour + laborAmt}" />
                                    <c:set var="laborTaxAmt" value="${stock.serviceTaxAmnt + laborTaxAmt}" />
                                    <c:set var="totAmount" value="${stock.spareAmount + stock.itemTaxAmount + stock.labour + stock.serviceTaxAmnt  + totAmount}" />
                                </c:if>        
                                <td class="<%=classText%>" height="30" ><c:out value="${stock.approver}"/></td>                   
                                </tr>              
                                <% index++;%>
                            </c:forEach>
                            <c:if test = "${totAmount != '0'}" >                
                                <tr>
                                    <td colspan="7"><td>
                                    <td>Total</td>
                                    <td align='right'><c:out value="${spareAmt}"/></td>
                                <td align='right'> <c:out value="${spareTaxAmt}"/></td>
                                <td align='right'><c:out value="${laborAmt}"/></td>
                                <td align='right'><c:out value="${laborTaxAmt}"/></td>
                                <td align='right'><c:out value="${totAmount}"/></td>
                                <td>&nbsp;</td>
                                </tr>
                            </c:if>
                        </table>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="stores.label.EntriesPerPage"  text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="stores.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="stores.label.of"  text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                        <br/>
                    </c:if>
                    <br>

                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
                    </body>
                </div>
            </div>
        </div>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>