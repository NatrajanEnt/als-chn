<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    
    
    <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %> 
    <script language="javascript" src="/throttle/js/validate.js"></script>    
    <title>MRSList</title>
</head>

<script language="javascript">
    
    function submitPage()
    {     
      if(document.stockTransfer.companyId.value==0){
            alert("Please Select Service Point Name");
             document.stockTransfer.companyId.focus();
             return;
             }        
     var temp= document.stockTransfer.companyId.value.split("-");
      document.stockTransfer.companyName.value=temp[1];
      document.stockTransfer.servicePtId.value=temp[0];      
        document.stockTransfer.action='/throttle/receiveStockList.do';
        document.stockTransfer.submit();
    }
   function submitReceive(indx, type){
      var checValidate = selectedItemValidation(); 
       var gdId=document.getElementsByName("gdIds");
       var requestId=document.getElementsByName("requestIds");
        document.stockTransfer.gdId.value=gdId[indx].value;
        document.stockTransfer.requestId.value=requestId[indx].value;
        document.stockTransfer.reqType.value=type;
        document.stockTransfer.action='/throttle/receivedItems.do';
        document.stockTransfer.submit();
       }         
    function setSelectbox(i)
    {
        var selected=document.getElementsByName("selectedIndex") ;
        selected[i];
    }
    
    function selectedItemValidation(){
        var index = document.getElementsByName("selectedIndex");
        var issuedQty = document.getElementsByName("issuedQtys");       
        var remarks = document.getElementsByName("remarks");               
        for( var i=0;(i<index.length && index.length!=0);i++ ){                       
            if( numberValidation(issuedQty[i],'Issued Quantity') ) {   
                return;
            }                        
        }
    }        
    function setValues() {       
        document.stockTransfer.companyId.focus();                     
    }    
    
    
    </script>
     <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Receive Stock" text="Receive Stock"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Stock Transfer" text="Stock Transfer"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Receive Stock" text="Receive Stock"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    
    <body onLoad=" setValues()" >
     <form method="post"  name="stockTransfer">                    
     
<%--<%@ include file="/content/common/path.jsp" %>--%>               
<%@ include file="/content/common/message.jsp" %>    

  
    <!--<table align="center" width="400" border="0" cellspacing="0" cellpadding="0" class="border">-->
<table class="table table-info mb30 table-hover"  >
    <tr>
            <td>From Service Point</td>  
            <td> 
                <select class="form-control" style="width:240px;height:40px"  name="companyId" >
                    <option value='0' >-Select-</option>  
                    <c:if test = "${servicePointList != null}" >
                        <c:forEach items="${servicePointList}" var="company"> 
                            <option value='<c:out value="${company.companyId}"/>-<c:out value="${company.companyName}"/>'><c:out value="${company.companyName}"/></option>                           
                      </c:forEach>  
                    </c:if>
                </select>
            </td>
            <input type="hidden" name="companyName" value="<%=request.getAttribute("FromServicePoint")%>" >
            <input type="hidden" name="servicePtId" value="<%=request.getAttribute("servicePtId")%>" >
                <input type="hidden" name="gdId" value="" >
                <input type="hidden" name="requestId" value="" >
                <input type="hidden" name="reqType" value="" >
            
            <td class="text1">
            <input type="button" align="center"  class="btn btn-success"  name="search" value="Search" onclick="submitPage();"></td>
        </tr>
    </table>
    <% int index = 0;%>    
    <br>
    <c:if test = "${receivedStockListAll != null}" >
<!--        <table width="500" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
            <tr>
                    <td colspan="4" height="20" align="center" class="text2" ><strong>Received StockList</strong></td>                    
                </tr>-->
<table class="table table-info mb30 table-hover" id="bg"   >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >Received StockList</th>
		</tr>
                    </thead>
            </table>
<table class="table table-info mb30 table-hover" >
    <thead>
            <tr align="center">
                <th>S.No</td>
                <th>Request No</td>
                <th>GD No</td>
                <th>To ServicePoint</td>
                <th>Receive</td>                
            </tr>                   
            </thead>
            
            <c:forEach items="${receivedStockListAll}" var="list"> 
                
                <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                %>         
                <tr> 
                    <td class="<%=classText %>" align="left" height="30"><%=index + 1%></td>
                    <td class="<%=classText %>" align="left" height="30"><input type="hidden" name="requestIds" value="<c:out value="${list.requestId}"/>"><c:out value="${list.requestId}"/></td>
                    <td class="<%=classText %>" align="left" height="30"><input type="hidden" name="gdIds" value="<c:out value="${list.gdId}"/>"><c:out value="${list.gdId}"/></td>
                    <td class="<%=classText %>" align="left" height="30"><input type="hidden" name="servicePtName" value="<c:out value="${list.companyName}"/>"><c:out value="${list.companyName}"/></td>                           
                    <td class="<%=classText %>" align="left"> <a  href="#" onclick="submitReceive(<%=index%>,'<c:out value="${list.reqType}"/>');" >Receive</a> </td>
                    <input type="hidden" name="index" value="<%=index%>">
                </tr>
                <%
            index++;
                %>
            </c:forEach >
            
        </table>
    </c:if>                 
    </form>
</body>
 </div>
            </div>
            </div>
            <%@ include file="../common/NewDesign/settings.jsp" %>