<%--
    Document   : newfuelprice
    Created on : Oct 27, 2013, 5:04:09 PM
    Author     : Arul
--%>



<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityNameList.do",
                    dataType: "json",
                    data: {
                        cityName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#cityName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var Name = ui.item.Name;
                var Id = ui.item.Id;
                $itemrow.find('#cityId').val(Id);
                $itemrow.find('#cityName').val(Name);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var itemId = item.Id;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });


    function resetCityId() {
        if (document.getElementById('cityName').value == '') {
            document.getElementById('cityId').value = '';
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Driver Salary Config </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Drivers </a></li>
            <li class="active">Driver Salary Config</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="driverSalaryConfig"  method="post" >
                    <br>
                    <table  id="bg" class="table table-info mb30 table-hover">
                        <thead>
                          <tr >
                              <th>&nbsp;</th>
                              <!--<th>&nbsp;</th>-->
                              <th colspan="3" >Driver</th>
                              <th colspan="3" >Cleaner</th>
                          </tr>
                          <tr >
                           <th>Location</th>
                           <!--<th>BaseFreight%age</th>-->
                           <th>WagePerDay</th>
                           <th>TripBhataPerDay</th>
                           <th>Bhata%ofBaseFreight</th>
                           <th>WagePerDay</th>
                           <th>TripBhataPerDay</th>
                           <th>Bhata%ofBaseFreight</th>
                          </tr>
                     </thead>
                     
                     <c:forEach items="${driverSalaryConfig}" var="dsc">
                         
                        <tr height="30">
                            <td height="30" > <input type="text" readonly id="locationName" name="locationName" style="width:200px;height:40px;" value='<c:out value="${dsc.companyName}" />' >
                                <input type="hidden" id="companyId" name="companyId" value='<c:out value="${dsc.companyId}" />' >
<!--                            </td>
                            <td height="30" > -->
                                <input type="hidden" maxlength="5" id="baseFreight" name="baseFreight" style="width:100px;height:40px;" value="0">
                            </td>

                            
                            <td height="30" > <input type="text" maxlength="5" id="driverWage" name="driverWage" style="width:100px;height:40px;" value="<c:out value="${dsc.driverWage}"/>" onKeyPress="return onKeyPressBlockCharacters(event);">
                            </td>
                            <td height="30" > <input type="text" maxlength="5" id="driverTBhata" name="driverTBhata" style="width:100px;height:40px;" value="<c:out value="${dsc.driverTBhata}"/>" onKeyPress="return onKeyPressBlockCharacters(event);">
                            </td>
                            <td  height="30"><input type="text" maxlength="5" id="driverBata" name="driverBata" onKeyPress="return onKeyPressBlockCharacters(event)" value="<c:out value="${dsc.driverBata}"/>" style="width:100px;height:40px;" ></td>
                            
                            <td height="30" > <input type="text" maxlength="5" id="cleanerWage" name="cleanerWage" style="width:100px;height:40px;" value="<c:out value="${dsc.cleanerWage}"/>" onKeyPress="return onKeyPressBlockCharacters(event);">
                            </td>
                            <td height="30" > <input type="text" maxlength="5" id="cleanerTBhata" name="cleanerTBhata" style="width:100px;height:40px;" value="<c:out value="${dsc.cleanerTBhata}"/>" onKeyPress="return onKeyPressBlockCharacters(event);">
                            </td>
                            <td  height="30"><input type="text" maxlength="5" id="cleanerBata" name="cleanerBata" onKeyPress="return onKeyPressBlockCharacters(event)" value="<c:out value="${dsc.cleanerBata}"/>" style="width:100px;height:40px;" ></td>
                        </tr>
                        </c:forEach >
                        <br><br>
                         <tr height="50"><td  height="30" align="center">
                                <input align="center" type="button" class="btn btn-info" value="Update"  onClick="submitPage();" id="buttonDesign"></td>
                        </tr>

                  

                    <script type="text/javascript">
                        function submitPage() {
                            
                            var driverWage = document.getElementsByName("driverWage");
                            var driverBata = document.getElementsByName("driverBata");
                            var cleanerWage = document.getElementsByName("cleanerWage");
                            var cleanerBata = document.getElementsByName("cleanerBata");
                            var driverTBhata = document.getElementsByName("driverTBhata");
                            var cleanerTBhata = document.getElementsByName("cleanerTBhata");
                            
                            for (var i = 0; i < driverWage.length; i++) {
                                if(driverWage[i].value == ''){
                                    driverWage[i].value = 0;
                                }
                                if(cleanerWage[i].value == ''){
                                    cleanerWage[i].value = 0;
                                }
                                if(cleanerBata[i].value == ''){
                                    cleanerBata[i].value = 0;
                                }
                                if(driverTBhata[i].value == ''){
                                    driverTBhata[i].value = 0;
                                }
                                if(cleanerTBhata[i].value == ''){
                                    cleanerTBhata[i].value = 0;
                                }
                            }
                            
//                           
                                document.driverSalaryConfig.action = '/throttle/saveDriverSalary.do';
                               
                                document.driverSalaryConfig.submit();
                            
                        }
                    </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

