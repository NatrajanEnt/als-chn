<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : gopi
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>PrintTripSheet</title>
    </head>
    <body>
        <form name="enter" method="post">
            <%
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String startDate = sdf.format(today);
            %>
            <div id="printContent">
                <table align="center" style="-webkit-print-color-adjust: exact; border-collapse: collapse;width:950px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                background:url(images/dict-logoWatermark.png);
                background-repeat:no-repeat;
                background-position:center;
                border:1px solid;
                /*border-color:#CD853F;width:800px;*/
                border:1px solid;
                /*opacity:0.8;*/
                font-weight:bold;">
                    <tr style="height: 50px;">
                        <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >

                            <table align="left" >
                                <tr>
                        <td colspan="2">
                            <font size="2">PAN NO-AACCB8054G </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">Service TaxRegnNo-AACCB8054GST001</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">CIN NO-U63040MH2006PTC159885</font><br>
                        </td>
                    </tr>
                                <tr style="height: 50px">
                                    <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                                    <td style="padding-left: 60px; padding-right:50px;">


                                    <font size="3"><center><b><u>International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</u></b></center></font><br>
                                    <center> <font size="2"> &nbsp;<u>Diesel memo</u></font> - <font color="green">ORIGINAL COPY </font></center>

                        </td>
                    </tr>
                </table>
                </td>
                </tr>
                <!--<tr>-->
                    <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px;height: 100px " align='center'>
                        <table width="100%" height="20%" align="center">
                            <tr>
                                <td ><font size="2">S.No.<c:out value="${voucherNo}"/></font><td/>
                            </tr>
                              <tr>
                                 <td><font size="2">BunkName:&nbsp;<c:out value="${bunkName}"/> </font></td>
                            </tr>
                            <tr>
                                <td ><font size="2">Lorry.No: <c:out value="${vehicleNo}"/></font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <td style="float: left;padding-left: 70px;" ><font size="2">Date : <c:out value="${expesneDate}"/></font></td>
                            </tr>
                            <tr>
                                <td ><font size="2">Driver's.Name:&nbsp;<c:out value="${driverName}"/></font></td>
                                <td style="float: left;padding-left: 70px;" ><font size="2">Transport :&nbsp;<c:out value="${transpoter}"/></font></td>
                            </tr>
                            <tr>
                                 <td width="100%" colspan="2"><font size="2">Route :&nbsp;<c:out value="${routeInfo}"/></font></td>
                            </tr>
                            <tr>
                                <td ><font size="2">GR.No:&nbsp;DICT/<c:out value="${grNo}"/></font></td>
                                <td style="float: left;padding-left: 70px;" ><font size="2"> GR Date.Of.Issue:&nbsp;<c:out value="${grDate}"/></font></td>
                            </tr>

                        </table>
                        <c:set var="totExpense" value="${dieselCost}"/>
                        <table align='center' border="1" width="100%">
                            <tr align='center' style="height:10px;">
                                <td style="width:2px;">S.No</td>
                                <td style="width:500px">Particulars</td>
                                <td style="width:100px">Quantity (Ltr.)</td>
                                <td style="width:100px">Amount</td>
                            </tr>
                            <tr>
                                <td align='center'>1</td>
                                <td >Diesel/Fuel Expenses

                                </td>
                                <td align='center'>
                                    <c:out value="${dieselUsed}"/>
                                </td>
                                <td align='center'>
                                    <c:out value="${dieselCost}"/>
                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td align='center'>Total Amount </td>
                                <td align='center'><c:out value="${dieselUsed}"/></td>
                                <td align='center'><c:out value="${dieselCost}"/></td>
                            </tr>

                        </table>
                        <table align="center">
                            <tr>
                                <td width="100%"><font size="2" colspan="2">Received With Thanks Rs<jsp:useBean id="spareTotalRoundOrg"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                    <% spareTotalRoundOrg.setRoundedValue(String.valueOf(pageContext.getAttribute("totExpense")));%>
                                    <% spareTotalRoundOrg.setNumberInWords(spareTotalRoundOrg.getRoundedValue());%>
                                    <b><jsp:getProperty name="spareTotalRoundOrg" property="numberInWords" />&nbsp;</b>
                                    </jsp:useBean></td>

                            </tr>
                            <tr>
                                <td  width="100%"colspan="2"><font size="2">from ICTRIPL against the expenses incurred as per above mentioned </td>

                            </tr>
                            <tr>
                                <td style="padding-top: 5px;" >Revenue Stamp</td>

                                <td  colspan="2" align="right"style="padding-top: 5px;"> For ICTRIPL <td>

                            </tr>


                            <tr>
                                <td style="padding-top: 10px;">Driver's Sign</td>
                                <td style="padding-right: 250px;padding-top: 10px;">Cashier</td>
                                <td style="padding-top: 10px;" align="left" >TT(Head)</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px;">Created By:&nbsp&nbsp<c:out value="${createdBy}"/></td>
                            </tr>

                        </table>
                </table>
                            <br>
                            <br>
                 <table align="center" style="-webkit-print-color-adjust: exact; border-collapse: collapse;width:950px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                background:url(images/dict-logoWatermark.png);
                background-repeat:no-repeat;
                background-position:center;
                border:1px solid;
                border:1px solid;
                font-weight:bold;">
                    <tr style="height: 50px;">
                        <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >

                            <table align="left" >
                                <tr>
                        <td colspan="2">
                            <font size="2">PAN NO-AACCB8054G </font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">Service TaxRegnNo-AACCB8054GST001</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size="2">CIN NO-U63040MH2006PTC159885</font><br>
                        </td>
                    </tr>
                                <tr style="height: 50px">
                                    <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                                    <td style="padding-left: 60px; padding-right:50px;">


                                    <font size="3"><center><b><u>International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</u></b></center></font><br>
                                    <center> <font size="2"> <u>Diesel memo</u></font> -<font color="red"> DUPLICATE COPY </font></center>

                        </td>
                    </tr>
                </table>
                </td>
                </tr>
                <!--<tr>-->
                    <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px;height: 100px " align='center'>
                        <table width="100%" height="20%" align="center">
                            <tr>
                                <td ><font size="2">S.No.<c:out value="${voucherNo}"/></font><td/>
                            </tr>
                              <tr>
                                 <td><font size="2">BunkName:&nbsp;<c:out value="${bunkName}"/> </font></td>
                            </tr>
                            <tr>
                                <td ><font size="2">Lorry.No: <c:out value="${vehicleNo}"/></font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <td style="float: left;padding-left: 70px;" ><font size="2">Date : <c:out value="${expesneDate}"/></font></td>
                            </tr>
                            <tr>
                                <td ><font size="2">Driver's.Name:&nbsp;<c:out value="${driverName}"/></font></td>
                                <td style="float: left;padding-left: 70px;" ><font size="2">Transport :&nbsp;<c:out value="${transpoter}"/></font></td>
                            </tr>
                            <tr>
                                 <td width="100%" colspan="2"><font size="2">Route :&nbsp;<c:out value="${routeInfo}"/></font></td>
                            </tr>
                            <tr>
                                <td ><font size="2">GR.No:&nbsp;DICT/<c:out value="${grNo}"/></font></td>
                                <td style="float: left;padding-left: 70px;" ><font size="2"> GR Date.Of.Issue:&nbsp;<c:out value="${grDate}"/></font></td>
                            </tr>

                        </table>
                        <c:set var="totExpense" value="${dieselCost}"/>
                        <table align='center' border="1" width="100%">
                            <tr align='center'>
                                <td style="width:2px">S.No</td>
                                <td style="width:500px">Particulars</td>
                                <td style="width:100px">Quantity (Ltr.)</td>
                                <td style="width:100px">Amount</td>
                            </tr>
                            <tr>
                                <td align='center'>1</td>
                                <td >Diesel/Fuel Expenses

                                </td>
                                <td align='center'>
                                    <c:out value="${dieselUsed}"/>
                                </td>
                                <td align='center'>
                                    <c:out value="${dieselCost}"/>
                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td align='center'>Total Amount </td>
                                <td align='center'><c:out value="${dieselUsed}"/></td>
                                <td align='center'><c:out value="${dieselCost}"/></td>
                            </tr>

                        </table>
                        <table align="center">
                            <tr>
                                <td width="100%"><font size="2" colspan="2">Received With Thanks Rs<jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                    <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totExpense")));%>
                                    <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                    <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp;</b>
                                    </jsp:useBean></td>

                            </tr>
                            <tr>
                                <td  width="100%"colspan="2"><font size="2">from ICTRIPL against the expenses incurred as per above mentioned </td>

                            </tr>
                            <tr>
                                <td style="padding-top: 5px;" >Revenue Stamp</td>

                                <td  colspan="2" align="right"style="padding-top: 5px;"> For ICTRIPL <td>

                            </tr>


                            <tr>
                                <td style="padding-top: 10px;">Driver's Sign</td>
                                <td style="padding-right: 250px;padding-top: 10px;">Cashier</td>
                                <td style="padding-top: 10px;" align="left" >TT(Head)</td>
                            </tr>
                            <tr>
                                <td style="padding-top: 10px;">Created By:&nbsp&nbsp<c:out value="${createdBy}"/></td>
                            </tr>

                        </table>
                </table>


            </div>
            <center><input type="button" class="button"  value="Print" onClick="print('printContent');" ></center>
            <script type="text/javascript">
                function print(val)
                {
                    var DocumentContainer = document.getElementById(val);
                    var WindowObject = window.open('', "TrackHistoryData",
                            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                    WindowObject.document.writeln(DocumentContainer.innerHTML);
                    WindowObject.document.close();
                    WindowObject.focus();
                    WindowObject.print();
                    WindowObject.close();
                }
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
