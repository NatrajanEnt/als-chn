package ets.domain.scheduler.web;

import ets.domain.trip.business.TallyTO;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

class sqlServiceConnection extends Thread {

    String invoiceFlag = null;
    String tripId = null;
    String tripIds = null;

    public sqlServiceConnection(String tripSheetIds)  throws ParseException, ClassNotFoundException, SQLException, org.json.simple.parser.ParseException {
        tripId = tripSheetIds;
    }

    public void run() {
        int updateStatus = 0;
        ArrayList  dktFlagList =new ArrayList();
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sqlQuery = "";
        String tripNo = "";
        PreparedStatement getDktIvoiceFlagStatus1 = null;
        try {

            System.out.println("=====STARTS=");
            
            String url = "jdbc:odbc:CA";
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            System.out.println("=====STARTSone=");
            conn = DriverManager.getConnection(url);

//           Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            System.out.println("=====STARTStwo="+conn);
//           conn = DriverManager.getConnection("jdbc:sqlserver://188.188.188.80:1433;TrsptFA", "Trt", "TrtFA@1234%");
            System.out.println("=====STARTSthree=");
            System.out.println("conn-------"+conn);
            System.out.println("tripId-------"+tripId);
            System.out.println("FA_Dkt_Flag-------");
            if (conn != null) {
                System.out.println("DKT Txn Database Successfully connected");
                
                sqlQuery = "Select Trljobid from [TrsptFA].[dbo].[Dkt] \n"
                        + " where  Trljobid="+tripId;

                TallyTO tally = new TallyTO();
                
                Statement stmts = conn.createStatement();
                rs = stmts.executeQuery(sqlQuery);
                while (rs.next()) {
                    tally = new TallyTO();
                    tally.setTripId(rs.getString("Trljobid"));
                    dktFlagList.add(tally);
                }
                System.out.println("getDktIvoiceFlagStatus----"+dktFlagList.size());
                System.out.println("DKT UPDATE = " + stmt);

            }

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("DKT====Sql Error==" + e.getMessage());
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(sqlServiceConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    Logger.getLogger(sqlServiceConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        if (dktFlagList.size() > 0) {
            Connection con = null;
// }
            String dbClassName = "com.mysql.jdbc.Driver";
            String dbUrl = "jdbc:mysql://localhost:3306/throttle_chakiat?useUnicode=yes&characterEncoding=UTF-8";
            String dbUserName = "root";
            String dbPassword = "etsadmin123$";
            int status = 0;

            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try {
                    TallyTO tally = null;
                    PreparedStatement updateMailDetails = null;
                    Iterator itr =dktFlagList.iterator();
                    while (itr.hasNext()) {
                     tally = new TallyTO();
                     tally = (TallyTO) itr.next();
                        String sql = "UPDATE ts_trip_vehicle SET \n"
                                + " FA_Dkt_Flag = 1"
                                + " WHERE  tripIds = ?";
                        updateMailDetails = con.prepareStatement(sql);
                        updateMailDetails.setString(1, tally.getTripId());
                        status = updateMailDetails.executeUpdate();
                        System.out.println("DKT Throttle Side = " + status);
                    }

                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("DKT====MySql Error==" + e.getMessage());
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (con != null) {
                    try {
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(sqlServiceConnection.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }
    }
}
