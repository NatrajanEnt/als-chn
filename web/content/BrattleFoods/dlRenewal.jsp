<%--
    Document   : standardChargesMaster
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script>

    var httpRequest;
    function checkDriverMapped(driverId) {
         $("#tripStatusShow").hide();
        if (driverId != '') {
            var driverIds = document.getElementsByName("driverIds");
            for(var i=0; i < driverIds.length; i++){
                if(driverIds[i].value == driverId){
                     $("#tripStatusShow").show();
                     $("#tripStatus").text('This Driver is already marked. please see in the below list.');
                     
                    $('#driverName').val('');
                    $('#driverId').val(0);
                    $('#driverName').focus();
                    $("#returnedDate").removeClass('datepicker');
                    $("#handoverDate").addClass('datepicker');                    
                    document.getElementById("mappingId").value ='';
                    document.getElementById("handoverDate").readOnly = false;
                    document.getElementById("returnedDate").readOnly = true;
                }
            }
            
        }
    }


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#driverName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getDriverName.do",
                    dataType: "json",
                    data: {
                        driverName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Driver Name");
                        $('#driverName').val('');
                        $('#driverId').val('');    
                        $('#driverName').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#driverName').val(value);
                $('#driverId').val(id);
                 
                 $("#returnedDate").removeClass('datepicker');
                $("#handoverDate").addClass('datepicker');
                document.getElementById("mappingId").value ='';
                checkDriverMapped(id);
                
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });




    //Update Vehicle Planning
    function setValues(mappingId, driverId, driverName, handoverDate, returnedDate, remarks, status,sno) {
        var count = parseInt(document.getElementById("count").value);
        //document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("mappingId").value = mappingId;
        document.getElementById("driverId").value = driverId;
        document.getElementById("driverName").value = driverName;
        document.getElementById("driverName").readOnly = true;
        document.getElementById("handoverDate").value = handoverDate;
        document.getElementById("handoverDate").readOnly = true;
        document.getElementById("returnedDate").value = '';
        document.getElementById("returnedDate").readOnly = false;
        $("#handoverDate").removeClass('datepicker');
        $("#returnedDate").addClass('datepicker');
        document.getElementById("remarks").value = remarks;
//        document.getElementById("status").value = status;
    }

    //savefunction
    function submitPage() {
        if (document.getElementById("driverName").value == '' || document.getElementById("driverId").value == '') {
            alert("Please Select Valid Driver Name");
            document.getElementById("driverName").focus();
         } else if (document.getElementById("handoverDate").value == '' ) {
            alert("Please Enter Start Date");
            document.getElementById("handoverDate").focus();
        } else if (document.getElementById("returnedDate").value == ''  && document.getElementById("mappingId").value != ''  ) {
            alert("Please Enter End Date");
            document.getElementById("returnedDate").focus();
        } else if (document.getElementById("remarks").value == '' ) {
            alert("Please enter Remarks");
            document.getElementById("remarks").focus();
        } else {
            $("#save").hide();
            document.vehicleDriverPlanning.action = '/throttle/saveDriverLicenseRenewal.do';
            document.vehicleDriverPlanning.submit();
        }
    }
    
    
    
</script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <h2><spring:message code="operations.label.DL Renewal"  text="DL Renewal"/></h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="operations.label.Drivers"  text="default text"/></a></li>
          <li class="active"><spring:message code="operations.label.DL Renewal"  text="DL Renewal"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
    <body onload="document.vehicleDriverPlanning.driverName.focus()">
        <form name="vehicleDriverPlanning"  method="post" >
           
            
            <%@ include file="/content/common/message.jsp" %>
            
            

           <table class="table table-info mb30 table-hover" >
                <input type="hidden" name="stChargeId" id="stChargeId" value=""  />
                <thead>
                <tr>
                    <th  colspan="4" ><spring:message code="operations.label.DL Renewal"  text="DL Renewal"/></th>
                </tr>
                </thead>
                <tr>
                    <td  colspan="4" align="center" id="tripStatusShow" style="display: none;"><label id="tripStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.Driver Name"  text="Driver Name"/></td>
                    <td ><input type="hidden" name="mappingId" id="mappingId" value="" class="textbox"  ><input type="hidden" name="driverId" id="driverId" class="textbox"  ><input type="text" name="driverName" id="driverName" style="width:260px;height:40px;"  class="form-control" ></td>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.handoverDate"  text="Handover Date"/></td>
                    <td ><input type="text"  autocomplete="off" value=""  name="handoverDate" id="handoverDate" style="width:260px;height:40px;"  class="form-control datepicker" ></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.returnedDate"  text="Returned Date"/></td>
                    <td ><input type="text" autocomplete="off"  value=""  name="returnedDate" readonly id="returnedDate" style="width:260px;height:40px;"  class="form-control datepicker" ></td>
                    
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.remarks"  text="Remarks"/></td>
                    <td ><textarea rows="3" cols="30" class="form-control" name="remarks" id="remarks"   style="width:142px"></textarea></td>
                    
                </tr>
                <tr>
                    <td  colspan="4" align="center"><input type="button" class="btn btn-success" value="<spring:message code="operations.label.SAVE"  text="default text"/>" id="save" onClick="submitPage();"   />
                    </td>
                </tr>
                
            </table>
            
            
           <table class="table table-info mb30 table-hover" id="table">
                <thead>
                    <tr height="30">
                        <th><spring:message code="operations.label.SNo"  text="default text"/></th>
                        <th><spring:message code="operations.label.Driver Name"  text="Driver Name"/> </th>
                        <th><spring:message code="operations.label.handoverDate"  text="Handover Date"/></th>
                        <th><spring:message code="operations.label.returnedDate"  text="Returned Date"/></th>
                        <th><spring:message code="operations.label.remarks"  text="Remarks"/></th>
                        <th><spring:message code="operations.label.Select"  text="default text"/></th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${driverLicenseRenewalList != null}">
                        <c:forEach items="${driverLicenseRenewalList}" var="license">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td   align="left"> <%= sno %> </td>
                                <td   align="left"><input type="hidden" name="driverIds" id="driverIds" value="<c:out value="${license.driverId}" />"/> <c:out value="${license.driverName}" /></td>
                                 <td   align="left"><c:out value="${license.handoverDate}" /></td>
                                 <td   align="left"><c:out value="${license.returnedDate}" /></td>
                                 <td   align="left"><c:out value="${license.remarks}" /></td>
                                <td > <input type="checkbox" id="edit<%=sno%>" onclick="setValues('<c:out value="${license.mappingId}" />', '<c:out value="${license.driverId}" />', '<c:out value="${license.driverName}" />', '<c:out value="${license.handoverDate}" />', '<c:out value="${license.returnedDate}" />', '<c:out value="${license.remarks}" />',  '<c:out value="${license.status}" />','<%=sno%>');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.label.EntriesPerPage"  text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="operations.label.of"  text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
