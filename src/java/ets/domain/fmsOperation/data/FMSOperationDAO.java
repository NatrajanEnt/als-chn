/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.fmsOperation.data;

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.programmingfree.excelexamples.calculateDistance;
import ets.domain.fmsOperation.business.FMSOperationTO;
import ets.domain.operation.business.OperationTO;
import ets.domain.util.FPLogUtils;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.List;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
//import com.Service;
//import com.ServiceSoap;
//import com.GetDriverAdvanceResponse.GetDriverAdvanceResult;

/**
 *
 * @author vijay
 */
public class FMSOperationDAO extends SqlMapClientDaoSupport {

    public FMSOperationDAO() {
    }
    private final int errorStatus = 4;
    private final static String CLASS = "FMSOperationDAO";

    public ArrayList getBodyBillList(int jobcardId) {
        Map map = new HashMap();
        ArrayList getJobCardList = new ArrayList();
        try {
            map.put("jobcardId", jobcardId);
            getJobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getBodyBillList", map);
            //////System.out.println("getBodyBillList size=" + getJobCardList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBodyBillList List", sqlException);
        }

        return getJobCardList;
    }

    public ArrayList getContractJobcards() {
        Map map = new HashMap();
        ArrayList jobcardList = new ArrayList();
        try {
            jobcardList = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getContractJobcards", map);

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return jobcardList;
    }

    public ArrayList getMfrList() {
        Map map = new HashMap();
        ArrayList MfrList = new ArrayList();
        try {

            MfrList = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.MfrList", map);

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "MfrList", sqlException);
        }
        return MfrList;

    }

    public ArrayList getServiceTypes() {
        Map map = new HashMap();
        ArrayList serviceTypes = new ArrayList();
        try {
            serviceTypes = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getServiceTypes", map);
            //////System.out.println("serviceTypes size=" + serviceTypes.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceTypes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceTypes List", sqlException);
        }

        return serviceTypes;
    }

    public ArrayList getServicePoints() {
        Map map = new HashMap();
        ArrayList serviceTypes = new ArrayList();
        try {
            serviceTypes = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getServicePoints", map);
            //////System.out.println("getServicePoints size=" + serviceTypes.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServicePoints Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServicePoints List", sqlException);
        }
        return serviceTypes;
    }

    public ArrayList getServiceVendors() {
        Map map = new HashMap();
        ArrayList serviceVendors = new ArrayList();
        try {
            serviceVendors = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getServiceVendors", map);
            //////System.out.println("serviceVendors size=" + serviceVendors.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serviceVendors Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "serviceVendors List", sqlException);
        }
        return serviceVendors;
    }

    public String getVehicleRegNo(String vehicleId) {
        Map map = new HashMap();
        map.put("vehicleId", vehicleId);
        String result = "";
        try {
            result = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.getVehicleRegNo", map);
            //////System.out.println("result=" + result);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNo", sqlException);
        }
        return result;
    }

    public ArrayList getVehicleRegNoForJobCard(String vehicleNo) {
        Map map = new HashMap();
        if (!vehicleNo.equals("")) {
            vehicleNo = vehicleNo + "%";
        }
        map.put("regNo", vehicleNo);

        ArrayList vehicleRegNoForJobCard = new ArrayList();
        System.out.println("map:" + map);
        try {
            vehicleRegNoForJobCard = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getVehicleRegNoForJobCard", map);
            System.out.println("size is=" + vehicleRegNoForJobCard.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNoForJobCard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNoForJobCard", sqlException);
        }

        return vehicleRegNoForJobCard;
    }

    public ArrayList trailerNos() {
        Map map = new HashMap();
        ArrayList trailerNos = new ArrayList();
        try {
            trailerNos = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.trailerNos", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("trailerNos Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "trailerNos", sqlException);
        }

        return trailerNos;
    }

    public int insertJobCardWorkOrder(FMSOperationTO operationTO) {
        Map map = new HashMap();
        int status = 0;

        map.put("serviceTypeId", operationTO.getServicetypeId());
        map.put("priority", operationTO.getPriority());
        map.put("fromOperationId", operationTO.getCompId());
        map.put("toServiceId", operationTO.getServiceLocationId());
        map.put("serviceLocation", operationTO.getServiceLocation());
        map.put("reqdate", operationTO.getReqDate());
        map.put("driverName", operationTO.getDriverName());
        map.put("dateOfIssue", operationTO.getDateOfIssue());
        map.put("km", operationTO.getKmReading());
        map.put("totalkm", operationTO.getTotalKm());
        map.put("hm", operationTO.getHourMeter());
        map.put("bayNo", operationTO.getBayNo());
        map.put("workOrderId", operationTO.getWorkOrderId());
        map.put("location", operationTO.getLocation());
        map.put("type", operationTO.getJobCardType());
        map.put("jobCardTypeNew", operationTO.getJobCardTypeNew());
        String sVendor = operationTO.getServiceVendor();
        if (sVendor.equals("")) {
            sVendor = "0";
        }
        map.put("serviceVendor", operationTO.getServiceVendor());

        map.put("userId", operationTO.getUserId());

        map.put("remark", operationTO.getRemarks());

        map.put("cust", operationTO.getCust());
        map.put("eCost", operationTO.getTotalAmount());

        int custId = 0;
        if (operationTO.getMfrName() == null || operationTO.getMfrName() == "") {
            map.put("mfrName", 0);
            map.put("modName", 0);
            map.put("usageName", 0);
            //map.put("regno",0);
        } else {
            map.put("mfrName", operationTO.getMfrName());
            map.put("modName", operationTO.getModelName());
            map.put("usageName", operationTO.getUsageName());

        }
        String temp = "";
        int jobcardId = 0;
        int vehicleId = 0;
        String temp1 = "";
        try {

            map.put("trailerId", operationTO.getTrailerId());
            map.put("jobCardFor", operationTO.getJobCardFor());
            map.put("regno", operationTO.getRegno());
            //////System.out.println("map = " + map);
            temp = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.getVehicleId", map);
            //////System.out.println("temp" + temp);
            if (temp != null) {
                vehicleId = Integer.parseInt(temp);
                //////System.out.println("vId" + vehicleId);
            }
            if (vehicleId != 0) {
                map.put("regno", operationTO.getRegno());
            }
            //////System.out.println("map = " + map);
            jobcardId = (Integer) getSqlMapClientTemplate().insert("fmsOperation.insertJobCardWork", map);
            //jobcardId = (Integer) getSqlMapClientTemplate().queryForObject("fmsOperation.lastinsertJobCardId", map);
            //////System.out.println("last job card Id" + jobcardId);

            if (vehicleId != 0) {
                map.put("vehicleId", vehicleId);
                // status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertKmHistory", map);
                //////System.out.println("map = " + map);
                temp1 = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.getCustId", map);
                if (temp1 != null) {

                    custId = Integer.parseInt(temp1);
                    //////System.out.println("cId" + custId);
                }
            } else {

                map.put("vehicleId", vehicleId);

            }

            map.put("custId", custId);
            map.put("jobcardId", jobcardId);
            //////System.out.println("jobcardId" + jobcardId);
            //////System.out.println("map = " + map);
            if (jobcardId != 0) {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertJobCardWorkOrder", map);
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertJobCardWorkOrder Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardWorkOrder", sqlException);
        }

        return jobcardId;
    }

    public int insertJobCardProblems(int secId, int probId, String symptoms, int Severity, int jobcardId, int temp, int oldJobcardId) {
        Map map = new HashMap();
        int status = 0;
        map.put("probId", probId);
        map.put("secId", secId);
        map.put("symptom", symptoms);
        map.put("severity", Severity);
        map.put("jobcardId", jobcardId);
        String IdentifiedBy = "Technician";
        if (temp == 0) {
            IdentifiedBy = "Customer";
        } else if (temp == 1) {
            IdentifiedBy = "oldComplaints";
        }
        map.put("IdentifiedBy", IdentifiedBy);
        try {
            if (oldJobcardId != 0) {
                map.put("oldJobcardId", oldJobcardId);
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateJobCardProblemstoNew", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertJobCardProblems", map);
            }
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardProblems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardProblems", sqlException);
        }

        return status;
    }

    public ArrayList getEmailDetails(String activitycode) {
        ArrayList EmailDetails = new ArrayList();
        Map map = new HashMap();
        try {
            map.put("actcode", activitycode);
            EmailDetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getEmailDetails", map);
            //////System.out.println("EmailDetails size=" + EmailDetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("EmailDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "EmailDetails List", sqlException);
        }

        return EmailDetails;
    }

    public ArrayList getJobCardDetails(int jobcardId) {
        Map map = new HashMap();
        map.put("jobcardId", jobcardId);
        ArrayList jobCardList = new ArrayList();
        try {
            jobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getJobCardDetails", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNoForJobCard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNoForJobCard", sqlException);
        }

        return jobCardList;
    }

    public String getJobcardEmail() {
        Map map = new HashMap();
        String jobCardEmailList = "";
        try {
            jobCardEmailList = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.getJobcardEmail", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobcardEmail Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobcardEmail", sqlException);
        }

        return jobCardEmailList;
    }

    public int insertSms(FMSOperationTO operationTO, int userId) {
        Map map = new HashMap();
        int index = 0;
        map.put("userId", userId);
        map.put("wayBillId", operationTO.getJobCardId());
        map.put("smsContent", operationTO.getSmsContent());
        map.put("smsTo", operationTO.getSmsTo());
        int insertSms = 0;
        try {
            insertSms = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertSms", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSms Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSms", sqlException);
        }

        return insertSms;
    }

    public int getTotalJobcards(String jcno, String regno, String status, int operId, String fromDate, String toDate, int compId, String custType) {
        Map map = new HashMap();
        int totalRecords = 0;

        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("rno", regno);
        map.put("compId", compId);
        map.put("status", status);
        map.put("operId", operId);
        map.put("jcno", jcno);

        //////System.out.println("regno in tot" + regno);
        //////System.out.println("jcno in tot" + jcno);
        //////System.out.println("fromDate in tot" + fromDate);
        //////System.out.println("compId in tot" + compId);
        //////System.out.println("status in tot" + status);
        //////System.out.println("operId in tot" + operId);
        try {
            if ("0".equals(custType)) {
                totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("fmsOperation.getTotalJobcards", map);
            } else if ("I".equals(custType)) {
                totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("fmsOperation.getTotalJobcardsInt", map);
            } else if ("E".equals(custType)) {
                totalRecords = (Integer) getSqlMapClientTemplate().queryForObject("fmsOperation.getTotalJobcardsExt", map);
            }

            System.out.println("getTotalJobcards " + totalRecords);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getTotalJobcards Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getTotalJobcards", sqlException);
        }

        return totalRecords;

    }

    public ArrayList searchJobCardStatus(String jcno, String regno, String status, int operId, String fromDate, String toDate, int compId, int startIndex, int endIndex, String custType) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("fromDate", fromDate);
        map.put("toDate", toDate);
        map.put("compId", compId);
        //        map.put("compId", 1023);
        map.put("regno", regno);
        map.put("jcno", jcno);
        map.put("operId", operId);
        map.put("status", status);
        map.put("sIndex", startIndex);
        map.put("eIndex", endIndex);
        map.put("custType", custType);
        if (custType == null) {
            custType = "0";
        }
        System.out.println("map Throttle here = " + map);
        try {
            if ("0".equals(custType)) {
                //                map.put("compId", 1022);
                //////System.out.println("map 0 = " + map);
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.searchJobCardStatus", map);
            } else if ("I".equals(custType)) {
                //                map.put("compId", 1022);
                //////System.out.println("map 1 = " + map);
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.searchJobCardStatusInt", map);
            } else if ("E".equals(custType)) {
                //                map.put("compId", 1022);
                //////System.out.println("map  2 = " + map);
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.searchJobCardStatusExt", map);
            }

            System.out.println("searchJobCardStatus size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("searchJobCardStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "searchJobCardStatus", sqlException);
        }

        return scheduleWODetails;
    }

    public int saveBodyWorksBill(String woId,
            String spareAmount, String vatAmount, String serviceAmount,
            String serviceTaxAmount, String remarks,
            String invoiceNo, String total, int userId, String invoiceDate) {
        Map map = new HashMap();
        int status = 0;

        map.put("woId", Integer.parseInt(woId));
        map.put("invoiceNo", invoiceNo);
        map.put("spareAmount", spareAmount);
        map.put("vatAmount", vatAmount);
        map.put("serviceAmount", serviceAmount);
        map.put("serviceTaxAmount", serviceTaxAmount);
        map.put("remarks", remarks);
        map.put("total", Float.parseFloat(total));
        map.put("userId", userId);
        map.put("invoiceDate", invoiceDate);

        try {

            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.saveBodyWorksBill", map);

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBodyWorksBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBodyWorksBill", sqlException);
        }

        return status;
    }

    public ArrayList getContractWos(int jobCardId) {
        Map map = new HashMap();
        ArrayList woList = new ArrayList();
        try {
            map.put("jobCardId", jobCardId);
            woList = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getContractwo", map);

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return woList;
    }

    public ArrayList getProblemDetails(int workOrderId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("workOrderId", workOrderId);
        if (jobcardId == 0) {
            String jocId = "";
            map.put("jobcardId", jocId);
        } else {

            map.put("jobcardId", jobcardId);
        }
        //////System.out.println("map = " + map);
        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getProblemDetails", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProblemDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList jobCardProblemDetails(int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();

        map.put("jobcardId", jobcardId);

        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.jobCardProblemDetails", map);
            //////System.out.println("jobCardProblemDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("jobCardProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardProblemDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList jobCardMrsDetails(int jobcardId) {
        Map map = new HashMap();
        ArrayList jobCardMrsDetails = new ArrayList();
        try {
            map.put("jobcardId", jobcardId);
            jobCardMrsDetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.jobCardMrsDetails", map);
            //////System.out.println("jobCardMrsDetails size=" + jobCardMrsDetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardMrsDetails List", sqlException);
        }
        return jobCardMrsDetails;
    }

    public ArrayList getBodyWorksList(int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduledServices = new ArrayList();

        try {

            map.put("jobcardId", jobcardId);
            scheduledServices = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getBodyWorksList", map);

            //////System.out.println("getBodyWorksList size=" + scheduledServices.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyWorksList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getBodyWorksList", sqlException);
        }

        return scheduledServices;
    }

    public ArrayList getVehicleDetails(int vehicleId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("vehicleId", vehicleId);
        map.put("jobcardId", jobcardId);
        //////System.out.println("vehicleid 1:" + vehicleId);
        try {
            if (vehicleId != 0) {
                //////System.out.println("am here 1");
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getVehicleDetails", map);
            } else {
                ////System.out.println("am here 2");
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.nonExistVehicleDetails", map);
            }
            System.out.println("getVehicleDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getScheduledServices(int vehicleId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduledServices = new ArrayList();

        try {

            map.put("vehicleId", vehicleId);
            map.put("jobcardId", jobcardId);

            scheduledServices = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getScheduledServices", map);

            //////System.out.println("scheduledServices size=" + scheduledServices.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getScheduledServices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getScheduledServices", sqlException);
        }

        return scheduledServices;
    }

    public String getcompDate(int jobcardId) {
        Map map = new HashMap();
        String status = "";

        ArrayList compDate = new ArrayList();
        map.put("jobcardId", jobcardId);
        FMSOperationTO operationTO = null;
        try {

            compDate = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getcompDate", map);
            Iterator itr = compDate.iterator();
            //////System.out.println("compDate:" + compDate.size());
            while (itr.hasNext()) {
                operationTO = (FMSOperationTO) itr.next();
                status = operationTO.getScheduledDate() + "~" + operationTO.getRemarks();
                //////System.out.println("status:" + status);
            }

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getcompDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getcompDate", sqlException);
        }

        return status;
    }

    public ArrayList getWorkOrderDetails(int workOrderId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("workOrderId", workOrderId);
        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getWorkOrderDetails", map);
            //////System.out.println("getWorkOrderDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("scheduleWODetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getWorkOrderDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getJobcardList(int vehicleId, int jobcardid) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("vehicleId", vehicleId);
        map.put("jobcardid", jobcardid);

        try {

            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getJobcardList", map);

            //////System.out.println("getJobcardList size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobcardList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJobcardList", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getProblemList(int vehicleId, int workOrderId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("vehicleId", vehicleId);
        map.put("workOrderId", workOrderId);
        System.out.println("getProblemList map=" + map);
        try {

            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getProblemList", map);

            //////System.out.println("getProblemList size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProblemList", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getServiceList(int vehicleId, int km, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        ArrayList scheduleAllWODetails = new ArrayList();
        ArrayList ServiceList = new ArrayList();
        FMSOperationTO operationTO = null;
        FMSOperationTO allTO = null;

        try {
            map.put("vehicleId", vehicleId);
            map.put("km", km);
            map.put("jobcardId", jobcardId);
            System.out.println(" map details....:" + map);
            if (vehicleId != 0) {
                scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getServiceList", map);

                System.out.println("scheduleWODetails" + scheduleWODetails.size());

                /*                scheduleAllWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getAllModelServiceList", map);
                    System.out.println("scheduleWODetails" + scheduleWODetails.size());
                    //////System.out.println("scheduleAllWODetails" + scheduleAllWODetails.size());
    
                    if (scheduleWODetails.size() != 0) {
                    Iterator ser = scheduleAllWODetails.iterator();
                    while (ser.hasNext()) {
                    operationTO = new FMSOperationTO();
                    operationTO = (FMSOperationTO) ser.next();
                    Iterator newSer = scheduleWODetails.iterator();
                    int count = 0;
                    while (newSer.hasNext()) {
                    allTO = new FMSOperationTO();
                    allTO = (FMSOperationTO) newSer.next();
                    System.out.println(operationTO.getServiceId() + ":" + allTO.getServiceId());
                    if (operationTO.getServiceId() == allTO.getServiceId()) {
                    count++;
                    //////System.out.println("configTO.getServiceId()" + operationTO.getServiceId());
                    //////System.out.println("newconfigTO.getServiceId()" + allTO.getServiceId());
                    //////System.out.println("count" + count);
                    }
                    }
                    //to add new service
                    if (count == 0) {
                    ServiceList.add(operationTO);
                    }
                    }
                    ServiceList.addAll(scheduleWODetails);
                    } else {
                    ServiceList.addAll(scheduleAllWODetails);
    
                    }
                    }
                     * */
            }
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getServiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getServiceList", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getNonGracePeriodServices(int vehicleId, int km) {
        Map map = new HashMap();
        ArrayList nonGracePeriodServices = new ArrayList();
        try {
            map.put("vehicleId", vehicleId);
            map.put("km", km);
            System.out.println("getNonGracePeriodServices: " + map);
            nonGracePeriodServices = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getNonGracePeriodServices", map);
            System.out.println("getNonGracePeriodServices size=" + nonGracePeriodServices.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getNonGracePeriodServices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getNonGracePeriodServices", sqlException);
        }
        return nonGracePeriodServices;
    }

    public ArrayList getExistingTechnicians(String jobCardId, String probId) {
        Map map = new HashMap();
        map.put("jobCardId", jobCardId);
        map.put("probId", probId);
        ArrayList existingTechnicians = new ArrayList();
        try {
            existingTechnicians = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getExistingTechnicians", map);
            System.out.println("existingTechnicians size=" + existingTechnicians.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("existingTechnicians Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "existingTechnicians List", sqlException);
        }

        return existingTechnicians;
    }

    public int saveJobCardTechnicians(String jobCardId, String probId,
            String technicianId, String estimatedHrs,
            String actualHrs, int userId) {
        Map map = new HashMap();
        int status = 0;

        map.put("jobCardId", jobCardId);
        map.put("probId", probId);
        map.put("technicianId", technicianId);
        map.put("estimatedHrs", estimatedHrs);
        map.put("actualHrs", actualHrs);
        map.put("userId", userId);
        try {

            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateJobCardTechnicians", map);
            if (status == 0) {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertJobCardTechnicians", map);
            }
            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateProblemStatus", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveBodyWorksBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveBodyWorksBill", sqlException);
        }

        return status;
    }

    public String getDueHmKm(FMSOperationTO operationTO) {
        Map map = new HashMap();
        String mess = "";
        try {
            map.put("vehicleId", operationTO.getVehicleId());
            map.put("serviceId", operationTO.getServiceId());
            map.put("km", operationTO.getKmReading());
            map.put("hm", operationTO.getHourMeter());
            mess = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.getbalHmKm", map);
            if (mess == null) {
                mess = "";
            }
            //////System.out.println("mess=" + mess);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getDueHmKm Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getDueHmKm", sqlException);
        }
        return mess;
    }

    public ArrayList getWoDetail(String woId) {
        Map map = new HashMap();
        ArrayList woDetail = new ArrayList();
        try {
            map.put("woId", woId);
            woDetail = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getContractorWODetails", map);

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBodyBillList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "vehicleKm ", sqlException);
        }
        return woDetail;
    }

    public int insertExternalTechnician(int jobcardId, int probId, String techName, String compName, String contactNo, String labourCharge) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobcardId", jobcardId);
        map.put("probId", probId);
        map.put("techName", techName);
        map.put("compName", compName);
        map.put("contactNo", contactNo);
        //////System.out.println("labourCharge" + labourCharge);

        float lc = Float.parseFloat(labourCharge);
        //////System.out.println("labourCharge" + lc);
        map.put("labourCharge", lc);

        try {
            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertExternalTechnician", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertExternalTechnician Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertExternalTechnician", sqlException);
        }

        return status;
    }

    public int updateCompletionDate(int jobcardId, String dateofIssue, String jremarks) {
        Map map = new HashMap();
        int status = 0;

        map.put("jobcardId", jobcardId);
        map.put("dateofIssue", dateofIssue);
        map.put("jremarks", jremarks);

        //////System.out.println("jremarks:" + jremarks);
        try {

            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateCompletionDate", map);

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateCompletionDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateCompletionDate", sqlException);
        }

        return status;
    }

    public int insertJobCardScheduleDetails(int jobcardId, int pId, String pcDate, int techId, String stat, String remarks, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("probId", pId);
        map.put("jobcardId", jobcardId);
        map.put("pcDate", pcDate);
        map.put("techId", techId);
        map.put("status", stat);
        map.put("remarks", remarks);
        map.put("userId", userId);

        try {

            String isExist = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.isThisProbExist", map);
            if (isExist == null) {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertJobCardScheduleDetails", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateJobCardScheduleDetails", map);

            }
            //            update JC TECH STATUS
            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateTechStatus", map);
            //update PCD in job card master 
            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateJobCardPCD", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardScheduleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardScheduleDetails", sqlException);
        }

        return status;
    }

    public int updateJobCardScheduleDetails(String pcDate, int jobcardId, int pId, int techId, String stat, String remarks, String cau, String rema) {
        Map map = new HashMap();
        int status = 0;
        map.put("probId", pId);
        map.put("jobcardId", jobcardId);
        map.put("techId", techId);
        map.put("status", stat);
        map.put("remarks", remarks);
        map.put("pcDate", pcDate);

        try {
            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.closeJobcardProblem", map);
            status = (Integer) getSqlMapClientTemplate().update("operation.updateTechStatus", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateJobCardScheduleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateJobCardScheduleDetails", sqlException);
        }

        return status;
    }

    public int insertJobCardServiceDetails(int jobcardId, int sId, String dateofIssue, int techId, String stat, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("serviceId", sId);
        map.put("jobcardId", jobcardId);
        map.put("pcDate", dateofIssue);
        map.put("techId", techId);
        map.put("status", stat);
        map.put("userId", userId);

        try {

            String isExist = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.isThisServiceExist", map);
            if (isExist == null) {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertJobCardServiceDetails", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateJobCardServiceDetails", map);

            }
            //update PCD in job card master 
            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateJobCardPCD", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardScheduleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardScheduleDetails", sqlException);
        }

        return status;
    }

    public int insertJobCardServices(int userId, int jobcardId, int service, int kiloMeter, int hM, int vehicleId) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobcardId", jobcardId);
        map.put("service", service);
        map.put("kiloMeter", kiloMeter);
        map.put("hM", hM);
        map.put("vehicleId", vehicleId);
        map.put("userId", userId);

        try {
            String isExist = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.isThisServiceClosed", map);
            if (isExist == null) {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertJobCardServices", map);
            } else {
                status = (Integer) getSqlMapClientTemplate().update("fmsOperation.updateClosedService", map);

            }

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardServices Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardServices", sqlException);
        }

        return status;

    }

    public int isThisProblemExist(int jobcardId, int probId) {
        Map map = new HashMap();
        String status = "";
        map.put("jobcardId", jobcardId);
        map.put("probId", probId);
        int temp = 0;
        try {

            status = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.isThisProblemExist", map);
            if (status != null) {
                temp = Integer.parseInt(status);
            }

        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("isThisProblemExist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "isThisProblemExist", sqlException);
        }

        return temp;

    }

    public ArrayList getVendorDetails() {
        Map map = new HashMap();
        ArrayList getVendorDetails = new ArrayList();
        try {
            getVendorDetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getVendorDetails", map);
            //////System.out.println("getVendorDetails size=" + getVendorDetails.size());
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVendorDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVendorDetails List", sqlException);
        }

        return getVendorDetails;
    }

    public ArrayList getTrailerDetails(int vehicleId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("trailerId", vehicleId);
        map.put("jobcardId", jobcardId);
        //////System.out.println("vehicleid 1:" + vehicleId);
        try {

            //////System.out.println("am here 1");
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getTrailerDetails", map);

            //////System.out.println("getVehicleDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getJcVehDetail(int jobcardId) {
        Map map = new HashMap();

        ArrayList list = new ArrayList();

        map.put("jcId", jobcardId);
        //////System.out.println("jcid=" + jobcardId);
        try {

            list = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.jcVehicleDetail", map);
            //////System.out.println("jcVehicleDetail" + list.size());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getcompDate Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getJcVehDetail", sqlException);
        }

        return list;
    }

    public ArrayList getWoProblemDetails(int workOrderId, int jobcardId) {
        Map map = new HashMap();
        ArrayList scheduleWODetails = new ArrayList();
        map.put("workOrderId", workOrderId);
        if (jobcardId == 0) {
            String jocId = "";
            map.put("jobcardId", jocId);
        } else {

            map.put("jobcardId", jobcardId);
        }
        //////System.out.println("workOrderId" + workOrderId);
        try {
            scheduleWODetails = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getWoProblemDetails", map);
            //////System.out.println("getWoProblemDetails size=" + scheduleWODetails.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getProblemDetails", sqlException);
        }

        return scheduleWODetails;
    }

    public ArrayList getServiceDetails(int jobcardId) {
        Map map = new HashMap();
        ArrayList Details = new ArrayList();

        map.put("jobCardId", jobcardId);
        //////System.out.println("jobcard id service" + jobcardId);
        try {
            Details = (ArrayList) getSqlMapClientTemplate().queryForList("fmsOperation.getServiceDetails", map);
            //////System.out.println("Details size=" + Details.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("jobCardProblemDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardProblemDetails", sqlException);
        }

        return Details;
    }

    public String getJobCardOwner(int jobCardId) {
        Map map = new HashMap();
        String jobCardOwner = "";

        try {
            //////System.out.println("jobCardId=" + jobCardId);
            map.put("jobCardId", jobCardId);
            jobCardOwner = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.jobCardOwner", map);

            //////System.out.println("jobCardOwner:" + jobCardOwner);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("jobCardOwner Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "jobCardOwner", sqlException);
        }

        return jobCardOwner;
    }

    public int insertServicedDetails(int userId, int jobcardId, int service, int kiloMeter, int hM, int vehicleId, String dateofIssue) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobcardId", jobcardId);
        map.put("service", service);
        map.put("kiloMeter", kiloMeter);
        map.put("hM", hM);
        map.put("vehicleId", vehicleId);
        map.put("userId", userId);
        map.put("servicedDate", dateofIssue);

        try {
            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.insertServicedDetails", map);
        } catch (Exception sqlException) {
            /*
                 * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertServicedDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertServicedDetails", sqlException);
        }

        return status;
    }

    public int changeJobCardStatus(FMSOperationTO operationTO) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobCardId", operationTO.getJobCardId());

        try {
            status = (Integer) getSqlMapClientTemplate().update("fmsOperation.changeJobCardStatus", map);
            int delStatus = (Integer) getSqlMapClientTemplate().update("fmsOperation.deleteJobCardActivity", map);
        } catch (Exception sqlException) {
            /*
                     * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("changeJobCardStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "changeJobCardStatus", sqlException);
        }

        return status;
    }

    public int getVehicleId(String regNo) {
        Map map = new HashMap();
        String status = "";
        map.put("regno", regNo);
        int vehicleId = 0;
        try {

            status = (String) getSqlMapClientTemplate().queryForObject("fmsOperation.getVehicleId", map);
            if (status != null) {
                vehicleId = Integer.parseInt(status);
            }

            //////System.out.println("vechileId BP" + status);
        } catch (Exception sqlException) {
            /*
	             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleId", sqlException);
        }

        return vehicleId;
    }

}
