<%-- 
    Document   : alterVehicleFinance
    Created on : Sep 20, 2012, 11:26:54 AM
    Author     : ASHOK
--%>


<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Finance Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>

        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }
            function validate(){
                var errMsg = "";
                if(document.VehicleFinance.regNo.value){
                    errMsg = errMsg+"Vehicle Registration No is not filled\n";
                }
                if(document.VehicleFinance.insuranceCompanyName.value){
                    errMsg = errMsg+"Insurance Company is not filled\n";
                }
                if(document.VehicleFinance.premiumNo.value){
                    errMsg = errMsg+"Premium No is not filled\n";
                }
                if(document.VehicleFinance.premiumPaidAmount.value){
                    errMsg = errMsg+"Premium paid Amount is not filled\n";
                }
                if(document.VehicleFinance.premiumPaidDate.value){
                    errMsg = errMsg+"Premium paid Date is not filled\n";
                }
                if(document.VehicleFinance.vehicleValue.value){
                    errMsg = errMsg+"Vehicle Value is not filled\n";
                }
                if(document.VehicleFinance.premiumExpiryDate.value){
                    errMsg = errMsg+"Premium expiry Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }
            /*var httpRequest;
            function getVehicleDetails(regNo)
            {

                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            var vehicleValues = detail.split("~");
                            document.VehicleInsurance.vehicleId.value = vehicleValues[0]
                            document.VehicleInsurance.chassisNo.value = vehicleValues[2]
                            document.VehicleInsurance.engineNo.value = vehicleValues[2]
                            document.VehicleInsurance.vehicleMake.value = vehicleValues[4]
                            document.VehicleInsurance.vehicleModel.value = vehicleValues[5]

                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }*/
        </script>

    </head>

    <body onLoad="">
        <form name="VehicleFinance" action="/throttle/updateVehicleFinance.do">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <h2 align="center">Vehicle Insurance Details</h2>
            <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="contenthead" colspan="4">Vehicle Details</td>
                </tr>
                <c:if test="${vehicleDetail != null}">
                    <c:forEach items="${vehicleDetail}" var="VDet" >
                        <tr>
                            <td class="texttitle1">Vehicle No</td>
                            <td class="text1"><input type="text" name="regNo" id="regNo" class="form-control" value='<c:out value="${VDet.regno}" />' ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${VDet.vehicleId}" />' /></td>
                            <td class="texttitle1">Make</td>
                            <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" value='<c:out value="${VDet.mfrName}" />'></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Model</td>
                            <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" value='<c:out value="${VDet.modelName}" />' ></td>
                            <td class="texttitle2">Usage</td>
                            <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control"  ></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Engine No</td>
                            <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" value='<c:out value="${VDet.engineNo}" />' ></td>
                            <td class="texttitle1">Chassis No</td>
                            <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" value='<c:out value="${VDet.chassisNo}" />' ></td>
                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td class="contenthead" colspan="4">Finance Details</td>
                </tr>
                <c:if test="${vehicleFinance != null}">
                    <c:forEach items="${vehicleFinance}" var="vFin" >
                        <tr>
                            <td class="texttitle1">Banker Name</td>
                            <td class="text1"><input type="text" name="bankerName" id="bankerName" class="form-control" value="<c:out value="${vFin.bankerName}" />" /><input type="hidden" name="amcId" id="amcId" value='<c:out value="${vFin.financeId}" />'></td>
                            <td class="texttitle1">Banker Address</td>
                            <td class="text1"><textarea name="bankerAddress" class="form-control" value="<c:out value="${vFin.bankerAddress}" />"></textarea></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Finance Amount</td>
                            <td class="text2"><input type="text" name="financeAmount" id="financeAmount" class="form-control" value="<c:out value="${vFin.financeAmount}" />" /></td>
                            <td class="text2">&nbsp;</td>
                            <td class="text2">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Rate of Interest</td>
                            <td class="text1"><input type="text" name="roi" id="roi" class="form-control" value="<c:out value="${vFin.roi}" />" /></td>
                            <td class="texttitle1">Interest Type</td>
                            <td class="text1">
                                <select name="interestType" class="form-control">
                                    <option value="Simple Interest">Simple Interest</option>
                                    <option value="Compound Interest">Compound Interest</option>
                                    <option value="Diminishing Balance Interest">Diminishing Balance Interest</option>
                                </select>
                                <script type="text/javascript">
                                    document.getElementById("interestType").value = '<c:out value="${vFin.interestType}"/>';
                                </script>
                            </td>
                        </tr>
                        <tr>
                            <td class="texttitle2">EMI Total Months</td>
                            <td class="text2"><input type="text" name="emiMonths" id="emiMonths" class="form-control" value="<c:out value="${vFin.emiMonths}" />" /></td>
                            <td class="texttitle2">EMI Amount</td>
                            <td class="text2"><input type="text" name="emiAmount" id="emiAmount" class="form-control" value="<c:out value="${vFin.emiAmount}" />" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">EMI Start Date</td>
                            <td class="text1"><input type="text" name="emiStartDate" id="emiStartDate" class="datepicker" value="<c:out value="${vFin.emiStartDate}" />" /></td>
                            <td class="texttitle1">EMI End Date</td>
                            <td class="text1"><input type="text" name="emiEndDate" id="emiEndDate" class="datepicker" value="<c:out value="${vFin.emiEndDate}" />" /></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">EMI Monthly Pay By Date</td>
                            <td class="text2">
                                <select name="emiPayDay" class="form-control">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                    <option value="10">11</option>
                                    <option value="12">12</option>
                                    <option value="13">13</option>
                                    <option value="14">14</option>
                                    <option value="15">15</option>
                                    <option value="16">16</option>
                                    <option value="17">17</option>
                                    <option value="18">18</option>
                                    <option value="19">19</option>
                                    <option value="20">20</option>
                                    <option value="21">21</option>
                                    <option value="22">22</option>
                                    <option value="23">23</option>
                                    <option value="24">24</option>
                                    <option value="25">25</option>
                                    <option value="26">26</option>
                                    <option value="27">27</option>
                                    <option value="28">28</option>
                                    <option value="29">29</option>
                                </select>
                                <script type="text/javascript">
                                    document.getElementById("emiPayDay").value = '<c:out value="${vFin.emiPayDay}"/>';
                                </script>
                            </td>
                            <td class="texttitle2">Pay Mode</td>
                            <td class="text2">
                                <select name="payMode" class="form-control">
                                    <option value="ECS">ECS</option>
                                    <option value="PDC">PDC</option>
                                    <option value="PDC">RTGS</option>
                                </select>
                                <script type="text/javascript">
                                    document.getElementById("payMode").value = '<c:out value="${vFin.payMode}"/>';
                                </script>
                            </td>
                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td align="center" class="texttitle1" colspan="4"></td>
                </tr>
            </table>
            <br>
            <center>                
                <br>
                <input type="submit" class="button" value=" Update " />
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>

