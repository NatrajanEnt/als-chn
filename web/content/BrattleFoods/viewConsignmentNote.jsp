<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount = '';
            var snumber = '';
            function addAllowanceRow()
            {
                if (snumber < 19) {
                    var tab = document.getElementById("expenseTBL");
                    var rowCount = tab.rows.length;

                    snumber = parseInt(rowCount) - 1;

                    var newrow = tab.insertRow(parseInt(rowCount) - 1);
                    newrow.height = "30px";
                    // var temp = sno1-1;
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> " + snumber + "</td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><input name='driName' type='text' class='textbox' id='driName' onkeyup='getDriverName(" + snumber + ")' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text1'><input name='cleanerName' type='text' class='textbox' id='cleanerName' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='date' type='text' id='date' class='datepicker' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    // rowCount++;

                    $(".datepicker").datepicker({
                        /*altField: "#alternate",
                         altFormat: "DD, d MM, yy"*/
                        changeMonth: true, changeYear: true
                    });
                }
                document.cNote.expenseId.value = snumber;
            }

            function delAllowanceRow() {
                try {
                    var table = document.getElementById("expenseTBL");
                    rowCount = table.rows.length - 1;
                    for (var i = 2; i < rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if (null != checkbox && true == checkbox.checked) {
                            if (rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                        }
                    }
                    document.cNote.expenseId.value = sno;
                } catch (e) {
                    alert(e);
                }
            }
            function parseDouble(value) {
                if (typeof value == "string") {
                    value = value.match(/^-?\d*/)[0];
                }
                return !isNaN(parseInt(value)) ? value * 1 : NaN;
            }
            function submitPage(obj) {
                if (obj.name == "search") {
                    var fromDate = document.cNote.fromDate.value;
                    var toDate = document.cNote.toDate.value;
                    //var regno=document.cNote.regno.value;
                    var driName = document.cNote.driName.value;
                    if (driName == "") {
                        alert("please enter the Driver Name");
                        document.cNote.driName.focus();
                    } else if (fromDate == "") {
                        alert("please enter the From Date");
                        document.cNote.fromDate.focus();
                    } else if (toDate == "") {
                        alert("please enter the To Date");
                        document.cNote.toDate.focus();
                    } else {
                        document.cNote.action = "/throttle/searchProDriverSettlement.do";
                        document.cNote.submit();
                    }
                }
                /*alert("hi main: "+obj.name);
                 if(obj.name=="save"){
                 alert("hi");
                 alert(document.cNote.expenseId.value);
                 document.cNote.buttonName.value = "save";
                 obj.name="none";
                 if(document.cNote.expenseId.value != ""){
                 document.cNote.action='/throttle/saveDriverExpenses.do';
                 document.cNote.submit();
                 }
                 }*/
                if (obj.name == "proceed") {
                    //alert("proceed");
                    document.cNote.buttonName.value = "proceed";
                    obj.name = "none";
                    document.cNote.action = '/throttle/saveDriverExpenses.do';
                    document.cNote.submit();
                }
            }
            function saveExp(obj) {
                document.cNote.buttonName.value = "save";
                obj.name = "none";
                if (document.cNote.expenseId.value != "") {
                    document.cNote.action = '/throttle/saveDriverExpenses.do';
                    document.cNote.submit();
                }
            }

            function getDriverName(sno) {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
    </head>

    <script type="text/javascript">
        var rowCount = 1;
        var sno = 0;
        var rowCount1 = 1;
        var sno1 = 0;
        var httpRequest;
        var httpReq;
        var styl = "";

        function addRow1() {
            if (parseInt(rowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            sno1++;
            var tab = document.getElementById("addTyres1");
            var newrow = tab.insertRow(rowCount1);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > " + sno1 + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productCodes' class='textbox' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productNames' class='textbox' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(3);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='packagesNos' id='packagesNos' class='textbox' value='' onkeyup='calcTotalPacks(this.value)'>";

            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='weights' id='weights' class='textbox' value='' onkeyup='calcTotalWeights(this.value)' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            rowCount1++;
        }


        function calculateTravelHours(sno) {
            var endDates = document.getElementById('endDates' + sno).value;
            var endTimeIds = document.getElementById('endTimeIds' + sno).value;
            var tempDate1 = endDates.split("-");
            var tempTime1 = endTimeIds.split(":");
            var stDates = document.getElementById('stDates' + sno).value;
            var stTimeIds = document.getElementById('stTimeIds' + sno).value;
            var tempDate2 = stDates.split("-");
            var tempTime2 = stTimeIds.split(":");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], tempTime1[0], tempTime1[1]);              // now
            var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
            document.getElementById('timeDifferences' + sno).value = hoursDifference;
        }

        function goToImportPage() {
            document.cNote.action = '/throttle/BrattleFoods/importCnoteDetails.jsp';
            document.cNote.submit();
        }
    </script>
    <script>
        //endingPointIds
        function resetAddRow(sno) {
            if (document.getElementById('endingPointIds' + sno).value == document.getElementById('destination').value) {
//           addRow 
                document.getElementById('addRowDetails').style.display = 'none';
            }
        }
    </script> 

    <script>
        $(document).ready(function() {
            $("#routeDetail").hide();
            $("#showRouteCourse").hide();
        });

        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#customerName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerName: request.term,
                            customerCode: document.getElementById('customerCode').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerName").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('~');
                    $itemrow.find('#customerId').val(tmp[0]);
                    $itemrow.find('#customerName').val(tmp[1]);
                    $itemrow.find('#customerCode').val(tmp[2]);
                    $('#customerAddress').text(tmp[3]);
                    $('#pincode').val(tmp[4]);
                    $('#customerMobileNo').val(tmp[6]);
                    $('#customerPhoneNo').val(tmp[5]);
                    $('#mailId').val(tmp[7]);
                    $('#billingTypeId').val(tmp[8]);
                    $('#billingTypeName').text(tmp[9]);
                    $('#billTypeName').text(tmp[9]);
                    $('#contractNo').text(tmp[10]);
                    $('#contractExpDate').text(tmp[11]);
                    $('#contractId').val(tmp[12]);
                    $('#ahref').attr('href', '/throttle/viewCustomerContract.do?custId=' + tmp[0]);
                    $("#contractDetails").show();
                    document.getElementById('customerCode').readOnly = true;
                    $('#origin').empty();
                    $('#origin').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    originList(tmp[12]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('~');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

            $('#customerCode').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerCode: request.term,
                            customerName: document.getElementById('customerName').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerCode").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#customerId').val(tmp[0]);
                    $itemrow.find('#customerName').val(tmp[1]);
                    $itemrow.find('#customerCode').val(tmp[2]);
                    $itemrow.find('#customerAddress').val(tmp[3]);
                    $itemrow.find('#customerMobileNo').val(tmp[4]);
                    $itemrow.find('#customerPhoneNo').val(tmp[5]);
                    $itemrow.find('#mailId').val(tmp[6]);
                    $itemrow.find('#billingTypeId').val(tmp[7]);
                    $('#billingTypeName').val(tmp[8]);
                    $('#billTypeName').val(tmp[8]);
                    $("#contractDetails").show();
                    document.getElementById('customerName').readOnly = true;
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[2] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

        function setOriginList(val) {
            $.ajax({
                url: '/throttle/getContractRouteList.do',
                data: {contractId: 0, billingTypeId: 0, customerTypeId: val},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#origin').append(
                                $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });

        }

        function originList(val) {
            $.ajax({
                url: '/throttle/getContractRouteList.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        var billingTypeId = $('#billingTypeId').val();
                        if (billingTypeId != 3) {
                            $('#origin').append(
                                    $('<option></option>').val(data.Id).html(data.Name)
                                    )
                        } else if (billingTypeId == 3) {
                            $('#origin').append(
                                    $('<option></option>').val(data.Id + "-" + data.Name).html(data.Name)
                                    )
                        }
                    });
                }
            });
            if (val != 3) {
                setDestination();
            }
        }


        var httpReq;
        var temp = "";
        function setDestination()
        {
            var billingTypeId = document.getElementById('billingTypeId').value;
            if (billingTypeId == 3) {
                var origin = $("#origin").val();
                var temp = origin.split("-");
                $("#startPointName").text(temp[1]);
                $("#pointId").val(temp[0]);
                $("#pointName").val(temp[1]);
                var url = "/throttle/getDestinationList.do?originId=" + temp[0] + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&contractId=" + document.getElementById('contractId').value;
            } else {
                var url = "/throttle/getDestinationList.do?originId=" + document.cNote.origin.value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&contractId=" + document.getElementById('contractId').value;
            }
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setDestinationOptions(temp, document.cNote.destination);
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }

        function setDestinationOptions(text, variab) {
            variab.options.length = 0;
            option0 = new Option("--select--", '0');
            variab.options[0] = option0;
            if (text != "") {
                var splt = text.split('~');
                var temp1;
                var id;
                var name;
                for (var i = 0; i < splt.length; i++) {
                    temp1 = splt[i].split('#');
//                    alert(temp1[0]);
//                    alert(temp1[1]);
                    if (document.getElementById('customerTypeId').value == 2) {
                        id = temp1[0];
                        name = temp1[1];
                        //setVehicleType();
                    } else if (document.getElementById('billingTypeId').value == 3 && document.getElementById('customerTypeId').value == 1) {
                        id = temp1[0] + "-" + temp1[1];
                        name = temp1[1];
                        //setVehicleType();
                    } else if (document.getElementById('billingTypeId').value != 3 && document.getElementById('customerTypeId').value == 1) {
                        id = temp1[0];
                        name = temp1[1];
                        //setVehicleType();
                    }
//                    alert(name);
//                    alert(id);
                    option1 = new Option(name, id)
                    variab.options[i + 1] = option1;
                }
            }
        }

        function setVehicleType() {
            //alert();
            //alert();
            var url = "/throttle/getContractVehicleTypeList.do?contractId=" + document.getElementById('contractId').value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&origin=" + document.getElementById('origin').value + "&destination=" + document.getElementById('destination').value;
            if (window.ActiveXObject) {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processVehicleTypeAjax();
            };
            httpReq.send(null);
        }

        function processVehicleTypeAjax() {
            if (httpReq.readyState == 4) {
                if (httpReq.status == 200) {
                    temp = httpReq.responseText.valueOf();
                    //alert(temp);
                    $('#vehTypeId').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    setVehicleTypeOptions(temp, document.cNote.vehTypeId);
                } else {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }

        function setVehicleTypeOptions(text, variab) {
            variab.options.length = 0;
            option0 = new Option("--select--", '0');
            variab.options[0] = option0;
            if (text != "") {
                var splt = text.split('~');
                var temp1;
                variab.options[0] = option0;
                for (var i = 0; i < splt.length; i++) {
                    temp1 = splt[i].split('#');
                    //alert(temp1[1]+"-"+temp1[0]);
                    option0 = new Option(temp1[1], temp1[0])
                    variab.options[i + 1] = option0;
                }
            }
        }


        function multiPickupShow() {
            var billingTypeId = document.getElementById('billingTypeId').value;
            if (document.getElementById('multiPickup').checked == true) {
                document.getElementById('multiPickupCharge').readOnly = false;
                document.getElementById('multiPickup').value = "Y";
                //alert(document.getElementById('multiPickup').value);
                if (billingTypeId == "3") {
                    $("#routeDetail").show();
                    $("#showRouteCourse").show();
                }
            } else if (document.getElementById('multiPickup').checked == false) {
                document.getElementById('multiPickupCharge').readOnly = true;
                document.getElementById('multiPickup').value = "N";
                //alert(document.getElementById('multiPickup').value);
                if (billingTypeId == "3") {
                    $("#routeDetail").hide();
                    $("#showRouteCourse").hide();
                }
            }
        }
        function multiDeliveryShow() {
            var billingTypeId = document.getElementById('billingTypeId').value;
            if (document.getElementById('multiDelivery').checked == true) {
                document.getElementById('multiDeliveryCharge').readOnly = false;
                document.getElementById('multiDelivery').value = "Y";
                if (billingTypeId == "3") {
                    $("#routeDetail").show();
                    $("#showRouteCourse").show();
                }
            } else if (document.getElementById('multiDelivery').checked == false) {
                document.getElementById('multiDeliveryCharge').readOnly = true;
                document.getElementById('multiDelivery').value = "N";
                if (billingTypeId == "3") {
                    $("#routeDetail").hide();
                    $("#showRouteCourse").hide();
                }
            }
        }

        function calcTotalPacks(val) {
            var totVal = 0;
            var packagesNos = document.getElementsByName('packagesNos');
            for (var i = 0; i < packagesNos.length; i++) {
                if (packagesNos[i].value != '') {
                    totVal += parseInt(packagesNos[i].value);
                }
            }
            $('#totalPackages').text(totVal);
            $('#totalPackage').val(totVal);
        }


        function calcTotalWeights(val) {
            var totVal = 0;
            var reeferRequired = document.getElementById('reeferRequired').value;
            var weights = document.getElementsByName('weights');
            var billingTypeId = document.getElementById('billingTypeId').value;
            var rateWithReefer = document.getElementById('rateWithReefer').value;
            var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
            for (var i = 0; i < weights.length; i++) {
                if (weights[i].value != '') {
                    totVal += parseInt(weights[i].value);
                }
            }
            $('#totalWeight').text(totVal);
            $('#totalWeightage').val(totVal);
            var freigthTotal = 0;
            if (billingTypeId == 2 && reeferRequired == 'Yes') {
                freigthTotal = parseInt(rateWithReefer) * parseInt(totVal);
                $('#freightAmount').text(freigthTotal)
                $('#totalCharges').val(freigthTotal)
            } else if (billingTypeId == 2 && reeferRequired == 'No') {
                freigthTotal = parseInt(rateWithoutReefer) * parseInt(totVal);
                $('#freightAmount').text(freigthTotal)
                $('#totalCharges').val(freigthTotal)
            }
        }


        function submitPage(value) {
            document.cNote.action = "/throttle/saveConsignmentNote.do";
            document.cNote.submit();
        }
    </script>

    <body onload="enableContract(1);
            addRow1();
            multiPickupShow();
            multiDeliveryShow()">
        <% String menuPath = "Operations >>  Create New Consignment Note";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="cNote" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <table width="100%">
                <tr></tr>
            </table>
            <br>
            <br>
            <br>
            <table cellpadding="0" cellspacing="0" width="800" border="0px" class="border" align="center">
                <tr>
                    <td class="contenthead" colspan="4">Consignment Note </td>
                </tr>
                <tr>
                    <td class="text2">Entry Option</td>
                    <td class="text2"><input type="radio" name="entryType" value="1" checked="">Manual</td>
                    <td class="text2"><input type="radio" name="entryType" value="2" onclick="goToImportPage()">Import</td>
                    <td class="text2" >&nbsp;</td>
                </tr>
                <tr>
                    <td class="text1">Consignment Note </td>
                    <td class="text1"><input type="text" name="consignmentNoteNo" id="consignmentNoteNo" value="<c:out value="${consignmentOrderNo}"/>" class="textbox"readonly style="width: 120px"/></td>
                    <td class="text1">Consignment Date </td>
                    <td class="text1"><input type="text" name="consignmentDate" id="consignmentDate" value="<c:out value="${curDate}"/>" class="textbox"readonly style="width: 120px"/></td>
                <tr>
                <tr>
                    <td class="text2">Order Reference No</td>
                    <td class="text2"><input type="text" name="orderReferenceNo" id="orderReferenceNo" value="" class="textbox" style="width: 120px"/></td>
                    <td class="text2">Order Reference  Remarks</td>
                    <td class="text2"><textarea name="orderReferenceRemarks" id="orderReferenceRemarks"></textarea></td>
                <tr>
                    <td class="text1">Customer Type</td>
                    <td class="text1">
                        <c:if test="${customerTypeList != null}">
                            <select name="customerTypeId" id="customerTypeId" onchange="enableContract(this.value);" class="textbox" style="width:120px;" >
                                <c:forEach items="${customerTypeList}" var="cusType">
                                    <option value="<c:out value="${cusType.customerTypeId}"/>"><c:out value="${cusType.customerTypeName}"/></option>
                                </c:forEach>
                            </select>
                        </c:if>
                    </td>
                    <td class="text1">Product Category</td>
                    <td class="text1">
                        <c:if test="${productCategoryList != null}">
                            <select name="productCategoryId" id="productCategoryId"  class="textbox" style="width:120px;">
                                <option value="0">--Select--</option>
                                <c:forEach items="${productCategoryList}" var="proList">
                                    <option value="<c:out value="${proList.productCategoryId}"/>"><c:out value="${proList.customerTypeName}"/></option>
                                </c:forEach>
                            </select>
                        </c:if>
                    </td>
            </table>
            <br>
            <br>
            <script type="text/javascript">

                function enableContract(val) {
                    if (val == 1) {
                        $("#contractCustomerTable").show();
                        $("#walkinCustomerTable").hide();
                        $("#contractFreightTable").show();
                        $("#walkinFreightTable").hide();
                        document.getElementById('customerAddress').readOnly = true;
                        document.getElementById('pincode').readOnly = true;
                        document.getElementById('customerMobileNo').readOnly = true;
                        document.getElementById('mailId').readOnly = true;
                        document.getElementById('customerPhoneNo').readOnly = true;
                        document.getElementById('walkinCustomerName').value = "";
                        document.getElementById('walkinCustomerCode').value = "";
                        document.getElementById('walkinCustomerAddress').value = "";
                        document.getElementById('walkinPincode').value = "";
                        document.getElementById('walkinCustomerMobileNo').value = "";
                        document.getElementById('walkinMailId').value = "";
                        document.getElementById('walkinCustomerPhoneNo').value = "";
                    } else {
                        $("#contractCustomerTable").hide();
                        $("#walkinCustomerTable").show();
                        $("#contractFreightTable").hide();
                        $("#walkinFreightTable").show();
                        document.getElementById('customerId').value = "";
                        document.getElementById('customerName').value = "";
                        document.getElementById('customerCode').value = "";
                        document.getElementById('customerAddress').value = "";
                        document.getElementById('pincode').value = "";
                        document.getElementById('customerMobileNo').value = "";
                        document.getElementById('mailId').value = "";
                        document.getElementById('customerPhoneNo').value = "";
                        setOriginList();
                    }
                }
            </script>
            <div id="tabs">
                <ul>
                    <li><a href="#customerDetail"><span>Basic Details</span></a></li>
                    <li><a href="#routeDetail" id="showRouteCourse"><span>Route Course</span></a></li>
                    <li><a href="#paymentDetails"><span>Invoice Info</span></a></li>

                </ul>

                <div id="customerDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable">
                        <tr>
                            <td class="contenthead" colspan="4" >Contract Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Name</td>
                            <td class="text1"><input type="hidden" name="customerId" id="customerId" class="textbox" /><input type="text" name="customerName" id="customerName" class="textbox"  /></td>
                            <td class="text1">Code</td>
                            <td class="text1"><input type="text" name="customerCode" id="customerCode" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="1" cols="16" id="customerAddress" name="customerAddress"></textarea></td>
                            <td class="text2">Pincode</td>
                            <td class="text2"><input type="text"  name="pincode" id="pincode" class="textbox" /></td>
                        </tr>
                        <tr>
                            <td class="text1">Mobile No</td>
                            <td class="text1"><input type="text"  name="customerMobileNo" id="customerMobileNo" class="textbox"  /></td>
                            <td class="text1">E-Mail ID</td>
                            <td class="text1"><input type="text"  name="mailId" id="mailId" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="text" name="customerPhoneNo" id="customerPhoneNo"  class="textbox" maxlength="10"   /></td>

                            <td class="text2">Billing Type</td>
                            <td class="text2" id="billingType"><input type="hidden" name="billingTypeId" id="billingTypeId" class="textbox" /><label id="billingTypeName"></label></td>

                        </tr>
                    </table>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinCustomerTable">
                        <tr>
                            <td class="contenthead" colspan="4" >Wlakin Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Name</td>
                            <td class="text1"><input type="text" name="walkinCustomerName" id="walkinCustomerName" class="textbox"   /></td>
                            <td class="text1">Code</td>
                            <td class="text1"><input type="text" name="walkinCustomerCode" id="walkinCustomerCode" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="1" cols="16" id="walkinCustomerAddress" name="walkinCustomerAddress"></textarea></td>
                            <td class="text2">Pincode</td>
                            <td class="text2"><input type="text"  name="walkinPincode" id="walkinPincode" class="textbox" /></td>
                        </tr>
                        <tr>
                            <td class="text1">Mobile No</td>
                            <td class="text1"><input type="text"  name="walkinCustomerMobileNo" id="walkinCustomerMobileNo" class="textbox"  /></td>
                            <td class="text1">E-Mail ID</td>
                            <td class="text1"><input type="text"  name="walkinMailId" id="walkinMailId" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="text" name="walkinCustomerPhoneNo" id="walkinCustomerPhoneNo"  class="textbox" maxlength="10"   /></td>

                            <td class="text2" colspan="2">&nbsp</td>

                        </tr>
                    </table>
                    <div id="contractDetails" style="display: none">
                        <table>
                            <tr>
                                <td class="text1">Contract No :</td>
                                <td class="text1"><b><input type="hidden" name="contractId" id="contractId" class="textbox" /><label id="contractNo"></label></b></td>
                                <td class="text1">Contract Expiry Date :</td>
                                <td class="text1"><font color="green"><b><label id="contractExpDate"></label></b></font></td>
                                <td class="text1">&nbsp;&nbsp;&nbsp;</td>
                                <td class="text1"><a id="ahref" href="/throttle/BrattleFoods/customerContractMaster.jsp"><b>View Contract</b></a></td>
                            </tr>
                        </table>
                    </div>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Consignment Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Origin</td>
                            <td class="text1">
                                <select id="origin" name="origin" onchange='setDestination()' class="textbox" style="width:150px"> 
                                    <option value="0">-Select-</option>
                                </select>
                            </td>
                            <td class="text1">Destination</td>
                            <td class="text1"><select id="destination" name="destination" class="textbox" onchange="setVehicleType();
                                    setRouteDetails()" style="width:150px">
                                    <option value="0">-Select-</option>
                                </select></td>
                            <td class="text1">Business Type</td>
                            <td class="text1">
                                <select name="businessType" class="textbox"  id="businessType" style="width:120px;">
                                    <option value="1" selected> Primary  </option>
                                    <option value="2"> Secondary  </option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2">Multi Pickup</td>
                            <td class="text2"><input type="checkbox" class="textbox" name="multiPickup" id="multiPickup" onclick="multiPickupShow()" value="N"></td>
                            <td class="text2">Multi Delivery</td>
                            <td class="text2"><input type="checkbox" class="textbox" name="multiDelivery" id="multiDelivery" onclick="multiDeliveryShow()" value="N"></td>
                            <td class="text2">Special Instruction</td>
                            <td class="text2"><textarea rows="1" cols="16" name="consignmentOrderInstruction" id="consignmentOrderInstruction"></textarea></td>
                        </tr>

                        <tr>
                            <td colspan="4" >

                                <table border="0" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="addTyres1">
                                    <tr>
                                        <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                        <td class="contenthead" height="30" >Product/Article Code</td>
                                        <td class="contenthead" height="30" >Product/Article Name </td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>No of Packages</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                    </tr>
                                    <br>

                                    <tr>
                                        <td colspan="5" align="center">
                                            &nbsp;&nbsp;&nbsp;<input type="reset" class="button" value="Clear">
                                            &emsp;<input type="button" class="button" value="Add Row" name="save" onClick="addRow1()">

                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <br>
                            </td>
                            <td colspan="2">
                                <table border="0"  align="right" width="300" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td colspan="2" height="100">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label class="contentsub">Total No Packages</label>
                                            <label id="totalPackages">0</label>
                                            <input type="hidden" id="totalPackage" name="totalPackage" />
                                        </td>
                                        <td>
                                            <label class="contentsub">Total Weight (Kg)</label>
                                            <label id="totalWeight">0</label>
                                            <input type="hidden" id="totalWeightage" name="totalWeightage" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Vehicle (Required) Details</td>
                        </tr>

                        <tr>
                            <td class="text1">Service Type</td>
                            <td class="text1">
                                <select name="serviceType" class="textbox"  id="paymentType" style="width:120px;">
                                    <option value="0" selected>--Select--</option>
                                    <option value="1"> FTL </option>
                                    <option value="2"> LTL </option>
                                </select>
                            </td>
                            <td class="text1">Vehicle Type</td>
                            <td class="text1"> <select name="vehTypeId" id="vehTypeId" class="textbox"  style="width:120px;" onchange="setFreightRate()">
                                    <option value="0">--Select--</option>
                                </select></td>
                            <td class="text1">Reefer Required</td>
                            <td class="text1">
                                <select name="reeferRequired" id="reeferRequired"  class="textbox"  style="width:120px;" onchange="calculateFreightTotal()">
                                    <option value="0" selected>--Select--</option>
                                    <option value="Yes" > Yes </option>
                                    <option value="No"> No </option>
                                </select>
                            </td>
                        </tr>
                        <script>

                            function setRouteDetails() {
                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                if (billingTypeId == 3 || customerTypeId == 2) {
                                    var destination = document.getElementById('destination').value;
                                    if (destination != 0) {
                                        var temp = destination.split("-");
                                        document.getElementById("routeId").value = temp[0];
                                        document.getElementById('totalKm').value = temp[1];
                                        document.getElementById('totalHours').value = temp[2];
                                        document.getElementById('totalMinutes').value = temp[3];
                                        document.getElementById('totalPoints').value = 2;
                                        document.getElementById('destinationId').value = temp[4];
                                        $("#endPointId").val(temp[4]);
                                        $("#endPointName").val(temp[5]);
                                        $("#endPointName1").text(temp[5]);
                                    } else {
                                        document.getElementById("routeId").value = 0;
                                        document.getElementById('totalKm').value = 0;
                                        document.getElementById('totalHours').value = 0;
                                        document.getElementById('totalMinutes').value = 0;
                                        document.getElementById('totalPoints').value = 0;
                                        document.getElementById('destinationId').value = 0;
                                    }

                                }
                            }

                            function setFreightRate() {
                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;
                                var vehTypeId = document.getElementById("vehTypeId").value;
                                if (customerTypeId == 1) {
                                    if (billingTypeId != 3) {
                                        if (vehTypeId != 0) {
                                            temp = vehTypeId.split("-");
                                            document.getElementById("routeContractId").value = temp[0];
                                            document.getElementById("routeId").value = 0;
                                            document.getElementById('contractRateId').value = temp[1];
                                            document.getElementById('totalKm').value = temp[2];
                                            document.getElementById('totalHours').value = temp[3];
                                            document.getElementById('totalMinutes').value = temp[4];
                                            document.getElementById('totalPoints').value = temp[5];
                                            document.getElementById('vehicleTypeId').value = temp[6];
                                            document.getElementById('rateWithReefer').value = temp[7];
                                            document.getElementById('rateWithoutReefer').value = temp[8];
                                        } else {
                                            document.getElementById("routeContractId").value = 0;
                                            document.getElementById("routeId").value = 0;
                                            document.getElementById('contractRateId').value = 0;
                                            document.getElementById('totalKm').value = 0;
                                            document.getElementById('totalHours').value = 0;
                                            document.getElementById('totalMinutes').value = 0;
                                            document.getElementById('totalPoints').value = 0;
                                            document.getElementById('vehicleTypeId').value = 0;
                                            document.getElementById('rateWithReefer').value = 0;
                                            document.getElementById('rateWithoutReefer').value = 0;
                                        }
                                    } else if (billingTypeId == 3) {
                                        if (vehTypeId != 0) {
                                            temp = vehTypeId.split("-");
                                            document.getElementById("routeContractId").value = 0;
                                            document.getElementById('contractRateId').value = temp[0];
                                            document.getElementById('vehicleTypeId').value = temp[1];
                                            document.getElementById('rateWithReefer').value = temp[2];
                                            document.getElementById('rateWithoutReefer').value = temp[3];
                                        } else {
                                            document.getElementById("routeContractId").value = 0;
                                            document.getElementById('contractRateId').value = 0;
                                            document.getElementById('vehicleTypeId').value = 0;
                                            document.getElementById('rateWithReefer').value = 0;
                                            document.getElementById('rateWithoutReefer').value = 0;
                                        }
                                    }
                                } else if (customerTypeId == 2) {
                                    document.getElementById("routeContractId").value = 0;
                                    document.getElementById('contractRateId').value = 0;
                                    document.getElementById('vehicleTypeId').value = 0;
                                    document.getElementById('rateWithReefer').value = 0;
                                    document.getElementById('rateWithoutReefer').value = 0;
                                }

                            }

                            function calculateFreightTotal() {
                                var reeferRequired = document.getElementById('reeferRequired').value;
                                var billingTypeId = document.getElementById('billingTypeId').value;
                                var rateWithReefer = document.getElementById('rateWithReefer').value;
                                var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
                                var totalKm = document.getElementById('totalKm').value;
                                var totalWeight = $("#totalWeight").text();
                                var totalAmount = 0;
                                if (reeferRequired == 'Yes' && billingTypeId == 1) {
                                    $('#freightAmount').text(rateWithReefer)
                                    $('#totFreightAmount').val(rateWithReefer)
                                    $('#totalCharges').val(rateWithReefer)
                                } else if (reeferRequired == 'No' && billingTypeId == 1) {
                                    $('#freightAmount').text(rateWithoutReefer)
                                    $('#totFreightAmount').val(rateWithoutReefer)
                                    $('#totalCharges').val(rateWithoutReefer)
                                } else if (reeferRequired == 'Yes' && billingTypeId == 2) {
                                    totalAmount = totalWeight * rateWithReefer;
                                    alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'No' && billingTypeId == 2) {
                                    totalAmount = totalWeight * rateWithReefer;
                                    alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'Yes' && billingTypeId == 3) {
                                    totalAmount = totalKm * rateWithReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'No' && billingTypeId == 3) {
                                    totalAmount = totalKm * rateWithoutReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                }
                            }
                        </script>
                        <tr>
                            <td class="text2">Vehicle Required Date</td>
                            <td class="text2">
                                <input type="hidden" class="textbox" name="destinationId" id="destinationId" >
                                <input type="hidden" class="textbox" name="routeContractId" id="routeContractId" >
                                <input type="hidden" class="textbox" name="routeId" id="routeId" >
                                <input type="hidden" class="textbox" name="contractRateId" id="contractRateId" >
                                <input type="hidden" class="textbox" name="totalKm" id="totalKm" >
                                <input type="hidden" class="textbox" name="totalHours" id="totalHours" >
                                <input type="hidden" class="textbox" name="totalMinutes" id="totalMinutes" >
                                <input type="hidden" class="textbox" name="totalPoints" id="totalPoints" >
                                <input type="hidden" class="textbox" name="vehicleTypeId" id="vehicleTypeId" >
                                <input type="hidden" class="textbox" name="rateWithReefer" id="rateWithReefer" >
                                <input type="hidden" class="textbox" name="rateWithoutReefer" id="rateWithoutReefer" >
                                <input type="text" class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" ></td>
                            <td class="text2">Vehicle Required</td>
                            <td class="text2">
                                Hours&nbsp;:&nbsp;&nbsp;<input type="text" name="vehicleRequiredHour" id="vehicleRequiredHour" class="textbox"  style="width: 60px" value=""/>
                                &nbsp;Minute&nbsp;:&nbsp;&nbsp;<input type="text" name="vehicleRequiredMinute" id="vehicleRequiredMinute" class="textbox" style="width: 60px" value="" /></td>
                            <td class="text2">Special Instruction</td>
                            <td class="text2"><textarea rows="1" cols="16" name="vehicleInstruction" id="vehicleInstruction"></textarea></td>
                        </tr>
                        <script>
                            function calculateTime() {
                                var date1 = new Date();
                                var tempDate = $("#vehicleRequiredDate").val();
                                var temp = tempDate.split("-");
                                var requiredDay = temp[0];
                                var requiredMonth = temp[1];
                                var requiredYear = temp[2];
                                var requiredHour = $("#vehicleRequiredHour").val();
                                if (requiredHour == "") {
                                    requiredHour = "00";
                                }
                                var requiredMinute = $("#vehicleRequiredMinute").val();
                                if (requiredMinute == "") {
                                    requiredMinute = "00";
                                }
                                var requiredSeconds = "00";
                                var date2 = new Date(requiredYear, requiredMonth, requiredDay, requiredHour, requiredMinute, requiredSeconds)
                                var diff = Math.abs(date1.getTime() - date2.getTime()) / 1000 / 60 / 60;
                                alert(diff);
                                var diffDays = diff.toFixed(2);
                                alert(diffDays);
                                if (diffDays < 10) {
                                    alert("No")
                                } else {
                                    alert("Yes");
                                }
                            }
                        </script>
                    </table>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Consignor Details</td>
                        </tr>
                        <tr>
                            <td class="text2">Consignor Name</td>
                            <td class="text2"><input type="text" class="textbox" id="consignorName" name="consignorName" ></td>
                            <td class="text2">Mobile No</td>
                            <td class="text2"><input type="text" class="textbox" id="consignorPhoneNo" name="consignorPhoneNo" ></td>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="1" cols="16" name="consignorAddress" id="consignorAddress"></textarea> </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Consignee Details</td>
                        </tr>
                        <tr>
                            <td class="text2">Consignee Name</td>
                            <td class="text2"><input type="text" class="textbox" id="consigneeName" name="consigneeName" ></td>
                            <td class="text2">Mobile No</td>
                            <td class="text2"><input type="text" class="textbox" id="consigneePhoneNo" name="consigneePhoneNo" ></td>
                            <td class="text2">Address</td>
                            <td class="text2"><textarea rows="1" cols="16" name="consigneeAddress" id="consigneeAddress"></textarea> </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" ></a>
                    </center>
                </div>



                <div id="routeDetail">
                    <script>
                        $(document).ready(function() {
                            // Get the table object to use for adding a row at the end of the table
                            var $itemsTable = $('#itemsTable1');

                            // Create an Array to for the table row. ** Just to make things a bit easier to read.
                            var count = 1;
                            var rowTemp = [
                                '<tr class="item-row">',
                                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                                '<td class="text1" height="30" ><input type="hidden" name="pointId" id="pointId" value=""/><input type="hidden" name="multiplePointRouteId" id="multiplePointRouteId" value=""/><input type="text" name="pointName" id="pointName" value=""/></td>',
                                '<td class="text1" height="30" ><select name="pointType" id="pointType"><option value="0" selected>--Select--</option><option value="Pick Up" >Pick Up</option><option value="Drop">Drop</option></select></td>',
                                '<td class="text1" height="30" ><input type="text" name="order" id="order" value="" style="width: 30px"/></td>',
                                '<td class="text1" height="30" ><textarea name="pointAddresss" id="pointAddresss" rows="2" cols="15"></textarea></td>',
                                '<td class="text1" height="30" ><input type="text" class="datepicker" name="pointPlanDate"  id="pointPlanDate"  value="" /></td>',
                                '<td class="text1" height="30" >Hr:<input type="text" name="pointPlanHour" id="pointPlanHour" value="" style="width: 30px"/>Min:<input type="text" name="pointPlanMinute" id="pointPlanMinute" value="" style="width: 30px"/></td>',
                                '</tr>'
                            ].join('');

                            // Add row to list and allow user to use autocomplete to find items.
                            $("#addRow1").bind('click', function() {

                                var $row = $(rowTemp);
                                count++;
                                // save reference to inputs within row
                                var $pointName = $row.find('#pointName');
                                var $pointId = $row.find('#pointId');
                                var lastPointId =  $('input[name=pointId]').last().val();
                                var lastorder = $('input[name=order]').last().val();
                                $row.find('#order').val(parseInt(lastorder)+1)
                                $("#endOrder").val(parseInt(lastorder)+2)
                                //alert("lastPointId"+lastPointId);
                                var endPointId = $("#endPointId").val();
                                
                                // apply autocomplete widget to newly created row
                                $row.find('#pointName').autocomplete({
                                    source: function(request, response) {
                                        $.ajax({
                                            url: "/throttle/getRouteCourse.do",
                                            dataType: "json",
                                            data: {
                                                pointName: request.term,
                                                lastPointId:lastPointId,
                                                endPointId:endPointId
                                            },
                                            success: function(data, textStatus, jqXHR) {
                                                //console.log( data);
                                                var items = data;
                                                response(items);
                                            },
                                            error: function(data, type) {
                                                console.log(type);
                                            }
                                        });
                                    },
                                    minLength: 0,
                                    select: function(event, ui) {
                                        var $itemrow = $(this).closest('tr');
                                        // Populate the input fields from the returned values
                                        var value = ui.item.Name;
                                        //                                alert(value);
                                        var tmp = value.split('-');
                                        $itemrow.find('#pointId').val(tmp[0]);
                                        var pointVal = tmp[0];
                                        $itemrow.find('#pointName').val(tmp[1]);
                                        $itemrow.find('#multiplePointRouteId').val(tmp[2]);
                                        checkRouteCode(lastPointId,pointVal);
                                        var routeStatus = $("#routeStatus").text();
                                        alert(routeStatus);
                                        if(routeStatus == 'Route Not Exists'){
                                        $row.find('#pointId').val('');
                                        $row.find('#pointName').val('');
                                        $row.find('#multiplePointRouteId').val(0);
                                        }else if(routeStatus == 'Exists'){
                                        $row.find('#pointId').val('');
                                        $row.find('#pointName').val('');
                                        $row.find('#multiplePointRouteId').val(0);
                                        }
                                        return false;
                                    }
                                    // Format the list menu output of the autocomplete
                                }).data("autocomplete")._renderItem = function(ul, item) {
                                    //alert(item);
                                    var itemVal = item.Name;
                                    var temp = itemVal.split('-');
                                    itemVal = '<font color="green">' + temp[1] + '</font>';
                                    return $("<li></li>")
                                            .data("item.autocomplete", item)
                                            //.append( "<a>"+ item.Name + "</a>" )
                                            .append("<a>" + itemVal + "</a>")
                                            .appendTo(ul);
                                };

                                 
                                 
                                // Add row after the first row in table
                                $('.item-row:last', $itemsTable).after($row);
                                $($pointName).focus();
                                 
                                 $(document).ready(function() {
                                $("#datepicker").datepicker({
                                    showOn: "button",
                                    buttonImage: "calendar.gif",
                                    buttonImageOnly: true

                                });
                            });


                                $(function() {
                                    $(".datepicker").datepicker({
                                        changeMonth: true, changeYear: true
                                    });
                                });
                                        
                                return false;
                            });

                            $('#pointName').focus(function() {
                                window.onbeforeunload = function() {
                                    return "You haven't saved your data.  Are you sure you want to leave this page without saving first?";
                                };
                            });
                        });// End DOM

                        // Remove row when clicked
                        $("#deleteRow").live('click', function() {
                            $(this).parents('.item-row').remove();
                            // Hide delete Icon if we only have one row in the list.
                            if ($(".item-row").length < 2)
                                $("#deleteRow").hide();
                        });

                        var httpRequest;
                        function checkRouteCode(cityFromId,cityToId) {
                            if (cityFromId != '' && cityToId != '') {
                                var url = '/throttle/checkRoute.do?cityFromId=' + cityFromId + '&cityToId=' + cityToId;
                                if (window.ActiveXObject) {
                                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                                } else if (window.XMLHttpRequest) {
                                    httpRequest = new XMLHttpRequest();
                                }
                                httpRequest.open("GET", url, true);
                                httpRequest.onreadystatechange = function() {
                                    processRequest();
                                };
                                httpRequest.send(null);
                            }
                        }


                        function processRequest() {
                            var val= "";
                            if (httpRequest.readyState == 4) {
                                if (httpRequest.status == 200) {
                                    val = httpRequest.responseText.valueOf();
                                    if (val == 'null') {
                                        $("#routeStatus").text('Route Not Exists');
                                    } else {
                                        $("#routeStatus").text('Exists');
                                    }
                                } else {
                                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                                }
                            }
                            return;
                        }

                    </script>
                    <div class="inpad">
                        <table id="itemsTable1" class="general" style="width: 980">
                            <thead>
                                <tr>
                                    <td></td>                          
                                    <td class="contenthead" height="30" >Start Point</td>
                                    <td class="contenthead" height="30" >Type</td>
                                    <td class="contenthead" height="30" >Route Order</td>
                                    <td class="contenthead" height="30" >Address</td>
                                    <td class="contenthead" height="30" >Planned Date</td>
                                    <td class="contenthead" height="30" >Planned Time</td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="item-row">
                                    <td></td>
                                    <td class="text1" height="30" ><input type="hidden" name="pointId" id="pointId" value=""/><input type="hidden" name="multiplePointRouteId" id="multiplePointRouteId" value="0"/><input type="hidden" name="pointName" id="pointName"value=""/><label id="startPointName" style="width: auto" ></label></td>
                                    <td class="text1" height="30" ><select name="pointType" id="pointType"><option value="Pick Up" selected>Pick Up</option></select></td>
                                    <td class="text1" height="30" ><input type="text" name="order" id="order" value="1" style="width:30px"/></td>
                                    <td class="text1" height="30" ><textarea name="pointAddresss" id="pointAddresss" rows="2" cols="15"></textarea></td>
                                    <td class="text1" height="30" ><input type="text" class="datepicker" name="pointPlanDate"  id="pointPlanDate"  value='' /></td>
                                    <td class="text1" height="30" >Hr:<input type="text" name="pointPlanHour" id="pointPlanHour" value='' style="width: 30px"/>Min:<input type="text" name="pointPlanMinute" id="pointPlanMinute" value='' style="width: 30px"/><label id="routeStatus" name="routeStatus" style="color: red"></label></td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="#" id="addRow1" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /> Add Route Course</span></a>
                        <br>
                    </div>

                    <table border="0" style="width: 980" >
                        <tr>
                            <td class="contenthead" height="30" >End Point</td>
                            <td class="contenthead" height="30" >Type</td>
                            <td class="contenthead" height="30" >Route Order</td>
                            <td class="contenthead" height="30" >Address</td>
                            <td class="contenthead" height="30" >Planned Date</td>
                            <td class="contenthead" height="30" >Planned Time</td>
                        </tr>
                        <tr>
                            <td class="text1" height="30" ><input type="hidden" name="endPointId" id="endPointId" value=""/><input type="hidden" name="finalRouteId" id="finalRouteId" value=""/><input type="hidden" name="endPointName" id="endPointName" value=""/><label id="endPointName1" style="width: auto"></label></td>
                            <td class="text1" height="30" ><select name="endPointType" id="endPointType"> <option value="Drop">Drop</option></select></td>
                            <td class="text1" height="30" ><input type="text" name="endOrder" id="endOrder" style="width:30px" /></td>
                            <td class="text1" height="30" ><textarea name="endPointAddresss" id="endPointAddresss" rows="2" cols="15"></textarea></td>
                            <td class="text1" height="30" ><input type="text" class="datepicker" name="endPointPlanDate" id="endPointPlanDate" value='' /></td>
                            <td class="text1" height="30" >Hr:<input type="text" name="endPointPlanHour" id="endPointPlanHour" value='' style="width: 30px"/>Min:<input type="text" name="endPointPlanMinute" id="endPointPlanMinute" value='' style="width: 30px"/></td>
                        </tr>
                    </table>

                    <br>
                    <center>
                        <a  class="nexttab" href="#"><input type="button" class="button" value="Save &amp; Next" name="Save" ></a>
                    </center>
                    <br>
                    <br>
                </div>



                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                <div id="paymentDetails">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractFreightTable">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6" >Contract Freight Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Billing Type</td>
                            <td class="text1"><label id="billTypeName"></label></td>
                            <td class="text1">Freight Charges</td>
                            <td class="text1">INR. <label id="freightAmount"></label>
                                <input type="hidden" name="totFreightAmount" id="totFreightAmount" />
                            </td>
                        </tr>
                    </table>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinFreightTable">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6" >Walkin Freight Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Billing Type</td>
                            <td class="text1"><select name="walkInBillingTypeId" id="walkInBillingTypeId" class="texttbox" onchange="displayWalkIndetails(this.value)" style="width: 120px">
                                    <c:if test="${billingTypeList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${billingTypeList}" var="billType">
                                            <option value="<c:out value="${billType.billingTypeId}"/>"><c:out value="${billType.billingTypeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                            <td class="text1" colspan="2">&nbsp;</td>
                        </tr>
                        <tr id="WalkInptp" style="display: none">
                            <td class="text2">Freight With Reefer</td>
                            <td class="text2"><input type="text" name="walkinFreightWithReefer" id="walkinFreightWithReefer" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                            <td class="text2">Freight Without Reefer</td>
                            <td class="text2"><input type="text" name="walkinFreightWithoutReefer" id="walkinFreightWithoutReefer" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        </tr>
                        <tr id="WalkInPtpw" style="display: none">
                            <td class="text2">Rate With Reefer/Kg</td>
                            <td class="text2"><input type="text" name="walkinRateWithReeferPerKg" id="walkinRateWithReeferPerKg" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                            <td class="text2">Rate Without Reefer/Kg</td>
                            <td class="text2"><input type="text" name="walkinRateWithoutReeferPerKg" id="walkinRateWithoutReeferPerKg" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        </tr>
                        <tr id="WalkinKilometerBased" style="display: none">
                            <td class="text2">Rate With Reefer/Km</td>
                            <td class="text2"><input type="text" name="walkinRateWithReeferPerKm" id="walkinRateWithReeferPerKm" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                            <td class="text2">Rate Without Reefer/Km</td>
                            <td class="text2"><input type="text" name="walkinRateWithoutReeferPerKm" id="walkinRateWithoutReeferPerKm" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        function displayWalkIndetails(val) {
                            var reeferRequired = document.getElementById('reeferRequired').value;
                            var vehicleTypeId = document.getElementById('vehTypeId').value;
                            alert(vehicleTypeId);
                            if (vehicleTypeId == '0') {
                                alert("Please Choose Vehicle Type");
                                document.getElementById('vehTypeId').focus();
                            } else if (reeferRequired == '0') {
                                alert("Please Choose Reefer Details");
                                document.getElementById('reeferRequired').focus();
                            } else {
                                if (val == 1) {
                                    $("#WalkInptp").show();
                                    $("#WalkInPtpw").hide();
                                    $("#WalkinKilometerBased").hide();
                                    if (reeferRequired == "Yes") {
                                        $('#walkinFreightWithReefer').attr('readonly', false);
                                        $('#walkinFreightWithoutReefer').attr('readonly', true);
                                    } else if (reeferRequired == "No") {
                                        $('#walkinFreightWithReefer').attr('readonly', true);
                                        $('#walkinFreightWithoutReefer').attr('readonly', false);
                                    }
                                    $("#walkinRateWithReeferPerKm").val("");
                                    $("#walkinRateWithoutReeferPerKm").val("");
                                    $("#walkinRateWithReeferPerKg").val("");
                                    $("#walkinRateWithoutReeferPerKg").val("");
                                } else if (val == 2) {
                                    $("#WalkInptp").hide();
                                    $("#WalkInPtpw").show();
                                    $("#WalkinKilometerBased").hide();
                                    if (reeferRequired == "Yes") {
                                        $('#walkinRateWithReeferPerKg').attr('readonly', false);
                                        $('#walkinRateWithoutReeferPerKg').attr('readonly', true);
                                    } else if (reeferRequired == "No") {
                                        $('#walkinRateWithReeferPerKg').attr('readonly', true);
                                        $('#walkinRateWithoutReeferPerKg').attr('readonly', false);
                                    }
                                    $("#walkinFreightWithReefer").val("");
                                    $("#walkinFreightWithoutReefer").val("");
                                    $("#walkinRateWithReeferPerKm").val("");
                                    $("#walkinRateWithoutReeferPerKm").val("");
                                } else if (val == 3) {
                                    $("#WalkInptp").hide();
                                    $("#WalkInPtpw").hide();
                                    $("#WalkinKilometerBased").show();
                                    if (reeferRequired == "Yes") {
                                        $('#walkinRateWithReeferPerKm').attr('readonly', false);
                                        $('#walkinRateWithoutReeferPerKm').attr('readonly', true);
                                    } else if (reeferRequired == "No") {
                                        $('#walkinRateWithReeferPerKm').attr('readonly', true);
                                        $('#walkinRateWithoutReeferPerKm').attr('readonly', false);
                                    }
                                    $("#walkinFreightWithReefer").val("");
                                    $("#walkinFreightWithoutReefer").val("");
                                    $("#walkinRateWithReeferPerKg").val("");
                                    $("#walkinRateWithoutReeferPerKg").val("");
                                }
                            }
                        }
                    </script>
                    <br/>
                    <br/>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Standard / Additional Charges</td>
                        </tr>
                        <tr>
                            <td class="text1">Document Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="docCharges" name="docCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text1">ODA Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="odaCharges" name="odaCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text1">Multi Pickup Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="multiPickupCharge" name="multiPickupCharge" readonly onkeyup="calculateSubTotal()"></td>
                        </tr>
                        <tr>
                            <td class="text2">Multi Delivery Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="multiDeliveryCharge" name="multiDeliveryCharge" readonly onkeyup="calculateSubTotal()"></td>
                            <td class="text2">Handling Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="handleCharges" name="handleCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text2">Other Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="otherCharges" name="otherCharges" onkeyup="calculateSubTotal()"></td>
                        </tr>
                        <tr>                            
                            <td class="text1">Unloading Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="unloadingCharges" name="unloadingCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text1">Loading Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="loadingCharges" name="loadingCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text1">Remarks</td>
                            <td class="text1"><textarea rows="3" cols="20" nmae="standardChargeRemarks" id="standardChargeRemarks"></textarea></td>
                        </tr>
                        <tr>
                            <td class="text2" colspan="4"></td>
                            <td class="text2">Sub Total</td>
                            <td class="text2"><input type="text" class="textbox" id="subTotal" name="subTotal"  value="0" readonly=""></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        function calculateSubTotal() {
                            var customerTypeId = document.getElementById("customerTypeId").value;
                            if (customerTypeId == 2) {
                                var fixedRateWithReefer = $("#walkinFreightWithReefer").val();
                                var fixedRateWithoutReefer = $("#walkinFreightWithoutReefer").val();
                                var rateWithReeferPerKm = $("#walkinRateWithReeferPerKm").val();
                                var rateWithoutReeferPerKm = $("#walkinRateWithoutReeferPerKm").val();
                                var rateWithReeferPerKg = $("#walkinRateWithReeferPerKg").val();
                                var rateWithoutReeferPerKg = $("#walkinRateWithoutReeferPerKg").val();
                            }
                            var freightAmount = $('#freightAmount').text();
                            var docCharges = document.getElementById('docCharges').value;
                            var odaCharges = document.getElementById('odaCharges').value;
                            var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                            var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                            var handleCharges = document.getElementById('handleCharges').value;
                            var otherCharges = document.getElementById('otherCharges').value;
                            var unloadingCharges = document.getElementById('unloadingCharges').value;
                            var loadingCharges = document.getElementById('loadingCharges').value;
                            var total = 0;
                            var walkInTotal = 0;
                            var kmRate = 0;
                            var kgRate = 0;
                            if (customerTypeId == 2) {
                                var billingTypeId = $("#walkInBillingTypeId").val();
                                var totalKm = $("#totalKm").val();
                                var totalWeight = $("#totalWeight").text();
                                if (fixedRateWithReefer != '' && billingTypeId == 1) {
                                    walkInTotal += parseInt(fixedRateWithReefer);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (fixedRateWithoutReefer != '' && billingTypeId == 1) {
                                    walkInTotal += parseInt(fixedRateWithoutReefer);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithReeferPerKm != '' && billingTypeId == 3) {
                                    kmRate = parseInt(totalKm) * parseInt(rateWithReeferPerKm);
                                    walkInTotal += parseInt(kmRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithoutReeferPerKm != '' && billingTypeId == 3) {
                                    kmRate = parseInt(totalKm) * parseInt(rateWithoutReeferPerKm);
                                    walkInTotal += parseInt(kmRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithReeferPerKg != '' && billingTypeId == 2) {
                                    kgRate = parseInt(totalWeight) * parseInt(rateWithReeferPerKg);
                                    walkInTotal += parseInt(kgRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithoutReeferPerKg != '' && billingTypeId == 2) {
                                    kgRate = parseInt(totalWeight) * parseInt(rateWithoutReeferPerKg);
                                    walkInTotal += parseInt(kgRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                            }
                            if (docCharges != '') {
                                total += parseInt(docCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (odaCharges != '') {
                                total += parseInt(odaCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiPickupCharge != '') {
                                total += parseInt(multiPickupCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiDeliveryCharge != '') {
                                total += parseInt(multiDeliveryCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (handleCharges != '') {
                                total += parseInt(handleCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (otherCharges != '') {
                                total += parseInt(otherCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (unloadingCharges != '') {
                                total += parseInt(unloadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (loadingCharges != '') {
                                total += parseInt(loadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            document.getElementById('subTotal').value = total;
                            if (customerTypeId == 1) {
                                if (freightAmount != '') {
                                    total += parseInt(freightAmount);
                                } else {
                                    total += parseInt(0);
                                }
                            } else if (customerTypeId == 2) {
                                total += parseInt(walkInTotal);
                            }
                            document.getElementById('totalCharges').value = total;
                        }
                    </script>
                    <br/>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="60">&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td class="contentsub"  height="30" colspan="6" align="right">Total Charges</td>
                            <td class="contentsub"  height="30" align="right">INR.<input align="right" value="" type="text" readonly class="textbox" id="totalCharges" name="totalCharges" ></td>
                        </tr>
                    </table>
                    <br/>
                    <br/>

                    <center>
                        <input type="button" class="button" name="Save" value="Save" onclick="submitPage(this.value)" >
                    </center>
                </div>

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>