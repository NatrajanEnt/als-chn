DROP TABLE IF EXISTS `rathimeena`.`ra_advance_issuername_master_epos`;
CREATE TABLE  `rathimeena`.`ra_advance_issuername_master_epos` (
  `Advance_Issuer_Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Advance_Issuer_Code` varchar(45) NOT NULL,
  `Advance_Issuer_Name` varchar(200) NOT NULL,
  `Designation` varchar(200) NOT NULL,
  `Act_Ind` varchar(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Advance_Issuer_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rathimeena`.`ra_fuel_location_master_epos`;
CREATE TABLE  `rathimeena`.`ra_fuel_location_master_epos` (
  `Fuel_Locat_Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Fuel_Locat_Code` varchar(45) NOT NULL,
  `Fuel_Locat_Name` varchar(200) NOT NULL,
  `Bunk_Name` varchar(200) NOT NULL,
  `Act_Ind` varchar(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Fuel_Locat_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rathimeena`.`ra_operation_location_master_epos`;
CREATE TABLE  `rathimeena`.`ra_operation_location_master_epos` (
  `Location_Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Location_Code` varchar(45) NOT NULL,
  `Location_Name` varchar(200) NOT NULL,
  `Act_Ind` varchar(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Location_Id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `rathimeena`.`ra_route_master`;
CREATE TABLE  `rathimeena`.`ra_route_master` (
  `route_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `route_code` varchar(10) NOT NULL,
  `Location_Id` int(10) unsigned NOT NULL,
  `from_location` varchar(200) NOT NULL,
  `to_location` varchar(200) NOT NULL,
  `via_route` varchar(200) DEFAULT NULL,
  `KM` int(10) unsigned NOT NULL DEFAULT '0',
  `Toll_Amount` decimal(18,0) NOT NULL,
  `TonnageRate` decimal(18,0) NOT NULL,
  `Driverbata` decimal(18,0) NOT NULL,
  `Description` varchar(1500) DEFAULT NULL,
  `Active_Ind` varchar(1) NOT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`route_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rathimeena`.`ra_trip_advance_epos`;
CREATE TABLE  `rathimeena`.`ra_trip_advance_epos` (
  `TripAdvanceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TripId` int(10) unsigned NOT NULL,
  `IdentityNo` varchar(45) NOT NULL,
  `DeviceId` int(10) unsigned NOT NULL,
  `Advance_Issuer_Id` int(10) unsigned NOT NULL,
  `Amount` float NOT NULL,
  `A_DateAndTime` datetime NOT NULL,
  `Act_Ind` varchar(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`TripAdvanceId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `rathimeena`.`ra_trip_eligible_status_epos`;
CREATE TABLE  `rathimeena`.`ra_trip_eligible_status_epos` (
  `Eligible_Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Route_Id` int(10) unsigned NOT NULL,
  `Vehicle_Id` int(10) unsigned NOT NULL,
  `Eligible_Count` int(10) unsigned NOT NULL,
  `Driver_Id` int(10) unsigned NOT NULL,
  `FromDate` datetime DEFAULT NULL,
  `ToDate` datetime DEFAULT NULL,
  `Act_Ind` varchar(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` datetime DEFAULT NULL,
  PRIMARY KEY (`Eligible_Id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



DROP TABLE IF EXISTS `rathimeena`.`ra_trip_fuel_epos`;
CREATE TABLE  `rathimeena`.`ra_trip_fuel_epos` (
  `TripFuelId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TripId` int(10) unsigned NOT NULL,
  `IdentityNo` varchar(45) NOT NULL,
  `DeviceId` int(10) unsigned NOT NULL,
  `Fuel_Locat_Id` int(10) unsigned NOT NULL,
  `Liters` int(10) NOT NULL,
  `F_DateAndTime` datetime NOT NULL,
  `Act_Ind` varchar(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`TripFuelId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;





DROP TABLE IF EXISTS `rathimeena`.`ra_trip_open_close_epos`;
CREATE TABLE  `rathimeena`.`ra_trip_open_close_epos` (
  `TripId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdentityNo` varchar(45) NOT NULL,
  `DeviceId` int(10) unsigned NOT NULL,
  `RouteId` int(10) NOT NULL,
  `OutKM` int(20) unsigned NOT NULL,
  `DriverId` int(10) NOT NULL,
  `Open_DateAndTime` datetime NOT NULL,
  `InKM` int(20) unsigned DEFAULT '0',
  `Location` varchar(200) DEFAULT NULL,
  `Close_DateAndTime` datetime DEFAULT NULL,
  `Settlement_Flag` varchar(1) NOT NULL DEFAULT 'N',
  `Status` varchar(45) NOT NULL,
  `Act_Ind` varchar(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`TripId`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `rathimeena`.`ra_trip_vehicle_in_out_epos`;
CREATE TABLE  `rathimeena`.`ra_trip_vehicle_in_out_epos` (
  `TripVehicleInOutId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TripId` int(10) unsigned NOT NULL,
  `IdentityNo` varchar(45) NOT NULL,
  `DeviceId` int(10) unsigned NOT NULL,
  `VInOutIndication` varchar(45) NOT NULL,
  `Location_Id` int(10) unsigned NOT NULL,
  `Location` varchar(200) NOT NULL,
  `VInOut_DateAndTime` datetime NOT NULL,
  `Act_Ind` varchar(1) DEFAULT NULL,
  `CreatedBy` int(10) unsigned NOT NULL,
  `CreatedDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ModifiedBy` int(10) unsigned DEFAULT NULL,
  `ModifiedDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`TripVehicleInOutId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;