<%-- 
    Document   : displayTripSettlement
    Created on : Jun 11, 2013, 11:52:27 PM
    Author     : Entitle
--%>

<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->

        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }

        </style>
    </head>
    <body onload="setFocus()" >
        <form name="tripsettlementwise" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "Diesel_Report-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>
         <br/>
            
         <%
                            DecimalFormat df = new DecimalFormat("0.00##");
                            String tripId = "",tripDate = "",destination = "",vehicleNumber = "",regNo = "",totalExpenses = "";
                            String departureName = "",liter = "",totalTonnage = "",distance = "",totalFreightAmount = "",driverName = "";
                            double totalFreight = 0,subTotalFreight = 0,totalSettlement = 0;
                            double grandTotalTonnage = 0;
                            double grandTotalDistance = 0;
                            double grandTotalFuel = 0;
                            String className = "text2",settlementAmount = "";
                            int index = 0;
                            int sno = 0;
%> <br>
                <table  border="1" class="border" align="center" width="950" cellpadding="0" style="margin-left:30px" cellspacing="0" id="bg">
                    <c:if test = "${dieselReportList != null}" >
                        <%
                        ArrayList dieselReportList = (ArrayList) request.getAttribute("dieselReportList");
                        Iterator detailsItr = dieselReportList.iterator();
                        ReportTO repTO = null;
                         if (dieselReportList.size() != 0) {
                            %>
                            <tr>
                                <td class="contentsub" height="30">S.No</td>
                                <td class="contentsub" height="30">Trip Id</td>
                                <td class="contentsub" height="30">Vehicle No</td>
                                <td class="contentsub" height="30">Trip Date</td>
                                <td class="contentsub" height="30">Driver Name</td>
                                <td class="contentsub" height="30">Route Name</td>
                                <td class="contentsub" height="30">Tonnage</td>
                                <td class="contentsub" height="30">Fuel Liters</td>
                                <td class="contentsub" height="30">KM</td>
                                </tr>
                    <%
                     while (detailsItr.hasNext()) {
                         index++;
                         sno++;
                           if(index%2 == 0){
                            className = "text2";

                            }else {
                            className = "text1";
                        }
                         repTO = new ReportTO();
                         repTO = (ReportTO) detailsItr.next();
                         tripId  = repTO.getTripId();
                         driverName  = repTO.getDriverName();
                         regNo  = repTO.getRegno();
                        if(tripId == null){
                            tripId = "";
                            }
                        tripDate  = repTO.getTripDate();
                        if(tripDate == null){
                            tripDate = "";
                            }
                            destination  = repTO.getDestination();
                        if(destination == null){
                            destination = "";
                            }

                            totalTonnage  = repTO.getTotalTonnage();
                            if(totalTonnage == null){
                                totalTonnage = "0";
                                }
                            grandTotalTonnage += Double.parseDouble(totalTonnage);

                            liter  = repTO.getTripTotalLitres();
                            if(liter == null){
                                liter = "0";
                                }
                            grandTotalFuel += Double.parseDouble(liter);

                            distance  = repTO.getDistance();
                            if(distance == null){
                                distance = "0";
                                }
                            grandTotalDistance += Double.parseDouble(distance);
                            
                        %>
                        <tr>
                          <td class="<%=className%>"  height="30"><%=sno%></td>
                          <td class="<%=className%>"  height="30"><%=tripId%></td>
                          <td class="<%=className%>"  height="30"><%=regNo%></td>
                          <td class="<%=className%>"  height="30"><%=tripDate%></td>
                          <td class="<%=className%>"  height="30"><%=driverName%></td>
                          <td class="<%=className%>"  height="30"><%=destination%></td>
                          <td class="<%=className%>"  align="right"  height="30"><%=totalTonnage%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  height="30"><%=liter%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  align="right"  height="30"><%=distance%>&nbsp;&nbsp;&nbsp;&nbsp;</td>

                        </tr>
                    
                        <%
                     }

                             }
%>
                    
                      <tr>
                          <td colspan="6" class="<%=className%>"  align="right"  height="30">Total</td>
                          <td class="<%=className%>"  align="right"  height="30"><%=df.format(grandTotalTonnage)%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  align="right"  height="30"><%=df.format(grandTotalFuel)%>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                          <td class="<%=className%>"  align="right" height="30"><%=df.format(grandTotalDistance)%>&nbsp;&nbsp;&nbsp;&nbsp;</td>

                        </tr>
</c:if>
                </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
<script type="text/javaScript">
        function getTripIds(){
            var oTextbox = new AutoSuggestControl(document.getElementById("tripId"),new ListSuggestions("tripId","/throttle/tripIdSuggestions.do?"));
        }
        function getVehicleNumbers(){
            var oTextbox = new AutoSuggestControl(document.getElementById("vehicleNo"),new ListSuggestions("vehicleNo","/throttle/vehicleNumberSuggestions.do?"));
        //alert('oTextbox is::'+oTextbox);
        }

    </script>