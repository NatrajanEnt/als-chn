
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({minDate: 0,
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true
        });
    });

    $(function() {
        $(".datepicker").datepicker({minDate: 0,
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });


</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>


<script type="text/javascript" language="javascript">
    function submitPage() {
        var confirms = "";
        confirms = confirm("Please Confirm");
        if (confirms == true) {
            document.trip.action = '/throttle/changeRouteDeviation.do';
            document.trip.submit();
//            $("#Update").hide();
        }
    }

</script>
<style>
    #index td {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Route Update</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Route Updation</a></li>
            <li class="active">Route Updation</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>

                <form name="trip" method="post">
                    <%
               Date today = new Date();
               SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
               String startDate = sdf.format(today);
                    %>
                    <%@ include file="/content/common/message.jsp" %>

                    <div style="display:block;">

                        <table width="100%">
                            <% int loopCntr = 0;%>
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <input type="hidden" nsame="driverId" value='<c:out value="${trip.driverId}" />'>
                                    <% if(loopCntr == 0) {%>
                                    <tr id="index" height="30">
                                        <td>Customer Job No: <c:out value="${trip.customerOrderReferenceNo}" /></td>
                                        <td>Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                                        <td>Trip Code: <c:out value="${trip.tripCode}"/></td>
                                    </tr>
                                    <tr id="index" height="30">
                                        <td>Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                                        <td>Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                                        <td>Status: <c:out value="${trip.status}"/>
                                            <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                            <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                            <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                                            <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>' />
                                            <input type="hidden" name="statusId" value='<c:out value="${statusId}"/>' />
                                            <input type="hidden" name="tripId" value='<c:out value="${trip.tripId}"/>' />
                                            <c:set var="customerId" value="${trip.customerId}"></c:set></td>
                                        </tr>
                                    <% }%>
                                    <% loopCntr++;%>
                                </c:forEach>
                            </c:if>
                        </table>
                        <div id="tabs" >
                            <ul class="nav nav-tabs">
                                <li data-toggle="tab"><a href="#routeDetail"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                            </ul>
                            <input type="hidden" name="pageId" id="pageId" value="<c:out value="${uniquePageId}"/>"/>

                            <div id="tripDetail" class="tab-pane" style="display:none">
                                <table class="table table-info mb30 table-hover" id="bg" width="100%" >
                                    <tr id="index" height="30">
                                        <td  colspan="6" >Trip Details</td>
                                    </tr>

                                    <c:if test = "${tripDetails != null}" >
                                        <c:forEach items="${tripDetails}" var="trip">
                                            <tr>
                                                <td >Consignment No(s)</td>
                                                <td >
                                                    <c:out value="${trip.cNotes}" />
                                                    <input type="hidden" name="cNotesEmail" class="form-control" value='<c:out value="${trip.cNotes}"/>' />
                                                </td>
                                                <td >Billing Type</td>
                                                <td >
                                                    <c:out value="${trip.billingType}" />
                                                </td>

                                            </tr>
                                            <tr>                                         
                                                <td >Customer Name</td>
                                                <td >
                                                    <c:out value="${trip.customerName}" />
                                                    <input type="hidden" name="customerName" Id="customerName" class="textbox" value='<c:out value="${customerName}" />'>
                                                </td>
                                                <td >Customer Type</td>
                                                <td  >
                                                    <c:out value="${trip.customerType}" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td >Route Name</td>
                                                <td colspan="3">
                                                    <c:out value="${trip.routeInfo}" />
                                                </td>                                                
                                            </tr>
                                            <tr>
                                                <td >Vehicle Type</td>
                                                <td >
                                                    <c:out value="${trip.vehicleTypeName}" />
                                                </td>
                                                <td ><font color="red">*</font>Vehicle No</td>
                                                <td >
                                                    <c:out value="${trip.vehicleNo}" />
                                                    <input type="hidden" name="vehicleno" value="<c:out value="${trip.vehicleNo}" />"/>
                                                    <input type="hidden" name="vehicleNoEmail" value='<c:out value="${trip.vehicleNo}"/>' />
                                                    <input type="hidden" id="movementType" name="movementType" value='<c:out value="${trip.movementType}"/>' />

                                                </td>

                                            </tr>

                                            <tr>
                                                <td >Vehicle Capacity (MT)</td>
                                                <td >
                                                    <c:out value="${trip.vehicleTonnage}" />

                                                </td>
                                                <td >Trip Schedule</td>
                                                <td ><c:out value="${trip.tripScheduleDate}" />  <c:out value="${trip.tripScheduleTime}" />
                                                    <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${trip.tripScheduleDate}" />'>
                                                    <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${trip.tripScheduleTime}" />'></td>
                                            </tr>


                                            <tr>
                                                <td >Driver </td>
                                                <td  >
                                                    <c:out value="${trip.driverName}" />
                                                </td>
                                                <td > </td>
                                                <td  colspan="3" >
                                                </td>

                                            </tr>
                                            <tr style="display:none;">
                                                <td >Product Info </td>
                                                <td  colspan="5" >
                                                    <c:out value="${trip.productInfo}" />
                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </table>
                                <br/>

                            </div>
                            <div id="routeDetail" class="tab-pane">

                                <c:if test = "${tripPointDetails != null}" >
                                    <table class="table table-info mb30 table-bordered" id="table" width="100%" >
                                        <!--<table border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" >-->
                                        <tr id="index" height="30">
                                            <td  height="30" >S No </td>
                                            <td  height="30" >Point Name</td>
                                            <td  height="30" >Type</td>
                                            <td  height="30" >Route Order</td>
                                            <td  height="30" >Address</td>
                                            <td  height="30" >Planned Date</td>
                                            <td  height="30" >Planned Time</td>
                                        </tr>

                                        <% int index2 = 1; %>
                                        <c:forEach items="${tripPointDetails}" var="tripPoint">
                                            <%
                                               String classText1 = "";
                                               int oddEven = index2 % 2;
                                               if (oddEven > 0) {
                                                   classText1 = "text1";
                                               } else {
                                                   classText1 = "text2";
                                               }
                                            %>
                                            <tr >
                                                <td  height="30" ><%=index2%></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointName}" /></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointType}" /></td>
                                                <td  height="30" ><c:out value="${tripPoint.pointSequence}" /></td>
                                                <td height="30" >
                                                
                                                    <c:if test = "${(tripPoint.pointType == 'Loading' ||  tripPoint.pointType == 'Loading Point' ||  tripPoint.pointType == 'Unloading Point' ||  tripPoint.pointType == 'Unloading') && tripPoint.pointSequence == 2}" >
                                                        <select id="pointAddresss" name="pointAddresss" class="form-control" style="width:500px;height:40px" onchange="setRevenue(<%=index2%>);">
                                                            <option value="0">-Select Address-</option>   
                                                            <c:forEach items="${customerContract}" var="cl">                                                                
                                                                <option value="<c:out value="${cl.pointIds}" />~<c:out value="${cl.route}" />~<c:out value="${cl.orderRevenue}" />"><c:out value="${cl.route}" /> (<c:out value="${cl.orderRevenue}" />)</option>
                                                            </c:forEach>
                                                        </select>                                                
                                                        <!--<input type="hidden" id="pointId<%=index2%>" name="pointId" value="<c:out value="${tripPoint.pointId}" />"/>-->
                                                        <input type="hidden" id="pointId" name="pointId" value="<c:out value="${tripPoint.pointIds}" />"/>
                                                        <!--<input type="hidden" id="pointName<%=index2%>" name="pointName" value="<c:out value="${tripPoint.pointName}" />"/>-->
                                                        <input type="hidden" id="pointName" name="pointName" value="<c:out value="${tripPoint.route}" />"/>
                                                        <input type="hidden" id="orderRevenue" name="orderRevenue" value="<c:out value="${tripPoint.revenue}" />"/>
                                                        <!--<input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>-->
                                                        <script>
//                                                            var pointId = '<c:out value="${tripPoint.pointId}" />';
                                                            var pointId = '<c:out value="${tripPoint.pointIds}" />';
//                                                            var pointName = '<c:out value="${tripPoint.pointName}" />';
                                                            var route = '<c:out value="${tripPoint.route}" />';
                                                            var revenue = '<c:out value="${tripPoint.revenue}" />';
//                                                            var val = pointId+"~"+pointName+"~"+revenue;                                                            
                                                            var val = pointId+"~"+route+"~"+revenue;                                                            
                                                            document.getElementById("pointAddresss").value = val.trim();                                                            
                                                        </script>
                                                    </c:if>

                                            <c:if test = "${(tripPoint.pointType == 'Loading' ||  tripPoint.pointType == 'Loading Point' ||  tripPoint.pointType == 'Unloading Point' ||  tripPoint.pointType == 'Unloading') && tripPoint.pointSequence != 2}" >
                                                <c:out value="${tripPoint.pointName}" />                                                
<!--                                                <input type="hidden" id="pointName" name="pointName" value="0"/>
                                                <input type="hidden" id="orderRevenue<%=index2%>" name="orderRevenue" value="0"/>
                                                <input type="hidden" id="pointId<%=index2%>" name="pointId" value="0"/>
                                                <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId<%=index2%>" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>-->
                                            </c:if>
                                            <c:if test = "${tripPoint.pointType != 'Loading' &&  tripPoint.pointType != 'Loading Point' &&  tripPoint.pointType != 'Unloading Point' &&  tripPoint.pointType != 'Unloading'}" >
                                                <c:out value="${tripPoint.pointName}" />                                                
<!--                                                <input type="hidden" id="pointName<%=index2%>" name="pointName" value="0"/>
                                                <input type="hidden" id="orderRevenue<%=index2%>" name="orderRevenue" value="0"/>
                                                <input type="hidden" id="pointId<%=index2%>" name="pointId" value="0"/>
                                                <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId<%=index2%>" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>-->
                                            </c:if>
                                            </td>

                                            <td  height="30" ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                            <td  height="30" ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                            </tr>
                                            <%index2++;%>
                                        </c:forEach >
                                    </c:if>
                                </table>

                                <br>
                                <script>                                    
                                    function setRevenue(index){
                                        var value = document.getElementById("pointAddresss").value;
                                        //alert(value);
                                        var det = value.split("~");
                                        document.getElementById("pointId").value = det[0];
                                        document.getElementById("pointName").value = det[1];
                                        document.getElementById("orderRevenue").value = det[2];
                                        submitPage();
                                    }
                                </script>

<!--                                <center>
                                    <input type="button" class="btn btn-success" id="Update" name="Update" style="width:100px;height:35px;padding:2px; " value="Update" onclick="submitPage();" />    
                                </center>-->


                                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                                </body>
                            </div>
                        </div>
                    </div>
                    <%@ include file="/content/common/NewDesign/settings.jsp" %>