/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.accident.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.accident.business.AccidentTO;
import ets.domain.accident.data.AccidentDAO;
import java.util.ArrayList;

/**
 *
 * @author hp
 */
public class AccidentBP {
    
    public AccidentBP() {
    }
    AccidentDAO accidentDAO;

    public AccidentDAO getAccidentDAO() {
        return accidentDAO;
    }

    public void setAccidentDAO(AccidentDAO accidentDAO) {
        this.accidentDAO = accidentDAO;
    }
    



 public ArrayList getVehicleAccident() throws FPBusinessException, FPRuntimeException {
         ArrayList VehicleAccident = new ArrayList();
         VehicleAccident = accidentDAO.getVehicleAccident();
         return VehicleAccident;
     }
     public ArrayList getVehicleAccidentList(String accidentId) throws FPBusinessException, FPRuntimeException {
             ArrayList vehicleAccidentList = new ArrayList();
             vehicleAccidentList = accidentDAO.getVehicleAccidentList(accidentId);
             return vehicleAccidentList;
         }
     
         public int insertVehicleAccidentDetails(AccidentTO accidentTO, int UserId, int companyId, String[] actualFilePath, String tripSheetId1, String[] fileSaved, String[] vehicleDetails1, String[] accidentUploadId, String altVehicleId) throws FPRuntimeException, FPBusinessException {
             System.out.println("vehicle BP");
             int insertStatus = 0;
             int Accident_Id = 0;
             Accident_Id = accidentDAO.insertVehicleAccidentDetails(accidentTO, UserId, actualFilePath, tripSheetId1, fileSaved, vehicleDetails1, accidentUploadId, altVehicleId);
     
             return Accident_Id;
         }
          public ArrayList processVehicleAccidentDetail(AccidentTO accidentTO) throws FPRuntimeException, FPBusinessException {
     
             ArrayList VehicleAccidentDetailUnique = new ArrayList();
             VehicleAccidentDetailUnique = accidentDAO.processVehicleAccidentDetail(accidentTO);
     
             return VehicleAccidentDetailUnique;
         }
     
         public int updateSaveAccidentVehicle(AccidentTO accidentTO, int UserId, int companyId, String[] actualFilePath, String tripSheetId1, String[] fileSaved, String[] vehicleDetails1, String[] insuImageId, String[] editStatus, String[] editStatus1, String[] editStatus2) throws FPRuntimeException, FPBusinessException {
             System.out.println("vehicle BP");
             int insertStatus = 0;
             int Accident_Id = 0;
             Accident_Id = accidentDAO.updateSaveAccidentVehicle(accidentTO, UserId, actualFilePath, tripSheetId1, fileSaved, vehicleDetails1, insuImageId, editStatus, editStatus1, editStatus2);
     
             return Accident_Id;
         }
     
         public ArrayList getVehicleAccidentImageDetails(int accident_Id) throws FPRuntimeException, FPBusinessException {
             ArrayList AccidentUploadDetails = new ArrayList();
             AccidentUploadDetails = accidentDAO.getVehicleAccidentImageDetails(accident_Id);
             return AccidentUploadDetails;
     
    }
       public ArrayList processVehicleAccidentHistory(AccidentTO accidentTO) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleAccidentHistory = new ArrayList();
        vehicleAccidentHistory = accidentDAO.processVehicleAccidentHistory(accidentTO);

        return vehicleAccidentHistory;
    }
    public ArrayList getVehicleAccidentHistorylist(AccidentTO accidentTO,String vehicleId) throws FPRuntimeException, FPBusinessException {

        ArrayList vehicleAccidentHistorylist = new ArrayList();
        vehicleAccidentHistorylist = accidentDAO.getVehicleAccidentHistorylist(accidentTO,vehicleId);

        return vehicleAccidentHistorylist;
    }
    public ArrayList viewvehicleAccidentHistorylist(AccidentTO accidentTO) throws FPRuntimeException, FPBusinessException {

        ArrayList viewvehicleAccidentHistorylist = new ArrayList();
        viewvehicleAccidentHistorylist = accidentDAO.viewvehicleAccidentHistorylist(accidentTO);

        return viewvehicleAccidentHistorylist;
    } 
    
//     public ArrayList processInsCompanyList() throws FPRuntimeException, FPBusinessException {
//
//        ArrayList InsComanyNameList = new ArrayList();
//        InsComanyNameList = accidentDAO.ProcessInsCompanyList();
//
//        return InsComanyNameList;
//
//    }
//     
     
      public int addAccidentVehicle(AccidentTO accidentTO, int userId)throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = accidentDAO.addAccidentVehicle(accidentTO,userId);
        return insertStatus;
    }
      public ArrayList getVehicleList() {
        ArrayList vehicleList = null;
        vehicleList = accidentDAO.getVehicleList();
        return vehicleList;
    }
}
