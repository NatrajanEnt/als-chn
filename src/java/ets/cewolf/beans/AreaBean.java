package ets.cewolf.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import de.laures.cewolf.DatasetProduceException;
import de.laures.cewolf.DatasetProducer;
import de.laures.cewolf.links.CategoryItemLinkGenerator;
import de.laures.cewolf.tooltips.CategoryToolTipGenerator;


public class AreaBean implements DatasetProducer, CategoryToolTipGenerator, CategoryItemLinkGenerator,Serializable  {

	private static final long serialVersionUID = 1L;
		private static final Log log = LogFactory.getLog(AreaBean.class);

	    String[] seriesNames = new String[10];
	    String[][] linkNames = new String[10][10];
	    String[][] links = new String[10][10];
	    int counter = 0;
	    int count = 0;
	    /**
		 *  Produces some random data.
		 */

	    public Object produceDataset(Map params) throws DatasetProduceException {
	    	log.debug("producing data.");

	    	  // Dataset can either be populated by hard-coded or else from a datasource
	    	DefaultCategoryDataset dataset = new DefaultCategoryDataset(){
				/**
				 *
				 */
				private static final long serialVersionUID = 1L;

				/**
				 * @see java.lang.Object#finalize()
				 */
				protected void finalize() throws Throwable {
					super.finalize();
					log.debug(this +" finalized.");
				}
	        };
					   seriesNames[0]="C";
					   seriesNames[1]="C++";
					   seriesNames[2]="Java";

					   links[0][0]="/index.jsp";
					   links[0][1]="/index.jsp";
					   links[0][2]="/index.jsp";
					   links[1][0]="/index.jsp";
					   links[1][1]="/index.jsp";
					   links[1][2]="/index.jsp";
					   links[2][0]="/index.jsp";
					   links[2][1]="/index.jsp";
					   links[2][2]="/index.jsp";

					   linkNames[0][0]="C";
					   linkNames[0][1]="C";
					   linkNames[0][2]="C";
					   linkNames[1][0]="C++";
					   linkNames[1][1]="C++";
					   linkNames[1][2]="C++";
					   linkNames[2][0]="Java";
					   linkNames[2][1]="Java";
					   linkNames[2][2]="Java";

	        		   dataset.addValue(30, "Developer with less than 2 years experience","C");
					   dataset.addValue(25, "Developer with 2 to 5 years experience","C");
					   dataset.addValue(55, "Developer with greater than 2 years experience","C");
					   dataset.addValue(50, "Developer with less than 2 years experience","C++");
					   dataset.addValue(35, "Developer with 2 to 5 years experience","C++");
					   dataset.addValue(15, "Developer with greater than 2 years experience","C++");
					   dataset.addValue(30, "Developer with less than 2 years experience",".Java");
					   dataset.addValue(55, "Developer with 2 to 5 years experience",".Java");
					   dataset.addValue(15, "Developer with greater than 2 years experience",".Java");

			      return dataset;

	    }

	    /**
	     * This producer's data is invalidated after 5 seconds. By this method the
	     * producer can influence Cewolf's caching behaviour the way it wants to.
	     */
		public boolean hasExpired(Map params, Date since) {
	        log.debug(getClass().getName() + "hasExpired()");
			return (System.currentTimeMillis() - since.getTime())  > 5000;
		}

		/**
		 * Returns a unique ID for this DatasetProducer
		 */
		public String getProducerId() {
			return "PageViewCountData DatasetProducer";
		}


		/**
		 * @see java.lang.Object#finalize()
		 */
		protected void finalize() throws Throwable {
			super.finalize();
			log.debug(this + " finalized.");
		}

		/**
		 * @see org.jfree.chart.tooltips.CategoryToolTipGenerator#generateToolTip(CategoryDataset, int, int)
		 */
		public String generateToolTip(CategoryDataset arg0, int series, int arg2) {

			return linkNames[arg2][series];
		}

		/**
	     * Returns a link target for a special data item.
	     */
	    public String generateLink(Object data, int series, Object category) {

	    	if(series == 0)
	    	{
		    	count = counter++;
	    		return links[count][series];
	    	}
	    	else {
	    		return links[count][series];
	    	}
	    }

	}


