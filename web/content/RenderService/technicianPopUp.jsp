<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<!--<html>-->
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <%@ page import="ets.domain.operation.business.OperationTO" %>
    <%@ page import="java.util.*" %>
    <%@ page import="java.http.*" %>
    <%@ page import="ets.domain.mrs.business.MrsTO" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javasckript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script type="text/javascript">
        function onKeyPressBlockCharacters(e) {
            var sts = false;
            var key = window.event ? e.keyCode : e.which;
            var keychar = String.fromCharCode(key);
            reg = /[a-zA-Z`~!@#$%^&*() _|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
            sts = !reg.test(keychar);
            return sts;
        }

        $(document).ready(function () {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function () {
            $(".datepicker").datepicker({
                changeMonth: true, changeYear: true
            });
        });
    </script>
    <script>
        var rowCount = 1;
        var sno = 0;
        var rowIndex = 0;
        var styl = '';
        function addRow()
        {

            if (parseInt(rowCount) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }




            sno++;
            //document.stkRequest.selectedIndex.value=sno;

            //if( parseInt(rowIndex) > 0 && document.getElementsByName("mfrCode")[rowIndex].value!= '')
            var tab = document.getElementById("techs");
            var iRowCount = tab.getElementsByTagName('tr').length;
            //alert("len:"+iRowCount);
            rowCount = iRowCount;
            var newrow = tab.insertRow(rowCount);
            sno = rowCount;
            rowIndex = rowCount;
            //alert("rowIndex:"+rowIndex);
            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > " + sno + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell1 = "<td class='text1' height='25' ><select class='form-control' style='width:240px;height:40px;' name='technicianId' id='technicianId" + rowIndex + "'   onchange='getAllotedJCTech(this.value)' ><option value='0'>--select--</option><c:if test = "${technicians != null}" ><c:forEach items="${technicians}" var="mfr"><option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" /></c:forEach ></c:if> </select></td>";
                    cell.setAttribute("className", styl);
            cell.innerHTML = cell1;

            cell = newrow.insertCell(2);
            var cell2 = "<td class='text1' height='25'><input name='estimatedHrs' id='estimatedHrs" + rowIndex + "'  class='form-control' style='width:140px;height:40px;'  class='form-control' onKeyPress='return onKeyPressBlockCharacters(event);'  onchange='getOthers(" + rowIndex + ")'  type='text'></td>";

            cell.setAttribute("className", styl);
            cell.innerHTML = cell2;

            cell = newrow.insertCell(3);
            cell2 = "<td class='text1' height='25'><input name='actualHrs' id='actualHrs" + rowIndex + "'  class='form-control' style='width:140px;height:40px;' onchange='getOthers(" + rowIndex + ")' onKeyPress='return onKeyPressBlockCharacters(event);' type='text'></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell2;

            var itemCode = document.getElementsByName("technicianId");
            itemCode[rowCount - 1].focus();

            rowIndex++;
            rowCount++;

        }

        function submitPage() {
            document.workOrderForm.action = '/throttle/saveJobCardTechnicians.do';
            document.workOrderForm.submit();
        }



        function getAllotedJCTech(technicianId) {
//            alert("12314-===="+technicianId);
            var url = './getAllotedJCTech.do';
            if (technicianId != 0) {
                $.ajax({
                    url: url,
                    data: {techId: technicianId
                    },
                    type: "GET",
                    dataType: 'json',
                    success: function (data) {
//                    alert(data.length);
                        if (data.length == 0) {
                            $("#displayData").hide();
                        } else {
                            $("#displayData tbody").html("");
                            $.each(data, function (i, data) {
                                var tblRow = "<tr>" + "<td>" + data.jobCard + "</td>"
                                        + "<td>" + data.regNo + "</td>"
                                        + "<td>" + data.probName + "</td>"
                                        + "<td>" + data.hrs + "</td>"
                                        + "<td>" + data.ScheduleDate + "</td>"
                                        + "<td>" + data.status + "</td>" + "</tr>"
                                $(tblRow).appendTo("#displayData tbody");
                            });
                            $("#displayData").show();
                        }
                    },
                    error: function (xhr, status, error) {
                    }
                });
            } else {
                $("#displayData").hide();
            }
        }
    </script>

</head>








<body >
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Assign Technician</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
                <li><a href="general-forms.html">Job Card</a></li>
                <li class="active">Assign Technician</li>
            </ol>
        </div>
    </div>
    <form name="workOrderForm" method="post">

        <%--<%@ include file="/content/common/path.jsp" %>--%>

        <!-- pointer table -->

        <!-- message table -->

        <%@ include file="/content/common/message.jsp"%>

        <input type="hidden" name="jobCardId" value='<%=request.getAttribute("jobCardId")%>'>
        <input type="hidden" name="probId" value='<%=request.getAttribute("probId")%>'>
        <table   class="table table-info mb30 table-hover" id="techs" style="margin-left: 10px;width:80%" >
            <!--<table align="center" id="techs" width="90%" border="0" cellspacing="0" cellpadding="0" class="border">-->
            <thead> <tr>
                    <th>Sno
                    </th>
                    <th>Technician
                    </th>
                    <th>EstimatedHours
                    </th>
                    <th>ActualHours
                    </th>
                </tr></thead>

            <%int index=0;%>
            <c:if test = "${existingTechnicians != null}" >
                <c:forEach items="${existingTechnicians}" var="temp">
                    <%index++;%>
                    <tr>
                        <td ><%=index%></td>
                        <td>
                            <select name="technicianId" class="form-control" style="width:240px;height:40px;" onblur="getAllotedJCTech(this.value)">
                                <c:if test = "${technicians != null}" >
                                    <option   value="0">--select--</option>
                                    <c:forEach items="${technicians}" var="mfr">
                                        <c:choose>
                                            <c:when test="${temp.technicianId==mfr.empId}">
                                                <option selected  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                            </c:when>
                                            <c:otherwise>
                                                <option  value='<c:out value="${mfr.empId}" />'><c:out value="${mfr.empName}" />
                                            </c:otherwise>
                                        </c:choose>

                                    </c:forEach >
                                </c:if>
                            </select>
                        </td>
                        <td><input type="text" name="estimatedHrs" class="form-control" style="width:140px;height:40px;"  value='<c:out value="${temp.estimatedHrs}" />'></td>
                        <td><input type="text" name="actualHrs" class="form-control" style="width:140px;height:40px;"   value='<c:out value="${temp.actualHrs}" />'></td>
                    <tr>
                </c:forEach>
            </c:if>


        </table>


        <br>
        <center>
            <input type="button" class="btn btn-success" name="add row" value="add row" onclick="addRow();"> &nbsp;&nbsp;<input type="button" class="btn btn-success" name="save" value="save & close" onclick="submitPage();" >
        </center>
        <br>
        <table class="table table-info mb30 table-hover" id="displayData" style="display:none" border="1">
            <thead>
            <th colspan="6" align="center">Techinician Alloted JC List</th>
            <tr>
                <th>JobCardNo
                </th>
                <th>Reg No
                </th>
                <th>Problem Name
                </th>
                <th>Estimate Hrs
                </th>
                <th>Scheduled Date
                </th>
                <th>Status
                </th>
            </tr></thead>
            <tbody></tbody>
        </table>
        <%@ include file="/content/common/NewDesign/settings.jsp" %>

        <!--</html>-->
