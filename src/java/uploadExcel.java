/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author root
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import java.util.ArrayList;
import java.io.File;
import java.io.IOException;
import java.util.Locale;
import jxl.read.biff.BiffException;
import ets.domain.report.business.ReportBP;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.FPLogUtils;


public class uploadExcel {

    public static void main(String a[]) {

        String path = "//root//Desktop//demo23.xls";
        new uploadExcel().updateStoreItems(path);
    }

    public void updateStoreItems(String path) {
        try {
            int status=0;
            ReportBP reportBP = new ReportBP();
            ArrayList client = new ArrayList();
            WorkbookSettings ws = new WorkbookSettings();
            ws.setLocale(new Locale("en", "EN"));
            Workbook workbook;

            workbook = Workbook.getWorkbook(new File(path), ws);

            Sheet s = workbook.getSheet(0);
            for (int j = 1; j < s.getRows(); j++) {
                for (int i = 0; i < s.getColumns(); i++) {
                    Cell val = s.getCell(i, j);
                    client.add(val.getContents());
                    System.out.println("Values-->"+val.getContents());
                }
            }
            System.out.println("Stock Report Size-->"+client.size());
        
        } catch (IOException io) {
            io.printStackTrace();
        } catch (BiffException ex) {
            Logger.getLogger(uploadExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
         catch (FPRuntimeException exception) {

            FPLogUtils.fpErrorLog("Run time exception --> " + exception);

        } 
    }
}
