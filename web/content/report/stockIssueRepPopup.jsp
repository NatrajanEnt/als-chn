<%-- 
    Document   : stockIssueRepPopup
    Created on : 29 Dec, 2018, 5:27:35 PM
    Author     : ramprasath
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<!--<html>-->
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>

    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
    <script src="/throttle/js/jquery.ui.core.js"></script>

</head>
<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            format: "dd-mm-yyyy",
            autoclose: true,
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        //	alert("cv");
        $(".datepicker").datepicker({
            format: "dd-mm-yyyy",
            autoclose: true
        });

    });


</script>

<script language="javascript">
    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }

    var httpReq;
    var temp = "";
    function ajaxData()
    {
        if (document.IssueReport.mfrId.value != '0') {
            var url = "/throttle/getModels1.do?mfrId=" + document.IssueReport.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function () {
                processAjax();
            };
            httpReq.send(null);
        }
    }
    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                setOptions(temp, document.IssueReport.modelId);
                if ('<%= request.getAttribute("modelId")%>' != 'null') {
                    document.IssueReport.modelId.value = <%= request.getAttribute("modelId")%>;
                }
            } else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }



    function submitPage() {
        var Item = document.getElementById("Item").value;
        var chek = validation();
        if (chek == 'true') {
            document.IssueReport.action = '/throttle/stockIssueReport.do?value=0&item=' + Item;
            document.IssueReport.submit();
        }
    }
    function validation() {
        if (document.IssueReport.companyId.value == 0) {
            alert("Please Select Data for Company Name");
            document.IssueReport.companyId.focus();
            return 'false';
        } else if (textValidation(document.IssueReport.fromDate, 'From Date')) {
            return 'false';
        } else if (textValidation(document.IssueReport.toDate, 'TO Date')) {
            return'false';
        }
        return 'true';
    }

    function setValues() {
        var poId = '<%=request.getAttribute("poId")%>';
        if ('<%=request.getAttribute("companyId")%>' != 'null') {
            document.IssueReport.companyId.value = '<%=request.getAttribute("companyId")%>';
            document.IssueReport.fromDate.value = '<%=request.getAttribute("fromDate")%>';
            document.IssueReport.toDate.value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("categoryId")%>' != 'null') {
            document.IssueReport.categoryId.value = '<%=request.getAttribute("categoryId")%>';
        }
        if ('<%=request.getAttribute("mfrId")%>' != 'null') {
            document.IssueReport.mfrId.value = '<%=request.getAttribute("mfrId")%>';
        }
        if ('<%=request.getAttribute("modelId")%>' != 'null') {
            ajaxData();
            document.IssueReport.modelId.value = '<%=request.getAttribute("modelId")%>';
        }
        if ('<%=request.getAttribute("mfrCode")%>' != 'null') {
            document.IssueReport.mfrCode.value = '<%=request.getAttribute("mfrCode")%>';
        }
        if ('<%=request.getAttribute("paplCode")%>' != 'null') {
            document.IssueReport.paplCode.value = '<%=request.getAttribute("paplCode")%>';
        }
        if ('<%=request.getAttribute("itemName")%>' != 'null') {
            document.IssueReport.itemName.value = '<%=request.getAttribute("itemName")%>';
        }
        if ('<%=request.getAttribute("regNo")%>' != 'null') {
            document.IssueReport.regNo.value = '<%=request.getAttribute("regNo")%>';
        }
        if ('<%=request.getAttribute("rcWorkorderId")%>' != 'null') {
            document.IssueReport.rcWorkorderId.value = '<%=request.getAttribute("rcWorkorderId")%>';
        }
        if ('<%=request.getAttribute("counterId")%>' != 'null') {
            document.IssueReport.counterId.value = '<%=request.getAttribute("counterId")%>';
        }

    }
    function getItemNames() {
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"), new ListSuggestions("itemName", "/throttle/handleItemSuggestions.do?"));

    }
    function getVehicleNos() {
//onkeypress='getList(sno,this.id)'
        var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
    }

    function toexcel() {

        document.IssueReport.action = '/throttle/handleStockIssueExcelRepNew.do';
        document.IssueReport.submit();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsstores.label.Stock Issue Report" text="Stock Issue Report"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsstores.label.FMSStores" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsstores.label.Stock Issue Report" text="Stock Issue Report"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">    

            <body onload="setValues();
                    getItemNames();
                    getVehicleNos();">
                <form name="IssueReport" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <c:set var="totalValue" value="0"/>
                    <c:set var="totalProfit" value="0"/>
                    <c:set var="totalNettProfit" value="0"/>
                    <c:set var="totalUnbilledValue" value="0"/>
                    <c:set var="totalUnbilledProfit" value="0"/>
                    <c:set var="totalUnbilledNettProfit" value="0"/>
                    <c:set var="totalQty" value="0"/>
                    <c:if test = "${stokIssueList != null}" >


                        <% int index = 0;
                            String type = "Billed";%>
                        <c:forEach items="${stokIssueList}" var="issue">

                            <c:if test = "${issue.type == 'unbilled'}" >
                                <%
                                    if (type.equals("Billed")) {
                                        index = 0;
                                        type = "UnBilled";
                                %></table><%
                                    }
                                %>

                            </c:if>
                            <%if (index == 0) {%>
                            <table class="table table-info mb30 table-hover">
                                <thead>
                                    <!--<table width="1200" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">-->
                                    <tr >
                                        <th >Sno</td>
                                        <th  >MFR</td>
                                        <th  >Model</td>
                                        <th  >REG NO</td>
                                        <th  >JOBCARD NO</td>
                                        <th  >MRSNo </td>
                                        <th  >MANUALMRSNo </td>
                                        <th  >MRSDATE</td>
                                        <th  >IssueDATE</td>
                                        <th >Company CODE</td>
                                        <th  >ITEM NAME</td>
                                        <th >Tech Name</td>
                                        <th >User</td>
                                        <th  >Iss Qty</td>
                                        <th  >Ret Qty</td>
                                        <th  >Net Qty</td>
                                        <th  >BuyPrice</td>

                                    </tr>
                                </thead>

                                <%
                                    }
                                    String classText = "";
                                    int oddEven = index % 2;
                                    if (oddEven > 0) {
                                        classText = "text2";
                                    } else {
                                        classText = "text1";
                                    }
                                %>

                                <tr>
                                    <%--//For Generate Excel--%>
                                <input type="hidden" name="sno" value="<%=index + 1%>">
                                <input type="hidden" name="mfrName" value="<c:out value="${issue.mfrName}"/>">
                                <input type="hidden" name="regNos" value="<c:out value="${issue.regNo}" />">
                                <input type="hidden" name="jobCardId" value="<c:out value="${issue.jobCardId}"/>">
                                <input type="hidden" name="rcWorkorderIds" value="<c:out value="${issue.rcWorkorderId}"/>">
        <!--                        <input type="hidden" name="counterIds" value="<c:out value="${issue.counterId}"/>">-->
                                <input type="hidden" name="counterIds" value="-">
                                <input type="hidden" name="mrsId" value="<c:out value="${issue.mrsId}"/>">
                                <input type="hidden" name="manualMrsNo" value="<c:out value="${issue.manualMrsNo}"/>">
                                <input type="hidden" name="createdDate" value="<c:out value="${issue.createdDate}"/>">
                                <input type="hidden" name="manualMrsDate" value="<c:out value="${issue.manualMrsDate}"/>">
                                <input type="hidden" name="paplCodes" value="<c:out value="${issue.paplCode}" />">
                                <input type="hidden" name="itemNames" value="<c:out value="${issue.itemName}"/>">
                                <input type="hidden" name="categoryName" value="<c:out value="${issue.categoryName}"/>">
                                <input type="hidden" name="itemType" value="<c:out value="${issue.itemType}"/>">
                                <input type="hidden" name="itemPrice" value="<c:out value="${issue.itemPrice}"/>">
                                <input type="hidden" name="itemQty" value="<c:out value="${issue.itemQty}"/>">

                                <td  ><%=index + 1%></td>
                                <td  ><c:out value="${issue.mfrName}"/></td>
                                <td  ><c:out value="${issue.modelName}"/></td>
                                <td ><c:out value="${issue.regNo}" /></td>
                                <td  ><c:out value="${issue.jobCardId}"/></td>
                                <td  ><c:out value="${issue.mrsId}"/></td>
                                <td  ><c:out value="${issue.manualMrsNo}"/></td>
                                <td  ><c:out value="${issue.manualMrsDate}"/></td>
                                <td  ><c:out value="${issue.issueDate}"/></td>
                                <td ><c:out value="${issue.paplCode}" /></td>
                                <td  ><c:out value="${issue.itemName}"/></td>

                                <td  ><c:out value="${issue.empName}"/></td>
                                <td  ><c:out value="${issue.user}"/></td>
                                <td   align="right" ><c:out value="${issue.issueQty}"/></td>
                                <td   align="right" ><c:out value="${issue.retQty}"/></td>
                                <td   align="right" ><c:out value="${issue.itemQty}"/></td>
                                <td   align="right" ><c:out value="${issue.buyPrice}"/></td>



                                <c:if test = "${issue.type == 'billed'}" >
                                    <c:set var="totalValue" value="${(issue.sellPrice * issue.itemQty) + totalValue}" />
                                    <c:set var="totalProfit" value="${issue.profit + totalProfit}" />
                                    <c:set var="totalNettProfit" value="${issue.nettProfit + totalNettProfit}" />
                                </c:if>
                                <c:if test = "${issue.type == 'unbilled'}" >
                                    <c:set var="totalUnbilledValue" value="${(issue.sellPrice * issue.itemQty) + totalUnbilledValue}" />
                                    <c:set var="totalUnbilledProfit" value="${issue.profit + totalUnbilledProfit}" />
                                    <c:set var="totalUnbilledNettProfit" value="${issue.nettProfit + totalUnbilledNettProfit}" />
                                </c:if>
                                <c:set var="totalQty" value="${issue.itemQty + totalQty}" />
                                </tr>
                                <% index++;%>
                        </c:forEach>

                        <br>
                    </c:if>
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


