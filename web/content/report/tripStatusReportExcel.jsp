<%--
    Document   : TripStatusReport
    Created on : jul 31, 2020, 1:48:05 PM
    Author     : hp
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
    <form name="tripSheet" method="post">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripStatusReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

        <c:if test="${tripStatusReport != null}">
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr ><th colspan="18"></th>
                    <th colspan="10" style="text-align:center;border-bottom-width: 2px;"><font size="4">Export</font></th>
                    <th colspan="6" style="text-align:center;border-bottom-width: 2px;"><font size="4">Import</font></th></tr>
                    <tr height="50">
                        <th > Sno</th>
                        <th >Axle Type</th>
                        <th >Moff OR Loc</th>
                        <th >MoveDate</th>
                        <th >StuffedDate</th>
                        <th >MovementType</th>
                        <th >ConsignmentOrderNo</th>
                        <th >CustomerName</th>
                        <th >TripCode </th>
                        <th>ContainerNo</th>
                        <th>ContainerSize</th>
                        <th>Wt</th>
                        <th>Routeinfo</th>
                        <th>Transporter Name</th>
                        <th>Liner</th>
                        <th> VehicleNo</th>
                        <th> TripStart </th>
                        <th> TripEnd </th>
                        <th> EmptyIn </th>
                        <th> EmptyOut </th>
                        <th> FactoryIn </th>
                        <th> FactoryOut </th>
<!--                        <th> FactoryOut with Clearance </th>
                        <th> FactoryOut without Clearance SB </th>
                        <th> FactoryOut with RFID Seal BR CheckList </th>
                        <th> FactoryOut with Dummy Seal </th>-->
                        <th> Waiting For SB </th>
                        <th> Waiting For BR and CheckList </th>
                        <th> ClearanceIn </th>
                        <th> ClearanceOut </th>
<!--                        <th> Clearanceat Cfs CustomsSeal </th>
                        <th> Clearanceat ICD Customs Seal </th>-->
                        <th> PortIn </th>
                        <th> PortOut </th>
                        <th> CFSPortIn </th>
                        <th> CFSPortOut </th>
                        <th> Import FactoryIn </th>
                        <th> ImportFactoryOut </th>
                        <th> ImportEmptyIn </th>
                        <th> Import EmptyOut </th>
                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${tripStatusReport}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="40">
                             <td class="<%=className%>" width="40" align="left"><%=index%></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.axleType}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.mofforLoc}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.moveDate}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.stuffedDate}"/></td>
                            <td class="<%=className%>" align="left" style="width:110px;"><c:out value="${tripDetails.movementType}"/></td>
                            <td class="<%=className%>" align="left" style="width:120px;"><c:out value="${tripDetails.consignmentOrderNo}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.customerName}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.tripCode}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.containerNo}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.containerSize}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.wt}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.routeinfo}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.transporterName}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.liner}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.vehicleNo}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.tripStart}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.tripEnd}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.emptyIn}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.emptyOut}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.factoryIn}"/></td>
                            <td class="<%=className%>" width="100" align="left">
                                <c:if test="${tripDetails.factoryOut !=null }"> 
                                    <c:out value="${tripDetails.factoryOut}"/>
                                </c:if>
                                <c:if test="${tripDetails.factoryOutwithClearance !=null }"> 
                                    <c:out value="${tripDetails.factoryOutwithClearance}"/>
                                </c:if>
                                <c:if test="${tripDetails.factoryOutwithoutClearanceSB !=null }"> 
                                    <c:out value="${tripDetails.factoryOutwithoutClearanceSB}"/>
                                </c:if>
                                <c:if test="${tripDetails.factoryOutwithRFIDSealBRCheckList !=null }"> 
                                    <c:out value="${tripDetails.factoryOutwithRFIDSealBRCheckList}"/>
                                </c:if>
                                <c:if test="${tripDetails.factoryOutwithDummySeal !=null }"> 
                                    <c:out value="${tripDetails.factoryOutwithDummySeal}"/>
                                </c:if>
                            </td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.waitingForSB}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.waitingForBRandCheckList}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.clearanceIn}"/></td>
                            <td class="<%=className%>" width="100" align="left">
                                <c:if test="${tripDetails.clearanceOut !=null }"> 
                                    <c:out value="${tripDetails.clearanceOut}"/>
                                </c:if>
                                <c:if test="${tripDetails.clearanceatCfsCustomsSeal !=null }"> 
                                    <c:out value="${tripDetails.clearanceatCfsCustomsSeal}"/>
                                </c:if>
                                <c:if test="${tripDetails.clearanceatICDCustomsSeal !=null }"> 
                                    <c:out value="${tripDetails.clearanceatICDCustomsSeal}"/>
                                </c:if>
                            </td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.portIn}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.portOut}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.cFSPortIn}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.cFSPortOut}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.impFactoryIn}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.impFactoryOut}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.impEmptyIn}"/></td>
                            <td class="<%=className%>" width="100" align="left"><c:out value="${tripDetails.impEmptyOut}"/></td>
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
