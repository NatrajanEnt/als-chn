<%--
    Document   : viewclosedtrip
    Created on : Dec 6, 2013, 4:14:16 PM
    Author     : srinientitle
--%>

<%@page import="ets.domain.finance.business.FinanceTO"%>
<%@page import="java.util.ArrayList"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!--<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">-->
<script type="text/javascript" src="/throttle/js/suest"></script>
<script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
<script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });


    });

</script>

<script type="text/javascript">


    function submitPage() {
         var statusId = document.getElementsByName('statusId');
         var activeInd = document.getElementsByName('activeInd');
        var booleanResult=false;
        for(var i=0;i<activeInd.length;i++){
         if(statusId[i].checked){
            booleanResult=true;
            break;
         }
        }
        if(booleanResult==false){
        alert("please choose checkbox")
        }
        else{
                document.enter.action = '/throttle/handleLocationApproval.do?param=update';
                document.enter.submit();
        }
    }

function setValue(){
    var statusId = document.getElementsByName('statusId'); 
    var activeInd = document.getElementsByName('activeInd'); 
    for(var i=0;i<activeInd.length;i++){
        if(statusId[i].checked){
        activeInd[i].value=1;
        
//        alert(statusId[i].value);
    }
    else{
        activeInd[i].value=0;
    }
}
}
function checkMappingId(val){
    var mappingId = document.getElementsByName('mappingId'); 
//    alert(mappingId[val-1].value);
    if(mappingId[val-1].value==null || mappingId[val-1].value==""||mappingId[val-1].value=="0"){
        alert("please enter valid mapping code");
        mappingId[val-1].focus();
        return false;
        }
}

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });


</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Location Approval</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Location Approval</a></li>
            <li class="active">Location Approval</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="enter" action=""  method="post">
                <%@ include file="/content/common/message.jsp" %>
                <c:if test = "${locationApprovedetails != null}" >
                    <table class="table table-info mb30 table-hover"  id="table" style="width:100%">
                        <thead>
                        <tr>
                            <th>S No</th>
                            <th>City Name</th>
                            <th>Location Type</th>
                            <th>Mapping Id</th>
                            <th>Status</th>
                        </tr>
                            </thead>
                        <tbody>
                            <% int sno = 0;%>
                            

                                <%
                                                    sno++;
                                                   String className = "text1";
                                                   if ((sno % 1) == 0) {
                                                       className = "text1";
                                                   } else {
                                                       className = "text2";
                                                   }
                                %>
                                <c:forEach items="${locationApprovedetails}" var="la">
                                    <tr>
                                        <td    height="30"> <%= sno%></td>
                                        <td   align="left" ><c:out value="${la.cityName }"/>
                                            <input type="hidden" name="cityId" id='cityId<%= sno%>' value="<c:out value="${la.cityId}"/>"/>
                                        </td>
                                         <td align="left" ><c:out value="${la.typeName}"/>
                                        </td>
                                        <td   align="left" >
                                            <input type="text" name="mappingId" id='mappingId<%= sno%>' value="<c:out value="${la.mappingId}"/>" onchange="checkMappingId(<%= sno%>);"/>
                                        </td>
                                        <td   align="left" >
                                            <input type="checkbox" name="statusId" id='statusId<%= sno%>' value="" onClick="setValue();"/>
                                            <input type="hidden" id="activeInd<%=sno%>" name="activeInd" value="0"/>
                                        </td>
                                    </tr>
                                    <%sno++;%>
                                </c:forEach>
                                  </c:if>  <tr>
                                        <td colspan="4"><center>
                                               <input type="button" class="btn btn-success"  value="update" style="width:100px;height:35px;" onClick="submitPage();">
                                        </center>
                                                   </td>
                                    </tr>
                                     </tbody>
                    </table>
                                
                     <center>
                    </center>
                            
                         
                            
                            
                    

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <script type="text/javascript">


                        function setActiveInd(sno) {
                            if (document.getElementById("contractRateId" + sno).checked == true) {
                                document.getElementById("activeInd" + sno).value = 'Y';
                            } else {
                                document.getElementById("activeInd" + sno).value = 'N';
                            }
                        }



                    </script>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>