/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.fmsOperation.web;

import com.ibatis.sqlmap.client.SqlMapClient;
import java.text.DecimalFormat;
import ets.domain.programmingfree.excelexamples.*;
import java.text.NumberFormat;
import jxl.*;
import jxl.WorkbookSettings;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
//import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;

import ets.arch.util.SendMail;
import ets.arch.business.PaginationHelper;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.mrs.business.MrsBP;
import ets.domain.mrs.web.MrsCommand;
import ets.domain.fmsOperation.business.FMSOperationBP;
import ets.domain.secondaryOperation.business.SecondaryOperationBP;
import ets.domain.customer.business.CustomerBP;
import ets.domain.vehicle.business.VehicleBP;
import ets.domain.fmsOperation.business.FMSOperationTO;
import ets.domain.security.business.SecurityBP;
import ets.domain.service.business.ServiceBP;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import java.text.SimpleDateFormat;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import java.io.*;
import java.util.*;
import java.text.DateFormat;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.json.JSONArray;
import org.json.JSONObject;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.company.business.CompanyBP;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;
import ets.domain.vehicle.business.VehicleTO;
import java.net.UnknownHostException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import ets.domain.util.ThrottleConstants;

/**
 *
 * @author vijay
 */
public class FMSOperationController extends BaseController {

    FMSOperationBP fmsOperationBP;
    LoginBP loginBP;
    SecurityBP securityBP;
    ServiceBP serviceBP;
    MrsBP mrsBP;
    FMSOperationCommand fmsOperationCommand;
    VehicleBP vehicleBP;
    CustomerBP customerBP;
    TripBP tripBP;
    CompanyBP companyBP;
    SecondaryOperationBP secondaryOperationBP;

    public CompanyBP getCompanyBP() {
        return companyBP;
    }

    public void setCompanyBP(CompanyBP companyBP) {
        this.companyBP = companyBP;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public CustomerBP getCustomerBP() {
        return customerBP;
    }

    public void setCustomerBP(CustomerBP customerBP) {
        this.customerBP = customerBP;
    }

    public MrsBP getMrsBP() {
        return mrsBP;
    }

    public void setMrsBP(MrsBP mrsBP) {
        this.mrsBP = mrsBP;
    }

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public ServiceBP getServiceBP() {
        return serviceBP;
    }

    public void setServiceBP(ServiceBP serviceBP) {
        this.serviceBP = serviceBP;

    }

    public SecurityBP getSecurityBP() {
        return securityBP;
    }

    public void setSecurityBP(SecurityBP securityBP) {
        this.securityBP = securityBP;
    }

    public FMSOperationBP getFmsOperationBP() {
        return fmsOperationBP;
    }

    public void setFmsOperationBP(FMSOperationBP fmsOperationBP) {
        this.fmsOperationBP = fmsOperationBP;
    }

    public FMSOperationCommand getFmsOperationCommand() {
        return fmsOperationCommand;
    }

    public void setFmsOperationCommand(FMSOperationCommand fmsOperationCommand) {
        this.fmsOperationCommand = fmsOperationCommand;
    }

    public SecondaryOperationBP getSecondaryOperationBP() {
        return secondaryOperationBP;
    }

    public void setSecondaryOperationBP(SecondaryOperationBP secondaryOperationBP) {
        this.secondaryOperationBP = secondaryOperationBP;
    }

    protected void bind(HttpServletRequest request, Object command) throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        System.out.println("request.getRequestURI() = " + request.getRequestURI());
        HttpSession session = request.getSession();
//        FacesContext context = FacesContext.getCurrentInstance();
//       HttpServletResponse response = (HttpServletResponse) context.getExternalContext().getResponse();
//        if (request.getSession().isNew()) {
//        System.out.println("request.getSession().isNew() = " + request.getSession().isNew());
//        }
//        else {
//        System.out.println("request.getSession().isold() = " + request.getSession().isNew());
        int userId = (Integer) session.getAttribute("userId");
//
        System.out.println("userId" + userId);
//        System.out.println("loginRecordId" + loginRecordId);
        String getUserFunctionsActive = "";
        String uri = request.getRequestURI();
//        getUserFunctionsActive = loginBP.getUserFunctionsActive(userId, uri);
        if (getUserFunctionsActive != "" && getUserFunctionsActive != null) {
            String temp[] = getUserFunctionsActive.split("~");
            System.out.println("function Id" + temp[0]);
            System.out.println("function Name  " + temp[0]);
            int status1 = 0;
            int functionId = Integer.parseInt(temp[0]);
            String functionName = temp[1];
//            status1 = loginBP.insertUserFunctionAccessDetails(functionId, functionName, userId, loginRecordId);
        }
//        }
        binder.closeNoCatch();
        initialize(request);
    }

    public ModelAndView searchBWBill(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Recevice Body Works Bill";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> Recevice Body Works Bill";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/receviceBWBill.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Create")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int woNo = Integer.parseInt(request.getParameter("mrsJobCardNumber"));
            int jcId = Integer.parseInt(request.getParameter("jcId"));
            ArrayList jobCardlist = new ArrayList();
            ArrayList vendorList = new ArrayList();
            vendorList = fmsOperationBP.getBodyBillList(woNo);
            jobCardlist = fmsOperationBP.getContractJobcards();
            //                jobCardlist = fmsOperationBP.getJobCardList();
            request.setAttribute("jobCardlist", jobCardlist);
            request.setAttribute("vendorList", vendorList);
            request.setAttribute("woNo", woNo);
            request.setAttribute("jcId", jcId);

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Create jobcard page --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView serviceEstimate(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Work Order Approval";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> Work Order Approval";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/serviceEstimate.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            ArrayList MfrList = new ArrayList();

            MfrList = fmsOperationBP.processGetMfrList();

            request.setAttribute("MfrList", MfrList);
            //            if (!loginBP.checkAuthorisation(userFunctions, "WorkOrder-ScheduleDate")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert ScheduleDate --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView createJobcardPage(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Create JobCard";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> Create JobCard ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/createJobcard.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Create")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            // setLocale(request, response);
            System.out.println("Locale called");
            System.out.println("language:" + request.getAttribute("language"));
            ArrayList serviceTypes = new ArrayList();
            ArrayList servicePoints = new ArrayList();
            ArrayList sections = new ArrayList();
            ArrayList serviceVendors = new ArrayList();
            serviceTypes = fmsOperationBP.getServiceTypes();
            servicePoints = fmsOperationBP.getServicePoints();
            sections = serviceBP.getSections();
            serviceVendors = fmsOperationBP.getServiceVendors();

            String vehicleId = request.getParameter("vehicleId");
            if (vehicleId != null && !"".equals(vehicleId)) {
                String regNo = fmsOperationBP.getVehicleRegNo(vehicleId);
                request.setAttribute("regNo", regNo);
            }

            ArrayList vehicleRegNoForJobCard = new ArrayList();
            vehicleRegNoForJobCard = fmsOperationBP.getVehicleRegNoForJobCard("");
            ArrayList trailerNos = new ArrayList();
            trailerNos = fmsOperationBP.trailerNos();
            request.setAttribute("trailerNos", trailerNos);
            request.setAttribute("vehicleRegNoForJobCard", vehicleRegNoForJobCard);
            request.setAttribute("sections", sections);
            request.setAttribute("serviceTypes", serviceTypes);
            request.setAttribute("servicePoints", servicePoints);
            request.setAttribute("serviceVendors", serviceVendors);

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Create jobcard page --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView insertConJobCardWorkOrder(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Add Direct WorkOrder";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Render Service >> Add Direct WorkOrder ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        FMSOperationTO operationTO = new FMSOperationTO();
        ModelAndView mv = null;
        int jobcardId = 0;
        try {

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ViewPage")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            path = "content/RenderService/supervisorView.jsp";
            //               path = "content/nonPAPLService/WMpermission.jsp";
            int status = 0;
            int userId = (Integer) session.getAttribute("userId");
            operationTO.setUserId(userId);
            String regno = request.getParameter("regno");
            operationTO.setRegno(regno);

            String trailerNo = request.getParameter("trailerNo");
            operationTO.setTrailerNo(trailerNo);
            String trailerId = request.getParameter("trailerId");
            operationTO.setTrailerId(trailerId);
            String jobCardFor = request.getParameter("jobCardFor");
            operationTO.setJobCardFor(jobCardFor);

            String jobCardType1 = request.getParameter("jobCardTypeNew");
            operationTO.setJobCardTypeNew(jobCardType1);
            //////System.out.println("operationTO.getRegno() 1" + operationTO.getRegno());
            int compId = Integer.parseInt(request.getParameter("compId"));
            operationTO.setCompId(compId);
            int priority = Integer.parseInt(request.getParameter("priority"));
            operationTO.setPriority(priority);
            String reqDate = request.getParameter("reqDate");
            operationTO.setReqDate(reqDate);
            String dateOfIssue = request.getParameter("dateOfIssue");
            operationTO.setDateOfIssue(dateOfIssue);
            String hour = request.getParameter("hour");

            operationTO.setHour(hour);
            String minute = request.getParameter("minute");

            operationTO.setMinute(minute);
            String sec = request.getParameter("sec");

            operationTO.setSec(sec);
            int kmReading = Integer.parseInt(request.getParameter("kmReading"));
            operationTO.setKmReading(kmReading);
            int hourMeter = Integer.parseInt(request.getParameter("hourMeter"));
            operationTO.setHourMeter(hourMeter);
            int serviceTypeId = Integer.parseInt(request.getParameter("serviceType"));
            operationTO.setServicetypeId(serviceTypeId);
            int serviceLocation = Integer.parseInt(request.getParameter("serviceLocation"));
            String serviceLocn = request.getParameter("serviceLocn");
            operationTO.setServiceLocationId(serviceLocation);
            String driverName = request.getParameter("driverName");
            operationTO.setDriverName(driverName);
            String remark = request.getParameter("remark");
            operationTO.setRemarks(remark);
            String cust = request.getParameter("cust");
            operationTO.setCust(cust);
            operationTO.setTotalKm("" + (Integer.parseInt(request.getParameter("totalKm")) + (kmReading - Integer.parseInt(request.getParameter("actualKm")))));
            // //////System.out.println("vehicleId" + request.getParameter("vehicleId"));
            //int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
            //operationTO.setVehicleId(vehicleId);
            String mfrName = request.getParameter("mfrName");
            operationTO.setMfrName(mfrName);
            String eCost = request.getParameter("eCost");
            operationTO.setTotalAmount(Float.parseFloat(eCost));
            String modName = request.getParameter("modName");
            operationTO.setModelName(modName);
            String useType = request.getParameter("useType");
            String bayNo = request.getParameter("bayNo");
            String serviceVendorId = request.getParameter("serviceVendor");
            operationTO.setServiceVendor(serviceVendorId);
            operationTO.setUsageName(useType);
            operationTO.setBayNo(bayNo);
            operationTO.setServiceLocation(serviceLocn);
            String jobCardType = request.getParameter("jobCardType");
            String workOrderId = request.getParameter("workOrderId");
            operationTO.setWorkOrderId(Integer.parseInt(workOrderId));
            operationTO.setJobCardType(jobCardType);

            jobcardId = fmsOperationBP.insertJobCardWorkOrder(operationTO);
            if ("1".equals(jobCardFor)) {
                vehicleBP.vehicleKmUpdate(fmsOperationBP.getVehicleId(regno), String.valueOf(kmReading), String.valueOf(hourMeter), userId);
            }

            if (jobcardId != 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Job card created successfully");
                String[] hiddenElemenet1 = request.getParameterValues("hiddenElemenet1");
                String[] hiddenElemenet2 = request.getParameterValues("hiddenElemenet2");
                String[] hiddenElemenet3 = request.getParameterValues("hiddenElemenet3");
                String[] hiddenElemenet4 = request.getParameterValues("hiddenElemenet4");

                String[] oldProbId = request.getParameterValues("oldProbId");
                String[] oldSymptoms = request.getParameterValues("oldSymptoms");
                String[] oldSeverity = request.getParameterValues("oldSeverity");
                String[] oldComp = request.getParameterValues("oldComp");
                String[] oldJobcardIds = request.getParameterValues("oldJobcardId");

                int probId = 0;
                int Severity = 0;
                int secId = 0;
                int oldJobcardId = 0;
                String symptoms = "";
                int temp = 0;
                if (hiddenElemenet1 != null) {
                    for (int i = 0; i < hiddenElemenet1.length; i++) {
                        secId = Integer.parseInt(request.getParameter(hiddenElemenet1[i]));
                        probId = Integer.parseInt(request.getParameter(hiddenElemenet2[i]));
                        symptoms = request.getParameter(hiddenElemenet3[i]);
                        Severity = Integer.parseInt(request.getParameter(hiddenElemenet4[i]));
                        status = fmsOperationBP.insertJobCardProblems(secId, probId, symptoms, Severity, jobcardId, temp, 0);
                    }
                }

                if (oldComp != null) {
                    for (int i = 0; i < oldComp.length; i++) {
                        System.out.println("oldComp[i]==========" + oldComp[i]);
                        System.out.println("oldProbId[i]==========" + oldProbId[i]);
                        System.out.println("oldSymptoms[i]==========" + oldSymptoms[i]);
                        System.out.println("oldSeverity[i]==========" + oldSeverity[i]);
                        if ("1".equals(oldComp[i])) {
                            secId = 0;
                            probId = Integer.parseInt(oldProbId[i]);
                            oldJobcardId = Integer.parseInt(oldJobcardIds[i]);
                            symptoms = oldSymptoms[i];
                            Severity = Integer.parseInt(oldSeverity[i]);
                            status = fmsOperationBP.insertJobCardProblems(secId, probId, symptoms, Severity, jobcardId, 1, oldJobcardId);
                        }
                    }
                }
            }

            String to = "";
            String cc = "";
            String smtp = "";
            int emailPort = 0;
            String frommailid = "";
            String password = "";
            String activitycode = "EMTRP1";

            ArrayList emaildetails = new ArrayList();
            emaildetails = fmsOperationBP.getEmailDetails(activitycode);
            Iterator itr1 = emaildetails.iterator();
            FMSOperationTO opTO = new FMSOperationTO();
            if (itr1.hasNext()) {
                opTO = new FMSOperationTO();
                opTO = (FMSOperationTO) itr1.next();
                smtp = opTO.getSmtp();
                emailPort = Integer.parseInt(opTO.getPort());
                frommailid = opTO.getEmailId();
                password = opTO.getPassword();
            }

            String vehicleId = request.getParameter("vehicleId");
            to = tripBP.getFCLeadMailId(vehicleId);

            ArrayList jobCardList = new ArrayList();
            jobCardList = fmsOperationBP.getJobCardDetails(jobcardId);
            Iterator itr2 = jobCardList.iterator();
            String jobcardIssueDate = "";
            String serviceTypeName = "";
            String priorty = "";
            String remarks = "";
            String mappedDriverName = "";
            String vehicleRequiredDate = "";
            int travelledKm = 0;
            String location = "";
            String empName = "";
            String customerMobile = "";
            String customerEmail = "";
            FMSOperationTO opTO2 = new FMSOperationTO();
            while (itr2.hasNext()) {
                opTO2 = new FMSOperationTO();
                opTO2 = (FMSOperationTO) itr2.next();
                jobcardIssueDate = opTO2.getDate();
                serviceTypeName = opTO2.getServicetypeName();
                priorty = opTO2.getPriority1();
                remarks = opTO2.getRemarks();
                mappedDriverName = opTO2.getDriverName();
                vehicleRequiredDate = opTO2.getVehicleRequiredDate();
                travelledKm = opTO2.getKmReading();
                location = opTO2.getServiceLocation();
                empName = opTO2.getEmpName();
                customerMobile = opTO2.getMobileNo();
                customerEmail = opTO2.getEmailId();
            }

            String emailFormat = "Team, <br><br> <b>Confirmation mail for jobcard created for the vehicle : " + request.getParameter("regno") + "</b>";
            emailFormat = emailFormat + "<br> Issue Date: " + jobcardIssueDate;
            emailFormat = emailFormat + "<br> Service Type Name: " + serviceTypeName;
            emailFormat = emailFormat + "<br> Service Location: " + location;
            emailFormat = emailFormat + "<br> Priority: " + priorty;
            emailFormat = emailFormat + "<br> Driver Name: " + mappedDriverName;
            emailFormat = emailFormat + "<br> Km Reading: " + travelledKm;
            emailFormat = emailFormat + "<br> Vehicle Required Date: " + vehicleRequiredDate;
            emailFormat = emailFormat + "<br> Created By: " + empName;
            emailFormat = emailFormat + "<br> Remarks: " + remarks;
            emailFormat = emailFormat + "<br><br>  Thanks,<br> Team BrattleFoods. ";

            String subject = "Jobcrad Created For Vehicle No: " + regno;
            ;
            String content = emailFormat;
            String email = fmsOperationBP.getJobcardEmail();
            String temp1[] = null;
            temp1 = email.split("~");
            String to1 = temp1[0];
            cc = temp1[1];

            int mailSendingId = 0;
            TripTO tripTO = new TripTO();
            tripTO.setMailTypeId("2");
            tripTO.setMailSubjectTo(subject);
            tripTO.setMailSubjectCc(subject);
            tripTO.setMailSubjectBcc("");
            tripTO.setMailContentTo(content);
            tripTO.setMailContentCc(content);
            tripTO.setMailContentBcc("");
            tripTO.setMailIdTo(to + "," + to1);
            tripTO.setMailIdCc(cc);
            tripTO.setMailIdBcc("");

            String bookingSMSTemplate = ThrottleConstants.jobCardCreateMsg;
            bookingSMSTemplate = bookingSMSTemplate.replaceAll("VEHICLENO", request.getParameter("regno"));
            bookingSMSTemplate = bookingSMSTemplate.replaceAll("DELIVERYDATE", vehicleRequiredDate);
            bookingSMSTemplate = bookingSMSTemplate.replaceAll("BRANCHINCHARGENAME", customerMobile);
            FMSOperationTO operationTO1 = new FMSOperationTO();
            operationTO1.setSmsContent(bookingSMSTemplate);
            operationTO1.setSmsTo(customerMobile);
            operationTO1.setJobCardId(jobcardId);
            //            operationTO.setSmsTo(opTONew.getConsignorPhNo()+","+opTONew.getConsigneePhNo());
            int insertSMS = fmsOperationBP.insertSms(operationTO1, userId);

            request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
            mv = jobCardViewPage(request, response, command);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewMrsUnApprovalList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView jobCardViewPage(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Job Card Status";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> View Job Cards ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            //  setLocale(request, response);

            String param = request.getParameter("param");
            System.out.println("param vale is:" + param);

            String fromDate = (String) session.getAttribute("currentDate");
            String toDate = (String) session.getAttribute("currentDate");
            String regno = "";
            String jcno = "";
            String status = "";
            int operId = 0;
            int pageNo = 0;
            int totalPages = 0;
            int startIndex = 0;
            int endIndex = 0;
            int totalRecords = 0;
            ArrayList opList = new ArrayList();
            opList = securityBP.getOpList();
            ArrayList spList = fmsOperationBP.getServicePoints();
            request.setAttribute("spList", spList);
            PaginationHelper pagenation = new PaginationHelper();
            int compId = Integer.parseInt((String) session.getAttribute("companyId"));

            int userId = (Integer) session.getAttribute("userId");
            if (userId == 1081) {
                compId = 0;
            }
            System.out.println("userId:" + userId);
            System.out.println("compId:" + compId);

            ArrayList searchJobCardStatus = new ArrayList();

            totalRecords = fmsOperationBP.getTotalJobcards(jcno, regno, status, operId, fromDate, toDate, compId, "0");

            session.setAttribute("totalRecords", totalRecords);

            pagenation.setTotalRecords(totalRecords);
            String buttonClicked = "";
            pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            totalPages = pagenation.getTotalNoOfPages();
            request.setAttribute("pageNo", pageNo);
            request.setAttribute("totalPages", totalPages);
            request.setAttribute("opList", opList);
            startIndex = pagenation.getStartIndex();
            endIndex = pagenation.getEndIndex();

            int completed = 0;
            int planned = 0;
            int unPlanned = 0;
            int billed = 0;

            int oldcompleted = 0;
            int oldplanned = 0;
            int oldunPlanned = 0;
            int oldbillingcompleted = 0;
            String pcd = "";
            boolean isExist = false;
            ArrayList statusList = new ArrayList();
            statusList = fmsOperationBP.searchJobCardStatus(jcno, regno, status, operId, fromDate, toDate, compId, startIndex, 0, "0");
            Iterator itr = statusList.iterator();
            FMSOperationTO operationTO = null;
            while (itr.hasNext()) {
                operationTO = new FMSOperationTO();
                operationTO = (FMSOperationTO) itr.next();
                pcd = operationTO.getPcd();
                //////System.out.println("pcd" + pcd);
                if (dateComparation(fromDate, toDate, pcd)) {
                    //current jc
                    //////System.out.println("in current");
                    if ((operationTO.getStatus()).equalsIgnoreCase("Completed")) {
                        completed++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("Billing Completed")) {
                        billed++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("Planned")) {
                        planned++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("NotPlanned")) {
                        unPlanned++;
                    }
                } else {
                    //old jc
                    //////System.out.println("in old");
                    if ((operationTO.getStatus()).equalsIgnoreCase("Completed")) {
                        oldcompleted++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("Billing Completed")) {
                        oldbillingcompleted++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("Planned")) {
                        oldplanned++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("NotPlanned")) {
                        oldunPlanned++;
                    }
                }
            }
            request.setAttribute("completed", completed);
            request.setAttribute("planned", planned);
            request.setAttribute("unPlanned", unPlanned);
            request.setAttribute("billingcompleted", billed);

            request.setAttribute("oldcompleted", oldcompleted);
            request.setAttribute("oldplanned", oldplanned);
            request.setAttribute("oldunPlanned", oldunPlanned);
            request.setAttribute("oldbillingcompleted", oldbillingcompleted);

            searchJobCardStatus = fmsOperationBP.searchJobCardStatus(jcno, regno, status, operId, fromDate, toDate, compId, startIndex, endIndex, "0");
            request.setAttribute("searchJobCardStatus", searchJobCardStatus);
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);

            if (param != null && param.equals("ExportExcel")) {
                path = "content/RenderService/viewJobcardExcel.jsp";
            } else {
                path = "content/RenderService/supervisorView.jsp";
            }

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  view Jobcard Status Page--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getVehicleData(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) throws IOException {
        HttpSession session = request.getSession();
        FMSOperationTO operationTO = new FMSOperationTO();
        //        JsonTO jsonTO = new JsonTO();
        ArrayList userDetails = new ArrayList();
        String path = "";
        PrintWriter pw = response.getWriter();
        try {
            String driverName = "";
            response.setContentType("text/html");
            driverName = request.getParameter("driverName");

            String vehicleNo = request.getParameter("regno1");

            System.out.println("am here....:" + vehicleNo);
            ArrayList vehicleRegNoForJobCard = new ArrayList();
            vehicleRegNoForJobCard = fmsOperationBP.getVehicleRegNoForJobCard(vehicleNo);

            //            operationTO.setDriverName(driverName);
            //            userDetails = fmsOperationBP.getTechinicanDriverName(operationTO);
            JSONArray jsonArray = new JSONArray();
            Iterator itr = vehicleRegNoForJobCard.iterator();
            while (itr.hasNext()) {
                JSONObject jsonObject = new JSONObject();
                operationTO = (FMSOperationTO) itr.next();
                jsonObject.put("vehicleId", operationTO.getVehicleId());
                jsonObject.put("vehicleNo", operationTO.getVehicleNo());
                jsonObject.put("km", operationTO.getTripendkm());
                jsonObject.put("hm", operationTO.getTripEndHm());
                jsonObject.put("status", operationTO.getPlannedStatus());
                System.out.println("jsonObject = " + jsonObject);
                jsonArray.put(jsonObject);
            }
            System.out.println("jsonArray = " + jsonArray);

            pw.print(jsonArray);
        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {
            FPLogUtils.fpErrorLog("Failed to login --> " + exception);
            exception.printStackTrace();
        }
    }

    //receiveBodyWOBill
    public ModelAndView receiveBodyWOBill(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Recevice Body Works Bill";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> Recevice Body Works Bill";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/receviceBWBill.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Create")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            ArrayList jobCardlist = new ArrayList();
            //                jobCardlist = fmsOperationBP.getJobCardList();
            jobCardlist = fmsOperationBP.getContractJobcards();
            request.setAttribute("jobCardlist", jobCardlist);
            request.removeAttribute("vendorList");
            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Create jobcard page --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView generateBWBill(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
//            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Recevice Body Works Bill";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> Recevice Body Works Bill";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
//            path = "content/stores/refreshScript.jsp";

            path = "content/RenderService/receviceBWBill.jsp";
            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Create")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int jobcardId = Integer.parseInt(request.getParameter("mrsJobCardNumber"));
            int userId = (Integer) session.getAttribute("userId");
            String woId = request.getParameter("mrsJobCardNumber");
            String invoiceNo = request.getParameter("invoiceNo");
            String invoiceDate = request.getParameter("invoiceDate");
            String totalAmount = request.getParameter("totalAmount");
            String spareAmount = request.getParameter("sparesAmount");
            String vatAmount = request.getParameter("vatAmount");
            String serviceAmount = request.getParameter("serviceAmount");
            String serviceTaxAmount = request.getParameter("serviceTaxAmount");
            String remarks = request.getParameter("remarks");

            int status = 0;
            status = fmsOperationBP.saveBodyWorksBill(woId, invoiceNo,
                    spareAmount, vatAmount, serviceAmount, serviceTaxAmount, remarks, totalAmount, userId, invoiceDate);
            ArrayList jobCardlist = new ArrayList();
            jobCardlist = fmsOperationBP.getContractJobcards();
            request.setAttribute("jobCardlist", jobCardlist);
            request.setAttribute("jobcardId", jobcardId);

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to save body work bill --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getContractWoList(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) throws IOException {
        //////System.out.println("i am in ajax ");
        String jcNo = request.getParameter("jcNo");

        String suggestions = fmsOperationBP.getContractWos(Integer.parseInt(jcNo));
        //////System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();
    }

    public ModelAndView searchJobCardStatus(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        String menuPath = "";
        HttpSession session = request.getSession();
        String pageTitle = "Job Card Status";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> View Job Cards ";

        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/supervisorView.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Search")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int pageNo = 0;
            int totalPages = 0;
            int startIndex = 0;
            int endIndex = 0;
            int totalRecords = 0;
            int operId = 0;
            String fromDate = request.getParameter("fromDate");
            String toDate = request.getParameter("toDate");
            String regno = request.getParameter("regno");
            String jcno = request.getParameter("jcno");
            String status = request.getParameter("status");
            String custType = request.getParameter("custType");
            operId = Integer.parseInt(request.getParameter("operId"));

            int compId = Integer.parseInt((String) session.getAttribute("companyId"));
            int userId = (Integer) session.getAttribute("userId");
            if (userId == 1081) {
                compId = 0;
            }
            System.out.println("userId:" + userId);
            System.out.println("compId:" + compId);
            ArrayList searchJobCardStatus = new ArrayList();

            //pagenation start
            totalRecords = fmsOperationBP.getTotalJobcards(jcno, regno, status, operId, fromDate, toDate, compId, custType);
            session.setAttribute("totalRecords", totalRecords);
            /*
             PaginationHelper pagenation = new PaginationHelper();
             pagenation.setTotalRecords(totalRecords);
             String buttonClicked = "";
             if (request.getParameter("button") != null) {
             buttonClicked = request.getParameter("button");
             }
             pageNo = Integer.parseInt(request.getParameter("pageNo"));
    
             pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
             totalPages = pagenation.getTotalNoOfPages();
             startIndex = pagenation.getStartIndex();
             endIndex = pagenation.getEndIndex();
             *
             */
            searchJobCardStatus = fmsOperationBP.searchJobCardStatus(jcno, regno, status, operId, fromDate, toDate, compId, startIndex, endIndex, custType);
            request.setAttribute("pageNo", pageNo);
            request.setAttribute("totalPages", totalPages);
            //pagenation end
            ArrayList opList = new ArrayList();
            ArrayList spList = new ArrayList();
            opList = securityBP.getOpList();
            spList = fmsOperationBP.getServicePoints();
            request.setAttribute("spList", spList);
            request.setAttribute("opList", opList);
            //////System.out.println("searchJobCardStatus.size(() New In the Controller = " + searchJobCardStatus.size());
            if (searchJobCardStatus.size() > 0) {
                request.setAttribute("searchJobCardStatus", searchJobCardStatus);
            }
            request.setAttribute("fromDate", fromDate);
            request.setAttribute("toDate", toDate);
            request.setAttribute("regno", regno);
            request.setAttribute("jcno", jcno);
            request.setAttribute("status", status);
            request.setAttribute("operId", operId);
            request.setAttribute("custType", custType);

            //
            int completed = 0;
            int planned = 0;
            int unPlanned = 0;
            int billingcompleted = 0;

            int oldcompleted = 0;
            int oldplanned = 0;
            int oldunPlanned = 0;
            int oldbillingcompleted = 0;

            String pcd = "";
            boolean isExist = false;
            ArrayList statusList = new ArrayList();
            statusList = fmsOperationBP.searchJobCardStatus(jcno, regno, status, operId, fromDate, toDate, compId, startIndex, 0, custType);
            Iterator itr = statusList.iterator();
            //////System.out.println("vijay JC size" + statusList.size());
            FMSOperationTO operationTO = null;
            while (itr.hasNext()) {
                operationTO = new FMSOperationTO();
                operationTO = (FMSOperationTO) itr.next();
                pcd = operationTO.getPcd();
                if (dateComparation(fromDate, toDate, pcd) || operationTO.getReqDate().equalsIgnoreCase("Not Scheduled")) {
                    //current jc

                    if ((operationTO.getStatus()).equalsIgnoreCase("Completed")) {
                        completed++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("Billing Completed")) {
                        billingcompleted++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("Planned")) {
                        planned++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("NotPlanned")) {
                        unPlanned++;
                    }
                } else {
                    //old jc
                    //////System.out.println("vijay JC new" + statusList.size());
                    if ((operationTO.getStatus()).equalsIgnoreCase("Completed")) {
                        oldcompleted++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("Billing Completed")) {
                        oldbillingcompleted++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("Planned")) {
                        oldplanned++;
                    } else if ((operationTO.getStatus()).equalsIgnoreCase("NotPlanned")) {
                        oldunPlanned++;
                    }
                }

                //////System.out.println("current" + (completed + planned + unPlanned + billingcompleted));
                //////System.out.println("current" + (oldcompleted + oldplanned + oldunPlanned + oldbillingcompleted));
            }
            request.setAttribute("totalJcard", (completed + planned + unPlanned + billingcompleted) + (oldcompleted + oldplanned + oldunPlanned + oldbillingcompleted));
            request.setAttribute("completed", (completed + oldcompleted));
            request.setAttribute("planned", (planned + oldplanned));
            request.setAttribute("unPlanned", (unPlanned + oldunPlanned));
            request.setAttribute("billingcompleted", (billingcompleted + oldbillingcompleted));

            request.setAttribute("oldcompleted", oldcompleted);
            request.setAttribute("oldplanned", oldplanned);
            request.setAttribute("oldunPlanned", oldunPlanned);
            request.setAttribute("oldbillingcompleted", oldbillingcompleted);

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  search JobCard Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView viewPreviousJobCard(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Operation >> Work Order ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/previousJobCard.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ViewPrevious")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int workOrderId = Integer.parseInt(request.getParameter("workOrderId"));
            int jobcardId = Integer.parseInt(request.getParameter("jobcardId"));
            String jcMYFormatNo = request.getParameter("jcMYFormatNo");
            //////System.out.println("jobcardId" + jobcardId);
            ArrayList problemDetails = new ArrayList();
            ArrayList jobCardProblemDetails = new ArrayList();
            ArrayList issueItemList = new ArrayList();
            float servicedRate = 0;

            problemDetails = fmsOperationBP.getProblemDetails(workOrderId, jobcardId);
            jobCardProblemDetails = fmsOperationBP.jobCardProblemDetails(jobcardId);
            servicedRate = fmsOperationBP.jobCardMrsDetails(jobcardId);
            issueItemList = mrsBP.processJcIssueHistory(jobcardId);

            ArrayList mrsItemStatus = mrsBP.processMrsItemStatus(jobcardId);

            FMSOperationTO operatonTO = null;
            String customer = "";
            if (jobCardProblemDetails.size() != 0) {
                Iterator itr = jobCardProblemDetails.iterator();
                while (itr.hasNext()) {
                    operatonTO = new FMSOperationTO();
                    operatonTO = (FMSOperationTO) itr.next();
                    //////System.out.println("in loop");
                    customer = operatonTO.getIdentifiedby();
                    if (customer.equalsIgnoreCase("Customer")) {
                        //////System.out.println("in loop");
                        problemDetails.add(operatonTO);

                    }
                }
            }

            ArrayList technicians = new ArrayList();
            int compId = Integer.parseInt((String) session.getAttribute("companyId"));
            technicians = mrsBP.techniciansList(compId);
            request.setAttribute("technicians", technicians);
            request.setAttribute("itemList", issueItemList);
            request.setAttribute("jobCardProblemDetails", jobCardProblemDetails);
            request.setAttribute("workOrderProblemDetails", problemDetails);
            request.setAttribute("workOrderId", workOrderId);
            request.setAttribute("jobcardId", jobcardId);
            request.setAttribute("jcMYFormatNo", jcMYFormatNo);
            request.setAttribute("servicedRate", servicedRate);
            request.setAttribute("mrsItemStatus", mrsItemStatus);
            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  View Previous JobCard Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView scheduleJobCard(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";

        String pageTitle = "Job Card Status";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >> Schedule Job card";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            //  setLocale(request, response);
            path = "content/RenderService/generateJobCard.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Schedule")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            //      setLocale(request, response);
            int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
            int workOrderId = Integer.parseInt(request.getParameter("workOrderId"));
            int jobcardId = Integer.parseInt(request.getParameter("jobcardId"));
            String jcMYFormatNo = request.getParameter("jcMYFormatNo");
            int km = Integer.parseInt(request.getParameter("km"));
            int hm = Integer.parseInt(request.getParameter("hm"));
            int compId = Integer.parseInt((String) session.getAttribute("companyId"));

            ArrayList vehicleDetails = new ArrayList();
            ArrayList workOrderDetails = new ArrayList();
            ArrayList problemDetails = new ArrayList();
            ArrayList jobcardList = new ArrayList();
            ArrayList problemList = new ArrayList();
            ArrayList routineServiceList = new ArrayList();
            ArrayList nonGracePeriodList = new ArrayList();
            ArrayList nonGraceSchedList = new ArrayList();

            ArrayList jobCardProblemDetails = new ArrayList();
            ArrayList bodyWorksList = new ArrayList();
            bodyWorksList = fmsOperationBP.getBodyWorksList(jobcardId);
            vehicleDetails = fmsOperationBP.getVehicleDetails(vehicleId, jobcardId);
            ArrayList scheduledServices = new ArrayList();
            scheduledServices = fmsOperationBP.getScheduledServices(vehicleId, jobcardId);
            String compDate = fmsOperationBP.getcomDate(jobcardId);
            String[] jdata = compDate.split("~");
            compDate = jdata[0];
            String jRemarks = jdata[1];

            problemDetails = fmsOperationBP.getProblemDetails(workOrderId, jobcardId);

            jobCardProblemDetails = fmsOperationBP.jobCardProblemDetails(jobcardId);
            FMSOperationTO operatonTO = null;
            //                String customer = "";
            //                if (jobCardProblemDetails.size() != 0) {
            //                    Iterator itr = jobCardProblemDetails.iterator();
            //                    while (itr.hasNext()) {
            //                        operatonTO = new FMSOperationTO();
            //                        operatonTO = (FMSOperationTO) itr.next();
            //                        //////System.out.println("in loop");
            //                        customer = operatonTO.getIdentifiedby();
            //                        if (customer.equalsIgnoreCase("Customer")) {
            //                            //////System.out.println("in loop");
            //                            problemDetails.add(operatonTO);
            //
            //                        }
            //                    }
            //                }

            workOrderDetails = fmsOperationBP.getWorkOrderDetails(workOrderId);
            jobcardList = fmsOperationBP.getJobcardList(vehicleId, jobcardId);
            problemList = fmsOperationBP.getProblemList(vehicleId, jobcardId);
            //                problemList = fmsOperationBP.getProblemList(vehicleId, workOrderId);
            System.out.println("am here.........1");
            routineServiceList = fmsOperationBP.getServiceList(vehicleId, km, jobcardId);
            //  nonGraceSchedList = fmsOperationBP.getNonGraceSchedServices(routineServiceList, vehicleId, jobcardId, km, hm);
            routineServiceList.addAll(nonGraceSchedList);
            nonGracePeriodList = fmsOperationBP.handleNonGracePeriodServices(vehicleId, km, routineServiceList);

            ArrayList technicians = new ArrayList();
            technicians = mrsBP.techniciansList(compId);
            request.setAttribute("vehicleDetails", vehicleDetails);
            request.setAttribute("technicians", technicians);
            request.setAttribute("bodyWorksList", bodyWorksList);
            request.setAttribute("vehicleId", vehicleId);
            request.setAttribute("jobCardProblemDetails", jobCardProblemDetails);
            ArrayList sections = new ArrayList();
            sections = serviceBP.getSections();
            request.setAttribute("sections", sections);
            request.setAttribute("vehicleDetails", vehicleDetails);
            request.setAttribute("scheduledServices", scheduledServices);
            //////System.out.println("problemDetails.size() = " + jobCardProblemDetails.size());
            //                request.setAttribute("workOrderProblemDetails", problemDetails);
            request.setAttribute("workOrderProblemDetails", jobCardProblemDetails);
            request.setAttribute("workOrderDetails", workOrderDetails);
            request.setAttribute("jobCardList", jobcardList);
            request.setAttribute("problemList", problemList);
            request.setAttribute("routineServiceList", routineServiceList);
            request.setAttribute("nonGracePeriodList", nonGracePeriodList);
            request.setAttribute("workOrderId", workOrderId);
            request.setAttribute("jobcardId", jobcardId);
            request.setAttribute("jcMYFormatNo", jcMYFormatNo);

            request.setAttribute("km", request.getParameter("km"));
            request.setAttribute("hm", request.getParameter("hm"));
            request.setAttribute("reqDate", request.getParameter("reqDate"));
            request.setAttribute("compDate", compDate);
            request.setAttribute("jRemarks", jRemarks);

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  search JobCard Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleTechnicianPopup(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Work Order Approval";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> Job Card Technicians";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/technicianPopUp.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Schedule")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int compId = Integer.parseInt((String) session.getAttribute("companyId"));
            System.out.println("compId" + compId);
            String jobCardId = (String) request.getParameter("jobCardId");
            System.out.println("jobCardId" + jobCardId);
            String probId = (String) request.getParameter("probId");
            System.out.println("probId" + probId);

            request.setAttribute("jobCardId", jobCardId);
            request.setAttribute("probId", probId);

            ArrayList technicians = new ArrayList();
            technicians = mrsBP.techniciansList(compId);
            request.setAttribute("technicians", technicians);

            ArrayList existingTechnicians = fmsOperationBP.getExistingTechnicians(jobCardId, probId);
            request.setAttribute("existingTechnicians", existingTechnicians);

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to schedule wo --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveJobCardTechnicians(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Work Order Approval";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> W O Approval";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/stores/refreshScript1.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-Schedule")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int compId = Integer.parseInt((String) session.getAttribute("companyId"));
            int userId = (Integer) session.getAttribute("userId");
            String jobCardId = (String) request.getParameter("jobCardId");
            String probId = (String) request.getParameter("probId");

            String[] technicianId = request.getParameterValues("technicianId");
            String[] estimatedHrs = request.getParameterValues("estimatedHrs");
            String[] actualHrs = request.getParameterValues("actualHrs");

            int status = fmsOperationBP.saveJobCardTechnicians(jobCardId, probId, technicianId, estimatedHrs, actualHrs, userId);

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to schedule wo --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public void getServiceHmKm(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) throws IOException {
        //////System.out.println("i am in ajax ");
        HttpSession session = request.getSession();
        FMSOperationTO serviceTO = new FMSOperationTO();
        String serviceId = request.getParameter("serviceId");
        String vehicleId = request.getParameter("vehicleId");
        String km = request.getParameter("km");
        String hm = request.getParameter("hm");

        serviceTO.setServiceId(Integer.parseInt(serviceId));
        serviceTO.setVehicleId(Integer.parseInt(vehicleId));
        serviceTO.setKmReading(Integer.parseInt(km));
        serviceTO.setHourMeter(Integer.parseInt(hm));

        String suggestions = fmsOperationBP.getDueHmKm(serviceTO);
        //////System.out.println("suggestions" + suggestions);
        PrintWriter writer = response.getWriter();
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        writer.print(suggestions);
        writer.close();
    }

    public ModelAndView handleWoDetails(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Service Details ";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >>Service Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList woDetail = new ArrayList();
            path = "content/RenderService/contractorWo.jsp";
            //
            // //////System.out.println("size is  " + userFunctions.size());
            // if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ExternalTech")) {
            //      path = "content/common/NotAuthorized.jsp";
            //
            // }
            int status = 0;
            String woId = (String) (request.getParameter("woId"));
            woDetail = fmsOperationBP.processWoDetail(woId);
            request.setAttribute("woDetail", woDetail);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  show external service details Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView externalTechDetailsPage(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "View JobCard  ";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >> View JobCard";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/externalTech.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ExternalTech")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int status = 0;
            int jobcardId = Integer.parseInt(request.getParameter("jobcardId"));
            int userId = (Integer) session.getAttribute("userId");
            String probId = request.getParameter("probId");
            //int workOrderId=Integer.parseInt(request.getParameter("workOrderId"));
            request.setAttribute("jobcardId", jobcardId);
            request.setAttribute("probId", request.getParameter("probId"));
            request.setAttribute("probName", request.getParameter("probName"));

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  show external technician details Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveExternalTechDetails(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "View JobCard  ";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >> View JobCard";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/externalTech.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ViewPage")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int status = 0;
            int jobcardId = Integer.parseInt(request.getParameter("jobcardId"));
            int userId = (Integer) session.getAttribute("userId");
            int probId = Integer.parseInt(request.getParameter("probId"));
            String techName = request.getParameter("techName");
            String compName = request.getParameter("compName");
            String contactNo = request.getParameter("contactNo");
            String labourCharge = request.getParameter("labourCharge");
            status = fmsOperationBP.insertExternalTechnician(jobcardId, probId, techName, compName, contactNo, labourCharge);

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  show external technician details Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveJobCardScheduledDetails(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            //            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        ModelAndView mv = null;
        String pageTitle = "View Job Cards ";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >> View Job Cards";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/supervisorView.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ViewPage")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int jobcardId = Integer.parseInt(request.getParameter("jobcardId"));
            int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
            //////System.out.println("vehicleId vijaysgf" + vehicleId);
            int userId = (Integer) session.getAttribute("userId");
            //work order compaliants

            int currentkm = Integer.parseInt(request.getParameter("km"));
            int currenthm = Integer.parseInt(request.getParameter("hm"));

            String CompletionDate = request.getParameter("compDate");
            System.out.println("CompletionDate = " + CompletionDate);
            String jremarks = request.getParameter("jremarks");
            //////System.out.println("jremarks:" + jremarks);
            String[] probId = request.getParameterValues("probId");
            String[] technicianId = request.getParameterValues("technicianId");
            String[] scheduledDate = request.getParameterValues("scheduledDate");
            String[] status = request.getParameterValues("status");
            String[] cremarks = request.getParameterValues("cremarks");
            String[] cause = request.getParameterValues("cause");
            String[] remark = request.getParameterValues("remark");
            String[] secId = request.getParameterValues("secId");
            String[] serviceindex = request.getParameterValues("selectedServices");
            String[] serviceId = request.getParameterValues("serviceId");

            String[] serviceTech = request.getParameterValues("tech");
            // String[] servicesDone = request.getParameterValues("servicesDone");
            String[] serviceStatus = request.getParameterValues("serviceStatus");
            //String[] kms = request.getParameterValues("kmDone");
            //String[] hms = request.getParameterValues("hmDone");
            String[] servicedDate = request.getParameterValues("servicedDate");
            String[] index = request.getParameterValues("selectedindex");
            String[] symptoms = request.getParameterValues("symptoms");
            String[] severity = request.getParameterValues("severity");
            String[] validate = request.getParameterValues("validate");

            String[] nonGraceServicedDate = request.getParameterValues("NonGraceDate");
            String[] nonGraceIndex = request.getParameterValues("nonGraceSelectInd");
            String[] nonGraceTech = request.getParameterValues("NonGraceTech");
            String[] nonGraceStatus = request.getParameterValues("nonGracePeriodStatus");
            String[] nonGraceServiceId = request.getParameterValues("NonGraceServiceId");

            //update jobcard completion date
            int insertStatus = 0;
            insertStatus = fmsOperationBP.updateCompletionDate(jobcardId, CompletionDate, jremarks);

            if (index != null) {
                insertStatus = fmsOperationBP.insertJobCardScheduleDetails(secId, userId, jobcardId, probId, technicianId, scheduledDate, status, cremarks, cause, remark, index, symptoms, severity, validate);

            }

            //insert service details (which is not done)
            if (serviceindex != null) {
                insertStatus = fmsOperationBP.insertJobCardServices(currentkm, currenthm, userId, jobcardId, serviceindex, serviceId, servicedDate, serviceTech, serviceStatus, vehicleId);
            }

            if (nonGraceIndex != null) {
                insertStatus = fmsOperationBP.insertJobCardServices(currentkm, currenthm, userId, jobcardId, nonGraceIndex, nonGraceServiceId, nonGraceServicedDate, nonGraceTech, nonGraceStatus, vehicleId);
            }

            String buttonValue = request.getParameter("buttonClick");
            //  generateBodyPartsWO(request,response,command);
            if (buttonValue.equals("generateWO")) {

                int workOrderId = Integer.parseInt(request.getParameter("workOrderId"));
                int km = Integer.parseInt(request.getParameter("km"));

                //work order compaliants
                String[] secName = request.getParameterValues("secName");
                //  String[] secId = request.getParameterValues("secId");
                String[] probName = request.getParameterValues("probName");

                String reqDate = request.getParameter("reqDate");

                ArrayList problemList = new ArrayList();
                FMSOperationTO operationTO = null;
                int temp = 0;
                int sectionId = 0;

                if (index != null) {
                    for (int i = 0; i < index.length; i++) {
                        temp = Integer.parseInt(index[i]);
                        sectionId = Integer.parseInt(secId[temp]);

                        if (sectionId == 1016 || sectionId == 1040) {
                            operationTO = new FMSOperationTO();
                            operationTO.setProbId(Integer.parseInt(probId[temp]));
                            operationTO.setProbName(probName[temp]);
                            operationTO.setSecName(secName[temp]);
                            operationTO.setSymptoms(symptoms[temp]);
                            operationTO.setSeverity(Integer.parseInt(severity[temp]));
                            if (fmsOperationBP.isThisProblemExist(jobcardId, operationTO.getProbId()) == 0) {
                                problemList.add(operationTO);
                            }
                        }

                    }
                }

                ArrayList vehicleDetails = new ArrayList();
                ArrayList vendorList = new ArrayList();
                //////System.out.println("problemList size" + problemList.size());
                vehicleDetails = fmsOperationBP.getVehicleDetails(vehicleId, jobcardId);
                vendorList = fmsOperationBP.getVendorDetails();
                request.setAttribute("vehicleDetails", vehicleDetails);
                request.setAttribute("vendorList", vendorList);
                request.setAttribute("problemList", problemList);
                request.setAttribute("reqDate", reqDate);
                request.setAttribute("jobcardId", jobcardId);
                request.setAttribute("workOrderId", workOrderId);
                request.setAttribute("vehicleId", vehicleId);
                request.setAttribute("km", km);

                if (problemList.size() != 0) {
                    path = "content/RenderService/bodyParts.jsp";
                    mv = new ModelAndView(path);
                }
            } else {
                request.setAttribute("sessionPageParam", session.getAttribute("sessionPageParam"));
                mv = scheduleJobCard(request, response, command);
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Updated Successfully");
            }

            //            }
            //                String fromDate = "";
            //                String toDate = "";
            //
            //                int pageNo = 0;
            //                int totalPages = 0;
            //                int startIndex = 0;
            //                int endIndex = 0;
            //                int totalRecords = 0;
            //                String regno ="";
            //                String status1 ="";
            //                int operId =0;
            //
            //                PaginationHelper pagenation = new PaginationHelper();
            //                int compId = Integer.parseInt((String) session.getAttribute("companyId"));
            //                ArrayList searchJobCardStatus = new ArrayList();
            //                totalRecords = (Integer) session.getAttribute("totalRecords");
            //
            //                    totalRecords = fmsOperationBP.getTotalJobcards(regno,status1,operId,fromDate, toDate, compId);
            //                    session.setAttribute("totalRecords", totalRecords);
            //
            //
            //                pagenation.setTotalRecords(totalRecords);
            //                String buttonClicked = "";
            //                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
            //                totalPages = pagenation.getTotalNoOfPages();
            //                request.setAttribute("pageNo", pageNo);
            //                request.setAttribute("totalPages", totalPages);
            //                startIndex = pagenation.getStartIndex();
            //                endIndex = pagenation.getEndIndex();
            //                searchJobCardStatus = fmsOperationBP.searchJobCardStatus(regno,status1,operId,fromDate, toDate, compId, startIndex, endIndex);
            //                request.setAttribute("searchJobCardStatus", searchJobCardStatus);
            //
            //                }
            //                String fromDate =(String) session.getAttribute("currentDate");
            //                String toDate = (String) session.getAttribute("currentDate");
            //                request.setAttribute("fromDate", fromDate);
            //                request.setAttribute("toDate", toDate);
            //
            //
            //                int completed=0;
            //                int planned=0;
            //                int unPlanned=0;
            //                String regno="";
            //                String status="";
            //                int compId = Integer.parseInt((String) session.getAttribute("companyId"));
            //                int startIndex=0;
            //                int operId=0;
            //                ArrayList statusList = new ArrayList();
            //                statusList= fmsOperationBP.searchJobCardStatus(regno,status,operId,fromDate, toDate, compId, startIndex,0);
            //                Iterator itr=statusList.iterator();
            //                FMSOperationTO operationTO=null;
            //                while(itr.hasNext()){
            //                    operationTO=new FMSOperationTO();
            //                    operationTO=(FMSOperationTO)itr.next();
            //                    if((operationTO.getStatus()).equalsIgnoreCase("Completed")){
            //                        completed++;
            //                    }else if((operationTO.getStatus()).equalsIgnoreCase("Planned")){
            //                        planned++;
            //                    }else if((operationTO.getStatus()).equalsIgnoreCase("NotPlanned")){
            //                        unPlanned++;
            //                    }
            //                }
            //
            //                request.setAttribute("completed", completed);
            //                request.setAttribute("planned", planned);
            //                request.setAttribute("unPlanned", unPlanned);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  save JobCard Schedule--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }

    public ModelAndView handlePrintJobCardPage(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        ArrayList serviceTypes = new ArrayList();
        ArrayList operationPointList = new ArrayList();
        ArrayList cardDetails = new ArrayList();
        FMSOperationTO heading = new FMSOperationTO();
        String menuPath = "Reports >>  Vehicle >> Jobcard Bills";
        path = "content/report/printJobCard2.jsp";
        try {
            //  setLocale(request, response);
            int workOrderId = Integer.parseInt(request.getParameter("workOrderId"));
            int jobcardId = Integer.parseInt(request.getParameter("jobcardId"));
            String jcMYFormatNo = request.getParameter("jcMYFormatNo");
            //////System.out.println("jobcardId" + jobcardId);
            ArrayList problemDetails = new ArrayList();
            ArrayList DetailsList = new ArrayList();
            ArrayList sortedDetailsList = new ArrayList();
            ArrayList serviceDetails = new ArrayList();
            ArrayList vehicleDetails = new ArrayList();
            ArrayList jobCardProblemDetails = new ArrayList();
            ArrayList issueItemList = new ArrayList();
            float servicedRate = 0;
            int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
            int trailerId = 0;
            if (request.getParameter("trailerId") != null) {
                trailerId = Integer.parseInt(request.getParameter("trailerId"));
            }
            if (trailerId == 0) {
                vehicleDetails = fmsOperationBP.getVehicleDetails(vehicleId, jobcardId);
            } else {
                vehicleDetails = fmsOperationBP.getTrailerDetails(trailerId, jobcardId);

            }
            Iterator itr1 = vehicleDetails.iterator();
            FMSOperationTO opTO = null;
            while (itr1.hasNext()) {
                opTO = (FMSOperationTO) itr1.next();
                ArrayList test = new ArrayList();
                test = fmsOperationBP.processJcVehDetail(jobcardId);
                Iterator itr2 = test.iterator();
                FMSOperationTO operTO = null;
                while (itr2.hasNext()) {
                    operTO = (FMSOperationTO) itr2.next();
                    opTO.setCreatedDate(operTO.getCreatedDate());
                    opTO.setDriverName(operTO.getDriverName());
                    opTO.setIntime(operTO.getIntime());
                    opTO.setScheduledDate(operTO.getScheduledDate());
                    opTO.setTotalKm(operTO.getTotalKm());
                    opTO.setRemarks(operTO.getRemarks());
                    opTO.setBayNo(operTO.getBayNo());
                    opTO.setCompName(operTO.getCompName());
                    opTO.setPriority1(operTO.getPriority1());
                    opTO.setServiceLocation(operTO.getServiceLocation());
                    opTO.setCustName(operTO.getCustName());
                    opTO.setCustAddress(operTO.getCustAddress());
                    opTO.setCustCity(operTO.getCustCity());
                    opTO.setCustState(operTO.getCustState());
                    opTO.setCustPhone(operTO.getCustPhone());
                    opTO.setCustMobile(operTO.getCustMobile());
                    opTO.setCustEmail(operTO.getCustEmail());
                    opTO.setServicetypeName(operTO.getServicetypeName());
                    opTO.setVehicleId(operTO.getVehicleId());
                    opTO.setTrailerId(operTO.getTrailerId());

                    //////System.out.println("operTO.getDriverName()" + operTO.getDriverName());
                    //////System.out.println("operTO.getCreatedDate()()" + operTO.getCreatedDate());
                }
            }
            problemDetails = fmsOperationBP.getWoProblemDetails(workOrderId, jobcardId);
            jobCardProblemDetails = fmsOperationBP.jobCardProblemDetails(jobcardId);
            serviceDetails = fmsOperationBP.processServiceDetails(jobcardId);
            issueItemList = mrsBP.processJcIssueHistory(jobcardId);

            if (problemDetails.size() != 0) {
                heading = new FMSOperationTO();
                heading.setProbName("WORKORDERCOMPLAINT");
                DetailsList.add(heading);
                DetailsList.addAll(problemDetails);
            }
            if (jobCardProblemDetails.size() != 0) {
                heading = new FMSOperationTO();
                heading.setProbName("JOBCARDCOMPLAINT");
                DetailsList.add(heading);
                DetailsList.addAll(jobCardProblemDetails);
            }
            if (serviceDetails.size() != 0) {
                heading = new FMSOperationTO();
                heading.setProbName("PERIODICSERVICE");
                DetailsList.add(heading);
                DetailsList.addAll(serviceDetails);
            }

            sortedDetailsList = fmsOperationBP.sortPrintJobcardData(DetailsList);
            //ArrayList jobCardTechnicians = fmsOperationBP.getJobCardTechnicians(jobcardId);
            FMSOperationTO operatonTO = null;
            String customer = "";

            String jobCardUser = fmsOperationBP.getJobCardOwner(jobcardId);
            int km = Integer.parseInt(request.getParameter("km"));
            ArrayList technicians = new ArrayList();
            int compId = Integer.parseInt((String) session.getAttribute("companyId"));
            String compName = ((String) session.getAttribute("companyName"));
            request.setAttribute("compName", compName);
            technicians = mrsBP.techniciansList(compId);
            //request.setAttribute("jobCardTechnicians", jobCardTechnicians);
            request.setAttribute("technicians", technicians);
            System.out.println("vehicleDetails" + vehicleDetails.size());
            request.setAttribute("details", vehicleDetails);
            request.setAttribute("itemList", issueItemList);
            request.setAttribute("jobCardProblemDetails", jobCardProblemDetails);
            request.setAttribute("workOrderProblemDetails", problemDetails);
            request.setAttribute("serviceDetails", serviceDetails);

            request.setAttribute("woSize", problemDetails.size());
            request.setAttribute("jobCardUser", jobCardUser);
            request.setAttribute("jcSize", jobCardProblemDetails.size());
            request.setAttribute("serviceSize", serviceDetails.size());

            request.setAttribute("DetailsList", sortedDetailsList);
            request.setAttribute("workOrderId", workOrderId);
            request.setAttribute("jobcardId", jobcardId);
            request.setAttribute("km", km);
            request.setAttribute("jcMYFormatNo", jcMYFormatNo);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView serviceDone(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Service Details ";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >>Service Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/serviceDone.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ExternalTech")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int status = 0;
            int serviceId = Integer.parseInt(request.getParameter("serviceId"));
            int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
            request.setAttribute("serviceId", serviceId);
            request.setAttribute("vehicleId", request.getParameter("vehicleId"));
            request.setAttribute("serviceName", request.getParameter("serviceName"));

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  show external technician details Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView saveExternalService(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Service Details ";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >>Service Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/serviceDone.jsp";

            //////System.out.println("size is  " + userFunctions.size());
            //            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ExternalTech")) {
            //                path = "content/common/NotAuthorized.jsp";
            //            } else {
            int status = 0;
            int serviceId = Integer.parseInt(request.getParameter("serviceId"));
            int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
            String dateofIssue = request.getParameter("date");
            int userId = (Integer) session.getAttribute("userId");
            int jobcardId = 0;
            int km = Integer.parseInt(request.getParameter("kmDone"));
            int hms = Integer.parseInt(request.getParameter("hmDone"));
            status = fmsOperationBP.insertServicedDetails(userId, jobcardId, serviceId, km, hms, vehicleId, dateofIssue);
            request.setAttribute("successMessage", "Processed Succesfully");

            //            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  show external service details Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    // This function used to view the job card details to change the job card status to planned.
    public ModelAndView changeJobCardStatus(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Job Card Status";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "Rendor Service >> View Job Cards ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            path = "content/RenderService/supervisorView.jsp";
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            System.out.println("size is  " + userFunctions.size());
            if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ViewPage")) {
                path = "content/common/NotAuthorized.jsp";
            } else {

                String successMessage = "";

                String fromDate = (String) session.getAttribute("currentDate");
                String toDate = (String) session.getAttribute("currentDate");
                String regno = "";
                String jcno = "";
                String status = "NotPlanned";
                int operId = 0;
                int pageNo = 0;
                int totalPages = 0;
                int startIndex = 0;
                int endIndex = 0;
                int totalRecords = 0;

                String serviceType = request.getParameter("serviceType");
                if (serviceType == null) {
                    serviceType = "0";
                }

                ArrayList opList = new ArrayList();
                opList = securityBP.getOpList();

                ArrayList spList = new ArrayList();
                spList = fmsOperationBP.getServicePoints();

                ArrayList serviceTypes = new ArrayList();
                serviceTypes = fmsOperationBP.getServiceTypes();

                PaginationHelper pagenation = new PaginationHelper();
                int compId = Integer.parseInt((String) session.getAttribute("companyId"));
                ArrayList searchJobCardStatus = new ArrayList();
                //                totalRecords = fmsOperationBP.getTotalJobcards(jcno, regno, status, operId, fromDate, toDate, compId,"0", serviceType);
                session.setAttribute("totalRecords", totalRecords);

                pagenation.setTotalRecords(totalRecords);
                String buttonClicked = "";
                pageNo = pagenation.getPageNoToBeDisplayed(pageNo, buttonClicked);
                totalPages = pagenation.getTotalNoOfPages();
                request.setAttribute("pageNo", pageNo);
                request.setAttribute("totalPages", totalPages);

                request.setAttribute("opList", opList);
                request.setAttribute("spList", spList);
                request.setAttribute("serviceTypes", serviceTypes);
                startIndex = pagenation.getStartIndex();
                endIndex = pagenation.getEndIndex();

                int completed = 0;
                int planned = 0;
                int unPlanned = 0;
                int billed = 0;

                int oldcompleted = 0;
                int oldplanned = 0;
                int oldunPlanned = 0;
                int oldbillingcompleted = 0;
                String pcd = "";
                boolean isExist = false;

                FMSOperationTO operationTO1 = new FMSOperationTO();
                if (request.getParameter("jobcardId") != null && request.getParameter("jobcardId").length() > 0) {
                    int jobcardId = Integer.parseInt(request.getParameter("jobcardId"));
                    operationTO1.setJobCardId(jobcardId);
                    int updstatus = fmsOperationBP.changeJobCardStatus(operationTO1);
                    if (updstatus != 0) {
                        successMessage = "Job Card Status Changed from Completed to Planned";
                    }
                }

                ArrayList statusList = new ArrayList();
                statusList = fmsOperationBP.searchJobCardStatus(jcno, regno, status, operId, fromDate, toDate, compId, startIndex, 0, "0");

                Iterator itr = statusList.iterator();
                FMSOperationTO operationTO = null;
                while (itr.hasNext()) {
                    operationTO = new FMSOperationTO();
                    operationTO = (FMSOperationTO) itr.next();
                    pcd = operationTO.getPcd();
                    System.out.println("pcd" + pcd);
                    if (dateComparation(fromDate, toDate, pcd)) {
                        //current jc
                        System.out.println("in current");
                        if ((operationTO.getStatus()).equalsIgnoreCase("Completed")) {
                            completed++;
                        } else if ((operationTO.getStatus()).equalsIgnoreCase("Billing Completed")) {
                            billed++;
                        } else if ((operationTO.getStatus()).equalsIgnoreCase("Planned")) {
                            planned++;
                        } else if ((operationTO.getStatus()).equalsIgnoreCase("NotPlanned")) {
                            unPlanned++;
                        }
                    } else {
                        //old jc
                        System.out.println("in old");
                        if ((operationTO.getStatus()).equalsIgnoreCase("Completed")) {
                            oldcompleted++;
                        } else if ((operationTO.getStatus()).equalsIgnoreCase("Billing Completed")) {
                            oldbillingcompleted++;
                        } else if ((operationTO.getStatus()).equalsIgnoreCase("Planned")) {
                            oldplanned++;
                        } else if ((operationTO.getStatus()).equalsIgnoreCase("NotPlanned")) {
                            oldunPlanned++;
                        }
                    }
                }
                request.setAttribute("completed", completed);
                request.setAttribute("planned", planned);
                request.setAttribute("unPlanned", unPlanned);
                request.setAttribute("billingcompleted", billed);

                request.setAttribute("oldcompleted", oldcompleted);
                request.setAttribute("oldplanned", oldplanned);
                request.setAttribute("oldunPlanned", oldunPlanned);
                request.setAttribute("oldbillingcompleted", oldbillingcompleted);

                //searchJobCardStatus = fmsOperationBP.searchJobCardStatus(jcno, regno, status, operId, fromDate, toDate, compId, startIndex, endIndex,"0", serviceType);
                request.setAttribute("searchJobCardStatus", statusList);
                request.setAttribute("fromDate", fromDate);
                request.setAttribute("toDate", toDate);

                request.setAttribute("message", "Job Card Status Changed from Completed to Planned");

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  view Jobcard Status Page--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public static boolean dateComparation(String fromDate, String toDate, String CompDate) {
        if (CompDate != "") {
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            Calendar cal3 = Calendar.getInstance();
            String[] temp = null;
            temp = fromDate.split("-");
            cal1.set(Integer.parseInt(temp[2]), Integer.parseInt(temp[1]), Integer.parseInt(temp[0]));
            temp = toDate.split("-");
            cal2.set(Integer.parseInt(temp[2]), Integer.parseInt(temp[1]), Integer.parseInt(temp[0]));
            temp = CompDate.split("-");
            cal3.set(Integer.parseInt(temp[2]), Integer.parseInt(temp[1]), Integer.parseInt(temp[0]));
            if ((cal3.after(cal1) && cal3.before(cal2)) || cal3.equals(cal1) || cal3.equals(cal2)) {
                //////System.out.println("true");
                return true;
            } else {
                //////System.out.println("false");
                return false;
            }
        } else {
            //////System.out.println("true");
            return true;
        }

    }

    public ModelAndView handleWoBillDetails(HttpServletRequest request, HttpServletResponse response, FMSOperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        String menuPath = "";
        String pageTitle = "Service Details ";
        request.setAttribute("pageTitle", pageTitle);
        menuPath = "RenderService >>Service Details ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            ArrayList woDetail = new ArrayList();
            path = "content/RenderService/contractorWoBill.jsp";
            //
            // //////System.out.println("size is  " + userFunctions.size());
            // if (!loginBP.checkAuthorisation(userFunctions, "JobCard-ExternalTech")) {
            //      path = "content/common/NotAuthorized.jsp";
            //
            // }
            int status = 0;
            String woId = (String) (request.getParameter("woId"));
            woDetail = fmsOperationBP.processWoDetail(woId);
            request.setAttribute("woDetail", woDetail);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to  show external service details Status--> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
}
