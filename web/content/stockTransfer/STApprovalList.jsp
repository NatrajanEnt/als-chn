<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 
<head>
    
    
    <%@page language="java" contentType="text/html; charset=UTF-8"%>
<title>Parveen Auto Care</title>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>
 <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>    
 <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %>       
</head>
<script>
    
    function submitPage(value){              
       requestPtId=document.stockTransfer.servicePoint.value; 
        if(document.stockTransfer.servicePoint.value==0){
            alert("Please Select Service Point name");
             document.stockTransfer.servicePoint.focus();
             return;
             }
        document.stockTransfer.action='/throttle/request.do?servicePoint='+value;
        document.stockTransfer.submit();
        }        
function ApproveStock(reqId)
{    
    document.stockTransfer.requestId.value=reqId;
    url = '/throttle/approvalScreen.do';        
    document.stockTransfer.action=url;
    document.stockTransfer.submit();    
}    
    
function setRequestPtId(){  
    document.stockTransfer.servicePoint.focus();
    if('<%= request.getAttribute("requestPtId") %>' != 'null'){
        document.stockTransfer.servicePoint.value = '<%= request.getAttribute("requestPtId") %>';
    }    
}    
</script>

<div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Receive Stock" text="Approve Stock"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
	  <li><a href="general-forms.html"><spring:message code="hrms.label.Stock Transfer" text="Stock Transfer"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Receive Stock" text="Approve Stock"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
<body onLoad="setRequestPtId();">
<form name="stockTransfer"  method="post" >

<%--<%@ include file="/content/common/path.jsp" %>--%>


<%@ include file="/content/common/message.jsp" %>

<!--<table align="center" width="390" border="0" cellspacing="0" cellpadding="0" class="border">-->
    <!--<table width="390" align="center" class="table table-info mb30 table-hover"  >--> 
    <table class="table table-info mb30 table-hover"  >
    <tr>
    <td >From ServicePoint</td>  
    <td><select class="form-control" style="width:240px;height:40px"  name="servicePoint" >
    <option>-Select-</option>  
     <c:if test = "${servicePointList != null}" >
    <c:forEach items="${servicePointList}" var="company"> 
    <option value="<c:out value="${company.companyId}"/>"><c:out value="${company.companyName}"/></option>     
    </c:forEach>  
</c:if>  
</select></td>
<td class="text1">
<input type="button" align="center" name="search" class="btn btn-success"  value="Search" onclick="submitPage(servicePoint.value);"></td>
 </tr>
</table>
<br>
    <br>
         <% int index = 0;%>
        <c:if test = "${requestList != null}" >
          
    <!--<table align="center" width="400" border="0" cellspacing="0" cellpadding="0" class="border">-->
<table width="400" align="center" class="table table-info mb30 table-hover"  > 
    <thead
    <tr>
    <th  align="left" height="30">Request No</th>
    <th  align="left" height="30">To Service Point</th>
    <th  align="left" height="30">Required date</th>
    <th  align="left" height="30">Approve</th>
    </tr>
    </thead>
 
<c:forEach items="${requestList}" var="request"> 
      <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>
<tr>
<td class="<%=classText %>" height="30"><c:out value="${request.requestId}"/></td>
<td class="<%=classText %>"  height="30"><c:out value="${request.companyName}"/></td>
<td class="<%=classText %>" height="30"> <c:out value="${request.requiredDate}"/> </td>
<td class="<%=classText %>" height="30"> <a href="#" onClick="ApproveStock(<c:out value="${request.requestId}"/>);" >Approve</a></td>
</tr>
   <%
  index++;
  %>
</c:forEach>
<input type="hidden" name="requestId" value="" >
</table>
<br>
    <br>
</c:if>

<center>
</center>

</form>
   <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
</body>
 </div>
            </div>
            </div>
            <%@ include file="../common/NewDesign/settings.jsp" %>
