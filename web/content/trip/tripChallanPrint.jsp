<%-- 
    Document   : PrintPage
    Created on : Mar 16, 2016, 3:45:02 PM
    Author     : Gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">



<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Way Bill Paid Print</title>
        <%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
        <%@ taglib uri="http://java.sun.com/jstl/fmt" prefix="fmt" %>
        <%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
     
        <jsp:directive.page import="org.displaytag.sample.*" />
        <jsp:useBean id="now" class="java.util.Date" scope="request" />
    </head>

    <script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/css/parcelx.css" type="text/css" media="screen">
    <link rel="stylesheet" href="/throttle/css/rupees.css" type="text/css" media="screen">
    <script type="text/javascript">
            function printPage(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
            function goBack() {
                document.waybillPrint.action = '/throttle/handleWayBillPaid.do';
                document.waybillPrint.submit();

            }

        </script>


    </head>
    <body >
        <%--@ include file="/content/common/path.jsp" --%>
    <center>
        <%-- @include file="/content/common/message.jsp" --%>
    </center>
    <form name="waybillPrint">
        <input type="hidden" name="wayBillMode" id="wayBillMode" value="4"/>
        <input type="hidden" class="textbox" name="fromLocation" id="fromLocation" value="<%= session.getAttribute("branchId")%>"/>

      <%--  <c:if test = "${BilledDataPrint!= null}" >
            <c:forEach items="${BilledDataPrint}" var="data"> --%>
                <div id="printDiv" >

                    <table style="border:#000000 solid 1px; border-bottom:solid 1px;border-right:solid 1px;border-top:solid 1px;border-left:solid 1px;width: 880px;height:800px;; " align="center"  cellpadding="0"  cellspacing="0" >
                        <div></div>
                        <tr>
                             <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                            <td colspan="14" style="height:80px;width:150px;font-size:15px;font-weight:bold;text-align: center">
                                <!--<div style="padding:0px;margin:0px;width:100px;height:50px;float:left;display:block;text-indent:-9999999%;background:url(/throttle/images/ponpurelogo.png) no-repeat 0 0;height:50px;"></div>-->
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           <center><font size="1">SUBJECT TO DELHI JUDICIAL</font><br></center>
                        <font size="4"><b>DELHI INTERNATIONAL CARGO TERMINAL</b></font><br>
                        <center> <font size="2" >  (A Unit of International Cargo Terminal And Rail InfraStructure Pvt Ltd)<br>
                            Inland Container Depot  : Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101<br>
                            E-Mail:-transportdict@ict.in, &nbsp;&nbsp;Website:www.ict.in ,Tel:0130-3053400  </font></center></td>
                        </tr>
                        <tr>
                            <td colspan="13" style="height:35px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                <table style="height:70px;border:#000000 solid 1px; border-bottom:none;border-right:none;border-left: none " align="center" width="100%" cellpadding="0"  cellspacing="0" >
                                    <tr height="10" colspan="1"> <td style="border:#000000 solid 1px; border-top:none;border-left:none;font-size:12px;">&emsp;&emsp;Booking Office:DICT,Sonepat </td>
                                </table>
                            </td>

                            <td colspan="2" style="height:70px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                <table style="height:70px;border:#000000 solid 1px; border-bottom:none;border-right:none;border-left: none " align="center" width="100%" cellpadding="0"  cellspacing="0" >
                                    <tr height="10" colspan="1"> <td style="border:#000000 solid 1px; border-top:none;border-left:none;border-right:none;font-size:12px;">&emsp;SI NO: </td>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp;Challan Number :<c:out value='${challanNumber}'/>
                            </td>
                            <td colspan="5" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp;Date :<c:out value='${challanDate}'/>
                            </td>
                        </tr>
                        <tr>

                            <td colspan="6" style="height:40px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp;Truck No :<c:out value='${vehicleNo}'/>
                            </td>
                            <td colspan="5" style="height:40px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp;Route :<c:out value='${routeInfo}'/>
                            </td>
<!--                            <td colspan="5" style="height:40px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp;To :..................................
                            </td>-->
                        </tr>
                        </tr>

                        <tr height="25">
                            <td colspan="3" style="border:#000000 solid 1px;background-color:lightyellow; border-top:solid 1px;border-left:none;border-right:solid 1px;border-bottom:solid 2px;font-size:12px;font-weight:bold;text-align: center;">C.No</td>
                            <td colspan="2" style="border:#000000 solid 1px;background-color:lightyellow; border-top:solid 1px;border-left:none;border-right:solid 1px;border-bottom:solid 2px;font-size:12px;font-weight:bold;text-align: center;">Date</td>
                            <td colspan="5" style="border:#000000 solid 1px;background-color:lightyellow; border-top:solid 1px;border-left:none;border-right:solid 1px;border-bottom:solid 2px;font-size:12px;font-weight:bold;text-align: center;">Particulars</td>
                            <td colspan="1" style="border:#000000 solid 1px;background-color:lightyellow; border-top:solid 1px;border-left:none;border-right:solid 1px;border-bottom:solid 2px;font-size:12px;font-weight:bold;text-align: center;">No.Of.Packages</td>
                            <td colspan="2" style="border:#000000 solid 1px;background-color:lightyellow; border-top:solid 1px;border-left:none;border-right:solid 1px;border-bottom:solid 2px;font-size:12px;font-weight:bold;text-align: center;">Weight</td>
                            <td colspan="2" style="border:#000000 solid 1px;background-color:lightyellow; border-top:solid 1px;border-left:none;border-right:none;border-bottom:solid 2px;font-size:12px;font-weight:bold;text-align: center;">Remarks</td>
                        </tr> 
                        <tr height=""> 
                            <td colspan="3" style="border:#000000 solid 1px; border-top:none;border-bottom:solid 1px;border-left:none;font-size:12px;text-align:center"></td>
                            <td colspan="2" style="border:#000000 solid 1px; border-top:none;border-left:none;border-bottom:solid 1px;font-size:12px;text-align:center"></td>
                            <td colspan="5" style="border:#000000 solid 1px; border-top:none;border-left:none;border-bottom:solid 1px;font-size:12px;text-align:center"></td>
                            <td colspan="1" style="border:#000000 solid 1px; border-top:none;border-left:none;border-bottom:solid 1px;font-size:12px;text-align:center"></td>
                            <td colspan="2" style="border:#000000 solid 1px; border-top:none;border-left:none;border-bottom:solid 1px;font-size:12px;text-align:center"></td>
                            <td colspan="2" style="border:#000000 solid 1px; border-top:none;border-left:none;border-bottom:solid 1px;font-size:12px;text-align:center;border-right:none"></td>
                        </tr> 

                        <tr>
                            <td colspan="25" style="height:45px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp; Consignor Name :<c:out value='${customerName}'/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="25" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:solid 1px; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp; Consignee Name :<c:out value='${billingParty}'/>
                            </td>


                        <tr>
                            <td colspan="25" style="height:45px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp; Owner's Name :.....................................................................
                            </td>
                        </tr>
                        <tr>
                            <td colspan="25" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp; Address :.....................................................................
                            </td>
                        </tr>
                        <tr>
                            <td colspan="25" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp; Driver's Name :.....................................................................
                            </td>
                        </tr>
                        <tr>
                            <td colspan="25" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp; Address :.....................................................................
                            </td>
                        </tr>
                        <tr>
                            <td colspan="13" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:solid 1px; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                <table style="height:70px;border:#000000 ; border-bottom:none;border-right:none; border-top: solid 1px " align="center" width="100%" cellpadding="0"  cellspacing="0" >
                                    <tr>
                                        <td colspan="20" style="height:45px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;&emsp; License No :...................................
                                        </td>  
                                        <td colspan="20" style="height:45px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                             Issue Form :...............................................
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;&emsp; Ch.No :........................................
                                        </td>  
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                             En.No :....................................................
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;&emsp; Insurance No :.................................
                                        </td>  
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                             Place :....................................................
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;&emsp; Ins.Policy No :................................
                                        </td>  
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                             Valid Upto :...............................................
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;&emsp; Trough:........................................
                                        </td>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                             Phone :....................................................
                                        </td>
                                    </tr> 
                                </table>
                            </td> 
                            <td colspan="5" style="height:70px;width:100px;border:#000000 solid 1px;border-bottom:solid 1px; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                <table style="height:70px;border:#000000 solid 1px; border-bottom:none;border-right:none; " align="center" width="100%" cellpadding="0"  cellspacing="0" >
                                    <tr>
                                        <td colspan="20" style="height:45px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;Payment :.......................................
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;Total Freight :..................................
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;Less Advance :...................................
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;Balance :.........................................
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="20" style="height:30px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                            &emsp;Payable at :.......................................
                                        </td>
                                    </tr>
                                </table>
                                
                                <tr>
                            <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp;Driver Signature
                            </td>
                            <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp;LORRY HIRE COPY
                            </td>
                            <td colspan="4" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                                &emsp;&emsp;For 
                            </td>
                        </tr>
                            </td>
                        </tr>

                    </table>
                </div>
                <br>
                <center>
                    <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
                    <!--<input type="button" class="button" name="back" value="Back" onClick="//goBack();" > &nbsp;-->
                </center>
                <br>
                <br>
             <%--   </c:if>  --%>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

                </body>
                </html>