/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.fmsOperation.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.data.OperationDAO;
import ets.domain.trip.data.TripDAO;
import java.util.ArrayList;
import java.util.Iterator;
import ets.arch.util.FPUtil;
import java.io.IOException;
import java.util.Map;
import java.util.List;
import java.io.File;
import java.io.*;
import jxl.*;
import java.util.*;
import jxl.Workbook;
import jxl.read.biff.*;
import com.ibatis.sqlmap.client.*;
import ets.domain.fmsOperation.data.FMSOperationDAO;

/**
 *
 * @author vijay
 */
public class FMSOperationBP {

    static FPUtil fpUtil = FPUtil.getInstance();
    static final int poTextSplitSize = Integer.parseInt(fpUtil.getInstance().getProperty("PO_ADDRESS_SPLIT"));
    OperationDAO operationDAO;
    TripDAO tripDAO;

    FMSOperationDAO fmsOperationDAO;

    public FMSOperationDAO getFmsOperationDAO() {
        return fmsOperationDAO;
    }

    public void setFmsOperationDAO(FMSOperationDAO fmsOperationDAO) {
        this.fmsOperationDAO = fmsOperationDAO;
    }

    public OperationDAO getOperationDAO() {
        return operationDAO;
    }

    public void setOperationDAO(OperationDAO operationDAO) {
        this.operationDAO = operationDAO;
    }

    public TripDAO getTripDAO() {
        return tripDAO;
    }

    public void setTripDAO(TripDAO tripDAO) {
        this.tripDAO = tripDAO;
    }

// sql session object
    public SqlMapClient getSqlMapClient() {
        SqlMapClient session = null;
        session = fmsOperationDAO.getSqlMapClient();
        return session;
    }

    public ArrayList getBodyBillList(int jobcardId) {
        ArrayList getJobCardList = new ArrayList();
        getJobCardList = fmsOperationDAO.getBodyBillList(jobcardId);
        if (getJobCardList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return getJobCardList;
    }

    public ArrayList getContractJobcards() {
        ArrayList jcList = new ArrayList();
        jcList = fmsOperationDAO.getContractJobcards();
        return jcList;
    }

    public ArrayList processGetMfrList() throws FPRuntimeException, FPBusinessException {

        ArrayList MfrList = new ArrayList();
        MfrList = fmsOperationDAO.getMfrList();
        if (MfrList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return MfrList;
    }

    public ArrayList getServiceTypes() throws FPBusinessException, FPRuntimeException {
        ArrayList serviceTypes = new ArrayList();
        serviceTypes = fmsOperationDAO.getServiceTypes();
        if (serviceTypes.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return serviceTypes;
    }

    public ArrayList getServicePoints() throws FPBusinessException, FPRuntimeException {
        ArrayList servicePoints = new ArrayList();
        servicePoints = fmsOperationDAO.getServicePoints();
        if (servicePoints.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return servicePoints;
    }

    public ArrayList getServiceVendors() throws FPBusinessException, FPRuntimeException {
        ArrayList serviceVendors = new ArrayList();
        serviceVendors = fmsOperationDAO.getServiceVendors();
        return serviceVendors;
    }

    public String getVehicleRegNo(String vehicleId) {
        String result = "";
        result = fmsOperationDAO.getVehicleRegNo(vehicleId);
        return result;
    }

    public ArrayList getVehicleRegNoForJobCard(String vehicleNo) throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleRegNoForJobCard = new ArrayList();
        vehicleRegNoForJobCard = fmsOperationDAO.getVehicleRegNoForJobCard(vehicleNo);
        return vehicleRegNoForJobCard;
    }

    public ArrayList trailerNos() throws FPBusinessException, FPRuntimeException {
        ArrayList trailerNos = new ArrayList();
        trailerNos = fmsOperationDAO.trailerNos();
        return trailerNos;
    }

    public int insertJobCardWorkOrder(FMSOperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        String temp = operationTO.getReqDate();
        String[] date = temp.split("-");
        int hour = 12;
        if ((operationTO.getSec()).equals("1")) {
            //////System.out.println("hour" + operationTO.getHour());
            hour = hour + Integer.parseInt(operationTO.getHour());
            operationTO.setHour(String.valueOf(hour));
        }
        String reqdate = date[2] + "-" + date[1] + "-" + date[0] + " " + operationTO.getHour() + ":" + operationTO.getMinute() + ":00";
        operationTO.setReqDate(reqdate);
        temp = operationTO.getDateOfIssue();
        date = temp.split("-");
        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        operationTO.setDateOfIssue(dateofIssue);
        status = fmsOperationDAO.insertJobCardWorkOrder(operationTO);
        return status;

    }

    public int insertJobCardProblems(int secId, int probIds, String symptoms, int Severity, int jobcardId, int temp, int oldJobcardId) {
        int status = 0;
        status = fmsOperationDAO.insertJobCardProblems(secId, probIds, symptoms, Severity, jobcardId, temp, oldJobcardId);
        return status;
    }

    public ArrayList getEmailDetails(String activitycode) throws FPRuntimeException, FPBusinessException {
        ArrayList EmailDetails = new ArrayList();
        EmailDetails = fmsOperationDAO.getEmailDetails(activitycode);
        return EmailDetails;
    }

    public ArrayList getJobCardDetails(int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList jobCardList = new ArrayList();
        jobCardList = fmsOperationDAO.getJobCardDetails(jobcardId);
        return jobCardList;
    }

    public String getJobcardEmail() throws FPBusinessException, FPRuntimeException {
        String jobCardMailList = "";
        jobCardMailList = fmsOperationDAO.getJobcardEmail();
        return jobCardMailList;
    }

    public int insertSms(FMSOperationTO operationTO, int userId) throws FPRuntimeException, FPBusinessException {
        int insertStatus = 0;
        insertStatus = fmsOperationDAO.insertSms(operationTO, userId);
        return insertStatus;
    }

    public int getTotalJobcards(String jcno, String regno, String status, int operId, String fromDate, String toDate, int compId, String custType) {
        int totalRecords = 0;
        String[] temp1 = null;
        if (fromDate != "" && toDate != "") {
            temp1 = fromDate.split("-");
            String sDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
            fromDate = sDate;
            temp1 = toDate.split("-");
            String eDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
            toDate = eDate;
        }

        totalRecords = fmsOperationDAO.getTotalJobcards(jcno, regno, status, operId, fromDate, toDate, compId, custType);
        return totalRecords;

    }

    public ArrayList searchJobCardStatus(String jcno, String regno, String status, int operId, String fromDate, String toDate, int compId, int startIndex, int endIndex, String custType) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        String[] temp1 = null;
        if (fromDate != "" && toDate != "") {
            temp1 = fromDate.split("-");
            String sDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
            fromDate = sDate;
            temp1 = toDate.split("-");
            String eDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
            toDate = eDate;
        }
        scheduleWODetails = fmsOperationDAO.searchJobCardStatus(jcno, regno, status, operId, fromDate, toDate, compId, startIndex, endIndex, custType);

        if (scheduleWODetails.size() == 0) {
            //throw new FPBusinessException("EM-GEN-01");
        } else {
            FMSOperationTO operationTO = null;
            String reqDate = "";
            Iterator itr = scheduleWODetails.iterator();
            while (itr.hasNext()) {
                operationTO = new FMSOperationTO();
                operationTO = (FMSOperationTO) itr.next();
                reqDate = operationTO.getReqDate();

                if (!reqDate.equalsIgnoreCase("Not Scheduled")) {
                    String[] time = reqDate.split(" ");
                    String date = time[0];
                    String[] redate = date.split("-");
                    String temp = time[1];
                    String pcd = "";
                    time = null;
                    time = temp.split(":");
                    String tempDate = "";
                    int hour = Integer.parseInt(time[0]);
                    if (hour > 12) {
                        hour = hour - 12;
                        pcd = redate[2] + "-" + redate[1] + "-" + redate[0];
                        tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + String.valueOf(hour) + ":" + time[1] + "PM";

                        operationTO.setReqDate(tempDate);
                        operationTO.setPcd(pcd);
                    } else {
                        pcd = redate[2] + "-" + redate[1] + "-" + redate[0];
                        tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + time[0] + ":" + time[1] + "AM";
                        //////System.out.println("d& t" + tempDate);
                        operationTO.setReqDate(tempDate);
                        operationTO.setPcd(pcd);
                    }

                }

            }
        }
        return scheduleWODetails;

    }

    public int saveBodyWorksBill(String woId, String invoiceNo,
            String spareAmount, String vatAmount, String serviceAmount,
            String serviceTaxAmount, String remarks,
            String totalAmount, int userId, String invoiceDate) {

        int status = 0;

        status = fmsOperationDAO.saveBodyWorksBill(woId, spareAmount, vatAmount, serviceAmount,
                serviceTaxAmount, remarks, invoiceNo, totalAmount, userId, invoiceDate);

        return status;
    }

    public String getContractWos(int jobCardId) {
        ArrayList woList = new ArrayList();
        FMSOperationTO woNos;
        Iterator itr;
        String sugestions = "";
        woList = fmsOperationDAO.getContractWos(jobCardId);
        itr = woList.iterator();
        while (itr.hasNext()) {
            woNos = new FMSOperationTO();
            woNos = (FMSOperationTO) itr.next();
            if (sugestions.equals("")) {
                sugestions = "" + woNos.getWoId() + "-" + woNos.getWoId();
            } else {
                sugestions = sugestions + "~" + woNos.getWoId() + "-" + woNos.getWoId();
            }
        }
        return sugestions;
    }

    public ArrayList getProblemDetails(int workOrderId, int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = fmsOperationDAO.getProblemDetails(workOrderId, jobcardId);
        //

        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList jobCardProblemDetails(int jobcardId) {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = fmsOperationDAO.jobCardProblemDetails(jobcardId);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public float jobCardMrsDetails(int jobcardId) {
        ArrayList jobCardMrsDetails = new ArrayList();
        FMSOperationTO operationTO = null;
        jobCardMrsDetails = fmsOperationDAO.jobCardMrsDetails(jobcardId);

        int totReqQty = 0;
        int totIssQty = 0;
        float serviedRate = 0;

        if (jobCardMrsDetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        } else {

            Iterator itr = jobCardMrsDetails.iterator();
            while (itr.hasNext()) {
                operationTO = new FMSOperationTO();
                operationTO = (FMSOperationTO) itr.next();
                totReqQty += operationTO.getReqQty();
                totIssQty += operationTO.getIssQty();
            }
            //////System.out.println("totReqQty" + totReqQty);
            //////System.out.println("totIssQty" + totIssQty);
            serviedRate = (float) totIssQty / totReqQty;
            //////System.out.println("serviedRate" + serviedRate);

        }
        return (serviedRate * 100);
    }

    public ArrayList getBodyWorksList(int jobcardId) {
        ArrayList scheduledServices = new ArrayList();
        scheduledServices = fmsOperationDAO.getBodyWorksList(jobcardId);
        return scheduledServices;
    }

    public ArrayList getVehicleDetails(int vehicleId, int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = fmsOperationDAO.getVehicleDetails(vehicleId, jobcardId);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList getScheduledServices(int vehicleId, int jobcardId) {
        ArrayList scheduledServices = new ArrayList();
        scheduledServices = fmsOperationDAO.getScheduledServices(vehicleId, jobcardId);

        return scheduledServices;
    }

    public String getcomDate(int jobcardId) {
        String status = "";

        status = fmsOperationDAO.getcompDate(jobcardId);

        return status;
    }

    public ArrayList getWorkOrderDetails(int workOrderId) throws FPBusinessException, FPRuntimeException {
        ArrayList woDetails = new ArrayList();
        woDetails = fmsOperationDAO.getWorkOrderDetails(workOrderId);

        if (woDetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        } else {
            FMSOperationTO operationTO = null;
            String reqDate = "";
            Iterator itr = woDetails.iterator();
            if (itr.hasNext()) {
                operationTO = new FMSOperationTO();
                operationTO = (FMSOperationTO) itr.next();
                reqDate = operationTO.getReqDate();

                String[] time = reqDate.split(" ");

                String date = time[0];
                String[] redate = date.split("-");
                String temp = time[1];
                time = null;
                time = temp.split(":");
                String tempDate = "";
                int hour = Integer.parseInt(time[0]);
                if (hour > 12) {
                    hour = hour - 12;
                    tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + String.valueOf(hour) + ":" + time[1] + "PM";

                    operationTO.setReqDate(tempDate);
                } else {

                    tempDate = redate[2] + "-" + redate[1] + "-" + redate[0] + " " + time[0] + ":" + time[1] + "AM";
                    //////System.out.println("d& t" + tempDate);
                    operationTO.setReqDate(tempDate);
                }

            }
        }

        return woDetails;
    }

    public ArrayList getJobcardList(int vehicleId, int jobcardid) {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = fmsOperationDAO.getJobcardList(vehicleId, jobcardid);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList getProblemList(int vehicleId, int workOrderId) {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = fmsOperationDAO.getProblemList(vehicleId, workOrderId);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList getServiceList(int vehicleId, int km, int jobcardId) {
        ArrayList serviceList = new ArrayList();
        serviceList = fmsOperationDAO.getServiceList(vehicleId, km, jobcardId);
        if (serviceList.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return serviceList;
    }

    public ArrayList handleNonGracePeriodServices(int vehicleId, int km, ArrayList serviceList) throws FPBusinessException, FPRuntimeException {
        ArrayList NonGraceServices = new ArrayList();
        ArrayList newServiceList = new ArrayList();
        FMSOperationTO serviceTO = new FMSOperationTO();
        FMSOperationTO nonGraceServTO = new FMSOperationTO();
        Iterator serviceItr, nonGraceServiceItr;
        int count = 0;

        NonGraceServices = fmsOperationDAO.getNonGracePeriodServices(vehicleId, km);

        nonGraceServiceItr = NonGraceServices.iterator();

        while (nonGraceServiceItr.hasNext()) {
            nonGraceServTO = new FMSOperationTO();
            nonGraceServTO = (FMSOperationTO) nonGraceServiceItr.next();
            serviceItr = serviceList.iterator();
            while (serviceItr.hasNext()) {
                serviceTO = new FMSOperationTO();
                serviceTO = (FMSOperationTO) serviceItr.next();
                if (serviceTO.getServiceId() == nonGraceServTO.getServiceId()) {
                    count++;
                }
                System.out.print("included service=" + serviceTO.getServiceId() + "-");
                //////System.out.println("nonGraceServTO service="+nonGraceServTO.getServiceId());
            }
            if (count == 0) {
                newServiceList.add(nonGraceServTO);
            }
            count = 0;
        }
        System.out.println("new ArrayList=" + newServiceList.size());
        return NonGraceServices;
    }

    public ArrayList getExistingTechnicians(String jobCardId, String probId) throws FPRuntimeException, FPBusinessException {

        ArrayList existingTechnicians = new ArrayList();
        existingTechnicians = fmsOperationDAO.getExistingTechnicians(jobCardId, probId);
        return existingTechnicians;
    }

    public int saveJobCardTechnicians(String jobCardId, String probId,
            String[] technicianId, String[] estimatedHrs,
            String[] actualHrs, int userId) {
        int status = 0;
        for (int i = 0; i < technicianId.length; i++) {
            if (!"0".equals(technicianId[i])) {
                status = fmsOperationDAO.saveJobCardTechnicians(jobCardId, probId,
                        technicianId[i], estimatedHrs[i], actualHrs[i], userId);
            }
        }
        return status;
    }

    public String getDueHmKm(FMSOperationTO operationTO) {
        String mess = "";
        mess = fmsOperationDAO.getDueHmKm(operationTO);
        return mess;
    }

    public ArrayList processWoDetail(String woId) {

        ArrayList poDetail = new ArrayList();
        ArrayList newPoDetail = new ArrayList();
        FMSOperationTO purch = new FMSOperationTO();
        Iterator itr;
        poDetail = fmsOperationDAO.getWoDetail(woId);
        itr = poDetail.iterator();
        while (itr.hasNext()) {
            purch = new FMSOperationTO();
            purch = (FMSOperationTO) itr.next();
            purch.setAddress(addressSplit(purch.getVendorAddress()));
            newPoDetail.add(purch);
        }
        return newPoDetail;
    }

    public int insertExternalTechnician(int jobcardId, int probId, String techName, String compName, String contactNo, String labourCharge) {
        int status = 0;
        status = fmsOperationDAO.insertExternalTechnician(jobcardId, probId, techName, compName, contactNo, labourCharge);
        return status;
    }

    public int updateCompletionDate(int jobcardId, String CompletionDate, String jremarks) {
        int status = 0;

        String[] date = CompletionDate.split("-");

        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];

        //////System.out.println("processed Final jremarks:" + jremarks);
        status = fmsOperationDAO.updateCompletionDate(jobcardId, dateofIssue, jremarks);

        return status;
    }

    public int insertJobCardScheduleDetails(String[] sec, int userId, int jobcardId, String[] probId, String[] technicianId, String[] scheduledDate, String[] status, String[] cremarks, String[] cause, String[] remark, String[] index, String[] symptoms, String[] severity, String[] validate) {

        //////System.out.println("technicianId l=" + technicianId.length);
        //////System.out.println("index l=" + index.length);
        int pId = 0;
        int techId = 0;
        String[] temp1 = null;
        String stat = "";
        String remarks = "";
        int insertStatus = 0;
        String cau = "";
        String rema = "";
        String[] prob = null;
        String sym = "";
        int sev = 0;
        int value = 0;
        int temp = 1;
        int secId = 0;
        int vali = 0;
        if (status != null) {

            for (int i = 0; i < index.length; i++) {
                value = Integer.parseInt(index[i]);
                stat = status[value];
                remarks = cremarks[value];
                vali = Integer.parseInt(validate[value]);
                //////System.out.println("vali" + vali);
                if (vali == 0) {
                    //////System.out.println("vali" + vali);
                    prob = probId[value].split("-");
                    pId = Integer.parseInt(prob[0]);
                    //////System.out.println("vali" + vali);
                } else {
                    pId = Integer.parseInt(probId[value]);
                }

                String pcDate = "";

                if (vali == 0) {
                    sev = Integer.parseInt(severity[value]);
                    sym = symptoms[value];
//                    insertStatus = fmsOperationDAO.insertJobCardProblems(secId, pId, sym, sev, jobcardId, temp);
                    insertStatus = fmsOperationDAO.insertJobCardProblems(secId, pId, sym, sev, jobcardId, 0, 0);
                }
                secId = Integer.parseInt(sec[value]);
                if (scheduledDate[value] != "" && scheduledDate[value] != null) {
                    //////System.out.println("date=" + scheduledDate[value]);
                    if (secId != 1016) {
                        //////System.out.println("techId=" + technicianId[value]);
                        techId = Integer.parseInt(technicianId[value]);
                    } else {
                        techId = 1;
                    }
                    temp1 = scheduledDate[value].split("-");
                    pcDate = temp1[2] + "-" + temp1[1] + "-" + temp1[0];
                    //////System.out.println("stat" + stat);
                    if (stat.equals("P")) {
                        //insert                                          
                        //////System.out.println("in insert");
                        //////System.out.println("pId" + pId);
                        //////System.out.println("remarks:" + remarks);

                        insertStatus = fmsOperationDAO.insertJobCardScheduleDetails(jobcardId, pId, pcDate, techId, stat, remarks, userId);

                    } else if (stat.equals("C")) {
                        //update                        
                        insertStatus = fmsOperationDAO.updateJobCardScheduleDetails(pcDate, jobcardId, pId, techId, stat, remarks, cau, rema);

                    }
                }

            }

        }
        return insertStatus;

    }

    public int insertJobCardServices(int currentkm, int currenthm, int userId, int jobcardId, String[] serviceindex, String[] serviceId, String[] servicedDate, String[] serviceTech, String[] serviceStatus, int vehicleId) {

        int temp = 0;
        String stat = "";
        int insertStatus = 0;
        int sId = 0;
        int techId = 0;
        String[] date = null;
        String dateofIssue = "";
        //////System.out.println("serviceStatus.len=" + serviceStatus.length);
        //////System.out.println("serviceindex.len=" + serviceindex.length);
        for (int i = 0; i < serviceindex.length; i++) {
            temp = Integer.parseInt(serviceindex[i]);
            //////System.out.println("inde value=" + temp);
            sId = Integer.parseInt(serviceId[temp]);
            stat = serviceStatus[temp];
            if (stat.equals("P") || stat.equals("C")) {
                techId = Integer.parseInt(serviceTech[temp]);
                date = servicedDate[temp].split("-");
                dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
                insertStatus = fmsOperationDAO.insertJobCardServiceDetails(jobcardId, sId, dateofIssue, techId, stat, userId);
                if (stat.equals("C")) {
                    insertStatus = fmsOperationDAO.insertJobCardServices(userId, jobcardId, sId, currentkm, currenthm, vehicleId);
                }
            }
        }
        return insertStatus;

    }

    public int isThisProblemExist(int jobcardId, int probId) {
        int status = 0;
        status = fmsOperationDAO.isThisProblemExist(jobcardId, probId);
        return status;

    }

    public ArrayList getVendorDetails() {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = fmsOperationDAO.getVendorDetails();
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList getTrailerDetails(int vehicleId, int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = fmsOperationDAO.getTrailerDetails(vehicleId, jobcardId);
        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList processJcVehDetail(int jobcardId) {
        ArrayList list = new ArrayList();

        list = fmsOperationDAO.getJcVehDetail(jobcardId);

        return list;
    }

    public ArrayList getWoProblemDetails(int workOrderId, int jobcardId) throws FPBusinessException, FPRuntimeException {
        ArrayList scheduleWODetails = new ArrayList();
        scheduleWODetails = fmsOperationDAO.getWoProblemDetails(workOrderId, jobcardId);
        //

        if (scheduleWODetails.size() == 0) {
            // throw new FPBusinessException("EM-MRS1-05");
        }
        return scheduleWODetails;
    }

    public ArrayList processServiceDetails(int jobcardId) {
        ArrayList Details = new ArrayList();
        Details = fmsOperationDAO.getServiceDetails(jobcardId);
        return Details;
    }

    public ArrayList sortPrintJobcardData(ArrayList problemList) {
        Iterator itr = problemList.iterator();
        ArrayList newProblemList = new ArrayList();
        String[] problemName, symptoms;
        FMSOperationTO oper;
        while (itr.hasNext()) {
            oper = new FMSOperationTO();
            oper = (FMSOperationTO) itr.next();
            problemName = longTextSplit(oper.getProbName(), 28);
            symptoms = longTextSplit(oper.getSymptoms(), 28);
            oper.setProblemNameSplit(problemName);
            oper.setSymptomsSplit(symptoms);
            newProblemList.add(oper);
        }
        return newProblemList;
    }

    public String getJobCardOwner(int jobcardId) {
        String jobCardOwner = fmsOperationDAO.getJobCardOwner(jobcardId);
        return jobCardOwner;
    }

    public int insertServicedDetails(int userId, int jobcardId, int serviceId, int km, int hms, int vehicleId, String servicedDate) {
        int status = 0;

        String[] date = servicedDate.split("-");

        String dateofIssue = date[2] + "-" + date[1] + "-" + date[0];
        status = fmsOperationDAO.insertServicedDetails(userId, jobcardId, serviceId, km, hms, vehicleId, dateofIssue);

        return status;

    }

    // this function is used to change job card status from completed to planned
    public int changeJobCardStatus(FMSOperationTO operationTO) throws FPBusinessException, FPRuntimeException {
        int status = 0;
        status = fmsOperationDAO.changeJobCardStatus(operationTO);
        return status;
    }

    public String[] addressSplit(String longText) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = poTextSplitSize;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

    public int getVehicleId(String regNo) {
        int status = 0;
        status = fmsOperationDAO.getVehicleId(regNo);
        return status;
    }

    public String[] longTextSplit(String longText, int spltSize) {
        String[] splitText = null;
        int lower = 0, higher;
        int splitSize = spltSize;
        int size = longText.length();
        splitText = new String[(size / splitSize) + 1];
        for (int i = 0; i < (size / splitSize) + 1; i++) {
            if ((size - lower) < splitSize) {
                higher = (size);
            } else {
                higher = lower + splitSize;
            }
            splitText[i] = longText.substring(lower, higher);
            System.out.println(splitText[i]);
            lower = lower + splitSize;
        }
        return splitText;
    }

}
