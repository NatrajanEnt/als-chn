<%--
    Document   : MVS
    Created on : Nov 21, 2012, 4:48:05 PM
    Author     : DINESHKUMAR.S
--%>
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
         <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true
                });
            });
            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
    </head>
    <script type="text/javascript">
        function submitPage(value)
        {
           if(textValidation(document.MVS.vendorName,'vendorName')){
                return;
            }
            else if(textValidation(document.MVS.fromDate,'FromDate')){
                return;
            }
            else if(textValidation(document.MVS.toDate,'ToDate')){
                return;
            }

            document.MVS.action="/throttle/marketVendorList.do";
            document.MVS.submit();

        }

        function setFocus() {

            var regno='<%=request.getAttribute("regno")%>';
            var regno='<%=request.getAttribute("vendorName")%>';


            if(regno!='null'){
                document.MVS.regno.value=regno;
                document.MVS.vendorName.value=vendorName;
            }
        }

        function displayCollapse(){
            if(document.getElementById("exp_table").style.display=="block"){
                document.getElementById("exp_table").style.display="none";
                document.getElementById("openClose").innerHTML="Open";
            } else {
                document.getElementById("exp_table").style.display="block";
                document.getElementById("openClose").innerHTML="Close";
            }
        }      

    </script>
</head>
<body onload="getVehicleNos();">
    <form name="MVS">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>
        <table width="700" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                    </h2></td>
                <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
            </tr>
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:700px;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1">Search Market Vehicle</li>
                        </ul>
                        <div id="first">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                <tr>
                                    <c:if test = "${vendorList != null}" >

                                        <td class="text" height="30"><font color="red">*</font>Vendor Name</td>
                                        <td class="text">
                                            <select class="textbox" name="vendorName" id="vendorName" >
                                                <option value='0'>-Select-</option>
                                                <c:forEach items="${vendorList}" var="vendor">
                                                    <c:if test="${vendor.vendorTypeId == 1001}">
                                                    <option value="<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.vendorName}"/>"><c:out value="${vendor.vendorName}"/></option>
                                               </c:if>
                                                </c:forEach>
                                            </select>
                                        </td>
                                    </c:if>
                                    <td>Vehicle No</td>
                                    <td><input name="regno" id="regno" type="text" class="textbox" size="20" value="" autocomplete="off"></td>
                                </tr>
                                <tr>
                                    <td  height="30"><font color="red">*</font>From Date</td>
                                    <td  height="30"><input type="text" name="fromDate" class="datepicker" ></td>
                                    <td ><font color="red">*</font>To Date</td>
                                    <td ><input type="text" name="toDate" class="datepicker" ></td>

<!--                                    <td height="30"><font color="red">*</font>From Date</td>
                                    <td height="30"><input type="text" name="fromDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.MVS.fromDate,'dd-mm-yyyy',this)"/></td>
                                    <td><font color="red">*</font>To Date</td>
                                    <td><input type="text" name="toDate" class="textbox" ><img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.MVS.toDate,'dd-mm-yyyy',this)"/></td>-->
                                    <td><input type="button" class="button" name="search"  onclick="submitPage(this);" value="Search"></td>
                                </tr>
                            </table>
                        </div></div>
                </td>
            </tr>
        </table>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <script type="text/javascript">
            //window.onload = getVehicleNos;
        function getVehicleNos(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
        }
        </script>
</body>
</html>
