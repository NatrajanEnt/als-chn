/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.trip.web;

import java.net.*;
import java.io.*;
import java.text.*;

import java.sql.SQLException;
import java.text.ParseException;
import org.json.JSONObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
//import junit.swingui.StatusLine;
import org.json.JSONArray;
import org.json.simple.parser.JSONParser;

public class thrERPTableSettlementPosting extends Thread {
//public class thrERPTablePosting {

    String tripId = null;

    public thrERPTableSettlementPosting(String tripSheetId) throws ParseException, ClassNotFoundException, SQLException, org.json.simple.parser.ParseException {
        tripId = tripSheetId;
    }
//    public static void main(String args[]) throws ParseException, ClassNotFoundException, SQLException {
//        thrERPTablePosting program = new thrERPTablePosting();
//        program.run();
//    }

    public void run() {
        try {
            Connection con = null;
            Statement statement = null;
            Statement statement1 = null;
            ResultSet rs = null;
            ResultSet rs1 = null;
            ResultSet rs2 = null;
            ResultSet rs3 = null;

            ResultSet rs4 = null;
            ResultSet rs5 = null;
            ResultSet rs6 = null;
            ResultSet rs7 = null;
            ResultSet rs8 = null;
            ResultSet rs9 = null;

            String fileName = "jdbc_url.properties";
            InputStream is = getClass().getResourceAsStream("/" + fileName);
            Properties dbProps = new Properties();

            try {
                dbProps.load(is);//this may throw IOException
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("ex.printStackTrace()");
            }

            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            try {

                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                PreparedStatement getDKTDetails = null;
                PreparedStatement updateDKTDetails = null;
                PreparedStatement getTxnDetails = null;
                PreparedStatement updateTxnDetails = null;
                PreparedStatement updateCreditTxnDetails = null;
                PreparedStatement getTxnDetails2 = null;
                PreparedStatement getTxnDetails3 = null;
                PreparedStatement getTxnDetails4 = null;

                //NEW
                PreparedStatement getTxnDetails1 = null;
                PreparedStatement getTxnFuelDetails = null;
                PreparedStatement getTxnFastTag = null;
                PreparedStatement getTxnFuelCash = null;
                PreparedStatement insertTxnDetails = null;
                PreparedStatement getExistsTxnDetails = null;
                PreparedStatement getNormalCreditExpenseList = null;
                PreparedStatement getTxnFuelSubCodeList = null;
                PreparedStatement getExistsNormalTxnDetails = null;

                ArrayList getExistsTxnList = new ArrayList();
                ArrayList getSubExpenseList = new ArrayList();
                ArrayList getExistsNormalExpTxnList = new ArrayList();

                statement = con.createStatement();

                String sql = "SELECT a.trip_code,a.trip_Id,DATE_FORMAT(a.trip_actual_start_date,'%Y%m%d') AS jobstartdate,DATE_FORMAT(trip_actual_end_date,'%Y%m%d') AS jobenddate,\n"
                        + "(select ifnull(DivCharge,0) from ts_trip_diversion dt where a.trip_id= dt.trip_id limit 1)as divertCharge,\n"
                        + "(select ifnull(OTCharges,0) from ts_trip_over_tonnage ot where a.trip_id= ot.trip_id limit 1)as oTCharges,\n"
                        + "(SELECT mapping_Id FROM ts_cities WHERE city_Id=(select tr.actual_point_id from  ts_trip_route_course tr where tr.trip_id = a.trip_id and point_sequence=1) LIMIT 1) AS cityfromcode,\n"
                        + "if((select count(*) from  ts_trip_route_course tr where tr.trip_id =a.trip_id)>2,\n"
                        + "(SELECT mapping_Id FROM ts_cities WHERE city_Id=(select tr.actual_point_id from  ts_trip_route_course tr where tr.trip_id = a.trip_id and point_sequence =2) LIMIT 1),null) AS cityTocode,\n"
                        + "if((select count(*) from  ts_trip_route_course tr where tr.trip_id =a.trip_id)>2,\n"
                        + "(SELECT mapping_Id FROM ts_cities WHERE city_Id=(select tr.actual_point_id from  ts_trip_route_course tr where tr.trip_id = a.trip_id and point_sequence =(select count(*) from  ts_trip_route_course tr where tr.trip_id =a.trip_id)) LIMIT 1),\n"
                        + "(SELECT mapping_Id FROM ts_cities WHERE city_Id=(select tr.actual_point_id from  ts_trip_route_course tr where tr.trip_id = a.trip_id and point_sequence=2) LIMIT 1)) AS cityReturncode,\n"
                        + "estimated_km AS distance,IF(vehicle_vendor_Id !=1,'Hired','Owned') AS ownedhired,b.regno AS hiredvehicleno,\n"
                        + "IFNULL((SELECT sum(container_freight_charges) FROM  ts_consignment_container_details cn,ts_trip_consignment tc \n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id),0.00) AS hireCharges,\n"
                        + "customerName AS partyName,\n"
                        + "(SELECT Erp_Id FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS AccCode,\n"
                        + "(select  d.allocated_fuel from ts_trip_fuel_recovery d where a.trip_id =d.trip_id) AS dieselsupplies,\n"
                        + "(select  d.consumed_fuel from ts_trip_fuel_recovery d where a.trip_id =d.trip_id) AS dieselConsumed,\n"
                        + "(select  d.recovery_fuel from ts_trip_fuel_recovery d where a.trip_id =d.trip_id) AS dieselRecovered,\n"
                        + "trip_start_km AS openingkm,\n"
                        + "trip_end_km AS closingkm,total_km_run AS totalkmrun,(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=1) AS size_20,\n"
                        + "(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=2 AND container_category NOT IN (6,2,3)) AS size_40,\n"
                        + "(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=2 AND container_category=6) AS size_40HC,\n"
                        + "(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=2 AND container_category=2) AS size_40FT,\n"
                        + "(SELECT COUNT(*) FROM ts_trip_container WHERE a.trip_Id=trip_Id AND container_type_Id=2 AND container_category=3) AS size_40OT,\n"
                        + "IFNULL((SELECT NetWt FROM ts_trip_over_tonnage ot where ot.trip_Id=a.trip_Id  limit 1),0) AS netwt,\n"
                        + "IFNULL((SELECT ActWt FROM ts_trip_over_tonnage ot where ot.trip_Id=a.trip_Id  limit 1),0) AS grosswt,\n"
                        + "(SELECT if(container_type_id = 1,'20','40') FROM ts_trip_container WHERE a.trip_Id=trip_Id limit 1) AS container_type,\n"
                        + "(SELECT mt.is_Export  FROM  ts_consignment_note cn,ts_trip_consignment tc,ts_movement_type_master mt WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id and mt.id = cn.movement_type) AS isExport,\n"
                        + "(SELECT movement_type  FROM  ts_consignment_note cn,ts_trip_consignment tc WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id) AS movement,\n"
                        + "(SELECT mt.movement_type  FROM  ts_consignment_note cn,ts_trip_consignment tc,ts_movement_type_master mt WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id and mt.id = cn.movement_type) AS MovType,\n"
                        + "(select erp_id from papl_vehicle_master vm where vm.vehicle_Id=b.vehicle_Id) as trailerCode,0 as consigneeCode,\n"
                        + "(select erp_Id from papl_emp_master em where em.emp_Id=c.emp_Id) as DriverCode,\n"
                        + "(select erp_Id from papl_emp_master em where em.emp_Id=c.cleaner_Id) as CleanerCode,\n"
                        + "invoice_No,IFNULL((SELECT sum(container_freight_charges) FROM  ts_consignment_container_details cn,ts_trip_consignment tc\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id),0.00)+ifnull((select sum(expense_value) from ts_trip_expense where trip_Id=a.trip_Id and expense_type=1),0.00)\n"
                        + "+ifnull((SELECT divCharge FROM ts_trip_diversion dt where dt.trip_Id=a.trip_id limit 1),0.00)+ifnull((SELECT OTCharges FROM ts_trip_over_tonnage ot where ot.trip_Id=a.trip_Id and submit_status = 1 limit 1),0.00)as invoice_value,\n"
                        + "IF(vehicle_vendor_Id !=1,(select erp_Id from papl_vendor_master where vendor_Id=vehicle_vendor_Id),'DC001') AS subCode,\n"
                        + "estimated_revenue,\n"
                        + "ifnull((select if(expense_type = 1,DATEDIFF(trip_actual_end_date, trip_actual_start_date),0) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),0) as haltingDay,\n"
                        + "ifnull((select if(expense_type = 1,if(halt_contract_amt >0,DATE_FORMAT(a.haltStartDate,'%Y%m%d'),null),null) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),null) AS haltingFrom,\n"
                        + "ifnull((select if(expense_type = 1,if(halt_contract_amt >0,DATE_FORMAT(a.haltEndDate,'%Y%m%d'),null),null) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),null) AS  haltingTo,\n"
                        + "ifnull((select if(expense_type = 1,expense_value,0) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),0) as haltingAmt,\n"
                        + "ifnull((select if(expense_type = 1,a.halt_contract_amt,0) from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),0) as HaltCntrctAmnt,\n"
                        + "ifnull((select if(expense_type = 1,if(halt_contract_amt >0,a.halt_days,0),0)from ts_trip_expense where expense_Id=1021 and trip_Id=a.trip_Id and approval=1),0) AS halt_days,\n"
                        + "ifnull((select if(expense_type = 1,if(clr_contract_amt >0,DATE_FORMAT(a.clrStartDate,'%Y%m%d'),null),null) from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id and approval=1),null) AS clrFrom,\n"
                        + "ifnull((select if(expense_type = 1,if(clr_contract_amt >0,DATE_FORMAT(a.clrEndDate,'%Y%m%d'),null),null) from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id and approval=1),null) AS  clrTo,\n"
                        + "ifnull((select if(expense_type = 1,expense_value,0) from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id and approval=1),0) as clrAmt,\n"
                        + "ifnull((select if(expense_type = 1,a.clr_contract_amt,0) from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id and approval=1),0) as clrCntrctAmnt,\n"
                        + "ifnull((select if(expense_type = 1,if(clr_contract_amt >0,a.clr_days,0),0)from ts_trip_expense where expense_Id=1049 and trip_Id=a.trip_Id and approval=1),0) AS clr_days,\n"
                        + "1 as TrpEndORClsr,\n"
                        + "(SELECT if(tally_billing_type = 1,'RCM','BOS') FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS TrpInvType,\n"
                        + "(SELECT concat(if(trip_type = 1,'Load','Empty'),' ',\n"
                        + "(SELECT if(container_type_id = 1,'20','40') FROM ts_trip_container WHERE a.trip_Id=trip_Id limit 1),' ',\n"
                        + "if(movement_Type in(1,2,18,19,20,21,24,25,26,27,28,29,30,31,32,33,34),'MOFUSSIL','LOCAL')) FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS TripType,\n"
                        + "(select ifnull(job_ref_no,0)as job_no FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS JobNo,\n"
                        + "(select if(movement_Type =1,shiping_line_no,if(movement_Type =2,customer_order_reference_no,0)) FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS BESBNo,\n"
                        + "(select ifnull(bill_of_entry,0)as job_no FROM  ts_consignment_note cn,ts_trip_consignment tc,papl_customer_master cm\n"
                        + "WHERE cn.consignment_order_Id=tc.consignment_order_Id AND tc.trip_Id=a.trip_Id AND cm.cust_Id=cn.customer_Id) AS BLBKGNo,\n"
                        + "now() as runDate\n"
                        + "FROM ts_trip_master a,ts_trip_vehicle b,ts_trip_driver c,papl_vehicle_type_master v\n"
                        + "WHERE a.comp_Id=1461 AND a.trip_Id=b.trip_Id AND a.trip_Id=c.trip_Id \n"
                        + "AND b.trip_Id=c.trip_Id \n"
                        + "AND b.Active_ind='Y' AND c.Active_ind='Y' AND  v.vehicle_type_Id= a.vehicletypeid\n"
                        + "AND a.trip_Id=?";

                String sql1 = "update Dkt set \n"
                        + "JobStartDate = ?,JobEndDate =?,CityFromCode=?,CityToCode=?,Distance=?,Owned_Hired=?,HiredVehicleNo=?,HireCharges=?,Partyname=?,AccCode=?,\n"
                        + "DieselSupplies = ?,DieselConsumed =?,OpeningKM =?,ClosingKM = ?,TotalKMRun=?,Size_20=?,Size_40=?,Size_40HC=?,Size_FT=?,Size_OT=?,\n"
                        + "NetWt=?,Closed=?,CompanyCode=?,BrnCd=?,RUNDATE=?,JobReachedDate=?,JobDate=?,DieselRecovered=?,IsExport=?,TrlJobId=?,post_ind=1,TrailerCode=?,\n"
                        + "ConsigneeCode=?, DriverCode=?, CleanerCode=?,  CityReturnedCode=?,SubCode=?,\n"
                        + "InvNo =?, InvAmount=?,FactoryHaltFromDate=?,FactoryHaltToDate=?,ChennaiHaltFromDate=?,ChennaiHaltToDate=?,\n"
                        + "MofussilHaltingDays=?,LocalHaltingDays=?,moffisalhalting_amt=?,localhalting_amt=?,\n"
                        + "HaltingDays=?,HaltCntrctAmnt=?,TrpEndORClsr=?,TrpInvType=?,tripType=?,GrossWt=?,"
                        + "JobNo=?,BESBNo=?,BLBKGNo=?,divCharge=?,OTCharge=?,MovType =?\n"
                        + "where TrlJobId = ?";

                String sql2 = "SELECT te.expense_id as expense_id,'JV' AS txnType,@acount:=@acount+1 serial_number,te.trip_Id,(select created_On from ts_trip_status_details where trip_Id=tm.trip_Id and status_Id=13 limit 1) as expense_date,\n"
                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
                        + "CONCAT('EXP INC VEH#',SUBSTRING(reg_no,-4,4),' AT TMP ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration1,\n"
                        + "CONCAT('PD ',alise_Name, ' AT ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration2,\n"
                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
                        + "as DriverLc\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n"
                        + "AND tm.destination_Id=tc.city_Id and tm.trip_Id=? and expense_value !=0 order by expense_id asc";

                String sql3 = "update Transactions set\n"
                        + "TRXTYP = ?,DOCDATE=?,\n"
                        + "DEBIT=?,CREDIT=?,post_ind =?,\n"
                        + "TrpEndORClsr=? \n"
                        + "where TrlJobId=? and expense_id=?";
                
                String sql17 = "update Transactions set\n"
                        + "TRXTYP = ?,DOCDATE=?,\n"
                        + "DEBIT=?,CREDIT=?,post_ind =?,\n"
                        + "TrpEndORClsr=? \n"
                        + "where TrlJobId=? and expense_id=?";

                // NEW INSERT Transaction
                String sql4 = "SELECT te.expense_id as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,te.trip_Id,(te.expense_date) as expense_date,\n"
                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
                        + "CONCAT('EXP INC VEH#',SUBSTRING(reg_no,-4,4),' AT TMP ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration1,\n"
                        + "CONCAT('PD ',alise_Name, ' AT ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration2,\n"
                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
                        + "as DriverLc\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n"
                        + "AND tm.destination_Id=tc.city_Id and tm.trip_Id=? and expense_value !=0 and expense_id not in(1010,1047,1011)  order by te.expense_id desc";

                String sql6 = "SELECT 'JV' AS txnType,@acount:=@acount+1 serial_number,if(fe.fuel_locat_id = 21,'','DC')as CHQNO, acc_code,sub_code,sum(fuelAmount) AS debit,\n"
                        + "FuelDateTime as fuel_date,\n"
                        + "concat((select replace(replace(replace(rg.reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no rg \n"
                        + "where rg.vehicle_Id=v.vehicle_Id AND rg.active_Ind='Y'),\n"
                        + "'-',(tm.customername),'-',\n"
                        + "(select city_name from ts_cities where city_id in\n"
                        + "(select actual_point_id  from ts_trip_route_course r where  r.trip_id = tf.tripid and\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)))as narration1,\n"
                        + "concat('Slip No.',tf.slipno,' & Date ',DATE_FORMAT(tf.FuelDateTime ,'%d-%m-%Y'))as narration2,\n"
                        + "(select replace(replace(replace(reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no r where r.vehicle_Id=v.vehicle_Id AND r.active_Ind='Y')as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from\n"
                        + "papl_emp_master em where em.emp_Id=d.emp_Id)as DriverLc \n"
                        + "FROM ra_fuel_location_master_epos fe,\n"
                        + "ra_trip_fuel_epos tf,ts_trip_driver d ,ts_trip_vehicle v,ts_trip_master tm \n"
                        + "where fe.fuel_locat_id = tf.fuel_locat_id and v.trip_id = tf.tripid and d.trip_id = tf.tripid \n"
                        + "and tm.trip_id =tf.tripid and  tf.tripid=? and tf.fuel_locat_id !=21 group by tf.fuel_locat_id";

                String sql7 = "SELECT te.expense_id as expenseId,@acount:=@acount+1 serial_number,'' as CHQNO,'JV' AS txnType,te.trip_Id,(te.expense_date) as expense_date,\n"
                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
                        + "concat(replace(replace(replace(reg_No,' ',''),'-',''),' ',''),\n"
                        + "'-',(tm.customername),'-',\n"
                        + "(select city_name from ts_cities where city_id in\n"
                        + "(select actual_point_id  from ts_trip_route_course r where\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)))as narration1,\n"
                        + "(select concat('Slip No.',tf.slipno,' & Date ',DATE_FORMAT(tf.FuelDateTime ,'%d-%m-%Y')) from ra_trip_fuel_epos tf \n"
                        + "where tf.tripid = tm.trip_id limit 1)as narration2,\n"
                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
                        + "as DriverLc\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n"
                        + "AND tm.destination_Id=tc.city_Id and tm.trip_Id=? and expense_value !=0 and expense_id in(1047) order by te.expense_id desc";

                String sql14 = "SELECT te.expense_id as expenseId,@acount:=@acount+1 serial_number,'' as CHQNO,'JV' AS txnType,te.trip_Id,(te.expense_date) as expense_date,\n"
                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
                        + "concat(replace(replace(replace(reg_No,' ',''),'-',''),' ',''),\n"
                        + "'-',(tm.customername),'-',\n"
                        + "(select city_name from ts_cities where city_id in\n"
                        + "(select actual_point_id  from ts_trip_route_course r where\n"
                        + "r.trip_id = tm.trip_id and point_sequence =2)))as narration1,\n"
                        + "CONCAT('From ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' To ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y')) AS narration2,\n"
                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
                        + "as DriverLc\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n"
                        + "AND tm.destination_Id=tc.city_Id and expense_value !=0 and expense_id in(1011) and tm.trip_Id=? order by te.expense_id desc";

                String sql5 = "insert into Transactions\n"
                        + "(TRXTYP,SRLNO,DOCDATE,ACCCODE,SUBCODE,NARRATION1,NARRATION2,DEBIT,CREDIT,CompanyCode,TrlJobId,post_ind,VehicleNo,licenseNo,CHQNO,expense_id,TrpEndORClsr) Values\n"
                        + "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

                String sql15 = "select sub_code from ra_fuel_location_master_epos where sub_Code is not null and sub_code = ?";

//                for update Transaction Table starts
                String sql8 = "select expense_id from Transactions where  Trljobid = ? order by expense_id asc";

                String sql16 = "select expense_id from Transactions where  Trljobid = ? and expense_id not in(1010,1047,1011,2,3,4) order by expense_id asc";

//                String sql9 = "SELECT te.expense_id as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,te.trip_Id,(select created_On from ts_trip_status_details where trip_Id=tm.trip_Id and status_Id=13 limit 1) as expense_date,\n"
//                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
//                        + "CONCAT('EXP INC VEH#',SUBSTRING(reg_no,-4,4),' AT TMP ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration1,\n"
//                        + "CONCAT('PD ',alise_Name, ' AT ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration2,\n"
//                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
//                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
//                        + "as DriverLc\n"
//                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n"
//                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
//                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n"
//                        + "AND tm.destination_Id=tc.city_Id and tm.trip_Id=? and expense_value !=0 and te.expense_id not in(select expense_id from Transactions where trljobid=?)  order by te.expense_id desc";
                String sql9 = "SELECT te.expense_id as expense_Id,@acount:=@acount+1 serial_number,'' as CHQNO,'JV' AS txnType,te.trip_Id,expense_value AS debit,\n" 
                            + "(te.expense_date) as expense_date,erp_Id AS AccCode,erp_subCode AS subCode,\n" 
                            + "CONCAT('EXP INC VEH#',SUBSTRING(reg_no,-4,4),' AT TMP ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration1,\n" 
                            + "CONCAT('PD ',alise_Name, ' AT ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration2,\n" 
                            + "replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n" 
                            + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n" 
                            + "as DriverLc\n" 
                            + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n" 
                            + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n" 
                            + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n" 
                            + "AND tm.destination_Id=tc.city_Id and tm.trip_Id=? and expense_value !=0\n" 
                            + "and  te.expense_id not in(select expense_id from Transactions where trljobid=?) and  te.expense_id not in(1010)\n" 
                            + "union\n" 
                            + "SELECT '1010' as expenseId ,2 as serial_number,if(fe.fuel_locat_id = 21,'','DC')as CHQNO,'JV' AS txnType,tm.trip_id,sum(fuelAmount) AS debit,\n" 
                            + "FuelDateTime as expense_date,acc_code,sub_code,\n" 
                            + "concat((select replace(replace(replace(rg.reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no rg\n" 
                            + "where rg.vehicle_Id=v.vehicle_Id AND rg.active_Ind='Y'),\n" 
                            + "'-',(tm.customername),'-',\n" 
                            + "(select city_name from ts_cities where city_id in\n" 
                            + "(select actual_point_id  from ts_trip_route_course r where  r.trip_id = tf.tripid and\n" 
                            + "r.trip_id = tm.trip_id and point_sequence =2)))as narration1,\n" 
                            + "concat('Slip No.',tf.slipno,' & Date ',DATE_FORMAT(tf.FuelDateTime ,'%d-%m-%Y'))as narration2,\n" 
                            + "(select replace(replace(replace(reg_No,' ',''),'-',''),' ','') from papl_vehicle_reg_no r where r.vehicle_Id=v.vehicle_Id AND r.active_Ind='Y')as reg_no,\n" 
                            + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from\n" 
                            + "papl_emp_master em where em.emp_Id=d.emp_Id)as DriverLc\n" 
                            + "FROM ra_fuel_location_master_epos fe,\n" 
                            + "ra_trip_fuel_epos tf,ts_trip_driver d ,ts_trip_vehicle v,ts_trip_master tm ,ts_trip_expense te\n" 
                            + "where fe.fuel_locat_id = tf.fuel_locat_id and v.trip_id = tf.tripid and d.trip_id = tf.tripid\n" 
                            + "and tm.trip_id =tf.tripid and te.trip_id =tf.tripid and  tf.tripid=? and tf.fuel_locat_id !=21\n" 
                            + "and te.expense_id not in(select expense_id from Transactions where trljobid=?)and te.expense_id=1010 group by tf.fuel_locat_id";

                String sql10 = "select expense_id from Transactions where  Trljobid = ? and expense_id = ? and SUBCODE =? order by expense_id asc ";

                String sql11 = "select count(*)as exp_id from Transactions where  Trljobid = ? and expense_id = ? order by expense_id asc ";

                String sql12 = "SELECT te.expense_id as expense_Id,'' as CHQNO,'JV' AS txnType,@acount:=@acount+1 serial_number,te.trip_Id,(select created_On from ts_trip_status_details where trip_Id=tm.trip_Id and status_Id=13 limit 1) as expense_date,\n"
                        + "erp_Id AS AccCode,erp_subCode AS subCode,\n"
                        + "CONCAT('EXP INC VEH#',SUBSTRING(reg_no,-4,4),' AT TMP ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration1,\n"
                        + "CONCAT('PD ',alise_Name, ' AT ',city_Name,' FM ',DATE_FORMAT(tm.trip_actual_start_date,'%d-%m-%Y'),' TO ',DATE_FORMAT(tm.trip_actual_end_date,'%d-%m-%Y'),' A/C MARKS') AS narration2,\n"
                        + "expense_value AS debit,replace(replace(replace(reg_No,' ',''),'-',''),' ','') as reg_no,\n"
                        + "(select replace(replace(replace(driving_license_no,' ',''),'-',''),' ','') from papl_emp_master em where em.emp_Id=td.emp_Id)\n"
                        + "as DriverLc\n"
                        + "FROM (SELECT @acount:= 0) AS acount,ts_trip_master tm,ts_cities tc,ts_trip_driver td,\n"
                        + "ts_trip_expense te LEFT JOIN papl_vehicle_reg_no pvr ON (te.vehicle_Id=pvr.vehicle_Id AND pvr.active_Ind='Y'),trip_expense_master em\n"
                        + "WHERE te.expense_id=em.expenseid AND te.trip_Id=tm.trip_Id AND te.trip_Id=td.trip_Id AND td.trip_Id=tm.trip_Id\n"
                        + "AND tm.destination_Id=tc.city_Id and tm.trip_Id=? and expense_value !=0 limit 1";

                // INSERT Transaction
                getDKTDetails = con.prepareStatement(sql);
                updateDKTDetails = con.prepareStatement(sql1);
                getTxnDetails = con.prepareStatement(sql2);
                getTxnDetails2 = con.prepareStatement(sql9);
                getTxnDetails3 = con.prepareStatement(sql10);
                getTxnDetails4 = con.prepareStatement(sql11);
                updateTxnDetails = con.prepareStatement(sql3);
                updateCreditTxnDetails = con.prepareStatement(sql17);

                getTxnDetails1 = con.prepareStatement(sql4);
                getTxnFuelDetails = con.prepareStatement(sql6);
                getTxnFuelCash = con.prepareStatement(sql7);
                getTxnFastTag = con.prepareStatement(sql14);
                insertTxnDetails = con.prepareStatement(sql5);
                getTxnFuelSubCodeList = con.prepareStatement(sql15);
                getExistsTxnDetails = con.prepareStatement(sql8);
                getNormalCreditExpenseList = con.prepareStatement(sql12);
                getExistsNormalTxnDetails = con.prepareStatement(sql16);

                System.out.println("Posting tripId===" + tripId);
                int status = 0;
                int movementType = 0;
                try {
                    if (tripId != null) {
//                        update DKT

                        getDKTDetails.setString(1, tripId);
                        rs = getDKTDetails.executeQuery();
                        while (rs.next()) {
                            movementType = rs.getInt("movement");

                            updateDKTDetails.setString(1, rs.getString("jobstartdate"));
                            updateDKTDetails.setString(2, rs.getString("jobenddate"));
                            updateDKTDetails.setString(3, rs.getString("cityfromcode"));
                            updateDKTDetails.setString(4, rs.getString("citytocode"));
                            updateDKTDetails.setString(5, rs.getString("distance"));
                            updateDKTDetails.setString(6, rs.getString("ownedhired"));
                            updateDKTDetails.setString(7, rs.getString("hiredvehicleno"));
                            updateDKTDetails.setString(8, rs.getString("hireCharges"));
                            updateDKTDetails.setString(9, rs.getString("partyName"));
                            updateDKTDetails.setString(10, "14001");
                            updateDKTDetails.setString(11, rs.getString("dieselsupplies"));
                            updateDKTDetails.setString(12, rs.getString("dieselConsumed"));
                            updateDKTDetails.setString(13, rs.getString("openingkm"));
                            updateDKTDetails.setString(14, rs.getString("closingkm"));
                            updateDKTDetails.setString(15, rs.getString("totalkmrun"));
                            updateDKTDetails.setString(16, rs.getString("size_20"));
                            updateDKTDetails.setString(17, rs.getString("size_40"));
                            updateDKTDetails.setString(18, rs.getString("size_40HC"));
                            updateDKTDetails.setString(19, rs.getString("size_40FT"));
                            updateDKTDetails.setString(20, rs.getString("size_40OT"));
                            updateDKTDetails.setString(21, rs.getString("netwt"));
                            updateDKTDetails.setString(22, "1");
                            updateDKTDetails.setString(23, "1");
                            updateDKTDetails.setString(24, "1");
                            updateDKTDetails.setString(25, rs.getString("runDate"));
                            updateDKTDetails.setString(26, rs.getString("jobenddate"));
                            updateDKTDetails.setString(27, rs.getString("jobenddate"));
                            updateDKTDetails.setString(28, rs.getString("dieselRecovered"));
                            updateDKTDetails.setString(29, rs.getString("isExport"));
                            updateDKTDetails.setString(30, tripId);
                            updateDKTDetails.setString(31, rs.getString("trailerCode"));
                            updateDKTDetails.setString(32, rs.getString("AccCode"));
                            updateDKTDetails.setString(33, rs.getString("DriverCode"));
                            updateDKTDetails.setString(34, rs.getString("CleanerCode"));
                            updateDKTDetails.setString(35, rs.getString("cityReturncode"));
                            updateDKTDetails.setString(36, rs.getString("subCode"));
                            updateDKTDetails.setString(37, rs.getString("invoice_No"));
                            updateDKTDetails.setString(38, rs.getString("invoice_value"));
//                            if (movementType == 1 || movementType == 2 || movementType == 18
//                                    || movementType == 19 || movementType == 20 || movementType == 21
//                                    || movementType == 24 || movementType == 25 || movementType == 26 || movementType == 27
//                                    || movementType == 28 || movementType == 29 || movementType == 30 || movementType == 31
//                                    || movementType == 32 || movementType == 33 || movementType == 34) {
                                updateDKTDetails.setString(39, rs.getString("haltingFrom"));
                                updateDKTDetails.setString(40, rs.getString("haltingTo"));
                                updateDKTDetails.setString(41, rs.getString("clrFrom"));
                                updateDKTDetails.setString(42, rs.getString("clrTo"));

                                updateDKTDetails.setString(43, rs.getString("halt_days")); //moffusill haltingDay
                                updateDKTDetails.setString(44, rs.getString("clr_days"));                         //local haltingDay

                                updateDKTDetails.setString(45, rs.getString("haltingAmt")); //moffusill amount
                                updateDKTDetails.setString(46, rs.getString("clrAmt"));                         //local amount

//                            } else {
//                                updateDKTDetails.setString(39, null);
//                                updateDKTDetails.setString(40, null);
//                                updateDKTDetails.setString(41, rs.getString("clrFrom"));
//                                updateDKTDetails.setString(42, rs.getString("clrTo"));
//
//                                updateDKTDetails.setString(43, null);                          //moffusill haltingDay
//                                updateDKTDetails.setString(44, rs.getString("clr_days"));    //local haltingDay
//
//                                updateDKTDetails.setString(45, null);                          //moffusill amount
//                                updateDKTDetails.setString(46, rs.getString("clrAmt"));  //local amount
//
//                            }
                            updateDKTDetails.setString(47, rs.getString("halt_days"));       //factory moffusil days
                            updateDKTDetails.setString(48, rs.getString("HaltCntrctAmnt")); //factory moffusil amount
                            updateDKTDetails.setString(49, rs.getString("TrpEndORClsr"));
                            updateDKTDetails.setString(50, rs.getString("TrpInvType"));
                            updateDKTDetails.setString(51, rs.getString("tripType"));
                            updateDKTDetails.setString(52, rs.getString("grosswt"));
                            updateDKTDetails.setString(53, rs.getString("JobNo"));
                            updateDKTDetails.setString(54, rs.getString("BESBNo"));
                            updateDKTDetails.setString(55, rs.getString("BLBKGNo"));
                            updateDKTDetails.setString(56, rs.getString("divertCharge"));
                            updateDKTDetails.setString(57, rs.getString("oTCharges"));
                            updateDKTDetails.setString(58, rs.getString("MovType"));

                            updateDKTDetails.setString(59, tripId);

                            status = updateDKTDetails.executeUpdate();
                            System.out.println("DKT= " + status);
                        }

                        double credit0 = 0.00D;
                        double credit = 0.00D;
                        double credit1 = 0.00D;
                        double credit2 = 0.00D;
                        double credit3 = 0.00D;
                        double credit4 = 0.00D;
                        int sno = 0;
                        String expenseDate = "";
                        int expenseId = 0;
                        int chkExpenseId = 0;
                        String lcNo = "";
                        String regNo = null;
                        String accCode = null;

                        //for insert
                        double credits = 0.00D;
                        int snos = 0;
                        String expenseDates = "";
                        String CHQNOs = "";
                        String lcNos = "";
                        String regNos1 = null;
                        String accCodes = null;
                        String accCode1 = null;
                        String subCode = null;
                        String subCode1 = null;
                        int totCount = 0;

                        getExistsTxnDetails.setString(1, tripId);
                        rs6 = getExistsTxnDetails.executeQuery();
                        System.out.println("rs6rs6rs6rs6rs6------" + rs6);
                        System.out.println("getExistsTxnDetails-----" + getExistsTxnDetails);
                        while (rs6.next()) {
                            System.out.println("expense_id---" + rs6.getString("expense_id"));
                            getExistsTxnList.add(rs6.getString("expense_id"));
                        }

                        System.out.println("totCount-----" + totCount);
                        if (getExistsTxnList.size() > 0) {
                            System.out.println("getExistsTxnList.size() > 0-----" + getExistsTxnList.size());

                            getTxnDetails.setString(1, tripId);
                            rs2 = getTxnDetails.executeQuery();
                            while (rs2.next()) {
                                accCode = rs2.getString("AccCode");
                                lcNo = rs2.getString("DriverLc");
                                System.out.println("start getTxnDetails check----" + rs2.getInt("expense_id"));

                                if ("70005".equals(accCode)) {
                                    if (movementType == 2 || movementType == 28) {
                                        accCode = "70009";
                                    }
                                }
                                if ("47010".equals(accCode)) {
                                    if (movementType == 1 || movementType == 2 || movementType == 18
                                            || movementType == 19 || movementType == 20 || movementType == 21
                                            || movementType == 24 || movementType == 25 || movementType == 26 || movementType == 27
                                            || movementType == 28 || movementType == 29 || movementType == 30 || movementType == 31
                                            || movementType == 32 || movementType == 33 || movementType == 34) {
                                        accCode = "47011";
                                    }
                                }
                                sno = rs2.getInt("serial_number");
                                expenseId = rs2.getInt("expense_id");
                                subCode = rs2.getString("subCode");
                                credit = credit + rs2.getDouble("debit");
                                expenseDate = rs2.getString("expense_date");

//                                getTxnDetails3.setString(1, tripId);
//                                getTxnDetails3.setInt(2, rs2.getInt("expense_id"));
//                                getTxnDetails3.setString(3, subCode);
//                                rs7 = getTxnDetails3.executeQuery();
//                                while (rs7.next()) {
//                                    chkExpenseId = rs7.getInt("expense_id");
//                                    update

                                    updateTxnDetails.setString(1, "JV");
                                    updateTxnDetails.setString(2, rs2.getString("expense_date"));
                                    if(expenseId == 1048 || expenseId == 1010){
                                    updateTxnDetails.setString(3, "0");
                                    updateTxnDetails.setString(4, rs2.getString("debit"));
                                    }else{
                                    updateTxnDetails.setString(3, rs2.getString("debit"));
                                    updateTxnDetails.setString(4, "0");
                                    }
                                    updateTxnDetails.setString(5, "1");
                                    updateTxnDetails.setString(6, "1");
                                    updateTxnDetails.setString(7, tripId);
                                    updateTxnDetails.setInt(8, expenseId);
//                                    updateTxnDetails.setString(9, subCode);

                                    status = updateTxnDetails.executeUpdate();
                                    System.out.println("MAIN updateTxnDetails= " + status);
//                                }
//                                   insert

                                getExistsNormalTxnDetails.setString(1, tripId);
                                rs9 = getExistsNormalTxnDetails.executeQuery();
                                System.out.println("rs77777------" + rs7);
                                System.out.println("getExistsNormalTxnDetails-----" + getExistsNormalTxnDetails);
                                while (rs9.next()) {
                                    System.out.println("expense_id---" + rs9.getString("expense_id"));
                                    getExistsNormalExpTxnList.add(rs9.getString("expense_id"));
                                }
                                totCount = getExistsNormalExpTxnList.size();

                                int insertCount = 0;
                                int creditCount = 0;
                                String narrate1 = "";
                                String narrate2 = "";
                                getTxnDetails2.setString(1, tripId);
                                getTxnDetails2.setString(2, tripId);
                                getTxnDetails2.setString(3, tripId);
                                getTxnDetails2.setString(4, tripId);
                                rs3 = getTxnDetails2.executeQuery();
                                insertCount = totCount - 1;
                                creditCount = totCount + 1;
                                while (rs3.next()) {
                                    accCodes = rs3.getString("AccCode");
                                    lcNos = rs3.getString("DriverLc");

                                    if ("70005".equals(accCodes)) {
                                        if (movementType == 2 || movementType == 28) {
                                            accCodes = "70009";
                                        }
                                    }
                                    if ("47010".equals(accCodes)) {
                                        if (movementType != 1 && movementType != 2 && movementType != 23 && movementType != 25 && movementType != 26) {
                                            accCodes = "47011";
                                        }
                                    }
                                    snos = insertCount;
                                    credits = credits + rs3.getDouble("debit");
                                    expenseDates = rs3.getString("expense_date");
                                    subCode = rs3.getString("subCode");
                                    narrate1 = rs3.getString("narration1");
                                    narrate2 = rs3.getString("narration2");
//                                    if("1010".equals(rs3.getString("expense_id")) ){
//                                        
//                                    }
                                    insertTxnDetails.setString(1, rs3.getString("txnType"));
                                    insertTxnDetails.setInt(2, snos);
                                    insertTxnDetails.setString(3, rs3.getString("expense_date"));
                                    insertTxnDetails.setString(4, accCodes);
                                    insertTxnDetails.setString(5, subCode);
                                    insertTxnDetails.setString(6, rs3.getString("narration1"));
                                    insertTxnDetails.setString(7, rs3.getString("narration2"));
                                    insertTxnDetails.setString(8, rs3.getString("debit"));
                                    insertTxnDetails.setString(9, "0");
                                    insertTxnDetails.setString(10, "1");
                                    insertTxnDetails.setString(11, tripId);
                                    insertTxnDetails.setString(12, "1");
                                    insertTxnDetails.setString(13, rs3.getString("reg_no"));
                                    insertTxnDetails.setString(14, rs3.getString("DriverLc"));
                                    insertTxnDetails.setString(15, CHQNOs);
                                    insertTxnDetails.setString(16, rs3.getString("expense_id"));
                                    insertTxnDetails.setString(17, "1"); //TrpEndORClsr
                                    regNos1 = rs3.getString("reg_no");
                                    status = insertTxnDetails.executeUpdate();
                                    System.out.println("insertTxnDetails= " + status);
                                }
                                int subExpenseId = 0;
                                System.out.println("expenseId before subexpense setting-------" + expenseId);
                                if (expenseId != 1010 && expenseId != 1047 && expenseId != 1011) {
                                    subExpenseId = 1; //  1 as notrmal other expense credit
                                }
                                if (expenseId == 1011) {
                                    subExpenseId = 2;  //  2 as fast tag toll credit
                                }
                                if (expenseId == 1047) {
                                    subExpenseId = 3;  // 3 as fuel expense driver CASH  Credit
                                }
                                if (expenseId == 1010) {
                                    subExpenseId = 4; // 4 as Fuel expense credit
                                }
                                System.out.println("subExpenseId-------" + subExpenseId);

                                getTxnDetails4.setString(1, tripId);
                                getTxnDetails4.setInt(2, subExpenseId);
                                rs8 = getTxnDetails4.executeQuery();
                                System.out.println("rs8rs8rs8-----" + rs8);
                                System.out.println("getTxnDetails4-----" + getTxnDetails4);
                                int counts = 0;
                                while (rs8.next()) {
                                    System.out.println("expense_id-suubexp-@@@-" + rs8.getString("exp_id"));
                                    counts = rs8.getInt("exp_id");
                                }

                                System.out.println("getSubExpenseList.size()---" + counts);
                                if (counts == 0) {
                                    //insert
                                    credit0 = credit0 + rs2.getDouble("debit");
                                    insertTxnDetails.setString(1, "JV");
                                    if (subExpenseId == 1) {
                                        getNormalCreditExpenseList.setString(1, tripId);
                                        rs3 = getNormalCreditExpenseList.executeQuery();
                                        while (rs3.next()) {
                                            narrate1 = rs3.getString("narration1");
                                            regNo = rs3.getString("reg_no");
                                            lcNo = rs3.getString("DriverLc");
                                            insertTxnDetails.setInt(2, insertCount);
                                            insertTxnDetails.setString(3, expenseDate);
                                            insertTxnDetails.setString(4, "15007");
                                            insertTxnDetails.setString(5, null);
                                            insertTxnDetails.setString(6, null);
                                            insertTxnDetails.setString(7, narrate1);
                                            insertTxnDetails.setString(8, "0");
                                            insertTxnDetails.setString(9, credit0 + "");
                                            insertTxnDetails.setString(10, "1");
                                            insertTxnDetails.setString(11, tripId);
                                            insertTxnDetails.setString(12, "1");
                                            insertTxnDetails.setString(13, regNo);
                                            insertTxnDetails.setString(14, lcNo);
                                            insertTxnDetails.setString(15, "");
                                            status = insertTxnDetails.executeUpdate();
                                            System.out.println("insertTxnDetails= " + status);
                                        }
                                    } else if (subExpenseId == 1010) {
                                        getTxnFuelDetails.setString(1, tripId);
                                        rs5 = getTxnFuelDetails.executeQuery();
                                        while (rs5.next()) {
                                            insertTxnDetails.setInt(2, 1);
                                            insertTxnDetails.setString(3, expenseDate);
                                            insertTxnDetails.setString(4, "15007");
                                            insertTxnDetails.setString(5, rs5.getString("sub_code"));
                                            insertTxnDetails.setString(6, rs5.getString("narration1"));
                                            insertTxnDetails.setString(7, rs5.getString("narration2"));
                                            insertTxnDetails.setString(8, "0");
                                            insertTxnDetails.setString(9, credit0 + "");
                                            insertTxnDetails.setString(10, "1");
                                            insertTxnDetails.setString(11, tripId);
                                            insertTxnDetails.setString(12, "1");
                                            insertTxnDetails.setString(13, rs5.getString("reg_no"));
                                            insertTxnDetails.setString(14, rs5.getString("DriverLc"));
                                            insertTxnDetails.setString(15, "DC");
                                        }
                                    } else if (subExpenseId == 1047) {
                                        getTxnFuelCash.setString(1, tripId);
                                        rs5 = getTxnFuelCash.executeQuery();
                                        while (rs5.next()) {
                                            insertTxnDetails.setInt(2, 1);
                                            insertTxnDetails.setString(3, expenseDate);
                                            insertTxnDetails.setString(4, "15007");
                                            insertTxnDetails.setString(5, rs5.getString("sub_code"));
                                            insertTxnDetails.setString(6, rs5.getString("narration1"));
                                            insertTxnDetails.setString(7, rs5.getString("narration2"));
                                            insertTxnDetails.setString(8, "0");
                                            insertTxnDetails.setString(9, credit0 + "");
                                            insertTxnDetails.setString(10, "1");
                                            insertTxnDetails.setString(11, tripId);
                                            insertTxnDetails.setString(12, "1");
                                            insertTxnDetails.setString(13, rs5.getString("reg_no"));
                                            insertTxnDetails.setString(14, rs5.getString("DriverLc"));
                                            insertTxnDetails.setString(15, "DC");
                                        }
                                    } else if (subExpenseId == 1011) {
                                        getTxnFastTag.setString(1, tripId);
                                        rs4 = getTxnFastTag.executeQuery();
                                        while (rs4.next()) {
                                            insertTxnDetails.setInt(2, 2);
                                            insertTxnDetails.setString(3, expenseDate);
                                            insertTxnDetails.setString(4, "15007");
                                            insertTxnDetails.setString(5, rs4.getString("sub_code"));
                                            insertTxnDetails.setString(6, rs4.getString("narration1"));
                                            insertTxnDetails.setString(7, rs4.getString("narration2"));
                                            insertTxnDetails.setString(8, "0");
                                            insertTxnDetails.setString(9, credit0 + "");
                                            insertTxnDetails.setString(10, "1");
                                            insertTxnDetails.setString(11, tripId);
                                            insertTxnDetails.setString(12, "1");
                                            insertTxnDetails.setString(13, rs4.getString("reg_no"));
                                            insertTxnDetails.setString(14, rs4.getString("DriverLc"));
                                            insertTxnDetails.setString(15, "FT");
                                        }
                                    }

                                    insertTxnDetails.setInt(16, subExpenseId);
                                    insertTxnDetails.setString(17, "1"); //TrpEndORClsr

                                    status = insertTxnDetails.executeUpdate();
                                    System.out.println("insertTxnDetails= " + status);
                                } else {
                                    System.out.println("else updatep--------------" + expenseId);
                                    //update
                                    //  1 as notrmal other expense credit update
                                    if (subExpenseId == 1) {
                                        sno = sno + 1;
                                        credit1 = credit1 + rs2.getDouble("debit");
                                        System.out.println("thrERPTablePosting notrmal other expense credit Enter======" + credit1);
                                        System.out.println("subExpenseIdr======" + subExpenseId);
                                        updateCreditTxnDetails.setString(1, "JV");
                                        updateCreditTxnDetails.setString(2, expenseDate);
                                        updateCreditTxnDetails.setString(3, "0");
                                        updateCreditTxnDetails.setString(4, credit1 + "");
                                        updateCreditTxnDetails.setString(5, "1");
                                        updateCreditTxnDetails.setString(6, "1");
                                        updateCreditTxnDetails.setString(7, tripId);
                                        updateCreditTxnDetails.setInt(8, subExpenseId);
                                        

                                        status = updateCreditTxnDetails.executeUpdate();
                                        System.out.println("updateTxnDetails Credit= " + status);
                                    }

                                    //  2 as fast tag toll credit update
                                    if (subExpenseId == 2) {
                                        credit2 = credit2 + rs2.getDouble("debit");
                                        System.out.println("thrERPTablePosting fast tag toll credit Enter======" + credit2);
                                        System.out.println("subExpenseIdr======" + subExpenseId);
                                        updateCreditTxnDetails.setString(1, "JV");
                                        updateCreditTxnDetails.setString(2, expenseDate);
                                        updateCreditTxnDetails.setString(3, "0");
                                        updateCreditTxnDetails.setString(4, credit2 + "");
                                        updateCreditTxnDetails.setString(5, "1");
                                        updateCreditTxnDetails.setString(6, "1");
                                        updateCreditTxnDetails.setString(7, tripId);
                                        updateCreditTxnDetails.setInt(8, subExpenseId);

                                        status = updateCreditTxnDetails.executeUpdate();
                                        System.out.println("updateTxnDetails Credit= " + status);
                                    }

                                    // 3 as fuel expense driver CASH  Credit updaet
                                    if (subExpenseId == 3) {
                                        System.out.println("thrERPTablePosting credit Enter======" + credit);
                                        credit3 = credit3 + rs2.getDouble("debit");
                                        System.out.println("thrERPTablePosting driver CASH credit Enter======" + credit3);
                                        System.out.println("subExpenseIdr======" + subExpenseId);
                                        updateCreditTxnDetails.setString(1, "JV");
                                        updateCreditTxnDetails.setString(2, expenseDate);
                                        updateCreditTxnDetails.setString(3, credit3 + "");
                                        updateCreditTxnDetails.setString(4, "0");
                                        updateCreditTxnDetails.setString(5, "1");
                                        updateCreditTxnDetails.setString(6, "1");
                                        updateCreditTxnDetails.setString(7, tripId);
                                        updateCreditTxnDetails.setInt(8, subExpenseId);

                                        status = updateCreditTxnDetails.executeUpdate();
                                        System.out.println("updateTxnDetails Credit= " + status);
                                    }

                                    // 4 as Fuel expense credit  update
                                    if (subExpenseId == 4) {
                                        System.out.println("thrERPTablePosting credit Enter======" + credit);
                                        credit4 = credit4 + rs2.getDouble("debit");
                                        System.out.println("thrERPTablePosting Fuel xpense credit Enter======" + credit4);
                                        System.out.println("subExpenseIdr======" + subExpenseId);
                                        updateCreditTxnDetails.setString(1, "JV");
                                        updateCreditTxnDetails.setString(2, expenseDate);
                                        updateCreditTxnDetails.setString(3, credit4 + "");
                                        updateCreditTxnDetails.setString(4, "0");
                                        updateCreditTxnDetails.setString(5, "1");
                                        updateCreditTxnDetails.setString(6, "1");
                                        updateCreditTxnDetails.setString(7, tripId);
                                        updateCreditTxnDetails.setInt(8, subExpenseId);

                                        status = updateCreditTxnDetails.executeUpdate();
                                        System.out.println("updateTxnDetails Credit= " + status);
                                    }
//                                        }

//                                    }
                                    System.out.println("thrERPTablePosting sno======" + sno);
                                    System.out.println("thrERPTablePosting credit======" + credit);

                                }
                            }
                        }
                        System.out.println("getExistsTxnList---just befor insert----" + getExistsTxnList.size());
                       int sno1 = 0;
                        if (getExistsTxnList.size() == 0) {  // insert
                            System.out.println("entreed INSERTTTT");
//                          Starts transctions insert
                            String narrate1 = "";
                            getTxnDetails1.setString(1, tripId);
                            rs3 = getTxnDetails1.executeQuery();
                            while (rs3.next()) {
                                sno1=sno1+1;
                                System.out.println("111111111111-getTxnDetails1--" + snos);
                                accCodes = rs3.getString("AccCode");
                                lcNos = rs3.getString("DriverLc");

                                if ("70005".equals(accCodes)) {
                                    if (movementType == 2 || movementType == 28) {
                                        accCodes = "70009";
                                    }
                                }
                                if ("47010".equals(accCodes)) {
                                    if (movementType == 1 || movementType == 2 || movementType == 18
                                            || movementType == 19 || movementType == 20 || movementType == 21
                                            || movementType == 24 || movementType == 25 || movementType == 26 || movementType == 27
                                            || movementType == 28 || movementType == 29 || movementType == 30 || movementType == 31
                                            || movementType == 32 || movementType == 33 || movementType == 34) {
                                        accCodes = "47011";
                                    }
                                }
                                snos = rs3.getInt("serial_number");
                                credit = credit + rs3.getDouble("debit");
                                expenseDates = rs3.getString("expense_date");
                                subCode = rs3.getString("subCode");
                                narrate1 = rs3.getString("narration1");
                                insertTxnDetails.setString(1, rs3.getString("txnType"));
                                insertTxnDetails.setString(2, rs3.getString("serial_number"));
                                insertTxnDetails.setString(3, rs3.getString("expense_date"));
                                insertTxnDetails.setString(4, accCodes);
                                insertTxnDetails.setString(5, subCode);
                                insertTxnDetails.setString(6, rs3.getString("narration1"));
                                insertTxnDetails.setString(7, rs3.getString("narration2"));
                                insertTxnDetails.setString(8, rs3.getString("debit"));
                                insertTxnDetails.setString(9, "0");
                                insertTxnDetails.setString(10, "1");
                                insertTxnDetails.setString(11, tripId);
                                insertTxnDetails.setString(12, "1");
                                if (Integer.parseInt(rs3.getString("expense_id")) == 1030 || Integer.parseInt(rs3.getString("expense_id")) == 1039) {
                                    insertTxnDetails.setString(13, null);
                                    insertTxnDetails.setString(14, null);
                                } else {
                                    insertTxnDetails.setString(13, regNo);
                                    insertTxnDetails.setString(14, lcNo);
                                }
                                insertTxnDetails.setString(15, "");
                                insertTxnDetails.setString(16, rs3.getString("expense_id"));
                                insertTxnDetails.setString(17, "1"); //TrpEndORClsr
                                status = insertTxnDetails.executeUpdate();
                                System.out.println("insertTxnDetails= " + status);
                            }
                            System.out.println("thrERPTablePosting snos======" + snos);
                            System.out.println("thrERPTablePosting credit======" + credit);
                            
                            if (snos != 0) {
                                sno1 =sno1+1;
                                System.out.println("thrERPTablePosting credit Enter======" + credit);
                                System.out.println("22222222222-getTxnDetails1--" + snos);
                                insertTxnDetails.setString(1, "JV");
                                insertTxnDetails.setInt(2, sno1);
                                insertTxnDetails.setString(3, expenseDates);
                                insertTxnDetails.setString(4, "15007");
                                insertTxnDetails.setString(5, null);
                                insertTxnDetails.setString(6, null);
                                insertTxnDetails.setString(7, narrate1);
                                insertTxnDetails.setString(8, "0");
                                insertTxnDetails.setString(9, credit + "");
                                insertTxnDetails.setString(10, "1");
                                insertTxnDetails.setString(11, tripId);
                                insertTxnDetails.setString(12, "1");
                                insertTxnDetails.setString(13, regNo);
                                insertTxnDetails.setString(14, lcNos);
                                insertTxnDetails.setString(15, "");
                                insertTxnDetails.setInt(16, 1);  // 1 as notrmal other expense credit
                                insertTxnDetails.setString(17, "1"); //TrpEndORClsr

                                status = insertTxnDetails.executeUpdate();
                                System.out.println("insertTxnDetails Credit= " + status);
                            }

                            System.out.println("getTxnDetails snos======" + snos);

//                        starts getTxnFuelCash
                            getTxnFuelCash.setString(1, tripId);
                            rs4 = getTxnFuelCash.executeQuery();
                            while (rs4.next()) {

                                if ("1047".equals(rs4.getString("expenseId"))) { // fuel expense driver CASH
                                    CHQNOs = "DC";
                                    accCode1 = "46019";
                                    subCode1 = "";
                                    System.out.println("Driver CASH");
                                    insertTxnDetails.setString(1, rs4.getString("txnType"));
                                    insertTxnDetails.setInt(2, 1);
                                    insertTxnDetails.setString(3, expenseDates);
                                    insertTxnDetails.setString(4, accCode1);
                                    insertTxnDetails.setString(5, subCode1);
                                    insertTxnDetails.setString(6, rs4.getString("narration1"));
                                    insertTxnDetails.setString(7, rs4.getString("narration2"));
                                    insertTxnDetails.setString(8, rs4.getString("debit"));
                                    insertTxnDetails.setString(9, "0");
                                    insertTxnDetails.setString(10, "1");
                                    insertTxnDetails.setString(11, tripId);
                                    insertTxnDetails.setString(12, "1");
                                    insertTxnDetails.setString(13, rs4.getString("reg_no"));
                                    insertTxnDetails.setString(14, rs4.getString("DriverLc"));
                                    insertTxnDetails.setString(15, CHQNOs);
                                    insertTxnDetails.setString(16, rs4.getString("expenseId"));
                                    insertTxnDetails.setString(17, "1"); //TrpEndORClsr
                                    status = insertTxnDetails.executeUpdate();
                                    System.out.println("insertTxnDetails= " + status);

                                    if (snos != 0) {
                                        snos = snos + 1;
                                        System.out.println("thrERPTablePosting credit Enter======" + credit);

                                        insertTxnDetails.setString(1, "JV");
                                        insertTxnDetails.setInt(2, 2);
                                        insertTxnDetails.setString(3, expenseDates);
                                        insertTxnDetails.setString(4, "15007");
                                        insertTxnDetails.setString(5, null);
                                        insertTxnDetails.setString(6, rs4.getString("narration1"));
                                        insertTxnDetails.setString(7, rs4.getString("narration2"));
                                        insertTxnDetails.setString(8, "0");
                                        insertTxnDetails.setString(9, rs4.getString("debit"));
                                        insertTxnDetails.setString(10, "1");
                                        insertTxnDetails.setString(11, tripId);
                                        insertTxnDetails.setString(12, "1");
                                        insertTxnDetails.setString(13, rs4.getString("reg_no"));
                                        insertTxnDetails.setString(14, rs4.getString("DriverLc"));
                                        insertTxnDetails.setString(15, CHQNOs);
                                        insertTxnDetails.setInt(16, 3);  //3 as  fuel expense driver CASH  Credit
                                        insertTxnDetails.setString(17, "1"); //TrpEndORClsr

                                        status = insertTxnDetails.executeUpdate();
                                        System.out.println("insertTxnDetails Credit= " + status);
                                    }
                                }
                            }

                            getTxnFastTag.setString(1, tripId);
                            rs4 = getTxnFastTag.executeQuery();
                            while (rs4.next()) {
                                if (snos == 0) {
                                    snos = Integer.parseInt(rs4.getString("serial_number"));
                                } else {
                                    snos = snos + 1;
                                }
                                System.out.println("3333333-getTxnFuelCashAndFastTag--" + snos);
                                accCodes = rs4.getString("AccCode");
                                credit = credit + rs4.getDouble("debit");
                                expenseDates = rs4.getString("expense_date");
                                subCode = rs4.getString("subCode");

                                if ("1011".equals(rs4.getString("expenseId"))) { // fast tag toll
                                    CHQNOs = "FT";
                                    accCode1 = "22001";
                                    subCode1 = "CI008";
                                    System.out.println("FT CRedit");
                                    insertTxnDetails.setString(1, rs4.getString("txnType"));
                                    insertTxnDetails.setInt(2, 1);
                                    insertTxnDetails.setString(3, expenseDates);
                                    insertTxnDetails.setString(4, "46097");
                                    insertTxnDetails.setString(5, "");
                                    insertTxnDetails.setString(6, rs4.getString("narration1"));
                                    insertTxnDetails.setString(7, rs4.getString("narration2"));
                                    insertTxnDetails.setString(8, rs4.getString("debit"));
                                    insertTxnDetails.setString(9, "0");
                                    insertTxnDetails.setString(10, "1");
                                    insertTxnDetails.setString(11, tripId);
                                    insertTxnDetails.setString(12, "1");
                                    insertTxnDetails.setString(13, rs4.getString("reg_no"));
                                    insertTxnDetails.setString(14, rs4.getString("DriverLc"));
                                    insertTxnDetails.setString(15, CHQNOs);
                                    insertTxnDetails.setString(16, rs4.getString("expenseid"));
                                    insertTxnDetails.setString(17, "1"); //TrpEndORClsr
                                    status = insertTxnDetails.executeUpdate();
                                    System.out.println("insertTxnDetails= " + status);

                                    if (snos != 0) {
                                        snos = snos + 1;
                                        System.out.println("thrERPTablePosting credit Enter======" + credit);
                                        CHQNOs = "FT";
                                        insertTxnDetails.setString(1, "JV");
                                        insertTxnDetails.setInt(2, 2);
                                        insertTxnDetails.setString(3, expenseDates);
                                        insertTxnDetails.setString(4, accCode1);
                                        insertTxnDetails.setString(5, subCode1);
                                        insertTxnDetails.setString(6, rs4.getString("narration1"));
                                        insertTxnDetails.setString(7, rs4.getString("narration2"));
                                        insertTxnDetails.setString(8, "0");
                                        insertTxnDetails.setString(9, rs4.getString("debit"));
                                        insertTxnDetails.setString(10, "1");
                                        insertTxnDetails.setString(11, tripId);
                                        insertTxnDetails.setString(12, "1");
                                        insertTxnDetails.setString(13, rs4.getString("reg_no"));
                                        insertTxnDetails.setString(14, rs4.getString("DriverLc"));
                                        insertTxnDetails.setString(15, CHQNOs);
                                        insertTxnDetails.setInt(16, 2);  // 2 as fast tag toll credit
                                        insertTxnDetails.setString(17, "1"); //TrpEndORClsr

                                        status = insertTxnDetails.executeUpdate();
                                        System.out.println("insertTxnDetails Credit= " + status);
                                    }

                                }
                            }
                            System.out.println("getTxnFastTag snos====e n d==" + snos);

                            //  starts getTxnFuelDetails  FUEL Bunk
                            String accCode2 = "";
                            String subCode2 = "";
                            String CHQNO2 = "";
                            String regNo2 = "";
                            String lcNo2 = "";
                            String narration1 = "";
                            String narration2 = "";
                            getTxnFuelDetails.setString(1, tripId);
                            Double creditss = 0.00;
                            rs4 = getTxnFuelDetails.executeQuery();
                            int snoss = 1;
                            while (rs4.next()) {
                                snoss = snoss + 1;
                                accCode2 = rs4.getString("acc_Code");
                                CHQNO2 = rs4.getString("CHQNO");
                                subCode2 = rs4.getString("sub_code");
                                regNo2 = rs4.getString("reg_no");
                                lcNo2 = rs4.getString("DriverLc");
                                narration1 = rs4.getString("narration1");
                                narration2 = rs4.getString("narration2");
                                creditss = Double.parseDouble(rs4.getString("debit"));

                                getTxnFuelSubCodeList.setString(1, subCode2);
                                rs5 = getTxnFuelSubCodeList.executeQuery();
                                while (rs5.next()) {
                                    System.out.println("RS4 subcode----" + rs5.getString("sub_code"));
                                    System.out.println("RS5 subcode----" + rs4.getString("sub_code"));
                                    insertTxnDetails.setString(1, rs4.getString("txnType"));
                                    insertTxnDetails.setInt(2, 2);
                                    //                              insertTxnDetails.setString(2, rs2.getString("serial_number"));
                                    insertTxnDetails.setString(3, rs4.getString("fuel_date"));
                                    insertTxnDetails.setString(4, accCode2);
                                    insertTxnDetails.setString(5, subCode2);
                                    insertTxnDetails.setString(6, narration1);
                                    insertTxnDetails.setString(7, narration2);
                                    insertTxnDetails.setString(8, "0");
                                    insertTxnDetails.setString(9, rs4.getString("debit"));
                                    insertTxnDetails.setString(10, "1");
                                    insertTxnDetails.setString(11, tripId);
                                    insertTxnDetails.setString(12, "1");
                                    insertTxnDetails.setString(13, regNo2);
                                    insertTxnDetails.setString(14, lcNo2);
                                    insertTxnDetails.setString(15, CHQNO2);
                                    insertTxnDetails.setString(16, "1010");  //fuel
                                    insertTxnDetails.setString(17, "1"); //TrpEndORClsr

                                    status = insertTxnDetails.executeUpdate();
                                    System.out.println("insertgetTxnFuelDetails= " + status);

                                    if (snoss != 0) {
                                        System.out.println("thrERPTablePosting 1010 credit Enter======" + creditss);
                                        insertTxnDetails.setString(1, "JV");
                                        insertTxnDetails.setInt(2, 1);
                                        insertTxnDetails.setString(3, rs4.getString("fuel_date"));
                                        insertTxnDetails.setString(4, "46019");
                                        insertTxnDetails.setString(5, null);
                                        insertTxnDetails.setString(6, narration1);
                                        insertTxnDetails.setString(7, narration2);
                                        insertTxnDetails.setString(8, creditss + "");
                                        insertTxnDetails.setString(9, "0");
                                        insertTxnDetails.setString(10, "1");
                                        insertTxnDetails.setString(11, tripId);
                                        insertTxnDetails.setString(12, "1");
                                        insertTxnDetails.setString(13, regNo2);
                                        insertTxnDetails.setString(14, lcNo2);
                                        insertTxnDetails.setString(15, CHQNO2);
                                        insertTxnDetails.setInt(16, 4); // 4 as Fuel expense Credit 
                                        insertTxnDetails.setString(17, "1"); //TrpEndORClsr

                                        status = insertTxnDetails.executeUpdate();
                                        System.out.println("insertTxnDetails Credit= " + status);
                                    }
                                }
                            }
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("Posting Caught SqlException: " + e.getMessage());

                }

            } catch (Exception e) {
                System.out.println("Posting Caught IOException: " + e.getMessage());
            }
            con.close();
        } catch (Exception e) {
            System.out.println("Posting Caught Exception: " + e.getMessage());
        }
    }

}
