<%--
    Document   : BPCLTransactionReport
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>

<html>
    <head>
       <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
            <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
            <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
            <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">


        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>


        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value){
        
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                }else{
                    if(value == "ExportExcel"){
                        document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else{   
                        document.BPCLTransaction.action = '/throttle/handleVehicleDriverAdvanceExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>



    </head>
    <body onload="sorter.size(50);">
        <form name="BPCLTransaction" method="post">
             <br>
             <br>
             Reports -->> Vehicle Driver Advance
             <br>
             <br>
             <br>

            <table width="75%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr>
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:650;">
                            <b>Vehicle Driver Advance Report</b>
                            <div id="first">
                                <table width="550" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <!--                                    <tr>
                                                                            <td><font color="red">*</font>Driver Name</td>
                                                                            <td height="30">
                                                                                <input name="driName" id="driName" type="text" class="form-control" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                        </tr>-->
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                      <td><font color="red">*</font>Type</td>
                                      <td height="30"><select name="expensetype" id="expensetype" type="text" >
                                              <option value=" ">--Select---</option>
                                              <option value="Food">Food</option>
                                              <option value="Repair">R&M</option>
                                          </select>
                                          <script>
                                              document.getElementById("expensetype").value = '<c:out value="${expensetype}"/>' ;
                                          </script>
                                      </td>
                                          <td><font color="red">*</font>Fleet </td>
                                      <td height="30"><select name="fleet" id="fleet" type="text" >
                                               <option value=" ">--Select---</option>
                                              <option value="2">Primary </option>
                                              <option value="1">Secondary</option>
                                          </select>
                                              <script>
                                                  document.getElementById("fleet").value = '<c:out value="${fleet}"/>' ;
                                              </script>
                                      </td>

                                    </tr>
                                    <tr>
                                        <td colspan="2" align="center"><input type="button" class="button" name="search" onclick="submitPage(this.name);" value="Search"></td>
                                        <td align="left"><input type="button" class="button" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
              <br>
              <br>
            <c:if test="${vehicleDriverAdvanceDetails == null}">
                <center><font color="red">No Records Found</font></center>
            </c:if>
            <c:if test="${vehicleDriverAdvanceDetails != null}">
                <table align="center" border="0" id="table" class="sortable" width="100%" >
                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                            <th align="center"><h3>Vehicle No</h3></th>
                            <th align="center"><h3>Primary Driver</h3></th>
                            <th align="center"><h3>Secondary Driver</h3></th>
                            <th align="center"><h3>Advance Paid</h3></th>
                            <th align="center"><h3>Advance paid Date</h3></th>
                            <th align="center"><h3>Expense Type</h3></th>
                            <th align="center"><h3>Remarks</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:forEach items="${vehicleDriverAdvanceDetails}" var="BPCLTD">
                            <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                            %>
                            <tr>
                                <td width="2" class="<%=classText%>"><%=index++%></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.regNo}"/></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.primaryDriver}"/></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.secondaryDriver}"/></td>
                                <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.paidAdvance}"/></td>
                                <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.advancePaidDate}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.expenseType}"/></td>
                                <td width="30" class="<%=classText%>"  ><c:out value="${BPCLTD.advanceRemarks}"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" >5</option>
                        <option value="10">10</option>
                        <option value="20" selected="selected">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>

