<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 

    <%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>

    <head>
        
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>    
        <%@ page import="ets.domain.stockTransfer.business.StockTransferTO" %>  
    </head>
    <script language="javascript">
        function submitPage(value){          
            document.stockApproval.approvalStatus.value=value;
            selectedItemValidation();            
        }
        function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var approvedQty = document.getElementsByName("approvedQtys");
            var requestedQtys = document.getElementsByName("requestedQtys");
             var rcItem = document.getElementsByName("rcItem");
              var newItem = document.getElementsByName("newItem");
            var remarks = document.getElementsByName("remarks");  
            for(var i=0;(i<index.length && index.length!=0);i++){                       
                if(floatValidation(approvedQty[i],'Issue Quantity')){   
                    return;
                }   
                alert;
                 if(parseFloat(approvedQty[i].value) > parseFloat(requestedQtys[i].value)){
                alert("Approved  Quantity Should not be Greater Than Requested Quantity");
                return false;
                }
                 if(parseFloat(approvedQty[i].value) > (parseFloat(rcItem[i].value) + parseFloat(newItem[i].value))){
                alert("Approved  Quantity Should not be Greater Than Stock Quantity");
                return;
                }
            }
            if(textValidation(document.stockApproval.remarks,'Remarks')){       
               return;
           }  
          document.stockApproval.requestPtId.value='<%=request.getAttribute("requestPtId")%>';
            document.stockApproval.action ="/throttle/STApproval.do"
            document.stockApproval.submit(); 
        }
        function setValues(){                 
            document.stockApproval.requestId.value='<%=request.getAttribute("RequestId")%>';            
            document.stockApproval.approvedQtys0.focus();
            }
       
        function setSelectbox(i)
        {               
            var selected=document.getElementsByName("selectedIndex");
            selected[i];
        } 
    </script>
            
            
            <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>
            
<div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Receive Stock" text="Approve Stock"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
	  <li><a href="general-forms.html"><spring:message code="hrms.label.Stock Transfer" text="Stock Transfer"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Receive Stock" text="Approve Stock"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">            
            
    <body onload="setValues();">
        <form name="stockApproval"  method="post" >
<%--<%@ include file="/content/common/path.jsp" %>--%>

<%@ include file="/content/common/message.jsp" %>
      

<% int index = 0;%> 
 <c:if test = "${itemList != null}" >
<!--                <table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">-->
<!--    <table class="table table-info mb30 table-hover" id="bg" >	
       <thead>
                    <tr>
                        <td class="contenthead" height="30" colspan="6"><div class="contenthead">Approve/Reject Request</div></td>
                    </tr>
                    </thead>-->

<table class="table table-info mb30 table-hover" id="bg"   >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >Approve/Reject Request</th>
		</tr>
                    </thead>
            </table>	

<table class="table table-info mb30 table-hover"   >
                    <c:forEach items="${itemList}" var="company"> 
                        <% if (index == 0) {%>  
                        <tr>
                            <td ><spring:message code="stores.label.ServicePoint:"  text="ServicePoint :"/>
</td>
                            <td ><c:out value="${company.companyName}"/></td>                                        
                            <% index++;
            }%>                     
                  </c:forEach>
                            <td ><spring:message code="stores.label.RequestNo:"  text="RequestNo :"/></td>
                            <td ><c:out value="${RequestId}"/></td>

                    </tr>
                </table>
                <br><br>
    <%  index = 0;%>             
                <!--<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">-->
                <table class="table table-info mb30 table-hover" id="bg"   >
                   <thead>
                    <tr>
                        <th width="29" rowspan="2" class="contentsub"><spring:message code="stores.label.Sno"  text="default text"/></td>
                        <th width="115" rowspan="2" class="contentsub">MFR Item Code</td>
                        <th width="91" rowspan="2" class="contentsub">PAPL Item Code</td>
                        <th width="64" rowspan="2" class="contentsub"><spring:message code="stores.label.ItemName"  text="default text"/></td>
                        <th width="77" rowspan="2" class="contentsub"><spring:message code="stores.label.RequestQuantity"  text="default text"/></td>
                        <th height="87" colspan="2" class="contentsub"><spring:message code="stores.label.RequestorStockAvailability"  text="default text"/> </td>
                        <th height="87" colspan="2" class="contentsub"><spring:message code="stores.label.ApproverStockAvailability"  text="default text"/> </td>
                        <th width="106"  class="contentsub"><spring:message code="stores.label.OtherRequestQty"  text="default text"/></span> </td>
                        <th width="113"  colspan="2" rowspan="2" class="contentsub"><spring:message code="stores.label.IssueQuantity"  text="default text"/></td>
                    </tr>
                    <tr>
                        <th height="5" class="contentsub" align="right" ><spring:message code="stores.label.New"  text="default text"/></td>
                        <th height="5" class="contentsub" align="right" >RC</td>
                        <th height="5" class="contentsub" align="right" ><spring:message code="stores.label.New"  text="default text"/></td>
                        <th height="5" class="contentsub" align="right" >RC</td>
                        <th height="5" class="contentsub" align="right" >&nbsp;</td>
                    </tr>
                    <thead>
        <c:forEach items="${itemList}" var="item">               
                    
                    <%
            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                    %>   
                  
                        <tr>
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${item.paplCode}"/></td>
                            <td class="<%=classText %>" height="30"><input type="hidden" name="itemIds" value="<c:out value="${item.itemId}"/>"><c:out value="${item.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><input type="hidden" name="requestedQtys" value="<c:out value="${item.requestedQty}"/>"><c:out value="${item.requestedQty}"/></td>
                            <td width="69" height="30" class="<%=classText %>"><div align="left"><c:out value="${item.requestPtNewItem}"/></div></td>
                            <td width="86" height="30" class="<%=classText %>"><div align="left"><c:out value="${item.requestPtRcItem}"/></div></td>
                            <td width="76" height="30" class="<%=classText %>"><div align="left"><input type="hidden" name="rcItem" value="<c:out value="${item.approvePtNewItem}"/>"><c:out value="${item.approvePtNewItem}"/></div></td>
                            <td width="76" height="30" class="<%=classText %>"><div align="left"><input type="hidden" name="newItem" value="<c:out value="${item.approvePtRcItem}"/>"><c:out value="${item.approvePtRcItem}"/></div></td>
                            <td class="<%=classText %>"><c:out value="${item.otherStockReq}"/></td>
							
                            <td class="<%=classText %>" height="30"><input type="text" maxlength="10" id="approvedQtys<%= index %>" name="approvedQtys" value="" class="form-control"  size="5" class="<%=classText %>" onchange="setSelectbox('<%= index %>');" ></td>                              
                            <td width="77" height="30" class="<%=classText %>"><input type="hidden" name="selectedIndex" value='<%= index %>'></td>
                        </tr>
                        <%
            index++;
                        %>
                  </c:forEach>                   
                    <input type="hidden" name="requestId" value="">
                    <input type="hidden" name="requestPtId" value="">
  </table>
<%
ArrayList al = (ArrayList)request.getAttribute("itemList");
Iterator itr = al.iterator();
StockTransferTO listTO = null;
if (itr.hasNext()) {
    listTO = (StockTransferTO) itr.next();
    System.out.println(listTO.getCompanyName());
    System.out.println(listTO.getRequestedQty());
    System.out.println("remarks:"+listTO.getRemarks());
 }
%>

                <br>
                <div  align="center"> Request Remarks &nbsp;&nbsp;:&nbsp;&nbsp;<textarea class="form-control" style="width:240px;height:40px" rows="5" cols="30" readonly name="requestRemarks" ><%=listTO.getRemarks()%></textarea> </div>
                <br>
                <br>
                <div  align="center"> Approve Remarks &nbsp;&nbsp;:&nbsp;&nbsp;<textarea class="form-control" style="width:240px;height:40px" rows="5" cols="30" name="remarks" ></textarea> </div>
                <br>
                <center>
                    <input type="button" class="btn btn-success"   value="Approve" name="APPROVED" onclick="submitPage(this.name);"> &nbsp;&nbsp;
                    <input type="button" class="btn btn-success"   value="Reject" name="REJECTED" onclick="submitPage(this.name);">
                    <input type="hidden" value="" name="approvalStatus">
                </center>                
          </c:if>
        <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
</body>
 </div>
            </div>
            </div>
            <%@ include file="../common/NewDesign/settings.jsp" %>
