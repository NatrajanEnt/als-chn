
<%--
    Document   : viewLPS
    Created on : Nov 21, 2012, 4:48:05 PM
    Author     : entitle
--%>



<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>

        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <title>Add LPS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(val){
                if(val == 'Add'){
                    document.lps.action = '/throttle/addLPSPage.do';
                    document.lps.submit();
                }
                else if(val == 'add'){
                    document.lps.action = '/throttle/addLPSPage.do';
                    document.lps.submit();
                }else if(val == 'modify'){
                    document.lps.action = '/throttle/alterLPSDetail.do';
                    document.lps.submit();
                }else if(val == 'fetchData'){
                    var fromDate = document.lps.fromDate.value;
                    var toDate = document.lps.toDate.value;
                    if(fromDate != ''){
                        if(toDate == ''){
                            alert('please enter to date');
                        }else {
                            document.lps.action = '/throttle/manageLPS.do';
                            document.lps.submit();
                        }
                    }else{
                        document.lps.toDate.value = '';
                        document.lps.action = '/throttle/manageLPS.do';
                        document.lps.submit();
                    }

                }

            }


            function setValues(){
                var fromDate='<%=request.getAttribute("fromDate")%>';
                if(fromDate!='null'){
                    document.lps.fromDate.value=fromDate;
                }
                var toDate='<%=request.getAttribute("toDate")%>';
                if(toDate!='null'){
                    document.lps.toDate.value=toDate;
                }
                var lpsNo='<%=request.getAttribute("lpsNo")%>';
                if(lpsNo!='null'){
                    document.lps.lpsNo.value=lpsNo;
                }
                var destination='<%=request.getAttribute("destination")%>';
                if(destination!='null'){
                    document.lps.destination.value=destination;
                }
                var lpsStatus='<%=request.getAttribute("lpsStatus")%>';
                if(lpsStatus !='null'){
                    document.lps.lpsStatus.value=lpsStatus;
                }
            }

        </script>

       
    </head>
    <body onload="setImages(1,0,0,0,0,0);setValues();">
        <form name="lps" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <table width="700" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Export" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:950px;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Search LPS </li>
                            </ul>
                            <div id="first">
                                <table width="100%" cellpadding="0" cellspacing="1" border="0" align="center" class="table4">
                                    <tr>
                                        <td>From Date</td>
                                        <td><input type="text" id="fromDate" name="date" value="" class="datepicker" autocomplete="off"></td>
                                        <td>To Date</td>
                                        <td><input type="text" id="toDate" name="date" value="" class="datepicker" autocomplete="off"></td>
                                        <td>LPS No</td>
                                        <td><input type="text" id="lpsNo" name="lpsNo" value="" class="textbox" autocomplete="off"></td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>Destination</td>
                                        <td><input type="text" id="destination" name="destination" value="" class="textbox" autocomplete="off"></td>
                                        <td>Status</td>
                                        <td>
                                            <select name="lpsStatus">
                                                <option value="0">ALL</option>
                                                <option value="1">Pink Slip Not Created</option>
                                                <option value="2">Pink Slip Generated, Trip Not Created</option>
                                                <option value="3">Pink Slip Generated, Trip Generated</option>
                                            </select>
                                        </td>

                                        <td><input type="button" class="button" onclick="submitPage(this.name);" name="fetchData" value="FetchData"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <c:if test = "${lpsList != null}" >

                <table  border="0" class="border" align="center" width="83%" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td class="table">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
                                            <tr>
                                            <td class="bottom" align="left"><img src="images/left_status.jpg" alt=""  /></td>
                                            <td class="bottom" height="35" align="right"><img src="images/icon_active.png" alt="" /></td>
                                            <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left"><%= request.getAttribute("pinkslipnotcreated")%></span></td>
                                            <td class="bottom" align="right"><img src="images/icon_ps_created.png" alt="" /></td>
                                            <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;"><%= request.getAttribute("pinkslipalonecreated")%></span></td>
                                            <td class="bottom" align="right"><img src="images/icon_ps_closed.png" alt="" /></td>
                                            <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;"><%= request.getAttribute("pinkslipandtripcreated")%></span></td>
                                            <td align="center"><h2>Total  <%= request.getAttribute("totalCount")%></h2></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                                            <br>
                <table align="center" width="100%" border="0" id="table" class="sortable">
                    <thead>
                    <tr height="30">
                        <th ><h3>S.No</h3></th>
                        <th><h3>LPS Number</h3></th>
                        <th><h3>Party's Name</h3></th>
                        <th><h3>LPS Date</h3></th>
                        <th><h3>Bill Status</h3></th>
                        <th><h3>Order No</h3></th>
                        <th><h3>Contractor</h3></th>
                        <th><h3>Product Name</h3></th>
                        <th><h3>Quantity</h3></th>
                        <th><h3>Bags</h3></th>
                        <th><h3>Priority</h3></th>
                        <th><h3>Destination</h3></th>
                        <th><h3>Edit</h3></th>
                    </tr>
                    </thead>
                    <tbody>

                        <% int index = 0;%>
                        <c:forEach items="${lpsList}" var="LL">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                            <tr height="30">
                                <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${LL.lpsID}" /></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.partyName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.lpsDate}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.billStatus}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.orderNumber}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.contractor}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.productName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.quantity}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.bags}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.clplPriority}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${LL.destination}"/> </td>
                                <td class="<%=classText%>" align="left"> <a href="/throttle/alterLPSDetail.do?lpsID=<c:out value='${LL.lpsID}' />" > Edit </a> </td>
                            </tr>

                            <% index++;%>
                        </c:forEach>
                    </c:if>
                </tbody>
            </table>
            <br>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

            <div id="controls">
		<div id="perpage">
			<select onchange="sorter.size(this.value)">
			<option value="5" selected="selected">5</option>
				<option value="10">10</option>
				<option value="20">20</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select>
			<span>Entries Per Page</span>
		</div>
		<div id="navigation">
			<img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
			<img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
			<img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
			<img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
		</div>
		<div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
	</div>
	
	<script type="text/javascript">
  var sorter = new TINY.table.sorter("sorter");
	sorter.head = "head";        
	sorter.asc = "asc";
        sorter.desc = "desc";
	sorter.even = "evenrow";
	sorter.odd = "oddrow";
	sorter.evensel = "evenselected";
	sorter.oddsel = "oddselected";
	sorter.paginate = true;
	sorter.currentid = "currentpage";
	sorter.limitid = "pagelimit";
	sorter.init("table",1);
  </script>
    </body>
</html>

