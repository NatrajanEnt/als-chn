
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
    <script>
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                dateFormat: 'dd-mm-yy',
                changeMonth: true, changeYear: true
            });
        });
        function searchPage() {
            document.BRS.action = '/throttle/bankReconciliationReport.do?param=search';
            document.BRS.submit();
            //debitAddRow();
            //creditAddRow();
        }

    </script>
    <script type="text/javascript">
        function getDates() {//pavithra
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

            var lastDayWithSlashes = (lastDay.getDate()) + '-' + (lastDay.getMonth() + 1) + '-' + lastDay.getFullYear();
            var firstDayWithSlashes = (firstDay.getDate()) + '-' + (firstDay.getMonth() + 1) + '-' + firstDay.getFullYear();
//alert(firstDayWithSlashes);
            document.getElementById("fromDate").value = firstDayWithSlashes;
            document.getElementById("toDate").value = lastDayWithSlashes;

        }

    </script>
    
    
    <style>
    #index td {
   color:white;
}
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.BankReconciliationStatementReport"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.BankReconciliationStatementReport"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    
    
<body onload="getDates();">
    <form method="post" action="/throttle/addGroupPage.do" name="BRS">
       <table class="table table-info mb30 table-hover">
           <thead>
              <tr >
                             <th colspan="4" ><b><spring:message code="finance.label.BankReconciliationStatementReport"  text="default text"/></b></th>&nbsp;&nbsp;&nbsp;                    
                    </tr>
           </thead>
                            <tr>
                                <td><font color="red">*</font><spring:message code="finance.label.FromDate"  text="default text"/></td>
                                <td height="30"><input name="fromDate"  autocomplete="off" id="fromDate" value="<c:out value="${date}"/>" type="text" class="datepicker" style="width:260px;height:40px;"></td>
                                <td><font color="red">*</font><spring:message code="finance.label.ToDate"  text="default text"/></td>
                                <td height="30"><input name="toDate"  autocomplete="off" id="toDate" value="<c:out value="${date}"/>" type="text" class="datepicker" style="width:260px;height:40px;"></td>
                                
                            </tr>
                            <tr>

                                <td><spring:message code="finance.label.BankName"  text="default text"/></td>
                                <td>
                                    <select class="form-control" name="bankId" id="bankId" style="width:260px;height:40px;" onchange="getBankBranch(this.value);">
                                            <option value="0">---<spring:message code="finance.label.Select"  text="default text"/>---</option>
                                            <c:if test = "${bankList != null}" >
                                                <c:forEach items="${bankList}" var="bank">
                                                    <option value='<c:out value="${bank.primaryBankId}" />'><c:out value="${bank.primaryBankName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                            <script>
                                                document.getElementById("bankId").value = <c:out value="${bankId}"/>;
                                            </script>
                                        </select>
                                </td>
                                <td><spring:message code="finance.label.BankBranch"  text="default text"/> </td>
                                <td>
                                   <select  id='bankBranchId' name='bankBranchId' class="form-control" style="width:260px;height:40px;">
                                        </select>
                                        <script>
                                            setBankBranch('<c:out value="${bankId}"/>', '<c:out value="${bankBranchId}"/>');
                                        </script>
                                </td>  
                                
                            </tr>
                            <tr>
                                <td colspan="4" align="center">
                                        <input type="button"  value="<spring:message code="finance.label.Search"  text="default text"/>" class="btn btn-success" name="Search" onclick="searchPage();" style="width:100px;height:35px;">
                                    </td>
                            </tr>

        </table>
        <script>
            function getBankBranch(bankId) {
                //alert(bankId);
                var temp = "";
                $.ajax({
                    url: '/throttle/getBankBranchDetails.do',
                    data: {bankId: bankId},
                    dataType: 'json',
                    success: function(temp) {
//                                         alert(temp);
                        if (temp != '') {
                            $('#bankBranchId').empty();
                            $('#bankBranchId').append(
                                    $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                                    )
                            $.each(temp, function(i, data) {
                                $('#bankBranchId').append(
                                        $('<option value="' + data.branchId + "~" + data.bankLedgerId + '" style="width:150px"></option>').val(data.branchId + "~" + data.bankLedgerId).html(data.branchName)
                                        )
                            });
                        } else {
                            $('#bankBranchId').empty();
                        }
                    }
                });
            }

        </script>
        <br>
          <c:set var="asPerStatement" value="${0.00}" />
                <c:set var="totalDifference" value="${0.00}" />
                <c:set var="grandTotal" value="${0.00}" />
        <c:if test = "${brsHeaderList != null}" >
            <c:forEach items="${brsHeaderList}" var="list1"> 
                <c:set var="asPerStatement" value="${list1.asPerStatement}" />
                <c:set var="totalDifference" value="${list1.totalDifference}" />
                <c:set var="grandTotal" value="${list1.grandTotal}" />
            </c:forEach>
        </c:if>

        <div id="chequeIssuranceTab" width="94%">
        <c:if test = "${brsIssuedList != null}" >
             <table>
                <tr id="index">
                    <td style="color:black" align="left">
                    <!--<td style="color:#0188b9;" align="left">-->
                    <b><spring:message code="finance.label.Add"  text="default text"/> : <spring:message code="finance.label.ChequeIssuedButNotCleared"  text="default text"/></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td width="80" style="color:black"><b><spring:message code="finance.label.AsPerStatement"  text="default text"/>:<b></td>
                    <td width="80" height="25"><input name="perStatement" id="perStatement" class="form-control" type="text"   style="width:260px;height:40px;" size="20" value="<c:out value="${asPerStatement}"/>" onkeyup="caldifferenceAmt();"></td>
                    <td width="80" style="color:black"><b><spring:message code="finance.label.BankBalance"  text="default text"/>:<b></td>
                    <td><input name="bankBalance" id="bankBalance" type="text"  class="form-control"  style="width:260px;height:40px;" value="<c:out value="${asPerStatement}"/>" readonly></td>
               </tr>
         </table>
            
                                            <br>
                                            <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                        <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                        <th><spring:message code="finance.label.VoucherNo"  text="default text"/></th>
                        <th><spring:message code="finance.label.PaidTo"  text="default text"/></th>
                        <th><spring:message code="finance.label.ChequeNo"  text="default text"/></th>
                        <th><spring:message code="finance.label.ChequeDate"  text="default text"/></th>
                        <th><spring:message code="finance.label.ClearanceDate"  text="default text"/></th>
                        <th><spring:message code="finance.label.ChequeAmount"  text="default text"/></th>
                </thead>
                                                
                                                    <tbody width="94%">
                                                        <% int sno = 0;
                           int index = 1;%>
                                                        <c:set var="totalIssuedAmount" value="${0.00}" />
                                                        <c:forEach items="${brsIssuedList}" var="list2"> 
                                                            <c:set var="totalIssuedAmount" value="${totalIssuedAmount + list2.chequeAmount}" />
                                                           
                                                            <tr height="40" > 
                                                                <td  height="20"><%=index%></td>
                                                                <td  height="20">
                                                                    <c:out value="${list2.voucherCode}"/>
                                                                </td>
                                                                <td  height="20">
                                                                    <c:out value="${list2.ledgerName}"/>
                                                                </td>
                                                                <td  height="20">
                                                                    <c:out value="${list2.chequeNo}"/>
                                                                </td>
                                                                <td  height="20">
                                                                    <c:out value="${list2.chequeDate}"/>
                                                                </td>
                                                                <td  height="20">
                                                                    <c:out value="${list2.chequeClearanceDate}"/>
                                                                </td>
                                                                <td  height="20" align="right">
                                                                    <c:out value="${list2.chequeAmount}"/>
                                                                </td>

                                                            </tr>
                                                            <%
                                                                index++;
                                                                sno++;
                                                            %>
                                                        </c:forEach>
                                                        <tr>
                                                            <td colspan="6" height="20" align="right" style="color:black"><spring:message code="finance.label.TotalIssuedAmount"  text="default text"/></td>
                                                            <td  height="20" align="right" style="color:black">
                                                                <c:out value="${totalIssuedAmount}"/></td>  
                                                        </tr>
                                                   
                                                </tbody>
                                            </table>
                                                        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <!--<div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>-->
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
        <br>
                                                         </c:if>
                                            <!--                                         </div> 
                                             <div id="creditTab">          -->
                                                                                   
                                              <c:if test = "${brsCreditList != null}" >                             
                                            <table>
                <tr id="index">
                    <td style="color:black" align="left">
                        <b><spring:message code="finance.label.Add"  text="default text"/> : <spring:message code="finance.label.BankCredit"  text="default text"/></b>
                                                    </td>
                                                    <td width="80"></td>
                                                    <td width="80" height="25"></td>
                                                    <td width="80"></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            <br>  

<table class="table table-info mb30 table-hover" id="table1">
                    <thead>
                        <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                        <th><spring:message code="finance.label.Date"  text="default text"/></th>
                        <th><spring:message code="finance.label.PaidTo"  text="default text"/></th>
                        <th><spring:message code="finance.label.ChequeNo"  text="default text"/></th>
                        <th><spring:message code="finance.label.ChequeDate"  text="default text"/></th>
                        <th><spring:message code="finance.label.Ourreference"  text="default text"/></th>
                        <th><spring:message code="finance.label.Amount"  text="default text"/> </th>
                </thead>
                                           
                                              
                                                    <tbody>
                                                        <% int sno = 0;
                                                           int index = 1;%>
                                                        <c:set var="creditTotalAmt" value="${0.00}" />
                                                        <c:forEach items="${brsCreditList}" var="list3"> 
                                                            <c:set var="creditTotalAmt" value="${creditTotalAmt + list3.chequeAmount}" />

                                                            <tr  width="208" height="40" > 
                                                                <td  height="20"><%=index%></td>
                                                                <td  height="20">
                                                                    <c:out value="${list3.fromDate}"/>
                                                                </td>
                                                                <td  height="20">
                                                                    <c:out value="${list3.ledgerName}"/>
                                                                </td>
                                                                <td  height="20">
                                                                    <c:out value="${list3.chequeNo}"/>
                                                                </td>
                                                                <td  height="20">
                                                                    <c:out value="${list3.chequeDate}"/>
                                                                </td>
                                                                <td  height="20">
                                                                    <c:out value="${list3.headerNarration}"/>
                                                                </td>
                                                                <td  height="20" align="right">
                                                                    <c:out value="${list3.chequeAmount}"/>
                                                                </td>

                                                            </tr>
                                                            <%
                                                                index++;
                                                                sno++;
                                                            %>
                                                        </c:forEach>
                                                        <tr>
                                                            <td colspan="6" height="20" align="right" style="color:black"><spring:message code="finance.label.TotalCreditAmount"  text="default text"/></td>
                                                            <td  height="20" align="right" style="color:black">
                                                                <c:out value="${creditTotalAmt}"/>
                                                            </td>  
                                                        </tr>
                                                </tbody>
                                            </table> 
                                                             <script language="javascript" type="text/javascript">
            setFilterGrid("table1");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <!--<div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>-->
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table1", 0);
        </script>
        <br>
                                                 </c:if>

                                            </div>
                                            <div id="chequeIssuranceTab" width="94%">
                                                  <c:if test = "${brsReceivedList != null}" >
                                                   <table>
                <tr id="index">
                    <td style="color:black" align="left"><b><spring:message code="finance.label.Less"  text="default text"/> : <spring:message code="finance.label.ChequeDepositedButNotCleared"  text="default text"/></b>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <br>
 <table class="table table-info mb30 table-hover" id="table2">
                                                <thead>
                                                       
                                                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.VoucherNo"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.ReceivedFrom"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.ChequeNo"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.ChequeDate"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.ClearanceDate"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.ChequeAmount"  text="default text"/></th>
                                                    </thead>
                                                  
                                                        <tbody>
                                                            <% int sno = 0;
                                                           int index = 1;%>
                                                            <c:set var="totalReceivedAmount" value="${0.00}" />
                                                            <c:forEach items="${brsReceivedList}" var="list4"> 
                                                                <c:set var="totalReceivedAmount" value="${totalReceivedAmount + list4.chequeAmount}" />

                                                              
                                                                <tr  width="208" height="40" > 
                                                                    <td  height="20"><%=index%></td>
                                                                    <td  height="20">
                                                                        <c:out value="${list4.voucherCode}"/>
                                                                    </td>
                                                                    <td  height="20">
                                                                        <c:out value="${list4.ledgerName}"/>
                                                                    </td>
                                                                    <td  height="20">
                                                                        <c:out value="${list4.chequeNo}"/>
                                                                    </td>
                                                                    <td  height="20">
                                                                        <c:out value="${list4.chequeDate}"/>
                                                                    </td>
                                                                    <td  height="20">
                                                                        <c:out value="${list4.chequeClearanceDate}"/>
                                                                    </td>
                                                                    <td  height="20" align="right">
                                                                        <c:out value="${list4.chequeAmount}"/>
                                                                    </td>

                                                                </tr>
                                                                <%
                                                                    index++;
                                                                    sno++;
                                                                %>
                                                            </c:forEach>
                                                            <tr>
                                                                <td colspan="6" height="20" align="right" style="color:black"><spring:message code="finance.label.TotalReceivedAmount"  text="default text"/></td>
                                                                <td  height="20" align="right" style="color:black">
                                                                    <c:out value="${totalReceivedAmount}" />
                                                                </td>  
                                                            </tr>
                                                       
                                                    </tbody>
                                                </table>
                                                                 <script language="javascript" type="text/javascript">
            setFilterGrid("table2");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <!--<div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>-->
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table2", 0);
        </script>
        <br>
                                                                 </c:if>
                                                <!--                                            </div> <div id="debitTab">  -->
                                                     <c:if test = "${brsDebitList != null}" >                                        
                                                <table>
                <tr id="index">
                    <td style="color:black" align="left"><b><spring:message code="finance.label.Less"  text="default text"/> :<spring:message code="finance.label.BankDebit"  text="default text"/> </b>
                                                        </td>
                                                        <td width="80"></td>
                                                        <td width="80" height="25"></td>
                                                        <td width="80"></td>
                                                        <td></td>
                                                    </tr>
                                                </table>

                                                <table class="table table-info mb30 table-hover" id="table3">
                                                <thead>
                                                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.Date"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.ReceivedFrom"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.ChequeNo"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.ChequeDate"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.Ourreference"  text="default text"/></th>
                                                            <th><spring:message code="finance.label.Amount"  text="default text"/></th>
                                                    </thead>
                                                   
                                                        <tbody width="94%">
                                                            <% int sno = 0;
                                                           int index = 1;%>
                                                            <c:set var="debitTotalAmt" value="${0.00}" />
                                                            <c:forEach items="${brsDebitList}" var="list5"> 
                                                                <c:set var="debitTotalAmt" value="${debitTotalAmt + list5.chequeAmount}" />

                                                               
                                                                <tr  width="208" height="40" > 
                                                                    <td  height="20"><%=index%></td>
                                                                    <td  height="20">
                                                                        <c:out value="${list5.fromDate}"/>
                                                                    </td>
                                                                    <td  height="20">
                                                                        <c:out value="${list5.ledgerName}"/>
                                                                    </td>
                                                                    <td  height="20">
                                                                        <c:out value="${list5.chequeNo}"/>
                                                                    </td>
                                                                    <td  height="20">
                                                                        <c:out value="${list5.chequeDate}"/>
                                                                    </td>
                                                                    <td  height="20">
                                                                        <c:out value="${list5.headerNarration}"/>
                                                                    </td>
                                                                    <td  height="20" align="right">
                                                                        <c:out value="${list5.chequeAmount}"/>
                                                                    </td>

                                                                </tr>
                                                                <%
                                                                    index++;
                                                                    sno++;
                                                                %>
                                                            </c:forEach>
                                                            <tr>
                                                                <td colspan="6" height="20" align="right"><spring:message code="finance.label.TotalCreditAmount"  text="default text"/></td>
                                                                <td  height="20" align="right">
                                                                    <c:out value="${debitTotalAmt}"/>
                                                                </td>  
                                                            </tr>
                                                       
                                                    </tbody>
                                                </table> 
                                                                 <script language="javascript" type="text/javascript">
            setFilterGrid("table3");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <!--<div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>-->
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table3", 0);
        </script>
 </c:if>
                                            </div>
                                            <br>
                                            <table width="94%" align="center" border="0">
                                                <tr>
                                                    <td>
                                                        <h2 align="left">&nbsp;</h2>
                                                    </td>
                                                    <td width="80" colspan="2" align="right" style="color:black" ><b><spring:message code="finance.label.TotalDifferenceAmount"  text="default text"/>:&emsp;<b></td>
                                                                <td width="80" height="25" style="color:black"><input name="diffrenceAmt" id="diffrenceAmt" class="form-control" style="width:260px;height:40px;" type="text"   value="<c:out value="${totalDifference}"/>"></td>
                                                                <td width="80" colspan="2" align="right" style="color:black"><b><spring:message code="finance.label.Groundtotal"  text="default text"/>:&emsp;<b></td>
                                                                            <td style="color:black"><input name="grandTotal" id="grandTotal" type="text" class="form-control" style="width:260px;height:40px;" value="<c:out value="${grandTotal}"/>"></td>
                                                                            </tr>
                                                                            </table>

                                                                            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                                                                            </body>
 </div>
 </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>