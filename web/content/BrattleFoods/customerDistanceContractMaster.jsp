<%--
    Document   : customerContractMaster
    Created on : Oct 29, 2013, 10:39:28 AM
    Author     : Arul
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    var httpRequest;
    function contractRouteCheck() {
        var companyId = 0;
        //var contractId = document.getElementById("contractId").value;
        //companyId =parseInt(document.getElementById("companyId").value);
        //alert("THIS --- ");

            var customerId = document.getElementById("customerId").value;
            
            if(customerId != "0"){
            var ptpVehicleTypeId = document.getElementById("ptpVehicleTypeId").value;
            var ptpPickupPointId = document.getElementById("ptpPickupPointId").value;
            var ptpInterimPoint1 = document.getElementById("interimPointId1").value;
            var ptpInterimPoint2 = document.getElementById("interimPoint2").value;
            var ptpInterimPoint3 = document.getElementById("interimPoint3").value;
            var ptpInterimPoint4 = document.getElementById("interimPoint4").value;
            var ptpDropPointId = document.getElementById("ptpDropPointId").value;
            var loadTypeId = document.getElementById("loadTypeId").value;
            var containerTypeId = document.getElementById("containerTypeId").value;
            var containerQty = document.getElementById("containerQty").value;
            var ptpRateWithReefer = document.getElementById("ptpRateWithReefer").value;
            var ptpRateWithoutReefer = document.getElementById("ptpRateWithoutReefer").value;
            if(ptpVehicleTypeId != "" && ptpVehicleTypeId != 0 && ptpPickupPointId != "" && ptpPickupPointId != 0 && ptpDropPointId != "" && ptpDropPointId != 0
                  && loadTypeId != "" && loadTypeId != 0 && containerTypeId != "" && containerTypeId != 0 &&  containerQty != "" && containerQty != 0 && ptpRateWithReefer != ""
                         &&   ptpRateWithoutReefer != ""  ) {
            
            $.ajax({
                        url: "/throttle/compareContract.do",
                        dataType: "json",
                        data: {
                            ptpVehicleTypeId: ptpVehicleTypeId,
                            ptpPickupPointId: ptpPickupPointId,
                            ptpInterimPoint1: ptpInterimPoint1,
                            ptpInterimPoint2: ptpInterimPoint2,
                            ptpInterimPoint3: ptpInterimPoint3,
                            ptpInterimPoint4: ptpInterimPoint4,
                            ptpDropPointId: ptpDropPointId,
                            loadTypeId: loadTypeId,
                            containerTypeId: containerTypeId,
                            //contractId: contractId,
                            containerQty: containerQty,
                            ptpRateWithReefer: ptpRateWithReefer,
                            ptpRateWithoutReefer: ptpRateWithoutReefer
                        },
                        success: function(data, textStatus, jqXHR) {
                                $.each(data, function(i, data) {
                                    if(data.Id==0){
                                        alert("Contract data for the route is not available at Organizational level. Please reenter");
                                        $('#ptpPickupPoint').val('');
                                        $('#ptpPickupPointId').val('');
                                        $("#interimPoint1").val('');
                                        $("#interimPointId1").val('');
                                        $("#interimPoint2").val('');
                                        $("#interimPointId2").val('');
                                        $("#interimPoint3").val('');
                                        $("#interimPointId3").val('');
                                        $("#interimPoint4").val('');
                                        $("#interimPointId4").val('');
                                        $("#ptpDropPoint").val('');
                                        $("#ptpDropPointId").val('');
                                        $("#interimPoint1Km").val('');
                                        $("#interimPoint2Km").val('');
                                        $("#interimPoint3Km").val('');
                                        $("#interimPoint4Km").val('');
                                        $("#ptpDropPointKm").val('');
                                        $("#interimPoint1Hrs").val('');
                                        $("#interimPoint2Hrs").val('');
                                        $("#interimPoint3Hrs").val('');
                                        $("#interimPoint4Hrs").val('');
                                        $("#ptpDropPointHrs").val('');
                                        $("#interimPoint1Minutes").val('');
                                        $("#interimPoint2Minutes").val('');
                                        $("#interimPoint3Minutes").val('');
                                        $("#interimPoint4Minutes").val('');
                                        $("#ptpDropPointMinutes").val('');
                                        $("#interimPoint1RouteId").val('');
                                        $("#interimPoint2RouteId").val('');
                                        $("#interimPoint3RouteId").val('');
                                        $("#interimPoint4RouteId").val('');
                                        $("#ptpDropPointRouteId").val('');
                                        $("#ptpTotalKm").val('');
                                        $("#ptpTotalHours").val('');
                                        $("#ptpTotalMinutes").val('');
                                        $('#ptpPickupPoint').focus();
                                        $("#ptpRateWithReefer").val('');
                                        $("#ptpRateWithoutReefer").val('');
                                        return false;
                                    }
                                    else{
                                           var valus = data.Name;
                                                var temp7 = valus.split("~");
                                                $("#orgRateWithReefer").text('');
                            $("#orgRateWithoutReefer").text('');
                                            if (temp7[0] == 'TBA') {
                                    alert("Contract rate doesn't match with Organizational data. Contract needs approval");
                                    $("#orgValues").text('Needs Approval');
                                    $("#orgValues").removeClass('noErrorClass');                
                                   $("#orgValues").addClass('errorClass');
                                     $("#orgRateWithReefer").text(temp7[1]);
                                                $("#orgRateWithoutReefer").text(temp7[2]);
                                    document.getElementById("approvalStatus").value = "2";
                                } else {
                                    $("#orgValues").text('Auto Approval');
                                    $("#orgValues").removeClass('errorClass');
                                    $("#orgValues").addClass('noErrorClass');
                                      $("#orgRateWithReefer").text(temp7[1]);
                                                $("#orgRateWithoutReefer").text(temp7[2]);
                                    document.getElementById("approvalStatus").value = "1";
                                }
                                    }
                                });
                        },
                        error: function (data, type) {
                            console.log(type);
                        }
                    });
                           }

            }
    }

</script>
<style>
    .noErrorClass{
        color: green;
    }
    .errorClass{
        color: red;
    }
</style>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });



</script>




    <% int index = 0;%>
    <script>
        var count = 1;
        $(document).ready(function() {
            // Get the table object to use for adding a row at the end of the table
            var $itemsTable = $('#itemsTable');

            // Create an Array to for the table row. ** Just to make things a bit easier to read.
            var rowTemp = "";
            <c:if test="${display == 'none'}">
                rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                        <c:if test="${customerId != '0'}">
                 '<td><span id="distanceOrgValues" style="font-size: 14px"></span><input type="hidden" name="distanceApprovalStatus" id="distanceApprovalStatus" value="1" /></td>',
                         </c:if>
                '<td><input name="referenceName" value="" class="form-control" id="referenceName"  style="width: 150px;" /></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="distnaceVehicleTypeId" id="distnaceVehicleTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                
                '<td><select name="containerTypeId" id="containerTypeId"  class="form-control" style="width:150px" ></td>',
                '<td><select name="containerQty" id="containerQty"  class="form-control" style="width:150px" ></td>',
                '<td><select name="loadTypeId" id="loadTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></td>',
                '<td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width: 150px;"  /></td>',
                
                '<td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width: 150px;"  /></td>',
               
                '<td><input name="rateWithReeferDistance" value="" class="form-control" id="rateWithReeferDistance"  style="width: 150px;" onchange="contractDistanceCheck(); onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="rateWithoutReeferDistance" value="" class="form-control" id="rateWithoutReeferDistance"  style="width: 150px;" onchange="contractDistanceCheck(); onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="fuelVehicle" value="" class="form-control" id="fuelVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                    <c:if test="${customerId == '0'}">
            '<td><input name="toll" value="" class="form-control" id="toll"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                 '<td><input name="driverBachat" value="" class="form-control" id="driverBachat"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                 '<td><input name="dala" value="" class="form-control" id="dala"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                 '<td><input name="misc" value="" class="form-control" id="misc"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                 '<td><input type="text" name="marketHireVehicle" value="0" class="form-control" id="marketHireVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input type="text" name="marketHireWithReefer" value="0" class="form-control" id="marketHireWithReefer"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                </c:if> 
                '</tr>'
            ].join('');
            </c:if>
            <c:if test="${display == 'block'}">
                rowTemp = [
                '<tr class="item-row">',
                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
                 <c:if test="${customerId != '0'}">
                 '<td><span id="distanceOrgValues" style="font-size: 14px"></span><input type="hidden" name="distanceApprovalStatus" id="distanceApprovalStatus" value="1" /></td>',
                                      </c:if>
                '<td><input name="referenceName" value="" class="form-control" id="referenceName"  style="width: 150px;" /></td>',
                '<td><c:if test="${vehicleTypeList != null}"><select name="distnaceVehicleTypeId" id="distnaceVehicleTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
                         '<td><select name="containerTypeId" id="containerTypeId"  class="form-control" style="width:150px" ></td>',
                '<td><select name="containerQty" id="containerQty"  class="form-control" style="width:150px" ></td>',
               '<td><select name="loadTypeId" id="loadTypeId"  class="form-control" style="width:150px" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></td>',
               '<td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width: 150px;" /></td>',
                
                '<td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width: 150px;"/></td>',
               
                '<td><input name="rateWithReeferDistance" value="0" class="form-control" id="rateWithReeferDistance"  style="width: 150px;" onchange="contractDistanceCheck(); onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input name="rateWithoutReeferDistance" value="0" class="form-control" id="rateWithoutReeferDistance"  style="width: 150px;" onchange="contractDistanceCheck(); onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                     <c:if test="${customerId == '0'}">
            '<td><input name="toll" value="" class="form-control" id="toll"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                 '<td><input name="driverBachat" value="" class="form-control" id="driverBachat"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                 '<td><input name="dala" value="" class="form-control" id="dala"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                 '<td><input name="misc" value="" class="form-control" id="misc"  style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input type="text" name="marketHireVehicle" value="0" class="form-control" id="marketHireVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
                '<td><input type="text" name="marketHireWithReefer" value="0" class="form-control" id="marketHireWithReefer"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
            </c:if>    
            '</tr>'
            ].join('');
            </c:if>
            // Add row to list and allow user to use autocomplete to find items.



            $("#addRow").bind('click', function() {
               
                count++;
               
              if( document.getElementById("itemsTable").style.visibility == "hidden"){
                 document.getElementById("itemsTable").style.visibility = "visible";
                 
                } else{  
                var $row = $(rowTemp);
               
                // save reference to inputs within row
                
                var $referenceName = $row.find('#referenceName');
                var $ptpVehicleTypeId = $row.find('#distnaceVehicleTypeId');
                
                var $containerTypeId = $row.find('#containerTypeId');
                var $containerQty = $row.find('#containerQty');
                var $ptpPickupPoint = $row.find('#fromDistance');
               
                
                var $ptpDropPoint = $row.find('#toDistance');
               
                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');
                 $row.find('#distnaceVehicleTypeId').change(function(){
//                        var $itemrow = $(this).closest('tr');
                        var val = $row.find('#distnaceVehicleTypeId').val();
                        if(val == '1058'){
                        // 20FT vehicle
                        $row.find('#containerTypeId').empty();
                        $row.find('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                        $row.find('#containerQty').empty();
                        $row.find('#containerQty').append($('<option ></option>').val(1).html('1'))
                        }else if(val == '1059'){
                        // 40FT vehicle
                        $row.find('#containerTypeId').empty();
                        $row.find('#containerTypeId').append($('<option ></option>').val(0).html('--Select--'))
                        $row.find('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                        $row.find('#containerTypeId').append($('<option ></option>').val(2).html('40'))
                        $row.find('#containerQty').empty();

                        }
                    });
                    $row.find('#containerTypeId').change(function(){
//                        var $itemrow = $(this).closest('tr');
                        var val = $row.find('#containerTypeId').val();
                        if(val == '1'){
                        // 20FT Container
                        $row.find('#containerQty').empty();
                        $row.find('#containerQty').append($('<option ></option>').val(2).html('2'))
                        }else if(val == '2'){
                        // 40FT Container
                        $row.find('#containerQty').empty();
                        $row.find('#containerQty').append($('<option ></option>').val(1).html('1'))

                        }
                    });
                $('.item-row:last', $itemsTable).after($row);
//                if ($('#ptpRouteContractCode:last').val() !== '') {}else{ // End if last itemCode input is empty
//                    alert("please fill route code");
//                }
                return false;}
            });


        });// End DOM
 $("#deleteRow").live('click', function() {
            $(this).parents('.item-row').remove();
            // Hide delete Icon if we only have one row in the list.
            if ($(".item-row").length < 2)
                $("#deleteRow").hide();
        });
       
            </script>

    <%index++;%>

<c:if test="${billingTypeId == 4}"></c:if>
<c:if test="${billingTypeId == 2}"></c:if>
<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term,
                        customerCode: document.getElementById('customerCode').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#customerCode').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerCode: request.term,
                        customerName: document.getElementById('customerName').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerCode").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#customerId').val(tmp[0]);
                $itemrow.find('#customerName').val(tmp[1]);
                $itemrow.find('#customerCode').val(tmp[2]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[2] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
</script>


<script type="text/javascript">
    function submitPage(value) {
        var validate = "";
//        if (document.getElementById('distnaceVehicleTypeId').value == "") {
//            alert("Please Select vehicle Type");
//            document.getElementById('distnaceVehicleTypeId').focus();
//        } else if (document.getElementById('containerTypeId').value == "") {
//            alert("Please Select container Type");
//            document.getElementById('containerTypeId').focus();
//        } else if (document.getElementById('fromDistance').value == "") {
//            alert("Please Enter from distance");
//            document.getElementById('fromDistance').focus();
//        } else if (document.getElementById('toDistance').value == "") {
//           alert("Please Enter from distance");
//            document.getElementById('fromDistance').focus();
//        } else {
                document.customerContract.action = "/throttle/saveCustomerDistanceContract.do";
                document.customerContract.submit();
         //   }
         
    }


    function validatePtpContract() {}

    function validateFixedContract() {}

    function validatePtpwContract() {}




    function  validateFixedKmRateDetails() {}
//    function loadRow(){
//        var contractListSize= document.getElementById("contractListSize").value;
//        if(contractListSize == 0){
//            document.getElementById("itemsTable").style.visibility = "visible";
//        }
//
//    }

</script>
<div class="pageheader">
    <c:if test="${customerId != '0'}">
    <h2><i class="fa fa-edit"></i> Customer</h2>
    </c:if>
    <c:if test="${customerId == '0'}">
    <h2><i class="fa fa-edit"></i> Company</h2>
    </c:if>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
           <li><spring:message code="general.label.home"  text="Home"/></li>
            <c:if test="${customerId == '0'}">
            <li>Company</li>
            </c:if>
            <c:if test="${customerId != '0'}">
            <li>Customer</li>
            </c:if>
            <li data-toggle="tab"class="active">Edit Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="loadRow();">
                <c:if test="${billingTypeId != 4}">
                <body onload="">
                </c:if>
                <c:if test="${billingTypeId == 4}">
                <body onload="addRow()">
                </c:if>
                <form name="customerContract"  method="post" >

                    <br>

                    <table class="table table-info mb30 table-hover"  >
                        <thead>
                        <tr >
                            <th colspan="4" >Customer Contract Master</th>
                        </tr>
                          </thead>
                     <c:if test="${customerId != '0'}">
                        <tr>
                            <td width="25%">Customer Name</td>
                            <td width="25%"><input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>" class="form-control"><input type="text" name="customerName" id="customerName" value="<c:out value="${customerName}"/>" class="form-control" readonly=""></td>
                            <td width="25%">Customer Code</td>
                            <td width="25%"><input type="text" name="customerCode" id="customerCode" value="<c:out value="${customerCode}"/>" class="form-control" readonly=""></td>
                        </tr>
                        <tr>
                            <td>Contract From</td>
                            <td ><c:if test="${contractListSize == 0}">
                                 <input type="text" name="contractFrom" id="contractFrom" value="<c:out value="${contractFrom}"/>" class="datepicker form-control">
                                 </c:if>
                                 <c:out value="${contractFrom}"/></td>
                            <td>Contract To</td>
                            <td > <c:if test="${contractListSize == 0}">
                               <input type="text" name="contractTo" id="contractTo" value="<c:out value="${contractTo}"/>" class="datepicker form-control">
                               </c:if>
                               <c:out value="${contractTo}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Contract No</td>
                            <td ><c:if test="${contractListSize == 0}">
                                <input type="text" name="contractNo" id="contractNo" value="<c:out value="${contractNo}"/>" class="form-control" >
                                </c:if>
                                <c:out value="${contractNo}"/></td>
                            <td>Billing Type</td>
                            <td><input type="hidden" name="billingTypeId" id="billingTypeId" value="<c:out value="${billingTypeId}"/>"/>
                                <c:if test="${billingTypeList != null}">
                                    <c:forEach items="${billingTypeList}" var="btl">
                                        <c:if test="${billingTypeId == btl.billingTypeId}">
                                            <c:out value="${btl.billingTypeName}"/>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                            </td>
                        </tr>
                        <input type="hidden" name="companyId" id="companyId" value="0" class="form-control">
                    </c:if>
                    <c:if test="${customerId == '0'}">
                        <tr>
                         <td width="25%">Company Name</td>
                            <td width="25%"><input type="hidden" name="companyId" id="companyId" value="<c:out value="${companyId}"/>" class="form-control"><input type="text" name="companyName" id="companyName" value="<c:out value="${companyName}"/>" class="form-control" readonly=""></td>
                            <td colspan="2"   >&nbsp;</td>
                            <input type="hidden" name="contractFrom" id="contractFrom" value="00-00-0000" class="datepicker form-control">
                            <input type="hidden" name="contractTo" id="contractTo" value="00-00-0000" class="datepicker form-control">
                            <input type="hidden" name="contractNo" id="contractNo" value="0" class="form-control">
                            <input type="hidden" name="billingTypeId" id="billingTypeId" value="1" class="form-control">
                            <input type="hidden" name="customerId" id="customerId" value="0" class="form-control">
                        </tr>
                    </c:if>
                    <input type="hidden" name="display" id="display" value="<c:out value="${display}"/>" class="form-control">
                    </table>
                    <br>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <c:if test="${billingTypeId == 1}">
                                </c:if>
                                <li data-toggle="tab" class="active"  id="pp" style="display: block"><a href="#ptp"><span>Distance - Based </span></a></li>
                               
                               
                            <!--                    <li><a href="#pc"><span>Penalty Charges </span></a></li>
                                                <li><a href="#dc"><span>Detention Charges </span></a></li>-->
                        </ul>

                        <script>
                            function setContainerTypeList(val){
                                if(val == '1058'){
                                    // 20FT vehicle
                                    $('#containerTypeId').empty();
                                    $('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                                    $('#containerQty').empty();
                                    $('#containerQty').append($('<option ></option>').val(1).html('1'))
                                }else if(val == '1059'){
                                    // 40FT vehicle
                                    $('#containerTypeId').empty();
                                    $('#containerTypeId').append($('<option ></option>').val(0).html('--Select--'))
                                    $('#containerTypeId').append($('<option ></option>').val(1).html('20'))
                                    $('#containerTypeId').append($('<option ></option>').val(2).html('40'))
                                    $('#containerQty').empty();

                                }
                               contractRouteCheck('');
                            }
                            function setContainerQtyList(val){
                                if(val == '1'){
                                    // 20FT vehicle
                                    $('#containerQty').empty();
                                    $('#containerQty').append($('<option ></option>').val(2).html('2'))
                                }else if(val == '2'){
                                    // 40FT vehicle
                                    $('#containerQty').empty();
                                    $('#containerQty').append($('<option ></option>').val(1).html('1'))

                                }
                                contractRouteCheck('');
                            }
                            
                            function checkKey(obj, e, id, id1) {
                                var idVal = $('#' + id1).val();
//                                alert(e.keyCode);
                                if (e.keyCode == 46 || e.keyCode == 8) {
                                    $('#' + obj.id).val('');
                                    $('#' + id).val('');
                                    $('#' + id1).val('');
                                } else if (idVal != '' && e.keyCode != 13) {
                                    $('#' + obj.id).val('');
                                    $('#' + id).val('');
                                    $('#' + id1).val('');
                                }
                            }
                            
                            function checkPointNames(id,id1){
                                var idValue = $('#'+id).val();
                                var idValue1 = $('#'+id1).val();
                                if(idValue!='' && idValue1==''){
                                    alert('Invalid Point Name');
                                    $('#'+id).focus();
                                    $('#'+id).val('');
                                    $('#'+id1).val('');
                                }
                                
                            }
                            
                            
                        </script>
                         <script>
                                function configModelFuel(typeId, contractRateId,status) {
                                    //                                                                                                alert(slabId)
                                    $.ajax({
                                        url: "/throttle/configDistanceModelFuel.do?",
                                        data: {
                                            typeId: typeId, contractRateId: contractRateId,status:status

                                        },
                                        type: "GET",
                                        success: function (response) {
                                            $("#modelFuelDetailsTab").html(response);
                                        }
                                    });
                                }

                            </script>
                            
                            
<script type="text/javascript">
    function submitModelFuel() {
//            var fuelModelId = document.getElementById("fuelModelId").value;
//            alert(fuelModelId)
        var contractRateId = document.getElementById("contractRateIds").value;
        var distanceFlag = document.getElementById("distanceflag").value;
        var fuelModelIds = [];
        var modelIds = [];
        var fuelVehicles = [];
        var fuelsDG = [];
        var totalsFuel = [];
        var fuelModelId = document.getElementsByName("fuelModelId");
//            alert(fuelModelId.length)
        var modelId = document.getElementsByName("modelId");
        var fuelVehicle = document.getElementsByName("fuelVehicles");
        var fuelDG = document.getElementsByName("fuelDGs");
        var totalFuel = document.getElementsByName("totalFuels");
        for (var i = 0; i < modelId.length; i++) {
            fuelModelIds.push(fuelModelId[i].value);
//          contractRateIds.push(contractRateId);
            modelIds.push(modelId[i].value);
            fuelVehicles.push(fuelVehicle[i].value);
            fuelsDG.push(fuelDG[i].value);
            totalsFuel.push(totalFuel[i].value);
        }
        $('#spinner').show();
        $("#loader").addClass('ui-loader-background');
        var url = "";
        url = './saveModelFuelDetails.do';
//                    alert(url);
        $.ajax({
            url: url,
            data: {fuelModelId: fuelModelIds, contractRateId: contractRateId, modelId: modelIds,
                fuelVehicle: fuelVehicles, fuelDG: fuelsDG, totalFuel: totalsFuel ,distanceFlag: distanceFlag},
            type: "POST",
            dataType: 'json',
            success: function (data) {
//                                    alert(data)
                if (data == 1) {
                    $("#modelFuelStatus").text("Model fuel Saved Sucessfully");
                    $("#saveModel").hide();
                    $('#spinner').hide();
                    $("#loader").hide();

                }
                if (data == 2) {
                    $("#modelFuelStatus").text("Model fuel Updated Sucessfully");
                    $("#saveModel").hide();
                    $('#spinner').hide();
                    $("#loader").hide();

                }
            },
            error: function (xhr, status, error) {
                $("#modelFuelStatusError").text("error");
            }
        });
    }

    function sumFuel(sno) {
        var fuelRate = document.getElementById("fuelVehicles1" + sno).value;
        var fuelDG = document.getElementById("fuelDGs1" + sno).value;
        var totFuel;
        ptpPickupPoint
        if (fuelDG == "") {
            totFuel = parseInt(fuelRate);
            document.getElementById("totalFuels1" + sno).value = totFuel;
        }
        if (fuelRate == "") {
            totFuel = parseInt(fuelDG);
            document.getElementById("totalFuels1" + sno).value = totFuel;
        }
        if (fuelDG != "" && fuelRate != "") {
            totFuel = parseInt(fuelRate) + parseInt(fuelDG);
            document.getElementById("totalFuels1" + sno).value = totFuel;
        }
    }
    function sumFuels() {
        var fuelRate = document.getElementById("fVehicle").value;
        var fuelDG = document.getElementById("fuelDG").value;
        var totFuel;
//        ptpPickupPoint
        if (fuelDG == "") {
            totFuel = parseInt(fuelRate);
            document.getElementById("totFuel").value = totFuel;
        }
        if (fuelRate == "") {
            totFuel = parseInt(fuelDG);
            document.getElementById("totFuel").value = totFuel;
        }
        if (fuelDG != "" && fuelRate != "") {
            totFuel = parseInt(fuelRate) + parseInt(fuelDG);
            document.getElementById("totFuel").value = totFuel;
        }
    }

    function setFuels() {
        var fuelRate = document.getElementsByName("fuelVehicles");
        var fuelDG = document.getElementsByName("fuelDGs");
        var totFuel = document.getElementsByName("totalFuels");
        for (var i = 0; i < fuelRate.length; i++) {
            fuelRate[i].value = document.getElementById("fVehicle").value;
            fuelDG[i].value = document.getElementById("fuelDG").value;
            totFuel[i].value = document.getElementById("totFuel").value;
        }
    }
    
    var httpRequest;
   function contractDistanceCheck() {
        var companyId = 0;
//alert("calling");
        var customerId = document.getElementById("customerId").value;
       // alert("calling"+customerId);
        if (customerId != "0") {
            var distnaceVehicleTypeId = document.getElementById("distnaceVehicleTypeId").value;
            var loadTypeId = document.getElementById("loadTypeId").value;
            var containerTypeId = document.getElementById("containerTypeId").value;
            var containerQty = document.getElementById("containerQty").value;
            var toDistance = document.getElementById("toDistance").value;
            var fromDistance = document.getElementById("fromDistance").value;
            var rateWithReeferDistance = document.getElementById("rateWithReeferDistance").value;
            var rateWithoutReeferDistance = document.getElementById("rateWithoutReeferDistance").value;
            if (distnaceVehicleTypeId != "" && distnaceVehicleTypeId != 0
                    && loadTypeId != "" && loadTypeId != 0 && containerTypeId != "" && containerTypeId != 0
                    && containerQty != "" && containerQty != 0
                    && toDistance != "" && toDistance != 0
                    && fromDistance != ""
                    && rateWithReeferDistance != ""
                    && rateWithoutReeferDistance != "") { //alert("inside");

                $.ajax({
                    url: "/throttle/compareDistanceContract.do",
                    dataType: "json",
                    data: {
                        ptpVehicleTypeId: distnaceVehicleTypeId,
                        loadTypeId: loadTypeId,
                        containerTypeId: containerTypeId,
                        containerQty: containerQty,
                        toDistance: toDistance,
                        fromDistance: fromDistance,
                        ptpRateWithReefer: rateWithReeferDistance,
                        ptpRateWithoutReefer: rateWithoutReeferDistance
                    },
                    success: function (data, textStatus, jqXHR) {
                        $.each(data, function (i, data) {
                            if (data.Id == 0) {
                                alert("Contract data for the route is not available at Organizational level. Please reenter");
                                $('#loadTypeId').val('0');
                                $('#distnaceVehicleTypeId').val('0');
                                $('#toDistance').val('');
                                $('#fromDistance').val('');
                                $("#rateWithReeferDistance").val('');
                                $("#rateWithoutReeferDistance").val('');
                                return false;
                            } else {
                                var valus = data.Name;
                                var temp7 = valus.split("~");
                                $("#distanceOrgRateWithReefer").text('');
                                $("#distanceOrgRateWithoutReefer").text('');
                                if (temp7[0] == 'TBA') {
                                    alert("Contract rate doesn't match with Organizational data. Contract needs approval");
                                    $("#distanceOrgValues").text('Needs Approval');
                                    $("#distanceOrgValues").removeClass('noErrorClass');
                                    $("#distanceOrgValues").addClass('errorClass');
                                    $("#distanceOrgRateWithReefer").text(temp7[1]);
                                    $("#distanceOrgRateWithoutReefer").text(temp7[2]);
                                    document.getElementById("distanceApprovalStatus").value = "2";
                                } else {
                                    $("#distanceOrgValues").text('Auto Approval');
                                    $("#distanceOrgValues").removeClass('errorClass');
                                    $("#distanceOrgValues").addClass('noErrorClass');
                                    $("#distanceOrgRateWithReefer").text(temp7[1]);
                                    $("#distanceOrgRateWithoutReefer").text(temp7[2]);
                                    document.getElementById("distanceApprovalStatus").value = "1";
                                }
                            }
                        });
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            }

        }
    }

</script>
                            <br>
                            <div class="modal fade" id="myModal" role="dialog" style="width: auto;height: auto;">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" onclick="resetTheDetails1()">&times;</button>
                                            <h4 class="modal-title">Model Fuel Details</h4>
                                        </div>
                                        <div class="modal-body" id="modelFuelDetailsTab" style="width: auto;height:auto;">
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br>
                        <input type="hidden" id="contractListSize" name="contractListSize" value="<c:out value="${contractListSize}"/>"/>
                        <%--<c:if test="${billingTypeId == 1}">--%>
                            <!--<div  style="overflow: auto">-->
                            <c:if test="${contractListSize > 0}">
                               <table class="table table-info mb30 table-hover" width="98%" >
                                        <thead>
                                            <tr>
                                                <th>Sno</th>
                                                <c:if test="${customerId != '0'}">
                                                <th  ><font color="red"></font>Approval Required ?</th>
                                                </c:if>
                                                <th  ><font color="red"></font>Reference name</th>


                                                <th  ><font color="red"></font>Vehicle Type</th>
                                                <th  ><font color="red"></font>Container Type</th>
                                                <th  ><font color="red"></font>Container Qty</th>
                                                <th  ><font color="red"></font>Load Type</th>
                                                <th ><font color="red"></font>From Distance</th>

                                                <th  ><font color="red"></font>To Distance</th>

                                                <th><font color="red">*</font>Rate With Reefer</th>
                                                <th><font color="red">*</font>Rate Without Reefer</th>
                                                <c:if test="${customerId == '0'}">
                                                        <th>Toll</th>
                                                        <th>Driver Bachat</th>
                                                        <th>Dala</th>
                                                        <th>Misc</th>
                                                        <th>Market Hire Without Reefer</th>
                                                        <th>Market Hire With Reefer</th>
                                                        </c:if>
                                                <th><font color="red">*</font>status</th>
                                                <c:if test="${customerId == '0'}">   <th>Configure Fuel</th> </c:if> 
                                                        
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                   int sno = 0;
                                            %>
                                           <c:forEach items="${contractList}" var="cList">
                                               <%sno++;%>
                                               <tr>
                                                   <td><%=sno%><input type="hidden" id="distanceContractId<%=sno%>" name="distanceContractId" value="<c:out value="${cList.distanceContractId}"/>"/></td>
                                                   <c:if test="${customerId != '0'}">
                                                   <td><span id="distanceOrgValues1" style="font-size: 14px"></span><input type="hidden" name="distanceApprovalStatus" id="distanceApprovalStatus" value="1" /></td>
                                                   </c:if>
                                                   <td><c:out value="${cList.referenceName}"/></td>
                                                   <td><c:out value="${cList.vehicleType}"/></td>
                                                   <td><c:out value="${cList.containerTypeName}"/></td>
                                                   <td><c:out value="${cList.containerQty}"/></td>
                                                   <td><c:out value="${cList.loadType}"/></td>
                                                   <td><c:out value="${cList.fromDistance}"/></td>
                                                   <td><c:out value="${cList.toDistance}"/></td>
                                                 <td>  <input type="text" name="rateWithReefer1" id="rateWithReefer1<%=sno%>" class="form-control"  value="<c:out value="${cList.rateWithReefer}"/>" style="width:120px;"/>
                                                  </td>
                                                 <td>  <input type="text" name="rateWithoutReefer1" id="rateWithoutReefer1<%=sno%>" class="form-control"  value="<c:out value="${cList.rateWithoutReefer}"/>" style="width:120px;"/>
                                                  </td>
                                                   <c:if test="${customerId == '0'}">
                                                  <td>
                                                       <input type="text" name="toll1" id="toll1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${cList.tollAmounts}"/>" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="driverBachat1" id="driverBachat1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${cList.driverBatta}"/>" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="dala1" id="dala1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${cList.dala}"/>" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="misc1" id="misc1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${cList.miscExpense}"/>" style="width:120px;"/>
                                                  </td>
                                                  
                                                  <td>
                                                       <input type="text" name="marketHire1" id="marketHire1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${cList.marketHire}"/>" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="marketHireWithReefer1" id="marketHireWithReefer1<%=sno%>" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${cList.marketHireWithReefer}"/>" style="width:120px;"/>
                                                  </td>
                                                  </c:if>
                                                 
                                                   <td>   <input type="hidden" name="approvalStatus1" id="approvalStatus1<%=sno%>" value="<c:out value="${cList.activeStatus}"/>"/>
                                                       <select name="validStatus" id="validStatus<%=sno%>" class="form-control" style="width:90px;">
                                                        <option value="Y" selected >Active</option>
                                                        <option value="N"  >In Active</option>
                                                    </select></td>
                                                     <c:if test="${customerId == '0'}">
                                                    <td> <a href="#" name="configure" id="configure" data-toggle="modal" data-target="#myModal"  onclick="configModelFuel('<c:out value="${cList.vehicleTypeId}"/>', '<c:out value="${cList.distanceContractId}"/>',1)">CONFIGURE</a> </td>
                                                    </c:if>
                                               </tr>
                                            </c:forEach>
                                        </tbody>
                               </table>
                            </c:if>


                            <div id="ptp" class="tab-pane active" style="overflow: auto">
                                <div class="inpad">
                                   
                                    <table class="table table-info mb30 table-hover sortable" width="98%" id="itemsTable" style="visibility:hidden;">
                                        <thead>
                                            <tr height="40" >
                                                <th>Sno</th>
                                                <c:if test="${customerId != '0'}">
                                                <th  height="80" style="width: 10px;"><font color="red">*</font>Approval Required?</th>
                                                </c:if>
                                                <th  height="80" style="width: 10px;"><font color="red">*</font>Reference name</th>
                                               
                                                                                              
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>Vehicle Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>Container Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>Container Qty</th>
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>Load Type</th>
                                                <th  height="30" style="width: 10px;"><font color="red">*</font>From Distance</th>
                                               
                                                <th  height="30" ><font color="red">*</font>To Distance</th>
                                               
                                                <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate With Reefer</th>
                                                <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate Without Reefer</th>
                                                        <c:if test="${customerId == '0'}">
                                                        <th>Toll</th>
                                                        <th>Driver Bachat</th>
                                                        <th>Dala</th>
                                                        <th>Misc</th>
                                                        <th>Market Hire Without Reefer</th>
                                                        <th>Market Hire With Reefer</th>
                                                </c:if>
                                            </tr>
                                        </thead>

                                        <tbody>
                                            <tr class="item-row">
                                                <td></td>
                                                <c:if test="${customerId != '0'}">
                                                <td> <span id="distanceOrgValues" style="font-size: 14px"></span><input type="hidden" name="distanceApprovalStatus" id="distanceApprovalStatus" value="1" class="form-control"  style="width:150px;height:44px;"/></td>
                                                
                                                    </c:if>                                         
                                                
                                                <td><input type="text" name="referenceName" id="referenceName" value="" class="form-control"  style="width:150px;height:44px;"/></td>
                                               
                                               
                                                <td><c:if test="${vehicleTypeList != null}"><select onchange='setContainerTypeList(this.value)' name="distnaceVehicleTypeId" id="distnaceVehicleTypeId"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>
                                                
                                                <td><select onchange='setContainerQtyList(this.value)' name="containerTypeId" id="containerTypeId"  class="form-control" style="width:150px;height:44px;"></select></td>
                                                <td><select name="containerQty" id="containerQty" onchange="contractRouteCheck('');" class="form-control" style="width:150px;height:44px;"></select></td>
                                                 <td><select name="loadTypeId" id="loadTypeId"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></select></td>
                                                <td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width:150px;height:44px;"  /></td>
                                                
                                                <td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width:150px;height:44px;" /></td>
                                                
                                                <td><input name="rateWithReeferDistance" value="0" class="form-control" id="rateWithReeferDistance"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="contractDistanceCheck();"/></td>
                                                <td><input name="rateWithoutReeferDistance" value="0" class="form-control" id="rateWithoutReeferDistance"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="contractDistanceCheck();" /></td>
                                                <c:if test="${customerId == '0'}">
                                                <td>
                                                       <input type="text" name="toll" id="toll" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="driverBachat" id="toll" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="dala" id="dala" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="misc" id="misc" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="marketHireVehicle" id="marketHireVehicle" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="" style="width:120px;"/>
                                                  </td>
                                                  <td>
                                                       <input type="text" name="marketHireWithReefer" id="marketHireWithReefer" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" value="" style="width:120px;"/>
                                                  </td>
                                                  </c:if>
                                                </tr>
                                                </tbody>
                                            </table>
                                               
                                            <a href="#" id="addRow" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /><b> Add Route Code</b></span></a>
                                            <br>
                                            <br>
                                            <center>
                                            <input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitPage(this.value)"/>
                                        </center>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                   

                            
                                    <!--</div>-->
                        <%--</c:if>--%>
                        <c:if test="${billingTypeId == 2}"></c:if>

                        <c:if test="${billingTypeId == 4}"></c:if>

                    <c:if test="${billingTypeId == 3}"></c:if>

        </div>
        <script>
            $('.btnNext').click(function() {
                $('.nav-tabs > .active').next('li').find('a').trigger('click');
            });
            $('.btnPrevious').click(function() {
                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
            });
        </script>

        <script>

            < script >
                    $(".nexttab").click(function() {
                var chek = validateFixedKmRateDetails();
                //                        alert(chek);
                if (chek == true) {
                    $("#fkmDetails").show();
                    $("#fkm").show();
                    var selected = $("#tabs").tabs("option", "selected");
                    $("#tabs").tabs("option", "selected", selected + 1);
                }
            });

        </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        </body>
    </div>
</div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
