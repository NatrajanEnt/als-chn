/*---------------------------------------------------------------------------
 * ServiceDAO.java
 * Mar 3, 2009
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 -------------------------------------------------------------------------*/
package ets.domain.renderservice.data; 

import com.ibatis.sqlmap.client.SqlMapClient;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.business.OperationTO;
import ets.domain.renderservice.business.JobCardItemTO;
import ets.domain.renderservice.business.ProblemActivityTO;
import ets.domain.renderservice.business.RenderServiceTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
//import com.Service;
//import com.ServiceSoap;
//import com.UpdateOrderStatusResponse.UpdateOrderStatusResult;
//import com.CreateTMSAccountDetailResponse.CreateTMSAccountDetailResult;
//import com.ArrayOfTMSModel;
//import com.TMSModel;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * **************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Mar 3, 2009 vijay	Created
 *
 *****************************************************************************
 */
public class RenderServiceDAO extends SqlMapClientDaoSupport {

    private final static String CLASS = "RenderServiceDAO";

    public ArrayList getWorkTypeList() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList workTypeList = new ArrayList();

        try {
            workTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getWorkTypeList", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getWorkTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getWorkTypeList", sqlException);
        }
        return workTypeList;
    }

    public ArrayList getCloseJobCardViewList(RenderServiceTO clodeJcTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {
            String regNo = clodeJcTO.getRegNo();
            regNo = regNo.replace(" ", "");
            map.put("startIndex", clodeJcTO.getStartIndex());
            map.put("endIndex", clodeJcTO.getEndIndex());
            map.put("regNo", regNo);
            map.put("jcId", clodeJcTO.getJcId());
            map.put("compId", clodeJcTO.getCompId());
            System.out.println("map = " + map);
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getCloseJobCardViewList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCloseJobCardViewList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCloseJobCardViewList", sqlException);
        }
        return jcList;
    }

    public void testAutoCommitFalse() {
        Map map = new HashMap();

        SqlMapClient sqlMap = null;
        try {
            sqlMap = getSqlMapClientTemplate().getSqlMapClient();
            sqlMap.startTransaction();
            // Set commit to false for rollback if any exception
            sqlMap.getCurrentConnection().setAutoCommit(false);
            sqlMap.update("renderservice.insertTest", map);
            sqlMap.getCurrentConnection().commit();

            sqlMap.update("renderservice.insertTest1", map);
            sqlMap.getCurrentConnection().commit();
            sqlMap.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (!(sqlMap.getCurrentConnection() == null)) {
                    sqlMap.getCurrentConnection().rollback();
                    sqlMap.endTransaction();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    public ArrayList getCloseJobCardDetails(int jobCardNo) {
        Map map = new HashMap();

        //////System.out.println("jcno in getCloseJobCardDetails:" + jobCardNo);
        map.put("jobCardId", jobCardNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getCloseJobCardDetails", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCloseJobCardDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCloseJobCardDetails", sqlException);
        }
        return jcList;
    }

    public ArrayList getCloseJobCardPAList(int jobCardNo) {
        Map map = new HashMap();
        //////System.out.println("jcno in getCloseJobCardPAList:" + jobCardNo);
        map.put("jobCardId", jobCardNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList paList = new ArrayList();

        try {
            if (getSqlMapClientTemplate().queryForList("renderservice.getCloseJobCardPAList", map) != null) {
                paList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getCloseJobCardPAList", map);
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCloseJobCardPAList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCloseJobCardPAList", sqlException);
        }
        return paList;
    }

    public ArrayList getJobCardActivityList(int jobCardNo, Float labourPercent) {
        Map map = new HashMap();
        map.put("jobCardId", jobCardNo);
        map.put("labourPercent", labourPercent);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList paList = new ArrayList();

        try {
            paList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getJobCardActivityList", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardActivityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getJobCardActivityList", sqlException);
        }
        return paList;
    }

    public ArrayList getActivityList(int jobCardNo, String custId, String companyStateId) {
        Map map = new HashMap();
        map.put("jobCardId", jobCardNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList paList = new ArrayList();
        ArrayList paListNew = new ArrayList();

        try {
            paList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getActivityList", map);
            Iterator itr = paList.iterator();
            ProblemActivityTO jcto = null;
            String customerState = getCustomerData(custId + "");
            while (itr.hasNext()) {
                jcto = (ProblemActivityTO) itr.next();
                jcto.setSacCode(ThrottleConstants.paplSACCode);
                if (companyStateId.equals(customerState)) {
                    jcto.setCgst(ThrottleConstants.sacCgst);
                    jcto.setSgst(ThrottleConstants.sacSgst);
                    jcto.setIgst("0");
                } else {

                    jcto.setCgst("0");
                    jcto.setSgst("0");
                    jcto.setIgst(ThrottleConstants.sacIgst);
                }
                paListNew.add(jcto);
            }

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getActivityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getActivityList", sqlException);
        }
        return paListNew;
    }

    public ArrayList getJobCardItemList(int jobCardNo, int custId, Float sparePercent, String companyStateId) {
        Map map = new HashMap();
        //////System.out.println("jcno in getJobCardItemList:" + jobCardNo);
        map.put("jobCardId", jobCardNo);
        map.put("custId", custId);
        map.put("sparePercent", sparePercent);
        //////System.out.println("Spare Percent Hari-->" + sparePercent);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList itemList = new ArrayList();
        ArrayList itemListNew = new ArrayList();
        ArrayList gstList = new ArrayList();
        String customerState = "";
        System.out.println("map============++"+map);
        try {
            itemList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getJobCardItemList", map);
            customerState = getCustomerData(custId + "");

            Iterator itr = itemList.iterator();
            JobCardItemTO jcto = null;
            JobCardItemTO gstData = null;
            String tempCategoryId = "";
            String Cgst = null, tempCgst = "0";
            String Sgst = null, tempSgst = "0";
            String Igst = null, tempIgst = "0";
            String Utgst = null, tempUtgst = "0";
            String hsnCode = "0";

            while (itr.hasNext()) {

                System.out.println("tempCgst01:" + Cgst);
                System.out.println("tempSgst01:" + Sgst);
                System.out.println("tempIgst01:" + Igst);
                System.out.println("temputgst01:" + Utgst);

                jcto = (JobCardItemTO) itr.next();
//                if (!tempCategoryId.equals(jcto.getCategoryId())) {
//                gstList = getGSTData(companyStateId, jcto.getCategoryId());
                gstList = getGSTData(companyStateId, jcto.getItemId() + "");
                Iterator itr1 = gstList.iterator();
                for (int j = 0; itr1.hasNext(); j++) {
                    gstData = new JobCardItemTO();
                    gstData = (JobCardItemTO) itr1.next();
                    hsnCode = gstData.getHsnCode();

                    System.out.println("companyStateId:" + companyStateId);
                    System.out.println("customerState01:" + customerState);

                    if ("CGST".equalsIgnoreCase(gstData.getGstName())) {
                        Cgst = gstData.getGstPercentage();
                    }
                    if ("SGST".equalsIgnoreCase(gstData.getGstName())) {
                        Sgst = gstData.getGstPercentage();
                    }
                    if ("IGST".equalsIgnoreCase(gstData.getGstName())) {
                        Igst = gstData.getGstPercentage();
                    }
                    if ("UTGST".equalsIgnoreCase(gstData.getGstName())) {
                        Utgst = gstData.getGstPercentage();
                    }

                    if (companyStateId.equals(customerState)) {
                        System.out.println("states are equal");
                        System.out.println("tempCgst02:" + Cgst);
                        System.out.println("tempSgst02:" + Sgst);
                        tempCgst = Cgst;
                        tempSgst = Sgst;
                        tempIgst = "0";
                        tempUtgst = "0";
                    } else {
                        System.out.println("states are not equal");
                        System.out.println("tempIgst02:" + Igst);
                        tempCgst = "0";
                        tempSgst = "0";
                        tempIgst = Igst;
                        tempUtgst = "0";
                    }

                    System.out.println("tempCgst00:" + tempCgst);
                    System.out.println("tempSgst00:" + tempSgst);
                    System.out.println("tempIgst00:" + tempIgst);

                }
//               }
                jcto.setHsnCode(hsnCode);
                jcto.setCgst(tempCgst);
                jcto.setSgst(tempSgst);
                jcto.setIgst(tempIgst);

                itemListNew.add(jcto);

            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getJobCardItemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getJobCardItemList", sqlException);
        }
        return itemList;
    }
//    public ArrayList getGSTData(String companyStateId, String categoryId) {

    public ArrayList getGSTData(String companyStateId, String itemId) {
        Map map = new HashMap();
        ArrayList gstList = new ArrayList();
        map.put("companyStateId", companyStateId);
//        map.put("categoryId", categoryId);
        map.put("itemId", itemId);
        System.out.println("map value is = " + map);
        try {
            gstList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getGSTData", map);
            System.out.println((new StringBuilder()).append("gstList Detail ").append(gstList.size()).toString());
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getGSTData Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "getGSTData", "getGSTData", sqlException);
        }
        return gstList;
    }

    public String getCustomerData(String custId) {
        Map map = new HashMap();
        String customerState = "";
        map.put("customerId", custId);
        System.out.println("map value is = " + map);
        try {

            customerState = (String) getSqlMapClientTemplate().queryForObject("renderservice.getCustomerData", map);
            System.out.println("customerState:" + customerState);
        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog((new StringBuilder()).append("getCustomerData Error").append(sqlException.toString()).toString());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("sqlException").append(sqlException).toString());
            throw new FPRuntimeException("EM-PR-01", "getCustomerData", "getCustomerData", sqlException);
        }
        return customerState;
    }

    public ArrayList getCloseJobCardPCList(int jobCardNo) {
        Map map = new HashMap();
        map.put("jobCardId", jobCardNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList pcList = new ArrayList();

        try {
            pcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getCloseJobCardPCList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCloseJobCardPCList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCloseJobCardPCList", sqlException);
        }
        return pcList;
    }

    public ArrayList getCloseJobCardServiceList(int jobCardNo) {
        Map map = new HashMap();
        map.put("jobCardId", jobCardNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList pcList = new ArrayList();

        try {
            pcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getCloseJobCardServiceList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCloseJobCardServiceList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCloseJobCardServiceList", sqlException);
        }
        return pcList;
    }

    public void insertJobCardClosure(int userId, int jobCardId, String activityId, String qty, String amt, String approve, String remarks) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobCardId", jobCardId);
        map.put("userId", userId);
        map.put("qty", qty);
        map.put("amt", amt);
        map.put("activityId", activityId);
        map.put("approve", approve);
        map.put("remarks", remarks);
        try {
            //////System.out.println("map = " + map);
            status = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardClosure", map);

            //make account entry for spares consumption and labour charges
            //items cost+
            /*
             System.out.println("itemCost map = "+map);
             String itemCost = (String) getSqlMapClientTemplate().queryForObject("renderservice.getJobCardItemListTotalValue", map);
             System.out.println("itemCost:"+itemCost);
             if(itemCost != null && Float.parseFloat(itemCost)>0){
             int formId = Integer.parseInt(ThrottleConstants.expenseFormId);
             map.put("formId", formId);
             int voucherNo = (Integer) getSqlMapClientTemplate().insert("purchase.getFormVoucherNo", map);
             map.put("voucherNo", voucherNo);
             //map.put("voucherCodeNo", ThrottleConstants.purchaseVoucherCode + voucherNo);

             String voucherCode = "Expense-" + voucherNo;
             map.put("detailCode", "1");
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "PAYMENT");
             map.put("ledgerId", ThrottleConstants.workshopSparesExpenseLedgerID);
             map.put("particularsId", ThrottleConstants.workshopSparesExpenseLedgerCode);
             map.put("amount",  itemCost);
             map.put("accountsType", "DEBIT");
             map.put("narration", "PO");
             map.put("searchCode",jobCardId);
             map.put("reference", "JobCardId");

             System.out.println("map1 jobcard spares issue during closure:=---------------------> " + map);
             status = (Integer) getSqlMapClientTemplate().update("renderservice.insertAccountEntry", map);

             System.out.println("status1 = " + status);
             //--------------------------------- acc 2nd row start --------------------------
             if (status > 0) {
             map.put("detailCode", "2");
             map.put("ledgerId", ThrottleConstants.stockInventoryLedgerID);
             map.put("particularsId", ThrottleConstants.stockInventoryLedgerCode);
             map.put("accountsType", "CREDIT");
             System.out.println("map2 updateClearnceDate=---------------------> " + map);
             status = (Integer) getSqlMapClientTemplate().update("renderservice.insertAccountEntry", map);
             System.out.println("status2 = " + status);
             }
             }
            
             */
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardClosure", sqlException);
        }

    }

    public void insertJobCardClosureAccounts(int userId, int jobCardId, String amt) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobCardId", jobCardId);
        map.put("userId", userId);
        map.put("amt", amt);
        int voucherNo = 0;
        String voucherCode = "";
        try {
            //make account entry for spares consumption and labour charges
            //items cost
            System.out.println("itemCost map = " + map);
            String itemCost = (String) getSqlMapClientTemplate().queryForObject("renderservice.getJobCardItemListTotalValue", map);
            System.out.println("itemCost  = " + itemCost);
            if (itemCost != null && Float.parseFloat(itemCost) > 0) {
                int formId = Integer.parseInt(ThrottleConstants.expenseFormId);
                map.put("formId", formId);

                //start sparec cost entry
                voucherNo = (Integer) getSqlMapClientTemplate().insert("purchase.getFormVoucherNo", map);
                map.put("voucherNo", voucherNo);
                map.put("voucherCodeNo", ThrottleConstants.purchaseVoucherCode + voucherNo);

                voucherCode = "Expense-" + voucherNo;
                map.put("detailCode", "1");
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "EXPENSE");
                map.put("ledgerId", ThrottleConstants.workshopSparesExpenseLedgerID);
                map.put("particularsId", ThrottleConstants.workshopSparesExpenseLedgerCode);
                map.put("amount", itemCost);
                map.put("accountsType", "DEBIT");
                map.put("narration", "R&M Spares Cost");
                map.put("searchCode", jobCardId);
                map.put("reference", "JobCardId");

                System.out.println("map1 jobcard spares issue during closure:=---------------------> " + map);
                status = (Integer) getSqlMapClientTemplate().update("renderservice.insertAccountEntry", map);

                System.out.println("status1 = " + status);
                //--------------------------------- acc 2nd row start --------------------------
                if (status > 0) {
                    map.put("detailCode", "2");
                    map.put("ledgerId", ThrottleConstants.stockInventoryLedgerID);
                    map.put("particularsId", ThrottleConstants.stockInventoryLedgerCode);
                    map.put("accountsType", "CREDIT");
                    System.out.println("map2 updateClearnceDate=---------------------> " + map);
                    status = (Integer) getSqlMapClientTemplate().update("renderservice.insertAccountEntry", map);
                    System.out.println("status2 = " + status);
                }
            }
            //end sparec cost entry
            //start labor cost entry
            System.out.println("amt value is=" + amt);
            if (Float.parseFloat(amt) > 0) {
                int formId = Integer.parseInt(ThrottleConstants.expenseFormId);
                map.put("formId", formId);
                voucherNo = (Integer) getSqlMapClientTemplate().insert("purchase.getFormVoucherNo", map);
                map.put("voucherNo", voucherNo);
                map.put("voucherCodeNo", ThrottleConstants.purchaseVoucherCode + voucherNo);

                voucherCode = "Expense-" + voucherNo;

                voucherNo = (Integer) getSqlMapClientTemplate().insert("purchase.getFormVoucherNo", map);
                map.put("voucherNo", voucherNo);
                map.put("voucherCodeNo", ThrottleConstants.purchaseVoucherCode + voucherNo);

                voucherCode = "Expense-" + voucherNo;
                map.put("detailCode", "1");
                map.put("voucherCode", voucherCode);
                map.put("mainEntryType", "VOUCHER");
                map.put("entryType", "EXPENSE");
                map.put("ledgerId", ThrottleConstants.workshopLaborExpenseLedgerID);
                map.put("particularsId", ThrottleConstants.workshopLaborExpenseLedgerCode);
                map.put("amount", amt);
                map.put("accountsType", "DEBIT");
                map.put("narration", "R&M Labor Cost");
                map.put("searchCode", jobCardId);
                map.put("reference", "JobCardId");

                System.out.println("map1 jobcard spares issue during closure:=---------------------> " + map);
                status = (Integer) getSqlMapClientTemplate().update("renderservice.insertAccountEntry", map);

                System.out.println("status1 = " + status);
                //--------------------------------- acc 2nd row start --------------------------
                if (status > 0) {
                    map.put("detailCode", "2");
                    map.put("ledgerId", ThrottleConstants.technicianCostLedgerID);
                    map.put("particularsId", ThrottleConstants.technicianCostLedgerCode);
                    map.put("accountsType", "CREDIT");
                    System.out.println("map2 updateClearnceDate=---------------------> " + map);
                    status = (Integer) getSqlMapClientTemplate().update("renderservice.insertAccountEntry", map);
                    System.out.println("status2 = " + status);
                }

                //end labor cost entry
            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardClosure Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardClosure", sqlException);
        }

    }

    public void insertJobCardProblemCause(int jobCardId, String problemId, String causeId) {
        Map map = new HashMap();
        int status = 0;
        map.put("jobCardId", jobCardId);
        map.put("problemId", problemId);
        map.put("causeId", causeId);

        try {
            status = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardProblemCause", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardProblemCause Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardProblemCause", sqlException);
        }
    }

    public int saveExtJobCardBill(int userId, String jobCardId, String invoiceNo, String invoiceRemarks, String invoiceDate,
            String sparesAmt, String sparesRemarks, String consumableAmt, String consumableRemarks,
            String laborAmt, String laborRemarks, String othersAmt, String othersRemarks, String totalAmt, String vatPercent,
            String vatAmt, String serviceTaxPercent, String serviceTaxAmt) {
        Map map = new HashMap();
        int billNo = 0;
        map.put("userId", userId);
        map.put("jobCardId", jobCardId);
        map.put("invoiceNo", invoiceNo);
        map.put("invoiceRemarks", invoiceRemarks);
        String[] temp = null;
        temp = invoiceDate.split("-");
        map.put("invoiceDate", temp[2] + "-" + temp[1] + "-" + temp[0]);

        map.put("sparesAmt", sparesAmt);
        map.put("sparesRemarks", sparesRemarks);
        map.put("consumableAmt", consumableAmt);
        map.put("consumableRemarks", consumableRemarks);
        map.put("laborAmt", laborAmt);
        map.put("laborRemarks", laborRemarks);
        map.put("othersAmt", othersAmt);
        map.put("othersRemarks", othersRemarks);
        map.put("totalAmt", totalAmt);
        map.put("vatPercent", vatPercent);
        map.put("vatAmt", vatAmt);
        map.put("serviceTaxPercent", serviceTaxPercent);
        map.put("serviceTaxAmt", serviceTaxAmt);

        try {
            billNo = (Integer) getSqlMapClientTemplate().insert("renderservice.saveExtJobCardBill", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardProblemCause Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardProblemCause", sqlException);
        }
        return billNo;
    }

    public int saveExtJobCardBillNew(int userId, String jobCardId, String invoiceNo, String invoiceRemarks, String invoiceDate) {
        Map map = new HashMap();
        int billNo = 0;
        map.put("userId", userId);
        map.put("jobCardId", jobCardId);
        map.put("invoiceNo", invoiceNo);
        map.put("invoiceRemarks", invoiceRemarks);
        String[] temp = null;
        temp = invoiceDate.split("-");
        map.put("invoiceDate", temp[2] + "-" + temp[1] + "-" + temp[0]);
        try {
            billNo = (Integer) getSqlMapClientTemplate().insert("renderservice.saveExtJobCardBillNew", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveExtJobCardBillNew Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveExtJobCardBillNew", sqlException);
        }
        return billNo;
    }

    public String insertJobCardBillMaster(int userId, int jobCardId, String spares, String labour, String total,
            String discount, String nett, String sparesPercent, String labourPercent, String bodyRepairTotal,
            String invoiceType, String labourHike, String date, String consumablesTotal,
            float itemsGstValue, float itemsNettValue,
            float laborsGstValue, float laborsNettValue,
            String sparesDiscount, String labourDiscount,
            float itemsValue, float itemsDiscountValue, float laborsValue, float laborsDiscountValue, float cgstValue,
            float sgstValue, float igstValue, float lcgstValue, float lsgstValue, float ligstValue) {
        Map map = new HashMap();
        int partBillNo = 0;
        int serviceBillNo = 0;
        //////System.out.println("dao vals:" + spares + ":" + labour + ":" + discount + ":" + bodyRepairTotal + ":" + date);
        map.put("itemsValue", itemsValue);
        map.put("itemsDiscountValue", itemsDiscountValue);
        map.put("laborsValue", laborsValue);
        map.put("laborsDiscountValue", laborsDiscountValue);
        map.put("itemsGstValue", itemsGstValue);
        map.put("cgstValue", cgstValue);
        map.put("sgstValue", sgstValue);
        map.put("igstValue", igstValue);
        map.put("lcgstValue", lcgstValue);
        map.put("lsgstValue", lsgstValue);
        map.put("ligstValue", ligstValue);

        map.put("itemsNettValue", itemsNettValue);
        map.put("laborsGstValue", laborsGstValue);
        map.put("laborsNettValue", laborsNettValue);

        map.put("sparesDiscount", sparesDiscount);
        map.put("labourDiscount", labourDiscount);
        map.put("consumablesTotal", consumablesTotal);
        map.put("userId", userId);
        map.put("jobCardId", jobCardId);
        map.put("spares", spares);
        map.put("labour", labour);
        map.put("total", total);
        map.put("discount", discount);
        map.put("nett", nett);
        map.put("sparesPercent", sparesPercent);
        map.put("labourPercent", labourPercent);
        map.put("bodyRepairTotal", bodyRepairTotal);
        map.put("invoiceType", invoiceType);
        map.put("labourHike", labourHike);

        //////System.out.println("date" + date);
        //////System.out.println("labourHike%" + labourHike);
        String[] temp = null;
        temp = date.split("-");

        map.put("date", temp[2] + "-" + temp[1] + "-" + temp[0]);

        //////System.out.println("invoiceType in Dao =" + invoiceType);
        String invoiceNo = "";
        String pcd = "";
        try {
            //get invoiceNo
            pcd = (String) getSqlMapClientTemplate().queryForObject("renderservice.getPCD", map);
            String invoiceCode1 = "INV/";
            String invoiceCode2 = "/17-18";
            int csLen = 0;
            float nettAmount = 0.00F;
            float roundOffAmount = 0.00F;
            String nettAmountStr = "";
            String paise = "";
            
            int partInvoiceCodeSequenceI = (Integer) getSqlMapClientTemplate().insert("renderservice.getPartInvoiceCodeSequence", map);
                String partInvoiceCodeSequence = partInvoiceCodeSequenceI + "";
                csLen = partInvoiceCodeSequence.length();

            if (itemsValue > 0) {
                //spares bill master entry start
                
                for (int j = csLen; j < 4; j++) {
                    partInvoiceCodeSequence = "0" + partInvoiceCodeSequence;
                }
                System.out.println("partInvoiceCodeSequence:" + partInvoiceCodeSequence);

                map.put("invoiceNo", invoiceCode1 + "P" + partInvoiceCodeSequence + invoiceCode2);
                nettAmountStr = itemsNettValue + "";
                nettAmount = itemsNettValue;
                roundOffAmount = 0.00F;
                paise = nettAmountStr.substring(nettAmountStr.indexOf("."), nettAmountStr.length());
                System.out.println(paise);
                if (Float.parseFloat(paise) > 0) {
                    nettAmount = nettAmount - Float.parseFloat(paise) + 1;
                    roundOffAmount = 1 - Float.parseFloat(paise);
                }
                System.out.println("roundOffAmount:" + nettAmount);
                System.out.println("roundOffAmount:" + roundOffAmount);

                map.put("roundOffAmount", roundOffAmount);
                map.put("finalAmount", nettAmount);
                System.out.println("map1:" + map);
                partBillNo = (Integer) getSqlMapClientTemplate().insert("renderservice.insertJobCardPartBillMasters", map);
                //spares bill master entry end
            }
            if (laborsValue > 0) {
                //service bill master entry start
//                int serviceInvoiceCodeSequenceI = (Integer) getSqlMapClientTemplate().insert("renderservice.getServiceInvoiceCodeSequence", map);
//                String serviceInvoiceCodeSequence = serviceInvoiceCodeSequenceI + "";
//
//                csLen = serviceInvoiceCodeSequence.length();
                for (int j = csLen; j < 4; j++) {
                    partInvoiceCodeSequence = "0" + partInvoiceCodeSequence;
                }
                System.out.println("serviceInvoiceCodeSequence:" + partInvoiceCodeSequence);
                map.put("invoiceNo", invoiceCode1 + "L" + partInvoiceCodeSequence + invoiceCode2);
                nettAmountStr = laborsNettValue + "";
                nettAmount = laborsNettValue;
                roundOffAmount = 0.00F;
                paise = nettAmountStr.substring(nettAmountStr.indexOf("."), nettAmountStr.length());
                System.out.println(paise);
                if (Float.parseFloat(paise) > 0) {
                    nettAmount = nettAmount - Float.parseFloat(paise) + 1;
                    roundOffAmount = 1 - Float.parseFloat(paise);
                }
                System.out.println("roundOffAmount:" + nettAmount);
                System.out.println("roundOffAmount:" + roundOffAmount);

                map.put("roundOffAmount", roundOffAmount);
                map.put("finalAmount", nettAmount);

                System.out.println("map1:" + map);
                serviceBillNo = (Integer) getSqlMapClientTemplate().insert("renderservice.insertJobCardServiceBillMasters", map);
                //service bill master entry end
            }

            /*} else {
             //////System.out.println("In Admin Login");
             invoiceNo = (String) getSqlMapClientTemplate().queryForObject("renderservice.getFebInvoiceNo", map);
             //////System.out.println("invoiceNo" + invoiceNo);

             map.put("invoiceNo", invoiceNo);
             //////System.out.println("User Id IN DAO2-->" + userId);
             //////System.out.println("map2:"+map);
             billNo = (Integer) getSqlMapClientTemplate().insert("renderservice.insertJobCardBillMaster", map);
             }
             */
            //account entry details start

            /*
             String code2 = "";
             String[] temp1 = null;
             int insertStatus = 0;
             map.put("userId", userId);
             map.put("DetailCode", "1");
             map.put("voucherType", "%PAYMENT%");
             code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
             temp1 = code2.split("-");
             int codeval2 = Integer.parseInt(temp1[1]);
             int codev2 = codeval2 + 1;
             String voucherCode = "PAYMENT-" + codev2;
             //////System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "PAYMENT");
             map.put("ledgerId", 1576);
             map.put("particularsId", "LEDGER-1556");

             map.put("amount", labour);
             map.put("Accounts_Type", "DEBIT");
             map.put("Remark", "Labour Charges");
             map.put("Reference", "Job");

             //////System.out.println("jobCardId = " + jobCardId);
             map.put("SearchCode", jobCardId);
             //////System.out.println("map1 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             //////System.out.println("status1 = " + insertStatus);
             //2nd Row entry
             if (insertStatus > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", "52");
             map.put("particularsId", "LEDGER-40");
             map.put("Accounts_Type", "CREDIT");
             //////System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             //////System.out.println("status2 = " + insertStatus);
             }
             //Spare Amount
             codev2++;
             voucherCode = "PAYMENT-" + codev2;
             //////System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "PAYMENT");
             map.put("ledgerId", 1577);
             map.put("particularsId", "LEDGER-1557");

             map.put("amount", spares);
             map.put("Accounts_Type", "DEBIT");
             map.put("Remark", "Spare Amount");
             map.put("Reference", "Job");

             //////System.out.println("jobCardId = " + jobCardId);
             map.put("SearchCode", jobCardId);
             //////System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             //////System.out.println("status1 = " + insertStatus);
             //--------------------------------- acc 2nd row start --------------------------
             if (insertStatus > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", "52");
             map.put("particularsId", "LEDGER-40");
             map.put("Accounts_Type", "CREDIT");
             //////System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             //////System.out.println("status2 = " + insertStatus);
             }
             //Vat Amount
             codev2++;
             voucherCode = "PAYMENT-" + codev2;
             //////System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "PAYMENT");
             map.put("ledgerId", 1578);
             map.put("particularsId", "LEDGER-1558");

             map.put("amount", 0);
             map.put("Accounts_Type", "DEBIT");
             map.put("Remark", "Vat Amount");
             map.put("Reference", "Job");

             //////System.out.println("jobCardId = " + jobCardId);
             map.put("SearchCode", jobCardId);
             //////System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             //////System.out.println("status1 = " + insertStatus);
             //--------------------------------- acc 2nd row start --------------------------
             if (insertStatus > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", "52");
             map.put("particularsId", "LEDGER-40");
             map.put("Accounts_Type", "CREDIT");
             //////System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             //////System.out.println("status2 = " + insertStatus);
             }
             //serveice Charge
             codev2++;
             voucherCode = "PAYMENT-" + codev2;
             //////System.out.println("voucherCode = " + voucherCode);
             map.put("voucherCode", voucherCode);
             map.put("mainEntryType", "VOUCHER");
             map.put("entryType", "PAYMENT");
             map.put("ledgerId", 1579);
             map.put("particularsId", "LEDGER-1559");

             map.put("amount", 0);
             map.put("Accounts_Type", "DEBIT");
             map.put("Remark", "Service Charge");
             map.put("Reference", "Job");

             //////System.out.println("jobCardId = " + jobCardId);
             map.put("SearchCode", jobCardId);
             //////System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             //////System.out.println("status1 = " + insertStatus);
             //--------------------------------- acc 2nd row start --------------------------
             if (insertStatus > 0) {
             map.put("DetailCode", "2");
             map.put("ledgerId", "52");
             map.put("particularsId", "LEDGER-40");
             map.put("Accounts_Type", "CREDIT");
             //////System.out.println("map2 =---------------------> " + map);
             insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
             //////System.out.println("status2 = " + insertStatus);
             }
             //push Data to EFS
             //                  updateJobCardBillToEFS(String.valueOf(jobCardId));
             */
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertJobCardBillMaster Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardBillMaster", sqlException);
        }
        return partBillNo + "~" + serviceBillNo;
    }

    public void insertJobCardBillItems(int billNo, String itemId, String quantity, String tax,
            String price, String lineItemAmount, String hsnCode, String cgst, String sgst, String igst,
            String sparesDiscount) {
        Map map = new HashMap();
        int status = 0;
        map.put("billNo", billNo);
        map.put("itemId", itemId);
        map.put("quantity", quantity);
        map.put("tax", tax);
        map.put("hsnCode", hsnCode);
        map.put("cgst", cgst);
        map.put("sgst", sgst);
        map.put("igst", igst);
        map.put("price", price);
        map.put("sparesDiscount", sparesDiscount);
        map.put("lineItemAmount", lineItemAmount);
        float taxAmount = 0;
        float taxableAmount = 0;
        float nettAmount = 0;
        taxableAmount = Float.parseFloat(lineItemAmount) * (1 - Float.parseFloat(sparesDiscount) / 100);
        taxAmount = taxableAmount * (Float.parseFloat(cgst) + Float.parseFloat(sgst) + Float.parseFloat(igst)) / 100;
        nettAmount = taxAmount + taxableAmount;
        map.put("taxableAmount", taxableAmount);
        map.put("taxAmount", taxAmount);
        map.put("nettAmount", nettAmount);
        String nettAmountStr = nettAmount + "";

        float roundOffAmount = 0.00F;
        String paise = nettAmountStr.substring(nettAmountStr.indexOf("."), nettAmountStr.length());
        System.out.println(paise);
        if (Float.parseFloat(paise) > 0) {
            nettAmount = nettAmount - Float.parseFloat(paise) + 1;
            roundOffAmount = Float.parseFloat(paise);
        }
        System.out.println("roundOffAmount:" + nettAmount);
        System.out.println("roundOffAmount:" + roundOffAmount);

        map.put("roundOffAmount", roundOffAmount);
        map.put("finalAmount", nettAmount);
        try {
            status = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardBillItems", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */

            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertJobCardBillItems Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardBillItems", sqlException);
        }
    }

    public void insertJobCardBillBodyWorks(int billNo, String amount, String woBillNo) {
        Map map = new HashMap();
        int status = 0;
        map.put("billNo", billNo);
        map.put("amount", amount);
        map.put("woBillNo", woBillNo);

        try {
            status = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardBillBodyWorks", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardBillBodyWorks Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardBillBodyWorks", sqlException);
        }
    }

    public void insertJobCardBillLabour(int billNo, String activityId, String qty, String amount, String sacCode,
            String cgst, String sgst, String igst, String labourDiscount) {
        Map map = new HashMap();
        int status = 0;
        map.put("billNo", billNo);
        map.put("qty", qty);
        map.put("amount", amount);
        map.put("activityId", activityId);
        map.put("sacCode", sacCode);
        map.put("cgst", cgst);
        map.put("sgst", sgst);
        map.put("igst", igst);
        map.put("labourDiscount", labourDiscount);

        float taxAmount = 0;
        float nettAmount = 0;
        float taxableAmount = 0;

        taxableAmount = Float.parseFloat(amount) * (1 - Float.parseFloat(labourDiscount) / 100);
        taxAmount = taxableAmount * (Float.parseFloat(cgst) + Float.parseFloat(sgst) + Float.parseFloat(igst)) / 100;
        nettAmount = taxAmount + taxableAmount;
        map.put("taxAmount", taxAmount);
        map.put("taxableAmount", taxableAmount);
        map.put("nettAmount", nettAmount);
        String nettAmountStr = nettAmount + "";

        float roundOffAmount = 0.00F;
        String paise = nettAmountStr.substring(nettAmountStr.indexOf("."), nettAmountStr.length());
        System.out.println(paise);
        if (Float.parseFloat(paise) > 0) {
            nettAmount = nettAmount - Float.parseFloat(paise) + 1;
            roundOffAmount = Float.parseFloat(paise);
        }
        System.out.println("roundOffAmount:" + nettAmount);
        System.out.println("roundOffAmount:" + roundOffAmount);

        map.put("roundOffAmount", roundOffAmount);
        map.put("finalAmount", nettAmount);

        try {
            status = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardBillLabour", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardBillLabour Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardBillLabour", sqlException);
        }
    }

    public void insertJobCardBillServiceTax(int billNo, String labourCharge) {
        Map map = new HashMap();
        int status = 0;
        float taxPercent = 0.00F;
        float taxValue = taxPercent * Float.parseFloat(labourCharge) / 100;
        map.put("labourCharge", labourCharge);
        map.put("taxValue", labourCharge);
        map.put("taxPercent", taxPercent);
        map.put("billNo", billNo);
        System.out.println("map"+map);

        try {
            status = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardBillServiceTax", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardBillLabour Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardBillLabour", sqlException);
        }
    }

    public void insertJobCardBillMargin(int billNo, String labour, String spares, String nett,
            String matlMargin, String matlMarginPercent, String laborMargin, String laborMarginPercent,
            String nettMargin, String nettMarginPercent,
            String purchaseSpares, String labourExpense, String jobCost) {
        Map map = new HashMap();
        int status = 0;
        map.put("billNo", billNo);
        map.put("labour", labour);
        map.put("spares", spares);
        map.put("nett", nett);
        map.put("matlMargin", matlMargin);
        map.put("matlMarginPercent", matlMarginPercent);
        map.put("laborMargin", laborMargin);
        map.put("laborMarginPercent", laborMarginPercent);
        map.put("nettMargin", nettMargin);
        map.put("nettMarginPercent", nettMarginPercent);
        map.put("purchaseSpares", purchaseSpares);
        map.put("labourExpense", labourExpense);
        map.put("jobCost", jobCost);

        //////System.out.println("map values:"+map);
        try {
            status = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardBillMargin", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("insertJobCardBillLabour Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardBillLabour", sqlException);
        }
    }

    public void insertJobCardBillService(int billNo, String amount, String serviceId) {
        Map map = new HashMap();
        int status = 0;
        map.put("billNo", billNo);
        map.put("amount", amount);
        map.put("serviceId", serviceId);

        try {
            status = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardBillService", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardBillService Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardBillService", sqlException);
        }
    }

    public int getLastInsertId() {
        Map map = new HashMap();
        int lastInsertId = 0;

        try {
            lastInsertId = (Integer) getSqlMapClientTemplate().queryForObject("renderservice.lastInsertId", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getLastInsertId Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getLastInsertId", sqlException);
        }
        return lastInsertId;
    }

    public void updateJobCardStatus(int jobCardId, String status, int userId, String remarks,
            String labourCount, String labourHours, String labourExpenseAmount, String consumbalesAmount) {
        Map map = new HashMap();
        int retStatus = 0;
        map.put("jobCardId", jobCardId);
        map.put("status", status);
        map.put("userId", userId);
        map.put("remarks", remarks);
        map.put("labourCount", labourCount);
        map.put("labourHours", labourHours);
        map.put("labourExpenseAmount", labourExpenseAmount);
        map.put("consumbalesAmount", consumbalesAmount);
        System.out.println("update ...map = " + map);
        try {
            retStatus = (Integer) getSqlMapClientTemplate().update("renderservice.updateJobCardStatus", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateJobCardStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateJobCardStatus", sqlException);
        }
    }

    public ArrayList getClosedJobCardForBill(RenderServiceTO clodeJcTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {

            String regNo = clodeJcTO.getRegNo();
            regNo = regNo.replace(" ", "");
            map.put("startIndex", clodeJcTO.getStartIndex());
            map.put("endIndex", clodeJcTO.getEndIndex());
            map.put("regNo", regNo);
            map.put("jcId", clodeJcTO.getJcId());
            map.put("compId", clodeJcTO.getCompId());
            System.out.println("map values="+map);
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getClosedJobCardForBill", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedJobCardForBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getClosedJobCardForBill", sqlException);
        }
        return jcList;
    }

    public ArrayList getBillHeaderInfo(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);

        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getBillHeaderInfo", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillHeaderInfo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBillHeaderInfo", sqlException);
        }
        return jcList;
    }

    public ArrayList getPartBillHeaderInfo(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);

        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getPartBillHeaderInfo", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillHeaderInfo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBillHeaderInfo", sqlException);
        }
        return jcList;
    }

    public ArrayList getServiceBillHeaderInfo(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);

        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getServiceBillHeaderInfo", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillHeaderInfo Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBillHeaderInfo", sqlException);
        }
        return jcList;
    }

    public ArrayList getBillItemList(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getBillItemList", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillItemList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBillItemList", sqlException);
        }
        return jcList;
    }

    public ArrayList getExtJobCardBillDetails(String billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);
        ArrayList extJobCardBillDetails = new ArrayList();

        try {
            extJobCardBillDetails = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getExtJobCardBillDetails", map);

        } catch (Exception sqlException) {
            FPLogUtils.fpDebugLog("getJobCardDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getJobCardDetails", sqlException);
        }
        return extJobCardBillDetails;
    }

    public ArrayList getBillLaborCharge(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {
//            if (billNo < 6816) { //production
            //if(billNo < 4900){
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.billLaborCharge", map);
//            } else {
//                jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.billLaborChargeNew", map);
//            }

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillLaborCharge Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBillLaborCharge", sqlException);
        }
        return jcList;
    }

    public ArrayList getBillTax(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();
        try {
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.billTax", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillTax Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getBillTax", sqlException);
        }
        return jcList;
    }

    public String getBillServiceTax(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        String serviceTax = "";
        try {
            serviceTax = (String) getSqlMapClientTemplate().queryForObject("renderservice.billServiceTax", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serviceTax Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "serviceTax", sqlException);
        }
        return serviceTax;
    }

    public String getBillServiceTaxPercent(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        String serviceTax = "";
        try {
            serviceTax = (String) getSqlMapClientTemplate().queryForObject("renderservice.billServiceTaxPercent", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("serviceTax Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "serviceTax", sqlException);
        }
        return serviceTax;
    }

    public String getClosedDetails(String jobCardId) {
        Map map = new HashMap();
        map.put("jobCardId", jobCardId);
        /*
         * set the parameters in the map for sending to ORM
         */
        String closedDetails = "";
        try {
            closedDetails = (String) getSqlMapClientTemplate().queryForObject("renderservice.getClosedDetails", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getClosedDetails", sqlException);
        }
        return closedDetails;
    }

    public String getActualLaborExpense(String jcNo) {
        Map map = new HashMap();
        map.put("jcNo", jcNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        String laborExpense = "";
        try {
            laborExpense = (String) getSqlMapClientTemplate().queryForObject("renderservice.getActualLaborExpense", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("laborExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "laborExpense", sqlException);
        }
        return laborExpense;
    }

    public String getActualLaborExpenseForClosing(String jcNo) {
        Map map = new HashMap();
        map.put("jcNo", jcNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        //////System.out.println("Hi... DAO: " + jcNo);
        String laborExpense = "";
        try {
            laborExpense = (String) getSqlMapClientTemplate().queryForObject("renderservice.getActualLaborExpenseForClosing", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("laborExpense Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "laborExpense", sqlException);
        }
        return laborExpense;
    }

    public String getContractVendors(int billNo) {
        Map map = new HashMap();
        map.put("billNo", billNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        String vendorNames = "";
        try {
            vendorNames = (String) getSqlMapClientTemplate().queryForObject("renderservice.getContractVendors", map);
            if (vendorNames == null) {
                vendorNames = "";
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getContractVendors Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getContractVendors", sqlException);
        }
        return vendorNames;
    }

    public String getActivityDetails(String acode, String modelId, String mfrId) {
        Map map = new HashMap();
        map.put("acode", acode);
        map.put("modelId", modelId);
        map.put("mfrId", mfrId);
        /*
         * set the parameters in the map for sending to ORM
         */
        String activityDetails = "";
        try {
            System.out.println("map value is=" + map);
            activityDetails = (String) getSqlMapClientTemplate().queryForObject("renderservice.getActivityDetails", map);
            /*
             if(activityDetails == null || "".equals(activityDetails)){
             activityDetails = (String) getSqlMapClientTemplate().queryForObject("renderservice.getActivityDetailsAllModel", map);
             if(activityDetails == null || "".equals(activityDetails)){
             activityDetails = (String) getSqlMapClientTemplate().queryForObject("renderservice.getActivityDetailsAllVehAllModel", map);
             }
             }
             */
            if (activityDetails == null || "".equals(activityDetails)) {
                activityDetails = "";
            }

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getActivityDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getActivityDetails", sqlException);
        }
        return activityDetails;
    }

    public ArrayList getSections() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList sections = new ArrayList();

        try {
            sections = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getSections", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSections Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSections", sqlException);
        }
        return sections;
    }

    public ArrayList getGroupList() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList groupList = new ArrayList();
        try {
            groupList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getGroupList", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getGroupList", sqlException);
        }
        return groupList;
    }

    public ArrayList getSubGroupList(int billGroupId) {
        Map map = new HashMap();
        map.put("billGroupId", billGroupId);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList subGroupList = new ArrayList();
        try {
            subGroupList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getSubGroupList", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getSubGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getSubGroupList", sqlException);
        }
        return subGroupList;
    }

    public ArrayList getVehicleList(int vehicleTypeId) {
        Map map = new HashMap();
        map.put("vehicleTypeId", vehicleTypeId);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList vehicleTypeList = new ArrayList();
        try {
            vehicleTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getVehicleList", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getVehicleList", sqlException);
        }
        return vehicleTypeList;
    }

    public int updateSubGroup(RenderServiceTO renderServiceTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int user = userId;
        map.put("userId", user);
        map.put("billSubGroupName", renderServiceTO.getBillSubGroupName());
        map.put("billSubGroupDesc", renderServiceTO.getBillSubGroupDesc());
        map.put("billGroupId", renderServiceTO.getBillGroupId());
        map.put("billSubGroupId", renderServiceTO.getBillSubGroupId());
        map.put("activeInd", renderServiceTO.getActiveInd());

        try {

            status = (Integer) getSqlMapClientTemplate().update("renderservice.updateSubGroup", map);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("updateJobCardScheduleDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "updateStandardCharge", sqlException);
        }

        return status;
    }

    public int saveSubGroupName(RenderServiceTO renderServiceTO, int userId) {
        Map map = new HashMap();
        int saveSubGroupName = 0;
        int user = userId;
        try {
            map.put("userId", user);
            map.put("billSubGroupName", renderServiceTO.getBillSubGroupName());
            map.put("billSubGroupDesc", renderServiceTO.getBillSubGroupDesc());
            map.put("billGroupId", renderServiceTO.getBillGroupId());
            map.put("status", renderServiceTO.getActiveInd());
            saveSubGroupName = (Integer) getSqlMapClientTemplate().update("renderservice.saveSubGroupName", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("saveSubGroupName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "saveSubGroupName ", sqlException);
        }
        return saveSubGroupName;
    }

    public String checkSubGroupName(RenderServiceTO renderServiceTO) {
        Map map = new HashMap();
        String checkSubGroupName = "";
        try {
            //////System.out.println("map = " + map);
            map.put("billSubGroupName", renderServiceTO.getBillSubGroupName());
            checkSubGroupName = (String) getSqlMapClientTemplate().queryForObject("renderservice.checkSubGroupName", map);
            //////System.out.println("checkStandardChargeName " + checkSubGroupName);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkSubGroupName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkSubGroupName", sqlException);
        }

        return checkSubGroupName;
    }

    public int insertJobCardBillDetails(RenderServiceTO renderServiceTO, int userId) {
        Map map = new HashMap();
        int insertBill = 0;
        int insertBillDetails = 0;
        int user = userId;
        try {
            map.put("userId", user);
            map.put("jobCardId", renderServiceTO.getJobCardId());
            map.put("invoiceNo", renderServiceTO.getInvoiceNo());
            map.put("invoiceRemarks", renderServiceTO.getInvoiceRemarks());
            map.put("invoiceAmount", renderServiceTO.getInvoiceAmount());
            map.put("invoiceDate", renderServiceTO.getInvoiceDate());
            map.put("totalCost", renderServiceTO.getTotalCost());
            map.put("totalLabour", renderServiceTO.getTotalLabour());
            map.put("totalAmount", renderServiceTO.getTotalAmount());
            map.put("vatAmount", renderServiceTO.getVatAmount());
            map.put("vatRemarks", renderServiceTO.getVatRemarks());
            map.put("serviceTaxAmount", renderServiceTO.getServiceTaxAmount());
            map.put("serviceTaxRemarks", renderServiceTO.getServiceTaxRemarks());
            map.put("labourRemarks", renderServiceTO.getLabourRemarks());
            insertBill = (Integer) getSqlMapClientTemplate().insert("renderservice.insertJobCardBill", map);
            if (insertBill > 0) {
                for (int i = 0; i < renderServiceTO.getGroupList().length; i++) {
                    map.put("billId", insertBill);
                    map.put("groupId", renderServiceTO.getGroupList()[i]);
                    map.put("subGroupId", renderServiceTO.getSubGroupList()[i]);
                    map.put("expenseRemarks", renderServiceTO.getExpenseRemarks()[i]);
                    map.put("cost", renderServiceTO.getCost()[i]);
                    map.put("labour", renderServiceTO.getLabour()[i]);
                    map.put("total", renderServiceTO.getTotal()[i]);
                    map.put("serviceTypeId", renderServiceTO.getServiceList1()[i]);
                    map.put("quantity", renderServiceTO.getQuantity()[i]);

                    map.put("partSGST", renderServiceTO.getPartSGST()[i]);
                    map.put("partCGST", renderServiceTO.getPartCGST()[i]);
                    map.put("partIGST", renderServiceTO.getPartIGST()[i]);
                    map.put("labourSGST", renderServiceTO.getLabourSGST()[i]);
                    map.put("labourCGST", renderServiceTO.getLabourCGST()[i]);
                    map.put("labourIGST", renderServiceTO.getLabourIGST()[i]);

                    insertBillDetails = (Integer) getSqlMapClientTemplate().update("renderservice.insertJobCardBillDetails", map);
                }
            }
            //account entry details start
            String code2 = "";
            String[] temp1 = null;
            int insertStatus = 0;
            map.put("userId", userId);
            map.put("DetailCode", "1");
            map.put("voucherType", "%PAYMENT%");
            code2 = (String) getSqlMapClientTemplate().queryForObject("trip.getTripVoucherCode", map);
            int codev2 = 0;
            if (code2 == null) {
                codev2 = 0;
            } else {
                temp1 = code2.split("-");
                int codeval2 = Integer.parseInt(temp1[1]);
                codev2 = codeval2 + 1;
            }
            String voucherCode = "PAYMENT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", 1576);
            map.put("particularsId", "LEDGER-1556");

            map.put("amount", renderServiceTO.getTotalLabour());
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", "Labour Charges");
            map.put("Reference", "Job");

            //////System.out.println("jobCardId = " + renderServiceTO.getJobCardId());
            map.put("SearchCode", renderServiceTO.getJobCardId());
            //////System.out.println("map1 =---------------------> " + map);
            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
            //////System.out.println("status1 = " + insertStatus);
            //2nd Row entry
            if (insertStatus > 0) {
                map.put("DetailCode", "2");
                map.put("ledgerId", "52");
                map.put("particularsId", "LEDGER-40");
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map2 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + insertStatus);
            }
            //Spare Amount
            codev2++;
            voucherCode = "PAYMENT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", 1577);
            map.put("particularsId", "LEDGER-1557");

            map.put("amount", renderServiceTO.getTotalCost());
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", "Spare Amount");
            map.put("Reference", "Job");

            //////System.out.println("jobCardId = " + renderServiceTO.getJobCardId());
            map.put("SearchCode", renderServiceTO.getJobCardId());
            //////System.out.println("map2 =---------------------> " + map);
            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
            //////System.out.println("status1 = " + insertStatus);
            //--------------------------------- acc 2nd row start --------------------------
            if (insertStatus > 0) {
                map.put("DetailCode", "2");
                map.put("ledgerId", "52");
                map.put("particularsId", "LEDGER-40");
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map2 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + insertStatus);
            }
            //Vat Amount
            codev2++;
            voucherCode = "PAYMENT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", 1578);
            map.put("particularsId", "LEDGER-1558");

            map.put("amount", 0);
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", "Vat Amount");
            map.put("Reference", "Job");

            //////System.out.println("jobCardId = " + renderServiceTO.getJobCardId());
            map.put("SearchCode", renderServiceTO.getJobCardId());
            //////System.out.println("map2 =---------------------> " + map);
            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
            //////System.out.println("status1 = " + insertStatus);
            //--------------------------------- acc 2nd row start --------------------------
            if (insertStatus > 0) {
                map.put("DetailCode", "2");
                map.put("ledgerId", "52");
                map.put("particularsId", "LEDGER-40");
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map2 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + insertStatus);
            }
            //serveice Charge
            codev2++;
            voucherCode = "PAYMENT-" + codev2;
            //////System.out.println("voucherCode = " + voucherCode);
            map.put("voucherCode", voucherCode);
            map.put("mainEntryType", "VOUCHER");
            map.put("entryType", "PAYMENT");
            map.put("ledgerId", 1579);
            map.put("particularsId", "LEDGER-1559");

            map.put("amount", 0);
            map.put("Accounts_Type", "DEBIT");
            map.put("Remark", "Service Charge");
            map.put("Reference", "Job");

            //////System.out.println("jobCardId = " + renderServiceTO.getJobCardId());
            map.put("SearchCode", renderServiceTO.getJobCardId());
            //////System.out.println("map2 =---------------------> " + map);
            insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
            //////System.out.println("status1 = " + insertStatus);
            //--------------------------------- acc 2nd row start --------------------------
            if (insertStatus > 0) {
                map.put("DetailCode", "2");
                map.put("ledgerId", "52");
                map.put("particularsId", "LEDGER-40");
                map.put("Accounts_Type", "CREDIT");
                //////System.out.println("map2 =---------------------> " + map);
                insertStatus = (Integer) getSqlMapClientTemplate().update("trip.insertTripAccountEntry", map);
                //////System.out.println("status2 = " + insertStatus);
            }
            //push Data to EFS
//                  updateJobCardBillToEFS(renderServiceTO.getJobCardId());

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertJobCardBillDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertJobCardBillDetails ", sqlException);
        }
        return insertBillDetails;
    }

    public ArrayList getServiceList() {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList serviceList = new ArrayList();
        try {
            serviceList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getServiceTypeMaster", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getGroupList", sqlException);
        }
        return serviceList;
    }

    public ArrayList getBilledJobCardForView(RenderServiceTO clodeJcTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {

            String regNo = clodeJcTO.getRegNo();
            regNo = regNo.replace(" ", "");
            map.put("startIndex", clodeJcTO.getStartIndex());
            map.put("endIndex", clodeJcTO.getEndIndex());
            map.put("regNo", regNo);
            map.put("jcId", clodeJcTO.getJcId());
            map.put("compId", clodeJcTO.getCompId());
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getBilledJobCardForView", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getClosedJobCardForBill Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getClosedJobCardForBill", sqlException);
        }
        return jcList;
    }

    public ArrayList getBilledJobCardDetails(int jobCardNo) {
        Map map = new HashMap();

        //////System.out.println("jcno in getCloseJobCardDetails:" + jobCardNo);
        map.put("jobCardId", jobCardNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList billDetails = new ArrayList();

        try {
            billDetails = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getBilledJobCardDetails", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCloseJobCardDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCloseJobCardDetails", sqlException);
        }
        return billDetails;
    }

    public ArrayList getBilledJobCardGroupDetails(int jobCardNo) {
        Map map = new HashMap();

        //////System.out.println("jcno in getCloseJobCardDetails:" + jobCardNo);
        map.put("jobCardId", jobCardNo);
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList billGroupDetails = new ArrayList();

        try {
            billGroupDetails = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getBilledJobCardGroupDetails", map);

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCloseJobCardDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCloseJobCardDetails", sqlException);
        }
        return billGroupDetails;
    }

    public int insertTyerDetails(RenderServiceTO renderServiceTO, int userId) {
        Map map = new HashMap();

        int insertTyerDetails = 0;
        int user = userId;
        try {
            map.put("userId", user);
            map.put("vehicleId", renderServiceTO.getVehicleId());
            map.put("oldTyerNo", renderServiceTO.getOldTyerNo());
            map.put("newTyerNo", renderServiceTO.getNewTyerNo());
            map.put("odometerReading", renderServiceTO.getOdometerReading());
            map.put("changeDate", renderServiceTO.getChangeDate());
            map.put("newTyerype", renderServiceTO.getNewTyerype());
            map.put("tyerCompanyName", renderServiceTO.getTyerCompanyName());
            map.put("remarks", renderServiceTO.getRemarks());
            map.put("tyerAmount", renderServiceTO.getTyerAmount());
            //////System.out.println("map for tyer:"+map);

            insertTyerDetails = (Integer) getSqlMapClientTemplate().update("renderservice.insertTyersDetails", map);
            //////System.out.println("insertTyerDetails size:"+insertTyerDetails);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("inserttyer_detils Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertTyersDetails ", sqlException);
        }
        return insertTyerDetails;
    }

//     public void updateJobCardBillToEFS(String jobCardId) {
//
//        Map map = new HashMap();
//        try {
//              map.put("jobCardId", jobCardId);
//             //////System.out.println("map::::" + map);
//            ArrayList tripAccountDetails = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getJobCardBillDetails", map);
//            ArrayOfTMSModel ar = new ArrayOfTMSModel();
//           ar.getTMSModel().addAll(tripAccountDetails);
//            //////System.out.println("tmsmodel Size:"+ar.getTMSModel().size()) ;
//            Iterator itr1 = tripAccountDetails.iterator();
//            TMSModel tpTO = new TMSModel();
//          while (itr1.hasNext()) {
//                tpTO = new TMSModel();
//                tpTO = (TMSModel) itr1.next();
//               //////System.out.println("tpTO.getOrder()"+tpTO.getOrderno());
//               //////System.out.println("tpTO.getAccountentrydate()"+tpTO.getAccountentrydate());
//               //////System.out.println("tpTO.getAccountstype()"+tpTO.getAccountstype());
//               //////System.out.println("tpTO.getPartycode()"+tpTO.getPartycode());
//               //////System.out.println("tpTO.getPartytype()"+tpTO.getPartytype());
//               //////System.out.println("tpTO.getNarration()"+tpTO.getNarration());
//               //////System.out.println("tpTO.getChargecode()"+tpTO.getChargecode());
//               //////System.out.println("tpTO.getChargeamount()"+tpTO.getChargeamount());
//               //////System.out.println("tpTO.getJobcardtype()"+tpTO.getJobcardtype());
//
//          }
//            Service srv = new Service();
//            ServiceSoap srvs = srv.getServiceSoap();
//            CreateTMSAccountDetailResult result = null;
//           // for (int j = 0; j < temp.length; j++) {
//             //   System.out.println(temp[j]);
//                result = srvs.createTMSAccountDetail(ar);
//                //////System.out.println("result.getContent() = " + result.getContent());
//                List resultList = result.getContent();
//                for (int k = 0; k < resultList.size(); k++) {
//                    System.out.println(resultList.get(k));
//                }
//           // }
//
//
//        } catch (Exception sqlException) {
//            /*
//             * Log the exception and propagate to the calling class
//             */
//            sqlException.printStackTrace();
//            FPLogUtils.fpDebugLog("updateOrderStatusToEFS Error" + sqlException.toString());
//            FPLogUtils.fpErrorLog("sqlException" + sqlException);
//            //throw new FPRuntimeException("EM-MRS-01", CLASS, "updateOrderStatusToEFS List", sqlException);
//        }
//    }
    public ArrayList getJobCardDetails(int jobcardId) {
        Map map = new HashMap();
        map.put("jobcardId", jobcardId);
        ArrayList jobCardList = new ArrayList();
        try {
            jobCardList = (ArrayList) getSqlMapClientTemplate().queryForList("operation.getJobCardDetails", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getVehicleRegNoForJobCard Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getVehicleRegNoForJobCard", sqlException);
        }

        return jobCardList;
    }

    public int insertSms(OperationTO operationTO, int userId) {
        Map map = new HashMap();
        int index = 0;
        map.put("userId", userId);
        map.put("wayBillId", operationTO.getJobCardId());
        map.put("smsContent", operationTO.getSmsContent());
        map.put("smsTo", operationTO.getSmsTo());
        int insertSms = 0;
        try {
            insertSms = (Integer) getSqlMapClientTemplate().update("operation.insertSms", map);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertSms Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertSms", sqlException);
        }

        return insertSms;
    }

    public String getExistingProblems(String probId, String vehicleId) {
        Map map = new HashMap();
        map.put("probId", probId);
        map.put("vehicleId", vehicleId);
        /*
         * set the parameters in the map for sending to ORM
         */
        String activityDetails = "";
        try {
            System.out.println("map value is=" + map);
            activityDetails = (String) getSqlMapClientTemplate().queryForObject("renderservice.getExistingProblems", map);
            /*
             if(activityDetails == null || "".equals(activityDetails)){
             activityDetails = (String) getSqlMapClientTemplate().queryForObject("renderservice.getActivityDetailsAllModel", map);
             if(activityDetails == null || "".equals(activityDetails)){
             activityDetails = (String) getSqlMapClientTemplate().queryForObject("renderservice.getActivityDetailsAllVehAllModel", map);
}
             }
             */
            if (activityDetails == null || "".equals(activityDetails)) {
                activityDetails = "";
            }

        } catch (Exception sqlException) {

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getActivityDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getActivityDetails", sqlException);
        }
        return activityDetails;
    }

    public ArrayList getOldComplaints(RenderServiceTO clodeJcTO) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        ArrayList jcList = new ArrayList();

        try {
            map.put("vehicleId", clodeJcTO.getVehicleId());
            System.out.println("map = " + map);
            jcList = (ArrayList) getSqlMapClientTemplate().queryForList("renderservice.getOldComplaints", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();

            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCloseJobCardViewList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getCloseJobCardViewList", sqlException);
        }
        return jcList;
    }

}
