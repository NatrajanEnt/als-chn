
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<style>
    #index td {
        color:white;
        font-weight: bold;
        background-color:#5BC0DE;
        font-size:14px;
    }
</style>


<% String status="";
           Date today = new Date();
           SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
           String startDate = sdf.format(today);
           String endDate = sdf.format(today);
            Calendar cal = Calendar.getInstance();
               cal.add(Calendar.DATE, 7);
              String nextDate = sdf.format(cal.getTime());
%>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">Change Vehicle</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setVehicleType(1);
//                setVehicle(1);
//                computeVehicleCapUtil();
                  ">

                <form name="trip" method="post">


                    <br>
                    <%@include file="/content/common/message.jsp" %>

                    <div id="tabs" >
                        <ul>

                            <c:if test="${tripDetails != null}">
                                <li><a href="#tripDetail"><span>Trip Details</span></a></li>
                                <!--<li><a href="#tripEnd"><span>Trip End</span></a></li>-->
                                </c:if>
                        </ul>


                        <div id="tripDetail">
                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <table  class="table table-info mb30 table-border" id="bg">
                                        <tr id="index" height="30">
                                            <td  colspan="6" >Trip Details &emsp; <c:out value="${trip.tripCode}" /></td>
                                        </tr>
                                        <tr>
                                            <td >CNote No(s)</td>
                                            <td >
                                                <c:out value="${trip.cNotes}" />
                                                <input type="hidden" name="tripId" Id="tripId" class="form-control" value='<c:out value="${trip.tripId}" />'>
                                                <input type="hidden" name="cNotes" Id="cNotes" class="form-control" value='<c:out value="${trip.cNotes}" />'>
                                            </td>
                                            <td >Billing Type</td>
                                            <td >
                                                <c:out value="${trip.billingType}" />
                                                <input type="hidden" name="billingType" Id="billingType" class="form-control" value='<c:out value="${trip.billingType}" />'>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td >Customer Name</td>
                                            <td >
                                                <c:out value="${trip.customerName}" />
                                                <input type="hidden" name="customerName" Id="customerName" class="form-control" value='<c:out value="${trip.customerName}" />'>
                                            </td>
                                            <td >Customer Type</td>
                                            <td  >
                                                <c:out value="${trip.customerType}" />
                                                <input type="hidden" name="customerType" Id="customerType" class="form-control" value='<c:out value="${trip.customerType}" />'>
                                                <input type="hidden" name="tripType" Id="tripType" class="form-control" value='<c:out value="${trip.tripType}" />'>
                                            </td>

                                        </tr>
                                        <tr>
                                            <td >Route Name</td>
                                            <td >
                                                <c:out value="${trip.routeInfo}" />
                                                <input type="hidden" name="routeInfo" Id="routeInfo" class="form-control" value='<c:out value="${trip.routeInfo}" />'>
                                            </td>
                                            <td >Special Instruction</td>
                                            <td ><textarea name="tripRemarks" cols="20" rows="2" > <c:out value="${consginmentRemarks}" /></textarea></td>
                                            <td  style="display: none">Reefer Required</td>
                                            <td  style="display: none">
                                                <c:out value="${reeferRequired}" />
                                                <input type="hidden" name="reeferRequired" Id="reeferRequired" class="form-control" value='<c:out value="${reeferRequired}" />'>
                                            </td>
                                            <td  style="display: none">Vehicle Type</td>
                                        <input type="hidden" name="vehicleType" Id="vehicleType" class="form-control" value=''/>
                                        </tr>
                                        <tr>
                                            <td >Transporter...</td>
                                            <td >
                                                <select name="vendor" id="vendor" onchange="setVehicleType(2);" class="form-control" style="width:240px;" >
                                                    <c:if test="${vendorList != null}">
                                                        <option value="0~0" selected>--select--</option>
                                                        <option value="1~0" >own</option>
                                                        <c:forEach items="${vendorList}" var="vehNo">
                                                            <option value='<c:out value="${vehNo.vendorId}"/>~<c:out value="${vehNo.contractTypeId}"/>'><c:out value="${vehNo.vendorName}"/></option>
                                                        </c:forEach>
                                                    </c:if>
                                                </select>
                                                <script>
                                                    document.getElementById("vendor").value = '<c:out value="${trip.vendorId}" />~<c:out value="${trip.vendorContractTypeId}" />';
                                                </script>
                                            </td>

                                            <td >Vehicle Type</td>
                                            <td >
                                                <input type="hidden" name="vehType" Id="vehType" class="form-control" value='<c:out value="${trip.vehicleType}" />-<c:out value="${trip.vehicleTonnage}" />~<c:out value="${trip.vehicleContractTypeId}" />'/>
                                                <select name="vehicleTypeName" id="vehicleTypeName" class="form-control" onchange="showVehicle();setVehicle(2);" style="width:240px;">

                                                </select>
                                            </td>
                                        </tr>
                                        <tr  class ="vehCategory" id="vehCate" >
                                            <td >
                                                Vehicle Category
                                            </td>
                                            <td>
                                                <select name="vehicleCategory" id="vehicleCategory"  style="width:130px" class="form-control" onchange="showVehicle();">
                                                    <option value="0" selected>---select---</option>
                                                    <!--<option value="1" >Leased </option>-->
                                                    <option value="2" >Hire</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                        <input type="hidden"   name="tripStatus" id="tripStatus" value="0" />
                                        <%int lableSno = 1;%>
                                        <c:forEach items="${orderPointDetails}" var="consignment">
                                            <input type="hidden" name="labelConsignmentOrderNo" id="labelConsignmentOrderNo<%=lableSno%>" value="<c:out value="${consignment.consignmentOrderNo}" />" />
                                            <%lableSno++;%>
                                        </c:forEach>
                                        <script>
                                            function setVehicleType(val) {
                                                var vendorTemp = document.getElementById("vendor").value;
                                                var temp = vendorTemp.split("~");
                                                if (temp[1] == '1' || temp[1] == '0') {
                                                    document.trip.elements['driver1Name'].style.display = 'block';
                                                    document.trip.elements['driverName'].style.display = 'none';
                                                    $("tr.vehCategory").hide();
                                                } else {
                                                    document.trip.elements['driver1Name'].style.display = 'block';
                                                    document.trip.elements['driverName'].style.display = 'none';
                                                    $("tr.vehCategory").show();

                                                }

                                                $.ajax({
                                                    url: "/throttle/getVendorVehicleType.do",
                                                    dataType: "json",
                                                    data: {

                                                        vendorId: temp[0]
                                                    },
                                                    success: function (data) {
                                                        //                                            alert(data);
                                                        if (data != '') {
                                                            $('#vehicleTypeName').empty();
                                                            $('#vehicleTypeName').append(
                                                                    $('<option></option>').val(0).html('--select--'))
                                                            //                                            $('#vehicleTypeName').append(
                                                            //                                                    $('<option></option>').val(1058).html('20\''))
                                                            //                                            $('#vehicleTypeName').append(
                                                            //                                                    $('<option></option>').val(1059).html('40\''))
                                                            $.each(data, function (i, data) {

                                                                $('#vehicleTypeName').append(
                                                                        $('<option style="width:90px"></option>').attr("value", data.Id).text(data.Name)
                                                                        )
                                                            });
                                                            if (val == 1) {
                                                                var vehType = document.getElementById("vehType").value;
                                                                document.getElementById("vehicleTypeName").value = vehType;
                                                                setVehicle(1);
                                                            }
                                                        } else {
                                                            $('#vehicleTypeName').empty();
                                                            $('#vehicleTypeName').append(
                                                                    $('<option></option>').val(0).html('--select--'))
                                                        }
                                                    }
                                                });
                                            }
                                            function setVehicle(val) {
                                                //                                    alert("bsdjhgfjs");
                                                var vendorTemp = document.getElementById("vendor").value;
                                                var vehicletypename = document.getElementById("vehicleTypeName").value;
                                                //                                alert("vehicletypename=="+vehicletypename);
                                                var temp = vendorTemp.split("~");
                                                var temp1 = vehicletypename.split("-");
                                                var vehicleType = temp1[0];
                                                //                                var vehicletypename= $("#vehicleTypeName option:selected").text();
                                                document.getElementById("vehicleType").value = vehicleType;
                                                //                                 alert("vehicleType=="+vehicleType);
                                                //var statusId = arr[0];
                                                $.ajax({
                                                    url: "/throttle/getVendorVehicle.do",
                                                    dataType: "json",
                                                    data: {
                                                        //                                            vehicleTypeId: $("#vehicleTypeName").val(),
                                                        vehicleTypeId: vehicleType,
                                                        vendorId: temp[0]
                                                    },
                                                    success: function (data) {
                                                        if (data != '') {
                                                            $('#vehicleNo').empty();
                                                            $('#vehicleNo').append(
                                                                    $('<option></option>').val(0).html('--select--'))
                                                            $.each(data, function (i, data) {
                                                                //                                            alert(data.vehicleId);
                                                                $('#vehicleNo').append(
                                                                        $('<option style="width:90px"></option>').attr("value", data.vehicleId).text(data.Name)
                                                                        )
                                                            });
                                                        } else {
                                                            $('#vehicleNo').empty();
                                                            $('#vehicleNo').append(
                                                                    $('<option></option>').val(0).html('--select--'))
                                                        }
                                                        if (val == 1) {
//                                                                                                                        alert("temp[0]=="+temp[0]);
                                                            if (temp[0] == 1) {
                                                                var vehId = document.getElementById("vehId").value;
                                                                //                                                            alert("if");
                                                                document.getElementById("vehicleNo").value = vehId;
                                                                document.trip.elements['hireVehicleNo'].style.display = 'none';
                                                            } else {
//                                                                                                                            alert("else");
                                                                document.trip.elements['vehicleNo'].style.display = 'none';
                                                                document.trip.elements['hireVehicleNo'].style.display = 'block';
                                                            }
                                                            computeVehicleCapUtil();
                                                        }
                                                    }
                                                });
                                                setVehicleTonnage();
                                            }
                                            function  setVehicleTonnage() {
                                                var temp = "";

                                                var vehicleTypeTemp = document.getElementById("vehicleTypeName").value;

                                                temp = vehicleTypeTemp.split('-');

                                                document.getElementById("vehicleTonnage").value = parseInt((temp[1].trim()));
                                                document.getElementById("vehicleTypeName").value = string.valueOf((temp[0].trim()));

                                            }

                                            function computeVehicleCapUtil() {
                                                setVehicleValues();
                                                var orderWeight = document.trip.totalWeight.value;
                                                var vehicleCapacity = document.trip.vehicleTonnage.value;
                                                var ordt = document.getElementById("orderType").value;
                                                var ordType = parseInt(ordt);
                                                var remainWeigh = "";
                                                if (ordType == 1) {
                                                    //alert('in order type 1');
                                                    remainWeigh = 0;
                                                } else {
                                                    //alert('not in order type 1');
                                                    remainWeigh = orderWeight - vehicleCapacity;
                                                    remainWeigh = remainWeigh.toFixed(2);
                                                }

                                                if (remainWeigh <= 0) {
                                                    remainWeigh = 0;
                                                }
                                                //alert("remain weight"+remainWeigh);
                                                document.getElementById("remainWeight").value = remainWeigh;
                                                //alert(orderWeight +"  "+vehicleCapacity);
                                                var utilPercent;
                                                if (vehicleCapacity > 0) {
                                                    // alert("ord weight"+orderWeight);
                                                    var orw = parseInt(orderWeight);
                                                    var vcap = parseInt(vehicleCapacity);
                                                    if (orw > vcap) {
                                                        orderWeight = vehicleCapacity;
                                                        // alert(orderWeight);
                                                        utilPercent = (orderWeight / vehicleCapacity) * 100;
                                                    } else {
                                                        // alert('djdjd');
                                                        utilPercent = (orderWeight / vehicleCapacity) * 100;
                                                    }

                                                    // alert(utilPercent);
                                                    document.trip.vehicleCapUtil.value = utilPercent.toFixed(0);
                                                    var e = document.getElementById("vehicleCheck");
                                                    e.style.display = 'block';
                                                } else {
                                                    //document.trip.vehicleCapUtil.value = 0;
                                                }
                                                // for running vehicle ajax
                                                var s = document.getElementById("vehicleNo").value;
                                                var vn = s.split("-");
                                                //alert(vn[14]);
                                                document.getElementById("vehicleTypeId").value = vn[14];
                                                var vstatus = document.getElementById("tripStatus").value;
                                                if (vstatus == "222") {
                                                    s = document.getElementById("vehicleNo").value;
                                                    var ordsts = document.getElementById("orderStatus").value;
                                                    //  alert(s+" "+ordsts);

                                                    $.ajax({url: "/throttle/getRunningTripDetails.do", dataType: "json", data: {
                                                            vehicleId: vn[0],
                                                            ordStatus: ordsts
                                                        }, success: function (data) {
                                                            // alert(data);
                                                        }
                                                    });
                                                }
                                            }

                                            function setVehicleValues() {
                                                var value = document.trip.vehicleNo.value;
                                                //  alert(value);
                                                if (value != 0) {
                                                    $('#vehicleId').val(value);
                                                } else {
                                                    $('#vehicleId').val('0');
                                                }
                                              
                                            }


                                        </script>        

                                        <script>
                                            document.getElementById("vehicleCategory").value = 0;
//                                            alert("Dd");
                                            function showVehicle() {
                                                var vehCategory = document.getElementById("vehicleCategory").value;
                                                var vendor = document.getElementById("vendor").value;
//                                                alert("vehCategory--"+vehCategory)
//                                                alert("vendor--"+vendor)
                                                if (vehCategory == '1' || vendor == '1~0') {
                                                    document.trip.elements['vehicleNo'].style.display = 'block';
                                                    document.trip.elements['hireVehicleNo'].style.display = 'none';

                                                } else {
                                                    document.trip.elements['vehicleNo'].style.display = 'none';
                                                    document.trip.elements['hireVehicleNo'].style.display = 'block';

                                                    document.getElementById("ownership").value = '3';
                                                }


                                            }
                                            
                                            function setDriverId() {
                                                var value = document.trip.driverName.value;
                                                $('#driver1Id').val(value);

                                            }

                                        </script>
                                        <script>
                                            var companyId = '<%=request.getAttribute("companyId")%>';

                                            $(document).ready(function () {
                                                // Use the .autocomplete() method to compile the list based on input from user

                                                $('#driver1Name').autocomplete({
                                                    source: function (request, response) {
                                                        $.ajax({
                                                            url: "/throttle/getDriverNames.do",
                                                            dataType: "json",
                                                            data: {
                                                                driverName: (request.term).trim(),
                                                                companyId: companyId,
                                                                textBox: 1
                                                            },
                                                            success: function (data, textStatus, jqXHR) {
                                                                var items = data;
                                                                response(items);
                                                            },
                                                            error: function (data, type) {
                                                            }
                                                        });
                                                    },
                                                    minLength: 1,
                                                    select: function (event, ui) {
                                                        var id = ui.item.Id;
                                                        var name = ui.item.Name;
                                                        var mobile = ui.item.Mobile;

                                                        $('#driver1Id').val(id);
                                                        $('#driver1Name').val(name);
                                                        $('#driverMobile').val(mobile);
//                $('#mobileNo').val(mobile);
                                                        return false;
                                                    }
                                                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                                                    var dlNo = item.License;
                                                    if (dlNo == '') {
                                                        dlNo = '-';
                                                    }
                                                    var itemVal = item.Name + '(DL:' + dlNo + ')';
                                                    itemVal = '<font color="green">' + itemVal + '</font>';
                                                    return $("<li></li>")
                                                            .data("item.autocomplete", item)
                                                            .append("<a>" + itemVal + "</a>")
                                                            .appendTo(ul);
                                                };

                                            });
                                            //end ajax for vehicle Nos
                                        </script>                           
                                        <td >Vehicle No</td>
                                        <td >
                                            <input type="hidden" name="vehId" Id="vehId" class="form-control" value='<c:out value="${trip.vehicleId}" />'/>
                                            <select name="vehicleNo" id="vehicleNo"  class="select2 input-default form-control" onchange="computeVehicleCapUtil();" style="width:240px;height:40px;" >
                                                <option value="0" selected>--Select--</option>
                                            </select>

                                            <!--                               <script>
                                                                                $('#vehicleNo').select2({placeholder: 'Fill vehicleNo'});
                                                                           </script> -->

                                            <input type="text" id="hireVehicleNo" name="hireVehicleNo" class="form-control" value='<c:out value="${trip.hireVehicleNo}" />' style="width:240px;"  style="display:none;"/>

                                        </td>
                                        <input type="hidden" name="ownership" id="ownership" class="form-control" value="0">
                                        <input type="hidden" name="vehicleId" Id="vehicleId" class="form-control" value="<c:out value="${trip.vehicleId}" />">
                                        <input type="hidden" name="vendorId" Id="vendorId" class="form-control" value="">
                                        <td >Vehicle Capacity (MT)</td>
                                        <td ><input type="text" name="vehicleTonnage" Id="vehicleTonnage" readonly class="form-control" value="<c:out value="0"/>"></td>
                                        <td  id="remain1" style="display: none;">Remain Order Weight</td>
                                        <td  id="remain2" style="display: none;"><input type="text" name="remainWeight" Id="remainWeight" readonly class="form-control" value="0"></td>
                                        </tr>

                                        <tr>
                                            <td >Veh. Cap [Util%]</td>
                                            <td ><input type="text" name="vehicleCapUtil" Id="vehicleCapUtil" readonly class="form-control"  style="width:240px;" value='<c:out value="${trip.vehicleCapUtil}" />'></td>
                                            <td >Generate New Fuel Slip</td>
                                            <td ><input type="checkbox" id="newSlip" name="newSlip" onclick="setValues();"/>
                                                <input type="hidden" id="newSlipValue" name="newSlipValue" value="N"/>
                                            </td>
                                            <td  style="display: none">Trip Schedule</td>
                                            <td  style="display: none"><c:out value="${tripSchedule}" /></td>
                                        </tr>


                                        <tr>
                                            <td >Primary Driver </td>
                                            <td  >
                                                <input type="hidden" name="orderType" id="orderType"  value='<c:out value="${trip.orderType}" />'>
                                                <input type="text" name="driver1Name"   Id="driver1Name"  style="width:240px;" class="form-control" value="<c:out value="${trip.driverInTrip}" />" style="display: none">
                                                <select id="driverName" name="driverName" onchange="setDriverId();" style="width:130px;display: none">
                                                    <c:if test="${driverList != null}">
                                                        <option value="0" selected>--Select--</option>
                                                        <c:forEach items="${driverList}" var="driverList">
                                                            <option value='<c:out value="${driverList.empId}"/>'><c:out value="${driverList.empName}"/></option>
                                                        </c:forEach>
                                                    </c:if>
                                                </select>
                                                <input type="text" name="driverMobile" readonly Id="driverMobile" class="form-control"  style="width:240px;" value='<c:out value="${trip.driverMobile}" />'/>
                                                <input type="hidden" name="driver1Id" Id="driver1Id" class="form-control" value='<c:out value="${trip.driverId}" />'>

                                            </td>
                                            <td >Odometer Reading</td>   
                                            <td   >
                                                <input type="text" name="odometerReading" Id="odometerReading" class="form-control" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" >
                                            </td>

                                            <td  style="display: none">Secondary Driver(s) </td>
                                            <td  colspan="5" style="display: none">
                                                <input type="text" name="driver2Name"  readonly Id="driver2Name" class="form-control" value="<c:out value="${secondaryDriver1Name}"/>"  >
                                                <input type="hidden" name="driver2Id" Id="driver2Id" class="form-control" value='0'  >
                                                <input type="text" name="driver3Name"  readonly  Id="driver3Name" class="form-control" value="<c:out value="${secondaryDriver2Name}"/>"  >
                                                <input type="hidden" name="driver3Id" Id="driver3Id" class="form-control" value='0' >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Change Date</td>
                                            <td>
                                                <input type="text" class="datepicker , form-control" name="driverChangeDate" id="driverChangeDate"  value='<c:out value="${startDate}"/>' style="width:240px;height:40px;"/>
                                            </td>
                                            <td>&emsp;</td>
                                        </tr>
                                        <tr style="display: none">
                                            <td >Product / Temp Info </td>
                                            <td ><c:out value="${productInfo}" /></td>
                                        </tr>
                                    </table>
                                    <br/>
                                    <br/>
                                    <div id="vehicleCheck" style="display:none;">
                                        <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                                            <tr>
                                                <td class="contenthead" colspan="4" >Vehicle Compliance Check</td>
                                            </tr>
                                            <tr>
                                                <td >Vehicle FC Valid UpTo</td>
                                                <td ><label id="fcExpiryDate"><font color="green"></font></label></td>
                                            </tr>
                                            <tr>
                                                <td >Vehicle Insurance Valid UpTo</td>
                                                <td ><label id="insuranceExpiryDate"><font color="green"></font></label></td>
                                            </tr>
                                            <tr>
                                                <td >Vehicle Permit Valid UpTo</td>
                                                <td ><label id="permitExpiryDate"><font color="green"></font></label></td>
                                            </tr>
                                            <tr>
                                                <td >Road Tax Valid UpTo</td>
                                                <td ><label id="roadExpiryDate"><font color="green"></font></label></td>
                                            </tr>
                                        </table>
                                        <br/>
                                    </div>
                                    <br/>
                                    <br>


                                </c:forEach>
                            </c:if>



                            <div id="bunkDetail" style="display:none;">
                                  <!--<table class="table table-info mb30 table-hover" width="100%"  id="fuelTBL" name="fuelTBL" >-->
                                <table style="width: 70%; left: 30px;" class="table table-info mb30 table-hover" cellpadding="0"  cellspacing="0" align="center" border="0" id="fuelTBL" name="fuelTBL">
                                    <tr >
                                        <th>S No</th>
                                        <th>Bunk Name</th>
                                        <th>Slip No</th>
                                        <th>Date</th>
                                        <th>Ltrs</th>
                                        <th>Amount</th>
                                        <th>Person</th>
                                        <th>Remarks</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    <tr >
                                        <td> 1</td>
                                        <td>
                                            <input type='hidden' id='uniqueIdNew' name='uniqueIdNew' value=''/>
                                            <select style="width:130px;" id='bunkName1' name='bunkName' >
                                                <option   value="0">---Select---</option>
                                                <c:if test = "${bunkList != null}" >
                                                    <c:forEach items="${bunkList}" var="bunk">
                                                        <option selected value='<c:out value="${bunk.bunkId}" />'> <c:out value="${bunk.bunkName}" />
                                                        </c:forEach >
                                                    </c:if>
                                            </select>
                                        </td>
                                        <td>
                                            <input name="slipNo" type="text" class="form-control" id="slipNo" value="" />
                                        </td>

                                        <td>
                                            <input name="bunkPlace" type="hidden" class="form-control" id="bunkPlace"  />
                                            <input name="fuelDate" id="fuelDate" type="text" class="datepicker" value='<%=startDate%>' id="fuelDate"  style="width:120px;" />
                                        </td>
                                        <td>
                                            <input name="fuelLtrs" type="text" class="form-control" value="0" id="fuelLtrs1"  onblur="sumFuel();" onchange="fuelPrice1(1);" style="width:80px;" />
                                        </td>
                                        <td>
                                            <input name="fuelAmount" readonly type="text" class="form-control" value="0" id="fuelAmount1"  onblur="sumFuel();"  style="width:80px;" readonly />
                                        </td>

                                        <td>
                                            <input name="fuelFilledName" type="text" class="form-control" id="fuelFilledName" value="<%= session.getAttribute("userName")%>" />
                                            <input name="fuelFilledBy" type="hidden" class="form-control" id="fuelFilledBy" value="<%= session.getAttribute("userId")%>" />
                                        </td>
                                        <td>
                                            <textarea name='fuelRemarks' id='fuelRemarks' class='textbox' type='text' rows='2' cols='10'></textarea>
                                            <input type="hidden" name="HfId" id="HfId" />
                                        </td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" align="center">
                                            <input type="button" name="add" value="Add" onclick="addFuelRow()" id="add" class="btn btn-info" />
                                            &nbsp;&nbsp;&nbsp;
                                            <input type="button" name="delete" value="Delete" onclick="delFuelRow()" id="delete" class="btn btn-info" />
                                            <input type="hidden" name="totalFuelLtrs" id="totalFuelLtrs" value=""/>
                                            <input type="hidden" name="totalKms" id="totalKms" value=""/>
                                            <input type="hidden" name="totalFuelAmount" id="totalFuelAmount" value=""/>
                                        </td>
                                    </tr>
                                    <script>
                                        var poItems = 0;
                                        var rowCount = '';
                                        var sno = '';
                                        var snumber = '';

                                        function addFuelRow()
                                        {
                                            var currentDate = new Date();
                                            var day = currentDate.getDate();
                                            var month = currentDate.getMonth() + 1;
                                            var year = currentDate.getFullYear();
                                            var myDate = day + "-" + month + "-" + year;

                                            if (sno < 9) {
                                                sno++;
                                                var tab = document.getElementById("fuelTBL");
                                                var rowCount = tab.rows.length;

                                                snumber = parseInt(rowCount) - 1;
                                                //                    if(snumber == 1) {
                                                //                        snumber = parseInt(rowCount);
                                                //                    }else {
                                                //                        snumber++;
                                                //                    }

                                                var newrow = tab.insertRow(parseInt(rowCount) - 1);
                                                newrow.height = "30px";
                                                // var temp = sno1-1;
                                                var cell = newrow.insertCell(0);
                                                var cell0 = "<td><input type='hidden'  name='fuelItemId' /> " + snumber + "</td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(1);
                                                //                                        cell0 = "<td class='text2'><input name='bunkName' type='text' class='textbox'     id='bunkName' class='Textbox' /></td>";
                                                cell0 = "<td class='text2'><select class='textbox' style='width:100px;' id='bunkName" + snumber + "' style='width:123px'  name='bunkName'><option selected value=0>---Select---</option><c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk"><option  value='<c:out value="${bunk.bunkId}" />'><c:out value="${bunk.bunkName}" /> </c:forEach ></c:if> </select></td><input type='text' id='uniqueIdNew" + snumber + "' name='uniqueIdNew' value=''/>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(2);
                                                cell0 = "<td class='text1'><input name='slipNo' id='slipNo" + snumber + "' type='text'  style='width:120px;' value='' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(3);
                                                cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate" + snumber + "' type='text' class='datepicker1' style='width:120px;' value='" + myDate + "' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(4);
                                                cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' onchange='fuelPrice1(" + snumber + ");' type='text' class='textbox' id='fuelLtrs" + snumber + "' value='0' style='width:80px;' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(5);
                                                cell0 = "<td class='text1'><input name='fuelAmount' readonly onBlur='sumFuel();' type='text' class='textbox' value='0' style='width:80px;' id='fuelAmount" + snumber + "' class='Textbox' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;



                                                cell = newrow.insertCell(6);
                                                cell0 = "<td class='text1'><input name='fuelFilledBy'  type='hidden' class='textbox' id='fuelFilledBy'  value='<%= session.getAttribute("userId")%>' /><input name='fuelFilledName' type='text' style='width:55px;' class='textbox' id='fuelFilledName' class='Textbox' value='<%= session.getAttribute("userName")%>' /></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(7);
                                                cell0 = "<td class='text1'><textarea name='fuelRemarks' id='fuelRemarks' class='textbox' type='text' rows='2' cols='10'></textarea></td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell0;

                                                cell = newrow.insertCell(8);
                                                var cell1 = "<td><input type='checkbox' name='deleteItem' value='" + snumber + "'/> </td>";
                                                //cell.setAttribute(cssAttributeName,"text1");
                                                cell.innerHTML = cell1;
                                                // rowCount++;


                                                $(".datepicker").datepicker({
                                                    /*altField: "#alternate",
                                                     altFormat: "DD, d MM, yy"*/
                                                    changeMonth: true, changeYear: true
                                                });
                                            }
                                        }
                                        function fuelPrice1(rowVal)
                                        {
                                            //  alert("----->"+rowVal);
                                            var fuelRate = document.getElementById('bunkName' + rowVal).value;
                                            //alert("=====>"+fuelRate);
                                            var temp = fuelRate.split('~');
                                            fuelRate = temp[3];
                                            //  alert(fuelRate);

                                            var fuelPrice = fuelRate;
                                            var fuelLters = document.getElementById("fuelLtrs" + rowVal).value;
                                            //  alert("fuelLters==>"+fuelLters);
                                            var fuelAmount = (fuelPrice * fuelLters).toFixed(2);
                                            //  alert("fuelAmount==>"+fuelAmount);
                                            //                                                        document.tripSheet.fuelAmount.value = fuelAmount;
                                            document.getElementById("fuelAmount" + rowVal).value = fuelAmount;
                                            // sumFuel();
                                        }

                                        function delFuelRow() {
                                            try {
                                                var table = document.getElementById("fuelTBL");
                                                rowCount = table.rows.length - 1;
                                                for (var i = 2; i < rowCount; i++) {
                                                    var row = table.rows[i];
                                                    var checkbox = row.cells[7].childNodes[0];
                                                    if (null != checkbox && true == checkbox.checked) {
                                                        if (rowCount <= 1) {
                                                            alert("Cannot delete all the rows");
                                                            break;
                                                        }
                                                        table.deleteRow(i);
                                                        rowCount--;
                                                        i--;
                                                        sno--;
                                                        // snumber--;
                                                    }
                                                }
                                                sumFuel();
                                            } catch (e) {
                                                alert(e);
                                            }
                                        }


                                        function sumFuel() {
                                            //alert("this s ctesting");
                                            var totFuel = 0;
                                            var totltr = 0;
                                            var totAmount = 0;
                                            var totAmt = 0;
                                            totFuel = document.getElementsByName('fuelLtrs');

                                            totAmount = document.getElementsByName('fuelAmount');
                                            for (i = 0; i < totFuel.length; i++) {
                                                totltr = +totltr.toFixed(2) + +totFuel[i].value;
                                            }
                                            document.getElementById('totalFuelLtrs').value = totltr.toFixed(2);
                                            for (i = 0; i < totAmount.length; i++) {
                                                totAmt = +totAmt.toFixed(2) + +totAmount[i].value;
                                            }
                                            document.getElementById('totalFuelAmount').value = totAmt.toFixed(2);
                                            // sumExpenses();
                                            //setBalance();
                                        }


                                    </script>

                                </table>

                                <br>


                            </div>


                            <c:if test = "${tripDetails != null}" >
                                <c:forEach items="${tripDetails}" var="trip">
                                    <c:if test = "${trip.vehicleCount == '1'}" >
                                        <center>
                                            <input type="button" class="btn btn-info" value="save" name="save" onclick="submitPage(this.value);"/>
                                        </center>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </div>

                        <div id="tripEnd" style="display:none">
                                    <table class="table table-info mb30 table-hover" id="bg" width="100%" >
                                        <!--<table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">-->
                                        <tr>
                                            <td   colspan="6" id="tableDesingTD">Trip End Details</td>
                                        </tr>
                                        <tr>
                                        <td ><font color="red">*</font>Trip Start Date</td> 
                                        <td ><c:out value="${startDate}"/></td> 
                                        <td ></td> 
                                        <td ></td> 
                                        <tr>
                                        <!--<input type="hidden" name="customerType" id="customerType" value="1"  >-->
                                        <td ><font color="red">*</font>Trip End Date</td> 
                                        <td ><input type="text" name="endDate" id="endDate" readonly class="form-control datepicker" value='<c:out value="${endDate}"/>'
                                            <input type="hidden" name="todayDate" id="todayDate" value='<%=endDate%>'/>
                                            <input type="hidden" name="vendorId" id="vendorId" value='<c:out value="${vendorId}"/>'/></td>

                                        <td ><font color="red">*</font>Trip Actual End Time</td>
                                        <td  height="25" >HH:<select name="tripEndHour" id="tripEndHour" class="textbox" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option                                                                         value="23">23</option></select>
                                            MI:<select name="tripEndMinute" id="tripEndMinute" class="textbox" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                                        </tr>
                                        
                                        <tr>
                                            <td>&emsp;<font color="red">*</font>Opening KM &emsp;</td>
                                            <td>
                                                <input type="text" name="openKM" id="openKM" onchange="kmCalculator()" value='<c:out value="${openingKM}"/>'   onKeyPress='return onKeyPressBlockCharacters(event);'  class="form-control" style="width:150px;height:40px;"> 
                                                <input type="hidden" name="openKMP" id="openKMP" value="<c:out value="${openingKM}"/>" > 
                                            </td>
                                            <td>&emsp;<font color="red">*</font>Closing KM&emsp;</td>
                                            <td> 
                                                <input type="text" name="closingKM" id="closingKM" onchange="kmCalculator()" value='<c:out value="${endKM}"/>'  class="form-control"  onKeyPress='return onKeyPressBlockCharacters(event);'  style="width:150px;height:40px;">
                                                <input type="hidden" name="closingKMP" id="closingKMP" value="<c:out value="${endKM}"/>" >
                                            </td>
                                            <td>&emsp;Total KM  &emsp;</td><td><input type="text" readonly name="totalKM" id="totalKM" value='<c:out value="${totalKM}"/>' class="form-control" style="width:150px;height:40px;"></td>
                                        </tr>
                                        
                                    </table>
                                        
                                         <script>
                                            function kmCalculator() {
                                                var openKM = document.getElementById("openKM").value;
                                                var closingKM = document.getElementById("closingKM").value;
                                                var totalKM = 0;
                                                if (parseFloat(openKM) < parseFloat(closingKM)) {
                                                    totalKM = parseFloat(closingKM) - parseFloat(openKM);
                                                    document.getElementById("totalKMs").value = totalKM;

                                                } else {
                                                    alert("Please enter valid KM");
                                                    document.getElementById("closingKM").value = "0.00";
                                                    document.getElementById("totalKMs").value = "0.00";
                                                }

                                            }
                                          var vendorId = document.getElementById("vendorId").value;
                                          if(vendorId == "1" ){
                                              $("#kmTable").show();
                                          }else{
                                              $("#kmTable").hide();
                                          } 


                                        </script>
                                    <br/>
                                <c:if test="${vehicleCount != 1}">
                                    <center>
                                        <input type="button" class="btn btn-info" name="TripEnd" value="TripEnd" id="TripEnd" onclick="tripEnd();" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;" />
                                    </center>
                                </c:if>
                            </div>


                        <script>
                            function setValues()
                            {
                                if (document.getElementById('newSlip').checked == true) {
                                    //  alert("ooooo");
                                    document.getElementById("newSlipValue").value = 'Y';
                                    $('#bunkDetail').show();
                                } else {
                                    document.getElementById("newSlipValue").value = 'N';
                                    $('#bunkDetail').hide();
                                }
                            }
                            //                    function setHireVehicle(){
                            //                        var hireVehicleNo = document.getElementById("hireVehicleNo").value;
                            //                         document.getElementById("vehicleId").value = hireVehicleNo;
                            //                    }

                            function onKeyPressBlockCharacters(e)
                            {
                                var key = window.event ? e.keyCode : e.which;
                                var keychar = String.fromCharCode(key);
                                reg = /[a-zA-Z]+$/;

                                return !reg.test(keychar);

                            }
                            function submitPage(val) {
                                var confirms = "";
                                if (document.getElementById('newSlip').checked == true) {

                                    confirms = confirm("Attention.!!Are you sure  to generate New Fuel Slip");
                                } else if (document.getElementById('newSlip').checked == false) {
                                    confirms = confirm("Attention!!Do you want to generate New Fuel  Slip.");
                                }
                                if (confirms == true) {
                                    document.trip.action = '/throttle/saveChangeVehicleAfterTripStart.do?param=' + val;
                                    document.trip.submit();
                                }
                                //                   if(odometerReading == ""){
                                //                       alert("please Enter Odometer Reading")
                                //                       document.getElementById("odometerReading").focus();
                                //                   }else{
                                //                   }
                            }
                            function tripEnd(val) {
                                document.trip.action = '/throttle/saveChangeVehicleAfterTripStart.do?param=' + val;
                                document.trip.submit();
                            }
                        </script>

                    </div>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
