/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import ets.domain.report.business.DprTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.operation.business.OperationBP;
import ets.domain.operation.business.OperationTO;
import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.scheduler.business.SchedulerBP;
import ets.domain.scheduler.business.SchedulerTO;
import ets.domain.thread.web.SendEmail;
import ets.domain.thread.web.SendEmailDetails;
import ets.domain.thread.web.SendSMS;
import ets.domain.scheduler.web.OrderStatusReport;
import ets.domain.scheduler.web.MovementTypeStatus;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.users.business.LoginBP;
import ets.domain.users.business.LoginTO;
import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.report.business.TallyTO;
import ets.domain.sales.business.SalesBP;
import ets.domain.sales.business.SalesTO;
import ets.domain.trip.web.thrERPTablePosting;
import ets.domain.trip.web.*;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
import static ets.domain.util.ThrottleConstants.vehicleStatusReportRunTime;
import static ets.domain.util.ThrottleConstants.vehicleUtilReportRunTime;
import static ets.domain.util.ThrottleConstants.weeklyProdReportRunTime;
//import static ets.domain.util.ThrottleConstants.smsSchedulerStatus;
import java.io.File;
import java.io.IOException;
import static java.lang.Compiler.command;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import throttlegps.gpsActivity;
import throttlegps.main.GpsLocationMain;

//excel
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

//excel
/**
 *
 * @author Nivan
 */
public class ScheduleController extends BaseController {

    TripBP tripBP;
    LoginBP loginBP;
    SchedulerBP schedulerBP;
    ReportBP reportBP;
    OperationBP operationBP;
    SalesBP salesBP;

    public SalesBP getSalesBP() {
        return salesBP;
    }

    public void setSalesBP(SalesBP salesBP) {
        this.salesBP = salesBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public SchedulerBP getSchedulerBP() {
        return schedulerBP;
    }

    public void setSchedulerBP(SchedulerBP schedulerBP) {
        this.schedulerBP = schedulerBP;
    }

    public ReportBP getReportBP() {
        return reportBP;
    }

    public void setReportBP(ReportBP reportBP) {
        this.reportBP = reportBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        ////System.out.println("request.getRequestURI() = " + request.getRequestURI());

    }
    public static String applicationServer;
    public static String serverIpAddress;
    int runHour = 0;
    int runMinute = 0;

    public void executeScheduler() throws FPRuntimeException, FPBusinessException {
//         printing current system time
        try {
            System.out.println("Current Time : " + Calendar.getInstance().getTime());
            boolean executionStatus = false;
            ThrottleConstants.PROPS_VALUES = loadProps();
            int smsSchedulerStatus = Integer.parseInt(ThrottleConstants.smsSchedulerStatus);
            int gpsSchedulerStatus = 1;

            System.out.println("Current Time : " + Calendar.getInstance().getTime());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
            SimpleDateFormat sdfNew1 = new SimpleDateFormat("yyyy-MM-dd");
            Calendar c = Calendar.getInstance();
            Date date = new Date();
            c.setTime(date);
            String asOnTime = sdfNew.format(c.getTime()).toString();
            String systemTime = sdf.format(c.getTime()).toString();
            String fromDate = sdfNew.format(c.getTime()).toString();
            String scheduleDate = sdfNew1.format(c.getTime()).toString();
            String[] dateTemp = systemTime.split(" ");
            String[] timeTemp = dateTemp[1].split(":");
            runHour = Integer.parseInt(timeTemp[0]);
            runMinute = Integer.parseInt(timeTemp[1]);

            ThrottleConstants.PROPS_VALUES = loadProps();

            smsSchedulerStatus = Integer.parseInt(ThrottleConstants.smsSchedulerStatus);
            //gpsSchedulerStatus = Integer.parseInt(ThrottleConstants.gpsSchedulerStatus);
            serverIpAddress = InetAddress.getLocalHost().getHostAddress();
            ServletContext servletContext = this.getServletContext();
            String path = servletContext.getRealPath(File.separator);
            System.out.println("path = " + path);
            System.out.println("serverIpAddress = " + serverIpAddress);
            String[] pathSplit = null;
            /*
             try {
             path = path.replace("\\", "/");
             System.out.println("path = " + path);
             pathSplit = path.split("/");
             System.out.println("pathSplit = " + pathSplit[3]);
             applicationServer = pathSplit[3];
             } catch (Exception ex) {
             ex.printStackTrace();
             }
             */

            // Gps Main Method start
//            gpsActivity gps =new gpsActivity();
//            gps.gpsData();
            // Gps End   
            System.out.println("throttle constant dvmTime...." + ThrottleConstants.PROPS_VALUES.get("dvmTime"));
            System.out.println("throttle constant key dvmTime...." + ThrottleConstants.PROPS_VALUES.containsKey("dvmTime"));
            System.out.println("smsSchedulerStatus:" + smsSchedulerStatus);
            System.out.println("am here....");

            String dailyAlertTime = ThrottleConstants.dailyProcessTime;
            String[] daTimeTemp = dailyAlertTime.split(":");
            System.out.println("runHour:" + runHour);
            ReportTO report = new ReportTO();
            System.out.println("Before...########%%%%%%%##########TTTTTTTTTTTTT." + smsSchedulerStatus);

            if (smsSchedulerStatus == 1) {

                HttpServletRequest request = null;
                HttpServletResponse response = null;
                try {
                    dailyBookingReportAlert(request, response);
                } catch (Exception exp) {
                    exp.printStackTrace();
                }
                try {
                    dailyBookingReport2Alert(request, response);
                } catch (Exception exp) {
                    exp.printStackTrace();
                }
                try {
                    companyWiseTripClosure(request, response);
                } catch (Exception exp) {
                    exp.printStackTrace();
                }

                // fastTag ntegration starts here
                System.out.println("daTimeTemp[0]:" + Integer.parseInt(daTimeTemp[0]));
                System.out.println("daTimeTemp[1]:" + Integer.parseInt(daTimeTemp[1]));

                // fastTag ntegration starts here
                report.setUserName(ThrottleConstants.fastTagEmailId);
                report.setId(ThrottleConstants.fastTagPassword);
                System.out.println("ONNNNNN" + runMinute);
                if (runMinute == 85) {
//                if (runMinute % 5 == 0) {
//                if (runMinute == 10 || runMinute == 20 || runMinute == 30 || runMinute == 40 || runMinute == 50 || runMinute == 00) {

                    System.out.println("ThrottleConstants.happayId : " + ThrottleConstants.fastTagEmailId);
                    System.out.println("ThrottleConstants.fastagPassword : " + ThrottleConstants.fastTagPassword);

                    int happay = reportBP.readFastTagMailInbox(report);
                    System.out.println("happay : " + happay);
                    //                        fasttag expense insert
//                    int status = reportBP.insertFastTagExpense(report);
//                    System.out.println("happay insertFastTagExpense: " + status);
                    //                        fasttag expense insert
                }
                // fastTag integration ends here
                
                  if (runHour == 85) {
//                if ((runHour == Integer.parseInt("06") && runMinute == Integer.parseInt("00"))) {

//                 Vehicle Daily status report New concept
                    int vehicleStatus = reportBP.insertVehicleStatusDetailsList(report);

//                ArrayList vehicleStatusDetailsList = new ArrayList();
//                vehicleStatusDetailsList = reportBP.getVehicleStatusDetailsList(report, 1403);
                    System.out.println("vehicleDetailsList::" + vehicleStatus);
                    if (vehicleStatus > 0) {
                        System.out.println("Status insert successful.......!!!");
                    } else {
                        System.out.println("Status insert failed.......!!!");
                    }

                    asOnTime = runHour + ":" + runMinute;
                    ////truncate ts_mail_master
                    ReportTO repTO = new ReportTO();
                    ReportTO repTO1 = new ReportTO();
                    ReportTO repTO2 = new ReportTO();
                    ReportTO repTO3 = new ReportTO();
                    ArrayList pnrList = new ArrayList();
                    OperationTO operationTO = new OperationTO();
                    SalesTO salesTO = new SalesTO();
                    Iterator itr = null;
                    Iterator itr1 = null;
                    Iterator itr2 = null;
                    Iterator itr3 = null;
                    Iterator itr4 = null;
                    String totalContent = "";
                    String rowContent = "";
                    String totalContent1 = "";
                    String rowContent1 = "";
                    String totalContent2 = "";
                    String rowContent2 = "";
                    String totalContent3 = "";
                    String rowContent3 = "";
                    String totalContent4 = "";
                    String rowContent4 = "";
                    Iterator itr5 = null;
                    Iterator itr6 = null;

                    String totalContent5 = "";
                    String rowContent5 = "";
                    String totalContent6 = "";
                    String rowContent6 = "";

                    String movementType = "0";

                    //Booking Movements Validity Starts here by Muthu++++++++++++++
                    repTO = new ReportTO();
                    pnrList = new ArrayList();
                    operationTO = new OperationTO();

                    System.out.println("getConsignmentViewList+++++");
                    ArrayList consignmentList = operationBP.getConsignmentViewListForEmail(operationTO);
                    System.out.println("consignmentList " + consignmentList.size());

                    ArrayList consignmentListSummary = operationBP.getConsignmentListSummaryForEmail(operationTO);
                    System.out.println("consignmentListSummary: " + consignmentListSummary.size());

                    salesTO = new SalesTO();
                    itr = null;
                    itr = consignmentList.iterator();
                    totalContent = "";
                    rowContent = "";
                    int total = 0;
                    int total20 = 0;
                    int total40 = 0;
                    System.out.println("consignmentList srini size:" + consignmentList.size());
                    while (itr.hasNext()) {
                        System.out.println("srini 000000000000");
                        operationTO = (OperationTO) itr.next();
                        rowContent = "";
                        rowContent = "<tr>";
//                        rowContent = rowContent + "<td>" + operationTO.getOrderReferenceNo() + "-" + operationTO.getCustomerName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getOrderReferenceNo() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getCustomerName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getMovementType() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getCreatedOn() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getRouteName() + "</td>";
                        rowContent = rowContent + "<td>20: " + operationTO.getContainerTypeName() + "<br>40: " + operationTO.getContainerTypeNameForty() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getConWeight() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getDesc() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getTripScheduleDate() + "</td>";
                        total++;
                        rowContent = rowContent + "</tr>";

                        totalContent = totalContent + rowContent;

                    }
                    System.out.println("totalContenttotalContent::%%%%::" + totalContent);

                    itr = consignmentListSummary.iterator();
                    String reportDate = "";
                    String totalOrders = "";
                    String total20Orders = "";
                    String total40Orders = "";

                    while (itr.hasNext()) {
                        System.out.println("srini 111111111");
                        operationTO = (OperationTO) itr.next();
                        reportDate = operationTO.getDate();
                        totalOrders = operationTO.getTotalOrders();
                        total20Orders = operationTO.getTwentyFeetTotal();
                        total40Orders = operationTO.getFortyFeetTotal();
                    }
                    String mailTemplate = "";
                    mailTemplate = salesBP.getMailTemplate("11", null);
                    mailTemplate = mailTemplate.replaceAll("RECEIVEDDATE", reportDate);
                    mailTemplate = mailTemplate.replaceAll("NETTTOTAL", totalOrders + " ");
                    mailTemplate = mailTemplate.replaceAll("20TOTAL", total20Orders + " ");
                    mailTemplate = mailTemplate.replaceAll("40TOTAL", total40Orders + " ");
                    mailTemplate = mailTemplate.replaceAll("movementList", totalContent);

                    System.out.println("mailTemplate" + mailTemplate);
                    SalesTO salesTO2 = new SalesTO();
                    salesTO2.setMailTypeId("11");
                    salesTO2.setMailSubjectTo("Bookings / Movements as on " + reportDate + " " + asOnTime + " Hrs");
                    salesTO2.setMailSubjectCc("Bookings / Movements  as on " + reportDate + " " + asOnTime + " Hrs");
                    salesTO2.setMailSubjectBcc("Bookings / Movements  as on " + reportDate + " " + asOnTime + " Hrs");
                    salesTO2.setMailContentTo(mailTemplate);
                    salesTO2.setMailContentCc(mailTemplate);
                    salesTO2.setMailContentBcc(mailTemplate);

                    salesTO2.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                    salesTO2.setMailIdCc(ThrottleConstants.pnrUploadCcMailId);
                    salesTO2.setMailIdBcc(ThrottleConstants.pnrUploadBccMailId);

                    int insertStatus = salesBP.insertMailDetails(salesTO2, 1403, null);
                    //TVS Movements Validity Ends here by Muthu...............

                    String dayOfWeek = "";
                    dayOfWeek = salesBP.getDayOfWeek();
                    System.out.println("dayOfWeek ::" + dayOfWeek);
                    if ("1".equals(dayOfWeek)) {
                        //Vehicle Permit Starts here by Muthu++++++++++++++
                        repTO = new ReportTO();
                        repTO.setDueIn("30");
                        System.out.println("getVehiclePermitDues+++++");
                        repTO.setCompanyId("1461");
                        ArrayList vehiclePermitDueList = reportBP.processPermitDueList(repTO);
                        System.out.println("vehiclePermitDueList " + vehiclePermitDueList.size());

                        itr1 = null;
                        itr1 = vehiclePermitDueList.iterator();
                        totalContent1 = "";
                        rowContent1 = "";
                        int ams = 1;
                        System.out.println("vehiclePermitDueList size:" + vehiclePermitDueList.size());
                        while (itr1.hasNext()) {
                            System.out.println("vehiclePermitDueList iterator");
                            repTO = (ReportTO) itr1.next();
                            rowContent1 = "";
                            rowContent1 = "<tr>";

                            rowContent1 = rowContent1 + "<td>" + ams + "</td>";
                            rowContent1 = rowContent1 + "<td>" + repTO.getRegNo() + "</td>";
                            rowContent1 = rowContent1 + "<td>" + repTO.getCompanyName() + "</td>";
                            rowContent1 = rowContent1 + "<td>" + repTO.getNextFc() + "</td>";
                            rowContent1 = rowContent1 + "<td>" + repTO.getPermitType() + "</td>";
                            rowContent1 = rowContent1 + "<td>" + repTO.getDue() + "</td>";

                            ams = ams + 1;
                            rowContent1 = rowContent1 + "</tr>";
                            totalContent1 = totalContent1 + rowContent1;

                        }
                        System.out.println("vehiclePermitDueList::%%%%::" + totalContent1);

                        String mailTemplate1 = "";
                        mailTemplate1 = salesBP.getMailTemplate("13", null);
                        mailTemplate1 = mailTemplate1.replaceAll("PermitExpList", totalContent1);

                        System.out.println("mailTemplate1" + mailTemplate1);
                        SalesTO salesTO3 = new SalesTO();
                        salesTO3.setMailTypeId("13");
                        salesTO3.setMailSubjectTo("Vehicle Permit Dues as on " + reportDate);
                        salesTO3.setMailSubjectCc("Vehicle Permit Dues as on " + reportDate);
                        salesTO3.setMailSubjectBcc("Vehicle Permit Dues as on " + reportDate);
                        salesTO3.setMailContentTo(mailTemplate1);
                        salesTO3.setMailContentCc(mailTemplate1);
                        salesTO3.setMailContentBcc(mailTemplate1);
                        System.out.println("hererere");
                        salesTO3.setMailIdTo(ThrottleConstants.vehicleDuesToMailId);
                        salesTO3.setMailIdCc(ThrottleConstants.vehicleDuesCcMailId);
                        salesTO3.setMailIdBcc(ThrottleConstants.vehicleDuesBccMailId);

                        insertStatus = salesBP.insertMailDetails(salesTO3, 1403, null);
                        System.out.println("insertStatus 1 :::" + insertStatus);
                        //Vehicle Permit Dues Ends here by Muthu...............

                        //Vehicle FC Starts here by Muthu++++++++++++++
                        repTO1 = new ReportTO();
                        repTO1.setDueIn("30");
                        System.out.println("getVehicleFCDues+++++");
                        repTO1.setCompanyId("1461");
                        ArrayList vehicleFCDueList = reportBP.processDueList(repTO1);
                        System.out.println("vehicleFCDueList " + vehicleFCDueList.size());

                        itr2 = null;
                        itr2 = vehicleFCDueList.iterator();
                        totalContent2 = "";
                        rowContent2 = "";
                        int sNo = 1;
                        System.out.println("vehicleFCDueList size:" + vehicleFCDueList.size());
                        while (itr2.hasNext()) {
                            System.out.println("vehicleFCDueList iterator");
                            repTO1 = (ReportTO) itr2.next();
                            rowContent2 = "";
                            rowContent2 = "<tr>";

                            rowContent2 = rowContent2 + "<td>" + sNo + "</td>";
                            rowContent2 = rowContent2 + "<td>" + repTO1.getRegNo() + "</td>";
                            rowContent2 = rowContent2 + "<td>" + repTO1.getCompanyName() + "</td>";
                            rowContent2 = rowContent2 + "<td>" + repTO1.getNextFc() + "</td>";
                            rowContent2 = rowContent2 + "<td>" + repTO1.getDue() + "</td>";

                            sNo = sNo + 1;
                            rowContent2 = rowContent2 + "</tr>";
                            totalContent2 = totalContent2 + rowContent2;

                        }
                        System.out.println("vehicleFCDueList::%%%%::" + totalContent2);

                        String mailTemplate2 = "";
                        mailTemplate2 = salesBP.getMailTemplate("14", null);
                        mailTemplate2 = mailTemplate2.replaceAll("FCExpList", totalContent2);

                        System.out.println("mailTemplate2" + mailTemplate2);
                        SalesTO salesTO4 = new SalesTO();
                        salesTO4.setMailTypeId("14");
                        salesTO4.setMailSubjectTo("Vehicle FC Dues as on " + reportDate);
                        salesTO4.setMailSubjectCc("Vehicle FC Dues as on " + reportDate);
                        salesTO4.setMailSubjectBcc("Vehicle FC Dues as on " + reportDate);
                        salesTO4.setMailContentTo(mailTemplate2);
                        salesTO4.setMailContentCc(mailTemplate2);
                        salesTO4.setMailContentBcc(mailTemplate2);
                        System.out.println("klklklk");
                        salesTO4.setMailIdTo(ThrottleConstants.vehicleDuesToMailId);
                        salesTO4.setMailIdCc(ThrottleConstants.vehicleDuesCcMailId);
                        salesTO4.setMailIdBcc(ThrottleConstants.vehicleDuesBccMailId);

                        insertStatus = salesBP.insertMailDetails(salesTO4, 1403, null);
                        System.out.println("insertStatus 22 :::" + insertStatus);
                        //Vehicle FC Dues Ends here by Muthu...............

                        //Vehicle Insurance Starts here by Muthu++++++++++++++
                        repTO2 = new ReportTO();
                        repTO2.setDueIn("30");
                        repTO2.setCompanyId("1461");
                        System.out.println("getVehicleInsuranceDues+++++");
                        ArrayList vehicleInsuranceDueList = reportBP.processInsuranceDueList(repTO2);
                        System.out.println("vehicleInsuranceDueList " + vehicleInsuranceDueList.size());

                        itr3 = null;
                        itr3 = vehicleInsuranceDueList.iterator();
                        totalContent3 = "";
                        rowContent3 = "";
                        int sNos = 1;
                        System.out.println("vehicleInsuranceDueList size:" + vehicleInsuranceDueList.size());
                        while (itr3.hasNext()) {
                            System.out.println("vehicleInsuranceDueList iterator");
                            repTO2 = (ReportTO) itr3.next();
                            rowContent3 = "";
                            rowContent3 = "<tr>";

                            rowContent3 = rowContent3 + "<td>" + sNos + "</td>";
                            rowContent3 = rowContent3 + "<td>" + repTO2.getRegNo() + "</td>";
                            rowContent3 = rowContent3 + "<td>" + repTO2.getCompanyName() + "</td>";
                            rowContent3 = rowContent3 + "<td>" + repTO2.getNextFc() + "</td>";
                            rowContent3 = rowContent3 + "<td>" + repTO2.getDue() + "</td>";

                            sNos = sNos + 1;
                            rowContent3 = rowContent3 + "</tr>";
                            totalContent3 = totalContent3 + rowContent3;

                        }
                        System.out.println("vehicleInsuranceDueList::%%%%::" + totalContent3);

                        String mailTemplate3 = "";
                        mailTemplate3 = salesBP.getMailTemplate("15", null);
                        mailTemplate3 = mailTemplate3.replaceAll("InsuranceExpList", totalContent3);

                        System.out.println("mailTemplate3" + mailTemplate3);
                        SalesTO salesTO5 = new SalesTO();
                        salesTO5.setMailTypeId("15");
                        salesTO5.setMailSubjectTo("Vehicle Insurance Dues as on " + reportDate);
                        salesTO5.setMailSubjectCc("Vehicle Insurance Dues as on " + reportDate);
                        salesTO5.setMailSubjectBcc("Vehicle Insurance Dues as on " + reportDate);
                        salesTO5.setMailContentTo(mailTemplate3);
                        salesTO5.setMailContentCc(mailTemplate3);
                        salesTO5.setMailContentBcc(mailTemplate3);
                        System.out.println("aaammm");
                        salesTO5.setMailIdTo(ThrottleConstants.vehicleDuesToMailId);
                        salesTO5.setMailIdCc(ThrottleConstants.vehicleDuesCcMailId);
                        salesTO5.setMailIdBcc(ThrottleConstants.vehicleDuesBccMailId);

                        insertStatus = salesBP.insertMailDetails(salesTO5, 1403, null);
                        System.out.println("insertStatus 33 :::" + insertStatus);
                        //Vehicle Insurance Dues Ends here by Muthu...............

                        //Vehicle RoadTax Starts here by Muthu++++++++++++++
                        repTO3 = new ReportTO();
                        repTO3.setDueIn("30");
                        repTO3.setCompanyId("1461");
                        System.out.println("getVehicleRoadTaxDues+++++");
                        ArrayList vehicleRoadTaxDueList = reportBP.processRoadTaxDueList(repTO3);
                        System.out.println("vehicleRoadTaxDueList " + vehicleRoadTaxDueList.size());

                        itr4 = null;
                        itr4 = vehicleRoadTaxDueList.iterator();
                        totalContent4 = "";
                        rowContent4 = "";
                        int serialNos = 1;
                        System.out.println("vehicleRoadTaxDueList size:" + vehicleRoadTaxDueList.size());
                        while (itr4.hasNext()) {
                            System.out.println("vehicleRoadTaxDueList iterator");
                            repTO3 = (ReportTO) itr4.next();
                            rowContent4 = "";
                            rowContent4 = "<tr>";

                            rowContent4 = rowContent4 + "<td>" + serialNos + "</td>";
                            rowContent4 = rowContent4 + "<td>" + repTO3.getRegNo() + "</td>";
                            rowContent4 = rowContent4 + "<td>" + repTO3.getCompanyName() + "</td>";
                            rowContent4 = rowContent4 + "<td>" + repTO3.getNextFc() + "</td>";
                            rowContent4 = rowContent4 + "<td>" + repTO3.getDue() + "</td>";

                            serialNos = serialNos + 1;
                            rowContent4 = rowContent4 + "</tr>";
                            totalContent4 = totalContent4 + rowContent4;

                        }
                        System.out.println("vehicleRoadTaxDueList::%%%%::" + totalContent4);

                        String mailTemplate4 = "";
                        mailTemplate4 = salesBP.getMailTemplate("16", null);
                        mailTemplate4 = mailTemplate4.replaceAll("InsuranceExpList", totalContent4);

                        System.out.println("mailTemplate4" + mailTemplate4);
                        SalesTO salesTO6 = new SalesTO();
                        salesTO6.setMailTypeId("16");
                        salesTO6.setMailSubjectTo("Vehicle Road Tax Dues as on " + reportDate);
                        salesTO6.setMailSubjectCc("Vehicle Road Tax Dues as on " + reportDate);
                        salesTO6.setMailSubjectBcc("Vehicle Road Tax Dues as on " + reportDate);
                        salesTO6.setMailContentTo(mailTemplate4);
                        salesTO6.setMailContentCc(mailTemplate4);
                        salesTO6.setMailContentBcc(mailTemplate4);
//                        System.out.println("mailTemplate4");
                        salesTO6.setMailIdTo(ThrottleConstants.vehicleDuesToMailId);
                        salesTO6.setMailIdCc(ThrottleConstants.vehicleDuesCcMailId);
                        salesTO6.setMailIdBcc(ThrottleConstants.vehicleDuesBccMailId);
                        if (vehicleRoadTaxDueList.size() > 0) {
                            insertStatus = salesBP.insertMailDetails(salesTO6, 1403, null);
                            System.out.println("mailTemplate4 33 :::" + insertStatus);
                        }
                        //Vehicle Insurance Dues Ends here by Muthu...............
                    }

                    String asOnTimeNew = sdf1.format(c.getTime()).toString();
                    sdfNew = new SimpleDateFormat("dd-MM-yyyy");
                    c.add(Calendar.DATE, -1);
                    String fromDates = sdfNew.format(c.getTime()).toString();

                    ArrayList completedJobcards = reportBP.completedJobcards(repTO);
                    ArrayList activeJobcards = reportBP.activeJobcards(repTO);

                    System.out.println("completedJobcards " + completedJobcards.size());
                    System.out.println("activeJobcards " + activeJobcards.size());

                    itr5 = null;
                    itr5 = completedJobcards.iterator();

                    int inProgress = 0;
                    int notOpen = 0;
                    while (itr5.hasNext()) {
                        repTO = new ReportTO();
                        repTO = (ReportTO) itr5.next();
                        rowContent5 = "";
                        rowContent5 = "<tr>";

                        rowContent5 = rowContent5 + "<td>" + repTO.getRegNo() + "</td>";
                        rowContent5 = rowContent5 + "<td>" + repTO.getModelName() + "</td>";
                        rowContent5 = rowContent5 + "<td>" + repTO.getServiceTypeName() + "</td>";
                        rowContent5 = rowContent5 + "<td>" + repTO.getSchedularDateTime() + "</td>";
                        rowContent5 = rowContent5 + "<td>" + repTO.getJobCardNo() + "</td>";
                        rowContent5 = rowContent5 + "<td>" + repTO.getKmReading() + "</td>";
                        rowContent5 = rowContent5 + "<td>" + repTO.getStatus() + "</td>";

                        rowContent5 = rowContent5 + "</tr>";
                        totalContent5 = totalContent5 + rowContent5;

                    }

                    itr6 = null;
                    itr6 = activeJobcards.iterator();

                    while (itr6.hasNext()) {
                        repTO = new ReportTO();
                        repTO = (ReportTO) itr6.next();
                        if ("NotPlanned".equals(repTO.getStatus())) {
                            notOpen = notOpen + 1;
                        } else if ("Planned".equals(repTO.getStatus())) {
                            inProgress = inProgress + 1;
                        }
                        rowContent6 = "";
                        rowContent6 = "<tr>";

                        rowContent6 = rowContent6 + "<td>" + repTO.getRegNo() + "</td>";
                        rowContent6 = rowContent6 + "<td>" + repTO.getModelName() + "</td>";
                        rowContent6 = rowContent6 + "<td>" + repTO.getServiceTypeName() + "</td>";
                        rowContent6 = rowContent6 + "<td>" + repTO.getSchedularDateTime() + "</td>";
                        rowContent6 = rowContent6 + "<td>" + repTO.getJobCardNo() + "</td>";
                        rowContent6 = rowContent6 + "<td>" + repTO.getKmReading() + "</td>";
                        rowContent6 = rowContent6 + "<td>" + repTO.getStatus() + "</td>";
                        rowContent6 = rowContent6 + "<td>" + repTO.getAge() + "</td>";

                        rowContent6 = rowContent6 + "</tr>";
                        totalContent6 = totalContent6 + rowContent6;

                    }
                    String mailTemplate6 = "";
                    mailTemplate6 = salesBP.getMailTemplate("19", null);
                    mailTemplate6 = mailTemplate6.replaceAll("YESTERDAYCOMPLETEDJOBS", completedJobcards.size() + "");
                    mailTemplate6 = mailTemplate6.replaceAll("completedList", totalContent5);
                    mailTemplate6 = mailTemplate6.replaceAll("TOTALJOBS", activeJobcards.size() + "");
                    mailTemplate6 = mailTemplate6.replaceAll("ACTIVEJOBS", inProgress + "");
                    mailTemplate6 = mailTemplate6.replaceAll("NOTSTARTEDJOBS", notOpen + "");
                    mailTemplate6 = mailTemplate6.replaceAll("jobcardList", totalContent6);
                    mailTemplate6 = mailTemplate6.replaceAll("DTIME", asOnTimeNew);
                    mailTemplate6 = mailTemplate6.replaceAll("YESTERDAYDATE", fromDates);

                    SalesTO salesTO3 = new SalesTO();
                    salesTO3.setMailTypeId("13");
                    salesTO3.setMailSubjectTo("JobCard Alerts as on " + fromDate);
                    salesTO3.setMailSubjectCc("JobCard Alerts as on   " + fromDate);
                    salesTO3.setMailSubjectBcc("JobCard Alerts as on  " + fromDate);
                    salesTO3.setMailContentTo(mailTemplate6);
                    salesTO3.setMailContentCc(mailTemplate6);
                    salesTO3.setMailContentBcc(mailTemplate6);
                    System.out.println("hererere");
                    salesTO3.setMailIdTo(ThrottleConstants.vehicleDuesToMailId);
                    salesTO3.setMailIdCc(ThrottleConstants.vehicleDuesCcMailId);
                    salesTO3.setMailIdBcc(ThrottleConstants.vehicleDuesBccMailId);

                    int insertStatus6 = salesBP.insertMailDetails(salesTO3, 1403, null);
                    System.out.println("insertStatus 1 :::" + insertStatus6);

                }

                //moving towards clearance start by Srini
                System.out.println("movingToClearanceRunTime::::::::::::::");
                String movingToClearanceRunTime = ThrottleConstants.movingToClearanceRunTime;
                daTimeTemp = movingToClearanceRunTime.split(":");
                System.out.println("runHour:" + runHour);
                System.out.println("runMinute:" + runMinute);
                System.out.println("daTimeTemp[0]:" + Integer.parseInt(daTimeTemp[0]));
                System.out.println("daTimeTemp[1]:" + Integer.parseInt(daTimeTemp[1]));
//                if ((runHour == Integer.parseInt("19") && runMinute == Integer.parseInt("00"))
//                        || (runHour == Integer.parseInt("21") && runMinute == Integer.parseInt("00"))
//                        || (runHour == Integer.parseInt("05") && runMinute == Integer.parseInt("30"))) {
                if (runHour == Integer.parseInt("85")) {
                    System.out.println("getConsignmentViewList+++++");
                    movingToClearanceRunTime = runHour + ":" + runMinute;
                    ArrayList tripList = reportBP.getTripsMovingTowardsClearance();
                    System.out.println("tripList " + tripList.size());

                    SalesTO salesTO = new SalesTO();
                    ReportTO reportTO = new ReportTO();
                    Iterator itr = null;
                    itr = tripList.iterator();
                    String totalContent = "";
                    String rowContent = "";
                    System.out.println("getTripsMovingTowardsClearance srini size:" + tripList.size());
                    while (itr.hasNext()) {
                        reportTO = (ReportTO) itr.next();
                        rowContent = "";
                        rowContent = "<tr>";
                        rowContent = rowContent + "<td>" + reportTO.getOrderReferenceNo() + "</td>";
                        rowContent = rowContent + "<td>" + reportTO.getVehicleNo() + "</td>";
                        rowContent = rowContent + "<td>" + reportTO.getCustomerName() + "</td>";
                        rowContent = rowContent + "<td>" + reportTO.getContainerTypeName() + "</td>";
                        rowContent = rowContent + "<td>" + reportTO.getContainerNo() + "</td>";
                        rowContent = rowContent + "<td>" + reportTO.getRouteName() + "</td>";
//                        rowContent = rowContent + "<td>" + reportTO.getDesc() + "</td>";
                        rowContent = rowContent + "<td>" + reportTO.getFactoryOutDate() + "</td>";
                        rowContent = rowContent + "<td>" + reportTO.getRemarks() + "</td>";
//                        total++;
                        rowContent = rowContent + "</tr>";

                        totalContent = totalContent + rowContent;

                    }
                    System.out.println("totalContentmovingToClearanceRunTime" + totalContent);

                    String mailTemplate = "";
                    if (tripList.size() > 0) {
                        mailTemplate = salesBP.getMailTemplate("18", null);
                        mailTemplate = mailTemplate.replaceAll("DTIME", asOnTime + " " + movingToClearanceRunTime);

                        mailTemplate = mailTemplate.replaceAll(" movingTowardsToClearance", totalContent);

                        System.out.println("mailTemplate" + mailTemplate);
                        SalesTO salesTO2 = new SalesTO();
                        salesTO2.setMailTypeId("18");
                        salesTO2.setMailSubjectTo("Vehicle Moving for Clearance as on " + asOnTime + " " + movingToClearanceRunTime + "Hrs");
                        salesTO2.setMailSubjectCc("Vehicle Moving for Clearance as on " + asOnTime + " " + movingToClearanceRunTime + "Hrs");
                        salesTO2.setMailSubjectBcc("Vehicle Moving for Clearance as on " + asOnTime + " " + movingToClearanceRunTime + "Hrs");
                        salesTO2.setMailContentTo(mailTemplate);
                        salesTO2.setMailContentCc(mailTemplate);
                        salesTO2.setMailContentBcc(mailTemplate);

                        salesTO2.setMailIdTo(ThrottleConstants.vehicleMovingToClearanceToMailId);
                        salesTO2.setMailIdCc(ThrottleConstants.vehicleMovingToClearanceCcMailId);
                        salesTO2.setMailIdBcc(ThrottleConstants.vehicleMovingToClearanceBccMailId);

                        int insertStatus = salesBP.insertMailDetails(salesTO2, 1403, null);
                        System.out.println("insertStatus:::" + insertStatus);
                    }
                }
                //moving towards clearance end by Srini

                //wfl wfu report starts here by Srini
                vehicleStatusReportRunTime = ThrottleConstants.vehicleStatusReportRunTime;
                daTimeTemp = vehicleStatusReportRunTime.split(":");
                System.out.println("vehicleStatusReportRunTimevehicleStatusReportRunTimevehicleStatusReportRunTime :::::: runHour:" + runHour);
                System.out.println("runMinute:" + runMinute);
                System.out.println("daTimeTemp[0] new:" + Integer.parseInt(daTimeTemp[0]));
                System.out.println("daTimeTemp[1] new:" + Integer.parseInt(daTimeTemp[1]));
//                if ((runHour == Integer.parseInt("85"))){
                if ((runHour == Integer.parseInt(daTimeTemp[0]) && runMinute == Integer.parseInt(daTimeTemp[1]))){
//                if ((runHour == Integer.parseInt("09") && runMinute == Integer.parseInt("00"))
//                        || (runHour == Integer.parseInt("15") && runMinute == Integer.parseInt("00"))
//                        || (runHour == Integer.parseInt("19") && runMinute == Integer.parseInt("00"))) {
                    String content = "";
                    vehicleStatusReportRunTime = runHour + ":" + runMinute;
                    String mailTemplate4 = "";
                    mailTemplate4 = salesBP.getMailTemplate("17", null);
                    mailTemplate4 = mailTemplate4.replaceAll("DTIME", asOnTime + " " + vehicleStatusReportRunTime);
                    
                    content = reportBP.getVehicleStatusReport(mailTemplate4);
                    System.out.println("content =zzzzzzzzzz ");
                    String[] tempContent = null;
                    SalesTO salesTO6 = new SalesTO();
                    if (!"".equals(content)) {
                        tempContent = content.split("~");
                        mailTemplate4 = tempContent[0];
                        salesTO6.setFileName(tempContent[1]);
                        salesTO6.setFilePath(tempContent[2]);
                    }

//                    System.out.println("mailTemplate4" + mailTemplate4);
                    salesTO6.setMailTypeId("17");

                    salesTO6.setMailSubjectTo("Vehicle Status Report as on " + asOnTime + " " + vehicleStatusReportRunTime + "Hrs");
                    salesTO6.setMailSubjectCc("Vehicle Status Report as on " + asOnTime + " " + vehicleStatusReportRunTime + "Hrs");
                    salesTO6.setMailSubjectBcc("Vehicle Status Report as on " + asOnTime + " " + vehicleStatusReportRunTime + "Hrs");
                    salesTO6.setMailContentTo(mailTemplate4);
                    salesTO6.setMailContentCc(mailTemplate4);
                    salesTO6.setMailContentBcc(mailTemplate4);
//                    System.out.println("mailTemplate4");
                    salesTO6.setMailIdTo(ThrottleConstants.vehicleStatusToMailId);
                    salesTO6.setMailIdCc(ThrottleConstants.vehicleStatusCcMailId);
                    salesTO6.setMailIdBcc(ThrottleConstants.vehicleStatusBccMailId);

                    int insertStatus = salesBP.insertMailDetails(salesTO6, 1403, null);
                    System.out.println("mailTemplate4 33 :::" + insertStatus);

                    //wfl wfu report ends here by Srini
                }
                
                
//                vehicle utilisations report

                vehicleUtilReportRunTime = ThrottleConstants.vehicleUtilReportRunTime;
                daTimeTemp = vehicleUtilReportRunTime.split(":");
                System.out.println("vehicleUtilReportRunTime :::::: runHour:" + runHour);
                System.out.println("runMinute:" + runMinute);
                System.out.println("daTimeTemp[0] new:" + Integer.parseInt(daTimeTemp[0]));
                System.out.println("daTimeTemp[1] new:" + Integer.parseInt(daTimeTemp[1]));
//                if ((runHour == Integer.parseInt("85"))){
                if ((runHour == Integer.parseInt(daTimeTemp[0]) && runMinute == Integer.parseInt(daTimeTemp[1]))){
//                if ((runHour == Integer.parseInt("09") && runMinute == Integer.parseInt("00"))
//                        || (runHour == Integer.parseInt("15") && runMinute == Integer.parseInt("00"))
//                        || (runHour == Integer.parseInt("19") && runMinute == Integer.parseInt("00"))) {
                    String content = "";
                    vehicleUtilReportRunTime = runHour + ":" + runMinute;
                    String mailTemplate4 = "";
//                    mailTemplate4 = salesBP.getMailTemplate("18", null);
//                    mailTemplate4 = mailTemplate4.replaceAll("DTIME", asOnTime + " " + vehicleUtilReportRunTime);
                   
                   content = reportBP.getVehicleUtilReport();
                    System.out.println("content =zzzzzzzzzz ");
                    String[] tempContent = null;
                    SalesTO salesTO6 = new SalesTO();
                    if (!"".equals(content)) {
                        tempContent = content.split("~");
                        mailTemplate4 = tempContent[0];
                        salesTO6.setFileName(tempContent[1]);
                        salesTO6.setFilePath(tempContent[2]);
                    }

//                    System.out.println("mailTemplate4" + mailTemplate4);
                    salesTO6.setMailTypeId("18");

                    salesTO6.setMailSubjectTo("vehicleUtilReport as on " + asOnTime + " " + vehicleUtilReportRunTime + "Hrs");
                    salesTO6.setMailSubjectCc("vehicleUtilReport as on " + asOnTime + " " + vehicleUtilReportRunTime + "Hrs");
                    salesTO6.setMailSubjectBcc("vehicleUtilReport as on " + asOnTime + " " + vehicleUtilReportRunTime + "Hrs");
                    salesTO6.setMailContentTo(mailTemplate4);
                    salesTO6.setMailContentCc(mailTemplate4);
                    salesTO6.setMailContentBcc(mailTemplate4);
//                    System.out.println("mailTemplate4");
                    salesTO6.setMailIdTo(ThrottleConstants.vehicleUtilToMailId);
                    salesTO6.setMailIdCc(ThrottleConstants.vehicleUtilCcMailId);
                    salesTO6.setMailIdBcc(ThrottleConstants.vehicleUtilBccMailId);

                    int insertStatus = salesBP.insertMailDetails(salesTO6, 1403, null);
                    System.out.println("mailTemplate4 33 :::" + insertStatus);

                }
                
                
                
//                Weekly Prod report

                weeklyProdReportRunTime = ThrottleConstants.weeklyProdReportRunTime;
                daTimeTemp = weeklyProdReportRunTime.split(":");
                System.out.println("weeklyProdReportRunTime:::::: runHour:" + runHour);
                System.out.println("runMinute:" + runMinute);
                System.out.println("daTimeTemp[0] new:" + Integer.parseInt(daTimeTemp[0]));
                System.out.println("daTimeTemp[1] new:" + Integer.parseInt(daTimeTemp[1]));
//                if ((runHour == Integer.parseInt("85"))){
                if ((runHour == Integer.parseInt(daTimeTemp[0]) && runMinute == Integer.parseInt(daTimeTemp[1]))){
//                if ((runHour == Integer.parseInt("09") && runMinute == Integer.parseInt("00"))
//                        || (runHour == Integer.parseInt("15") && runMinute == Integer.parseInt("00"))
//                        || (runHour == Integer.parseInt("19") && runMinute == Integer.parseInt("00"))) {
                    String content = "";
                    weeklyProdReportRunTime = runHour + ":" + runMinute;
                    String mailTemplate4 = "";
//                    mailTemplate4 = salesBP.getMailTemplate("18", null);
//                    mailTemplate4 = mailTemplate4.replaceAll("DTIME", asOnTime + " " + weeklyProdReportRunTime);
                   
                   content = reportBP.getVehicleWeeklyProductivityAlerts();
                    System.out.println("content =zzzzzzzzzz ");
                    String[] tempContent = null;
                    SalesTO salesTO6 = new SalesTO();
                    if (!"".equals(content)) {
                        tempContent = content.split("~");
                        mailTemplate4 = tempContent[0];
                        salesTO6.setFileName(tempContent[1]);
                        salesTO6.setFilePath(tempContent[2]);
                    }

//                    System.out.println("mailTemplate4" + mailTemplate4);
                    salesTO6.setMailTypeId("19");

                    salesTO6.setMailSubjectTo("weeklyProdReport as on " + asOnTime + " " + weeklyProdReportRunTime + "Hrs");
                    salesTO6.setMailSubjectCc("weeklyProdReport as on " + asOnTime + " " + weeklyProdReportRunTime + "Hrs");
                    salesTO6.setMailSubjectBcc("weeklyProdReport as on " + asOnTime + " " + weeklyProdReportRunTime + "Hrs");
                    salesTO6.setMailContentTo(mailTemplate4);
                    salesTO6.setMailContentCc(mailTemplate4);
                    salesTO6.setMailContentBcc(mailTemplate4);
//                    System.out.println("mailTemplate4");
                    salesTO6.setMailIdTo(ThrottleConstants.weeklyProdToMailId);
                    salesTO6.setMailIdCc(ThrottleConstants.weeklyProdCcMailId);
                    salesTO6.setMailIdBcc(ThrottleConstants.weeklyProdBccMailId);

                    int insertStatus = salesBP.insertMailDetails(salesTO6, 1403, null);
                    System.out.println("mailTemplate4 33 :::" + insertStatus);

                }
                

                ReportTO reportTO = new ReportTO();
                ArrayList emailList = new ArrayList();
                Iterator itr = null;
                try {
                    emailList = reportBP.getEmailList();
                    System.out.println("Arun emailList ------------- " + emailList);
                    System.out.println("Arun  emailList ----------------------- " + emailList.size());
                    itr = emailList.iterator();
                    while (itr.hasNext()) {
                        reportTO = (ReportTO) itr.next();
                        System.out.println("reportTO ----- mail contenet  " + reportTO.getMailContentTo());
                        System.out.println("applicationServer ----------- " + applicationServer);
                        System.out.println("serverIpAddress----------- " + serverIpAddress);
                        reportTO.setServerName(applicationServer);
                        reportTO.setServerIpAddress(serverIpAddress);
                        new EmailSSL(reportTO).start();
                        System.out.println("Arun end ------------- ");
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
//                FPLogUtils.fpErrorLog("Email not delivered --> " + exp.getMessage());
                    ReportTO rTO = new ReportTO();
                    rTO.setMailSubjectTo("Email not delivered");
                    rTO.setMailContentTo(exp.getMessage());
                    rTO.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO = null;
                } finally {
                    emailList = null;
                    itr = null;
                }

                //run gps program every 5 min start
//                if ((runMinute % 5) == 0) {
                if ((runMinute) == 85) {
                    System.out.println("gps running started....!!!............");
                      GpsCurrentLocationTriway.main(null);
                    System.out.println("gps running started.....@@@...........");

//                    TallyTO tally = new TallyTO();
//                    ArrayList dktList = new ArrayList();
//                    Iterator itr6 = null;
//                    try {
//                        dktList = reportBP.dktList();
//                        if (dktList.size() > 0) {
//                            itr6 = dktList.iterator();
//                            while (itr6.hasNext()) {
//                                tally = (TallyTO) itr6.next();
////                                new sqlserviceconn(tally).start();
//                            }
//                        }
//                    } catch (Exception exp) {
//                        exp.printStackTrace();
//                        ReportTO rTO6 = new ReportTO();
//                        rTO6.setMailSubjectTo("tally DKT list of transactionDetails error...");
//                        rTO6.setMailContentTo(exp.getMessage());
//                        rTO6.setMailIdTo("ramprasathk@entitlesolutions.com.muthuv@entitleslutions.com");
//                        rTO6.setMailIdCc("throttleerror@gmail.com");
//                        new EmailSSL(rTO6).start();
//                        rTO6 = null;
//                    } finally {
//                        dktList = null;
//                        itr6 = null;
//                    }
//
//                    TallyTO tally1 = new TallyTO();
//                    ArrayList conList = new ArrayList();
//                    Iterator itr7 = null;
//                    try {
//                        conList = reportBP.conList();
//                        if (conList.size() > 0) {
//                            itr7 = conList.iterator();
//                            while (itr7.hasNext()) {
//                                tally1 = (TallyTO) itr7.next();
////                                new sqlserviceconn1(tally1).start();
//                            }
//                        }
//                    } catch (Exception exp) {
//                        exp.printStackTrace();
//                        ReportTO rTO6 = new ReportTO();
//                        rTO6.setMailSubjectTo("tally Container list of transactionDetails error...");
//                        rTO6.setMailContentTo(exp.getMessage());
//                        rTO6.setMailIdTo("ramprasathk@entitlesolutions.com.muthuv@entitleslutions.com");
//                        rTO6.setMailIdCc("throttleerror@gmail.com");
//                        new EmailSSL(rTO6).start();
//                        rTO6 = null;
//                    } finally {
//                        conList = null;
//                        itr7 = null;
//                    }
//
//                    TallyTO tally2 = new TallyTO();
//                    ArrayList txnList = new ArrayList();
//                    Iterator itr8 = null;
//                    try {
//                        txnList = reportBP.txnList();
//                        if (txnList.size() > 0) {
//                            itr8 = txnList.iterator();
//                            while (itr8.hasNext()) {
//                                tally2 = (TallyTO) itr8.next();
////                                new sqlserviceconn2(tally2).start();
//                            }
//                        }
//                    } catch (Exception exp) {
//                        exp.printStackTrace();
//                        ReportTO rTO6 = new ReportTO();
//                        rTO6.setMailSubjectTo("tally Txn list of transactionDetails error...");
//                        rTO6.setMailContentTo(exp.getMessage());
//                        rTO6.setMailIdTo("ramprasathk@entitlesolutions.com.muthuv@entitleslutions.com");
//                        rTO6.setMailIdCc("throttleerror@gmail.com");
//                        new EmailSSL(rTO6).start();
//                        rTO6 = null;
//                    } finally {
//                        conList = null;
//                        itr8 = null;
//                    }
                }
                //run gps program every 5 min end

                //////////////////////// Control Tower Automation Arun ////////////////////////////////////////////
                System.out.println("tripAutomationList 1: " + runMinute);

//                if (runMinute % 10 == 0) {
                if (runMinute == 85) {
                    System.out.println("tripAutomationList 2: " + runMinute);
                    DprTO reportTO3 = new DprTO();
                    ArrayList tripAutomationList = new ArrayList();
                    Iterator itr3 = null;
                    try {
                        System.out.println("tripAutomationList 3: " + runMinute);
                        tripAutomationList = reportBP.getTripAutomationList();
                        System.out.println("tripAutomationList size : " + tripAutomationList.size());
                        if (tripAutomationList.size() > 0) {
                            itr3 = tripAutomationList.iterator();
                            while (itr3.hasNext()) {
                                reportTO3 = (DprTO) itr3.next();
                                new tripAutomation(reportTO3).start();
                            }
                        }
                    } catch (Exception exp) {
                        exp.printStackTrace();
//                FPLogUtils.fpErrorLog("Email not delivered --> " + exp.getMessage());
//                        ReportTO rTO = new ReportTO();
//                        rTO.setMailSubjectTo("Trip Automation Error");
//                        rTO.setMailContentTo(exp.getMessage());
//                        rTO.setMailIdTo("arun@entitlesolutions.com");
//                        rTO.setMailIdCc("throttleerror@gmail.com");
                        // new EmailSSL(rTO).start();
                        //rTO = null;
                    } finally {
                        tripAutomationList = null;
                        itr3 = null;
                    }

                }

                //////////////////////// Control Tower Automation RAM ////////////////////////////////////////////
            }

            ///////////////////////////////New Reports  /////////////////////////////////
            SimpleDateFormat simpleDateformat = new SimpleDateFormat("E"); // the day of the week abbreviated
            String day = simpleDateformat.format(date);
            simpleDateformat = new SimpleDateFormat("MMM"); // the day of the week abbreviated
            System.out.println("month=" + simpleDateformat.format(date).toUpperCase());
            String month = simpleDateformat.format(date);
            String movementDate = date.getDate() + "-" + month;

            if (runMinute  == 85) {
//            if (runMinute % 5 == 0) {
                int timeSlot = 0;
                int flag = 1;
                System.out.println("timeSlot==" + timeSlot);
                System.out.println("scheduleDate==" + scheduleDate);
                ArrayList movementList = new ArrayList();
                movementList = reportBP.getMovementListWithoutDocument(scheduleDate);
                System.out.println("movementList==" + movementList.size());
                Iterator itr = null;
                itr = movementList.iterator();
                ReportTO reportTO = new ReportTO();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getConsignmentOrderId()==" + reportTO.getConsignmentOrderId());
                    System.out.println("reportTO.getTimeSlot1()==" + reportTO.getTimeSlot1());
                    timeSlot = Integer.parseInt(reportTO.getTimeSlot1());
                    String compId = reportTO.getCompId();
                    new OrderStatusReport(flag, timeSlot, scheduleDate, reportTO.getConsignmentOrderId(), day, movementDate, compId).start();
                }

                flag = 2;
                ArrayList movementList1 = new ArrayList();
                movementList1 = reportBP.getMovementListWithDocument(scheduleDate);
                itr = null;
                itr = movementList1.iterator();
                reportTO = new ReportTO();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getConsignmentOrderId()==" + reportTO.getConsignmentOrderId());
                    System.out.println("reportTO.getTimeSlot1()==" + reportTO.getTimeSlot1());
                    timeSlot = Integer.parseInt(reportTO.getTimeSlot1());
                    String compId = reportTO.getCompId();
                    new OrderStatusReport(flag, timeSlot, scheduleDate, reportTO.getConsignmentOrderId(), day, movementDate, compId).start();
                }
            }
           if (runHour  == 85) {
//            if (runHour == 22 && runMinute == 25) {
                new OrderStatusReport(1, 7, scheduleDate, "", day, movementDate, "1470").start();
            }

            ///////////////////////Trip Details //////////////////////////////////////////
            if (runHour  == 85) {
//            if (runHour == 22 && runMinute == 30) {
                int timeSlot = 0;
                int flag = 1;
                ArrayList tripStartedDetils = new ArrayList();
                tripStartedDetils = reportBP.getTripStartedDetils(scheduleDate);
                ReportTO reportTO = new ReportTO();
                Iterator itr = null;
                itr = tripStartedDetils.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getConsignmentOrderId()==" + reportTO.getConsignmentOrderId());
                    timeSlot = Integer.parseInt(reportTO.getTimeSlot1());
                    String compId = reportTO.getCompId();
                    new OrderStatusReport(flag, timeSlot, scheduleDate, reportTO.getConsignmentOrderId(), day, movementDate, compId).start();
                }                               

            }
            if (runHour  == 85) {
//            if (runHour == 22 && runMinute == 35) {
                new OrderStatusReport(1, 11, scheduleDate, "", day, movementDate, "1470").start();
            }
             if (runHour  == 85) {
//            if (runHour == 22 && runMinute == 40) {
                int flag = 1;
                int count1 = 0;
                int count2 = 0;
                int count3 = 0;
                int count4 = 0;
                int count5 = 0;
                int count6 = 0;

                ArrayList getEmptyPlotInList = new ArrayList();
                getEmptyPlotInList = reportBP.getEmptyPlotInList(scheduleDate);
                ReportTO reportTO = new ReportTO();
                Iterator itr = null;
                itr = getEmptyPlotInList.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getCount()1==" + reportTO.getCount());
                    if ("1".equals(reportTO.getCount())) {
                        count1++;
                    }
                    if ("2".equals(reportTO.getCount())) {
                        count2++;
                    }
                    if ("3".equals(reportTO.getCount())) {
                        count3++;
                    }
                }

                ArrayList getEmptyPlotTimeDiff = new ArrayList();
                getEmptyPlotTimeDiff = reportBP.getEmptyPlotTimeDiff(scheduleDate);
                reportTO = new ReportTO();
                itr = null;
                itr = getEmptyPlotTimeDiff.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getCount()2==" + reportTO.getCount());
                    if ("1".equals(reportTO.getCount())) {
                        count4++;
                    }
                    if ("2".equals(reportTO.getCount())) {
                        count5++;
                    }
                    if ("3".equals(reportTO.getCount())) {
                        count6++;
                    }
                }
                System.out.println("count1=" + count1);
                System.out.println("count2=" + count2);
                System.out.println("count3=" + count3);
                System.out.println("count4=" + count4);
                System.out.println("count5=" + count5);
                System.out.println("count6=" + count6);

                new MovementTypeStatus(flag, scheduleDate, day, count1, count2, count3, count4, count5, count6).start();

                flag = 2;
                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;

                ArrayList getCFSPortInList = new ArrayList();
                getCFSPortInList = reportBP.getCFSPortInList(scheduleDate);
                System.out.println("getCFSPortInList==" + getCFSPortInList.size());
                reportTO = new ReportTO();
                itr = null;
                itr = getCFSPortInList.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getCount()3==" + reportTO.getCount());
                    if ("1".equals(reportTO.getCount())) {
                        count1++;
                    }
                    if ("2".equals(reportTO.getCount())) {
                        count2++;
                    }
                    if ("3".equals(reportTO.getCount())) {
                        count3++;
                    }
                }

                ArrayList getCFSPortInTimeDiff = new ArrayList();
                getCFSPortInTimeDiff = reportBP.getCFSPortInTimeDiff(scheduleDate);
                System.out.println("getCFSPortInTimeDiff==" + getCFSPortInTimeDiff.size());
                reportTO = new ReportTO();
                itr = null;
                itr = getCFSPortInTimeDiff.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getCount()4==" + reportTO.getCount());
                    if ("1".equals(reportTO.getCount())) {
                        count4++;
                    }
                    if ("2".equals(reportTO.getCount())) {
                        count5++;
                    }
                    if ("3".equals(reportTO.getCount())) {
                        count6++;
                    }
                }
                System.out.println("count1=" + count1);
                System.out.println("count2=" + count2);
                System.out.println("count3=" + count3);
                System.out.println("count4=" + count4);
                System.out.println("count5=" + count5);
                System.out.println("count6=" + count6);
                new MovementTypeStatus(flag, scheduleDate, day, count1, count2, count3, count4, count5, count6).start();

                flag = 3;
                count1 = 0;
                count2 = 0;
                count3 = 0;
                count4 = 0;
                count5 = 0;
                count6 = 0;

                ArrayList getFactoryInList = new ArrayList();
                getFactoryInList = reportBP.getFactoryInList(scheduleDate);
                System.out.println("getFactoryInList==" + getFactoryInList.size());
                reportTO = new ReportTO();
                itr = null;
                itr = getFactoryInList.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getCount()5==" + reportTO.getCount());
                    if ("1".equals(reportTO.getCount())) {
                        count1++;
                    }
                    if ("2".equals(reportTO.getCount())) {
                        count2++;
                    }
                    if ("3".equals(reportTO.getCount())) {
                        count3++;
                    }
                }

                ArrayList getFactoryInTimeDiff = new ArrayList();
                getFactoryInTimeDiff = reportBP.getFactoryInTimeDiff(scheduleDate);
                System.out.println("getFactoryInTimeDiffsize==" + getFactoryInTimeDiff.size());
                reportTO = new ReportTO();
                itr = null;
                itr = getFactoryInTimeDiff.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getCount()6==" + reportTO.getCount());
                    if ("1".equals(reportTO.getCount())) {
                        count3++;
                    }
                    if ("2".equals(reportTO.getCount())) {
                        count4++;
                    }
                    if ("3".equals(reportTO.getCount())) {
                        count6++;
                    }
                }
                System.out.println("count1=" + count1);
                System.out.println("count2=" + count2);
                System.out.println("count3=" + count3);
                System.out.println("count4=" + count4);
                System.out.println("count5=" + count5);
                System.out.println("count6=" + count6);
                new MovementTypeStatus(flag, scheduleDate, day, count1, count2, count3, count4, count5, count6).start();

            }

            ////////////////////////////////////////////////////////////////
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void dailyBookingReportAlert(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        String fromDate = sdfNew.format(c.getTime()).toString();
        System.out.println("fromDate = " + fromDate);
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        int hours = 0;
        int mins = 0;
        if (timeTemp[0] != null) {
            hours = Integer.parseInt(timeTemp[0]);
        }
        if (timeTemp[1] != null) {
            mins = Integer.parseInt(timeTemp[1]);
        }
        ReportTO reportTO = new ReportTO();
        TripTO tripTO = new TripTO();

        try {

            System.out.println(" System Hours" + hours);
            System.out.println(" System mins" + mins);
            System.out.println(" dailyBookingListStartTime" + ThrottleConstants.dailyBookingListStartTime);
            String tempTime[] = ThrottleConstants.dailyBookingListStartTime.split(":");
            System.out.println("dailyBookingListStartTime-1--Hours--- " + tempTime[0]);
            System.out.println("dailyBookingListStartTime-2--Mins--- " + tempTime[1]);
            
            if(hours == Integer.parseInt("85")) { // between the planned hours
//            if(hours == Integer.parseInt(tempTime[0]) && mins == Integer.parseInt(tempTime[1])) { // between the planned hours
//            if (runMinute == 10 || runMinute == 00 || runMinute == 57 || runMinute == 27 || runMinute == 45 || runMinute == 50) {
                reportTO = new ReportTO();
                tripTO = new TripTO();
                System.out.println("Please find the below daily Booking report,");
                reportTO.setReportContentFormat("Please find the below daily Booking report,");
                //String content = reportBP.getDailySalesReportAlert(reportTO);
                String content = reportBP.getDailyBookingReportAlert(reportTO);
                System.out.println("contentSSSSSS result :" + content);

                if (!"".equals(content)) {

                    System.out.println("content : " + content);

                    String[] tempContent = content.split("~");

                    tripTO.setMailTypeId("11");
                    tripTO.setMailSubjectTo("Daily Booking Report");
                    tripTO.setMailSubjectCc("Daily Booking Report");
                    tripTO.setMailSubjectBcc("Daily Booking Report");
                    tripTO.setMailContentTo(tempContent[0]);
                    tripTO.setMailContentCc(tempContent[0]);
                    tripTO.setMailContentBcc(tempContent[0]);

                    tripTO.setMailIdTo(ThrottleConstants.dailyBookingListToMailId);
                    tripTO.setMailIdCc(ThrottleConstants.dailyBookingListCCMailId);

                    tripTO.setFileName(tempContent[1]);
                    System.out.println("contrr filepath------" + tempContent[2]);
                    tripTO.setFilePath(tempContent[2]);

                    int insertStatus = tripBP.insertMailDetails(tripTO, 1);
                    System.out.println("insertStatus==" + insertStatus);

                }
            }
            
            
//           ---test------ 
//            ReportTO reportTO1 = new ReportTO();
//            ArrayList emailList = new ArrayList();
//            Iterator itr = null;
//            try {
//                emailList = reportBP.getEmailList();
//                System.out.println("emailList ------------- " + emailList);
//                System.out.println("emailList ----------------------- " + emailList.size());
//                itr = emailList.iterator();
//                while (itr.hasNext()) {
//                    reportTO1 = (ReportTO) itr.next();
//                    System.out.println("reportTO ----- mail contenet  " + reportTO1.getMailContentTo());
//                    System.out.println("applicationServer ----------- " + applicationServer);
//                    reportTO1.setServerName(applicationServer);
//                    reportTO1.setServerIpAddress(serverIpAddress);
//                    new EmailSSL(reportTO1).start();
//                }
//            } catch (Exception exp) {
//                exp.printStackTrace();
////                FPLogUtils.fpErrorLog("Email not delivered --> " + exp.getMessage());
//                ReportTO rTO1 = new ReportTO();
//                rTO1.setMailSubjectTo("Email not delivered");
//                rTO1.setMailContentTo(exp.getMessage());
//                rTO1.setMailIdTo(ThrottleConstants.developerSupport);
//                rTO1.setMailIdCc("throttleerror@gmail.com");
//                //new EmailSSL(rTO).start();
//                rTO1 = null;
//            } finally {
//                emailList = null;
//                itr = null;
//            }
//           ---test------ 

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to search Course sems--> " + exception);
            exception.printStackTrace();
        } finally {
            sdf = null;
            sdfNew = null;
            date = null;
            c = null;
            systemTime = null;
            fromDate = null;
            dateTemp = null;
            timeTemp = null;
            tripTO = null;
            reportTO = null;
        }
    }

    public void dailyBookingReport2Alert(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        String fromDate = sdfNew.format(c.getTime()).toString();
        System.out.println("fromDate = " + fromDate);
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        int hours = 0;
        int mins = 0;
        if (timeTemp[0] != null) {
            hours = Integer.parseInt(timeTemp[0]);
        }
        if (timeTemp[1] != null) {
            mins = Integer.parseInt(timeTemp[1]);
        }
        ReportTO reportTO = new ReportTO();
        TripTO tripTO = new TripTO();

        try {

            System.out.println(" System Hours" + hours);
            System.out.println(" System mins" + mins);
            System.out.println(" dailyBookingListStartTime" + ThrottleConstants.dailyBookingListStartTime);
            String tempTime[] = ThrottleConstants.dailyBookingListStartTime.split(":");
            System.out.println("dailyBookingListStartTime-1--Hours--- " + tempTime[0]);
            System.out.println("dailyBookingListStartTime-2--Mins--- " + tempTime[1]);
            if (hours  == Integer.parseInt("85")) {
//            if ((hours == Integer.parseInt(tempTime[0])) && mins == Integer.parseInt(tempTime[1])) { // between the planned hours
                reportTO = new ReportTO();
                tripTO = new TripTO();
                System.out.println("Please find the below daily Booking report,");
                reportTO.setReportContentFormat("Please find the below daily Booking report,");
                //String content = reportBP.getDailySalesReportAlert(reportTO);
                String content = reportBP.dailyBookingReportAlert2(reportTO);

                System.out.println("contentSSSSSS result :" + content);

                if (!"".equals(content)) {

                    System.out.println("content : " + content);

                    String[] tempContent = content.split("~");

                    tripTO.setMailTypeId("11");
                    tripTO.setMailSubjectTo("Daily Customer wise Booking Report");
                    tripTO.setMailSubjectCc("Daily Customer wise Booking Report");
                    tripTO.setMailSubjectBcc("Daily Customer wise Booking Report");
                    tripTO.setMailContentTo(tempContent[0]);
                    tripTO.setMailContentCc(tempContent[0]);
                    tripTO.setMailContentBcc(tempContent[0]);

                    tripTO.setMailIdTo(ThrottleConstants.dailyBookingListToMailId);
                    tripTO.setMailIdCc(ThrottleConstants.dailyBookingListCCMailId);

                    tripTO.setFileName(tempContent[1]);
                    System.out.println("contrr filepath------" + tempContent[2]);
                    tripTO.setFilePath(tempContent[2]);

//                    int insertStatus = tripBP.insertMailDetails(tripTO, 1);
//                    System.out.println("insertStatus==" + insertStatus);
                }

            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to search Course sems--> " + exception);
            exception.printStackTrace();
        } finally {
            sdf = null;
            sdfNew = null;
            date = null;
            c = null;
            systemTime = null;
            fromDate = null;
            dateTemp = null;
            timeTemp = null;
            tripTO = null;
            reportTO = null;
        }
    }

    public void factoryInOutScheduler(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        String fromDate = sdfNew.format(c.getTime()).toString();
        System.out.println("fromDate = " + fromDate);
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        int hours = 0;
        int mins = 0;
        if (timeTemp[0] != null) {
            hours = Integer.parseInt(timeTemp[0]);
        }
        if (timeTemp[1] != null) {
            mins = Integer.parseInt(timeTemp[1]);
        }
        ReportTO reportTO = new ReportTO();
        TripTO tripTO = new TripTO();

        try {
            System.out.println(" System hours" + hours);
            System.out.println(" System mins" + mins);
            ArrayList companyList = new ArrayList();
            companyList = reportBP.getFactoryInOutList();
            System.out.println("factoryInOutList.size" + companyList.size());
            int status = 0;
            Iterator itr = null;
            itr = companyList.iterator();
            while (itr.hasNext()) {
                reportTO = (ReportTO) itr.next();
                String tripId = reportTO.getTripId();
                String tripStatusId = reportTO.getTripStatusId();
                String durationDate = reportTO.getDurationDate();
                String durationTime = reportTO.getDurationTime();

                reportTO.setTripId(tripId);
                reportTO.setTripStatusId(tripStatusId);
                reportTO.setDurationDate(durationDate);
                reportTO.setDurationTime(durationTime);
                status = reportBP.updateFactoryInOutList(reportTO);
                System.out.println("status-controller----" + status);
            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to search Course sems--> " + exception);
            exception.printStackTrace();
        } finally {
            sdf = null;
            sdfNew = null;
            date = null;
            c = null;
            systemTime = null;
            fromDate = null;
            dateTemp = null;
            timeTemp = null;
            tripTO = null;
            reportTO = null;
        }
    }

     public void companyWiseTripClosure(HttpServletRequest request, HttpServletResponse response) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
        Calendar c = Calendar.getInstance();
        Date date = new Date();
        c.setTime(date);
        String systemTime = sdf.format(c.getTime()).toString();
        String fromDate = sdfNew.format(c.getTime()).toString();
        System.out.println("fromDate = " + fromDate);
        System.out.println("systemTime = " + systemTime);
        String[] dateTemp = systemTime.split(" ");
        String[] timeTemp = dateTemp[1].split(":");
        int hours = 0;
        int mins = 0;
        if (timeTemp[0] != null) {
            hours = Integer.parseInt(timeTemp[0]);
        }
        if (timeTemp[1] != null) {
            mins = Integer.parseInt(timeTemp[1]);
        }
        ReportTO reportTO = new ReportTO();
        ReportTO reportTO1 = new ReportTO();
        TripTO tripTO = new TripTO();

        try {
            System.out.println("MINS System hours" + hours);
            System.out.println("MINS System mins" + mins);
            System.out.println("MINS timeTemp[1]" + timeTemp[1]);
            
//            if ("00".equals(timeTemp[1])  || "15".equals(timeTemp[1]) || "30".equals(timeTemp[1]) || "45".equals(timeTemp[1])) {
            if ("85".equals(timeTemp[1])) {
                ArrayList companyList = new ArrayList();
                ArrayList tripClosureList = new ArrayList();
                companyList = reportBP.getCompanyLists();
                System.out.println("companyList.size" + companyList.size());
                int status = 0;
                Iterator itr = null;
                Iterator itr1 = null;
                
                itr = companyList.iterator();
                while (itr.hasNext()) {
                    reportTO = (ReportTO) itr.next();
                    System.out.println("reportTO.getCompId()-------"+reportTO.getCompId());
                    tripClosureList = reportBP.getTripClosureList(reportTO);
                    System.out.println("tripClosureListaa.size" + tripClosureList.size());

                    itr1 = tripClosureList.iterator();
                    while (itr1.hasNext()) {
                        reportTO1 = (ReportTO) itr1.next();

                        String tripId = reportTO1.getTripId();
                        String vehicleId = reportTO1.getVehicleId();
                        String companyId = reportTO.getCompId();
                        String statusId = reportTO1.getStatusId();
                        String activeStatus = reportTO1.getActiveStatus();
                        String tripIds = reportTO1.getTripIds();
                        String dktStatus = reportTO1.getDktStatus();
                        String vendIds = reportTO1.getVendorId();
//                        String dktFAStatus = reportTO1.getDktFAStatus();

                        System.out.println("statusId " + statusId);
                        System.out.println("activeStatus " + activeStatus);
                        System.out.println("tripIds" + tripIds);
                        System.out.println("dktStatus" + dktStatus);
                        System.out.println("vendIds" + vendIds);
//                        System.out.println("dktFAStatus" + dktFAStatus);

                        System.out.println("Im iN Company " + reportTO.getCompId());

                        System.out.println("insert and update DKT,ContDetails , Transactions tables");
                        new thrERPTablePosting(tripId, vehicleId, companyId ,statusId ,activeStatus ,tripIds,vendIds).start();
                        System.out.println("status-updateTripDktStatus----" + status);
                    }
                }
            }

        } catch (FPRuntimeException excp) {
            FPLogUtils.fpErrorLog("Run time exception --> " + excp.getErrorDetails());
        } catch (Exception exception) {

            FPLogUtils.fpErrorLog("Failed to search Course sems--> " + exception);
            exception.printStackTrace();
        } finally {
            sdf = null;
            sdfNew = null;
            date = null;
            c = null;
            systemTime = null;
            fromDate = null;
            dateTemp = null;
            timeTemp = null;
            tripTO = null;
            reportTO = null;
        }
    }

    public HashMap<String, String> loadProps() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        HashMap<String, String> maps = new HashMap<String, String>();
        try {
            ArrayList list = new ArrayList();
            list = loginBP.LoadProps();
//            System.out.println("IM HERE-------");
            ThrottleConstants.PROPS_VALUES = new HashMap<String, String>();
            for (Iterator it = list.iterator(); it.hasNext();) {
                LoginTO object = (LoginTO) it.next();
                try {
//                    System.out.println("object.getKeyName() = " + object.getKeyName());
//                    System.out.println("object.getValue() = " + object.getValue());
                    ThrottleConstants.PROPS_VALUES.put(object.getKeyName().trim(), object.getValue().trim());
                } catch (Exception e) {
                }
            }

        } catch (Exception e) {
            String error = e.getMessage();
        }
        map.clear();
        return ThrottleConstants.PROPS_VALUES;
    }

}
