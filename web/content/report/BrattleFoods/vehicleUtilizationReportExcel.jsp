<%-- 
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
         <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleUtilizationReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test="${vehicleUtilizationList == null}">
                <center>
                <font color="blue">Please Wait Your Request is Processing</font>
                </center>
            </c:if>
            <c:if test="${vehicleUtilizationList != null}">
                  <table align="center" border="0" id="table" class="sortable" style="width:1000px;" >
                    <thead>
                        <tr>
                            <th align="center">S.No</th>
                           <th align="center">Vehicle No</th>
                           <th align="center">Vehicle Type</th>
                           <th align="center">Fleet Center</th>
                           <th align="center">Local Trips</th>
                           <th align="center">Muff Trips</th>
                           <th align="center">Total Trips</th>  
                            <th align="center">Total Utilised Days</th>
                            <th align="center">Total KM Run</th>
                            <th align="center">Vehicle Utilization(%)</th>
                        </tr>
                    </thead>
                            <tbody>
                    <% int index = 1;%>
                            <c:forEach items="${vehicleUtilizationList}" var="utilList">
                                  <c:set var="days" value="${utilList.totDays}"/>
                                  <c:set var="percentage" value="${(utilList.utilDays*100) /days }"/>
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                        <tr>
                                            <td   ><%=index++%></td>
                                            <td   >
                                               <c:out value="${utilList.regNo}"/></td>
                                            <td    ><c:out value="${utilList.vehicleType}"/></td>
                                            <td   ><c:out value="${utilList.companyName}"/></td>
                                            <td   ><c:out value="${utilList.noOfTripsLocal}"/></td>
                                            <td   ><c:out value="${utilList.noOfTripsMoff}"/></td>
                                            <td   ><c:out value="${utilList.noOfTripsMoff + utilList.noOfTripsLocal}"/></td>
                                            <td  align="right"    ><c:out value="${utilList.utilDays}"/></td>
                                            <td  align="right"    ><c:out value="${utilList.runKm}"/></td>
                                            <td  align="right"  ><fmt:formatNumber pattern="##0.00" value="${percentage}"/>
                                        </tr>

                            </c:forEach>
                </tbody>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
