<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 
    <head>
        
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.mrs.business.MrsTO" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <title>MRSList</title>
    </head>
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Alter Rc WO" text="Alter Rc WO"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Reconditioning" text="Reconditioning"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Alter Rc WO" text="Alter Rc WO"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    
    <body onload="document.purchase.woId.focus();">
        
        
        <script>
        function submitPage() {
            if( (document.purchase.woId.value != '') && ( !isFloat(document.purchase.woId.value) )  ) {            
            document.purchase.action="/throttle/handleAlterRCWOPage.do"
            document.purchase.submit();
            }
        }
               
            
        </script>
        
        
        
        <script>
   function changePageLanguage(langSelection){
   if(langSelection== 'ar'){
   document.getElementById("pAlign").style.direction="rtl";
   }else if(langSelection== 'en'){
   document.getElementById("pAlign").style.direction="ltr";
   }
   }
 </script>

  <c:if test="${jcList != null}">
  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
  </c:if>
      
  <span style="float: right">
	<a href="?paramName=en">English</a>
	|
	<a href="?paramName=ar">Arabic</a>
  </span>
        
        <form name="purchase"  method="post" >
          
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <!-- pointer table -->
            <!-- message table -->           
            <%@ include file="/content/common/message.jsp" %>    
           
                    
<!--            <table align="center" border="0" cellpadding="0" cellspacing="0" width="550" id="bg" class="border">
                <tr>
                    <td colspan="9" height="80" align="center" class="contenthead" ><div class="contenthead">MODIFY WO </div></td>
                </tr>-->
<table class="table table-info mb30 table-hover" id="bg"   >
		    <thead>
		<tr>
		    <th colspan="4" height="30" >MODIFY WO</th>
		</tr>
                    </thead>
            </table>

<table class="table table-info mb30 table-hover" id="bg" >

                <thead>
                <tr>
                    <td><spring:message code="stores.label.WorkOrderNo."  text="default text"/>
</td>
                    <td> <input type="text" name="woId" value="" class="form-control" style="width:240px;height:40px"> </td>
                </tr>  
                </thead>
            </table>
            
            <br>                                                
             <center>   
            <input type="button" class="btn btn-success" name="Search" value="<spring:message code="stores.label.SEARCH"  text="default text"/>" onClick="submitPage();" > &nbsp;            
            </center>                           
        <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
            </div>
            </div>
            <%@ include file="../common/NewDesign/settings.jsp" %>