package ets.domain.customer.business;

/**
 *
 * @author vidya
 */
public class CustomerTO {

    // private String secondaryBillingTypeId;
    // private String accountManagerId;
    // private String paymentType;
    private String contractId = "";
    private String tallyBillingType = "";
    private String billingPartyAddress = "";
    private String billingState = "";
    private String latitude = "";
    private String longitute = "";
    private int compId = 0;
    private String billingCustId = "";
    private String billingCustName = "";
    private String billingCustFlag = "";
    private String pinCodeAdd = "";
    private String companyId = "";
    private String cityName = "";
    private String consigneeName = "";
    private String consigneeId = "";
    private String consigneeCode = "";
    private String localContractStatus = "";
    private String consignorId = "";
    private String contactPerson = "";
    private String consignorName = "";
    private String phoneNo = "";
    private String customerSiteLocation = "";
    private String address1 = "";
    private String address2 = "";
    private String cityId = "";
    private String activeInd = "";
    private String email = "";
    private String pinCode = "";
    private String consignorCode = "";
    private String lclContractStatus = "";
    private String distanceContractStatus = "";
    private String companyType = "";
    private String stateCode = "";
    private String stateId = "";
    private String stateName = "";
    private String erpId = "";
    private String panNo = "";
    private String billingNameAddress = "";
    private String secondaryRouteStatus = "";
    private String primaryContractStatus;
    private String customerGroup;
    private String customerGroupId;
    private String ledgerId;
    // private String groupName;
    // private String creditLimit;
    private String name;
    private String empName;
    private String empId;
    // private String creditDays;
    private String customerName;
    private String userId;
    private String roleId;
    private String customerId;
    private String secondaryBillingTypeId;
    private String groupId;
    private String groupName;
    private String secondaryBillingTypeName;
    private String paymentType;
    private int productStatus;
    private int secondaryContractSatus;
    private String contractType;
    private int custId;
    private String creditDays;
    private String creditLimit;
    private String custName;
    private String custContactPerson;
    private String custAddress;
    private String custCity;
    private String custState;
    private String custPhone;
    private String custMobile;
    private String custEmail;
    private String custStatus;
    private String contManufacturerName;
    private String contModelName;
    private String contPercentAgeUpSpares;
    private String contPercentAgeUpLabour;
    private String contStatus;
    private String contFromDate;
    private String contToDate;
    private String customerType;
    private String bunkName;
    private String bunkId;
    private String fuelType;
    private String currRate;
    private String currlocation;
    private String bunkState;
    private String bunkStatus;
    private String remarks;
    private double currentrate;
    private String packingId;
    private String packingName;
    private String toLocation;
    private String routeId;
    private String tancem;
    private String customerCode = "";
    private String contractStatus = "";
    private String billingTypeName = "";
    private String enrollDate = "";
    private String billingTypeId = "";
    private String customerTypeId = "";
    private String customerTypeName = "";
    private String accountManagerId = "";
    private String fuelVehicle = "";
    private String fuelDG = "";
    private String totalFuel = "";
    private String fuelModelId = "";
    private String vehicleTypeId = "";
    private String modelId = "";
    private String modelName = "";
    private String orgId = "";
    private String orgName = "";
    private String organizationId = "";
    private String editId = "";
 private String custTypeid="";
   private String custAddresstwo = "";
    private String custAddressthree = "";
      private String gstNo = "";
    
    private String cityGroupID = "";
      private String cityGroupName = "";

      
      
      
      
      
      
    public String getCustTypeid() {
        return custTypeid;
    }

    public void setCustTypeid(String custTypeid) {
        this.custTypeid = custTypeid;
    }

  

    
    //gst
  
      
   
    public String getCityGroupID() {
        return cityGroupID;
    }

    public void setCityGroupID(String cityGroupID) {
        this.cityGroupID = cityGroupID;
    }

    public String getCityGroupName() {
        return cityGroupName;
    }

    public void setCityGroupName(String cityGroupName) {
        this.cityGroupName = cityGroupName;
    }

//    12/05/16
  

    public String getCustAddresstwo() {
        return custAddresstwo;
    }

    public void setCustAddresstwo(String custAddresstwo) {
        this.custAddresstwo = custAddresstwo;
    }

    public String getCustAddressthree() {
        return custAddressthree;
    }

    public void setCustAddressthree(String custAddressthree) {
        this.custAddressthree = custAddressthree;
    }

    public String getBunkId() {
        return bunkId;
    }

    public void setBunkId(String bunkId) {
        this.bunkId = bunkId;
    }

    public String getBunkName() {
        return bunkName;
    }

    public void setBunkName(String bunkName) {
        this.bunkName = bunkName;
    }

    public String getBunkState() {
        return bunkState;
    }

    public void setBunkState(String bunkState) {
        this.bunkState = bunkState;
    }

    public String getBunkStatus() {
        return bunkStatus;
    }

    public void setBunkStatus(String bunkStatus) {
        this.bunkStatus = bunkStatus;
    }

    public String getCurrRate() {
        return currRate;
    }

    public void setCurrRate(String currRate) {
        this.currRate = currRate;
    }

    public double getCurrentrate() {
        return currentrate;
    }

    public void setCurrentrate(double currentrate) {
        this.currentrate = currentrate;
    }

    public String getCurrlocation() {
        return currlocation;
    }

    public void setCurrlocation(String currlocation) {
        this.currlocation = currlocation;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCustAddress() {
        return custAddress;
    }

    public void setCustAddress(String custAddress) {
        this.custAddress = custAddress;
    }

    public String getCustCity() {
        return custCity;
    }

    public void setCustCity(String custCity) {
        this.custCity = custCity;
    }

    public String getCustContactPerson() {
        return custContactPerson;
    }

    public void setCustContactPerson(String custContactPerson) {
        this.custContactPerson = custContactPerson;
    }

    public String getCustEmail() {
        return custEmail;
    }

    public void setCustEmail(String custEmail) {
        this.custEmail = custEmail;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId) {
        this.custId = custId;
    }

    public String getCustMobile() {
        return custMobile;
    }

    public void setCustMobile(String custMobile) {
        this.custMobile = custMobile;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPhone() {
        return custPhone;
    }

    public void setCustPhone(String custPhone) {
        this.custPhone = custPhone;
    }

    public String getCustState() {
        return custState;
    }

    public void setCustState(String custState) {
        this.custState = custState;
    }

    public String getCustStatus() {
        return custStatus;
    }

    public void setCustStatus(String custStatus) {
        this.custStatus = custStatus;
    }

    public String getContFromDate() {
        return contFromDate;
    }

    public void setContFromDate(String contFromDate) {
        this.contFromDate = contFromDate;
    }

    public String getContManufacturerName() {
        return contManufacturerName;
    }

    public void setContManufacturerName(String contManufacturerName) {
        this.contManufacturerName = contManufacturerName;
    }

    public String getContModelName() {
        return contModelName;
    }

    public void setContModelName(String contModelName) {
        this.contModelName = contModelName;
    }

    public String getContPercentAgeUpLabour() {
        return contPercentAgeUpLabour;
    }

    public void setContPercentAgeUpLabour(String contPercentAgeUpLabour) {
        this.contPercentAgeUpLabour = contPercentAgeUpLabour;
    }

    public String getContPercentAgeUpSpares() {
        return contPercentAgeUpSpares;
    }

    public void setContPercentAgeUpSpares(String contPercentAgeUpSpares) {
        this.contPercentAgeUpSpares = contPercentAgeUpSpares;
    }

    public String getContStatus() {
        return contStatus;
    }

    public void setContStatus(String contStatus) {
        this.contStatus = contStatus;
    }

    public String getContToDate() {
        return contToDate;
    }

    public void setContToDate(String contToDate) {
        this.contToDate = contToDate;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getPackingId() {
        return packingId;
    }

    public void setPackingId(String packingId) {
        this.packingId = packingId;
    }

    public String getPackingName() {
        return packingName;
    }

    public void setPackingName(String packingName) {
        this.packingName = packingName;
    }

    public String getToLocation() {
        return toLocation;
    }

    public void setToLocation(String toLocation) {
        this.toLocation = toLocation;
    }

    public String getRouteId() {
        return routeId;
    }

    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    public String getTancem() {
        return tancem;
    }

    public void setTancem(String tancem) {
        this.tancem = tancem;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getContractStatus() {
        return contractStatus;
    }

    public void setContractStatus(String contractStatus) {
        this.contractStatus = contractStatus;
    }

    public String getBillingTypeName() {
        return billingTypeName;
    }

    public void setBillingTypeName(String billingTypeName) {
        this.billingTypeName = billingTypeName;
    }

    public String getEnrollDate() {
        return enrollDate;
    }

    public void setEnrollDate(String enrollDate) {
        this.enrollDate = enrollDate;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getCustomerTypeId() {
        return customerTypeId;
    }

    public void setCustomerTypeId(String customerTypeId) {
        this.customerTypeId = customerTypeId;
    }

    public String getCustomerTypeName() {
        return customerTypeName;
    }

    public void setCustomerTypeName(String customerTypeName) {
        this.customerTypeName = customerTypeName;
    }

    public String getAccountManagerId() {
        return accountManagerId;
    }

    public void setAccountManagerId(String accountManagerId) {
        this.accountManagerId = accountManagerId;
    }

    public String getCreditDays() {
        return creditDays;
    }

    public void setCreditDays(String creditDays) {
        this.creditDays = creditDays;
    }

    public String getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(String creditLimit) {
        this.creditLimit = creditLimit;
    }

    public int getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(int productStatus) {
        this.productStatus = productStatus;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getSecondaryBillingTypeId() {
        return secondaryBillingTypeId;
    }

    public void setSecondaryBillingTypeId(String secondaryBillingTypeId) {
        this.secondaryBillingTypeId = secondaryBillingTypeId;
    }

    public String getSecondaryBillingTypeName() {
        return secondaryBillingTypeName;
    }

    public void setSecondaryBillingTypeName(String secondaryBillingTypeName) {
        this.secondaryBillingTypeName = secondaryBillingTypeName;
    }

    public int getSecondaryContractSatus() {
        return secondaryContractSatus;
    }

    public void setSecondaryContractSatus(int secondaryContractSatus) {
        this.secondaryContractSatus = secondaryContractSatus;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getCustomerGroup() {
        return customerGroup;
    }

    public void setCustomerGroup(String customerGroup) {
        this.customerGroup = customerGroup;
    }

    public String getCustomerGroupId() {
        return customerGroupId;
    }

    public void setCustomerGroupId(String customerGroupId) {
        this.customerGroupId = customerGroupId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContractType() {
        return contractType;
    }

    public void setContractType(String contractType) {
        this.contractType = contractType;
    }

    public String getPrimaryContractStatus() {
        return primaryContractStatus;
    }

    public void setPrimaryContractStatus(String primaryContractStatus) {
        this.primaryContractStatus = primaryContractStatus;
    }

    public String getSecondaryRouteStatus() {
        return secondaryRouteStatus;
    }

    public void setSecondaryRouteStatus(String secondaryRouteStatus) {
        this.secondaryRouteStatus = secondaryRouteStatus;
    }

    public String getBillingNameAddress() {
        return billingNameAddress;
    }

    public void setBillingNameAddress(String billingNameAddress) {
        this.billingNameAddress = billingNameAddress;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLedgerId() {
        return ledgerId;
    }

    public void setLedgerId(String ledgerId) {
        this.ledgerId = ledgerId;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getErpId() {
        return erpId;
    }

    public void setErpId(String erpId) {
        this.erpId = erpId;
    }

    public String getPanNo() {
        return panNo;
    }

    public void setPanNo(String panNo) {
        this.panNo = panNo;
    }

    public String getFuelDG() {
        return fuelDG;
    }

    public void setFuelDG(String fuelDG) {
        this.fuelDG = fuelDG;
    }

    public String getFuelModelId() {
        return fuelModelId;
    }

    public void setFuelModelId(String fuelModelId) {
        this.fuelModelId = fuelModelId;
    }

    public String getFuelVehicle() {
        return fuelVehicle;
    }

    public void setFuelVehicle(String fuelVehicle) {
        this.fuelVehicle = fuelVehicle;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getTotalFuel() {
        return totalFuel;
    }

    public void setTotalFuel(String totalFuel) {
        this.totalFuel = totalFuel;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getStateId() {
        return stateId;
    }

    public void setStateId(String stateId) {
        this.stateId = stateId;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getGstNo() {
        return gstNo;
    }

    public void setGstNo(String gstNo) {
        this.gstNo = gstNo;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType;
    }

    public String getDistanceContractStatus() {
        return distanceContractStatus;
    }

    public void setDistanceContractStatus(String distanceContractStatus) {
        this.distanceContractStatus = distanceContractStatus;
    }

    public String getLclContractStatus() {
        return lclContractStatus;
    }

    public void setLclContractStatus(String lclContractStatus) {
        this.lclContractStatus = lclContractStatus;
    }

    public String getLocalContractStatus() {
        return localContractStatus;
    }

    public void setLocalContractStatus(String localContractStatus) {
        this.localContractStatus = localContractStatus;
    }

    public String getConsignorId() {
        return consignorId;
    }

    public void setConsignorId(String consignorId) {
        this.consignorId = consignorId;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCustomerSiteLocation() {
        return customerSiteLocation;
    }

    public void setCustomerSiteLocation(String customerSiteLocation) {
        this.customerSiteLocation = customerSiteLocation;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getConsignorCode() {
        return consignorCode;
    }

    public void setConsignorCode(String consignorCode) {
        this.consignorCode = consignorCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneeId() {
        return consigneeId;
    }

    public void setConsigneeId(String consigneeId) {
        this.consigneeId = consigneeId;
    }

    public String getConsigneeCode() {
        return consigneeCode;
    }

    public void setConsigneeCode(String consigneeCode) {
        this.consigneeCode = consigneeCode;
    }

    public String getPinCodeAdd() {
        return pinCodeAdd;
    }

    public void setPinCodeAdd(String pinCodeAdd) {
        this.pinCodeAdd = pinCodeAdd;
    }

    public int getCompId() {
        return compId;
    }

    public void setCompId(int compId) {
        this.compId = compId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getBillingCustFlag() {
        return billingCustFlag;
    }

    public void setBillingCustFlag(String billingCustFlag) {
        this.billingCustFlag = billingCustFlag;
    }

    public String getBillingCustId() {
        return billingCustId;
    }

    public void setBillingCustId(String billingCustId) {
        this.billingCustId = billingCustId;
    }

    public String getBillingCustName() {
        return billingCustName;
    }

    public void setBillingCustName(String billingCustName) {
        this.billingCustName = billingCustName;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitute() {
        return longitute;
    }

    public void setLongitute(String longitute) {
        this.longitute = longitute;
    }

    public String getEditId() {
        return editId;
    }

    public void setEditId(String editId) {
        this.editId = editId;
    }

    public String getTallyBillingType() {
        return tallyBillingType;
    }

    public void setTallyBillingType(String tallyBillingType) {
        this.tallyBillingType = tallyBillingType;
    }

    public String getBillingPartyAddress() {
        return billingPartyAddress;
    }

    public void setBillingPartyAddress(String billingPartyAddress) {
        this.billingPartyAddress = billingPartyAddress;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }
    
    

}
