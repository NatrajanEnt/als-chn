
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

        <script>
            function submitPage(){
               document.log.action = '/throttle/searchInvoiceEditLog.do';
               document.log.submit();
            }
            </script>
   <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Invoice Edit Log Details</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="setValue();
            sorter.size(10);">
        <form name="log" action=""  method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
                                <table class="table table-info mb30 table-hover" style="width:100%">
                                    <tr height="30"   ><td colSpan="3" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Invoice Edit Log Details</td></tr>

                                    <tr colspan="2"  height="30">
                                        <td  height="30"><font color="red">*</font>GR No :</td>
                                        <td  height="30"><input type="text" name="grNo" id="grNo" class="form-control" value="<c:out value="${grNo}"/>" style="width:240px;height:40px;"/>  </td>


                                    <!--<td><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);"></td>-->
                                    <td><input type="button" class="btn btn-info" name="Search"   value="Search" onclick="submitPage();"></td>
                                    </tr>
                                </table>
                                        <br>
              <table align="center" border="0" id="table" class="sortable" style="width:90%;" >
                    <thead height="40">
                        <tr id="tableDesingTH" height="40">
                        <th>S.No</th>
                        <th>GR No</th>
                        <th>Container No. Old</th>
                        <th>Container No. New</th>
                        <th>Shipping No. Old</th>
                        <th>Shipping No. New</th>
                        <th>Bill of  Entry Old</th>
                        <th>Bill of Entry New</th>
                        <th>Detention Charge Old</th>
                        <th>Detention Charge New </th>
                        <th>Green Tax Charge Old </th>
                        <th>Green Tax Charge New</th>
                        <th>Toll Tax Old</th>
                        <th>Toll Tax New </th>
                        <th>Other Expense New </th>
                        <th>Other Expense Old </th>
                        <th>weightment New </th>
                        <th>weightment Old </th>
                        <th>Billing Party Old </th>
                        <th>Billing Party New </th>
                        <th>Movement Type </th>
                        <th>Billing Type</th>
                        <th>User Name</th>
                        <th>Time</th>

                    </tr>
                </thead>
                <tbody>


                    <% int index = 1,sno = 1;%>
                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text2";
                        }
                    %>

                    <c:forEach items="${invoiceEditLogDetails}" var="log">


                        <tr height="50">
                            <td align="center" ><%=sno%></td>
                            <td align="center" >&nbsp;<c:out value="${log.grNo}"/></td>
                            <td align="center" ><c:out value="${log.containerNoOld}"/></td>
                            <td align="center" ><c:out value="${log.containerNo}"/></td>
                            <td align="center" ><c:out value="${log.shipBillNoOld}"/></td>
                            <td align="center" ><c:out value="${log.shipBillNo}"/></td>
                            <td align="center" >&nbsp;&nbsp;&nbsp;<c:out value="${log.billOfEntryOld}"/></td>
                            <td align="center" >&nbsp;&nbsp;&nbsp;<c:out value="${log.billOfEntry}"/></td>
                            <td align="center" >&nbsp;&nbsp;&nbsp;<c:out value="${log.detentionChargeOld}"/></td>
                            <td align="center" ><c:out value="${log.detentionCharge}"/></td>
                            <td align="center" ><c:out value="${log.greenTaxOld}"/></td>
                            <td align="center" ><c:out value="${log.greenTax}"/></td>
                            <td align="center" ><c:out value="${log.tollTaxOld}"/></td>
                            <td align="center" ><c:out value="${log.tollTax}"/></td>
                            <td align="center" ><c:out value="${log.otherExpenseOld}"/></td>
                            <td align="center" ><c:out value="${log.otherExpense}"/></td>
                            <td align="center" ><c:out value="${log.weightmentOld}"/></td>
                            <td align="center" ><c:out value="${log.weightment}"/></td>
                            <td align="center" ><c:out value="${log.billingPartyIdOld}"/></td>
                            <td align="center" ><c:out value="${log.billingPartyIdNew}"/></td>
                            <td align="center" ><c:out value="${log.movementType}"/></td>
                            <td align="center" ><c:out value="${log.billType}"/></td>
                            <td align="center" ><c:out value="${log.userName}"/></td>
                            <td align="center" ><c:out value="${log.modifiedTime}"/></td>



                        </tr>
                   <%
                   index++;
                     sno++;
                        %>
                    </c:forEach>

                </tbody>


            </table>
            <br/>
            <br/>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <br/>
            <br/>
            <br/>
            <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1,true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1,true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table",0);
        </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
 </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>s