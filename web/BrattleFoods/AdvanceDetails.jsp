<%-- 
    Document   : AdvanceDetails
    Created on : Nov 4, 2013, 12:01:53 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
     <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

 
            function submitPage(value){
//                alert(hi);
                document.AdvanceDetails.action='/throttle/BrattleFoods/SearchAdvanceDetails.jsp';
                document.AdvanceDetails.submit();
            }
            function excelPage(){
                document.AdvanceDetails.action='/throttle/BrattleFoods/SearchAdvanceDetailsExcel.jsp';
                document.AdvanceDetails.submit();
            }
        </script>
    </head>
    <body>
          <form name="AdvanceDetails" method="post">
              <%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>

        <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Advance Details</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                          <td colspan="2"><input type="button" class="button"   value="Search" onclick="submitPage(this.value)"></td>
                                        <td colspan="2"><input type="button" class="button"   value="Generate Excel" onclick="excelPage()"></td>
                                    </tr>
                                    <tr >
                                     </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
          <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
