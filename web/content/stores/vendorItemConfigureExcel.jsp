<%--
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
           Date dNow = new Date();
           SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
           //System.out.println("Current Date: " + ft.format(dNow));
           String curDate = ft.format(dNow);
           String expFile = "vendorItemConfigure-"+curDate+".xls";

           String fileName = "attachment;filename=" + expFile;
           response.setContentType("application/vnd.ms-excel;charset=UTF-8");
           response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${vendorItemList != null}" >

                <table style="width: 1100px" align="center" border="1">
                    <thead>
                        <tr >
                            <th>SNo</th>
                            <th>MFRItemcode</th>
                            <th>ItemCode</th>
                            <th>ItemName</th>
                            <th>UnitPrice</th>
                            <th>OfferedDiscount(%age)</th>
                            <th>BuyingPrice</th>
                            <th>CGST (%)</th>
                            <th>SGST (%)</th>
                            <th>IGST (%)</th>
                            <th>Landing Cost</th>
                        </tr>
                    </thead>
                    <% int index = 0;int sno = 1;%>
                    <tbody>
                        <c:forEach items="${vendorItemList}" var="list">
                            <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                            %>
                            <tr>
                                <td align="left"  ><%=sno%></td>
                                <td align="left"  ><c:out value="${list.mfrCode}"/></a></td>
                                <td align="left"  ><c:out value="${list.paplCode}"/></a></td>
                                <td align="left"  ><c:out value="${list.itemName}"/></a></td>
                                <td align="left"  ><c:out value="${list.unitPrice}"/></td>
                                <td align="left"  ><c:out value="${list.discount}"/></td>
                                <td align="left"  ><c:out value="${list.buyPrice}"/></td>
                                <td align="left"  ><c:out value="${list.cgstPercent}"/></td>
                                <td align="left"  ><c:out value="${list.sgstPercent}"/></td>
                                <td align="left"  ><c:out value="${list.igstPercent}"/></td>
                                <td align="left"  ><fmt:formatNumber value="${(list.buyPrice)+(list.buyPrice*(list.igstPercent/100))}" pattern="##.00"/></td>


                                <%index++;%>
                                <%sno++;%>
                            </c:forEach>
                    </tbody>
                </table>
            </c:if>

            <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
