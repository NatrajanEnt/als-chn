
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>




<script type="text/javascript">
    function submitPage(value) {
        var statusId=document.getElementById('statusId').value;
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/movementTypeStatusReport.do?param=ExportExcel&statusId='+statusId;
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/movementTypeStatusReport.do?param=Search&statusId='+statusId;
                document.accountReceivable.submit();
            }
        }
    }
   

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Movement Status Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValue();
            sorter.size(20);">
                <form name="accountReceivable"   method="post">
                    
                    <%@ include file="/content/common/message.jsp" %>
                    <table style="width:100%" align="center" border="0"  class="table table-info mb30 table-hover"   >
                       
                        <tr height="30"   >
                            <td colSpan="8" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Movement status Report</td></tr>
                             <tr height="40">
                            <td align="left" >From Date</td>
                            <td height="30" ><input name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>" type="text" class="form-control datepicker" style="width: 200px"></td>
                            <td align="left">To Date</td>
                            <td height="30" ><input name="toDate" id="toDate" value="<c:out value="${toDate}" />" type="text" class="form-control datepicker" style="width: 200px" ></td>
                            <td align="left">Movement Type</td>
                            <td height="30" >
                                <select id="statusId" name="statusId" style="width: 200px;height: 30px" >
                                    <c:if test="${statusId==1}">
                                    <option value="1" selected>Empty In/Out </option>
                                    <option value="2">CFS Port In/Out </option>
                                    <option value="3">Factory In/Out </option>
                                    </c:if>
                                    <c:if test="${statusId==2}">
                                    <option value="1">Empty In/Out </option>
                                    <option value="2" selected>CFS Port In/Out </option>
                                    <option value="3">Factory In/Out </option>
                                    </c:if>
                                    <c:if test="${statusId==3}">
                                    <option value="1">Empty In/Out </option>
                                    <option value="2">CFS Port In/Out </option>
                                    <option value="3" selected>Factory In/Out </option>
                                    </c:if>
                                </select>
                            </td>
                            <td height="30" >
                                
                        </tr>
                        <tr height="40" align="center">
                            <td colspan="8" align="center"> <input type="button" class="btn btn-info" name="ExportExcel" id="ExportExcel"  value="<spring:message code="sales.label.customer.Export Excel"  text="Export Excel"/>" onclick="submitPage(this.name);" >&nbsp;&nbsp;
                                <input type="button" class="btn btn-info" name="Search"  id="Search"  value="<spring:message code="sales.label.customer.search"  text="Search"/>" onclick="submitPage(this.name);" ></td>
                        </tr>
                    </table>
                    <br>
                    <c:if test="${movementStatusReportDetails == null}">
                        <center>
                            <font color="red">No Records</font>
                        </center>
                    </c:if>
                    <c:if test="${movementStatusReportDetails != null}">
                        <table align="center" id="table" class="table table-info mb30 table-hover"style="width:100%;" >
                            <c:if test="${statusId==1}">
                            <thead height="30">
                                <tr>
                                    <th align="center" rowspan="2">Sno</th>
                                    <th align="center" rowspan="2">Date</th>
                                    <th align="center" rowspan="2">Day</th>
                                    <th align="center" colspan="3">DEPOT Gate in Before ….</th>
                                    <th align="center" rowspan="2">TOTAL NO OF EXPORT MOVEMENTS using CA Log vehicles</th>
                                    <th align="center" colspan="3">DWELL TIME @ Depot</th>
                                </tr> <tr>
                                    <th align="center">Before 3 PM</th>
                                    <th align="center">3 PM to 5:30 PM</th>
                                    <th align="center">after 5:30 PM</th>
                                    <th align="center">Less than 1 hr</th>
                                    <th align="center">1 to 2 hrs</th>
                                    <th align="center">abv 2 hrs</th>
                                </tr>
                            </thead>
                            </c:if>
                            <c:if test="${statusId==2}">
                            <thead height="30">
                                <tr>
                                    <th align="center" rowspan="2">Sno</th>
                                    <th align="center" rowspan="2">Date</th>
                                    <th align="center" rowspan="2">Day</th>
                                    <th align="center" colspan="3">DEPOT Gate in Before ….<br>Port Gate(DPD) in Before…</th>
                                    <th align="center" rowspan="2">TOTAL NO OF EXPORT MOVEMENTS using CA Log vehicles</th>
                                    <th align="center" colspan="3">DWELL TIME @ CFS<br>DWELL TIME @ Terminal(DPD)</th>
                                </tr> <tr>
                                    <th align="center">Before 3 PM</th>
                                    <th align="center">3 PM to 5:30 PM</th>
                                    <th align="center">after 5:30 PM</th>
                                    <th align="center">Less than 1 hr</th>
                                    <th align="center">1 to 2 hrs</th>
                                    <th align="center">abv 2 hrs</th>
                                </tr>
                            </thead>
                            </c:if>
                            <c:if test="${statusId==3}">
                            <thead height="30">
                                <tr>
                                    <th align="center" rowspan="2">Sno</th>
                                    <th align="center" rowspan="2">Date</th>
                                    <th align="center" rowspan="2">Day</th>
                                    <th align="center" colspan="3">FACTORY Gate in Before ….</th>
                                    <th align="center" rowspan="2">TOTAL NO OF EXPORT / IMPORT MOVEMENTS using CA Log Vehicles</th>
                                    <th align="center" colspan="3">DWELL TIME @ Factory</th>
                                </tr> <tr>
                                    <th align="center">Before 9 AM</th>
                                    <th align="center">9 AM to 10:30 AM</th>
                                    <th align="center">after 10:30 AM</th>
                                    <th align="center">Less than 3 hr</th>
                                    <th align="center">3 to 4 hrs</th>
                                    <th align="center">abv 4 hrs</th>
                                </tr>
                            </thead>
                            </c:if>
                            <tbody>
                                <%int i=0;%>
                                <c:forEach items="${movementStatusReportDetails}" var="ts">
                                    <%i++;%>
                                    <tr>
                                        <td><%=i%></td>
                                        <td><c:out value="${ts.date}"/></td>
                                        <td><c:out value="${ts.day}"/></td>
                                        <td><c:out value="${ts.timeSlot1}"/></td>
                                        <td><c:out value="${ts.timeSlot2}"/></td>
                                        <td><c:out value="${ts.timeSlot3}"/></td>
                                        <td><c:out value="${ts.totalCount}"/></td>
                                        <td><c:out value="${ts.timeSlot4}"/></td>
                                        <td><c:out value="${ts.timeSlot5}"/></td>
                                        <td><c:out value="${ts.timeSlot6}"/></td>
                                    </tr>

                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" >5</option>
                                <option value="10">10</option>
                                <option value="20" selected="selected">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>    
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
