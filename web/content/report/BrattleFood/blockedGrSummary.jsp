<%@page import="java.text.DecimalFormat"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });


            function replaceSpecialCharacters()
            {
                var content = document.getElementById("requestremarks").value;

                //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
                // content=content.replace(/[^a-zA-Z0-9]/g,'');
                content=content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g,' ');
                document.getElementById("requestremarks").value=content;
                //alert("Repalced");

            }
        </script>

    </head>

    <script type="text/javascript">
        function submitPage(value) {
            if (document.getElementById('fromDate').value == '') {
                alert("Please select from Date");
                document.getElementById('fromDate').focus();
            } else if (document.getElementById('toDate').value == '') {
                alert("Please select to Date");
                document.getElementById('toDate').focus();
            } else {
                if (value == 'ExportExcel') {
                    document.summary.action = '/throttle/handleBlockedGrSummaryReport.do?param=ExportExcel';
                    document.summary.submit();
                } else {
                    document.summary.action = '/throttle/handleBlockedGrSummaryReport.do?param=Search';
                    document.summary.submit();
                }
            }
        }
    </script>


    <body>
        <form name="summary"  method="post" >
            <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
            <%@ include file="/content/common/message.jsp" %>

<br/>
<br/>
<br/>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Blocked GR Summary</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td>Customer</td>
                                        <td height="30">
                                            <select class="textbox" name="customerId" id="customerId"  style="width:125px;">
                                                <option value="">---Select---</option>
                                                <c:forEach items="${customerList}" var="customerList">
                                                    <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                                </c:forEach>
                                            </select>
                                            <script>
                                                document.getElementById('customerId').value = <c:out value="${customerId}"/> ;
                                            </script>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:129px;" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" style="width:129px;" value="<c:out value="${toDate}"/>"></td>
                                    </tr>


                                    <tr>
                                        <td colspan="4" align="center"><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;<input type="button" class="button" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>

            <br>
            <br>
            <br>

            <c:if test="${blockedGrList != null}">


                <table align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr>
                        <th><h3 align="center">S.No</h3></th>
                        <th><h3 align="center">CustomerName</h3></th>
                        <th><h3 align="center">GrDate</h3></th>
                        <th><h3 align="center">BlockedGR</h3></th>
                        <th><h3 align="center">UsedGR</h3></th>
                        <th><h3 align="center">NotUsedGR</h3></th>
                        <th><h3 align="center">CancelledGR</h3></th>
                        <th><h3 align="center">Tot.BlockedGR</h3></th>
                        <th><h3 align="center">Tot.UsedGR</h3></th>
                        <th><h3 align="center">Tot.NotUsedGR</h3></th>
                        <th><h3 align="center">Tot.CancelledGR</h3></th>
                        <th><h3 align="center">CancelledBy </h3></th>
                        <th><h3 align="center">Remarks </h3></th>
                    </tr>
                    </thead>
                    <%int index1 = 1;%>
                    <c:set var="totalBlocked" value="0"/>
                    <c:set var="totalNotUsed" value="0"/>
                    <c:set var="totalUsed" value="0"/>
                    <c:set var="totalCancelled" value="0"/>
                    <c:forEach items="${blockedGrList}" var="blockGr">
                        <tr>
                            <c:set var="totalBlocked" value="${totalBlocked + blockGr.totBlockedGr}"/>
                            <c:set var="totalNotUsed" value="${totalNotUsed + blockGr.totNotUsedGr}"/>
                            <c:set var="totalUsed" value="${totalUsed + blockGr.totUsedGr}"/>
                            <c:set var="totalCancelled" value="${totalCancelled + blockGr.totCancelledGr}"/>
                            <td ><%=index1++%></td>
                            <td ><c:out value="${blockGr.custName}"/></td>
                            <td > <c:out value="${blockGr.grDate}"/> </td>
                            <td > <c:out value="${blockGr.blockedGr}"/></td>
                            <td > <c:out value="${blockGr.usedGr}"/> </td>
                            <td > <c:out value="${blockGr.notUsedGr}"/> </td>
                            <td > <c:out value="${blockGr.cancelledGr}"/> </td>
                            <td > <c:out value="${blockGr.totBlockedGr}"/> </td>
                            <td > <c:out value="${blockGr.totUsedGr}"/> </td>
                            <td > <c:out value="${blockGr.totNotUsedGr}"/> </td>
                            <td > <c:out value="${blockGr.totCancelledGr}"/> </td>
                            <td > <c:out value="${blockGr.createdBy}"/> </td>
                            <td > <c:out value="${blockGr.remarks}"/> </td>
                        </tr>
                    </c:forEach>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                            <td colspan="2" align="center">Total</td>
                            <td ><c:out value="${totalBlocked}"/></td>
                            <td ><c:out value="${totalNotUsed}"/></td>
                            <td ><c:out value="${totalUsed}"/></td>
                            <td ><c:out value="${totalCancelled}"/></td>
                            <td colspan="2">&nbsp;</td>
                        </tr>



                </table>


            </c:if>




        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
