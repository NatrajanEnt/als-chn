<%-- 
    Document   : SearchAdvanceDetails
    Created on : Nov 4, 2013, 12:12:26 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        
        

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });


             function submitPage(value){
                document.AdvanceDetails.action='/throttle/BrattleFoods/SearchAdvanceDetails.jsp';
                document.AdvanceDetails.submit();
            }
             function excelPage(){
                document.AdvanceDetails.action='/throttle/BrattleFoods/SearchAdvanceDetailsExcel.jsp';
                document.AdvanceDetails.submit();
            }
        </script>
        
    </head>
     
    <body>
        <form name="AdvanceDetails" method="post">
            <%@ include file="/content/common/path.jsp" %>

            <%@ include file="/content/common/message.jsp" %>

            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Advance Details</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >

                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td colspan="2"><input type="button" class="button"   value="Search" onclick="submitPage(this.value)"></td>
                                        <td colspan="2"><input type="button" class="button"   value="Generate Excel" onclick="excelPage()"></td>
                                    </tr>
                                    <tr >
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >                <tr>
                    <td class="table">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
                            <tr>
                                <td class="bottom" align="left"><img src="/throttle/images/left_status.jpg" alt=""  /></td>
                                <td class="bottom" height="35" align="right"><img src="/throttle/images/icon_active.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left">4</span></td>
                                <td class="bottom" align="right"><img src="/throttle/images/icon_closed.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;">2</span></td>
                                <td class="bottom" align="center"><h2>Total  6</h2></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="980"  border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">

                        <td rowspan="2" class="contenthead">S.No</td>
                        <td rowspan="2" class="contenthead">TripSheetId</td>
                        <td rowspan="2" class="contenthead">Trip Date &amp; Time</td>
                        <td rowspan="2" class="contenthead">Estimated Delivery Date</td>
                        <td rowspan="2" class="contenthead">Transit Days</td>
                        <td rowspan="2" class="contenthead">Vechile No</td>
                        <td rowspan="2" class="contenthead">Driver No</td>
                        <td rowspan="2" class="contenthead">Account No</td>
                        <td rowspan="2" class="contenthead">Last Advance </td>
                        <td rowspan="2" class="contenthead">Advance Account</td>
                        <td rowspan="2" class="contenthead">Status</td>
                    </tr>
                </thead>

                <tr>
                    <td class="text1">1</td>
                    <td class="text1">12411</td>
                    <td class="text1">19:11:2013 &amp; 7:00AM</td>
                    <td class="text1">21:11:2013 &amp; 5:00PM</td>
                    <td class="text1">2.5 Days</td>
                    <td class="text1">DL 19 AB 1099</td>
                    <td class="text1">Prem</td>
                    <td class="text1">139785462789456</td>
                    <td class="text1">10000</td>
                    <td class="text1">4500</td>
                    <td class="text1"><a href="/throttle/BrattleFoods/tripSheet.jsp?advance=4">Proceed</a></td>
                </tr>
                <tr>
                    <td class="text2">2</td>
                    <td class="text2">23231</td>
                    <td class="text2">10:11:2013 &amp; 7:00AM</td>
                    <td class="text2">13:11:2013 &amp; 5:00PM</td>
                    <td class="text2">2.5 Days</td>
                    <td class="text2">DL 18 AB 5056</td>
                    <td class="text2">Senthil </td>
                    <td class="text2">156894567894562</td>
                    <td class="text2">50000</td>
                    <td class="text2">1500</td>
                    <td class="text2"><a href="/throttle/BrattleFoods/tipSheet.jsp">Proceed</a></td>
                </tr>
                    <tr>
                        <td class="text1">3</td>
                        <td class="text1">13636</td>
                        <td class="text1">19:10:2013 &amp; 7:00AM</td>
                        <td class="text1">21:10:2013 &amp; 7:00PM</td>
                        <td class="text1">2.0 Days</td>
                        <td class="text1">TN 09 QX 9098</td>
                        <td class="text1">Kumar</td>
                        <td class="text1">896478945612354</td>
                        <td class="text1">12000</td>
                        <td class="text1">3000</td>
                        <td class="text1">Proceeded</td>
                    </tr>
                    <tr>
                        <td class="text2">4</td>
                        <td class="text2">12132</td>
                        <td class="text2">19:11:2013 &amp; 7:00AM</td>
                        <td class="text2">21:11:2013 &amp; 7:00PM</td>
                        <td class="text2">2.5 Days</td>
                        <td class="text2">DL 20 FG 2346</td>
                        <td class="text2">Tamil  </td>
                        <td class="text2">567889546598956</td>
                        <td class="text2">40200</td>
                        <td class="text2">1213</td>
                        <td class="text2"><a href="/throttle/BrattleFoods/tipSheet.jsp">Proceed</a></td>
                    </tr>
                    <tr>
                        <td class="text1">5</td>
                        <td class="text1">11232</td>
                        <td class="text1">19:11:2013 &amp; 7:00AM</td>
                        <td class="text1">21:11:2013 &amp; 10:00PM</td>
                        <td class="text1">2.5 Days</td>
                        <td class="text1">DL 34 RT 2446</td>
                        <td class="text1">selvam</td>
                        <td class="text1">568975468923597</td>
                        <td class="text1">58000</td>
                        <td class="text1">4345</td>
                        <td class="text1"><a href="/throttle/BrattleFoods/tipSheet.jsp">Proceed</a></td>
                    </tr>
                    <tr>
                        <td class="text2">6</td>
                        <td class="text2">12422</td>
                        <td class="text2">10:10:2013 &amp; 8:00AM</td>
                        <td class="text2">13:10:2013 &amp; 8:00AM</td>
                        <td class="text2">3.0 Days</td>
                        <td class="text2">DL 24 CG 5356</td>
                        <td class="text2">madhan</td>
                        <td class="text2">865234987456128</td>
                        <td class="text2">20000</td>
                        <td class="text2">2300</td>
                        <td class="text2">Proceeded</td>
                    </tr>
            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
