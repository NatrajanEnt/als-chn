<%-- 
    Document   : GRSummaryReportExcel
    Created on : Feb 15, 2016, 2:09:44 PM
    Author     : hp
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<html>
    <head>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "BlockedGrSummaryReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
             <c:if test="${blockedGrList != null}">


                <table align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr>
                        <th><h3 align="center">S.No</h3></th>
                        <th><h3 align="center">CustomerName</h3></th>
                        <th><h3 align="center">GrDate</h3></th>
                        <th><h3 align="center">BlockedGR</h3></th>
                        <th><h3 align="center">UsedGR</h3></th>
                        <th><h3 align="center">NotUsedGR</h3></th>
                        <th><h3 align="center">CancelledGR</h3></th>
                        <th><h3 align="center">Tot.BlockedGR</h3></th>
                        <th><h3 align="center">Tot.UsedGR</h3></th>
                        <th><h3 align="center">Tot.NotUsedGR</h3></th>
                        <th><h3 align="center">Tot.CancelledGR</h3></th>
                        <th><h3 align="center">CancelledBy </h3></th>
                        <th><h3 align="center">Remarks </h3></th>
                    </tr>
                    </thead>
                    <%int index1 = 1;%>
                    <c:set var="totalBlocked" value="0"/>
                    <c:set var="totalNotUsed" value="0"/>
                    <c:set var="totalUsed" value="0"/>
                    <c:set var="totalCancelled" value="0"/>
                    <c:forEach items="${blockedGrList}" var="blockGr">
                        <tr>
                            <c:set var="totalBlocked" value="${totalBlocked + blockGr.totBlockedGr}"/>
                            <c:set var="totalNotUsed" value="${totalNotUsed + blockGr.totNotUsedGr}"/>
                            <c:set var="totalUsed" value="${totalUsed + blockGr.totUsedGr}"/>
                            <c:set var="totalCancelled" value="${totalCancelled + blockGr.totCancelledGr}"/>
                            <td ><%=index1++%></td>
                            <td ><c:out value="${blockGr.custName}"/></td>
                            <td > <c:out value="${blockGr.grDate}"/> </td>
                            <td > <c:out value="${blockGr.blockedGr}"/></td>
                            <td > <c:out value="${blockGr.usedGr}"/> </td>
                            <td > <c:out value="${blockGr.notUsedGr}"/> </td>
                            <td > <c:out value="${blockGr.cancelledGr}"/> </td>
                            <td > <c:out value="${blockGr.totBlockedGr}"/> </td>
                            <td > <c:out value="${blockGr.totUsedGr}"/> </td>
                            <td > <c:out value="${blockGr.totNotUsedGr}"/> </td>
                            <td > <c:out value="${blockGr.totCancelledGr}"/> </td>
                            <td > <c:out value="${blockGr.createdBy}"/> </td>
                            <td > <c:out value="${blockGr.remarks}"/> </td>
                        </tr>
                    </c:forEach>
                        <tr>
                            <td colspan="5">&nbsp;</td>
                            <td colspan="2" align="center">Total</td>
                            <td ><c:out value="${totalBlocked}"/></td>
                            <td ><c:out value="${totalNotUsed}"/></td>
                            <td ><c:out value="${totalUsed}"/></td>
                            <td ><c:out value="${totalCancelled}"/></td>
                            <td colspan="2">&nbsp;</td>
                        </tr>



                </table>


            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
