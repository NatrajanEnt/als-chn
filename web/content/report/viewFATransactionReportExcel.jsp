<%--
    Document   : ViewTransactionReport
    Created on : jul 31, 2020, 1:48:05 PM
    Author     : hp
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>
         <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>
    <form name="tripSheet" method="post">
        <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TransactionReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

        <c:if test="${transactionsDetails != null}">
            <table align="center" border="1" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="70">
                        <th>Sno</th>
                        <th width="120">Job Id</th>
                        <th width="240">SRLNO</th>
                        <th width="240">CHQNO</th>
                        <th width="120">NARRATION1</th>
                        <th width="120">NARRATION2</th>
                        <th width="60">ACCCODE</th>
                        <th width="280">SUBCODE</th>
                        <th width="120">DEBIT</th>
                        <th width="120">CREDIT</th>
                        <th width="120">VEHICLE NO</th>
                        <th width="120">LICENSE NO</th>
                        <th width="120">EXPENSE NAME</th>

                    </tr>
                </thead>
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${transactionsDetails}" var="tripDetails">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr height="30">
                            <td width="40" align="left"><%=index%></td>
                            <td width="40" align="left"><c:out value="${tripDetails.trlJobId}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.SRLNO}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.CHQNO}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.NARRATION1}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.NARRATION2}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.ACCCODE}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.SUBCODE}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.DEBIT}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.CREDIT}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.vehicleNo}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.licenseNo}"/></td>
                            <td width="40" align="left"><c:out value="${tripDetails.expenseId}"/></td>
                        </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
