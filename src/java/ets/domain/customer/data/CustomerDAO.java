package ets.domain.customer.data;

/**
 *
 * @author vidya
 */
import com.ibatis.sqlmap.client.SqlMapClient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.customer.business.CustomerTO;
import ets.domain.util.ThrottleConstants;

;

public class CustomerDAO extends SqlMapClientDaoSupport {

    private final int errorStatus = 4;
    private final static String CLASS = "CustomerDAO";

    public int doInsertCustomer(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int insertContractRouteMater = 0;
        map.put("customerId", insertContractRouteMater);
        String custCode = "";
        String[] temp1 = null;
        custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        if (custCode == null) {
            custCode = "CC-0001";
        } else if (custCode.length() == 1) {
            custCode = "CC-000" + custCode;
        } else if (custCode.length() == 2) {
            custCode = "CC-00" + custCode;
        } else if (custCode.length() == 3) {
            custCode = "CC-0" + custCode;
        } else if (custCode.length() == 4) {
            custCode = "CC-" + custCode;
        }
        map.put("custCode", custCode);
        map.put("erpId", customerTO.getErpId());
        map.put("panNo", customerTO.getPanNo());
        map.put("companyType", customerTO.getCompanyType());
        map.put("tallyBillingType", customerTO.getTallyBillingType());

        if ("1".equalsIgnoreCase(customerTO.getCustomerTypeId())) {
            map.put("paymentType", customerTO.getPaymentType());
            map.put("Code", customerTO.getCustomerCode());
            map.put("Name", customerTO.getCustName());
            map.put("cityId", customerTO.getCityId());
            map.put("compId", customerTO.getCompId());
            map.put("billingTypeId", customerTO.getBillingTypeId());
            map.put("Address", customerTO.getCustAddress());
            map.put("Addresstwo", customerTO.getCustAddresstwo());
            map.put("Addressthree", customerTO.getCustAddressthree());
            map.put("City", customerTO.getCustCity());
            map.put("Contact", customerTO.getCustContactPerson());
            map.put("State", customerTO.getCustState());
            map.put("Phone", customerTO.getCustPhone());
            map.put("Mobile", customerTO.getCustMobile());
            map.put("Email", customerTO.getCustEmail());
            map.put("creditDays", customerTO.getCreditDays());
            map.put("creditLimit", customerTO.getCreditLimit());
            map.put("accountManagerId", customerTO.getAccountManagerId());
            map.put("secondaryBillingTypeId", customerTO.getSecondaryBillingTypeId());
            map.put("billingNameAddress", customerTO.getBillingNameAddress());
            map.put("customerTypeId", customerTO.getCustomerTypeId());
            map.put("customerGroupId", customerTO.getCustomerGroupId());
            map.put("stateId", customerTO.getStateId());
            map.put("gstNo", customerTO.getGstNo());
            map.put("custTypeId",customerTO.getCustTypeid());

            map.put("organizationId", customerTO.getOrganizationId());
            //        map.put("tancem", customerTO.getTancem());
            map.put("userId", userId);
            System.out.print("customerTO.getCustName()" + customerTO.getCustName());
        } else {
            map.put("paymentType", "1");
            map.put("Code", customerTO.getCustomerCode());
            map.put("Name", customerTO.getCustName());
            map.put("billingTypeId", "0");
            map.put("Address", customerTO.getCustAddress());
            map.put("compId", 0);
            map.put("Addresstwo", customerTO.getCustAddresstwo());
            map.put("Addressthree", customerTO.getCustAddressthree());
            map.put("City", customerTO.getCustCity());
            map.put("Contact", "ADMIN");
            map.put("State", customerTO.getCustState());
            map.put("Phone", "0");
            map.put("Mobile", customerTO.getCustMobile());
            map.put("Email", customerTO.getCustEmail());
            map.put("creditDays", "0");
            map.put("creditLimit", "0");
            map.put("accountManagerId", "0");
            map.put("secondaryBillingTypeId", "0");
            map.put("billingNameAddress", customerTO.getBillingNameAddress());
            map.put("customerTypeId", customerTO.getCustomerTypeId());
            map.put("customerGroupId", "0");
            //        map.put("tancem", customerTO.getTancem());
            map.put("userId", userId);
            map.put("stateId", customerTO.getStateId());
            map.put("cityId", customerTO.getCityId());
            map.put("gstNo", customerTO.getGstNo());
            map.put("organizationId", customerTO.getOrganizationId());
            map.put("custTypeId",customerTO.getCustTypeid());
        }
        try {
            System.out.println("test1" + status);
            //            if ("2".equals(customerTO.getCustomerType())) {
            String code = (String) getSqlMapClientTemplate().queryForObject("customer.getLedgerCode", map);
            String[] temp = code.split("-");
            System.out.println("temp[1] = " + temp[1]);
            int codeval = Integer.parseInt(temp[1]);
            int codev = codeval + 1;
            System.out.println("codev = " + codev);
            String ledgercode = "LEDGER-" + codev;
            System.out.println("ledgercode = " + ledgercode);
            map.put("ledgercode", ledgercode);

            //current year and month start
            String accYear = (String) getSqlMapClientTemplate().queryForObject("customer.accYearVal", map);
            System.out.println("accYear:" + accYear);
            map.put("accYear", ThrottleConstants.financialYear);
            map.put("customerGroupCode", ThrottleConstants.customerGroupCode);
            map.put("customerLevelId", ThrottleConstants.customerLevelId);
            //current year end

            status = (Integer) getSqlMapClientTemplate().insert("customer.insertCustomerLedger", map);
            map.put("ledgerId", status);
            //            } else {
            //                map.put("ledgerId", "0");
            //            }
            System.out.println("map" + map);
            if ("1".equals(customerTO.getBillingCustFlag())) {
                map.put("billingCustName", "0");
            } else {
                map.put("billingCustName", customerTO.getBillingCustId());
            }

            status = (Integer) getSqlMapClientTemplate().insert("customer.insertCustomer", map);

            map.put("customerId", status);
//            if("1".equals(customerTO.getConsigId())){
//                status = (Integer) getSqlMapClientTemplate().insert("customer.insertConsignor", map);
//            }
//           if("2".equals(customerTO.getConsigId())){
//                status = (Integer) getSqlMapClientTemplate().insert("customer.insertConsignee", map);
//           }

//            if (!"0".equals(customerTO.getSecondaryBillingTypeId())) {
//                int insertOperationPointForSecondary = (Integer) getSqlMapClientTemplate().update("customer.insertOperationPointForSecondary", map);
//            }
            String consigType = customerTO.getCustTypeid();
            System.out.println("consigType====" + consigType);

            map.put("Name", customerTO.getCustName());
            map.put("Contact", customerTO.getCustContactPerson());
            map.put("Address", customerTO.getCustAddress());
            map.put("Addresstwo", customerTO.getCustAddresstwo());
            map.put("stateId", customerTO.getStateId());
            map.put("cityId", customerTO.getCityId());
            map.put("Contact", customerTO.getCustContactPerson());
            map.put("State", customerTO.getCustState());
            map.put("Phone", customerTO.getCustPhone());
            map.put("Mobile", customerTO.getCustMobile());
            map.put("Email", customerTO.getCustEmail());
            map.put("createdby", userId);
            
            String consignorCode = "CSR";
            String consignorId = "";
            String consigneeCode = "CSE";
            String consigneeId = "";
            
            consignorId = (String) getSqlMapClientTemplate().queryForObject("customer.getConsignorCode", map);
            consignorCode = consignorCode + consignorId;
            map.put("consignorCode", consignorCode);
            
            consigneeId = (String) getSqlMapClientTemplate().queryForObject("customer.getConsigneeCode", map);
            consigneeCode = consigneeCode + consigneeId;
            map.put("consigneeCode", consigneeCode);
            
            System.out.println("consigType----"+ consigType);
            System.out.println("ConsignorCode" + consignorCode);
            System.out.println("ConsigneeCode" + consigneeCode);
            if (consigType.contains(",")) {
                System.out.println("if");
                status = (Integer) getSqlMapClientTemplate().insert("customer.insertConsignor", map);
                System.out.println("insertConsignor---"+status);
                status = (Integer) getSqlMapClientTemplate().insert("customer.insertConsignee", map);
                System.out.println("insertConsignee---"+status);

            }else{
                System.out.println("else");
                if ("1".equals(customerTO.getCustTypeid())) {
                    status = (Integer) getSqlMapClientTemplate().insert("customer.insertConsignor", map);
                } else if ("2".equals(customerTO.getCustTypeid())) {
                    status = (Integer) getSqlMapClientTemplate().insert("customer.insertConsignee", map);
                }
            } 

            if ("1".equals(customerTO.getBillingCustFlag())) {
                map.put("billingCustName", status);
                int update = (Integer) getSqlMapClientTemplate().update("customer.updateCustBillingId", map);
                System.out.println("update==" + update);
            }
            System.out.println("map in the customer adding screen = " + map);
            int insertCustomerOutStand = 0;
            insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerOutStand", map);
            insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerOutStandDetails", map);
            int insertCustomerSecondary = 0;
            if (customerTO.getSecondaryBillingTypeId() != null && !customerTO.getSecondaryBillingTypeId().equals("0")) {
                insertCustomerOutStand = (Integer) getSqlMapClientTemplate().update("customer.insertCustomerSecondaryApproval", map);
            }

            System.out.println("status customer details is is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public ArrayList getLPSCustomerList() {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getLPSCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public int doUpdateCustomer(ArrayList List, int userId) {
        Map map = new HashMap();
        int status = 0;
        try {
            Iterator itr = List.iterator();
            CustomerTO customerTO = null;
            while (itr.hasNext()) {
                customerTO = (CustomerTO) itr.next();
                System.out.println("getCustId" + customerTO.getCustId());
                System.out.println("custName" + customerTO.getCustName());
                System.out.println("custContact" + customerTO.getCustContactPerson());
                System.out.println("custAddress" + customerTO.getCustAddress());
                System.out.println("customerTO.getCustCity());" + customerTO.getCustCity());
                System.out.println("custState" + customerTO.getCustState());
                System.out.println("custPhone" + customerTO.getCustPhone());
                System.out.println("custMobile" + customerTO.getCustMobile());
                System.out.println("custStatus" + customerTO.getCustStatus());
                map.put("custId", customerTO.getCustId());
                map.put("custName", customerTO.getCustName());
                map.put("custType", customerTO.getCustomerType());
                map.put("custContact", customerTO.getCustContactPerson());
                map.put("custAddress", customerTO.getCustAddress());
                map.put("custCity", customerTO.getCustCity());
                map.put("custState", customerTO.getCustState());
                map.put("custPhone", customerTO.getCustPhone());
                map.put("custMobile", customerTO.getCustMobile());
                map.put("custEmail", customerTO.getCustEmail());
                map.put("custStatus", customerTO.getCustStatus());
                map.put("tancem", customerTO.getTancem());
                map.put("userId", userId);
                map.put("erpId", customerTO.getErpId());
                status = (Integer) getSqlMapClientTemplate().update("customer.updateCustomer", map);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-03", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }

    public int doInsertContract(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int ManufacturerId = 1023;
        int ModelId = 1512;
        map.put("ManufacturerId", ManufacturerId);
        map.put("ModelId", ModelId);
        map.put("Spare", customerTO.getContPercentAgeUpSpares());
        map.put("Labour", customerTO.getContPercentAgeUpLabour());
        map.put("Status", customerTO.getContStatus());
        map.put("userId", userId);
        System.out.print("customerTO.getContManufacturerName()" + customerTO.getContPercentAgeUpLabour());
        try {
            status = (Integer) getSqlMapClientTemplate().update("customer.insertContract", map);
            System.out.println("status is" + status);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CONT-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getBunkList(String bunkName) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            System.out.println("bunkName.." + bunkName);
            if (bunkName.length() != 0) {
                map.put("bunkname", bunkName);
            }

            System.out.println("map.size().." + map.size());
            if (map.size() > 0) {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getselBunk", map);
            } else {
                bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBunkList", map);
            }
            System.out.println("bunkselList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doInsertBunk(CustomerTO customerTO, int userId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        int bunkid = 0;
        int id = 0;

        try {
            id = (Integer) getSqlMapClientTemplate().queryForObject("customer.getMaxIDList", mapmaster);
            String FuelLocateId = customerTO.getBunkName().substring(0, 1) + id;

            System.out.println("FuelLocateId" + FuelLocateId);

            mapmaster.put("FuelLocateId", FuelLocateId);
            mapmaster.put("bunkname", customerTO.getBunkName());
            mapmaster.put("location", customerTO.getCurrlocation());
            mapmaster.put("state", customerTO.getBunkState());
            mapmaster.put("act_ind", customerTO.getBunkStatus());
            mapmaster.put("createdby", userId);

            System.out.println("test1" + status);
            int bunkId = (Integer) getSqlMapClientTemplate().insert("customer.insertBunkmaster", mapmaster);
            System.out.println("bunkId master is" + bunkId);

            //Ledger Code start
            if (bunkId != 0) {
                String code = (String) getSqlMapClientTemplate().queryForObject("customer.getLedgerCode", mapmaster);
                String[] temp = code.split("-");
                int codeval = Integer.parseInt(temp[1]);
                int codev = codeval + 1;
                String ledgercode = "LEDGER-" + codev;
                mapmaster.put("ledgercode", ledgercode);

                //current year and month start
                String accYear = (String) getSqlMapClientTemplate().queryForObject("customer.accYearVal", mapmaster);
                System.out.println("accYear:" + accYear);
                mapmaster.put("accYear", accYear);
                //current year end

                String bunkName = customerTO.getBunkName() + "-" + bunkId;
                System.out.println("bunkName =====> " + bunkName);

                int VendorLedgerId = (Integer) getSqlMapClientTemplate().insert("customer.insertVendorLedger", mapmaster);
                System.out.println("VendorLedgerId......." + VendorLedgerId);
                if (VendorLedgerId != 0) {
                    mapmaster.put("ledgerId", VendorLedgerId);
                    mapmaster.put("bunkId", bunkId);
                    System.out.println("map update ::::=> " + mapmaster);
                    status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkInfo", mapmaster);
                }
            }
            //Ledger Code end

            //bunkid = (Integer) getSqlMapClientTemplate().queryForObject("customer.getBunkselectedList", mapmaster);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", customerTO.getFuelType());
            mapdetail.put("currentrate", customerTO.getCurrRate());
            mapdetail.put("bunkremarks", customerTO.getRemarks());
            mapdetail.put("act_ind", customerTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("test2" + status);
            status = (Integer) getSqlMapClientTemplate().update("customer.insertBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getBunkalterList(String bunkId) {
        Map map = new HashMap();
        ArrayList bunkList = new ArrayList();
        try {
            if (bunkId != null) {
                map.put("bunkid", bunkId);
            }
            bunkList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getalterBunk", map);

            System.out.println("bunkList size=" + bunkList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "bunkList", sqlException);
        }
        return bunkList;
    }

    public int doUpdateBunk(CustomerTO customerTO, int userId, String bunkId) {
        Map mapmaster = new HashMap();
        Map mapdetail = new HashMap();
        Map map = new HashMap();
        ArrayList bunkmasterList = new ArrayList();
        int status = 0;
        mapmaster.put("bunkname", customerTO.getBunkName());
        mapmaster.put("location", customerTO.getCurrlocation());
        mapmaster.put("state", customerTO.getBunkState());
        mapmaster.put("act_ind", customerTO.getBunkStatus());
        mapmaster.put("createdby", userId);
        mapmaster.put("bunkid", bunkId);

        try {
            System.out.println("test1=" + status);
            status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkmaster", mapmaster);
            System.out.println("status master is=" + status);
            System.out.println("bunkid..." + bunkId);
            mapdetail.put("bunkid", bunkId);
            mapdetail.put("fueltype", customerTO.getFuelType());
            mapdetail.put("currentrate", customerTO.getCurrRate());
            mapdetail.put("bunkremarks", customerTO.getRemarks());
            mapdetail.put("act_ind", customerTO.getBunkStatus());
            mapdetail.put("createdby", userId);

            System.out.println("\nbunkid.." + bunkId);
            System.out.println("\ncustomerTO.getFuelType()=" + customerTO.getFuelType());
            System.out.println("\ncustomerTO.getCurrRate()=" + customerTO.getCurrRate());
            System.out.println("\ncustomerTO.getRemarks()=" + customerTO.getRemarks());
            System.out.println("\ncustomerTO.getBunkStatus()=" + customerTO.getBunkStatus());
            System.out.println("\ncreatedby=" + userId);
            status = (Integer) getSqlMapClientTemplate().update("customer.updateBunkdetails", mapdetail);
            System.out.println("status details is" + status);

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS, "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public ArrayList getPackingList() {
        Map map = new HashMap();
        ArrayList packList = new ArrayList();
        try {
            packList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getPackingList", map);
            System.out.println("packList size=" + packList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return packList;
    }

    public String getToDestination(String destination) {
        Map map = new HashMap();
        map.put("destination", destination);
        String suggestions = "";
        CustomerTO custTO = new CustomerTO();
        try {
            ArrayList destinations = new ArrayList();
            System.out.println("map = " + map);
            destinations = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getDestinations", map);
            Iterator itr = destinations.iterator();
            while (itr.hasNext()) {
                custTO = new CustomerTO();
                custTO = (CustomerTO) itr.next();
                suggestions = custTO.getToLocation() + "~" + suggestions;
                System.out.println("suggestions: " + suggestions);
            }
            if ("".equals(suggestions)) {
                suggestions = "no match found";
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getToDestination Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getToDestination", sqlException);
        }
        return suggestions;
    }

    public ArrayList getBillingTypeList() {
        Map map = new HashMap();
        ArrayList billingTypeList = new ArrayList();
        try {
            billingTypeList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getBillingTypeList", map);
            System.out.println("billingTypeList =" + billingTypeList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBillingTypeList", sqlException);
        }
        return billingTypeList;
    }

    public ArrayList getAccountManagerList() {
        Map map = new HashMap();
        ArrayList accountManagerList = new ArrayList();
        try {
            accountManagerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getAccountManagerList", map);
            System.out.println("billingTypeList =" + accountManagerList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getBillingTypeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getBillingTypeList", sqlException);
        }
        return accountManagerList;
    }

    public ArrayList getCustomerGroupList() {
        Map map = new HashMap();
        ArrayList result = new ArrayList();
        try {
            result = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerGroupList", map);
            System.out.println("getCustomerGroupList =" + result.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return result;
    }

    public String getCustomerCode(CustomerTO customerTO, int insertContractRouteMater) {
        Map map = new HashMap();
        String custCode = "";
        map.put("customerId", customerTO.getCustomerId());
        try {
            custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerCode Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "getCustomerCode", sqlException);
        }

        return custCode;
    }

    public ArrayList getCustomerName(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList getCustomerName = new ArrayList();
        map.put("custName", customerTO.getCustName() + "%");
        map.put("companyId", customerTO.getCompanyId());
        try {
            System.out.println("getCustomerName map" + map);
            getCustomerName = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerName", map);
            System.out.println("getCustomerName =" + getCustomerName.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerName", sqlException);
        }
        return getCustomerName;
    }

    public ArrayList getCustomerCodes(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList getCustomerCodes = new ArrayList();
        map.put("customerCode", customerTO.getCustomerCode() + "%");
        try {
            System.out.println("getCustomerCode map" + map);
            getCustomerCodes = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerCodes", map);
            System.out.println("getCustomerCodes =" + getCustomerCodes.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerCodes Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerCodes", sqlException);
        }
        return getCustomerCodes;
    }

    public ArrayList getCustomerDetails(CustomerTO customerTO) {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getCustomerDetails = new ArrayList();
        map.put("customerId", customerTO.getCustomerId());

        try {
            System.out.println("getCustomerDetails map" + map);
            getCustomerDetails = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerDetails", map);
            System.out.println("getCustomerGroupList =" + getCustomerDetails.size());
            System.out.println("getCustomerDetails=="+getCustomerDetails);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return getCustomerDetails;
    }

    public ArrayList getCustomerLists(CustomerTO customerTO) {
        Map map = new HashMap();
        ArrayList customerList = new ArrayList();
        map.put("customerId", customerTO.getCustomerId());
        map.put("secondaryContractSatus", customerTO.getContractType());
        if ("1".equals(customerTO.getCustomerType())) {
            map.put("primaryCustomerType", customerTO.getCustomerType());
        } else if ("2".equals(customerTO.getCustomerType())) {
            map.put("secondaryCustomerType", customerTO.getCustomerType());
        }

        map.put("roleId", customerTO.getRoleId());
        map.put("userId", customerTO.getUserId());
        map.put("companyId", customerTO.getCompanyId());

        String empId = (String) getSqlMapClientTemplate().queryForObject("customer.getEmployeeId", map);
        map.put("empId", empId);

        System.out.println("map size=" + map);
        try {
            customerList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCustomerList", map);
            System.out.println("customerList size=" + customerList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            sqlException.printStackTrace();
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "customerList", sqlException);
        }
        return customerList;
    }

    public int updateCustomer(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        int insertContractRouteMater = 0;
        map.put("customerId", customerTO.getCustomerId());
        String custCode = "";
        String[] temp1 = null;
        custCode = (String) getSqlMapClientTemplate().queryForObject("customer.getCustomerCode", map);
        if (custCode == null) {
            custCode = "CC-0001";
        } else {
            custCode = "CC-0001";
        }
        map.put("custCode", custCode);
        map.put("cityId", customerTO.getCityId());
        map.put("tallyBillingType", customerTO.getTallyBillingType());
        map.put("paymentType", customerTO.getPaymentType());
        map.put("Code", customerTO.getCustomerCode());
        map.put("Name", customerTO.getCustomerName());
        map.put("billingTypeId", customerTO.getBillingTypeId());
        map.put("compId", customerTO.getCompId());
        map.put("Address", customerTO.getCustAddress());
        map.put("Addresstwo", customerTO.getCustAddresstwo());
        map.put("Addressthree", customerTO.getCustAddressthree());
        map.put("City", customerTO.getCustCity());
        map.put("Contact", customerTO.getCustContactPerson());
        map.put("State", customerTO.getCustState());
        map.put("Phone", customerTO.getCustPhone());
        map.put("Mobile", customerTO.getCustMobile());
        map.put("Email", customerTO.getCustEmail());
        map.put("creditDays", customerTO.getCreditDays());
        map.put("creditLimit", customerTO.getCreditLimit());
        map.put("accountManagerId", customerTO.getAccountManagerId());
        map.put("secondaryBillingTypeId", customerTO.getSecondaryBillingTypeId());
        map.put("billingNameAddress", customerTO.getBillingNameAddress());
        map.put("customerTypeId", 1);
        map.put("customerGroupId", customerTO.getCustomerGroupId());
        map.put("activeInd", customerTO.getCustStatus());
        map.put("userId", userId);
        map.put("erpId", customerTO.getErpId());
        map.put("panNo", customerTO.getPanNo());
        map.put("gstNo", customerTO.getGstNo());
        map.put("stateId", customerTO.getStateId());
        map.put("organizationId", customerTO.getOrganizationId());
        map.put("companyType", customerTO.getCompanyType());
        System.out.print("customerTO.getCustName()" + customerTO.getCustName());
        map.put("customerName", customerTO.getCustomerName());
        System.out.print("customerTO:: customer name :" + customerTO.getCustomerName());
        System.out.print("Update====" + map);
        try {

            if ("1".equals(customerTO.getBillingCustFlag())) {
                map.put("billingCustName", customerTO.getCustomerId());
            } else {
                map.put("billingCustName", customerTO.getBillingCustId());
            }

            System.out.println("test1" + status);

            status = (Integer) getSqlMapClientTemplate().update("customer.updateEditCustomer", map);
            System.out.println("status customer details is is" + status);
            String billingTypeId = "";
            String contractId = "";
            ArrayList contractDetails = new ArrayList();
            contractDetails = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getContractDetails", map);
            Iterator itr = contractDetails.iterator();
            CustomerTO custTO = new CustomerTO();
            while (itr.hasNext()) {
                custTO = new CustomerTO();
                custTO = (CustomerTO) itr.next();
                billingTypeId = custTO.getBillingTypeId();
                contractId = custTO.getContractId();
                System.out.println("billingTypeId in contract:::::: " + billingTypeId);
                System.out.println("contractId:::::: " + contractId);
            }
            map.put("contractId", contractId);
            if (!billingTypeId.equals(customerTO.getBillingTypeId())) {
                status = (Integer) getSqlMapClientTemplate().update("customer.updateContractType", map);
                System.out.println("updateContractType ====" + status);
                status = (Integer) getSqlMapClientTemplate().update("customer.updateContractRates", map);
                System.out.println("updateContractRates ====" + status);
                status = (Integer) getSqlMapClientTemplate().update("customer.updateContractRoutes", map);
                System.out.println("updateContractRoutes ====" + status);
            }
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;
    }

    public int checkCustomerName(CustomerTO customerTO) {
        Map map = new HashMap();
        int customerName = 0;
        map.put("customerName", customerTO.getCustomerName());
        map.put("companyId", customerTO.getCompanyId());
        try {
            customerName = (Integer) getSqlMapClientTemplate().queryForObject("customer.checkCustomerName", map);
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("checkCustomerName Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "checkCustomerName", sqlException);
        }

        return customerName;
    }

    public ArrayList getModelFuelList(String vehicleTypeId, String contractRateId) {
        Map map = new HashMap();
//        String modelFuelIds = "";
        String checkContractId = "";
        ArrayList getModelFuelList = new ArrayList();
        try {
            map.put("vehicleTypeId", vehicleTypeId);
            map.put("contractRateId", contractRateId);
            System.out.println("map@@List" + map);
            checkContractId = (String) getSqlMapClientTemplate().queryForObject("customer.checkContractId", map);
            System.out.println("checkContractId" + checkContractId);
            if ("0".equals(checkContractId)) {
                getModelFuelList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getModelList", map);
                System.out.println("getModelList =" + getModelFuelList.size());
            } else {
                getModelFuelList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getModelFuelList", map);
                System.out.println("getModelFuelList =" + getModelFuelList.size());
            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getModelFuellist Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getModelFuelList", sqlException);
        }
        return getModelFuelList;
    }

    public int saveModelFuelDetails(String[] fuelModelId, String contractRateId, String[] modelId, String[] fuelVehicle, String[] fuelDG, String[] totalFuel, Integer userId) {
        Map map = new HashMap();
        Integer insertStatus = 0;
        Integer updateStatus = 0;
        String modelFuelIds = "";
        int response = 0;

        map.put("userId", userId);
        System.out.println("map@@@@" + map);
        System.out.println("modelId.length;" + modelId.length);
        try {
            for (int i = 0; i < modelId.length; i++) {
                map.put("fuelModelId", fuelModelId[i]);
                map.put("modelId", modelId[i]);
                map.put("fuelVehicle", fuelVehicle[i]);
                map.put("fuelDG", fuelDG[i]);
                map.put("totalFuel", totalFuel[i]);
                map.put("contractRateId", contractRateId);
                System.out.println("map" + map);
                System.out.println("fuelModelId[i]" + fuelModelId[i]);
                if ("0".equals(fuelModelId[i]) || "".equals(fuelModelId[i])) {
                    System.out.println("map@@@" + map);
                    insertStatus = (Integer) getSqlMapClientTemplate().update("customer.saveModelFuelDetails", map);
                    System.out.println("insertStatus@@" + insertStatus);
                    response = 1;
                } else {
                    System.out.println("map###" + map);
                    updateStatus = (Integer) getSqlMapClientTemplate().update("customer.updateModelFuelDetails", map);
                    System.out.println("updateStatus@@" + updateStatus);
                    response = 2;
                }
            }
//            }

            //System.out.println("checkRouteStage " + checkRouteStage);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("insertStatus Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MRS-01", CLASS, "insertStatus", sqlException);
        }

        return response;
    }

    public ArrayList getStateList() {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getStateList = new ArrayList();

        try {
            System.out.println("getStateList map" + map);
            getStateList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getStateList", map);
            System.out.println("getStateList =" + getStateList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCustomerGroupList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getCustomerGroupList", sqlException);
        }
        return getStateList;
    }

    public ArrayList getOrganizationList() {
        Map map = new HashMap();
        //         CustomerTO customerTO = new CustomerTO();
        ArrayList getOrganizationList = new ArrayList();

        try {
            System.out.println("getOrganizationList map" + map);
            getOrganizationList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getOrganizationList", map);
            System.out.println("getOrganizationList =" + getOrganizationList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getOrganizationList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "getOrganizationList", sqlException);
        }
        return getOrganizationList;
    }

    public ArrayList processContractConsigneeList(CustomerTO customerTO, String custId) {
        Map map = new HashMap();
        map.put("custId", custId);
        ArrayList ConsigneeList = new ArrayList();
        System.out.println("map customerId" + map);
        try {
            if ((customerTO.getCustomerId() == null) && "".equals(customerTO.getCustomerId())) {
                map.put("customerid", "0");
            } else {
                map.put("customerid", customerTO.getCustomerId());
            }
            ConsigneeList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getContractConsigneeList", map);
            //System.out.println("processContractConsigneeList dao size=" + ConsigneeList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processContractConsigneeList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "processContractConsigneeList", sqlException);
        }
        return ConsigneeList;
    }

    public ArrayList getCityList() {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCityList", map);
            //System.out.println("getCityList dao size=" + cityList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "CityList", sqlException);
        } finally {
            map = null;
        }
        return cityList;
    }

    public ArrayList processContractConsignorList(CustomerTO customerTO, String custId) {
        Map map = new HashMap();
        map.put("custId", custId);
        ArrayList consignorList = new ArrayList();
        int customerId = 0;
        try {

            consignorList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getContractConsignorList", map);
            //System.out.println("processContractConsignorList dao size=" + consignorList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("processContractConsignorList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "processContractConsignorList", sqlException);
        }
        return consignorList;
    }

    public int insertConsignorDetails(CustomerTO customerTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        String consignorId = "";
        String consignorCode = "CSR";
        // map.put("customerName", customerTO.getCustomerName());
        map.put("consignorId", customerTO.getConsignorId());
        map.put("consignorName", customerTO.getConsignorName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());

        String address = "";
        int saveCityMaster = 0;
        try {
            address = customerTO.getCityId();

            map.put("zoneId", 0);
            map.put("cityName", address);
            map.put("type", "5");
            map.put("cityCode", "NA");
            map.put("countryId", "1");
            map.put("companyId", customerTO.getCompanyId());
            map.put("latitude", customerTO.getLatitude());
            map.put("longitute", customerTO.getLongitute());
            map.put("googleCityName", customerTO.getCityId());
            System.out.println("map 1= " + map);
            saveCityMaster = (Integer) session.insert("customer.saveCity", map);
            System.out.println("saveCityMaster : " + saveCityMaster);

        } catch (Exception e) {

        }

        map.put("cityId", saveCityMaster);
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCodeAdd());
        map.put("userId", userId);
        System.out.println("map details***************************************" + map);
        try {
            if (customerTO.getConsignorId().equals("")) {
                consignorId = (String) session.queryForObject("customer.getConsignorCode", map);
                consignorCode = consignorCode + consignorId;
                map.put("consignorCode", consignorCode);
                status = (Integer) session.insert("customer.insertConsignorDetails", map);
                //System.out.println("insertConsignorDetails status is" + status);

            }
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("InsertConsignorDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "InsertConsignorDetails", sqlException);
        } finally {
            consignorId = null;
            consignorCode = null;
            map = null;
        }
        return status;
    }

    public int updateConsignorDetails(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consignorId", customerTO.getConsignorId());
        map.put("consignorName", customerTO.getConsignorName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCodeAdd());
        map.put("userId", userId);
        try {

            status = (Integer) getSqlMapClientTemplate().update("customer.updateConisgnorDetails", map);

            map.put("cityName", customerTO.getCityId());
            map.put("cityId", customerTO.getEditId());
            map.put("latitude", customerTO.getLatitude());
            map.put("longitute", customerTO.getLongitute());
            map.put("googleCityName", customerTO.getCityId());
            System.out.println("map 1= " + map);
            status = (Integer) getSqlMapClientTemplate().update("customer.updateCity", map);

            //System.out.println("updateConisgnorDetails status is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("UpdateConisgnorDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "UpdateConisgnorDetails", sqlException);
        }
        return status;
    }

    public int updateConsigneeDetails(CustomerTO customerTO, int userId) {
        Map map = new HashMap();
        int status = 0;
        map.put("consigneeId", customerTO.getConsigneeId());
        map.put("consigneeName", customerTO.getConsigneeName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("erpId", customerTO.getErpId());
        map.put("address2", customerTO.getAddress2());
        map.put("stateId", customerTO.getStateId());
//        map.put("cityId", customerTO.getCityId());
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCode());
        map.put("userId", userId);
        //System.out.println("map updateConsigneeDetails = " + map);
        try {

            status = (Integer) getSqlMapClientTemplate().update("customer.updateConisgneeDetails", map);
            //System.out.println("updateConisgneeDetails status is" + status);

            map.put("cityName", customerTO.getCityId());
            map.put("cityId", customerTO.getEditId());
            map.put("latitude", customerTO.getLatitude());
            map.put("longitute", customerTO.getLongitute());
            map.put("googleCityName", customerTO.getCityId());
            System.out.println("map 1= " + map);
            status = (Integer) getSqlMapClientTemplate().update("customer.updateCity", map);

        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("UpdateConisgeerDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "UpdateConisgneeDetails", sqlException);
        }
        return status;
    }

    public int insertConsigneeDetails(CustomerTO customerTO, int userId, SqlMapClient session) {
        Map map = new HashMap();
        int status = 0;
        int status1 = 0;
        String ConsigneeId = "";
        String ConsigneeCode = "CSE";
        // map.put("customerName", customerTO.getCustomerName());
        map.put("consigneeId", customerTO.getConsigneeId());
        map.put("consigneeName", customerTO.getConsigneeName());
        map.put("customerId", customerTO.getCustomerId());
        map.put("contactPerson", customerTO.getContactPerson());
        map.put("phoneNo", customerTO.getPhoneNo());
        map.put("address1", customerTO.getAddress1());
        map.put("address2", customerTO.getAddress2());

        String address = "";
        int saveCityMaster = 0;
        try {
            address = customerTO.getCityId();

            map.put("zoneId", 0);
            map.put("cityName", address);
            map.put("type", "5");
            map.put("cityCode", "NA");
            map.put("countryId", "1");
            map.put("companyId", customerTO.getCompanyId());
            map.put("latitude", customerTO.getLatitude());
            map.put("longitute", customerTO.getLongitute());
            map.put("googleCityName", customerTO.getCityId());
            System.out.println("map 1= " + map);
            saveCityMaster = (Integer) session.insert("customer.saveCity", map);
            System.out.println("saveCityMaster : " + saveCityMaster);

        } catch (Exception e) {

        }

        map.put("stateId", customerTO.getStateId());
        map.put("cityId", saveCityMaster);
        map.put("email", customerTO.getEmail());
        map.put("activeInd", customerTO.getActiveInd());
        map.put("remarks", customerTO.getRemarks());
        map.put("pinCode", customerTO.getPinCode());
        map.put("userId", userId);
        System.out.println("map details***************************************" + map);
        try {
            if (customerTO.getConsigneeId().equals("")) {
                ConsigneeId = (String) session.queryForObject("customer.getConsigneeCode", map);
                ConsigneeCode = ConsigneeCode + ConsigneeId;
                map.put("consigneeCode", ConsigneeCode);
                System.out.println("ConsigneeCode" + ConsigneeCode);
                status = (Integer) session.insert("customer.insertConsigneeDetails", map);
                //System.out.println("insertConsignorDetails status is" + status);

            }

            //System.out.println("insertConsigneeDetails status is" + status);
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("InsertConsigneeDetails Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-02", CLASS,
                    "InsertConsigneeDetails", sqlException);
        } finally {
            ConsigneeId = null;
            ConsigneeCode = null;
            map = null;
        }
        return status;
    }

    public ArrayList getConsignorView(CustomerTO customerTO) {
        Map map = new HashMap();
        map.put("customerId", customerTO.getCustomerId());

        ArrayList consignor = new ArrayList();
        try {

            consignor = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getConsignorViewList", map);
            System.out.println("value" + consignor.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsignorView Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getConsignorView", sqlException);
        }
        return consignor;

    }

    public ArrayList getConsigneeView(CustomerTO customerTO) {
        Map map = new HashMap();
        map.put("customerId", customerTO.getCustomerId());

        ArrayList consignee = new ArrayList();
        try {

            consignee = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getConsigneeViewList", map);
            System.out.println("value" + consignee.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getConsigneeView Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-MFR-01", CLASS, "getConsigneeView", sqlException);
        }
        return consignee;

    }

    public ArrayList getCityLists() {
        Map map = new HashMap();
        ArrayList cityList = new ArrayList();
        try {
            cityList = (ArrayList) getSqlMapClientTemplate().queryForList("customer.getCityLists", map);
            //System.out.println("getCityList dao size=" + cityList.size());
        } catch (Exception sqlException) {
            sqlException.printStackTrace();
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getCityList Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-CUST-01", CLASS, "CityList", sqlException);
        } finally {
            map = null;
        }
        return cityList;
    }

}
