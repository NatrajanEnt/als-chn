<%--
    Document   : CompanyWiseProfitability
    Created on : Oct 31, 2013, 11:09:54 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>



        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

    </head>
    <script>
        function submitPage() {
            document.CNoteSearch.action = "/throttle/BrattleFoods/SearchCNoteReport.jsp";
            document.CNoteSearch.submit();
        }
         function excelExport(){
                document.CNoteSearch.action="/throttle/BrattleFoods/CNoteReportExcel.jsp";
                document.CNoteSearch.submit();
        }
    </script>
    <body>
        <%
            String menuPath = "Consignment Note >> View / Edit";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="CNoteSearch" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">C NOTE VIEW/EDIT</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td >Customer Name</td>
                                        <td><input type="text" class="form-control" name="CustomerName" id="CustomerName" style="width: 110px"/> </td>
                                        <td>Service Type</td>
                                        <td>  <select name="Service"  id="Service" class="form-control" style="width:120px;" >
                                                <option value="o">--Select--</option>
                                                <option value="1">FLT</option>
                                                <option value="2">LTL</option>
                                            </select>
                                        </td>
                                        <td>Business Type</td>
                                        <td>
                                            <select name="Business"  id="Business" class="form-control" style="width:120px;" >
                                                <option value="o">--Select--</option>
                                                <option value="1">Primary </option>
                                                <option value="2">Secondary</option>
                                            </select>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Status</td>
                                        <td><select name="status"  id="status" class="form-control" style="width:120px;" >
                                                <option value="o">--Select--</option>
                                                <option value="1">Open</option>
                                                <option value="2">Secondary</option>
                                            </select>
                                        </td>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>
                                    </tr>
                                    <tr align="center">
                                        <td colspan="3"><input type="button" class="button"   value="Search" onclick="submitPage()"></td>
                                        <td colspan="3"><input type="button" class="button"   value="Generate Excel" onclick="excelExport()"></td>
                                    </tr>
                                </table>
                                <br>


                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" >
                <tr>
                    <td class="table">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
                            <tr>
                                <td class="bottom" align="left"><img src="/throttle/images/left_status.jpg" alt=""  /></td>                                            
                                <td class="bottom" height="35" align="right"><img src="/throttle/images/Trip_Create.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left">1</span></td>
                                <td class="bottom" height="35" align="right"><img src="/throttle/images/Trip_generated.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left">2</span></td>
                                <td class="bottom" height="35" align="right"><img src="/throttle/images/Trip_planned.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left">1</span></td>
                            </tr>
                            <tr>
                                <td class="bottom" align="left"><img src="/throttle/images/left_status.jpg" alt=""  /></td>                                            
                                <td class="bottom" height="35" align="right"><img src="/throttle/images/Trip-Started.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#3C0;" align="left">1</span></td>
                                <td class="bottom" align="right"><img src="/throttle/images/Trip_closed.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;">2</span></td>                                            
                                <td class="bottom" align="right"><img src="/throttle/images/Trip_total.png" alt="" /></td>
                                <td class="bottom">&nbsp;<span style="font-size:16px; color:#F30;">7</span></td>                                            
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="95%"  align="center" cellpadding="0" cellspacing="0" class="border">
                <thead>
                    <tr>
                        <td width="35" class="contentsub">Sno</td>
                        <td width="55" class="contentsub">CN No</td>
                        <td width="135" class="contentsub">Customer Name </td>
                        <td width="135" class="contentsub">Customer Code </td>
                        <td width="127" class="contentsub">Origin </td>
                        <td width="121" class="contentsub">Destination </td>
                        <td width="121" class="contentsub">Vehicle Type </td>
                        <td width="74" class="contentsub">Weight(MT)</td>
                        <td width="174" height="30" class="contentsub">Required Pickup Date &amp; Time </td>
                        <td width="174" height="30" class="contentsub">Estimated Delivery Date &amp; Time</td>
                        <td width="174" height="30" class="contentsub">Transit Days</td>
                        <td width="104" height="30" class="contentsub">Business Type </td>
                        <td width="134" height="30" class="contentsub">Service Type </td>
                        <td width="134" height="30" class="contentsub">Status</td>
                        <td width="134" height="30" class="contentsub">&nbsp;</td>
                    </tr>
                </thead>
                    <tr>
                        <td class="text1">1</td>
                        <td class="text1"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD16700</a></td>
                        <td class="text1">Sundaram</td>
                        <td class="text1">BF001</td>
                        <td class="text1">Kashipur</td>
                        <td class="text1">Delhi</td>
                        <td class="text1">TATA/3118</td>
                        <td class="text1">20</td>
                        <td class="text1">01/11/2013&amp;07:30AM</td>
                        <td class="text1">03/11/2013&amp;05:30PM</td>
                        <td class="text1">2.5Days</td>
                        <td class="text1">Primary</td>
                        <td class="text1">FTL</td>
                        <td class="text1"><font color="red"><img src="/throttle/images/Trip_closed.png" alt="" /></font></td>
                        <td class="text1">Print</td>
                    </tr>
                    <tr>
                        <td class="text2">2<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text2"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD14500</a></td>
                        <td class="text2">John</td>
                        <td class="text2">BF019</td>
                        <td class="text2">Kashipur</td>
                        <td class="text2">Balwal</td>
                        <td class="text2">TATA/3118</td>
                        <td class="text2">35</td>
                        <td class="text2">05/11/2011&amp;10:00AM</td>
                        <td class="text2">07/11/2011&amp;10:00AM</td>
                        <td class="text2" >2 Days</td>
                        <td class="text2" >Secondary</td>
                        <td class="text2">FTL</td>
                        <td class="text2"><font color="blue"><img src="/throttle/images/Trip_Create.png" alt="" /></font></td>
                        <td class="text2">Print</td>
                    </tr>
                    <tr>
                        <td class="text1">3<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text1"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD1034</a></td>
                        <td class="text1">Balaji</td>
                        <td class="text1">BF005</td>
                        <td class="text1">Balwal</td>
                        <td class="text1">Delhi</td>
                        <td class="text1">AL/2516</td>
                        <td class="text1">25</td>
                        <td class="text1">06/11/2013&amp;04:00PM</td>
                        <td class="text1">09/11/2013&amp;04:00PM</td>
                        <td class="text1">3 Days</td>
                        <td class="text1">Primary</td>
                        <td class="text1">FTL</td>
                        <td class="text1"><font color="blue"><img src="/throttle/images/Trip_planned.png" alt="" /></font></td>
                        <td class="text1">Print</td>
                    </tr>
                    <tr>
                        <td class="text2">4</td>
                        <td class="text2"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD100</a></td>
                        <td class="text2" >Prakash</td>
                        <td class="text2">BF147</td>
                        <td class="text2">Kashipur</td>
                        <td class="text2">Chennai</td>
                        <td class="text2">TATA/3118</td>
                        <td class="text2">27</td>
                        <td class="text2">01/11/2013&amp;11:00AM</td>
                        <td class="text2">04/11/2013&amp;11:00AM</td>
                        <td class="text2">3 Days</td>
                        <td class="text2">Primary</td>
                        <td class="text2" >FTL</td>
                        <td class="text2"><font color="green"><img src="/throttle/images/Trip_generated.png" alt="" /></font></td>
                        <td class="text2">Print</td>
                    </tr>
                    <tr>
                        <td class="text1">5<img width="15px" height="15px" src="/throttle/images/flag2.gif" align="middle" border="0"></td>
                        <td class="text1"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD1036</a></td>
                        <td class="text1">John</td>
                        <td class="text1">BF019</td>
                        <td class="text1">Balwal</td>
                        <td class="text1">Chennai</td>
                        <td class="text1">AL/2516</td>
                        <td class="text1">30</td>
                        <td class="text1">05/11/2013&amp;05:00AM</td>
                        <td class="text1">09/11/2013&amp;05:00AM</td>
                        <td class="text1">4 Days</td>
                        <td class="text1">Primary</td>
                        <td class="text1">FTL</td>
                        <td class="text1"><font color="blue"><img src="/throttle/images/Trip-Started.png" alt="" /></font></td>
                        <td class="text1">Print</td>
                    </tr>
                    <tr>
                        <td class="text2">6</td>
                        <td class="text2"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD100</a></td>
                        <td class="text2" >Prakash</td>
                        <td class="text2">BF147</td>
                        <td class="text2">Chandigarh</td>
                        <td class="text2">Chennai</td>
                        <td class="text2">TATA/2518</td>
                        <td class="text2">28</td>
                        <td class="text2">01/11/2013&amp;09:00AM</td>
                        <td class="text2">03/11/2013&amp;09:00AM</td>
                        <td class="text2">2 Days</td>
                        <td class="text2">Primary</td>
                        <td class="text2" >FTL</td>
                        <td class="text2"><font color="green"><img src="/throttle/images/Trip_generated.png" alt="" /></font></td>
                        <td class="text2">Print</td>
                    </tr>
                     <tr>
                        <td class="text1">7</td>
                        <td class="text1"><a href="/throttle/BrattleFoods/editConsignment.jsp">AD1034</a></td>
                        <td class="text1">John</td>
                        <td class="text1">BF019</td>
                        <td class="text1">Balwal</td>
                        <td class="text1">Delhi</td>
                        <td class="text1">AL/2516</td>
                        <td class="text1">25</td>
                        <td class="text1">01/11/2013&amp;12:00PM</td>
                        <td class="text1">04/11/2013&amp;12:00PM</td>
                        <td class="text1">3 Days</td>
                        <td class="text1">Primary</td>
                        <td class="text1">FTL</td>
                        <td class="text1"><font color="red"><img src="/throttle/images/Trip_closed.png" alt="" /></font></td>
                        <td class="text1"><a href="/throttle/BrattleFoods/printCNote.jsp">Print</a></td>
                    </tr>
            </table>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
