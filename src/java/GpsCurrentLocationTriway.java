/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Nilesh
 */
import java.net.*;
import java.io.*;
import java.text.*;

import java.sql.SQLException;
import java.text.ParseException;
import org.json.JSONObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;
import org.json.JSONArray;

//import ets.domain.gps.Service;
//import ets.domain.gps.ServiceSoap;
//import ets.domain.gps.GetInformation;
//import ets.domain.gps.GetInformationResponse;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class GpsCurrentLocationTriway {

    public static void main(String args[]) throws ParseException, ClassNotFoundException, SQLException {
        GpsCurrentLocationTriway program = new GpsCurrentLocationTriway();
       // program.start();
    }

//    public void start() throws ParseException, ClassNotFoundException, SQLException {
//        Connection con = null;
//        Statement statement = null;
//        Statement statement1 = null;
//        ResultSet rs = null;
//        ResultSet rs1 = null;
//        ResultSet rs2 = null;
//        ResultSet rs3 = null;
//        String url = "jdbc:mysql://59.144.140.27:3306/";
//        String dbName = "triwayRLPL";
//        String driverName = "com.mysql.jdbc.Driver";
//        String userName = "root";
//        String password = "Dbuser#2015";
//
//        String fileName = "jdbc_url.properties";
//        InputStream is = getClass().getResourceAsStream("/" + fileName);
//        Properties dbProps = new Properties();
//
//        try {
//            dbProps.load(is);//this may throw IOException
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        String dbClassName = dbProps.getProperty("jdbc.driverClassName");
//        String dbUrl = dbProps.getProperty("jdbc.url");
//        String dbUserName = dbProps.getProperty("jdbc.username");
//        String dbPassword = dbProps.getProperty("jdbc.password");
//
//        try {
//            //Class.forName(driverName);
//            //con = DriverManager.getConnection(url + dbName, userName, password);
//
//            Class.forName(dbClassName).newInstance();
//            con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
//
//            PreparedStatement insertVehicleLocation = null;
//            PreparedStatement insertVehicleLocationLog = null;
//            PreparedStatement checkVehicleLocation = null;
//            PreparedStatement updateVehicleLocation = null;
//            statement = con.createStatement();
//
//            String sql = "insert into ts_vehicle_location (vehicle_id,vehicle_no,gps_date,location, "
//                    + "latitude,longitude,distance_travelled,speed,geoLocation,geoLocationInd,geoLocationDT)"
//                    + "values(?,?,STR_TO_DATE(?,'%d %b %Y %k:%i:%s'),?,?,?,?,?,?,?,?) ";
//            String sql4 = "insert into ts_vehicle_location1_log (vehicle_id,veh_reg,updated,location,"
//                    + " latitude,longitude,km_travelled,speed,geoLocation,geoLocationInd,geoLocationDT)"
//                    + "values(?,?,STR_TO_DATE(?,'%d %b %Y %k:%i:%s'),?,?,?,?,?,?,?,?) ";
//            String sql1 = "update ts_vehicle_location set gps_date=STR_TO_DATE(?,'%d %b %Y %k:%i:%s'), "
//                    + "location=?,latitude=?,longitude=?,distance_travelled=?,speed=?,geoLocation=?,geoLocationInd=?,geoLocationDT=? "
//                    + " where vehicle_id = ?";
//
//            String sql2 = "select a.vehicle_id,gps_system_id, b.reg_no "
//                    + "from "
//                    + "  papl_vehicle_master a, "
//                    + "  papl_vehicle_reg_no b "
//                    + "where "
//                    + "gpssystem='YES' "
//                    + "and a.vehicle_id = b.vehicle_id "
//                    + "and b.active_ind = 'Y'";
//            String sql3 = "update ts_vehicle_location1 set updated=?,location=?,poiName=?,"
//                    + "latitude=?,longitude=?,km_travelled=?,speed=? where vehicle_id=?";
//            String sql5 = "select count(*) from ts_vehicle_location where vehicle_id=?;";
//            rs = statement.executeQuery(sql2);
//            insertVehicleLocation = con.prepareStatement(sql);
//            insertVehicleLocationLog = con.prepareStatement(sql4);
//            checkVehicleLocation = con.prepareStatement(sql5);
//            updateVehicleLocation = con.prepareStatement(sql1);
//            String gpsId = "";
//            String vehicleId = "";
//            String regNo = "";
//
//            Service srv = new Service();
//            ServiceSoap srvs = srv.getServiceSoap();
//            Date dNow = new Date();
//            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy kk:mm:ss");
//            String today = ft.format(dNow);
//            String Userid = "route";
//            String vehicleid = "9511908580";
//            String result = "";
//            int count = 0;
//
//            /*
//            result.getContent() = 9511908580::9511908580::TN18K3979::23 Jan 2018 18:32:26::2.5KM FROM NELAMANGALA,BANGALORE (RURAL),KARNATAKA,INDIA::13.122395::77.3936733333333::0::462.67::3381.35::SilverSpark Unit-1::OUT::Jan 23 2018  4:45PM::ROUTE LOGISTICS$
//            0 value is = 9511908580
//            1 value is = 9511908580
//            2 value is = TN18K3979
//            3 value is = 23 Jan 2018 18:32:26
//            4 value is = 2.5KM FROM NELAMANGALA,BANGALORE (RURAL),KARNATAKA,INDIA
//            5 value is = 13.122395
//            6 value is = 77.3936733333333
//            7 value is = 0
//            8 value is = 462.67
//            9 value is = 3381.35
//            10 value is = SilverSpark Unit-1
//            11 value is = OUT
//            12 value is = Jan 23 2018  4:45PM
//            13 value is = ROUTE LOGISTICS$            
//             */
//            String Veh_reg = "";
//            String Updated = "";
//            String Location = "";
//            String Latitude = "";
//            String Longitude = "";
//            String KM_Travelled = "";
//            String Speed = "";
//            String geoLocation = "";
//            String geoLocationInd = "";
//            String geoLocationDT = "";
//            while (rs.next()) {
//                vehicleId = rs.getString(1);
//                gpsId = rs.getString(2);
//                regNo = rs.getString(3);
//
//                System.out.println("gpsId:" + gpsId);
//                System.out.println("vehicleId:" + vehicleId);
//                System.out.println("regNo:" + regNo);
//
//                result = srvs.getInformation(Userid, gpsId);
//                System.out.println("result.getContent() = " + result);
//
//                if (!"".equals(result)) {
//                    String[] temp = result.split("::");
//
////                for (int i = 0; i < temp.length; i++) {
////                    System.out.println(i + " value is = " + temp[i]);
////                }
//                    Veh_reg = regNo;
//                    Updated = temp[3];
//                    Location = temp[4];
//                    Latitude = temp[5];
//                    Longitude = temp[6];
//                    KM_Travelled = temp[9];
//                    Speed = temp[8];
//                    geoLocation = temp[10];
//                    geoLocationInd = temp[11];
//                    geoLocationDT = temp[12];
//
//                    System.out.println(Veh_reg);
//                    System.out.println(Updated);
//                    System.out.println(Location);
//                    System.out.println(Latitude);
//                    System.out.println(Longitude);
//                    System.out.println(KM_Travelled);
//                    System.out.println(Speed);
//
//                    checkVehicleLocation.setString(1, vehicleId);
//
//                    rs1 = checkVehicleLocation.executeQuery();
//                    if (rs1.next()) {
//                        count = rs1.getInt(1);
//                    }
//                    System.out.println("count:" + count);
//                    if (count == 0) {
//
//                        insertVehicleLocation.setString(1, vehicleId);
//                        insertVehicleLocation.setString(2, Veh_reg);
//                        insertVehicleLocation.setString(3, Updated);
//                        insertVehicleLocation.setString(4, Location);
//                        insertVehicleLocation.setString(5, Latitude);
//                        insertVehicleLocation.setString(6, Longitude);
//                        insertVehicleLocation.setString(7, KM_Travelled);
//                        insertVehicleLocation.setString(8, Speed);
//                        insertVehicleLocation.setString(9, geoLocation);
//                        insertVehicleLocation.setString(10, geoLocationInd);
//                        insertVehicleLocation.setString(11, geoLocationDT);
//                        int update = insertVehicleLocation.executeUpdate();
//                        if (update != 0) {
//                            System.out.println("Data Inserted Successfully.:-)");
//                        }
//                    } else {
//
//                        //update vehicle_location
//                        updateVehicleLocation.setString(10, vehicleId);
//                        updateVehicleLocation.setString(1, Updated);
//                        updateVehicleLocation.setString(2, Location);
//                        updateVehicleLocation.setString(3, Latitude);
//                        updateVehicleLocation.setString(4, Longitude);
//                        updateVehicleLocation.setString(5, KM_Travelled);
//                        updateVehicleLocation.setString(6, Speed);
//                        updateVehicleLocation.setString(7, geoLocation);
//                        updateVehicleLocation.setString(8, geoLocationInd);
//                        updateVehicleLocation.setString(9, geoLocationDT);
//
//                        int update2 = updateVehicleLocation.executeUpdate();
//                        if (update2 != 0) {
//                            System.out.println("Data Updated Successfully.:-)");
//                        }
//                    }
//
//                    //vehicle_location_log
//                    insertVehicleLocationLog.setString(1, vehicleId);
//                    insertVehicleLocationLog.setString(2, Veh_reg);
//                    insertVehicleLocationLog.setString(3, Updated);
//                    insertVehicleLocationLog.setString(4, Location);
//                    insertVehicleLocationLog.setString(5, Latitude);
//                    insertVehicleLocationLog.setString(6, Longitude);
//                    insertVehicleLocationLog.setString(7, KM_Travelled);
//                    insertVehicleLocationLog.setString(8, Speed);
//                    insertVehicleLocationLog.setString(9, geoLocation);
//                    insertVehicleLocationLog.setString(10, geoLocationInd);
//                    insertVehicleLocationLog.setString(11, geoLocationDT);
//
//                    int update1 = insertVehicleLocationLog.executeUpdate();
//                    if (update1 != 0) {
//                        //////System.out.println("Data Inserted Successfully in Log.:-)");
//                    }
//                } else {
//                    System.out.println("no data found");
//                }
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            con.close();
//        }
//
//    }
}
