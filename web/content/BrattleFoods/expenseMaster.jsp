<%-- 
    Document   : expenseMaster
    Created on : May 12, 2016, 11:15:04 AM
    Author     : Jp
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>

<script type="text/javascript">
    function slabsubmit()
    {
//        alert("save")
        
        if (document.getElementById("expenseName").value == "") {
            alert("Please enter Expense Name");
            document.getElementById("expenseName").focus();
            return;
        }
           document.expenseMaster.action = "/throttle/saveExpenseMaster.do";
//            document.slabMaster.method = "post";
            document.expenseMaster.submit();
       
    }

    function setValues(sno, expenseId,expenseName,ledgerId, levelID) {
//        alert (ledgerId);
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("expenseId").value = expenseId;
        document.getElementById("expenseName").value = expenseName;
        document.getElementById("levelID").value = levelID;
        document.getElementById("ledgerId").value = ledgerId;
    }

    function checkslabName() {
        

        var slabName = document.getElementById('slabName').value;

        var url = '/throttle/checkslabName.do?slabName=' + slabName;
        if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function() {
            processRequest();
        };
        httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#slabStatus").text('Please Check City Name  :' + val + ' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#slabStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Slab Master Brattle Food</title>
    </head>
    <body onload="">
        <form name="expenseMaster"  method="POST">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="contenthead" colspan="4" >Expense Master</td>
                    </tr>
                    <input type="hidden" name="expenseId" id="expenseId" value=""> 
                    <input type="hidden" name="levelID" id="levelID" value=""> 
                    <input type="hidden" name="ledgerId" id="ledgerId" value=""> 
                    <tr>
                        <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="slabStatus" style="color: red"></label></td>
                    </tr>
                    <tr>
                    <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Expense Name</td>
                    <td class="text1"><input type="text" name="expenseName" id="expenseName" class="textbox"  maxlength="50"></td>

                    
                        <td class="text2">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                        <td class="text2">
                            <select  align="center" class="textbox" name="status" id="status" >
                                <option value='Y'>Active</option>
                                <option value='N' id="inActive" >In-Active</option>
                            </select>
                        </td>
                    </tr>
                </table>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="slabsubmit()">
                        </center>
                    </td>
                </tr>
                <br>
                <br>

                <h2 align="center">Expense List</h2>

                <table width="815" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="30">
                            <th><h3>S.No</h3></th>
                            <th><h3>Expense Name </h3></th>
                            <th><h3>Primary Group </h3></th>
                            <th><h3>Group</h3></th>
                            <!--<th><h3>Status</h3></th>-->
                            <th><h3>Edit</h3></th>
                        </tr>
                    </thead>

                    <% int sno = 0;%>
                    <c:if test = "${expenselist!= null}">
                        <c:forEach items="${expenselist}" var="expense">
                            <%
                                sno++;
                                String className = "text1";
                                if ((sno % 1) == 0) {
                                    className = "text1";
                                } else {
                                    className = "text2";
                                }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${expense.expenseName}" /></td>
                                <td class="<%=className%>"  align="left"><c:out value="${expense.groupName}"/></td>
                                <td class="<%=className%>"  align="left"><c:out value="${expense.levelGroupName}"/></td>
                                
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%=sno%>, '<c:out value="${expense.expenseId}" />', '<c:out value="${expense.expenseName}" />','<c:out value="${expense.ledgerId}" />','<c:out value="${expense.levelID}" />');" /></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                        <input type="hidden" name="count" id="count" value="<%=sno%>" />
                    </c:if>
                </table>
                <br>
                <br>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 0);
                </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
