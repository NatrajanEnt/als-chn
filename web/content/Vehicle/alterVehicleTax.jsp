<%-- 
    Document   : alterVehicleTax
    Created on : Sep 21, 2012, 6:09:41 PM
    Author     : ASHOK
--%>




<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Insurance Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            function validate(){
                var errMsg = "";
                if(document.VehicleInsurance.regNo.value){
                    errMsg = errMsg+"Vehicle Registration No is not filled\n";
                }
                if(document.VehicleInsurance.insuranceCompanyName.value){
                    errMsg = errMsg+"Insurance Company is not filled\n";
                }
                if(document.VehicleInsurance.premiumNo.value){
                    errMsg = errMsg+"Premium No is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidAmount.value){
                    errMsg = errMsg+"Premium paid Amount is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidDate.value){
                    errMsg = errMsg+"Premium paid Date is not filled\n";
                }
                if(document.VehicleInsurance.vehicleValue.value){
                    errMsg = errMsg+"Vehicle Value is not filled\n";
                }
                if(document.VehicleInsurance.premiumExpiryDate.value){
                    errMsg = errMsg+"Premium expiry Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }


            var httpRequest;
            function getVehicleDetails(regNo)
            {

                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            var vehicleValues = detail.split("~");
                            document.VehicleInsurance.vehicleId.value = vehicleValues[0]
                            document.VehicleInsurance.chassisNo.value = vehicleValues[2]
                            document.VehicleInsurance.engineNo.value = vehicleValues[2]
                            document.VehicleInsurance.vehicleMake.value = vehicleValues[4]
                            document.VehicleInsurance.vehicleModel.value = vehicleValues[5]

                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }
        </script>

    </head>

    <body onLoad="setImages(1,0,0,0,0,0);">
        <form name="VehicleInsurance" action="/throttle/alterVehicleTax.do">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <h2 align="center">Vehicle Road Tax Details</h2>
            <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="contenthead" colspan="4">Vehicle Details</td>
                </tr>
                <c:if test="${vehicleRoadTax != null}">
                    <c:forEach items="${vehicleRoadTax}" var="VDet" >
                        <tr>
                            <td class="texttitle1">Vehicle No</td>
                            <td class="text1"><input type="text" name="regNo" id="regNo" class="form-control" value='<c:out value="${VDet.regno}" />' ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${VDet.vehicleId}" />' /></td>
                            <td class="texttitle1">Make</td>
                            <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" value='<c:out value="${VDet.mfrName}" />'></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Model</td>
                            <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" value='<c:out value="${VDet.modelName}" />' ></td>
                            <td class="texttitle2">Usage</td>
                            <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control"  ></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Engine No</td>
                            <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" value='<c:out value="${VDet.engineNo}" />' ></td>
                            <td class="texttitle1">Chassis No</td>
                            <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" value='<c:out value="${VDet.chassisNo}" />' ></td>
                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td class="contenthead" colspan="4">Road Tax Details</td>
                </tr>
                <c:if test="${vehicleTax != null}">
                    <c:forEach items="${vehicleTax}" var="VeT" >
                <tr>
                    <td class="texttitle1">Receipt No</td>
                    <td class="text1"><input type="text" name="roadTaxReceiptNo" id="roadTaxReceiptNo" class="form-control" value='<c:out value="${VeT.roadtaxreceiptno}" />' ><input type="hidden" name="id" id="id" value='<c:out value="${VeT.id}" />' /> </td>
                    <td class="texttitle1">Receipt Date</td>
                    <td class="text1"><input type="text" name="roadTaxReceiptDate" id="roadTaxReceiptDate" class="datepicker" value='<c:out value="${VeT.roadtaxreceiptdate}" />' ></td>
                </tr>
                <tr>
                    <td class="texttitle2">Location</td>
                    <td class="text2"><input type="text" name="roadTaxPaidLocation" id="roadTaxPaidLocation" class="form-control" value='<c:out value="${VeT.roadtaxpaidlocation}" />' ></td>
                    <td class="texttitle2">Amount</td>
                    <td class="text2"><input type="text" name="roadTaxPaidAmount" id="roadTaxPaidAmount" class="form-control" value='<c:out value="${VeT.roadtaxamount}" />' ></td>
                </tr>
                <tr>
                    <td class="texttitle1">Period</td>
                    <td class="text1"><select name="roadTaxPeriod" id="roadTaxPeriod" class="form-control"  >
                            <option>Quarterly</option>
                            <option>Half Yearly</option>
                            <option>Yearly</option>
                        </select>
                    </td>
                    <td class="texttitle1">Road Tax From Date</td>
                    <td class="text1"><input type="text" name="roadTaxFromDate" id="roadTaxFromDate" class="datepicker" value='<c:out value="${VeT.roadTaxFromDate}" />' ></td>
                </tr>
                <tr>
                    <td class="texttitle1">Road Tax To Date</td>
                    <td class="text1"><input type="text" name="nextRoadTaxDate" id="nextRoadTaxDate" class="datepicker" value='<c:out value="${VeT.nextRoadTaxDate}" />' ></td>
                    <td class="texttitle1">Remarks</td>
                    <td class="text1"><input type="text" name="roadTaxRemarks" id="roadTaxRemarks" class="form-control" value='<c:out value="${VeT.remarks}" />' ></td>                    
                </tr>
                    </c:forEach>
                    </c:if>
                <tr>
                    <td align="center" class="texttitle1" colspan="4"></td>
                </tr>
            </table>
            <br>
            <center>
                <!--Upload Copy Of Bills&emsp;<input type="file" />
                <br>-->
                <br>
                <input type="submit" class="button" value=" Update " />
            </center>
    </body>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</html>


