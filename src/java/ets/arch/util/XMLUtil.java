/*---------------------------------------------------------------------------
 * XMLUtil.java
 *
 * Copyright (c) ES Systems.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ES Systems ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ES Systems.
 -----------------------------------------------------------------------------*/
package ets.arch.util;
/**
*
* Modification Log
* ---------------------------------------------------------------------
* Ver   Date              Modified By               Description
* ---------------------------------------------------------------------
* 1.0   26 Mar 2008       Srinivasan.R             Initial Version
*
***********************************************************************/

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import ets.arch.exception.FPRuntimeException;
import ets.arch.exception.XMLErrorHandler;
import ets.domain.util.FPLogUtils;

/**
 * XMLUtil:This class has methods specific to loading All application related support XMLs.<br>
 * This Loads the document, parses it and returns the XML in the form of an Element.<br>
 * The method also creates XML outputs in the form of a string.
 *
 * @author Srinivasan.R <br>
 * @version 1.0 <br>
 * @since   event Portal Iteration 0
 */


public class XMLUtil {
	   private Document xmlDocument = null;
	   //private String filename = null;

	   public XMLUtil() {
		   //
	   }

	   /**
	    * Loads the input XML , parses it and validates it against the xml
	    *  template <br>
	    * and builds a tree to form a DOM structure.<br>
	    * Once the DOM structure is formed it intializes to the Document object
	    * XMLObject<br>
	    * @param xmlInputRequest - This represents the xml input that comes
	    * in the form of a String
	    * @param xmlTemplateName - This represents the xml template name
	    *  which is given for validation.
	    *
	    */
	   public void validateXml(InputStream xmlInputRequest, String xmlTemplateName) {
	      try {

					System.setProperty("javax.xml.parsers.DocumentBuilderFactory","org.apache.xerces.jaxp.DocumentBuilderFactoryImpl");
					DocumentBuilderFactory event = DocumentBuilderFactory.newInstance();
					event.setNamespaceAware(true);
					event.setValidating(true);
					event.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaLanguage","http://www.w3.org/2001/XMLSchema");
					event.setAttribute("http://java.sun.com/xml/jaxp/properties/schemaSource",xmlTemplateName);
					DocumentBuilder builder = event.newDocumentBuilder();

					XMLErrorHandler handler=new XMLErrorHandler();
					builder.setErrorHandler(handler);

					xmlDocument = builder.parse(xmlInputRequest);

					if (handler.getValidationError() == true) {
					handler.getSaxParseException().printStackTrace(System.out);
						throw new SAXException(handler.getSaxParseException());
					}else {
						FPLogUtils.fpDebugLog("XML Document is valid");
					}
	      } catch (SAXException sAXException){
	         throw new FPRuntimeException("EM-SYS-08","XMLUtil","validateXml",
	        sAXException);
	      } catch(ParserConfigurationException parserConfigurationException){
	         throw new FPRuntimeException("EM-SYS-08","XMLUtil","validateXml",
	        parserConfigurationException);
	      } catch (IOException iOException){
	         throw new FPRuntimeException("EM-SYS-07","XMLUtil","validateXml",
	        iOException);
	      }
	   }

	   /**
	    * Loads the document, parses it and returns the XML in the form of an Element.<br>
	    * @param location - This represents path from where the xml is to be loaded.
	    * @return root- This represents the complete tree structure representing the XML.
	    */
	   protected Element loadDocument(String location) {
	      Element root = null;
	      try {
	          URL url = new URL(location);
	          InputSource xmlInp = new InputSource(url.openStream());
	          DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
	          DocumentBuilder parser = docBuilderFactory.newDocumentBuilder();
	          xmlDocument = parser.parse(xmlInp);
	          //returns the corresponding xml file
	          root = xmlDocument.getDocumentElement();
	          root.normalize();


	      } catch (SAXException sAXException){
	         throw new FPRuntimeException("EM-SYS-08","XMLProcessor",
	        		 "loadDocument",sAXException);
	      } catch(ParserConfigurationException parserConfigurationException){
	         throw new FPRuntimeException("EM-SYS-08","XMLProcessor",
	        		 "loadDocument",parserConfigurationException);
	      } catch (IOException iOException){
	         throw new FPRuntimeException("EM-SYS-07","XMLProcessor",
	        		 "loadDocument",iOException);
	      }
	      return root;
	   }


	   /**
	    * Returns the value of an element attribute.<br>
	    * @param tagName - The node or element or tagName from
	    * which the attribute value is required.
	    * @param attributeName -The attribute name for which the
	    * value is required.
	    * @return returnVal -The data within the element attribute.
	    */
	   public String getNodeAttributeValue(String tagName, String attributeName)
	   {
	    String returnVal = "";
	    NodeList nodes = xmlDocument.getElementsByTagName(tagName);
	    Node node = null;
	    if (nodes != null) {
	      node = nodes.item(0);
	    	NamedNodeMap attrs = node.getAttributes();
				for (int i = 0; i < attrs.getLength(); i++) {
					Node myAttribute = attrs.item(i);
					if (myAttribute.getNodeName().equals(attributeName)) {
						returnVal =  myAttribute.getNodeValue();
					}
				}
			}
	    return returnVal;
	   }

	   /**
	    * Returns the tag value for the given Tag Name and Node.
	    * @param node -The node from which the value is required.
	    * @param subTagName -The Tag for which the value is required.
	    * @return returnString -The data within the tag.
	    */
	   public String getSubTagValue(Node node, String subTagName) {
	      int childrenLength = 0;
	      String returnString = null;
	      if (node != null) {
	         NodeList  children = node.getChildNodes();
	         childrenLength = children.getLength();
	         for (int innerLoop =0; innerLoop < childrenLength; innerLoop++) {
	            Node child = children.item(innerLoop);
	            if ((child != null) && (child.getNodeName() != null) && child.getNodeName().equals(subTagName) ) {
	               Node grandChild = child.getFirstChild();
	               if (grandChild.getNodeValue() != null) {
	                  return grandChild.getNodeValue();
	                  }
	               }
	         }
	         }
	      return returnString;
	   }

	   /**
	    * Returns the value from the specified tag.<br>
	    *
	    * @param tagName -The Tag for which the value is required.
	    * @return returnVal- The data within the tag.
	    */
	   public String getNodeValue(String tagName) {
	      // Root Node of the XML structure
	      Node root        = xmlDocument.getDocumentElement();
	      // Node List of a particular TAG in the XML structure.
	      NodeList list    = xmlDocument.getElementsByTagName(tagName);
	      Node subNode      = null;
	      String returnVal  = "";
	      if (list == null || list.getLength() == 0) {
	         return "";
	      } else {
	         subNode = list.item(0);
	      }

	      Node valueNode = subNode.getFirstChild();
	      if (valueNode == null) {
	         return "";
	      }
	      if(!valueNode.hasChildNodes()) {
	         returnVal = valueNode.getNodeValue();
	      }
	      return returnVal;
	   }

	}
