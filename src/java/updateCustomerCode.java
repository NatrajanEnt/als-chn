/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateCustomerCode {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Connection con1 = null;
        Connection con2 = null;
        Statement statement = null;
        Statement statement1 = null;
        Statement statement2 = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        ResultSet rs2 = null;
        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "throttlebaxi_NEWDesign";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "admin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            con1=DriverManager.getConnection(url + dbName, userName, password);
            con2=DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement updateEmpCode = null;
                PreparedStatement selectCust = null;
                PreparedStatement updateCust = null;
                statement = con.createStatement();
                statement1 = con1.createStatement();
                statement2 = con2.createStatement();

                System.out.println("Updated successfully");
                String sql1="select a.route_contract_id,first_pickup_id,point1_id,point2_id,point3_id,point4_id,final_point_id,vehicle_type_id,load_type_id,container_type_id,container_qty,ratewithreefer,ratewithoutreefer,approval_status from ts_contract_routes a,ts_contract_rates b where a.contract_id=b.contract_id and a.route_contract_id=b.route_contract_id and b.contract_id <> 408";

                String sql3="update ts_contract_rates set approval_status=?,approval_remarks=? where route_contract_id=?";

                selectCust=con.prepareStatement(sql1);

                //String sql2 = "select a.route_contract_id,ratewithreefer, ratewithoutreefer,approval_status from ts_contract_routes a,ts_contract_rates b where a.contract_id=b.contract_id and a.route_contract_id=b.route_contract_id and b.contract_id =408  and first_pickup_id=? and point1_id=? and point2_id=? and point3_id=? and point4_id=? and final_point_id=? and vehicle_type_id=? and load_type_id=? and container_type_id=? and container_qty=?";


                      rs = statement.executeQuery(sql1);

/* rs.getInt[2], rs.getInt[3], rs.getInt[4], rs.getInt[5], rs.getInt[6], rs.getInt[7],
rs.getInt[8], rs.getInt[9], rs.getInt[10], rs.getInt[11]); */

                System.out.println("----------------------");
                int i=1; int OrgRouteContract=0;int OrgRWR=0;int OrgRWOR=0;int custRWR=0;int custRWOR=0;int routeContId=0;
                String approvalStatus = "";
                while (rs.next()) {
                    System.out.println("------- "+rs.getInt(2)+" ------  "+rs.getInt(3)+"  --------  "+rs.getInt(4)+" -------");
                    System.out.println("------- "+rs.getInt(5)+" ------  "+rs.getInt(6)+"  --------  "+rs.getInt(7)+" -------");
                    System.out.println("------- "+rs.getInt(8)+" ------  "+rs.getInt(9)+"  --------  "+rs.getInt(10)+" -------");
                    System.out.println("------- "+rs.getInt(11));

                    String sql2 = "select a.route_contract_id,ratewithreefer, ratewithoutreefer,approval_status from ts_contract_routes a,ts_contract_rates b where a.contract_id=b.contract_id and a.route_contract_id=b.route_contract_id and b.contract_id =408  and first_pickup_id="+rs.getInt(2)+ " and point1_id="+rs.getInt(3)+ " and point2_id="+rs.getInt(4)+ " and point3_id="+rs.getInt(5)+ " and point4_id="+rs.getInt(6)+ " and final_point_id="+rs.getInt(7)+" and vehicle_type_id="+rs.getInt(8)+" and load_type_id="+rs.getInt(9)+" and container_type_id="+rs.getInt(10)+" and container_qty="+rs.getInt(11)+"";
                    System.out.println("Query ------------- "+sql2);
                    updateEmpCode = con1.prepareStatement(sql2);


                    routeContId=rs.getInt(1);

                    custRWR=rs.getInt(12);
                    custRWOR=rs.getInt(13);

                    rs1 = statement1.executeQuery(sql2);
                    if(rs1.next()){
                        updateCust=con2.prepareStatement(sql3);

                        OrgRouteContract=rs1.getInt(1);
                        OrgRWR=rs1.getInt(2);
                        OrgRWOR=rs1.getInt(3);

                        if(custRWR >= OrgRWR && custRWOR >= OrgRWOR){
                            approvalStatus="1";
                        }
                         else{
                            approvalStatus="2";
                        }

                        updateCust.setString(1,approvalStatus);
                        updateCust.setString(2,"SYSTEM APPROVED");
                        updateCust.setInt(3,routeContId);
                        int updateSucc=updateCust.executeUpdate();

                        System.out.println("--------- TEST -----------");
                        System.out.println("--------- approvalFlag ----------- "+approvalStatus);
                        System.out.println("--------- Route contract Id -----------"+routeContId);
                        System.out.println("--------- Org RateWith Reefer -----------"+OrgRWR);
                        System.out.println("--------- Org RateWithout Reefer -----------"+OrgRWOR);
                        System.out.println("--------- cust Rate With Reefer -----------"+custRWR);
                        System.out.println("--------- cust Rate Without Reefer -----------"+custRWOR);
                        System.out.println("--------- Update status -----------"+updateSucc);
                    }

                }

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
