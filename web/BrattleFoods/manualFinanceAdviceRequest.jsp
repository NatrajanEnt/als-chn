<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.text.DecimalFormat"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!--        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {

                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });


            function replaceSpecialCharacters()
            {
                var content = document.getElementById("requestremarks").value;

                //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
                // content=content.replace(/[^a-zA-Z0-9]/g,'');
                content=content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g,' ');
                document.getElementById("requestremarks").value=content;
                //alert("Repalced");

            }
        </script>

    </head>
    <script language="javascript">
        function submitPage(){

            if(textValidation(document.approve.advancerequestamt,'Request Amount')){
                return;
            }
            if(textValidation(document.approve.requeststatus,'Request Status')) {

                return;
            }
            if(textValidation(document.approve.requeststatus,'Request Status')) {

                return;
            }
            if(textValidation(document.approve.requestremarks,'Request Remarks')){

                return;
            }

            document.getElementById("submitButton").style.display = 'none';
            document.approve.action = '/throttle/manualAdvanceRequest.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.advancerequestamt.select();
        }
    </script>
<div class="pageheader">
      <h2><i class="fa fa-edit"></i> Request Advance </h2>
      <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
          <li><a href="index.html">Home</a></li>
          <li><a href="general-forms.html">Leasing Operation</a></li>
          <li class="">Request Advance</li>

        </ol>
      </div>
      </div>
   <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">

    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
                        request.setAttribute("menuPath", "Advance >> Manual Request");
                        String tripid = request.getParameter("tripid");
                        String type = "M";
            %>
            <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
           
            <%@ include file="/content/common/message.jsp" %>

            <br>
            
            <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	 

<!--	  <span style="float: right">
		<a href="?paramName=en">English</a>
		|
		<a href="?paramName=ar">العربية</a>
	  </span>-->
            

            <%

                        String ovrtotalamount = (String) request.getAttribute("ovrtotalamount");

                        String todayreqamt = (String) request.getAttribute("totalReqamount");
                        String totalTodayReqamount = (String) request.getAttribute("totalTodayReqamount");
                        String fcreqamt = (String) request.getAttribute("fcRequestamount");
                        Double todayreqted = Double.parseDouble(todayreqamt);
                        Double fcreqted = Double.parseDouble(fcreqamt);
                        Double todayallowed = fcreqted - todayreqted;

                        DecimalFormat df = new DecimalFormat("#0.00");
                        double totreq = Double.parseDouble(todayreqamt);


            %>
            <input type="hidden" name="consignmentOrderId" value="<c:out value="${consignmentOrderId}"/>"/>
            <input type="hidden" name="tripCode" value="<c:out value="${tripCode}"/>"/>
            <input type="hidden" name="totalTodayReqamount" value="<%=totalTodayReqamount%>"/>
            <input type="hidden" name="todayreqamount" value="<%=todayreqamt%>"/>
            <input type="hidden" name="fcreqamount" value="<%=fcreqamt%>"/>
            <input type="hidden" name="ovrtotalamount" value="<%=ovrtotalamount%>"/>
            <input type="hidden" id="todayallow" name="todayallow" value="<%=todayallowed%>"/>
            <input type="hidden" id="tripType" name="tripType" value="<c:out value="${tripType}"/>"/>
            <input type="hidden" id="statusId" name="statusId" value="<c:out value="${statusId}"/>"/>
            <input type="hidden" id="nextTrip" name="nextTrip" value="<c:out value="${nextTrip}"/>"/>


            
            
            
            

          <table width="300" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">

                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:300;">

                            <div id="first">

                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b><spring:message code="operations.label.ActualAdvancePaid" text="default text"/>: </b></font></td>
                                        <c:if test = "${manualfinanceAdviceDetails != null}" >
                                            <c:forEach items="${manualfinanceAdviceDetails}" var="FD">
                                                <td align="right"> <c:out value="${FD.actualadvancepaid}"/></td>
                                            </c:forEach>
                                        </c:if>
                                    </tr>
                                    <tr id="exp_table" >
                                        <td> <font color="white"><b><spring:message code="operations.label.AlreadyRequested+PaidToday" text="default text"/>:  </b></font></td>
                                        <td align="right"> <%=df.format(totreq)%> </td>
                                    </tr>

                                    <tr id="exp_table" >
                                        <td> <font color="white"><b><spring:message code="operations.label.YoucanRequestupto" text="default text"/>:</b></font></td>
                                        <td align="right">
                                         <%--   <%=df.format(todayallowed)%>  --%>
                                         <c:if test = "${manualfinanceAdviceDetails != null}" >
                                            <c:forEach items="${manualfinanceAdviceDetails}" var="FD">
                                                <td align="right"> <c:out value="${FD.estimatedexpense}"/></td>
                                            </c:forEach>
                                        </c:if>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>


            <c:if test = "${manualfinanceAdviceDetails != null}" >

              <table class="table table-bordered">
                    <c:forEach items="${manualfinanceAdviceDetails}" var="FD">
                        <tr align="center">
                            <td colspan="2" align="center" class="contenthead" height="30">
                                <div ><spring:message code="operations.label.ManualAdvanceRequest" text="default text"/></div></td>
                        </tr>
                        <tr>
                            <td class="text1" height="30"><spring:message code="operations.label.CnoteName" text="default text"/></td>
                            <td class="text1" height="30"><c:out value="${FD.cnoteName}"/>
                                <input type="hidden" name="cnote" value='<c:out value="${FD.cnoteName}"/>'/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2" height="30"><spring:message code="operations.label.VehicleType" text="default text"/></td>
                            <td class="text2" height="30"><c:out value="${FD.vehicleTypeName}"/>
                                <input type="hidden" name="vehicletype" value="<c:out value="${FD.vehicleTypeName}"/>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text1" height="30"><spring:message code="operations.label.VehicleNo" text="default text"/></td>
                            <td class="text1" height="30"><c:out value="${FD.regNo}"/>
                                <input type="hidden" name="vegno" value="<c:out value="${FD.regNo}"/>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2" height="30"><spring:message code="operations.label.RouteName" text="default text"/></td>
                            <td class="text2" height="30"><c:out value="${FD.routeName}"/>
                                <input type="hidden" name="routename" value="<c:out value="${FD.routeName}"/>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text1" height="30"><spring:message code="operations.label.DriverName" text="default text"/></td>
                            <td class="text1" height="30"><c:out value="${FD.driverName}"/>
                                <input type="hidden" name="drivername" value="<c:out value="${FD.driverName}"/>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2" height="30"><spring:message code="operations.label.PlannedDate" text="default text"/></td>
                            <td class="text2" height="30"><c:out value="${FD.planneddate}"/>
                                <input type="hidden" name="planneddate" value="<c:out value="${FD.planneddate}"/>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text1" height="30"><spring:message code="operations.label.ActualAdvancePaid" text="default text"/></td>
                            <td class="text1" height="30"><c:out value="${FD.actualadvancepaid}"/>
                                <input type="hidden" name="actadvancepaid" value="<c:out value="${FD.actualadvancepaid}"/>"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text2" height="30"><spring:message code="operations.label.EstimatedExpense" text="default text"/></td>
                            <td class="text2" height="30"><c:out value="${FD.estimatedexpense}"/>
                                <input type="hidden" name="estimatedexpense" value="<c:out value="${FD.estimatedexpense}"/>"/>
                            </td>
                        </tr>

                        <tr>
                            <td class="text1" height="30"><spring:message code="operations.label.TripDay" text="default text"/></td>
                            <td class="text1" height="30"><c:out value="${FD.tripday}"/>
                                <input type="hidden" name="tripday" value="<c:out value="${FD.tripday}"/>"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="text2" height="30"><font color="red">*</font><spring:message code="operations.label.ReqAdvanceAmt" text="default text"/>.</td>
                            <td class="text2" height="30"><input id="advancerequestamt" name="advancerequestamt" type="text" class="textbox" value="0" onchange="checkamt();"></td>
                        </tr>
                        <tr>
                            <td class="text2" height="30"><font color="red">*</font><spring:message code="operations.label.SelectCurrency" text="default text"/></td>
                            <td class="text2" height="30">
                                <select name="currencyId" id="currencyId" >
                                    <option value="0">--<spring:message code="operations.label.YoucanRequestupto" text="default text"/><spring:message code="operations.label.Select" text="default text"/>--</option>
                                    <c:if test = "${countryList != null}">
                                 <c:forEach items="${countryList}" var="cc">
                                     <option value="<c:out value="${cc.currencyid}" />" selected><c:out value="${cc.currencyCode}" /></option>
                                 </c:forEach>
                             </c:if>
                                </select>

                            </td>
                        </tr>

                        <input type="hidden" name="requeststatus" value="0"/>
                        <input type="hidden" name="tripid" value="<%=tripid%>"/>
                        <input type="hidden" name="batchType" value="<%=type%>"/>
                        <input type="hidden" name="estimatedadvance" value="<c:out value="${FD.tobepaidtoday}"/>"/>

                        <input name="tobepaidtoday" type="hidden" class="textbox" value="<c:out value="${FD.cnoteName}"/>" readonly>
                        <tr>
                            <td class="text1" height="30"><font color="red">*</font><spring:message code="operations.label.RequestOn" text="default text"/></td>
                            <td class="text1" height="30">
                                <input name="requeston" type="text" class="datepicker" value="">
                            </td>
                        </tr>

                        <tr>
                            <td class="text2" height="30"><font color="red">*</font><spring:message code="operations.label.Requestremarks" text="default text"/></td>
                            <td class="text2" height="30">
                                <select name="requestremarks" id="requestremarks" class="form-control" style="width:180px;height:40px;">
                                            <option selected value="For Food & Toll">For Food & Toll </option>
                                            <option  value="For  Food">For Food </option>
                                            <option  value="For Toll">For Toll </option>
                                            <option value="other"> other </option>
                                        </select>
                               
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
            <br>
            <div id="submitButton" style="display:block">
                <center>
                    <input type="button" value="<spring:message code="operations.label.SAVE" text="default text"/>" id="mySubmit" class="button" onClick="submitPage();">
                    &emsp;
                    <!--                <input type="reset" class="button" value="Clear">-->
                </center>
            </div>
            <script type="text/javascript">
                function validateSpecialCharacters(){
                    var spclChars = "!@#$%^&*()/-:?<>+.~`'_="; // specify special characters
                    var content = document.getElementById("requestremarks").value;
                    for (var i = 0; i < content.length; i++){
                        if (spclChars.indexOf(content.charAt(i)) != -1){
                            alert ("Special characters are not allowed here.");
                            document.getElementById("requestremarks").value = "";
                            return false;
                        }
                    }
                }
            </script>
            <script>
                function checkamt(){
                    if(document.getElementById("nextTrip").value == "0" ){
                        if( document.getElementById("advancerequestamt").value > parseInt(document.approve.todayallow.value)){
                            alert("Maximum amount payable is INR. "+document.approve.todayallow.value+" per day");
                            document.approve.advancerequestamt.value=0;
                            document.getElementById("mySubmit").style.display = "none";
                            document.approve.advancerequestamt.focus();
                        }else{
                            document.getElementById("mySubmit").style.display = "block";
                        }
                    }else if(document.getElementById("nextTrip").value == "1" ){
                        if( document.getElementById("advancerequestamt").value > parseInt(10000)){
                            alert("Maximum amount payable is INR. 10,000 per day");
                            document.approve.advancerequestamt.value=0;
                            document.getElementById("mySubmit").style.display = "none";
                            document.approve.advancerequestamt.focus();
                        }else{
                            document.getElementById("mySubmit").style.display = "block";
                        }
                    }

                    
                    
                }
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
