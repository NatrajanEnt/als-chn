<%-- 
    Document   : popUpCustomerWiseProfit
    Created on : Dec 28, 2013, 6:35:52 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Popup Customer Wise Profit Report</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
         <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
         <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
       <script type="text/javascript">
            function submitPage() {
                document.popUpCustomerReport.action = '/throttle/viewCustomerWiseProfitMergingDetails.do?param=ExportExcel';
                document.popUpCustomerReport.submit();
            }

            function viewTripDetails(tripId) {
                   //alert('333');
                window.open('/throttle/viewTripSheetDetails.do?tripId='+tripId+'&tripSheetId='+tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }

        </script>
    </head>
    <body>
        <form name="popUpCustomerReport" action=""  method="post">
            <br>
            <br>
            <br>
    <br>
            <br>
            <br>
            <c:if test = "${popupCustomerProfitReport != null}" >
                <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                    <thead>
                        <tr height="80">
                            <th class="contentsub"><h3>S.No</h3></th>
                            <th class="contentsub"><h3>Trip Code</h3></th>
                            <th class="contentsub"><h3>Merging Code</h3></th>
                            <th class="contentsub"><h3>C Note</h3></th>
                            <th class="contentsub"><h3>Trip Start date</h3></th>
                            <th class="contentsub"><h3>Trip End date</h3></th>
                            <th class="contentsub"><h3>Customer</h3></th>
                            <th class="contentsub"><h3>Route</h3></th>
                            <th class="contentsub"><h3>Vehicle No</h3></th>
                            <th class="contentsub"><h3>Revenue</h3></th>
                            <th class="contentsub"><h3>Expense</h3></th>
                            <th class="contentsub"><h3>Profit</h3></th>
                            <th class="contentsub"><h3>profit %</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${popupCustomerProfitReport}" var="popCus">

                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" >
                                    <a href="" onclick="viewTripDetails('<c:out value="${popCus.tripId}"/>');"><c:out value="${popCus.tripCode}"/></a>
                                </td>
                                <td align="left" >
                                    <c:if test="${popCus.tripMergingId != ''}">
                                    MC-<c:out value="${popCus.tripMergingId}"/>/<c:out value="${popCus.orderSequence}"/>
                                    </c:if>
                                    <c:if test="${popCus.tripMergingId == ''}">
                                    &nbsp;
                                    </c:if>
                                </td>
                                <c:if test="${popCus.customerName != 'Empty Trip'}">
                                <td align="left" ><c:out value="${popCus.consignmentNoteNo}"/></td>
                                </c:if>
                                <c:if test="${popCus.customerName == 'Empty Trip'}">
                                <td align="left" >&nbsp;</td>
                                </c:if>
                                <td align="left" ><c:out value="${popCus.tripStartDate}"/></td>
                                <td align="left" ><c:out value="${popCus.tripEndDate}"/></td>
                                <td align="left" ><c:out value="${popCus.customerName}"/></td>
                                <td align="left" ><c:out value="${popCus.routeName}"/></td>
                                <td align="left" ><c:out value="${popCus.regno}"/></td>
                                <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${popCus.revenue}" /></td>
                                <td align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${popCus.expenses}" /></td>
                                <c:if test="${popCus.customerName != 'Empty Trip'}">
                                <c:set var="profitValue" value="${popCus.revenue - popCus.expenses}"/>
                                <c:set var="profit" value="${profitValue/popCus.revenue}"/>
                                <c:set var="profitPercent" value="${profit*100}"/>
                                <c:if test="${profitValue > 0}" >
                                    <td align="right">
                                    <font color="green">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitValue}" />
                                    </font>
                                    </td>
                                </c:if>
                                <c:if test="${profitValue <= 0}" >
                                    <td align="right">
                                    <font color="red">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitValue}" />
                                    </font>
                                    </td>
                                </c:if>
                                     <c:if test="${profitPercent > 0}" >
                                    <td align="right">
                                    <font color="green">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />
                                    </font>
                                    </td>
                                </c:if>
                                <c:if test="${profitPercent <= 0}" >
                                    <td align="right">
                                    <font color="red">
                                     <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />
                                    </font>
                                    </td>
                                </c:if>
                               </c:if>
                               <c:if test="${popCus.customerName == 'Empty Trip'}">
                                   <td align="right"><font color="red">0.00</font></td>
                                   <td align="right"><font color="red">0.00</font></td>
                               </c:if>
                            </tr>
                            <%
                                       index++;
                                       sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <table style="width: 1100px" align="center" border="0">
            <tr>
                <td align="center">
                    <input type="hidden" name="tripId" id="tripId" value="<c:out value="${tripId}"/>"/>
                    <input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage();">
                </td>
            </tr>
            </table>
       <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>



        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>