<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : RAM
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <title>PrintTripSheet</title>
    </head>
    <body>
<style type="text/css">
    @media print {
        #printbtn {
            display :  none;
        }
    }
</style>
<style type="text/css" media="print">
    @media print
    {
        @page {
            margin-top: 0;
            margin-bottom: 0;
        }
    } 
</style>
        <script>

//            window.onunload = function() {
//                window.opener.location.reload();
//            }
        </script>
        <form name="enter" method="post">
            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            String billingPartyId = (String)request.getAttribute("billingPartyId");
            String companyId = (String)request.getAttribute("companyId");
            %>
            <div id="printContent">
                <table align="center"  style="border-collapse: collapse;width:400px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                       background:url(images/dict-logoWatermark.png);
                       background-repeat:no-repeat;
                       background-position:center;
                       border:1px solid;
                       /*border-color:#CD853F;width:800px;*/
                       border:1px solid;
                       /*opacity:0.8;*/
                       /*font-weight:bold;*/
                       ">
                    <tr>
                        <br>
                        <br>
                        <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                            <table align="left" >
                                <tr>
                                    <% if ("2479".equals(billingPartyId)) {%>
                                    <% // if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                     <td align="left"><img src="images/triway_forward.jpg" width="165" height="85"/></td>
                                        <%}else {%>
                                    <td align="left"><img src="images/Route Logo.JPG" width="160" height="65"/></td>
                                        <%}%>

                                    <td style="padding-left: 180px; padding-right:42px;">
                                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;


                                <center>
                                    <% if ("2479".equals(billingPartyId)) {%>
                                     <font face="verdana" size="4"><b>Triway Forwarder Pvt Ltd.</b></font></center><br>
                                    <%}else {%>
                                <font face="verdana" size="4"><b>Route Logistics India Pvt.Ltd.</b></font></center>
                                <%}%>

                                <center>
                                    <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                    <font face="verdana" size="3" >148, Ponneri High Road, Edayanchavadi, New Nappalayam, Chennai,Tamilnadu.
                                    </font>
                                    <%}else if ("1464".equals(companyId)) {%>
                                     <font face="verdana" size="3" >Industrial Subrub, Yeshwanthpur, Bangalore 560022  </font>
                                    <%} else{%>
                                       <font face="verdana" size="3" >14,Jaffer Street, Chennai,Tamilnadu.
                                    </font>
                                    <% } %>
                                </center>
                        </td>
                        <td align="right">
                            <br>
                            <div id="externalbox" style="width:3in">
                                <div id="inputdata" ></div>
                            </div>
                        </td>
                    </tr>
                </table>
                </td>
                </tr>

					<td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <tbody>
                            <tr>
                                 <td valign="top" width="55%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                    <br>
                                    <font face="verdana" size="3"><b>&nbsp;Consignor<br></b></font><br> <font face="verdana" size="3"> <c:out value="${consignorName}"/>,<br><c:out value='${consignorAddress}'/>,<br>GST IN :<c:out value="${consignorGST}" /></font>  <br>
                                    <br>
                                    <br>
                                    <br>
                                </td>
                                 <td width="45%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                            <font face="verdana" size="3">
                                            <!--&emsp;<b>Trip.NO:</b></font><font face="verdana" size="2">&nbsp;&nbsp;<c:out value="${tCode}"/></font>-->
                                            <br>
<!--                                                <font face="verdana" size="2">&nbsp;&nbsp;<b>LR NO&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp;</b><font face="verdana" size="2">MAA/<c:out value="${consignmentId}"/>/2018-19&nbsp; </font><br><br>-->
                                              <font face="verdana" size="2">&nbsp;&nbsp;<b>LR NO :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</b></font><font face="verdana" size="3"> MAA
                                                <c:if test="${orderType == 'Moffusil-Export-FCL'}">
                                                  EX 
                                                </c:if>
                                                <c:if test="${orderType == 'Moffusil-Import-FCL'|| 'Moffusil-Import-LCL'}">
                                                  IM 
                                                </c:if>
                                                <c:if test="${orderType == 'Moffusil-ICD'}">
                                                  IC
                                                </c:if> 
                                                /&nbsp;<c:out value="${lrNumber}"/> / 2017-18&nbsp; </font><br><br>
                                            <font face="verdana"  size="2">
                                                <font face="verdana"  size="2">&nbsp;&nbsp;<b>LR DATE :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b><font face="verdana" size="3"><c:out value="${preStartDate}"/>&nbsp; </font>
                                            <font face="verdana"  size="2">
                                                <font face="verdana"  size="2"><br>
                                                <br>
                                                &nbsp;&nbsp;<b>Booking No :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font><font face="verdana"  size="3"><c:out value="${tCode}"/></font>
                                                <font face="verdana"  size="2"><br>
                                                <br>
                                                &nbsp;&nbsp;<b>Job No :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font><font  face="verdana" size="2"><c:out value="${customerOrderReferenceNo}"/><br></font>
                                                <br>
                                   </td>
                              </tr>
                              <tr>
                              <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">

                                  <br> <font face="verdana" size="3"><b>&nbsp;Consignee</b></font><br>
                                                                                                    <br><font face="verdana"  size="3"><c:out value="${consigneeName}"/>,<br><c:out value="${consigneeAddress}"/>,<br>GST IN : <c:out value="${consigneeGST}"/></font><br>
								                                    <br>
                                </td>
								<td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">


									<!--<br><font face="verdana" size="2">&emsp;<b>Form KK</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;</font><br><br>-->
									<font face="verdana"  size="2">&emsp;<b>Value Declared :&emsp;&emsp;</b></font><font face="verdana" size="3">&nbsp; As Per Bill</font><br><br>

									<font face="verdana" size="2">&emsp;<b>No of Packages :&emsp;&emsp;</b></font><font face="verdana"  size="3">&nbsp;<c:out value="${packageNos}"/> pkg.</font><br><br>
									<font face="verdana" size="2">&emsp;<b>Volume :&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</b></font><font  face="verdana" size="3">&nbsp; <c:out value="${volume}"/> CBM.</font><br><br>

									<font face="verdana"  size="2">&emsp;<b>Gross Weight :&emsp;&emsp;&emsp;</b></font><font face="verdana" size="3">&nbsp;<c:out value="${weight}"/> kgs.</font><br><br>
                                                                        <font face="verdana" size="2">&emsp;<b>E-Way Bill No :&emsp;&emsp;&emsp;</b></font><font face="verdana"  size="3">&nbsp;<c:out value="${ewayBillNo}"/>&emsp;</font><br><br>
								</td>
                              </tr>
                         <tr>
                              <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                              <br> <font face="verdana"  size="3"><b>Delivery At<br></b></font><br><font face="verdana"  size="3"><c:out value="${consigneeName}"/>,<br><c:out value="${consigneeAddress}"/>,<br>GST IN :<c:out value="${consigneeGST}"/></font><br>
							  <br>
							  <br>
							  <br>
                              </td>
                        	  <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                            <% if ("2479".equals(billingPartyId)) {%>
                                             <br><font face="verdana" size="2">&emsp;<b>Contact Person :</b></font><font face="verdana" size="3">&emsp;&emsp;<b></b>Mr.Sanjay</font><br><br>
                                             <font face="verdana" size="2">&emsp;<b>Mobile :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b>+91 9945536141</font><br><br>
                                             <font face="verdana" size="2">&emsp;<b>Email :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;<b>&nbsp;</b>sanjay@routelogistics.net </font><br><br>
                                             <%}else if("1464".equals(companyId)) {%>
                                            <br><font face="verdana" size="2">&emsp;<b>Contact Person :</b></font><font face="verdana" size="3">&emsp;&emsp;<b></b>Mr.Sanjay</font><br><br>
                                             <font face="verdana" size="2">&emsp;<b>Mobile :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b>+91 9945536141</font><br><br>
                                             <font face="verdana" size="2">&emsp;<b>Email :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;<b>&nbsp;</b>sanjay@routelogistics.net </font><br><br>
                                             <%} else {%>
                                             <br><font face="verdana" size="2">&emsp;<b>Contact Person :</b></font><font face="verdana" size="3">&emsp;&emsp;<b></b>Mr.Saran</font><br><br>
                                             <font face="verdana" size="2">&emsp;<b>Mobile :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&nbsp;&nbsp;<b>&nbsp;</b>+91 89398 33084 /85 /45</font><br><br>
                                             <font face="verdana" size="2">&emsp;<b>Email :</b></font><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;<b>&nbsp;</b> </font><br><br>
                                             <%}%>
                                    </td>
							</tr>

							<tr>
							  <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
								<% if ("1464".equals(companyId)) {%> 
                                                                <br> <font face="verdana" size="3"><b>&nbsp;Booking Office</b></font><br><br><font face="verdana" size="3">&nbsp;&nbsp;BANGALORE &emsp;&emsp; ROUTE LOGISTICS (I) P LTD</font><br>
                                                                <% }else{%>
                                                              <br> <font face="verdana" size="3"><b>&nbsp;Booking Office</b></font><br><br><font face="verdana" size="3">&nbsp;&nbsp;CHENNAI &emsp;&emsp; ROUTE LOGISTICS (I) P LTD</font><br>
                                                              <%}%>
							  <br>
							  <br>
							  <br>
							  </td>
							  <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                                                              <br><font face="verdana" size="2">&emsp;<b>Container :&nbsp;</b></font><font face="verdana" size="3"><c:out value='${containerNo}'/>&emsp;<c:out value='${containerNo1}'/></font><br><br>
                                                              <font face="verdana" size="2">&emsp;<b>Driver :&nbsp;&nbsp;&nbsp;&emsp;</b></font><font face="verdana" size="3"><c:out value='${driverName}'/></font>
                                                              <font face="verdana" size="3">&nbsp; /&nbsp; +91&nbsp;<c:out value='${driverMobile}'/><br><br>
								 <font face="verdana" size="2">&emsp;<b>Truck No:&nbsp;</b></font><font face="verdana" size="3">&nbsp;<c:out value='${vehicleNo}'/>&nbsp;&nbsp;(<c:out value='${vehicleType}'/>)</font><br>
								 <br><font face="verdana" size="2">&emsp;<b>Route :&nbsp;&nbsp;&emsp;</b></font><font face="verdana" size="3">&nbsp;<c:out value='${routeInfo}'/></font><br><br>
								 <font face="verdana" size="2">&emsp;<b>Terms :&emsp;&emsp;</b></font><font face="verdana" size="3">Collect</font><br>
								<br> <font face="verdana" size="2">&emsp;<b>Type of Delivery :&emsp;</b></font><font face="verdana" size="3">&emsp; Normal</font><br><br>

							  </td>
							</tr>

							<tr>
							  <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
								 <br> <font face="verdana" size="3"><b>&nbsp;List of Document</b></font><br><br><font face="verdana" size="3">&nbsp;&nbsp;Invoice,&emsp; Packing List,&emsp; Bill of Entry.</font><br>
								  <br>
								  <br>
								  <br>
							  </td>
							  <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
								 <br><font face="verdana" size="2">&emsp; <b>Remarks</b></font><font face="verdana" size="3">&emsp;<c:out value='${vehicleRemarks}'/></font><br>
								 <br><br><br><br><br>
							  </td>
							</tr>

							<tr>
								<td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
									<br><font face="verdana" size="3"><b>&nbsp;Owners Risk</b></font><br><font face="verdana" size="3">
									&nbsp;&nbsp;&nbsp;Goods to be insured by party at Owners Risk.<br>
									&nbsp;&nbsp;&nbsp;Not Responsible for Breakage,Damages,etc.<br>
									&nbsp;&nbsp;&nbsp;Subject to Chennai Jurisdiction Only.<br>
									 <br>
									 <br>
									 <br>
    		   				  </td>
							  <td valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
								<% if ("1464".equals(companyId)) {%>   
                                                                    <font face="verdana" size="2"><br>&emsp;<b>Prepared By : </b></font><font face="verdana" size="2"><b>ROUTE LOGISTICS (I)P LTD &emsp;&emsp;&emsp;&emsp;BANGALORE</b></font><br>
                                                                    <font face="verdana" size="3"><br>&emsp;&emsp;&emsp;&emsp;bangalore@routelogistics.net</font><br><br>
								<%}else{%>
                                                                    <font face="verdana" size="2"><br>&emsp;<b>Prepared By : </b></font><font face="verdana" size="2"><b>ROUTE LOGISTICS (I)P LTD &emsp;&emsp;&emsp;&emsp;CHENNAI</b></font><br>
                                                                    <font face="verdana" size="3"><br>&emsp;&emsp;&emsp;&emsp;chennai@routelogistics.net</font><br><br>
                                                                <%}%>
                                                                    <font face="verdana" size="2">&emsp;<b>Prepared On :</b></font><font face="verdana" size="3">&emsp;<c:out value="${grDate}"/></font><br>

							  </td>
							</tr>

							<tr>
								<td colspan="2" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse"style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
										 <br><font face="verdana" size="3">&nbsp;&nbsp;<b>Description :</b></font><br><font face="verdana" size="3">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<c:out value="${orderType}"/><br>
										 &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;BE NO :<c:out value="${billOfEntry}"/><br>
										&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;BE DATE :<c:out value="${challanDate}"/>
									  <br>
									  <br>
									  <br>
								  </td>
							</tr>
                            </tbody>
                          </table>
                </table>
                <!--Muthu....-:) -->
            </div>
            <br>
            <center>
                <input type="button" class="button" id="printbtn" value="Back" onClick="viewTripDetails();" />
                <input type="button" class="button"  value="Print" id="printbtn" onClick="print('printContent');" />
                &nbsp;&nbsp;&nbsp;
                    <c:if test="${tripDetailsList != null}">
                        <c:forEach items="${tripDetailsList}" var="tripStatus">
                            <c:if test="${tripStatus.tripStatusId < 8}">
                                <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripId}"/>&tripType=1&statusId=8"><input type="button" value="Start Trip"></a>
                            </c:if>
                        </c:forEach>
                </c:if>

            </center>
        </form>
        <script type="text/javascript">
            function viewTripDetails() {
                document.enter.action = '/throttle/viewTripSheets.do?&statusId=10&tripType=1&admin=No';
                document.enter.submit();
            }
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            function get_object(id) {
                var object = null;
                if (document.layers) {
                    object = document.layers[id];
                } else if (document.all) {
                    object = document.all[id];
                } else if (document.getElementById) {
                    object = document.getElementById(id);
                }
                return object;
            }
            //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
            get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
            /* ]]> */
        </script>
    </body>
</html>