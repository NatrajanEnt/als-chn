<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
<!--                <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
        function submitPage(){
            $("#save").hide();
            document.approve.action = '/throttle/saveEmptyTripApproval.do';
            document.approve.submit();
        }
      
    </script>


    <body onload="setFocus();">
        <form name="approve"  method="post" >
            <%
            request.setAttribute("menuPath"," Trip Closure >> Approval");
            String tripid = request.getParameter("tripid");
            %>

             <%
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        String todayDate = sdf.format(today);
        %>
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <c:if test = "${emptyTripDetails != null}" >
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="50%" id="bg" class="border">
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30">
                        <div class="contenthead">Trip Closure Approval</div></td>
                </tr>
                <c:forEach items="${emptyTripDetails}" var="emptyTrip">
                <tr>
                    <td class="text2" height="30">Cnote Name</td>
                    <td class="text2" height="30">
                        <input type="hidden" name="cnote" value="<c:out value="${emptyTrip.consignmentNote}"/>"/>
                        <c:out value="${emptyTrip.consignmentNote}"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Trip Code</td>
                    <td class="text1" height="30">
                        <input type="hidden" name="tripCode" value="<c:out value="${emptyTrip.tripCode}"/>"/>
                        <c:out value="${emptyTrip.tripCode}"/>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Customer Name</td>
                    <td class="text2" height="30">
                        <input type="hidden" name="customerName" value="<c:out value="${emptyTrip.customerName}"/>"/>
                        <c:out value="${emptyTrip.customerName}"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Vehicle Type</td>
                    <td class="text1" height="30">
                        <input type="hidden" name="vehicleType" value="<c:out value="${emptyTrip.vehicleType}"/>"/>
                        <c:out value="${emptyTrip.vehicleType}"/></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Vehicle No</td>
                    <td class="text2" height="30">
                        <input type="hidden" name="vehicleno" value="<c:out value="${emptyTrip.vehicleNo}"/>"/>
                        <c:out value="${emptyTrip.vehicleNo}"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Route Name</td>
                    <td class="text1" height="30">
                        <input type="hidden" name="routename" value="<c:out value="${emptyTrip.routeInfo}"/>"/>
                        <c:out value="${emptyTrip.routeInfo}"/>
                    </td>
                </tr>
                <tr>
                    <td class="text2" height="30">Driver Name</td>
                    <td class="text2" height="30">
                        <input type="hidden" name="driverName" value="<c:out value="${emptyTrip.empName}"/>"/>
                        <c:out value="${emptyTrip.empName}"/></td>
                </tr>
               
                <tr>
                    <td class="text1" height="30">Estimated Expense</td>
                    <td class="text1" height="30">
                        <input type="hidden" name="orderExpense" value="<c:out value="${emptyTrip.estimatedExpense}"/>"/>
                        <c:out value="${emptyTrip.estimatedExpense}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Travel Km</td>
                    <td class="text1" height="30">
                        <input type="hidden" name="totalKM" value="<c:out value="${emptyTrip.totalKM}"/>"/>
                        <c:out value="${emptyTrip.totalKM}"/></td>
                </tr>
                 <tr>
                    <td class="text1" height="30">Schedule Date</td>
                    <td class="text1" height="30">
                        <input type="hidden" name="tripScheduleDate" value="<c:out value="${emptyTrip.tripScheduleDate}"/>"/>
                        <c:out value="${emptyTrip.tripStartDate}"/></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Schedule Time</td>
                    <td class="text2" height="30">
                        <input type="hidden" name="tripScheduleTime" value="<c:out value="${emptyTrip.tripStartTime}"/>"/>
                        <c:out value="${emptyTrip.tripStartTime}"/></td>
                </tr>

                <input type="hidden" name="tripid" value="<c:out value="${emptyTrip.tripId}"/>"/>
                 <tr>
                    <td class="text1" height="30"><font color="red">*</font>Request On</td>
                    <td class="text1" height="30">
                        <input name="approvedOn" type="text" class="datepicker" value="<%=todayDate%>">
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Status</td>
                    <td class="text1" height="30"><select name="approvestatus">
                            <option value="" >-Select Any One-</option>
                            <option value="2">Approved</option>
                            <option value="3">Rejected</option>
                </select></td>
                </tr>


                <tr>
                    <td class="text1" height="30">Approve Remarks</td>
                    <td class="text1" height="30"><textarea class="textbox" name="approveremarks"></textarea></td>
                </tr>
</c:forEach>
            </table>
                </c:if>
            <br>
            <center>
                <input type="button" value="Save" id="save" class="button" onClick="submitPage();">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
