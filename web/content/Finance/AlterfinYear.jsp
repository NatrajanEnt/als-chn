
<%--
    Document   : finBank
    Created on : Oct 19, 2012, 12:58:13 PM
    Author     : ASHOK
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">        
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>
        <%@ include file="/content/common/path.jsp" %>
        <%@ include file="/content/common/message.jsp" %>        
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <title> Manage Financial Year</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        
        <script>
  function submitPage()
    {
        if(textValidation(document.enter.finYear,'financial Year')){
            return;
        }
        if(textValidation(document.enter.fromDate,'FromDate')){
            return;
        }
        if(textValidation(document.enter.toDate,'ToDate')){
            return;
        }
        if(textValidation(document.enter.description,'description')){
            return;
        }

        document.enter.action='/throttle/updateFinYear.do';
        document.enter.submit();
}


</script>
        
    </head>
    <body>
        
    <form method="post" name="enter">
        
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        
         <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        
            <c:if test = "${getFinanceYear != null}" >
            
            <table align="center" width="700" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <Td colspan="2" class="contenthead">Alter Fin Year</Td>&nbsp;&nbsp;&nbsp;
                    <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                        <div align="center" id="bankNameStatus" height="20" >&nbsp;&nbsp;</div>
                    </font>
                </tr>
                   <c:forEach items="${getFinanceYear}" var="year">
                <tr height="30">
                    <td class="text2"><font color="red">*</font>Financial Year</td>
                    <td class="text2"><input name="finYear" id="bankName" type="text" readonly class="form-control" value="<c:out value="${year.accountYear}" />" size="50" onchange="checkBankAccountName();" ></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>From Date</td>
                    <td class="text1"> <input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${year.fromDate}" />" ></td>
                </tr>
                <tr height="30">
                    <td class="text2"><font color="red">*</font>To Date</td>
                    <td class="text2"> <input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${year.toDate}" />" ></td>
                </tr>
                <tr height="30">
                    <td class="text1"><font color="red">*</font>Description</td>
                    <td class="text1"><input name="description" type="text" class="form-control" value="<c:out value="${year.discription}"/> " size="50"  ></td>
                </tr>
                <tr height="30">
                    <td class="text2"><font color="red">*</font>Status</td>
                    <td class="text2">
                        <select name="active_ind" id="active_ind">                                                        
                            <option value="Y">Active</option>
                            <option value="N">InActive</option> 
                        </select>                   
                    </td>
                    <input type="hidden" name="statusName" value="<c:out value='${year.statusName}' />"/>
                    <script>
                        document.enter.active_ind.value = document.enter.statusName.value;
                    </script>
                    <input type="hidden" name="accountYearId" value="<c:out value='${year.accountYearId}' />"/>
                </tr> 
            </table>
            <br>
            <br> 
            <center> 
                <input type="button" class="button" value="Update" onclick="submitPage();" />                
            </center>
            <br/><br/>
 </c:forEach>
                </c:if>
          
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
