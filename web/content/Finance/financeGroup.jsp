

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

   <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.ManageGroupMaster"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.ManageGroupMaster"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">  
    <body>
        <form method="post" action="/throttle/addGroupPage.do" class="form-horizontal form-bordered">
 <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
            <c:if test = "${groupLists != null}" >
              <table class="table table-info mb30 table-hover" id="table">
<thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th><spring:message code="finance.label.Groupcode"  text="default text"/></th>
                            <th><spring:message code="finance.label.GroupName"  text="default text"/></th>
                            <th><spring:message code="finance.label.Description"  text="default text"/></th>
                            </thead>
                    
                    <% int index = 0;%>
                    <c:forEach items="${groupLists}" var="GL">
                       
                        <tr height="30">
                            <td  align="left"> <%= index + 1%> </td>
                            <td  align="left"> <c:out value="${GL.groupcode}" /></td>
                            <td   align="left"> <c:out value="${GL.groupname}"/> </td>
                            <td   align="left"> <c:out value="${GL.groupdesc}"/> </td>
<!--                            <td  align="left"> <a href="/throttle/alterGroupDetail.do?groupid=" > Edit </a> </td>-->
                        </tr>
                        <% index++;%>
                    </c:forEach>
                </c:if>
            </table>
            <br>
            <br>
<!--            <center><input type="submit" class="button" value="Add" /></center>-->
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
   </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
