<%--
    Document   : viewTripClosureVehicleList
    Created on : Jul 18, 2014, 11:32:08 AM
    Author     : Arul
--%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery.tokeninput.js"></script>

    <link rel="stylesheet" href="/throttle/css/token-input.css" type="text/css" />
    <link rel="stylesheet" href="/throttle/css/token-input-facebook.css" type="text/css" />

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>


        <form name="ticketing"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table width="815" align="center" border="1">
                <c:if test="${ticketingList != null}">
                    <c:forEach items="${ticketingList}" var="ticket">
                        <tr>
                            <td>Ticket No</td>
                            <td>
                                <input type="hidden" name="ticketId" value="<c:out value="${ticket.ticketId}"/>" />
                                <input type="hidden" name="ticketNo" value="<c:out value="${ticket.ticketNo}"/>" />
                                <c:out value="${ticket.ticketNo}"/>
                            </td>
                            <td>Ticket Subject</td>
                            <td><input type="hidden" name="title" value="<c:out value="${ticket.title}"/>" /><c:out value="${ticket.title}"/></td>
                        </tr>
                        <tr>
                            <td>Raised By</td>
                            <td><c:out value="${ticket.raisedBy}"/>
                            <input type="hidden" name="raisedBy" value="<c:out value="${ticket.raisedBy}"/>" />
                            </td>
                            <td>Raised On</td>
                            <td><c:out value="${ticket.raisedOn}"/>
                            </td>
                        </tr>
                        <tr>
                            <td>Ticket Priority</td>
                            <td><c:out value="${ticket.priority}"/></td>
                            <td>Ticket Status</td>
                            <td><c:out value="${ticket.status}"/></td>
                        </tr>
                        <tr>
                           <td>From</td>
                           <td colspan="3"><input type="text" name="from" id="from" value="<%=session.getAttribute("emailId")%>"  style="width: 200px">
                           </td>
                        </tr>
                        <tr>
                            <td>To</td>
                            <td colspan="3"><textarea name="to" cols="90" rows="1" readonly>naved.ahmad@brattlefoods.com,madhand@entitlesolutions.com,arul@entitlesolutions.com</textarea></td>
<!--                            <td colspan="3"><textarea name="to" id="to" cols="90" rows="1" ></textarea>
                             <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#to").tokenInput("/throttle/getTOEmailList.do", {
                                            preventDuplicates: true
                                        });
                                    });
                                </script>
                            </td>-->
                        </tr>
                        <tr>
                            <td>CC</td>
                            <td colspan="3"><textarea name="cc" cols="90" rows="1" readonly>subhrakanta.mishra@brattlefoods.com,srini@entitlesolutions.com</textarea></td>
<!--                            <td colspan="3"><textarea name="cc" id="cc" cols="90" rows="1" ></textarea>
                                 <script type="text/javascript">
                                    $(document).ready(function() {
                                        $("#cc").tokenInput("/throttle/getTOEmailList.do", {
                                            preventDuplicates: true,
                                            allowCustomEntry: true
                                        });
                                    });
                                </script>
                            </td>-->
                        </tr>
                        <tr>
                            <td>Message</td>
                            <td colspan="3"><textarea name="message" cols="90" rows="10">&#13;&#13;---------Follow Message--------&#13;From: <c:out value="${ticket.raisedBy}"/>&nbsp;<<c:out value="${ticket.from}"/>>&#13;Date: <c:out value="${ticket.raisedOn}"/>&#13;Subject: <c:out value="${ticket.title}"/>&#13;To: <c:out value="${ticket.to}"/>&#13;&#13;<c:out value="${ticket.message}"/></textarea> </td>
                        </tr>
                        <tr>
                            <td>Next Status</td>
                            <td><select name="statusId">
                                    <c:if test="${ticketingStatusList != null}">
                                        <c:forEach items="${ticketingStatusList}" var="statusList">
                                            <c:if test="${statusList.type == 'status' && statusList.statusId != '5'}">
                                                <option value="<c:out value="${statusList.statusId}"/>"><c:out value="${statusList.statusName}"/></option>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                            <td colspan="2"><input type="button" class="button" name="update" value="Send" onclick="updateStatus()"/></td>
                        </tr>
                    </c:forEach>
                </c:if>
            </table>
            <br>
            <c:if test="${ticketingDetailsList != null}">
                <table width="815" align="center" border="1">
                    <tr>
                        <td>Sno</td>
                        <td>Remarks</td>
                        <td>File Name</td>
                    </tr>
                    <c:forEach items="${ticketingDetailsList}" var="ticketList">
                        <tr>
                            <td><c:out value="${ticketList.serialNumber}"/></td>
                            <td><c:out value="${ticketList.remarks}"/></td>
                            <td><c:out value="${ticketList.fileName}"/></td>
                            <td><a href="/throttle/viewTicketingBlobData.jsp?ticketingDetailId=<c:out value="${ticketList.ticketingDetailId}"/>"/>Detail</td>
                        </tr>
                    </c:forEach>
                </table>
            </c:if>
            <br>
            <table width="815" align="center" border="1">
                <c:if test="${ticketingStatusDetailsList != null}">
                    <tr>
                        <td>Sno</td>
                        <td>Remarks</td>
                        <td>Status</td>
                        <td>Followed By</td>
                        <td>Followed On</td>
                    </tr>
                    <c:forEach items="${ticketingStatusDetailsList}" var="ticket">
                        <tr>
                            <td><c:out value="${ticket.serialNumber}"/></td>
                            <td><c:out value="${ticket.remarks}"/></td>
                            <td><c:out value="${ticket.statusName}"/></td>
                            <td><c:out value="${ticket.followedBy}"/></td>
                            <td><c:out value="${ticket.followedOn}"/></td>
                        </tr>
                    </c:forEach>
                </c:if>
            </table>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <script>
            function updateStatus(){
                document.ticketing.action=" /throttle/updateTicketStatus.do";
                document.ticketing.submit();
            }
        </script>
    </body>
</html>