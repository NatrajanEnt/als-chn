<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!--<link rel="stylesheet" href="collapse/css/collapseStyle.css" type="text/css" media="screen, projection">-->
<script type="text/javascript" src="collapse/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="collapse/js/scripts.js"></script>
<c:if test="${nextLevelMasterList != null}">
    <c:forEach items="${nextLevelMasterList}" var="grp">
        <li id="ulNext<c:out value="${grp.groupid}"/>" onclick="callNext('<c:out value="${grp.groupid}"/>')">
            <c:out value="${grp.groupname}"/><c:out value="${grp.groupid}"/>
            <ul id="ulNext<c:out value="${grp.groupid}"/>"></ul>
        </li>
    </c:forEach>
</c:if>
<script>
    function callNext(levelId) {
        alert("levelId == " + levelId);
        var url = './getNextLevelList.do';
        $.ajax({
            url: url,
            data: {levelId: levelId},
            type: "GET",
            success: function (response) {
                $("#ulNext" + levelId).html(response);
            },
            error: function (xhr, status, error) {
            }
        });
    }
</script>        

