
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateBillofEntry {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        String url = "jdbc:mysql://localhost:3306/";
//        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "throttledict";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "root";
//        String password = "admin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement tripIds = null;
                PreparedStatement updateBillofEntry = null;
                statement = con.createStatement();

// updating records
                String sql = "select group_concat(trip_id) from ts_trip_consignment where consignment_order_id=?";
               
                tripIds = con.prepareStatement(sql);
                String sql1 = "update ts_trip_master set bill_of_entry=? where trip_id in(?)";
                updateBillofEntry = con.prepareStatement(sql1);
               
                String[] orderIds = new String[]{"6877","6942"};
                String[] billofEntry=new String[]{"9427130","9315019"};
                for(int i=0;i<orderIds.length;i++){
                    System.out.println("passed orderid"+orderIds[i]);
                tripIds.setString(1,orderIds[i]);
                rs1 = tripIds.executeQuery();
                while (rs1.next()) {
                String tripId=rs1.getString(1);
                    System.out.println("tripId:"+tripId);
                    System.out.println("passed bill of entry:"+billofEntry[i]);
                    String[] temp=tripId.split(",");
                    for(int j=0;j<temp.length;j++){
                        System.out.println("tripId inside:"+temp[j]);
                    updateBillofEntry.setString(1, billofEntry[i]);
                  updateBillofEntry.setString(2, temp[j]);
                  System.out.println("updateBillofEntry pstmt:"+updateBillofEntry);
                  int status=updateBillofEntry.executeUpdate();
                  System.out.println("Bill of entry updated for tripids: "+tripId);
                    }
                  
                    
                }
                    System.out.println("Bill of entry updated for  order "+orderIds[i]);
                }
                
                
               

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
