<%-- 
    Document   : viewBillDetail
    Created on : Dec 10, 2013, 2:28:18 AM
    Author     : srinientitle
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <%@ page import="java.util.*" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
         <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


             <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
               // alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
         function submitPage(){
        document.enter.action='/throttle/saveinvoicebill.do';
        document.enter.submit();
        }

      </script>
        </head>
    <body>
        <form name="enter" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">
                <tr>
                    <td  class="contentsub">Customer Name</td>
                    <td  ><c:out value="${billdetail.custName}"/></td>
                    <td class="contentsub">Route Name</td>
                    <td  ><c:out value="${billdetail.routeName}"/></td>
                </tr>
            </table>
                <br>
                <br>
            <c:if test="${billdetail != null}">
            <table  border="0" class="border" align="center" width="95%" cellpadding="0" cellspacing="0" id="bg">               

        <tr><td  style="text-align: center" class="contentsub" height="30" colspan="2">Invoice Details</td></tr>
        <tr><td class="text1" height="30">&nbsp;&nbsp;Total Km Run</td><td class="text1" height="30"><c:out value="${billdetail.totalTripKmRun}"/></td></tr>
        <tr><td class="text1" height="30">&nbsp;&nbsp;No of Drivers </td><td class="text1" height="30"><c:out value="${billdetail.totaldrivers}"/></td></tr>
        <tr><td class="text1" height="30">&nbsp;&nbsp;Total Expenses </td><td class="text1" height="30"><c:out value="${billdetail.totalExpenseAmt}"/></td></tr>
        <tr><td class="text1" height="30">&nbsp;&nbsp;Nett Amount </td><td class="text1" height="30"><c:out value="${billdetail.totalFreightAmt}"/></td></tr>
            <tr>
            <td>Enter Your Remark</td>
            <td><textarea name="remark" rows='2' cols='20' class='form-control' ></textarea></td>
            </tr>
            </table>
            <%
            ArrayList list=(ArrayList)request.getAttribute("tripIdList");
            if(list !=null&&list.size()>0){
            for(int i=0;i<list.size();i++){
            String tripId=(String)list.get(i);
            if(i==0){%>
            <input type="hidden" name="invoiceHeaderTripId" value="<%=tripId%>"/>
            <%}%>
            <input type="hidden" name="invoiceTripId" value="<%=tripId%>"/>
            <%}}%>
            <input type="hidden" name="grandTotal" value="<c:out value="${billdetail.totalFreightAmt}"/>"/>
            </c:if>
          <br>
        <center>
            <input type="button" class="button" value="Save Bill" name="Save Bill" onClick="submitPage()">
        </center>
     
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
 </html>
