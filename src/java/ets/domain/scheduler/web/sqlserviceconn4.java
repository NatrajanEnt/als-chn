package ets.domain.scheduler.web;

import ets.domain.report.business.ReportTO;
import ets.domain.thread.web.SendEmail;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

class sqlserviceconn4 extends Thread {

    String TallyCompId = "";
    String TripNo = "";
    String transactionType = "";
    String tranNo = "";
    String transactionDate = "";
    String billedPartyname = "";
    String address1 = "";
    String address2 = "";
    String address3 = "";
    String city = "";
    String billingState = "";
    String country = "";
    String gstType = "";
    String gstNo = "";
    String panNo = "";
    String transactionAmount = "";
    String trnMode = "";

    public sqlserviceconn4(ReportTO reportTO) {

        TallyCompId = reportTO.getTallyCmpId();
        TripNo = reportTO.getTripNos();
        transactionType = reportTO.getTransactionType();
        tranNo = reportTO.getTranNo();
        transactionDate = reportTO.getTransactionDate();
        billedPartyname = reportTO.getBilledPartyname();
        address1 = reportTO.getAddress1();
        address2 = reportTO.getAddress2();
        address3 = reportTO.getAddress3();
        city = reportTO.getCity();
        billingState = reportTO.getBillingState();
        country = reportTO.getCountry();
        gstType = reportTO.getGstType();
        gstNo = reportTO.getGstNo();
        panNo = reportTO.getPanNo();
        transactionAmount = reportTO.getTransactionAmount();
        trnMode = reportTO.getTrnMode();

    }

    public void run() {
        int updateStatus = 0;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sqlQuery = "";
        String tripNo = "";
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conn = DriverManager.getConnection("jdbc:sqlserver://123.136.170.227:1433;ABCTEST=company", "sa", "triway");

            if (conn != null) {
                System.out.println("Database Successfully connected");

                sqlQuery = "INSERT INTO [ABCTEST].[dbo].[AccountTransation](tallyCmpId,TripNo,TransactionType,TranNo,TrnDate,PartyName,PartyAddress1,PartyAddress2,PartyAddress3,PartyCity,PartyState,PartyCountry,PartyGSTType,PartyGSTNo,PartyPANNo,TrnValue,PostInd,TrnMode) VALUES "
                        + "(?, ?, ?, ?, convert(datetime,?), ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,0, ?)";

                stmt = conn.prepareStatement(sqlQuery);
                
                System.out.println("TallyCompId="+TallyCompId);
                System.out.println("TripNo="+tranNo);
                System.out.println("transactionType="+transactionType);
                System.out.println("tranNo="+tranNo);
                System.out.println("transactionDate="+transactionDate);
                System.out.println("billedPartyname="+billedPartyname);
                System.out.println("address1="+address1);
                System.out.println("address2="+address2);
                System.out.println("address3="+address3);
                System.out.println("city="+city);
                System.out.println("billingState="+billingState);
                System.out.println("country="+country);
                System.out.println("gstType="+gstType);
                System.out.println("gstNo="+gstNo);
                System.out.println("panNo="+panNo);
                System.out.println("transactionAmount="+transactionAmount);
                System.out.println("trnMode="+trnMode);
                
                stmt.setString(1, TallyCompId);
                stmt.setString(2, TripNo);
                stmt.setString(3, transactionType);
                stmt.setString(4, tranNo);
                stmt.setString(5, transactionDate);
                stmt.setString(6, billedPartyname);
                stmt.setString(7, address1);
                stmt.setString(8, address2);
                stmt.setString(9, address3);
                stmt.setString(10, city);
                stmt.setString(11, billingState);
                stmt.setString(12, country);
                stmt.setString(13, gstType);
                stmt.setString(14, gstNo);
                stmt.setString(15, panNo);
                stmt.setString(16, transactionAmount);
                stmt.setString(17, trnMode);
                System.out.println("stmt---"+stmt.toString());
                updateStatus = stmt.executeUpdate();
                System.out.println("UPDATE = " + updateStatus);

//                while (rs.next()) {
//                    tripNo = rs.getString("TripNo");
//                }
//                System.out.println("tripNo==========" + tripNo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
              if (conn != null) {            
                  try {
                      conn.close();
                  } catch (SQLException ex) {
                      Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                  }
              }
            }
        if (updateStatus != 0) {
//        if (!"".equals(updateStatus) && "0".equals(updateStatus)) {

            String fileNameNew = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/" + fileNameNew);
            try {
                dbProps.load(is);//this may throw IOException
            } catch (IOException ex) {
                Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
            }
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            int status = 0;
            Connection con = null;
            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try {
                    PreparedStatement updateMailDetails = null;
                    String sql = "UPDATE AccountTransation SET \n"
                            + " throttle_postind = 1"
                            + " WHERE  TripNo = ?";
                    updateMailDetails = con.prepareStatement(sql);
                    updateMailDetails.setString(1, TripNo);
                    status = updateMailDetails.executeUpdate();
                    System.out.println("NEW  = " + status);

                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("TripNo doesn't exist.");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }finally{
              if (con != null) {            
                  try {
                      con.close();
                  } catch (SQLException ex) {
                      Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                  }
              }
            }
        }
    }
}
