<%-- 
    Document   : vehicleDetainDetailsExcel
    Created on : May 12, 2016, 4:04:15 PM
    Author     : Gulshan kumar
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                Date dNow = new Date();
                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                //System.out.println("Current Date: " + ft.format(dNow));
                String curDate = ft.format(dNow);
                String expFile = "vehicleVendorWiseTrip-" + curDate + ".xls";

                String fileName = "attachment;filename=" + expFile;
                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <table style="width: 1100px" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="40" >
                        <th><h3 align="center">Transporter</h3></th>
                        <th><h3 align="center">Vehicle No</h3></th>
                        <th><center><h3>No.of.Trips</h3></center></th>
                        <th><center><h3>BD</h3></center></th>
                        <th><center><h3>DTN</h3></center></th>
                        <th><center><h3>IDL</h3></center></th>
                        <th><h3 align="center">OT</h3></th>
                        <th><h3 align="center">Total Days</h3></th>
                    </tr>
                </thead>
                <tbody>
                    <c:set var="OnTrip" value="0"/>
                    <c:set var="TotalTrips" value="0"/>
                    <c:set var="TotalBD" value="0"/>
                    <c:set var="TotalDTN" value="0"/>
                    <c:set var="TotalIDL" value="0"/>
                    <c:set var="TotalOT" value="0"/>

                    <% int index = 1;%>
                    <%
                        String classText = "";
                        int oddEven = index % 2;
                        if (oddEven > 0) {
                            classText = "text2";
                        } else {
                            classText = "text1";
                        }
                    %>
                    <c:set var="vendorName" value=""/>  
                    <c:forEach items="${VehicleDetainDetails}" var="Vehicle">




                        <c:if test = "${vendorName != ''}" >
                            <c:if test = "${vendorName !=Vehicle.vendorName}" >
                                <tr height="30">
                                    <td align="center"><b><c:out value="${vendorName}"/> Total()</b></td>
                                    <td align="center"></td>
                                    <td align="center"><b><c:out value="${TotalTrips}"/></b></td>
                                    <td align="center"><b><c:out value="${TotalBD}"/></b></td>
                                    <td align="center"><b><c:out value="${TotalDTN}"/></b></td>
                                    <td align="center"><b><c:out value="${TotalIDL}"/></b></td>
                                    <td align="center"><b><c:out value="${TotalOT}"/></b></td>
                                    <td align="center"></td>

                                    <c:set var="TotalTrips" value="${0}"/>
                                    <c:set var="TotalBD" value="${0}"/>
                                    <c:set var="TotalDTN" value="${0}"/>
                                    <c:set var="TotalIDL" value="${OnTrip}"/>
                                    <c:set var="TotalOT" value="${0}"/>

                                </tr>
                            </c:if>
                        </c:if>
                        <tr height="30">
                            <c:set var="vendorName" value="${Vehicle.vendorName}"/>
                            <c:set var="OnTrip" value="${Vehicle.totalDays - Vehicle.tripDays}"/>
                            <c:set var="TotalTrips" value="${TotalTrips + Vehicle.noOfTrips}"/>
                            <c:set var="TotalBD" value="${TotalBD + 0}"/>
                            <c:set var="TotalDTN" value="${TotalDTN + 0}"/>
                            <c:set var="TotalIDL" value="${TotalIDL + OnTrip}"/>
                            <c:set var="TotalOT" value="${TotalOT + Vehicle.tripDays}"/>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.vendorName}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.regNo}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.noOfTrips}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${0}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${0}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${OnTrip}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.tripDays}"/></td>
                            <td align="center" class="<%=classText%>"><c:out value="${Vehicle.totalDays}"/></td>
                            

                        </tr>

                    </c:forEach>
                    <tr height="30">
                        <td align="center"><b><c:out value="${vendorName}"/> Total()</b></td>
                        <td align="center"></td>
                        <td align="center"><b><c:out value="${TotalTrips}"/></b></td>
                        <td align="center"><b><c:out value="${TotalBD}"/></b></td>
                        <td align="center"><b><c:out value="${TotalDTN}"/></b></td>
                        <td align="center"><b><c:out value="${TotalIDL}"/></b></td>
                        <td align="center"><b><c:out value="${TotalOT}"/></b></td>
                        <td align="center"></td>
                    </tr>
                </tbody>
            </table>
        </table>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>    
</html>