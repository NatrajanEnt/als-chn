/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.fuelManagement.business;

/**
 *
 * @author vinoth
 */
import ets.domain.fuelManagement.data.FuelManagementDAO;
import ets.domain.purchase.data.PurchaseDAO;
import ets.domain.fuelManagement.business.FuelManagementTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import java.util.ArrayList;
import java.util.Iterator;

public class FuelManagementBP {

    private FuelManagementDAO fuelDAO;
    private PurchaseDAO purchaseDAO;

    public PurchaseDAO getPurchaseDAO() {
        return purchaseDAO;
    }

    public void setPurchaseDAO(PurchaseDAO purchaseDAO) {
        this.purchaseDAO = purchaseDAO;
    }

    public FuelManagementDAO getFuelDAO() {
        return fuelDAO;
    }

    public void setFuelDAO(FuelManagementDAO fuelDAO) {
        this.fuelDAO = fuelDAO;
    }

    public ArrayList processGetCompanyList() throws FPBusinessException, FPRuntimeException {
        ArrayList companyList = new ArrayList();
        companyList = fuelDAO.getCompanyList();
        if (companyList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return companyList;
    }

    public ArrayList processUsingCompanyList() throws FPBusinessException, FPRuntimeException {
        ArrayList companyList = new ArrayList();
        companyList = fuelDAO.getUsingCompanyList();
        if (companyList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return companyList;
    }

    public ArrayList processUsingCompanyList(FuelManagementTO fuelTO) throws FPBusinessException, FPRuntimeException {
        ArrayList companyList = new ArrayList();
        companyList = fuelDAO.getUsingCompanyList(fuelTO);
        if (companyList.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return companyList;
    }

    public ArrayList processVehicleCompList(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = fuelDAO.getVehCompList(fuelTO);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    public int processInsertVehicleCompany(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doInsertVehicleCompany(fuelTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int processAddUsingComp(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doInsertUsingCompany(fuelTO);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int processAlterVehComp(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doUpdateVehComp(fuelTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-02");
        }
        return status;
    }

    public int processAlterUsingComp(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doUpdateUsingComp(fuelTO);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-02");
        }
        return status;
    }

    public int processDeleteVehComp(ArrayList List, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doDeleteVehComp(List, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-02");
        }
        return status;
    }

    public ArrayList processTankDetails(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = fuelDAO.getTankDetails(fuelTO);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    public int processInsertTankDetails(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doInsertTankDetails(fuelTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int processAlterTankDetails(ArrayList List, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doUpdateTankDetails(List, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int processAddFuelFilling(FuelManagementTO fuelTO, int userId, String companyId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doInsertFuelFilling(fuelTO, userId, companyId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int processAddOutFillFuel(ArrayList List, int userId, String companyId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doInsertOutFillFuel(List, userId, companyId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public float processFuelPrice(String companyId) throws FPRuntimeException, FPBusinessException {
        float status = 0.0f;
        String test = fuelDAO.getFuelPrice(companyId);
        status = Float.parseFloat(test);
        //////System.out.println("test" + test);
        if (test.equalsIgnoreCase("0")) {
            throw new FPBusinessException("EM-FM-01");
        }
        return status;
    }

    public String processVehDetails(String regNo, String companyId) {
        String status = "";
        ArrayList List = new ArrayList();
        List = fuelDAO.getVehDetails(regNo);
        FuelManagementTO fuelTO = null;
        Iterator itr = List.iterator();
        while (itr.hasNext()) {
            fuelTO = new FuelManagementTO();
            fuelTO = (FuelManagementTO) itr.next();
            String previous = fuelDAO.getPreviousKm(regNo, companyId);
            String lastFilled = fuelDAO.getPreviousFuel(regNo, companyId);
            //////System.out.println("lastFilled" + lastFilled);
            status = status + "-" + fuelTO.getTypeName() + "-" + fuelTO.getCompanyName() + "-" + fuelTO.getServicePtName() + "-" + previous + "-" + lastFilled;
        }

        return status;
    }

    public String processVehComp(String regNo) {
        String status = "";

        status = fuelDAO.getVehComp(regNo);

        return status;
    }

    public String processChekMilleage(String mfrId, String modelId, String fromYear, String toYear) {
        String status = "";

        status = fuelDAO.chekMilleage(mfrId, modelId, fromYear, toYear);

        return status;
    }

    public ArrayList processFuelFillingList(FuelManagementTO fuelTO, int userId, int startIndex, int endIndex) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList List1 = new ArrayList();
        String previous = "";

        List = fuelDAO.getFuelFillingList(fuelTO, userId, startIndex, endIndex);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {

            Iterator itr = List.iterator();
            while (itr.hasNext()) {
                fuelTO = new FuelManagementTO();
                fuelTO = (FuelManagementTO) itr.next();
                String previousReading = fuelDAO.getPrevious(fuelTO.getVehicleId(), fuelTO.getDate());
                fuelTO.setPrevious(previousReading);
                //////System.out.println("fuelTO.getVehicleId()" + fuelTO.getVehicleId());
                //////System.out.println("previous" + previousReading);
                String reading = fuelDAO.getCurrentCapacity(fuelTO.getVehicleId(), fuelTO.getDate());
                fuelTO.setTankReading(reading);
                String runReading = fuelDAO.getCurrentRunReading(fuelTO.getVehicleId(), fuelTO.getDate());
                fuelTO.setRunReading(runReading);

            }
        }

        return List;
    }

    public ArrayList processFuelPriceList(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = fuelDAO.getFuelPriceList(fuelTO);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    public ArrayList processTankReading(String companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = fuelDAO.getTankReading(companyId);
        if (List.size() == 0) {

            FuelManagementTO fuelTO = null;
            Iterator itr = List.iterator();
            while (itr.hasNext()) {
                fuelTO = (FuelManagementTO) itr.next();

                fuelTO.setCompanyName("");
                fuelTO.setPresent("0");
                fuelTO.setCapacity(fuelDAO.getTankCapacity(companyId));

            }
        }
        return List;
    }

    public String processRunReading(String companyId) throws FPRuntimeException, FPBusinessException {

        String runReading = "";
        runReading = fuelDAO.getRunReading(companyId);

        return runReading;
    }

    public int processInsertPrice(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doInsertPrice(fuelTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int processAddMilleage(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doInsertMilleage(fuelTO);
        if (status == 0) {
            throw new FPBusinessException("EM-GEN-02");
        }
        return status;
    }

    public int processUpdateMilleage(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doUpdateMilleage(fuelTO);
        if (status == 0) {
            throw new FPBusinessException("EM-GEN-02");
        }
        return status;
    }

    public int processUpdateFuelPrice(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doUpdatePrice(fuelTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public int processDeleteFuelPrice(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.doDeletePrice(fuelTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processMilleageList(FuelManagementTO fuelTO, String companyId) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        List = fuelDAO.getMilleageList(fuelTO, companyId);
        System.out.println("List=="+List.size());
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        }
        return List;
    }

    public int updateMileage(String[] vehicleTypeIds, String[] vehicleMileage, String[] reeferMileage, String[] selected, FuelManagementTO fuelTO, String[] roadType, String[] loadType, String[] fromAge, String[] toAge, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.updateMileage(vehicleTypeIds, vehicleMileage, reeferMileage, selected, fuelTO,roadType,loadType, fromAge, toAge,  userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList processMilleageReport(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList milleage = new ArrayList();
        String[] dat = (fuelTO.getFromDate().split("-"));
        String[] dat1 = (fuelTO.getToDate().split("-"));
        String from = dat[2];
        String to = dat1[2];
        List = fuelDAO.getMilleageActual(fuelTO, userId);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {

            Iterator itr = List.iterator();
            FuelManagementTO fueTO = null;
            while (itr.hasNext()) {
                fueTO = new FuelManagementTO();
                fueTO = (FuelManagementTO) itr.next();

                milleage = fuelDAO.getMilleage(fueTO.getVehicleId(), from, to);
                //////System.out.println("fueTO.getVehicleId()" + fueTO.getVehicleId());
                FuelManagementTO fuelMTO = null;
                Iterator itr1 = milleage.iterator();
                while (itr1.hasNext()) {
                    fuelMTO = new FuelManagementTO();
                    fuelMTO = (FuelManagementTO) itr1.next();
                    fueTO.setCompanyName(fuelMTO.getCompanyName());
                    fueTO.setServicePtName(fuelMTO.getServicePtName());
                    fueTO.setMilleage(fuelMTO.getMilleage());
                    //////System.out.println("fuelMTO.getMilleage()" + fuelMTO.getMilleage());

                }
            }
        }
        return List;
    }

    public ArrayList processVehicleMonthlyReport(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList milleage = new ArrayList();
        String[] dat = (fuelTO.getFromDate().split("-"));
        String[] dat1 = (fuelTO.getToDate().split("-"));
        String from = dat[2] + "-" + dat[1] + "-" + dat[0];
        String to = dat1[2] + "-" + dat1[1] + "-" + dat1[0];
        List = fuelDAO.getVehicleMonthlyReport(fuelTO, userId, from, to);
        if (List.size() == 0) {
            throw new FPBusinessException("EM-GEN-01");
        } else {
            Iterator itr = List.iterator();
            FuelManagementTO fueTO = null;
            while (itr.hasNext()) {
                fueTO = new FuelManagementTO();
                fueTO = (FuelManagementTO) itr.next();
                fueTO.setTotalKm(fuelDAO.getTotalKm(fueTO.getVehicleId(), from, to));
                fueTO.setOutFill(fuelDAO.getTotalOutFill(fueTO.getVehicleId(), from, to));

            }
        }

        return List;
    }

    public ArrayList processMonthlyAvgReport(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {

        ArrayList List = new ArrayList();
        ArrayList newList = new ArrayList();
        ArrayList monthlyReport = new ArrayList();
        String[] dat = (fuelTO.getFromDate().split("-"));
        String[] dat1 = (fuelTO.getToDate().split("-"));
        String from = Integer.parseInt(dat[1]) + "-" + (dat[0]);
        String to = dat1[1] + "-" + dat1[0];
        String fromDate = dat[1] + "-" + dat[0] + "-" + "01";
        String toDate = dat1[1] + "-" + dat1[0] + "-" + 31;
        Float avg = 0.0f;
        int i = 0;
        int km = 0;
        int divisor = 0;
        int diff = 0;
        int yearDiff = (Integer.parseInt(dat1[1]) - Integer.parseInt(dat[1]));

        if (yearDiff == 1) {
            diff = (((Integer.parseInt(dat1[0]) + 12) - (Integer.parseInt(dat[0])))) + 1;
        } else if (yearDiff == 0) {
            diff = (Integer.parseInt(dat1[0]) - Integer.parseInt(dat[0])) + 1;
        }
        divisor = diff;
        if (diff == 1) {
            List = fuelDAO.getMonthlyAvgReport(fuelTO, userId, from, to);
            Iterator itr = List.iterator();
            FuelManagementTO fueTO = null;
            while (itr.hasNext()) {
                fueTO = new FuelManagementTO();
                fueTO = (FuelManagementTO) itr.next();
                ArrayList average = new ArrayList();
                ArrayList monthFuel = new ArrayList();
                ArrayList outFuel = new ArrayList();
                fueTO.setFuel(fuelDAO.getFuelFilled(fueTO.getVehicleId(), from, to));
                fueTO.setTotalKm(fuelDAO.getTotalKm(fueTO.getVehicleId(), fromDate, toDate));
                fueTO.setOutFill(fuelDAO.getTotalOutFill(fueTO.getVehicleId(), fromDate, toDate));
                avg = Float.parseFloat(fueTO.getTotalKm()) / (Float.parseFloat(fueTO.getFuel()) + Float.parseFloat(fueTO.getOutFill()));
                average.add(average.size(), avg);
                monthFuel.add(monthFuel.size(), fueTO.getFuel());
                outFuel.add(outFuel.size(), fueTO.getOutFill());
                fueTO.setAvg(average);
                fueTO.setFuelMonth(monthFuel);
                fueTO.setFuelOut(outFuel);
                fueTO.setTotalKm(fueTO.getTotalKm());
                newList.add(fueTO);
            }

        }
        if (diff != 1) {
            List = fuelDAO.getMonthlyAvgReport(fuelTO, userId, fromDate, toDate);
//            while (diff != 0) {
//
//                diff--;
//
//                if (Integer.parseInt(dat[0]) <= 8) {
//                    from = Integer.parseInt(dat[1]) + "-" + "0" + (Integer.parseInt(dat[0]) + 1);
//                    fromDate = Integer.parseInt(dat[1]) + "-" + "0" + (Integer.parseInt(dat[0]) + 1) + "-" + "01";
//                    toDate = Integer.parseInt(dat[1]) + "-" + "0" + (Integer.parseInt(dat[0]) + 1) + "-" + 31;
//
//                } else {
//                    from = Integer.parseInt(dat[1]) + "-" + (Integer.parseInt(dat[0]) + 1);
//                    fromDate = Integer.parseInt(dat[1]) + "-" + (Integer.parseInt(dat[0]) + 1) + "-" + "01";
//                    toDate = Integer.parseInt(dat[1]) + "-" + (Integer.parseInt(dat[0]) + 1) + "-" + 31;
//
//                }
//
//                to = from;

            Iterator itr = List.iterator();
            FuelManagementTO fueTO = null;
            while (itr.hasNext()) {
                fueTO = new FuelManagementTO();
                fueTO = (FuelManagementTO) itr.next();
                i = 0;
                km = 0;
                ArrayList average = new ArrayList();
                ArrayList monthFuel = new ArrayList();
                ArrayList outFuel = new ArrayList();
                if (yearDiff == 1) {
                    diff = (((Integer.parseInt(dat1[0]) + 12) - (Integer.parseInt(dat[0])))) + 1;
                } else if (yearDiff == 0) {
                    diff = (Integer.parseInt(dat1[0]) - Integer.parseInt(dat[0])) + 1;
                }
                divisor = diff;
                while (diff != 0) {
                    diff--;
                    //////System.out.println("difference" + diff);
                    if ((Integer.parseInt(dat[0]) + i) <= 8) {
                        from = Integer.parseInt(dat[1]) + "-" + "0" + ((Integer.parseInt(dat[0])) + i);
                        fromDate = Integer.parseInt(dat[1]) + "-" + "0" + ((Integer.parseInt(dat[0])) + i) + "-" + "01";
                        toDate = Integer.parseInt(dat[1]) + "-" + "0" + ((Integer.parseInt(dat[0])) + i) + "-" + 31;
                        //////System.out.println("from date in bp" + from);
                    } else if ((Integer.parseInt(dat[0]) + i) <= 12 && Integer.parseInt(dat[0]) > 8) {

                        from = Integer.parseInt(dat[1]) + "-" + (Integer.parseInt(dat[0]) + i);
                        fromDate = Integer.parseInt(dat[1]) + "-" + (Integer.parseInt(dat[0]) + i) + "-" + "01";
                        toDate = Integer.parseInt(dat[1]) + "-" + (Integer.parseInt(dat[0]) + i) + "-" + 31;
                        //////System.out.println("from date in bp1" + from);
                    } else if (((Integer.parseInt(dat[0]) + i) > 12)) {

                        from = (Integer.parseInt(dat[1]) + 1) + "-" + "0" + ((Integer.parseInt(dat[0])) + i - 12);
                        fromDate = (Integer.parseInt(dat[1]) + 1) + "-" + "0" + ((Integer.parseInt(dat[0])) + i - 12) + "-" + "01";
                        toDate = (Integer.parseInt(dat[1]) + 1) + "-" + "0" + ((Integer.parseInt(dat[0])) + i - 12) + "-" + 31;
                        //////System.out.println("from  in bp" + from);
                        //////System.out.println("diff in bp" + diff);
                        //////System.out.println("dat[0] in bp" + ((Integer.parseInt(dat[0]))));
                        //////System.out.println("to date in bp" + toDate);
                    }

                    i++;
                    to = from;
                    fueTO.setFuel(fuelDAO.getFuelFilled(fueTO.getVehicleId(), from, to));
                    fueTO.setTotalKm(fuelDAO.getTotalKm(fueTO.getVehicleId(), fromDate, toDate));
                    km = km + Integer.parseInt(fueTO.getTotalKm());
                    fueTO.setOutFill(fuelDAO.getTotalOutFill(fueTO.getVehicleId(), fromDate, toDate));
                    avg = Float.parseFloat(fueTO.getTotalKm()) / (Float.parseFloat(fueTO.getFuel()) + Float.parseFloat(fueTO.getOutFill()));
                    average.add(average.size(), avg);
                    if (String.valueOf(avg).equalsIgnoreCase("NaN")) {
                        divisor--;
                    }
                    monthFuel.add(monthFuel.size(), fueTO.getFuel());
                    outFuel.add(outFuel.size(), fueTO.getOutFill());

                }

                //////System.out.println("average" + average);
                fueTO.setAvg(average);
                fueTO.setFuelMonth(monthFuel);
                fueTO.setFuelOut(outFuel);
                fueTO.setTotalKm(String.valueOf(km / divisor));
                newList.add(fueTO);
            }

        // }
        }
//        Iterator itr2 = monthlyReport.iterator();
//        FuelManagementTO fuelManagementTO = null;
//        while (itr2.hasNext()) {
//            int i=0;
//            int totKm=0;
//            int totKm1=0;
//            Float filled=0.0f;
//            Float filled1=0.0f;
//        fuelManagementTO=new FuelManagementTO();
//        fuelManagementTO=(FuelManagementTO)itr2.next();
//        Iterator itr3 = monthlyReport.iterator();
//        FuelManagementTO fuelManageTO = null;
//        while (itr3.hasNext()) {
//        fuelManageTO=new FuelManagementTO();
//        fuelManageTO=(FuelManagementTO)itr3.next();
//                if(i==0){
//            if(fuelManagementTO.getVehicleId()==fuelManageTO.getVehicleId()){
//                i++;
//
//        filled=Float.parseFloat(fuelManageTO.getFuel())+Float.parseFloat(fuelManageTO.getOutFill());
//        totKm=Integer.parseInt(fuelManageTO.getTotalKm());
//        fuelManagementTO.setAvgKm(String.valueOf((totKm)/(filled)));
//        average.add(average.size(),fuelManagementTO.getAvgKm());
//
//            }
//
//                }
//        }
//       fuelManageTO.setAvg(average);
//        newList.add(fuelManageTO);
//         //////System.out.println("fuelManagementTO.setAvg"+fuelManagementTO.getAvg().size());
//        }
        return newList;
    }

    public String processRegNo(String companyId) throws FPRuntimeException, FPBusinessException {
        String status = "";
        status = fuelDAO.getRegNo(companyId);
        return status;
    }

    public int processTotalRecords(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = Integer.parseInt(fuelDAO.getTotalRecords(fuelTO));
        return status;
    }

    public int processDeleteMilleage(FuelManagementTO fuelTO) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.deleteMilleage(fuelTO);
        return status;
    }

    public ArrayList getFuelPriceNew() throws FPRuntimeException, FPBusinessException {
        ArrayList fuelPriceList = new ArrayList();
        fuelPriceList = fuelDAO.getFuelPriceNew();
        return fuelPriceList;
    }

    public ArrayList getBunkList() throws FPBusinessException, FPRuntimeException {
        ArrayList bunkList = new ArrayList();
        bunkList = fuelDAO.getBunkList();
        //////System.out.println("bunkList.size() in BP =====> " + bunkList.size());
        return bunkList;
    }

    public int insertNewfuelPrice(FuelManagementTO fuelTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = fuelDAO.insertNewfuelPrice(fuelTO, userId);
        if (status == 0) {
            throw new FPBusinessException("EM-CM-03");
        }
        return status;
    }

    public ArrayList getVehicleTypeCostList(FuelManagementTO fuelTO) throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleTypeCostList = new ArrayList();
        vehicleTypeCostList = fuelDAO.getVehicleTypeCostList(fuelTO);
        return vehicleTypeCostList;
    }

    public ArrayList getCountryList() throws FPBusinessException, FPRuntimeException {
        ArrayList countryList = new ArrayList();
        countryList = fuelDAO.getCountryList();
        return countryList;
    }

    public ArrayList getVehicleTypeList() throws FPBusinessException, FPRuntimeException {
        ArrayList vehicleTypeList = new ArrayList();
        vehicleTypeList = fuelDAO.getVehicleTypeList();
        return vehicleTypeList;
    }

    public ArrayList getCheckFuelList(String countryId) throws FPBusinessException, FPRuntimeException {
        ArrayList checkFuelList = new ArrayList();
        checkFuelList = fuelDAO.getCheckFuelList(countryId);
        return checkFuelList;
    }

    public ArrayList getFuelConfigVehicleTypeList(String countryId) throws FPBusinessException, FPRuntimeException {
        ArrayList fuelConfigVehicleTypeList = new ArrayList();
        fuelConfigVehicleTypeList = fuelDAO.getFuelConfigVehicleTypeList(countryId);
        return fuelConfigVehicleTypeList;
    }

    public ArrayList editVehicleTypeList(FuelManagementTO fuelTO) throws FPBusinessException, FPRuntimeException {
        ArrayList editVehicleTypeList = new ArrayList();
        editVehicleTypeList = fuelDAO.editVehicleTypeList(fuelTO);
        return editVehicleTypeList;
    }

    public int saveVehicleTypeOperatingCost(FuelManagementTO fuelTO) throws FPBusinessException, FPRuntimeException {
        int insertStatus = 0;
        insertStatus = fuelDAO.saveVehicleTypeOperatingCost(fuelTO);
        return insertStatus;
    }

    public ArrayList viewMfrMilleageAgeing(FuelManagementTO fuelTO) throws FPBusinessException, FPRuntimeException {
        ArrayList viewMfrMilleageAgeing = new ArrayList();
        viewMfrMilleageAgeing = fuelDAO.viewMfrMilleageAgeing(fuelTO);
        return viewMfrMilleageAgeing;
    }

     public int saveMfrMilleageAgeing(FuelManagementTO fuelTO, int userId) throws FPBusinessException, FPRuntimeException {
        int insertStatus = 0;
        insertStatus = fuelDAO.saveMfrMilleageAgeing(fuelTO,userId);
        return insertStatus;
    }
     
    public ArrayList viewMfrModelList(FuelManagementTO fuelTO) throws FPBusinessException, FPRuntimeException {
        ArrayList viewMfrModelList = new ArrayList();
        viewMfrModelList = fuelDAO.viewMfrModelList(fuelTO);
        return viewMfrModelList;
    }
    
    public ArrayList viewMfrMilleageConfigDetails(FuelManagementTO fuelTO) throws FPBusinessException, FPRuntimeException {
        ArrayList mfrMilleageConfigDetials = new ArrayList();
        mfrMilleageConfigDetials = fuelDAO.viewMfrMilleageConfigDetails(fuelTO);
        return mfrMilleageConfigDetials;
    }
    
    public int updateMfrMilleage(FuelManagementTO fuelTO, int userId) throws FPBusinessException, FPRuntimeException {
        int insertStatus = 0;
        insertStatus = fuelDAO.updateMfrMilleage(fuelTO,userId);
        return insertStatus;
    }
   
    public ArrayList processMilleageDetails(FuelManagementTO fuelTO, String companyId) throws FPRuntimeException, FPBusinessException {

      ArrayList List = new ArrayList();
      List = fuelDAO.getMilleageDetails(fuelTO, companyId);
      System.out.println("List=="+List.size());
      if (List.size() == 0) {
          throw new FPBusinessException("EM-GEN-01");
      }
      return List;
  }
    
}
