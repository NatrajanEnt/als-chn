/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

/**
 *
 * @author ramprasath
 */
import ets.domain.programmingfree.excelexamples.calculateDistance;
import ets.domain.report.business.DprTO;
import ets.domain.report.business.ReportTO;
//import ets.domain.thread.web.EmailSSL;
import ets.domain.thread.web.SendEmail;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import net.sf.json.JSONException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.sql.SQLException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.ArrayList;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Properties;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import java.util.*;
import java.text.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.*;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import org.jsoup.Jsoup;

public class MovementTypeStatus extends Thread {

    int timeSlot = 0;
    int count1 = 0;
    int count2 = 0;
    int count3 = 0;
    int count4 = 0;
    int count5 = 0;
    int count6 = 0;
    int StatusFlag = 0;
    String currentDate = null;
    String currentDay = null;
    int totalSlot = 0;
    int totalDiff = 0;

    public MovementTypeStatus(int flag,String date,String day,int timeCount1,int timeCount2,int timeCount3,int timeCount4,int timeCount5,int timeCount6) throws ParseException, ClassNotFoundException, SQLException, org.json.simple.parser.ParseException {
        currentDate = date;
        currentDay=day;
        StatusFlag=flag;
        count1=timeCount1;
        count2=timeCount2;
        count3=timeCount3;
        count4=timeCount4;
        count5=timeCount5;
        count6=timeCount6;
        totalSlot = count1 + count2 + count3;
        totalDiff = count4 + count5 + count6;
    }

    public void run() {
        try {
//            System.out.println("timeslot=="+timeSlot);
            Connection con = null;
            System.out.println("I am in order status.........");
            String fileName = "jdbc_url.properties";
            InputStream is = getClass().getResourceAsStream("/" + fileName);
            Properties dbProps = new Properties();
            try {
                dbProps.load(is);//this may throw IOException
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("ex.printStackTrace()");
            }
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            try {
                    Class.forName(dbClassName).newInstance();
                    con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                    PreparedStatement updatemovementStatus = null;

                    String sq1 = " insert into ts_movement_status_details "
                            + "    (Date,day,time_slot1,time_slot2,time_slot3,total_slot,time_diff1,time_diff2,time_diff3,total_diff,created_on,status_id) "
                            + "    values (?,?,?,?,?,?,?,?,?,?,curdate(),?)";
                    int updateStatus=0;
                    System.out.println("StatusFlag="+StatusFlag);
                    // empty In
//                    if(StatusFlag==1){
                       updatemovementStatus=con.prepareStatement(sq1);
                       updatemovementStatus.setString(1,currentDate);
                       updatemovementStatus.setString(2,currentDay);
                       updatemovementStatus.setInt(3,count1);
                       updatemovementStatus.setInt(4,count2);
                       updatemovementStatus.setInt(5,count3);
                       updatemovementStatus.setInt(6,totalSlot);
                       updatemovementStatus.setInt(7,count4);
                       updatemovementStatus.setInt(8,count5);
                       updatemovementStatus.setInt(9,count6);
                       updatemovementStatus.setInt(10,totalDiff);
                       updatemovementStatus.setInt(11,StatusFlag);
                       System.out.println("ts_movement_status_details=="+updatemovementStatus.toString());
                       updateStatus = updatemovementStatus.executeUpdate();
                        System.out.println("updateStatus=="+updateStatus);
//                    }
                    // 
                    
            } catch (SQLException e) {
                System.err.println("Caught SQLException: " + e.getMessage());
            } finally {
                con.close();
            }
        } catch (Exception e) {
            //   System.err.println("Caught Exception: " + e.getMessage());
//            ReportTO rTO = new ReportTO();
//            rTO.setMailSubjectTo("Trip Automation Error");
//            rTO.setMailContentTo("vehicle No : "+regNo);
//            rTO.setMailIdTo("arun@entitlesolutions.com");
//            rTO.setMailIdCc("throttleerror@gmail.com");
            // new EmailSSL(rTO).start();
        }
    }
}
