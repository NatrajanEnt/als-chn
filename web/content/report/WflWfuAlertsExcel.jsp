<%@page import="java.sql.SQLException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.util.HSSFColor"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCellStyle"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ets.domain.report.business.ReportTO"%>
<%@page import="ets.domain.util.ThrottleConstants"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <body>        
        <form name="enter" method="post">

            <%
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Calendar c = Calendar.getInstance();
                Date date = new Date();
                c.setTime(date);
                String systemTime = sdf.format(c.getTime()).toString();
                System.out.println("system date = " + systemTime);

                String filename = "CALOG_Vehicle-Status-Report_" + systemTime + ".xls";
                String sheetName = "";
                HSSFWorkbook my_workbook = new HSSFWorkbook();

                ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
                OutputStream outStream = null;

                try {

                    ArrayList wflList = (ArrayList) request.getAttribute("wflList");
                    ArrayList wfuList = (ArrayList) request.getAttribute("wfuList");
                    ArrayList tripInProgressList = (ArrayList) request.getAttribute("tripInProgressList");
                    ArrayList tripInProgressHireList = (ArrayList) request.getAttribute("tripInProgressHireList");
                    ArrayList tripNotStartedList = (ArrayList) request.getAttribute("tripNotStartedList");
                    ArrayList jobCardList = (ArrayList) request.getAttribute("jobCardList");

                    System.out.println("wflList = " + wflList.isEmpty());

                    Iterator itr = wflList.iterator();
                    ReportTO repTO = new ReportTO();
                    
                    Iterator itr1 = wfuList.iterator();
                    ReportTO repTO1 = new ReportTO();

                    Iterator itr2 = tripInProgressList.iterator();
                    ReportTO repTO2 = new ReportTO();
                    
                    Iterator itr3 = tripNotStartedList.iterator();
                    ReportTO repTO3 = new ReportTO();

                    Iterator itr4 = jobCardList.iterator();
                    ReportTO repTO4 = new ReportTO();

                    int cntr = 1;
                    int wfl_North_West_Corridor = 0;
                    int wfl_North_South_Corridor = 0;
                    int wfl_Dedicate = 0;
                    int wfl_General = 0;
                    
                    int wfu_North_West_Corridor = 0;
                    int wfu_North_South_Corridor = 0;
                    int wfu_Dedicate = 0;
                    int wfu_General = 0;

                    int progress_North_West_Corridor = 0;
                    int progress_North_South_Corridor = 0;
                    int progress_Dedicate = 0;
                    int progress_General = 0;

                    int job_North_West_Corridor = 0;
                    int job_North_South_Corridor = 0;
                    int job_Dedicate = 0;
                    int job_General = 0;
                    
                    int ns_North_West_Corridor = 0;
                    int ns_North_South_Corridor = 0;
                    int ns_Dedicate = 0;
                    int ns_General = 0;
                    
                    String mailContent = "";
                    int chennaiTotal = 0;
                    int tutiTotal = 0;
                    int vizagTotal = 0;
                    int kpctTotal = 0;

                    while (itr.hasNext()) {
                        repTO = (ReportTO) itr.next();
                        if ("CA LOG - CHENNAI".equals(repTO.getOperationPoint())) {
                            wfl_North_West_Corridor++;
                            chennaiTotal++;
                        } else if ("CA LOG - TUTICORIN".equals(repTO.getOperationPoint())) {
                            wfl_North_South_Corridor++;
                            tutiTotal++;
                        } else if ("CA LOG - VIZAG".equals(repTO.getOperationPoint())) {
                            vizagTotal++;
                        } else if ("CA LOG - KPCT".equals(repTO.getOperationPoint())) {
                            wfl_General++;
                            kpctTotal++;
                        }
                    }
                    while (itr1.hasNext()) {
                        repTO = (ReportTO) itr1.next();
                        if ("CA LOG - CHENNAI".equals(repTO.getOperationPoint())) {
                            wfu_North_West_Corridor++;
                            chennaiTotal++;
                        } else if ("CA LOG - TUTICORIN".equals(repTO.getOperationPoint())) {
                            wfu_North_South_Corridor++;
                            tutiTotal++;
                        } else if ("CA LOG - VIZAG".equals(repTO.getOperationPoint())) {
                            vizagTotal++;
                        } else if ("CA LOG - KPCT".equals(repTO.getOperationPoint())) {
                            wfu_General++;
                            kpctTotal++;
                        }
                    }

                    System.out.println("ddd : " + repTO.getOperationPoint());

                    while (itr2.hasNext()) {
                        repTO2 = (ReportTO) itr2.next();
                        ////System.out.println("repTO.getOperationPoint() 3 = " + repTO.getOperationPoint());
                        if ("CA LOG - CHENNAI".equals(repTO2.getOperationPoint())) {
                            progress_North_West_Corridor++;
                            chennaiTotal++;
                        } else if ("CA LOG - TUTICORIN".equals(repTO2.getOperationPoint())) {
                            progress_North_South_Corridor++;
                            tutiTotal++;
                        } else if ("CA LOG - VIZAG".equals(repTO2.getOperationPoint())) {
                            progress_Dedicate++;
                            vizagTotal++;
                        } else if ("CA LOG - KPCT".equals(repTO2.getOperationPoint())) {
                            progress_General++;
                            kpctTotal++;
                        }
                    }
                    while (itr3.hasNext()) {
                        repTO2 = (ReportTO) itr3.next();
                        ////System.out.println("repTO.getOperationPoint() 3 = " + repTO.getOperationPoint());
                        if ("CA LOG - CHENNAI".equals(repTO2.getOperationPoint())) {
                            ns_North_West_Corridor++;
                            chennaiTotal++;
                        } else if ("CA LOG - TUTICORIN".equals(repTO2.getOperationPoint())) {
                            ns_North_South_Corridor++;
                            tutiTotal++;
                        } else if ("CA LOG - VIZAG".equals(repTO2.getOperationPoint())) {
                            ns_Dedicate++;
                            vizagTotal++;
                        } else if ("CA LOG - KPCT".equals(repTO2.getOperationPoint())) {
                            ns_General++;
                            kpctTotal++;
                        }
                    }
                    while (itr4.hasNext()) {
                        repTO4 = (ReportTO) itr4.next();
                        ////System.out.println("repTO.getOperationPoint() 5 = " + repTO.getOperationPoint());
                        if ("CA LOG - CHENNAI".equals(repTO4.getOperationPoint())) {
                            job_North_West_Corridor++;
                            chennaiTotal++;
                        } else if ("CA LOG - TUTICORIN".equals(repTO4.getOperationPoint())) {
                            job_North_South_Corridor++;
                            tutiTotal++;
                        } else if ("CA LOG - VIZAG".equals(repTO4.getOperationPoint())) {
                            job_Dedicate++;
                            vizagTotal++;
                        } else if ("CA LOG - KPCT".equals(repTO4.getOperationPoint())) {
                            job_General++;
                            kpctTotal++;
                        }
                    }

                    for (int k = 0; k <= 6; k++) {
                        if (k == 0) {
                            sheetName = "Summary";

                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                            HSSFRow header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("Vehicle Status");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("Chennai");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

//                            HSSFCell cell3 = header.createCell((short) 2);
//                            cell3.setCellValue("KPCT");
//                            cell3.setCellStyle(style);
//                            my_sheet.setColumnWidth((short) 2, (short) 6000);

                            HSSFCell cell3 = header.createCell((short) 2);
                            cell3.setCellValue("Total");
                            cell3.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);

                            cntr = 1;
                            HSSFRow row = my_sheet.createRow(cntr);
                            row = my_sheet.createRow((short) cntr);
                            row.createCell((short) 0).setCellValue("WFL");
                            row.getCell((short) 0).setCellStyle(style1);
                            row.createCell((short) 1).setCellValue(ns_North_West_Corridor);
                            row.getCell((short) 1).setCellStyle(style1);
//                            row.createCell((short) 2).setCellValue(ns_General);
//                            row.getCell((short) 2).setCellStyle(style1);
                            row.createCell((short) 2).setCellValue((ns_General + ns_North_West_Corridor));
                            row.getCell((short) 2).setCellStyle(style1);
                            cntr++;
                            mailContent = "<tr><td>Idle</td><td align='right'>" + ns_North_West_Corridor + "</td><td align='right'>" + ns_General + "</td><td align='right'>" + (ns_General + ns_North_West_Corridor) + "</td></tr>";
                            
                            row = my_sheet.createRow(cntr);
                            row = my_sheet.createRow((short) cntr);
                            row.createCell((short) 0).setCellValue("Idle - No Load");
                            row.getCell((short) 0).setCellStyle(style1);
                            row.createCell((short) 1).setCellValue(wfl_North_West_Corridor);
                            row.getCell((short) 1).setCellStyle(style1);
//                            row.createCell((short) 2).setCellValue(wfl_General);
//                            row.getCell((short) 2).setCellStyle(style1);
                            row.createCell((short) 2).setCellValue((wfl_General + wfl_North_West_Corridor));
                            row.getCell((short) 2).setCellStyle(style1);
                            cntr++;
                            mailContent = "<tr><td>Idle</td><td align='right'>" + wfl_North_West_Corridor + "</td><td align='right'>" + wfl_General + "</td><td align='right'>" + (wfl_General + wfl_North_West_Corridor) + "</td></tr>";

                            
                            row = my_sheet.createRow(cntr);
                            row = my_sheet.createRow((short) cntr);
                            row.createCell((short) 0).setCellValue("Idle - No Driver");
                            row.getCell((short) 0).setCellStyle(style1);
                            row.createCell((short) 1).setCellValue(wfu_North_West_Corridor);
                            row.getCell((short) 1).setCellStyle(style1);
//                            row.createCell((short) 2).setCellValue(wfu_General);
//                            row.getCell((short) 2).setCellStyle(style1);
                            row.createCell((short) 2).setCellValue((wfu_General + wfu_North_West_Corridor));
                            row.getCell((short) 2).setCellStyle(style1);
                            cntr++;
                            mailContent = "<tr><td>Idle</td><td align='right'>" + wfu_North_West_Corridor + "</td><td align='right'>" + wfu_General + "</td><td align='right'>" + (wfu_General + wfu_North_West_Corridor) + "</td></tr>";

                            
                            row = my_sheet.createRow(cntr);
                            row = my_sheet.createRow((short) cntr);
                            row.createCell((short) 0).setCellValue("Trip In Progress");
                            row.getCell((short) 0).setCellStyle(style1);
                            row.createCell((short) 1).setCellValue(progress_North_West_Corridor);
                            row.getCell((short) 1).setCellStyle(style1);
//                            row.createCell((short) 2).setCellValue(progress_General);
//                            row.getCell((short) 2).setCellStyle(style1);
                            row.createCell((short) 2).setCellValue((progress_North_West_Corridor + progress_General));
                            row.getCell((short) 2).setCellStyle(style1);
                            cntr++;
                            mailContent = mailContent + "<tr><td>Trip In Progress</td><td align='right'>" + progress_North_West_Corridor + "</td><td align='right'>" + progress_General + "</td><td align='right'>" + (progress_North_West_Corridor + progress_General) + "</td></tr>";

                            row = my_sheet.createRow(cntr);
                            row = my_sheet.createRow((short) cntr);
                            row.createCell((short) 0).setCellValue("Workshop");
                            row.getCell((short) 0).setCellStyle(style1);
                            row.createCell((short) 1).setCellValue(job_North_West_Corridor);
                            row.getCell((short) 1).setCellStyle(style1);
//                            row.createCell((short) 2).setCellValue(job_General);
//                            row.getCell((short) 2).setCellStyle(style1);
                            row.createCell((short) 2).setCellValue((job_General + job_North_West_Corridor));
                            row.getCell((short) 2).setCellStyle(style1);
                            cntr++;
                            mailContent = mailContent + "<tr><td>Workshop</td><td align='right'>" + job_North_West_Corridor + "</td><td align='right'>" + job_General + "</td><td align='right'>" + (job_North_West_Corridor + job_General) + "</td></tr>";

                            row = my_sheet.createRow(cntr);
                            row = my_sheet.createRow((short) cntr);
                            row.createCell((short) 0).setCellValue("Total");
                            row.getCell((short) 0).setCellStyle(style1);
                            row.createCell((short) 1).setCellValue(chennaiTotal);
                            row.getCell((short) 1).setCellStyle(style1);
//                            row.createCell((short) 2).setCellValue(kpctTotal);
//                            row.getCell((short) 2).setCellStyle(style1);
                            row.createCell((short) 2).setCellValue((chennaiTotal + kpctTotal));
                            row.getCell((short) 2).setCellStyle(style1);
                            mailContent = mailContent + "<tr><td align='right'><b>Total</b></td><td align='right'><b>" + chennaiTotal + "</b></td><td align='right'><b>" + kpctTotal + "</b></td><td align='right'><b>" + (chennaiTotal + kpctTotal) + "</b></td></tr>";

                            cntr++;
                        } else if (k == 1) {
                            sheetName = "WFL";

                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                            //                            style.setBorderTop((short) 1);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                            

                            HSSFRow header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

//                            HSSFCell cell3 = header.createCell((short) 2);
//                            cell3.setCellValue("Branch");
//                            cell3.setCellStyle(style);
//                            my_sheet.setColumnWidth((short) 2, (short) 6000);
                            
                            HSSFCell cell3 = header.createCell((short) 2);
                            cell3.setCellValue("VehicleType");
                            cell3.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);
                            
                            HSSFCell cell4 = header.createCell((short) 3);
                            cell4.setCellValue("Driver Name");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);
                            
                            HSSFCell cell5 = header.createCell((short) 4);
                            cell5.setCellValue("Driver Mobile");
                            cell5.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);
                            
                            HSSFCell cell6 = header.createCell((short) 5);
                            cell6.setCellValue("Period");
                            cell6.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 6000);
                            
                            HSSFCell cell7 = header.createCell((short) 6);
                            cell7.setCellValue("No of Days in Idle");
                            cell7.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 6, (short) 6000);

                            cntr = 1;
                            
                            itr = tripNotStartedList.iterator();
                            while (itr.hasNext()) {
                                repTO = (ReportTO) itr.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getTonnage());
                                row.getCell((short) 2).setCellStyle(style1);
//                                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                                row.getCell((short) 2).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getDriverName());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                                row.getCell((short) 4).setCellStyle(style1);
                                row.createCell((short) 5).setCellValue(repTO.getPeriod());
                                row.getCell((short) 5).setCellStyle(style1);
                                row.createCell((short) 6).setCellValue(repTO.getAge());
                                row.getCell((short) 6).setCellStyle(style1);
                                cntr++;
                            }

                        }else if (k == 2) {
                            sheetName = "Idle - No Load";

                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                            
                            HSSFRow header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

//                            HSSFCell cell3 = header.createCell((short) 2);
//                            cell3.setCellValue("Branch");
//                            cell3.setCellStyle(style);
//                            my_sheet.setColumnWidth((short) 2, (short) 6000);
                            
                            HSSFCell cell31 = header.createCell((short) 2);
                            cell31.setCellValue("VehicleType");
                            cell31.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);
                            
                            HSSFCell cell4 = header.createCell((short) 3);
                            cell4.setCellValue("DriverName");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);
                            
                            HSSFCell cell5 = header.createCell((short) 4);
                            cell5.setCellValue("DriverMobile");
                            cell5.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);
                            
                            HSSFCell cell6 = header.createCell((short) 5);
                            cell6.setCellValue("Period");
                            cell6.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 6000);
                            
                            HSSFCell cell7 = header.createCell((short) 6);
                            cell7.setCellValue("No of Days in Idle");
                            cell7.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 6, (short) 6000);

                            cntr = 1;
                            itr = wflList.iterator();
                            while (itr.hasNext()) {
                                repTO = (ReportTO) itr.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getTonnage());
                                row.getCell((short) 2).setCellStyle(style1);
//                                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                                row.getCell((short) 2).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getDriverName());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                                row.getCell((short) 4).setCellStyle(style1);
                                row.createCell((short) 5).setCellValue(repTO.getPeriod());
                                row.getCell((short) 5).setCellStyle(style1);
                                row.createCell((short) 6).setCellValue(repTO.getAge());
                                row.getCell((short) 6).setCellStyle(style1);
                                cntr++;
                            }

                        } else if (k == 3) {
                            sheetName = "Idle - No Driver";

                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
//                            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                            HSSFRow header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

//                            HSSFCell cell3 = header.createCell((short) 2);
//                            cell3.setCellValue("Branch");
//                            cell3.setCellStyle(style);
//                            my_sheet.setColumnWidth((short) 2, (short) 6000);
                            

                            HSSFCell cell31 = header.createCell((short) 2);
                            cell31.setCellValue("VehicleType");
                            cell31.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);
                            
                            HSSFCell cell4 = header.createCell((short) 3);
                            cell4.setCellValue("Driver Name");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);
                            
                            HSSFCell cell5 = header.createCell((short) 4);
                            cell5.setCellValue("Driver Mobile");
                            cell5.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);
                            
                            HSSFCell cell6 = header.createCell((short) 5);
                            cell6.setCellValue("Period");
                            cell6.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 6000);
                            
                            HSSFCell cell7 = header.createCell((short) 6);
                            cell7.setCellValue("No of Days in Idle");
                            cell7.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 6, (short) 6000);

                            cntr = 1;
                            itr = wfuList.iterator();
                            while (itr.hasNext()) {
                                repTO = (ReportTO) itr.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getTonnage());
                                row.getCell((short) 2).setCellStyle(style1);
//                                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                                row.getCell((short) 2).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getDriverName());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                                row.getCell((short) 4).setCellStyle(style1);
                                row.createCell((short) 5).setCellValue(repTO.getPeriod());
                                row.getCell((short) 5).setCellStyle(style1);
                                row.createCell((short) 6).setCellValue(repTO.getAge());
                                row.getCell((short) 6).setCellStyle(style1);
                                cntr++;
                            }

                        }else if (k == 4) {
                            sheetName = "Trip-InProgress";
                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                             style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                            HSSFRow header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cell3 = header.createCell((short) 2);
                            cell3.setCellValue("TripNo");
                            cell3.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);

//                            HSSFCell cell4 = header.createCell((short) 3);
//                            cell4.setCellValue("Branch");
//                            cell4.setCellStyle(style);
//                            my_sheet.setColumnWidth((short) 3, (short) 6000);

                            HSSFCell cell4 = header.createCell((short) 3);
                            cell4.setCellValue("Driver Name");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);
                            
                            HSSFCell cell5 = header.createCell((short) 4);
                            cell5.setCellValue("Driver Mobile");
                            cell5.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);
                            
                            HSSFCell cell6 = header.createCell((short) 5);
                            cell6.setCellValue("Route");
                            cell6.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 6000);

                            HSSFCell cell7 = header.createCell((short) 6);
                            cell7.setCellValue("CurrentLocation");
                            cell7.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 6, (short) 6000);

                            HSSFCell cell8 = header.createCell((short) 7);
                            cell8.setCellValue("Status");
                            cell8.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 7, (short) 6000);

                            HSSFCell cell9 = header.createCell((short) 8);
                            cell9.setCellValue("CustomerName");
                            cell9.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 8, (short) 6000);

                            HSSFCell cell10 = header.createCell((short) 9);
                            cell10.setCellValue("TripStartedOn");
                            cell10.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 9, (short) 6000);

                            HSSFCell cell11 = header.createCell((short) 10);
                            cell11.setCellValue("No of Days in Trip");
                            cell11.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 10, (short) 6000);
                            
                            HSSFCell cell12 = header.createCell((short) 11);
                            cell12.setCellValue("Duration time hrs");
                            cell12.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 11, (short) 6000);

                            cntr = 1;
                            itr = tripInProgressList.iterator();
                            while (itr.hasNext()) {
                                repTO = (ReportTO) itr.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getTripCode());
                                row.getCell((short) 2).setCellStyle(style1);
//                                row.createCell((short) 3).setCellValue(repTO.getOperationPoint());
//                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getDriverName());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                                row.getCell((short) 4).setCellStyle(style1);
                                row.createCell((short) 5).setCellValue(repTO.getRouteInfo());
                                row.getCell((short) 5).setCellStyle(style1);
                                row.createCell((short) 6).setCellValue(repTO.getLocation());
                                row.getCell((short) 6).setCellStyle(style1);
                                row.createCell((short) 7).setCellValue(repTO.getStatusName());
                                row.getCell((short) 7).setCellStyle(style1);
                                row.createCell((short) 8).setCellValue(repTO.getCustomerName());
                                row.getCell((short) 8).setCellStyle(style1);
                                row.createCell((short) 9).setCellValue(repTO.getTripstarttime());
                                row.getCell((short) 9).setCellStyle(style1);
                                row.createCell((short) 10).setCellValue(repTO.getAge());
                                row.getCell((short) 10).setCellStyle(style1);
                                row.createCell((short) 11).setCellValue(repTO.getDurationHours());
                                row.getCell((short) 11).setCellStyle(style1);
                                cntr++;
                            }
                        }else if (k == 5) {
                            sheetName = "Trip-InProgress Hire";
                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                             style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                            HSSFRow header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cell3 = header.createCell((short) 2);
                            cell3.setCellValue("TripNo");
                            cell3.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);

//                            HSSFCell cell4 = header.createCell((short) 3);
//                            cell4.setCellValue("Branch");
//                            cell4.setCellStyle(style);
//                            my_sheet.setColumnWidth((short) 3, (short) 6000);

                            HSSFCell cell4 = header.createCell((short) 3);
                            cell4.setCellValue("Driver Name");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);
                            
                            HSSFCell cell5 = header.createCell((short) 4);
                            cell5.setCellValue("Driver Mobile");
                            cell5.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);
                            
                            HSSFCell cell6 = header.createCell((short) 5);
                            cell6.setCellValue("Route");
                            cell6.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 6000);

                            HSSFCell cell7 = header.createCell((short) 6);
                            cell7.setCellValue("Transporter");
                            cell7.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 6, (short) 6000);

                            HSSFCell cell8 = header.createCell((short) 7);
                            cell8.setCellValue("Status");
                            cell8.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 7, (short) 6000);

                            HSSFCell cell9 = header.createCell((short) 8);
                            cell9.setCellValue("CustomerName");
                            cell9.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 8, (short) 6000);

                            HSSFCell cell10 = header.createCell((short) 9);
                            cell10.setCellValue("TripStartedOn");
                            cell10.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 9, (short) 6000);

                            HSSFCell cell11 = header.createCell((short) 10);
                            cell11.setCellValue("No of Days in Trip");
                            cell11.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 10, (short) 6000);

                            HSSFCell cell12 = header.createCell((short) 11);
                            cell12.setCellValue("Duration time hrs");
                            cell12.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 11, (short) 6000);
                            
                            cntr = 1;
                            itr = tripInProgressHireList.iterator();
                            while (itr.hasNext()) {
                                repTO = (ReportTO) itr.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getTripCode());
                                row.getCell((short) 2).setCellStyle(style1);
//                                row.createCell((short) 3).setCellValue(repTO.getOperationPoint());
//                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getDriverName());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getDriverMobile());
                                row.getCell((short) 4).setCellStyle(style1);
                                row.createCell((short) 5).setCellValue(repTO.getRouteInfo());
                                row.getCell((short) 5).setCellStyle(style1);
                                row.createCell((short) 6).setCellValue(repTO.getLocation());
                                row.getCell((short) 6).setCellStyle(style1);
                                row.createCell((short) 7).setCellValue(repTO.getStatusName());
                                row.getCell((short) 7).setCellStyle(style1);
                                row.createCell((short) 8).setCellValue(repTO.getCustomerName());
                                row.getCell((short) 8).setCellStyle(style1);
                                row.createCell((short) 9).setCellValue(repTO.getTripstarttime());
                                row.getCell((short) 9).setCellStyle(style1);
                                row.createCell((short) 10).setCellValue(repTO.getAge());
                                row.getCell((short) 10).setCellStyle(style1);
                                row.createCell((short) 11).setCellValue(repTO.getDurationHours());
                                row.getCell((short) 11).setCellStyle(style1);
                                cntr++;
                            }
                        } else if (k == 6) {
                            sheetName = "Workshop";
                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
//                            style.setBorderTop((short) 1);
//                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                            HSSFRow header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

//                            HSSFCell cell3 = header.createCell((short) 2);
//                            cell3.setCellValue("Branch");
//                            cell3.setCellStyle(style);
//                            my_sheet.setColumnWidth((short) 2, (short) 6000);

                            HSSFCell cell4 = header.createCell((short) 2);
                            cell4.setCellValue("Status");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);

                            HSSFCell cell5 = header.createCell((short) 3);
                            cell5.setCellValue("ServiceType");
                            cell5.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);
                            
                            HSSFCell cell6 = header.createCell((short) 4);
                            cell6.setCellValue("Period");
                            cell6.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);

                            HSSFCell cell7 = header.createCell((short) 5);
                            cell7.setCellValue("No of Days in Workshop");
                            cell7.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 8000);
                            
                            HSSFCell cell8 = header.createCell((short) 6);
                            cell8.setCellValue("Expected Date of Delivery");
                            cell8.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 6, (short) 8000);
                            
                            HSSFCell cell9 = header.createCell((short) 7);
                            cell9.setCellValue("Remarks");
                            cell9.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 7, (short) 8000);

                            cntr = 1;
                            itr = jobCardList.iterator();
                            while (itr.hasNext()) {
                                repTO = (ReportTO) itr.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
//                                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
//                                row.getCell((short) 2).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getStatusName());
                                row.getCell((short) 2).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getServiceTypeName());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getPeriod());
                                row.getCell((short) 4).setCellStyle(style1);
                                row.createCell((short) 5).setCellValue(repTO.getAge());
                                row.getCell((short) 5).setCellStyle(style1);
                                row.createCell((short) 6).setCellValue(repTO.getDeliveryDate());
                                row.getCell((short) 6).setCellStyle(style1);
                                row.createCell((short) 7).setCellValue(repTO.getRemarks());
                                row.getCell((short) 7).setCellStyle(style1);
                                cntr++;
                            }
                        }
                    }

                    System.out.println("excel Sheet created");


                my_workbook.write(outByteStream);
                byte[] outArray = outByteStream.toByteArray();
                response.setContentType("application/ms-excel");
                response.setContentLength(outArray.length);
                response.setHeader("Expires:", "0"); // eliminates browser caching
                response.setHeader("Content-Disposition", "attachment; filename=" + filename + "");
                outStream = response.getOutputStream();
                outStream.write(outArray);

                System.out.println("Excel written successfully..");
                
                }catch (Exception e) {
                    e.printStackTrace();
                }

                out.clear();
                out = pageContext.pushBody();

            %>

       

        </form>
    </body>
</html>
