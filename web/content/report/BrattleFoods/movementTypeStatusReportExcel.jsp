<%-- 
    Document   : driverSettlementReportExcel
    Created on : M 13, 2019, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "orderStatusReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
                        
            <c:if test="${movementStatusReportDetails != null}">
                        <table align="center" id="table" class="table table-info mb30 table-hover"style="width:100%;" >
                            <c:if test="${statusId==1}">
                            <thead height="30">
                                <tr>
                                    <th align="center" rowspan="2">Sno</th>
                                    <th align="center" rowspan="2">Date</th>
                                    <th align="center" rowspan="2">Day</th>
                                    <th align="center" colspan="3">DEPOT Gate in Before ….</th>
                                    <th align="center" rowspan="2">TOTAL NO OF EXPORT MOVEMENTS using CA Log vehicles</th>
                                    <th align="center" colspan="3">DWELL TIME @ Depot</th>
                                </tr> <tr>
                                    <th align="center">Before 3 PM</th>
                                    <th align="center">3 PM to 5:30 PM</th>
                                    <th align="center">after 5:30 PM</th>
                                    <th align="center">Less than 1 hr</th>
                                    <th align="center">1 to 2 hrs</th>
                                    <th align="center">abv 2 hrs</th>
                                </tr>
                            </thead>
                            </c:if>
                            <c:if test="${statusId==2}">
                            <thead height="30">
                                <tr>
                                    <th align="center" rowspan="2">Sno</th>
                                    <th align="center" rowspan="2">Date</th>
                                    <th align="center" rowspan="2">Day</th>
                                    <th align="center" colspan="3">DEPOT Gate in Before ….<br>Port Gate(DPD) in Before…</th>
                                    <th align="center" rowspan="2">TOTAL NO OF EXPORT MOVEMENTS using CA Log vehicles</th>
                                    <th align="center" colspan="3">DWELL TIME @ CFS<br>DWELL TIME @ Terminal(DPD)</th>
                                </tr> <tr>
                                    <th align="center">Before 3 PM</th>
                                    <th align="center">3 PM to 5:30 PM</th>
                                    <th align="center">after 5:30 PM</th>
                                    <th align="center">Less than 1 hr</th>
                                    <th align="center">1 to 2 hrs</th>
                                    <th align="center">abv 2 hrs</th>
                                </tr>
                            </thead>
                            </c:if>
                            <c:if test="${statusId==3}">
                            <thead height="30">
                                <tr>
                                    <th align="center" rowspan="2">Sno</th>
                                    <th align="center" rowspan="2">Date</th>
                                    <th align="center" rowspan="2">Day</th>
                                    <th align="center" colspan="3">FACTORY Gate in Before ….</th>
                                    <th align="center" rowspan="2">TOTAL NO OF EXPORT / IMPORT MOVEMENTS using CA Log Vehicles</th>
                                    <th align="center" colspan="3">DWELL TIME @ Factory</th>
                                </tr> <tr>
                                    <th align="center">Before 9 AM</th>
                                    <th align="center">9 AM to 10:30 AM</th>
                                    <th align="center">after 10:30 AM</th>
                                    <th align="center">Less than 3 hr</th>
                                    <th align="center">3 to 4 hrs</th>
                                    <th align="center">abv 4 hrs</th>
                                </tr>
                            </thead>
                            </c:if>
                            <tbody>
                                <%int i=0;%>
                                <c:forEach items="${movementStatusReportDetails}" var="ts">
                                    <%i++;%>
                                    <tr>
                                        <td><%=i%></td>
                                        <td><c:out value="${ts.date}"/></td>
                                        <td><c:out value="${ts.day}"/></td>
                                        <td><c:out value="${ts.timeSlot1}"/></td>
                                        <td><c:out value="${ts.timeSlot2}"/></td>
                                        <td><c:out value="${ts.timeSlot3}"/></td>
                                        <td><c:out value="${ts.totalCount}"/></td>
                                        <td><c:out value="${ts.timeSlot4}"/></td>
                                        <td><c:out value="${ts.timeSlot5}"/></td>
                                        <td><c:out value="${ts.timeSlot6}"/></td>
                                    </tr>

                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
</form>
    </body>
</html>