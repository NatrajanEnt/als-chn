<%@page import="java.sql.SQLException"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.OutputStream"%>
<%@page import="java.io.ByteArrayOutputStream"%>
<%@page import="java.io.FileOutputStream"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCell"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFRow"%>
<%@page import="org.apache.poi.hssf.util.HSSFColor"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFCellStyle"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFSheet"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="ets.domain.report.business.ReportTO"%>
<%@page import="ets.domain.util.ThrottleConstants"%>
<%@page import="java.io.File"%>
<%@page import="org.apache.poi.hssf.usermodel.HSSFWorkbook"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <body>        
        <form name="enter" method="post">

            <%
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Calendar c = Calendar.getInstance();
                Date date = new Date();
                c.setTime(date);
                String systemTime = sdf.format(c.getTime()).toString();
                System.out.println("system date = " + systemTime);

                String filename = "CALOG_Booking-Status-Report_" + systemTime + ".xls";
                String sheetName = "";
                HSSFWorkbook my_workbook = new HSSFWorkbook();

                ByteArrayOutputStream outByteStream = new ByteArrayOutputStream();
                OutputStream outStream = null;

                try {

                    ArrayList bookingList = (ArrayList) request.getAttribute("bookingList");
                    ArrayList bookingReportList = (ArrayList) request.getAttribute("bookingReportList");
                    ArrayList movementAllocationList = (ArrayList) request.getAttribute("movementAllocationList");
                    ArrayList ownBookingCountList = (ArrayList) request.getAttribute("ownBookingCountList");
                    ArrayList ownVehicleAvail = (ArrayList) request.getAttribute("ownVehicleAvail");
                    ArrayList hireBookingCountList = (ArrayList) request.getAttribute("hireBookingCountList");

                    ArrayList tripStartContainerDetails = (ArrayList) request.getAttribute("tripStartContainerDetails");
                    ArrayList tripInProgressDetails = (ArrayList) request.getAttribute("tripInProgressDetails");
                    ArrayList endedTrips = (ArrayList) request.getAttribute("endedTrips");
                    ArrayList clsureTrips = (ArrayList) request.getAttribute("clsureTrips");
                    ArrayList moffusilOwn = (ArrayList) request.getAttribute("moffusilOwn");
                    ArrayList localOwn = (ArrayList) request.getAttribute("localOwn");
                    ArrayList cancelledTrips = (ArrayList) request.getAttribute("cancelledTrips");
                    ArrayList workshopList = (ArrayList) request.getAttribute("workshopList");
                    ArrayList wfuList = (ArrayList) request.getAttribute("wfuList");
                    ArrayList tripNotStartedList = (ArrayList) request.getAttribute("tripNotStartedList");

                    Iterator itr1 = bookingList.iterator();
                    Iterator itr2 = bookingReportList.iterator();
                    Iterator itr3 = movementAllocationList.iterator();
                    Iterator itr4 = ownBookingCountList.iterator();
                    Iterator itr5 = ownVehicleAvail.iterator();
                    Iterator itr6 = hireBookingCountList.iterator();

                    Iterator itr7 = tripStartContainerDetails.iterator();
                    Iterator itr8 = tripInProgressDetails.iterator();
                    Iterator itr9 = endedTrips.iterator();
                    Iterator itr10 = clsureTrips.iterator();
                    Iterator itr11 = moffusilOwn.iterator();
                    Iterator itr12 = localOwn.iterator();
                    Iterator itr13 = cancelledTrips.iterator();
                    Iterator itr14 = workshopList.iterator();
                    Iterator itr15 = wfuList.iterator();
                    Iterator itr16 = tripNotStartedList.iterator();

                    int cntr = 1;

                    for (int k = 0; k < 11; k++) {
                        if (k == 0) {
                            sheetName = "Daily Booking Report";

                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);

                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderTop((short) 1);

                            HSSFRow header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("Sno");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            style.setWrapText(true);
                            cell2.setCellValue("Customer Name");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 4000);

                            HSSFCell cell3 = header.createCell((short) 2);
                            cell3.setCellValue("Route Interim");
                            cell3.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cell4 = header.createCell((short) 3);
                            cell4.setCellValue("Qty");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 4000);

                            HSSFCell cell5 = header.createCell((short) 4);
                            cell5.setCellValue("");
                            cell5.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cell6 = header.createCell((short) 5);
                            cell6.setCellValue("Own");
                            cell6.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 4000);

                            HSSFCell cell7 = header.createCell((short) 6);
                            cell7.setCellValue("");
                            cell7.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cell8 = header.createCell((short) 7);
                            cell8.setCellValue("Hire");
                            cell8.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cell9 = header.createCell((short) 8);
                            cell9.setCellValue("");
                            cell9.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 8, (short) 4000);

                            header = my_sheet.createRow(1);
                            header.setHeightInPoints(20);

                            HSSFCell cell11 = header.createCell((short) 0);
                            cell11.setCellValue("");
                            cell11.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell12 = header.createCell((short) 1);
                            cell12.setCellValue("");
                            cell12.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 4000);

                            HSSFCell cell13 = header.createCell((short) 2);
                            cell13.setCellValue("");
                            cell13.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cell14 = header.createCell((short) 3);
                            cell14.setCellValue("20Ft");
                            cell14.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 4000);

                            HSSFCell cell15 = header.createCell((short) 4);
                            cell15.setCellValue("40Ft");
                            cell15.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cell17 = header.createCell((short) 5);
                            cell17.setCellValue("20Ft");
                            cell17.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 4000);

                            HSSFCell cell18 = header.createCell((short) 6);
                            cell18.setCellValue("40Ft");
                            cell18.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cell117 = header.createCell((short) 7);
                            cell117.setCellValue("20Ft");
                            cell117.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cell118 = header.createCell((short) 8);
                            cell118.setCellValue("40Ft");
                            cell118.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 8, (short) 4000);

                            ReportTO reportTO = new ReportTO();
                            cntr = 2;
                            int rr = 1;
                            String countType = "";
                            boolean check = false;

                            while (itr1.hasNext()) {
                                reportTO = (ReportTO) itr1.next();
                                check = true;
                                System.out.println("second table---" + cntr);
                                HSSFRow rows = my_sheet.createRow(cntr);
                                rows = my_sheet.createRow((short) cntr);
                                rows.createCell((short) 0).setCellValue(rr);
                                rows.createCell((short) 1).setCellValue(reportTO.getCustomerName());
                                rows.createCell((short) 2).setCellValue(reportTO.getRouteInfo());
                                rows.createCell((short) 3).setCellValue(reportTO.getTotal20Count());
                                rows.createCell((short) 4).setCellValue(reportTO.getTotal40Count());
                                rows.createCell((short) 5).setCellValue(reportTO.getOwn20Count());
                                rows.createCell((short) 6).setCellValue(reportTO.getOwn40Count());
                                rows.createCell((short) 7).setCellValue(reportTO.getHire20Count());
                                rows.createCell((short) 8).setCellValue(reportTO.getHire40Count());

                                cntr++;
                                rr++;
                            }

                            cntr = cntr + 2;
                            int totalOwn = 0;
                            int totalHire = 0;
                            ReportTO reportTOs = null;

                            System.out.println("Bookings Status count before  -  " + cntr);

                            style.setBorderTop((short) 1);
                            header = my_sheet.createRow(cntr);
                            header.setHeightInPoints(20);

                            HSSFCell cells101 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cells101.setCellValue("Bookings Status");
                            cells101.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 6000);

                            HSSFCell cells102 = header.createCell((short) 1);
                            cells102.setCellValue("20 ft");
                            cells102.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 4000);

                            HSSFCell cells103 = header.createCell((short) 2);
                            cells103.setCellValue("40 ft");
                            cells103.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cells104 = header.createCell((short) 3);
                            cells104.setCellValue("Total");
                            cells104.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 4000);

                            while (itr2.hasNext()) {
                                reportTO = (ReportTO) itr2.next();
                                check = true;
                                ++cntr;
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);

                                if ("1".equals(reportTO.getCountType())) {
                                    countType = "Bookings Received";
                                } else if ("2".equals(reportTO.getCountType())) {
                                    countType = "Started Trips ";
                                } else if ("3".equals(reportTO.getCountType())) {
                                    countType = "Ended Trips ";
                                } else if ("4".equals(reportTO.getCountType())) {
                                    countType = "Closure Trips ";
                                } else if ("5".equals(reportTO.getCountType())) {
                                    countType = "Pending Bookings";
                                } else if ("6".equals(reportTO.getCountType())) {
                                    countType = "Cancelled Bookings";
                                }

                                row.createCell((short) 0).setCellValue(countType);
                                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
                                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
                                row.createCell((short) 3).setCellValue(reportTO.getTotCount());
                            }

                            cntr = cntr + 2;

                            style.setBorderTop((short) 1);
                            header = my_sheet.createRow(cntr);
                            header.setHeightInPoints(20);

                            HSSFCell cells11 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cells11.setCellValue("Bookings Assigned");
                            cells11.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 6000);

                            HSSFCell cells12 = header.createCell((short) 1);
                            cells12.setCellValue("Own");
                            cells12.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 4000);

                            HSSFCell cells13 = header.createCell((short) 2);
                            cells13.setCellValue("Hire");
                            cells13.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cells4 = header.createCell((short) 3);
                            cells4.setCellValue("Total");
                            cells4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 4000);

                            int totMoff = 0;
                            int totLoff = 0;
                            int totCan = 0;

                            while (itr3.hasNext()) {
                                reportTOs = (ReportTO) itr3.next();
                                ++cntr;
                                System.out.println("second table---" + cntr);
                                HSSFRow rows = my_sheet.createRow(cntr);
                                rows = my_sheet.createRow((short) cntr);

                                if ("1".equals(reportTOs.getMovementType())) {
                                    countType = "Moffusil";
                                    totMoff += reportTOs.getOwnCount() + reportTOs.getHireCount();
                                } else if ("2".equals(reportTOs.getMovementType())) {
                                    countType = "Local";
                                    totLoff += reportTOs.getOwnCount() + reportTOs.getHireCount();
                                } else if ("3".equals(reportTOs.getMovementType())) {
                                    countType = "Cancelled Trips";
                                    totCan += reportTOs.getOwnCount() + reportTOs.getHireCount();
                                }

                                int total = totalOwn + totalHire;
                                rows.createCell((short) 0).setCellValue(countType);
                                rows.createCell((short) 1).setCellValue(reportTOs.getOwnCount());
                                rows.createCell((short) 2).setCellValue(reportTOs.getHireCount());

                                if ("1".equals(reportTOs.getMovementType())) {
                                    rows.createCell((short) 3).setCellValue(totMoff);
                                } else if ("2".equals(reportTOs.getMovementType())) {
                                    rows.createCell((short) 3).setCellValue(totLoff);
                                } else if ("3".equals(reportTOs.getMovementType())) {
                                    rows.createCell((short) 3).setCellValue(totCan);
                                }
                            }

                            cntr = cntr + 2;

                            style.setBorderTop((short) 1);
                            header = my_sheet.createRow(cntr);
                            header.setHeightInPoints(20);

                            cells101 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cells101.setCellValue("Own Vehicle Status");
                            cells101.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 6000);

                            cells102 = header.createCell((short) 1);
                            cells102.setCellValue("20 ft");
                            cells102.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 4000);

                            cells103 = header.createCell((short) 2);
                            cells103.setCellValue("40 ft");
                            cells103.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 4000);

                            cells104 = header.createCell((short) 3);
                            cells104.setCellValue("Total");
                            cells104.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 4000);

                            totalOwn = 0;
                            totalHire = 0;

                            ++cntr;

                            reportTO = new ReportTO();
                            countType = "";
                            check = false;
                            while (itr4.hasNext()) {
                                reportTO = (ReportTO) itr4.next();
                                check = true;
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);

                                if ("1".equals(reportTO.getCountType())) {
                                    countType = "Started Trips ";
                                } else if ("2".equals(reportTO.getCountType())) {
                                    countType = "Trips in Progress ";
                                } else if ("3".equals(reportTO.getCountType())) {
                                    countType = "Ended Trips ";
                                } else if ("4".equals(reportTO.getCountType())) {
                                    countType = "Closure Trips";
                                } else if ("5".equals(reportTO.getCountType())) {
                                    countType = "WorkShop";
                                } else if ("6".equals(reportTO.getCountType())) {
                                    countType = "Idle - No Driver";
                                }

                                row.createCell((short) 0).setCellValue(countType);
                                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
                                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
                                row.createCell((short) 3).setCellValue(reportTO.getTotCount());

                                cntr++;
                            }

                            cntr = cntr + 2;

                            style.setBorderTop((short) 1);

                            header = my_sheet.createRow(cntr);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cells21 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cells21.setCellValue("Own Vehicle Availability");
                            cells21.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 6000);

                            HSSFCell cells22 = header.createCell((short) 1);
                            cells22.setCellValue("20 ft");
                            cells22.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 4000);

                            HSSFCell cells23 = header.createCell((short) 2);
                            cells23.setCellValue("40 ft");
                            cells23.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cell24 = header.createCell((short) 3);
                            cell24.setCellValue("Total");
                            cell24.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 4000);

                            totalOwn = 0;
                            totalHire = 0;

                            reportTO = new ReportTO();
                            countType = "";
                            check = false;
                            while (itr5.hasNext()) {
                                reportTO = (ReportTO) itr5.next();
                                check = true;
                                ++cntr;
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);

                                if ("1".equals(reportTO.getCountType())) {
                                    countType = "WFL ";
                                } else if ("2".equals(reportTO.getCountType())) {
                                    countType = "Vehicles can plan for next trip ";
                                }

                                row.createCell((short) 0).setCellValue(countType);
                                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
                                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
                                row.createCell((short) 3).setCellValue(reportTO.getTotCount());

                            }

                            cntr = cntr + 2;

                            style.setBorderTop((short) 1);

                            header = my_sheet.createRow(cntr);
                            header.setHeightInPoints(20);

                            HSSFCell cells31 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cells31.setCellValue("Hire Vehicle Status");
                            cells31.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 6000);

                            HSSFCell cells32 = header.createCell((short) 1);
                            cells32.setCellValue("20 ft");
                            cells32.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 4000);

                            HSSFCell cells33 = header.createCell((short) 2);
                            cells33.setCellValue("40 ft");
                            cells33.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cell34 = header.createCell((short) 3);
                            cell34.setCellValue("Total");
                            cell34.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 4000);

                            totalOwn = 0;
                            totalHire = 0;

                            reportTO = new ReportTO();
                            countType = "";
                            check = false;
                            while (itr6.hasNext()) {
                                reportTO = (ReportTO) itr6.next();
                                check = true;
                                ++cntr;
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);

                                if ("1".equals(reportTO.getCountType())) {
                                    countType = "Started Trips ";
                                } else if ("2".equals(reportTO.getCountType())) {
                                    countType = "Trips in Progress ";
                                } else if ("3".equals(reportTO.getCountType())) {
                                    countType = "Ended Trips ";
                                } else if ("4".equals(reportTO.getCountType())) {
                                    countType = "Closure Trips";
                                }

                                row.createCell((short) 0).setCellValue(countType);
                                row.createCell((short) 1).setCellValue(reportTO.getTwentyFtCount());
                                row.createCell((short) 2).setCellValue(reportTO.getFourtyFtCount());
                                row.createCell((short) 3).setCellValue(reportTO.getTotCount());
                            }

                            cntr++;
                        } else if (k == 1) {
                            sheetName = "Started Trips";
                            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);

                            HSSFRow header1 = my_sheet1.createRow(0);
                            header1.setHeightInPoints(20); // row hight

                            HSSFCell cellOne1 = header1.createCell((short) 0);
                            style1.setWrapText(true);
                            cellOne1.setCellValue("SNo");
                            cellOne1.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 0, (short) 2000);

                            HSSFCell cellOne2 = header1.createCell((short) 1);
                            style1.setWrapText(true);
                            cellOne2.setCellValue("CustRefNo");
                            cellOne2.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cellOne3 = header1.createCell((short) 2);
                            cellOne3.setCellValue("Customer");
                            cellOne3.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cellOne4 = header1.createCell((short) 3);
                            cellOne4.setCellValue("Goods");
                            cellOne4.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 3, (short) 5000);

                            HSSFCell cellOne5 = header1.createCell((short) 4);
                            cellOne5.setCellValue("Vehicle No");
                            cellOne5.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cellOne6 = header1.createCell((short) 5);
                            cellOne6.setCellValue("TripCode");
                            cellOne6.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 5, (short) 7000);

                            HSSFCell cellOne7 = header1.createCell((short) 6);
                            cellOne7.setCellValue("Container No");
                            cellOne7.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cellOne8 = header1.createCell((short) 7);
                            cellOne8.setCellValue("LR Nos");
                            cellOne8.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cellOne9 = header1.createCell((short) 8);
                            cellOne9.setCellValue("Pkg/Wgt/Cbm");
                            cellOne9.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 8, (short) 4000);

                            HSSFCell cellOne10 = header1.createCell((short) 9);
                            cellOne10.setCellValue("Trip Date");
                            cellOne10.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 9, (short) 4000);

                            HSSFCell cellOne11 = header1.createCell((short) 10);
                            cellOne11.setCellValue("Order Type");
                            cellOne11.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 10, (short) 4000);

                            HSSFCell cellOne12 = header1.createCell((short) 11);
                            cellOne12.setCellValue("Transporter");
                            cellOne12.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 11, (short) 4000);

                            HSSFCell cellOne13 = header1.createCell((short) 12);
                            cellOne13.setCellValue("Route");
                            cellOne13.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 12, (short) 4000);

                            HSSFCell cellOne14 = header1.createCell((short) 13);
                            cellOne14.setCellValue("Driver");
                            cellOne14.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 13, (short) 4000);

                            HSSFCell cellOne15 = header1.createCell((short) 14);
                            cellOne15.setCellValue("Vehicle Type");
                            cellOne15.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 14, (short) 4000);

                            HSSFCell cellOne16 = header1.createCell((short) 15);
                            cellOne16.setCellValue("Status");
                            cellOne16.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 15, (short) 4000);

                            ReportTO reportToOne = null;
                            int cntrOne = 1;

                            while (itr7.hasNext()) {
                                reportToOne = (ReportTO) itr7.next();
                                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                                row1 = my_sheet1.createRow((short) cntrOne);

                                row1.createCell((short) 0).setCellValue(cntrOne);
                                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                                cntrOne++;
                            }

                        } else if (k == 2) {
                            sheetName = "Trip in Progress";
                            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);

                            HSSFRow header1 = my_sheet1.createRow(0);
                            header1.setHeightInPoints(20); // row hight

                            HSSFCell cellOne1 = header1.createCell((short) 0);
                            style1.setWrapText(true);
                            cellOne1.setCellValue("SNo");
                            cellOne1.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 0, (short) 2000);

                            HSSFCell cellOne2 = header1.createCell((short) 1);
                            style1.setWrapText(true);
                            cellOne2.setCellValue("CustRefNo");
                            cellOne2.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cellOne3 = header1.createCell((short) 2);
                            cellOne3.setCellValue("Customer");
                            cellOne3.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cellOne4 = header1.createCell((short) 3);
                            cellOne4.setCellValue("Goods");
                            cellOne4.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 3, (short) 5000);

                            HSSFCell cellOne5 = header1.createCell((short) 4);
                            cellOne5.setCellValue("Vehicle No");
                            cellOne5.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cellOne6 = header1.createCell((short) 5);
                            cellOne6.setCellValue("TripCode");
                            cellOne6.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 5, (short) 7000);

                            HSSFCell cellOne7 = header1.createCell((short) 6);
                            cellOne7.setCellValue("Container No");
                            cellOne7.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cellOne8 = header1.createCell((short) 7);
                            cellOne8.setCellValue("LR Nos");
                            cellOne8.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cellOne9 = header1.createCell((short) 8);
                            cellOne9.setCellValue("Pkg/Wgt/Cbm");
                            cellOne9.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 8, (short) 4000);

                            HSSFCell cellOne10 = header1.createCell((short) 9);
                            cellOne10.setCellValue("Trip Date");
                            cellOne10.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 9, (short) 4000);

                            HSSFCell cellOne11 = header1.createCell((short) 10);
                            cellOne11.setCellValue("Order Type");
                            cellOne11.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 10, (short) 4000);

                            HSSFCell cellOne12 = header1.createCell((short) 11);
                            cellOne12.setCellValue("Transporter");
                            cellOne12.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 11, (short) 4000);

                            HSSFCell cellOne13 = header1.createCell((short) 12);
                            cellOne13.setCellValue("Route");
                            cellOne13.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 12, (short) 4000);

                            HSSFCell cellOne14 = header1.createCell((short) 13);
                            cellOne14.setCellValue("Driver");
                            cellOne14.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 13, (short) 4000);

                            HSSFCell cellOne15 = header1.createCell((short) 14);
                            cellOne15.setCellValue("Vehicle Type");
                            cellOne15.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 14, (short) 4000);

                            HSSFCell cellOne16 = header1.createCell((short) 15);
                            cellOne16.setCellValue("Status");
                            cellOne16.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 15, (short) 4000);

                            ReportTO reportToOne = null;
                            int cntrOne = 1;
                            while (itr8.hasNext()) {
                                reportToOne = (ReportTO) itr8.next();

                                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                                row1 = my_sheet1.createRow((short) cntrOne);

                                row1.createCell((short) 0).setCellValue(cntrOne);
                                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                                cntrOne++;
                            }

                        } else if (k == 3) {
                            sheetName = "Ended Trips";
                            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);

                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);

                            HSSFRow header1 = my_sheet1.createRow(0);
                            header1.setHeightInPoints(20);

                            HSSFCell cellOne1 = header1.createCell((short) 0);
                            style1.setWrapText(true);
                            cellOne1.setCellValue("SNo");
                            cellOne1.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 0, (short) 2000);

                            HSSFCell cellOne2 = header1.createCell((short) 1);
                            style1.setWrapText(true);
                            cellOne2.setCellValue("CustRefNo");
                            cellOne2.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cellOne3 = header1.createCell((short) 2);
                            cellOne3.setCellValue("Customer");
                            cellOne3.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cellOne4 = header1.createCell((short) 3);
                            cellOne4.setCellValue("Goods");
                            cellOne4.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 3, (short) 5000);

                            HSSFCell cellOne5 = header1.createCell((short) 4);
                            cellOne5.setCellValue("Vehicle No");
                            cellOne5.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cellOne6 = header1.createCell((short) 5);
                            cellOne6.setCellValue("TripCode");
                            cellOne6.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 5, (short) 7000);

                            HSSFCell cellOne7 = header1.createCell((short) 6);
                            cellOne7.setCellValue("Container No");
                            cellOne7.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cellOne8 = header1.createCell((short) 7);
                            cellOne8.setCellValue("LR Nos");
                            cellOne8.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cellOne9 = header1.createCell((short) 8);
                            cellOne9.setCellValue("Pkg/Wgt/Cbm");
                            cellOne9.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 8, (short) 4000);

                            HSSFCell cellOne10 = header1.createCell((short) 9);
                            cellOne10.setCellValue("Trip Date");
                            cellOne10.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 9, (short) 4000);

                            HSSFCell cellOne11 = header1.createCell((short) 10);
                            cellOne11.setCellValue("Order Type");
                            cellOne11.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 10, (short) 4000);

                            HSSFCell cellOne12 = header1.createCell((short) 11);
                            cellOne12.setCellValue("Transporter");
                            cellOne12.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 11, (short) 4000);

                            HSSFCell cellOne13 = header1.createCell((short) 12);
                            cellOne13.setCellValue("Route");
                            cellOne13.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 12, (short) 4000);

                            HSSFCell cellOne14 = header1.createCell((short) 13);
                            cellOne14.setCellValue("Driver");
                            cellOne14.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 13, (short) 4000);

                            HSSFCell cellOne15 = header1.createCell((short) 14);
                            cellOne15.setCellValue("Vehicle Type");
                            cellOne15.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 14, (short) 4000);

                            HSSFCell cellOne16 = header1.createCell((short) 15);
                            cellOne16.setCellValue("Status");
                            cellOne16.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 15, (short) 4000);

                            ReportTO reportToOne = null;
                            int cntrOne = 1;
                            while (itr9.hasNext()) {
                                reportToOne = (ReportTO) itr9.next();

                                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                                row1 = my_sheet1.createRow((short) cntrOne);

                                row1.createCell((short) 0).setCellValue(cntrOne);
                                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                                cntrOne++;

                            }

                        } else if (k == 4) {
                            sheetName = "Closure Trips";
                            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);

                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);

                            HSSFRow header1 = my_sheet1.createRow(0);
                            header1.setHeightInPoints(20);

                            HSSFCell cellOne1 = header1.createCell((short) 0);
                            style1.setWrapText(true);
                            cellOne1.setCellValue("SNo");
                            cellOne1.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 0, (short) 2000);

                            HSSFCell cellOne2 = header1.createCell((short) 1);
                            style1.setWrapText(true);
                            cellOne2.setCellValue("CustRefNo");
                            cellOne2.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cellOne3 = header1.createCell((short) 2);
                            cellOne3.setCellValue("Customer");
                            cellOne3.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cellOne4 = header1.createCell((short) 3);
                            cellOne4.setCellValue("Goods");
                            cellOne4.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 3, (short) 5000);

                            HSSFCell cellOne5 = header1.createCell((short) 4);
                            cellOne5.setCellValue("Vehicle No");
                            cellOne5.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cellOne6 = header1.createCell((short) 5);
                            cellOne6.setCellValue("TripCode");
                            cellOne6.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 5, (short) 7000);

                            HSSFCell cellOne7 = header1.createCell((short) 6);
                            cellOne7.setCellValue("Container No");
                            cellOne7.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cellOne8 = header1.createCell((short) 7);
                            cellOne8.setCellValue("LR Nos");
                            cellOne8.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cellOne9 = header1.createCell((short) 8);
                            cellOne9.setCellValue("Pkg/Wgt/Cbm");
                            cellOne9.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 8, (short) 4000);

                            HSSFCell cellOne10 = header1.createCell((short) 9);
                            cellOne10.setCellValue("Trip Date");
                            cellOne10.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 9, (short) 4000);

                            HSSFCell cellOne11 = header1.createCell((short) 10);
                            cellOne11.setCellValue("Order Type");
                            cellOne11.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 10, (short) 4000);

                            HSSFCell cellOne12 = header1.createCell((short) 11);
                            cellOne12.setCellValue("Transporter");
                            cellOne12.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 11, (short) 4000);

                            HSSFCell cellOne13 = header1.createCell((short) 12);
                            cellOne13.setCellValue("Route");
                            cellOne13.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 12, (short) 4000);

                            HSSFCell cellOne14 = header1.createCell((short) 13);
                            cellOne14.setCellValue("Driver");
                            cellOne14.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 13, (short) 4000);

                            HSSFCell cellOne15 = header1.createCell((short) 14);
                            cellOne15.setCellValue("Vehicle Type");
                            cellOne15.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 14, (short) 4000);

                            HSSFCell cellOne16 = header1.createCell((short) 15);
                            cellOne16.setCellValue("Status");
                            cellOne16.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 15, (short) 4000);

                            ReportTO reportToOne = null;
                            int cntrOne = 1;
                            while (itr10.hasNext()) {
                                reportToOne = (ReportTO) itr10.next();

                                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                                row1 = my_sheet1.createRow((short) cntrOne);

                                row1.createCell((short) 0).setCellValue(cntrOne);
                                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                                cntrOne++;

                            }

                        } else if (k == 5) {
                            sheetName = "Moffusil - Own";
                            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);

                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);

                            HSSFRow header1 = my_sheet1.createRow(0);
                            header1.setHeightInPoints(20);

                            HSSFCell cellOne1 = header1.createCell((short) 0);
                            style1.setWrapText(true);
                            cellOne1.setCellValue("SNo");
                            cellOne1.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 0, (short) 2000);

                            HSSFCell cellOne2 = header1.createCell((short) 1);
                            style1.setWrapText(true);
                            cellOne2.setCellValue("CustRefNo");
                            cellOne2.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cellOne3 = header1.createCell((short) 2);
                            cellOne3.setCellValue("Customer");
                            cellOne3.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cellOne4 = header1.createCell((short) 3);
                            cellOne4.setCellValue("Goods");
                            cellOne4.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 3, (short) 5000);

                            HSSFCell cellOne5 = header1.createCell((short) 4);
                            cellOne5.setCellValue("Vehicle No");
                            cellOne5.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cellOne6 = header1.createCell((short) 5);
                            cellOne6.setCellValue("TripCode");
                            cellOne6.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 5, (short) 7000);

                            HSSFCell cellOne7 = header1.createCell((short) 6);
                            cellOne7.setCellValue("Container No");
                            cellOne7.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cellOne8 = header1.createCell((short) 7);
                            cellOne8.setCellValue("LR Nos");
                            cellOne8.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cellOne9 = header1.createCell((short) 8);
                            cellOne9.setCellValue("Pkg/Wgt/Cbm");
                            cellOne9.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 8, (short) 4000);

                            HSSFCell cellOne10 = header1.createCell((short) 9);
                            cellOne10.setCellValue("Trip Date");
                            cellOne10.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 9, (short) 4000);

                            HSSFCell cellOne11 = header1.createCell((short) 10);
                            cellOne11.setCellValue("Order Type");
                            cellOne11.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 10, (short) 4000);

                            HSSFCell cellOne12 = header1.createCell((short) 11);
                            cellOne12.setCellValue("Transporter");
                            cellOne12.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 11, (short) 4000);

                            HSSFCell cellOne13 = header1.createCell((short) 12);
                            cellOne13.setCellValue("Route");
                            cellOne13.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 12, (short) 4000);

                            HSSFCell cellOne14 = header1.createCell((short) 13);
                            cellOne14.setCellValue("Driver");
                            cellOne14.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 13, (short) 4000);

                            HSSFCell cellOne15 = header1.createCell((short) 14);
                            cellOne15.setCellValue("Vehicle Type");
                            cellOne15.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 14, (short) 4000);

                            HSSFCell cellOne16 = header1.createCell((short) 15);
                            cellOne16.setCellValue("Status");
                            cellOne16.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 15, (short) 4000);

                            ReportTO reportToOne = null;
                            int cntrOne = 1;
                            while (itr11.hasNext()) {
                                reportToOne = (ReportTO) itr11.next();

                                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                                row1 = my_sheet1.createRow((short) cntrOne);

                                row1.createCell((short) 0).setCellValue(cntrOne);
                                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                                cntrOne++;

                            }
                        } else if (k == 6) {
                            sheetName = "Local - Own";
                            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);

                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);

                            HSSFRow header1 = my_sheet1.createRow(0);
                            header1.setHeightInPoints(20);

                            HSSFCell cellOne1 = header1.createCell((short) 0);
                            style1.setWrapText(true);
                            cellOne1.setCellValue("SNo");
                            cellOne1.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 0, (short) 2000);

                            HSSFCell cellOne2 = header1.createCell((short) 1);
                            style1.setWrapText(true);
                            cellOne2.setCellValue("CustRefNo");
                            cellOne2.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cellOne3 = header1.createCell((short) 2);
                            cellOne3.setCellValue("Customer");
                            cellOne3.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cellOne4 = header1.createCell((short) 3);
                            cellOne4.setCellValue("Goods");
                            cellOne4.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 3, (short) 5000);

                            HSSFCell cellOne5 = header1.createCell((short) 4);
                            cellOne5.setCellValue("Vehicle No");
                            cellOne5.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cellOne6 = header1.createCell((short) 5);
                            cellOne6.setCellValue("TripCode");
                            cellOne6.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 5, (short) 7000);

                            HSSFCell cellOne7 = header1.createCell((short) 6);
                            cellOne7.setCellValue("Container No");
                            cellOne7.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cellOne8 = header1.createCell((short) 7);
                            cellOne8.setCellValue("LR Nos");
                            cellOne8.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cellOne9 = header1.createCell((short) 8);
                            cellOne9.setCellValue("Pkg/Wgt/Cbm");
                            cellOne9.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 8, (short) 4000);

                            HSSFCell cellOne10 = header1.createCell((short) 9);
                            cellOne10.setCellValue("Trip Date");
                            cellOne10.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 9, (short) 4000);

                            HSSFCell cellOne11 = header1.createCell((short) 10);
                            cellOne11.setCellValue("Order Type");
                            cellOne11.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 10, (short) 4000);

                            HSSFCell cellOne12 = header1.createCell((short) 11);
                            cellOne12.setCellValue("Transporter");
                            cellOne12.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 11, (short) 4000);

                            HSSFCell cellOne13 = header1.createCell((short) 12);
                            cellOne13.setCellValue("Route");
                            cellOne13.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 12, (short) 4000);

                            HSSFCell cellOne14 = header1.createCell((short) 13);
                            cellOne14.setCellValue("Driver");
                            cellOne14.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 13, (short) 4000);

                            HSSFCell cellOne15 = header1.createCell((short) 14);
                            cellOne15.setCellValue("Vehicle Type");
                            cellOne15.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 14, (short) 4000);

                            HSSFCell cellOne16 = header1.createCell((short) 15);
                            cellOne16.setCellValue("Status");
                            cellOne16.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 15, (short) 4000);

                            ReportTO reportToOne = null;
                            int cntrOne = 1;
                            while (itr12.hasNext()) {
                                reportToOne = (ReportTO) itr12.next();

                                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                                row1 = my_sheet1.createRow((short) cntrOne);

                                row1.createCell((short) 0).setCellValue(cntrOne);
                                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                                cntrOne++;

                            }
                        } else if (k == 7) {
                            sheetName = "Cancelled Trips";
                            HSSFSheet my_sheet1 = my_workbook.createSheet(sheetName);

                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);

                            HSSFRow header1 = my_sheet1.createRow(0);
                            header1.setHeightInPoints(20);

                            HSSFCell cellOne1 = header1.createCell((short) 0);
                            style1.setWrapText(true);
                            cellOne1.setCellValue("SNo");
                            cellOne1.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 0, (short) 2000);

                            HSSFCell cellOne2 = header1.createCell((short) 1);
                            style1.setWrapText(true);
                            cellOne2.setCellValue("CustRefNo");
                            cellOne2.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cellOne3 = header1.createCell((short) 2);
                            cellOne3.setCellValue("Customer");
                            cellOne3.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 2, (short) 4000);

                            HSSFCell cellOne4 = header1.createCell((short) 3);
                            cellOne4.setCellValue("Goods");
                            cellOne4.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 3, (short) 5000);

                            HSSFCell cellOne5 = header1.createCell((short) 4);
                            cellOne5.setCellValue("Vehicle No");
                            cellOne5.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 4, (short) 4000);

                            HSSFCell cellOne6 = header1.createCell((short) 5);
                            cellOne6.setCellValue("TripCode");
                            cellOne6.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 5, (short) 7000);

                            HSSFCell cellOne7 = header1.createCell((short) 6);
                            cellOne7.setCellValue("Container No");
                            cellOne7.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 6, (short) 4000);

                            HSSFCell cellOne8 = header1.createCell((short) 7);
                            cellOne8.setCellValue("LR Nos");
                            cellOne8.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 7, (short) 4000);

                            HSSFCell cellOne9 = header1.createCell((short) 8);
                            cellOne9.setCellValue("Pkg/Wgt/Cbm");
                            cellOne9.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 8, (short) 4000);

                            HSSFCell cellOne10 = header1.createCell((short) 9);
                            cellOne10.setCellValue("Trip Date");
                            cellOne10.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 9, (short) 4000);

                            HSSFCell cellOne11 = header1.createCell((short) 10);
                            cellOne11.setCellValue("Order Type");
                            cellOne11.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 10, (short) 4000);

                            HSSFCell cellOne12 = header1.createCell((short) 11);
                            cellOne12.setCellValue("Transporter");
                            cellOne12.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 11, (short) 4000);

                            HSSFCell cellOne13 = header1.createCell((short) 12);
                            cellOne13.setCellValue("Route");
                            cellOne13.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 12, (short) 4000);

                            HSSFCell cellOne14 = header1.createCell((short) 13);
                            cellOne14.setCellValue("Driver");
                            cellOne14.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 13, (short) 4000);

                            HSSFCell cellOne15 = header1.createCell((short) 14);
                            cellOne15.setCellValue("Vehicle Type");
                            cellOne15.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 14, (short) 4000);

                            HSSFCell cellOne16 = header1.createCell((short) 15);
                            cellOne16.setCellValue("Status");
                            cellOne16.setCellStyle(style1);
                            my_sheet1.setColumnWidth((short) 15, (short) 4000);

                            ReportTO reportToOne = null;
                            int cntrOne = 1;
                            while (itr13.hasNext()) {
                                reportToOne = (ReportTO) itr13.next();

                                HSSFRow row1 = my_sheet1.createRow(cntrOne);
                                row1 = my_sheet1.createRow((short) cntrOne);

                                row1.createCell((short) 0).setCellValue(cntrOne);
                                row1.createCell((short) 1).setCellValue(reportToOne.getOrderReferenceNo());
                                row1.createCell((short) 2).setCellValue(reportToOne.getCustName());
                                row1.createCell((short) 3).setCellValue(reportToOne.getDescription());
                                row1.createCell((short) 4).setCellValue(reportToOne.getRegNo());
                                row1.createCell((short) 5).setCellValue(reportToOne.getTripCode());
                                row1.createCell((short) 6).setCellValue(reportToOne.getContainerNo());
                                row1.createCell((short) 7).setCellValue(reportToOne.getTripLrNo());
                                row1.createCell((short) 8).setCellValue(reportToOne.getContainerDetail());
                                row1.createCell((short) 9).setCellValue(reportToOne.getTripStartDate());
                                row1.createCell((short) 10).setCellValue(reportToOne.getMovementType());
                                row1.createCell((short) 11).setCellValue(reportToOne.getVendor());
                                row1.createCell((short) 12).setCellValue(reportToOne.getRouteInfo());
                                row1.createCell((short) 13).setCellValue(reportToOne.getDriverName());
                                row1.createCell((short) 14).setCellValue(reportToOne.getVehicleType());
                                row1.createCell((short) 15).setCellValue(reportToOne.getStatus());

                                cntrOne++;

                            }
                        } else if (k == 8) {
                            sheetName = "WorkShop";
                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);

                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderTop((short) 1);

                            HSSFRow header = my_sheet.createRow(0);

                            style = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                            header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cell3 = header.createCell((short) 2);
                            cell3.setCellValue("Branch");
                            cell3.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);

                            HSSFCell cell4 = header.createCell((short) 3);
                            cell4.setCellValue("Status");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);

                            HSSFCell cellx5 = header.createCell((short) 4);
                            cellx5.setCellValue("ServiceType");
                            cellx5.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);

                            HSSFCell cellx6 = header.createCell((short) 5);
                            cellx6.setCellValue("How Many Days in Workshop");
                            cellx6.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 5, (short) 6000);

                            cntr = 1;
                            ReportTO repTO = null;
                            while (itr14.hasNext()) {
                                repTO = (ReportTO) itr14.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
                                row.getCell((short) 2).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getStatusName());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getServiceTypeName());
                                row.getCell((short) 4).setCellStyle(style1);
                                row.createCell((short) 5).setCellValue(repTO.getAge());
                                row.getCell((short) 5).setCellStyle(style1);
                                cntr++;
                            }

                        } else if (k == 9) {
                            sheetName = "Idle - No Driver";
                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);

                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderTop((short) 1);

                            HSSFRow header = my_sheet.createRow(0);

                            style = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                            header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cell3 = header.createCell((short) 2);
                            cell3.setCellValue("Branch");
                            cell3.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);

                            HSSFCell cell31 = header.createCell((short) 3);
                            cell31.setCellValue("VehicleType");
                            cell31.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);

                            HSSFCell cell4 = header.createCell((short) 4);
                            cell4.setCellValue("How Many Days in Idle?");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);

                            cntr = 1;
                            ReportTO repTO = null;
                            while (itr15.hasNext()) {
                                repTO = (ReportTO) itr15.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
                                row.getCell((short) 2).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getTonnage());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getAge());
                                row.getCell((short) 4).setCellStyle(style1);
                                cntr++;
                            }

                        } else if (k == 10) {
                            sheetName = "WFL";
                            HSSFSheet my_sheet = my_workbook.createSheet(sheetName);
                            HSSFCellStyle style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            HSSFCellStyle style = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style1.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style1.setBorderTop((short) 1);
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderTop((short) 1);

                            HSSFRow header = my_sheet.createRow(0);

                            style = (HSSFCellStyle) my_workbook.createCellStyle();
                            style1 = (HSSFCellStyle) my_workbook.createCellStyle();
                            style.setFillForegroundColor(HSSFColor.PALE_BLUE.index);
                            style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
                            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderTop(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderRight(HSSFCellStyle.BORDER_THIN);
                            style1.setBorderLeft(HSSFCellStyle.BORDER_THIN);

                            header = my_sheet.createRow(0);
                            header.setHeightInPoints(20); // row hight

                            HSSFCell cell1 = header.createCell((short) 0);
                            style.setWrapText(true);
                            cell1.setCellValue("SNo");
                            cell1.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 0, (short) 4000);

                            HSSFCell cell2 = header.createCell((short) 1);
                            cell2.setCellValue("TruckNo");
                            cell2.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 1, (short) 6000);

                            HSSFCell cell3 = header.createCell((short) 2);
                            cell3.setCellValue("Branch");
                            cell3.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 2, (short) 6000);

                            HSSFCell cell31 = header.createCell((short) 3);
                            cell31.setCellValue("VehicleType");
                            cell31.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 3, (short) 6000);

                            HSSFCell cell4 = header.createCell((short) 4);
                            cell4.setCellValue("How Many Days in Idle?");
                            cell4.setCellStyle(style);
                            my_sheet.setColumnWidth((short) 4, (short) 6000);

                            cntr = 1;
                            ReportTO repTO = null;
                            while (itr16.hasNext()) {
                                repTO = (ReportTO) itr16.next();
                                HSSFRow row = my_sheet.createRow(cntr);
                                row = my_sheet.createRow((short) cntr);
                                row.createCell((short) 0).setCellValue(cntr);
                                row.getCell((short) 0).setCellStyle(style1);
                                row.createCell((short) 1).setCellValue(repTO.getVehicleNo());
                                row.getCell((short) 1).setCellStyle(style1);
                                row.createCell((short) 2).setCellValue(repTO.getOperationPoint());
                                row.getCell((short) 2).setCellStyle(style1);
                                row.createCell((short) 3).setCellValue(repTO.getTonnage());
                                row.getCell((short) 3).setCellStyle(style1);
                                row.createCell((short) 4).setCellValue(repTO.getAge());
                                row.getCell((short) 4).setCellStyle(style1);
                                cntr++;
                            }
                        }

                    }

                    System.out.println("excel Sheet created");

                    my_workbook.write(outByteStream);
                    byte[] outArray = outByteStream.toByteArray();
                    response.setContentType("application/ms-excel");
                    response.setContentLength(outArray.length);
                    response.setHeader("Expires:", "0"); // eliminates browser caching
                    response.setHeader("Content-Disposition", "attachment; filename=" + filename + "");
                    outStream = response.getOutputStream();
                    outStream.write(outArray);

                    System.out.println("Excel written successfully..");

                } catch (Exception e) {
                    e.printStackTrace();
                }

                out.clear();
                out = pageContext.pushBody();

            %>



        </form>
    </body>
</html>
