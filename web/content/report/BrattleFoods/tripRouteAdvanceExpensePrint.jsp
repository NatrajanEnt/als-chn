<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <!--<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script> 
        <title>PrintTripAdvance</title>
        <style type="text/css" media="print">
            @media print
            {
                @page {
                    margin-top: 0;
                    margin-bottom: 0;
                }
            } 
        </style>
        <style type="text/css">
            @media print {
                #printbtn {
                    display :  none;
                }
            }
        </style>
    </head>

    <body>
        <form name="enter" method="post">
            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>
            <br>
            <br>
            <br>
            <br>
            <br>
            <%
                Date today = new Date();
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                String startDate = sdf.format(today);
                String billingPartyId = (String)request.getAttribute("billingPartyId");
            %>
            <div id="printContent" >
                <center> <font face="verdana" size="3">&emsp;&emsp;&emsp;<b>Trip (Advance Expenses)<br/></b></font></center><br>
                <table align="center"  style="-webkit-print-color-adjust: exact;border-collapse: collapse;width:880px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                       background:url(images/dict-logoWatermark.png);
                       /*background-repeat:no-repeat;*/
                       background-position:center;
                       border:1px solid;
                       /*background-color: white;*/
                       /*border-color:#CD853F;width:800px;*/
                       border:1px solid;
                       /*opacity:0.8;*/
                       /*font-weight:bold;*/
                       ">

                    <tr>
                        <td style=" border: solid #888 1px;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                            <table align="left" style=" border: solid #888 1px;border-bottom: none;border-left: none;border-right: none;border-top: none;border-collapse: collapse" >
                                <tr>
                                    <td colspan="4"><br>
                                        <font face="verdana" size="2">&ensp;&ensp;&ensp;&ensp;<b>PAN NO-AAACT2874J</b> </font>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font face="verdana" size="2"><b>GST NO-33AAACT2874J2Z8</b></font>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font face="verdana" size="2"><b></b></font><br>
                                    </td>

                                </tr>

                                <tr>
                                    <td>&emsp;&emsp;&emsp;</td>
                                    <!--<td align="left"><img src="images/GatiLogo.png" width="100" height="50"/></td>-->
                                    <td valign="top" align="left">&ensp;<img src="images/Chakiat Logo.png" width="170" height="65"/></td>

                                    <td style="padding-left: 60px; padding-right:50px;">
                                        <br>
                                <center>
                                    <font face="verdana" size="3">&emsp;&emsp;&emsp;<b>CA Logistics India Pvt.Ltd.</b></font><br><br>
                                    <font face="verdana" size="3">&emsp;&emsp;&emsp;40,Rajaji Salai, Chennai,Tamilnadu.</font><br><br>

                                    <!--                                    <font face="verdana" size="3">&emsp;&emsp;&emsp;<b><u>Triway CFS Pvt Ltd.</u></b></font><br>
                                                                        <font face="verdana" size="2">&emsp;&emsp;&emsp;148,Ponneri High Road,<br>&emsp;&emsp;&emsp;Edayanchavadi,<br>&emsp;&emsp;&emsp;New Nappalayam,<br>&emsp;&emsp;&emsp;Chennai.</font><br><br>-->
                                    <!--<font face="verdana" size="2" >&emsp;&emsp;&emsp;&emsp;<b><u>Trip(Advance Expenses)</u></b></font>-->
                                </center>
                        </td>

                        <td align="right" >
                            <br>
                            <div id="externalbox" style="width:3in">
                                &emsp; <div id="inputdata" ></div>
                            </div>
                        </td>
                    </tr>
                </table>
                </td>
                </tr>
                <tr>
                    <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px; " align='center'>
                        <table width="100%"  align="center">
                            <tr>
                                <td ><font face="verdana" size="3"><b>&nbsp;S.No:</b>&nbsp;<c:out value="${voucherNo}"/></font><td/>
                            </tr>
                            <tr>
                                <td ><font face="verdana" size="3"><b>&nbsp;Lorry.No:</b> &nbsp;<c:out value="${vehicleNo}"/></font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <td style="float: left;padding-left: 40px;" ><font face="verdana" size="3"><b>&nbsp;Date :</b> <c:out value="${expesneDate}"/></font></td>
                            </tr>
                            <tr>
                                <td ><font face="verdana" size="3"><b>&nbsp;Driver's.Name & DL No:</b>&nbsp;<c:out value="${driverName}"/> & &nbsp;<c:out value="${registerNo}"/> </font></td>
                                <td style="float: left;padding-left: 40px;" ><font face="verdana" size="3"><b>&nbsp;Transport :</b>&nbsp;<c:out value="${transpoter}"/></font></td>
                            </tr>
                            <tr>
                                <td width="100%" colspan="2"><font face="verdana" size="3"><b>&nbsp;Route :</b>&nbsp;<c:out value="${routeInfo}"/></font></td>
                                <!--                                                                <td width="40%"><font face="verdana" size="2">From&nbsp;...................</font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                                                <td style="float: left;padding-left: 150px;" width="40%"><font face="verdana" size="2">To&nbsp;...................</font></td>-->
                            </tr>
                            <tr>
                                <td ><font face="verdana" size="3"><b>&nbsp;Movement Type :</b>&nbsp;<c:out value="${movementType}"/>  </font></td>
                                <td style="float: left;padding-left: 40px;" ><font face="verdana" size="3"><b>&nbsp;Consignee :</b>&nbsp;<c:out value="${consigneeName}"/></font></td>
                            </tr>
                            <tr>
                                <c:if test="${movementType == 'Moffusil-Export-FCL'  || movementType == 'Moffusil-Import-FCL' }">
                                    <td ><font face="verdana" size="3"><b>&nbsp;Container :</b>&nbsp;</font>
                                        <c:if test="${containerQty == '1' }">
                                            1 X 40
                                        </c:if>
                                        <c:if test="${containerQty == '2' }">
                                            1 X 20
                                        </c:if>
                                        <c:if test="${containerQty == '3' }">
                                            2 X 20
                                        </c:if>
                                    </td>
                                </c:if>

                                <c:if test="${movementType != 'Moffusil-Export-FCL'  && movementType != 'Moffusil-Import-FCL' }">
                                    <td ><font face="verdana" size="3"><b>&nbsp;Pkgs / CBM / Weight :</b>&nbsp;<c:out value="${pkgs}"/> &nbsp; / &nbsp; <c:out value="${volume}"/> &nbsp; / &nbsp; <c:out value="${weight}"/></font></td>
                                </c:if>  
                            </tr>
                            <tr><br></tr>
                            <tr><br></tr>

                            <tr>
                                <td ><font face="verdana" size="3"><b>&nbsp;GR.No:</b>&nbsp;<c:out value="${grNo}"/></font></td>
                                <td style="float: left;padding-left: 40px;" ><font face="verdana" size="3"><b>&nbsp;GR Date.Of.Issue:</b>&nbsp;<c:out value="${grDate}"/></font></td>
                            </tr>
                            <tr>
                                <td  ><font face="verdana" size="3"><b>&nbsp;Party:</b>&nbsp;<c:out value="${billingParty}"/></font></td>
                                <td style="float: left;padding-left: 40px;" ><font face="verdana" size="3"><b>&nbsp;Job No :</b>&nbsp;<c:out value="${customerOrderReferenceNo}"/></font></td>
                                 <!--<td style="float: left;padding-left: 70px;" ><font face="verdana" size="2">Diesel(ltr):&nbsp;<c:out value="${dieselUsed}"/> & Amount: <c:out value="${dieselCost}"/></font></td>-->
                            </tr>
                            <tr>
                                <td ><font face="verdana" size="3"><b>&nbsp;Trip Advance:</b>&nbsp;Rs.<c:out value="${foodCost}"/>/-</font></td>
                                <td style="float: left;padding-left: 40px;" ><font face="verdana" size="3"><b>&nbsp;LR No(s):</b>&nbsp;<c:out value="${tripLrNo}"/><br></font></td>
                                <!--<td style="float: left;padding-left: 70px;" ><font face="verdana" size="2">OP.M/R:&nbsp;<c:out value="${startKm}"/></font></td>-->
                            </tr>
                            <!--                          <tr>
                                                            <td ><font face="verdana" size="2">Vehicle Moved On:&nbsp;&nbsp;<c:out value="${startDate}"/></font></td>
                                                            <td style="float: left;padding-left: 70px;" ><font face="verdana" size="2">CL.M/R:&nbsp;<c:out value="${endKm}"/></font></td>
                                                        </tr>
                                                        <tr>
                                                            <td ><font face="verdana" size="2">Vehicle Returned On:&nbsp;&nbsp;<c:out value="${endDate}"/></font></td>
                                                            <td style="float: left;padding-left: 70px;" ><font face="verdana" size="2">Total Kms:&nbsp;<c:out value="${tripTotalKms}"/></font></td>
                                                        </tr>-->
                        </table>
                        <br>
                        <c:set var="totExpense" value="${0}"/>
                        <c:set var="balance" value="${0}"/>
                        <table align='center' border="1" width="100%">
                            <tr align='center'>
                                <td style="width:2px"><b><font face="verdana" size="3">S.No</font></b></td>
                                <td style="width:500px"><b><font face="verdana" size="3">Particulars</font></b></td>
                                <td style="width:100px"><b><font face="verdana" size="3">Amount</font></b></td>
                            </tr>


                            <!--                            <tr>
                                                            <td align='center'>1</td>
                                                            <td >Diesel/Fuel Expenses
                            <c:forEach items="${getTripExpenseSummaryDetails}" var="trip">
                                <c:if test="${trip.expenseName == 'Fuel Expanses' }">
                                     (   <c:out value="${trip.expenseRemarks}"/> )

                                </c:if>
                            </c:forEach>
                   </td>
                   <td > <c:forEach items="${getAdvanceTripExpense}" var="trip">
                                <c:if test="${trip.expenseName == 'Fuel Expanses' }">
                                    <c:out value="${trip.totalExpenses}"/>
                                    <c:set var="totExpense" value="${trip.totalExpenses + totExpense}"/>
                                </c:if>
                            </c:forEach>
                        </td>
                    </tr>-->
                            <tr>
                                <td align='center'><font face="verdana" size="3">1</font ></td>
                                <td><font face="verdana" size="3">
                                    <c:if test="${transpoter == 'own' }">
                                        &nbsp;Trip Advance + FastTag Recharge 
                                    </c:if>
                                    <c:if test="${transpoter != 'own' }">
                                        &nbsp;Trip Advance 
                                    </c:if>
                                    </font ></td>
                                <td><font face="verdana" size="3"> <c:forEach items="${getAdvanceTripExpense}" var="trip">
                                        <c:if test="${trip.expenseName == 'For  Food' || trip.expenseName == 'For Food & Toll'}">
                                            <c:out value="${trip.expenseValue}"/> &nbsp;</font > (<c:out value="${remarks}"/>) 
                                            <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'><font face="verdana" size="3">2</font ></td>
                                <td><font face="verdana" size="3">&nbsp;Momul</font ></td>
                                <td><font face="verdana" size="3"> <c:forEach items="${getAdvanceTripExpense}" var="trip">
                                        <c:if test="${trip.expenseName == 'For  Food' || trip.expenseName == 'For Food & Toll'}">
                                            <c:out value="${trip.mamul}"/> &nbsp;</font >
                                            <c:set var="totExpense" value="${trip.mamul + totExpense}"/>
                                            <c:set var="balance" value="${trip.balance}"/>
                                        </c:if>
                                    </c:forEach></td>
                            </tr>
                            <tr>
                                <td align='center'><font face="verdana" size="3">3</font ></td>
                                <td><font face="verdana" size="3">&nbsp;Food Expenses</font ></td>
                                <td> <font face="verdana" size="3"><c:forEach items="${getAdvanceTripExpense}" var="trip">
                                        <c:if test="${trip.expenseName == 'For Food' }">
                                            <c:out value="${trip.expenseValue}"/>
                                            <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                        </c:if>
                                    </c:forEach></font ></td>
                            </tr>
                            <tr>
                                <td align='center'><font face="verdana" size="3">4</font ></td>
                                <td><font face="verdana" size="3">&nbsp;Toll Expense</font ></td>
                                <td><font face="verdana" size="3"><c:forEach items="${getAdvanceTripExpense}" var="trip">
                                        <c:if test="${trip.expenseName == 'For Toll' }">
                                            <c:out value="${trip.expenseValue}"/>
                                            <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                        </c:if>
                                    </c:forEach></font ></td>
                            </tr>
                            <tr>
                                <td align='center'><font face="verdana" size="3">5</font ></td>
                                <td><font face="verdana" size="3">&nbsp;Other Expenses</font ></td>
                                <td><font face="verdana" size="3"><c:forEach items="${getAdvanceTripExpense}" var="trip">
                                        <c:if test="${trip.expenseName == 'other' }">
                                            <c:out value="${trip.expenseValue}"/>
                                            <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                        </c:if>
                                    </c:forEach></font ></td>
                            </tr>
                            <tr>
                                <td align='center'><font face="verdana" size="3">6</font ></td>
                                <td><font face="verdana" size="3">&nbsp;For  FastTrack Recharge</font ></td>
                                <td><font face="verdana" size="3"> <c:forEach items="${getAdvanceTripExpense}" var="trip">
                                        <c:if test="${trip.expenseName == 'For  FastTrack Recharge'}">
                                            <c:out value="${trip.expenseValue}"/>
                                            <c:set var="totExpense" value="${trip.expenseValue + totExpense}"/>
                                        </c:if>
                                    </c:forEach></font ></td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td align='center'><b><font face="verdana" size="3">Total Amount</font ></b> </td>
                                <td>&nbsp;<font face="verdana" size="3"><b><c:out value="${totExpense}"/>/-</b><font face="verdana" size="3"></td>
                            </tr>

                        </table>
                        <c:if test="${transpoter != 'own' }">
                            <div align="right"><font face="verdana" size="3">Balance :&nbsp; <b><c:out value="${balance}"/>/-</font></div> 

                        </c:if>

                        <br>
                        <table align="center">
                            <tr>
                                <td width="100%"><font face="verdana" size="3" colspan="2">&nbsp;Received With Thanks Rs.<jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWords" >
                                        <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totExpense")));%>
                                        <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                        <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; </b>
                                    </jsp:useBean>Only.</td>
                                <!--                                <td></td>
                                                                <td></td>-->
                            </tr>
                            <tr>
                                <td  width="100%"colspan="2"><font face="verdana" size="3">&nbsp;from <b>CA Log</b> against the expenses incurred as per above mentioned </td>
                                <!--                                <td></td>
                                                                <td></td>-->
                            </tr>
                            <tr>
                                <td style="padding-top: 15px;" ><font face="verdana" size="3"><b>&nbsp;Revenue Stamp</b></font ></td>

                                <td  colspan="2" align="right"style="padding-top: 15px;"><font face="verdana" size="3"><b> For CA Log </b></font ><td>

                            </tr>


                            <tr>
                                <td style="padding-top: 30px;"><b><font face="verdana" size="3">&nbsp;Driver's Sign</font ></b></td>
                                <td style="padding-right: 250px;padding-top: 30px;" ><font face="verdana" size="3"><b>&nbsp;Cashier</b></font ></td>
                                <td style="padding-top: 30px;" align="left" ><font face="verdana" size="3"><b>&nbsp;TT(Head)</b></font ></td>
                            </tr>
                            <tr>
                                <td style="padding-top: 30px;"><font face="verdana" size="3"><b>&nbsp;Created By:</b>&nbsp&nbsp<c:out value="${createdBy}"/></font ><br></td>
                            </tr>
                        </table>
                        <br>
                        </table>
            </div>
            <br>
            <br>
            <center><input type="button" class="btn btn-info"  id="printbtn" value="Print" onClick="print('printContent');" style="width:90px;height:30px;"></center>
            <script type="text/javascript">
                function print(val)
                {
                    var DocumentContainer = document.getElementById(val);
                    var WindowObject = window.open('', "TrackHistoryData",
                            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                    WindowObject.document.writeln(DocumentContainer.innerHTML);
                    WindowObject.document.close();
                    WindowObject.focus();
                    WindowObject.print();
                    WindowObject.close();
                }
            </script>
            <script type="text/javascript">
                /* <![CDATA[ */
                function get_object(id) {
                    var object = null;
                    if (document.layers) {
                        object = document.layers[id];
                    } else if (document.all) {
                        object = document.all[id];
                    } else if (document.getElementById) {
                        object = document.getElementById(id);
                    }
                    return object;
                }
                //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
                get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
                /* ]]> */
            </script>
        </form>
    </body>
</html>