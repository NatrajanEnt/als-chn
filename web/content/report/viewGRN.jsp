
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
</head>
<body>
    <form name="grn" method="post">

        <% int index = 0;%> 
        <c:if test = "${supplyList != null}" >
            <table  class="table table-info mb30 table-hover" style="width: 50%">
                <c:forEach items="${supplyList}" var="stock"> 
                    <% if (index == 0) {%>   
                    <tr>
                        <td class="contentsub" colspan="2" align="center"> GRN&nbsp;&nbsp;: &nbsp;&nbsp;<c:out value="${stock.supplyId}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Vendor :</td>
                        <td class="text1"><c:out value="${stock.vendorName}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Invoice No:</td>
                        <td class="text1"><c:out value="${stock.inVoiceNumber}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Invoice Date :</td>
                        <td class="text1"><c:out value="${stock.inVoiceDate}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Freight Charge :</td>
                        <td class="text1"><c:out value="${stock.freight}"/></td>
                    </tr>
                    <tr>
                        <td class="text1"> Invoice Amount :</td>
                        <td class="text1"><c:out value="${stock.inVoiceAmount}"/></td>
                    </tr>
                    <% index++;
            }%>               
                </c:forEach>
            </table>

            <table  class="border" style="width: 40%"> 
                <tr>
                    <td class="contentsub" >S No</td>
                    <td class="contentsub" >Item <br>Code</td>
                    <td class="contentsub" >Item<br> Name</td>                        
                    <td class="contentsub" >UOM</td>
                    <td class="contentsub" >Unit<br> Price</td>
                    <td class="contentsub" > Rec<br>Quantity</td>
                    <td class="contentsub" > Acc<br>Quantity</td>
                    <td class="contentsub" > Rtn<br>Quantity</td>
                    <td class="contentsub" >Discount</td>
                    <td class="contentsub" >CGST(%)</td>
                    <td class="contentsub" >SGST(%)</td>
                    <td class="contentsub" >IGST(%)</td>
                    <td class="contentsub" >Amount(Rs.)</td>
                </tr>
                <%  index = 0;%> 
                <c:forEach items="${supplyList}" var="stock"> 
                    <%
        String classText = "";
        int oddEven = index % 2;
        if (oddEven > 0) {
            classText = "text2";
        } else {
            classText = "text1";
        }
                    %>	
                    <tr>
                        <td class="text1" ><%=index + 1%></td>
                        <td class="text1" ><c:out value="${stock.paplCode}"/></td>
                        <td class="text1" ><c:out value="${stock.itemName}"/></td>                          
                        <td class="text1" ><c:out value="${stock.uomName}"/></td>
                        <td class="text1" ><c:out value="${stock.unitPrice}"/></td>
                        <td class="text1" ><c:out value="${stock.receivedQty}"/></td>
                        <td class="text1" ><c:out value="${stock.acceptedQty}"/></td>
                        <td class="text1" ><c:out value="${stock.returnedQty}"/></td>
                        <td class="text1" ><c:out value="${stock.discount}"/></td>
                        <td class="text1" ><c:out value="${stock.cgstPercent}"/></td>
                        <td class="text1" ><c:out value="${stock.sgstPercent}"/></td>
                        <td class="text1" ><c:out value="${stock.igstPercent}"/></td>
                        <td class="text1"  ><c:out value="${stock.itemAmount}"/></td> 
                    </tr>
                    <% index++;%>
                </c:forEach>

            </table>
        </c:if>
        <br>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>


<%@ include file="/content/common/NewDesign/settings.jsp" %>
