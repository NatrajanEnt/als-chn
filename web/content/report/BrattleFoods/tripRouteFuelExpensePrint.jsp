<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<html>
    <head>
        <title>Trip Fuel Slip</title>
        <!--<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
        <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
    </head>
    <body>
        <form name="enter" method="post">
            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>
            <br>
            <br>
            <br>
            <br>
            <br>
            <body>
                <form name="enter" method="post">
                    <%
                        Date today = new Date();
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        String startDate = sdf.format(today);
String billingPartyId = (String)request.getAttribute("billingPartyId");
                    %>
                    <div id="printContent">
                        <table align="center" style="-webkit-print-color-adjust: exact; border-collapse: collapse;width:950px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                               background:url(images/dict-logoWatermark.png);
                               background-repeat:no-repeat;
                               background-position:center;
                               border:1px solid;
                               /*border-color:#CD853F;width:800px;*/
                               border:1px solid;
                               /*opacity:0.8;*/
                               /*font-weight:bold;*/
                               ">
                            <tr style="height: 50px;">
                                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >

                                    <table align="left" >
                                        <tr>
                                            <td colspan="4">
                                                <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                                <font size="2">&ensp;&ensp;<b>PAN NO-xxx</b> </font>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font size="2"><b>GST NO-33AABFC5853E2ZS</b></font>
                                                &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font size="2"><b>CIN NO-U63090TN2006PTC083204</b></font><br>
                                                <%}else {%>
                                                <font size="2">&ensp;&ensp;<b>PAN NO-xxx</b> </font>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font size="2"><b>GST NO-33AABFC5853E2ZS</b></font>
                                                &ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font size="2"><b>CIN NO-U63090TN2006PTC083204</b></font><br>
                                                <%}%>
                                            </td>
                                        </tr>
                                        <tr style="height: 50px">
                                            
                                            <td align="left">&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                                                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                                <!--<img src="images/Chakiat Logo.png" width="120" height="60"/>-->
                                            </td>
                                             
                                            <td style="padding-left: 60px; padding-right:50px;">
                                                <br>
                                        <center>
                                            
                                            <font size="3"><b>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<u>CA Logistics India Pvt.Ltd.</u></b></font><br>
                                            <font size="2">&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;40,Rajaji Salai, Chennai,Tamilnadu..</font><br><br>
                                            <font size="2">&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<b><u>Diesel memo</u></b></font> - <font color="green"><b>ORIGINAL COPY</b></font>
                                        </center>
                                </td>
                                <td align="right">
                                    <br>
                                    <div id="externalbox" style="width:3in">
                                        <div id="inputdata" ></div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        </td>
                        </tr>
                        <!--<tr>-->
                        <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px;height: 100px " align='center'>
                            <table width="100%" height="20%" align="center">
                                <tr>
                                    <td ><font size="2"><b>S.No:</b><c:out value="${voucherNo}"/></font><td/>
                                </tr>
                                <tr>
                                    <td><font size="2"><b>BunkName:</b>&nbsp;<c:out value="${bunkName}"/> </font></td>
                                </tr>
                                <tr>
                                    <td ><font size="2"><b>Lorry.No:</b> <c:out value="${vehicleNo}"/></font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <td style="float: left;padding-left: 70px;" ><font size="2"><b>Date :</b> <c:out value="${expesneDate}"/></font></td>
                                </tr>
                                <tr>
                                    <td ><font size="2"><b>Driver's.Name:</b>&nbsp;<c:out value="${driverName}"/></font></td>
                                    <td style="float: left;padding-left: 70px;" ><font size="2"><b>Transport :</b>&nbsp;<c:out value="${transpoter}"/></font></td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="2"><font size="2"><b>Route :</b>&nbsp;<c:out value="${routeInfo}"/></font></td>
                                </tr>
<!--                                <tr>
                                    <td ><font size="2"><b>GR.No:</b>&nbsp;<c:out value="${grNo}"/></font></td>
                                    <td style="float: left;padding-left: 70px;" ><font size="2"> <b>GR Date.Of.Issue:</b>&nbsp;<c:out value="${grDate}"/></font></td>
                                </tr>-->
                                <tr>
                                    <td ><font size="2"><b>Fuel Slip No:</b>&nbsp;<c:out value="${tripFuelSlipNo}"/></font></td>
                                </tr>

                            </table>
                            <c:set var="totExpense" value="${dieselCost}"/>
                            <table align='center' border="1" width="100%">
                                <tr align='center' style="height:10px;">
                                    <td style="width:2px;"><b>S.No</b></td>
                                    <td style="width:500px"><b>Particulars</b></td>
                                    <td style="width:100px"><b>Quantity (Ltr.)</b></td>
                                    <td style="width:100px"><b>Amount</b></td>
                                </tr>
                                <tr>
                                    <td align='center'>1</td>
                                    <td >Diesel/Fuel Expenses

                                    </td>
                                    <td align='center'>
                                        <c:out value="${dieselUsed}"/>
                                    </td>
                                    <td align='center'>
                                        <c:out value="${dieselCost}"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td align='center'><b>Total Amount</b></td>
                                    <td align='center'><b><c:out value="${dieselUsed}"/></b></td>
                                    <td align='center'><b><c:out value="${dieselCost}"/></b></td>
                                </tr>

                            </table>
                            <table align="center">
                                <tr>
                                    <td width="100%"><font size="2" colspan="2">Received With Thanks Rs.<jsp:useBean id="spareTotalRoundOrg"   class="ets.domain.report.business.NumberWords" >
                                            <%  spareTotalRoundOrg.setRoundedValue(String.valueOf(pageContext.getAttribute("totExpense")));%>
                                            <%  spareTotalRoundOrg.setNumberInWords(spareTotalRoundOrg.getRoundedValue());%>
                                            <b><jsp:getProperty name="spareTotalRoundOrg" property="numberInWords" />&nbsp;</b>
                                        </jsp:useBean>Only.</td>

                                </tr>
                                <tr>
                                    <td  width="100%"colspan="2"><font size="2">from <b>CA Log</b> against the expenses incurred as per above mentioned </td>

                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" ><b>Revenue Stamp</b></td>

                                    <td  colspan="2" align="right"style="padding-top: 5px;"><b> For CA Log</b> <td>

                                </tr>


                                <tr>
                                    <td style="padding-top: 10px;"><b>Driver's Sign</b></td>
                                    <td style="padding-right: 250px;padding-top: 10px;"><b>Cashier</b></td>
                                    <td style="padding-top: 10px;" align="left" ><b>Authorised Signatory</b></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px;"><b>Created By:</b>&nbsp&nbsp<c:out value="${createdBy}"/></td>
                                </tr>

                            </table>
                            </table>
                            <br>
                            <br>
                            <table align="center" style="-webkit-print-color-adjust: exact; border-collapse: collapse;width:950px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                                   background:url(images/dict-logoWatermark.png);
                                   background-repeat:no-repeat;
                                   background-position:center;
                                   border:1px solid;
                                   border:1px solid;
                                   /*font-weight:bold;*/
                                   ">
                                <tr style="height: 50px;">
                                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >

                                        <table align="left" >
                                            <tr>
                                                <td colspan="4">
                                                    <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                                    <font size="2">&ensp;&ensp;<b>PAN NO-AABCT8013A</b> </font>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font size="2"><b>GST NO-33AABCT8013A1Z0</b></font>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font size="2"><b>CIN NO-U63090TN2001PTCO48035</b></font><br>
                                                    <%}else {%>
                                                    <font size="2">&ensp;&ensp;<b>PAN NO-AACCR6979E</b> </font>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font size="2"><b>GST NO-33AACCR6979E1Z6</b></font>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<font size="2"><b>CIN NO-U63011TN2004PTC052290</b></font><br>
                                                    <%}%>
                                                </td>
                                            </tr>
                                            <tr style="height: 50px">
                                                
                                                <td align="left">&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                                                    <!--<img src="images/Chakiat Logo.png" width="120" height="60"/>-->
                                                </td>
                                                    
                                                <td style="padding-left: 60px; padding-right:50px;">
                                                    <br>
                                            <center>
                                               
                                                <font size="3"><b>&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<u>CA Logistics India Pvt.Ltd.</u></b></font><br>
                                                <font size="2">&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;40,Rajaji Salai, Chennai,Tamilnadu.</font><br><br>
                                               
                                                <font size="2">&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;&ensp;<b><u>Diesel memo</u></b></font> -<font color="red"><b>DUPLICATE COPY</b></font>
                                            </center>
                                    </td>
                                    <td align="right">
                                        <br>
                                        <div id="externalbox" style="width:3in">
                                            <div id="inputdata1" ></div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        </tr>
                        <!--<tr>-->
                        <td style="border-bottom: solid #888 1px;border-left: solid #888 1px;border-right: solid #888 1px;height: 100px " align='center'>
                            <table width="100%" height="20%" align="center">
                                <tr>
                                    <td ><font size="2"><b>S.No:</b><c:out value="${voucherNo}"/></font><td/>
                                </tr>
                                <tr>
                                    <td><font size="2"><b>BunkName:</b>&nbsp;<c:out value="${bunkName}"/> </font></td>
                                </tr>
                                <tr>
                                    <td ><font size="2"><b>Lorry.No:</b> <c:out value="${vehicleNo}"/></font></td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <td style="float: left;padding-left: 70px;" ><font size="2"><b>Date :</b> <c:out value="${expesneDate}"/></font></td>
                                </tr>
                                <tr>
                                    <td ><font size="2"><b>Driver's.Name:</b>&nbsp;<c:out value="${driverName}"/></font></td>
                                    <td style="float: left;padding-left: 70px;" ><font size="2"><b>Transport :</b>&nbsp;<c:out value="${transpoter}"/></font></td>
                                </tr>
                                <tr>
                                    <td width="100%" colspan="2"><font size="2"><b>Route :</b>&nbsp;<c:out value="${routeInfo}"/></font></td>
                                </tr>
<!--                                <tr>
                                    <td ><font size="2"><b>GR.No:</b>&nbsp;<c:out value="${grNo}"/></font></td>
                                    <td style="float: left;padding-left: 70px;" ><font size="2"><b> GR Date.Of.Issue:</b>&nbsp;<c:out value="${grDate}"/></font></td>
                                </tr>-->
                                <tr>
                                    <td ><font size="2"><b>Fuel Slip No:</b>&nbsp;<c:out value="${tripFuelSlipNo}"/></font></td>
                                </tr>

                            </table>
                            <c:set var="totExpense" value="${dieselCost}"/>
                            <table align='center' border="1" width="100%">
                                <tr align='center'>
                                    <td style="width:2px"><b>S.No</b></td>
                                    <td style="width:500px"><b>Particulars</b></td>
                                    <td style="width:100px"><b>Quantity (Ltr.)</b></td>
                                    <td style="width:100px"><b>Amount</b></td>
                                </tr>
                                <tr>
                                    <td align='center'>1</td>
                                    <td >Diesel/Fuel Expenses

                                    </td>
                                    <td align='center'>
                                        <c:out value="${dieselUsed}"/>
                                    </td>
                                    <td align='center'>
                                        <c:out value="${dieselCost}"/>
                                    </td>
                                </tr>

                                <tr>
                                    <td>&nbsp;</td>
                                    <td align='center'><b>Total Amount</b> </td>
                                    <td align='center'><b><c:out value="${dieselUsed}"/></b></td>
                                    <td align='center'><b><c:out value="${dieselCost}"/></b></td>
                                </tr>

                            </table>
                            <table align="center">
                                <tr>
                                    <td width="100%"><font size="2" colspan="2">Received With Thanks Rs.<jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWords" >
                                            <%  spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totExpense")));%>
                                            <%  spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                            <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp;</b>
                                        </jsp:useBean>Only.</td>

                                </tr>
                                <tr>
                                    <td  width="100%"colspan="2"><font size="2">from <b>CA Log</b> against the expenses incurred as per above mentioned </td>

                                </tr>
                                <tr>
                                    <td style="padding-top: 5px;" ><b>Revenue Stamp</b></td>

                                    <td  colspan="2" align="right"style="padding-top: 5px;"><b> For CA Log</b> <td>

                                </tr>
                                <tr>
                                    <td style="padding-top: 10px;"><b>Driver's Sign</b></td>
                                    <td style="padding-right: 250px;padding-top: 10px;"><b>Cashier</b></td>
                                    <td style="padding-top: 10px;" align="left" ><b>Authorised Signatory</b></td>
                                </tr>
                                <tr>
                                    <td style="padding-top: 10px;"><b>Created By:</b>&nbsp&nbsp<c:out value="${createdBy}"/></td>
                                </tr>

                            </table>
                            </table>

                    </div>
                    <br>
                    <br>
                    <center><input type="button" class="btn btn-info"  value="Print" onClick="print('printContent');" style="width:90px;height:30px;"></center>
                    <script type="text/javascript">
                        function print(val)
                        {
                            var DocumentContainer = document.getElementById(val);
                            var WindowObject = window.open('', "TrackHistoryData",
                                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                            WindowObject.document.writeln(DocumentContainer.innerHTML);
                            WindowObject.document.close();
                            WindowObject.focus();
                            WindowObject.print();
                            WindowObject.close();
                        }
                    </script>
                    <script type="text/javascript">
                        /* <![CDATA[ */
                        function get_object(id) {
                            var object = null;
                            if (document.layers) {
                                object = document.layers[id];
                            } else if (document.all) {
                                object = document.all[id];
                            } else if (document.getElementById) {
                                object = document.getElementById(id);
                            }
                            return object;
                        }
                        //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
                        get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
                        get_object("inputdata1").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
                        /* ]]> */
                    </script>
                </form>
            </body>
        </div>
    </div>
</div>
</html>