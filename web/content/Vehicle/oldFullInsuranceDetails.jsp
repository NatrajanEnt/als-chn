
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>



<!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
<title>City Master </title>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Old Insurance Details</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Old Insurance Details</a></li>
            <li class="active">Old Insurance Details</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <form name="cityMaster"  method="POST">


                    <center><font color="black" size="4"><c:out value="${regNo}"/>&nbsp;&nbsp;&nbsp;  Old Insurance Details </font></center>
                        <c:if test = "${oldVehicleInsurance != null}">
                            <c:forEach items="${oldVehicleInsurance}" var="oldins">
                            <table class="table table-info mb30 table-hover">

                                <tr>
                                    <td height="30">Policy</td>
                                    <td height="30">
                                        <select class="form-control" style="width:260px;height:40px;"  name="insPolicy" id="insPolicy" style="width:260px;height:40px;" onchange="dispalyPackageType(this.value);" disabled>
                                            <option value="0">New</option>
                                            <option value="1">Renewal</option>
                                        </select>
                                    </td>
                                <script>
                                    document.getElementById("insPolicy").value = '<c:out value="${oldins.insPolicy}" />';
                                </script>
                                <td height="30" style ="display:none" id ="labelPrevPolicy">Previous Policy No</td>
                                <td height="30"  id ="fieldPrevPolicy"   >
                                    <input name="prevPolicy" id ="prevPolicy" readonly style ="display:none;width:250px" maxlength="25" type="text" value="" onkeypress="return blockNonNumbers(this, event, true, false);">
                                </td>
                                </tr>
                                <tr>
                                    <td height="30">Insurance Type :</td>
                                    <td height="30">
                                        <select class="form-control" style="width:260px;height:40px;" readonly name="packageType" id="packageType" style="width:260px;height:40px;" disabled>
                                            <option value="1">Comprehensive Policy</option>
                                            <option value="2">ThirdParty Claim</option>
                                        </select>
                                    </td>
                                <script>
                                    document.getElementById("packageType").value = '<c:out value="${oldins.packageType}" />';
                                </script>
                                <td height="30" id ="blankid1">&nbsp;</td>
                                <td height="30"  id ="blankid2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="6"  height="30">
                                        <div  align="center"><b>Owner Details</b></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30">Name</td>
                                    <td height="30">
                                        <input type="text" name="insName" id="insName" readonly class="form-control" style="width:260px;height:40px;" value='<c:out value="${oldins.insName}" />' style="width:260px;height:40px;"></td>
                                    <td height="30">Address</td>
                                    <td  colspan="3"><input type="text" name="insAddress" id="insAddress" value='<c:out value="${oldins.insAddress}" />'   cols="20" rows="2" style="width:260px;height:40px;"  readonly></td> 
                                </tr>
                                <tr>
                                    <td height="30">Mobile No</td>
                                    <td height="30">
                                        <input type="text" name="insMobileNo" id="insMobileNo" readonly  class="form-control" style="width:260px;height:40px;" value='<c:out value="${oldins.insMobileNo}" />' onkeypress="return blockNonNumbers(this, event, true, false);"></td>
                                    <td height="30"></td>
                                    <td height="30"></td>
                                </tr>
                                <tr>
                                    <td colspan="6"  height="30">
                                        <div  align="center"><b>Insurance Details</b></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30">Policy No</td>
                                    <td height="30"><input name="insPolicyNo" id="insPolicyNo" readonly maxlength="45" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${oldins.insPolicyNo}" />' style="width:260px;height:40px;"></td>
                                    <td height="30"><spring:message code="trucks.label.IDVValue"  text="default text"/> :</td>
                                    <td height="30">
                                        <input name="insIdvValue" id="insIdvValue"  readonly style="width:260px;height:40px;" maxlength="7" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${oldins.insIdvValue}" />' onkeypress="return blockNonNumbers(this, event, true, false);">
                                    </td>
                                </tr>
                                <tr>
                                    <td height="30"><font color="red">*</font><spring:message code="trucks.label.ValidFromDate"  text="default text"/></td>
                                    <td><input name="fromDate1" id="fromDate1" type="text" readonly style="width:260px;height:40px;" class="form-control datepicker" id="fromDate1"   value='<c:out value="${oldins.premiumPaidDate}" />' ></td>
                                    <td height="30"><font color="red">*</font><spring:message code="trucks.label.ValidToDate"  text="default text"/></td>
                                    <td>
                                        <input name="toDate1" id="toDate1" type="text" readonly style="width:260px;height:40px;" class="form-control datepicker" id="toDate1"  value='<c:out value="${oldins.expirydate}" />'  >
                                    </td>
                                </tr>
                                <tr >
                                    <td height="30"><spring:message code="trucks.label.PremiumAmount"  text="default text"/></td>
                                    <td height="30"><input name="premiunAmount" readonly id="premiunAmount" maxlength="7"  type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${oldins.premiumamount}" />' onkeypress="return blockNonNumbers(this, event, true, false);" onchange="premiumAmountSet(this.value);"></td>
                                    <td id="paymentDateIns" style="visibility: hidden;"><font color="red">*</font><spring:message code="trucks.label.PaymentDate"  text="default text"/></td>
                                    <td id="paymentDateIns1" style="visibility: hidden;" height="30"><input style="width:260px;height:40px;" name="paymentDate" id="paymentDate" type="text" class="datepicker" value='<c:out value="${oldins.paymentDate}" />' ></td>
                                </tr>
                                <tr>
                                    <td id="vendorIns" style="display:none" ><spring:message code="trucks.label.InsuranceCompany"  text="default text"/></td>
                                    <td id="vendorIns1" style="display:none"><input type="text" name="vendorId1" id="vendorId1" readonly class="form-control" value="<c:out value="${oldins.vendorName}" />" style="width:260px;height:40px;" >
                                        <!--    <c:if test="${vendorList != null}">
                                        <option value="0" >---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
                                            <c:forEach items="${vendorList}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
                                            </c:forEach>
                                        </c:if>          
                                    </select>
                                    <script>
                                                                document.getElementById("vendorId1").value = '<c:out value="${ins.vendorId}" />'
                                                                                       </script>-->
                                    </td>  

                                    <td id="bankPaymentModeIns" style="display:none" height="30"><spring:message code="trucks.label.PaymentMode"  text="Payment Mode"/>:</td>
                                    <td id="bankPaymentModeIns1" style="display:none" height="30" >
                                        <select class="form-control" readonly style="width:260px;height:40px;" name="paymentType" id="paymentType" style="width:260px;height:40px;" onchange="getBankPayment(this.value)" disabled >
                                            <option value="0">----<spring:message code="trucks.label.Select"  text="default text"/>----</option>
                                            <!--<option value="1">Cash Payment</option>-->
                                            <option value="2"><spring:message code="trucks.label.BankPayment"  text="default text"/></option>
                                        </select> 
                                    </td>
                                <script>
                                    document.getElementById("paymentType").value = '<c:out value="${oldins.paymentType}"/>'
                                </script>
                                </tr>
                                <tr>
                                    <td><spring:message code="trucks.label.IssuingOffice"  text="default text"/></td>
                                    <td>
                                        <input name="issuingOffice" readonly id="issuingOffice" maxlength="45" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${oldins.issuingOffice}"/>' style="width:260px;height:40px;">         
                                        </select>
                                    </td>  
                                    <td  height="30"></td>
                                    <td  height="30">

                                    </td>
                                </tr>
                                <tr id="bankPayment1" >
                                    <td colspan="6"  height="30">
                                        <div   align="center"><b><spring:message code="trucks.label.BankPaymentDetails"  text="default text"/></b></div>
                                    </td>
                                </tr>
                                <tr id="bankPayment2" >
                                    <td height="30"><spring:message code="trucks.label.BankHead"  text="default text"/></td>
                                    <td height="30"><select name="bankId" readonly id="bankId" class="form-control" style="width:260px;height:40px;"  onchange="getBankBranch(this.value)" style="width:260px;height:40px;" disabled>
                                            <c:if test="${primarybankList != null}">
                                                <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                                <c:forEach items="${primarybankList}" var="bankVar">
                                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>

                                                </c:forEach>
                                            </c:if>          
                                        </select>
                                        <script>
                                            document.getElementById("bankId").value = '<c:out value="${oldins.bankId}"/>'
                                        </script>
                                    </td>
                                    <td height="30"><spring:message code="trucks.label.Bankbranch"  text="default text"/> :</td>
                                    <td height="30"> <select name="bankBranchId" readonly id="bankBranchId" class="form-control" style="width:260px;height:40px;" style="width:260px;height:40px;" disabled>
                                            <c:if test="${bankBranchList != null}">
                                                <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                                <c:forEach items="${bankBranchList}" var="branch">
                                                    <option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.branchName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                <script>
                                    document.getElementById("bankBranchId").value = '<c:out value="${oldins.bankBranchId}"/>'
                                </script>
                                </tr>
                                <tr id="bankPayment3" >
                                    <td ><font color="red">*</font><spring:message code="trucks.label.ChequeDate"  text="default text"/></td>
                                    <td><input name="chequeDate" id="chequeDate" readonly type="text" class="form-control datepicker" value='<c:out value="${oldins.chequeDate}"/>' style="width:260px;height:40px;"></td>
                                    <td ><font color="red">*</font><spring:message code="trucks.label.ChequeNo"  text="default text"/></td>
                                    <td><input name="chequeNo" id="chequeNo" readonly maxlength="45" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${oldins.chequeNo}"/>' style="width:260px;height:40px;"></td>
                                </tr>
                                <tr id="bankPayment4" >
                                    <td ><spring:message code="trucks.label.ChequeAmount"  text="default text"/></td>
                                    <td ><input name="chequeAmount" id="chequeAmount" readonly maxlength="7" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${oldins.chequeAmount}"/>' onkeypress="return blockNonNumbers(this, event, true, false);" readonly style="width:260px;height:40px;"></td>
                                    <td></td>  
                                    <td></td>  
                                </tr>
                                </tr>
                            </table>
                        </c:forEach>
                    </c:if>





                    <br>
                    <br>
                    <!--            <script language="javascript" type="text/javascript">
                                    setFilterGrid("table");
                                </script>
                                <div id="controls">
                                    <div id="perpage">
                                        <select onchange="sorter.size(this.value)">
                                            <option value="5" selected="selected">5</option>
                                            <option value="10">10</option>
                                            <option value="20">20</option>
                                            <option value="50">50</option>
                                            <option value="100">100</option>
                                        </select>
                                        <span>Entries Per Page</span>
                                    </div>
                                    <div id="navigation">
                                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                    </div>
                                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                                </div>
                                <script type="text/javascript">
                                    var sorter = new TINY.table.sorter("sorter");
                                    sorter.head = "head";
                                    sorter.asc = "asc";
                                    sorter.desc = "desc";
                                    sorter.even = "evenrow";
                                    sorter.odd = "oddrow";
                                    sorter.evensel = "evenselected";
                                    sorter.oddsel = "oddselected";
                                    sorter.paginate = true;
                                    sorter.currentid = "currentpage";
                                    sorter.limitid = "pagelimit";
                                    sorter.init("table", 0);
                                </script>-->
                    <br>
                    <br>
                    <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>
                    <br>
                    <br>
                    <br>
                </form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>