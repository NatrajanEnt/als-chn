<%--
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "View_Employee_"+curDate+".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test = "${employeeDetails != null}" >
            <table style="width: 1100px" align="center" border="1">
                <thead>
                    <tr height="45">
                        <th >Sno</th>
                        <th >Employee Code</th>
                        <th >Employee Name</th>
                        <th >Mobile No</th>
                        <th >Role Name </th>
                        <th >Department</th>
                        <th >Designation</th>
                        <th >Status</th>
                    </tr>
                </thead>
                <% int index = 0;int sno = 1;%>
                <tbody>
                    <c:forEach items="${employeeDetails}" var="employee">
                        <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                        <tr height="30">
                            <td align="left"  ><%=sno%></td>
                            <td align="left"  ><c:out value="${employee.empCode}"/></a></td>
                            <td align="left"  ><c:out value="${employee.name}"/></a></td>
                            <td align="left"  ><c:out value="${employee.mobile}"/></td>
                            <td align="left"  ><c:out value="${employee.roleName}"/></td>
                            <td align="left"  ><c:out value="${employee.deptName}"/></td>
                            <td align="left"  ><c:out value="${employee.desigName}"/></td>
                            <td align="left"  ><c:out value="${employee.status}"/></td>
                          
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
