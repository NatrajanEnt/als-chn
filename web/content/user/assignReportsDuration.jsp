<%-- 
    Document   : assignReportsDuration
    Created on : 24 Oct, 2016, 12:38:41 PM
    Author     : pavithra
--%>
<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import = "ets.domain.users.business.LoginTO" %>
<%@ page import="java.util.*" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
         <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>
        <script language="javascript">
            function submitPage() {
                document.branchRole.action = '/throttle/assignDurationToReports.do?param=2';
                document.branchRole.submit();

            }
            
            function triggerReport(reportType,reportId){
                alert("reportType"+reportType)
                var url = "";
                if(reportType == 1){ // User
                    url: "/throttle/basicReports.do";
                }else if(reportType == 2){ // Customer
                     //alert("Cusromer reportType"+reportType)
                    url: "/throttle/customerReport.do";
                }else if(reportType == 3){ //Supplier
                    url: "/throttle/basicReports.do";
                }
                $.ajax({
                        url: "/throttle/basicReports.do",
                        async:false,
                        data: {
                            reportId: reportId
                        },
                        success: function (data) {
                                $("#reportStatus").text("Report Triggered Successfully").css("color", "green");
                        }
                    });
            }
            function showHide(value,sno){
//                alert(value);
               if(value==1){//Fixed
                    $("#startHour"+sno).show();
                    $("#endHour"+sno).show();
                    $("#timeDuration"+sno).hide();
                    document.getElementById("timeDuration"+sno).value = "00";
             }else if(value==2){//minute
                    $("#startHour"+sno).hide();
                    document.getElementById("startHour"+sno).value = "00";
                    document.getElementById("timeDuration"+sno).value = "00";
                    $("#endHour"+sno).show();
                    $("#timeDuration"+sno).hide();
             }else if(value==3){//minute
                    $("#startHour"+sno).hide();
                    $("#endHour"+sno).hide();
                    document.getElementById("startHour"+sno).value = "00";
                    document.getElementById("endHour"+sno).value = "00";
                    $("#timeDuration"+sno).show();
             }else if(value==0){//minute
                    $("#startHour"+sno).show();
                    $("#endHour"+sno).show();
                    $("#timeDuration"+sno).show();
                    document.getElementById("startHour"+sno).value = "00";
                    document.getElementById("endHour"+sno).value = "00";
                    document.getElementById("timeDuration"+sno).value = "00";
               }
            }
            
            </script>
    </head>  
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Assign Duration To Reports" text="Assign Duration To Reports"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Settings" text="Settings"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Assign Duration To Reports" text="Assign Duration To Reports"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">

    <body>

        <form name="branchRole" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
         
           
            <% int index = 0; %>
            <c:if test = "${reportsList != null}">
                 <font style="font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight:bold; ">
                    <div align="center" id="reportStatus">
                    </div>
                    </font>
                 
                <!--<table width="700" align="center" border="0" cellpadding="0" cellspacing="0" id="bg" class="border">-->
              
                <table class="table table-info mb30 table-hover" id="bg">
                
                <thead>
                <tr height="35">
                        <th>S.No</td>
                        <th>Report Name</td>
                        <th>Schedule Type</td>
                        <th>Hour</td>
                        <th>Minute</td>
                        <th>Every Hour</td>
                        <th>Trigger</td>
                    </tr>
                    </thead>
                    <c:forEach items="${reportsList}" var="list"> 
                        <%

                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>

                        <tr height="30">
                            <td height="30" ><%= index + 1%> </td>
                            <td height="30" >
                                <input type="hidden" name="reportId" id="reportId<%= index%>" onclick="triggerReport('<c:out value="${list.reportId}" />')" value='<c:out value="${list.reportId}" />' /><c:out value="${list.reportName}" />
                               
                            </td>
                            <td height="30" >
                                 <select name="scheduleType" id="scheduleType<%= index%>" class="form-control" onChange="showHide(this.value,'<%= index%>');" style='width:200px;height:40px'>
                                     <option value="0">--Select--</option>
                                     <option value="1">Fixed</option>
                                     <option value="2">Every Minute</option>
                                     <option value="3">Every Hour</option>
                                 </select>        
                                 <script>
                                     document.getElementById("scheduleType<%= index%>").value = '<c:out value="${list.scheduleType}" />';
                                    
                                 </script>
                            </td>
                            <td height="30" >
                                <select name="startHour" id="startHour<%= index%>" class="form-control" style='width:200px;height:40px'><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                            <script>document.getElementById("startHour<%= index%>").value = '<c:out value="${list.startHour}" />';</script>
                            </td>
                            <td height="30" >
                                <select name="endHour" id="endHour<%= index%>" class="form-control" style='width:200px;height:40px'>
                                    <option value="00">00</option>
<!--                                    <option value="01">01</option>
                                    <option value="02">02</option><option value="03">03</option><option value="04">04</option>-->
                                    <option value="05">05</option>
                                    <!--<option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option>-->
                                    <option value="10">10</option>
                                    <!--<option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option>-->
                                    <option value="15">15</option>
                                    <!--<option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option>-->
                                    <option value="20">20</option>
                                    <!--<option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option>-->
                                    <option value="25">25</option>
                                    <!--<option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option>-->
                                    <option value="30">30</option>
                                    <!--<option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option>-->
                                    <option value="35">35</option>
                                    <!--<option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option>-->
                                    <option value="40">40</option>
                                    <!--<option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option>-->
                                    <option value="45">45</option>
                                    <!--<option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option>-->
                                    <option value="50">50</option>
                                    <!--<option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option>-->
                                    <option value="55">55</option>
                                    <!--<option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option>-->
                                </select>
                             <script> document.getElementById("endHour<%= index%>").value = '<c:out value="${list.endHour}" />';</script>
                            </td>
                            <td height="30" >
                                <select name="timeDuration" id="timeDuration<%= index%>" class="form-control" style='width:200px;height:40px'>
                                    <option value="00">00</option><option value="01">01</option>
                                    <option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option>
                                    <option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option>
                                    <option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option>
                                    <option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option>
                                    <option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option>
                                    <option value="22">22</option><option value="23">23</option>
<!--                                    <option value="24">24</option><option value="25">25</option>
                                    <option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option>
                                    <option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option>
                                    <option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option>
                                    <option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option>
                                    <option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option>
                                    <option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option>
                                    <option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option>
                                    <option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option>
                                    <option value="58">58</option><option value="59">59</option>-->
                                </select>
                             <script>
                                 document.getElementById("timeDuration<%= index%>").value = '<c:out value="${list.timeDuration}" />';
                                  showHide('<c:out value="${list.scheduleType}" />','<%= index%>');
                             </script>
                            </td>
                            <td height="30" >
                                 <input type="button" class="btn btn-success" name="reportIdTrigger" id="reportIdTrigger<%= index%>" onclick="triggerReport('<c:out value="${list.reportType}" />','<c:out value="${list.reportId}" />')" value='MailTrigger' />
                            </td>
                        </tr>
                        <%
                            index++;
                        %>
                    </c:forEach >
                </table>
                <br>
                <center>
                    <input type="button" name="save" value="Update" onClick="submitPage();" class="btn btn-success">
                </center>
                <br>
            </c:if> 
                  <br>
            <br>  <br>
            <br>
        </form>         
    </body>  
</div>
            </div>
            </div>
            <%@ include file="../common/NewDesign/settings.jsp" %>