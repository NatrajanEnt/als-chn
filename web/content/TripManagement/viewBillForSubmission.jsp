<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script>
    function submitPage()
    {
        //alert('hi');
        document.billDetails.action = "/throttle/viewbillpageforsubmit.do";
        document.billDetails.submit();
    }
    function showBillDeatil(invoiceId) {
        //alert("hi");
        document.billDetails.action = "/throttle/showinvoicedetail.do?invoiceId=" + invoiceId;
        document.billDetails.submit();

    }
    function submitBillCourieDetails(invoiceId, tripId) {
        //alert("hi");
        document.billDetails.action = "/throttle/submitBillCourierDetails.do?invoiceId=" + invoiceId + '&tripId=' + tripId;
        document.billDetails.submit();

    }
</script>
</head>
<%
    String menuPath = "Finance >> View Bills";
    request.setAttribute("menuPath", menuPath);
%>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i>  <spring:message code="billing.label.BillDetails"  text="default text"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="billing.label.Billing"  text="default text"/></a></li>
            <li class="active"><spring:message code="billing.label.BillDetails"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="billDetails" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp"%>
<!--                    <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                        <tr id="exp_table" >
                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:850;">
                                    <ul class="tabNavigation">
                                        <li style="background:#76b3f1"> </li>
                                    </ul>-->
                                    <div id="first">
                                        <table class="table table-info mb30 table-hover" >
                                            <thead><tr><th colspan="6"><spring:message code="billing.label.ViewBillDetails"  text="default text"/></th></tr></thead>
                                            <tr>
                                                <td><font color="red">*</font><spring:message code="billing.label.CustomerName"  text="default text"/></td>
                                                <td height="30">  
                                                    <select class="form-control" style="width:260px;height:40px;" name="customerId" id="customerId"  ><option value="">---<spring:message code="billing.label.Select"  text="default text"/>---</option>
                                                        <c:forEach items="${customerList}" var="customerList">
                                                            <option value='<c:out value="${customerList.custId}"/>'><c:out value="${customerList.custName}"/></option>
                                                        </c:forEach>
                                                    </select>
                                                    <script>
                                                document.getElementById('customerId').value = '<c:out value="${customerId}"/>';
                                                    </script>
                                                </td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td><font color="red">*</font><spring:message code="billing.label.FromDate"  text="default text"/></td>
                                                <td height="30"><input name="fromDate" autocomplete="off" id="fromDate"  style="width:260px;height:40px;" type="text" class="datepicker form-control"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                                <td><font color="red">*</font><spring:message code="billing.label.ToDate"  text="default text"/></td>
                                                <td height="30"><input name="toDate" autocomplete="off" id="toDate"  style="width:260px;height:40px;" type="text" class="datepicker form-control" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>

                                                <td> <input type="hidden" name="days"  id="days" value="" /><input type="hidden" name="tripType" id="tripType" value="<c:out value="${tripType}"/>" /></td>

                                                <td><input type="button" class="btn btn-success"   value="<spring:message code="billing.label.FETCHDATA"  text="default text"/>" onclick="submitPage();"></td>
                                            </tr>
                                        </table>
                                    
                            </td>
                        </tr>
                    </table>
                    <c:if test="${closedBillList !=nul}">
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead>
                                <tr >
                                    <th><spring:message code="billing.label.Sno"  text="default text"/></th>
                            <th><spring:message code="billing.label.Bill.No"  text="default text"/></th>
                            <th><spring:message code="billing.label.BillDate"  text="default text"/></th>
                            <th><spring:message code="billing.label.Customer"  text="default text"/></th>
                            <th><spring:message code="billing.label.ContractType"  text="default text"/></th>
                            <th><spring:message code="billing.label.Amount"  text="default text"/></th>
                            <th><spring:message code="billing.label.PODStatus"  text="default text"/></th>
                            <th><spring:message code="billing.label.Select"  text="default text"/></th>
                            </tr>
                            </thead>
                            <tbody>
                                <%int sno=1;
                                int index =0;
                                String classText="";
                                %>
                                <c:forEach items="${closedBillList}" var="closedBillList">
                                    <%int oddEven = index % 2;
                                  if (oddEven > 0) {
                                             classText = "text2";
                                         } else {
                                             classText = "text1";
                                         }
                                    %>
                                    <tr>
                                        <td  height="30"><%=sno++%></td>
                                        <td  height="30"><c:out value="${closedBillList.invoiceCode}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.createddate}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.invoicecustomer}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.billingTypeName}"/></td>
                                        <td  height="30"><c:out value="${closedBillList.grandTotal}"/></td> 
                                        <c:set var="tripId" value="${closedBillList.tripId}" />
                                        <td  height="30" >

                                            <c:if test="${tripType == 2}">
                                                NA
                                            </c:if>
                                            <c:if test="${tripType == 1}">
                                                <c:if test="${closedBillList.tripPod == 0}">
                                                    <a href="viewTripPod.do?tripSheetId=<c:out value="${tripId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                                    </c:if>
                                                    <c:if test="${closedBillList.tripPod > 0}">
                                                    <a href="viewTripPod.do?tripSheetId=<c:out value="${tripId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                                    </c:if>
                                                </c:if>
                                        </td>
                                        <c:if test="${tripType == 1}">
                                            <c:if test="${closedBillList.tripPod == 0}">
                                                <td  height="30">Waiting For POD Upload</td>
                                            </c:if>
                                            <c:if test="${closedBillList.tripPod > 0}">
                                                <td  height="30"><a href="" onclick="submitBillCourieDetails('<c:out value="${closedBillList.invoiceId}"/>', '<c:out value="${closedBillList.tripId}"/>')">view</a></td>
                                            </c:if>
                                        </c:if>
                                        <c:if test="${tripType == 2}">                                            
                                            <td  height="30"><a href="" onclick="submitBillCourieDetails('<c:out value="${closedBillList.invoiceId}"/>', '<c:out value="${closedBillList.tripId}"/>')">view</a></td>
                                        </c:if>
                                    </tr>
                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                                <br>
                                <br>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="billing.label.EntriesPerPage"  text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="billing.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="billing.label.of"  text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                    </c:if>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
