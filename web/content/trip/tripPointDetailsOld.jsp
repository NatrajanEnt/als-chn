<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

    <%@page import="java.text.SimpleDateFormat" %>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ page import="java.util.* "%>
    <%@ page import=" javax. servlet. http. HttpServletRequest" %>
    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
    <script type="text/javascript" src="/throttle/js/validate.js"></script>
    <link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
    <script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
    <script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
    <script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
    <!-- jQuery libs -->
    <script type="text/javascript">
        
        
        $(document).ready(function() {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function() {
            $(".datepicker").datepicker({
                changeMonth: true, changeYear: true
            });
        });
       
</script>

<script>
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#offloadCfsName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getoffloadContractRoutesOrigin.do",
                    dataType: "json",
                    data: {
                        offloadCfsName: request.term,
                        tripId: document.getElementById('tripId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        alert(data);
                        var items = data;
                        if(items == ''){
                        alert("Please select valid place");
                        $('#offloadCfsId').val('');    
                        $('#offloadCfsName').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#offloadCfsId').val(id);
                $('#offloadCfsName').val(value);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            alert(itemVal);
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    
</script>


<script>
function onKeyPressBlockCharacters1(e)
        {
            var fieldLength = document.getElementById('containerNo2').value.length;
            if (fieldLength <= 3) {
                var key = window.event ? e.keyCode : e.which;
                var keychar = String.fromCharCode(key);
                reg = /\d/;
                return !reg.test(keychar);
            } else if (fieldLength <= 10) {
                var key = window.event ? e.keyCode : e.which;
                var keychar = String.fromCharCode(key);
                reg = /[a-zA-Z]+$/;
                return !reg.test(keychar);
            }
       }
        function currentTime() {
            var date, curr_hour, curr_minute;
            date = new Date();
            var day = date.getDate();
            var month = date.getMonth() + 1; //January is 0!
            var year = date.getFullYear();
            if (day < 10) {
                day = '0' + day;
            }
            if (month < 10) {
                month = '0' + month;
            }
            curr_hour = date.getHours();
            curr_minute = date.getMinutes();
            if (parseInt(curr_hour) < 10) {
                curr_hour = '0' + curr_hour;
            }
            if (parseInt(curr_minute) < 10) {
                curr_minute = '0' + curr_minute;
            }
//                alert(day +"-"+ month+"-" + year);
            document.getElementById("pcmremarks1").value = curr_hour;
            document.getElementById("vehicleactreportmin1").value = curr_minute;
            document.getElementById("startDate1").value = day + "-" + month + "-" + year;
        }
        // replace this function 9999
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#pointName9').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCityFromName.do",
                        dataType: "json",
                        data: {
                            cityFrom: request.term,
                            cityToId: document.getElementById('pointId9').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#pointId9').val('');
                                $('#pointName9').val('');
                            } else {
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#pointId9').val(tmp[0]);
                    $('#pointName9').val(tmp[1]);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

        // replace this function 2222
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#pointName2').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCityFromName.do",
                        dataType: "json",
                        data: {
                            cityFrom: request.term,
                            cityToId: document.getElementById('pointId2').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#pointId2').val('');
                                $('#pointName2').val('');
                            } else {
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#pointId2').val(tmp[0]);
                    $('#pointName2').val(tmp[1]);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

        // replace this function 3333
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#pointName3').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCityFromName.do",
                        dataType: "json",
                        data: {
                            cityFrom: request.term,
                            cityToId: document.getElementById('pointId3').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#pointId3').val('');
                                $('#pointName3').val('');
                            } else {
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#pointId3').val(tmp[0]);
                    $('#pointName3').val(tmp[1]);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

        // replace this function 444
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#pointName4').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCityFromName.do",
                        dataType: "json",
                        data: {
                            cityFrom: request.term,
                            cityToId: document.getElementById('pointId4').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if (items == '') {
                                $('#pointId4').val('');
                                $('#pointName4').val('');
                            } else {
                                response(items);
                            }
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#pointId4').val(tmp[0]);
                    $('#pointName4').val(tmp[1]);
                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

</script>



    <script>
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#driver1Name').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getDriverNames.do",
                        dataType: "json",
                        data: {
                            driverName: (request.term).trim(),
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
//                                alert("data"+data); Muthu
//                        var items = data;
//                        response(items);
                            var items = data;
                            var primaryDriver = $('#primaryDriver').val();
//                        if (items == '' && primaryDriver != '') {
//                            alert("Invalid Primary Driver Name");
//                            $('#driver1Name').val('');
//                            $('#driver1Name').val('');
//                            $('#driver1Name').focus();
//                        } else {
//                        }
                            response(items);
                        },
                        error: function(data, type) {
                            //console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var id = ui.item.Id;
                    var name = ui.item.Name;
                    var mobile = ui.item.Mobile;
                    var licenseNo = ui.item.License;
                    var ExpiryDate = ui.item.ExpiryDate;
//                alert("value" + id);

                    $('#driver1Id').val(id);
                    $('#driver1Name').val(name);
                    $('#mobileNo').val(mobile);
                    $('#licenseNo').val(licenseNo);
                    $('#licenseDate').val(ExpiryDate);
                    return false;
                }
                // Format the list menu output of the autocomplete
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
//            alert("item" + item);
                var dlNo = item.License;
                if (dlNo == '') {
                    dlNo = '-';
                }
                var itemVal = item.Name + '(DL:' + dlNo + ')';
//            var temp = itemVal.split('-');
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        //.append( "<a>"+ item.Name + "</a>" )
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });
        //end ajax for vehicle Nos
    </script>

    <script type="text/javascript">
         
        function setStatusDiv() {
            var selectedStatus = document.getElementById("pointStatus").value;

            document.getElementById("penality1").value = selectedStatus;
//            alert(selectedStatus);
            /*   if(selectedStatus == 32){
             $("#towardsEmptyPickup").show();
             $("#towardsFactory").hide();
             $("#towardsClearance").hide();
             $("#towardsPort").hide();
             $("#statusUpdate").hide();
             $("#buttonDiv").show();
             }else */if (selectedStatus == 34) {
                $("#towardsEmptyPickup").hide();
                $("#towardsFactory").show();
                $("#towardsClearance").hide();
                $("#towardsPort").hide();
                $("#statusUpdate").hide();
                $("#buttonDiv").hide();
                $("#statusUpdate").show();
                $("#mailIntimate").show(); 
                $("#createOffloadMovement").hide();
            } else if (selectedStatus == 36) {
                $("#createOffloadMovement").show();
                $("#towardsEmptyPickup").hide();
                $("#towardsFactory").hide();
                $("#towardsClearance").hide();
                $("#towardsPort").hide();
                $("#buttonDiv").hide();
                $("#statusUpdate").show();
                $("#mailIntimate").hide(); 
            } else if (selectedStatus == 38) {
                $("#createOffloadMovement").hide();
                $("#towardsEmptyPickup").hide();
                $("#towardsFactory").hide();
                $("#towardsClearance").hide();
                $("#towardsPort").hide();
                $("#statusUpdate").show();
                $("#buttonDiv").hide();
                $("#mailIntimate").hide(); 
            } else {
                $("#createOffloadMovement").hide();
                $("#mailIntimate").hide(); 
                $("#towardsEmptyPickup").hide();
                $("#towardsFactory").hide();
                $("#towardsClearance").hide();
                $("#towardsPort").hide();
                $("#statusUpdate").show();
                $('#penality1').css('pointer-events', 'none');
                $("#buttonDiv").hide();

            }

        }
        
     
//         function createOffloadMovement() {
//            var val = document.getElementById("tripId").value;
//            var conNo = document.getElementById("containerNo3").value;
//            var conNo = document.getElementById("sealNo3").value;
//            alert(val);
//            var confirms = confirm("Do you really want to update?");
//            if (confirms == true) {
//                var  myWindow;
//                myWindow = window.open('/throttle/createOffloadMovementinTrip.do?tripId=' + val + "&containerNoTemp=" + conNo + + "&containerTon=" + ton);
//                location.reload();
//                myWindow.close();
//            }
//       }
        
        function dateFormat(parameter){
            var temp = parameter.split('-');
            var temp1;
            if(temp[2].length == 4){
            temp1 = temp[2]+'-'+temp[1]+'-'+temp[0];    
            }else{
            temp1 = temp[0]+'-'+temp[1]+'-'+temp[2];    
            }    
            return temp1;
        }
        function compareDates(fromDate,toDate)
        {
        var fDate = new Date(dateFormat(fromDate));
        var tDate = new Date(dateFormat(toDate));
        
        if ( fDate > tDate ) { 
            return 1;
        }else if ( +fDate === +tDate ) { 
            return 2;
        }else {
            return 0;
        }


        }
        function Submit() {

            var status = document.getElementsByName("penality");
            var pointId = document.getElementsByName("pointId");
            var startDate = document.getElementsByName("startDate");
            
            var pcmremarks = document.getElementsByName("pcmremarks");
            var vehicleactreportmin = document.getElementsByName("vehicleactreportmin");
            var cdate = document.getElementsByName("cdate");
            
            var currentLogHour = document.trip.pcmremarks.value;
            var currentLogMin = document.trip.vehicleactreportmin.value;
           
           
            $("#addRoute").hide();
            
            var check = 0;
            for (var i = 0; i < pointId.length; i++) {                
                if (startDate[i] = '') {
                    alert("Please enter date");
                } else {
                    check = 1;
                }
            }
            
            //alert(compareDates('01-01-2019','02-01-2019'));
            if (check == 1) {
                var dateCheckStatus = compareDates(document.trip.startDate.value,document.trip.lastLogDate.value);
                //alert(compareDates(document.trip.startDate.value,document.trip.lastLogDate.value));
                //alert(dateCheckStatus);
                var lastLogTime = document.trip.lastLogTime.value
                var lastLogHour = lastLogTime.split(":")[0];
                var lastLogMin = lastLogTime.split(":")[1];
                if (dateCheckStatus == 0) {
                    alert("Status Date is earlier than last log date");
                    //check = 0;
                    $("#addRoute").show();
                    return;
                }else if (dateCheckStatus == 2) {
                    if(parseInt(currentLogHour) < parseInt(lastLogHour)){
                        alert("Status Time is earlier than last log time");
                        //check = 0;
                        $("#addRoute").show();
                        return;
                    }else  if(parseInt(currentLogHour) == parseInt(lastLogHour)){
                        if(parseInt(currentLogMin) <= parseInt(lastLogMin)){
                           alert("Status Time is earlier than last log time");
                           //check = 0;
                           $("#addRoute").show();
                            return; 
                        }
                    }
                }

            }
            
            
            if (check == 1) {
                document.trip.action = "/throttle/saveTripStausHistory.do";
                document.trip.method = "post";
                document.trip.submit();
            }

        }



        function insert() {

            var selectedStatus = document.getElementById("pointStatus").value;
            var pointId1 = document.getElementById("pointId9").value;
            var driver1Name = document.getElementById("driver1Name").value;

            var pointId2 = document.getElementById("pointId2").value;
            var containerNo2 = document.getElementById("containerNo2").value;
            var sealNo2 = document.getElementById("sealNo2").value;

            var pointId3 = document.getElementById("pointId3").value;

            var pointId4 = document.getElementById("pointId4").value;

            document.trip.action = '/throttle/insertTrackingStatus.do';
            document.trip.submit();


        }
    </script>




    <!--<script  type="text/javascript" src="js/jq-ac-script.js"></script>-->
    <script>

        function callOriginAjaxFullTruck(val) {
            // Use the .autocomplete() method to compile the list based on input from user
            //alert(val);
            //       var pointNameId = 'originNameFullTruck' + val;
            var pointNameId = 'pointName' + val;
            var pointId = 'pointId' + val;
            var desPointName = 0;


            //alert(prevPointId);
            $('#' + pointNameId).autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCityFromList.do",
                        dataType: "json",
                        data: {
                            //                                                    cityName: request.term,
                            cityName: $("#" + pointNameId).val(),
                            //                                                    cityName: $("#" + pointNameId).val(),
                            textBox: 1
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                            //alert("asdfasdf")
                        },
                        error: function(data, type) {
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Value;
                    //alert("value ="+value);
                    var temp = value.split("-");
                    var textId = temp[0];
                    var textName = temp[1];
                    var id = ui.item.Id;
                    //alert("id ="+id);
                    //alert(id+" : "+value);
                    $('#' + pointId).val(textId);
                    $('#' + pointNameId).val(textName);
                    $('#' + desPointName).focus();
                    //validateRoute(val,value);

                    return false;
                }

                // Format the list menu output of the autocomplete
            }).data("ui-autocomplete")._renderItem = function(ul, item) {
                //alert(item);
                //var temp [] = "";
                var itemVal = item.Value;
                var temp = itemVal.split("-");
                var textId = temp[0];
                var t2 = temp[1];
                //                                        alert("t1 = "+t1)
                //                                        alert("t2 = "+t2)
                //alert("itemVal = "+itemVal)
                t2 = '<font color="green">' + t2 + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + t2 + "</a>")
                        .appendTo(ul);
            };


        }
    </script>

    <script language="javascript">
        var poItems = 1;
        var rowCount = 2;
        var sno = 1;
        var snumber = 1;

        function addRow()
        {
            //                                           alert(rowCount);
            if (sno < 20) {
                sno++;
                var tab = document.getElementById("statusTableUpdate");
                rowCount = tab.rows.length;

                snumber = (rowCount) - 1;
                if (snumber == 1) {
                    snumber = parseInt(rowCount);
                } else {
                    snumber++;
                }
                snumber = snumber - 1;
                var newrow = tab.insertRow((rowCount));
                newrow.height = "30px";
                // var temp = sno1-1;
                var cell = newrow.insertCell(0);
                var cell0 = "<td class='text1' align='center'> " + snumber + "</td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;

                cell = newrow.insertCell(1);
                cell0 = "<td class='text1'>\n\
                <input type='hidden' id='pointName" + snumber + "' onChange='validateRoute(" + snumber + ");'  name='pointName' class='form-control' value='' ><input type='hidden' id='pointId" + snumber + "'  readonly name='pointId' class='textbox' value='0' >    \n\
                <select name='penality' id='penality" + snumber + "' class='form-control' style='width:240px;height:40px;' >\n\
                <c:if test="${statusMaster != null}" ><option value='0'>--Select</option> <c:forEach items="${statusMaster}" var="driver"><option  value='<c:out value="${driver.subStatusId}" />'><c:out value="${driver.statusName}" /> </c:forEach > </c:if> \n\
                                            </select></td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;

//                 cell = newrow.insertCell(2);
//                cell0 = "<td class='text1'>\n\
//                <input type='text' id='pointName" + snumber + "' onChange='validateRoute(" + snumber + ");'  name='pointName' class='form-control' value='' ><input type='hidden' id='pointId" + snumber + "'  readonly name='pointId' class='textbox' value='0' >    \n\
//                </td>";
//                //cell.setAttribute(cssAttributeName,"text1");
//                cell.innerHTML = cell0;

                cell = newrow.insertCell(2);
                cell0 = "<td class='text1'><input type='text' autocomplete='off' name='startDate' id='startDate" + snumber + "' value=''  class='datepicker form-control'  style='width:240px;height:40px;' ></td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;

                cell = newrow.insertCell(3);
                cell0 = "<td  height='25' ><select name='pcmremarks' id='pcmremarks" + snumber + "' class='form-control'  /><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select></td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;


                cell = newrow.insertCell(4);
                cell0 = "<td  height='25' ><select name='vehicleactreportmin' id='vehicleactreportmin" + snumber + "' class='form-control' /><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;

                cell = newrow.insertCell(5);
                cell0 = "<td class='text1'><input type='text' name='cdate' id='cdate" + snumber + "' value=''  class='form-control'></td>";
                //cell.setAttribute(cssAttributeName,"text1");
                cell.innerHTML = cell0;

                $(".datepicker").datepicker({
                    //dateFormat: 'dd-mm-yy',
                    changeMonth: true, changeYear: true
                });
                callOriginAjaxFullTruck(snumber);
                rowCount++;
            }
        }
        


            </script>

            <script type="text/javascript" language="javascript">
                $(document).ready(function() {
                    $("#tabs").tabs();
                });
            </script>


            <script type="text/javascript" language="javascript">

                function submitPage() {
                    var errorMsg1 = "";
                    if (isEmpty(document.getElementById("startDate").value)) {
                        alert('please enter plan start date');
                        document.getElementById("startDate").focus();
                    }
                    if (isEmpty(document.getElementById("startOdometerReading").value)) {
                        alert('please enter start odometer reading');
                        document.getElementById("startOdometerReading").focus();
                        return;
                    }
                    if (isEmpty(document.getElementById("vehicleactreportdate").value)) {
                        alert('please enter the vehicle reporting date');
                        document.getElementById("vehicleactreportdate").focus();
                    }
                    if (isEmpty(document.getElementById("vehicleloadtemperature").value)) {
                        alert('please enter the loading temperature');
                        document.getElementById("vehicleloadtemperature").focus();
                    }
                    if (isEmpty(document.getElementById("startHM").value)) {
                        alert('please enter start HM');
                        document.getElementById("startHM").focus();
                    }
                    if (parseFloat(document.getElementById("vehicleloadtemperature").value) == 0) {
                        errorMsg1 += "The value of vehicle temperature is 0\n";
                    }
                    if (parseFloat(document.getElementById("startOdometerReading").value) == 0) {
                        errorMsg1 += "The value of start odometer meter reading is 0\n";
                    }
                    if (parseFloat(document.getElementById("startHM").value) == 0) {
                        errorMsg1 += "The value of start hour meter  reading is 0";
                    }
                    if (errorMsg1 != "") {
                        var r = confirm(errorMsg1);
                        if (r == true)
                        {
                            document.trip.action = '/throttle/updateStartTripSheet.do';
                            document.trip.submit();
                        }
                    } else {
                        document.trip.action = '/throttle/updateStartTripSheet.do';
                        document.trip.submit();

                    }
                }
            </script>

<style>
    html {
	-webkit-font-smoothing: antialiased!important;
	-moz-osx-font-smoothing: grayscale!important;
	-ms-font-smoothing: antialiased!important;
}
body {
  font-family: 'Open Sans', sans-serif;
  font-size:16px;
  color:#555555; 
}
.md-stepper-horizontal {
	display:table;
	width:100%;
	margin:0 auto;
	background-color:#FFFFFF;
	box-shadow: 0 3px 8px -6px rgba(0,0,0,.50);
}
.md-stepper-horizontal .md-step {
	display:table-cell;
	position:relative;
	padding:24px;
}
.md-stepper-horizontal .md-step:hover,
.md-stepper-horizontal .md-step:active {
	background-color:rgba(0,0,0,0.04);
}
.md-stepper-horizontal .md-step:active {
	border-radius: 15% / 75%;
}
.md-stepper-horizontal .md-step:first-child:active {
	border-top-left-radius: 0;
	border-bottom-left-radius: 0;
}
.md-stepper-horizontal .md-step:last-child:active {
	border-top-right-radius: 0;
	border-bottom-right-radius: 0;
}
.md-stepper-horizontal .md-step:hover .md-step-circle {
	background-color:#757575;
}
.md-stepper-horizontal .md-step:first-child .md-step-bar-left,
.md-stepper-horizontal .md-step:last-child .md-step-bar-right {
	display:none;
}
.md-stepper-horizontal .md-step .md-step-circle {
	width:30px;
	height:30px;
	margin:0 auto;
	background-color:#999999;
	border-radius: 50%;
	text-align: center;
	line-height:30px;
	font-size: 16px;
	font-weight: 600;
	color:#FFFFFF;
}
.md-stepper-horizontal.green .md-step.active .md-step-circle {
	background-color:#00AE4D;
}
.md-stepper-horizontal.orange .md-step.active .md-step-circle {
	background-color:#0E880A;
}
.md-stepper-horizontal .md-step.active .md-step-circle {
	background-color: rgb(33,150,243);
}
.md-stepper-horizontal .md-step.done .md-step-circle:before {
	font-family:'FontAwesome';
	font-weight:100;
	content: "\f00c";
}
.md-stepper-horizontal .md-step.done .md-step-circle *,
.md-stepper-horizontal .md-step.editable .md-step-circle * {
	display:none;
}
.md-stepper-horizontal .md-step.editable .md-step-circle {
	-moz-transform: scaleX(-1);
	-o-transform: scaleX(-1);
	-webkit-transform: scaleX(-1);
	transform: scaleX(-1);
}
.md-stepper-horizontal .md-step.editable .md-step-circle:before {
	font-family:'FontAwesome';
	font-weight:100;
	content: "\f040";
}
.md-stepper-horizontal .md-step .md-step-title {
	margin-top:16px;
	font-size:16px;
	font-weight:600;
}
.md-stepper-horizontal .md-step .md-step-title,
.md-stepper-horizontal .md-step .md-step-optional {
	text-align: center;
	color:rgba(0,0,0,.26);
}
.md-stepper-horizontal .md-step.active .md-step-title {
	font-weight: 600;
	color:rgba(0,0,0,.87);
}
.md-stepper-horizontal .md-step.active.done .md-step-title,
.md-stepper-horizontal .md-step.active.editable .md-step-title {
	font-weight:600;
}
.md-stepper-horizontal .md-step .md-step-optional {
	font-size:12px;
}
.md-stepper-horizontal .md-step.active .md-step-optional {
	color:rgba(0,0,0,.54);
}
.md-stepper-horizontal .md-step .md-step-bar-left,
.md-stepper-horizontal .md-step .md-step-bar-right {
	position:absolute;
	top:36px;
	height:1px;
	border-top:1px solid #DDDDDD;
}
.md-stepper-horizontal .md-step .md-step-bar-right {
	right:0;
	left:50%;
	margin-left:20px;
}
.md-stepper-horizontal .md-step .md-step-bar-left {
	left:0;
	right:50%;
	margin-right:20px;
}
</style>



        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> Operation</h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">Loading & Unloading</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="addRow();
                    currentTime();">

                <form name="trip" method="post">
                    <%
               Date today = new Date();
               SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
               String startDate = sdf.format(today);
                    %>
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <br>

                    <input type="hidden" name="today" id="today" value="<%=startDate%>">
                    <br><% int srNo = 1;%>
                    <div class="md-stepper-horizontal orange">
                        <c:if test = "${tripStatusHistory != null}" >
                            <c:forEach items="${tripStatusHistory}" var="hist">
                                 <div class="md-step active done">
                                    <div class="md-step-circle"><span><%=srNo++%></span></div>
                                    <div class="md-step-title"><c:out value="${hist.subStatusId}" /></div>
                                    <div class="md-step-bar-left"></div>
                                    <div class="md-step-bar-right"></div>
                                  </div>
                            </c:forEach>
                         </c:if>    
                        <c:if test = "${statusMasterIncomplete != null}" >
                            <c:forEach items="${statusMasterIncomplete}" var="statusMaster">
                                <div class="md-step">
                                    <div class="md-step-circle"><span><%=srNo++%></span></div>
                                    <div class="md-step-title"><c:out value="${statusMaster.statusName}" /></div>
                                    <div class="md-step-bar-left"></div>
                                    <div class="md-step-bar-right"></div>
                                </div>
                            </c:forEach>
                         </c:if>    
                    </div>
                    <br>
                    
                    <table width="100%" >
                        <% int loopCntr = 0;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <% if(loopCntr == 0) {%>
                                <tr bgcolor="skyblue" style="height:40px;">
                                    <td>&emsp;</td>
                                    <td  > <font color="white"><b> Customer Reference No: <c:out value="${trip.customerOrderReferenceNo}" /></td>
                                    <td  > <font color="white"><b> Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                                    <td  > <font color="white"><b> Vehicle: <c:out value="${trip.vehicleNo}" /></td>
                                    <td  > <font color="white"><b>Driver: <c:out value="${trip.driverName}" /></td>
                                    <td  > <font color="white"><b>Trip Code: <c:out value="${trip.tripCode}"/></td>
                                    <td  > <font color="white"><b>Customer Name:&nbsp;<c:out value="${trip.customerName}"/></td>
                                    <td  > <font color="white"><b>Route: &nbsp;<c:out value="${trip.routeInfo}"/></td>
                                    <!--<td  >Status: <c:out value="${trip.status}"/></td>-->
                                <input type="hidden" name="tripCodeEmail" value='<c:out value="${trip.tripCode}"/>' />
                                <input type="hidden" name="customerNameEmail" value='<c:out value="${trip.customerName}"/>' />
                                <input type="hidden" name="routeInfoEmail" value='<c:out value="${trip.routeInfo}"/>' />
                                <input type="hidden" name="tripType" value='<c:out value="${tripType}"/>' />
                                <input type="hidden" name="movementType" id="movementType" value='<c:out value="${trip.movementType}"/>' />
                                <input type="hidden" name="statusId" value='<c:out value="${statusId}"/>' />
                                </tr>
                                <% }%>
                                <% loopCntr++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br>
                    <center>
                        <font size="4"> Status : &emsp;
                        <select style="width:280px;height: 30px;" name="pointStatus" id="pointStatus" onchange="setStatusDiv()" >
                            <option value="0" selected>--Select--</option>
                            <c:if test="${statusMaster != null}">                                        
                                <c:forEach items="${statusMaster}" var="statusDetails">
                                    <option value='<c:out value="${statusDetails.subStatusId}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                </c:forEach>
                            </c:if>
                        </select >    
                    </center>
                    <br>
                    <div id="createOffloadMovement" style="display:none;"> 
                       <center> <input type="button" class="btn btn-danger" name="markasOffload" id="markasOffload" value="Mark as Offloaded" onchange="markasOffloaded();"> </center> 
                    </div>   
                    <div  style="display:none;"> 
                    <!--<div id="createOffloadMovement" style="display:none;">--> 
                        <center>
                           &emsp; CFS: &emsp;
                           &emsp; <input type="hidden" class="form-control" name="offloadCfsId" id="offloadCfsId" value=""  style="width: 240px;height: 40px"> 
                           &emsp; <input type="text" class="form-control" name="offloadCfsName" id="offloadCfsName" value=""  style="width: 240px;height: 40px"> &emsp;<br>
                            &emsp; PORT: &emsp;
                           &emsp; <input type="hidden" class="form-control" name="offloadPortId" id="offloadPortId" value=""  style="width: 240px;height: 40px"> 
                           &emsp; <input type="text" class="form-control" name="offloadPortName" id="offloadPortName" value=""  style="width: 240px;height: 40px"> <br>
                          &emsp;  <input type="button" class="btn btn-danger" name="offloadMov" id="offloadMov" value="Create Offload Movement" onchange="createOffloadMovement();"> 
                        </center>
                    </div> 
                                
                       <c:if test = "${hireVehicleId != null && hireVehicleId == '0'}" >
                            <table style="display:block"> 
                         </c:if>   
                         <c:if test = "${hireVehicleId != null && hireVehicleId != '0'}" >
                            <table style="display:none;"> 
                         </c:if>   
                                <td>Vehicle No:&emsp;</td>
                                <td>
                                    <input type="hidden" id="hireVehicleId" name="hireVehicleId" value="<c:out value="${hireVehicleId}" />"  class="form-control" style="width:160px" >
                                    <input type="text" id="hireVehicleNo" name="hireVehicleNo" value="<c:out value="${hireVehicleNo}" />"  class="form-control" style="width:160px" >
                                </td>
                            </table> 
                        <br>    
                     <table  class="table table-info mb30 table-hover" id="mailIntimate" style="display: none">
                            <tr  >
                                <td  >Do you want to intimate?</td>
                                <td  >
                                    Client <input type="checkbox" checked name="client" value="1" /> &nbsp;
                                    A/C Mgr<input type="checkbox" checked name="manager" value="1" /> &nbsp;
                                    Fleet Mgr <input type="checkbox" checked name="fleetManager" value="1" /> &nbsp;
                                </td>
                            </tr>
                        </table> 
                    <div id="towardsEmptyPickup" style="display: none">
                        <table   class="table table-info mb30 table-hover"  id="bg">
                            <thead><tr>
                                    <th  colspan="4" >Vehicle Start Details</th>
                                </tr></thead>
                            <tr>
                                <td >Point Name</td>
                                <td >
                                    <input type="text" name="pointName9" id="pointName9" value="<c:out value="${emptyLocName}"/>" class="form-control" style="width: 240px;height: 40px">
                                    <input type="hidden" name="pointId9" id="pointId9" value="<c:out value="${emptyLocation}"/>" class="form-control" style="width: 240px;height: 40px">
                                </td>
                                <td  height="25" colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font>Vehicle Actual Start Date</td>
                                <!--<td ><input type="text" name="startDate1" id="startDate1"  class="datepicker form-control" style="width:240px;height:40px;" value="<%=startDate%>"></td>-->
                                <td ><font color="red">*</font>Vehicle Actual Start Time </td>
                                <td  colspan="3" align="left" height="35" >
                                    HH:<select name="startHuor1" id="startHuor1" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                    MI:<select name="startMin1" id="startMin1" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                            </tr>
                            <tr>
                                <td>Primary Driver </td>
                                <td>
                                    <input type="text" name="driver1Name"  Id="driver1Name" class="form-control" style="width:240px;height:40px;" value="<c:out value="${driverName}"/>"  >
                                    <input type="text" name="driver1Id"   Id="driver1Id" class="form-control" style="width:240px;height:40px;"  style="display: none" value="<c:out value="${driverId}"/>" >
                                </td>
                                <td style="dsplay:none"> Mobile No </td>
                                <td style="dsplay:none" ><input type="text" id="mobileNo" name="mobileNo" class="form-control"   style="width:240px;height:40px;"  maxlength="13" onKeyPress="return onKeyPressBlockCharacters(event);" ></td>

                            </tr>
                            <tr  style="dsplay:none">
                                <td style="dsplay:none">Driver License No</td>
                                <td style="dsplay:none"><input type="text" id="licenseNo" name="licenseNo"  class="form-control" style="width:240px;height:40px;"></td>
                                <td>License Expiry Date</td>
                                <td style="dsplay:none"><input type="text" id="licenseDate" name="licenseDate" readonly class="form-control" style="width:240px;height:40px;"></td>
                            </tr> 
                        </table>

                    </div>

                    <div id="towardsFactory" style="display: none">
                        <table   class="table table-info mb30 table-hover"  id="bg">
                            <thead><tr style="display:none">
                                    <th  colspan="4" >Empty Pickup Details</th>
                                </tr></thead>
                            <tr style="display:none">
                                <td >Point Name</td>
                                <td >
                                    <input type="text" name="pointName2" id="pointName2" class="form-control" style="width:240px; height: 40px;">
                                    <input type="hidden" name="pointId2" id="pointId2" class="form-control" style="width:240px; height: 40px;">
                                </td>
                                <td  height="25" colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr style="display:none">
                                <td ><font color="red">*</font>Vehicle Actual Reporting Date &emsp;&emsp;&emsp;&emsp;</td>
                                <td ><input type="text" name="reportingDate2" id="reportingDate2"  class="datepicker form-control" style="width:240px;height:40px;" value="<%=startDate%>"></td>
                                <td ><font color="red">*</font>Vehicle Actual Reporting Time  </td>
                                <td  colspan="3" align="left" height="25" >
                                    HH:<select name="reportingHuor2" id="reportingHuor2" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                    MI:<select name="reportingMin2" id="reportingMin2" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                            </tr>
                        </table>

                        <table   class="table table-info mb30 table-hover"  id="bg" style="display: none;">
                            <thead>
                            <th  colspan="4" >Vehicle Start From Empty Plot Details</td>
                            </th></thead>
                            <tr>
                                <td ><font color="red">*</font>Empty Container Loading Date &emsp;&emsp;&emsp;&emsp;</td>
                                <td ><input type="text" name="startDate2" id="startDate2" class="datepicker form-control" style="width:240px;height:40px;" value="<%=startDate%>" onchange="calculateDate();" ></td>
                                <td  height="25" ><font color="red">*</font>Empty Container Loading Time </td>
                                <td  colspan="3" align="left" height="25" >HH:<select name="startHuor2" id="startHuor2" class="textbox" ><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                    MI:<select name="startMin2" id="startMin2" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                            </tr>

                        </table>
                            
                          
                        <br>  <% int index10 = 1; int containerCount = 0; %>
                        <c:if test = "${containerDetails != null}" >
                            <table class="table table-info mb30 table-border"  width="100%" >
                                <tr  bgcolor="skyblue" id="index" height="30">
                                    <td   >S No</td>
                                    <td   >Container Type</td>
                                    <td   >Container No</td>
                                    <td   >Seal No</td>
                                    <td   >Wgt(Tonnage)</td>
                                    <td   >Goods Desc</td>
                                </tr>
                                <c:forEach items="${containerDetails}" var="container">
                                    <%
                                           containerCount++;
                                           String classText = "";
                                           int oddEven1 = index10 % 2;
                                           if (oddEven1 > 0) {
                                               classText = "text1";
                                           } else {
                                               classText = "text2";
                                           }
                                    %>
                                    <tr >
                                        <td  > &emsp; &emsp;<%=index10++%></td>
                                        <td  > &emsp; &emsp;<c:out value="${container.containerName}" /></td>
                                        <td  >
                                            <input type="text" name="containerNo2" id="containerNo2" maxlength="11"  onKeyPress="return onKeyPressBlockCharacters1(event);" class="form-control" value='<c:out value="${container.containerNo}" />'  />
                                        </td>
                                         <td  >
                                            <input type="text" name="sealNo2" id="sealNo2" class="form-control" style="width:160px"  value='<c:out value="${container.sealNo}" />' />
                                        </td>
                                        <td  >
                                            <input type="text" name="tonnage2" id="tonnage2" class="form-control" onKeyPress='return onKeyPressBlockCharacters(event);' maxlength="3" style="width:160px" value='<c:out value="${container.tonnage}" />' />
                                        </td>
                                        <td  > &emsp; <c:out value="${container.description}" /></td>
                                       
                                    </tr>
                                </c:forEach >
                            </table>
                            <br/> 

                        </c:if>
                                        
                    </div>

                    <div id="statusUpdate" style="display:none;">
                        <table  class="table table-info mb30 table-hover" id="statusTableUpdate">
                            <thead>
                                <tr>
                                    <th>Sno</th>
                                    <th >Status</th>
                                    <!--<th>Location</th>-->
                                    <th>Date</th>
                                    <th>Hour</th>
                                    <th>Minute</th>
                                    <th>Remarks</th>
                                </tr>
                            </thead>
                            <tr></tr>
                        </table>                        
                        <center><!--<input class="btn btn-info" type="button" name="addRoute" id="addRoute" value="Add Row" onclick="addRow()" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>-->
                            &nbsp;<input class="btn btn-info" type="button" name="addRoute" id="addRoute" value="Save"  onClick="Submit()" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                        </center>
                    </div>
                    <br/>

                    <div id="towardsClearance" style="display: none">

                        <br>
                        <br>   
                        <c:if test = "${containerDetails != null}" >
                            <table class="table table-info mb30 table-border"  width="100%" >
                                <tr  bgcolor="skyblue" id="index" height="30">
                                    <td   >S No</td>
                                    <td   >Container Type</td>
                                    <td   >Container No</td>
                                    <td   >Wgt(Tonnage)</td>
                                    <td   >Goods Desc</td>
                                    <td   >Seal No</td>
                                </tr>
                                <c:forEach items="${containerDetails}" var="container">
                                    <%
                                           containerCount++;
                                           String classText = "";
                                           int oddEven1 = index10 % 2;
                                           if (oddEven1 > 0) {
                                               classText = "text1";
                                           } else {
                                               classText = "text2";
                                           }
                                    %>
                                    <tr >
                                        <td  > &emsp; &emsp;<%=index10++%></td>
                                        <td  > &emsp; &emsp;<c:out value="${container.containerName}" /></td>
                                        <td  >
                                            <input type="text" name="containerNo3" id="containerNo3" maxlength="11" class="form-control" value='<c:out value="${container.containerNo}" />' />
                                        </td>

                                        <td  >
                                            <input type="text" name="tonnage3" id="tonnage3" class="form-control" style="width:160px" value='<c:out value="${container.tonnage}" />' />
                                        </td>
                                        <td  > &emsp; <c:out value="${container.description}" /></td>
                                        <td  >
                                            <input type="text" name="sealNo3" id="sealNo3" class="form-control" style="width:160px"  value='<c:out value="${container.sealNo}" />' />
                                        </td>
                                    </tr>
                                </c:forEach >
                            </table>
                            <br/> 


                        </c:if>

                        <table   class="table table-info mb30 table-hover"  id="bg">
                            <thead><tr>
                                    <th  colspan="4" >Vehicle Reporting Details (Factory)</th>
                                </tr></thead>
                            <tr>
                                <td >Point Name</td>
                                <td >
                                    <input type="text" name="pointName3" id="pointName3" class="form-control" style="width:240px; height: 40px;">
                                    <input type="hidden" name="pointId3" id="pointId3" class="form-control" style="width:240px; height: 40px;">
                                </td>

                                <td  height="25" colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font>Vehicle Actual Reporting Date</td>
                                <td ><input type="text" name="reprtingDate3" id="reprtingDate3"  class="datepicker form-control" style="width:180px;height:40px;" value="<%=startDate%>"></td>
                                <td ><font color="red">*</font>Vehicle Actual Reporting Time </td>
                                <td  colspan="3" align="left" height="25" >
                                    HH:<select name="reportingHuor3" id="reportingHuor3" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                    MI:<select name="reportingMin3" id="reportingMin3" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                            </tr>
                        </table>

                        <table   class="table table-info mb30 table-hover"  id="bg">
                            <thead>
                            <th  colspan="4" >Vehicle Start Details (Factory)</td>
                            </th></thead>
                            <tr>
                                <td ><font color="red">*</font>Vehicle Actual Loading Date</td>
                                <td ><input type="text" name="startDate3" id="startDate3" class="datepicker form-control" style="width:180px;height:40px; value="<%=startDate%>" onchange="calculateDate();" ></td>
                                <td  height="25" ><font color="red">*</font>Vehicle Actual Loading Time </td>
                                <td  colspan="3" align="left" height="25" >HH:<select name="startHuor3" id="startHuor3" class="textbox" onchange="calculateTime();"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                    MI:<select name="startMin3" id="startMin3" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                            </tr>

                        </table>



                    </div>
                    <div id="towardsPort" style="display:none;">
                        <table   class="table table-info mb30 table-hover"  id="bg">
                            <thead><tr>
                                    <th  colspan="4" >Vehicle Reporting Details (Clearance)</th>
                                </tr></thead>
                            <tr>
                                <td >Clearance Point Name</td>
                                <td >
                                    <input type="text" id="pointName4"   name="pointName4" class='form-control' style="width:240px;height: 40px" value="" >
                                    <input type="hidden" id="pointId4"  readonly name="pointId4" class='textbox' value='0' >
                                </td>

                                <td  height="25" colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font>Vehicle Actual Reporting Date</td>
                                <td ><input type="text" name="reprtingDate4" id="reprtingDate4"  class="datepicker form-control" style="width:240px;height:40px;" value="<%=startDate%>"></td>
                                <td ><font color="red">*</font>Vehicle Actual Reporting Time </td>
                                <td  colspan="3" align="left" height="25" >
                                    HH:<select name="reportingHuor" id="reportingHuor4" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                    MI:<select name="reportingMin" id="reportingMin4" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                            </tr>

                            <tr>
                                <td ><font color="red">*</font>Vehicle Actual Start Date</td>
                                <td ><input type="text" name="startDate4" id="startDate4"  class="datepicker form-control" style="width:240px;height:40px;" value="<%=startDate%>"></td>
                                <td ><font color="red">*</font>Vehicle Actual Start Time </td>
                                <td  colspan="3" align="left" height="35" >
                                    HH:<select name="startHuor4" id="startHuor4" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option></select>
                                    MI:<select name="startMin4" id="startMin4" class="textbox"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option><option value="60">60</option></select></td>
                            </tr>

                        </table>     


                    </div>

                    <div id="buttonDiv" style="display: none">
                        <center>
                            <input class="btn btn-info" type="button" name="addRoute" id="addRoute" value="Save"  onClick="insert()" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                        </center>
                    </div>    
                               
 
                    <div>   
                        <c:set var="lastLogDateVal" value="" />
                        <c:set var="lastLogTimeVal" value="" />
                        <c:if test = "${tripStatusHistory != null}" >
                            <table  class="table table-info mb30 table-hover"   >
                                <thead> <tr   >
                                        <td align="center"  colspan="8"><b>Trip Status Details</b></td>
                                    </tr>
                                    <tr >
                                        <th   >S No</th>
                                        <th   >Status</th>
                                        <th   >Date</th>
                                        <th   >Time</th>
                                        <th  >Duration(Hours)</th>
                                        <th   >Remarks</th>
                                    </tr>
                                    <% int index2 = 1; %>
                                    <c:forEach items="${tripStatusHistory}" var="hist">
                                        <%
                                                      String classText1 = "";
                                                      int oddEven = index2 % 2;
                                                      if (oddEven > 0) {
                                                          classText1 = "text1";
                                                      } else {
                                                          classText1 = "text2";
                                                      }
                                        %>
                                        
                                        <tr >
                                            <td  ><%=index2++%></td>
                                            <td  > <c:out value="${hist.subStatusId}" /></td>
                                            <td  > <c:out value="${hist.logDate}" /></td>
                                            <td  > <c:out value="${hist.logTime}" /></td>
                                            <td > <c:out value="${hist.currlocation}" /></td>
                                            <td  > <c:out value="${hist.wfuRemarks}" /></td>
                                            <c:set var="lastLogDateVal" value="${hist.logDate}" />
                                            <c:set var="lastLogTimeVal" value="${hist.logTime}" />
                                            
                                        </tr>
                                    </c:forEach>
                                            <input type="hidden" name="lastLogDate" value='<c:out value="${lastLogDateVal}" />' />
                                            <input type="hidden" name="lastLogTime" value='<c:out value="${lastLogTimeVal}" />' />

                                </thead>
                            </table> 
                        </c:if>
                    </div>   

                    <div >
                        <c:if test = "${tripPointDetails != null}" >
                            <table  class="table table-info mb30 table-hover" style="display:none"  >
                                <thead> <tr   >
                                        <td align="center"  colspan="8"><b>Trip Status Details</b></td>
                                    </tr>
                                    <tr >
                                        <th   >S No</th>
                                        <th   >Status</th>
                                        <th   >Location</th>
                                        <th   >Planned Date</th>
                                        <th   >Planned Time</th>
                                        <th   >Address</th>
                                        <!--<th   >Route Order</th>-->
                                        <!--<th   >Select</th>-->
                                    </tr>
                                </thead>

                                <% int index2 = 1; %>
                                <c:forEach items="${tripPointDetails}" var="tripPoint">
                                    <%
                                                   String classText1 = "";
                                                   int oddEven = index2 % 2;
                                                   if (oddEven > 0) {
                                                       classText1 = "text1";
                                                   } else {
                                                       classText1 = "text2";
                                                   }
                                    %>

                                    <tr >
                                        <td  ><%=index2++%></td>
                                        <td  >
                                            <c:if test = "${tripPoint.movementType == 1}" >
                                                <c:if test = "${tripPoint.pointType == 'PickUp'}" >
                                                    Empty Pickup
                                                </c:if>
                                                <c:if test = "${tripPoint.pointType != 'PickUp'}" >
                                                    <c:out value="${tripPoint.pointType}" />
                                                </c:if>
                                            </c:if>

                                            <c:if test = "${tripPoint.movementType == 2}" >
                                                <c:if test = "${tripPoint.pointType == 'PickUp'}" >
                                                    Container Pickup
                                                </c:if>
                                                <c:if test = "${tripPoint.pointType != 'PickUp'}" >
                                                    <c:out value="${tripPoint.pointType}" />
                                                </c:if>
                                                <c:if test = "${tripPoint.pointType == 'End Point' || tripPoint.pointType == 'Drop' }" >
                                                    Empty Drop
                                                </c:if>
                                                <c:if test = "${tripPoint.pointType == 'End Point' || tripPoint.pointType == 'Drop' }" >
                                                    Empty Drop
                                                </c:if>
                                            </c:if>
                                        </td>
                                        <td  ><c:out value="${tripPoint.pointName}" /></td>

                                        <td  ><c:out value="${tripPoint.pointPlanDate}" /></td>
                                        <td  ><c:out value="${tripPoint.pointPlanTime}" /></td>
                                        <td style="display:none" ><input type="hidden" id="tripId" name="tripId" value="<c:out value="${tripPoint.tripId}"/>">
                                            <%--<c:if test = "${tripPoint.pointSequence != '1' && tripPoint.pointType eq 'Start Point' || tripPoint.pointType eq 'Pick Up'}" >--%>
                                            <c:if test = "${tripPoint.pointType == 'PickUp'}" >
                                                <a href="viewTripPointDetails.do?tripRouteCourseId=<c:out value="${tripPoint.tripRouteCourseId}"/>&pointType=<c:out value="${tripPoint.pointType}"/>&tripId=<c:out value="${tripPoint.tripId}"/>&pointName=<c:out value="${tripPoint.pointName}"/>">Enter Pickup Details</a>  
                                                <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>
                                            </c:if>
                                            <c:if test = "${tripPoint.pointType == 'Loading'}" >
                                                <a href="viewTripPointDetails.do?tripRouteCourseId=<c:out value="${tripPoint.tripRouteCourseId}"/>&pointType=<c:out value="${tripPoint.pointType}"/>&tripId=<c:out value="${tripPoint.tripId}"/>&pointName=<c:out value="${tripPoint.pointName}"/>">Enter Loading Details</a>  
                                                <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>
                                            </c:if>
                                            <c:if test = "${tripPoint.pointSequence > tripPointDetailsSize && tripPoint.pointType eq 'End Point' || tripPoint.pointType eq 'Drop' || tripPoint.pointType eq 'Interam Point'}" >
                                                <a href="viewTripPointDetails.do?tripRouteCourseId=<c:out value="${tripPoint.tripRouteCourseId}"/>&pointType=<c:out value="${tripPoint.pointType}"/> &pointName=<c:out value="${tripPoint.pointName}"/>&tripId=<c:out value="${tripPoint.tripId}"/>">Enter UnLoading Details</a>
                                                <input type="hidden" name="tripRouteCourseId" id="tripRouteCourseId" value="<c:out value="${tripPoint.tripRouteCourseId}"/>"/>
                                            </c:if>
                                        </td>
                                        <td  ><c:out value="${tripPoint.pointAddress}" /></td>
                                        <%--<td  ><c:out value="${tripPoint.pointSequence}" /></td>--%>
                                    </tr>
                                </c:forEach >
                            </table>

                        </c:if>
                        <br/>
                        <br/>
                    </div>
                        
                    <c:if test = "${tripPointDetails != null}" >
                        <table  class="table table-info mb30 table-hover" style="display:none" >
                            <thead> <tr   >
                                    <td align="center" colspan="8"><b>Reporting Details</b></td>
                                </tr>
                                <tr >
                                    <th   >S No</th>
                                    <th   >Point Name</th>
                                    <th   >Point Type</th>
                                    <th   >Reporting Date</th>
                                    <th   >Reporting Time</th>
                                    <th   >Loading & Unloading Date</th>
                                    <th   >Loading & Unloading Time</th>
                                    <th   >Temperature</th>
                                </tr></thead>
                                <% int index2 = 1; %>
                                <c:forEach items="${tripPointDetails}" var="tripPoint">
                                    <%
                                                   String classText1 = "";
                                                   int oddEven = index2 % 2;
                                                   if (oddEven > 0) {
                                                       classText1 = "text1";
                                                   } else {
                                                       classText1 = "text2";
                                                   }
                                    %>
                                <tr >
                                    <td  ><%=index2++%></td>
                                    <td  ><c:out value="${tripPoint.pointName}" /></td>
                                    <td  ><c:out value="${tripPoint.pointType}" /></td>
                                    <td  ><c:out value="${tripPoint.startReportingDate}" /></td>
                                    <td  ><c:out value="${tripPoint.startReportingTime}" /></td>
                                    <td  ><c:out value="${tripPoint.loadingDate}" /></td>
                                    <td  ><c:out value="${tripPoint.loadingTime}" /></td>
                                    <td  ><c:out value="${tripPoint.loadingTemperature}" /></td>
                                </tr>
                            </c:forEach >
                        </table>
                        <br/>
                    </c:if>

                </form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>