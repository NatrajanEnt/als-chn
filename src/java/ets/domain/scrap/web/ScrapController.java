/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scrap.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.scrap.business.ScrapBP;
import ets.domain.scrap.business.ScrapTO;
import ets.domain.section.business.SectionBP;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ParveenErrorConstants;
import java.util.ArrayList;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;

public class ScrapController extends BaseController {

    ScrapBP scrapBP;
    ScrapCommand scrapCommand;
    SectionBP sectionBP;
    LoginBP loginBP;

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public SectionBP getSectionBP() {
        return sectionBP;
    }

    public void setSectionBP(SectionBP sectionBP) {
        this.sectionBP = sectionBP;
    }

    public ScrapCommand getScrapCommand() {
        return scrapCommand;
    }

    public void setScrapCommand(ScrapCommand scrapCommand) {
        this.scrapCommand = scrapCommand;
    }

    public ScrapBP getScrapBP() {
        return scrapBP;
    }

    public void setScrapBP(ScrapBP scrapBP) {
        this.scrapBP = scrapBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);
    }

    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView manageDisposePage(HttpServletRequest request, HttpServletResponse response, ScrapCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        scrapCommand = command;
        ScrapTO scrapTO = new ScrapTO();
        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Scrap  >> Manage Scrap";
        path = "content/scrap/disposeScrap.jsp";
        try {
             setLocale(request, response);

            if (!loginBP.checkAuthorisation(userFunctions, "Scrap-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Scrap";
                request.setAttribute("pageTitle", pageTitle);

                ArrayList disposeInList = new ArrayList();
                disposeInList = scrapBP.processDisposeInList();
                request.setAttribute("DisposeInList", disposeInList);

                ArrayList uomList = new ArrayList();
                uomList = sectionBP.processGetUomList();
                request.setAttribute("UomList", uomList);

                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //saveScrap
    /**
     * This method used to Modify Designation Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView disposeScrapList(HttpServletRequest request, HttpServletResponse response, ScrapCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ModelAndView mv = null;
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        ArrayList kgAmountUnique = new ArrayList();
        ArrayList noAmountUnique = new ArrayList();
        String kgAmountAll = null;
        int kgUniqueAmount = 0;
        int noUniqueAmount = 0;
        int toMakeKgTotal = 0;
        int noTotalAmount = 0;
        int kgRate = 0;
        int totalAmountAll = 0;
        int disposeMasterStatus = 0;
        ArrayList noAmountAll = new ArrayList();
        scrapCommand = command;
        String menuPath = "HRMS  >>  manageScrap    ";
        path = "content/scrap/disposeScrap.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        int userId = (Integer) session.getAttribute("userId");
        int companyId = Integer.parseInt((String) session.getAttribute("companyId"));

        try {
             setLocale(request, response);

            ScrapTO scrapTO = new ScrapTO();
            String pageTitle = "Manage scrap";
            request.setAttribute("pageTitle", pageTitle);

            String[] selectedIndex = scrapCommand.getSelectedIndex();
            String[] unitPrices = scrapCommand.getUnitPrices();

            //single rate for all  kg specified in the list
            String indudualRate = scrapCommand.getKgunitPrice();

            String[] uomNames = scrapCommand.getUomNames();
            String[] quanditys = scrapCommand.getQuanditys();
            String[] scrapIds = scrapCommand.getScrapIds();

            ScrapTO kgList = null;

            //for others
            int unitPrice = 0;
            int kgTotalWeight = 0;
            //for kg price unique
            int unitKgPrice = 0;
            int quandity = 0;
            scrapTO = new ScrapTO();
            int index = 0;
            for (int i = 0; i < selectedIndex.length; i++) {
                scrapTO = new ScrapTO();
                index = Integer.parseInt(selectedIndex[i]);
                if (uomNames[index].equals("Kg")) {
                    if (indudualRate != "NA") {
                        scrapTO.setScrapId(scrapIds[index]);
                        quandity = Integer.parseInt(quanditys[index]);
                        scrapTO.setQuandity(String.valueOf(quandity));
                        unitKgPrice = Integer.parseInt(indudualRate);
                        scrapTO.setUnitPrice(String.valueOf(unitKgPrice));
                        kgUniqueAmount = (unitKgPrice * quandity);
                        scrapTO.setKgAmountUnique(String.valueOf(kgUniqueAmount));
                        // total ka weight
                        kgTotalWeight = kgTotalWeight + quandity;
                        scrapTO.setKgTotalWg(kgTotalWeight);
                        ////System.out.println("scrapIds=" + scrapIds[index]);
                        ////System.out.println("quandity=" + quandity);
                        ////System.out.println("unitKgPrice=" + unitKgPrice);
                        ////System.out.println("kgUniqueAmount=" + kgUniqueAmount);
                        //total amout for all kg
                        toMakeKgTotal = toMakeKgTotal + kgUniqueAmount;
                        scrapTO.setKgTotalAmount(toMakeKgTotal);
                        kgAmountUnique.add(scrapTO);
                    }
                } else {
                    scrapTO.setScrapId(scrapIds[index]);
                    quandity = Integer.parseInt(quanditys[index]);
                    scrapTO.setQuandity(String.valueOf(quandity));
                    unitPrice = Integer.parseInt(unitPrices[index]);
                    scrapTO.setUnitPrice(String.valueOf(unitPrice));
                    noUniqueAmount = unitPrice * quandity;
                    scrapTO.setNoAmountUnique(String.valueOf(noUniqueAmount));
                    ////System.out.println("Kg scrapIds=" + scrapIds[index]);
                    ////System.out.println("quandity=" + quandity);
                    ////System.out.println("unitPrice=" + unitPrice);
                    ////System.out.println("noUniqueAmount=" + noUniqueAmount);
                    noTotalAmount = noTotalAmount + noUniqueAmount;
                    noAmountUnique.add(scrapTO);
                }
            }
            if (kgTotalWeight != 0) {
                kgRate = toMakeKgTotal / kgTotalWeight;
            }
            totalAmountAll = toMakeKgTotal + noTotalAmount;
            disposeMasterStatus = scrapBP.processSaveScrap(kgRate, totalAmountAll, kgAmountUnique, noAmountUnique, userId, companyId, scrapTO);
            mv = manageDisposePage(request, response, command);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Scrap Disposed Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to process scrap --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return mv;
    }
//viewScrap
    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView viewScrap(HttpServletRequest request, HttpServletResponse response, ScrapCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        scrapCommand = command;
        ScrapTO scrapTO = new ScrapTO();
        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Scrap  >> Manage Scrap";
        path = "content/scrap/manageScrap.jsp";
        try {
             setLocale(request, response);

            if (!loginBP.checkAuthorisation(userFunctions, "Scrap-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Section";
                request.setAttribute("pageTitle", pageTitle);

                ArrayList sectionList = new ArrayList();
                sectionList = sectionBP.processGetSectionList();
                request.setAttribute("SectionList", sectionList);

                ArrayList uomList = new ArrayList();
                uomList = sectionBP.processGetUomList();
                request.setAttribute("UomList", uomList);

                ArrayList disposeInList = new ArrayList();
                disposeInList = scrapBP.processDisposeInList();
                request.setAttribute("DisposeInList", disposeInList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView searchScrap(HttpServletRequest request, HttpServletResponse response, ScrapCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        scrapCommand = command;
        ScrapTO scrapTO = new ScrapTO();
        HttpSession session = request.getSession();
        String path = "";

        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");

        String menuPath = "Scrap  >> Manage Scrap";
        path = "content/scrap/manageScrap.jsp";
        try {
             setLocale(request, response);

            if (!loginBP.checkAuthorisation(userFunctions, "Scrap-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                ArrayList sectionList = new ArrayList();
                sectionList = sectionBP.processGetSectionList();
                request.setAttribute("SectionList", sectionList);

                ArrayList uomList = new ArrayList();
                uomList = sectionBP.processGetUomList();
                request.setAttribute("UomList", uomList);

                int sectionId = 0;
                int uomId = 0;
                String secId = scrapCommand.getSectionId();
                String uId = scrapCommand.getUomId();

                if (scrapCommand.getSectionId() != null && scrapCommand.getSectionId() != "") {
                    scrapTO.setSectionId(scrapCommand.getSectionId());
                } else {
                    scrapTO.setSectionId("");
                }

                if (scrapCommand.getUomId() != null && scrapCommand.getUomId() != "") {
                    scrapTO.setUomId(scrapCommand.getUomId());
                } else {
                    scrapTO.setUomId("");
                }

                /*      if(secId != "" && secId != null )
                {
                sectionId = Integer.parseInt(secId);
                }
                if(uId != "" && uId != null )
                {
                uomId = Integer.parseInt(uId);
                }    */
                ArrayList sectionWiseList = new ArrayList();
                ////System.out.println("scrapTO.uom" + scrapTO.getUomId());
                ////System.out.println("scrapTO.Section" + scrapTO.getSectionId());
                // sectionWiseList = scrapBP.processSectionWiseList(sectionId, uomId);
                request.setAttribute("uomId", scrapCommand.getUomId());
                request.setAttribute("sectionId", scrapCommand.getSectionId());
                sectionWiseList = scrapBP.processSectionWiseList(scrapTO);
                request.setAttribute("SectionWiseList", sectionWiseList);
                ////System.out.println("sectionWiseList.siz=" + sectionWiseList.size());
                menuPath = "Scrap  >> Manage Scrap";

            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView scrapApprovalPage(HttpServletRequest request, HttpServletResponse response, ScrapCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
         setLocale(request, response);

        String menuPath = "Srap >> Scrap Approval";
        String path = "content/scrap/approveScrapItems.jsp";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        return new ModelAndView(path);
    }
    
    
    public ModelAndView scrapApproval(HttpServletRequest request, HttpServletResponse response, ScrapCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        scrapCommand = command;
        ScrapTO scrapTO = new ScrapTO();
        HttpSession session = request.getSession();
        String path = "";
        
        ArrayList scrapItems = new ArrayList();
        ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
        String menuPath = "Srap >> Scrap Approval";
        path = "content/scrap/approveScrapItems.jsp";
        try {
             setLocale(request, response);

            if (!loginBP.checkAuthorisation(userFunctions, "Scrap-View")) {
                path = "content/common/NotAuthorized.jsp";
            } else {
                scrapTO.setItemName(scrapCommand.getItemName());
                scrapTO.setPaplCode(scrapCommand.getPaplCode());
                scrapTO.setFromDate(scrapCommand.getFromDate());
                scrapTO.setToDate(scrapCommand.getToDate());
                
                scrapItems = scrapBP.scrapApprovalItems(scrapTO);
                if(scrapItems.size() != 0){
                    request.setAttribute("scrapItems", scrapItems);                    
                }
                request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
                String pageTitle = "View Section";
                request.setAttribute("pageTitle", pageTitle);  
                
                request.setAttribute("fromDate",scrapCommand.getFromDate());
                request.setAttribute("toDate",scrapCommand.getToDate());
                request.setAttribute("paplCode",scrapCommand.getPaplCode());
                request.setAttribute("itemName",scrapCommand.getItemName());
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }    
    

  public ModelAndView handleScrapItems(HttpServletRequest request, HttpServletResponse response, ScrapCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int  companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Scraps";
        menuPath = "Srap >> Scrap Approval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        scrapCommand = command;
        try {
            
             setLocale(request, response);

            // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());
            /* if (!loginBP.checkAuthorisation(userFunctions, "Contract-Details")) {
            path = "content/common/NotAuthorized.jsp";
            } else { */
            int userId = (Integer) session.getAttribute("userId");
            String vendorId=request.getParameter("vendorId");
            String[] approvedId = request.getParameterValues("approveId");
            String[] priceId = request.getParameterValues("priceId");
            String[] itemId = request.getParameterValues("itemId");
            String[] approvedQty = request.getParameterValues("approvedQty");           
            String[] selectedIndex = request.getParameterValues("selectedIndex");           
            
            int index = 0;
            String approver = request.getParameter("approver");           
            String status = request.getParameter("status");           
            
             path = "content/scrap/approveScrapItems.jsp";           
             ScrapTO scrap = null;
             
            for (int i = 0; i < selectedIndex.length; i++) {
                    index = Integer.parseInt(selectedIndex[i]);
                    scrap = new ScrapTO();
                    scrap.setApproveId(approvedId[index]);
                    scrap.setQuandity(approvedQty[index]);                                        
                    scrap.setPriceId(priceId[index]);                                        
                    scrap.setApprovedBy(approver);
                    scrap.setItemId(itemId[index]);
                    
                    scrap.setStatus(status);             
                    if(status.equalsIgnoreCase("APPROVE")){
                    scrapBP.processScrapQty(scrap, userId);
                    }else if(status.equalsIgnoreCase("REJECT")){
                    scrapBP.processRejectScrapReq(scrap, userId);                        
                    }
            }
             request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Items Scrapped successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }  catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Issue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }     
  
  
  public ModelAndView handleReturnScraps(HttpServletRequest request, HttpServletResponse response, ScrapCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        int  companyId = Integer.parseInt((String) session.getAttribute("companyId"));
        String menuPath = "";
        String pageTitle = "Scraps";
        menuPath = "Srap >> Scrap Approval";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        scrapCommand = command;
        try {
            // ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            //////System.out.println("size is  " + userFunctions.size());
            /* if (!loginBP.checkAuthorisation(userFunctions, "Contract-Details")) {
            path = "content/common/NotAuthorized.jsp";
            } else { */
            int userId = (Integer) session.getAttribute("userId");
            String vendorId=request.getParameter("vendorId");
            String[] approvedId = request.getParameterValues("approveId");     
            String[] priceId = request.getParameterValues("priceId");
            String[] approvedQty = request.getParameterValues("approvedQty");  
            String[] itemId = request.getParameterValues("itemId");
            String[] selectedIndex = request.getParameterValues("selectedIndex");           
            
            int index = 0;
            String approver = request.getParameter("approver");           
            String status = request.getParameter("status");           
            
             path = "content/scrap/approveScrapItems.jsp";           
             ScrapTO scrap = null;
             
            for (int i = 0; i < selectedIndex.length; i++) {
                    index = Integer.parseInt(selectedIndex[i]);
                    scrap = new ScrapTO();
                    scrap.setApproveId(approvedId[index]);
                    scrap.setQuandity(approvedQty[index]);     
                    scrap.setPriceId(priceId[index]);   
                    scrap.setApprovedBy(approver);
                    scrap.setItemId(itemId[index]);
                    scrap.setStatus(status);             
                    
                    scrapBP.processdoScrapReturn(scrap, userId);                
            }
             request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Scrap Returned to stock Succesfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }  catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Issue --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }     
    
    
    
    
}
