<%--
    Document   : CompanyWiseProfitability
    Created on : Oct 31, 2013, 11:09:54 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">    
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">


    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

</head>


<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#customerName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerDetails.do",
                    dataType: "json",
                    data: {
                        customerName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                $("#customerName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');
                $('#customerId').val(tmp[0]);
                $('#customerName').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<script>


    function submitPageForTripMerging() {
        var tripId = document.getElementById('tripIdValue').value;
        var temp = tripId.split(",");
        if (document.getElementById('tripTypeValue').value != "" && document.getElementById('tripIdValue').value != "") {
            document.CNoteSearch.action = "/throttle/emptyTripMergingDetails.do";
            document.CNoteSearch.submit();
        } else {
            alert("Please select trip and proceed");
        }
    }



    var cntr = 0;
    var temp = "";
    function setTripId(val) {
        var tripId = document.getElementById('tripId' + val).value;
        var tripType = document.getElementById('tripType' + val).value;
        if (tripType == 'E' && document.getElementById('tripTypeValue').value == '') {
            alert("please select loaded trip first");
            document.getElementById('tripTypeValue').value == '';
            document.getElementById('selectedIndex' + val).checked = false;
            temp ="";
        } else {
            if (temp == '') {
                temp = tripId;
                document.getElementById('selectedIndex' + val).checked = true;
            } else if (temp != '' && document.getElementById('tripTypeValue').value == '') {
                temp = temp + "," + tripId;
                document.getElementById('selectedIndex' + val).checked = true;
            } else if (temp != '' && document.getElementById('tripTypeValue').value != '' && document.getElementById('tripType' + val).value == 'E') {
                if (temp.indexOf(tripId) != -1) {
                    document.getElementById('selectedIndex' + val).checked = false;
//                    alert("check"+temp);
                    var tempNew = temp.split(",");
                    var tempNew1 = "";
                    for(var i=0; i<tempNew.length; i++){
                        if(tripId == tempNew[i]){
                         //tempNew1 = '';   
                        }else{
                          tempNew1 =   tempNew1 +","+tempNew[i];
                        }
                        temp = tempNew1;
                    }
                } else {
                    temp = temp + "," + tripId;
                    document.getElementById('selectedIndex' + val).checked = true;
                }
            } else {
                var x;
                var r = confirm("Click ok for change the trip merging!");
                if (r == true) {
                    document.getElementById('tripTypeValue').value = "";
                    tripId = "";
                    tripType = "";
                    document.getElementById('tripIdValue').value = "";
                    var selected = document.getElementsByName('selectedIndex');
                    temp = "";
                    for (var i = 0; i < selected.length; i++) {
                        document.getElementById('selectedIndex' + i).checked = false;
                    }
                } else {
                    document.getElementById('selectedIndex' + val).checked = false;
                }
            }
        }
        if (tripType == 'L' && document.getElementById('tripTypeValue').value == '' && tripId != '' && tripType !='') {
            document.getElementById('tripTypeValue').value = tripType + "-" + tripId;
        }

//        alert(temp);
        document.getElementById('tripIdValue').value = temp;
    }



    function viewTripDetails(tripId) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }



    function searchPage() {
        document.CNoteSearch.action = "/throttle/emptyTripMerging.do";
        document.CNoteSearch.submit();
    }
</script>

<body>
    <%
                String menuPath = "Operation >> Empty Trip Merging";
                request.setAttribute("menuPath", menuPath);
    %>
    <form name="CNoteSearch" method="post" enctype="multipart">
        <%@ include file="/content/common/path.jsp" %>
        <br>
        <br>

        <table width="900" height="50" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
            <tr id="exp_table" >
                <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                    <div class="tabs" align="left" style="width:850;">
                        <ul class="tabNavigation">
                            <li style="background:#76b3f1;width:850;">Trip Details</li>
                        </ul>
                        <div id="first">
                            <table width="830" cellpadding="0" cellspacing="5" border="0" align="center" class="table4" >
                                <tr>
                                    <td >Customer Name</td>
                                    <td>
                                        <input type="hidden" class="textbox" name="tripTypeValue" id="tripTypeValue" style="width:120px;" value=""/>
                                        <input type="hidden" class="textbox" name="tripIdValue" id="tripIdValue" style="width:120px;" value=""/>
                                        <input type="hidden" class="textbox" name="customerId" id="customerId" style="width:120px;" value="<c:out value="${customerId}"/>"/>
                                        <input type="text" class="textbox" name="customerName" id="customerName" style="height:16px; width:120px;" value="<c:out value="${customerName}"/>"/>
                                    </td>

                                    <td >Trip Code</td>
                                    <td><input type="text" class="textbox" name="tripCode" id="tripCode" style="height:16px; width:120px;" value="<c:out value="${tripCode}"/>"/></td>


                                </tr>
                                <tr>

                                    <td><font color="red"></font>From Date</td>
                                    <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" style="height:16px; width:120px;"  onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>"></td>
                                    <td><font color="red"></font>To Date</td>
                                    <td height="30"><input name="toDate" id="toDate" type="text" style="height:16px; width:120px;" class="datepicker" onclick="ressetDate(this);" value="<c:out value="${toDate}"/>"></td>

                                </tr>
                                <tr>
                                    <td>Vehicle No</td>
                                    <td> <select name="vehicleId" id="vehicleId" class="textbox" style="height:20px; width:122px;"" >
                                            <c:if test="${vehicleList != null}">
                                                <option value="" selected>--Select--</option>
                                                <c:forEach items="${vehicleList}" var="vehicleList">
                                                    <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById("vehicleId").value = '<c:out value="${vehicleId}"/>';
                                        </script>
                                    </td>
                                    <td colspan="2" align="center"><input type="button" class="button"   value="Search" onclick="searchPage()"></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </td>
            </tr>
        </table>
        <br>
        <c:if test = "${emptyTripList == null}" >
            <center><font color="red" style="text-align: right">No Records Found</font></center>
        </c:if>    
        <c:if test = "${emptyTripList != null}" >
            <table width="100%" align="center" border="0" id="table" class="sortable"  style="width:1150px;" >
                <thead>
                    <tr height="40">
                        <th><h3>Sno</h3></th>
                        <th><h3>Consignment No</h3></th>
                        <th><h3>Trip Code</h3></th>
                        <th><h3>Vehicle No</h3></th>
                        <th><h3>Route </h3></th>
                        <th><h3>Trip Start Date</h3> </th>
                        <th><h3>Trip Start Time</h3> </th>
                        <th><h3>Trip End Date</h3></th>
                        <th><h3>Trip End Time</h3></th>
                        <th><h3>Trip Type</h3></th>
                        <th><h3>Status</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <% int index = 0;
                            int sno = 1;
                %>
                <tbody>
                    <c:forEach items="${emptyTripList}" var="cnl">

                        <tr height="30">
                            <td align="left" ><%=sno%></td>

                            <td align="left" > <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentId}"/>');"><c:out value="${cnl.consignmentNote}"/></a></td>
                            <td align="left" >
                                <input type="hidden" name="tripId" id="tripId<%=index%>" value="<c:out value="${cnl.tripId}"/>"/>
                                <a href="#" onclick="viewTripDetails('<c:out value="${cnl.tripId}"/>');"><c:out value="${cnl.tripCode}"/></a>
                            </td>
                            <td align="left" >
                                <a href="#" onclick="viewVehicleDetails('<c:out value="${cnl.vehicleId}"/>')"><c:out value="${cnl.regNo}"/></a>
                            </td>

                            <td align="left" ><c:out value="${cnl.routeInfo}"/></td>
                            <td align="left" ><c:out value="${cnl.tripStartDate}"/></td>
                            <td align="left" ><c:out value="${cnl.tripStartTime}"/></td>
                            <td align="left" ><c:out value="${cnl.tripEndDate}"/></td>
                            <td align="left" ><c:out value="${cnl.tripEndTime}"/></td>
                            <td align="left" >
                                <c:if test="${cnl.tripType == 'E'}">
                                    Empty Trip
                                </c:if>
                                <c:if test="${cnl.tripType == 'L'}">
                                    Loaded Trip
                                </c:if>
                            </td>
                            <td align="left" >
                                <c:out value="${cnl.statusName}"/>
                            </td>
                            <td align="left" >
                                <input type="hidden" name="tripType" id="tripType<%=index%>" value="<c:out value="${cnl.tripType}"/>" />
                                <input type="checkbox" name="selectedIndex" id="selectedIndex<%=index%>" value="<c:out value="${cnl.tripId}"/>" onclick="setTripId('<%=index%>');" />
                            </td>
                        </tr>
                        <%index++;%>
                        <%sno++;%>
                    </c:forEach>
                </tbody>
            </table>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 1);
        </script>
        <center>
            &nbsp;&nbsp;<input type="button" class="button" name="trip"  value="Empty Trip Merging" onclick="submitPageForTripMerging()"/>
        </center>
    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </c:if>
</body>
</html>
