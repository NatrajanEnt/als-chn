
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript" language="javascript">
    function searchPage() {
        if (document.getElementById('fromDate').value == '') {
            alert("please select from date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("please enter the to date");
            document.getElementById('toDate').focus();
        }  else if (document.getElementById('orderStatus').value == '') {
            alert("please select order status");
            document.getElementById('orderStatus').focus();
        } else {
            document.cNote.action = '/throttle/consignmentOrderStatus.do?pageType=<c:out value="${pageType}"/>';
            document.cNote.submit();
        }
    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.Order Status"  text="Order Status"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Order Status"  text="Order Status"/></a></li>
            <li class="active"><c:out value="${menuPath}"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <br/>
                <br/>
                <br/>
                <form name="cNote" method="post">
                    <table class="table table-info mb30 table-hover" style="width:100%">

                        <tr   ><td colSpan="8" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">CONSIGNMENT ORDERS STATUS</td></tr>
                        <tr>
                            <td><font color="red">*</font>From Date</td>
                            <td><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:180px;height:40px;"   value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td><input name="toDate" id="toDate" type="text" class="datepicker" style="width:180px;height:40px;"  value="<c:out value="${toDate}"/>"></td>
                            <td><font color="red">*</font>Status</td>
                            <td>
                                <select name="orderStatus" id="orderStatus" class="form-control" style="width:180px;height:40px;"  >
                                    <option value="1">Pending</option>
                                    <option value="2">Completed</option>
                                </select>
                                <script>
                                    document.getElementById("orderStatus").value=<c:out value="${orderStatus}"/>;
                                </script>
                            </td>
                            
                        </tr>
                    </table>
                    
                    <center>
                            <input type="button" class="btn btn-info"   value="Search" onclick="searchPage('Search')">
                            <!--&emsp;<input type="button"  class="btn btn-info" value="Generate Excel" name="ExcelExport" onClick="searchPage(this.name)">-->
                    </center>
                            <br>
                            
                    <c:if test="${orderList != null}">
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead >
                                <tr>
                                    <th height="30" >S.No</th>
                                    <th height="30" >Cust.Ref.No</th>
                                    <th height="30" >Vehicle Type</th>
                                    <th height="30" >Container Type</th>
                                    <th height="30" >Container No</th>
                                    <th height="30" >Seal No</th>
                                    <th height="30" >Liner Name</th>
                                    <th height="30" >Trip Code</th>
                                    <th height="30" >Vehicle No</th>
                                    <th height="30" >Driver</th>
                                    <th height="30" >Trip Status</th>
                                    <th height="30" >Current Location</th>
                                    <th height="30" >Trip Age(Days)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 0;
                                    int sno = 1;
                                %>
                                <c:forEach items="${orderList}" var="cnoteContainer">
                                    <tr>
                                        <td class="text1" height="30" ><%=sno%></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.orderReferenceNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.vehicleTypeName}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.containerTypeName}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.containerNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.sealNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.linerName}"/></td>
                                        <td class="text1" height="30" >
                                            <c:out value="${cnoteContainer.tripCode}"/>
                                        </td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.vehicleRegNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.driverName}"/><br><c:out value="${cnoteContainer.mobileNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.tripStatus}"/></td>
                                        <td class="text1"  height="30">
                                            <c:if test="${cnoteContainer.gpsLocation == 'NA'}">
                                                NA
                                            </c:if>
                                            <c:if test="${cnoteContainer.gpsLocation != 'NA'}">
                                                <c:out value="${cnoteContainer.gpsLocation}" />
                                            </c:if>
                                        </td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.age}"/></td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </c:if>
                    </table>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>





                </form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>