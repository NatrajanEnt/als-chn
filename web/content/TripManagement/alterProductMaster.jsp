

<%--
    Document   : alterProductMaster
    Created on :
    Author     :Dinesh
--%>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title> CLPL </title>
<link href="/throttle/css/parveen.css" rel="stylesheet"/>

<script language="javascript" src="/throttle/js/validate.js"></script>
</head>
<script>
  function submitPage()
    {

        if(textValidation(document.alter.productCode,'productCode')){
            return;
        }
        if(textValidation(document.alter.productName,'productName')){
            return;
        }
        if(textValidation(document.alter.remarks,'remarks')){
            return;
        }
        document.alter.action='/throttle/saveAlterProductMaster.do';
        document.alter.submit();
}


</script>
<body>
<form name="alter" method="post">
<%@ include file="/content/common/path.jsp" %>

<%@ include file="/content/common/message.jsp" %>



<c:if test="${productList != null}">
<c:forEach items="${productList}" var="PL">
<table align="center" width="500" border="0" cellspacing="0" cellpadding="0" class="border">
 <tr height="30">
  <Td colspan="2" class="contenthead">Edit Product Master</Td>
 </tr>
  <tr height="30">
      <td class="text2"><font color="red">*</font>Product Code</td>
      <td class="text2"><input name="productCode" type="text" class="textbox" value="<c:out value="${PL.productCode}"/>" size="20">
          <input type="hidden" name="productId" value="<c:out value="${PL.productId}"/>"> </td>
  </tr>
  <tr height="30">
    <td class="text1"><font color="red">*</font>Product Name</td>
    <td class="text1"><input name="productName" type="text" class="textbox" value="<c:out value="${PL.productName}"/>" size="20"></td>
  </tr>
   <tr height="30">
    <td class="text2"><font color="red">*</font>Remarks</td>
    <td class="text2"><input name="remarks" type="text" class="textbox" value="<c:out value="${PL.remarks}"/>" size="20"></td>
  </tr>

</table>
</c:forEach>
</c:if>

<br>
<br>
<center><input type="button" class="button" value="Save" onclick="submitPage();" /></center>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>


