<%-- 
    Document   : GRSummaryReportExcel
    Created on : Feb 15, 2016, 2:09:44 PM
    Author     : hp
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<html>
    <head>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
            <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "GRSummaryReport-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
            %>

            <br>
            <br>
            <br>
            <c:if test = "${GRSummaryList != null}" >
                <table border="1"  align="center" width="100%" cellpadding="0" cellspacing="1" >
                    <thead>
                        <tr>
                            <td rowspan="2" class="contenthead">S.No</td>
                            <td rowspan="2" class="contenthead">Booking Office</td>
                            <td rowspan="2" class="contenthead">Empty Pick Up</td>
                            <!--<td rowspan="2" class="contenthead">MFR Name</td>-->
                            <td rowspan="2" class="contenthead">Movement Type</td>
                            <td rowspan="2" class="contenthead">G.R No.</td>
                            <td rowspan="2" class="contenthead">Challan No.</td>
                            <td rowspan="2" class="contenthead">TPT Name</td>
                            <td rowspan="2" class="contenthead">GR Submission Date</td>
                            <td rowspan="2" class="contenthead">Trip Sheet No.</td>
                            <td rowspan="2" class="contenthead">Amount</td>
                            <td rowspan="2" class="contenthead">Diesel Qtty.</td>
                            <td rowspan="2" class="contenthead">Trip Sheet Submit</td>
                            <td rowspan="2" class="contenthead">Date of Issue</td>
                            <td colspan="3" class="contenthead" style="text-align:center">Shipper Detail</td>
                            <td colspan="2" class="contenthead" style="text-align:center">Movement </td>
                            <td colspan="6" class="contenthead" style="text-align:center">Conatiner Details </td>
                            <td colspan="7" class="contenthead" style="text-align:center">Revenue </td>

                        </tr> 
                        <tr>
                            <td class="contenthead">Consignee</td>
                            <td class="contenthead">Consignor </td>
                            <td class="contenthead">Billing Party </td>

                            <td class="contenthead">Origin </td>
                            <td class="contenthead">Destination </td>

                            <td class="contenthead">Conatiner No</td>
                            <td class="contenthead">20' </td>
                            <td class="contenthead">40' </td>
                            <td class="contenthead">Type L/E </td>
                            <td class="contenthead">S.liNe </td>
                            <td class="contenthead">Lorry No. </td>

                            <td class="contenthead">Bill No.</td>
                            <td class="contenthead">Transporation Charges</td>
                            <td class="contenthead">Detention Charges</td>
                            <td class="contenthead">Weightment</td>
                            <td class="contenthead">Other Charges</td>
                            <td class="contenthead">Total</td>

                        </tr>
                    </thead>
                    <% int index = 0,sno = 1;%>
                    <c:forEach items="${GRSummaryList}" var="csList">
                        <tr>
                            <td align="center"><%=sno%></td>
                            <td align="left">DICT(Sonepa    t)</td>
                            <td align="left"><c:out value="${csList.emptyPickup}"/></td>
                            <td align="left"><c:out value="${csList.movementType}"/></td>
                            <td align="left"><c:out value="${csList.grNo}"/></td>
                            <td align="left"><c:out value="${csList.challanNo}"/></td>
                            <td align="left"><c:out value="${csList.transporter}"/></td>
                            <%--<td align="left"><c:out value="${csList.grDate}"/></td>--%>
                            <td align="left"><c:out value=""/></td>
                            <td align="left"><c:out value="${csList.tripCode}"/></td>
                            <td align="left"><c:out value="${csList.tripFuelAmount}"/></td>
                            <td align="left"><c:out value="${csList.liters}"/></td>
                            <td align="left"><c:out value="${csList.grDate}"/></td>
                            <td align="left">

                                <c:set var="currentDate" value="${csList.grDate}" />
     <fmt:parseDate value="${currentDate}" var="parsedCurrentDate" pattern="dd-MM-yyyy HH:mm:ss" type="date"/>  &nbsp;
     <fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${parsedCurrentDate}" type="date"/>
                            </td>
                            <td align="left"><c:out value="${csList.consigneeName}"/></td>
                            <td align="left"><c:out value="${csList.consignorName}"/></td>
                            <td align="left"><c:out value="${csList.billingParty}"/></td>
                            <td align="left">
                                <c:if test = "${csList.movementType == 'Import' || csList.movementType == 'Import DSO' }">
                                    <c:out value="${csList.destination}"/>
                                </c:if>
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Export DSO' || csList.movementType == 'Repo' }">
                                    <c:out value="${csList.origin}"/>
                                </c:if>
                            </td>
                            <td align="left">
                                <c:if test = "${csList.movementType == 'Import' || csList.movementType == 'Import DSO' }">
                                    <c:out value="${csList.origin}"/>
                                </c:if>
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Export DSO' || csList.movementType == 'Repo' }">
                                    <c:out value="${csList.destination}"/>
                                </c:if>
                            </td>
                            <%--<td align="left"><c:out value="${csList.destination}"/></td>--%>
                            <td align="left"><c:out value="${csList.containerNo}"/></td>
                            <td align="left"><c:out value="${csList.twentyFT}"/></td>
                            <td align="left"><c:out value="${csList.fortyFT}"/></td>
                            <%--<td align="left"><c:out value="${csList.containerType}"/></td>--%>
                            <td align="center">
                                <c:if test = "${csList.movementType == 'Export' || csList.movementType == 'Import'|| csList.movementType == 'Export DSO'|| csList.movementType == 'Import DSO' }" >
                                    L
                                </c:if>
                                <c:if test = "${csList.movementType == 'Repo'}">
                                    E
                                </c:if>
                            </td>
                            <td align="left"><c:out value="${csList.linerName}"/></td>
                            <td align="left"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="left"></td>
                            <td align="left"><a href="#" onclick="viewCustomerProfitDetails('<c:out value="${csList.tripId}"/>');"><c:out value="${csList.tripFuelAmount + csList.requestedAdvance}"/></a></td>
                            <td align="left"></td>
                            <td align="left"></td>
                            <td align="left"></td>
                            <td align="left"><c:out value="${csList.tripFuelAmount + csList.requestedAdvance}"/></td>
                            <td align="left"></td>
                        </tr>
                        <%
                   index++;
                     sno++;
                        %>

                    </c:forEach>

                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
