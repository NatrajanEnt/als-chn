package ets.domain.racks.web;

import ets.arch.web.BaseController;
import ets.domain.racks.business.RackBP;
import ets.domain.racks.business.RackTO;
import ets.domain.users.business.LoginBP;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import ets.domain.vehicle.business.VehicleBP;
import java.io.*;
import ets.domain.racks.business.RackTO;
import ets.domain.vehicle.business.VehicleTO;
import ets.domain.vehicle.web.VehicleCommand;

public class RackController extends BaseController {

    RackCommand rackCommand;
    VehicleCommand vehicleCommand;
    RackBP rackBP;
    LoginBP loginBP;
    VehicleBP vehicleBP;

    public VehicleBP getVehicleBP() {
        return vehicleBP;
    }

    public void setVehicleBP(VehicleBP vehicleBP) {
        this.vehicleBP = vehicleBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public RackBP getRackBP() {
        return rackBP;
    }

    public void setRackBP(RackBP rackBP) {
        this.rackBP = rackBP;
    }

    public RackCommand getRackCommand() {
        return rackCommand;
    }

    public void setRackCommand(RackCommand rackCommand) {
        this.rackCommand = rackCommand;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();  initialize(request);

    }

    public ModelAndView handleViewRacks(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        rackCommand = command;
        String menuPath = "";
        String pageTitle = "View Racks";
        request.setAttribute("pageTitle", pageTitle);
        ArrayList rackList = new ArrayList();
        menuPath = "Stores >> Manage Rack ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Rack-View")) {
            path = "content/common/NotAuthorized.jsp";
            } else {
            path = "content/racks/addRacks.jsp";
            rackList = rackBP.processRackList();
            path = "content/racks/manageRacks.jsp";
            request.setAttribute("rackLists", rackList);
        }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {

        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        }catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewRackList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewAddScreen(HttpServletRequest request, HttpServletResponse reponse, RackCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Stores >>Manage Racks  >> Add Rack ";
        String pageTitle = "Add Racks";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            setLocale(request, reponse);
             ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "Rack-Add")) {
            path = "content/common/NotAuthorized.jsp";
            } else {
            path = "content/racks/addRacks.jsp";
        }
        }catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Add Rack --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddRack(HttpServletRequest request, HttpServletResponse reponse, RackCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        ArrayList rackList = new ArrayList();
        rackCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        RackTO rackTO = new RackTO();
        String menuPath = "";
        menuPath = "Stores  >> Manage Rack ";
        String pageTitle = "Manage Rack";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {

            if (rackCommand.getRackName() != null && rackCommand.getRackName() != "") {
                rackTO.setRackName(rackCommand.getRackName());
            }
            if (rackCommand.getRackDescription() != null && rackCommand.getRackDescription() != "") {
                rackTO.setRackDescription(rackCommand.getRackDescription());
            }
            path = "content/racks/manageRacks.jsp";
            status = rackBP.processInsertRack(rackTO, userId);
            rackList = rackBP.processRackList();
            request.setAttribute("rackLists", rackList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New Rack Added Successfully");
            }

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }catch (FPBusinessException exception) {

        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Rack Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewRackAlterScreen(HttpServletRequest request, HttpServletResponse response, RackCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        rackCommand = command;
        String menuPath = "";
        menuPath = "Stores >> Manage Racks  >> Alter";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
             if (!loginBP.checkAuthorisation(userFunctions, "Rack-Alter")) {
            path = "content/common/NotAuthorized.jsp";
            } else {
            String pageTitle = "Alter Rack Details";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList rackList = new ArrayList();
            path = "content/racks/alterRacks.jsp";
            rackList = rackBP.processRackList();
            request.setAttribute("rackLists", rackList);

        }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Alter rackList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateRack(HttpServletRequest request, HttpServletResponse reponse, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        rackCommand = command;
        String path = "";
        try {
            String menuPath = "Stores >>Manage Rack";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            int index = 0;
            int update = 0;
            String[] idList = rackCommand.getRackIdList();
            String[] nameList = rackCommand.getRackNameList();
            String[] descriptionList = rackCommand.getRackDescriptionList();
            String[] statusList = rackCommand.getRackStatusList();
            String[] selectedIndex = rackCommand.getSelectedIndex();
            int userId = (Integer) session.getAttribute("userId");
            ArrayList List = new ArrayList();
            RackTO rackTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                rackTO = new RackTO();
                index = Integer.parseInt(selectedIndex[i]);
                rackTO.setRackId((idList[index]));
                rackTO.setRackName(nameList[index]);
                rackTO.setRackDescription(descriptionList[index]);
                rackTO.setRackStatus(statusList[index]);
                List.add(rackTO);
            }
            path = "content/racks/manageRacks.jsp";
            update = rackBP.processUpdateRack(List, userId);
            String pageTitle = "Manage Customer";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList rackList = new ArrayList();
            rackList = rackBP.processRackList();
            request.setAttribute("rackLists", rackList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Rack Details Modified Successfully");
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewSubRacks(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        ArrayList subRackList = new ArrayList();
        ArrayList rackList = new ArrayList();
        HttpSession session = request.getSession();
        rackCommand = command;
        String menuPath = "";
        String pageTitle = "Manage SubRack";
        menuPath = "Stores  >>Manage SubRack ";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);
             
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "SubRack-View")) {
            path = "content/common/NotAuthorized.jsp";
            } else {
            rackList = rackBP.processRackList();
            request.setAttribute("rackLists", rackList);
            path = "content/racks/addSubRack.jsp";
            subRackList = rackBP.processSubRackList();
            path = "content/racks/manageSubRack.jsp";
            request.setAttribute("subRackLists", subRackList);
        }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }catch (FPBusinessException exception) {

        FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
        request.setAttribute(ParveenErrorConstants.ERROR_KEY,
        exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to viewCustomer --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewAddSubRackScreen(HttpServletRequest request, HttpServletResponse response, RackCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        ArrayList rackList = new ArrayList();
        String menuPath = "";
        menuPath = "Stores  >>Manage SubRack >> Add Sub Rack ";
        String pageTitle = "Add Sub Rack";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);

             ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "SubRack-Add")) {
            path = "content/common/NotAuthorized.jsp";
            } else {
            path = "content/racks/addSubRack.jsp";
            rackList = rackBP.processRackList();
            request.setAttribute("rackLists", rackList);
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Sub Rack Add Screen --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleAddSubRack(HttpServletRequest request, HttpServletResponse response, RackCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        int status = 0;
        String path = "";
        rackCommand = command;
        int userId = (Integer) session.getAttribute("userId");
        ArrayList subRackList = new ArrayList();
        RackTO rackTO = new RackTO();
        String menuPath = "";
        menuPath = "Stores  >>Manage SubRack";
        String pageTitle = "Manage Sub Rack";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);

            if (rackCommand.getRackId() != null && rackCommand.getRackId() != "") {
                rackTO.setRackId(rackCommand.getRackId());
            }
            if (rackCommand.getSubRackName() != null && rackCommand.getSubRackName() != "") {
                rackTO.setSubRackName(rackCommand.getSubRackName());
            }
            if (rackCommand.getSubRackDescription() != null && rackCommand.getSubRackDescription() != "") {
                rackTO.setSubRackDescription(rackCommand.getSubRackDescription());
            }
            path = "content/racks/manageSubRack.jsp";
            status = rackBP.processInsertSubRack(rackTO, userId);
            subRackList = rackBP.processSubRackList();
            request.setAttribute("subRackLists", subRackList);
            if (status > 0) {
                request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "New SubRack Added  Successfully");
            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to insert Rack Details --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleViewSubRackAlterScreen(HttpServletRequest request, HttpServletResponse response, RackCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        rackCommand = command;
        String menuPath = "";
        menuPath = "Stores  >>Manage SubRack  >> Alter";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
             setLocale(request, response);

           ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            if (!loginBP.checkAuthorisation(userFunctions, "SubRack-Alter")) {
            path = "content/common/NotAuthorized.jsp";
            } else {
            String pageTitle = "Alter SubRack";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList subRackList = new ArrayList();
            path = "content/racks/altersubRack.jsp";
            ArrayList rackList = new ArrayList();
            rackList = rackBP.processRackList();
            subRackList = rackBP.processSubRackList();
            request.setAttribute("subRackLists", subRackList);
            request.setAttribute("rackLists", rackList);
        }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view subRackList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUpdateSubRack(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        rackCommand = command;
        String path = "";
        try {
             setLocale(request, response);

            String menuPath = "Stores  >> Manage SubRack";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            int index = 0;
            int update = 0;
            String[] subRackIdList = rackCommand.getSubRackIdList();
            String[] subRackNameList = rackCommand.getSubRackNameList();
            String[] subRackDescriptionList = rackCommand.getSubRackDescriptionList();
            String[] subRackStatusList = rackCommand.getSubRackStatusList();
            String[] rackIdList = rackCommand.getRackIdList();
            String[] selectedIndex = rackCommand.getSelectedIndex();
            int userId = (Integer) session.getAttribute("userId");
            ArrayList List = new ArrayList();
            RackTO rackTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                rackTO = new RackTO();
                index = Integer.parseInt(selectedIndex[i]);
                rackTO.setSubRackId(subRackIdList[index]);
                rackTO.setRackId(rackIdList[index]);
                rackTO.setSubRackName(subRackNameList[index]);
                rackTO.setSubRackDescription(subRackDescriptionList[index]);
                rackTO.setSubRackStatus(subRackStatusList[index]);
                List.add(rackTO);
            }
            path = "content/racks/manageSubRack.jsp";
            update = rackBP.processUpdateSubRack(List, userId);
            String pageTitle = "Manage SubRack";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList subRackList = new ArrayList();

            subRackList = rackBP.processSubRackList();
            request.setAttribute("subRackLists", subRackList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "SubRack Details Modified Successfully");
        //      }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to update data  --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }




    //handlemanageSectionPage
    /**
     * This method used to View MFR Page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handlemanageSectionPage(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";

        String menuPath = "Vehicle  >>  STORES >>  Manage Section";

        try {

            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            String pageTitle = "View Section";
            request.setAttribute("pageTitle", pageTitle);

            ArrayList sectionList = new ArrayList();
            path = "content/section/addSection.jsp";
            sectionList = rackBP.processGetSectionList();

            request.setAttribute("SectionList", sectionList);
            path = "content/section/manageSection.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Section data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to  Add MFR Page  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleaddSectionPage(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        rackCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "Stores  >>  STORES >>  Manage Section";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

        String pageTitle = "Add Section";
        request.setAttribute("pageTitle", pageTitle);
        path = "content/section/addSection.jsp";

        return new ModelAndView(path);
    }
    //handleAddSection

    /**
     * This method used to Insert MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddSection(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        rackCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String menuPath = "Stores  >>  STORES >>  Add Section";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            RackTO rackTO = new RackTO();

            if (rackCommand.getSectionName() != null && rackCommand.getSectionName() != "") {
                rackTO.setSectionName(rackCommand.getSectionName());
            }
            if (rackCommand.getDescription() != null && rackCommand.getDescription() != "") {
                rackTO.setDescription(rackCommand.getDescription());
            }

            String pageTitle = "Add Section";
            request.setAttribute("pageTitle", pageTitle);

            int insertStatus = 0;
            path = "content/section/manageSection.jsp";
            insertStatus = rackBP.processInsertSectionDetails(rackTO, userId);
            request.removeAttribute("SectionList");
            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);

            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Section  Added Successfully");


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
    //handleAlterSectionPage

    /**
     * This method used to View Alter MFR page Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterSectionPage(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        path = "content/section/alterSection.jsp";

        try {
            rackCommand = command;
            String menuPath = "Stores  >>  STORES >>  Alter  Section";
            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            String pageTitle = "Alter  Section";
            request.setAttribute("SectionList", sectionList);
            request.setAttribute("pageTitle", pageTitle);
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    /**
     * This method used to Modify MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterSection(HttpServletRequest request, HttpServletResponse reponse, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        HttpSession session = request.getSession();
        rackCommand = command;
        String path = "";

        try {
            String menuPath = "Stores  >>  STORES >>  Alter  Section";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            int index = 0;
            int modify = 0;
            String[] sectionids = rackCommand.getSectionIds();
            String[] sectionnames = rackCommand.getSectionNames();
            String[] activeStatus = rackCommand.getActiveInds();
            String[] selectedIndex = rackCommand.getSelectedIndex();
            String[] description = rackCommand.getDescriptions();
            int UserId = (Integer) session.getAttribute("userId");

            ArrayList List = new ArrayList();
            RackTO rackTO = null;
            for (int i = 0; i < selectedIndex.length; i++) {
                rackTO = new RackTO();
                index = Integer.parseInt(selectedIndex[i]);
                rackTO.setSectionId(sectionids[index]);
                rackTO.setSectionName(sectionnames[index]);
                rackTO.setActiveInd(activeStatus[index]);
                rackTO.setDescription(description[index]);
                List.add(rackTO);
            }
            String pageTitle = "View Manufacturers";
            path = "content/section/manageSection.jsp";
            request.setAttribute("pageTitle", pageTitle);
            modify = rackBP.processModifySectionDetails(List, UserId);
            request.removeAttribute("SectionList");
            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Manufacturer Details Modified Successfully");
//            }
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve designation --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }



    //handleAddParts
    /**
     * This method used to Insert Parts Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAddParts(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        rackCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String menuPath = "Stores  >>  STORES >>  Add Parts";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            RackTO rackTO = new RackTO();

            if (rackCommand.getSectionId() != null && rackCommand.getSectionId() != "") {
                rackTO.setSectionId(rackCommand.getSectionId());
            }
            if (rackCommand.getMfrId() != null && rackCommand.getMfrId() != "") {
                rackTO.setMfrId(rackCommand.getMfrId());

            }
            if (rackCommand.getModelId() != null && rackCommand.getModelId() != "") {
                rackTO.setModelId(rackCommand.getModelId());

            }
            if (rackCommand.getItemCode() != null && rackCommand.getItemCode() != "") {
                rackTO.setItemCode(rackCommand.getItemCode());
            }
            if (rackCommand.getPaplCode() != null && rackCommand.getPaplCode() != "") {
                rackTO.setPaplCode(rackCommand.getPaplCode());
            }
            if (rackCommand.getItemName() != null && rackCommand.getItemName() != "") {
                rackTO.setItemName(rackCommand.getItemName());
            }
            if (rackCommand.getUomId() != null && rackCommand.getUomId() != "") {
                rackTO.setUomId(rackCommand.getUomId());
            }
            if (rackCommand.getSpecification() != null && rackCommand.getSpecification() != "") {
                rackTO.setSpecification(rackCommand.getSpecification());
            }
            if (rackCommand.getReConditionable() != null && rackCommand.getReConditionable() != "") {
                rackTO.setReConditionable(rackCommand.getReConditionable());
            }
            if (rackCommand.getScrapUomId() != null && rackCommand.getScrapUomId() != "") {
                rackTO.setScrapUomId(rackCommand.getScrapUomId());
            }
            if (rackCommand.getMaxQuandity() != null && rackCommand.getMaxQuandity() != "") {
                rackTO.setMaxQuandity(rackCommand.getMaxQuandity());
            }
            if (rackCommand.getRoLevel() != null && rackCommand.getRoLevel() != "") {
                rackTO.setRoLevel(rackCommand.getRoLevel());
            }
            if (rackCommand.getSubRackId() != null && rackCommand.getSubRackId() != "") {
                rackTO.setSubRackId(rackCommand.getSubRackId());
            }

             String pageTitle = "Add Parts";
             request.setAttribute("pageTitle", pageTitle);
             int insertStatus = 0;

              ArrayList MfrList = new ArrayList();
              MfrList = vehicleBP.processGetMfrList();
              request.setAttribute("MfrList", MfrList);
              ArrayList sectionList = new ArrayList();
              sectionList = rackBP.processGetSectionList();
             request.setAttribute("SectionList", sectionList);
             path = "content/parts/manageParts.jsp";

             insertStatus = rackBP.processInsertPartsDetails(rackTO, userId);
             request.removeAttribute("PartsList");
             ArrayList partsList = new ArrayList();
             partsList = rackBP.processGetSectionList();
             request.setAttribute("PartsList", partsList);
             request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, "Section  Added Successfully");
         } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //processGetModels
    public void handleGetModels(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        rackCommand = command;
        String mfrId = request.getParameter("mfrId");
        String suggestions = "";
        try {
            suggestions = rackBP.processGetModels(Integer.parseInt(mfrId));
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve Model data in Ajax --> " + exception);
        }
    }

    //handleGetSubRack
    public void handleGetSubRack(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        rackCommand = command;
        String rackId = request.getParameter("rackId");
        String suggestions = "";
        try {
            suggestions = rackBP.processGetSubRack(Integer.parseInt(rackId));
            if (rackId != null && rackId != "") {
            }
            PrintWriter writer = response.getWriter();
            response.setContentType("text/xml");
            response.setHeader("Cache-Control", "no-cache");
            writer.print(suggestions);
            writer.close();
        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve  SubRack data in Ajax --> " + exception);
        }
    }


    /**  == 99
     * This method used to  Add MFR Page  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleManagePartsPage(HttpServletRequest request, HttpServletResponse response, RackCommand command) throws FPRuntimeException, FPBusinessException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        rackCommand = command;
        String path = "";
        String menuPath = "Stores  >> Manage Parts >>  Manage";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String pageTitle = "Search Parts";
        request.setAttribute("pageTitle", pageTitle);

   try {
        setLocale(request, response);
        path = "content/parts/manageParts.jsp";
        ArrayList MfrList = new ArrayList();
        MfrList = vehicleBP.processGetMfrList();
        request.setAttribute("MfrList", MfrList);
        ArrayList sectionList = new ArrayList();
        sectionList = rackBP.processGetSectionList();
        request.setAttribute("SectionList", sectionList);
        request.removeAttribute("PartsList");

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }
    //handleSearch

    /**
     * This method caters to search Models
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleSearch(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        String menuPath = "Stores  >>  Manage Parts  >>  view";
        rackCommand = command;
        HttpSession session = request.getSession();
        String pageTitle = "View Item";
        request.setAttribute("pageTitle", pageTitle);
        String mfrId = request.getParameter("mfrId");
        try {
            path = "content/parts/manageParts.jsp";

              ArrayList sectionList = new ArrayList();
             sectionList = rackBP.processGetSectionList();
             request.setAttribute("SectionList", sectionList);


             ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            RackTO rackTO = new RackTO();
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList partsList = new ArrayList();
            String mrfId = rackCommand.getMfrId();
            String modelId = rackCommand.getModelId();
            String sectionId = rackCommand.getSectionId();
            String itemCode = rackCommand.getItemCode();
            String paplCode = rackCommand.getPaplCode();
            String itemName = rackCommand.getItemName();
            String reConditionable = rackCommand.getReConditionable();
            if (rackCommand.getMfrId().equals("0")) {

                rackTO.setMfrId(null);
            } else {

                rackTO.setMfrId(mrfId);
            }
            if (rackCommand.getModelId().equals("0")) {

                rackTO.setModelId(null);
            } else {

                rackTO.setModelId(modelId);
            }
            if (rackCommand.getSectionId() == "") {

                rackTO.setSectionId(null);
            } else {

                rackTO.setSectionId(sectionId);

            }
            if (rackCommand.getItemCode() == "") {

                rackTO.setItemCode(null);
            } else {

                rackTO.setItemCode(itemCode);
            }
            if (rackCommand.getPaplCode() == "") {

                rackTO.setPaplCode(null);
            } else {

                rackTO.setPaplCode(paplCode);
            }
            if (rackCommand.getItemName() == "") {

                rackTO.setItemName(null);
            } else {

                rackTO.setItemName(itemName);
            }
            if (rackCommand.getReConditionable() == "") {

                rackTO.setReConditionable(null);
            } else {

                rackTO.setReConditionable(reConditionable);
            }

             request.setAttribute("sectionId", rackCommand.getSectionId());
             request.setAttribute("itemCode",rackCommand.getItemCode());
             request.setAttribute("paplCode",rackCommand.getPaplCode());
             request.setAttribute("itemName",rackCommand.getItemName());
             request.setAttribute("mfrId", rackCommand.getMfrId());
             request.setAttribute("modelId", rackCommand.getModelId());
             request.setAttribute("reConditionable", rackCommand.getReConditionable());

             partsList = rackBP.getpartsDetails(rackTO);
             request.setAttribute("PartsList", partsList);







        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());

        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to search model data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView((path));
    }



     /**
     * This method caters to alter Model Page
     *
     * @param 	request - Http request object
     *
     * @param 	response - Http response object
     *
     * @param	command - JobCommand object contains the request values.
     *
     * @return ModelAndView
     */
    public ModelAndView handleAlterPartsPage(HttpServletRequest request, HttpServletResponse response, RackCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        rackCommand = command;
        String menuPath = "";
        menuPath = "Stores  >> Parts ";
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
           // String mfrId = request.getParameter("mfrId");
            String suggestions = "";
        try {
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            String pageTitle = "View Parts Details";
            request.setAttribute("pageTitle", pageTitle);
            ArrayList partsDetail = new ArrayList();
            int itemId = Integer.parseInt(request.getParameter("itemId"));
            path = "content/parts/alterParts.jsp";
            //request.removeAttribute("GetPartsDetail");
            ArrayList getPartsDetail = new ArrayList();
            ArrayList MfrList = new ArrayList();
            ArrayList uomList = new ArrayList();
            ArrayList getRackList = new ArrayList();
            ArrayList sectionList = new ArrayList();
            ArrayList ModelList = new ArrayList();

            MfrList = vehicleBP.processGetMfrList();
            sectionList = rackBP.processGetSectionList();
            uomList = rackBP.processGetUomList();
            getRackList = rackBP.processRackList();
            getPartsDetail = rackBP.processGetPartsDetail(itemId);

            request.setAttribute("MfrList", MfrList);
            request.setAttribute("SectionList", sectionList);
            request.setAttribute("UomList", uomList);
            request.setAttribute("getRackList", getRackList);
            request.setAttribute("GetPartsDetail", getPartsDetail);
            String itemCode = rackCommand.getItemCode();
           // request.setAttribute(itemCode, itemCode);

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to display alter page --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    //handleAlterParts

     /**
     * This method used to alter Parts  Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ModelAndView handleAlterParts(HttpServletRequest request, HttpServletResponse response, RackCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/login.jsp");
        }

        rackCommand = command;
        HttpSession session = request.getSession();
        String path = "";
        int userId = (Integer) session.getAttribute("userId");
        try {
            String pageTitle = "Alter Parts";
            request.setAttribute("pageTitle", pageTitle);

            String menuPath = "Stores  >>  STORES >>  alter Parts";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
            ArrayList userFunctions = (ArrayList) session.getAttribute("userFunction");
            RackTO rackTO = new RackTO();

            if (rackCommand.getSectionId() != null && rackCommand.getSectionId() != "") {
                rackTO.setSectionId(rackCommand.getSectionId());

            }
            if (rackCommand.getRackId() != null && rackCommand.getRackId() != "") {
                rackTO.setRackId(rackCommand.getRackId());

            }
            if (rackCommand.getMfrId() != null && rackCommand.getMfrId() != "") {
                rackTO.setMfrId(rackCommand.getMfrId());

            }
            if (rackCommand.getModelId() != null && rackCommand.getModelId() != "") {
                rackTO.setModelId(rackCommand.getModelId());

            }
              if (rackCommand.getMfrCode() != null && rackCommand.getMfrCode() != "") {
                rackTO.setMfrCode(rackCommand.getMfrCode());

            }
            if (rackCommand.getPaplCode() != null && rackCommand.getPaplCode() != "") {
                rackTO.setPaplCode(rackCommand.getPaplCode());

            }
            if (rackCommand.getItemName() != null && rackCommand.getItemName() != "") {
                rackTO.setItemName(rackCommand.getItemName());

            }
            if (rackCommand.getUomId() != null && rackCommand.getUomId() != "") {
                rackTO.setUomId(rackCommand.getUomId());

            }
            if (rackCommand.getSpecification() != null && rackCommand.getSpecification() != "") {
                rackTO.setSpecification(rackCommand.getSpecification());

            }
            if (rackCommand.getReConditionable() != null && rackCommand.getReConditionable() != "") {
                rackTO.setReConditionable(rackCommand.getReConditionable());

            }
            if (rackCommand.getScrapUomId() != null && rackCommand.getScrapUomId() != "") {
                rackTO.setScrapUomId(rackCommand.getScrapUomId());

            }
            if (rackCommand.getMaxQuandity() != null && rackCommand.getMaxQuandity() != "") {
                rackTO.setMaxQuandity(rackCommand.getMaxQuandity());

            }
            if (rackCommand.getRoLevel() != null && rackCommand.getRoLevel() != "") {
                rackTO.setRoLevel(rackCommand.getRoLevel());

            }
            if (rackCommand.getSubRackId() != null && rackCommand.getSubRackId() != "") {
                rackTO.setSubRackId(rackCommand.getSubRackId());

            }
            if (rackCommand.getItemId() != null && rackCommand.getItemId() != "") {
                rackTO.setItemId(rackCommand.getItemId());

            }

            ArrayList MfrList = new ArrayList();
            MfrList = vehicleBP.processGetMfrList();
            request.setAttribute("MfrList", MfrList);
            ArrayList sectionList = new ArrayList();
            sectionList = rackBP.processGetSectionList();
            request.setAttribute("SectionList", sectionList);
            path = "content/parts/manageParts.jsp";
            int insertStatus = 0;
            insertStatus = rackBP.processUpdatePartsDetails(rackTO, userId);
            //request.removeAttribute("PartsList");
            request.setAttribute(ParveenErrorConstants.MESSAGE_KEY, " Parts Altered Successfully");


        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (FPBusinessException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Business exception --> " + exception.getErrorDetails());
            request.setAttribute(ParveenErrorConstants.ERROR_KEY,
                    exception.getErrorMessage());
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to retrieve acad data --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }
}






