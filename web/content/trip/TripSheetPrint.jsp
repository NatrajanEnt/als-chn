<%-- 
    Document   : dataTable
    Created on : Mar 2, 2016, Mar 2, 2016 12:12:56 PM
    Author     : Nivan
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <%@ page import="ets.domain.trip.business.PointTO" %>
        <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&callback=initMap"
        async defer></script>
        <script type="text/javascript">
            function printPage(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
            function goBack() {
                document.tripSheetPrint.action = '/throttle/handleWayBillPaid.do';
                document.tripSheetPrint.submit();

            }

        </script>

        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <style type="text/css">
            #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
        </style>
        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map {
                height: 100%;
                float: left;
                width: 63%;
                height: 100%;
            }
            #right-panel {
                float: right;
                width: 34%;
                height: 100%;
            }
            #right-panel {
                font-family: 'Roboto','sans-serif';
                line-height: 30px;
                padding-left: 10px;
            }

            #right-panel select, #right-panel input {
                font-size: 15px;
            }

            #right-panel select {
                width: 100%;
            }

            #right-panel i {
                font-size: 12px;
            }

            .panel {
                height: 100%;
                overflow: auto;
            }
        </style>

    </head>
    <body onload="initMap()">

        <form name="tripSheetPrint">
            <% String tripCode = "";%>
            <input type="hidden" name="wayBillMode" id="wayBillMode" value="4"/>
            <input type="hidden" class="textbox" name="fromLocation" id="fromLocation" value="<%= session.getAttribute("branchId")%>"/>

            <div id="printDiv" >

                <table style="border:#000000 solid 1px; border-bottom:none;border-right:solid 1px;border-top:solid 1px;border-left:solid 1px;width: 805px;height:75px; " align="center"  cellpadding="0"  cellspacing="0" >
                    <div></div>
                    <tr>
                        <td>
                             <img src="images/tahoos_logo.png" alt="tahoos" style="height:100px;width:190px;margin-left: 0px;"/>
                        </td>
                        <td colspan="15" style="height:100px;width:600px;font-weight:bold;text-align: left;border-bottom:solid 1px;padding-left:30px;">
<!--                            <div style="padding:0px;margin:0px;width:100px;height:50px;float:left;display:block;text-indent:-9999999%;background:url(/throttle/images/tahoos_logo.png) no-repeat 0 0;height:50px;"></div>-->
                             <font size="4"> Tahoos Logistics and Petrolium Service </font><br/>
                             <center>  <font size="2">  Dammam,Kingdom of Saudi Arabia.</font> </center><br/></td>
                            <!--&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Customer Care: xxxxxxxxx</td>-->

                        <td colspan="4" style="border:#000000 solid 1px;border-bottom:solid 1px; border-top:none;border-left:none;border-right:none;text-align: right">
                            <div id="externalbox" style="width:3in">
                                <div id="inputdata" ></div>
                            </div>
                        </td>    
                    </tr>
                </table>
                <c:set var="tripCodeValue" value="" />

                <table style="border:#000000 solid 1px; border-bottom:solid 1px;border-right:solid 1px;border-top:none;border-left:solid 1px;width: 805px;height:120px; " align="center"  cellpadding="0"  cellspacing="0" >
                    <c:if test = "${tripDetails != null}" >
                        <c:forEach items="${tripDetails}" var="trip">
                            <c:set var="tripCodeValue" value="${trip.tripCode}" />
                            <%
                                tripCode = (String) pageContext.getAttribute("tripCodeValue");
                                //TS/15-16/121 
                                tripCode = tripCode.substring(tripCode.indexOf("/", 3) + 1, tripCode.length());
                            %>                                

                            <tr>
                                <td colspan="12" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    &emsp;&emsp;Trip Sheet No : <c:out value="${trip.tripCode}"/>
                                </td>
                                <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    Trip Sheet Date : <c:out value="${trip.plannedPickupDate}"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    &emsp;&emsp;Vehicle No : <c:out value="${trip.regNo}" />
                                </td>
                                <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    Vehicle Make/Model : <c:out value="${trip.vehicleTypeName}"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    &emsp;&emsp;Driver Name : <c:out value="${trip.empName}"/>
                                </td>
                                <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    Driver Number : <c:out value="${trip.mobileNo}"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    &emsp;&emsp;Vehicle Owner :Tahoos
                                </td>
                                <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                 Destination : <c:out value="${trip.routeInfo}"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    &emsp;&emsp;Customer Name : <c:out value="${trip.customerName}"/>
                                </td>
                                <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                 Department :  <c:out value="${trip.dept}"/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="12" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                    &emsp;&emsp;Authorise Person : <c:out value="${trip.contactPerson}"/>
                                </td>
                                <td colspan="6" style="height:50px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:15px;font-weight:bold;">
                                 Authorised Signature : 
                                </td>
                            </tr>
                        </c:forEach>
                    </c:if>    
                </table>
                
       <%--         <table style="border:#000000 solid 1px; border-bottom:solid 1px;border-right:solid 1px;border-top:none;border-left:solid 1px;width: 650px;height:60px; " align="center"  cellpadding="0"  cellspacing="0" >
                    <c:if test = "${tripDetails != null}" >
                        <c:forEach items="${tripDetails}" var="trip">
                            <tr>
                                <td class="text1" width="150">Estimated KM :</td>
                                <td class="text1" width="60" > <c:out value="${trip.estimatedKM}" />&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="text2" width="150">Estimated Transit Duration (Hours) :</td>
                                <td class="text2" width="120"> <c:out value="${trip.estimatedTransitHours}" />&nbsp;</td>
                            </tr>

                        </c:forEach>
                    </c:if>
</table>  --%>
                
                <table style="border:#000000 solid 1px; border-bottom:solid 1px;border-right:solid 1px;border-top:none;border-left:solid 1px;width: 805px;height:100px; " align="center"  cellpadding="0"  cellspacing="0" >
                    <tr rowspan="2">
                        <td colspan="6" style="height:40px;width:100px;border:#000000 solid 1px;border-bottom:none; border-top:none;border-left:none;border-right:none;font-size:12px;font-weight:bold;">
                            &emsp;&emsp;Notes :
                        </td>
                    </tr>
                </table>
       <table style="border:#000000 solid 1px; border-bottom:solid 1px;border-right:solid 1px;border-top:none;border-left:solid 1px;width: 805px;height:100px; " align="center"  cellpadding="0"  cellspacing="0">
                    <tr>
                        <td  style="height:40px;width:100px;border:#000000 solid 1px; border-top: none;font-size:12px;">
                            
                            <b>   &emsp;&emsp;Time Out </b>
                           
                        </td>
                        <td style="height:40px;width:100px;border:#000000 solid 1px; border-top: none;font-size:12px;"><b> &emsp;&emsp; Released Time</b></td>
                    </tr>
                    <tr>
                        <td style="height:40px;width:100px;border:#000000 solid 1px;border-top: none;border-bottom: none;font-size:12px;"></td>
                        <td style="height:40px;width:100px;border:#000000 solid 1px;border-top: none;border-bottom: none;font-size:12px;"></td>
                    </tr>
                </table>
              
            </div>
            <br>
            <center>
                <input type="button" class="button" name="ok" value="Print" onClick="printPage('printDiv');" > &nbsp;&nbsp;&nbsp;
                <!--<input type="button" class="button" name="back" value="Back" onClick="//goBack();" > &nbsp;-->
            </center>
            <br>
            <br>
            <script type="text/javascript">
                /* <![CDATA[ */
                function get_object(id) {
                    var object = null;
                    if (document.layers) {
                        object = document.layers[id];
                    } else if (document.all) {
                        object = document.all[id];
                    } else if (document.getElementById) {
                        object = document.getElementById(id);
                    }
                    return object;
                }
                //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
                get_object("inputdata").innerHTML = DrawCode39Barcode('<%=tripCode%>', 0);
                /* ]]> */
            </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>