<%-- 
    Document   : alterVehicleFC
    Created on : Sep 24, 2012, 11:35:22 AM
    Author     : entitle
--%>

<%@page contentType="text/html" import="java.sql.*,java.text.DecimalFormat" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <head>
        <title>Vehicle Insurance Update</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>

        <script language="javascript">

            function show_src() {
                document.getElementById('exp_table').style.display='none';
            }
            function show_exp() {
                document.getElementById('exp_table').style.display='block';
            }
            function show_close() {
                document.getElementById('exp_table').style.display='none';
            }



            function validate(){
                var errMsg = "";
                if(document.VehicleInsurance.regNo.value){
                    errMsg = errMsg+"Vehicle Registration No is not filled\n";
                }
                if(document.VehicleInsurance.insuranceCompanyName.value){
                    errMsg = errMsg+"Insurance Company is not filled\n";
                }
                if(document.VehicleInsurance.premiumNo.value){
                    errMsg = errMsg+"Premium No is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidAmount.value){
                    errMsg = errMsg+"Premium paid Amount is not filled\n";
                }
                if(document.VehicleInsurance.premiumPaidDate.value){
                    errMsg = errMsg+"Premium paid Date is not filled\n";
                }
                if(document.VehicleInsurance.vehicleValue.value){
                    errMsg = errMsg+"Vehicle Value is not filled\n";
                }
                if(document.VehicleInsurance.premiumExpiryDate.value){
                    errMsg = errMsg+"Premium expiry Date is not filled\n";
                }
                if(errMsg != "") {
                    alert(errMsg);
                    return false;
                }else {
                    return true;
                }
            }


            var httpRequest;
            function getVehicleDetails(regNo)
            {

                if(regNo != "") {
                    var url = "/throttle/getVehicleDetailsInsurance.do?regNo1="+ regNo;
                    url = url+"&sino="+Math.random();
                    if (window.ActiveXObject)  {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest)  {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() { processRequest(); } ;
                    httpRequest.send(null);
                }
            }


            function processRequest()
            {
                if (httpRequest.readyState == 4)  {

                    if(httpRequest.status == 200) {
                        if(httpRequest.responseText.valueOf()!=""){
                            var detail = httpRequest.responseText.valueOf();
                            var vehicleValues = detail.split("~");
                            document.VehicleInsurance.vehicleId.value = vehicleValues[0]
                            document.VehicleInsurance.chassisNo.value = vehicleValues[2]
                            document.VehicleInsurance.engineNo.value = vehicleValues[2]
                            document.VehicleInsurance.vehicleMake.value = vehicleValues[4]
                            document.VehicleInsurance.vehicleModel.value = vehicleValues[5]

                        }
                    }
                    else
                    {
                        alert("Error loading page\n"+ httpRequest.status +":"+ httpRequest.statusText);
                    }
                }
            }
        </script>

    </head>

    <body onLoad="setImages(1,0,0,0,0,0);">
        <form name="VehicleInsurance" action="/throttle/alterVehicleFC.do">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <h2 align="center">Vehicle FC Details</h2>
            <table width="800" align="center" class="table2" cellpadding="0" cellspacing="0">

                <tr>
                    <td class="contenthead" colspan="4">Vehicle Details</td>
                </tr>
                <c:if test="${vehicleFC != null}">
                    <c:forEach items="${vehicleFC}" var="FCV" >
                        <tr>
                            <td class="texttitle1">Vehicle No</td>
                            <td class="text1"><input type="text" name="regNo" id="regNo" class="form-control" value='<c:out value="${FCV.regno}" />' ><input type="hidden" name="vehicleId" id="vehicleId" value='<c:out value="${FCV.vehicleId}" />' /></td>
                            <td class="texttitle1">Make</td>
                            <td class="text1"><input type="text" name="vehicleMake" id="vehicleMake" class="form-control" value='<c:out value="${FCV.mfrName}" />'></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Model</td>
                            <td class="text2"><input type="text" name="vehicleModel" id="vehicleModel" class="form-control" value='<c:out value="${FCV.modelName}" />' ></td>
                            <td class="texttitle2">Usage</td>
                            <td class="text2"><input type="text" name="vehicleUsage" id="vehicleUsage" class="form-control"  ></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">Engine No</td>
                            <td class="text1"><input type="text" name="engineNo" id="engineNo" class="form-control" value='<c:out value="${FCV.engineNo}" />' ></td>
                            <td class="texttitle1">Chassis No</td>
                            <td class="text1"><input type="text" name="chassisNo" id="chassisNo" class="form-control" value='<c:out value="${FCV.chassisNo}" />' ></td>
                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td class="contenthead" colspan="4">FC Details</td>
                </tr>
                <c:if test="${vehicleFCDetail != null}">
                    <c:forEach items="${vehicleFCDetail}" var="FCde" >
                        <tr>
                            <td class="texttitle1">RTO Details</td>
                            <td class="text1"><input type="text" name="rtoDetail" id="rtoDetail" class="form-control" value='<c:out value="${FCde.rtodetail}" />' ><input type="hidden" name="fcid" id="fcid" value='<c:out value="${FCde.fcid}" />' /> </td>
                            <td class="texttitle1">Fc Date</td>
                            <td class="text1"><input type="text" name="fcDate" id="fcDate" class="datepicker" value='<c:out value="${FCde.fcdate}" />' ></td>
                        </tr>
                        <tr>
                            <td class="texttitle2">Receipt No</td>
                            <td class="text2"><input type="text" name="fcReceiptNo" id="fcReceiptNo" class="form-control" value='<c:out value="${FCde.receiptno}" />' ></td>
                            <td class="texttitle2">FC Amount</td>
                            <td class="text2"><input type="text" name="fcAmount" id="fcAmount" class="form-control" value='<c:out value="${FCde.fcamount}" />' ></td>
                        </tr>
                        <tr>
                            <td class="texttitle1">FC Expiry Date</td>
                            <td class="text1"><input type="text" name="fcExpiryDate" id="fcExpiryDate" class="datepicker" value='<c:out value="${FCde.fcexpirydate}" />' > </td>
                            <td class="texttitle1">Remarks</td>
                            <td class="text1"><input type="text" name="fcRemarks" id="fcRemarks" class="form-control" value='<c:out value="${FCde.remarks}" />' ></td>

                        </tr>
                    </c:forEach>
                </c:if>
                <tr>
                    <td align="center" class="texttitle1" colspan="4"></td>
                </tr>
            </table>
            <br>
            <center>
                Upload Copy Of Bills&emsp;<input type="file" />
                <br>
                <br>
                <input type="submit" class="button" value=" Update " />
            </center>
    </body>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</html>


