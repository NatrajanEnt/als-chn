<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

<style type="text/css">
    option.red {font-color: red;}
    option.green {font-color: greenyellow;}
</style>
<script>
    $('.btnNext').click(function () {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });
    $('.btnPrevious').click(function () {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
</script>

<script>
    // Use the .autocomplete() method to compile the list based on input from user
    $(document).ready(function () {
        $('#consignorName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getConsignorDetails.do",
//                            url: "/throttle/getCustomerDetailsWithoutContract.do",
                    dataType: "json",
                    data: {
                        customerName: request.term,
                        customerCode: 0,
                        customerId: document.getElementById('customerId').value
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#consignorName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');

                $('#consignorId').val(tmp[0]);
                $('#consignorName').val(tmp[1]);

                return false;


            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);


        };


    });
    // Use the .autocomplete() method to compile the list based on input from user
    $(document).ready(function () {
        $('#consigneeName').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getConsigneeDetails.do",
//                            url: "/throttle/getCustomerDetailsWithoutContract.do",
                    dataType: "json",
                    data: {
                        customerName: request.term,
                        customerCode: 0,
                        customerId: document.getElementById('customerId').value
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#consigneeName").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('~');

                $('#consigneeId').val(tmp[0]);
                $('#consigneeName').val(tmp[1]);

                return false;


            }
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);


        };


    });

    function checkContainerNoInTripExists(val,sno) {
//    alert("jdsa"+sno);
        var containerNumber = val;
        var containerNo =  $("#containerNo"+sno).val();
//         if (containerNo.length != 11 || containerNo == "") {
//            alert("Please Enter Valid Container No");
//            $("#containerNo"+sno).val("");
//            document.getElementById("selectedIndex" + sno).checked = false;
//            $("#containerNo"+sno).focus();
//         }else 
         if ((containerNumber == 11) || (containerNumber != '' && containerNumber != null && containerNumber != "NA")) {
            $.ajax({
                url: "/throttle/checkContainerNoInTripExists.do",
                dataType: "json",
                data: {
                    containerNumber: containerNumber
                },
                success: function (data, textStatus, jqXHR) {
                    var count = data.ContainerCount;
//                    alert("count:::"+count);
                    if (count == 0) {
                        $("#containerStatus").text("");
                        $("#Save").show();
                    } else {
                        $("#containerStatus").text("Container Already In Trip");
                        $("#Save").hide();
                    }
                },
                error: function (data, type) {
                    console.log(type);
                }
            });
        }

    }
</script>

<script>
    function autoSetTransporter() {
        var pageType = document.getElementById("pageType").value;
        var advancerequestamt = document.getElementById("advancerequestamt").value;
//        alert("advancerequestamt="+advancerequestamt);
//        alert("pageType="+pageType);
        if (pageType == 2 && advancerequestamt == 0) {
            document.getElementById("vendor").value = "1~0";
            setVehicleType();
            resetAll();
            resetCopobox();
            setRCMForVehicleType('1');
        }
    }
//     $("#vehicleType").select2({
    $("#vehicleNo").select2({
        placeholder: '--select--'
    });
    function resetCopobox() {
        //alert("2222")

        $('#vehicleNo').val(0);
//                    $('#vehicleNo').select2('val', '0');
//                    $('#vehicleNo').html('');
        //alert("3333")
    }
</script>

<script type="text/javascript">
    function calBalance() {
        autoSetTransporter();
        var hire = document.getElementById("hire").value;
        var advancerequestamt = document.getElementById("advancerequestamt").value;
        document.getElementById("balance").value = hire - advancerequestamt;
    }
</script>
<script type="text/javascript">


    var containerTypeIdSelected = 0;
    var noOfContainerSelected = 0;

    function setDriverId() {
        var value = document.trip.driverName.value;
        $('#driver1Id').val(value);

    }
    function setDriverDetails() {
        if ('<%=request.getAttribute("vehicleNo")%>' != null) {
            computeVehicleCapUtilNew();
        }
    }


    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });


</script>
<script>
    var companyId = '<%=request.getAttribute("companyId")%>';

    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user

        $('#driver1Name').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getDriverNames.do",
                    dataType: "json",
                    data: {
                        driverName: (request.term).trim(),
                        companyId: companyId,
                        textBox: 1
                    },
                    success: function (data, textStatus, jqXHR) {
//                                alert("data"+data); Muthu
//                        var items = data;
//                        response(items);
                        var items = data;
                        var primaryDriver = $('#primaryDriver').val();
//                        if (items == '' && primaryDriver != '') {
//                            alert("Invalid Primary Driver Name");
//                            $('#driver1Name').val('');
//                            $('#driver1Name').val('');
//                            $('#driver1Name').focus();
//                        } else {
//                        }
                        response(items);
                    },
                    error: function (data, type) {
                        //console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var id = ui.item.Id;
                var name = ui.item.Name;
                var mobile = ui.item.Mobile;
                var licenseNo = ui.item.License;
                var ExpiryDate = ui.item.ExpiryDate;
//                alert("value" + id);

                $('#driver1Id').val(id);
                $('#driver1Name').val(name);
                $('#mobileNo').val(mobile);
                $('#licenseNo').val(licenseNo);
//                alert("ddddddd")
                $('#licenseDate').val(ExpiryDate);
                return false;
            }
            // Format the list menu output of the autocomplete
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
//            alert("item" + item);
            var dlNo = item.License;
            if (dlNo == '') {
                dlNo = '-';
            }
            var itemVal = item.Name + '(DL:' + dlNo + ')';
//            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    //.append( "<a>"+ item.Name + "</a>" )
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    //end ajax for vehicle Nos
</script>
<script>
    var companyId = '<%=request.getAttribute("companyId")%>';
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#driver2Name').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getDriverNames.do",
                    dataType: "json",
                    data: {
                        driverName: (request.term).trim(),
                        companyId: companyId,
                        textBox: 1
                    },
                    success: function (data, textStatus, jqXHR) {
//                                alert("data"+data); Muthu
//                        var items = data;
//                        response(items);
                        var items = data;
                        var primaryDriver = $('#primaryDriver').val();
//                        if (items == '' && primaryDriver != '') {
//                            alert("Invalid Primary Driver Name");
//                            $('#driver1Name').val('');
//                            $('#driver1Name').val('');
//                            $('#driver1Name').focus();
//                        } else {
//                        }
                        response(items);
                    },
                    error: function (data, type) {
                        //console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var id = ui.item.Id;
                var name = ui.item.Name;
                var mobile = ui.item.Mobile;
                var licenseNo = ui.item.License;
                var ExpiryDate = ui.item.ExpiryDate;
//                alert("value" + id);

                $('#driver2Id').val(id);
                $('#driver2Name').val(name);
                return false;
            }
            // Format the list menu output of the autocomplete
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
//            alert("item" + item);
            var dlNo = item.License;
            if (dlNo == '') {
                dlNo = '-';
            }
            var itemVal = item.Name + '(DL:' + dlNo + ')';
//            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    //.append( "<a>"+ item.Name + "</a>" )
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    //end ajax for vehicle Nos
</script>

<script type="text/javascript">

    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });



    var httpReq;
    var temp = "";



    function setButtons() {
        var temp = document.trip.actionName.value;
        if (temp != '0') {
            if (temp == '1') {
                $("#actionDiv").hide();
                $("#tripDiv").show();
//                        $("#preStartDiv").show();
            }
        } else {
//                    $("#preStartDiv").hide();
            var vehicleId = document.trip.vehicleId.value;
            if (vehicleId != '' && vehicleId != '0') {
                $("#actionDiv").hide();
                $("#tripDiv").show();
            } else {
                $("#actionDiv").show();
                $("#tripDiv").show();
            }


        }
    }
</script>

<script  type="text/javascript" src="js/jq-ac-script.js"></script>


<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>


<script >



    function print(val) {

        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();

    }

    //start ajax for vehicle Nos
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getVehicleRegNos.do",
                    dataType: "json",
                    data: {
                        vehicleNo: (request.term).trim(),
                        textBox: 1
                    },
                    success: function (data, textStatus, jqXHR) {
//                                alert(data);
                        var items = data;
                        response(items);
                    },
                    error: function (data, type) {
                        //console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
//                alert("another---"+value);
                var tmp = value.split('-');

                $('#vehicleId').val(tmp[0]);
                //alert("1");
                $('#vehicleNo').val(tmp[1]);
                //alert("2");
                $('#vehicleTonnage').val(tmp[2]);
                //alert("3");
                // $('#driver1Id').val(tmp[3]);
                //alert("4"+tmp[3]);
                $('#driver1Name').val(tmp[4]);
                //alert("5"+tmp[4]);
                $('#driver2Id').val(tmp[5]);
                //alert("6"+tmp[5]);
                $('#driver2Name').val(tmp[6]);
                //alert("7");
                $('#driver3Id').val(tmp[7]);
                //alert("8"+tmp[7]);pl
                $('#driver3Name').val(tmp[8]);
//                alert("all="+tmp[13]+tmp[14]+tmp[15])
                $('#mobileNo').val(tmp[13]);
                $('#licenseDate').val(tmp[14]);
                $('#licenseNo').val(tmp[15]);

                //<option value="Cancel">Cancel Order</option>
                //<option value="Suggest Schedule Change">Suggest Schedule Change</option>
                //<option value="Hold Order for further Processing">Hold Order for further Processing</option>
//                        $('#actionName').append(
//                            $('<option></option>').val(0).html('-select-')
//                            )
//                        $('#actionName').append(
//                            $('<option></option>').val(1).html('Freeze')
//                            )
                $("#actionDiv").hide();

                //
                //alert("9");
                //$itemrow.find('#vehicleId').val(tmp[0]);
                //$itemrow.find('#vehicleNo').val(tmp[1]);
                return false;
            }
            // Format the list menu output of the autocomplete
        }).data("ui-autocomplete")._renderItem = function (ul, item) {
            //alert(item);
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    //.append( "<a>"+ item.Name + "</a>" )
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
    //end ajax for vehicle Nos


    function submitPage() {
        $("#Save").hide();
        var statusCheck = true;
        var temp = document.trip.actionName.value;
//        alert("temp:"+temp);
        if (temp != '1') {

            alert("please frezze the trip");
            $("#Save").show();
            return;
        }
        if (statusCheck) {
            $("#Save").hide();
            var temp = "";
            var temp1 = "";
            var repo = "";
            var cntr = 0;
            var temp2 = "";
            var temp3 = "";
            var temp4 = "";
            var temp5 = "";
            // repo container start
            var reposelectedIndex = document.getElementsByName('repoSelectedIndex');
            var repocontainerTypeId = document.getElementsByName('repoContainerTypeId');
            var repocontainerNo = document.getElementsByName('repoContainerNo');
            var repoUniqueId = document.getElementsByName('repoUniqueId');
            var repoSealNo = document.getElementsByName('repoSealNo');
            var repoGrStatus = document.getElementsByName('repoGrStatus');
            if (reposelectedIndex.length > 0) {
                repo = 'Y';
                // alert("i m here?");
                //  alert(reposelectedIndex.length);
                for (var i = 0; i < reposelectedIndex.length; i++) {
                    if (reposelectedIndex[i].checked == true) {
                        if (cntr == 0) {
                            temp = repocontainerTypeId[i].value;
                            temp1 = repocontainerNo[i].value;
                            temp2 = repoUniqueId[i].value;
                            temp3 = repoSealNo[i].value;
                            temp4 = repoGrStatus[i].value;
                            //  alert("temp1:"+temp);
                            //  alert("temp11:"+temp1);
                        } else {
                            temp = temp + "," + repocontainerTypeId[i].value;
                            temp1 = temp1 + "," + repocontainerNo[i].value;
                            temp2 = temp2 + "," + repoUniqueId[i].value;
                            temp3 = temp3 + "," + repoSealNo[i].value;
                            temp4 = temp4 + "," + repoGrStatus[i].value;
                            // alert("temp12:"+temp);
                            //  alert("temp122:"+temp1);
                        }
                        cntr++;
                    }
                }
            }
            // repo container End

            var selectedIndex = document.getElementsByName('selectedIndex');
            var containerTypeId = document.getElementsByName('containerTypeId');
            var containerNo = document.getElementsByName('containerNo');
            var uniqueId = document.getElementsByName('uniqueId');
            var sealNo = document.getElementsByName('sealNo');
            var grStatus = document.getElementsByName('grStatus');
            var orderId = document.getElementsByName('orderIds');

            if (selectedIndex.length > 0) {
                repo = 'N';
                //     alert("i m here....");
                for (var i = 0; i < selectedIndex.length; i++) {
                    if (selectedIndex[i].checked == true) {
                        if (cntr == 0) {
                            temp = containerTypeId[i].value;
                            temp1 = containerNo[i].value;
                            temp2 = uniqueId[i].value;
                            temp3 = sealNo[i].value;
                            temp4 = grStatus[i].value;
                            temp5 = orderId[i].value;
                            // alert(temp);
                            // alert(temp2);
                        } else {
                            temp = temp + "," + containerTypeId[i].value;
                            temp1 = temp1 + "," + containerNo[i].value;
                            temp2 = temp2 + "," + uniqueId[i].value;
                            temp3 = temp3 + "," + sealNo[i].value;
                            temp4 = temp4 + "," + grStatus[i].value;
                            temp5 = temp5 + "," + orderId[i].value;
                            // alert(temp);
                            //  alert(temp1);
                        }
                        cntr++;
                    }
                }
            }
            var grSelectedIndex = document.getElementsByName('grSelectedIndex');
            var advanceGrNo = document.getElementsByName('advanceGrNo');
            var tempGr = "";

            var vehicleNo = document.getElementById("vehicleNo").value;
//            alert("vehicleNo"+vehicleNo);
            var vendorIdTemp = document.getElementById("vendor").value;
            var vehicleCategory = document.getElementById("vehicleCategory").value;
            var fuelAmount = document.getElementById("fuelAmount1").value;
            var bunkName = document.getElementById("bunkName1").value;
            var temp15 = vendorIdTemp.split("~");
            var vendorId = temp15[0];
            var check = "0";
            var companyId = '<%=request.getAttribute("companyId")%>';

            if (vendorId == 1 || (vendorId != 1 && vehicleCategory == 1)) {
                if (vehicleNo == 0) {
                    check = 1;
                    alert("Please choose the Vehicle Number");

                }

//                if(companyId != '1472'){
//                    if (fuelAmount == 0 && bunkName == '0~0~0~0') {
//                        check = 1;
//                        alert("Please choose the bunk name");
//
//                    }
//                }

            }
            var driverId = document.getElementById("driver1Id").value;
            var cleanerId = document.getElementById("driver2Id").value;
            var driverName = document.getElementById("driver1Name").value;
            var mobileNo = document.getElementById("mobileNo").value;
            var dlNo = document.getElementById("licenseNo").value;
            var hireVehNo = document.getElementById("hireVehicleNo").value;
            var ownership = document.getElementById("ownership").value;
            var contractHireId = document.getElementById("contractHireId").value;
            var hireRate = document.getElementById("hire").value;
            var contractHireRate = document.getElementById("contractHireRate").value;
            if (vendorId == '1') {//driver details is mandatory for own vehicles
                if (driverId == '' || driverId == 0 || driverId == '0') {
                    check = 1;
                    alert("Please enter driver details");
                }
                if (cleanerId == '' || cleanerId == 0 || cleanerId == '0') {
                    check = 1;
                    alert("Please enter cleaner details");
                }
            } else {
                if (contractHireId == 0 && ownership == 3 && hireRate == 0) {
                    check = 1;
                    alert("Hire contract is not available");
                    document.getElementById("contractStatus").innerHTML = "Hire contract is not available";
                }
            }
//            else {
//                if (hireVehNo == 'Hired 001') {
//                    check = 1;
//                    alert("Please enter vehicle number");
//                }
//                if (driverName == '' || driverName == 0 || driverName == '0') {
//                    check = 1;
//                    alert("Please enter driver details");
//                }
//                if (mobileNo == '' || mobileNo == 0 || mobileNo == '0') {
//                    check = 1;
//                    alert("Please enter driver mobile number");
//                }
//                if (dlNo == '' || dlNo == 0 || dlNo == '0') {
//                    check = 1;
//                    alert("Please enter driver License number");
//                }
//            }
            // alert("check,."+check);
            if (check == 0) {
//                $("#Save").show();
                document.trip.action = "/throttle/saveTripSheet.do?containerTypeIdTemp=" + temp + "&containerNoTemp=" + temp1 + "&repo=" + repo + "&uniqueId=" + temp2 + "&sealNo=" + temp3 + "&grStatus=" + temp4 + "&advanceGrNo=" + tempGr + "&orderIdss=" + temp5;
                document.trip.submit();
            } else {
                $("#Save").show();
            }

        }
    }


    function saveAction() {
        if (document.trip.actionName.value != 0) {
            document.trip.action = "/throttle/saveAction.do?";
            document.trip.submit();
        } else {
            alert("please select your action");
            document.trip.actionName.focus();
        }
    }

    function setVehicleValuesNew() {
        if ($('#vehicleId').val() != '' && $('#vehicleTonnage').val() != '' && $('#driver1Id').val() != '' && $('#driver1Name').val() != '' && $('#driver2Id').val() != '' && $('#driver2Name').val() != '' && $('#driver3Id').val() != '' && $('#driver3Name').val() != '') {
            $('#actionName').empty();
            var actionOpt = document.trip.actionName;
            var optionVar = new Option("-select-", '0');
            actionOpt.options[0] = optionVar;
            optionVar = new Option("Freeze", '1');
            actionOpt.options[1] = optionVar;
            $("#actionDiv").hide();
            $("#save").show();
        }
    }



    function computeVehicleCapUtilNew() {
        setVehicleValuesNew();
        var orderWeight = document.trip.totalWeight.value;
        var vehicleCapacity = document.trip.vehicleTonnage.value;
        if (vehicleCapacity > 0) {
            var utilPercent = (orderWeight / vehicleCapacity) * 100;
            //document.trip.vehicleCapUtil.value = utilPercent.toFixed(2);



        } else {
            //document.trip.vehicleCapUtil.value = 0;
        }
        var e = document.getElementById("vehicleCheck");
        e.style.display = 'block';
    }

    function setKmVakues(str, textn, c) {

        var routeDistance = document.getElementById("routeDistance").value;
        // alert("routeDistance"+routeDistance);
        if (routeDistance != "null" && routeDistance != "0") {
            //   alert("i m in if");
            document.getElementById("totalkm").value = routeDistance;

        } else {
            // alert(" i m in else");
            var divId = "distancekm";
            if (c == "fcl") {
                //     alert("dfsd");
                divId = "distancekm1";
            }

            var totalKM = 0;
            var cntr = parseInt(document.getElementById("totalCntr" + c).value);
            //  alert(cntr);
            for (var i = 1; i < cntr; i++) {
                //    alert(i);
                var km = document.getElementById(divId + "" + i).value;
                //  alert(km);
                if (km == null || km == "") {
                    km = 0;
                }
                totalKM = totalKM + parseInt(km);
            }
            //  alert("my total "+totalKM);
            document.getElementById("totalkm").value = totalKM;
            document.getElementById("routeDistance").value = totalKM;
        }
    }
    function hideContainerTab() {
        var movementType = document.getElementById("movementType").value;
        if (movementType == '7') {
            document.getElementById("containers").style.visibility = "hidden";
            document.getElementById("tabDetails").children[3].style.display = "none";
            $("tr.containerQuantitySelect").hide();
            //document.getElementById("sinTwentyCheck").checked = false;
            document.getElementById("sinTwentyContainer").value = 0;
        } else {
            $("tr.containerQuantitySelect").hide();
            //document.getElementById("sinTwentyCheck").checked = false;
            document.getElementById("sinTwentyContainer").value = 0;
        }
    }
    function setContainer() { //alert("setContainer");
        var vehicleTypeName1 = document.getElementById("vehicleTypeName").value;
        var vehicleTypeName2 = vehicleTypeName1.split("-");
        var vehicleTypeName = vehicleTypeName2[0];
        var containerQuantity = document.getElementById("containerQuantity").value;
        var movementType = document.getElementById("movementType").value;
        var selectedIndex = document.getElementsByName("selectedIndex");
        var orderId = document.getElementById("consignmentOrderId").value;
        var orderIds = document.getElementsByName("orderIds");
        var temp = "";
        var selectCheck = 0;
        var multilpleOrderFlag = false;
        if (vehicleTypeName == "1058") {
            document.getElementById("containerQuantity").value = "2";
            containerQuantity = document.getElementById("containerQuantity").value;  //new add
        }
        // alert("containerQuantity:"+containerQuantity +"vehicleTypeName:"+vehicleTypeName);
        if (parseInt(containerQuantity) == 2 && vehicleTypeName == "1059" || parseInt(containerQuantity) == 2 && vehicleTypeName == "1060") {
            document.getElementById("sinTwentyContainer").value = 1;
        } else {
            document.getElementById("sinTwentyContainer").value = 0;
        }
        for (var i = 1; i < parseInt(selectedIndex.length - 1); i++) {
            if (orderIds[i].value != orderIds[(i + 1)].value)
                multilpleOrderFlag = true;
        }//alert("movementType:"+movementType +"multilpleOrderFlag:"+multilpleOrderFlag);
        if (movementType == '7' && parseInt(containerQuantity) != 0) {
            if (multilpleOrderFlag) {
                if (vehicleTypeName == "1059" && parseInt(containerQuantity) == 3 || vehicleTypeName == "1060" && parseInt(containerQuantity) == 3) {
                    temp = orderIds[0].value;
                    for (var i = 0; i < selectedIndex.length; i++) {
                        if (temp == orderIds[i].value && selectCheck == 0) {
                            if (document.getElementById("containerTypeId" + (i + 1)).value == "1") {
                                document.getElementById("selectedIndex" + (i + 1)).checked = true;
                                document.getElementById("sealNo" + (i + 1)).value = "NA";
                                document.getElementById("grStatus" + (i + 1)).value = "To be Billed";
                                selectCheck = selectCheck + 1;
                                temp = orderIds[i].value;
                            }
                        }
                        if (temp != orderIds[i].value && selectCheck == 1) {
                            if (document.getElementById("containerTypeId" + (i + 1)).value == "1") {
                                document.getElementById("selectedIndex" + (i + 1)).checked = true;
                                document.getElementById("sealNo" + (i + 1)).value = "NA";
                                document.getElementById("grStatus" + (i + 1)).value = "To be Billed";
                                selectCheck = selectCheck + 1;
                            }
                        }
                        if (selectCheck == 2) {
                            calculateTripRevenue((i + 1), this, 1, orderId);
                            break;
                        }
                    }
                    if (selectCheck == 1) {
                        alert("20' container is not available in one of the orders")
                    }
                }
            } else {

                if (vehicleTypeName == "1058" && parseInt(containerQuantity) == 2) {
                    for (var i = 0; i < selectedIndex.length; i++) {
                        if (document.getElementById("containerTypeId" + (i + 1)).value == "1") {
                            document.getElementById("selectedIndex" + (i + 1)).checked = true;
                            document.getElementById("sealNo" + (i + 1)).value = "NA";
                            document.getElementById("grStatus" + (i + 1)).value = "To be Billed";
                            calculateTripRevenue((i + 1), this, 1, orderId);
                            break;
                        } else {
                            alert("you cannot accomodate 40Foot Container in 20Foot Trailer");
                            return;
                        }
                    }

                } else if (vehicleTypeName == "1059" || vehicleTypeName == "1060") {
                    var sinTwentyContainer = document.getElementById("sinTwentyContainer").value;
                    for (var i = 0; i < selectedIndex.length; i++) {
                        document.getElementById("selectedIndex" + (i + 1)).checked = false;
                    }
//                    alert(vehicleTypeName)
//                    alert(containerQuantity)
                    var checkk = 0;
                    for (var i = 0; i < selectedIndex.length; i++) {
                        if (document.getElementById("selectedIndex" + (i + 1)).checked) {
                            checkk = 1;
                        }
                    }

                    if (parseInt(containerQuantity) == 1) {
                        for (var i = 0; i < selectedIndex.length; i++) {
                            if (document.getElementById("containerTypeId" + (i + 1)).value == "2") {
                                document.getElementById("selectedIndex" + (i + 1)).checked = true;
                                document.getElementById("sealNo" + (i + 1)).value = "NA";
                                document.getElementById("grStatus" + (i + 1)).value = "To be Billed";
                                calculateTripRevenue((i + 1), this, 1, orderId);
                                break;
                            }
                        }
                    } else if (checkk == 0 && parseInt(containerQuantity) == 3) {
                        for (var i = 0; i < selectedIndex.length; i++) {
                            if (document.getElementById("containerTypeId" + (i + 1)).value == "1" && selectedIndex.length > 1) {
                                document.getElementById("selectedIndex" + (i + 1)).checked = true;
                                document.getElementById("selectedIndex" + (i + 2)).checked = true;
                                document.getElementById("sealNo" + (i + 1)).value = "NA";
                                document.getElementById("grStatus" + (i + 1)).value = "To be Billed";
                                document.getElementById("sealNo" + (i + 2)).value = "NA";
                                document.getElementById("grStatus" + (i + 2)).value = "To be Billed";
                                calculateTripRevenue((i + 1), this, 1, orderId);
                                break;
                            } else {
                                alert("you cannot plan the trip only one 20Foot Container in 40Foot Trailer ");
                                document.getElementById("selectedIndex" + (i + 1)).checked = true;
                                document.getElementById("sealNo" + (i + 1)).value = "NA";
                                document.getElementById("grStatus" + (i + 1)).value = "To be Billed";
                                calculateTripRevenue((i + 1), this, 1, orderId);
                                break;
                            }
                        }
                    } else if (parseInt(containerQuantity) == 2) {
                        for (var i = 0; i < selectedIndex.length; i++) {
                            if (document.getElementById("containerTypeId" + (i + 1)).value == "1") {
                                document.getElementById("selectedIndex" + (i + 1)).checked = true;
                                document.getElementById("sealNo" + (i + 1)).value = "NA";
                                document.getElementById("grStatus" + (i + 1)).value = "To be Billed";
                                calculateTripRevenue((i + 1), this, 1, orderId);
                                break;
                            }
                        }
                    }
                }
            }
        } else {
            calOtherVehTypeRevenue(vehicleTypeName);
        }
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Primary Operation<c:if test = "${pageType == 2 }" >(Own)</c:if><c:if test = "${pageType == 3 }" >(Hire)</c:if></h2>
        <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Trip Expense Report</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body" style="overflow:scroll;">
            <body onload="calBalance();
                        changediv();
                        addRowContainer();
                        addRowTrailer();
                        showRemarks();
                        setDriverDetails();
                        setKmVakues(str, textn, c);
                        setRemainBox();
                        autoSetTransporter();" >

                <form name="trip" method="post">


                    <c:if test = "${vehicleTypeContainerTypeRates != null}" >
                        <c:forEach items="${vehicleTypeContainerTypeRates}" var="vcr">
                            <input type="hidden" name="vehicleTypeIdR" id="vehicleTypeIdR" value='<c:out value="${vcr.vehicleTypeId}" />'>
                            <input type="hidden" name="containerTypeIdR" id="containerTypeIdR" value='<c:out value="${vcr.containerTypeId}" />'>
                            <input type="hidden" name="containerQtyR" id="containerQtyR" value='<c:out value="${vcr.containerQty}" />'>
                            <input type="hidden" name="LoadTypeR" id="loadTypeR" value='<c:out value="${vcr.loadType}" />'>
                            <input type="hidden" name="currRateR" id="currRateR" value='<c:out value="${vcr.currRate}" />'>
                        </c:forEach>
                    </c:if>



                    <% String status="";
                    Date today = new Date();
                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    String startDate = sdf.format(today);
                     Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.DATE, 7);
                       String nextDate = sdf.format(cal.getTime());
                    %>

                    <input type="hidden" name="currentDate" id="currentDate" value='<%=startDate%>'/>
                    <input type="hidden" name="nextDate" id="nextDate" value='<%=nextDate%>'/>
                    <input type="hidden" name="routeDistance" id="routeDistance" value='<%=request.getAttribute("routeDistance")%>'/>
                    <input type="hidden" name="routePoints" id="routePoints" value='<%=request.getAttribute("routePoints")%>'/>

                    <%
                    String vehicleMileageAndTollRate = "";
                    String vehicleMileage = "0";
                    String tollRate = "0";
                    String fuelPrice = "0";
                    String[] temp = null;
                    if(request.getAttribute("vehicleMileageAndTollRate") != null){
                        vehicleMileageAndTollRate = (String)request.getAttribute("vehicleMileageAndTollRate");
                        temp = vehicleMileageAndTollRate.split("-");
                        vehicleMileage = temp[0];
                        tollRate = temp[1];
                        fuelPrice = temp[2];
                    }
                    %>
                    <input type="hidden" name="vehicleMileage" value="<%=vehicleMileage%>" />
                    <input type="hidden" name="tollRate" value="<%=tollRate%>" />
                    <input type="hidden" name="fuelPrice" value="<%=fuelPrice%>" />
                    <input type="hidden" name="vehicleNoEmail" id="vehicleNoEmail" value="" />
                    <input type="hidden" name="estimatedExpense" id="estimatedExpense" value="0" />
                    <input type="hidden" name="ConsignmentOrderIds" id="ConsignmentOrderIds" value='<%=request.getAttribute("ConsignmentOrderIds")%>'/>

                    <c:set var="cNotes" value="" />
                    <c:set var="nettKm" value="0" />
                    <c:set var="routeInfo" value="" />
                    <c:set var="customerName" value="" />
                    <c:set var="customerType" value="" />
                    <c:set var="vehicleType" value="" />
                    <c:set var="billingType" value="" />
                    <c:set var="consginmentRemarks" value="" />
                    <c:set var="tripSchedule" value="" />
                    <c:set var="routeInfo" value="" />
                    <c:set var="routeInfo1" value="" />
                    <c:set var="customerId" value="" />
                    <c:set var="customerId1" value="" />
                    <c:set var="totalPoints" value="" />
                    <c:set var="totalHours" value="" />
                    <c:set var="reeferRequired" value="" />
                    <c:set var="orderRevenue" value="" />
                    <c:set var="orderExpense" value="" />
                    <c:set var="profitMargin" value="" />
                    <c:set var="totalWeight" value="" />
                    <c:set var="productInfo" value="" />
                    <c:set var="vehicleCapUtilValue" value="" />
                    <c:set var="orderType" value="" />
                    <c:set var="creditLimit" value="" />
                    <c:set var="availableLimit" value="" />
                    <c:set var="remainOrderWeight" value="" />
                    <c:set var="containerCapacity" value="" />
                    <c:set var="movementType" value="" />
                    <c:set var="clearanceType" value="" />
                    <% int i=0; %>
                    <c:if test = "${consignmentList != null}" >
                        <c:forEach items="${consignmentList}" var="consignment">


                            <input type="hidden" name="consOriginId" id="consOriginId<%=i%>" value='<c:out value="${consignment.originId}" />'>
                            <input type="hidden" name="orderTripType" id="orderTripType" value='<c:out value="${consignment.orderTripType}" />'>
                            <input type="hidden" name="consDestinationId" id="consDestinationId<%=i%>" value='<c:out value="${consignment.destinationId}" />'>
                            <% if (i == 0){%>


                            <c:set var="clearanceType" value="${consignment.clearanceType}" />
                            <c:set var="customerId" value="${consignment.customerId}" />
                            <c:set var="customerName" value="${consignment.customerName}" />
                            <c:set var="consignorName" value="${consignment.consignorName}" />
                            <c:set var="consigneeName" value="${consignment.consigneeName}" />
                            <c:set var="customerType" value="${consignment.customerType}" />
                            <c:set var="billingType" value="${consignment.billingType}" />
                            <input type="hidden" name="billingTypeId" id="billingTypeId" value='<c:out value="${consignment.billingTypeId}" />'>
                            <c:set var="consginmentRemarks" value="${consignment.consginmentRemarks}" />
                            <c:set var="reeferRequired" value="${consignment.reeferRequired}" />
                            <c:set var="orderRevenue" value="${consignment.orderRevenue}" />
                            <c:set var="creditLimit" value="${consignment.creditLimit}" />
                            <c:set var="availableLimit" value="${consignment.availableLimit}" />
                            <c:set var="orderExpense" value="0" />
                            <c:set var="totalWeight" value="${consignment.totalWeight}" />
                            <c:set var="vehicleCapUtilValue" value="${consignment.vehicleCapUtil}" />
                            <c:set var="orderType" value="${consignment.orderType}" />
                            <c:set var="orderStatus" value="${consignment.consginmentOrderStatus}" />
                            <c:set var="containerCapacity" value="${consignment.containerCapacity}" />
                            <c:set var="movementType" value="${consignment.movementType}" />
                            <c:set var="shipingLineNo" value="${consignment.shipingLineNo}" />
                            <c:set var="billOfEntry" value="${consignment.billOfEntry}" />
                            <c:set var="routeContractId" value="${consignment.routeContractId}" />
                            <c:set var="consignorId" value="${consignment.consignorId}" />
                            <c:set var="consigneeId" value="${consignment.consigneeId}" />

                            <!--                    //has to choose the earliest-->
                            <c:set var="tripSchedule" value="${consignment.tripScheduleDate} : ${consignment.tripScheduleTime}" />
                            <input type="hidden" name="tripScheduleDate"  id="tripScheduleDate" value='<c:out value="${consignment.tripScheduleDate}" />'>
                            <input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value='<c:out value="${consignment.orderId}" />'>
                            <input type="hidden" name="consignmentRouteId" value='<c:out value="${consignment.routeId}" />'>
                            <input type="hidden" name="tripScheduleTime" id="tripScheduleTime" value='<c:out value="${consignment.tripScheduleTimeDB}" />'>
                            <input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value='<c:out value="${consignment.vehicleTypeId}" />'>
                            <input type="hidden" name="originId" id="originId" value='<c:out value="${consignment.originId}" />'>
                            <input type="hidden" name="destinationId" id="destinationId" value='<c:out value="${consignment.destinationId}" />'>


                            <input type="hidden" name="productInfo" id="productInfo" value='<c:out value="${consignment.productInfo}" />'>
                            <input type="hidden" name="productInfo" id="productInfo" value='<c:out value="${consignment.productInfo}" />'>


                            <c:set var="vehicleType" value="${consignment.vehicleTypeName}" />
                            <c:set var="productInfo" value="${consignment.productInfo}" />
                            <c:set var="totalHours" value="${consignment.totalHrs}" />
                            <c:set var="cNotes" value="${consignment.orderNo}" />
                            <c:set var="consignmentOrderId" value="${consignment.orderId}" />

                            <c:if test="${consignment.orderType == 1}">
                                <c:set var="routeInfo" value="${consignment.origin} - ${consignment.destination}}" />
                            </c:if>
                            <c:if test="${consignment.orderType == 2}">
                                <c:if test="${consignment.interamPoint != null}">
                                    <c:set var="routeInfo" value="${consignment.origin} - ${consignment.interamPoint} - ${consignment.destination}" />
                                </c:if>
                                <c:if test="${consignment.interamPoint == null}">
                                    <c:set var="routeInfo" value="${consignment.origin}  - ${consignment.destination}" />
                                </c:if>
                            </c:if>

                            <input type="hidden" name="orderType" id="orderType"  value='<c:out value="${consignment.orderType}" />'>
                            <input type="hidden" name="orderStatus" id="orderStatus" value='<c:out value="${consignment.consginmentOrderStatus}" />'>
                            <c:set var="remainOrderWeight" value="${consignment.remainOrderWeight}" />
                            <%}else{%>

                            <input type="hidden" name="consignmentOrderIdM" id="consignmentOrderIdM" value='<c:out value="${consignmentOrderId},${consignment.orderId}" />'/>
                            <input type="hidden" name="consignmentRouteId" value='<c:out value="${consignment.routeId}" />'>
                            <c:set var="totalWeight" value="${totalWeight + consignment.totalWeight}" />
                            <c:set var="containerCapacity" value="${containerCapacity + consignment.containerCapacity}" />
                            <c:set var="orderType" value="${orderType} , ${consignment.orderType}" />
                            <c:set var="orderRevenueM" value="${orderRevenue + consignment.orderRevenue}" />
                            <c:set var="creditLimit" value="${creditLimit},{consignment.creditLimit}" />
                            <c:set var="availableLimit" value="${availableLimit},{consignment.availableLimit}" />
                            <c:set var="totalHours" value="${totalHours + consignment.totalHrs}" />
                            <c:set var="cNotes" value="${cNotes},${consignment.orderNo}" />
                            <c:set var="routeContractId" value="${routeContractId},${consignment.routeContractId}" />
                            <c:set var="customerName1" value="${consignment.customerName}" />
                            <c:set var="customerId1" value="${consignment.customerId}" />



                            <c:if test="${consignment.interamPoint != null}">
                                <c:set var="routeInfo1" value="${consignment.origin} - ${consignment.interamPoint} - ${consignment.destination}" />
                            </c:if>
                            <c:if test="${consignment.interamPoint == null}">
                                <c:set var="routeInfo1" value="${consignment.origin}  - ${consignment.destination}" />
                            </c:if>

                            <c:if test="${routeInfo ne routeInfo1}">
                                <c:set var="routeInfo" value="${routeInfo},${routeInfo1}" />
                            </c:if>
                            <c:if test="${routeInfo eq routeInfo1}">
                                <c:set var="routeInfo" value="${routeInfo}" />
                            </c:if>
                            <c:if test="${customerName ne customerName1}">
                                <c:set var="customerName" value="${customerName},${consignment.customerName}" />
                            </c:if>
                            <c:if test="${customerName eq customerName1}">
                                <c:set var="customerName" value="${customerName}" />
                            </c:if>
                            <c:if test="${customerId ne customerId1}">
                                <c:set var="customerId" value="${customerId},${consignment.customerId}" />
                            </c:if>
                            <c:if test="${customerName eq customerName1}">
                                <c:set var="customerId" value="${customerId}" />
                            </c:if>

                            <input type="hidden" name="orderType" id="orderType" value='<c:out value="${orderType}" />'>
                            <input type="hidden" name="orderStatus" id="orderStatus" value='<c:out value="${consignment.consginmentOrderStatus}" />'>

                            <%}

                              i++;
                            %>
                            <input type="hidden" name="containerCapacity" value='<c:out value="${containerCapacity}" />'>
                            <input type="hidden" name="movementType" id="movementType" value='<c:out value="${consignment.movementType}" />'>
                        </c:forEach>
                        <c:set var="profitMargin" value="${orderRevenue - orderExpense}" />
                        <c:set var="totalWeight" value="${totalWeight/1000}" />
                    </c:if>
                    <input type="hidden" name="customerId" id="customerId" value='<c:out value="${customerId}" />'>
                    <input type="hidden" name="clearanceType" id="clearanceType" value='<c:out value="${clearanceType}" />'>
                    <input type="hidden" name="routeContractId" id="routeContractId" value='<c:out value="${routeContractId}" />'>
                    <input type="hidden" name="totalHours" value='<c:out value="${totalHours}" />'>


                    <%
                    boolean isSingleConsignmentOrder = true;
                    if(i > 1){
                        isSingleConsignmentOrder = false;
                    }
                    String totalPointsStr = "2";
                    System.out.println("val"+pageContext.getAttribute("totalPoints"));
                    if(i == 1){
                        totalPointsStr = "" + (String)pageContext.getAttribute("totalPoints");
                    }else{
                        totalPointsStr = "" + (String)pageContext.getAttribute("totalPoints");

                    }
                    int totalPointsValue = 0;
                    if(!"".equals(totalPointsStr)){
                        totalPointsValue = Integer.parseInt(totalPointsStr);
                    }
                    %>

                    <input type="hidden" name="contractHireId" id="contractHireId" value='0'>
                    <c:if test="${userId == 1523 || userId == 1403 }">
                        <div style="display:block;">
                        </c:if>   
                        <c:if test="${userId != 1523 && userId != 1403 }">
                            <div style="display:none;">
                            </c:if>   
                            <table width="300" cellpadding="0" cellspacing="0" align="right" border="0" id="report" style="margin-top:0px;">

                                <tr id="exp_table" >
                                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                        <div class="tabs" align="left" style="width:300;">

                                            <div id="first">
                                                <%if (isSingleConsignmentOrder){%>
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">

                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                        <td> INR.<span id="orderRevenueSpan"><c:out value="${orderRevenue}" /></span></td>
                                                    <input type="hidden" name="orderRevenue" id="orderRevenue" value='<c:out value="${orderRevenue}" />'>
                                                    <input type="hidden" name="orderRevenueTemp" id="orderRevenueTemp" value='<c:out value="${orderRevenue}" />'>
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                        <td id="projectedExpense"> INR.<c:out value="${orderExpense}" /></td>
                                                    <input type="hidden" name="orderExpense" id="orderExpense" value='<c:out value="${orderExpense}" />'>
                                                    </tr>


                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit :</b></font></td>
                                                        <td id="projectedProfit">
                                                        </td>
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:</b></font></td>
                                                        <td id="projectedMargin">
                                                        </td>
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Credit Limit:</b></font></td>
                                                        <td>  INR. 1,00,00,000
                                                        <td>
                                                    </tr>
                                                    <tr id="exp_table" style="display:none">
                                                        <td> <font color="white"><b>Available Credit Limit:</b></font></td>
                                                        <td>  INR. <c:out value="${availableLimit}" />
                                                        <td>
                                                    </tr>
                                                    <br>

                                                </table>
                                                <%}else{%>
                                                <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Expected Revenue:</b></font></td>
                                                        <td> INR.<span id="orderRevenueSpan"><c:out value="${orderRevenue}" /></span></td>
                                                    <input type="hidden" name="orderRevenue" id="orderRevenue" value='<c:out value="${orderRevenue}" />'>
                                                    <input type="hidden" name="orderRevenueTemp" id="orderRevenueTemp" value='<c:out value="${orderRevenue}" />'>

                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Projected Expense:</b></font></td>
                                                        <td id="projectedExpense"> INR.<c:out value="${orderExpense}" /></td>
                                                    <input type="hidden" name="orderExpense" id="orderExpense" value='<c:out value="${orderExpense}" />'>
                                                    </tr>


                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit :0.00</b></font></td>
                                                        <td id="projectedProfit">
                                                        </td>
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Profit Margin:0.00</b></font></td>
                                                        <td id="projectedMargin">
                                                        </td>
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Credit Limit:</b></font></td>
                                                        <td>  INR. 0.00
                                                        <td>
                                                    </tr>
                                                    <tr id="exp_table" >
                                                        <td> <font color="white"><b>Available Credit Limit:</b></font></td>
                                                        <td>  INR. 0.00
                                                        <td>
                                                    </tr>
                                                </table>
                                                <%}%>
                                                <input type="hidden" name="orderRevenue" id="orderRevenue"  value='<c:out value="${orderRevenue}" />'>
                                            </div>
                                        </div>
                                    </td>
                                </tr>

                                <input type="hidden" name="orderExpense" id="orderExpense" value='<c:out value="${orderExpense}" />'>
                            </table>

                        </div>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <input type="hidden" id="hireRate" name="hireRate" value="0"/>
                        <input type="hidden" name="plannedContainer" id="plannedContainer" value="<%=request.getAttribute("plannedContainer")%>">
                        <br>
                        <br>
                        <br>
                        <c:if test = "${vesselList != null}" >
                            <table width="300" cellpadding="0" cellspacing="1" border="0" align="center">
                                <c:forEach items="${vesselList}" var="vl">
                                    <tr id="vessel_table" colspan="8">
                                        <td colspan="2" style="width:180px;"> 
                                            Customer Reference No:<font size="3" color="black">&nbsp;<c:out value="${vl.customerOrderReferenceNo}" />  &nbsp;</font>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> 
                                            <input type="hidden" name="vesselName" id="vesselName" value='<c:out value="${vl.linerId}" />' >
                                            Movement :<font size="3" color="black"> &nbsp;<c:out value="${vl.movementTypeName}" />  &nbsp;<c:out value="${vl.twtyTotal}" /> &nbsp; <c:out value="${vl.frtyTotal}" /> </font>
                                        </td>
                                    </tr> 
                                </c:forEach>
                            </table>
                        </c:if>
                        <br>
                        <center><font color='red' size="3"><span id="approvalStatusSpan"></span></font></center>
                        <div id="tabs" >

                            <c:if test = "${movementType == 7}" >
                                <ul class="nav nav-tabs" id="tabDetails">
                                    <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                                    <li data-toggle="tab"><a href="#bunkDetail" onclick="#" ><span>FuelDetails</span></a></li>
                                    <li data-toggle="tab"><a href="#routeDetail" id="showdiv" onclick="changediv();"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                                    <li data-toggle="tab"><a href="#containers" onclick="getContainerDetails();" >Container Details</a></li>
                                </ul>
                            </c:if>
                            <c:if test = "${movementType != 7}" >
                                <ul class="nav nav-tabs" id="tabDetails">
                                    <li class="active" data-toggle="tab"><a href="#tripDetail"><span>Trip Details</span></a></li>
                                        <c:if test = "${movementType != 14 && movementType != 16}" >
                                        <li data-toggle="tab"><a href="#containers" onclick="getContainerDetails();" ><span>Container Details</span></a></li>
                                        </c:if>

                                    <c:if test = "${companyId == '1461' }" >
                                        <c:if test = "${pageType != 3 }" >
                                            <li data-toggle="tab"><a href="#bunkDetail" onclick="#" ><span>Fuel/Advance Details</span></a></li>
                                            </c:if>
                                        </c:if>
                                        <c:if test = "${companyId != '1461' }" >
                                        <li data-toggle="tab"><a href="#bunkDetail" onclick="#" ><span>Fuel/Advance Details</span></a></li>
                                        </c:if>
                                    <li data-toggle="tab"><a href="#routeDetail" id="showdiv" onclick="changediv();"><span>Consignment Note(s) / Route Course / Trip Plan </span></a></li>
                                </ul>
                            </c:if>
                            <div id="tripDetail" class="tab-pane active">
                                <table   class="table table-info mb30 table-hover" style="margin-left: 10px;width:100%" id="bg">
                                    <thead>
                                    <center><font color="red" size="4"><span id="contractStatus"></span></font></center>
                                    <tr id="tableDesingTH" height="30">
                                        <td colspan="4" >Trips Details</td>
                                    </tr>

                                    <tr>
                                        <!--                            <td><font color="red">*</font>Trip Sheet Date</td>
                                                                    <td><input type="text" name="tripDate" class="datepicker" value=""></td>-->
                                        <td>CNote No(s)</td>
                                        <td>
                                            <c:out value="${cNotes}" />
                                            <input type="hidden" name="cNotes" Id="cNotes" class="form-control" value='<c:out value="${cNotes}" />'>
                                        </td>
                                        <td>Billing Type</td>
                                        <td>
                                            <c:out value="${billingType}" />
                                            <input type="hidden" name="billingType" Id="billingType" class="form-control" value='<c:out value="${billingType}" />'>
                                        </td>
                                    </tr>
                                    <tr>
                                        <!--                            <td>Customer Code</td>
                                                                    <td>BF00001</td>-->
                                        <td>Customer Name</td>
                                        <td>
                                            <c:out value="${customerName}" />
                                            <input type="hidden" name="customerName" Id="customerName" class="form-control" value='<c:out value="${customerName}" />'>
                                        </td>
                                        <td>Customer Type</td>
                                        <td >
                                            <c:out value="${customerType}" />
                                            <input type="hidden" name="customerType" Id="customerType" class="form-control" value='<c:out value="${customerType}" />'>
                                            <input type="hidden" name="tripType" Id="tripType" class="form-control" value='<c:out value="${tripType}" />'>
                                        </td>

                                    </tr>

                                    <tr style="display:none;">

                                        <td>Consignor Name</td>
                                        <td>
                                            <input type="hidden" name="consignorId" Id="consignorId" class="form-control" value='<c:out value="${consignorId}" />'>
                                            <input type="text" name="consignorName" Id="consignorName" class="form-control"  style="width:240px;height:40px;" value='<c:out value="${consignorName}" />'>
                                        </td>
                                        <td>Consignee Name</td>
                                        <td >
                                            <input type="text" name="consigneeName" Id="consigneeName" class="form-control"  style="width:240px;height:40px;" value='<c:out value="${consigneeName}" />'>
                                            <input type="hidden" name="consigneeId" Id="consigneeId" class="form-control" value='<c:out value="${consigneeId}" />' >
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>Route Name</td>
                                        <td>

                                            <%if (!isSingleConsignmentOrder){%>
                                            <c:out value="${routeInfo}" />
                                            <input type="hidden" name="routeInfo" Id="routeInfo" class="form-control" value='<c:out value="${routeInfo}" />'>
                                            <%}else{%>
                                            <c:out value="${routeInfo}" />
                                            <input type="hidden" name="routeInfo" Id="routeInfo" class="form-control" value='<c:out value="${routeInfo}" />'>
                                            <%}%>

                                        </td>
                                        <!--                            <td>Route Code</td>
                                                                    <td >DL001</td>-->
                                        <td style="display: block">Reefer Required</td>
                                        <td style="display: block">
                                            <c:out value="${reeferRequired}" />
                                            <input type="hidden" name="reeferRequired" Id="reeferRequired" class="form-control" value='<c:out value="${reeferRequired}" />'>
                                        </td>
                                        <td  style="display: none;">Order Est Weight (MT)</td>
                                        <td  style="display: none;">
                                            <c:if test="${remainOrderWeight > 0.00}">
                                                <input type="text" name="totalWeight" Id="totalWeight" class="form-control" style="width:240px;height:40px;" value='<c:out value="${remainOrderWeight}" />' readonly >
                                            </c:if>
                                            <c:if test="${remainOrderWeight == 0.00}">
                                                <input type="text" name="totalWeight" Id="totalWeight" class="form-control" style="width:240px;height:40px;" value='<c:out value="${totalWeight}" />' readonly >
                                            </c:if>
           <!--                                    <input type="text" name="totalWeight" Id="totalWeight" class="form-control" value='<c:out value="${totalWeight}" />' readonly >-->

                                        </td>
                                        <td style="display: none">Vehicle Type</td>
                                        <!--                                <td id="vehicleTypeName">-->
                                    <input type="hidden" name="vehicleType" Id="vehicleType" class="form-control" value=''/>
                                    <input type="hidden" name="pageType" Id="pageType" class="form-control" value='<c:out value="${pageType}"/>'/>

                                    </tr>

                                    <tr>
                                        <td><font color="red">*</font>Transporter</td>
                                        <td>
                                            <select name="vendor" id="vendor" class="select2 input-default form-control" onchange="setVehicleType();
                                                    resetAll();
                                                    resetCopobox();
                                                    setRCMForVehicleType('1');" class="form-control" style="width:240px;height:40px;">
                                                <c:if test="${vendorList != null}">
                                                    <option value="0~0" >--select--</option>
                                                    <c:if test = "${pageType == 1 || pageType == 2}" >
                                                        <c:if test = "${companyId != '1472' }" >
                                                            <option value="1~0" selected>CA Log (P) Ltd.</option>
                                                        </c:if>
                                                        <c:if test = "${companyId == '1472' }" >
                                                            <option value="1~0" selected>Chakiat Agencies.</option>
                                                        </c:if>

                                                    </c:if>
                                                    <c:if test = "${pageType == 3 }" >
                                                        <c:forEach items="${vendorList}" var="vehNo">
                                                            <option value='<c:out value="${vehNo.vendorId}"/>~<c:out value="${vehNo.contractTypeId}"/>'><c:out value="${vehNo.vendorName}"/></option>
                                                        </c:forEach>
                                                    </c:if>
                                                </c:if>
                                                <script>
                                                    $('#vendor').select2({placeholder: 'Fill vendor'});
                                                </script>
                                            </select>
                                        </td>
                                        <td><font color="red">*</font>Vehicle Type</td>

                                        <td>
                                            <c:if test="${movementType != 14 && movementType != 16}">  
                                                <select name="vehicleTypeName" id="vehicleTypeName" onchange="resetAll();
                                                        setVehicle();
                                                        setRCMForVehicleType('1');
                                                        calOtherVehTypeRevenue(this.value);" class="form-control" style="width:240px;height:40px;">

                                                </select>
                                                <input type="hidden" name="vehicleTypeIdSelected" Id="vehicleTypeIdSelected" class="form-control" value=''/>

                                            </c:if>
                                            <c:if test="${movementType == 14 || movementType == 16}">
                                                <select name="vehicleTypeName" id="vehicleTypeName" onchange="resetAll();
                                                        setRCMForLCLVehicleType();
                                                        setVehicle();
                                                        calOtherVehTypeRevenue(this.value);" class="form-control" style="width:240px;height:40px;">
                                                    <input type="hidden" name="vehicleTypeIdSelected" Id="vehicleTypeIdSelected" class="form-control" value=''/>
                                                </select>
                                                <input type="hidden" name="vehicleTypeIdSelected" Id="vehicleTypeIdSelected" class="form-control" value=''/>
                                            </c:if>
                                        </td>
                                    </tr>
                                    <c:forEach items="${consgncontainer}" var="noss">
                                        <c:set var="containerTypeList1" value="0" />
                                        <c:if test = "${noss.containerId == '2'}" >
                                            <c:set var="containerTypeList1" value="1" />
                                        </c:if>
                                    </c:forEach>
                                    <tr id="containerQuantitySelect1" class="containerQuantitySelect">
                                        <td id="contSize"><font color="red">*</font>Container Size</td>
                                        <td>
                                            <select name="containerQuantity" id="containerQuantity" onchange="setContainer();"  class="form-control" style="width:240px;height:40px;">
                                                <option value="0" >--select--</option>
                                                <c:if test = "${containerTypeList1 == 1}" >
                                                    <option value="1" >1 * 40</option>
                                                </c:if>
                                                <c:if test = "${containerTypeList1 == 2}" >
                                                    <option value="1" >1 * 40</option>
                                                </c:if>
                                                <c:if test = "${containerTypeList1 != 2 && containerTypeList1 != 1}" >
                                                    <option value="1" >1 * 40</option>
                                                </c:if>
                                                <option value="2" >1 * 20</option>
                                                <option value="3" >2 * 20</option>
                                            </select>
                                        </td>

                                        <td colspan="2">&nbsp;
                                        </td>
                                    </tr>

                                    <!--                                <tr class="vehContainerSelect"  id="vehContainerSelect1" >
                                                                        <td>
                                                                            Plan Single 20' Container
                                                                        </td>
                                                                        <td>
                                                                            <input type="checkbox" name="sinTwentyCheck" id="sinTwentyCheck" onclick="selectSingleTwentyContainer()" 
                                                                                   class="checkbox"/>-->
                                    <input type="hidden"   name="sinTwentyContainer" id="sinTwentyContainer" value="0" />
                                    <!--                                    </td>
                                                                        <td></td>
                                                                        <td></td>-->
                                    <script>
                                        function selectSingleTwentyContainer() {
                                            if (document.getElementById("sinTwentyCheck").checked == true) {
                                                document.getElementById("sinTwentyContainer").value = 1;
                                            } else {
                                                document.getElementById("sinTwentyContainer").value = 0;
                                            }
                                            setContainer();
                                        }
                                    </script>
                                    <!--</tr>-->
                                    <tr  class ="vehCategory" id="vehCate" >
                                        <td>
                                            <font color="red">*</font>Vehicle Category
                                        </td>
                                        <td>
                                            <select name="vehicleCategory" id="vehicleCategory"  class="form-control" style="width:240px;height:40px;" onchange="showVehicle();
                                                    resetAll();
                                                    setRCMForVehicleType('1');
                                                    setVehicle();">
                                                <option value="0" selected>---select---</option>
                                                <!--<option value="1" >Leased </option>-->
                                                <option value="2" >Hire</option>
                                            </select>
                                        </td>
                                        <td id="hireL" >Hire Charge</td>
                                        <td id="hiree">
                                            <input name="hire" type="text" class="form-control" style="width:240px;height:40px;" id="hire" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" />
                                            <input type="hidden"   name="contractHireRate" id="contractHireRate" value="0" />
                                        </td>
                                    </tr>
                                    <tr>
                                    <input type="hidden"   name="tripStatus" id="tripStatus" value="0" />
                                    <!--                                <td>Trip Status</td>
                                                                    <td>
                                                                        <select name="tripStatus" id="tripStatus" onchange="tripStatusVehicle();" class="form-control" >
                                    <c:if test="${statusList != null}">
                                        <option value="0" selected>--select--</option>
                                        <c:forEach items="${statusList}" var="status">
                                            <option value='<c:out value="${status.statusId}"/>'><c:out value="${status.statusName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                            <option value="222">Running Vehicle's</option>
                                    </select>
                                    </td>-->
                                    <%int lableSno = 1;%>
                                    <c:forEach items="${orderPointDetails}" var="consignment">
                                        <input type="hidden" name="labelConsignmentOrderNo" id="labelConsignmentOrderNo<%=lableSno%>" value="<c:out value="${consignment.consignmentOrderNo}" />" />
                                        <%lableSno++;%>
                                    </c:forEach>
                                    <script>
                                        function showVehicle() {
                                            var vehCategory = document.getElementById("vehicleCategory").value;

                                            //alert("vehCategory:"+vehCategory);
                                            if (vehCategory == '1') {
                                                $("#vehOwn").hide();
                                                $("#vehHire").show();
                                                //                                            document.trip.elements['vehicleNo'].style.display = 'block';
                                                //                                            document.getElementById("hireVehicleNo").value = "";
                                                //                                            document.trip.elements['hireVehicleNo'].style.display = 'none';
                                                document.getElementById("ownership").value = '2';
                                            } else if (vehCategory == '2') {
                                                $("#vehOwn").hide();
                                                $("#vehHire").show();

                                                //                                            document.trip.elements['vehicleNo'].style.display = 'none';
                                                //                                            $('#vehicleNo').empty();
                                                //                                            $('#vehicleNo').append($('<option></option>').val(0).html('--select--'))
                                                //                                            document.trip.elements['hireVehicleNo'].style.display = 'block';
                                                //                                            document.getElementById("hireVehicleNo").value = "Hired 001";
                                                document.getElementById("ownership").value = '3';
                                            }
                                        }
                                        function getDistance(id) {
                                            //alert("id:"+id);
                                            if (id == 1) {
                                            } else {
                                                var temp = "";
                                                var temp1 = "";
                                                var temp2 = "";
                                                var id1 = id - 1;
                                                //  alert(id1);
                                                temp2 = document.getElementById('pointNameRepo' + id).value;
                                                temp1 = document.getElementById('pointNameRepo' + id1).value;
                                                //document.getElementById('pointId' + id1).value = temp1;
                                                //document.getElementById('pointId' + id).value = temp2;
                                                document.getElementById('pointIdRepo' + id1).value = temp1;
                                                document.getElementById('pointIdRepo' + id).value = temp2;
                                                var url = "/throttle/getDistance.do?origin=" + temp1 + "&destination=" + temp2;
                                                if (window.ActiveXObject)
                                                {
                                                    httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                                                } else if (window.XMLHttpRequest)
                                                {
                                                    httpReq = new XMLHttpRequest();
                                                }
                                                httpReq.open("GET", url, true);
                                                httpReq.onreadystatechange = function () {
                                                    processAjax11();
                                                };
                                                httpReq.send(null);
                                            }
                                        }

                                        function processAjax11()
                                        {

                                            if (httpReq.readyState == 4)
                                            {
                                                if (httpReq.status == 200)
                                                {
                                                    temp = httpReq.responseText.valueOf();


                                                    document.getElementById("totalkm").value = parseInt(document.getElementById("totalkm").value) + parseInt(temp);



                                                } else
                                                {
                                                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                                                }

                                            }
                                        }

                                        function changediv() {

                                            //$("#vehHire").hide();

                                            setKmVakues("str", "textn", "fcl");

                                            var ordsts = document.getElementById("orderStatus").value;
                                            var ordtyp = document.getElementById("orderType").value;
                                            var orderStatus = parseInt(ordsts);
                                            var orderType = parseInt(ordtyp);

                                            //                              alert(orderStatus+" "+orderType);
                                            if (orderType == 2) {
                                                if (orderStatus == 5) {
                                                    // alert(1);
                                                    //  var multiOrder=parseInt(isMultiPle);

                                                    document.getElementById("fclLeg").style.display = 'block';



                                                } else if (orderStatus == 25) {
                                                    // alert(2);whAddress
                                                    document.getElementById("fclLeg").style.display = 'block';

                                                } else {
                                                    // alert(3);
                                                    document.getElementById("fclLeg").style.display = 'block';

                                                }
                                            } else {
                                                // alert(4);
                                                document.getElementById("fclLeg").style.display = 'block';



                                            }
                                            if (document.getElementById("tempRepo").checked == true) {
                                                document.getElementById("fclLeg").style.display = 'none';
                                            }
                                            //    setRouteKm();


                                        }
                                        var httpReq;
                                        var temp = "";
                                        function getContainerDetails() {
                                            var temp = "";
                                            temp = document.getElementById('ConsignmentOrderIds').value;

                                            var url = "/throttle/getContainerDetails.do?ConsignmentOrderIds=" + temp;
                                            if (window.ActiveXObject)
                                            {
                                                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
                                            } else if (window.XMLHttpRequest)
                                            {
                                                httpReq = new XMLHttpRequest();
                                            }
                                            httpReq.open("GET", url, true);
                                            httpReq.onreadystatechange = function () {
                                                processAjax1();
                                            };
                                            httpReq.send(null);
                                        }


                                        function processAjax1() {
                                            if (httpReq.readyState == 4)
                                            {
                                                if (httpReq.status == 200)
                                                {
                                                    temp = httpReq.responseText.valueOf();

                                                    var tempArr = temp.split("+");


                                                    document.getElementById("containerNo" + 0).value = tempArr[0];

                                                    document.getElementById("containerType" + 0).value = tempArr[1];
                                                    document.getElementById("containerTypeId" + 0).value = tempArr[2];

                                                } else
                                                {
                                                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                                                }
                                            }
                                        }
                                        //temp
                                        function setRCMForVehicleType(select) { //alert("setRCMForVehicleType");
                                            var totalFuelRequiredN = 0;
                                            var marketHireRateN = 0;
                                            var containerTonnage = "";
                                            var containerTypeIdSelected = "";
                                            var vehicleTypeIdTemp = document.getElementById("vehicleTypeName").value;
//                                            alert(vehicleTypeIdTemp)
                                            var temp2 = vehicleTypeIdTemp.split("-");
                                            var vehicleTypeId = temp2[0];
                                            if (vehicleTypeId == 1058 || vehicleTypeId == 1059 || vehicleTypeId == 1060) {
//                                            alert(vehicleTypeId)

                                                var selectContainer = 0;
                                                var selectedIndex = document.getElementsByName("selectedIndex");
//                                                alert(selectedIndex.length);
                                                for (var i = 0; i < selectedIndex.length; i++) {
                                                    if (document.getElementById("selectedIndex" + (i + 1)).checked == true) {
                                                        containerTonnage = document.getElementById("tonnage" + (i + 1)).value;
                                                        containerTypeIdSelected = document.getElementById("containerTypeId" + (i + 1)).value;
//                                                        alert("containerTypeIdSelected==" + containerTypeIdSelected);
                                                        selectContainer++;
                                                    }
                                                }
//                                                alert(selectContainer);
                                                if (selectContainer > 0) {
//                                                    alert("inside....");
                                                    var pointIds = document.trip.pointId;
                                                    var ownership = document.getElementById("ownership").value;

                                                    var vehicleCategory = document.getElementById("vehicleCategory").value;
                                                    var routeContractId = document.getElementById("routeContractId").value;
                                                    //var vehicleTypeIdTemp = document.getElementById("vehicleTypeName").value;
                                                    //var temp2 = vehicleTypeIdTemp.split("-");
                                                    //var vehicleTypeId = temp2[0];
                                                    var vendorIdTemp = document.getElementById("vendor").value;
                                                    var movementType = document.getElementById("movementType").value;
                                                    var vehicleNo = document.getElementById("vehicleNo").value;
                                                    var vehicleNoTemp = vehicleNo.split("-");
                                                    var temp = vendorIdTemp.split("~");
                                                    var vendorId = temp[0];
                                                    var sinTwentyContainer = document.getElementById("sinTwentyContainer").value;
                                                    if (vehicleTypeId == 1059 && sinTwentyContainer == 1 || vehicleTypeId == 1060 && sinTwentyContainer == 1) {
                                                        vehicleTypeId = "1058";
                                                    } else {
                                                        vehicleTypeId = vehicleTypeId;
                                                    }
                                                    if (vehicleTypeId == 1059 || vehicleTypeId == 1058 || vehicleTypeId == 1060) {
                                                        selectContainer = selectContainer;
                                                    } else {
                                                        selectContainer = 0;
                                                    }
                                                    var goodToGo = false;
                                                    if (vehicleTypeId != 0 && vendorId != 0) { //vendor is selected and vehicle type is selected
                                                        if (vendorId != 1) { //if vendor is not own
                                                            if (vehicleCategory != 0) { //vehicle category is chosen
                                                                goodToGo = true;
                                                            }
                                                        } else {
                                                            goodToGo = true;
                                                        }
                                                    }
                                                    if (goodToGo) {
                                                        var sourceId = 0;
                                                        var point1Id = 0;
                                                        var point2Id = 0;
                                                        var point3Id = 0;
                                                        var point4Id = 0;
                                                        var destinationId = 0;

                                                        if (pointIds.length == '2') {
                                                            sourceId = pointIds[pointIds.length - 2].value;
                                                            destinationId = pointIds[pointIds.length - 1].value;
                                                        } else if (pointIds.length == '3') {
                                                            sourceId = pointIds[pointIds.length - 3].value;
                                                            point1Id = pointIds[pointIds.length - 2].value;
                                                            destinationId = pointIds[pointIds.length - 1].value;
                                                        } else if (pointIds.length == '4') {
                                                            sourceId = pointIds[pointIds.length - 4].value;
                                                            point1Id = pointIds[pointIds.length - 3].value;
                                                            point2Id = pointIds[pointIds.length - 2].value;
                                                            destinationId = pointIds[pointIds.length - 1].value;
                                                        }

                                                        var urlVal = "";
//                                                        alert(movementType)
                                                        if (movementType == 8) {
                                                            urlVal = "/throttle/getDistanceRcmForVehicleType.do";
                                                        } else {
                                                            urlVal = "/throttle/getRcmForVehicleType.do";
                                                        }

//                                                        if(movementType == 12 || movementType == 13  || movementType == 14  || movementType == 15){
//                                                            point1Id=0;
//                                                            point2Id=0
//                                                        }

                                                        var travelKm = document.getElementById("totalkm").value;
                                                        var productInfo = document.getElementById("productInfo").value;
                                                        var orderTripType = document.getElementById("orderTripType").value;
                                                        var billingTypeId = document.getElementById("billingTypeId").value;
//                                                        alert("urlVal-----" + urlVal)
                                                        $.ajax({
                                                            url: urlVal,
                                                            dataType: "text",
                                                            async: false,
                                                            data: {
                                                                travelKm: travelKm,
                                                                vehicleTypeId: vehicleTypeId,
                                                                vendorId: vendorId,
                                                                sourceId: sourceId,
                                                                point1Id: point1Id,
                                                                point2Id: point2Id,
                                                                destinationId: destinationId,
                                                                ownership: ownership,
                                                                movementType: movementType,
                                                                noOfContainer: selectContainer,
                                                                productInfo: productInfo,
                                                                vehicleCategory: vehicleCategory,
                                                                vehicleNoId: vehicleNoTemp[0],
                                                                containerTypeId: containerTypeIdSelected,
                                                                contractId: routeContractId,
                                                                orderTripType: orderTripType,
                                                                billingTypeId: billingTypeId,
                                                                containerTonnage: containerTonnage
                                                            },
                                                            success: function (temp) {

//                                                                alert(temp);
                                                                if (temp != '' && temp != null && temp != 'null') {
//                                                                    alert("ownership=" + ownership)
                                                                    if (ownership == 3) {
                                                                        var vendorApprovalStatus = 0;
                                                                        var vendorRateWithReeferN = 0;
                                                                        var vendorRateWithOutReeferN = 0;

                                                                        var tempArr00 = temp.split("~");
                                                                        var hireRate = tempArr00[1];
                                                                        var tempArr7 = temp.split("#");
                                                                        vendorRateWithOutReeferN = hireRate;
                                                                        var approvalStatus = tempArr00[2];
                                                                        var contractHireId = tempArr00[3];
                                                                        vendorApprovalStatus = approvalStatus;

//                                                                        alert("hireRate==" + hireRate);
//                                                                        alert("approvalStatus==" + approvalStatus);
//                                                                        alert("contractHireId==" + contractHireId);
                                                                        if (contractHireId == 0) {
                                                                            alert("Hire Contract not available...");
                                                                            document.getElementById("contractStatus").innerHTML = "Hire Contract not available...";
                                                                        } else {
                                                                            document.getElementById("contractStatus").innerHTML = "";
                                                                        }
                                                                        document.getElementById("contractHireId").value = contractHireId;
                                                                        document.getElementById("hire").value = hireRate;
                                                                        document.getElementById("contractHireRate").value = hireRate;
                                                                        document.getElementById("estimatedExpense").value = parseFloat(hireRate);
                                                                        document.getElementById("projectedExpense").innerHTML = "INR: " + parseInt(hireRate);
                                                                        document.getElementById("projectedProfit").innerHTML = parseInt(document.getElementById("orderRevenueTemp").value) - parseInt(hireRate);
                                                                        var val1 = parseFloat(parseInt(document.getElementById("orderRevenueTemp").value) - parseInt(hireRate));
                                                                        document.getElementById("projectedMargin").innerHTML = parseFloat((val1 / parseFloat(hireRate)) * 100).toFixed(2) + " %";
                                                                    } else {
                                                                        var vendorRateWithReeferN = 0;
                                                                        var vendorRateWithOutReeferN = 0;
                                                                        var vendorApprovalStatus = 0;
                                                                        var tempArr7 = temp.split("#");
                                                                        var tempArr3 = tempArr7[1].split("@");
                                                                        var tempArr = tempArr3[1].split("-");
                                                                        //   alert("tempArr"+tempArr);
                                                                        var tempArr1 = tempArr[0].split("~");
                                                                        var contractHireId = 0;
                                                                        document.getElementById("contractHireId").value = contractHireId;
                                                                        var tempArr2 = null;
                                                                        if (tempArr.length > 1) {
                                                                            tempArr2 = tempArr[1].split("~");
                                                                            vendorRateWithReeferN = tempArr2[0];
                                                                            if (tempArr2.length > 1) {
                                                                                vendorRateWithOutReeferN = tempArr2[1];
                                                                                vendorApprovalStatus = tempArr2[2];
                                                                            }
                                                                        }
                                                                        //                                                            alert("tempArr[0]"+tempArr[0]);
                                                                        //   alert("tempArr[1]"+tempArr[1]);
                                                                        var tempArr5 = tempArr3[0].split("~");
                                                                        if (tempArr5.length > 1) {
                                                                            if (productInfo == 1) {
                                                                                totalFuelRequiredN = parseFloat(tempArr5[0]);
                                                                            } else {
                                                                                totalFuelRequiredN = parseFloat(tempArr5[0]) + parseFloat(tempArr5[1]);
                                                                            }
                                                                        }

                                                                        var rateWithReeferN = tempArr1[0];
                                                                        var rateWithOutReeferN = tempArr1[1];
                                                                        var fuelVehicleN = tempArr1[2];
                                                                        var fuelDgN = tempArr1[3];
                                                                        var tollN = tempArr1[4];
                                                                        var driverBachatN = tempArr1[5];

                                                                        var perDayCostN = tempArr1[0];
                                                                        var tripCostN = tempArr1[1];
//                                                                    alert("perDayCost="+tempArr1[0])
//                                                                    alert("tripCost="+tempArr1[1])

                                                                        var dalaN = tempArr1[6];
                                                                        var miscN = tempArr1[7];
                                                                        marketHireRateN = tempArr1[8];
                                                                    }
                                                                    //alert("productInfo:"+productInfo);

                                                                    //                                                            alert("selectContainer"+selectContainer);
                                                                    //                                                            alert('containerTypeIdSelected == '+containerTypeIdSelected);
                                                                    //   alert('vehicleCategory == '+vehicleCategory);
                                                                    //  alert('tollN == '+tollN +"driverBachatN==="+driverBachatN+"dalaN=="+dalaN+"miscN=="+miscN);
                                                                    //  alert('tempArr7 == '+tempArr7[0]);

//                                                                    if (containerTypeIdSelected != 0 && vehicleTypeId != 0) {
                                                                    if (vehicleTypeId != 0) {
                                                                        $("#approvalStatusSpan").text('');
//                                                                            alert("tempArr7[0]==="+tempArr7[0])
                                                                        if (tempArr7[0] == 1) {
                                                                            if (vendorId == 1 || (vendorId != 1 && vehicleCategory == 1)) {
                                                                                //alert("am here...2");
                                                                                $("#approvalStatusSpan").text('');
                                                                                var fuelAmounts = document.getElementById("totalFuelAmount").value;
                                                                                if (document.getElementById("ownership").value == 1) {
                                                                                    document.getElementById("estimatedExpense").value = parseFloat(tollN) + parseFloat(driverBachatN)
                                                                                            + parseFloat(dalaN) + parseFloat(miscN) + parseFloat(fuelAmounts)
                                                                                            + parseFloat(tripCostN);
                                                                                    document.getElementById("projectedExpense").innerHTML = "INR: " + parseInt(document.getElementById("estimatedExpense").value);
                                                                                } else {
                                                                                    document.getElementById("estimatedExpense").value = tempArr1[0];
                                                                                    document.getElementById("projectedExpense").innerHTML = "INR: " + parseInt(tempArr1[0]);
                                                                                }

                                                                                document.getElementById("orderExpense").value = parseInt(document.getElementById("estimatedExpense").value);
                                                                                //alert(parseInt(document.getElementById("orderRevenue").value));
                                                                                //alert(parseInt(document.getElementById("orderExpense").value));
                                                                                if (parseInt(document.getElementById("orderRevenue").value) > 0 && parseInt(document.getElementById("estimatedExpense").value) > 0) {
                                                                                    document.getElementById("projectedProfit").innerHTML = ((parseInt(document.getElementById("orderRevenue").value)
                                                                                            - parseInt(document.getElementById("orderExpense").value))).toFixed(2);
                                                                                    document.getElementById("projectedMargin").innerHTML = ((parseInt(document.getElementById("orderRevenue").value)
                                                                                            - parseInt(document.getElementById("orderExpense").value)) * 100 / parseInt(document.getElementById("orderRevenue").value)).toFixed(2) + '%';
                                                                                } else {
                                                                                    document.getElementById("projectedProfit").innerHTML = '0';
                                                                                    document.getElementById("projectedMargin").innerHTML = '0%';
                                                                                }
                                                                                if (tempArr5.length > 1) {
                                                                                    $("#approvalStatusSpan").text('');
                                                                                    document.getElementById("totalFuelLtrs").value = totalFuelRequiredN;
                                                                                    document.getElementById("fuelLtrs1").value = totalFuelRequiredN;
                                                                                    if (document.getElementById("tempRepo").checked == true) {
                                                                                        document.getElementById("totalFuelLtrs").value = "";
                                                                                        document.getElementById("fuelLtrs1").value = "";
                                                                                        document.getElementById("fuelLtrs1").readOnly = false;
                                                                                    } else {
                                                                                        document.getElementById("fuelLtrs1").readOnly = false;
                                                                                    }
                                                                                } else {
                                                                                    if (document.getElementById("tempRepo").checked == true) {
                                                                                        document.getElementById("fuelLtrs1").readOnly = false;
                                                                                        document.getElementById("totalFuelLtrs").value = "";
                                                                                        document.getElementById("fuelLtrs1").value = "";
                                                                                    } else {
                                                                                        document.getElementById("fuelLtrs1").readOnly = false;
                                                                                    }
                                                                                    var actionOpt = document.trip.actionName;
                                                                                    var optionVar = new Option("-select-", '0');
                                                                                    actionOpt.options[0] = optionVar;
                                                                                    optionVar = new Option("Cancel Order", 'Cancel');
                                                                                    actionOpt.options[1] = optionVar;
                                                                                    optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                                                    actionOpt.options[2] = optionVar;
                                                                                    optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                                                    actionOpt.options[3] = optionVar;
                                                                                    $("#actionDiv").show();
                                                                                    $("#approvalStatusSpan").text('Fuel rate is not available. Please configure the fuel for choosen vehicle model.');
                                                                                    alert("Fuel rate is not available. Please configure the fuel for choosen vehicle model.");
                                                                                    $("#save").hide();
                                                                                }
                                                                            } else {
                                                                                document.getElementById("fuelLtrs1").readOnly = false;
                                                                                if (vendorApprovalStatus == 1) {
                                                                                    //alert("am here...1:"+vendorRateWithOutReeferN);
                                                                                    //alert("am here...3:"+vendorRateWithReeferN);
                                                                                    $("#approvalStatusSpan").text('');
                                                                                    var fuelAmounts = document.getElementById("totalFuelAmount").value;
                                                                                    //alert("am here...11::"+fuelAmounts);
                                                                                    if (productInfo == 1) {//ambient
                                                                                        document.getElementById("estimatedExpense").value = parseFloat(vendorRateWithOutReeferN) + parseFloat(fuelAmounts);
                                                                                        document.getElementById("hireRate").value = parseFloat(vendorRateWithOutReeferN);
                                                                                    } else {//reefer
                                                                                        document.getElementById("estimatedExpense").value = parseFloat(vendorRateWithReeferN) + parseFloat(fuelAmounts);
                                                                                        document.getElementById("hireRate").value = parseFloat(vendorRateWithReeferN);
                                                                                    }

                                                                                    document.getElementById("projectedExpense").innerHTML = "INR: " + parseInt(document.getElementById("estimatedExpense").value);
                                                                                    document.getElementById("orderExpense").value = parseInt(document.getElementById("estimatedExpense").value);
                                                                                    //alert(parseInt(document.getElementById("orderRevenue").value));
                                                                                    //alert(parseInt(document.getElementById("orderExpense").value));
                                                                                    if (parseInt(document.getElementById("orderRevenue").value) > 0 && parseInt(document.getElementById("estimatedExpense").value) > 0) {
                                                                                        document.getElementById("projectedProfit").innerHTML = ((parseInt(document.getElementById("orderRevenue").value)
                                                                                                - parseInt(document.getElementById("orderExpense").value))).toFixed(2);
                                                                                        document.getElementById("projectedMargin").innerHTML = ((parseInt(document.getElementById("orderRevenue").value)
                                                                                                - parseInt(document.getElementById("orderExpense").value)) * 100 / parseInt(document.getElementById("orderRevenue").value)).toFixed(2) + '%';
                                                                                    } else {
                                                                                        document.getElementById("projectedProfit").innerHTML = '0';
                                                                                        document.getElementById("projectedMargin").innerHTML = '0%';
                                                                                    }
                                                                                } else {

                                                                                    var actionOpt = document.trip.actionName;
                                                                                    var optionVar = new Option("-select-", '0');
                                                                                    actionOpt.options[0] = optionVar;
                                                                                    optionVar = new Option("Cancel Order", 'Cancel');
                                                                                    actionOpt.options[1] = optionVar;
                                                                                    optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                                                    actionOpt.options[2] = optionVar;
                                                                                    optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                                                    actionOpt.options[3] = optionVar;
                                                                                    $("#actionDiv").show();
                                                                                    $("#save").hide();
                                                                                    //                                                                $("#generateTripSheet").hide();
//                                                                                    alert("vendorApprovalStatus == "+vendorApprovalStatus);
                                                                                    if (vendorApprovalStatus == 0) {
                                                                                        $("#approvalStatusSpan").text('TPT Contract is not available. Please create the contract.');
                                                                                        alert("TPT Contract is not available. Please create the contract.");
                                                                                    } else {
                                                                                        $("#approvalStatusSpan").text('TPT Contract is not approved. Please get approval to proceed further.');
                                                                                        alert("TPT Contract is not approved. Please get approval to proceed further.");
                                                                                    }
                                                                                }
                                                                            }
                                                                        } else {
                                                                            var actionOpt = document.trip.actionName;
                                                                            var optionVar = new Option("-select-", '0');
                                                                            actionOpt.options[0] = optionVar;
                                                                            optionVar = new Option("Cancel Order", 'Cancel');
                                                                            actionOpt.options[1] = optionVar;
                                                                            optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                                            actionOpt.options[2] = optionVar;
                                                                            optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                                            actionOpt.options[3] = optionVar;
                                                                            $("#actionDiv").show();
                                                                            $("#save").hide();
                                                                            document.getElementById("orderRevenue").value = "0";
                                                                            $("#orderRevenueTemp").val('0');
                                                                            $("#orderRevenueSpan").text('0');
                                                                            if (tempArr7[0] == 0) {
                                                                                $("#approvalStatusSpan").text('Customer Contract is not available. Please create the contract.');
                                                                                alert("Customer Contract is not available. Please create the contract.");
                                                                            } else if (tempArr7[0] == 2) {
                                                                                $("#approvalStatusSpan").text('Customer Contract is not approved. Please get approval to proceed further.');
                                                                                alert("Customer Contract is not approved. Please get approval to proceed further.");
                                                                            } else {
                                                                                $("#approvalStatusSpan").text('Customer Contract is not approved. Please get approval to proceed further.');
                                                                                alert("Customer Contract is not available. Please create the contract.");
                                                                            }
                                                                        }

                                                                    }

                                                                } else {
                                                                    //                                                            alert('no data found for expense....');
                                                                }
                                                            }
                                                        });
                                                    }
                                                } else {
                                                    document.getElementById("estimatedExpense").value = 0;
                                                    document.getElementById("orderExpense").value = 0;
//                                                    document.getElementById("orderRevenue").value = $("#orderRevenueTemp").val();
                                                    $("#projectedProfit").text("0.00");
                                                    $("#projectedMargin").text("0.00");
                                                    $("#projectedExpense").text("0.00");
//                                                    $("#orderRevenueSpan").text("0.00");


                                                }
                                            }


                                        }
                                        function setRCMForLCLVehicleType(select) {//alert("setRCMForLCLVehicleType");



                                            var ownership = document.getElementById("ownership").value;
                                            //    alert(ownership);
                                            //       var pointIds==document.getElementsByName("pointId");

                                            var sourceId = document.getElementById("originId").value;
                                            var destinationId = document.getElementById("destinationId").value;

                                            var vehicleTypeIdTemp = document.getElementById("vehicleTypeName").value;
                                            var temp2 = vehicleTypeIdTemp.split("-");
                                            var vehicleTypeId = temp2[0];
                                            var vendorIdTemp = document.getElementById("vendor").value;
                                            var movementType = document.getElementById("movementType").value;

                                            var temp = vendorIdTemp.split("~");
                                            var vendorId = temp[0];
//                                            alert("vendorId"+vendorId);
//                                            alert("vehicleTypeId"+vehicleTypeId);
//                                            alert("sourceId"+sourceId);
//                                            alert("destinationId"+destinationId);
                                            //  alert("selectContainer"+selectContainer);
                                            $.ajax({
                                                url: "/throttle/getRCMForLCLVehicleType.do",
                                                dataType: "text",
                                                data: {
                                                    vehicleTypeId: vehicleTypeId,
                                                    vendorId: vendorId,
                                                    sourceId: sourceId,
                                                    destinationId: destinationId,
                                                    movementType: movementType
                                                },
                                                success: function (temp) {

                                                    if (temp != '') {



                                                        var temp1 = temp.split("#");
                                                        var temp2 = temp1[1].split("@");
                                                        var temp3 = temp2[1].split("-");
                                                        var temp4 = temp2[1].split("~");

                                                        var tollN = temp4[4];
                                                        var driverBachatN = temp4[5];
                                                        var miscN = temp4[7];
                                                        var perDayCost = temp4[0];
                                                        var tripCost = temp4[1];

                                                        var vendorRateWithReeferN = 0;
                                                        var vendorRateWithOutReeferN = temp4[9];
                                                        var vendorApprovalStatus = temp4[10];

//                                                            alert("fuel="+temp4[2]);
//                                                            alert("perDayCost="+temp4[0]);
//                                                            alert("tripCost="+temp4[1]);
//                                                            alert("toll="+temp4[4]);
//                                                            alert("bhata="+temp4[5]);
//                                                            alert("misc="+temp4[7]);
//                                                            alert("vendorApprovalStatus="+temp4[10]);
//                                                            alert("vendorRateWithOutReeferN="+temp4[9]);
                                                        //New Insert
                                                        var fuelLtrs = temp4[2];
                                                        var fuelAmounts = document.getElementById("totalFuelAmount").value;
//                                                        alert("fuelAmounts="+fuelAmounts)
                                                        if (fuelAmounts == 0) {
                                                            document.getElementById("fuelLtrs1").value = fuelLtrs;
                                                        }
                                                        if (vendorId != "1" && vendorApprovalStatus == "1") {
                                                            document.getElementById("estimatedExpense").value = vendorRateWithOutReeferN;
                                                        } else {
                                                            document.getElementById("estimatedExpense").value = parseFloat(tollN) + parseFloat(driverBachatN) + parseFloat(miscN) + parseFloat(fuelAmounts)
                                                                    + parseFloat(tripCost);
                                                            document.getElementById("projectedExpense").innerHTML = "INR: " + parseInt(document.getElementById("estimatedExpense").value);
                                                            document.getElementById("orderExpense").value = parseInt(document.getElementById("estimatedExpense").value);
                                                        }

//                                                        alert(parseInt(document.getElementById("orderRevenue").value));
//                                                        alert(parseInt(document.getElementById("orderExpense").value));
                                                        if (parseInt(document.getElementById("orderRevenue").value) > 0 && parseInt(document.getElementById("estimatedExpense").value) > 0) {
                                                            document.getElementById("projectedProfit").innerHTML = ((parseInt(document.getElementById("orderRevenue").value)
                                                                    - parseInt(document.getElementById("orderExpense").value))).toFixed(2);
                                                            document.getElementById("projectedMargin").innerHTML = ((parseInt(document.getElementById("orderRevenue").value)
                                                                    - parseInt(document.getElementById("orderExpense").value)) * 100 / parseInt(document.getElementById("orderRevenue").value)).toFixed(2) + '%';
                                                        }

                                                    } else {
//                                                        alert('elseee');
                                                    }
                                                }
                                            });
//                                            alert("enddd")
                                            $('#actionName').empty();
                                            var actionOpt = document.trip.actionName;
                                            var optionVar = new Option("Freeze", '1');
                                            actionOpt.options[0] = optionVar;
                                            $("#save").show();

                                        }





                                        var isMultiPle = 0;
                                        isMultiPle = "<%=request.getAttribute("isMultiPle")%>";


                                        function setRepoDiv() {

                                            if (document.getElementById("repo").checked == true) {
                                                document.getElementById("repoDiv").style.display = 'block';
                                                document.getElementById("fclLeg").style.display = 'none';
                                                document.getElementById("repoId").value = 1;
                                                document.getElementById("totalkm").value = 0;
                                            } else {

                                                document.getElementById("repoDiv").style.display = 'none';
                                                document.getElementById("fclLeg").style.display = 'block';
                                                document.getElementById("repoId").value = 0;
                                                setKmVakues("str", "textn", "fcl");
                                            }
                                        }
                                        function setChangeRouteDiv() {

                                            if (document.getElementById("tempRepo").checked == true) {
                                                document.getElementById("repoDiv").style.display = 'block';
                                                document.getElementById("fclLeg").style.display = 'none';
                                                document.getElementById("repoId").value = 1;
                                                document.getElementById("totalkm").value = 0;
                                            } else {

                                                document.getElementById("repoDiv").style.display = 'none';
                                                document.getElementById("fclLeg").style.display = 'block';
                                                document.getElementById("repoId").value = 0;
                                                setKmVakues("str", "textn", "fcl");
                                            }
                                        }

                                        var rowCount3 = 1;
                                        var sno = 0;
                                        var rowCount2 = 1;
                                        var sno11 = 1;
                                        var sno2 = 0;
                                        var sno5 = 0;
                                        var httpRequest;
                                        var httpReq;
                                        var styl = "";
                                        <% int counter1 = 1; %>
                                        function addRowRepo() {
                                            //  alert("hellooo");
                                            if (parseInt(rowCount2) % 2 == 0)
                                            {
                                                styl = "text2";
                                            } else {
                                                styl = "text1";
                                            }


                                            sno2++;

                                            var pointPlanDate = document.getElementsByName("pointPlanDate");
                                            //alert(pointPlanDate.length);
                                            var pointPlanDateVal = pointPlanDate[0].value;
                                            var pointPlanTimeHrs = document.getElementsByName("pointPlanTimeHrs");
                                            var pointPlanTimeMins = document.getElementsByName("pointPlanTimeMins");

                                            var tab = document.getElementById("repoTBL");
                                            //find current no of rows

                                            var rowCountNew = document.getElementById('repoTBL').rows.length;
                                            //  var rowCountNew = 2;
                                            var labelConsignmentName = document.getElementById('labelConsignmentOrderNo' + sno11).value;
                                            rowCountNew--;
                                            var newrow = tab.insertRow(rowCountNew);
                                            cell = newrow.insertCell(0);
                                            var cell0 = "<td class='text1' height='25' style='width:10px;'>" + labelConsignmentName + "</td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;

                                            cell = newrow.insertCell(1);
                                            cell0 = "<td class='text1' height='25'><select type='text' value='' style='width:120px;'  name='pointNameRepo'  id='pointNameRepo" + sno11 + "' onchange='getDistance(" + sno11 + ");'    ><option value='0'>--select--</option><c:forEach items="${orderPointDetails}" var="consignment"><option value='<c:out value="${consignment.pointId}" />'><c:out value="${consignment.pointName}" /></option></c:forEach></select><input type='hidden' name='orderId' value='<c:out value="${consignment.consignmentOrderId}" />' /> <input type='hidden' name='pointIdRepo' id='pointIdRepo" + sno11 + "'  value='<c:out value="${consignment.pointId}" />' /></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(2);
                                            cell0 = "<td class='text1' height='25'><select type='text' value='' style='width:120px;'  name='pointTypeRepo'  id='pointTypeRepo" + sno11 + "'   value='' ><option value='Starting Point'>Starting Point</option><option value='Container Pickup'>Container Pickup</option><option value='Container Drop'>Container Drop</option><option value='Loading Point'>Loading Point</option><option value='Unoading Point'>Uloading Point</option><option value='Final Drop Point'>Drop Point</option></select>   <input type='hidden' name='pointOrderRepo' value='<%=counter1%>'/></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(3);
                                            cell0 = "<td class='text1' height='25'><input type='textarea' value='' style='width:240px;'  name='pointAddresssRepo'  id='pointAddresssRepo" + sno11 + "'   value='' ></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            cell = newrow.insertCell(4);
                                            cell0 = "<td class='text1' height='25'><input type='text' style='width:120px;'  name='pointPlanDateRepo'  id='pointPlanDateRepo" + sno11 + "' value='" + pointPlanDateVal + "'   class='datepicker'></td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;
                                            //	                                 cell = newrow.insertCell(5);
                                            //	                                  cell0 = "<td class='text1' height='25'>HH:<input type='text' value='' style='width:30px;'  name='pointPlanTimeHrsRepo'  id='pointPlanTimeHrs"+ sno1 +"'   value='' ></td>";
                                            //	                                  cell.setAttribute("className", styl);
                                            //                                         cell.innerHTML = cell0;

                                            cell = newrow.insertCell(5);
                                            cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanTimeHrsRepo'  id='pointPlanTimeHrsRepo" + sno11 + "' onChange='validateTripSchedule();validateTransitTime(" + sno11 + ");'class='textbox'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03' selected >03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanTimeMinsRepo'   id='pointPlanTimeMinsRepo" + sno11 + "'   class='textbox'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                                            // cell0 = "<td class='text1' height='25'>MM:<input type='text' value='' style='width:30px;'  name='pointPlanTimeMinsRepo'  id='pointPlanTimeMins"+ sno1 +"'   value='' > </td>";
                                            cell.setAttribute("className", styl);
                                            cell.innerHTML = cell0;

                                            rowCount2++;
                                            sno++;
                                            sno11++;
                                            sno2++;
                                        <% counter1++; %>

                                            $(".datepicker").datepicker({
                                                /*altField: "#alternate",
                                                 altFormat: "DD, d MM, yy"*/
                                                changeMonth: true, changeYear: true
                                            });
                                        }


                                    </script>
                                    <td><font color="red">*</font>Vehicle No</td>
                                    <td style="display:block;" id="vehOwn">
                                        <c:if test="${movementType != 14 && movementType != 16}">                                    
                                            <select name="vehicleNo" id="vehicleNo"  class="select2 input-default form-control" onchange="resetAll();
                                                    setRCMForVehicleType('1');
                                                    computeVehicleCapUtil();
                                                    setContainer();" style="width:240px;height:40px;" >
                                                <option value="0" selected>--Select--</option>
                                            </select>
                                        </c:if>
                                        <c:if test="${movementType == 14 || movementType == 16}">                                    
                                            <select name="vehicleNo" id="vehicleNo"  class="select2 input-default form-control" onchange="resetAll();
                                                    setRCMForLCLVehicleType('1');
                                                    computeVehicleCapUtil();" style="width:240px;height:40px;" >
                                                <option value="0" selected>--Select--</option>
                                            </select>
                                        </c:if>

                                        <script>
                                            $('#vehicleNo').select2({placeholder: 'Fill vehicleNo'});
                                        </script>


                                    </td>
                                    <td style="display:none;" id="vehHire" >
                                        <input type="text" id="hireVehicleNo" name="hireVehicleNo" class="form-control" style="width:240px;height:40px;" value="Hired 001" />

                                    </td>
                                    <input type="hidden" name="ownership" id="ownership" class="form-control" value="0">
                                    <input type="hidden" name="vehicleId" Id="vehicleId" class="form-control" value="0">
                                    <input type="hidden" name="vendorId" Id="vendorId" class="form-control" value="">
                                    <td>Vehicle Capacity (MT)</td>
                                    <td><input type="text" name="vehicleTonnage" Id="vehicleTonnage" readonly class="form-control" style="width:240px;height:40px;" value="<c:out value="${vehicleTonnage}"/>"></td>
                                    <td id="remain1" style="display: none;">Remain Order Weight</td>
                                    <td id="remain2" style="display: none;"><input type="text" name="remainWeight" Id="remainWeight" readonly class="form-control" style="width:240px;height:40px;" value="0"></td>
                                    </tr>

                                    <tr>
                                        <td>Veh. Cap [Util%]</td>
                                        <td><input type="text" name="vehicleCapUtil" Id="vehicleCapUtil" readonly class="form-control" style="width:240px;height:40px;" value='<c:out value="${vehicleCapUtilValue}" />'></td>
                                        <td>Special Instruction</td>
                                        <td><textarea name="tripRemarks" cols="20" rows="2" class="form-control" > <c:out value="${consginmentRemarks}" /></textarea></td>
                                        <td style="display: none">Trip Schedule</td>
                                        <td style="display: none"><c:out value="${tripSchedule}" /></td>
                                    </tr>


                                    <tr>
                                        <td>Primary Driver </td>
                                        <td>
                                            <input type="text" name="driver1Name"  Id="driver1Name" class="form-control" style="width:240px;height:40px;" value="<c:out value="${primaryDriverName}"/>"  >
                                            <input type="hidden" name="driver1Id"   Id="driver1Id" class="form-control" style="width:240px;height:40px;"  style="display: none" value="" >
                                            <select id="driverName" name="driverName" onchange="setDriverId();" style="display: none">
                                                <c:if test="${driverList != null}">
                                                    <option value="" selected>--Select--</option>
                                                    <c:forEach items="${driverList}" var="driverList">
                                                        <option value='<c:out value="${driverList.empId}"/>'><c:out value="${driverList.empName}"/></option>
                                                    </c:forEach>
                                                </c:if>
                                            </select>
                                            <input type="hidden" name="driver1Id" Id="driver1Id" class="form-control" value='0'>

                                        </td>
                                        <td> Mobile No </td>
                                        <!--<td ><label id="mobileNo"></label></td>-->
                                        <td ><input type="text" id="mobileNo" name="mobileNo" class="form-control"   style="width:240px;height:40px;"  maxlength="13" onKeyPress="return onKeyPressBlockCharacters(event);" ></td>

                                    </tr>
                                    <tr>
                                        <td>Driver License No</td>
                                        <!--<td ><label id="licenseNo"></label></td>-->
                                        <td ><input type="text" id="licenseNo" name="licenseNo"  class="form-control" style="width:240px;height:40px;"></td>
                                        <td>License Expiry Date</td>
                                        <td ><input type="text" id="licenseDate" name="licenseDate" readonly class="form-control" style="width:240px;height:40px;"></td>
                                    </tr>
                                    <tr>
                                        <td >Cleaner </td>
                                        <td >
                                            <input type="text" name="driver2Name"   Id="driver2Name" class="form-control" style="width:240px;height:40px;" value="<c:out value="${secondaryDriver1Name}"/>"  >
                                            <input type="hidden" name="driver2Id" Id="driver2Id" class="form-control" style="width:240px;height:40px;" value='0'  >
                                            <input type="hidden" name="driver3Name"  readonly  Id="driver3Name" class="form-control" style="width:240px;height:40px;"value="<c:out value="${secondaryDriver2Name}"/>"  >
                                            <input type="hidden" name="driver3Id" Id="driver3Id"class="form-control" style="width:240px;height:40px;" value='0' >
                                        </td>
                                        <!--2,28,29,30-->
                                        <c:if test="${movementType == 2 || movementType == 28 || movementType == 29 || movementType == 30}">

                                            <td  >Bill of Entry: </td>
                                            <td  ><input type="text" id="billOfEntry" name="billOfEntry" value="<c:out value="${billOfEntry}" />" style="width:240px;" maxlength = "50" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            </c:if>
                                        <!--1,25,26,27,31,32-->
                                        <c:if test="${movementType == 1 || movementType == 25 || movementType == 26  || movementType == 27 || movementType == 31 || movementType == 32}">
                                            <td  >Shipping Bill No:</td>
                                            <td  > <input type="text" id="shipingLineNo" name="shipingLineNo" value="<c:out value="${shipingLineNo}" />" style="width:240px;" maxlength = "50" class="form-control" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                            </c:if>
                                    </tr>
                                    <tr style="display: none">
                                        <td>Product / Temp Info </td>
                                        <td><c:out value="${productInfo}" /></td>

                                    </tr>
                                </table>
                                <br/>
                                <br/>
                                <div id="vehicleCheck" style="display:none;">

                                    <!--<table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">-->
                                   <table   class="table table-info mb30 table-hover" style="margin-left: 10px;width:100%" id="bg">  
                                        <tr id="tableDesingTH" height="30">
                                            <td class="contenthead" colspan="4" >Vehicle Compliance Check</td>
                                        </tr>
                                        <tr>
                                            <td>Vehicle FC Valid UpTo</td>
                                            <td><label id="fcExpiryDate"><font color="green"></font></label></td>
                                        </tr>
                                        <tr>
                                            <td>Vehicle Insurance Valid UpTo</td>
                                            <td><label id="insuranceExpiryDate"><font color="green"></font></label></td>
                                        </tr>
                                        <tr>
                                            <td>Vehicle Permit Valid UpTo</td>
                                            <td><font color="green"><span id="permitExpiryDate"></font></span></td>
                                        </tr>
                                        <tr>
                                            <td>Road Tax Valid UpTo</td>
                                            <td><label id="roadExpiryDate"><font color="green"></font></label></td>
                                        </tr>
                                    </table>
                                    <br/>
                                </div>

                                <center>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                                <br>
                                <br>

                            </div>
                            <div id="containers" class="tab-pane" >
                                <%if((Integer)request.getAttribute("consgncontainerLength") != 0){%>
                                <c:if test = "${consgncontainer != null}" >
                                    <input type="hidden" id="checkedContainerCount" name="checkedContainerCount" value="0"/>
                                    <center><span id="containerStatus" style="color: red"></span></center>


                                    <table class="table table-info mb30 table-hover" style="width:100%;" id="containerTBL1" >
                                        <thead>
                                            <tr>
                                                <th class="contenthead">Sno </th>
                                                <th class="contenthead">Cust.Ref.No</th>
                                                <th class="contenthead">Container Type</th>
                                                <th class="contenthead">Required Vehicle Type</th>
                                                <th class="contenthead">Liner</th>
                                                <th class="contenthead">Container No</th>
                                                <th class="contenthead">Wgt(Tonnage)</th>
                                                <th class="contenthead">Seal No</th>
                                                <th class="contenthead">Goods Desc</th>
                                                <th class="contenthead">Freight</th>
                                                <th class="contenthead">Select</th>
                                            </tr>
                                        </thead>
                                        <% int index = 0;
                                           int sno = 1;
                                        %>
                                        <c:forEach items="${consgncontainer}" var="no">
                                            <tr>
                                                <td  ><%=sno%>
                                                    <input type="hidden" class="form-control" style="width:100px;height:40px;" name="uniqueId" id="uniqueId<%=sno%>" value="<c:out value="${no.uniqueId}"/>"/>
                                                </td>
                                                <td><c:out value="${no.customerOrderReferenceNo}"/></td>
                                                <td  >
                                                    <input type="hidden" name="orderIds" id="orderIds<%=sno%>" value="<c:out value="${no.orderId}"/>"/>
                                                    <input type="hidden" name="containerTypeId" id="containerTypeId<%=sno%>" value="<c:out value="${no.containerId}"/>"/>
                                                    <input type="hidden" name="vehicleTypeIdVar" id="vehicleTypeIdVar<%=sno%>" value="<c:out value="${no.vehicleTypeId}"/>"/>
                                                    
                                                    <input type="text" class="form-control" style="width:60px;"  maxlength="11" name="containerTypeName" id="containerTypeName<%=sno%>" value="<c:out value="${no.containerName}"/>" readonly/>
                                                </td>
                                                <td  >

                                                    <input type="text" class="form-control" style="width:60px;" name="vehicleTypeNames" id="vehicleTypeNames<%=sno%>" value="<c:out value="${no.vehicleTypeName}"/>" readonly/>
                                                </td>
                                                <td  >

                                                    <input type="text" class="form-control" style="width:60px;" name="linerName" id="linerName<%=sno%>" value="<c:out value="${no.linerName}"/>" readonly/>
                                                </td>

                                                <td >
                                                    <input type="text" class="form-control" style="width:150px;height:40px;" maxlength='11' name="containerNo" id="containerNo<%=sno%>" value="<c:out value="${no.containerNo}"/>" onkeypress="return onKeyPressBlockCharacters1(<%=sno%>, event);" onchange="checkContainerNo(<%=sno%>, this.value, 1,<c:out value="${no.orderId}"/>);checkContainerNoInTripExists(this.value,<%=sno%>);"/>
                                                    <script>
                                                        function selectCheckBox(val) {
                                                            //alert(val);
                                                            // alert(document.getElementById("selectedIndex"+val).value);
                                                            document.getElementById("selectedIndex" + val).checked = true;
                                                            resetRevenue();
                                                        }
                                                    </script>

                                                </td>

                                                <td  >
                                                    <input type="text" class="form-control" style="width:90px;" name="tonnage" id="tonnage<%=sno%>" value="<c:out value="${no.tonnage}"/>"/>
                                                </td>
                                                <td  >
                                                    <input type="text" class="form-control" style="width:90px;" name="sealNo" id="sealNo<%=sno%>" value="<c:out value="${no.sealNo}"/>"/>
                                                    <input type="hidden" name="grStatus" id="grStatus<%=sno%>" value="To be billed" >
                                                </td>
                                                <td  >
                                                    <input type="text" class="form-control" style="width:90px;" name="goodsDesc" id="goodsDesc<%=sno%>" value="<c:out value="${no.description}"/>"/>
<!--                                                    <select  name="grStatus" id="grStatus<%=sno%>" class="form-control" style="width:120px;height:40px;">
                                                        <option value="To be billed" selected>To be billed</option>
                                                        <option value="Paid">Paid</option>
                                                    </select>-->
                                                </td>
                                                <td><input type="text" name="freightAmount" id="freightAmount<%=sno%>" value="<c:out value="${no.freightAmount}"/>"/></td>
                                                <td  style="width:10px">

<!--<input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" value="<%=sno%>"  onclick="preRevenueCal(<%=sno%>, this, 1,<c:out value="${no.orderId}"/>)" />-->
                                                    <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" value="<%=sno%>"  onclick="calculateTripRevenue(<%=sno%>, this, 1,<c:out value="${no.orderId}"/>);checkContainerNoInTripExists(this.value,<%=sno%>);" />

                                                </td>
                                            </tr>
                                            <%index++;%>
                                            <%sno++;%>
                                        </c:forEach >
                                    </table>

                                </c:if>                            
                                <%}%>


                                <!--                           planned container-->

                                <c:if test = "${consgnPlannedcontainer != null}" >


                                    <table class="table table-info mb30 table-hover" style="width:100%;"  >
                                        <thead>
                                            <tr>
                                                <th class="contenthead">Sno</th>
                                                <th class="contenthead">Container Type</th>
                                                <th class="contenthead">Required Vehicle Type</th>
                                                <th class="contenthead">Liner</th>
                                                <th class="contenthead">Container No</th>
                                                <th class="contenthead">Seal No</th>
                                                <th class="contenthead"> GR Status</th>

                                            </tr>
                                        </thead>
                                        <% int index7 = 0;
                                           int sno7 = 1;
                                        %>
                                        <c:forEach items="${consgnPlannedcontainer}" var="no">
                                            <tr>
                                                <td  ><%=sno7%>
                                                </td>
                                                <td  >
                                                    <c:out value="${no.containerName}"/>

                                                </td>
                                                <td  >

                                                    <c:out value="${no.vehicleTypeName}"/>
                                                </td>
                                                <td  >

                                                    <c:out value="${no.linerName}"/>
                                                </td>

                                                <td >
                                                    <c:out value="${no.containerNo}"/>

                                                </td>
                                                <td  >
                                                    <c:out value="${no.sealNo}"/>
                                                </td>
                                                <td  >
                                                    <c:out value="${no.grStatus}"/>
                                                </td>

                                            </tr>
                                            <%index7++;%>
                                            <%sno7++;%>
                                        </c:forEach >
                                    </table>

                                </c:if>

                                <center>
                                    <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                    <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                </center>
                                <script>
                                    function showLog() {
                                        alert("check");
                                    }
                                </script>



                                <script>
                                    function checkContainerNo(sno, value, type, orderId) {


                                        var tempContainerNo = "";
                                        $.ajax({
                                            url: "/throttle/getPlannedContainerNo.do",
                                            dataType: "text",
                                            data: {
                                                orderId: orderId

                                            },
                                            success: function (temp) {
                                                if (temp != '') {
                                                    //   alert("temp:"+temp);
                                                    tempContainerNo = temp.split(",");
                                                    for (var i = 0; i < tempContainerNo.length; i++) {
                                                        if (value == tempContainerNo[i]) {
                                                            alert("This container No has been already planned !check  Container No");
                                                            document.getElementById("containerNo" + sno).value = "";
                                                        }
                                                    }


                                                }

                                            }
                                        });

                                    }
                                    function calOtherVehTypeRevenue(val) {
                                        var movementType = document.getElementById("movementType").value;
                                        if (parseInt(movementType) != 7) {
                                            var temp = val.split("-");
                                            if (temp[0] != '1058' && temp[0] != '1059' && temp[0] != '1060') {

                                                document.getElementById("containers").style.visibility = "hidden";
                                                //document.getElementById("tabDetails").children[1].style.display = "none";

                                                //document.getElementById("selectedIndex1").checked = true;
                                                //document.getElementById("sealNo1").value="0";
                                                calculateTripRevenue('1', '', '', '');
                                            }/*else{
                                             document.getElementById("selectedIndex1").checked = false;
                                             document.getElementById("container").style.visibility = "visible";
                                             document.getElementById("tabDetails").children[1].style.display = "block";
                                             }*/

                                        }
                                    }
                                    function resetAll() {
//                                        alert("reset ...");
                                        $("#approvalStatusSpan").text('');
                                        var selectedIndex = document.getElementsByName("selectedIndex");
                                        for (var i = 0; i < selectedIndex.length; i++) {
                                            document.getElementById("selectedIndex" + (i + 1)).checked = false;
                                        }


                                        var actionOpt = document.trip.actionName;
                                        var optionVar = new Option("-select-", '0');
                                        actionOpt.options[0] = optionVar;
                                        optionVar = new Option("Cancel Order", 'Cancel');
                                        actionOpt.options[1] = optionVar;
                                        optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                        actionOpt.options[2] = optionVar;
                                        optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                        actionOpt.options[3] = optionVar;
                                        $("#actionDiv").show();
                                        $("#save").hide();
                                        document.getElementById("totalFuelLtrs").value = 0;
                                        document.getElementById("fuelLtrs1").value = 0;
                                        document.getElementById("fuelAmount1").value = 0;
                                        document.getElementById("bunkName1").value = "0~0~0~0";
                                        document.getElementById("totalFuelAmount").value = 0;
                                        document.getElementById("estimatedExpense").value = 0;
                                        document.getElementById("orderExpense").value = 0;
                                        document.getElementById("orderRevenue").value = $("#orderRevenueTemp").val();
                                        $("#projectedProfit").text("0.00");
                                        $("#projectedMargin").text("0.00");
                                        $("#projectedExpense").text("0.00");
//                                        $("#orderRevenueSpan").text("0.00");

                                        //global variables
                                        containerTypeIdSelected = 0;
                                        noOfContainerSelected = 0;
                                        totalFuelRequiredN = 0;
                                        marketHireRateN = 0;


//                                        document.getElementById('vehicleNo').reset();
//                                        $('vehicleNo.select2-search-choice').remove();
//                                       $('#vehicleNo')[0].reset();
//                                       var vehicleNo = document.trip.vehicleNo;
//                                        var optionVariable = new Option("-select-", '0');
//                                        vehicleNo.options[0] = optionVariable;


                                        var e = document.getElementById("vehicleCheck");
                                        e.style.display = 'none';

//                                        $('#vehicleNo').val('0').trigger('change');

//                                        $('#vehicleNo').select2("data", "0");
//                                        $("#vehicleNo > option").removeAttr("selected");
//                                        $("#vehicleNo").trigger("change");
//                                        $('#vehicleNo').last().val('').trigger('change.select2');
//                                             $('#vehicleNo').select2({placeholder: 'Fill vehicleNo'});
//                                        $('#vehicleNo').select2('val', 0);
//$('#vehicleNo').attr('value', '');
//                                        $('#vehicleNo').empty();
//                                        $('#vehicleNo').append(
//                                                $('<option></option>').val(0).html('--select--'));
                                        $('#fcExpiryDate').text('');
                                        $('#insuranceExpiryDate').text('');
                                        $('#permitExpiryDate').text('');
                                        $('#roadExpiryDate').text('');

                                        var vendorTemp = document.getElementById("vendor").value;
                                        //                                alert(vendorTemp)
                                        var temp = vendorTemp.split("~");
                                        //alert("setVehicleType2:"+temp[1]);

                                        if (temp[1] == '1' || temp[1] == '0') {

                                            $("tr.vehCategory").hide();
                                            $("#vehHire").hide();
                                            $("#vehOwn").show();
                                            //alert('1');
//                                            resetCopobox()
                                        } else {

                                            $("tr.vehCategory").show();
                                            $("#vehHire").show();
                                            $("#vehOwn").hide();
                                            //alert('2');
//                                             resetCopobox()
                                        }

                                    }



                                    function calculateTripRevenue(sno, obj, type, orderId) { // final
                                        containerTypeIdSelected = 0;
                                        noOfContainerSelected = 0;
                                        //reset value

                                        var vehicleTypeIdSelected = document.getElementById("vehicleTypeIdSelected").value;
                                        var sinTwentyContainer = document.getElementById("sinTwentyContainer").value;
                                        if (vehicleTypeIdSelected == 1059 && sinTwentyContainer == 1 || vehicleTypeIdSelected == 1060 && sinTwentyContainer == 1) {
                                            vehicleTypeIdSelected = vehicleTypeIdSelected;

                                        } else if (vehicleTypeIdSelected == 1060 && sinTwentyContainer == 1) {
                                            vehicleTypeIdSelected = vehicleTypeIdSelected;
                                        } else {
                                            vehicleTypeIdSelected = vehicleTypeIdSelected;
                                        }
                                        //reset value
                                        if (vehicleTypeIdSelected == 1060 || vehicleTypeIdSelected == 1059 || vehicleTypeIdSelected == 1058) {

//                                            document.getElementById("orderRevenue").value = 0;
//                                            $("#orderRevenueTemp").val("0");
//                                            $("#orderRevenueSpan").text("0.00");
                                            $("#approvalStatusSpan").text('');
                                            document.getElementById("estimatedExpense").value = 0;
                                            document.getElementById("orderExpense").value = 0;
                                            $("#projectedProfit").text("0.00");
                                            $("#projectedMargin").text("0.00");
                                            $("#projectedExpense").text("0.00");
//                                            $("#orderRevenueSpan").text("0.00");
                                            $("#save").hide();

                                            //$("#projectedMargin").text("0.00");
                                            //$("#projectedExpense").text(
                                            //
                                            //"0.00");(

                                            var tripRevenue = 0;
//                                            if (document.getElementById("sealNo" + sno).value == '') {
//                                                alert("Please Enter seal No");
//                                                document.getElementById("selectedIndex" + sno).checked = false;
//                                                return;
//                                            }
                                            var pointIds = document.trip.pointId;
                                            var routeContractId = document.getElementById("routeContractId").value;
                                            var productInfo = document.getElementById("productInfo").value;
                                            var movementType = document.getElementById("movementType").value;
                                            var containerTypeId = document.getElementById("containerTypeId" + sno).value;
                                            var vehicleTypeId = document.getElementById("vehicleTypeIdVar" + sno).value;
                                            //                                    var vehicleTypeIdSelected = document.getElementById("vehicleTypeIdSelected").value;
                                            var orderId = document.getElementById("orderIds" + sno).value;

                                            //                                    if (vehicleTypeIdSelected == 1059 && sinTwentyContainer == 1) {
                                            //                                        vehicleTypeIdSelected = "1058";
                                            //
                                            //                                    } else {
                                            //                                        vehicleTypeIdSelected = vehicleTypeIdSelected;
                                            //                                    }
                                            var alreadySelectedId = "";
                                            var checkk = 0;
                                            var flag = false;


                                            var loadType = 0;

                                            //                                    if (movementType == 3 || movementType == 9) {
                                            //                                    if (movementType == 3 || movementType == 9 || movementType == 11) {
                                            if (movementType == 3) {
                                                loadType = 2;
                                            } else {
                                                loadType = 2;
                                            }
                                            var isContainerSelectionComplete = false;
                                            var resetVar = true;
                                            //compute 20' selected and 40'selected start
                                            var twentyFeetContainer = 0;
                                            var fortyFeetContainer = 0;
                                            var routeContractId = document.getElementById("routeContractId").value;
                                            var routeTemp = routeContractId.split(",");
                                            var selectedIndex = document.getElementsByName("selectedIndex");
                                            var sinTwentyContainer = document.getElementById("sinTwentyContainer").value;

                                            //alert(isContainerSelectionComplete +":"+resetVar);
                                            for (var i = 0; i < selectedIndex.length; i++) {
                                                if (document.getElementById("selectedIndex" + (i + 1)).checked == true) {
                                                    if (document.getElementById("containerTypeId" + (i + 1)).value == 1) {
                                                        twentyFeetContainer++;
                                                    } else if (document.getElementById("containerTypeId" + (i + 1)).value == 2) {
                                                        fortyFeetContainer++;
                                                    }
                                                }
                                            }

                                            for (var i = 0; i < selectedIndex.length; i++) {
                                                if (document.getElementById("selectedIndex" + (i + 1)).checked == true &&
                                                        document.getElementById("containerTypeId" + (i + 1)).value == 1 &&
                                                        checkk == 0) {
                                                    alreadySelectedId = document.getElementById("orderIds" + (i + 1)).value;
                                                    checkk = checkk + 1;
                                                    flag = true;
                                                    break;
                                                }
                                            }

                                            if (!flag) {
                                                checkk = 0;
                                            }
                                            //                                 if(vehicleTypeIdSelected == 1059 || vehicleTypeIdSelected ==1058){
                                            if (routeTemp.length > 1) {
                                                if (fortyFeetContainer > 0) {
                                                    alert("Please select 2 20Foot Container in 1 40Foot Trailer for 2 order");
                                                }
                                                if (twentyFeetContainer > 0) {
                                                    if (vehicleTypeIdSelected == 1059 && sinTwentyContainer == 0 || vehicleTypeIdSelected == 1060 && sinTwentyContainer == 0) { //40' trailer
                                                        if (twentyFeetContainer > 2) {
                                                            alert("you cannot accomodate more than 2 20Foot Container in 1 40Foot Trailer");

                                                        } else if (twentyFeetContainer == 1) {
                                                            alert("please select one more 20Foot Container second order");
                                                            resetVar = false;
                                                        } else if (twentyFeetContainer == 2 && alreadySelectedId == orderId) {
                                                            alert("Selected container is in same order please select second 20Foot Container in second order");
                                                        } else if (twentyFeetContainer == 2 && alreadySelectedId != orderId) {
                                                            isContainerSelectionComplete = true;
                                                        }
                                                    } else {
                                                        isContainerSelectionComplete = true;
                                                    }

                                                    if (!isContainerSelectionComplete && resetVar) {//reset selection
                                                        for (var i = 0; i < selectedIndex.length; i++) {
                                                            document.getElementById("selectedIndex" + (i + 1)).checked = false;
                                                        }
                                                    } else {//fetch rate
                                                        if (vehicleTypeIdSelected == 1059 || vehicleTypeIdSelected == 1060) { //40' trailer
                                                            if (twentyFeetContainer == 2) {
                                                                tripRevenue = getVCR(vehicleTypeIdSelected, 1, 2, loadType);
                                                                containerTypeIdSelected = 1;
                                                                noOfContainerSelected = 2;
                                                            }
                                                        }
                                                        if (parseFloat(tripRevenue) >= 0 && isContainerSelectionComplete) {
                                                            $('#actionName').empty();
                                                            var actionOpt = document.trip.actionName;
                                                            var optionVar = new Option("Freeze", '1');
                                                            actionOpt.options[0] = optionVar;
                                                            $("#save").show();
                                                        } else {
                                                            var actionOpt = document.trip.actionName;
                                                            var optionVar = new Option("-select-", '0');
                                                            actionOpt.options[0] = optionVar;
                                                            optionVar = new Option("Cancel Order", 'Cancel');
                                                            actionOpt.options[1] = optionVar;
                                                            optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                            actionOpt.options[2] = optionVar;
                                                            optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                            actionOpt.options[3] = optionVar;
                                                            $("#actionDiv").show();
                                                            $("#save").hide();
                                                            //resetAll();
                                                        }
                                                        //alert(tripRevenue);
                                                        tripRevenue = parseFloat(tripRevenue).toFixed(2);
//                                                        document.getElementById("orderRevenue").value = tripRevenue;
//                                                        $("#orderRevenueTemp").val(tripRevenue);
//                                                        $("#orderRevenueSpan").text(tripRevenue);
                                                        setRCMForVehicleType('1');
                                                    }
                                                } else {

                                                    alert("Please select correct vehicle Type && container to proceed-----");
                                                    for (var i = 0; i < selectedIndex.length; i++) {
                                                        document.getElementById("selectedIndex" + (i + 1)).checked = false;
                                                        return;
                                                        var actionOpt = document.trip.actionName;
                                                        var optionVar = new Option("-select-", '0');
                                                        actionOpt.options[0] = optionVar;
                                                        optionVar = new Option("Cancel Order", 'Cancel');
                                                        actionOpt.options[1] = optionVar;
                                                        optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                        actionOpt.options[2] = optionVar;
                                                        optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                        actionOpt.options[3] = optionVar;
                                                        $("#actionDiv").show();
                                                        $("#save").hide();
                                                    }

                                                }
                                            } else {

                                                //compute 20' selected and 40'selected end
                                                //    alert(twentyFeetContainer + ":::" + fortyFeetContainer + ":::" + vehicleTypeIdSelected);
                                                if (twentyFeetContainer > 0 || fortyFeetContainer > 0) {
                                                    if (vehicleTypeIdSelected == 1059 && sinTwentyContainer == 0 || vehicleTypeIdSelected == 1060 && sinTwentyContainer == 0) { //40' trailer
                                                        //we can accomdate 1 40' container or 2 20' container
                                                        if (fortyFeetContainer > 1) {
                                                            alert("you cannot accomodate only 1 40Foot Container in 1 40Foot Trailer");
                                                        } else if (fortyFeetContainer == 1) {
                                                            isContainerSelectionComplete = true;
                                                        }
                                                        //                                                if (movementType != 7 && isContainerSelectionComplete == true) {
                                                        if (!isContainerSelectionComplete == true) {
                                                            if (isContainerSelectionComplete && twentyFeetContainer > 0) {
                                                                alert("you can accomodate only 1 40Foot Container in 1 40Foot Trailer");
                                                                isContainerSelectionComplete = false;
                                                            } else if (twentyFeetContainer > 2) {
                                                                alert("you cannot accomodate more than 2 20Foot Container in 1 40Foot Trailer");
                                                            } else if (twentyFeetContainer == 1) {
                                                                $("#save").hide();
                                                                alert("please select one more 20Foot Container");
                                                                resetVar = false;
                                                            } else if (twentyFeetContainer == 2) {
                                                                isContainerSelectionComplete = true;
                                                            }
                                                        }
                                                    } else if (vehicleTypeIdSelected == 1059 && sinTwentyContainer == 1 || vehicleTypeIdSelected == 1060 && sinTwentyContainer == 1) { //20' single trailer on 40' vehicle
                                                        if (fortyFeetContainer > 1 || twentyFeetContainer > 1) {
                                                            alert("please select  single 20' container only for this trip.");
                                                        } else if (twentyFeetContainer == 1) {
                                                            isContainerSelectionComplete = true;
                                                        }

                                                    } else if (vehicleTypeIdSelected == 1058) { //20' trailer
                                                        //we can accomdate 1 20' container    
                                                        if (fortyFeetContainer > 0) {
                                                            alert("you cannot accomodate 40Foot Container in 20Foot Trailer");
                                                        } else if (twentyFeetContainer > 1) {
                                                            alert("you cannot accomodate more than 1 20Foot Container in 1 20Foot Trailer");
                                                        } else if (twentyFeetContainer == 1) {
                                                            isContainerSelectionComplete = true;
                                                        }
                                                    }
                                                    if (!isContainerSelectionComplete && resetVar) {//reset selection
                                                        for (var i = 0; i < selectedIndex.length; i++) {
                                                            document.getElementById("selectedIndex" + (i + 1)).checked = false;
                                                        }
                                                    } else {//fetch rate
                                                        if (vehicleTypeIdSelected == 1059 || vehicleTypeIdSelected == 1060) { //40' trailer
                                                            if (fortyFeetContainer == 1) {
                                                                tripRevenue = getVCR(vehicleTypeIdSelected, 2, 1, loadType);
                                                                containerTypeIdSelected = 2;
                                                                noOfContainerSelected = 1;
                                                            } else if (twentyFeetContainer == 2) {
                                                                tripRevenue = getVCR(vehicleTypeIdSelected, 1, 2, loadType);
                                                                containerTypeIdSelected = 1;
                                                                noOfContainerSelected = 2;
                                                            } // Muthu here for test case....
                                                            else if (vehicleTypeIdSelected == 1059 && twentyFeetContainer == 1 || vehicleTypeIdSelected == 1060 && twentyFeetContainer == 1) {
                                                                tripRevenue = getVCR(vehicleTypeIdSelected, 1, 2, loadType);
                                                                containerTypeIdSelected = 1;
                                                                noOfContainerSelected = 2;
                                                                //                                                        isContainerSelectionComplete = true;
                                                            }
                                                        } else if (vehicleTypeIdSelected == 1058) { //20' trailer
                                                            if (twentyFeetContainer == 1) {
                                                                tripRevenue = getVCR(vehicleTypeIdSelected, 1, 1, loadType);
                                                                containerTypeIdSelected = 1;
                                                                noOfContainerSelected = 1;
                                                            }
                                                        }

                                                        if (parseFloat(tripRevenue) >= 0 && isContainerSelectionComplete) {
                                                            $('#actionName').empty();
                                                            var actionOpt = document.trip.actionName;
                                                            var optionVar = new Option("Freeze", '1');
                                                            actionOpt.options[0] = optionVar;
                                                            $("#save").show();
                                                        } else {
                                                            var actionOpt = document.trip.actionName;
                                                            var optionVar = new Option("-select-", '0');
                                                            actionOpt.options[0] = optionVar;
                                                            optionVar = new Option("Cancel Order", 'Cancel');
                                                            actionOpt.options[1] = optionVar;
                                                            optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                            actionOpt.options[2] = optionVar;
                                                            optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                            actionOpt.options[3] = optionVar;
                                                            $("#actionDiv").show();
                                                            $("#save").hide();
                                                            //resetAll();
                                                        }
                                                    }
                                                    //alert(tripRevenue);
                                                    tripRevenue = parseFloat(tripRevenue).toFixed(2);
//                                                    document.getElementById("orderRevenue").value = tripRevenue;
//                                                    $("#orderRevenueTemp").val(tripRevenue);
//                                                    $("#orderRevenueSpan").text(tripRevenue);
                                                    setRCMForVehicleType('1');
                                                } else {
                                                    alert("Please select correct vehicle Type && container to proceed..");
                                                    for (var i = 0; i < selectedIndex.length; i++) {
                                                        document.getElementById("selectedIndex" + (i + 1)).checked = false;
                                                        return;
                                                        var actionOpt = document.trip.actionName;
                                                        var optionVar = new Option("-select-", '0');
                                                        actionOpt.options[0] = optionVar;
                                                        optionVar = new Option("Cancel Order", 'Cancel');
                                                        actionOpt.options[1] = optionVar;
                                                        optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                        actionOpt.options[2] = optionVar;
                                                        optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                        actionOpt.options[3] = optionVar;
                                                        $("#actionDiv").show();
                                                        $("#save").hide();
                                                    }
                                                    //resetAll();
                                                }
                                            }
                                        } else {
                                            containerTypeIdSelected = 4;
//                                            $("#orderRevenueTemp").val(document.getElementById("orderRevenue").value);
//                                            $("#orderRevenueSpan").text(document.getElementById("orderRevenue").value);
                                            //   alert(document.getElementById("orderRevenue").value)
                                            setRCMForVehicleType('1');
                                            $('#actionName').empty();
                                            var actionOpt = document.trip.actionName;
                                            var optionVar = new Option("Freeze", '1');
                                            actionOpt.options[0] = optionVar;
                                            $("#save").show();

                                            //                                    var sourceId =0,destinationId=0,point1Id=0,point2Id=0;
                                            //                                               if (pointIds.length == '2') {
                                            //                                                    sourceId = pointIds[pointIds.length - 2].value;
                                            //                                                    destinationId = pointIds[pointIds.length - 1].value;
                                            //                                                } else if (pointIds.length == '3') {
                                            //                                                    sourceId = pointIds[pointIds.length - 3].value;
                                            //                                                    point1Id = pointIds[pointIds.length - 2].value;
                                            //                                                    destinationId = pointIds[pointIds.length - 1].value;
                                            //                                                } else if (pointIds.length == '4') {
                                            //                                                    sourceId = pointIds[pointIds.length - 4].value;
                                            //                                                    point1Id = pointIds[pointIds.length - 3].value;
                                            //                                                    point2Id = pointIds[pointIds.length - 2].value;
                                            //                                                    destinationId = pointIds[pointIds.length - 1].value;
                                            //                                                }
                                            //                              //  alert(vehicleTypeId+":"+movementType+":"+loadType+":"+productInfo+":"+routeContractId+":"+sourceId+":"+point1Id+":"+point2Id+":"+destinationId);
                                            //                                    //other vehicle type rates
                                            //                                    $.ajax({
                                            //                                        url: "/throttle/getOtherVehicleTypeRates.do",
                                            //                                        dataType: "text",
                                            //                                        data: {
                                            //                                            vehicleTypeId: vehicleTypeIdSelected,
                                            //                                            movementType : movementType,
                                            //                                            loadType : loadType,
                                            //                                            productInfo : productInfo,
                                            //                                            routeContractId : routeContractId,
                                            //                                            sourceId : sourceId,
                                            //                                            point1Id : point1Id,
                                            //                                            point2Id : point2Id,
                                            //                                            destinationId : destinationId
                                            //                                            
                                            //                                        },
                                            //                                        success: function (temp) {
                                            //                                            if (temp != '') {
                                            //                                                var tripRevenue = parseFloat(temp);
                                            //                                                 //alert("tripRevenue == "+tripRevenue);
                                            //                                                 
                                            //                                                 $('#actionName').empty();
                                            //                                                    var actionOpt = document.trip.actionName;
                                            //                                                    var optionVar = new Option("Freeze", '1');
                                            //                                                    actionOpt.options[0] = optionVar;
                                            //                                                    $("#save").show();
                                            //                                                 
                                            //                                            tripRevenue = parseFloat(tripRevenue).toFixed(2);
                                            //                                            document.getElementById("orderRevenue").value = tripRevenue;
                                            //                                            $("#orderRevenueTemp").val(tripRevenue);
                                            //                                            $("#orderRevenueSpan").text(tripRevenue);
                                            //                                            setRCMForVehicleType('1');
                                            //                                                
                                            //
                                            //                                            }else{
                                            //                                                alert("rate is not available .please check");
                                            //                                            for (var i = 0; i < selectedIndex.length; i++) {
                                            //                                                document.getElementById("selectedIndex" + (i + 1)).checked = false;
                                            //                                                return;
                                            //                                                var actionOpt = document.trip.actionName;
                                            //                                                var optionVar = new Option("-select-", '0');
                                            //                                                actionOpt.options[0] = optionVar;
                                            //                                                optionVar = new Option("Cancel Order", 'Cancel');
                                            //                                                actionOpt.options[1] = optionVar;
                                            //                                                optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                            //                                                actionOpt.options[2] = optionVar;
                                            //                                                optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                            //                                                actionOpt.options[3] = optionVar;
                                            //                                                $("#actionDiv").show();
                                            //                                                $("#save").hide();
                                            //                                            } 
                                            //                                                
                                            //                                            }
                                            //                                        }
                                            //                                    });
                                        }

                                        //end if
                                    }





                                    function getVCR(vtid, ctid, cqty, loadType) {
                                        var vtids = document.getElementsByName("vehicleTypeIdR");
                                        var ctids = document.getElementsByName("containerTypeIdR");
                                        var ctQtys = document.getElementsByName("containerQtyR");
                                        var loadTypes = document.getElementsByName("LoadTypeR");
                                        var currRates = document.getElementsByName("currRateR");
                                        var vcr = 0;
                                        for (i = 0; i < vtids.length; i++) {
                                            if (vtids[i].value == vtid && ctids[i].value == ctid &&
                                                    ctQtys[i].value == cqty && loadTypes[i].value == loadType) {
                                                vcr = currRates[i].value;
                                            }
                                        }
                                        return vcr;
                                    }

                                    function calculateRevenue(sno, obj, type, orderId, singleTwentyRate, singleFourtyRate, twoTwentyRate) {

                                        var consignmentOrderId = orderId;
                                        var twentycontainerType = "20'";
                                        var fourtycontainerType = "40'";
                                        // alert(fourtycontainerType);
                                        var selectedContainer = 0;
                                        $.ajax({
                                            url: "/throttle/getPlannedContainerCount.do",
                                            dataType: "text",
                                            data: {
                                                consignmentOrderId: consignmentOrderId
                                            },
                                            success: function (temp) {
                                                if (temp != '') {
                                                    selectedContainer = parseInt(temp);
                                                    // alert("selectedContainer == "+selectedContainer);
                                                    var selectContainer = 0;
                                                    var revenueForSelectedContainer = "";
                                                    if (selectedContainer == 0) {

                                                        var selectedIndex = "";
                                                        var requiredVehicleType = "";
                                                        var contianerType = "";
                                                        //  alert("hrerrr..111");
                                                        if (type == 1) {
                                                            selectedIndex = document.getElementsByName("selectedIndex");

                                                        } else if (type == 2) {
                                                            selectedIndex = document.getElementsByName("repoSelectedIndex");

                                                        }
                                                        //   alert("hrerrr..");
                                                        var orderRevenue = $("#orderRevenue").val();
                                                        var orderRevenueTemp = $("#orderRevenueTemp").val();
                                                        var totalContainer = selectedIndex.length;
                                                        var revenuePerContainer = parseFloat(orderRevenueTemp / totalContainer);
                                                        if (type == 1) {
                                                            for (var i = 0; i < selectedIndex.length; i++) {
                                                                if (document.getElementById("selectedIndex" + (i + 1)).checked == true) {
                                                                    selectContainer++;

                                                                    if (document.getElementById("vehicleTypeName" + (i + 1)).value == document.getElementById("containerTypeName" + (i + 1)).value) {
                                                                        //   alert("samtype");
                                                                        if (document.getElementById("containerTypeName" + (i + 1)).value == twentycontainerType) {

                                                                            revenueForSelectedContainer = parseFloat(singleTwentyRate) * parseFloat(selectContainer);
                                                                            //  alert("single twenty:"+revenueForSelectedContainer);

                                                                        } else if (document.getElementById("containerTypeName" + (i + 1)).value == fourtycontainerType) {

                                                                            revenueForSelectedContainer = parseFloat(singleFourtyRate) * parseFloat(selectContainer);
                                                                            //   alert("single fourty"+revenueForSelectedContainer);
                                                                        }

                                                                    } else {

                                                                        revenueForSelectedContainer = parseFloat((twoTwentyRate / 2)) * parseFloat(selectContainer);
                                                                        //alert("two twenty"+revenueForSelectedContainer);

                                                                    }
                                                                    // alert("revenueForSelectedContainer:"+revenueForSelectedContainer);
                                                                    $("#orderRevenueSpan").text(revenueForSelectedContainer);
                                                                    $("#orderRevenue").val(revenueForSelectedContainer);
                                                                }
                                                            }
                                                        } else if (type == 2) {
                                                            for (var i = 0; i < selectedIndex.length; i++) {
                                                                if (document.getElementById("repoSelectedIndex" + (i + 1)).checked == true) {
                                                                    selectContainer++;
                                                                }
                                                            }
                                                        }


                                                        if (selectContainer > 0) {
                                                            $('#actionName').empty();
                                                            var actionOpt = document.trip.actionName;
                                                            var optionVar = new Option("Freeze", '1');
                                                            actionOpt.options[0] = optionVar;
                                                            $("#save").show();
                                                        } else {
                                                            var actionOpt = document.trip.actionName;
                                                            var optionVar = new Option("-select-", '0');
                                                            actionOpt.options[0] = optionVar;
                                                            optionVar = new Option("Cancel Order", 'Cancel');
                                                            actionOpt.options[1] = optionVar;
                                                            optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                            actionOpt.options[2] = optionVar;
                                                            optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                            actionOpt.options[3] = optionVar;
                                                            $("#actionDiv").show();
                                                            $("#save").hide();
                                                        }
                                                        //    setRCMForVehicleType(selectContainer);
                                                    } else if (selectedContainer > 0) {

                                                        var selectedIndex = "";
                                                        if (type == 1) {
                                                            selectedIndex = document.getElementsByName("selectedIndex");
                                                        } else if (type == 2) {
                                                            selectedIndex = document.getElementsByName("repoSelectedIndex");
                                                        }
                                                        var orderRevenue = $("#orderRevenue").val();
                                                        var orderRevenueTemp = $("#orderRevenueTemp").val();
                                                        var totalContainer = selectedIndex.length;
                                                        totalContainer = parseInt(totalContainer + selectedContainer);
                                                        //alert("Total container :"+totalContainer);


                                                        // var revenuePerContainer = parseFloat(orderRevenueTemp/totalContainer);
                                                        var revenueForSelectedContainer = "";
                                                        //                        alert("revenuePerContainer == "+revenuePerContainer);
                                                        if (type == 1) {
                                                            for (var i = 0; i < selectedIndex.length; i++) {
                                                                if (document.getElementById("selectedIndex" + (i + 1)).checked == true) {
                                                                    selectContainer++;


                                                                    if (document.getElementById("vehicleTypeName" + (i + 1)).value == document.getElementById("containerTypeName" + (i + 1)).value) {

                                                                        if (document.getElementById("containerTypeName" + (i + 1)).value == twentycontainerType) {

                                                                            revenueForSelectedContainer = parseFloat(singleTwentyRate * selectContainer);
                                                                        } else if (document.getElementById("containerTypeName" + (i + 1)).value == fourtycontainerType) {

                                                                            revenueForSelectedContainer = parseFloat(singleFourtyRate * selectContainer);
                                                                        }

                                                                        $("#orderRevenueSpan").text(revenueForSelectedContainer);
                                                                        $("#orderRevenue").val(revenueForSelectedContainer);
                                                                    } else {

                                                                        revenueForSelectedContainer = parseFloat((twoTwentyRate / 2) * selectContainer);
                                                                        $("#orderRevenueSpan").text(revenueForSelectedContainer);
                                                                        $("#orderRevenue").val(revenueForSelectedContainer);

                                                                    }
                                                                }
                                                                //put here calculation
                                                            }

                                                        } else if (type == 2) {
                                                            for (var i = 0; i < selectedIndex.length; i++) {
                                                                if (document.getElementById("repoSelectedIndex" + (i + 1)).checked == true) {
                                                                    selectContainer++;
                                                                }
                                                            }
                                                        }
                                                        if (selectContainer > 0) {
                                                            $('#actionName').empty();
                                                            var actionOpt = document.trip.actionName;
                                                            var optionVar = new Option("-select-", '0');
                                                            actionOpt.options[0] = optionVar;
                                                            optionVar = new Option("Freeze", '1');
                                                            actionOpt.options[1] = optionVar;
                                                            $("#save").show();
                                                        } else {
                                                            var actionOpt = document.trip.actionName;
                                                            var optionVar = new Option("-select-", '0');
                                                            actionOpt.options[0] = optionVar;
                                                            optionVar = new Option("Cancel Order", 'Cancel');
                                                            actionOpt.options[1] = optionVar;
                                                            optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                                            actionOpt.options[2] = optionVar;
                                                            optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                                            actionOpt.options[3] = optionVar;
                                                            $("#actionDiv").show();
                                                            $("#save").hide();
                                                        }
                                                        //   setRCMForVehicleType(selectContainer);
                                                    }

                                                }
                                            }
                                        });


                                    }


                                    var rendererOptions = {
                                        draggable: true
                                    };
                                    var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
                                    ;
                                    var directionsService = new google.maps.DirectionsService();
                                    var map;

                                    function initialize() {
                                        //alert("hi...1");
                                        var mapOptions = {
                                            zoom: 7
                                                    //    center: alkhail
                                        };
                                        map = new google.maps.Map(document.getElementById('mapdiv'), mapOptions);
                                        directionsDisplay.setMap(map);
                                        directionsDisplay.setPanel(document.getElementById('directionsPanel'));

                                        google.maps.event.addListener(directionsDisplay, 'directions_changed', function () {
                                            computeTotalDistance(directionsDisplay.getDirections());
                                        });

                                        calcRoute();

                                    }





                                    function calcRoute() {
                                        var routePoint = document.getElementById("routePoints").value;

                                        var routePoints = routePoint.split('@');
                                        var routePointsLength = routePoints.length;

                                        var waypts = [];
                                        for (var j = 1; j <= routePoints.length - 2; j++) {
                                            //alert('"'+routePoints[j].split("~")[1],routePoints[j].split("~")[2] +'"');
                                            if ('"' + routePoints[j].split("~")[1] + ',' + routePoints[j].split("~")[2] + '"' != '') {
                                                waypts.push({
                                                    location: new google.maps.LatLng(routePoints[j].split("~")[1], routePoints[j].split("~")[2]),
                                                    stopover: true});
                                            }
                                        }



                                        //alert('"'+routePoints[0].split("~")[1]+','+routePoints[0].split("~")[2] +'"');
                                        //alert('"'+routePoints[routePointsLength-1].split("~")[1]+','+routePoints[routePointsLength-1].split("~")[2] +'"');
                                        //var start = '"'+routePoints[0].split("~")[1]+', '+routePoints[0].split("~")[2] +'"';
                                        //var end = '"'+routePoints[routePointsLength-1].split("~")[1]+', '+routePoints[routePointsLength-1].split("~")[2] +'"';
                                        var start = new google.maps.LatLng(routePoints[0].split("~")[1], routePoints[0].split("~")[2]);
                                        var end = new google.maps.LatLng(routePoints[routePointsLength - 1].split("~")[1], routePoints[routePointsLength - 1].split("~")[2]);
                                        //alert(start);
                                        //alert(end);
                                        var request = {
                                            origin: start,
                                            destination: end,
                                            waypoints: waypts,
                                            optimizeWaypoints: false,
                                            travelMode: google.maps.TravelMode.DRIVING
                                        };
                                        directionsService.route(request, function (response, status) {
                                            if (status == google.maps.DirectionsStatus.OK) {
                                                directionsDisplay.setDirections(response);
                                            }
                                        });
                                    }




                                    function computeTotalDistance(result) {
                                        var total = 0;
                                        var myroute = result.routes[0];
                                        for (var i = 0; i < myroute.legs.length; i++) {
                                            total += myroute.legs[i].distance.value;
                                        }
                                        total = total / 1000.0;
                                        //document.getElementById('mapdistance').innerHTML = 'Total distance in km' + total;
                                        //document.getElementById('totalRouteKm').innerHTML ='Total Route Km: '+total;
                                        document.getElementById("googleDistance").value = total;

                                    }





                                    function setVehicleValues() {
                                        var value = document.trip.vehicleNo.value;
//                                        alert("first---"+value);
                                        if (value != 0) {
                                            var tmp = value.split('-');
//                                            alert(tmp.length);
                                            $('#vehicleId').val(tmp[0]);
                                            //alert("1");
                                            //   $('#vehicleNoEmail').val(tmp[1]);
                                            //alert("2");
                                            $('#vehicleTonnage').val(tmp[1]);
//                                            alert("3--"+tmp[4]);
                                            $('#driver1Id').val(tmp[4]);
                                            var pageType='<c:out value="${pageType}"/>';
//                                            alert(tmp[5]+"--"+pageType)
                                            if((tmp[5] == 'null' || tmp[5] == 'NA') && pageType == '2'){
                                                $('#driver1Name').val('');
                                                $('#licenseDate').val('Driver Not Mapped');
                                                
//                                                var actionOpt = document.trip.actionName;
//                                                var optionVar = new Option("-select-", '0');
//                                                actionOpt.options[0] = optionVar;
//                                                optionVar = new Option("Cancel Order", 'Cancel');
//                                                actionOpt.options[1] = optionVar;
//                                                optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
//                                                actionOpt.options[2] = optionVar;
//                                                optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
//                                                actionOpt.options[3] = optionVar;
//                                                $("#actionDiv").show();
//                                                $("#save").hide();
//                                                
                                            }else{
                                                $('#driver1Name').val(tmp[5]);
                                                $('#licenseDate').val(tmp[13] + "-" + tmp[14]+ "-" + tmp[15]);
                                            }
//                                            $('#mobileNo').val(tmp[6]);
//                                            $('#licenseNo').val(tmp[12]);
                                            $('#mobileNo').val(tmp[12]);
                                            $('#licenseNo').val(tmp[11]);
                                             document.getElementById("driver1Name").readOnly = false;
                                             document.getElementById("mobileNo").readOnly = true;
                                             document.getElementById("licenseNo").readOnly = true;
//                                            alert("5---"+tmp[5]);
//                                            alert("6---"+tmp[6]);
//                                            alert("16---"+tmp[16]);
//                                            alert("17---"+tmp[17]);
                                            $('#driver2Id').val(tmp[16]);
//                                            alert("aaaa---"+tmp[5]);
                                            $('#driver2Name').val(tmp[17]);
                                            //alert("7");
                                            //   $('#driver3Id').val(tmp[7]);
                                            //alert("8"+tmp[7]);
                                            //    $('#driver3Name').val(tmp[8]);

                                            $('#fcExpiryDate').text(tmp[8]);
                                            $('#insuranceExpiryDate').text(tmp[7]);
                                            $('#permitExpiryDate').text(tmp[9]);
                                            $('#roadExpiryDate').text(tmp[10]);

                                            var date = tmp[8];
//                                            alert("date" + date);
                                            var dateee = date.split('/');
                                            var d = dateee[0];
                                            var m = dateee[1];
                                            var y = dateee[2];
                                            var fcDate = new Date(y, m - 1, d);
                                            var today = new Date();

                                            if (today < fcDate) {
//                                                  alert("hhhh");
                                                document.getElementById("fcExpiryDate").style.color = "green";
                                            } else {
                                                document.getElementById("fcExpiryDate").style.color = "red";

                                            }


                                            var date1 = tmp[7];
//                                            alert("date" + date);
                                            var date_1 = date1.split('/');
                                            var d1 = date_1[0];
                                            var m1 = date_1[1];
                                            var y1 = date_1[2];
                                            var insurenceDate = new Date(y1, m1 - 1, d1);
                                            if (today < insurenceDate) {
//                                                  alert("hhhh");
                                                document.getElementById("insuranceExpiryDate").style.color = "green";
                                            } else {
                                                document.getElementById("insuranceExpiryDate").style.color = "red";

                                            }




                                            var date2 = tmp[10];
//                                            alert("date" + date);
                                            var date_2 = date2.split('/');
                                            var d2 = date_2[0];
                                            var m2 = date_2[1];
                                            var y2 = date_2[2];
                                            var roadTaxDate = new Date(y2, m2 - 1, d2);
                                            if (today < roadTaxDate) {
//                                                  alert("hhhh");
                                                document.getElementById("roadExpiryDate").style.color = "green";
                                            } else {
                                                document.getElementById("roadExpiryDate").style.color = "red";

                                            }

                                            //  document.getElementById("vehicleType").value=tmp[4];
                                            document.getElementById("vendorId").value = tmp[2];
                                            document.getElementById("ownership").value = tmp[3];
                                            //    document.getElementById("vehicleTypeName").innerHTML=tmp[13];
                                            var vty = document.getElementById("vehicleType").value;

                                            var currentDate = document.getElementById("currentDate").value;
                                            var nextDate = document.getElementById("nextDate").value;

                                            if (tmp[8] == Date.parse(nextDate)) {
                                                alert("FC date is going to expiry within 7 days");
                                                document.getElementById("vehicleNo").value = "0";
                                            } else if (tmp[7] == nextDate) {
                                                alert("Insurence date is going to expiry within 7 days");
                                                document.getElementById("vehicleNo").value = "0";
                                            } else if (tmp[9] == nextDate) {
                                                alert("Permit date going to expiry within 7 days");
                                                document.getElementById("vehicleNo").value = "0";
                                            } else if (tmp[10] == nextDate) {
                                                alert("RoadTax date going to expiry within 7 days");
                                                document.getElementById("vehicleNo").value = "0";
                                            }
                                            $("#save").show();

                                        } else {
                                            $('#vehicleId').val('');
                                            $('#vehicleNoEmail').val('');
                                            $('#vehicleTonnage').val('');
                                            $('#driver1Id').val('');
                                            $('#driver1Name').val('');
                                            $('#driver2Id').val('');
                                            $('#driver2Name').val('');
                                            $('#driver3Id').val('');
                                            $('#driver3Name').val('');
                                            $('#actionName').empty();
                                            $('#fcExpiryDate').text('');
                                            $('#insuranceExpiryDate').text('');
                                            $('#permitExpiryDate').text('');
                                            $('#roadExpiryDate').text('');
                                            var actionOpt = document.trip.actionName;
                                            var optionVar = new Option("-select-", '0');
                                            actionOpt.options[0] = optionVar;
                                            optionVar = new Option("Cancel Order", 'Cancel');
                                            actionOpt.options[1] = optionVar;
                                            optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                            actionOpt.options[2] = optionVar;
                                            optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                            actionOpt.options[3] = optionVar;
                                            $("#actionDiv").show();
                                            $("#save").hide();
                                        }
                                        /*       if(tmp[3] == 0){
                                         alert("Please Map Primary Driver and plan trip");
                                         $('#vehicleNo').val(0);
                                         $('#vehicleId').val('');
                                         $('#vehicleNoEmail').val('');
                                         $('#vehicleTonnage').val('');
                                         $('#driver1Id').val('');
                                         $('#driver1Name').val('');
                                         $('#driver2Id').val('');
                                         $('#driver2Name').val('');
                                         $('#driver3Id').val('');
                                         $('#driver3Name').val('');
                                         $('#actionName').empty();
                                         var actionOpt = document.trip.actionName;
                                         var optionVar = new Option("-select-", '0');
                                         actionOpt.options[0] = optionVar;
                                         optionVar = new Option("Cancel Order", 'Cancel');
                                         actionOpt.options[1] = optionVar;
                                         optionVar = new Option("Suggest Schedule Change", 'Suggest Schedule Change');
                                         actionOpt.options[2] = optionVar;
                                         optionVar = new Option("Hold Order for further Processing", 'Hold Order for further Processing');
                                         actionOpt.options[3] = optionVar;
                                         $("#actionDiv").show();
                                         $("#tripDiv").hide();
                                         } */
                                    }




                                    function computeVehicleCapUtil() {
                                        setVehicleValues();
                                        var orderWeight = document.trip.totalWeight.value;
                                        var vehicleCapacity = document.trip.vehicleTonnage.value;
                                        var ordt = document.getElementById("orderType").value;
                                        var ordType = parseInt(ordt);
                                        var remainWeigh = "";
                                        if (ordType == 1) {
                                            //alert('in order type 1');
                                            remainWeigh = 0;
                                        } else {
                                            //alert('not in order type 1');
                                            remainWeigh = orderWeight - vehicleCapacity;
                                            remainWeigh = remainWeigh.toFixed(2);
                                        }

                                        if (remainWeigh <= 0) {
                                            remainWeigh = 0;
                                        }
                                        //alert("remain weight"+remainWeigh);
                                        document.getElementById("remainWeight").value = remainWeigh;
                                        //alert(orderWeight +"  "+vehicleCapacity);
                                        var utilPercent;
                                        if (vehicleCapacity > 0) {
                                            // alert("ord weight"+orderWeight);
                                            var orw = parseInt(orderWeight);
                                            var vcap = parseInt(vehicleCapacity);
                                            if (orw > vcap) {
                                                orderWeight = vehicleCapacity;
                                                // alert(orderWeight);
                                                utilPercent = (orderWeight / vehicleCapacity) * 100;
                                            } else {
                                                // alert('djdjd');
                                                utilPercent = (orderWeight / vehicleCapacity) * 100;
                                            }

                                            // alert(utilPercent);
                                            document.trip.vehicleCapUtil.value = utilPercent.toFixed(0);

                                        } else {
                                            //document.trip.vehicleCapUtil.value = 0;
                                        }
                                        var e = document.getElementById("vehicleCheck");
                                        e.style.display = 'block';
                                        // for running vehicle ajax
                                        var s = document.getElementById("vehicleNo").value;
                                        var vn = s.split("-");
                                        //alert(vn[14]);
                                        document.getElementById("vehicleTypeId").value = vn[14];
                                        var vstatus = document.getElementById("tripStatus").value;
                                        if (vstatus == "222") {
                                            s = document.getElementById("vehicleNo").value;
                                            var ordsts = document.getElementById("orderStatus").value;
                                            //  alert(s+" "+ordsts);

                                            $.ajax({url: "/throttle/getRunningTripDetails.do", dataType: "json", data: {
                                                    vehicleId: vn[0],
                                                    ordStatus: ordsts
                                                }, success: function (data) {
                                                    // alert(data);
                                                }
                                            });
                                        }
                                    }

                                    function onKeyPressBlockCharacters1(sno3, e) {
                                        var fieldLength = document.getElementById('containerNo' + sno3).value.length;
                                        if (fieldLength <= 3) {
                                            var key = window.event ? e.keyCode : e.which;
                                            var keychar = String.fromCharCode(key);
                                            reg = /\d/;
                                            return !reg.test(keychar);
                                        } else if (fieldLength <= 10) {
                                            var key = window.event ? e.keyCode : e.which;
                                            var keychar = String.fromCharCode(key);
                                            reg = /[a-zA-Z]+$/;
                                            return !reg.test(keychar);
                                        }
                                    }



                                    function setRemainBox()
                                    {
                                        var ordType = document.getElementById("orderType").value;
                                        // alert(ordType);
                                        var ord = parseInt(ordType);
                                        if (ord == 1) {
                                            //alert('dfghjf');
                                            document.getElementById("remain1").style.display = 'none';
                                            document.getElementById("remain2").style.display = 'none';
                                        }
                                    }


                                    var rowCount = 1;
                                    var sno = 0;
                                    var rowCount1 = 1;
                                    var sno1 = 0;
                                    var sno2 = 0;
                                    var sno4 = 0;
                                    var httpRequest;
                                    var httpReq;
                                    var styl = "";
                                    function addRowContainer() {
                                        if (parseInt(rowCount1) % 2 == 0)
                                        {
                                            styl = "text2";
                                        } else {
                                            styl = "text1";
                                        }

                                        sno4++;
                                        var tab = document.getElementById("containerTBL");
                                        //find current no of rows
                                        var rowCountNew = document.getElementById('containerTBL').rows.length;
                                        rowCountNew--;
                                        var newrow = tab.insertRow(rowCountNew);
                                        cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' style='width:10px;'> " + sno4 + "</td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(1);
                                        cell0 = "<td class='text1' height='25'><input type='hidden' value='' style='width:120px;'  name='containerTypeId'  id='containerTypeId" + sno1 + "'   value='' ><input type='text' value='' style='width:120px;'  name='containerType'  id='containerType" + sno1 + "'   value='' ></td>";
                                        cell.setAttribute("className", styl);
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(2);
                                        cell0 = "<td class='text1' height='25' ><input type='text' value='' style='width:120px;'  name='containerNo' maxlength='11'  id='containerNo" + sno1 + "' onKeyPress='return onKeyPressBlockCharacters1(" + sno1 + ",event);   value='' ></td>";
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(3);
                                        cell0 = "<td class='text1' height='25' ><input type='text' value='' style='width:120px;'  name='sealNo'  id='sealNo" + sno1 + "'   value='' ></td>";
                                        cell.innerHTML = cell0;
                                        cell = newrow.insertCell(4);
                                        cell0 = "<td class='text1' height='25' ><select type='text'  style='width:120px;'  name='grStatus'  id='grStatus" + sno1 + "' ><option value='To be billed' selected>To be billed</option><option value='Paid'>Paid</option></select></td>";
                                        cell.innerHTML = cell0;


                                        rowCount1++;
                                        sno++;
                                        sno2++;
                                    }


                                </script>
                            </div>
                            <c:if test = "${companyId != '1461' }" >
                                <div id="bunkDetail" class="tab-pane" style="display:block;">
                                    <table   class="table table-info mb30 table-hover" style="width:100%;display: block;" id="fuelTBL">
                                    </c:if>
                                    <c:if test = "${companyId == '1461' }" >

                                        <c:if test = "${pageType == 1 || pageType == 2}" >
                                            <div id="bunkDetail" class="tab-pane" style="display:none;">
                                                <table   class="table table-info mb30 table-hover" style="width:100%;display: none;" id="fuelTBL">
                                                </c:if>
                                                <c:if test = "${pageType == 3 }" >
                                                    <div id="bunkDetail" class="tab-pane" style="display:none;">
                                                        <table   class="table table-info mb30 table-hover" style="width:100%;display: none;" id="fuelTBL">
                                                        </c:if>

                                                    </c:if> 

                                                    <thead>
                                                        <tr id="tableDesingTH" height="30">
                                                            <th class="contenthead" >S No</th>
                                                            <th class="contenthead" >Bunk Name</th>
                                                            <th class="contenthead">Slip No</th>
                                                            <th class="contenthead">Date</th>
                                                            <th class="contenthead">Ltrs</th>
                                                            <th class="contenthead">Amount</th>
                                                            <th class="contenthead">Person</th>
                                                            <th class="contenthead">Remarks</th>
                                                            <th class="contenthead">&nbsp;</th>
                                                        </tr>
                                                    </thead>
                                                    <tr>
                                                        <td> 1</td>
                                                        <td>
                                                            <input type='hidden' id='uniqueIdNew' name='uniqueIdNew' value=''/>
                                                            <select style="width:130px;" id='bunkName1' name='bunkName' class="form-control" style="width:100px;height:40px;" onchange="fuelPrice1(1);
                                                                    showRemarks();">
                                                                <option value="0~0~0~0">---Select---</option>
                                                                <c:if test = "${bunkList != null}" >
                                                                    <c:forEach items="${bunkList}" var="bunk">
                                                                        <option  value='<c:out value="${bunk.bunkId}" />'> <c:out value="${bunk.bunkName}" />
                                                                        </c:forEach >
                                                                    </c:if>
                                                            </select>
                                                        </td>
                                                        <td>
                                                            <input name="slipNo" type="text" class="form-control" style="width:100px;height:40px;" id="slipNo" value="" maxlength="10" />
                                                        </td>

                                                        <td>
                                                            <input name="bunkPlace" type="hidden" class="form-control" id="bunkPlace"  />
                                                            <input name="fuelDate" id="fuelDate" type="text" class="form-control datepicker" value='<%=startDate%>' style="width:120px;"/>
                                                        </td>
                                                        <td>
                                                            <input name="fuelLtrs" type="text" class="form-control"  style="width:100px;height:40px;" value="0" id="fuelLtrs1"  onblur="sumFuel();" onkeyup="fuelPrice1(1);" style="width:80px;" maxlength="3" onKeyPress="return onKeyPressBlockCharacters(event);"/>
                                                        </td>
                                                        <td>
                                                            <input name="fuelAmount" readonly type="text" class="form-control" style="width:100px;height:40px;" value="0" id="fuelAmount1"  onblur="sumFuel();"  style="width:80px;" readonly />
                                                        </td>

                                                        <td>
                                                            <input name="fuelFilledName" type="text" class="form-control" style="width:100px;height:40px;" id="fuelFilledName" value="<%= session.getAttribute("userName")%>" />
                                                            <input name="fuelFilledBy" type="hidden" class="form-control" id="fuelFilledBy" value="<%= session.getAttribute("userId")%>" />
                                                        </td>
                                                        <td>
                                                            <textarea name='fuelRemarks' id='fuelRemarks' class="form-control" style="width:100px;height:40px;" type='text' rows='2' cols='10'></textarea>
                                                            <input type="hidden" name="HfId" id="HfId" />
                                                        </td>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="9" align="center">
                                                            <!--                                        <input type="button" name="add" value="Add" onclick="addFuelRow()" id="add" class="btn btn-info" />
                                                                                                    &nbsp;&nbsp;&nbsp;
                                                                                                    <input type="button" name="delete" value="Delete" onclick="delFuelRow()" id="delete" class="btn btn-info" />-->
                                                            <input type="hidden" name="totalFuelLtrs" id="totalFuelLtrs" value=""/>
                                                            <input type="hidden" name="totalKms" id="totalKms" value=""/>
                                                            <input type="hidden" name="totalFuelAmount" id="totalFuelAmount" value="0"/>
                                                        </td>
                                                    </tr>
                                                    <script>
                                                        function onKeyPressBlockCharacters(e) {
                                                            var sts = false;
                                                            var key = window.event ? e.keyCode : e.which;
                                                            var keychar = String.fromCharCode(key);
                                                            reg = /[a-zA-Z`~!@#$%^&*() _|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
                                                            sts = !reg.test(keychar);
                                                            return sts;
                                                        }
                                                        var poItems = 0;
                                                        var rowCount = '';
                                                        var sno = '';
                                                        var snumber = '';

                                                        function addFuelRow()
                                                        {
                                                            var currentDate = new Date();
                                                            var day = currentDate.getDate();
                                                            var month = currentDate.getMonth() + 1;
                                                            var year = currentDate.getFullYear();
                                                            var myDate = day + "-" + month + "-" + year;

                                                            if (sno < 9) {
                                                                sno++;
                                                                var tab = document.getElementById("fuelTBL");
                                                                var rowCount = tab.rows.length;

                                                                snumber = parseInt(rowCount) - 1;
                                                                //                    if(snumber == 1) {
                                                                //                        snumber = parseInt(rowCount);
                                                                //                    }else {
                                                                //                        snumber++;
                                                                //                    }

                                                                var newrow = tab.insertRow(parseInt(rowCount) - 1);
                                                                newrow.height = "30px";
                                                                // var temp = sno1-1;
                                                                var cell = newrow.insertCell(0);
                                                                var cell0 = "<td><input type='hidden'  name='fuelItemId' /> " + snumber + "</td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;

                                                                cell = newrow.insertCell(1);
                                                                //                                        cell0 = "<td class='text2'><input name='bunkName' type='text' class='textbox'     id='bunkName' class='Textbox' /></td>";
                                                                cell0 = "<td class='text2'><select class='textbox' style='width:135px;' id='bunkName" + snumber + "' style='width:123px'  name='bunkName'><option selected value=0>---Select---</option><c:if test = "${bunkList != null}" ><c:forEach items="${bunkList}" var="bunk"><option  value='<c:out value="${bunk.bunkId}" />'><c:out value="${bunk.bunkName}" /> </c:forEach ></c:if> </select></td><input type='hidden' id='uniqueIdNew" + snumber + "' name='uniqueIdNew' value=''/>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;

                                                                cell = newrow.insertCell(2);
                                                                cell0 = "<td class='text1'><input name='slipNo' id='slipNo" + snumber + "' type='text'   value='' class='textbox' style='width:100px;'/></td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;

                                                                cell = newrow.insertCell(3);
                                                                cell0 = "<td class='text1'><input name='fuelDate' id='fuelDate" + snumber + "' type='text' class='datepicker1' value='" + myDate + "'   style='width:100px;'/></td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;

                                                                cell = newrow.insertCell(4);
                                                                cell0 = "<td class='text1'><input name='fuelLtrs' onBlur='sumFuel();' onchange='fuelPrice1(" + snumber + ");' type='text' class='textbox' style='width:100px;' id='fuelLtrs" + snumber + "' value='0'/></td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;

                                                                cell = newrow.insertCell(5);
                                                                cell0 = "<td class='text1'><input name='fuelAmount' readonly onBlur='sumFuel();' type='text'  value='0' id='fuelAmount" + snumber + "' class='textbox' style='width:100px;' /></td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;



                                                                cell = newrow.insertCell(6);
                                                                cell0 = "<td class='text1'><input name='fuelFilledBy'  type='hidden'  id='fuelFilledBy'  value='<%= session.getAttribute("userId")%>' /><input name='fuelFilledName' type='text'  id='fuelFilledName' value='<%= session.getAttribute("userName")%>' class='textbox' style='width:100px;'/></td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;

                                                                cell = newrow.insertCell(7);
                                                                cell0 = "<td class='text1'><textarea name='fuelRemarks' id='fuelRemarks' class='textbox' style='width:100px;' type='text' rows='2' cols='10'></textarea></td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell0;

                                                                cell = newrow.insertCell(8);
                                                                var cell1 = "<td><input type='checkbox' name='deleteItem' value='" + snumber + "'/> </td>";
                                                                //cell.setAttribute(cssAttributeName,"text1");
                                                                cell.innerHTML = cell1;
                                                                // rowCount++;


                                                                $(".datepicker").datepicker({
                                                                    /*altField: "#alternate",
                                                                     altFormat: "DD, d MM, yy"*/
                                                                    changeMonth: true, changeYear: true
                                                                });
                                                            }
                                                        }
                                                        function fuelPrice1(rowVal)
                                                        {
                                                            //                        alert("----->"+rowVal);
                                                            var fuelRate = document.getElementById('bunkName' + rowVal).value;
                                                            //alert("=====>"+fuelRate);
                                                            var temp = fuelRate.split('~');
                                                            fuelRate = temp[3];
                                                            //  alert(fuelRate);

                                                            var fuelPrice = fuelRate;
                                                            var fuelLters = document.getElementById("fuelLtrs" + rowVal).value;
                                                            //  alert("fuelLters==>"+fuelLters);
                                                            var fuelAmount = (fuelPrice * fuelLters).toFixed(2);
                                                            //  alert("fuelAmount==>"+fuelAmount);
                                                            //                                                        document.tripSheet.fuelAmount.value = fuelAmount;
                                                            document.getElementById("fuelAmount" + rowVal).value = fuelAmount;
                                                            $("#save").show();
                                                            sumFuel();
                                                            var movementType = document.getElementById("movementType").value;
                                                            if (movementType == 14 || movementType == 16) {
                                                                setRCMForLCLVehicleType();
                                                            }
                                                        }

                                                        function delFuelRow() {
                                                            try {
                                                                var table = document.getElementById("fuelTBL");
                                                                rowCount = table.rows.length - 1;
                                                                for (var i = 2; i < rowCount; i++) {
                                                                    var row = table.rows[i];
                                                                    var checkbox = row.cells[7].childNodes[0];
                                                                    if (null != checkbox && true == checkbox.checked) {
                                                                        if (rowCount <= 1) {
                                                                            alert("Cannot delete all the rows");
                                                                            break;
                                                                        }
                                                                        table.deleteRow(i);
                                                                        rowCount--;
                                                                        i--;
                                                                        sno--;
                                                                        // snumber--;
                                                                    }
                                                                }
                                                                sumFuel();
                                                            } catch (e) {
                                                                alert(e);
                                                            }
                                                        }


                                                        function sumFuel() {
                                                            //alert("this s ctesting");
                                                            var totFuel = 0;
                                                            var totltr = 0;
                                                            var totAmount = 0;
                                                            var totAmt = 0;
                                                            totFuel = document.getElementsByName('fuelLtrs');

                                                            totAmount = document.getElementsByName('fuelAmount');
                                                            for (i = 0; i < totFuel.length; i++) {
                                                                totltr = +totltr.toFixed(2) + +totFuel[i].value;
                                                            }
                                                            document.getElementById('totalFuelLtrs').value = totltr.toFixed(2);
                                                            for (i = 0; i < totAmount.length; i++) {
                                                                totAmt = +totAmt.toFixed(2) + +totAmount[i].value;
                                                            }
                                                            document.getElementById('totalFuelAmount').value = totAmt.toFixed(2);
                                                            // hidden by me              setRCMForVehicleType('1');
                                                            // sumExpenses();
                                                            //setBalance();
                                                        }


                                                        function totalKmsFunc() {
                                                            //document.getElementById('totalKms').value=parseInt(document.getElementById('kmsIn').value)-parseInt(document.getElementById('kmsOut').value);
                                                            document.getElementById('totalKms').value = 0;
                                                        }

                                                        function sumExpenses() {
                                                            var sumAmt = 0;
                                                            sumAmt = parseInt(document.getElementById('totalAllowance').value) + parseInt(document.getElementById('totalFuelAmount').value);
                                                            document.getElementById('totalExpenses').value = parseInt(sumAmt);
                                                        }
                                                        function setBalance() {
                                                            var sumAmt = 0;
                                                            sumAmt = parseInt(document.getElementById('totalAllowance').value) - parseInt(document.getElementById('totalFuelAmount').value);
                                                            document.getElementById('balanceAmount').value = parseInt(sumAmt);
                                                        }


                                                    </script>

                                                </table>
                                                <c:if test = "${companyId == '1461' }" >
                                                    <c:if test = "${pageType == 1 || pageType == 2}" >
                                                        <table   class="table table-info mb30 table-hover" style="width:100%;display: block" id="fuelTBL1">
                                                        </c:if>
                                                        <c:if test = "${pageType == 3 }" >
                                                            <table   class="table table-info mb30 table-hover" style="width:100%;display: none" id="fuelTBL1">
                                                            </c:if>
                                                        </c:if>
                                                        <c:if test = "${companyId != '1461' }" >
                                                            <table   class="table table-info mb30 table-hover" style="width:100%;display: block" id="fuelTBL1">
                                                            </c:if>      

                                                            <thead>
                                                                <tr id="tableDesingTH" height="30">

                                                                    <!--<th class="contenthead" id="hireL" >Hire </th>-->
                                                                    <th class="contenthead" >Req.Advance Amt.</th>
                                                                    <th class="contenthead" id="balanL">Balance</th>
                                                                    <th class="contenthead" id="mamulL" >Mamul</th>
                                                                    <th class="contenthead">Date</th>
                                                                    <th class="contenthead">Transaction Type</th>
                                                                    <th class="contenthead">Remarks</th>
                                                                    <th></th>

                                                                </tr>
                                                            </thead>
                                                            <tr>
                                                                <!--                                        <td id="hiree">
                                                                                                            <input name="hire" type="text" class="form-control" style="width:100px;height:40px;" id="hire" value="0" onchange="calBalance()" />
                                                                                                        </td>-->
                                                                <td>
                                                                    <input name="advancerequestamt" type="text" class="form-control" style="width:100px;height:40px;" id="advancerequestamt" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);"  onchange="calBalance()" />
                                                                </td>

                                                                <td id="balan">
                                                                    <input name="balance" type="text" readonly class="form-control" style="width:100px;height:40px;" id="balance" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" />
                                                                </td>
                                                                <td id="mamul">
                                                                    <input name="mamul" type="text" class="form-control" style="width:100px;height:40px;" id="mamul" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"  />
                                                                </td>
                                                                <td>
                                                                    <input name="requeston" id="requeston" type="text" class="datepicker" value='<%=startDate%>'   style="width:180px;height:40px;" />
                                                                </td>
                                                                <td>

                                                                    <select name="advTrnType" id="advTrnType" class="form-control" style="width:180px;height:40px;" onchange="showRemarks()">
                                                                        <option  value="Cash">By Cash</option>
                                                                        <option  value="BankTransfer">By Transfer</option>
                                                                    </select>
                                                                </td>
                                                                <td>

                                                                    <select name="requestremarks" id="requestremarks" class="form-control" style="width:180px;height:40px;" onchange="showRemarks()">
                                                                        <option  value="For Food & Toll">Trip Advance</option>
                                                                        <option  value="For  FastTrack Recharge">For Food & Toll </option>
                                                                        <option  value="For  Food">For Food </option>
                                                                        <option  value="For Toll">For Toll </option>
                                                                        <option value="other"> other </option>
                                                                    </select>
                                                                </td>
                                                                <td id="othersRemarks" style="display:none">
                                                                    <input type="text" id="advanceRemarks" name="advanceRemarks" class="form-control" style="width:180px;height:40px;" value="" >
                                                                </td>
                                                            <script>
                                                                function showRemarks() {
                                                                    var other = document.getElementById("requestremarks").value;
                                                                    //                                                alert("other="+other)
                                                                    if (other == 'other' || other == 'For Food & Toll') {
                                                                        $("#othersRemarks").show();
                                                                    } else {
                                                                        $("#othersRemarks").hide();
                                                                    }
                                                                }
                                                            </script>



                                                            </tr>
                                                        </table>
                                                        <br>
                                                        <!--click to Allocate Advance Gr:<input type="checkbox" id="advanceGRAlloc"name="advanceGRAlloc" onclick="allocateAdvanceGr();" readonly/>-->
                                                        <table   class="table table-info mb30 table-hover" id="advanceGR" style='display:none;' >
                                                            <thead>
                                                                <tr id="tableDesingTH" height="30">
                                                                    <td  height="30" >Sno </td>
                                                                    <td  height="30" >Available GR </td>
                                                                    <td  height="30" >Select</td>
                                                                </tr>
                                                            </thead>
                                                            <%
                                                               int serilNo = 1;
                                                            %>
                                                            <tbody>
                                                                <c:forEach items="${advanceGRDeatils}" var="no">
                                                                    <tr>
                                                                        <td align="left" >

                                                                            <%=serilNo%>
                                                                        </td>
                                                                        <td align="left" >

                                                                            <input type="text" name="advanceGrNo" id="advanceGrNo<%=serilNo%>" value="<c:out value="${no.grNo}"/>" class="form-control" style="width:100px;height:40px;" readonly/>
                                                                        </td>
                                                                        <td>
                                                                            <input type="checkbox" name="grSelectedIndex" id="grSelectedIndex<%=serilNo%>" onclick="selectGRNo(<%=serilNo%>)" class="checkbox"/>
                                                                        </td>
                                                                    </tr>
                                                                    <%serilNo++;%>
                                                                </c:forEach>
                                                            </tbody>
                                                        </table>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <center>
                                                            <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                                            <a><input type="button" class="btn btn-success btnNext" value="Next" name="Next" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                                        </center>
                                                        <br>
                                                        <br>

                                                        </div>



                                                        <script>
                                                            function selectGRNo(sno) {
                                                                var grSelectedIndex = document.getElementsByName("grSelectedIndex");
                                                                if (document.getElementById('grSelectedIndex' + sno).checked) {
                                                                    for (var i = 0; i < grSelectedIndex.length; i++) {
                                                                        grSelectedIndex[i].checked = false;
                                                                    }
                                                                    document.getElementById('grSelectedIndex' + sno).checked = true;
                                                                } else {
                                                                    document.getElementById('grSelectedIndex' + sno).checked = false;
                                                                }
                                                            }
                                                            function setVehicleType() {
                                                                //                                    alert("setVeh="+document.getElementById("vendor").value);
                                                                $("tr.containerQuantitySelect").hide();
                                                                document.getElementById("ownership").value = '1';
                                                                //                                document.trip.elements['vehicleNo'].style.display = 'block';
                                                                //                                document.getElementById("hireVehicleNo").value = "";
                                                                //                                document.trip.elements['hireVehicleNo'].style.display = 'none';
                                                                //                                $('#vehicleNo').empty();
                                                                //                                $('#vehicleNo').append($('<option></option>').val(0).html('--select--'))
                                                                var vendorTemp = document.getElementById("vendor").value;
                                                                //                                alert(vendorTemp)
                                                                var temp = vendorTemp.split("~");
                                                                //alert("setVehicleType1:"+temp[1]);
                                                                //alert("setVehicleType1000:"+temp[0]);
                                                                if (temp[1] == '1' || temp[1] == '0') {


                                                                    document.trip.elements['driverName'].style.display = 'none';
                                                                    document.trip.elements['driver1Name'].style.display = 'block';

                                                                    $("tr.vehCategory").hide();
                                                                    $("#vehHire").hide();
                                                                    $("#vehOwn").show();
                                                                    //                                        Muthuhereeeee
                                                                    $("#hireL").hide();
                                                                    $("#balanL").hide();
                                                                    $("#mamulL").hide();

                                                                    $("#hiree").hide();
                                                                    $("#balan").hide();
                                                                    $("#mamul").hide();

                                                                } else {

                                                                    document.trip.elements['driver1Name'].style.display = 'block';
                                                                    document.trip.elements['driverName'].style.display = 'none';
                                                                    $("tr.vehCategory").show();
                                                                    $("#vehHire").show();
                                                                    $("#vehOwn").hide();

                                                                    $("#hireL").show();
                                                                    $("#balanL").show();
                                                                    $("#mamulL").show();

                                                                    $("#hiree").show();
                                                                    $("#balan").show();
                                                                    $("#mamul").show();

                                                                }

                                                                $.ajax({
                                                                    url: "/throttle/getVendorVehicleType.do",
                                                                    dataType: "json",
                                                                    data: {
                                                                        vendorId: temp[0]
                                                                    },
                                                                    success: function (data) {
                                                                        //  alert(data);
                                                                        if (data != '') {
                                                                            $('#vehicleTypeName').empty();
                                                                            $('#vehicleTypeName').append(
                                                                                    $('<option></option>').val(0).html('--select--'))
                                                                            //                                            $('#vehicleTypeName').append(
                                                                            //                                                    $('<option></option>').val(1058).html('20\''))
                                                                            //                                            $('#vehicleTypeName').append(
                                                                            //                                                    $('<option></option>').val(1059).html('40\''))
                                                                            $.each(data, function (i, data) {
                                                                                var vehicleTypeId1 = data.Id;
                                                                                //alert(vehicleTypeId1);
                                                                                var temp1 = vehicleTypeId1.split("~");
                                                                                $('#vehicleTypeName').append(
                                                                                        $('<option style="width:90px"></option>').attr("value", temp1[0]).text(data.Name)
                                                                                        )


                                                                                if (temp1[1] == 3) {
                                                                                    $('#vehicleCategory').empty();
                                                                                    //                                                        $('#vehicleCategory').append($('<option ></option>').val(1).html('Leased'))
                                                                                    $('#vehicleCategory').append($('<option ></option>').val(2).html('Hire'))
                                                                                    showVehicle();
                                                                                    //setVehicle();
                                                                                } else if (temp1[1] == 2) {
                                                                                    $('#vehicleCategory').empty();
                                                                                    $('#vehicleCategory').append($('<option ></option>').val(2).html('Hire'))
                                                                                    showVehicle();
                                                                                    resetAll();
                                                                                    setRCMForVehicleType('1');
                                                                                } else if (temp1[1] == 1) {
                                                                                    $('#vehicleCategory').empty();
                                                                                    //                                                        $('#vehicleCategory').append($('<option ></option>').val(1).html('Leased'))
                                                                                    $('#vehicleCategory').append($('<option ></option>').val(2).html('Hire'))
                                                                                    showVehicle();
                                                                                    resetAll();
                                                                                    setRCMForVehicleType('1');
                                                                                    //setVehicle();
                                                                                } else {
                                                                                    $('#vehicleCategory').empty();
                                                                                    //                                                    alert("7777777777777777")
                                                                                    //                                                    setVehicleType();
                                                                                    resetAll();
                                                                                    setRCMForVehicleType('1');
                                                                                    //setVehicle();
                                                                                    showVehicle();
                                                                                }



                                                                            });
                                                                        } else {
                                                                            $('#vehicleTypeName').empty();
                                                                            $('#vehicleTypeName').append(
                                                                                    $('<option></option>').val(0).html('--select--'))
                                                                        }
                                                                    }
                                                                });




                                                            }
                                                            function setVehicle() {


                                                                $('#vehicleNo').empty();
                                                                //alert("hiii");
                                                                var vendorTemp = document.getElementById("vendor").value;
                                                                var temp1 = vendorTemp.split("~");
                                                                var vehicletypename = $("#vehicleTypeName option:selected").text();
                                                                document.getElementById("vehicleType").value = vehicletypename;
                                                                var vtid = $("#vehicleTypeName option:selected").val();
                                                                var vtid = vtid.split("-")[0];
                                                                document.getElementById("vehicleTypeIdSelected").value = vtid;

                                                                var ownership = document.getElementById("ownership").value;
                                                                var vehicleTypeName = document.getElementById("vehicleTypeName").value;
                                                                var temp = vehicleTypeName.split('-');
                                                                var selectedIndex = document.getElementsByName("selectedIndex");
                                                                var orderIds = document.getElementsByName("orderIds");
                                                                var multilpleOrderFlag = false;
                                                                for (var i = 1; i < parseInt(selectedIndex.length - 1); i++) {
                                                                    if (orderIds[i].value != orderIds[(i + 1)].value)
                                                                        multilpleOrderFlag = true;
                                                                }

                                                                if (parseInt(temp[0]) == 1059 || parseInt(temp[0]) == 1060) {
                                                                    $("tr.containerQuantitySelect").show();
                                                                    //document.getElementById("sinTwentyCheck").checked = false;
                                                                    //                                    document.getElementById("sinTwentyContainer").value = 0;
                                                                } else {
                                                                    $("tr.containerQuantitySelect").hide();
                                                                    //document.getElementById("sinTwentyCheck").checked = false;
                                                                    //                                    document.getElementById("sinTwentyContainer").value = 0;
                                                                }
                                                                if (ownership == '3' && vehicleTypeName != '0') {
                                                                }
                                                                setContainer();
                                                                //var statusId = arr[0];
                                                                $.ajax({
                                                                    url: "/throttle/getVendorVehicle.do",
                                                                    dataType: "json",
                                                                    data: {
                                                                        vehicleTypeId: $("#vehicleTypeName").val(),
                                                                        vendorId: temp1[0]
                                                                    },
                                                                    success: function (data) {
                                                                        // alert(data);
                                                                        if (data != '') {
                                                                            $('#vehicleNo').empty();
                                                                            $('#vehicleNo').append(
                                                                                    $('<option></option>').val(0).html('--select--'))
                                                                            $.each(data, function (i, data) {
                                                                                $('#vehicleNo').append(
                                                                                        $('<option style="width:90px" ></option>').attr("value", data.Id).text(data.Name).attr("style", "text-color:red")
                                                                                        )
                                                                            });
                                                                        } else {
                                                                            $('#vehicleNo').empty();
                                                                            $('#vehicleNo').append(
                                                                                    $('<option></option>').val(0).html('--select--'))
                                                                        }
                                                                    }
                                                                });
                                                                setVehicleTonnage();

                                                            }
                                                            function  setVehicleTonnage() {
                                                                var temp = "";

                                                                var vehicleTypeTemp = document.getElementById("vehicleTypeName").value;
                                                                temp = vehicleTypeTemp.split('-');

                                                                document.getElementById("vehicleTonnage").value = parseInt((temp[1].trim()));
                                                                document.getElementById("vehicleTypeName").value = vehicleTypeTemp;

                                                            }

                                                        </script>


                                                        <div id="routeDetail" class="tab-pane">
                                                            <% int z = 1; %>
                                                            <input type="hidden" id="repoId" name="repoId" value="0"/>
                                                            <!--
                                                            Export Repo: <input type="checkbox" name="repo" id="repo" onclick="setRepoDiv();" style="margin-left: -52px"/>
                                                            -->

                                                            Change Execution Route: <input type="checkbox" name="tempRepo" id="tempRepo" onclick="setChangeRouteDiv();" style="margin-left: -50px"/>
                                                            <br> <br>

                                                            <input type="hidden" onclick="processPreStartCheckBox();"  name="preStartLocationCheckbox"  id="preStartLocationCheckbox" value="1"  />
                                                            <% int cntrRepo = 1; %>
                                                            <!-- repo Div Start ---->
                                                            <div id="repoDiv" style="display:none;">
                                                                <% int counterRepo = 1; %>
                                                                <input type="hidden" onclick="processPreStartCheckBox();"  name="preStartLocationCheckbox"  id="preStartLocationCheckbox" value="1"  />

                                                                <c:set var="prevConsignmentOrderId" value="0" />
                                                                <c:if test = "${orderPointDetails != null}" >

                                                                    <table border="0" class="border" align="center" width="100%" cellpadding="0" id="repoTBL" cellspacing="0" >

                                                                        <tr>
                                                                            <td colspan="4"> <input type="hidden" name="" id="" value=""/> </td>
                                                                        </tr>
                                                                        <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">
                                                                            <td  height="30" >Consignment No</td>
                                                                            <td  height="30" >Point Name</td>
                                                                            <td  height="30" >Type</td>
                                                                            <td  height="30" >Address</td>
                                                                            <td  height="30" >Planned Date</td>
                                                                            <td  height="30" >Planned Time</td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="6" align="center">
                                                                                <input type="button" class="btn btn-info" name="add" value="add" onclick="addRowRepo();"/>

                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <br>

                                                                    <input type="hidden" name="totalCntrrepo" id="totalCntrrepo" value="<%=counterRepo%>" readonly/>
                                                                </c:if>

                                                                <br>
                                                                <br>
                                                                <!--                     <table border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" >
                                                                                        <tr>
                                                                                            <td><font color="red">*</font>Total Km For This Trip: </td>
                                                                                            <td>
                                                                                                <input type="text" name="totalkm" id="totalkm" value='<c:out value="${routeDistance}" />' readonly/></td></tr></table>-->
                                                            </div>
                                                            <!--Repo Div End --->

                                                            <!-- For FCL with with Single Leg -->

                                                            <div id="fclLeg" style="display:none;">
                                                                <% int counter = 1; %>
                                                                <input type="hidden" onclick="processPreStartCheckBox();"  name="preStartLocationCheckbox"  id="preStartLocationCheckbox" value="1"  />

                                                                <c:set var="prevConsignmentOrderId" value="0" />
                                                                <c:if test = "${orderPointDetails != null}" >

                                                                    <table   class="table table-info mb30 table-hover" style="margin-left: 10px;width:100%" >
                                                                        <input type="hidden" name="orderSequenceNo" id="orderSequenceNo" value=""/>
                                                                        <thead>
                                                                            <tr id="tableDesingTH" height="30">
                                                                                <th height="30" >Consignment No</th>
                                                                                <th height="30" >Point Name</th>
                                                                                <th height="30" >Type</th>
                                                                                <th height="30" >Address</th>
                                                                                <th height="30" >Planned Date</th>
                                                                                <th height="30" >Planned Time</th>
                                                                                <!--                                <td class="contenthead" height="30" >Distance</td>-->
                                                                            </tr>
                                                                        </thead>
                                                                        <!--                                        <tr>
                                                                                                                    <td colspan="6"> <input type="hidden" name="orderSequenceNo" id="orderSequenceNo" value=""/> </td>
                                                                                                                </tr>-->

                                                                        <% int countId=0;%>
                                                                        <c:forEach items="${orderPointDetails}" var="consignment">
                                                                            <c:if test="${consignment.pointType == 'Loading' || consignment.pointType == 'Loading Point'}">
                                                                            <%--<c:if test="${consignment.pointType == 'Drop' || consignment.pointType == 'Loading Point' || consignment.pointType == 'Unloading Point' || consignment.pointType == 'Loading' || consignment.pointType == 'Unloading'}">--%>
                                                                                <c:set var="distancekm" value="${consignment.pointDistance}" scope="page" />
                                                                            </c:if>
                                                                            <tr>
                                                                                <td height="30" ><c:out value="${consignment.consignmentOrderNo}" /></td>
                                                                                <td height="30" ><c:out value="${consignment.pointName}" /></td>
                                                                            <input type="hidden" name="pointName" id="pointName<%=counter%>" value='<c:out value="${consignment.pointName}" />' />
                                                                            <input type="hidden" name="pointId" id="pointId<%=counter%>" value='<c:out value="${consignment.pointId}" />' />
                                                                            <input type="hidden" name="loadingPointId" id="loadingPointId" value='<c:out value="${consignment.pointId}" />' />
                                                                            <input type="hidden" name="pointOrder" value="<%=counter%>"/>
                                                                            <input type="hidden" name="orderId" value='<c:out value="${consignment.consignmentOrderId}" />' />
                                                                            <input type="hidden" name="latitude" id="latitude<%=counter%>" value='<c:out value="${consignment.latitude}" />' />
                                                                            <input type="hidden" name="longitude" id="longitude<%=counter%>" value='<c:out value="${consignment.longitude}" />' />
                                                                            <input type="hidden" name="actualPointId" id="actualPointId<%=counter%>" value='0' />
                                                                            <input type="hidden" name="actualPointName" id="actualPointName<%=counter%>" value='-' />

                                                                            <td height="30" ><input type="text" name="pointType" readonly value='<c:out value="${consignment.pointType}" />' class="form-control" style="width:180px;height:40px;"/></td>
                                                                            <td height="30" >
                                                                                <!--<textarea name="pointAddresss" rows="2" cols="15" class="form-control" style="width:180px;height:40px;"><c:out value="${consignment.pointAddress}" />  </textarea>-->

                                                                                <!------------------ export movement part ------------------------>
                                                                                <%--<c:out value="${movementType}"/>--%>
                                                                                <!--emplot & CFS -->
                                                                                <c:if test="${movementType == 1 || movementType == 21 || movementType == 25 || movementType == 26 || movementType == 27 || movementType == 31 || movementType == 32}">

                                                                                    <c:if test="${consignment.pointType == 'PickUp'}"> 
                                                                                        <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                            <option value="0">-Select Address-</option>   
                                                                                            <c:forEach items="${emptyPlotList}" var="cl">
                                                                                                <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </c:if>
                                                                                <!--factory & CFS-->
                                                                                <c:if test="${consignment.pointType == 'Loading' || consignment.pointType == 'Loading Point'}">
                                                                                        <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                            <option value="0">-Select Address-</option>   
                                                                                            <c:forEach items="${factoryList}" var="cl">
                                                                                                <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </c:if>
                                                                                
                                                                                
                                                                                    <c:if test="${consignment.pointType == 'Custom Clearance'}">
                                                                                        <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                            <option value="0">-Select Address-</option>   
                                                                                            <c:forEach items="${clearanceList}" var="cl">
                                                                                                <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </c:if>
                                                                                
                                                                                    <c:if test="${consignment.pointType == 'Customs Clearance Point'}">
                                                                                        <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                            <option value="0">-Select Address-</option>   
                                                                                            <c:forEach items="${clearanceList}" var="cl">
                                                                                                <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </c:if>
                                                                                
                                                                                <!--port and CFS-->
                                                                                    <c:if test="${consignment.pointType == 'Drop'}">
                                                                                        <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                            <option value="0">-Select Address-</option>   
                                                                                            <c:forEach items="${portAndCFS}" var="cl">
                                                                                                <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </c:if>


                                                                                </c:if>



                                                                                <!------------------ Import movement part ------------------------>
                                                                                
                                                                                <c:if test="${movementType == 2 || movementType == 28 || movementType == 29 || movementType == 30}">
                                                                                    
                                                                                    <!--port and CFS-->
                                                                                    
                                                                                    <c:if test="${consignment.pointType == 'PickUp'}"> 
                                                                                        <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                            <option value="0">-Select Address-</option>   
                                                                                            <c:forEach items="${portAndCFS}" var="cl">
                                                                                                <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </c:if>
                                                                                    
                                                                                    <!--factory-->
                                                                                    <c:if test="${consignment.pointType == 'Unloading'}">
                                                                                        <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                            <option value="0">-Select Address-</option>   
                                                                                            <c:forEach items="${factoryList}" var="cl">
                                                                                                <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </c:if>
                                                                                    
                                                                                    <!--empty plot and port-->
                                                                                    <c:if test="${consignment.pointType == 'Drop'}"> 
                                                                                        <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                            <option value="0">-Select Address-</option>   
                                                                                            <c:forEach items="${emptyPlotList}" var="cl">
                                                                                                <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                            </c:forEach>
                                                                                        </select>
                                                                                    </c:if>                                                                        

                                                                                </c:if>

                                                                                <!------------------ FTL and ODC movement part ------------------------>
                                                                                 <!--port & CFS -->
                                                                                <c:if test="${movementType != 1 && movementType != 2 && movementType != 21 && movementType != 25 && movementType != 26 && movementType != 27 && movementType != 28 && movementType != 29 && movementType != 30 && movementType != 31 && movementType != 32}">

                                                                                    <select id="pointAddresss<%=counter%>" name="pointAddresss" class="form-control" style="width:200px;height:40px" onchange="setPointValues(this.value,<%=counter%>);">
                                                                                        <option value="0">-Select Address-</option>   
                                                                                        <c:forEach items="${portAndCFS}" var="cl">
                                                                                            <option value="<c:out value="${cl.cityId}" />~<c:out value="${cl.latitude}" />~<c:out value="${cl.longitude}" />~<c:out value="${cl.cityName}" />"><c:out value="${cl.cityName}" /></option>
                                                                                        </c:forEach>
                                                                                    </select>
                                                                                </c:if>                                                     

                                                                            </td>
                                                                            <td height="30" ><input type="text" name="pointPlanDate"  class="datepicker"  value='<c:out value="${consignment.pointPlanDate}" />' style="width:180px;height:40px;"/></td>
                                                                            <td height="30" >
                                                                                HH:<input type="text" class="textboxl" style="width:30px;" name="pointPlanTimeHrs" value='<c:out value="${consignment.pointPlanTimeHrs}" />' />
                                                                                MI:<input type="text" class="textbox" style="width:30px;"  name="pointPlanTimeMins" value='<c:out value="${consignment.pointPlanTimeMins}" />' />
                                                                            </td>
                                                                            <td>    <input type="hidden" name="distancekm" id="distancekm1<%=counter%>" value='<c:out value="${consignment.pointDistance}" />'  onkeyup="setKmVakues(this.value, this.id, 'fcl')"/></td>
                                                                            </tr>

                                                                            <script>
                                                                                function setPointValues(value, sn) {
                                                                                    var tmp = value.split('~');
                                                                                    $('#actualPointId' + sn).val(tmp[0]);
                                                                                    $('#latitude' + sn).val(tmp[1]);
                                                                                    $('#longitude' + sn).val(tmp[2]);
                                                                                    $('#actualPointName' + sn).val(tmp[3]);
                                                                                }
                                                                            </script>
                                                                            <% countId++; %>
                                                                            <% counter++; %>

                                                                            <c:set var="nettKm" value="${nettKm + consignment.pointDistance}" />
                                                                        </c:forEach>
                                                                        <c:if test="${orderType == '4'}">
                                                                            <c:forEach items="${orderPointDetails}" var="consignment" begin="0" end="0">

                                                                                <tr>
                                                                                    <td height="30" ><c:out value="${consignment.consignmentOrderNo}" /></td>
                                                                                    <td height="30" ><c:out value="${consignment.pointName}" /></td>
                                                                                <input type="hidden" name="pointName" id="pointName<%=counter%>" value='<c:out value="${consignment.pointName}" />' />
                                                                                <input type="hidden" name="pointId" id="pointId<%=counter%>" value='<c:out value="${consignment.pointId}" />' />
                                                                                <input type="hidden" name="loadingPointId" id="loadingPointId" value='<c:out value="${consignment.pointId}" />' />
                                                                                <input type="hidden" name="pointOrder" value="<%=counter%>"/>
                                                                                <input type="hidden" name="orderId"  value='<c:out value="${consignment.consignmentOrderId}" />' />

                                                                                <td height="30" ><input type="text" name="pointType" readonly value='Drop' /></td>
                                                                                <td height="30" ><textarea name="pointAddresss" rows="2" cols="15"><c:out value="${consignment.pointAddress}" /> </textarea></td>
                                                                                <td height="30" ><input type="text" name="pointPlanDate"  class="datepicker"  value='' /></td>
                                                                                <td height="30" >
                                                                                    HH:<input type="text"  class="textbox" style="width:30px;" name="pointPlanTimeHrs" value='00' />
                                                                                    MI:<input type="text"  class="textbox" style="width:30px;"  name="pointPlanTimeMins" value='00' />
                                                                                </td>
                                                                                <td>    <input type="hidden" name="distancekm" id="distancekm1<%=counter%>" value='<c:out value="${distancekm}" />'  onkeyup="setKmVakues(this.value, this.id, 'fcl')"/></td>
                                                                                </tr>
                                                                                <% countId++; %>
                                                                                <% counter++; %>

                                                                                <c:set var="nettKm" value="${nettKm + distancekm}" />


                                                                            </c:forEach>
                                                                        </c:if>
                                                                        <c:set var="prevConsignmentOrderId" value="${consignment.consignmentOrderId}" />

                                                                    </table>
                                                                    <br>

                                                                    <input type="hidden" name="totalCntrfcl" id="totalCntrfcl" value="<%=counter%>" readonly/>
                                                                </c:if>

                                                                <br>
                                                                <br>
                                                            </div>
                                                            <table   class="table table-info mb30 table-hover" style="margin-left: 10px;width:100%" >
                                                                <tr>
                                                                    <td><font color="red">*</font>Total Km For This Trip: </td>
                                                                    <td><input type="text" name="totalkm" id="totalkm" class="form-control" style="width:180px;height:40px;" value='<c:out value="${routeDistance}" />'readonly/></td>
                                                                </tr>
                                                            </table>
                                                            <table   class="table table-info mb30 table-hover" style="margin-left: 10px;width:100%" >
                                                                <tr>
                                                                    <td>Action: </td>
                                                                    <td  class="text1">
                                                                        <select name="actionName" id="actionName" class="form-control" style="width:180px;height:40px;" onChange="setButtons();">
                                                                            <option value="0">-select-</option>
                                                                            <option value="Cancel">Cancel Order</option>
                                                                            <option value="Suggest Schedule Change">Suggest Schedule Change</option>
                                                                            <option value="Hold Order for further Processing">Hold Order for further Processing</option>
                                                                        </select>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td  class="text2">Remarks</td>
                                                                    <td  class="text2"><textarea name="actionRemarks" rows="3" cols="100" class="form-control" style="width:180px;height:40px;"></textarea> </td>
                                                                </tr>
                                                                <tr style="display:none" >
                                                                    <td  class="text1">Do you want to intimate?</td>
                                                                    <td  class="text1">
                                                                        Client <input type="checkbox" checked name="client" value="1" /> &nbsp;
                                                                        A/C Mgr<input type="checkbox" checked name="manager" value="1" /> &nbsp;
                                                                        Fleet Mgr <input type="checkbox" checked name="fleetManager" value="1" /> &nbsp;
                                                                    </td>
                                                                </tr>
                                                                <br>
                                                                <br>
                                                                <tr><td>
                                                                        <!--  &nbsp;&nbsp;<input type="button" class="button" id="freezeRoute"   value="Freeze Route" name="save1" onclick="getSecRouteExpense();">-->

                                                                        <a class="nexttab" href="#">
                                                                            <!--<input type="button"  class="button"  value="Estimate Expense" name="Save" onclick="setRCMForVehicleType();" /></a> &nbsp;&nbsp;&nbsp;-->
                                                                            <!--           <%if (!isSingleConsignmentOrder){%>
                                                                            <%}else{%>
                                                                            &nbsp;&nbsp;<input type="button" class="button" id="freezeRoute"   value="Estimate Expense" name="save1" onclick="getProjectedExpense();">
                                                                            <%}%>   -->
                                                                            <input type="hidden" name="divcheck" id="divcheck" value=""/>
                                                                    </td>

                                                                </tr>
                                                            </table>

                                                            <br>
                                                            <br>
                                                            <center>
                                                                <a><input type="button" class="btn btn-success btnPrevious" value="Previous" name="Previous" style="background-color:#5BC0DE;color:white;width:100px;height:35px;font-weight: bold;"/></a>
                                                                <input type="button" class="btn btn-success" value="Generate TripSheet"  id="Save" name="Save" onclick="submitPage();" style="background-color:#5BC0DE;color:white;width:200px;height:35px;font-weight: bold;" />
                                                            </center>
                                                            <br>
                                                            <br>
                                                        </div>
                                                        </div>
                                                        <input type="hidden" name="summaryOption" id="summaryOption" value="0"/>
                                                        <input type="hidden" name="ismultiple" id="ismultiple" value="0"/>
                                                        <input type="hidden" name="googleDistance" id="googleDistance" value="0"/>
                                                        <br><br>
                                                        <br><br>
                                                        <br><br>
                                                        <br>

                                                        <table>
                                                            <tr>
                                                                <td>
                                                                    <div id="mapdiv"   style="width:550px; height:400px">

                                                                    </div>
                                                                    <div id="mapdistance"   style="width:550px; height:400px">

                                                                    </div>
                                                                </td>
                                                                <td valign="top" >&nbsp;
                                                                    <%if(request.getAttribute("routePoints") != null){%>
                                                                    <table width="100%" align="center" border="0" id="table1" class="sortable" style="width:360px; visibility:hidden;" >
                                                                        <thead>
                                                                            <tr>
                                                                                <td>Sno</td>
                                                                                <td>Legend</td>
                                                                                <td>Point Name</td>
                                                                                <td>Point Type</td>
                                                                                <td>Planned Date Time</td>
                                                                            </tr>
                                                                        </thead>

                                                                        <%
                                                                        String routeDetails = (String)request.getAttribute("routePoints");
                                                                        String[] routeData = routeDetails.split("@");
                                                                        String[] tempData = null;
                                                                        int cntr = 0;
                                                                        String tempLegend = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
                                                                        String[] tempLegendArray = tempLegend.split(",");
                                                                        for(int j=0; j< routeData.length; j++){
                                                                            cntr++;
                                                                            tempData = routeData[j].split("~");
                                                                            if(tempData.length > 4){// pickup
                                                                        %>
                                                                        <tr>
                                                                            <td><%=cntr%></td>
                                                                            <td><%=tempLegendArray[j]%></td>
                                                                            <td><%=tempData[3]%></td>
                                                                            <td>PickUp</td>
                                                                            <td><%=tempData[4]%></td>
                                                                        </tr>
                                                                        <%
                                                                    }else{
                                                                        %>
                                                                        <tr>
                                                                            <td><%=cntr%></td>
                                                                            <td><%=tempLegendArray[j]%></td>
                                                                            <td><%=tempData[3]%></td>
                                                                            <td>Drop</td>
                                                                            <td>-</td>
                                                                        </tr>
                                                                        <%
                                                                    }

                                                                }
                                                                        %>

                                                                    </table>
                                                                    <%}%>
                                                                </td>
                                                        </table>

                                                        <script>
                                                            function viewExpenseDetails(vehicleTypeName, vehicleTypeId, routeDistance, noOfVehicles) {
                                                                var temp = "";

                                                                if (routeDistance = 'null') {
                                                                    var km = document.getElementById("totalkm").value;

                                                                    temp = document.getElementById('ConsignmentOrderIds').value;

                                                                    window.open('/throttle/viewExpenseDetails.do?vehicleTypeId=' + vehicleTypeId + '&noOfVehicles=' + noOfVehicles + '&routeDistance=' + km + '&vehicleTypeName=' + vehicleTypeName + '&temp=' + temp, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                                                } else {
                                                                    temp = document.getElementById('ConsignmentOrderIds').value;

                                                                    window.open('/throttle/viewExpenseDetails.do?vehicleTypeId=' + vehicleTypeId + '&noOfVehicles=' + noOfVehicles + '&routeDistance=' + routeDistance + '&vehicleTypeName=' + vehicleTypeName + '&temp=' + temp, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                                                }
                                                            }
                                                        </script>






                                                        <script>
                                                            function callDestinationAjax(val) {
                                                                // Use the .autocomplete() method to compile the list based on input from user
                                                                alert(val);
                                                                var pointNameId = 'truckDestinationName' + val;
                                                                var pointIdId = 'truckDestinationId' + val;
                                                                var originPointId = 'truckOriginId' + val;
                                                                var truckRouteId = 'truckRouteId' + val;
                                                                var travelKm = 'truckTravelKm' + val;
                                                                var travelHour = 'truckTravelHour' + val;
                                                                var travelMinute = 'truckTravelMinute' + val;
                                                                var fleetType = 'fleetTypeId' + val;

                                                                //alert(pointNameId+"ttyyy");
                                                                $('#' + pointNameId).autocomplete({
                                                                    source: function (request, response) {
                                                                        $.ajax({
                                                                            url: "/throttle/getCityFromName.do",
                                                                            dataType: "json",
                                                                            data: {
                                                                                cityFrom: request.term,
                                                                                originCityId: $("#" + originPointId).val(),
                                                                                textBox: 1
                                                                            },
                                                                            success: function (data, textStatus, jqXHR) {
                                                                                var items = data;
                                                                                response(items);
                                                                            },
                                                                            error: function (data, type) {

                                                                                //console.log(type);
                                                                            }
                                                                        });
                                                                    },
                                                                    minLength: 1,
                                                                    select: function (event, ui) {
                                                                        var value = ui.item.Name;
                                                                        //   var id = ui.item.Id;
                                                                        // alert(" : "+value);
                                                                        var tmp = value.split('-');


                                                                        $('#' + pointNameId).val(tmp[1]);
                                                                        $('#' + pointIdId).val(tmp[0]);
                                                                        //                                                    $('#'+travelKm).val(ui.item.TravelKm);
                                                                        //                                                    $('#'+travelHour).val(ui.item.TravelHour);
                                                                        //                                                    $('#'+travelMinute).val(ui.item.TravelMinute);
                                                                        //                                                    $('#'+truckRouteId).val(ui.item.RouteId);
                                                                        //                                                    $('#'+fleetType).focus();
                                                                        //validateRoute(val,value);

                                                                        return false;
                                                                    }
                                                                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                                                                    //alert(item);
                                                                    var itemVal = item.Name;
                                                                    var temp = itemVal.split('-');
                                                                    itemVal = '<font color="green">' + temp[1] + '</font>';
                                                                    return $("<li></li>")
                                                                            .data("item.autocomplete", item)
                                                                            .append("<a>" + itemVal + "</a>")
                                                                            .appendTo(ul);
                                                                };
                                                            }
                                                            function callOrginAjax(val) {
                                                                // Use the .autocomplete() method to compile the list based on input from user
                                                                //alert(val);
                                                                var pointNameId = 'truckOriginName' + val;
                                                                var pointIdId = 'truckOriginId' + val;
                                                                var originPointId = 'truckOriginId' + val;
                                                                var truckRouteId = 'truckRouteId' + val;
                                                                var travelKm = 'truckTravelKm' + val;
                                                                var travelHour = 'truckTravelHour' + val;
                                                                var travelMinute = 'truckTravelMinute' + val;
                                                                var fleetType = 'fleetTypeId' + val;

                                                                //alert(pointNameId+"ttyyy");
                                                                $('#' + pointNameId).autocomplete({
                                                                    source: function (request, response) {
                                                                        $.ajax({
                                                                            url: "/throttle/getCityFromName.do",
                                                                            dataType: "json",
                                                                            data: {
                                                                                cityFrom: request.term,
                                                                                originCityId: $("#" + originPointId).val(),
                                                                                textBox: 1
                                                                            },
                                                                            success: function (data, textStatus, jqXHR) {
                                                                                var items = data;
                                                                                response(items);
                                                                            },
                                                                            error: function (data, type) {

                                                                                //console.log(type);
                                                                            }
                                                                        });
                                                                    },
                                                                    minLength: 1,
                                                                    select: function (event, ui) {
                                                                        var value = ui.item.Name;
                                                                        //   var id = ui.item.Id;
                                                                        // alert(" : "+value);
                                                                        var tmp = value.split('-');

                                                                        $('#' + pointIdId).val(tmp[0]);
                                                                        $('#' + pointNameId).val(tmp[1]);
                                                                        //                                                    $('#'+travelKm).val(ui.item.TravelKm);
                                                                        //                                                    $('#'+travelHour).val(ui.item.TravelHour);
                                                                        //                                                    $('#'+travelMinute).val(ui.item.TravelMinute);
                                                                        //                                                    $('#'+truckRouteId).val(ui.item.RouteId);
                                                                        //                                                    $('#'+fleetType).focus();
                                                                        //validateRoute(val,value);

                                                                        return false;
                                                                    }
                                                                }).data("ui-autocomplete")._renderItem = function (ul, item) {
                                                                    //alert(item);
                                                                    var itemVal = item.Name;
                                                                    var temp = itemVal.split('-');
                                                                    itemVal = '<font color="green">' + temp[1] + '</font>';
                                                                    return $("<li></li>")
                                                                            .data("item.autocomplete", item)
                                                                            .append("<a>" + itemVal + "</a>")
                                                                            .appendTo(ul);
                                                                };
                                                            }

                                                            /// mehtod for route validation
                                                            function routeValidate(val) {
                                                                var currId = 0;
                                                                //var destinationId=0;
                                                                // var interemId=0;
                                                                var prevId = 0;
                                                                var nextId = 0;
                                                                if (val == 1) {
                                                                    prevId = 0;
                                                                    currId = document.getElementById("truckOriginId1").value;
                                                                    nextId = document.getElementById("truckDestinationId1").value;

                                                                    alert(prevId + " " + currId + " " + nextId);
                                                                } else {

                                                                    var newval = val - 1;
                                                                    prevId = document.getElementById("truckOriginId" + newval).value;
                                                                    currId = document.getElementById("truckOriginId" + val).value;
                                                                    nextId = document.getElementById("truckDestinationId1").value;
                                                                    alert(prevId + " " + currId + " " + nextId);
                                                                }

                                                                // document.getElementById("truckDestinationName1").focus();
                                                            }
                                                        </script>
                                                        <script>

                                                        </script>




                                                        <script>



                                                            callOrginAjax(1);
                                                            //  callDestinationAjax(1);
                                                            $(document).ready(function () {
                                                                var truckCnt = 2;
                                                                var interemCnt = 1;

                                                                //alert(cnt);
                                                                $("#truck_add").click(function () {


                                                                    $('#truckTable tr').last().after('<tr><td>' + truckCnt + '</td><td> Point' + interemCnt + '</td><td><input type="hidden" name="truckOriginId" id="truckOriginId' + truckCnt + '" value="" ><input type="text" name="truckOriginName" id="truckOriginName' + truckCnt + '" value=""></td></tr>');
                                                                    $(".datepicker").datepicker({
                                                                        changeMonth: true, changeYear: true
                                                                    });
                                                                    callOrginAjax(truckCnt);
                                                                    // addcuntr(truckCnt);
                                                                    document.getElementById("countrval").value = truckCnt;

                                                                    truckCnt++;
                                                                    interemCnt++;
                                                                });

                                                                $("#truck_rem").click(function () {
                                                                    if ($('#truckTable tr').size() > 2) {
                                                                        $('#truckTable tr:last-child').remove();
                                                                        truckCnt = truckCnt - 1;
                                                                    } else {
                                                                        alert('One row should be present in table');
                                                                    }
                                                                });

                                                            });








                                                        </script>





                                                        <script>
                                                            $('.btnNext').click(function () {
                                                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                                            });
                                                            $('.btnPrevious').click(function () {
                                                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                                            });

                                                            hideContainerTab();
                                                        </script>

                                                        </body>
                                                        </div>
                                                        </div>
                                                        </div>
                                                        <%@ include file="/content/common/NewDesign/settings.jsp" %>
