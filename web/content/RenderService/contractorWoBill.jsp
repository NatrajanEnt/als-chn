

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
        <%@ page import="ets.domain.fmsOperation.business.FMSOperationTO" %> 
        <%@ page import="java.util.*" %> 
        <title>Contract Work Order Bill</title>




    </head>
    <body>


        <script>
            function submitPage(val) {
                if (val == 'approve') {
                    document.mpr.status.value = "APPROVED"
                    document.mpr.action = "/throttle/approveMpr.do"
                    document.mpr.submit();
                } else if (val == 'reject') {
                    document.mpr.status.value = "REJECTED"
                    document.mpr.action = "/throttle/approveMpr.do"
                    document.mpr.submit();
                }
            }


            function maxlength(field, maxlen) {
                if (field.value.length > maxlen) {
                    field.value = field.value.substring(0, maxlen);
                }
            }

            function maxCharacters() {
                var desc = document.mpr.report;
                maxlength(desc, 72);
            }

            function print(ind)
            {
                if (document.mpr.report.value == 'Enter Remarks Here')
                    document.mpr.report.value = '';

                var DocumentContainer = document.getElementById(ind);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                //WindowObject.close();   
            }


        </script>

        <form name="mpr"  method="post" >                        
            <br>


            <%
                int index = 0;
                ArrayList poDetail = new ArrayList();
                poDetail = (ArrayList) request.getAttribute("woDetail");
                FMSOperationTO purch = new FMSOperationTO();
                int listSize = 10;
                int problemNameLimit = 100;
                int mfrNameLimit = 15;

                FMSOperationTO headContent = new FMSOperationTO();
                System.out.println("poDetail size in jsp=" + poDetail.size());
                headContent = (FMSOperationTO) poDetail.get(0);
                String[] address = headContent.getAddress();

                String problemName = "";
                String remarks = "";
                String symptoms = "";
                String mfrName = headContent.getMfrName();

                if (headContent.getMfrName().length() > mfrNameLimit) {
                    mfrName = mfrName.substring(0, mfrNameLimit - 1);
                }

                for (int i = 0; i < poDetail.size(); i = i + listSize) {
            %>



            <div id="print<%= i%>" >
                <style type="text/css">
                    .header {font-family:Arial;
                             font-size:15px;
                             color:#000000;
                             text-align:center;
                             padding-top:10px;
                             font-weight:bold;
                    }
                    .border {border:1px;
                             border-color:#000000;
                             border-style:solid;
                    }
                    .text1 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:14px;
                        color:#000000;
                    }
                    .text2 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:16px;
                        color:#000000;
                    }
                    .text3 {
                        font-family:Arial, Helvetica, sans-serif;
                        font-size:18px;
                        color:#000000;
                    }

                </style>    


                <table width="700" align="center" border="0" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td valign="top" colspan="2" height="30" align="center" class="border"><strong>CONTRACT WORK ORDER BILL</strong></td>
                    </tr>
                    <tr>
                        <td height="125" valign="top">
                            <table width="350" height="131" border="0" cellpadding="0" cellspacing="0" class="border">
                                <tr>
                                    <Td height="80" width="400" style="padding-left:10px;" class="text3" align="left">
                                        <b>C.A. Logistics (P) Ltd </b>
                                </tr>
                                <tr>
                                    <Td align="left" class="text2" style="padding-left:10px; ">
                                        40, 6th Floor, Rajaji Salai,
                                        <br>
                                        Parrys,<br>
                                        Chennai <br>
                                        Tamilnadu- 600001.
                                    </Td>
                                </tr>
                            </table>
                        </td>
                        <td height="125"  valign="top" >

                            <table width="350" height="131" border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <Td  width="350" valign="top">
                                        <table height="125" width="350" align="left" border="0" cellpadding="0" cellspacing="0" class="border" >
                                            <tr>
                                                <td colspan="2" class="text2" style="padding-left:10px;" ><strong>To</strong></td>
                                            </tr>


                                            <tr>
                                                <td colspan="2" class="text2" style="line-height:20px;"  height="113" style="padding-left:10px;">

                                                    <p><strong>M/S&nbsp;&nbsp; <%= headContent.getVendorName()%>  </strong> <br>                                
                                                        <% for (int k = 0; k < address.length; k++) {%>
                                                        <%= address[k]%>   <br>
                                                        <% }%>         

                                                        Phno &nbsp;:&nbsp; <%= headContent.getPhone()%> </p>  
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="text3" width="137" style="padding-left:5px;"> <strong>WO.No.:<span class="text3" > <%= headContent.getWoId()%></span></strong> </td>
                                                <td class="text3" width="161"  style="padding-left:5px;"><strong>WO.Date</strong>:<%= headContent.getCreatedDate()%> </td>
                                            </tr>

                                        </table>
                                    </Td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <Td valign="top" colspan="2">
                            <table width="700" height="30" align="right" border="0" cellpadding="0" cellspacing="0" class="border">
                                <tr>
                                    <Td width="233" style="padding-left:10px;" align="left" class="text1"> Vehicle Make&nbsp;:&nbsp;<%= mfrName%></Td>
                                    <Td width="233" align="center" class="text1"> Vehicle Number&nbsp;:&nbsp;<%= headContent.getRegno()%> </Td>
                                    <Td width="233" style="padding-right:28px;" class="text1" align="right" > Vehicle KM&nbsp;:&nbsp;<%= headContent.getCurrentKM()%> </Td>
                                </tr>

                            </table>
                        </Td>
                    </tr>
                    <tr>
                        <Td valign="top" colspan="2">
                            <table width="700" height="30" align="right" border="0" cellpadding="0" cellspacing="0" class="border">

                                <tr>
                                    <Td width="233" style="padding-left:10px;" align="left" class="text1"> Bill No&nbsp;:&nbsp;<%= headContent.getBillNo()%></Td>
                                    <Td width="233" align="center" class="text1"> Bill Date&nbsp;:&nbsp;<%= headContent.getBillDate()%> </Td>
                                    <Td width="233" style="padding-right:28px;" class="text1" align="right" > Invoice No&nbsp;:&nbsp;<%= headContent.getInvoiceNo()%> </Td>
                                </tr>
                            </table>
                        </Td>
                    </tr>
                    <tr>
                        <Td valign="top" colspan="2">

                            <table width="700" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:0px;border-bottom: 0px;border-top: 0px ">
                                <tr >
                                    <Td class="text2"   style="padding-left:10px; border-top: 0px;border-bottom: 1px;border-left: 0px;border-right: 0px; border-color:#000000; border-style:solid;"> <strong>Description</strong></Td>
                                    <Td class="text2"  style="padding-left:5px; border-top: 0px;border-bottom: 1px;border-left: 1px;border-right: 0px; border-color:#000000; border-style:solid;"  align="right"><strong>Amount (Rs.)</strong></Td>
                                </tr> 
                                <tr >
                                    <Td class="text2"   style="padding-left:10px; border-top: 0px;border-bottom: 1px;border-left: 0px;border-right: 0px; border-color:#000000; border-style:solid;"> <strong>Spare Amount</strong></Td>
                                    <Td class="text2"  style="padding-left:5px; border-top: 0px;border-bottom: 1px;border-left: 1px;border-right: 0px; border-color:#000000; border-style:solid;"  align="right"><strong><%= headContent.getSpareAmt()%> </strong></Td>
                                </tr> 
                                <tr>
                                    <Td class="text2"    style="padding-left:10px; border-top: 0px;border-bottom: 1px;border-left: 0px;border-right: 0px; border-color:#000000; border-style:solid;"> <strong>Spare Vat</strong></Td>
                                    <Td class="text2"  style="padding-left:5px; border-top: 0px;border-bottom: 1px;border-left: 1px;border-right: 0px; border-color:#000000; border-style:solid;"  align="right"><strong><%= headContent.getVatAmt()%> </strong></Td>
                                </tr> 
                                <tr>
                                    <Td class="text2"    style="padding-left:10px; border-top: 0px;border-bottom: 1px;border-left: 0px;border-right: 0px; border-color:#000000; border-style:solid;"> <strong>Service Amount</strong></Td>
                                    <Td class="text2"  style="padding-left:5px; border-top: 0px;border-bottom: 1px;border-left: 1px;border-right: 0px; border-color:#000000; border-style:solid;" class="border" align="right"><strong><%= headContent.getServiceAmt()%> </strong></Td>
                                </tr> 
                                <tr>
                                    <Td class="text2"   style="padding-left:10px; border-top: 0px;border-bottom: 1px;border-left: 0px;border-right: 0px; border-color:#000000; border-style:solid;"> <strong>Service Tax</strong></Td>
                                    <Td class="text2"  style="padding-left:5px; border-top: 0px;border-bottom: 1px;border-left: 1px;border-right: 0px; border-color:#000000; border-style:solid;" align="right"><strong><%= headContent.getServiceTax()%> </strong></Td>
                                </tr> 
                                <tr style="margin-top: 15px;border-bottom: 0px">
                                    <Td class="text2"    style="padding-left:10px; border:0px; border-color:#000000; border-style:solid;"> <strong>Total Amount</strong></Td>
                                    <Td class="text2"  style="padding-left:5px; border-top: 0px;border-bottom: 0px;border-left: 1px;border-right: 0px; border-color:#000000; border-style:solid;"  align="right"><strong><%= headContent.getTotalAmt()%> </strong></Td>
                                </tr> 


                            </table>
                        </td>
                    </tr>


                    <Tr>
                        <Td valign="top" colspan="3">
                            <table width="700" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

                                <Tr>
                                    <Td class="text1" colspan='3' height="20" ALIGN="left" ><strong>REMARKS</strong> </Td>
                                    <Td width="600" height="20" style="font-size:15px;" colspan='3' >&nbsp;&nbsp;<%= headContent.getRemarks()%> </Td>
                                </tr>


                            </table>

                        </td>
                    </Tr>






                    <Tr>
                        <Td valign="top" colspan="3">
                            <table width="700" height="30" align="center" border="0" cellpadding="0" cellspacing="0" class="border">

                                <Tr>
                                    <Td width="110" class="text1" height="30" > &nbsp; </Td>
                                    <Td width="336" class="text1" height="30" >&nbsp;</Td>
                                    <Td width="154" class="text1" height="30" >&nbsp;</Td>
                                </Tr>

                                <Tr>
                                    <Td width="110" class="text1"><strong>Prepared by</strong></Td>
                                    <Td width="336" class="text1">&nbsp;</Td>
                                    <Td width="154" class="text1"><strong>Authorised Signatory</strong></Td>
                                </Tr>
                            </table>
                        </Td>
                    </Tr>
                </table>
            </div>
            <center>   
                <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i%>');" > &nbsp;        
            </center>
            <br>    
            <br>    
            <br>    
            <% }%>    

            <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
