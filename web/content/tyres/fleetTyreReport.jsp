<%@ include file="../common/NewDesign/header.jsp" %>
   <%@ include file="../common/NewDesign/sidemenu.jsp" %>
<%@page import="java.sql.*,java.util.Iterator,java.util.ArrayList,java.text.DecimalFormat,ets.domain.tyres.business.TyresTO" pageEncoding="UTF-8"%>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        
        <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"> </script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script>
        <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }


        </script>




    </head>

<div class="pageheader">
      <h2><i class="fa fa-edit"></i><spring:message code="hrms.label.Departmentccc" text="Fleet Tyre Report"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.HRMSdd" text="Tyre"/></a></li>
            <li class="active"><spring:message code="hrms.label.Department33" text="Vehicle wise Tyres Report"/></li>
          


        </ol>
      </div>
      </div>
             <div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">       
    
<body>
<form name="tyreReportVehicle" action="#" method="post" >




    <%

    String toDateDisp = "";
    int sino = 0;
    String tdClassName = "form-control";

    String mfrName = "";
    TyresTO tyresTO = new TyresTO();
    ArrayList tyresDetails = (ArrayList) request.getAttribute("tyresDetails");
    System.out.println("srini tyre size=="+tyresDetails.size());
    if(tyresDetails != null && tyresDetails.size() >0) {

    %>
     
         <table class="table table-info mb30 table-hover " id="table">
         <thead>
        <tr>
        <th  >S.No</th>
        <th  >Tyre No</th>
        <th  >Position</th>
        <th  >Make</th>
        <th  >In Date</th>
        <th  >Out Date</th>
        <th  >Type</th>
        <th  >Active Status</th>
        <th  >In KM</th>
        <th  >Out KM</th>
        <th  >Total KM</th>
    </tr>
    </thead>
    <%    
    
    Iterator it = tyresDetails.iterator();
    String type = "";
    String fromKm = "";
    String toKm = "";
    String totalKm = "";
    while(it.hasNext()){
        tyresTO = (TyresTO) it.next();
        mfrName = tyresTO.getItemName();
        type = tyresTO.getRcType();
        fromKm = tyresTO.getFromKm();
        toKm = tyresTO.getToKm();
        if("NA".equals(toKm)){
           totalKm = "NA";
        }else{
            totalKm = "" + (Integer.parseInt(toKm) - Integer.parseInt(fromKm));
        }
        if("N".equals(type)){
            type = "New";
        }else {
            type = "RC";
        }

        

        toDateDisp = tyresTO.getToDate();
        if(toDateDisp != null && toDateDisp.equals("00-00-0000")){
            toDateDisp = "Till Date";
        }
            sino++;
            if((sino%2) == 0){
            tdClassName = "form-control";
            } else {
            tdClassName = "form-control";
            }
            if(toDateDisp == null ){
            toDateDisp = "Till Date";
            }
        %>

    <tr>
        <td  ><%=sino%></td>
        <td  ><%=tyresTO.getTyreNumber()%></td>        
        <td  ><%=tyresTO.getTyrePosition()%></td>
        <td  ><%=tyresTO.getItemName()%></td>
        <td  ><%=tyresTO.getFromDate()%></td>
        <td  ><%=toDateDisp%></td>
        <td  ><%=type%></td>
        <td  ><%=tyresTO.getActive()%></td>
        <td  ><%=tyresTO.getFromKm()%></td>
        <td  ><%=tyresTO.getToKm()%></td>
        <td  ><%=totalKm%></td>
      
    </tr>
<%
}

    }
%>
    </table>
    </td>
        </tr>
        
</table>

<script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>



        <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="../common/NewDesign/settings.jsp" %>
