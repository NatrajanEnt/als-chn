<%--
    Document   : CustomerPortal
    Created on : Nov 5, 2013, 11:18:13 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script>

            function submitPage(value) {
                alert(hi);
                document.CustomerPortal.action = '/throttle/CustomerPortal.jsp';
                document.CustomerPortal.submit();
            }
        </script>
        <script
            src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false">
        </script>

        <script>
            function initialize()
            {
                var mapProp = {
                    center: new google.maps.LatLng(22.421306, 78.457553),
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
            }
            //https://maps.google.com/maps?hl=en&q=kashipur+uttarkhand
            google.maps.event.addDomListener(window, 'load', initialize);
        </script>
        <script>
            function submitPage() {
                document.customerTracking.action = '/throttle/BrattleFoods/customerTracking.jsp?tracking';
                document.customerTracking.submit();
            }
        </script>
    </head>
    <% String menuPath = "Reports >>  Customer Tracking";
        request.setAttribute("menuPath", menuPath);
    %>
    <style>
        .box {
    background: #ffffff;
    padding: 5px;
    
    -moz-border-radius: 5px;
    border-radius: 5px;
}

#container {
    margin: 0 auto;
    width: 100%;
    background: #ccc;
    overflow: hidden
}
#left, #right {
    float: left;
    width: 50%;
    height: 300px;
}
#left .box {
    
    height : 300px;
}
#right .box {
    
    height : 300px;
}
    </style>
    <body>
        <form name="customerTracking" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <br>
            <table width="900" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Customer Tracking</li>
                            </ul>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >

                                    <tr>
                                        <td>Customer Name</td>
                                        <td height="30"><input name="CustomerName" id="CustomerName" type="text" class="form-control"></td>
                                        <td> Consignment No</td>
                                        <td height="30"><input name="CustomerLoc" id="CustomerLoc" type="text" class="form-control" ></td>
                                        <td> Trip Sheet No</td>
                                        <td height="30"><input name="CustomerLoc" id="CustomerLoc" type="text" class="form-control" ></td>

                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td colspan="2"><input type="button" class="button"   value="Search" onclick="submitPage(this.value)"></td>
                                    </tr>
                                    <tr >
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
             <% if (request.getParameter("tracking") != null) { %>


             <table width="95%" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">
                <thead>
                    <tr height="30">

                        <td class="contenthead">S.No</td>
                        <td class="contenthead">Customer</td>
                        <td class="contenthead">CNote No</td>
                        <td class="contenthead">Trip Id</td>
                        <td class="contenthead">Vehicle No</td>
                        <td class="contenthead">Vehicle Type</td>
                        <td class="contenthead">Route Name</td>
                        <td class="contenthead">Trip Start Date</td>
                        <td class="contenthead">Trip End Date</td>
                        <td class="contenthead">Load Tonnage</td>
                        <td class="contenthead">Status</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text1" >1</td>
                        <td class="text1" >M/s Doshi Foods</td>
                        <td class="text1" >CN/13-14/10023</td>
                        <td class="text1" >TS/13-14/10045</td>
                        <td class="text1" >TN 02 AA 2025</td>
                        <td class="text1" >AL / 2516 / 12 PLT</td>
                        <td class="text1" >Delhi-Chennai</td>
                        <td class="text1" >07-11-2013</td>
                        <td class="text1" >10-11-2013</td>
                        <td class="text1" >25.00</td>
                        <td class="text1" ><img src="/throttle/images/icon_active.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td class="text1" >2</td>
                        <td class="text1" >M/s Bakers Circle</td>
                        <td class="text1" >CN/13-14/10321</td>
                        <td class="text1" >TS/13-14/10925</td>
                        <td class="text1" >DL 22 SR 4578</td>
                        <td class="text1" >AL / 2516 / 12 PLT</td>
                        <td class="text1" >Kashipur - Delhi</td>
                        <td class="text1" >06-11-2013</td>
                        <td class="text1" >10-11-2013</td>
                        <td class="text1" >35.00</td>
                        <td class="text1" ><img src="/throttle/images/icon_active.png" alt="" /></td>
                    </tr>
                    <tr>
                        <td class="text1" >3</td>
                        <td class="text1" >M/s Balaji & Co</td>
                        <td class="text1" >CN/13-14/10549</td>
                        <td class="text1" >TS/13-14/10741</td>
                        <td class="text1" >DL 02 WE 7658</td>
                        <td class="text1" >TATA / 3118 / 16 PLT</td>
                        <td class="text1" >Delhi - Chandigarh</td>                        
                        <td class="text1" >03-11-2013</td>
                        <td class="text1" >04-11-2013</td>
                        <td class="text1" >29.00</td>
                        <td class="text1" ><img src="/throttle/images/icon_closed.png" alt="" /></td>
                    </tr>
                </tbody>
            </table>

             <br>
             <br>


            <div id="container">
<!--                <div id="left">
                    <div class="box">
                        <table width="95%" align="center" border="0" class="border" style="height: 300px">
                        <tr>
                            <td width="107" Colspan="2" class="contenthead">Selected Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Date:</td>
                            <td class="text1">12:10:2013 - 01:11:2013</td>
                        </tr>
                        <tr>
                            <td class="text2">Ro:</td>
                            <td class="text2">ALL - ALL</td>
                        </tr>
                        <tr>
                            <td class="text1">Location:</td>
                            <td class="text1">ALL - ALL </td>
                        </tr>
                        <tr>
                            <td class="text2">Document Types:</td>
                            <td class="text2">CNote</td>
                        </tr>
                        <tr>
                            <td class="text1">Customer:</td>
                            <td class="text1">COOO10002:ABC INDIA PVT LMD </td>
                        </tr>
                        <tr>
                            <td class="text2">Type</td>
                            <td class="text2">---ALL--</td>
                        </tr>

                    </table></div>
                </div>-->
                <div id="center">
                    <div class="box"><div id="googleMap" style="width:500px;height:380px; top: 10px; bottom: 10px; left: 10px"></div></div>
                </div>
            </div>

            <br>



            
            <br>
            <br>
            <%}%>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
