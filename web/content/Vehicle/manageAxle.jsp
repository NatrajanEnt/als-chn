
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script type="text/javascript" language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<script type="text/javascript">

    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }

    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }

    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="vehicle.lable.axleMaster"  text="axleMaster"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="vehicle.lable.axleMaster"  text="axleMaster"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">
            <body onLoad="setImages(1, 0, 0, 0, 0, 0);
            setDefaultVals('<%= request.getAttribute("regNo")%>', '<%= request.getAttribute("typeId")%>', '<%= request.getAttribute("mfrId")%>', '<%= request.getAttribute("usageId")%>', '<%= request.getAttribute("groupId")%>');">
                <form name="viewVehicleDetails"  method="post" >



                    <%@ include file="/content/common/message.jsp" %>

                    <br>

                    <%
                                int index = 1;

                    %>


                    <c:if test="${axleDetails == null }" >
                        <br>
                        <center><font color="red" size="2"> <spring:message code="trucks.label.NoRecordsFound"  text="default text"/> </font></center>
                        </c:if>
                        <c:if test="${axleDetails != null }" >
                        <table class="table table-info mb30 table-hover " id="table" >
                            <thead>
                                <tr>
                                    <th><spring:message code="trucks.label.Sno"  text="default text"/></th>
                            <th><spring:message code="trucks.label.ThisIsFor"  text="default text"/></th>
                            <th><spring:message code="trucks.label.AxleTypeName"  text="default text"/></th>
                            <th><spring:message code="trucks.label.AxleCount"  text="default text"/></th>
                            <th><spring:message code="trucks.label.Action"  text="default text"/></th>
                            </tr>
                            </thead>
                            <tbody>
                                <%String style = "text1";%>
                                <c:forEach items="${axleDetails}" var="veh" >
                                    <%
                                                if ((index % 2) == 0) {
                                                    style = "text1";
                                                } else {
                                                    style = "text2";
                                                }%>
                                    <tr>
                                        <td > <%= index++%> </td>
                                        <td > <c:out value="${veh.typeFor}" /> </td>
                                        <td > <c:out value="${veh.axleTypeName}" /> </td>
                                        <td ><c:out value="${veh.axleCount}" /></td>
                                        <td >
            <!--                                <a href='/throttle/handleViewAxleDetails.do?axleId=<c:out value="${veh.vehicleAxleId}" />&axleCount=<c:out value="${veh.axleCount}" />&axleTypeName=<c:out value="${veh.axleTypeName}" />'  >edit </a>-->
                                            &nbsp;
                                            &nbsp;
                                            <span class="label label-info"><a href='/throttle/handleViewAxleDetails.do?axleId=<c:out value="${veh.vehicleAxleId}" />&axleCount=<c:out value="${veh.axleCount}" />&typeFor=<c:out value="${veh.typeFor}" />&axleTypeName=<c:out value="${veh.axleTypeName}" />'  ><font color="white"><spring:message code="trucks.label.view"  text="default text"/></font> </a></span>
                                            &nbsp;
                                            &nbsp;
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <center>
                        <input type="button" class="btn btn-success" name="add" value="<spring:message code="trucks.label.Add"  text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;
                    </center>
                    <br>
                    <br>

                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span><spring:message code="trucks.label.EntriesPerPage"  text="default text"/></span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text"><spring:message code="trucks.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span><spring:message code="trucks.label.of"  text="default text"/>  <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
            <script type="text/javascript">
                function submitPage(value) {

                    if (value == 'add') {
                        document.viewVehicleDetails.action = '/throttle/vehicleAxleTyreMaster.do';
                        document.viewVehicleDetails.submit();
                    }
                }


                function setDefaultVals(regNo, typeId, mfrId, usageId, groupId) {

                    if (regNo != 'null') {
                        document.viewVehicleDetails.regNo.value = regNo;
                    }
                    if (typeId != 'null') {
                        document.viewVehicleDetails.typeId.value = typeId;
                    }
                    if (mfrId != 'null') {
                        document.viewVehicleDetails.mfrId.value = mfrId;
                    }
                    if (usageId != 'null') {
                        document.viewVehicleDetails.usageId.value = usageId;
                    }
                    if (groupId != 'null') {
                        document.viewVehicleDetails.groupId.value = groupId;
                    }
                }


                function getVehicleNos() {
                    //onkeypress='getList(sno,this.id)'
                    var oTextbox = new AutoSuggestControl(document.getElementById("regno"), new ListSuggestions("regno", "/throttle/getVehicleNos.do?"));
                }

            </script>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>