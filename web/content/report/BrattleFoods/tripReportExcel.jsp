<%-- 
    Document   : tripReportExcel
    Created on : 20 Jan, 2016, 9:51:40 AM
    Author     : pavithra
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
            
        </style>
    </head>
    <body>
       
        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleWiseProfitability-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
            <c:if test = "${TripDetails != null}" >
               <table align="center" width="100%" border="0" id="table" class="sortable">
                    <thead>
                        <tr>
                            <th><h3>S.No</h3></th>
                            <th ><h3>Order NO</h3></th>
                            <th ><h3>Order Type</h3></th>
                            <th><h3>Billing party</h3></th>
                            <th><h3>Customer name</h3></th>
                            <th><h3>20FT</h3></th>
                            <th><h3>40FT</h3></th>
                            <th><h3>Shipper</h3></th>
                            <th><h3>No of Trip(s)</h3></th>
                           
                        </tr>
                    </thead>
                    <tbody>
                       <% int index = 0;%>
                        <c:forEach items="${TripDetails}" var="PL">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text1";
                                        } else {
                                            classText = "text2";
                                        }
                            %>
                            <tr height="30">
                                <td class="<%=classText%>"  align="left"> <%= index + 1%> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.orderNo}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.movementType}"/> </td>
                                <td class="<%=classText%>" align="left"> <c:out value="${PL.billingParty}" /></td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.customerName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.twentyFT}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.fortyFT}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.linerName}"/> </td>
                                <td class="<%=classText%>"  align="left"> <c:out value="${PL.noOfTrips}"/> </td>
                              
                            </tr>
                          <% index++;%>
                        </c:forEach>
                </tbody>
            </table>
                         </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>