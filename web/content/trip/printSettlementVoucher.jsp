<%-- 
    Document   : PrintVoucher
    Created on : Oct 22, 2013, 10:36:08 PM
    Author     : Arul
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


        <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

        <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
        <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
        <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>
        <%@ page import="ets.domain.operation.business.OperationTO" %>


        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.*" %>
        <%@ page import="java.lang.Double" %>

        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" href="style.css" />
        <%@ page import="ets.domain.contract.business.ContractTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <title>Cash Payment Voucher</title>
    </head>

    <script>
function generatePDF(){
    var doc = new jsPDF();
    $('#cmd').click(function () {
        doc.fromHTML($('#printDiv').html(), 15, 15, {
            'width': 170,
                'elementHandlers': specialElementHandlers
        });
        doc.save('sample-file.pdf');
    });
}

function viewTripDetails() {
                document.enter.action = '/throttle/viewTripSheets.do?menuClick=1&statusId=12&tripType=1&admin=No&movement=0&subStatusId=0&vendorType=0';
//                document.enter.action = '/throttle/viewTripSheets.do?&statusId=12&tripType=1&admin=No';
                document.enter.submit();
            }

        function print()
        {
            var DocumentContainer = document.getElementById("printDiv");
            var WindowObject = window.open('', "TrackHistoryData",
                    "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
            WindowObject.document.writeln(DocumentContainer.innerHTML);
            WindowObject.document.close();
            WindowObject.focus();
            WindowObject.print();
            WindowObject.close();
        }

        function back()
        {
            window.history.back()
        }

    </script>
    <%
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        String voucherDate = dateFormat.format(date);;
        String billingPartyId = (String)request.getAttribute("billingPartyId");
    %>



    <body onload="calcExpenses();">
    <style type="text/css">
        #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
    </style>
    <form name="enter" method="post">
    <div id="printDiv">

        <table width="860" border="1" cellpadding="0" cellspacing="0" align="center" border="0" style="font-family:Arial, Helvetica, sans-serif; font-size:13px; ">

            <tr>
                <td  class="contenthead" colspan="4" >
                    Trip Settlement Voucher
                    <input type="hidden" name="tripSheetId" id="tripSheetId" value="<c:out value="${tripSheetId}"/>"  />
                    <input type="hidden" name="tollAmount" id="tollAmount" value="<c:out value="${tollAmount}"/>"  />
                    <input type="hidden" name="battaAmount" id="battaAmount" value="<c:out value="${driverBatta}"/>"  />
                    <input type="hidden" name="incentiveAmount" id="incentiveAmount" value="<c:out value="${driverIncentive}"/>"  />
                    <input type="hidden" name="fuelPrice" id="fuelPrice" value="<c:out value="${fuelPrice}"/>"  />
                    <input type="hidden" name="milleage" id="milleage" value="<c:out value="${milleage}"/>"  />
                    <input type="hidden" name="reeferConsumption" id="reeferConsumption" value="<c:out value="${reeferConsumption}"/>"  />
                    <input type="hidden" name="vehicleTypeId" id="vehicleTypeId" value="<c:out value="${vehicleTypeId}"/>"  />
                    <input type="hidden" name="gpsKm" id="gpsKm" value="<c:out value="${gpsKm}"/>"  />
                    <input type="hidden" name="totalRunKm" id="totalRunKm" value="<c:out value="${runKm}"/>"  />
                    <input type="hidden" name="totalDays" id="totalDays" value="<c:out value="${totaldays}"/>"  />
                    <input type="hidden" name="totalRefeerHours" id="totalRefeerHours" value="<c:out value="${runHm}"/>"  />
                    <input type="hidden" name="estimatedExpense" id="estimatedExpense" value="<c:out value="${estimatedExpense}"/>"  />
                    <input type="hidden" name="miscValue" id="miscValue" value="<c:out value="${miscValue}"/>"  />
                    <input type="hidden" name="driverCount" id="driverCount" value="<c:out value="${driverCount}"/>"  />
                    <input type="hidden" name="extraExpenseValue" id="extraExpenseValue" value="<c:out value="${extraExpenseValue}"/>"  />
                    <input type="hidden" name="extraExpenseValue" id="extraExpenseValue" value="<c:out value="${tripAdvance}"/>"  />
                    <input type="hidden" name="bpclAmount" id="bpclAmount" value="<c:out value="${bpclAmount}"/>"  />
                </td>
            </tr>
            <script>
                function calcExpenses() {
                    var runKM = document.getElementById("totalRunKm").value;
                    var tollAmount = document.getElementById("tollAmount").value;
                    var driverBatta = document.getElementById("battaAmount").value;
                    var driverIncentive = document.getElementById("incentiveAmount").value;
                    var transitDays = document.getElementById("totalDays").value;
                    var mileage = document.getElementById("milleage").value;
                    var fuelPrice = document.getElementById("fuelPrice").value;
                    var reeferMilleage = document.getElementById("reeferConsumption").value;
                    var driverCount = document.getElementById("driverCount").value;
                    var extraExpenseValue = document.getElementById("extraExpenseValue").value;


                    //Calc Bhatta
                    var bhatta = (transitDays * 100) * driverCount;
                    $("#bhatta").text(bhatta);
                    $("#totalTripBhatta").val(bhatta);
                    //Calc Bhatta


                    //Calculate Misc
                    var reeferHour = document.getElementById("totalRefeerHours").value;
                    var miscValue = document.getElementById("miscValue").value;
                    var misc = miscValue * runKM;
//                                var totalMisc = (misc / 100) * 5;
                    var totalMisc = ((parseFloat(misc) + parseFloat(extraExpenseValue) + parseFloat(bhatta)) / 100) * 5;
                    totalMisc = totalMisc.toFixed(2);
                    $("#totalMisc").text(totalMisc);
                    $("#totalTripMisc").val(totalMisc);
                    //Calculate Misc



                    //Calc Diesel Consume
                    var reeferConsume = (reeferHour * reeferMilleage).toFixed(2);
                    var vehicleConsume = (runKM * mileage).toFixed(2);
                    var totalConsume = parseInt(vehicleConsume) + parseInt(reeferConsume);
                    var ratePerKm = parseFloat(document.getElementById("estimatedExpense").value) / parseFloat(document.getElementById("totalRunKm").value);
                    ratePerKm = ratePerKm.toFixed(2);
                    $("#ratePerKm").text(ratePerKm);
                    $("#vehicleDiesel").text(vehicleConsume);
                    $("#reeferDiesel").text(reeferConsume);
                    $("#totalConsume").text(totalConsume.toFixed(2));
                    $("#tripDieselConsume").val(totalConsume.toFixed(2));
                    //Calc Diesel Consume

                    //Calc RCm Allocation
                    //var rcmAllocation = totalConsume  * fuelPrice;
                    //alert(document.getElementById("estimatedExpense").value)
                    var rcmAllocation = parseFloat(document.getElementById("estimatedExpense").value);
                    $("#rcmAllocation").text(rcmAllocation.toFixed(2));
                    //Calc RCm Allocation

                    //Calc Total Expense
                    var extraExpense = $("#extraExpense").text();


                    var nettExtraExpense = (parseFloat(bhatta) + parseFloat(totalMisc) + parseFloat(extraExpense)).toFixed(2);
                    //alert(nettExtraExpense);
                    $("#extraExpense").text(nettExtraExpense);
                    $("#tripExtraExpense").val(nettExtraExpense)
                    extraExpense = nettExtraExpense;
                    var totalExpense = parseFloat(rcmAllocation);
                    $("#totalExpense").text(totalExpense.toFixed(2));
                    //  $("#totalTripExpense").val(totalExpense.toFixed(2));
                    //Calc Total Expense

                    //Calc Balance
                    var driverFuelExpensetmp = "";
                    var driverFuelExpense = "";
                    var totalTripExpense = parseFloat(document.getElementById("totalTripExpense").value);
                    var totalTripAdvance = parseFloat(document.getElementById("totalTripAdvance").value);
                    if(document.getElementById("driverFuelExpense").value == ""){
                       driverFuelExpensetmp = "0.00";
                    }
                    driverFuelExpense = parseFloat(driverFuelExpensetmp); //CASH
                    var driverExp = parseFloat(document.getElementById("driverExp").value);
                    var driverSum = parseFloat(driverFuelExpense) + parseFloat(driverExp);
                    var recoverable = parseFloat(document.getElementById("recoverable").value);
//                    alert("totalTripExpense-"+totalTripExpense);
//                    alert("totalTripAdvance-"+totalTripAdvance);
                    
//                    alert("recoverable-"+recoverable);
//                    if(totalTripAdvance != 0 || totalTripAdvance != ""){
//                        var balance = (parseFloat(totalTripAdvance) - parseFloat(totalTripExpense)  - parseFloat(recoverable)).toFixed(2);
//                    }else{
//                        var balance = (parseFloat(totalTripExpense)  - parseFloat(recoverable)).toFixed(2);
//                    }
//                        alert("parseFloat(driverSum)---"+parseFloat(driverSum))
//                        alert("parseFloat(totalTripAdvance)---"+parseFloat(totalTripAdvance))
//                        alert("parseFloat(recoverable)---"+parseFloat(recoverable))
//                        alert("balance)---"+(parseFloat(driverSum) - parseFloat(totalTripAdvance)  - parseFloat(recoverable)).toFixed(2))
                    var balance = (parseFloat(driverSum) - parseFloat(totalTripAdvance)  - parseFloat(recoverable)).toFixed(2);
                    
                    $("#balance").text(balance);
                    $("#tripBalance").val(balance);
                    //Calc Balance

                    //Calc End Balance
                    var startBalance = $("#startBalance").text();
//                    alert("balance"+balance);
                    var endBalance = parseFloat(balance);
                    
                    $("#endBalance").text(endBalance.toFixed(2));
                    $("#tripEndBalance").val(endBalance.toFixed(2));
                    $("#tripStartingBalance").val(endBalance.toFixed(2));
                }
            </script>
            <tr>

                <td height="30" style="text-align:center; text-transform:uppercase;" colspan="4">
                    <table align="left" >
                        <tr>
                            <td align="left">&ensp;&ensp;&emsp;&emsp;
                                <!--<img src="images/Chakiat Logo.png"  alt="Chakiat"  width="150" height="70"/>-->
                            </td>
                            <td style="padding-left: 180px; padding-right:42px;">
                                <center>
                                    <b>C.A Logistics India Pvt.Ltd.</b></center><br>
                                <center><u>Driver Settlement Voucher</u></center>
                            </td>
                            <td align="right">
                                <br>
                                <div id="externalbox" style="width:3in">
                                    <div id="inputdata" ></div>
                                </div>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>

            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Customer Reference No</td>
                <td  height="30" height="30" style="text-align:left;"><label id="cNoteNo">&emsp;<c:out value="${customerOrderReferenceNo}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Trip Start Date/Time</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${startDate}"/> &nbsp;<c:out value="${startTime}"/></label></td>
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Customer</td>
                <td  height="30" height="30" style="text-align:left;"><label id="customer">&emsp;<c:out value="${customerName}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Trip End Date/Time</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${tripEndDate}"/> &nbsp;<c:out value="${tripEndTime}"/></label></td>
            </tr>
            
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;LR Number</td>
                <td  height="30" height="30" style="text-align:left;"><label id="lrNumber">&emsp;<c:out value="${lrNumber}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&emsp;No of Days</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${totaldays}"/> </label></td>
            </tr>
            <tr>
                <td  height="30" height="30" colspan="4" style="text-align:left;">&emsp;Route Interim &emsp;
                <label id="lrNumber">&emsp;&emsp;<c:out value="${routeInfo}"/></label></td>
            </tr>
            
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Vehicle Number</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${vehicleNo}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Diesel (ltrs)</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${fuelLiters}"/></label></td>
<!--                <td  height="30" height="30" style="text-align:left;">Trip No</td>
                <td  height="30" height="30" style="text-align:left;"><label><c:out value="${tripCode}"/></label></td>-->

            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Container Number</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${containerNo}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Size</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;
                       <c:if test = "${containerQuantity == 1}" >
                            1 * 40
                        </c:if>
                       <c:if test = "${containerQuantity == 2}" >
                            1 * 20
                        </c:if>
                       <c:if test = "${containerQuantity == 3}" >
                            2 * 20
                       </c:if>
                    </label></td>
<!--                <td  height="30" height="30" style="text-align:left;">Trip No</td>
                <td  height="30" height="30" style="text-align:left;"><label><c:out value="${tripCode}"/></label></td>-->

            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Driver</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${driverName1}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Bunk</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${bunkName}"/></td>
                <!--<td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${customerOrderReferenceNo}"/></td>-->
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Cleaner</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${cleanerName}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Expense Details</td>
                <td  height="30" height="30" style="text-align:left;"><label></label></td>
                <!--<td  height="30" height="30" style="text-align:left;"><label><c:out value="${dieselPrice}"/></label></td>-->
            </tr>
<!--            <tr>
                <td  height="30" height="30" style="text-align:left;">Stop No 1</td>
                <td  height="30" height="30" style="text-align:left;">Pickup</td>
                <td  height="30" height="30" style="text-align:left;"><label><c:out value="${origin}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&nbsp;</td>
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">Stop No 2</td>
                <td  height="30" height="30" style="text-align:left;">Drop</td>
                <td  height="30" height="30" style="text-align:left;"><label><c:out value="${destination}"/></label></td>
                <td  height="30" height="30" style="text-align:left;">&nbsp;</td>
            </tr>-->
<!--            <tr>
                <td  height="30" height="30" style="text-align:left;"></td>
                <td  height="30" height="30" style="text-align:left;">
                    <input type="hidden" name="tripStartingBalance" id="tripStartingBalance" value="0" />
                </td>
                <td  height="30" height="30" style="text-align:left;" colspan="2">&nbsp;</td>
            </tr>-->
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Odometer Starting</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${startKm}"/></label></td>
                 <td  height="30" height="30" style="text-align:left;">&emsp;Pooja Expense</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${poojaExpense}"/></label>
                <input type="hidden" name="driverExp" id="driverExp" value="<c:out value="${poojaExpense+driverBhatta+cleanerBhatta}"/>" /></td>  
                
                    
                <td  height="30" height="30" style="text-align:left;display:none;">&emsp;Reefer Hours Starting</td>
                <td  height="30" height="30" style="text-align:left;display:none;"><label>&emsp;<c:out value="${startHm}"/></label></td>
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Odometer Ending</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${endKm}"/></label></td>
                
                <td  height="30" height="30" style="text-align:left;">&emsp;Driver Bhatta</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${driverBhatta}"/>
                    
                <td  height="30" height="30" style="text-align:left;display:none;">&emsp;Reefer Hours Ending</td>
                <td  height="30" height="30" style="text-align:left;display:none;"><label>&emsp;<c:out value="${endHm}"/></label></td>
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Odometer Km</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${runKm}"/></label></td>                    
                
                  <td  height="30" height="30" style="text-align:left;">&emsp;Cleaner Bhatta</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${cleanerBhatta}"/>
                </td>
                <td  height="30" height="30" style="text-align:left;display:none;">&emsp;Reefer Hours</td>
                <td  height="30" height="30" style="text-align:left;display:none;"><label>&emsp;<c:out value="${runHm}"/></label></td>
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;GPS  Km</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${gpsKm}"/></label>
                </td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Container Lift On/Off Charges</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${ContainerLiftCharges}"/>
                </td>
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Km Run</td>
                <td  height="30" height="30" style="text-align:left;"><label>&emsp;<c:out value="${gpsKm}"/></label></td>
                <td  height="30" height="30"  style="text-align:left;">&emsp;Weightment Expenses</td>
                <td  height="30" height="30"  style="text-align:left;">&emsp;<c:out value="${WeightmentExpenses}"/></td>

            </tr>
            <%--     <tr>
                     <td height="30" height="30" style="text-align:left;">RCM</td>
                     <td height="30" height="30" style="text-align:left;"><c:out value="${rcmExpense}"/></td>
                     <td  height="30" height="30" style="text-align:left;">Extra Expenses</td>
                     <td  height="30" height="30" style="text-align:left;"><label id="extraExpense"><c:out value="${tripExpense}"/></label>
                     <input type="hidden" name="tripExtraExpense" id="tripExtraExpense" value="" /></td>

                </tr> --%>

            <tr>
               <td  height="30" height="30" style="text-align:left;">&emsp;Advance Paid</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${tripAdvance}"/>
                    <input type="hidden" name="totalTripAdvance" id="totalTripAdvance" value="<c:out value="${tripAdvance}"/>" />
                </td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Toll Cash</td>
                <td height="30" height="30" style="text-align:left;">&emsp;<c:out value="${tollcash}"/></td>

            </tr>
            <tr>
                 <td  height="30" height="30" style="text-align:left;">&emsp;Driver Fuel Expense</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${driverFuelExpense}"/>
                    <input type="hidden" name="driverFuelExpense" id="driverFuelExpense" value="<c:out value="${driverFuelExpense}"/>" />
                    <input type="hidden" name="tripRcmAllocation" id="tripRcmAllocation" value="" />
                </td>
                
                <td  height="30" height="30" style="text-align:left;">&emsp;Loading / Unloading Charges</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${loadCharges}"/></td>
                <td height="30" height="30" style="text-align:left;display:none;">&emsp;Misc Expense</td>
                <td  height="30" height="30" style="text-align:left;display:none;">&emsp;<c:out value="${miscExpense}"/>
                    <input type="hidden" name="totalTripMisc" id="totalTripMisc" value="" />
                </td>
            </tr>
            <tr >
                <td  height="30" height="30" style="text-align:left;">&emsp;Fuel Recovery(ltrs)</td>
                <td  height="30" height="30" style="text-align:left;">&emsp; <c:out value="${recoveryFuel}"/></td>
                <td  height="30" height="30" style="text-align:left;display: none;">&emsp;Total Expenses</td>
                <td  height="30" height="30" style="text-align:left;display: none;">&emsp; <c:out value="${tripExpense}"/>
                    <input type="hidden" name="totalTripExpense" id="totalTripExpense" value="<c:out value="${tripExpense}"/>" />
                    <input type="hidden" name="totalTripBhatta" id="totalTripBhatta" value="" />
                </td>
                 <td  height="30" height="30" style="text-align:left;">&emsp;Total Fuel Expenses</td>
                <td height="30" height="30" style="text-align:left;">&emsp;<c:out value="${fuelExpense}"/></td>

            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Recoverable</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${recoveryAmount}"/>
                <input type="hidden" name="recoverable" id="recoverable" value="<c:out value="${recoveryAmount}"/>">
                <!--<input type="hidden" name="recoverable" id="recoverable" value="<c:out value="${enrouteDamage}"/>">-->
                </td>
                
                <td  height="30" height="30" style="text-align:left;display:none;">&emsp;Balance</td>
                <td  height="30" height="30" style="text-align:left;display:none;">&emsp;<label id="balance"></label>
                    <input type="hidden" name="tripBalance" id="tripBalance" value="" />
                </td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Other Expenses</td> <!-- detention + other charges-->
                <td height="30" height="30" style="text-align:left;">&emsp;<c:out value="${OtherExp}"/></td>
                
                <td height="30" height="30" style="text-align:left;display:none;">&emsp;IOCL Transaction</td>
                <td height="30" height="30" style="text-align:left;display:none;">&emsp;<c:out value="${bpclAmount}"/></td>
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Payable</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<label id="endBalance"></label>
                    <input type="hidden" name="tripEndBalance" id="tripEndBalance" value="" />
                </td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Total</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${tripExpense}"/></td>
                
                <td  height="30" height="30" style="text-align:left;display:none;">&emsp;FastTag Trans</td>
                <td  height="30" height="30" style="text-align:left;display:none;">&emsp;<label id="fastTag">&emsp;</label></td>
            </tr>
            <tr>
                <td  height="30" height="30" style="text-align:left;">&emsp;Remarks for Extra Expenses</td>
                <td  height="30" height="30" style="text-align:left;" >&emsp;<c:out value="${settlementRemarks}"/></td>
                <td  height="30" height="30" style="text-align:left;">&emsp;Pay Mode</td>
                <td  height="30" height="30" style="text-align:left;">&emsp;<c:out value="${paymentMode}"/></td>
            </tr>

            <tr>
                <% String total = "";
                    if (request.getAttribute("amount") != null) {
                        total = (String) request.getAttribute("amount");
                    } else {
                        total = "00";
                    }
                %>
                <% if(total.contains("-")) {
                    total = total.replaceAll("-","");
                %>
                <td colspan="5" height="35" align="left">
                    <b>Return amount in rupees:</b><%float spareTotal = 0.00F;
                        spareTotal = Float.parseFloat(total);
                    %>
                    <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                        <% spareTotalRound.setRoundedValue(String.valueOf(spareTotal));%>
                        <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                        <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                        <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />Only</b>
                    </jsp:useBean>
                    </p> &nbsp;&nbsp;&nbsp;<c:out value="${remarks}"/>
                </td>
                <%}else{ %>
                <td colspan="5" height="35" align="left">
                    <b>&emsp;Balance amount in rupees:</b><%float spareTotal = 0.00F;
                        spareTotal = Float.parseFloat(total);
                    %>
                    <jsp:useBean id="spareTotalRound1"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                        <% spareTotalRound1.setRoundedValue(String.valueOf(spareTotal));%>
                        <%-- <b><jsp:getProperty name="spareTotalRound" property="roundedValue" /> </b> --%>
                        <% spareTotalRound1.setNumberInWords(spareTotalRound1.getRoundedValue());%>
                        <b><jsp:getProperty name="spareTotalRound1" property="numberInWords" />Only</b>
                    </jsp:useBean>
                    </p> &nbsp;&nbsp;&nbsp;<c:out value="${remarks}"/>
                </td>   
                <%}%>

            </tr>
            <tr>
                <td height="90" colspan="2" valign="bottom">&emsp;Prepared By</td>
                <td height="90" colspan="2" valign="bottom">&emsp;Passed By</td>
            </tr>

        </table>
        <script type="text/javascript">
            /* <![CDATA[ */
            function get_object(id) {
                var object = null;
                if (document.layers) {
                    object = document.layers[id];
                } else if (document.all) {
                    object = document.all[id];
                } else if (document.getElementById) {
                    object = document.getElementById(id);
                }
                return object;
            }
//get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
            get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tCode}"/>', 0);
            /* ]]> */
        </script>

    </div><div id="editor"></div>
    <br>
    <center>  
        <input type="button" id="printbtn" class="button"  value="Back" onClick="viewTripDetails();" />
        &emsp;    <input align="center" type="button" class="button"  onclick="print();" value = "Print"   />
        &emsp;    <input align="center" type="button" class="button" id="cmd" onclick="generatePDF();" value = "Generate PDF"   />
    </center>
</form>
</body>
</html>
