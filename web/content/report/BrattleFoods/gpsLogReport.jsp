
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">



            function viewLogDetails(tripId) {
                window.open('/throttle/viewTripLog.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }


            //auto com

            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#vehicleNo').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getRegistrationNo.do",
                            dataType: "json",
                            data: {
                                regno: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#vehicleId').val(tmp[0]);
                        $('#vehicleNo').val(tmp[1]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });


        </script>



        <script type="text/javascript">
            function submitPage(value) {
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleGPSLog.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleGPSLog.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
                if('<%=request.getAttribute("tripId")%>' != 'null'){
                    document.getElementById('tripId').value='<%=request.getAttribute("tripId")%>';
                }
            }
              
                
                function viewVehicleDetails(vehicleId) {
                window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
            }
        </script>
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">GPS Log Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="setValue();sorter.size(20);">
        <form name="accountReceivable"   method="post">
            <br>
            <br>
            <br>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;"  align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">GPS Log Report</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr height="30">
                                         <td align="center">Vehicle No</td>
                                        <td height="30">
                                            <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                                            <input type="text" name="vehicleNo" id="vehicleNo" value="<c:out value="${vehicleNo}"/>" class="textbox"/>
                                        </td>
                                         <td align="center">Trip Code</td>
                                        <td height="30">
                                            <input type="text" name="tripId" id="tripId" value="" class="textbox"/>
                                        </td>
                                    </tr>
                                    <tr height="30">
                                        <td align="center"><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td align="center"><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>" ></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td  align="right"><input type="button" class="btn btn-info" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td><input type="button" class="btn btn-info" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>


            <br>
            <br>
            <br>
            <c:if test="${logDetailsSize != '0'}">
                  <table align="center" border="0" id="table" class="sortable" width="100%" >
                    <thead>
                      <tr id="tableDesingTH" height="30">
                            <th align="center"><h3>S.No</h3></th>
                           <th align="center"><h3>Vehicle No</h3></th>
                           <th align="center"><h3>Trip Code</h3></th>
                           <th align="center"><h3>Trip Date</h3></th>
                           <th align="center"><h3>Current Location</h3></th>
                            <th align="center"><h3>Distance Travelled</h3></th>
                            <th align="center"><h3>Current Temperature</h3></th>
                            <th align="center"><h3>Log Date</h3></th>
                        </tr> 
                    </thead>
                            <tbody>
                    <% int index = 1;%>
                            <c:forEach items="${logDetails}" var="logDetails">
                                    <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                                    %>
                                        <tr>
                                            <td class="<%=classText%>"><%=index++%></td>
                                            <td  width="90" class="<%=classText%>">
                                               <a href="#" onclick="viewVehicleDetails('<c:out value="${logDetails.vehicleId}"/>')"><c:out value="${logDetails.vehicleNo}"/></a></td>

                                        <td width="90" class="<%=classText%>">
                                                <a href="#" onclick="viewLogDetails('<c:out value="${logDetails.tripId}"/>')"><c:out value="${logDetails.tripCode}"/></a>
                                       </td>
                                            <td class="<%=classText%>" ><c:out value="${logDetails.tripDate}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${logDetails.location}"/></td>
                                            <td width="140" class="<%=classText%>"  ><c:out value="${logDetails.distance}"/></td>
                                            <td width="140" class="<%=classText%>"  ><c:out value="${logDetails.currentTemperature}"/></td>
                                            <td class="<%=classText%>"  ><c:out value="${logDetails.logDate}"/></td>
                                        </tr>

                            </c:forEach>
                </tbody>
                </table>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
          <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" >5</option>
                        <option value="10">10</option>
                        <option value="20" selected="selected">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
            </c:if>
        </form>
     </body>    
   </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>