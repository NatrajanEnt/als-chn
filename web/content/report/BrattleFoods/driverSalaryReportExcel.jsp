<%-- 
    Document   : driverSettlementReportExcel
    Created on : M 13, 2019, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="BPCLTransaction" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "TripExtraExpense-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
                        <c:if test="${driverSalaryReportDetails != null}">
                         <table align="center" border="1" id="table" class="sortable" style="width:100%;" >
                            <thead height="30">
                         <tr id="tableDesingTH" height="30">
                           <th align="center"><h3>Trip No</h3></th>
                           <th align="center"><h3>Vehicle No</h3></th>
                           <th align="center"><h3>Driver Name</h3></th>
                           <th align="center"><h3>Movement Date - From</h3></th>
                           <th align="center"><h3>Movement Date - To</h3></th>
                           <th align="center"><h3>Place</h3></th>
                           <th align="center"><h3>Customer</h3></th>
                           <th align="center"><h3>Collection Bata</h3></th>
                           <th align="center"><h3>Wages</h3></th>
                           <th align="center"><h3>Diesel Recovery (in Negative Figure)</h3></th>
                           <th align="center"><h3>Damages (in Negative Figure)</h3></th>
                           <th align="center"><h3>Salary Advance (in Negative Figure)</h3></th>
                           <th align="center"><h3>Net Payable</h3></th>
                           
                             
                        </tr> 
                    </thead>
                            <tbody>
                   
                            <c:forEach items="${driverSalaryReportDetails}" var="driverSR">
                               
                                   <c:set var="totlaNetPayable" value="${0 }"/>
                                   <tr>
                                        <td   ><c:out value="${driverSR.tripCode}"/></td>
                                        <td   ><c:out value="${driverSR.regNo}"/></td>
                                        <td   ><c:out value="${driverSR.empName}"/></td>
                                        <td   ><c:out value="${driverSR.startDate}"/></td>
                                        <td   ><c:out value="${driverSR.endDate}"/></td>
                                        <td   ><c:out value="${driverSR.routeInfo}"/></td>
                                        <td   ><c:out value="${driverSR.customerName}"/></td>
                                        <td   ><c:out value="${driverSR.collectionBata}"/></td>
                                        <td   ><c:out value="${driverSR.wages}"/></td>
                                        <td   ><c:out value="${driverSR.recoveryAmount}"/></td>
                                        <td   ><c:out value="${driverSR.damage}"/></td>
                                        <td   ><c:out value="${driverSR.salaryAdvance}"/></td>
                                        <td   >
                                             <c:set var="totlaNetPayable" value="${(driverSR.collectionBata + driverSR.wages) - (driverSR.recoveryAmount + driverSR.damage)}"/>
                                             <fmt:formatNumber type="number"  maxFractionDigits="2" value="${totlaNetPayable }" />
                                        </td>
                                    </tr>
                            </c:forEach>
                </tbody>
                </table>
            </c:if>
</form>
    </body>
</html>