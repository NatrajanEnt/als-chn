/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.department.business;

/**
 *
 * @author vinoth
 */
import ets.domain.department.data.DepartmentDAO;
import ets.domain.department.business.DepartmentTO;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import java.util.*;

public class DepartmentBP {

    private DepartmentDAO departmentDAO;

    public DepartmentDAO getDepartmentDAO() {
        return departmentDAO;
    }

    public void setDepartmentDAO(DepartmentDAO departmentDAO) {
        this.departmentDAO = departmentDAO;
    }

    public int processInsertDepartment(DepartmentTO departmentTO, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;       
        status = departmentDAO.doInsertDepartment(departmentTO, userId);       
        if (status == 0) {
           throw new FPBusinessException("EM-DE-02");
        }
        return status;
    }

    public ArrayList getDepartmentList() throws FPBusinessException, FPRuntimeException {
        ArrayList departmentList = new ArrayList();
        departmentList = departmentDAO.getDepartmentList();
        if (departmentList.size() == 0) {
           throw new FPBusinessException("EM-GM-01");
        }
        return departmentList;
    }

    public ArrayList processActiveDeptList() throws FPBusinessException, FPRuntimeException {
        ArrayList departmentList = new ArrayList();
        ArrayList activeDeptList = new ArrayList();
        DepartmentTO depTo = null;
        departmentList = departmentDAO.getDepartmentList();
        Iterator itr = departmentList.iterator();
        while(itr.hasNext()){
            depTo = new DepartmentTO();
            depTo = (DepartmentTO) itr.next();            
            if(depTo.getStatus().equalsIgnoreCase("Y")){
                activeDeptList.add(depTo);
            }
        }
         if (activeDeptList.size() == 0) {
           throw new FPBusinessException("EM-GEN-01");
        }
        return activeDeptList;
    }

    public int processUpdatetDepartment(ArrayList List, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;       
        status = departmentDAO.doUpdateDepartment(List, userId);       
//        if (status == 0) {
//            throw new FPBusinessException("EM-DE-03");
//        }
        return status;
    }
   
}