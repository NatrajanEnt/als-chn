
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });


    function replaceSpecialCharacters()
    {
        var content = document.getElementById("requestremarks").value;

        //alert(content.replace(/[^a-zA-Z0-9]/g,'_'));
        // content=content.replace(/[^a-zA-Z0-9]/g,'');
        content = content.replace(/[@&\/\\#,+()$~%'":*?<>{}]/g, ' ');
        document.getElementById("requestremarks").value = content;
        //alert("Repalced");

    }
</script>

</head>
<script language="javascript">
    function submitPage() {

        document.approve.action = '/throttle/saveBlockGr.do';
        document.approve.submit();
    }
    function setFocus() {
        document.approve.advancerequestamt.select();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Operation</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">Canceled Gr </li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setFocus();">
                <form name="approve"  method="post" >
                    <%
                                request.setAttribute("menuPath", "Fuel >> Edit Fuel");
                                String tripid = request.getParameter("tripid");
                                String type = "M";
                    %>
                    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <c:if test="${cancelGRDeatils != null}">

                            <table class="table table-info mb30 table-hover" id="table"  style="width:100%">
                            <thead>
                                <tr>
                                    <th  >S No</th>
                                    <th  >GR NO</th>
                                    <th >GR Date </th>
                                    <th >GR Status </th>
                                    <th >Cancel By </th>
                                    <th >Remarks </th>
                                </tr>
                            </thead>
                            <%int index1=1;%>
                            <c:forEach items="${cancelGRDeatils}" var="tripGr">
                                <tr>
                                    <td><%=index1++%></td>
                                    <td><c:out value="${tripGr.grNo}"/>
                                    </td>
                                    <td>
                                        <c:out value="${tripGr.grDate}"/>
                                    </td>
                                    <td>
                                        <c:out value="${tripGr.grStatus}"/>
                                    </td>
                                    <td>
                                        <c:out value="${tripGr.userName}"/>
                                    </td>
                                    <td>
                                        <c:out value="${tripGr.remarks}"/>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>