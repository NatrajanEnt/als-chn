<%-- 
    Document   : trialBalanceExcel
    Created on : 22 Jan, 2016, 6:04:10 PM
    Author     : pavithra
--%>




<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
           <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "Trial_Balance"+curDate+".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
         
            <center>
                <h2>CHETTINAD LOGISTICS PRIVATE LIMITED</h2>
                <h2>Trial Balance</h2>
                <br>
                <table>
                    <tr>
                        <td><b>From : </b><%=request.getAttribute("fromDate")%></td>
                        <td></td>
                        <td><b>To : </b><%=request.getAttribute("toDate")%></td>
                        <td></td>
                    </tr>
                </table>
                <br>
                
            </center>
            <br>
            <table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">
                <tr height="30">
                    <td  align="left" width="60%" class="contenthead" scope="col"><b>ACCOUNT HEAD</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>DEBITS</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>CREDITS</b></td>
                </tr>
                <c:set var="debitAmount" value="${0.00}"/>
                <c:set var="creditAmount" value="${0.00}"/>
                <c:if test="${levelMasterList != null}">
                    <c:forEach items="${levelMasterList}" var="level">

                        <tr height="30">
                            <td  class="text1" ><b><c:out value="${level.groupName}"/></b></td>
                                <c:if test="${groupLedgerList != null}">
                                    <c:forEach items="${groupLedgerList}" var="group">
                                        <c:if test="${level.groupID == group.groupCode}">
                                            <c:set var="debitAmount" value="${debitAmount + group.debitAmt}"/> 
                                            <c:set var="creditAmount" value="${debitAmount + group.creditAmt}"/> 
                                        <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${group.debitAmt}"/></b></td>
                                        <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${group.creditAmt}"/></b></td>  
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                        </tr>
                        <c:set var="groupID" value="${level.groupID}"/>
                        <c:set var="levelgroupId" value="${level.groupID}"/>
                        <c:set var="levelgroupName" value="${level.groupName}"/>

                        <c:forEach items="${level.childList}" var="level1">
                            <c:set var="groupID" value="${level.groupID}"/>
                            <c:set var="levelgroupId" value="${level1.levelgroupId}"/>
                            <tr height="30">
                                <td  class="text1" >&nbsp;&nbsp;&nbsp;
                                    <b><c:out value="${level1.levelgroupName}"/></b>    
                                </td>
                                <c:if test="${subPrimaryLedgerList != null}">
                                    <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                        <c:if test="${level1.levelgroupId == subList.levelID}">
<!--                                           <input type="hidden" id="subDebitAmt" name="subDebitAmt" value="<c:out value="${subList.debitAmt}"/>">
                                                               <script type="text/javascript">
                                                                 var subDebitAmt =0.00;
                                                                        subDebitAmt = document.getElementById("subDebitAmt").value;
                                                                 document.getElementById("DebitAmt").innerHTML= parseFloat(subDebitAmt).toFixed(2);
                                                                 </script>
                                                             <td  class="text1" align="right"><b><span id="DebitAmt"></span>  </b></td>-->
                                              <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                            <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                            </tr>
                            <c:if test="${ledgerTransactionList != null}">
                                <c:forEach items="${ledgerTransactionList}" var="ledger">
                                    <c:if test="${level1.levelgroupId == ledger.levelID}">
                                        <tr>
                                            <td class="text1"><c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                            <td class="text1" align ="right"><c:out value="${ledger.debitAmmount}"/></td>
                                            <td class="text1" align ="right"><c:out value="${ledger.creditAmmount}"/></td>

                                        </tr>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                            <c:forEach items="${level1.childList}" var="level2">
                                <c:set var="groupID" value="${level1.groupID}"/>
                                <c:set var="levelgroupId" value="${level2.levelgroupId}"/>
                                <tr height="30">
                                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <b><c:out value="${level2.levelgroupName}"/></b>
                                    </td>
                                    <c:if test="${subPrimaryLedgerList != null}">
                                        <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                            <c:if test="${level2.levelgroupId == subList.levelID}">
                                                <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                                <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                </tr>
                                <c:if test="${ledgerTransactionList != null}">
                                    <c:forEach items="${ledgerTransactionList}" var="ledger">
                                        <c:if test="${level2.levelgroupId == ledger.levelID}">
                                            <tr>
                                                <td class="text1"><c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                                <td class="text1" align ="right"><c:out value="${ledger.debitAmmount}"/></td>
                                                <td class="text1" align ="right"><c:out value="${ledger.creditAmmount}"/></td>

                                            </tr>
                                        </c:if>
                                    </c:forEach>
                                </c:if>
                                <c:forEach items="${level2.childList}" var="level3">
                                    <c:set var="groupID" value="${level2.groupID}"/>
                                    <c:set var="levelgroupId" value="${level3.levelgroupId}"/>
                                    <tr height="30">
                                        <td  class="text1" >
                                            <b><c:out value="${level3.levelgroupName}"/></b>
                                        </td>
                                        <c:if test="${subPrimaryLedgerList != null}">
                                            <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                                <c:if test="${level3.levelgroupId == subList.levelID}">
                                                    <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                                    <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                    </tr>
                                    <c:if test="${ledgerTransactionList != null}">
                                        <c:forEach items="${ledgerTransactionList}" var="ledger">
                                            <c:if test="${level3.levelgroupId == ledger.levelID}">
                                                <tr>
                                                    <td class="text1"><c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                                    <td class="text1" align ="right"><c:out value="${ledger.debitAmmount}"/></td>
                                                    <td class="text1" align ="right"><c:out value="${ledger.creditAmmount}"/></td>

                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                    </c:if>
                                    <c:forEach items="${level3.childList}" var="level4">
                                        <c:set var="groupID" value="${level3.groupID}"/>
                                        <c:set var="levelgroupId" value="${level4.levelgroupId}"/>
                                        <tr height="30">
                                            <td  class="text1" >
                                                <b><c:out value="${level4.levelgroupName}"/></b>
                                            </td>
                                            <c:if test="${subPrimaryLedgerList != null}">
                                                <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                                    <c:if test="${level4.levelgroupId == subList.levelID}">
                                                        <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                                        <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                        </tr>
                                        <c:if test="${ledgerTransactionList != null}">
                                            <c:forEach items="${ledgerTransactionList}" var="ledger">
                                                <c:if test="${level4.levelgroupId == ledger.levelID}">
                                                    <tr>
                                                        <td class="text1"><c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                                        <td class="text1" align ="right"><c:out value="${ledger.debitAmmount}"/></td>
                                                        <td class="text1" align ="right"><c:out value="${ledger.creditAmmount}"/></td>

                                                    </tr>
                                                </c:if>
                                            </c:forEach>
                                        </c:if>
                                        <c:forEach items="${level4.childList}" var="level5">
                                            <c:set var="groupID" value="${level4.groupID}"/>
                                            <c:set var="levelgroupId" value="${level5.levelgroupId}"/>
                                            <tr height="30">
                                                <td  class="text1" >
                                                    <c:out value="${level5.levelgroupName}"/></b>
                                                </td>
                                                <c:if test="${subPrimaryLedgerList != null}">
                                                    <c:forEach items="${subPrimaryLedgerList}" var="subList">
                                                        <c:if test="${level5.levelgroupId == subList.levelID}">
                                                            
                                                            <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.debitAmt}"/></b></td>
                                                            <td  class="text1" align="right"><b><fmt:formatNumber pattern="##0.00" value="${subList.creditAmt}"/></b></td>  
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                            </tr>
                                            <c:if test="${ledgerTransactionList != null}">
                                                <c:forEach items="${ledgerTransactionList}" var="ledger">
                                                    <c:if test="${level5.levelgroupId == ledger.levelID}">
                                                        <tr>
                                                            <td class="text1"><c:out value="${ledger.ledgerName}"/>&nbsp; A/C </td>
                                                            <td class="text1" align ="right"><c:out value="${ledger.debitAmmount}"/></td>
                                                            <td class="text1" align ="right"><c:out value="${ledger.creditAmmount}"/></td>

                                                        </tr>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>


                                        </c:forEach>

                                    </c:forEach>

                                </c:forEach>

                            </c:forEach>

                        </c:forEach>

                    </c:forEach>
                </c:if>
                <tr> 
                    <td  class="text1"><b>Total</b></td>
                    <td  class="text1"  align="right"><b><fmt:formatNumber pattern="##0.00" value="${debitAmount}"/></b></td>
                    <td  class="text1"  align="right"><b><fmt:formatNumber pattern="##0.00" value="${creditAmount}"/></b></td>
                </tr> 
            </table>
            <br>
            <br>
            <br>
            

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
