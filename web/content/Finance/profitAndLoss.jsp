<%-- 
    Document   : financeLedger
    Created on : 24 Oct, 2012, 7:51:06 PM
    Author     : ASHOK
--%>

<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="ets.domain.finance.business.FinanceTO" %>
        <%@ page import="java.util.*" %>

        <title> Manage Group</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
                <script type="text/javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });
        </script>
        <script type="text/javascript">
            function submitPage(){
                //alert("am here..")
                document.trialBalance.action='/throttle/profitAndLoss.do';
                document.trialBalance.submit();
            }

            function viewDetails(levelId, fromDate, toDate) {
             window.open('/throttle/viewTBDetails.do?fromDate='+fromDate+'&toDate='+toDate+'&levelId='+levelId, 'PopupPage', 'height = 500, width = 800, scrollbars = yes, resizable = yes');
            }
        </script>
    </head>
    <body>
        <form name="trialBalance" method="post" action="" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>

            <%
                        ArrayList level1 = (ArrayList) request.getAttribute("levelMasterList");
                        Iterator itr1 = level1.iterator();
                        Iterator itr2 = level1.iterator();
                        Iterator itr3 = level1.iterator();
                        Iterator itr4 = level1.iterator();
                        Iterator itr5 = level1.iterator();
                        Iterator itr6 = level1.iterator();
                        FinanceTO lLevel1 = null;
                        FinanceTO lLevel2 = null;
                        FinanceTO lLevel3 = null;
                        FinanceTO lLevel4 = null;
                        FinanceTO lLevel5 = null;
                        FinanceTO lLevel6 = null;
                        FinanceTO levelMaster = null;
                        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                        Date date = new Date();
                        String fDate = "";
                        String tDate = "";
                        if(request.getAttribute("fromDate") != null) {
                            fDate = (String) request.getAttribute("fromDate");
                        }else{
                            fDate = "01-04-2013";
                        }
                        if(request.getAttribute("toDate") != null) {
                            tDate = (String)request.getAttribute("toDate");
                        }else{
                            tDate = dateFormat.format(date);
                        }

//                        float debit = 0.0f;
//                        float credit = 0.0f;
                        float debit = 0.0f;
                        float credit = 0.0f;
                        //System.out.println(dateFormat.format(date));
%>
            <center>
                <h2>LAXMAN LOGISTICS PRIVATE LIMITED</h2>
                <h2>P & L</h2>

            
                <table>
                    <tr>
                        <td>From:</td>
                        <td><input type="text" class='datepicker' value="<%=fDate %>" name="fromDate" /></td>
                        <td>To:</td>
                        <td><input type="text" class='datepicker' value="<%=tDate %>" name="toDate" /></td>
                    </tr>
                </table>
                 
                    <input type="button" id="saveButton" class="button" value="fetch" onclick="submitPage();" />
                <br>
                </center>
            <table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">


                <tr height="30">
                    <td  align="left" width="60%" class="contenthead" scope="col"><b>Income</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>Debits</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>Credits</b></td>
                </tr>
                <%
                            System.out.println("22222");
                            int cntr = 0;
                            while (itr1.hasNext()) {
                                cntr++ ;
                                System.out.println("333");
                                lLevel1 = new FinanceTO();
                                lLevel1 = (FinanceTO) itr1.next();
                                System.out.println("lLevel1: " + lLevel1.getGroupName());

                                if("Income".equalsIgnoreCase(lLevel1.getGroupName())
                                        ){
                %>
                
                <%
                                                levelMaster = new FinanceTO();
                                                levelMaster.setGroupID(lLevel1.getGroupID());
                                                levelMaster.setLevelgroupId(lLevel1.getGroupID());
                                                levelMaster.setLevelgroupName(lLevel1.getGroupName());


                                                itr2 = lLevel1.getChildList().iterator();
                                                while (itr2.hasNext()) {
                                                    lLevel2 = new FinanceTO();
                                                    lLevel2 = (FinanceTO) itr2.next();
                                                    System.out.println("  lLevel2: " + lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName());

                                                    levelMaster = new FinanceTO();
                                                    levelMaster.setGroupID(lLevel1.getGroupID());
                                                    levelMaster.setLevelgroupId(lLevel2.getLevelgroupId());
                                                    levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName());
                                                    if(lLevel2.getDebit() != null){
                                                        debit += Float.parseFloat(lLevel2.getDebit());
                                                    }else{
                                                                debit  += 0;
                                                            }
                                                    if(lLevel2.getCredit() != null){
                                                        credit += Float.parseFloat(lLevel2.getCredit());
                                                    }else{
                                                                credit  += 0;
                                                            }
                                                    //out.println("debit :"+debit);
                                                    //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;
                        <a href="" onclick="viewDetails('<%=lLevel2.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel2.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1" align="right">&nbsp;&nbsp;&nbsp;<%=lLevel2.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;<%=lLevel2.getCredit()%></td>
                </tr>
                <%

                                                    itr3 = lLevel2.getChildList().iterator();
                                                    while (itr3.hasNext()) {
                                                        lLevel3 = new FinanceTO();
                                                        lLevel3 = (FinanceTO) itr3.next();
                                                        System.out.println("   lLevel3: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName());
                                                        levelMaster = new FinanceTO();
                                                        levelMaster.setGroupID(lLevel1.getGroupID());
                                                        levelMaster.setLevelgroupId(lLevel3.getLevelgroupId());
                                                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName());
                                                        System.out.println("l;l;l;l;l;l;"+lLevel3.getDebit());
                                                        System.out.println("l;l;l;l;l;l;"+lLevel3.getCredit());
                                                        if((!"".equals(lLevel3.getDebit()) && !"null".equals(lLevel3.getDebit())) && lLevel3.getDebit() != null){
                                                        debit += Float.parseFloat(lLevel3.getDebit());
                                                        }else{
                                                                debit  += 0;
                                                            }

                                                        if((!"".equals(lLevel3.getCredit()) && !"null".equals(lLevel3.getCredit()))  && lLevel3.getCredit() != null){
                                                        credit += Float.parseFloat(lLevel3.getCredit());

                                                        }else{
                                                                credit  += 0;
                                                            }

                                                        //out.println("debit :"+debit);
                                                        //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="" onclick="viewDetails('<%=lLevel3.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel3.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel3.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel3.getCredit()%></td>
                </tr>
                <%
                                                        itr4 = lLevel3.getChildList().iterator();
                                                        while (itr4.hasNext()) {
                                                            lLevel4 = new FinanceTO();
                                                            lLevel4 = (FinanceTO) itr4.next();
                                                            System.out.println("    lLevel4: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName());
                                                            levelMaster = new FinanceTO();
                                                            levelMaster.setGroupID(lLevel1.getGroupID());
                                                            levelMaster.setLevelgroupId(lLevel4.getLevelgroupId());
                                                            levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName());
//                                                            debit += Float.parseFloat(lLevel4.getDebit());
                                                            if(lLevel4.getDebit() != null){
                                                                debit  += Float.parseFloat(lLevel4.getDebit());
                                                            }else{
                                                                debit  += 0;
                                                            }
                                                            if(lLevel4.getCredit() != null){
                                                                credit += Float.parseFloat(lLevel4.getCredit());
                                                            }else{
                                                                credit  += 0;
                                                            }
                                                            //out.println("debit :"+debit);
                                                            //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="" onclick="viewDetails('<%=lLevel4.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel4.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel4.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel4.getCredit()%></td>
                </tr>
                <%
                                                            itr5 = lLevel4.getChildList().iterator();
                                                            while (itr5.hasNext()) {
                                                                lLevel5 = new FinanceTO();
                                                                lLevel5 = (FinanceTO) itr5.next();
                                                                System.out.println("     lLevel5: " + lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                                                                levelMaster = new FinanceTO();
                                                                levelMaster.setGroupID(lLevel1.getGroupID());
                                                                levelMaster.setLevelgroupId(lLevel5.getLevelgroupId());
                                                                levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                                                                debit += Float.parseFloat(lLevel5.getDebit());
                                                                credit += Float.parseFloat(lLevel5.getCredit());
                                                                //out.println("debit :"+debit);
                                                                //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="" onclick="viewDetails('<%=lLevel5.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel5.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel5.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel5.getCredit()%></td>
                </tr>
                <%
                                                                itr6 = lLevel5.getChildList().iterator();
                                                                while (itr6.hasNext()) {
                                                                    lLevel6 = new FinanceTO();
                                                                    lLevel6 = (FinanceTO) itr6.next();
                                                                    System.out.println("     lLevel6: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName() + "-" + lLevel5.getLevelgroupName() + "-" + lLevel6.getLevelgroupName());
                                                                    levelMaster = new FinanceTO();
                                                                    levelMaster.setGroupID(lLevel1.getGroupID());
                                                                    levelMaster.setLevelgroupId(lLevel6.getLevelgroupId());
                                                                    levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName() + "->" + lLevel6.getLevelgroupName());
                                                                    debit += Float.parseFloat(lLevel6.getDebit());
                                                                    credit += Float.parseFloat(lLevel6.getCredit());
                                                                    //out.println("debit :"+debit);
                                                                    //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="" onclick="viewDetails('<%=lLevel6.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel6.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel6.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel6.getCredit()%></td>
                </tr>
                <%
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            }

                            if(cntr > 0) {
                %>
                <tr>
                    <td  class="text1">Total</td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=debit%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=credit%></td>
                </tr>
                <%
                }
                %>
            </table>

            <br>

            <table align="center" width="900" border="0" cellspacing="0" cellpadding="0" class="border">


                <tr height="30">
                    <td  align="left" width="60%" class="contenthead" scope="col"><b>Expenses</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>Debits</b></td>
                    <td  align="right" width="20%" class="contenthead" scope="col"><b>Credits</b></td>
                </tr>
                <%

                            level1 = (ArrayList) request.getAttribute("levelMasterList");
                        itr1 = level1.iterator();
                        itr2 = level1.iterator();
                        itr3 = level1.iterator();
                        itr4 = level1.iterator();
                        itr5 = level1.iterator();
                        itr6 = level1.iterator();
                        lLevel1 = null;
                        lLevel2 = null;
                        lLevel3 = null;
                        lLevel4 = null;
                        lLevel5 = null;
                        lLevel6 = null;
                        levelMaster = null;
                            System.out.println("22222");
                            cntr = 0;
                            credit = 0;
                            debit = 0;
                            while (itr1.hasNext()) {
                                cntr++ ;
                                System.out.println("333");
                                lLevel1 = new FinanceTO();
                                lLevel1 = (FinanceTO) itr1.next();
                                System.out.println("lLevel1: " + lLevel1.getGroupName());

                                if("Expenses".equalsIgnoreCase(lLevel1.getGroupName())
                                        ){
                %>
                
                <%
                                                levelMaster = new FinanceTO();
                                                levelMaster.setGroupID(lLevel1.getGroupID());
                                                levelMaster.setLevelgroupId(lLevel1.getGroupID());
                                                levelMaster.setLevelgroupName(lLevel1.getGroupName());


                                                itr2 = lLevel1.getChildList().iterator();
                                                while (itr2.hasNext()) {
                                                    lLevel2 = new FinanceTO();
                                                    lLevel2 = (FinanceTO) itr2.next();
                                                    System.out.println("  lLevel2: " + lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName());

                                                    levelMaster = new FinanceTO();
                                                    levelMaster.setGroupID(lLevel1.getGroupID());
                                                    levelMaster.setLevelgroupId(lLevel2.getLevelgroupId());
                                                    levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName());
                                                    if(lLevel2.getDebit() != null){
                                                        debit += Float.parseFloat(lLevel2.getDebit());
                                                    }else{
                                                                debit  += 0;
                                                            }
                                                    if(lLevel2.getCredit() != null){
                                                        credit += Float.parseFloat(lLevel2.getCredit());
                                                    }else{
                                                                credit  += 0;
                                                            }
                                                    //out.println("debit :"+debit);
                                                    //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;
                        <a href="" onclick="viewDetails('<%=lLevel2.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel2.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1" align="right">&nbsp;&nbsp;&nbsp;<%=lLevel2.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;<%=lLevel2.getCredit()%></td>
                </tr>
                <%

                                                    itr3 = lLevel2.getChildList().iterator();
                                                    while (itr3.hasNext()) {
                                                        lLevel3 = new FinanceTO();
                                                        lLevel3 = (FinanceTO) itr3.next();
                                                        System.out.println("   lLevel3: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName());
                                                        levelMaster = new FinanceTO();
                                                        levelMaster.setGroupID(lLevel1.getGroupID());
                                                        levelMaster.setLevelgroupId(lLevel3.getLevelgroupId());
                                                        levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName());
                                                        System.out.println("l;l;l;l;l;l;"+lLevel3.getDebit());
                                                        System.out.println("l;l;l;l;l;l;"+lLevel3.getCredit());
                                                        if((!"".equals(lLevel3.getDebit()) && !"null".equals(lLevel3.getDebit())) && lLevel3.getDebit() != null){
                                                        debit += Float.parseFloat(lLevel3.getDebit());
                                                        }else{
                                                                debit  += 0;
                                                            }

                                                        if((!"".equals(lLevel3.getCredit()) && !"null".equals(lLevel3.getCredit()))  && lLevel3.getCredit() != null){
                                                        credit += Float.parseFloat(lLevel3.getCredit());

                                                        }else{
                                                                credit  += 0;
                                                            }

                                                        //out.println("debit :"+debit);
                                                        //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <a href="" onclick="viewDetails('<%=lLevel3.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel3.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel3.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel3.getCredit()%></td>
                </tr>
                <%
                                                        itr4 = lLevel3.getChildList().iterator();
                                                        while (itr4.hasNext()) {
                                                            lLevel4 = new FinanceTO();
                                                            lLevel4 = (FinanceTO) itr4.next();
                                                            System.out.println("    lLevel4: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName());
                                                            levelMaster = new FinanceTO();
                                                            levelMaster.setGroupID(lLevel1.getGroupID());
                                                            levelMaster.setLevelgroupId(lLevel4.getLevelgroupId());
                                                            levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName());
//                                                            debit += Float.parseFloat(lLevel4.getDebit());
                                                            if(lLevel4.getDebit() != null){
                                                                debit  += Float.parseFloat(lLevel4.getDebit());
                                                            }else{
                                                                debit  += 0;
                                                            }
                                                            if(lLevel4.getCredit() != null){
                                                                credit += Float.parseFloat(lLevel4.getCredit());
                                                            }else{
                                                                credit  += 0;
                                                            }
                                                            //out.println("debit :"+debit);
                                                            //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="" onclick="viewDetails('<%=lLevel4.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel4.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel4.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel4.getCredit()%></td>
                </tr>
                <%
                                                            itr5 = lLevel4.getChildList().iterator();
                                                            while (itr5.hasNext()) {
                                                                lLevel5 = new FinanceTO();
                                                                lLevel5 = (FinanceTO) itr5.next();
                                                                System.out.println("     lLevel5: " + lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                                                                levelMaster = new FinanceTO();
                                                                levelMaster.setGroupID(lLevel1.getGroupID());
                                                                levelMaster.setLevelgroupId(lLevel5.getLevelgroupId());
                                                                levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName());
                                                                debit += Float.parseFloat(lLevel5.getDebit());
                                                                credit += Float.parseFloat(lLevel5.getCredit());
                                                                //out.println("debit :"+debit);
                                                                //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="" onclick="viewDetails('<%=lLevel5.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel5.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel5.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel5.getCredit()%></td>
                </tr>
                <%
                                                                itr6 = lLevel5.getChildList().iterator();
                                                                while (itr6.hasNext()) {
                                                                    lLevel6 = new FinanceTO();
                                                                    lLevel6 = (FinanceTO) itr6.next();
                                                                    System.out.println("     lLevel6: " + lLevel1.getGroupName() + "-" + lLevel2.getLevelgroupName() + "-" + lLevel3.getLevelgroupName() + "-" + lLevel4.getLevelgroupName() + "-" + lLevel5.getLevelgroupName() + "-" + lLevel6.getLevelgroupName());
                                                                    levelMaster = new FinanceTO();
                                                                    levelMaster.setGroupID(lLevel1.getGroupID());
                                                                    levelMaster.setLevelgroupId(lLevel6.getLevelgroupId());
                                                                    levelMaster.setLevelgroupName(lLevel1.getGroupName() + "->" + lLevel2.getLevelgroupName() + "->" + lLevel3.getLevelgroupName() + "->" + lLevel4.getLevelgroupName() + "->" + lLevel5.getLevelgroupName() + "->" + lLevel6.getLevelgroupName());
                                                                    debit += Float.parseFloat(lLevel6.getDebit());
                                                                    credit += Float.parseFloat(lLevel6.getCredit());
                                                                    //out.println("debit :"+debit);
                                                                    //out.println("credit :"+credit);
                %>
                <tr height="30">
                    <td  class="text1" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="" onclick="viewDetails('<%=lLevel6.getLevelgroupId()%>','<%=fDate%>','<%=tDate%>');"><%=lLevel6.getLevelgroupName()%></a>
                    </td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel6.getDebit()%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=lLevel6.getCredit()%></td>
                </tr>
                <%
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            }

                            if(cntr > 0) {
                %>
                <tr>
                    <td  class="text1">Total</td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=debit%></td>
                    <td  class="text1"  align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%=credit%></td>
                </tr>
                <%
                }
                %>
            </table>


        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
