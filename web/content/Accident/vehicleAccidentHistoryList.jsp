<%--
    Document   : MonthWiseEmptyRunSummary
    Created on : Jun 12, 2014, 01:31:16 PM
    Author     : Arul
--%>

<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">


<html>
    <head>
        <%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" href="/throttle/css/page.css"  type="text/css" />
       <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js" type="text/javascript"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
//            function submitPage(value) {
//               if(value == 'ExportExcel'){
//                   alert(value);
//                    document.orderExpenseRevenue.action = '/throttle/handleOrderExpenseRevenueSearch.do?param=ExportExcel';
//                    document.orderExpenseRevenue.submit();
//                }else{
//                    document.orderExpenseRevenue.action = '/throttle/handleOrderExpenseRevenueSearch.do?param=Search';
//                    document.orderExpenseRevenue.submit();
//
//            }
//            }


            function viewVehicleAccidentlist(accident_Id,insId) {
               

                   document.vehicleAccidentHistorylist.action = '/throttle/vehicleAccidentHistory.do?accidentId='+accident_Id+'&insId='+insId;
                    document.vehicleAccidentHistorylist.submit();
            }
        </script>
        
    </head>
    <body>
<!--    <body onload="setValue();">-->
        <form name="vehicleAccidentHistorylist" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>



            <c:if test = "${vehicleAccidentHistorylist != null}" >
                <table align="center" border="0" id="table" class="sortable" style="width:1200px;">
                   <thead>
                      <tr height="40">
                          
                            <th><h3><spring:message code="accidents.label.SNo" text="default text"/></h3></th>
                            <th><h3><spring:message code="accidents.label.VehicleNumber" text="default text"/></h3></th>
                            <th><h3><spring:message code="accidents.label.AccidentDate" text="default text"/></h3></th>
                            <th><h3><spring:message code="accidents.label.VehicleStatus" text="default text"/></h3></th>
                        </tr>
                    </thead>

                   
                           
                    <tbody>
                        <%int sno = 1;%>
                        

                            <c:forEach items="${vehicleAccidentHistorylist}" var="history">
                            <tr>
                                <td><%=sno%></td>
                                <td><c:out value="${history.acc_VehicleNo}"/></td>
                                <td><c:out value="${history.createdOn}"/></td>
                                <td align="center"> <a href="" onclick="viewVehicleAccidentlist('<c:out value="${history.accident_Id}"/>','<c:out value="${history.insId}"/>');">View</a></td>
                            </tr>
                            <%sno++;%>
                            </c:forEach>

                    </tbody>
                  
                </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

    </body>
</html>