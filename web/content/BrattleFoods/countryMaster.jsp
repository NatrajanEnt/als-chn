<%-- 
    Document   : countryMaster
    Created on : 16 Dec, 2015, 3:51:47 PM
    Author     : Gulshan kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<!--FOR google City Starts-->

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<script type="text/javascript">

    function submitPage()
    {
        var errStr = "";
        var nameCheckStatus = $("#countryNameStatus").text();
        if (document.getElementById("countryCode").value == "") {
            errStr = "Please enter Country Code.\n";
            alert(errStr);
            document.getElementById("countryCode").focus();
        }
        else if (document.getElementById("countryName").value == "") {
            errStr = "Please enter Country Name.\n";
            alert(errStr);
            document.getElementById("countryName").focus();
        }
        else if (nameCheckStatus != "") {
            errStr = "Country Name Already Exists.\n";
            alert(errStr);
            document.getElementById("countryName").focus();
        }
        else if (document.getElementById("currencyCode").value == "")
        {
            errStr = "Please enter Currency Code.\n";
            alert(errStr);
            document.getElementById("currencyCode").focus();
        }
        if (errStr == "") {
            document.symbolMaster.action = " /throttle/saveCountryMaster.do";
            document.symbolMaster.method = "post";
            document.symbolMaster.submit();
        }
    }

    function setValues(sno, countryId,countryCode, countryName, currencyCode, status) {
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("countryId").value = countryId;
        document.getElementById("countryName").value = countryName;
        document.getElementById("countryCode").value = countryCode;
        document.getElementById("currencyCode").value = currencyCode;
        document.getElementById("status").value = status;
    }

    var httpRequest;
    function checkCountryName() {

        var countryName = document.getElementById('countryName').value;

        var url = '/throttle/checkCountryNameList.do?countryName=' + countryName;
        if (window.ActiveXObject) {
            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest) {
            httpRequest = new XMLHttpRequest();
        }
        httpRequest.open("GET", url, true);
        httpRequest.onreadystatechange = function () {
            processRequest();
        };
        httpRequest.send(null);

    }


    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                if (val != "" && val != 'null') {
                    $("#nameStatus").show();
                    $("#countryNameStatus").text('Please Check Country Name: ' + val + ' is Already Exists');
                } else {
                    $("#nameStatus").hide();
                    $("#countryNameStatus").text('');
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    // new

    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#currencyCode').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getcurrencySymbolList.do",
                    dataType: "json",
                    data: {
                        currencyCode: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                       var items = data;
                        if (items == '') {
                            $('#currencyid').val('');
                            $('#currencyCode').val('');
                        }
                        response(items);
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#currencyid').val(tmp[0]);
                $('#currencyCode').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });
</script>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body onload="document.symbolMaster.countryNameStatus.focus();">


        <form name="symbolMaster"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table  border="1" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                <input type="hidden" name="countryId" id="countryId" value=""  />
                <tr>
                <table  border="0" class="border" align="center" width="70%" cellpadding="0" cellspacing="0" id="bg">
                    <tr>
                        <td class="contenthead" colspan="8" >Country Master</td>
                    </tr>
                    <tr>
                        <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="countryNameStatus" style="color: red"></label></td>
                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Country Name</td>
                        <td class="text1"><input type="text" name="countryName" id="countryName" class="textbox" maxlength="50" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCountryName();" autocomplete="off"/></td>
                        
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Country Code</td>
                        <td class="text1"><input type="text" name="countryCode" id="countryCode" class="textbox" maxlength="50" onkeypress="return onKeyPressBlockNumbers(event);" autocomplete="off"/></td>

                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Currency Symbol</td>
                        <td class="text1"><input type="hidden" name="currencyid" id="currencyid" <c:out value="${currencyid}"/> class="textbox" />
                            <input type="text" name="currencyCode" id="currencyCode" class="textbox" maxlength="50" <c:out value="${currencyCode}"/> /></td>
                        
                        <td class="text1">&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                        <td class="text1">
                            <select  align="center" class="textbox" name="status" id="status" >
                                <option value='Y'>Active</option>
                                <option value='N' id="inActive" style="display: none">In-Active</option>
                            </select>
                        </td>
                    </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" name="Submit" onClick="submitPage()">
                        </center>
                    </td>
                </tr>
            </table>
            <br>
            <br>

            <h2 align="center">List Of The Currency</h2>
            <table width="815" align="center" border="0" id="table" class="sortable">
                <thead>
                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Country Code</h3></th>
                        <th><h3>Country Name</h3></th>
                        <th><h3>Currency Symbol</h3></th>
                        <th><h3>Status</h3></th>
                        <th><h3>Select</h3></th>
                    </tr>
                </thead>
                <tbody>

                    <% int sno = 0;%>
                    <c:if test = "${Countrylist != null}">
                        <c:forEach items="${Countrylist}" var="country">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <%= sno%> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${country.countryCode}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${country.countryName}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${country.currencyCode}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${country.status}" /></td>
                                <td class="<%=className%>"> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${country.countryId}" />','<c:out value="${country.countryCode}" />', '<c:out value="${country.countryName}" />', '<c:out value="${country.currencyCode}" />', '<c:out value="${country.status}" />');"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>

            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body> 
</html> 
