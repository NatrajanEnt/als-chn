<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
  
    <script language="javascript">
        $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#custName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerName.do",
                        dataType: "json",
                        data: {
                            custName: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                $('#customerId').val('');
                                $('#custName').val('');
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#customerId').val(tmp[0]);
                    $('#custName').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };

        });
        $(document).ready(function() {
            $('#customerCode').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerCode.do",
                        dataType: "json",
                        data: {
                            customerCode: request.term
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            if(items == ''){
                                $('#customerId').val('');
                                $('#customerCode').val('');
                            }
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#customerId').val(tmp[0]);
                    $('#customerCode').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                .data("item.autocomplete", item)
                .append("<a>" + itemVal + "</a>")
                .appendTo(ul);
            };
        });


        //    function checkValue(value,id){
        //                if(value == '' && id=='custName'){
        //                   $('#customerCode').attr('readonly', true);
        //                   document.getElementById('customerId').value = '';
        //                }
        //                if(value == '' && id=='customerCode'){
        //                   $('#custName').attr('readonly', true);
        //                   document.getElementById('customerCode').value = '';
        //                }
        //            }
        //

       function submitPage(value) {
        var param="trans";
            if (value == "add") {
                document.manufacturer.action = '/throttle/handleViewAddCustomer.do?param='+param;
                document.manufacturer.submit();
            } else if (value == 'search') {
                document.manufacturer.action = '/throttle/handleViewCustomer.do';
                document.manufacturer.submit();
            }
        }
       
    </script>
    
    
     <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	 

<!--	  <span style="float: right">
		<a href="?paramName=en">English</a>
		|
		<a href="?paramName=ar">???????</a>
	  </span>-->
     <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <spring:message code="sales.label.QuotationView"  text="default text"/> </h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="default text"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="default text"/></a></li>
          <li><a href="general-forms.html"><spring:message code="sales.label.Sales/Ops"  text="default text"/></a></li>
          <li class="active"><spring:message code="sales.label.QuotationView"  text="default text"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
    <body>
        <form name="manufacturer" method="post" >
          <%@ include file="/content/common/message.jsp" %>
                 <table class="table table-info mb30 table-hover" id="table">
<!--                <table width="100%" align="center" border="0" id="table" class="sortable">-->
                    <thead>
                        <tr height="40">
                            <th><spring:message code="sales.label.Sno"  text="default text"/></th>
                            <th><spring:message code="sales.label.QuotationNo"  text="default text"/></th>
                            <th><spring:message code="sales.label.QuotationDate"  text="default text"/></th>
                            <th><spring:message code="sales.label.BusinessType"  text="default text"/></th>
                            <th><spring:message code="sales.label.QuotationValue(SAR)"  text="default text"/></th>
                            <th><spring:message code="sales.label.Status"  text="default text"/></th>
                            <th><spring:message code="sales.label.CustomerName"  text="default text"/></th>
                            <th><spring:message code="sales.label.SalesManager"  text="default text"/></th>
                            <th><spring:message code="sales.label.ValidUpto"  text="default text"/></th>
                            <th><spring:message code="sales.label.View"  text="default text"/></th>
                            <th><spring:message code="sales.label.Print"  text="default text"/></th>
                            
                            
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0, sno = 1;%>
                       
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <c:if test="${quotationList!=null}">
                                <c:forEach items="${quotationList}" var="list">
                                <tr height="30">
                                <td  ><%=sno%></td>
                                <td  ><c:out value="${list.quotationNo}"/> </td>
                                <td  ><c:out value="${list.quotationDate}"/> </td>
                                <td  ><c:out value="${list.quotationType}"/> </td>
                                <td  ><c:out value="${list.totalAmount}"/> </td>
                                <td  ><c:out value="${list.statusName}"/> </td>
                                <td  ><c:out value="${list.customerName}"/> </td>
                                <td  ><c:out value="${list.empName}"/> </td>
                                <td  ><c:out value="${list.validity}"/> </td>
                                <td  >
                                 <a href="handleViewEditQuotation.do?quotationId=<c:out value="${list.quotationId}"/>"><spring:message code="sales.label.View"  text="default text"/></a>
                                </td>
                                <td>
                                    <a href="handlePrintQuotation.do?quotationId=<c:out value="${list.quotationId}"/>"><spring:message code="sales.label.Print"  text="default text"/></a>
                                </td>
                             
                               
                                

                            </tr>
                            <%
                                        index++;
                                        sno++;
                            %>

                            </c:forEach>
                            </c:if>
                            

                    </tbody>
                </table>
          
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.label.EntriesPerPage"  text="default text"/>
</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="operations.label.of"  text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>