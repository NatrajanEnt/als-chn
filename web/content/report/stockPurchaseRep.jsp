<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<head>
    <!--        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
    <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
    <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
    <!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
    <script src="/throttle/js/jquery.ui.core.js"></script>

</head>
<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            format: "dd-mm-yyyy",
            autoclose: true,
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });
    $(function () {
        //	alert("cv");
        $(".datepicker").datepicker({
            format: "dd-mm-yyyy",
            autoclose: true
        });
    });</script>

<script language="javascript">
    function show_src() {
        document.getElementById('exp_table').style.display = 'none';
    }
    function show_exp() {
        document.getElementById('exp_table').style.display = 'block';
    }
    function show_close() {
        document.getElementById('exp_table').style.display = 'none';
    }


    var httpReq;
    var temp = "";
    function ajaxData()
    {

        var url = "/throttle/getModels1.do?mfrId=" + document.purchaseReport.mfrId.value;
        if (window.ActiveXObject)
        {
            httpReq = new ActiveXObject("Microsoft.XMLHTTP");
        } else if (window.XMLHttpRequest)
        {
            httpReq = new XMLHttpRequest();
        }
        httpReq.open("GET", url, true);
        httpReq.onreadystatechange = function () {
            processAjax();
        };
        httpReq.send(null);
    }

    function processAjax()
    {
        if (httpReq.readyState == 4)
        {
            if (httpReq.status == 200)
            {
                temp = httpReq.responseText.valueOf();
                setOptions(temp, document.purchaseReport.modelId);
                if ('<%= request.getAttribute("modelId")%>' != 'null') {
                    document.purchaseReport.modelId.value = <%= request.getAttribute("modelId")%>;
                }
            } else
            {
                alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
            }
        }
    }



    function submitPage(val) {
        var Item = document.getElementById("Item").value;
        var chek = validation();
        if (chek == 'true') {
            document.purchaseReport.action = '/throttle/purchaseReport.do?Search=' + val + "&item=" + Item;
            document.purchaseReport.submit();
        }
    }
    function validation() {
        if (document.purchaseReport.companyId.value == 0) {
            alert("Please Select Data for Company Name");
            document.purchaseReport.companyId.focus();
            return 'false';
        } else if (textValidation(document.purchaseReport.fromDate, 'From Date')) {
            return 'false';
        } else if (textValidation(document.purchaseReport.toDate, 'TO Date')) {
            return'false';
        }
        return 'true';
    }

    function setValues() {
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.purchaseReport.fromDate.value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.purchaseReport.toDate.value = '<%=request.getAttribute("toDate")%>';
        }
        var poId = '<%=request.getAttribute("poId")%>';
        if ('<%=request.getAttribute("companyId")%>' != 'null') {
            document.purchaseReport.companyId.value = '<%=request.getAttribute("companyId")%>';
            document.purchaseReport.vendorId.value = '<%=request.getAttribute("vendorId")%>';
        }
        if ('<%=request.getAttribute("categoryId")%>' != 'null') {
            document.purchaseReport.categoryId.value = '<%=request.getAttribute("categoryId")%>';
        }
        if ('<%=request.getAttribute("mfrId")%>' != 'null') {
            document.purchaseReport.mfrId.value = '<%=request.getAttribute("mfrId")%>';
        }
        if ('<%=request.getAttribute("modelId")%>' != 'null') {
            ajaxData();
            document.purchaseReport.modelId.value = '<%=request.getAttribute("modelId")%>';
        }
        if ('<%=request.getAttribute("mfrCode")%>' != 'null') {
            document.purchaseReport.mfrCode.value = '<%=request.getAttribute("mfrCode")%>';
        }
        if ('<%=request.getAttribute("paplCode")%>' != 'null') {
            document.purchaseReport.paplCode.value = '<%=request.getAttribute("paplCode")%>';
        }
        if ('<%=request.getAttribute("itemName")%>' != 'null') {
            document.purchaseReport.itemName.value = '<%=request.getAttribute("itemName")%>';
        }
        if ('<%=request.getAttribute("purType")%>' != 'null') {
            document.purchaseReport.purType.value = '<%=request.getAttribute("purType")%>';
        }
        if (poId != 'null') {
            document.purchaseReport.poId.value = '<%=request.getAttribute("poId")%>';
        }

    }
    function getItemNames() {
        var oTextbox = new AutoSuggestControl(document.getElementById("itemName"), new ListSuggestions("itemName", "/throttle/handleItemSuggestions.do?"));
    }
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="fmsstores.label. Purchase Report" text=" Purchase Report"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="fmsstores.label.FMSStores" text="default text"/></a></li>
            <li class="active"><spring:message code="fmsstores.label. Purchase Report" text=" Purchase Report"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="setValues();">
                <form name="purchaseReport" method="post">
                    <%--<%@ include file="/content/common/path.jsp" %>--%>           
                    <%@ include file="/content/common/message.jsp" %>
                    <table class="table table-info mb30 table-hover">
                        <thead><tr><th colspan="4">Material Purchase Report</th></tr></thead>

                        <!--<table width="850" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                            </h2></td>
                            <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                            </tr>
                            <tr id="exp_table" >
                            <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                                <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                    <li style="background:#76b3f1">Material Purchase Report</li>
                            </ul>-->
                        <div id="first">
                            <table class="table table-info mb30 table-hover" id="bg" >	
                                <tr><input type="hidden" name="Item" id="Item" value="<c:out value="${Item}"/>">
                                <td width="114" height="30">&nbsp;&nbsp;Category</td>
                                <td width="172" height="30"><select name="categoryId"  class="form-control" style="width:240px;height:40px">
                                        <option value="0">-select-</option>
                                        <c:if test = "${Item == 1}" >
                                            <option value="1011">TYRES</option>
                                            <option value="1038">TYRES SPARE ITEMS</option>
                                        </c:if>
                                        <c:if test = "${Item != 1}" >
                                            <c:if test = "${categoryList != null}" >
                                                <c:forEach items="${categoryList}" var="category">
                                                    <option value="<c:out value="${category.categoryId}"/>"><c:out value="${category.categoryName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </c:if>

                                    </select></td>
                                <td height="30">&nbsp;&nbsp;MFR</td>
                                <td height="30"><select name="mfrId" onchange="ajaxData();"  class="form-control" style="width:240px;height:40px">
                                        <option value="0">-select-</option>
                                        <c:if test = "${MfrLists != null}" >
                                            <c:forEach items="${MfrLists}" var="mfr">
                                                <option value="<c:out value="${mfr.mfrId}"/>"><c:out value="${mfr.mfrName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                    </select></td>
                                </tr>
                                <tr>
                                    <td height="30">&nbsp;&nbsp;Model</td>
                                    <td height="30"><select name="modelId"  class="form-control" style="width:240px;height:40px">
                                            <option value="0">-select-</option>
                                            <c:if test = "${ModelLists != null}" >
                                                <c:forEach items="${ModelLists}" var="model">
                                                    <option value="<c:out value="${model.modelId}"/>"><c:out value="${model.modelName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>

                                    <td height="30"><font color="red">*</font>Location</td>
                                    <td height="30"><select   class="form-control" style="width:240px;height:40px" name="companyId" >
                                            <c:set var="companyId" value="${companyId}"/>
                                            <c:out value="${companyId}"/>
                                            <c:if test = "${LocationLists != null}" >
                                                <c:forEach items="${LocationLists}" var="company">
                                                    <c:if test="${company.cmpId==companyId}" >
                                                        <option selected value="<c:out value="${company.cmpId}"/>"><c:out value="${company.name}"/></option>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>

                                        </select></td>
                                </tr>
                                <tr>
                                    <td width="148">&nbsp;&nbsp;Vendor</td>
                                    <td width="148"><select name="vendorId"  class="form-control" style="width:240px;height:40px">
                                            <option value="0">-select-</option>
                                            <c:if test = "${VendorLists != null}" >
                                                <c:forEach items="${VendorLists}" var="vendor">
                                                    <option value="<c:out value="${vendor.vendorId}"/>"><c:out value="${vendor.vendorName}"/></option>
                                                </c:forEach>
                                            </c:if>
                                        </select></td>
                                    <td width="162" height="30">&nbsp;&nbsp;PO/ LPO No</td>
                                    <td width="151" height="30" class="text1"> <input name="poId" type="text"  class="form-control" style="width:240px;height:40px"  size="20" autocomplete="off"></td>
                                </tr>
                                <tr style="display:none">
                                    <td width="114" height="30">&nbsp;&nbsp;Item Name</td>
                                    <td width="172" height="30"><input name="itemName" id="itemName" type="text"   class="form-control" style="width:240px;height:40px" value=""></td>
                                    <td width="111" height="30">&nbsp;&nbsp;Mfr Item Code</td>
                                    <td width="151" height="30"><input name="mfrCode" type="text" class="form-control" style="width:240px;height:40px"value="" size="20"></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;&nbsp;Company Code</td>
                                    <td height="30"><input name="paplCode" type="text"  class="form-control" style="width:240px;height:40px" value="" size="20" autocomplete="off"></td>

                                    <td height="30">Purchase Type</td>
                                    <td height="30"><select name="purType"  class="form-control" style="width:240px;height:40px">
                                            <option value="0">-select-</option>
                                            <option value="N">Direct</option>
                                            <option value="Y">Vendor</option>
                                        </select></td>
                                </tr>
                                <tr>
                                    <td width="80" height="30"><font color="red">*</font>From Date</td>
                                    <td width="182"><input name="fromDate" type="text" class="datepicker form-control" style="width:240px;height:40px" value="" size="20" autocomplete="off">
                                    </td>
                                    <td width="148"><font color="red">*</font>To Date</td>
                                    <td width="172" height="30"><input name="toDate" type="text" class="datepicker form-control" style="width:240px;height:40px" value="" size="20" autocomplete="off">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4" align="center"> <input type="button"  value="Fetch Data" class="btn btn-success" name="Fetch Data" onClick="submitPage(this.value);"/>
                                        <input type="hidden" value="" name="reqfor">
                                        <input type="button" class="btn btn-success" id="Search" name="Search" value="ExcelExport" onclick="submitPage(this.value);"/>
                                    </td></tr>
                            </table>
                        </div></div>
                        </td>
                        </tr>
                    </table>
                    <br>
                    <c:if test = "${purchaseReportList != null}" >
                        <table class="table table-info mb30 table-hover" id="bg" >
                            <thead>
                                <!--<table width="1266" border="0" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">-->
                                <tr class="contenthead">
                                    <th width="71" class="contentsub">Date</th>    
                                    <th width="102" height="30" class="contentsub">Manufacturer</th>
                                    <th width="82" class="contentsub">Model</th>
                                    <th width="104" height="30" class="contentsub">Mfr Item Code </th>
                                    <th width="82" class="contentsub">PAPL Code </th>
                                    <th width="142" height="30" class="contentsub">Item Name</th>
                                    <th width="78" height="30" class="contentsub">Category</th>
                                    <th width="71" height="30" class="contentsub">Vendor</th>
                                    <th width="71" height="30" class="contentsub">Purchase Type</th>                        
                                    <th width="76" height="30" class="contentsub">PO / LPO No </th>
                                    <th width="76" height="30" class="contentsub">GRN No</th>
                            <blockquote>&nbsp;</blockquote>
                            <th width="80" height="30" class="contentsub">Accepted Qty</th>
                            <th width="80" height="30" class="contentsub">Unused Qty</th>
                            <th width="80" height="30" class="contentsub">Unit Price</th>
                            <th width="80" height="30" class="contentsub">GST Amount</th>
                            <th width="80" height="30" class="contentsub">Unit Total</th>
                            <th width="99" height="30" class="contentsub">Purchase Value (INR) </th>
                            <th width="99" height="30" class="contentsub">GRN Status</th>
                            </tr>
                            <c:set var="directPurAmount" value="0" />
                            <c:set var="vendorPurAmount" value="0" />
                            <c:set var="totPurAmount" value="0" />
                            <c:set var="unusedAmount" value="0" />
                            <c:set var="unitTotal" value="0" />
                            <c:set var="unitPrice" value="0" />
                            <c:set var="gstAmount" value="0" />
                            <c:set var="unusedQty" value="0" />
                            <c:set var="accQty" value="0" />
                            <% int index = 0;%> 
                            <c:forEach items="${purchaseReportList}" var="purchase"> 
                                <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                %>	

                                <tr>
                                    <td ><c:out value="${purchase.purDate}" /></td>
                                <td  height="30"><c:out value="${purchase.mfrName}" /></td>
                                <td ><c:out value="${purchase.modelName}" /></td>
                                <td  height="30"><c:out value="${purchase.mfrCode}" /></td>
                                <td ><c:out value="${purchase.paplCode}" /></td>
                                <td  height="30"><c:out value="${purchase.itemName}" /></td>
                                <td  height="30"><c:out value="${purchase.categoryName}" /></td>
                                <td  height="30"><c:out value="${purchase.vendorName}" /> </td>

                                <c:choose>
                                    <c:when test="${purchase.purType=='n' || purchase.purType=='N'}" >
                                        <td  height="30">Direct Purchase</td>
                                        <c:set var="directPurAmount" value="${directPurAmount + purchase.purchaseAmt}" />
                                    </c:when>
                                    <c:otherwise>
                                        <td  height="30">Vendor Purchase</td>
                                        <c:set var="vendorPurAmount" value="${vendorPurAmount + purchase.purchaseAmt}" />
                                    </c:otherwise>
                                </c:choose>


                                <td  height="30"><c:out value="${purchase.poId}" /></td>
                                <td  height="30"><c:out value="${purchase.supplyId}" /></td>
                                <td  height="30"><c:out value="${purchase.aqty}" /></td>    
                                <td  height="30"><c:out value="${purchase.unusedQty}" /></td>    
                                <td  height="30"><c:out value="${purchase.unitPrice}" /></td>    
                                <td  height="30"><c:out value="${purchase.itemTaxAmount}" /></td>    
                                <td  height="30">
                                <c:set var="accQty" value="${accQty + purchase.aqty}" />
                                <c:set var="unusedQty" value="${unusedQty + purchase.unusedQty}" />
                                <c:set var="unitPrice" value="${unitPrice + purchase.unitPrice}" />
                                <c:set var="gstAmount" value="${gstAmount + purchase.itemTaxAmount}" />
                                <c:set var="unitTotal" value="${unitTotal + purchase.unitPrice+purchase.itemTaxAmount}" />
                                <fmt:setLocale value="en_US" /><fmt:formatNumber value="${(purchase.unitPrice+purchase.itemTaxAmount)}" pattern="##.00"/>
                                </td>
                                <td  height="30">
                                <fmt:setLocale value="en_US" /><fmt:formatNumber value="${(purchase.unitPrice+purchase.itemTaxAmount)*purchase.aqty}" pattern="##.00"/>
                                </td>
                                <c:set var="unusedAmount" value="${unusedAmount + (purchase.unusedQty * purchase.unitPrice) }" />
                                <c:set var="totPurAmount" value="${totPurAmount + ((purchase.unitPrice+purchase.itemTaxAmount)*purchase.aqty) }" />
                                <td height="30">
                                <c:choose>
                                    <c:when test="${purchase.purchaseAmt=='0.00'}" >
                                        <font style="color: red">Pending</font>
                                    </c:when>
                                    <c:otherwise>
                                        <font style="color: green">Completed</font>
                                    </c:otherwise>
                                </c:choose> 
                                </td>
                                </tr>  
                                <% index++;%>
                            </c:forEach>
                            <tr >
                                <td width="71" >&nbsp;</td>    
                                <td width="102" height="30" >&nbsp;</td>
                                <td width="82" >&nbsp;</td>
                                <td width="104" height="30" >&nbsp; </td>
                                <td width="82" >&nbsp;</td>
                                <td width="82" >&nbsp;</td>
                                <td width="142" height="30" >&nbsp;</td>
                                <td width="100" height="30" >&nbsp;</td>
                                <td width="99" height="30" >&nbsp;

                                </td>
                                <td width="78" height="30" >&nbsp;</td>
                                <td width="71" height="30" >&nbsp;</td>
                                <td width="71" height="30" >

                            <fmt:setLocale value="en_US" /><fmt:formatNumber value="${accQty}" pattern="##.00"/> 
                            </td>                                               
                            <td width="71" height="30" >
                            <fmt:setLocale value="en_US" /><fmt:formatNumber value="${unusedQty}" pattern="##.00"/> 

                            </td>                                                                   
                            <td width="71" height="30" >
                            <fmt:setLocale value="en_US" /><fmt:formatNumber value="${unitPrice}" pattern="##.00"/> 

                            </td>    
                            <td width="71" height="30" >
                            <fmt:setLocale value="en_US" /><fmt:formatNumber value="${gstAmount}" pattern="##.00"/> 
                            </td>    
                            <td width="71" height="30" >
                            <fmt:setLocale value="en_US" /><fmt:formatNumber value="${unitTotal}" pattern="##.00"/> 
                            </td>    
                            <td width="71" height="30" >
                            <fmt:setLocale value="en_US" /><fmt:formatNumber value="${totPurAmount}" pattern="##.00"/> 
                            </td>    
                            <td width="100" height="30" >&nbsp;</td>
                            </tr>
                            <tr style="display:none">
                                <td width="71" >&nbsp;</td>    
                                <td width="102" height="30" >&nbsp;</td>
                                <td width="82" >&nbsp;</td>
                                <td width="104" height="30" >&nbsp; </td>
                                <td width="82" >&nbsp;</td>
                                <td width="142" height="30" >&nbsp;</td>
                                <td width="100" height="30" ><b>Direct Purchase Amount</b></td>
                                <td width="80" height="30" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber  value="${directPurAmount}" pattern="##.00"/> </b> 
                            </td>  
                            <td width="78" height="30" >&nbsp;</td>
                            <td width="71" height="30" >&nbsp;</td>
                            <td width="71" height="30" >&nbsp;</td>                        
                            <td width="71" height="30" >&nbsp;</td>                                                                      
                            <td width="71" height="30" >&nbsp;</td> 
                            <td width="80" height="30" >&nbsp;</td> 

                            </tr>
                            <tr style="display:none">
                                <td width="71" >&nbsp;</td>    
                                <td width="102" height="30" >&nbsp;</td>
                                <td width="82" >&nbsp;</td>
                                <td width="104" height="30" >&nbsp; </td>
                                <td width="82" >&nbsp;</td>
                                <td width="142" height="30" >&nbsp;</td>
                                <td width="100" height="30" ><b>Unused Stock Value</b></td>
                                <td width="80" height="30" >
                            <fmt:setLocale value="en_US" /><b>INR: <fmt:formatNumber  value="${unusedAmount}" pattern="##.00"/></b>  
                            </td>  
                            <td width="78" height="30" >&nbsp;</td>
                            <td width="71" height="30" >&nbsp;</td>
                            <td width="71" height="30" >&nbsp;</td>                        
                            <td width="71" height="30" >&nbsp;</td>                                                                      
                            <td width="71" height="30" >&nbsp;</td> 
                            <td width="80" height="30" >&nbsp;</td> 

                            </tr>
                        </table>
                    </c:if>
                    <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
