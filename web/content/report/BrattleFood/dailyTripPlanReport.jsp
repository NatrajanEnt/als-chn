<%--
    Document   : BPCLTransactionReport
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value) {

                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
//                } else if (document.getElementById('toDate').value == '') {
//                    alert("Please select to Date");
//                    document.getElementById('toDate').focus();
                }
                else {
                    if (value == "ExportExcel") {
                        document.BPCLTransaction.action = '/throttle/handleDailyTripPlanningDetailsExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else {
                        document.BPCLTransaction.action = '/throttle/handleDailyTripPlanningDetailsExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                }
            }
        </script>



    </head>
    <body>
        <form name="BPCLTransaction" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>

            <table width="55%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr>
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <b>Trip Plan Report</b>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <!--                                    <tr>
                                                                            <td><font color="red">*</font>Driver Name</td>
                                                                            <td height="30">
                                                                                <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                        </tr>-->
                                    <tr>
                                        <td><font color="red">*</font> Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <!--                                        <td><font color="red">*</font>To Date</td>
                                                                                <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>-->
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="button" class="button" name="search" onclick="submitPage(this.name);" value="Search"></td>
                                        <td><input type="button" class="button" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                        <td><center><input type="button" class="button"  value="Print" onClick="print('printContent');" ></center></td>
                                    <td></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
                                    <br>
                                    <br>
            <div id="printContent">
                <c:if test="${tripDetails != null}">
                    <table align="center" border="0" id="table" class="sortable" width="100%" >

                        <thead>
                            <tr height="50">
                                <th align="center"><h3>S.No</h3></th>
                        <th align="center"><h3>Shipper Name</h3></th>
                        <th align="center"><h3>Billing Party </h3></th>
                        <th align="center"><h3>shipping Line</h3></th>
                        <th align="center"><h3>vehicle Type</h3></th>
                        <th align="center"><h3>ISO/DSO</h3></th>
                        <th align="center"><h3>FS/MT REPO</h3></th>
                        <th align="center"><h3>PickUp Location</h3></th>
                        <th align="center"><h3>To Location</h3></th>
                        <th align="center" colspan="2"><h3>Total Booking</h3></th>
                        <th align="center" colspan="2"><h3>Achived</h3></th>
                        <th align="center" colspan="0"><h3>Address</h3></th>
                        <th align="center" colspan="0"><h3>Consignment Order No</h3></th>
                        <th align="center" colspan="0"><h3>Container(s)</h3></th>
                        <th align="center" colspan="0"><h3>Date</h3></th>

                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td align="center" >20'</td>
                            <td align="center" >40'</td>
                            <td align="center" >20'</td>
                            <td align="center" >40'</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                            <c:set var="totalUnplannedTwenty" value="0"/>
                            <c:set var="totalUnplannedFourty" value="0"/>
                            <c:set var="totalPlannedTwenty" value="0"/>
                            <c:set var="totalPlannedFourty" value="0"/>
                            <% int index = 1;%>

                            <c:forEach items="${tripDetails}" var="BPCLTD">
                                <%
                                            String classText = "";
                                            int oddEven = index % 2;
                                            if (oddEven > 0) {
                                                classText = "text2";
                                            } else {
                                                classText = "text1";
                                            }
                                %>

                                <tr>
                                    <td align="center" width="30"class="<%=classText%>"><%=index++%></td>
                                    <td  width="30" align="center" class="<%=classText%>"><c:out value="${BPCLTD.customerName}"/></td>
                                    <td  width="30" align="center" class="<%=classText%>"><c:out value="${BPCLTD.billingParty}"/></td>
                                    <td width="30" align="center" class="<%=classText%>"><c:out value="${BPCLTD.linerName}"/></td>
                                    <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.vehicleTypeName}"/></td>
                                    <td align="center">
                                        <c:if test="${BPCLTD.id == '1' || BPCLTD.id == '2'|| BPCLTD.id == '3' }" >
                                            ISO
                                        </c:if>
                                        <c:if test="${BPCLTD.id == '4'|| BPCLTD.id == '5'}">
                                            DSO
                                        </c:if>
                                    </td>
                                    <td align="center">
                                        <c:if test="${ BPCLTD.id == '3' }" >
                                           MT REPO
                                        </c:if>
                                        <c:if test="${BPCLTD.id == '1' || BPCLTD.id == '4'}">
                                            FS
                                        </c:if>
                                        <c:if test="${BPCLTD.id == '2' || BPCLTD.id == '5'}">
                                            Import
                                        </c:if>
                                    </td>
                                    <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.origin}"/></td>
                                    <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.destination}"/></td>
                                    <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.twentyFT}"/></td>
                                    <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.fortyFT}"/></td>
                                    <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.plannedTwentyFT}"/></td>
                                    <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.plannedFortyFT}"/></td>
                                       <c:set var="totalUnplannedTwenty" value="${totalUnplannedTwenty+ BPCLTD.twentyFT}"/>
                                       <c:set var="totalUnplannedFourty" value="${totalUnplannedFourty+ BPCLTD.fortyFT}"/>
                                       <c:set var="totalPlannedTwenty" value="${totalPlannedTwenty+ BPCLTD.plannedTwentyFT}"/>
                                       <c:set var="totalPlannedFourty" value="${totalPlannedFourty+ BPCLTD.plannedFortyFT}"/>
                                    <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.address}"/></td>
                                        <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.consignmentOrderNo}"/></td>
                                        <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.containerNo}"/></td>
                                        <td width="30" align="center" class="<%=classText%>" ><c:out value="${BPCLTD.consignmentDate}"/></td>
                                </tr>
                            </c:forEach>
                                
                                <tr>
                                    <td>Total</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td><c:out value="${totalUnplannedTwenty}"/></td>
                                    <td><c:out value="${totalUnplannedFourty}"/></td>
                                    <td><c:out value="${totalPlannedTwenty}"/></td>
                                    <td><c:out value="${totalPlannedFourty}"/></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                        </tbody>
                    </table>


                </c:if>
            </div>                <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5"selected="selected" >5</option>
                        <option value="10">10</option>
                        <option value="20" >20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

        <script type="text/javascript">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
    </body>
</html>

