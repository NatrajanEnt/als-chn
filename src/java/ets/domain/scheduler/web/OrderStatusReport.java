/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

/**
 *
 * @author ramprasath
 */
import ets.domain.programmingfree.excelexamples.calculateDistance;
import ets.domain.report.business.DprTO;
import ets.domain.report.business.ReportTO;
//import ets.domain.thread.web.EmailSSL;
import ets.domain.thread.web.SendEmail;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import net.sf.json.JSONException;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.sql.SQLException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Iterator;
import java.util.ArrayList;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Properties;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Properties;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;

import java.util.*;
import java.text.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.*;
import javax.mail.BodyPart;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import org.jsoup.Jsoup;

public class OrderStatusReport extends Thread {

    int timeSlot = 0;
    int StatusFlag = 0;
    String currentDate = null;
    String currentDay = null;
    String congOrderId = null;
    String moveDate = null;
    String compId = null;

    public OrderStatusReport(int flag, int slot, String date, String consignmentOrderId, String day, String movementDate, String compid) throws ParseException, ClassNotFoundException, SQLException, org.json.simple.parser.ParseException {
        timeSlot = slot;
        currentDate = date;
        congOrderId = consignmentOrderId;
        currentDay = day;
        StatusFlag = flag;
        moveDate = movementDate;
        compId = compid;
    }

    public void run() {
        try {
            System.out.println("timeslot==" + timeSlot);
            Connection con = null;
            System.out.println("I am in order status.........");
            String fileName = "jdbc_url.properties";
            InputStream is = getClass().getResourceAsStream("/" + fileName);
            Properties dbProps = new Properties();
            try {
                dbProps.load(is);//this may throw IOException
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println("ex.printStackTrace()");
            }
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                PreparedStatement updateFlag = null;
                PreparedStatement updateOrderStatus = null;
                PreparedStatement checkReportExists = null;
                PreparedStatement insertNewReport = null;
                PreparedStatement updateReport = null;
                ResultSet rs = null;

                String sql = "update  ts_consignment_note cn set time_slot_flag=1 where cn.consignment_order_id=?";
                String sq2 = "update  ts_consignment_note cn set time_slot_flag=2 where cn.consignment_order_id=?";
                String sq3 = "update  ts_consignment_note cn set time_slot_flag=3 where cn.consignment_order_id=?";
                String sq4 = "update  ts_consignment_note cn set time_slot_flag=4 where cn.consignment_order_id=?";
                String sq5 = "update  ts_consignment_note cn set time_slot_flag=5 where cn.consignment_order_id=?";
                String sq6 = "update  ts_consignment_note cn set time_slot_flag=6 where cn.consignment_order_id=?";
                
                String sq7 = "select "
                        + "(select count(*) from ts_consignment_note where comp_id=? and time_slot_flag=1 and date(if(date_format(modified_on,'%d')=00,created_on,modified_on))=date(?)) as wfTime1, "
                        + "(select count(*) from ts_consignment_note where comp_id=? and time_slot_flag=2 and date(if(date_format(modified_on,'%d')=00,created_on,modified_on))=date(?)) as wTime1, "
                        + "(select count(*) from ts_consignment_note where comp_id=? and time_slot_flag=3 and date(if(date_format(modified_on,'%d')=00,created_on,modified_on))=date(?)) as wfTime2, "
                        + "(select count(*) from ts_consignment_note where comp_id=? and time_slot_flag=4 and date(if(date_format(modified_on,'%d')=00,created_on,modified_on))=date(?)) as wTime2, "
                        + "(select count(*) from ts_consignment_note where comp_id=? and time_slot_flag=5 and date(if(date_format(modified_on,'%d')=00,created_on,modified_on))=date(?)) as wfTime3, "
                        + "(select count(*) from ts_consignment_note where comp_id=? and time_slot_flag=6 and date(if(date_format(modified_on,'%d')=00,created_on,modified_on))=date(?)) as wTime3, "
                        + "(select count(*) from ts_consignment_note where comp_id=? and time_slot_flag!=0 and date(if(date_format(modified_on,'%d')=00,created_on,modified_on))=date(?)) as total ";
            
                String sq8 = "insert into ts_order_status_details  (wf_time_slot1,time_slot1,wf_time_slot2,time_slot2,wf_time_slot3,time_slot3,total_movement,Date,day,created_on,comp_id) "
                            + "values (?,?,?,?,?,?,?,?,?,curdate(),?)";

                String sqll = "update  ts_trip_master set timediff_status=1, modified_on = now() where trip_id=? ";
                String sql2 = "update  ts_trip_master set timediff_status=2, modified_on = now() where trip_id=? ";
                String sql3 = "update  ts_trip_master set timediff_status=3, modified_on = now() where trip_id=? ";
                
                String sql4 = "select "
                        + "(select count(*) from ts_trip_master where comp_id=? and timediff_status=1 and date(modified_on)=date(?)) as timeDiff1, "
                        + "(select count(*) from ts_trip_master where comp_id=? and timediff_status=2 and date(modified_on)=date(?)) as timeDiff2, "
                        + "(select count(*) from ts_trip_master where comp_id=? and timediff_status=3 and date(modified_on)=date(?)) as timeDiff3, "
                        + "(select count(*) from ts_trip_master where comp_id=? and timediff_status!=0 and date(modified_on)=date(?)) as total ;";
                
                String sql5 = "update ts_order_status_details set time_diff1=?,time_diff2=?,time_diff3=?,total_trip=? where date(created_on)=date(?) and comp_id=?";

                int update = 0;
                int wfDoc1 = 0;
                int wfDoc2 = 0;
                int wfDoc3 = 0;
                int wDoc1 = 0;
                int wDoc2 = 0;
                int wDoc3 = 0;
                int timeDiff1 = 0;
                int timeDiff2 = 0;
                int timeDiff3 = 0;
                int totalTrip = 0;
                int totalMovement = 0;
                
                if (timeSlot == 1) {
                    updateFlag = con.prepareStatement(sql);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 1 : " + updateFlag);
                } 
                if (timeSlot == 2) {
                    updateFlag = con.prepareStatement(sq2);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 2 : " + updateFlag);
                }
                if (timeSlot == 3) {
                    updateFlag = con.prepareStatement(sq3);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 3 : " + updateFlag);
                } 
                
                if (timeSlot == 4) {
                    updateFlag = con.prepareStatement(sq4);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 4 : " + updateFlag);
                }
                if (timeSlot == 5) {
                    updateFlag = con.prepareStatement(sq5);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 5 : " + updateFlag);
                } 
                if (timeSlot == 6) {
                    updateFlag = con.prepareStatement(sq6);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 6 : " + updateFlag);
                }
                if (timeSlot == 7) {
                    
                    updateOrderStatus = con.prepareStatement(sq7);
                    updateOrderStatus.setString(1, compId);
                    updateOrderStatus.setString(2, currentDate);
                    updateOrderStatus.setString(3, compId);
                    updateOrderStatus.setString(4, currentDate);
                    updateOrderStatus.setString(5, compId);
                    updateOrderStatus.setString(6, currentDate);
                    updateOrderStatus.setString(7, compId);
                    updateOrderStatus.setString(8, currentDate);
                    updateOrderStatus.setString(9, compId);
                    updateOrderStatus.setString(10, currentDate);
                    updateOrderStatus.setString(11, compId);
                    updateOrderStatus.setString(12, currentDate);
                    updateOrderStatus.setString(13, compId);
                    updateOrderStatus.setString(14, currentDate);

                    System.out.println("OrderStatusReport insert part Arun = " + updateOrderStatus.toString());
                    rs = updateOrderStatus.executeQuery();
                    
                    if (rs.next()) {
                        wfDoc1 = rs.getInt("wfTime1");
                        wfDoc2 = rs.getInt("wfTime2");
                        wfDoc3 = rs.getInt("wfTime3");
                        wDoc1 = rs.getInt("wTime1");
                        wDoc2 = rs.getInt("wTime2");
                        wDoc3 = rs.getInt("wTime3");
                        totalMovement = rs.getInt("total");
                        System.out.println("wfDoc1=" + wfDoc1);
                        System.out.println("wfDoc2=" + wfDoc2);
                        System.out.println("wfDoc3=" + wfDoc3);
                        System.out.println("wDoc1=" + wDoc1);
                        System.out.println("wDoc2=" + wDoc2);
                        System.out.println("wDoc3=" + wDoc3);
                        System.out.println("totalMovement=" + totalMovement);
                    }
                    insertNewReport = con.prepareStatement(sq8);
                    insertNewReport.setInt(1, wfDoc1);
                    insertNewReport.setInt(2, wDoc1);
                    insertNewReport.setInt(3, wfDoc2);
                    insertNewReport.setInt(4, wDoc2);
                    insertNewReport.setInt(5, wfDoc3);
                    insertNewReport.setInt(6, wDoc3);
                    insertNewReport.setInt(7, totalMovement);
                    insertNewReport.setString(8, moveDate);
                    insertNewReport.setString(9, currentDay);
                    insertNewReport.setString(10, compId);
                    System.out.println("ts_order_status_details=" + insertNewReport.toString());
                    update = insertNewReport.executeUpdate();
                    System.out.println("insert report=" + update);
                }
                
                
                ////////////////////////// Trip Part ///////////////////////////
                
                
                if (timeSlot == 8) {
                    updateFlag = con.prepareStatement(sqll);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 8 : " + update);
                }
                if (timeSlot == 9) {
                    updateFlag = con.prepareStatement(sql2);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 9 : " + update);
                }
                if (timeSlot == 10) {
                    updateFlag = con.prepareStatement(sql3);
                    updateFlag.setString(1, congOrderId);
                    update = updateFlag.executeUpdate();
                    System.out.println("slot 10 : " + update);
                }
                if (timeSlot == 11) {
                    updateOrderStatus = con.prepareStatement(sql4);
                    updateOrderStatus.setString(1, compId);
                    updateOrderStatus.setString(2, currentDate);
                    updateOrderStatus.setString(3, compId);
                    updateOrderStatus.setString(4, currentDate);
                    updateOrderStatus.setString(5, compId);
                    updateOrderStatus.setString(6, currentDate);
                    updateOrderStatus.setString(7, compId);
                    updateOrderStatus.setString(8, currentDate);
                    System.out.println("updateTripStatus= " + updateOrderStatus.toString());
                    rs = updateOrderStatus.executeQuery();
                    if (rs.next()) {
                        timeDiff1 = rs.getInt("timeDiff1");
                        timeDiff2 = rs.getInt("timeDiff2");
                        timeDiff3 = rs.getInt("timeDiff3");
                        totalTrip = rs.getInt("total");
                    }
                    
                    System.out.println("total=" + totalTrip);
                    updateReport = con.prepareStatement(sql5);                    
                    updateReport.setInt(1, timeDiff1);                    
                    updateReport.setInt(2, timeDiff2);
                    updateReport.setInt(3, timeDiff3);
                    updateReport.setInt(4, totalTrip);
                    updateReport.setString(5, currentDate);
                    updateReport.setString(6, compId);
                    update = updateReport.executeUpdate();
                    System.out.println("timeDiff1=" + timeDiff1);
                    System.out.println("update report=" + update);

                }

            } catch (SQLException e) {
                System.err.println("Caught SQLException: " + e.getMessage());
            } finally {
                con.close();
            }
        } catch (Exception e) {
            //   System.err.println("Caught Exception: " + e.getMessage());
//            ReportTO rTO = new ReportTO();
//            rTO.setMailSubjectTo("Trip Automation Error");
//            rTO.setMailContentTo("vehicle No : "+regNo);
//            rTO.setMailIdTo("arun@entitlesolutions.com");
//            rTO.setMailIdCc("throttleerror@gmail.com");
            // new EmailSSL(rTO).start();
        }
    }
}
