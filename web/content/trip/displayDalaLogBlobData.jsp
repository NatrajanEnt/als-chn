<%-- 
    Document   : displayBlobData
    Created on : Feb 22, 2014, 10:57:49 PM
    Author     : Arul
--%>

<%@page import="java.util.Properties"%>
<%@ page import="java.sql.*"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ page import="java.io.*"%>
                    <%
                        Blob image = null;
                        Blob licenseImage = null;

                        Connection con = null;

                        byte[] imgData = null;
                        byte[] licenseImgData = null;

                        Statement stmt = null;

                        ResultSet INR = null;
                        String tripGraphId = request.getParameter("tripGraphId");
                        String tripId = request.getParameter("tripId");
                         String fileName = "jdbc_url.properties";
                        Properties dbProps = new Properties();
                        //The forward slash "/" in front of in_filename will ensure that
                        //no package names are prepended to the filename when the Classloader
                        //search for the file in the classpath

                        InputStream is = getClass().getResourceAsStream("/"+fileName);
                        dbProps.load(is);//this may throw IOException
                        String dbClassName = dbProps.getProperty("jdbc.driverClassName");

                        String dbUrl = dbProps.getProperty("jdbc.url");
                        String dbUserName = dbProps.getProperty("jdbc.username");
                        String dbPassword = dbProps.getProperty("jdbc.password");

                        try {

                            Class.forName(dbClassName).newInstance();

                            con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);

                            stmt = con.createStatement();

                                INR = stmt.executeQuery("select graph_file from ts_trip_temperature_graph where trip_graph_id = " + tripGraphId);

                                if (INR.next()) {

                                    image = INR.getBlob(1);

                                    imgData = image.getBytes(1, (int) image.length());

                                } else {

                                    //out.println("Display Blob Example");

                                    //out.println("image not found for given id>");

                                    return;

                                }

                                // display the image
//                                response.setContentType("image/jpeg");
//                                response.setContentType("application/pdf");
                                SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy");
                                //System.out.println("Current Date: " + ft.format(dNow));
                                String curDate = "19-03-2014";
                                String expFile = "Data_Logger_Report-" + tripId + ".xls";
                                String fileNameNew = "attachment;filename=" + expFile;
                                response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                                response.setHeader("Content-disposition", fileNameNew);

                                OutputStream o = response.getOutputStream();
                                o.write(imgData);
                                o.flush();

                                o.close();


                        } catch (Exception e) {

                            out.println("Please Upload Image");

                            //out.println("Image Display Error=" + e.getMessage());

                            return;

                        } finally {

                            try {

                                INR.close();

                                stmt.close();

                                con.close();

                            } catch (SQLException e) {

                                e.printStackTrace();

                            }

                        }

                    %> 
