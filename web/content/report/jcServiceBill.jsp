
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <link rel="stylesheet" href="/throttle/css/rupees.css"  type="text/css" />

    <%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
    <%@ page import="ets.domain.renderservice.business.JobCardItemTO" %>
    <%@ page import="ets.domain.problemActivities.business.ProblemActivitiesTO" %>


    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
    <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt"  %>
    <%@ page import="java.util.*" %>

    <%@ page import="java.text.DecimalFormat" %>
    <%@ page import="java.text.NumberFormat" %>
    <title>Job Card Bill</title>

</head>







<script>



    function print(val)
    {
        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
            "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }


</script>


<body>

<%

int itemNameLength = 35,uomLength=10;
JobCardItemTO itemTO=null;
ProblemActivitiesTO activityTO = null;
int pageSize = 24;
String[] activity = null;
String[] companyAddress = null;
String[] customerAddress = null;
float discountVal = 0.00f;
float finalAmount = 0.00f;
ArrayList billRecord = (ArrayList) request.getAttribute("billRecord");
ArrayList billHeaderInfo = (ArrayList) request.getAttribute("billHeaderInfo");
    Iterator itr = billHeaderInfo.iterator();
    RenderServiceTO jcto =null;
    while(itr.hasNext()) {
        jcto = (RenderServiceTO)itr.next();
        discountVal = Float.parseFloat(jcto.getDiscount());
        finalAmount = Float.parseFloat(jcto.getFinalAmount());
        companyAddress = jcto.getCompanyAddressSplit();
        customerAddress = jcto.getAddressSplit();
    }

int itemSize = (Integer)request.getAttribute("itemSize");
int laborSize = (Integer) request.getAttribute("laborSize") ;
int taxSize = (Integer) request.getAttribute("taxSize") ;
int totalRecords = laborSize;
float laborTotal = 0;
float spareTotal = 0;
float taxTotal = 0;
double total = 0;
int index = 0;
int loopCntr = 0;

System.out.println("am here...0:"+billRecord.size());

for(int i=0;i< billRecord.size(); i=i+pageSize){
loopCntr = i;
%>

<div id="print<%= i %>" >

<style>


.text1 {
font-family:Tahoma;
font-size:14px;
color:#333333;
background-color:#E9FBFF;
padding-left:10px;
line-height:15px;
}
.text2 {
font-family:Tahoma;
font-size:12px;
color:#333333;
background-color:#FFFFFF;
padding-left:10px;
line-height:15px;
}


</style>
<% int cntr1 = 1; %>

    <c:if test= "${billHeaderInfo != null}"  >
        <c:forEach items="${billHeaderInfo}" var="bill" >
            <% if (cntr1 ==1) {

%>
    <table width="825" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
        <tr>
            <td height="27" colspan="4" class="text2"  align="center"  scope="row" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
               <b> <c:out value="${bill.invoiceType}" /> INVOICE </b>
            </td>
        </tr>
        <c:set var="totalAmountN" value="${bill.totalAmount}" />
        <c:set var="nettAmountN" value="${bill.nettAmount}" />
        <c:set var="gstAmountN" value="${bill.gstAmount}" />
        <c:set var="discountAmountN" value="${bill.discountAmount}" />
        <c:set var="roundOffN" value="${bill.roundOff}" />
        <c:set var="finalAmountN" value="${bill.finalAmount}" />
        <c:set var="jobcardNo" value="${bill.jcMYFormatNo}" />
        <c:set var="createdDate" value="${bill.date}" />
        <c:set var="billDiscount" value="${bill.discount}" />
        <c:set var="jcManualCreatedDate" value="${bill.createdDate}" />
        <c:set var="manualJcNo" value="${bill.remarks}" />
        <c:set var="servicePtName" value="${bill.servicePtName}" />
        <tr>
            <td class="text2" colspan="2" width="427"  align="left" rowspan="2" scope="col" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                <img src="images/throttle_logo_header.png" style="height:30px;width:100px;margin-left:-01px;">
                <p> <b>Throttle </b><br>
                   <% for(int m=0; m<companyAddress.length;m++){
                        %>
                        <%= companyAddress[m]%> <br>
                        <%
                        }
                    %>
                    <c:out value="${bill.companyCity}" /> - <c:out value="${bill.companyPincode}" /> <br>
                    E-mail :<c:out value="${bill.companyEmail}" />
                    GST No :33AAECP6192M1Z0
                </p>
            <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                <p>Invoice No<br>
                    <c:if test= "${bill.invoiceNo != ''}"  >
                <b> <c:out value="${bill.invoiceNo}" /> </b>
                </c:if>
                <c:if test= "${bill.invoiceNo == ''}"  >
                <b> <c:out value="${bill.billNo}" /> </b>
                </c:if>
                </p>
            </td>
            <td  class="text2"  align="left"  width="148" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                <p>Dated<br>
                <b> <c:out value="${bill.billDate}" /> </b> </p>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left" scope="col"  style=" padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Vehicle No.<br>
                <b> <c:out value="${bill.vehicleNo}" /> </b> </p>
            </td>
            <td  class="text2"  align="left" scope="col"  style="padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
                <p>Vehicle Current km<br>
                <b><c:out value="${bill.km}" /></b>

                    <!--<b>-&nbsp;<c:out value="${bill.usageTypeName}" /></b>-->

                </p>
            </td>
        </tr>
        <tr>
            <td colspan="2" rowspan="4" align="left" valign="top" class="text2" scope="col"  style="border:1px; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p><b>Customer</b><br>
                <b>M/S <c:out value="${bill.customerName}" /></b> <br>
                 <% for(int m=0; m< customerAddress.length;m++){
                        %>
                        <%= customerAddress[m]%> <br>
                        <%
                        }
                    %>
                    <c:out value="${bill.city}" /> <br>
                    <c:out value="${bill.phone}" /> <br>
                    E-mail :<c:out value="${bill.email}" />
                    GST No :<c:out value="${bill.gstNo}" /> 
                <p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-right-color:#CCCCCC; border-right-style:solid;border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Customer's Order No.<br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;">
                <p>Dated<br>
                &nbsp;</p>
            </td>
        </tr>
        <%--<tr>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Despatch Document  No.<br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Dated<br>
                &nbsp;</p>
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p>Despatched through <br>
                &nbsp;</p>
            </td>
            <td class="text2"  align="left"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; ">
                <p>Destination<br>
                &nbsp;</p>
            </td>
        </tr> --%>
        <tr>
            <td  class="text2"  colspan="2" align="left"  style="border:1px;">
                <p>Terms of Delivery <br>
                <p> &nbsp;
            </td>
        </tr>
    </table>
            <% }
    cntr1++;
%>
    </c:forEach>
</c:if>








    <table width="825"  border="0" cellpadding="0"  style="border:1px; border-left-color:#E8E8E8; border-left-style:solid; border-right-color:#E8E8E8; border-right-style:solid;"  cellspacing="0">
       


<%
System.out.println("am here...2");
if(loopCntr > (itemSize+taxSize-1) && loopCntr < i+pageSize ) { %>

       <tr>
            <th width="20" class="text2"  height="30" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; " scope="col" >Sno</th>
            <th colspan="6" width="600" class="text2" style="border:1px;width:200px; border-color:#FFFFFF; border-bottom-color:#CCCCCC; border-bottom-style:solid;  border-top-color:#CCCCCC; border-top-style:solid;" scope="col" >Labour Charges</th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">SAC Code</th>
            <!--<th width="50"  class="text2" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Qty</th>-->
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Price</th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">Disc%</th>
            <th width="50"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col">Taxable Amount</th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">CGST %</th>
            <th width="65"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">SGST %</th>
            <th width="50" class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; "  scope="col">IGST %</th>            
            
            <th width="50"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col">GST Amount</th>
            <th width="55"  class="text2"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " scope="col"> Total Amount</th>
        </tr>

<%
    float gstTotal = 0.00F;
    float nettTotal = 0.00F;
    for(int l=loopCntr; ( l < ( billRecord.size() ) && ( loopCntr < i+pageSize )  ) ; l++ ) {
       activityTO = new ProblemActivitiesTO();
       activityTO = (ProblemActivitiesTO) billRecord.get(l);
       activity = activityTO.getActivitySplt();
       ++loopCntr;

       gstTotal = gstTotal + Float.parseFloat(activityTO.getTaxAmount());
       nettTotal = nettTotal + Float.parseFloat(activityTO.getNettAmount());
%>




        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
		<% index++; %>
                 <%= index %>
            </td>
            <td class="text2"  align="left"  colspan="6"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                    <% for(int k=0;k<activity.length ; k++){ %>
                      <%= activity[k] %>   <br>
                    <% } %>
            </td>
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                    <%= activityTO.getSacCode() %>
            </td>
<!--            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
//                    <%= activityTO.getQty() %>
//            </td>-->
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                    <%= activityTO.getAmount() %>
            </td>
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                   <%= activityTO.getDiscount() %>
            </td>
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                   <%= activityTO.getTaxableAmount() %>
            </td>
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                   <%= activityTO.getCgst() %>
            </td>
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                   <%= activityTO.getSgst() %>
            </td>
            <td class="text2"   align="center"   style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; " scope="row" >
                   <%= activityTO.getIgst() %>
            </td>

            
            <td class="text2"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
               <%= activityTO.getTaxAmount() %>
            </td>
            <td class="text2"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <%= activityTO.getNettAmount() %>
            </td>

        </tr>
<% } %>




<% } %>

<%
System.out.println("am here...3");
total = (double) (spareTotal + laborTotal + taxTotal ); %>

            <% for(int m=loopCntr; ( (m%pageSize) != 0); m++){ %>

       <%-- <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;"  scope="row" >
                &nbsp;Hi
            </td>
            <td class="text2" colspan="10"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>  --%>
            <% } %>

<% if(loopCntr != (totalRecords) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="10"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>

                <tr>
	            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
	                &nbsp;
	            </td>
	            <td class="text2" colspan="10"  align="left"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
	               &nbsp;
	            </td>
	            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
	                &nbsp;
	            </td>
        </tr>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="10"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td class="text2" colspan="10"  align="left" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid;" scope="row" >
               &nbsp;
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-bottom-color:#CCCCCC; border-bottom-style:solid; "  align="right" >
                &nbsp;
            </td>
        </tr>


<% } if(loopCntr == (totalRecords) ) {
System.out.println("am here...4");
    %>


        <tr>
            <td class="text2" colspan="16" align="right"   scope="row" >
                &nbsp;
            </td>
        </tr>
        <tr>
            <td class="text2" colspan="15" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Total Amount
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                  <c:out value="${totalAmountN}" />
            </td>
        </tr>








        <%if(discountVal > 0){%>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="14" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Discount(<%=discountVal%>%)
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
 			<%
                        NumberFormat formatter1 = new DecimalFormat("#0.00");
                        %>

                        <c:out value="${discountAmountN}" /> 

            </td>
        </tr>
        <%}%>
        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="14" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                GST
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >

                <c:out value="${gstAmountN}" />
            </td>
        </tr>


        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="14" align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid; border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
                Round Off
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >

                <c:out value="${roundOffN}" />
            </td>
        </tr>


        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC; " scope="row" >
				&nbsp;
            </td>
            <td class="text2" colspan="14"  align="right"  style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;border-bottom-style:solid; border-bottom-color:#CCCCCC;  border-left-color:#CCCCCC; border-left-style:solid;" scope="row" >
               <b> Grand Total <span class="WebRupee">SAR.</span></b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF; border-right-color:#CCCCCC; border-right-style:solid;  "  align="right" >
                <b><c:out value="${finalAmountN}" /></b>
            </td>

        </tr>


<% } %>


        

<% if(loopCntr != (totalRecords) ) { %>

        <tr>
            <td class="text2"  align="left"  style="border:1px; border-color:#FFFFFF;  border-left-color:#CCCCCC; border-left-style:solid;  border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                &nbsp;
            </td>
            <td class="text2" colspan="10"  align="center" style="border:1px; border-color:#FFFFFF;    border-bottom-color:#CCCCCC; border-bottom-style:solid; " scope="row" >
                <b>Contnd..</b>
            </td>
            <td  class="text2" style="border:1px; border-color:#FFFFFF;  border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid;"  align="right" >
               &nbsp;
            </td>
        </tr>






<% } %>

    </table>




<table width="825" height="79" border="0" style="border:1px; border-color:#E8E8E8; border-style:solid;"  cellpadding="0" cellspacing="0">
<tr>
<td width="720">
<div class="text2" >
 <% if(loopCntr == (totalRecords) ) { %>
 Amount Chargeable (in words)
                    <jsp:useBean id="numberWords1"   class="ets.domain.report.business.NumberWords" >
 			<% numberWords1.setRoundedValue(finalAmount+""); %>

 			<% numberWords1.setNumberInWords(numberWords1.getRoundedValue()); %>

                    <b>  Rupees <jsp:getProperty name="numberWords1" property="numberInWords" /> Only</b>
                    </jsp:useBean>
<% } %>
 <br>
<%--<br>--%>

Remarks:
<% if(loopCntr == (totalRecords) ) { %>
<% if(request.getAttribute("vendorNames") != null ){ %>
<!--<b> <%= request.getAttribute("vendorNames") %> </b>-->
<% } %>
<% } %>
<br>
<table height="10" align="left" cellpadding="0" cellspacing="0" border="0">

    <tr>
        <td width="200" class="text2">
            <b>  Jobcard Created Date</b>
        </td>
        <td class="text2">
            :&nbsp;<b><c:out value="${jcManualCreatedDate}" /></b>
        </td>
    </tr>

    <tr>
        <td width="200" class="text2" >
            <b> AS PER <c:out value="${servicePtName}" />&nbsp;JC.No</b>
        </td>
        <td class="text2">
            :&nbsp;<b><c:out value="${jobcardNo}" /></b>
        </td>
    </tr>

<!--    <tr>
        <td width="200" class="text2" >
            <b> Corporate Office Address</b>
        </td>
        <td class="text2">
            :&nbsp;<p> <b>PARVEEN AUTOMOBILES PVT. LTD. </b><br>
                   OLD NO-25 NEW NO-3/67,palavayal,<br>
                    sothupakkam road,vadakarai(post),<br>
                    Redhills, Chennai-600 052<br>
                    E-mail : accounts@parveenautomobiles.in</p>
        </td>
    </tr>-->
    <tr>
        <td width="200" class="text2"  >
            <b> Helpline NO (24X7)</b>
        </td>
        <td class="text2">
            :&nbsp;<b>+918754498622</b>
        </td>
    </tr>
   <tr>
       <td class="text2" width="380" align="left" colspan="2" >&nbsp; </td>
   </tr>

   <tr>
       <td class="text2" width="380" align="left" colspan="2" >&nbsp; </td>
   </tr>

   <tr>
    <td class="text2" width="380" align="left"><b>Declaration</b> </td>
    <td class="text2" width="315" align="right"><b> for Throttle. </b> </td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">We declare that this invoice shows the actual price of the</td>
    <td class="text2" width="325" align="right"> &nbsp;</td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">goods described and that all particulars are true and correct</td>
    <td class="text2" width="325" align="right">&nbsp; </td>
    </tr>
    <tr>
    <td class="text2" width="380" align="left">&nbsp; </td>
    <td class="text2" width="315" align="right"><b> Authorized Signatory </b></td>
    </tr>
    <tr>
    <table border="0" align="center">
     <tr>
        <td class="text2"  align="center">
            <p><b>SUBJECT TO CHENNAI JURISDICTION</b><br>
               This is a Computer Generated Invoice</p></td>
     </tr>
    </table>
    </tr>

</table>

</div>

</td>
</tr>
</table>



<%--<br>
<div ALIGN="CENTER" class="text2">
SUBJECT TO CHENNAI JURISDICTION &nbsp;This is a Computer Generated Invoice
</div>--%>
</div>
<br>
    <center>
        <input type="button" class="button" name="Print" value="Print" onClick="print('print<%= i %>');" > &nbsp;
    </center>
<br>
<br>
<% } %>

<!--   <table width="825" border="1" cellspacing="0" cellpadding="0"  style="border:1px; border-color:#E8E8E8; border-style:solid;" >
        <tr>
<td width="720">
<div class="text2" >
<td height="27" colspan="4" class="text2"  align="center"  scope="row" style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  ">
              <b>GATE PASS </b>
            </td>    
</table>-->
<%--<c:if test= "${billHeaderInfo != null}"  >
        <c:forEach items="${billHeaderInfo}" var="bill" >--%>
<!--<table width="825" height="79" border="0" style="border:1px; border-color:#E8E8E8; border-style:solid;"  cellpadding="0" cellspacing="0">
<tr>
    <td class="text2"  align="left" scope="col"  style=" padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
             <p>Vehicle No.<br>
                <b> <c:out value="${bill.vehicleNo}" /> </b> </p>
               
    </td>
    
   <td class="text2"  align="left" scope="col"  style=" padding-bottom:3px; padding-top:3px; border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; ">
             <p>JC No.<br>
               <b><c:out value="${servicePtName}" /></b>   </p>    
        
           <b><c:out value="${jobcardNo}" /></b> 
        </td>
   
    <td  class="text2"  align="left"  width="150" height="53" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid; border-right-color:#CCCCCC; border-right-style:solid; " >
                <p>Invoice No<br>
                   <c:if test= "${bill.invoiceNo != ''}"  >
                <b> <c:out value="${bill.invoiceNo}" /> </b>
                </c:if>
                <c:if test= "${bill.invoiceNo == ''}"  >
                <b> <c:out value="${bill.billNo}" /> </b>
                </c:if>
                </p>
               
            </td>
            
             <td  class="text2"  align="left"  width="148" scope="col"  style="border:1px; border-bottom-color:#CCCCCC; border-bottom-style:solid;  " >
                <p>Dated<br>
                <b> <c:out value="${bill.billDate}" /> </b> </p>
            </td>            
            </tr>
            <tr>
                 <td  rowspan="4" align="left" valign="top" class="text2" scope="col"  style="border:1px; border-right-color:#CCCCCC; border-right-style:solid; ">
                <p><b>Customer</b><br>
                <b>M/S <c:out value="${bill.customerName}" /></b> <br>
                 <% for(int m=0; m< customerAddress.length;m++){
                        %>
                        <%= customerAddress[m]%> <br>
                        <%
                        }
                    %>
                    <c:out value="${bill.city}" /> <br>
                    <c:out value="${bill.phone}" /> <br>
                    E-mail :<c:out value="${bill.email}" />
                    GST No :<c:out value="${bill.gstNo}" /> 
                <p>
            </td>
            
            <td class="text2" width="150" align="left"><b>Declaration</b> </td>
            
            </tr>
             <tr>
                 
    <td class="text2" width="200" align="left">We declare that this invoice has been fully paid     </td>
         </tr>       
         
          <tr>
    <td class="text2" width="150" align="left">&nbsp; </td>
    <td class="text2" width="200" align="right"><b> Authorized Signatory </b></td>
    </tr>

</table>-->

</body>
</html>

