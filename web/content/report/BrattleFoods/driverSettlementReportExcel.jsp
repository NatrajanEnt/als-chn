<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "DriverSettlementDetails-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

            <c:if test = "${driverSettlementDetails != null}" >
                <table style="width: 1100px" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="80" >
                            <th><h3 align="center">S.No</h3></th>
                            <th><h3 align="center">Trip Code</h3></th>
                            <th><h3 align="center">Vehicle No</h3></th>
                            <th><h3 align="center">Settlement Date</h3></th>
                            <th><h3 align="center">Driver Name</h3></th>
                            <th><h3 align="center">Run Km</h3></th>
                            <th><h3 align="center">Run Hour</h3></th>
                            <th><h3 align="center">Fuel Price</h3></th>
                            <th><h3 align="center">Diesel Used</h3></th>
                            <th><h3 align="center">RCM Allocation</h3></th>
                            <th><h3 align="center">BPCL Allocation</h3></th>
                            <th><h3 align="center">Extra Expense</h3></th>
                            <th><h3 align="center">Total Misc</h3></th>
                            <th><h3 align="center">Bhatta</h3></th>
                            <th><h3 align="center">Total Expense</h3></th>
                            <th><h3 align="center">Balance</h3></th>
                            <th><h3 align="center">Start Balance</h3></th>
                            <th><h3 align="center">End Balance</h3></th>
                            <th><h3 align="center">Pay Mode</h3></th>
                            <th><h3 align="center">Remarks</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                         <% int index = 0, sno = 1;%>
                        <c:forEach items="${driverSettlementDetails}" var="dsList">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <% if (oddEven > 0) { %>
                            <tr height="30">

                                <td align="center"><%=sno%></td>
                                <td align="left" class="text1"><c:out value="${dsList.tripCode}"/></td>
                                <td align="left" class="text1"><c:out value="${dsList.vehicleNo}"/></td>
                                <td align="left" class="text1"><c:out value="${dsList.settlementDate}"/></td>
                                <td align="left" class="text1"><c:out value="${dsList.empName}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.distance}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.runHour}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.fuelPrice}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.dieselUsed}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.rcmAllocation}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.bpclAllocation}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.extraExpense}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.totalMiscellaneous}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.bhatta}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.totalExpense}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.balance}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.startingBalance}"/></td>
                                <td align="right" class="text1"><c:out value="${dsList.endingBalance}"/></td>
                                <td align="left" class="text1"><c:out value="${dsList.payMode}"/></td>
                                <td align="left" class="text1"><c:out value="${dsList.remarks}"/></td>
                            </tr>
                            <%}else{%>
                             <tr height="30">

                                <td align="center"><%=sno%></td>
                                <td align="left" class="text2"><c:out value="${dsList.tripCode}"/></td>
                                <td align="left" class="text2"><c:out value="${dsList.vehicleNo}"/></td>
                                <td align="left" class="text2"><c:out value="${dsList.settlementDate}"/></td>
                                <td align="left" class="text2"><c:out value="${dsList.empName}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.distance}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.runHour}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.fuelPrice}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.dieselUsed}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.rcmAllocation}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.bpclAllocation}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.extraExpense}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.totalMiscellaneous}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.bhatta}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.totalExpense}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.balance}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.startingBalance}"/></td>
                                <td align="right" class="text2"><c:out value="${dsList.endingBalance}"/></td>
                                <td align="left" class="text2"><c:out value="${dsList.payMode}"/></td>
                                <td align="left" class="text2"><c:out value="${dsList.remarks}"/></td>
                            </tr>
                            <%}%>
                            <%
                                       index++;
                                       sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>


            <table>
                <tr>
                    <td></td>
                    <td></td>
                </tr>

            </table>
            <br/>
            <br/>
            <c:if test = "${customerWiseProfitList != null}" >
            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="0" >
                <tr height="25" align="right">
                    <td style="background-color: #6374AB; color: #ffffff">Total Trips Carried Out</td>
                    <td width="150" align="right"><fmt:formatNumber type="number"  value="${totalTripNos}" /></td>
                </tr>
                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff">Total Income</td>
                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRevenue}" /></td>
                </tr>
                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff">Total Fixed Expenses</td>
                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${expenses}" /></td>
                </tr>
                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff">Total Profit </td>
                    <td width="150" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profit}" /></font></td>
                </tr>
                <tr height="25">
                    <td style="background-color: #6374AB; color: #ffffff">Total Profit % </td>
                    <td width="150" align="right"><font color="green">
                            <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${perc/count}" />
                            </font></td>
                </tr>
            </table>
            </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>