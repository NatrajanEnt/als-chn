<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript" language="javascript">

    function submitPage() {
        var podFiles = document.getElementsByName("podFile");
        var statusCheck = true;
        for (var i = 0; i < podFiles.length; i++) {
            if (podFiles[i].value == '' || podFiles[i].value == null) {
                alert('upload the pod attachment..')
                statusCheck = false;
            }
        }

        if (statusCheck) {
            document.cNote.action = '/throttle/saveCNoteDocumentUpload.do';
            document.cNote.submit();
        }
    }

</script>
<script language="">
    function print(val)
    {
        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> CNote</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">View Consignment Details</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body onload="addRow();">
                <form name="cNote" method="post" enctype="multipart/form-data">
                    <c:set var="orderStatus" value="0" />
                    <c:set var="emptyVaidityDate" value="" />
                    <c:if test="${cNoteDetails != null}">
                        <c:forEach items="${cNoteDetails}" var="cnote">
                             <c:set var="cNote" value="" />
                             <c:set var="bookingNo" value="" />
                             <c:set var="bookingDate" value="" />
                            <table class="table table-info mb30 table-hover" style="width:100%;display: none">
                                <tr height="30"   ><td colSpan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Consignment Note <input type="hidden" name="consignmentOrderId" id="consignmentOrderId" value="<c:out value="${cnote.consignmentOrderId}"/>"/></td></tr>
                                <tr>
                                    <td  >Consignment Note </td>
                                    <td  ><label><c:out value="${cnote.consignmentNoteNo}"/></label></td>
                                    <td  >Consignment Date</td>
                                    <td  ><label><c:out value="${cnote.date}"/></label></td>
                                            <c:set var="cNote" value="${cnote.consignmentNoteNo}" />
                                            <c:set var="bookingNo" value="${cnote.customerOrderRefNo}" />
                                            <c:set var="bookingDate" value="${cnote.date}" />
                                <tr>
                                <tr>
                                    <td  >Booking No</td>
                                    <td  ><label><c:out value="${cnote.customerOrderRefNo}"/></label></td>
                                    
                                </tr>
                            </table>
                                


                                        <script>
                                            //                                    $(".nexttab").click(function() {
                                            //                                        var selected = $("#tabs").tabs("option", "selected");
                                            //                                        $("#tabs").tabs("option", "selected", selected + 1);
                                            //                                    });
                                            $('.btnNext').click(function() {
                                                $('.nav-tabs > .active').next('li').find('a').trigger('click');
                                            });
                                            $('.btnPrevious').click(function() {
                                                $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                                            });
                                        </script>

                                    </c:forEach>
                                </c:if>
                                        <center><font size="4" color="black">C.Note :&emsp; <c:out value="${cNote}"/> &emsp; Booking No : &emsp;<c:out value="${bookingNo}"/> &emsp; Date : <c:out value="${bookingDate}"/></center>
                                        <br>
                                        <br>
                                        
                                        <table align="center" class="table table-info mb30 table-border" id="pod1" style="width: 80%">
                                <tr id="rowId0" style="background-color:#5BC0DE;width:100%;color:white;font-size: 14px;font-weight: bold;" height="30px;">
                                    <td  align="center" height="30" >Sno</td>
                                    <td  height="30" align="left">Attachment</td>
                                    <td  height="30" align="left">Remarks</td>
                                    <td  height="30" align="left">Delete</td>
                                </tr>
                                <% int index = 1;%>
                                <% int index2 = 1;%>
                                   <tr>
                                        <c:if test="${podDetails != null}">
                                            <c:forEach items="${podDetails}" var="viewPODDetails">
                                        <%
                                           String classText3 = "";
                                           int oddEven = index2 % 2;
                                           if (oddEven > 0) {
                                               classText3 = "text1";
                                           } else {
                                               classText3 = "text2";
                                           }
                                        %>
                                        <tr height="30px">
                                            <td  ><%=index2++%></td>
                                            <td  >
                                                <input type="hidden" value="<c:out value="${viewPODDetails.originId}"/>" name="uploadId" id="uploadId">
                                                <a href="uploadFiles/Files/<c:out value="${viewPODDetails.consolName}"/>" target="_blank"> <c:out value="${viewPODDetails.consolName}"/></a>  </td>
<%--                                            <td  >
                                                <a onclick="viewPODFiles('<c:out value="${viewPODDetails.originId}"/>')" href="#"><c:out value="${viewPODDetails.podFile}"/></a>
                                            </td>--%>
                                            <td  ><c:out value="${viewPODDetails.portName}"/></td>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </tr>
                                    </c:forEach>
                                        </c:if>
                                    </tr> 
                                <script>
                                    var podRowCount1 = 1;
                                    function addRow() {
                                        var podSno1 = document.getElementById('selectedRowCount').value;
                                        var tab = document.getElementById("pod1");
                                        var newrow = tab.insertRow(podSno1);

                                        var newrow = tab.insertRow(podSno1);
                                        //                alert(newrow.id);
                                        newrow.id = 'rowId' + podSno1;

                                        var cell = newrow.insertCell(0);
                                        var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
                                        cell.innerHTML = cell0;


                                        cell = newrow.insertCell(1);
                                        var cell0 = "<td class='text1' align='center' height='25' ><input type='file'   id='podFile" + podSno1 + "' name='podFile' class='form-control' value='' style='width:240px;' ><br><font size='2' color='blue'> Allowed file type:pdf & image</td>";
                                        cell.innerHTML = cell0;

                                        cell = newrow.insertCell(2);
                                        cell0 = "<td class='text1' height='25' ><textarea rows='2' cols='50' class='textbox' name='podRemarks' id='podRemarks" + podSno1 + "' style='width:260px;height:40px;'></textarea></td>";
                                        cell.innerHTML = cell0;
                                        
                                        cell = newrow.insertCell(3);
                                        var cell0= "<td class='text1' height='25' ><input type='button' class='btn btn-info'   name='delete'  id='delete" + podSno1 + "'   value='Delete Row' onclick='deleteRow(this);' ></td>";
                                        cell.innerHTML = cell0;


                                        podSno1++;
                                        if (podSno1 > 0) {
                                            document.getElementById('selectedRowCount').value = podSno1;
                                        }

                                    }
                                     function deleteRow(src) {
//                                         alert("sno");
                                          podRowCount1--;
                                          var oRow = src.parentNode.parentNode;
                                          var dRow = document.getElementById('pod1');
                                          dRow.deleteRow(oRow.rowIndex);
                                          document.getElementById("selectedRowCount1").value--;
                                        }

                                </script>

                                <%index++;%>
                                
                                
                            </table>
                                     <center>
                                        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="1"/> 
                                        <input type="hidden" name="selectedRowCount1" id="selectedRowCount1" value="0"/> 
                                        <input type="hidden" name="consignmentId" id="consignmentId" class="form-control" value="<%=request.getParameter("consignmentId")%>" />
                                        <input type="button" class="btn btn-success" name="add" value="Add" onclick="addRow();" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                                        <input type="button" class="btn btn-success" name="Save" value="Save & Close" onclick="submitPage();" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>
                                    </cenetr>   
                                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                                </body>
                        </div>
                    </div>
                    <%@ include file="/content/common/NewDesign/settings.jsp" %>