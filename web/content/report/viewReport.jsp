<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script>

    function viewTripDetails(tripIds) {
        //alert(tripIds);
        window.open('/throttle/viewTripDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
    }

</script>
<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>


<script type="text/javascript">

    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        // alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

    $(document).ready(function() {
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNoforReport.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        // alert("My city");
                        if (data == '') {
                            alert('please enter valid vehicleNo');
                            $('#vehicleNo').val('');
                            $('#vehicleNo').focus();
                        }
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {

                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#vehicleId').val(id);
                $('#vehicleNo').val(value);

                return false;
            }

        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });

    function submitPage(val) {
        if (val == 'Search') {
            document.manufacturer.action = '/throttle/report.do?param=Search';
            document.manufacturer.submit();
        } else {
            document.manufacturer.action = '/throttle/report.do?param=ExportExcel';
            document.manufacturer.submit();
        }

    }
    function clearDate() {
        document.getElementById("fromDate").value = "";
        document.getElementById("toDate").value = "";
    }

</script>
<style>
    #index td {
        color:white;
    }
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>
        <spring:message code="operations.reports.label.TruckDriverMappingReport" text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="header.label.YouAreHere" text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="header.label.Home" text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="header.label.Reports" text="default text"/></a></li>
            <li class="active">
                <spring:message code="operations.reports.label.TruckDriverMappingReport" text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <form name="manufacturer" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>

                    <table class="table table-bordered">

                        <td style="border-color:#5BC0DE;padding:16px;">
                            <table  class="table table-info mb30 table-hover">
                                <tr>
                                    <td><font color="red"></font><spring:message code="operations.reports.label.VehicleNo" text="default text"/></td>
                                    <td height="30">
                                        <input type="hidden" name="vehicleId" id="vehicleId" value=""/>
                                        <input type="text" style="width:240px;height:40px;" name="vehicleNo" id="vehicleNo" class="form-control" value="" />

                                    </td>
                                    <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                    <td height="30"><input style="width:260px;height:40px;" name="fromDate" id="fromDate" type="text" class="datepicker" value="" ></td>
                                    <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                    <td height="30"><input style="width:260px;height:40px;" name="toDate" id="toDate" type="text" class="datepicker" value=""></td>
                                </tr>

                                <tr >
                                    <td colspan="6" align="center" height="30">
                                        <input type="button"  class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;&nbsp;&nbsp;
                                        <input type="button"  class="btn btn-success" name="Search"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </table>

                    <% int index = 0; %>

                    <c:if test = "${reportList != null}" >

                        <table class="table table-info mb30 table-hover">
                            <thead>

                            <th ><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                            <th ><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>
                            <th ><spring:message code="operations.reports.label.DriverName" text="default text"/></th>
                            <th ><spring:message code="operations.reports.label.NoofTrip" text="default text"/></th>
                            </thead>

                            <%

                            %>

                            <c:forEach items="${reportList}" var="report">
                                <%

                    String classText = "";
                    int oddEven = index % 2;
                    if (oddEven > 0) {
                        classText = "text2";
                    } else {
                        classText = "text1";
                    }
                                %>
                                <tr>
                                    <td class="<%=classText %>" height="30"><%=index + 1%></td>
                                    <td class="<%=classText %>" height="30"><c:out value="${report.regNo}"/></td>
                                    <td class="<%=classText %>" height="30"><c:out value="${report.driverName}"/></td>
                                    <td class="<%=classText %>" height="30"> <a href="" onclick="viewTripDetails('<c:out value="${report.tripId}"/>');"><c:out value="${report.noOfTrips}"/></a></td>


                                </tr>
                                <%
                    index++;
                                %>
                            </c:forEach >

                        </table>

                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>

                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                    </c:if>

                <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>


