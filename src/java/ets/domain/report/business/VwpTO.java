/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.report.business;

public class VwpTO {

    
    private String vehicleTypeId = null;
    private String vehicleTypeName = null;
    private String movementType = null;
    private String totalCount = null;
    
    private String year = null;
    private String startDate = null;
    private String endDate = null;
    private String weekId = null;
    private String weekName = null;
    private String noOfDays = null;
    private String dateRange = null;
    private String tripIds = "0";
    private String fortyTripleAxleMofIds = "0";
    private String fortyTripleAxleLocIds = "0";
    private String fortyDoubleAxleMofIds = "0";
    private String fortyDoubleAxleLocIds = "0";
    private String twentyDoubleAxleMofIds = "0";
    private String twentyDoubleAxleLocIds = "0";
    private String fortyTripleAxleMof = "0";
    private String fortyTripleAxleLoc = "0";
    private String fortyDoubleAxleMof = "0";
    private String fortyDoubleAxleLoc = "0";
    private String twentyDoubleAxleMof = "0";
    private String twentyDoubleAxleLoc = "0";
    private String totalMof = "0";
    private String totalLoc = "0";
    private String nettTotal = "0";
    private String nettMof = "0";
    private String avePerVehicleWeek = "0";
    private String avePerVehicleMonth = "0";

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getMovementType() {
        return movementType;
    }

    public void setMovementType(String movementType) {
        this.movementType = movementType;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

    public String getWeekName() {
        return weekName;
    }

    public void setWeekName(String weekName) {
        this.weekName = weekName;
    }

    public String getNoOfDays() {
        return noOfDays;
    }

    public void setNoOfDays(String noOfDays) {
        this.noOfDays = noOfDays;
    }

    public String getDateRange() {
        return dateRange;
    }

    public void setDateRange(String dateRange) {
        this.dateRange = dateRange;
    }

    public String getFortyTripleAxleMof() {
        return fortyTripleAxleMof;
    }

    public void setFortyTripleAxleMof(String fortyTripleAxleMof) {
        this.fortyTripleAxleMof = fortyTripleAxleMof;
    }

    public String getFortyTripleAxleLoc() {
        return fortyTripleAxleLoc;
    }

    public void setFortyTripleAxleLoc(String fortyTripleAxleLoc) {
        this.fortyTripleAxleLoc = fortyTripleAxleLoc;
    }

    public String getFortyDoubleAxleMof() {
        return fortyDoubleAxleMof;
    }

    public void setFortyDoubleAxleMof(String fortyDoubleAxleMof) {
        this.fortyDoubleAxleMof = fortyDoubleAxleMof;
    }

    public String getFortyDoubleAxleLoc() {
        return fortyDoubleAxleLoc;
    }

    public void setFortyDoubleAxleLoc(String fortyDoubleAxleLoc) {
        this.fortyDoubleAxleLoc = fortyDoubleAxleLoc;
    }

    public String getTwentyDoubleAxleMof() {
        return twentyDoubleAxleMof;
    }

    public void setTwentyDoubleAxleMof(String twentyDoubleAxleMof) {
        this.twentyDoubleAxleMof = twentyDoubleAxleMof;
    }

    public String getTwentyDoubleAxleLoc() {
        return twentyDoubleAxleLoc;
    }

    public void setTwentyDoubleAxleLoc(String twentyDoubleAxleLoc) {
        this.twentyDoubleAxleLoc = twentyDoubleAxleLoc;
    }

    public String getTotalMof() {
        return totalMof;
    }

    public void setTotalMof(String totalMof) {
        this.totalMof = totalMof;
    }

    public String getTotalLoc() {
        return totalLoc;
    }

    public void setTotalLoc(String totalLoc) {
        this.totalLoc = totalLoc;
    }


    public String getAvePerVehicleWeek() {
        return avePerVehicleWeek;
    }

    public void setAvePerVehicleWeek(String avePerVehicleWeek) {
        this.avePerVehicleWeek = avePerVehicleWeek;
    }

    public String getAvePerVehicleMonth() {
        return avePerVehicleMonth;
    }

    public void setAvePerVehicleMonth(String avePerVehicleMonth) {
        this.avePerVehicleMonth = avePerVehicleMonth;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getNettTotal() {
        return nettTotal;
    }

    public void setNettTotal(String nettTotal) {
        this.nettTotal = nettTotal;
    }

    public String getNettMof() {
        return nettMof;
    }

    public void setNettMof(String nettMof) {
        this.nettMof = nettMof;
    }

    public String getWeekId() {
        return weekId;
    }

    public void setWeekId(String weekId) {
        this.weekId = weekId;
    }

    public String getTripIds() {
        return tripIds;
    }

    public void setTripIds(String tripIds) {
        this.tripIds = tripIds;
    }

    public String getFortyTripleAxleMofIds() {
        return fortyTripleAxleMofIds;
    }

    public void setFortyTripleAxleMofIds(String fortyTripleAxleMofIds) {
        this.fortyTripleAxleMofIds = fortyTripleAxleMofIds;
    }

    public String getFortyTripleAxleLocIds() {
        return fortyTripleAxleLocIds;
    }

    public void setFortyTripleAxleLocIds(String fortyTripleAxleLocIds) {
        this.fortyTripleAxleLocIds = fortyTripleAxleLocIds;
    }

    public String getFortyDoubleAxleMofIds() {
        return fortyDoubleAxleMofIds;
    }

    public void setFortyDoubleAxleMofIds(String fortyDoubleAxleMofIds) {
        this.fortyDoubleAxleMofIds = fortyDoubleAxleMofIds;
    }

    public String getFortyDoubleAxleLocIds() {
        return fortyDoubleAxleLocIds;
    }

    public void setFortyDoubleAxleLocIds(String fortyDoubleAxleLocIds) {
        this.fortyDoubleAxleLocIds = fortyDoubleAxleLocIds;
    }

    public String getTwentyDoubleAxleMofIds() {
        return twentyDoubleAxleMofIds;
    }

    public void setTwentyDoubleAxleMofIds(String twentyDoubleAxleMofIds) {
        this.twentyDoubleAxleMofIds = twentyDoubleAxleMofIds;
    }

    public String getTwentyDoubleAxleLocIds() {
        return twentyDoubleAxleLocIds;
    }

    public void setTwentyDoubleAxleLocIds(String twentyDoubleAxleLocIds) {
        this.twentyDoubleAxleLocIds = twentyDoubleAxleLocIds;
    }
    
    
    
}
