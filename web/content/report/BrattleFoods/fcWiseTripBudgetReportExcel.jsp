<%--
    Document   : vehicleUtilizationReportExcel
    Created on : Dec 23, 2013, 3:59:10 PM
    Author     : Administrator
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "FCWiseTripPerformanceSummary-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test="${FcWiseTripBudgetDetails != null}">
                  <table align="center" border="1" id="table" class="sortable" style="width:1000px;" >

                    <thead>
                    <tr>
                    <th><h3> S.No</h3></th>
                    <th width="120"><h3>FleetCenterName</h3></th>
                    <th><h3>Cost</h3></th>
                    <th><h3>TotalTrucks</h3></th>
                    <th><h3>TotalKms</h3></th>
                    <th><h3>TotalReferHrs</h3></th>
                    <th><h3>ReferCost</h3></th>
                    <th><h3>TotalCost</h3></th>
                    <th><h3>WFURefer(Approx)</h3></th>
                    <th><h3>Cost/KM</h3></th>
                    <th><h3>RCM</h3></th>
                    <th><h3>DeviationperTruck/KM</h3></th>
                    <th><h3>%Empty/Loaded(KM)</h3></th>
                    <th><h3>AvgKm</h3></th>
                    <th><h3>Fooding Advance</h3></th>
                    <th><h3>R&M Advance</h3></th>
                    </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        <c:set var="costPerKM" value="${0}"/>
                        <c:set var="averageKmEmpty" value="${0}"/>
                        <c:forEach items="${FcWiseTripBudgetDetails}" var="BPCLTD">
                            <c:set var="referCost" value="${BPCLTD.totalHms * 160}"/>
                            <c:if test="${BPCLTD.totalkms > 0.0}">
                            <c:set var="costPerKM" value="${BPCLTD.totalCost / BPCLTD.totalkms}"/>
                            </c:if>
                            <c:if test="${BPCLTD.totalkms > 0.0}">
                            <c:set var="averageKmEmpty" value="${(BPCLTD.emptyTripKM/BPCLTD.totalkms)*100}"/>
                            </c:if>
                            <c:set var="averageKm" value="${BPCLTD.totalkms/BPCLTD.noOfVehicles}"/>

                             <c:set var="totalKmEmpty" value="${TotalKmEmpty+BPCLTD.emptyTripKM}"/>

                            <c:set var="totalReferCost" value="${totalReferCost + referCost }"/>
                            <c:set var="totalNoofVehicles" value="${totalNoofVehicles + BPCLTD.noOfVehicles }"/>
                            <c:set var="totalTotalKMs" value="${totalTotalKMs + BPCLTD.totalkms }"/>
                            <c:set var="totalTotalHMs" value="${totalTotalHMs + BPCLTD.totalHms }"/>
                            <c:set var="totalTotalCost" value="${totalTotalCost + BPCLTD.totalCost }"/>
                            <c:set var="totalWfuAmount" value="${totalWfuAmount + BPCLTD.wfuAmount }"/>
                            <c:set var="totalrcm" value="${totalrcm + BPCLTD.rcm }"/>
                             <c:set var="totalFoodingAdvance" value="${totalFoodingAdvance + BPCLTD.foodingAdvance }"/>
                            <c:set var="totalRnmAdvance" value="${totalRnmAdvance + BPCLTD.repairAdvance }"/>
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                <td class="test1"><%=index++%></td>
                                <td class="test1" ><c:out value="${BPCLTD.companyName}"/></td>
                                <td class="test1" >&nbsp;</td>
                                <td class="test1" ><c:out value="${BPCLTD.noOfVehicles}"/></td>
                                <td class="test1" ><c:out value="${BPCLTD.totalkms}"/></td>
                                <td class="test1" ><c:out value="${BPCLTD.totalHms}"/></td>
                                <td class="test1" ><fmt:formatNumber pattern="##0.00" value="${referCost}"/></td>
                                <td class="test1" ><c:out value="${BPCLTD.totalCost}"/></td>
                                <td class="test1" ><c:out value="${BPCLTD.wfuAmount}"/></td>
                                <td class="test1" ><fmt:formatNumber pattern="##0.00" value="${costPerKM}"/></td>
                                <td class="test1" ><c:out value="${BPCLTD.rcm}"/></td>
                                <td  class="test1">&nbsp;</td>
                                <td class="test1" ><fmt:formatNumber pattern="##0.00" value="${averageKmEmpty}"/></td>
                                <td class="test1" ><fmt:formatNumber pattern="##0.00" value="${averageKm}"/></td>
                              	 <td   class="text1"><fmt:formatNumber pattern="##0.00" value="${BPCLTD.foodingAdvance}"/></td>
                                <td  class="text1"><fmt:formatNumber pattern="##0.00" value="${BPCLTD.repairAdvance}"/></td>
                            </tr>
                        </c:forEach>
                                <c:set var="totalCostPerKM" value="${0}"/>
                                <c:set var="totalAverageKmEmpty" value="${0}"/>
                                <c:set var="totalAverageKm" value="${totalTotalKMs /totalNoofVehicles}"/>
                                <c:if test="${totalTotalKMs > 0.0}">
                                <c:set var="totalCostPerKM" value="${totalTotalCost /totalTotalKMs}"/>
                                </c:if>
                                <c:if test="${totalTotalKMs > 0.0}">
                                <c:set var="totalAverageKmEmpty" value="${(TotalKmEmpty / totalTotalKMs)*100}"/>
                                </c:if>
                            <tr>
                                 <td   colspan="3" align="center">&nbsp;Total</td>
                                 <td  ><c:out value="${totalNoofVehicles}"/></td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalTotalKMs}"/></td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalTotalHMs}"/></td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalReferCost}"/></td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalTotalCost}"/></td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalWfuAmount}"/></td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalCostPerKM}"/></td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalrcm}"/></td>
                                 <td  >&nbsp;</td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalAverageKmEmpty}"/></td>
                                 <td  ><fmt:formatNumber pattern="##0.00" value="${totalAverageKm}"/></td>
                                 <td> <fmt:formatNumber pattern="##0.00" value="${totalFoodingAdvance}"/></td>
                                 <td> <fmt:formatNumber pattern="##0.00" value="${totalRnmAdvance}"/></td>
                            </tr>
                    </tbody>
                </table>

            </c:if>

            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
