<%-- 
    Document   : Accounts Receivable
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
    <body>
       
        <form name="accountReceivable" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "vehicleVendorWiseTrip-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>

            <c:if test = "${vehicleVendorwiseList != null}" >
                <table style="width: 1100px" align="center" border="1" id="table" class="sortable">
                    <thead>
                        <tr height="80" >
                            <th><h3 align="center" >S.No</h3></th>
                            <th><h3 align="center">Trailer No.</h3></th>
                            <th><h3 align="center">Vehicle Type</h3></th>
                            <th><h3 align="center">TPT</h3></th>
                            <th><h3 align="center">No. Of Trips Empty</h3></th>
                            <th><h3 align="center">No. Of Trips Loaded</h3></th>
                            <th><h3 align="center">Total No. Of Trips <br>(Container Wise, Empty & Loaded,<br> Bothway also taken as single)</h3></th>
                            <th><h3 align="center">Total Leased<br> Monthly Charges</h3></th>
                            <th><h3 align="center">Fixed Cost Per Trip <br>Per Vehicle <br>(Without Diesel & Trip Advance)</h3></th>
                            <th><h3 align="center">Average Trip Per Vehicle</h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:set var="profitPercent" value="${0}"/>
                         <c:set var="tranSporter" value=""/> 
                         <c:set var="emptyTripKM" value="${0}"/> 
                         <c:set var="loadedTrip" value="${0}"/> 
                         <c:set var="vehicleTypeName" value="${20}"/> 
                         <c:set var="vehicleTypeNames" value="${40}"/>
                         <c:set var="totalTwentyFtVehicle" value="0"/>
                         <c:set var="totalFourtyFtVehicle" value="0"/>
                         <c:set var="totalTwentyFtTrip" value="0"/>
                         <c:set var="totalFourtyFtTrip" value="0"/>
                         <c:set var="totalDICTVehicle" value="0"/>
                         <c:set var="totalPRCVehicle" value="0"/>
                         <c:set var="totalSKVehicle" value="0"/>
                         <c:set var="totalBABRAVehicle" value="0"/>
                         <c:set var="totalITLVehicle" value="0"/>
                        <c:forEach items="${vehicleVendorwiseList}" var="csList">
                            <c:set var="totalTripNos" value="${csList.loadedTrip + totalTripNos}"/>
                               <c:set var="totalEmptyTrip" value="${csList.emptyTripKM + totalEmptyTrip}"/>
                               <c:if test="${csList.vehicleTypeId == '1058'}">
                               <c:set var="totalTwentyFtVehicle" value="${totalTwentyFtVehicle + 1}"/>
                               <c:set var="totalTwentyFtTrip" value="${totalTwentyFtTrip +csList.loadedTrip +csList.emptyTripKM }"/>
                               </c:if>
                               <c:if test="${csList.vehicleTypeId == '1059'}">
                               <c:set var="totalFourtyFtVehicle" value="${totalFourtyFtVehicle + 1}"/>
                               <c:set var="totalFourtyFtTrip" value="${totalFourtyFtTrip+csList.loadedTrip +csList.emptyTripKM}"/>
                               </c:if>
                               <c:if test="${csList.tranSporter == 'DICT'}">
                                    <c:set var="totalDICTVehicle" value="${totalDICTVehicle + 1}"/>
                               </c:if>
                               <c:if test="${csList.tranSporter=='PRC'}">
                                    <c:set var="totalPRCVehicle" value="${totalPRCVehicle+1}"/>
                               </c:if>
                               <c:if test="${csList.tranSporter=='ITL'}">
                                    <c:set var="totalITLVehicle" value="${totalITLVehicle+1}"/>
                               </c:if>
                               <c:if test="${csList.tranSporter=='BABRA TPT'}">
                                    <c:set var="totalBABRAVehicle" value="${totalBABRAVehicle+1}"/>
                               </c:if>
                       <c:if test = "${tranSporter != ''}" >
                       <c:if test = "${tranSporter != csList.transporter}" >
                            
                             
                             <tr height="30">

                                <td align="center"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"><b><c:out value="${tranSporter}"/> Total  <c:out value="${totalDICTVehicle}"/></b></td>
                                <td align="left"><b><c:out value="${emptyTripKM}"/></b></td>
                                <td align="left"><b><c:out value="${loadedTrip}"/></b></td>
                                <td align="left"><b><c:out value="${emptyTripKM +loadedTrip}"/></b></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                 <c:set var="emptyTripKM" value="${0}"/>  
                                 <c:set var="loadedTrip" value="${0}"/>  
                            </tr>
                               </c:if> 
                               </c:if> 
                               <tr height="30">

                                <td align="center"><%=sno%></td>
                                <td align="left"><c:out value="${csList.regNo}"/></td>
                                <td align="left"><c:out value="${csList.vehicleTypeName}"/></td>
                                <td align="left">
                                <c:out value="${csList.transporter}"/> 
                                </td>
                                <td align="left"><c:out value="${csList.emptyTripKM}"/></td>
                                <td align="left"><c:out value="${csList.loadedTrip}"/></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                               
                                 <c:set var="tranSporter" value="${csList.transporter}"/>  
                                 <c:set var="emptyTripKM" value="${emptyTripKM + csList.emptyTripKM}"/>  
                                 <c:set var="loadedTrip" value="${loadedTrip + csList.loadedTrip}"/>  
                            </tr>
                               
                               
                            <%
                                       index++;
                                       sno++;
                            %>
                             
                        </c:forEach>
                           <tr height="30">

                                <td align="center"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"><b><c:out value="${tranSporter}"/> Total(
                                        <c:if test="${tranSporter=='DICT'}">
                                            <c:out value="${totalDICTVehicle}"/>
                                        </c:if>
                                        ) </b></td>
                                <td align="left"><b><c:out value="${emptyTripKM}"/></b></td>
                                <td align="left"><b><c:out value="${loadedTrip}"/></b></td>
                                <td align="left"><b><c:out value="${emptyTripKM +loadedTrip}"/></b></td>
                                <td align="left"></td>
                                <td align="left"></td>
                                <td align="left"></td>
                            </tr>
                    </tbody>
                </table>
                        <br/>
              <c:if test = "${vehicleVendorwiseList != null}" >
            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                <tr height="25" align="right">
                    <td style="background-color: #6374AB; color: #ffffff">Vehicle Type</td>
                    <td  style="background-color: #6374AB; color: #ffffff">No of Vehicles</td>
                    <td style="background-color: #6374AB; color: #ffffff">No.of Trips</td>
                    <td style="background-color: #6374AB; color: #ffffff">Avg. Trips Per Vehicle</td>
                </tr>


                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff"> 20"</td>
                    <td width="100" align="center"><c:out value="${totalTwentyFtVehicle}"/></td>
                    <td width="100" align="center"><fmt:formatNumber type="number"  value="${totalTwentyFtTrip}" /></td>
                    <td width="150" align="center"><fmt:formatNumber type="number" pattern="#" value="${totalTwentyFtTrip/totalTwentyFtVehicle}" /></td>
                </tr>


                <tr height="25" >
                    <td style="background-color: #6374AB; color: #ffffff"> 40"</td>
                    <td width="100" align="center"><c:out value="${totalFourtyFtVehicle}"/></td>
                    <td width="100" align="center"><fmt:formatNumber type="number"  value="${totalFourtyFtTrip}" /></td>
                    <td width="150" align="center"><fmt:formatNumber type="number" pattern="#" value="${totalFourtyFtTrip/totalFourtyFtVehicle}" /></td>
                </tr>

            </table>
            </c:if>
             </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>