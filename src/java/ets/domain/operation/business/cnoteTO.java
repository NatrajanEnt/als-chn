/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.operation.business;

/**
 *
 * @author srini
 */
public class cnoteTO {

    private String consignorPhone = "";
    private String productCategoryId = "";
    private String productCategory = "";
    private String reeferStatus = "";
    private String vehicleTypeId = "";
    private String totalKm = "";
    private String totalHm = "";
    private String routeContractId = "";

    private String customerName = "";
    private String customerCode = "";
    private String customerId = "";//////20-12-2013
    private String customerType = "";//////20-12-2013
    private String customerAddress = "";
    private String customerPhone = "";
    private String customerMobile = "";
    private String customerEmail = "";
    private String billingTypeId = "";
    private String contractId = "";

    private String originId = "";
    private String destinationId = "";
    private String point1Id = "";
    private String point2Id = "";
    private String point3Id = "";
    private String point4Id = "";



    private String customerRefNo = "";
    private String fromCity = "";
    private String errorMessage = "";
    private String iterimPoint1 = "";
    private String iterimPoint1Type = "";
    private String iterimPoint2 = "";
    private String iterimPoint2Type = "";
    private String iterimPoint3 = "";
    private String iterimPoint3Type = "";
    private String iterimPoint4 = "";
    private String iterimPoint4Type = "";
    private String toCity = "";
    private String pickupDate = "";
    private String pickupTime = "";
    private String consignorName = "";
    private String consignorAddress = "";
    private String consigneePhone = "";
    private String consigneeName = "";
    private String consigneeAddress = "";
    private String vehicleType = "";
    private String prodCategory = "";
    private String splRemark = "";
    private String customerNameStatus = "";
    private String customerCodeStatus = "";
    private String customerRefNoStatus = "";
    private String fromCityStatus = "";
    private String iterimPoint1Status = "";
    private String iterimPoint1TypeStatus = "";
    private String iterimPoint2Status = "";
    private String iterimPoint2TypeStatus = "";
    private String iterimPoint3Status = "";
    private String iterimPoint3TypeStatus = "";
    private String iterimPoint4Status = "";
    private String iterimPoint4TypeStatus = "";
    private String toCityStatus = "";
    private String pickupDateStatus = "";
    private String pickupTimeStatus = "";
    private String consignorNameStatus = "";
    private String consignorAddressStatus = "";
    private String consigneeNameStatus = "";
    private String consigneeAddressStatus = "";
    private String consigneePhoneStatus = "";
    private String vehicleTypeStatus = "";
    private String prodCategoryStatus = "";
    private String splRemarkStatus = "";

    public String getConsigneeAddress() {
        return consigneeAddress;
    }

    public void setConsigneeAddress(String consigneeAddress) {
        this.consigneeAddress = consigneeAddress;
    }

    public String getConsigneeName() {
        return consigneeName;
    }

    public void setConsigneeName(String consigneeName) {
        this.consigneeName = consigneeName;
    }

    public String getConsigneePhone() {
        return consigneePhone;
    }

    public void setConsigneePhone(String consigneePhone) {
        this.consigneePhone = consigneePhone;
    }

    public String getConsignorAddress() {
        return consignorAddress;
    }

    public void setConsignorAddress(String consignorAddress) {
        this.consignorAddress = consignorAddress;
    }

    public String getConsignorName() {
        return consignorName;
    }

    public void setConsignorName(String consignorName) {
        this.consignorName = consignorName;
    }

    public String getCustomerCode() {
        return customerCode;
    }

    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerRefNo() {
        return customerRefNo;
    }

    public void setCustomerRefNo(String customerRefNo) {
        this.customerRefNo = customerRefNo;
    }

    public String getFromCity() {
        return fromCity;
    }

    public void setFromCity(String fromCity) {
        this.fromCity = fromCity;
    }

    public String getIterimPoint1() {
        return iterimPoint1;
    }

    public void setIterimPoint1(String iterimPoint1) {
        this.iterimPoint1 = iterimPoint1;
    }

    public String getIterimPoint1Type() {
        return iterimPoint1Type;
    }

    public void setIterimPoint1Type(String iterimPoint1Type) {
        this.iterimPoint1Type = iterimPoint1Type;
    }

    public String getIterimPoint2() {
        return iterimPoint2;
    }

    public void setIterimPoint2(String iterimPoint2) {
        this.iterimPoint2 = iterimPoint2;
    }

    public String getIterimPoint2Type() {
        return iterimPoint2Type;
    }

    public void setIterimPoint2Type(String iterimPoint2Type) {
        this.iterimPoint2Type = iterimPoint2Type;
    }

    public String getIterimPoint3() {
        return iterimPoint3;
    }

    public void setIterimPoint3(String iterimPoint3) {
        this.iterimPoint3 = iterimPoint3;
    }

    public String getIterimPoint3Type() {
        return iterimPoint3Type;
    }

    public void setIterimPoint3Type(String iterimPoint3Type) {
        this.iterimPoint3Type = iterimPoint3Type;
    }

    public String getIterimPoint4() {
        return iterimPoint4;
    }

    public void setIterimPoint4(String iterimPoint4) {
        this.iterimPoint4 = iterimPoint4;
    }

    public String getIterimPoint4Type() {
        return iterimPoint4Type;
    }

    public void setIterimPoint4Type(String iterimPoint4Type) {
        this.iterimPoint4Type = iterimPoint4Type;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getProdCategory() {
        return prodCategory;
    }

    public void setProdCategory(String prodCategory) {
        this.prodCategory = prodCategory;
    }

    public String getSplRemark() {
        return splRemark;
    }

    public void setSplRemark(String splRemark) {
        this.splRemark = splRemark;
    }

    public String getToCity() {
        return toCity;
    }

    public void setToCity(String toCity) {
        this.toCity = toCity;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getConsigneeAddressStatus() {
        return consigneeAddressStatus;
    }

    public void setConsigneeAddressStatus(String consigneeAddressStatus) {
        this.consigneeAddressStatus = consigneeAddressStatus;
    }

    public String getConsigneeNameStatus() {
        return consigneeNameStatus;
    }

    public void setConsigneeNameStatus(String consigneeNameStatus) {
        this.consigneeNameStatus = consigneeNameStatus;
    }

    public String getConsigneePhoneStatus() {
        return consigneePhoneStatus;
    }

    public void setConsigneePhoneStatus(String consigneePhoneStatus) {
        this.consigneePhoneStatus = consigneePhoneStatus;
    }

    public String getConsignorAddressStatus() {
        return consignorAddressStatus;
    }

    public void setConsignorAddressStatus(String consignorAddressStatus) {
        this.consignorAddressStatus = consignorAddressStatus;
    }

    public String getConsignorNameStatus() {
        return consignorNameStatus;
    }

    public void setConsignorNameStatus(String consignorNameStatus) {
        this.consignorNameStatus = consignorNameStatus;
    }

    public String getCustomerCodeStatus() {
        return customerCodeStatus;
    }

    public void setCustomerCodeStatus(String customerCodeStatus) {
        this.customerCodeStatus = customerCodeStatus;
    }

    public String getCustomerNameStatus() {
        return customerNameStatus;
    }

    public void setCustomerNameStatus(String customerNameStatus) {
        this.customerNameStatus = customerNameStatus;
    }

    public String getCustomerRefNoStatus() {
        return customerRefNoStatus;
    }

    public void setCustomerRefNoStatus(String customerRefNoStatus) {
        this.customerRefNoStatus = customerRefNoStatus;
    }

    public String getFromCityStatus() {
        return fromCityStatus;
    }

    public void setFromCityStatus(String fromCityStatus) {
        this.fromCityStatus = fromCityStatus;
    }

    public String getIterimPoint1Status() {
        return iterimPoint1Status;
    }

    public void setIterimPoint1Status(String iterimPoint1Status) {
        this.iterimPoint1Status = iterimPoint1Status;
    }

    public String getIterimPoint1TypeStatus() {
        return iterimPoint1TypeStatus;
    }

    public void setIterimPoint1TypeStatus(String iterimPoint1TypeStatus) {
        this.iterimPoint1TypeStatus = iterimPoint1TypeStatus;
    }

    public String getIterimPoint2Status() {
        return iterimPoint2Status;
    }

    public void setIterimPoint2Status(String iterimPoint2Status) {
        this.iterimPoint2Status = iterimPoint2Status;
    }

    public String getIterimPoint2TypeStatus() {
        return iterimPoint2TypeStatus;
    }

    public void setIterimPoint2TypeStatus(String iterimPoint2TypeStatus) {
        this.iterimPoint2TypeStatus = iterimPoint2TypeStatus;
    }

    public String getIterimPoint3Status() {
        return iterimPoint3Status;
    }

    public void setIterimPoint3Status(String iterimPoint3Status) {
        this.iterimPoint3Status = iterimPoint3Status;
    }

    public String getIterimPoint3TypeStatus() {
        return iterimPoint3TypeStatus;
    }

    public void setIterimPoint3TypeStatus(String iterimPoint3TypeStatus) {
        this.iterimPoint3TypeStatus = iterimPoint3TypeStatus;
    }

    public String getIterimPoint4Status() {
        return iterimPoint4Status;
    }

    public void setIterimPoint4Status(String iterimPoint4Status) {
        this.iterimPoint4Status = iterimPoint4Status;
    }

    public String getIterimPoint4TypeStatus() {
        return iterimPoint4TypeStatus;
    }

    public void setIterimPoint4TypeStatus(String iterimPoint4TypeStatus) {
        this.iterimPoint4TypeStatus = iterimPoint4TypeStatus;
    }

    public String getPickupDateStatus() {
        return pickupDateStatus;
    }

    public void setPickupDateStatus(String pickupDateStatus) {
        this.pickupDateStatus = pickupDateStatus;
    }

    public String getPickupTimeStatus() {
        return pickupTimeStatus;
    }

    public void setPickupTimeStatus(String pickupTimeStatus) {
        this.pickupTimeStatus = pickupTimeStatus;
    }

    public String getProdCategoryStatus() {
        return prodCategoryStatus;
    }

    public void setProdCategoryStatus(String prodCategoryStatus) {
        this.prodCategoryStatus = prodCategoryStatus;
    }

    public String getSplRemarkStatus() {
        return splRemarkStatus;
    }

    public void setSplRemarkStatus(String splRemarkStatus) {
        this.splRemarkStatus = splRemarkStatus;
    }

    public String getToCityStatus() {
        return toCityStatus;
    }

    public void setToCityStatus(String toCityStatus) {
        this.toCityStatus = toCityStatus;
    }

    public String getVehicleTypeStatus() {
        return vehicleTypeStatus;
    }

    public void setVehicleTypeStatus(String vehicleTypeStatus) {
        this.vehicleTypeStatus = vehicleTypeStatus;
    }

    public String getBillingTypeId() {
        return billingTypeId;
    }

    public void setBillingTypeId(String billingTypeId) {
        this.billingTypeId = billingTypeId;
    }

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerMobile() {
        return customerMobile;
    }

    public void setCustomerMobile(String customerMobile) {
        this.customerMobile = customerMobile;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public String getReeferStatus() {
        return reeferStatus;
    }

    public void setReeferStatus(String reeferStatus) {
        this.reeferStatus = reeferStatus;
    }

    public String getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(String destinationId) {
        this.destinationId = destinationId;
    }

    public String getOriginId() {
        return originId;
    }

    public void setOriginId(String originId) {
        this.originId = originId;
    }

    public String getPoint1Id() {
        return point1Id;
    }

    public void setPoint1Id(String point1Id) {
        this.point1Id = point1Id;
    }

    public String getPoint2Id() {
        return point2Id;
    }

    public void setPoint2Id(String point2Id) {
        this.point2Id = point2Id;
    }

    public String getPoint3Id() {
        return point3Id;
    }

    public void setPoint3Id(String point3Id) {
        this.point3Id = point3Id;
    }

    public String getPoint4Id() {
        return point4Id;
    }

    public void setPoint4Id(String point4Id) {
        this.point4Id = point4Id;
    }

    public String getVehicleTypeId() {
        return vehicleTypeId;
    }

    public void setVehicleTypeId(String vehicleTypeId) {
        this.vehicleTypeId = vehicleTypeId;
    }

    public String getRouteContractId() {
        return routeContractId;
    }

    public void setRouteContractId(String routeContractId) {
        this.routeContractId = routeContractId;
    }

    public String getTotalHm() {
        return totalHm;
    }

    public void setTotalHm(String totalHm) {
        this.totalHm = totalHm;
    }

    public String getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(String totalKm) {
        this.totalKm = totalKm;
    }

    public String getConsignorPhone() {
        return consignorPhone;
    }

    public void setConsignorPhone(String consignorPhone) {
        this.consignorPhone = consignorPhone;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public String getProductCategoryId() {
        return productCategoryId;
    }

    public void setProductCategoryId(String productCategoryId) {
        this.productCategoryId = productCategoryId;
    }


    
    
}
