package ets.domain.sales.web;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.ParamPart;
import com.oreilly.servlet.multipart.Part;
import ets.arch.web.BaseController;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.text.*;
import java.util.*;
import java.text.ParseException;
import ets.domain.users.business.LoginBP;
import ets.domain.operation.business.OperationBP;
import ets.domain.operation.business.OperationTO;
import ets.domain.sales.business.SalesBP;
import ets.domain.sales.business.SalesTO;
import ets.domain.util.FPLogUtils;
import org.springframework.web.bind.ServletRequestDataBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.web.servlet.ModelAndView;
import java.util.Iterator;
import java.util.ArrayList;
import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.operation.web.OperationCommand;
import ets.domain.sales.business.SalesCommand;
import ets.domain.util.ParveenErrorConstants;
import ets.domain.users.web.CryptoLibrary;
import ets.domain.util.ThrottleConstants;
import java.io.*;
import java.util.Date;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import ets.domain.secondaryOperation.business.SecondaryOperationBP;
import ets.domain.secondaryOperation.business.SecondaryOperationTO;

public class SalesController extends BaseController {

    SalesBP salesBP;
    OperationBP operationBP;
    OperationCommand operationCommand;
    SecondaryOperationBP secondaryOperationBP;

    public SalesBP getSalesBP() {
        return salesBP;
    }

    public void setSalesBP(SalesBP salesBP) {
        this.salesBP = salesBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public OperationCommand getOperationCommand() {
        return operationCommand;
    }

    public void setOperationCommand(OperationCommand operationCommand) {
        this.operationCommand = operationCommand;
    }

    public SecondaryOperationBP getSecondaryOperationBP() {
        return secondaryOperationBP;
    }

    public void setSecondaryOperationBP(SecondaryOperationBP secondaryOperationBP) {
        this.secondaryOperationBP = secondaryOperationBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        initialize(request);

    }

    public ModelAndView pnrUploadPage(HttpServletRequest request, HttpServletResponse reponse, SalesCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Stores >>Manage Racks  >> Add Rack ";
        String pageTitle = "Add Racks";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        try {
            setLocale(request, reponse);
            path = "content/sales/pnrImport.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Add Rack --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView handleUploadDATfile(HttpServletRequest request, HttpServletResponse response, SalesCommand command) throws IOException, FPBusinessException, ParseException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/LoginForward.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        String pageTitle = "";
        String DatFile = "";
        String pnrNo = "";
        String portNo = "";
        String pnrDate = "";
        String pnrDates = "";
        int status = 0;

        int userId = (Integer) session.getAttribute("userId");
        String newFileName = "", actualFilePath = "";
        String tempServerFilePath = "", actualServerFilePath = "", fileSavedAs = "", fileName = "";
        Part partObj = null;
        FilePart fPart = null;
        int j = 0;
        String[] fileSaved = new String[10];
        String[] uploadedFileName = new String[10];
        String[] tempFilePath = new String[10];
        Part _part = null;

        SalesTO salesTO = new SalesTO();
        try {
            menuPath = "Operation >>Upload Trip Planning ";
            path = "BrattleFoods/tripPlanningImport.jsp";
            pageTitle = "Upload DAT File ";
            request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);

            request.setAttribute("pageTitle", pageTitle);
            String sub = "";
            String planDate = "";
            String filename[] = new String[10];
            String fileSavedAs1[] = new String[10];
            String filePath = "";
            boolean isMultipart = ServletFileUpload.isMultipartContent(request);
            if (isMultipart) {
                filePath = getServletContext().getRealPath("/");
                String filePath1 = filePath.replace("\\", "\\\\");
                //////System.out.println("File path.." + filePath1);
                Date now = new Date();
                String date = now.getDate() + "" + now.getMonth() + "" + (now.getYear() + 1900);
                String time = now.getHours() + "" + now.getMinutes() + "" + now.getSeconds();
                MultipartParser parser = new MultipartParser(request, 10 * 1024 * 1024);
                while ((_part = parser.readNextPart()) != null) {
                    if (_part.isParam()) {
                        ParamPart paramPart = (ParamPart) _part;
                        String value = paramPart.getStringValue();
                        if (_part.getName().equals("planDate")) {
                            planDate = value;
                        }
                    } else if (_part.isFile()) {

                        fPart = (FilePart) _part;

                        //////System.out.println("fileName=" + fPart.getFileName());
                        filename[j] = fPart.getFileName();

                        if (!"".equals(filename[j]) && filename[j] != null) {
                            //////System.out.println("filename..[" + j + "]..in string array.." + filename[j]);
//                                String[] fp = filename[j].split("\\.");
                            String[] fp = filename[j].split("\\.");
                            fileSavedAs = fp[0] + date + time + ".";
                            fileSavedAs1[j] = fp[0] + date + time + "." + fp[1];
                            String tempPath = filePath1 + "/" + fileSavedAs1[j];
                            //////System.out.println("tempPath" + tempPath);
                            String actPath = filePath + "/" + filename[j];
                            //////System.out.println("actPath" + actPath);
                            long fileSize = fPart.writeTo(new java.io.File(filePath));
                            File f1 = new File(actPath);
                            f1.renameTo(new File(tempPath));

                        }
                        j++;
                    }
                }
            }

            filePath = filePath + fileSavedAs1[0];
            System.out.println("filePath====" + filePath);
            BufferedReader br = new BufferedReader(new FileReader(filePath));

            String sCurrentLine;
            int count = 0;
            while ((sCurrentLine = br.readLine()) != null) {
                count++;
                System.out.println(sCurrentLine);
                String[] temp = null;

                if (sCurrentLine != null) {
                    String tmp = sCurrentLine.replace("'", "");
                    System.out.println("tmpo===" + tmp);
                    temp = tmp.split("~");
                    salesTO.setPnrId(temp[0]);
                    salesTO.setPnrNo(temp[1]);
                    pnrNo = temp[1];
                    portNo = temp[22];
                    pnrDate = temp[2];
                    salesTO.setCreated(temp[2]);
                    salesTO.setLinearName(temp[3]);
                    salesTO.setField1(temp[4]);
                    salesTO.setField2(temp[5]);
                    salesTO.setField3(temp[6]);
                    salesTO.setField4(temp[7]);
                    salesTO.setConsignee(temp[8]);
                    salesTO.setField5(temp[9]);
                    salesTO.setField6(temp[10]);
                    salesTO.setpName(temp[11]);
                    salesTO.setField7(temp[12]);
                    salesTO.setField8(temp[13]);
                    salesTO.setField9(temp[14]);
                    salesTO.setConNo(temp[15]);
                    salesTO.setField10(temp[16]);
                    salesTO.setField11(temp[17]);
                    salesTO.setLoadCap(temp[18]);
                    salesTO.setConType(temp[19]);
                    salesTO.setTarewgt(temp[20]);
                    salesTO.setField12(temp[21]);
                    salesTO.setField13(temp[22]);

                    salesTO.setUserId(userId);

                }
                System.out.println("controller Here.. ");

                int pnrCount = 0;
                System.out.println("count:" + count);
                if (count == 1) {
                    pnrCount = salesBP.getPNRCount(salesTO.getPnrNo());
                    request.setAttribute("pnrCount", pnrCount);
                    System.out.println("pnrCount:" + pnrCount);
                }

                if (pnrCount > 0) {
                    request.setAttribute("errorMessage", "PNR No " + pnrNo + " already processed!!");
                    path = "content/sales/pnrImport.jsp";
                    return new ModelAndView(path);
                } else {
                    System.out.println("insert========");
                    status = salesBP.processPnrDatFile(salesTO, pnrNo, portNo, userId);
                    path = "content/sales/pnrImport.jsp";

                }

            }
//
            DateFormat df = new SimpleDateFormat("MMM dd yyyy hh:mma");
            Date startDate1 = df.parse(pnrDate);

            System.out.println(startDate1);
            System.out.println(startDate1.toString());

            DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

            String reportDate = df1.format(startDate1);
            request.setAttribute("pnrDates==", pnrDates);

            System.out.println("reportDate=" + reportDate);

            int pnrCNoteStatus = salesBP.saveConsignmentNote(pnrNo, portNo, reportDate, userId);

            request.setAttribute("status", status);
            if (status != 0 && pnrCNoteStatus != 0) {
                request.setAttribute("successMessage", "PNR Data Uploaded Successfully with PNR " + pnrNo);
            } else {
                if (status == 0) {
                    request.setAttribute("errorMessage", "Error: Issue with PNR File format / data for PNR No " + pnrNo);
                } else if (pnrCNoteStatus == 0) {
                    request.setAttribute("errorMessage", "Error: Customer Route Contract Does Not Exists for PNR No " + pnrNo);
                }
            }

            //process the pnr data into cnote end 
        } catch (FPRuntimeException exception) {
            System.out.println("error occured......");
            exception.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to upload pnrdata  file --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);

    }

    public ModelAndView handlevehicleProjection(HttpServletRequest request, HttpServletResponse reponse, SalesCommand command) throws IOException {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        HttpSession session = request.getSession();
        String path = "";
        String menuPath = "";
        menuPath = "Stores >>Manage Racks  >> Add Rack ";
        String pageTitle = "Add Racks";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        SalesTO salesTO = new SalesTO();
        try {
            ArrayList vehicleAvailability = new ArrayList();
            vehicleAvailability = salesBP.getVehicleAvailability(salesTO);
            request.setAttribute("vehicleAvailability", vehicleAvailability);
            System.out.println("vehicleAvailability in cont = " + vehicleAvailability.size());

            ArrayList PNRprojection = new ArrayList();
            PNRprojection = salesBP.getPNRprojection(salesTO);
            request.setAttribute("PNRprojection", PNRprojection);
            System.out.println("PNRprojection in cont = " + PNRprojection.size());

            setLocale(request, reponse);
            path = "content/sales/vehicleProjection.jsp";

        } catch (FPRuntimeException exception) {
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/comon/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to view Add Rack --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView cNoteLiveSummary(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        operationCommand = command;
        String menuPath = "";
        OperationTO operationTO = new OperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String roleId = "" + (Integer) session.getAttribute("RoleId");
        int userId = (Integer) session.getAttribute("userId");
        String compId = (String) session.getAttribute("companyId");
        operationTO.setRoleId(roleId);
        operationTO.setUserId(userId);
        operationTO.setCompId(Integer.parseInt(compId));
        operationTO.setCompanyId(compId);
        System.out.println("11111111111111111111111111");

        try {
            String pageType = request.getParameter("pageType");
            System.out.println("page Type:" + pageType);
            request.setAttribute("pageType", pageType);

            String flag = request.getParameter("flag");
            System.out.println("flag Type:" + flag);
            request.setAttribute("flag", flag);

            ArrayList consignmentListPendingSummary = operationBP.getConsignmentViewListSummary(operationTO);
            System.out.println("consignmentListPendingSummary: " + consignmentListPendingSummary.size());
            request.setAttribute("consignmentListPendingSummary", consignmentListPendingSummary);

            ArrayList consignmentListViewCustomerSummary = operationBP.consignmentListViewCustomerSummary(operationTO);
            System.out.println("consignmentListViewCustomerSummary: " + consignmentListViewCustomerSummary.size());
            request.setAttribute("consignmentListViewCustomerSummary", consignmentListViewCustomerSummary);

            ArrayList consignmentListViewCustomerPreviousSummary = operationBP.consignmentListViewCustomerPreviousSummary(operationTO);
            System.out.println("consignmentListViewCustomerPreviousSummary: " + consignmentListViewCustomerPreviousSummary.size());
            request.setAttribute("consignmentListViewCustomerPreviousSummary", consignmentListViewCustomerPreviousSummary);

            ArrayList consignmentListLiveSummary = operationBP.consignmentListLiveSummary(operationTO);
            System.out.println("consignmentListLiveSummary: " + consignmentListLiveSummary.size());
            request.setAttribute("consignmentListLiveSummary", consignmentListLiveSummary);
            System.out.println("pageType---" + pageType);
            System.out.println("flag---" + flag);
            if ("1".equals(pageType)) {
                if ("1".equals(flag)) {
                    path = "content/trip/cNoteLiveSummaryExcel.jsp";
                } else {
                    path = "content/trip/cNoteLiveSummary.jsp";
                }

            } else {
                if ("1".equals(flag)) {
                    path = "content/trip/cNotePreviousSummaryExcel.jsp";
                } else {
                    path = "content/trip/cNotePreviousSummary.jsp";
                }

            }

        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

    public ModelAndView consignmentOrderStatus(HttpServletRequest request, HttpServletResponse response, OperationCommand command) {
        if (request.getSession().isNew()) {
            return new ModelAndView("content/common/login.jsp");
        }
        String path = "";
        HttpSession session = request.getSession();
        operationCommand = command;
        String menuPath = "";
        SecondaryOperationTO operationTO = new SecondaryOperationTO();
        String pageTitle = "View Route Details";
        menuPath = "Operation >>  Route Details";
        request.setAttribute("pageTitle", pageTitle);
        request.setAttribute(ParveenErrorConstants.PATH_KEY, menuPath);
        String roleId = "" + (Integer) session.getAttribute("RoleId");
        int userId = (Integer) session.getAttribute("userId");
        String compId = (String) session.getAttribute("companyId");
        operationTO.setRoleId(roleId);
        operationTO.setUserId(userId);
        operationTO.setCompId(compId);
        operationTO.setCompanyId(compId);

        try {
             Date dNow = new Date();
            Calendar cal = Calendar.getInstance();
            cal.setTime(dNow);
            cal.add(Calendar.DATE, 0);
            dNow = cal.getTime();

            Date dNow1 = new Date();
            Calendar cal1 = Calendar.getInstance();
            cal1.setTime(dNow1);
            cal1.add(Calendar.DATE, -6);
            dNow1 = cal1.getTime();

            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
            String today = ft.format(dNow);
            String fromday = ft.format(dNow1);
            System.out.println("fromday:::"+fromday);
            System.out.println("today:::"+today);
            
            if (operationCommand.getFromDate() != null && operationCommand.getFromDate() != "") {
                operationTO.setFromDate(operationCommand.getFromDate());
                request.setAttribute("fromDate", operationCommand.getFromDate());
            } else {
                operationTO.setFromDate(fromday);
                request.setAttribute("fromDate", fromday);
            }

            if (operationCommand.getToDate() != null && operationCommand.getToDate() != "") {
                operationTO.setToDate(operationCommand.getToDate());
                request.setAttribute("toDate", operationCommand.getToDate());
            } else {
                operationTO.setToDate(today);
                request.setAttribute("toDate", today);
            }
            String orderStatus = request.getParameter("orderStatus");
            request.setAttribute("orderStatus", orderStatus);
            operationTO.setOrderStatus(orderStatus); 
            String pageType = request.getParameter("pageType");
            request.setAttribute("pageType", pageType);
            operationTO.setPageType(pageType);

            ArrayList orderList = secondaryOperationBP.getConsignmentContainerList(operationTO);
            System.out.println("orderList: " + orderList.size());
            request.setAttribute("orderList", orderList);

            path = "content/trip/cNoteOrderStatus.jsp";

        } catch (FPRuntimeException exception) {
            exception.printStackTrace();
            /*
             * run time exception has occurred. Directed to error page.
             */
            FPLogUtils.fpErrorLog("Run time exception --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        } catch (Exception exception) {
            exception.printStackTrace();
            FPLogUtils.fpErrorLog("Failed to View ProductList --> " + exception);
            return new ModelAndView("content/common/error.jsp");
        }
        return new ModelAndView(path);
    }

}
