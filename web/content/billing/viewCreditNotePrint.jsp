<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <!--        <script type="text/javascript" src="/throttle/js/suest"></script>
                <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
                <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
                <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
                <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

        <!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
                <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
                <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>




        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"><!--For Currency Symbol -->

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

       
<!--        <style>
        body {
            background-image: url("img_tree.png");
            background-repeat: no-repeat;
            background-position: right top;
            margin-right: 200px;
        }
        </style>-->
        
        <script type="text/javascript">
            var currencyCode = 'INR';
            function checkInvoiceNo() {
                var invoiceNo = document.trip.invoiceNo.value;
                if (invoiceNo == '' || invoiceNo == '0') {
                    alert('please enter invoice No');
                    document.trip.invoiceNo.focus();
                    return false;
                } else {
                    return true;
                }
            }
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function () {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
            $(document).ready(function () {
                $("#tabs").tabs();
            });

            function submitOrderBill(invoiceId) {
                $("#SubmitBill").hide();
                document.trip.action = "/throttle/submitOrderBill.do?invoiceId="+invoiceId;
                document.trip.submit();

            }
             function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
                 }
        </script>
    </head>
    <body  onload="checkSpace();">
        <form name="trip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%int sno = 1;%>
            <div id="tabs" >
                <ul>
<!--                    <li><a href="#tripDetail"><span>Billing Trip Details</span></a></li>-->
                    <li><a href="#invoiceDetail"><span>Billing Invoice Details</span></a></li>
                </ul>
                <c:set var="totalRevenue" value="0" />
                <c:set var="totalExpToBeBilled" value="0" />


      <%--          <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" >Sno</td>
                            <td class="contenthead" >Customer</td>
                            <td class="contenthead" >C Note</td>
                            <td class="contenthead" >Trip Code</td>
                            <td class="contenthead" >InvoiceCode</td>
                            <td class="contenthead" >FreightAmount</td>
                            <td class="contenthead" >OtherExp</td>
                            <td class="contenthead" >TotalAmount</td>
                            <td class="contenthead" >PodStatus</td>
                        </tr>
                        <% int submissionStatus = 0 ;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <tr>
                                    <td class="text1" ><%=sno%></td>
                                    <td class="text1" ><c:out value="${trip.customerName}"/></td>
                                    <td class="text1" ><c:out value="${trip.consignmentNoteNo}"/></td>
                                    <td class="text1" ><c:out value="${trip.tripCode}"/></td>
                                    <td class="text1" ><c:out value="${trip.invoiceCode}"/></td>
                                    <td class="text1"  align="right"><c:out value="${trip.freightAmount}"/></td>
                                    <td class="text1"  align="right"><c:out value="${trip.otherExpenseAmount}"/></td>
                                    <td class="text1"  align="right"><c:out value="${trip.totalAmount}"/></td>
                                    <td class="text1"  align="right">
                                    <c:if test="${trip.podCount == 1}">
                                        <a href="viewTripPod.do?tripSheetId=<c:out value="${trip.tripId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                    </c:if>
                                    <c:if test="${trip.podCount == 0}">
                                        <a href="viewTripPod.do?tripSheetId=<c:out value="${trip.tripId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                    </c:if>
                                    </td>
                                    <c:if test="${trip.podCount > 0}">
                                        <%submissionStatus = 1 ;%>

                                    </c:if>

                                <%sno++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br>
                    <center>
                        <% if(submissionStatus == 1){ %>
                        <c:if test="${submitStatus == 0}">
                        <input type="button" class="button" value="SubmitBill" id="SubmitBill" name="Next" onclick="submitOrderBill('<c:out value="${invoiceId}"/>');" />
                        </c:if>
                        <%}else{%>
                        Waiting for POD Upload, we cant submit bill.
                        <%}%>
                    </center>
                </div>  --%>
                <div id="invoiceDetail" >
                    <div id="print" >
                        <c:forEach  items="${invoiceDetailsList}" var="invoiceDetailsList">  
                            <c:set var="activeInd"  value="${invoiceDetailsList.activeInd}"/>
                      </c:forEach>
                        <%--<c:if test="${activeInd == N}">--%>
                       <table  align="center" cellpadding="0" cellspacing="0" border="2"  rules="all" frame="hsides" 
                                style="border-collapse: collapse;width:1000px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;height:800px;border-left: none;border-right: none;">     
                         <%--</c:if>--%>   
                           
                           <tr>
                <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" colspan="3" >
                    <table align="left" >
                        <tr>
                            <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                            <td style="padding-left: 75px; padding-right:42px;">
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        <font size="3"><b>International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</b></font><br>
                        <center> <font size="2" style="padding-left: 85px" >  (Formely Known As Boxtrans Logistics (India) Services Pvt Ltd.)</font><br>
                           <font size="2" style="padding-left: 85px" >   Panchi Gujran,Tehsil-Ganaur, Dist-Sonepat-131101<br>
                             </font>
                            <font size="2" style="padding-left: 85px">CIN No:U63040MH2006PTC159885</font>
                        </center>
                </td>
                <td></td>
                </tr>
                </table>
                </td>
                </tr>

                            <tr>
<!--                                <td height="60" align="left" style="border-right:none;"><img src="images/Dict_logo.png" width="100" height="50"/></td>-->
                               
                                <c:if test="${paymentMode == 'Credit'}">
                                <td  height="30" colspan="4" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse">        <center><h3><font size="3"><b>CREDIT NOTE INVOICE</b></font></h3></center>
                                </td>
                                </c:if>
                                 <c:if test="${paymentMode == 'Debit'}">
                                <td  height="30" colspan="4" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse">        <center><h3><font size="3"><b>DEBIT NOTE INVOICE</b></font></h3></center>
                                </td>
                                </c:if>

                            </tr>
                            <tr>
                                <td style="width: 460px;">
                                    <table width="100%">

<!--                                        <tr>
                                            <td style="border-bottom-style: solid;border-right-style: solid;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                                                <font size="2">Billed to:</font>
                                            </td>
                                        </tr>-->
                                        <tr>
                                            <td style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                                             <font size="2">Billed to:-</font> <br>

                                              <font size="2">  <b><c:out value="${billingParty}"/></b></font><br>
                                               <font size="2">  <c:out value="${customerAddress}"/></font><br>
<!--                                             <font size="2">    <c:out value="${cityName}"/>,</font><br>
                                                 <font size="2">   <c:out value="${state}"/> </font>-->
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                <td style="width: 260px;">
                                    <table width="100%" >


                                                 <tr>
                                                        <td style="border-bottom-style: solid;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;text-align:center;padding-bottom:6px;">
                                                             
                                                            <font size="2"><B><c:if test="${paymentMode == 'Credit'}">Credit Note No:</c:if><c:if test="${paymentMode == 'Debit'}">Debit Note No:</c:if><c:out value="${creditNoteCode}"/></B></font></td>
                                                       
                                        </tr>
                                        <tr>
                                            <td style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;text-align:left;">
                                            <font size="2" style="padding-left: 15px;"><b><c:out value="${billingType}"/></b></font>
                                            </td>
                                        </tr>


                                    </table>
                      <%--</c:forEach>--%>
                                </td>
                                    <td style="width: 260px;">
                                        <table width="100%" >


                                                 <tr>
                                            <td style="border-bottom-style: solid;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 6px;text-align:left;">
                                                <b> <font size="2">Bill Date:<c:out value="${billDate}"/></font> </b></td>
                                              </tr>
                                                 <tr>
                                            <td style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;text-align:left;">
                                                <b>  <font size="2">Ref.Invoice No:&nbsp;<c:out value="${invoicecode}"/></font></b>
                                            </td>
                                        </tr>
                                        </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="30"  colspan="3"><font size="2"><center><b>PARTICULARS</b></center></font></td>
                                <!--<td height="30" style="border-bottom-style: none;border-right-color:#000000;border-right-width: 1px;padding: 0px;" colspan="3"><font size="2"><center><b>PARTICULARS</b></center></font></td>-->
                            </tr>
                            <c:if test="${invoiceDetailsList != null}">
                           <tr  >
                                <td height="60" colspan="4" >
                                  <table border=1  >
                                        <tr>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Date</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 340px;">Description</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">No.Of Container</th>
                                            <c:if test="${paymentMode == 'Credit'}">
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Credit Amount(Rs)</th>
                                            </c:if>
                                            <c:if test="${paymentMode == 'Debit'}">
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Debit Amount(Rs)</th>
                                            </c:if>

                                        </tr>
                                         <% int index = 0;%>
                                        <c:set var="totalFreightCharge" value="0.00"/>
                                        <c:set var="totalFreightChargeOfRow" value="0.00"/>
                                        <c:set var="totalContainer" value="0"/>


                                           <c:forEach begin="0" end="16" items="${invoiceDetailsList}" var="invoiceDetailsList">

                                           <tr>
                                           <!--<font size="2">-->

                                           <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${invoiceDate}"/></td>
                                            <td  height="30" style="font-size: 12px;width: 340px;text-align: center;"><c:out value="${remarks}"/></td>
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"></td>
                                            <c:if test="${paymentMode == 'Credit'}">
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${creditNoteAmt}"/></td>
                                            </c:if>
                                            <c:if test="${paymentMode == 'Debit'}">
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${debitNoteAmt}"/></td>
                                            </c:if>
                                   

                                        </tr>
                                        <c:set var="totalFreightChargeOfRow" value="0"/>
                                            <%index++;%>
                                        </c:forEach>
                                         <input type="hidden" name="count1" id="count1" value="<%=index%>"/>

                                        </table>
                                </td>

                            </tr>
                            
                            
                            <tr style="border-left: 0px;border-right: 0px;display: none;border-bottom: none;border-top:  none;" id="breakOne">
                                <td height="150" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                               <table align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">
                       
                        <tr style="border:none">
                            <td style="border:none">
                                <br>
                             <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>


                            </td>
                        </tr>
                    </table>
                                    </td>
                        </tr>


                            <tr id="tableTwo">
                               <td height="150" colspan="4">
                                     <table border=1 >
                                        <tr id="headerOne">
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Date</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 340px;">Description</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">No.Of Container</th>
                                             <c:if test="${paymentMode == 'Credit'}">
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Credit Amount(Rs)</th>
                                            </c:if>
                                            <c:if test="${paymentMode == 'Debit'}">
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Debit Amount(Rs)</th>
                                            </c:if>

                                        </tr>

                                           <% int index1 = 20;%>
                                         <c:forEach begin="17" end="39" items="${invoiceDetailsList}" var="invoiceDetailsList">
                                            <%--<c:forEach items="${invoiceDetailsList}" var="invoiceDetailsList" >--%>
                                           <tr>
                                           <!--<font size="2">-->

                                          <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${invoiceDate}"/></td>
                                            <td  height="30" style="font-size: 12px;width: 340px;text-align: center;"><c:out value="${remarks}"/></td>
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"></td>
                                            <c:if test="${paymentMode == 'Credit'}">
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${creditNoteAmt}"/></td>
                                            </c:if>
                                            <c:if test="${paymentMode == 'Debit'}">
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${debitNoteAmt}"/></td>
                                            </c:if>

                                        </tr>

                                        <c:set var="totalFreightChargeOfRow" value="0"/>
                                            <%index1++;%>
                                        </c:forEach>
                                        <!--<input type="text" name="count2" id="count2" value="<%=index1%>"/>-->

                                    </table>
                                </td>
                            </tr>
<!--                            <tr style="border-left: 0px;border-right: 0px;display: none;" id="breakTwo">
                                <td height="150" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                               <table align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">
                        <tr style="border:none">
                            <td style="border:none">
                                  <%--<c:if test="${index1 <= 34}">--%>
                                <br>
                                <br>
                                  <%--</c:if>--%>
                            </td>
                        </tr>
                    </table>
                                    </td>
                        </tr>-->
                                  <tr style="border-left: 0px;border-right: 0px;display: none;border-bottom: none;border-top:  none;" id="breakTwo">
                                <td height="150" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                               <table border=1  align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">
                       
                        <tr style="border:none">
                            <td style="border:none">
                                <br>
                             <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>

                            </td>
                        </tr>
                    </table>
                                    </td>
                        </tr>
                                  <tr id="tableThree">
                                    <td height="150" colspan="4">
                                       <table border=1 >
                                          <tr id="headerTwo">
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Date</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 340px;">Description</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">No.Of Container</th>
                                            <c:if test="${paymentMode == 'Credit'}">
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Credit Amount(Rs)</th>
                                            </c:if>
                                            <c:if test="${paymentMode == 'Debit'}">
                                            <th height="30" style="font-size: 12px;text-align: center;width: 220px;">Debit Amount(Rs)</th>
                                            </c:if>

                                        </tr>
                                           <% int index2 = 40;%>
                                         <c:forEach begin="40" end="59" items="${invoiceDetailsList}" var="invoiceDetailsList">
                                            <%--<c:forEach items="${invoiceDetailsList}" var="invoiceDetailsList" >--%>
                                           <tr>
                                           <!--<font size="2">-->

                                           <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${invoiceDate}"/></td>
                                            <td  height="30" style="font-size: 12px;width: 340px;text-align: center;"><c:out value="${remarks}"/></td>
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"></td>
                                            <c:if test="${paymentMode == 'Credit'}">
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${creditNoteAmt}"/></td>
                                            </c:if>
                                            <c:if test="${paymentMode == 'Debit'}">
                                            <td  height="30" style="font-size: 12px;width: 220px;text-align: center;"><c:out value="${debitNoteAmt}"/></td>
                                            </c:if>

                                        </tr>
                                        <c:set var="totalFreightChargeOfRow" value="0"/>
                                            <%index2++;%>
                                        </c:forEach>
                                        <!--<input type="text" name="count2" id="count2" value="<%=index2%>"/>-->

                                    </table>
                                </td>
                            </tr>


                            <tr id="tableTotal">
                               <td height="30" colspan="4">
                                  <table border=1 >
<!--                                        <tr  id="headerTwo">
                                              <th height="30" style="font-size: 12px;text-align: center;width: 48px;">GR.No</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 65px;">Date</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 275px;">Description</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 89px;">No.Of Container</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 67px;">Commodity</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 55px;">Freight</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 59px;">Toll Tax</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 92px;">Detention Charges</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 70px;">Weightment</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 79px;">Other Charges</th>
                                            <th height="30" style="font-size: 12px;text-align: center;width: 98px;">Total Amount(Rs.)</th>

                                        </tr>-->
<!--                                        <tr>
                                            <td height="30" style="width: 48px;"></td>
                                            <td height="30" style="width: 65px;"></td>
                                            <td height="30" style="font-size: 12px;text-align: center;width:275px;"><c:out value="${firstRemarks}"/></td>
                                            <td height="30" style="width: 89px;"></td>
                                            <td height="30" style="width: 67px;"></td>
                                            <td height="30" style="width: 55px;"></td>
                                            <td height="30" style="width: 59px;"></td>
                                            <td height="30" style="width: 92px;"></td>
                                            <td height="30" style="width: 70px;"></td>
                                            <td height="30" style="width: 79px;"></td>
                                            <td height="30" style="width: 98px;"></td>
                                        </tr>
                                        <tr>
                                            <td height="30" style="width: 48px;"></td>
                                            <td height="30" style="width: 65px;"></td>
                                            <td height="30" style="font-size: 12px;text-align: center;width:275px;"><c:out value="${secondRemarks}"/></td>
                                            <td height="30" style="width: 89px;"></td>
                                            <td height="30" style="width: 67px;"></td>
                                            <td height="30" style="width: 55px;"></td>
                                            <td height="30" style="width: 59px;"></td>
                                            <td height="30" style="width: 92px;"></td>
                                            <td height="30" style="width: 70px;"></td>
                                            <td height="30" style="width: 79px;"></td>
                                            <td height="30" style="width: 98px;"></td>
                                        </tr>
                                        <tr>
                                             <td height="30" style="width: 48px;"></td>
                                            <td height="30" style="width: 65px;"></td>
                                            <td height="30" style="font-size: 12px;text-align: center;width:275px;"><c:out value="${thirdRemarks}"/></td>
                                            <td height="30" style="width: 89px;"></td>
                                            <td height="30" style="width: 67px;"></td>
                                            <td height="30" style="width: 55px;"></td>
                                            <td height="30" style="width: 59px;"></td>
                                            <td height="30" style="width: 92px;"></td>
                                            <td height="30" style="width: 70px;"></td>
                                            <td height="30" style="width: 79px;"></td>
                                            <td height="30" style="width: 98px;"></td>
                                        </tr>-->
                                         <c:set var="taxableAmount" value='${(totalFreightCharge*30)/100}'/>
                                         <c:set var="serviceTaxAmount" value='${(taxableAmount*14)/100}'/>
                                        <c:set var="abatement" value='${totalFreightCharge - taxableAmount}'/>
                                        <c:set var="swachhaBharat" value='${(taxableAmount*0.5)/100}'/>
                                        <c:set var="test12" value="12000"/>


                                        <tr id="firstRemarks">

                                            <td  height="30" style="width: 220px; " ></td>
                                            <td  height="30" style="width: 340px;">
<!--                                                 <span id="freightAmountSpan"></span>-->

                                            </td>
                                            <td  height="30" style="font-size: 12px;width: 275px;">

                                                <c:out value="${firstRemarks}"/>
                                            </td>
                                            <td  height="30" style="width:220px;"></td>
                                            <td  height="30" style="width:340px;"></td>
                                            <td  height="30" style="width:220px;"></td>
                                            <td  height="30" style="width:220px;"></td>
                                            <td  height="30" style="width:220px;"></td>
                                         
                                           </td>
                                        </tr>

                                         <tr id="secondRemarks">

                                            <td  height="30" style="width: 220px; " ></td>
                                            <td  height="30" style="width: 340px;">
<!--                                                 <span id="freightAmountSpan"></span>-->

                                            </td>
                                            <td  height="30" style="font-size: 12px;width: 220px;">
                                          <c:out value="${secondRemarks}"/>

                                            </td>
                                            <td  height="30" style="width:220px;"></td>
                                            <td  height="30" style="width:340px;"></td>
                                            <td  height="30" style="width:220px;"></td>
                                            <td  height="30" style="width:220px;"></td>
                                            <td  height="30" style="width:220px;"></td>
                                         
                                           </td>
                                        </tr>

                                        <tr>

                                            <!--<td  height="30" style="text-align:right;width: 220px;">-->
<!--                                                 <span id="freightAmountSpan"></span>-->
                                        <script>
                                            var freightAmount = '<c:out value="${totalFreightCharge}"/>';
                                             var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                            $("#freightAmountSpan").text(numberWithCommas(formattedAmount));

//                                            $("#freightAmountSpan").text(parseFloat(9000.0).toFixed(2));
                                        </script></td>
<!--                                            <td  height="30" style="width:340px;">


                                            </td>
                                            <td  height="30" style="width:220px;text-align: center;"><c:out value="${totalContainer}"/></td>
                                          
                                            <td  height="30" style="text-align:right;width:220px;">
                                            <td  height="30" style="text-align:right;width:220px;">-->
                                          <script>
                                            var freightAmount = '<c:out value="${totalFreightCharge}"/>';
                                            var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                            $("#totFreightAmountSpan").text(numberWithCommas(formattedAmount));
//
                                          </script>
                                           </td>
                                        </tr>
                                   </table>

                                </td>
                            </tr>
                            </c:if>
                            <tr style="border-left: 0px;border-right: 0px;display: none;" id="remarksBreak">
                                <td height="10" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                               <table border=0 rules=ALL FRAME=VOID align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">
<!--                            <td colspan="1"  id="displayrow11" align="right" style="margin-right: 190px;border:none;" border="0"><b>Page 01 of 02</b></td>
                            <td colspan="1"  id="displayrow12" align="right" style="margin-right: 190px;border:none;" border="0"><b>Page 01 of 03</b></td>-->
<!--                        <tr style="border:none">
                            <td style="border:none">
                                <br>
                            <br><br>
                            <br><br>
                            <br><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>



                            </td> -->
                        <!--</tr>-->
                    </table>
                                    </td>
                        </tr>


<!--                        <tr id="teableThree">
                               <td height="" colspan="">
                                    <table  border="1" >


                        <% int index = 3;%>


                                           <c:forEach begin="40" end="59" items="${invoiceDetailsList}" var="invoiceDetailsList">

                                           <tr>
                                           <font size="2">

                                            <td  height="30" style="font-size: 12px;width: 48px;"><c:out value="${invoiceDetailsList.grNumber}"/></td>
                                            <td  height="40" style="font-size: 12px;width: 65px;"> <c:out value="${invoiceDetailsList.grDate}"/></td>
                                            <td  height="30" style="font-size: 12px;width: 275px;"> <c:out value="${invoiceDetailsList.destination}"/></td>
                                            <td  height="30" style="font-size: 12px;width: 89px;text-align: center;"><c:out value="${invoiceDetailsList.containerQty}"/>
                                            <c:set var="totalContainer" value='${invoiceDetailsList.containerQty+totalContainer}'/>
                                            </td>
                                            <td  height="30" style="font-size: 12px;width: 67px;text-align: center;"><c:out value="${invoiceDetailsList.articleName}"/></td>
                                            <td  height="30" style="font-size: 12px;text-align:right;width: 55px;"><span id="initialFreightAmountSpan<%=index%>"></span>
                                             <script>
                                            var freightAmount = '<c:out value="${invoiceDetailsList.freightAmount}"/>';
                                              var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                            $("#initialFreightAmountSpan<%=index%>").text(numberWithCommas(formattedAmount));
//
                                        </script>
                                            </td>

                                            <td  height="30" style="font-size: 12px;text-align:right;width: 59px;"><span id="otherExpenseSpan<%=index%>"></span>
                                              <script>
                                            var freightAmount = '<c:out value="${invoiceDetailsList.otherExpense}"/>';
                                              if(freightAmount != ''){
                                              var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                            $("#otherExpenseSpan<%=index%>").text(numberWithCommas(formattedAmount));
                                             }
//
                                        </script></font>
                                            </td>
                                            <td  height="30" style="font-size: 12px;text-align:right;width: 92px;"><span id="detaintionSpan<%=index%>"></span>
                                             <script>
                                            var freightAmount = '<c:out value="${invoiceDetailsList.detaintion}"/>';
                                               if(freightAmount != ''){
                                            var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                          $("#detaintionSpan<%=index%>").text(numberWithCommas(formattedAmount));
                                               }
//
                                        </script>
                                            </td>
                                            <td style="font-size: 12px;text-align:right;width: 70px;"></td>
                                            <td style="font-size: 12px;text-align:right;width: 79px;"></td>
                                            <td  height="30" style="font-size: 12px;text-align:right;width: 98px;"> <span id="prefreightAmountSpan<%=index%>"></span>
                                                <c:set var="totalFreightChargeOfRow" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.detaintion+totalFreightChargeOfRow}'/></font>
                                                <c:set var="totalFreightCharge" value='${invoiceDetailsList.freightAmount+invoiceDetailsList.tollTax+invoiceDetailsList.otherExpense+invoiceDetailsList.detaintion+totalFreightCharge}'/></font>
                                          <script>
                                            var freightAmount = '<c:out value="${totalFreightChargeOfRow}"/>';
                                              var formattedAmount=parseFloat(freightAmount).toFixed(2);
                                            $("#prefreightAmountSpan<%=index%>").text(numberWithCommas(formattedAmount));

//
                                        </script>
                                            </td>

                                        </tr>
                                        <c:set var="totalFreightChargeOfRow" value="0"/>
                                            <%index++;%>
                                        </c:forEach>
                                         <input type="hidden" name="count1" id="count1" value="<%=index%>"/>

                                        </table>
                                </td>

                            </tr>
                            <tr style="border-left: 0px;border-right: 0px;display: none;" id="breakTwo">
                                <td height="150" style="width: 300px;border-left: 0px;border-right: 0px;" colspan="3">
                               <table align="center"  width="100%" height="20%" style="font-size: 10px;border:none;" id="spaceTable">
                        <tr>
                            <td colspan="1"  id="displayrow11" align="right" style="margin-right: 190px;border:none;" border="0"><b>Page 01 of 02</b></td>
                            <td colspan="1"  id="displayrow12" align="right" style="margin-right: 190px;border:none;" border="0"><b>Page 01 of 03</b></td>
                        </tr>
                        <tr style="border:none">
                            <td style="border:none">
                                <br>
                             <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>

                            </td>
                        </tr>
                    </table>
                                    </td>
                        </tr>-->



                            <tr>
                                <td colspan="4" height="30">
                            <!--<div colspan="3" style="background-image: url(images/Cancelled.jpg);  height: 200px; width: 800px; border: 1px solid black;"></div>-->
<!--<div style="background-image: url(../images/test-background.gif); height: 200px; width: 400px; border: 1px solid black;"> </div>-->
                                    <font size="2"> Amount Chargeable(In words)</font><br>
                                    <font size="3"> <b>Rs.
                                    <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                    <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totalFreightCharge")));%>
                                    <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                    <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; Only</b>
                                    </jsp:useBean>
                                     </b></font>
                                    <br>
                                    <br>
                                    <br>
                                    <font size="3"><b>Remarks</b></font><br>
                                    <font size="2">Being Road Transportation Charges Of <c:out value="${containerTypeName}"/> <c:out value="${movementType}"/> Containers<br></font> <br>
                                    <font size="2">Declaration<br></font>
                                    <font size="3">1)All diputes are subject to Delhi Jurisdition</font><br>
                                    <font size="3">2)CENVAT credit on inputs, capital goods and input services, used for providing the taxable service, has not been taken under the provisions of the CENVAT Credit Rules, 2004.</font><br>
                                    <font size="3">3)Service tax, in respect of services provided by a goods transport agency in respect of transportation of goods by road, will be paid by service receiver as per Notification No. 30/2012 - ST dated 20.06.2012 .</font><br>
                                    <font size="3">4)Service Tax under GTA service including Trailer Detention Charges "Service tax will be paid by service receiver"</font><br>
                                    <font size="3">5)PAN No.AACCB8054G</font><br>
                                    <font size="3">6)SERVICE TAX No.AACCB8054GST001</font><br>
                                    <table style="float: right;">
                                        <tr>
                                            <td align="right" colspan="3">
                                    <font size="3" ><b>For International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</b></font><br>
                                    <font size="3" >(Formerly Known As Boxtrans Logistics (India) Services Pvt Ltd)</font><br><br><br><br><br>

                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left"></td>
                                            <td></td>
                                            <td align="right">
                                                 <font size="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Authorised Signature </font><br>
                                            </td>
                                        </tr>
                                    </table>
                                    <br>
                                    <table style="float: left;">
                                        <td colspan="2"></td>
                                        <td style="padding-top: 95px;"> <font size="1"> Ref code:<c:out value="${userName}"/></font> </td>
                                    </table>
                                    <br>

                                     </td>
                        </tr>
                        <tr>
                            <table>

                            <font >   <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Regd.Office:</b> Godrej Coliseum,Office No. 801,8th Floor,C-Wing,Behind Everard Nagar,off Somaiya Hospital Road,Sion (East)Mumbai-400022 <br> </font>
                           </table>
                         </tr>
                        </table>

                    </div>
                    <br>
                    <br>
                    <center>
                        <input type="button" class="button"  value="Print" onClick="print('print');" >
                        &nbsp;&nbsp;&nbsp;<input type="button" class="button"  value="Back" onClick="goBack();" >
                        &nbsp;&nbsp;&nbsp;
                        <%--<%=String status = '<%=request.getAttribute("status")%>';%>--%>
                        <br>
                        <br>
                        <!--<span id="cancel" style="display:none"><input type="button" class="button"  value="cancel" onClick="cancel('<c:out value="${invoicecode}"/>');" ></span>-->
                        
                        <c:if test="${billSubmitStatus == '0'}">
                        <input type="button" class="button"  value="Edit Invoice Amount" onClick="editInvoice(<c:out value="${tripId}"/>);" style="width: 180px">
                        </c:if>
                    </center>
                </div>
                                    <script>
function goBack() {
    window.history.back();
}
</script>
                                            <script type="text/javascript">
                                     function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
                </script>
                <script>
                    $(".nexttab").click(function () {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                 <script type="text/javascript">
                function cancel(billingNo) {
                    var submitStatus= <%=request.getAttribute("submitStatus")%>;
//                    alert(submitStatus);
//                    alert(billingNo);
                   var confirms =confirm("Do You want to Cancel the Invoice?");
                    if(confirms == true){
                    alert("You Pressed Yes")
                 if(submitStatus == '0'){
                    document.trip.action = '/throttle/viewOrderBillForSubmit.do?cancelStatus='+ 1 +'&billingNo='+billingNo
                    document.trip.submit();
                 }
             }
                 else{
                 alert("You Pressed No")
                 }
                 }
                     
                     
                function checkSpace() {
                    
                    var invoiceSize = <%=request.getAttribute("invoiceDetailsListSize")%> ;
//                   alert(invoiceSize)
                    
                   if(16<=invoiceSize){
                       //alert("1");
                        $("#breakOne").show();
                        $("#tableTwo").show();
                        $("#breakTwo").hide();
                        $("#remarksBreak").show();
//                        $("#headerOne").show();
                        $("#headerTwo").hide();
                        $("#tableThree").hide();
                  }else if(40<=invoiceSize){
                     // alert("2");
                        $("#breakOne").show();
                        $("#breakTwo").show();
                        $("#tableThree").show();
                        $("#remarksBreak").show();
                        $("#headerOne").show();
//                        $("#headerTwo").hide();
                  }else if(60<=invoiceSize){
                     // alert("3");
                        $("#breakOne").show();
                        $("#breakTwo").show();
                        $("#remarksBreak").show();
                        $("#headerOne").show();
                        $("#headerTwo").show();
                }else{
                   // alert("4");
                    $("#breakOne").hide();
                    $("#breakTwo").hide();
                    $("#headerOne").hide();
                    $("#headerTwo").hide();
                    $("#tableTwo").hide();
                    $("#tableThree").hide();
                    $("#remarksBreak").hide();
                }
                 var firstRemarks= <%=request.getAttribute("firstRemarks")%> ;

                   //alert("first:"+firstRemarks );
                if(firstRemarks != '' && firstRemarks !=null && firstRemarks!='null'){
                        $("#firstRemarks").show();

                    }else{
                      $("#firstRemarks").hide();
                    }
                     var secondRemarks= <%=request.getAttribute("secondRemarks")%> ;
                    if(secondRemarks != '' && secondRemarks !=null && secondRemarks!='null'){
                        $("#secondRemarks").show();

                    }else{
                        $("#secondRemarks").hide();
                    }

//            function changes(){
//              //  alert(<%=request.getAttribute("invoiceDetailsListSize")%>);
//                var count = <%=request.getAttribute("invoiceDetailsListSize")%> ;
//                for(i=0;i<=count;i++){
//                    if(i == 20){
////                        break;
//                    twenty();
//                }
//                    else if(i == 40){
////                        break;
//                    forty();
//                }   else if(i == 60){
////                        break;
//                    sixty();
//                }else{
//                    $("#breakOne").hide();
//                    $("#breakTwo").hide();
//                    $("#headerOne").hide();
//                    $("#headerTwo").hide();
//                    $("#tableTwo").hide();
//                    $("#tableThree").hide();
//                    $("#remarksBreak").hide();
//                }
//            }
//        }
//
//            function twenty(){
//                        $("#breakOne").show();
//                        $("#tableTwo").show();
//                        $("#breakTwo").hide();
//                        $("#remarksBreak").show();
//                        $("#headerTwo").hide();
//                        $("#headerOne").show();
//            }
//            function forty(){
//                        $("#breakOne").show();
//                        $("#breakTwo").show();
//                        $("#tableThree").show();
//                        $("#remarksBreak").show();
//                        $("#headerOne").show();
//                        $("#headerTwo").show();
//
//            }
//            function sixty(){
//                        $("#breakOne").show();
//                        $("#breakTwo").show();
//                        $("#remarksBreak").show();
//                        $("#headerOne").show();
//                        $("#headerTwo").show();
//
//            }

                   var status = <%=request.getAttribute("status")%> ;
                    if(status == '1'){
//                        alert(status)
                       document.getElementById('cancel').style.display = 'block';   
                      }
                      else{
                        document.getElementById('cancel').style.display = 'none';     
                      }
                  }
            </script>
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    

    </body>
</html>