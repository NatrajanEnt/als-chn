<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

            <style type="text/css">
              option.red {font-color: red;}
              option.green {font-color: greenyellow;}
              </style>

<script type="text/javascript">


    function viewTripDetails(tripId) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function setValues() {
        if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
            document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
        }
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.getElementById('startDateFrom').value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.getElementById('startDateTo').value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("endDateFrom")%>' != 'null') {
            document.getElementById('endDateFrom').value = '<%=request.getAttribute("endDateFrom")%>';
        }
        if ('<%=request.getAttribute("endDateTo")%>' != 'null') {
            document.getElementById('endDateTo').value = '<%=request.getAttribute("endDateTo")%>';
        }
        if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
            document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
        }
        if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
            document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
        }
        if ('<%=request.getAttribute("customerId")%>' != 'null') {
            document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
        }
        if ('<%=request.getAttribute("tripStatusId")%>' != 'null') {
            document.getElementById('tripStatusId').value = '<%=request.getAttribute("tripStatusId")%>';
        }
        if ('<%=request.getAttribute("tripStatusIdTo")%>' != 'null') {
            document.getElementById('tripStatusIdTo').value = '<%=request.getAttribute("tripStatusIdTo")%>';
        }
        if ('<%=request.getAttribute("not")%>' != 'null' && '<%=request.getAttribute("not")%>' != '') {
            document.getElementById('not').checked = true;
        }
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }

    function submitPage(val) {
        if (val == 'ExportExcel') {
            document.tripSheet.action = '/throttle/handleFATransactionReport.do?param=ExportExcel';
            document.tripSheet.submit();
        } else {
            document.tripSheet.action = '/throttle/handleFATransactionReport.do?param=search';
            document.tripSheet.submit();
        }
    }
</script>

<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©</a>
              </span>-->

    <style type="text/css">





        .container {width: 960px; margin: 0 auto; overflow: hidden;}
        .content {width:800px; margin:0 auto; padding-top:50px;}
        .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

        /* STOP ANIMATION */



        /* Second Loadin Circle */

        .circle1 {
            background-color: rgba(0,0,0,0);
            border:5px solid rgba(100,183,229,0.9);
            opacity:.9;
            border-left:5px solid rgba(0,0,0,0);
            /*	border-right:5px solid rgba(0,0,0,0);*/
            border-radius:50px;
            /*box-shadow: 0 0 15px #2187e7; */
            /*	box-shadow: 0 0 15px blue;*/
            width:40px;
            height:40px;
            margin:0 auto;
            position:relative;
            top:-50px;
            -moz-animation:spinoffPulse 1s infinite linear;
            -webkit-animation:spinoffPulse 1s infinite linear;
            -ms-animation:spinoffPulse 1s infinite linear;
            -o-animation:spinoffPulse 1s infinite linear;
        }

        @-moz-keyframes spinoffPulse {
            0% { -moz-transform:rotate(0deg); }
            100% { -moz-transform:rotate(360deg);  }
        }
        @-webkit-keyframes spinoffPulse {
            0% { -webkit-transform:rotate(0deg); }
            100% { -webkit-transform:rotate(360deg);  }
        }
        @-ms-keyframes spinoffPulse {
            0% { -ms-transform:rotate(0deg); }
            100% { -ms-transform:rotate(360deg);  }
        }
        @-o-keyframes spinoffPulse {
            0% { -o-transform:rotate(0deg); }
            100% { -o-transform:rotate(360deg);  }
        }



    </style>
    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i><spring:message code="operations.reports.label.ViewTransactionReport" text="ViewTransactionReport"/></h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="default text"/>:</span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="default text"/></a></li>
                <li><a href="general-forms.html"><spring:message code="head.label.Report" text="default text"/></a></li>
                <li class="active"><spring:message code="operations.reports.label.ViewTransactionReport" text="ViewTransactionReport"/></li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setValues();
                        sorter.size(5);">
                    <form name="tripSheet" method="post">

                        <table class="table table-bordered" id="report" >
                            <!--            <tr><td colspan="7" align="left"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                                                </h2></td>
                                            <td align="right"><div style="height:17px;margin-top:0px;"><img src="../images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="../images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                                        </tr>-->
                            <!--            <tr height="30" id="index" >
                                                        <td colspan="4"  style="background-color:#5BC0DE;">
                                               
                                                        <b><spring:message code="operations.reports.label.ViewTripSheetReport" text="default text"/></b>
                                                        </td></tr>-->
                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">
                                    <table  class="table table-info mb30 table-hover" style="width:60%;" align="left" >
                                        
                                        <tr>
                                            <td width="80" ><spring:message code="operations.reports.label.TripStartDateFrom" text="default text"/></td>
                                            <td  width="80" ><input type="textbox"  style="width:260px;height:40px;"  value="" class="datepicker" name="startDateFrom" id="startDateFrom" ></td>
                                            <td width="80" ><spring:message code="operations.reports.label.TripStartDateTo" text="default text"/></td>
                                            <td  width="80" ><input type="textbox"  style="width:260px;height:40px;"  value="" class="datepicker" name="startDateTo" id="startDateTo" ></td>
                                        </tr>
                                          <tr>
<!--                                            <td width="80" ><spring:message code="operations.reports.label.FleetCenter" text="default text"/></td>
                                            <td width="80"> <select name="fleetCenterId" style="width:260px;height:40px;" id="fleetCenterId" class="form-control" style="height:20px; width:122px;"" >
                                                    <c:if test="${companyList != null}">
                                                        <option value="" selected>--<spring:message code="operations.reports.label.Select" text="default text"/>--</option>
                                                        <c:forEach items="${companyList}" var="companyList">
                                                            <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                                        </c:forEach>
                                                    </c:if>
                                                </select>
                                                <script>
                                                    document.getElementById("fleetCenterId").value = '1461';
                                                </script>
                                            </td>-->
                                            <td width="80" ><spring:message code="operations.reports.label.Expense Type" text="Expense Type"/></td>
                                            <td width="80"> <select name="expenseType" style="width:260px;height:40px;" id="expenseType" class="form-control" style="height:20px; width:122px;"" >
                                                        <option value="0" selected>All</option>
                                                        <option value="1">Normal expense</option>
                                                        <option value="1010">Fuel</option>
                                                        <option value="1011">Fastag</option>
                                                        <option value="1036">Fuel Recovery</option>
                                                </select>
                                                <script>
                                                    document.getElementById("expenseType").value = '<c:out value="${expenseType}"/>';
                                                </script>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" align="center">
                                                <input type="button"   value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" class="btn btn-success" name="Search" onclick="submitPage('Search');" >
                                                &nbsp;&nbsp;<input type="button"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" class="btn btn-success" name="ExportExcel" onclick="submitPage('ExportExcel');" >
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                </tr>
                        </table>
                        <br>

                        <%--<c:if test = "${tripDetails == null}" >--%>
                        <!--<center>-->
                            <!--<font color="blue"><spring:message code="operations.reports.label.PleaseWaitYourRequestisProcessing" text="default text"/></font>-->
                        <!--                                <div class="container">
                                                            <div class="content">
                                                                <div class="circle"></div>
                                                                <div class="circle1"></div>-->
                        <!--                                    </div>
                                                        </div>
                                                    </center>-->
                        <%--</c:if>--%>
                        <c:if test="${transactionsDetailsSize != '0'}">
                            <table class="table table-info mb30 table-bordered" id="table" >
                                <thead>
                                    <tr >
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.Jobid" text="Jobid"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.SRLNO" text="SRLNO"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.CHQNO" text="CHQNO"/></th>
                                        <th style="border-left:1px solid white;width:100px;"><spring:message code="operations.reports.label.NARRATION1" text="NARRATION1"/></th>
                                        <th style="border-left:1px solid white;width:100px;"><spring:message code="operations.reports.label.NARRATION2" text="NARRATION2"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.ACCCODE" text="ACCCODE"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.SUBCODE" text="SUBCODE"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.DEBIT" text="DEBIT"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.CREDIT" text="CREDIT"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.vehicleNo" text="vehicleNo"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.licenseNo" text="licenseNo"/></th>
                                        <th style="border-left:1px solid white"><spring:message code="operations.reports.label.expenseName" text="expenseName"/></th>
                                        <!--                        <th><h3>Trip Created By</h3></th>
                                                                <th><h3>Trip Ended By</h3></th>
                                                                <th><h3>Trip Closed By</h3></th>
                                                                <th><h3>Trip Settled By</h3></th>-->

                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:forEach items="${transactionsDetails}" var="tripDetails">
                                        <%
                                                    String className = "text1";
                                                    if ((index % 2) == 0) {
                                                        className = "text1";
                                                    } else {
                                                        className = "text2";
                                                    }
                                        %>
                                        <tr height="30">
                                            <td class="<%=className%>" width="40" align="left"><%=index%></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.trlJobId}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.SRLNO}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.CHQNO}"/></td>
                                            <td class="<%=className%>" align="left" style="width:100px;"><c:out value="${tripDetails.NARRATION1}"/></td>
                                            <td class="<%=className%>" align="left" style="width:100px;"><c:out value="${tripDetails.NARRATION2}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.ACCCODE}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.SUBCODE}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.DEBIT}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.CREDIT}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.vehicleNo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.licenseNo}"/></td>
                                            <td class="<%=className%>" width="40" align="left"><c:out value="${tripDetails.expenseId}"/></td>
                                        </tr>

                                        <%index++;%>
                                    </c:forEach>
                                </tbody>
                            </table>

                            <script language="javascript" type="text/javascript">
                                setFilterGrid("table");
                            </script>
                            <div id="controls">
                                <div id="perpage">
                                    <select onchange="sorter.size(this.value)">
                                        <option value="5" >5</option>
                                        <option value="10">10</option>
                                        <option value="20" selected="selected">20</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                                </div>
                                <div id="navigation">
                                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />


                                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                                </div>
                                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                            </div>
                            <script type="text/javascript">
                                var sorter = new TINY.table.sorter("sorter");
                                sorter.head = "head";
                                sorter.asc = "asc";
                                sorter.desc = "desc";
                                sorter.even = "evenrow";
                                sorter.odd = "oddrow";
                                sorter.evensel = "evenselected";
                                sorter.oddsel = "oddselected";
                                sorter.paginate = true;
                                sorter.currentid = "currentpage";
                                sorter.limitid = "pagelimit";
                                sorter.init("table", 0);
                            </script>
                        </c:if>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>

