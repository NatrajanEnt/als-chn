/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.trip.business;

/**
 * for Tally
 *
 * @author ramprasath
 */
public class TallyTO {
    
    private String invoiceFlag = null;
    private String tripId = null;
    private String mofussilhaltingAmt = null;
    private String mofussilHaltingDays = null;
    private String localhaltingAmt = null;
    private String localHaltingDays = null;
    
    private String haltingDays = null;
    private String haltCntrctAmnt = null;
    private String trpEndORClsr = null;
    private String trpInvType = null;
    private String tripType = null;
    private String grossWt = null;
    
    private String InvAmount = null;
    private String InvNo = null;
    private String cityReturnedCode = null;
    private String cleanerCode = null;
    private String driverCode = null;
    private String consigneeCode = null;
    private String trailerCode = null;
    private String credit = null;
    private String debit = null;
    private String narration2 = null;
    private String narration1 = null;
    private String subCode = null;
    private String accCode = null;
    private String jobStartDate = null;
    private String jobEndDate = null;
    private String cityFromCode = null;
    private String cityToCode = null;
    private String distance = null;
    private String ownedHired = null;
    private String hiredVehicleNo = null;
    private String hireCharges = null;
    private String partyname = null;
    private String dieselSupplies = null;
    private String dieselConsumed = null;
    private String openingKM = null;
    private String closingKM = null;
    private String totalKMRun = null;
    private String size_20 = null;
    private String size_40 = null;
    private String size_40HC = null;
    private String size_FT = null;
    private String size_OT = null;
    private String netWt = null;
    private String closed = null;
    private String companyCode = null;
    private String brnCd = null;
    private String runDate = null;
    private String jobReachedDate = null;
    private String jobDate = null;
    private String dieselRecovered = null;
    private String isExport = null;
    private String trlJobId = null;
    private String invDt = null;
    private String size_20Taurus = null;
    private String size_40SngAxle = null;
    private String size_24DblAxle = null;
    private String size_40TplAxle = null;
    private String size_20DblAxle = null;
    private String srlNo = null;
    private String containerSize = null;
    private String containerName = null;
    private String sealNo = null;
    private String vehicleName = null;
    private String trxType = null;

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }

    public String getDebit() {
        return debit;
    }

    public void setDebit(String debit) {
        this.debit = debit;
    }

    public String getNarration2() {
        return narration2;
    }

    public void setNarration2(String narration2) {
        this.narration2 = narration2;
    }

    public String getNarration1() {
        return narration1;
    }

    public void setNarration1(String narration1) {
        this.narration1 = narration1;
    }

    public String getRunDate() {
        return runDate;
    }

    public void setRunDate(String runDate) {
        this.runDate = runDate;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

   
    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getAccCode() {
        return accCode;
    }

    public void setAccCode(String accCode) {
        this.accCode = accCode;
    }

    public String getJobStartDate() {
        return jobStartDate;
    }

    public void setJobStartDate(String jobStartDate) {
        this.jobStartDate = jobStartDate;
    }

    public String getJobEndDate() {
        return jobEndDate;
    }

    public void setJobEndDate(String jobEndDate) {
        this.jobEndDate = jobEndDate;
    }

    public String getCityFromCode() {
        return cityFromCode;
    }

    public void setCityFromCode(String cityFromCode) {
        this.cityFromCode = cityFromCode;
    }

    public String getCityToCode() {
        return cityToCode;
    }

    public void setCityToCode(String cityToCode) {
        this.cityToCode = cityToCode;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getOwnedHired() {
        return ownedHired;
    }

    public void setOwnedHired(String ownedHired) {
        this.ownedHired = ownedHired;
    }

    public String getHiredVehicleNo() {
        return hiredVehicleNo;
    }

    public void setHiredVehicleNo(String hiredVehicleNo) {
        this.hiredVehicleNo = hiredVehicleNo;
    }

    public String getHireCharges() {
        return hireCharges;
    }

    public void setHireCharges(String hireCharges) {
        this.hireCharges = hireCharges;
    }

    public String getPartyname() {
        return partyname;
    }

    public void setPartyname(String partyname) {
        this.partyname = partyname;
    }

    public String getDieselSupplies() {
        return dieselSupplies;
    }

    public void setDieselSupplies(String dieselSupplies) {
        this.dieselSupplies = dieselSupplies;
    }

    public String getDieselConsumed() {
        return dieselConsumed;
    }

    public void setDieselConsumed(String dieselConsumed) {
        this.dieselConsumed = dieselConsumed;
    }

    public String getOpeningKM() {
        return openingKM;
    }

    public void setOpeningKM(String openingKM) {
        this.openingKM = openingKM;
    }

    public String getClosingKM() {
        return closingKM;
    }

    public void setClosingKM(String closingKM) {
        this.closingKM = closingKM;
    }

    public String getTotalKMRun() {
        return totalKMRun;
    }

    public void setTotalKMRun(String totalKMRun) {
        this.totalKMRun = totalKMRun;
    }

    public String getSize_20() {
        return size_20;
    }

    public void setSize_20(String size_20) {
        this.size_20 = size_20;
    }

    public String getSize_40() {
        return size_40;
    }

    public void setSize_40(String size_40) {
        this.size_40 = size_40;
    }

    public String getSize_40HC() {
        return size_40HC;
    }

    public void setSize_40HC(String size_40HC) {
        this.size_40HC = size_40HC;
    }

    public String getSize_FT() {
        return size_FT;
    }

    public void setSize_FT(String size_FT) {
        this.size_FT = size_FT;
    }

    public String getSize_OT() {
        return size_OT;
    }

    public void setSize_OT(String size_OT) {
        this.size_OT = size_OT;
    }

    public String getNetWt() {
        return netWt;
    }

    public void setNetWt(String netWt) {
        this.netWt = netWt;
    }

    public String getClosed() {
        return closed;
    }

    public void setClosed(String closed) {
        this.closed = closed;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getBrnCd() {
        return brnCd;
    }

    public void setBrnCd(String brnCd) {
        this.brnCd = brnCd;
    }


    public String getJobReachedDate() {
        return jobReachedDate;
    }

    public void setJobReachedDate(String jobReachedDate) {
        this.jobReachedDate = jobReachedDate;
    }

    public String getJobDate() {
        return jobDate;
    }

    public void setJobDate(String jobDate) {
        this.jobDate = jobDate;
    }

    public String getDieselRecovered() {
        return dieselRecovered;
    }

    public void setDieselRecovered(String dieselRecovered) {
        this.dieselRecovered = dieselRecovered;
    }

    public String getIsExport() {
        return isExport;
    }

    public void setIsExport(String isExport) {
        this.isExport = isExport;
    }

    public String getTrlJobId() {
        return trlJobId;
    }

    public void setTrlJobId(String trlJobId) {
        this.trlJobId = trlJobId;
    }

    public String getInvDt() {
        return invDt;
    }

    public void setInvDt(String invDt) {
        this.invDt = invDt;
    }

    public String getSize_20Taurus() {
        return size_20Taurus;
    }

    public void setSize_20Taurus(String size_20Taurus) {
        this.size_20Taurus = size_20Taurus;
    }

    public String getSize_40SngAxle() {
        return size_40SngAxle;
    }

    public void setSize_40SngAxle(String size_40SngAxle) {
        this.size_40SngAxle = size_40SngAxle;
    }

    public String getSize_24DblAxle() {
        return size_24DblAxle;
    }

    public void setSize_24DblAxle(String size_24DblAxle) {
        this.size_24DblAxle = size_24DblAxle;
    }

    public String getSize_40TplAxle() {
        return size_40TplAxle;
    }

    public void setSize_40TplAxle(String size_40TplAxle) {
        this.size_40TplAxle = size_40TplAxle;
    }

    public String getSize_20DblAxle() {
        return size_20DblAxle;
    }

    public void setSize_20DblAxle(String size_20DblAxle) {
        this.size_20DblAxle = size_20DblAxle;
    }

    public String getSrlNo() {
        return srlNo;
    }

    public void setSrlNo(String srlNo) {
        this.srlNo = srlNo;
    }

    public String getContainerSize() {
        return containerSize;
    }

    public void setContainerSize(String containerSize) {
        this.containerSize = containerSize;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getSealNo() {
        return sealNo;
    }

    public void setSealNo(String sealNo) {
        this.sealNo = sealNo;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getCityReturnedCode() {
        return cityReturnedCode;
    }

    public void setCityReturnedCode(String cityReturnedCode) {
        this.cityReturnedCode = cityReturnedCode;
    }

    public String getCleanerCode() {
        return cleanerCode;
    }

    public void setCleanerCode(String cleanerCode) {
        this.cleanerCode = cleanerCode;
    }

    public String getDriverCode() {
        return driverCode;
    }

    public void setDriverCode(String driverCode) {
        this.driverCode = driverCode;
    }

    public String getConsigneeCode() {
        return consigneeCode;
    }

    public void setConsigneeCode(String consigneeCode) {
        this.consigneeCode = consigneeCode;
    }

    public String getTrailerCode() {
        return trailerCode;
    }

    public void setTrailerCode(String trailerCode) {
        this.trailerCode = trailerCode;
    }

    public String getInvAmount() {
        return InvAmount;
    }

    public void setInvAmount(String InvAmount) {
        this.InvAmount = InvAmount;
    }

    public String getInvNo() {
        return InvNo;
    }

    public void setInvNo(String InvNo) {
        this.InvNo = InvNo;
    }

    public String getHaltingDays() {
        return haltingDays;
    }

    public void setHaltingDays(String haltingDays) {
        this.haltingDays = haltingDays;
    }

    public String getHaltCntrctAmnt() {
        return haltCntrctAmnt;
    }

    public void setHaltCntrctAmnt(String haltCntrctAmnt) {
        this.haltCntrctAmnt = haltCntrctAmnt;
    }

    public String getTrpEndORClsr() {
        return trpEndORClsr;
    }

    public void setTrpEndORClsr(String trpEndORClsr) {
        this.trpEndORClsr = trpEndORClsr;
    }

    public String getTrpInvType() {
        return trpInvType;
    }

    public void setTrpInvType(String trpInvType) {
        this.trpInvType = trpInvType;
    }

    public String getTripType() {
        return tripType;
    }

    public void setTripType(String tripType) {
        this.tripType = tripType;
    }

    public String getGrossWt() {
        return grossWt;
    }

    public void setGrossWt(String grossWt) {
        this.grossWt = grossWt;
    }

    public String getMofussilhaltingAmt() {
        return mofussilhaltingAmt;
    }

    public void setMofussilhaltingAmt(String mofussilhaltingAmt) {
        this.mofussilhaltingAmt = mofussilhaltingAmt;
    }

    public String getMofussilHaltingDays() {
        return mofussilHaltingDays;
    }

    public void setMofussilHaltingDays(String mofussilHaltingDays) {
        this.mofussilHaltingDays = mofussilHaltingDays;
    }

    public String getLocalhaltingAmt() {
        return localhaltingAmt;
    }

    public void setLocalhaltingAmt(String localhaltingAmt) {
        this.localhaltingAmt = localhaltingAmt;
    }

    public String getLocalHaltingDays() {
        return localHaltingDays;
    }

    public void setLocalHaltingDays(String localHaltingDays) {
        this.localHaltingDays = localHaltingDays;
    }

    public String getInvoiceFlag() {
        return invoiceFlag;
    }

    public void setInvoiceFlag(String invoiceFlag) {
        this.invoiceFlag = invoiceFlag;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    
    
}
