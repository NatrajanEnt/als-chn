<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@ page import="ets.domain.vehicle.business.VehicleTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        
        
        <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

    </head>
    <script language="javascript">

function show_src() {
    document.getElementById('exp_table').style.display='none';
}
function show_exp() {
    document.getElementById('exp_table').style.display='block';
}
function show_close() {
    document.getElementById('exp_table').style.display='none';
}

        function submitPage(value)
        {
        if(value == 'search' ){
//                || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
//                        if(value=='GoTo'){
//                            var temp=document.jobCardCloseView.GoTo.value;
//                            document.jobCardCloseView.pageNo.value=temp;
//                            document.jobCardCloseView.button.value=value;
//                            document.jobCardCloseView.action = '/throttle/ViewJobCardsBilling.do';
//                            document.jobCardCloseView.submit();
//                        }else if(value == "First"){
//                        temp ="1";
//                        document.jobCardCloseView.pageNo.value = temp;
//                        value='GoTo';
//                    }else if(value == "Last"){
//                    temp =document.jobCardCloseView.last.value;
//                    document.jobCardCloseView.pageNo.value = temp;
//                    value='GoTo';
//                }
//                /*
//                 if(document.jobCardCloseView.jobcardId.value != ""){
//                 **/
//                document.jobCardCloseView.button.value=value;
                document.jobCardCloseView.action = '/throttle/ViewJobCardsBilling.do';
                document.jobCardCloseView.submit();
                /*
                 } else {
                 alert("Job Dard No is not Filled");
                }
                 **/
            }
        }
        
        function setValues(){
            if( '<%= request.getAttribute("regNo") %>' != 'null' ){
                document.jobCardCloseView.regNo.value = '<%= request.getAttribute("regNo") %>';
            }
            if( '<%= request.getAttribute("jcId") %>' != 'null' ){
                document.jobCardCloseView.jobcardId.value = '<%= request.getAttribute("jcId") %>';
            }
            if( '<%= request.getAttribute("compId") %>' != 'null' ){
                document.jobCardCloseView.serviceLocation.value = '<%= request.getAttribute("compId") %>';
            }

         }
function getVehicleNos(){
    //onkeypress='getList(sno,this.id)'
    var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/getVehicleNos.do?"));
}

function viewPartBill(partBillNo) {
        window.open('/throttle/viewPartBillDetails.do?billType=0&billNumber=' + partBillNo, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
function viewServiceBill(serviceBillNo) {
        window.open('/throttle/viewServiceBillDetails.do?billType=0&billNumber=' + serviceBillNo, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    </script>
                      <script>
   function changePageLanguage(langSelection){

            if(langSelection== 'ar'){
            document.getElementById("pAlign").style.direction="rtl";
            }else if(langSelection== 'en'){
            document.getElementById("pAlign").style.direction="ltr";
            }
        }

    </script>
            <div class="pageheader">
    <h2><i class="fa fa-edit"></i> View Billed Job Cards</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">View Billed Job Cards</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

        <c:if test="${jcList != null}">
        <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
        </c:if>
        <c:if test="${jcList == null && sessionCount == null}">
        <input type="hidden"  name="sessionCount" value="1"/>
        <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
        </c:if>
        <form method="post" name="jobCardCloseView">
<%
            int index = 0;
            int pag = (Integer)request.getAttribute("pageNo");
            index = ((pag -1) *10) +1 ;
%>



    <table class="table table-info mb30 table-hover">
        <thead><tr>
            <th colspan="4"><spring:message code="service.label.ViewBilledJobCards"  text="default text"/></th>
            </tr></thead>
    <tr>
         <td align="left" height="30"><font color="red">*</font><spring:message code="service.label.RegNo"  text="default text"/></td>
            <td height="30"><input type="text" class="form-control" style="width:240px;height:40px" id="regno" name="regNo" value="" /></td>
            <td align="left" height="30"><spring:message code="service.label.JobCardNo"  text="default text"/></td>
            <td><input type="text" class="form-control" style="width:249px;height:40px" name="jobcardId" value="" /> </td>
    </tr>
    <tr>
            <td><spring:message code="service.label.ServicePoint"  text="default text"/></td>
            <td> <select class="form-control" name="serviceLocation"  style="width:249px;height40px">
                <option selected   value=0>---Select---</option>
                <c:if test = "${servicePoints != null}" >
                <c:forEach items="${servicePoints}" var="mfr">
                <option  value='<c:out value="${mfr.compId}" />'>
                <c:out value="${mfr.compName}" />
                </c:forEach >
                </c:if>
                </select>
            </td>
        <td><input type="button" class="btn btn-info" name="search" value="<spring:message code="service.label.SEARCH"  text="default text"/>" onClick="submitPage(this.name);"  />   </td>
    </tr>
    </table>


            <c:if test = "${jcList != null}" >
		<table class="table table-info mb30 table-hover" id="table">

                    <thead><tr height="30">
		<th  align="left"><spring:message code="service.label.SNo"  text="default text"/></th>
		<th  align="left"><spring:message code="service.label.JobCardNo"  text="default text"/></th>
		<th  align="left"><spring:message code="service.label.VehicleNo"  text="default text"/></th>
		<th  align="left"><spring:message code="service.label.servicePoint"  text="default text"/></th>
		<th  align="left"><spring:message code="service.label.DeliveryTime"  text="default text"/></th>
		<th  align="left"><spring:message code="service.label.ClosedBy"  text="default text"/></th>
		<th  align="left"><spring:message code="service.label.ClosedOn"  text="default text"/></th>
		<th  align="left"><spring:message code="service.label.Status"  text="default text"/></th>
		<th  align="left"><spring:message code="service.label.select"  text="Parts Invoice"/></th>
		<!--<th  align="left"><spring:message code="service.label.select"  text="Parts Pdf Print"/></th>-->
		<th  align="left"><spring:message code="service.label.select"  text="Labour Invoice"/></th>
		<!--<th  align="left"><spring:message code="service.label.select"  text="Labour Pdf Print"/></th>-->
		
                        </tr></thead>
                    <%

                    %>

                    <c:forEach items="${jcList}" var="list">
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                %>
                <tr height="30">
                    <td  height="30"><%=index %></td>
                    <td  height="30"><c:out value="${list.jcMYFormatNo}"/></td>
                    <td  height="30"><c:out value="${list.vehicleNo}"/></td>
                    <td  height="30"><c:out value="${list.companyName}"/></td>
                    <td  height="30"><c:out value="${list.jobCardPCD}"/></td>
                    <td  height="30"><c:out value="${list.closedBy}"/></td>
                    <td  height="30"><c:out value="${list.closedOn}"/></td>
                    <td  height="30"><c:out value="${list.status}"/></td>

                    <c:if test="${list.partBillNo != 0}">
                    <td><a href='#' onclick='viewPartBill(<c:out value="${list.partBillNo}"/>);'>View</a></td>
                    </c:if>
                    <c:if test="${list.partBillNo == 0}">
                    <td>-</td>
                    </c:if>

                    <c:if test="${list.labourBillNo != 0}">
                    <td><a href='#' onclick='viewServiceBill(<c:out value="${list.labourBillNo}"/>);'>View</a></td>
                    </c:if>
                    <c:if test="${list.labourBillNo == 0}">
                    <td>-</td>
                    </c:if>

                </tr>
                <%
                index++;
                %>
                </c:forEach>
                </table>
            </c:if>

            <br>

<!--            <table align="center" cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td>
                        <%--<%@ include file="/content/common/pagination.jsp"%>--%>
                    </td>
                </tr>
            </table>-->
                <input type="hidden" name="reqfor" value="">
            <br>
                
                
                <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
        <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
    </body>
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>