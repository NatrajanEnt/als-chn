<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $( "#datepicker" ).datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $( ".datepicker" ).datepicker({

            /*altField: "#alternate",
                        altFormat: "DD, d MM, yy"*/
            changeMonth: true,changeYear: true
        });

    });
</script>

    </head>
    <script language="javascript">
        
        function submitManualPage(){
            $("#mySubmit").hide();
            document.approve.action = '/throttle/saveVehicleDriverAdvanceApproval.do';
            document.approve.submit();
        }
        function setFocus(){
            document.approve.approveamt.focus();
        }
    </script>


    <body onload="setFocus();">
        <form name="approve"  method="post" >
           
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <table align="center" border="0" cellpadding="0" cellspacing="0" width="50%" id="bg" class="border" >

                <a style="display: none"></a>
                <tr align="center">
                    <td colspan="2" align="center" class="contenthead" height="30">
                        <div class="contenthead">Advance Approval</div></td>
                </tr>
                  <c:if test="${viewVehicleDriverAdvanceList != null}" >
                       <c:forEach items="${viewVehicleDriverAdvanceList}" var="trip">
                <tr>
                    <td class="text2" height="30" style="height:25px;border-bottom:1px solid;border-bottom-color: #f2f2f2;font-weight:normal;background:#f2f2f2;font-size:12px;">Trip Code</td>
                    <td class="text2" height="30"><c:out value="${trip.tripCode}"/></td>
                </tr>
                <tr>
                    <td class="text2" height="30">Customer Name</td>
                    <td class="text2" height="30"><c:out value="${trip.customerName}"/></td>
                </tr>
                
                <tr>
                    <td class="text1" height="30">CNotes</td>
                    <td class="text1" height="30"><c:out value="${trip.cNotes}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Route Name</td>
                    <td class="text1" height="30"><c:out value="${trip.routeName}"/></td>
                </tr>
                       </c:forEach>
                  </c:if>
                <tr>
                    <td class="text2" height="30">Driver Name</td>
                    <td class="text2" height="30">
                        <input name="vehicleDriverAdvanceId" id="vehicleDriverAdvanceId" type="hidden" class="textbox" value='<c:out value="${vehicleDriverAdvanceId}"/>' />
                        <input type="hidden" name="primaryDriver" id="primaryDriver" value="<c:out value="${primaryDriver}"/>"/><c:out value="${primaryDriver}"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Vehicle No</td>
                    <td class="text1" height="30">
                        <input type="hidden" name="vehicleNo" id="vehicleNo" value="<c:out value="${vehicleNo}"/>"/><c:out value="${vehicleNo}"/>
                        <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                    </td>
                </tr>
                <tr>
                    <td class="text1" height="30">Request On</td>
                    <td class="text1" height="30">
                        <input name="advanceDate" class="textbox" type="hidden" value='<c:out value="${advanceDate}"/>'/><c:out value="${advanceDate}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30">Expense Type</td>
                    <td class="text1" height="30">
                        <input name="expenseType" class="textbox" type="hidden" value='<c:out value="${expenseType}"/>'/><c:out value="${expenseType}"/></td>
                </tr>

                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Req.Advance Amt.</td>
                    <td class="text2" height="30"><input name="advanceAmount" type="hidden" class="textbox" value='<c:out value="${advanceAmount}"/>'/><c:out value="${advanceAmount}"/></td>
                </tr>
                <tr>
                    <td class="text1" height="30"><font color="red">*</font>Status</td>
                    <td class="text1" height="30"><select name="approvestatus">
                            <option value="" >-Select Any One-</option>
                            <option value="2">Approved</option>
                            <option value="3">Rejected</option>
                </select></td>
                </tr>


                <tr>
                    <td class="text2" height="30"><font color="red">*</font>Approve Remarks</td>
                    <td class="text2" height="30"><textarea class="textbox" name="advanceRemarks"><c:out value="${advanceRemarks}"/></textarea></td>
                </tr>
            </table>
            <br>
            <center>
                
                <input type="button" id="mySubmit" value="Submit" class="button" onClick="submitManualPage();">
                
                &emsp;<input type="reset" class="button" value="Clear">
            </center>



        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
