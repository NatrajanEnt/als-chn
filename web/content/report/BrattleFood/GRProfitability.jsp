<%-- 
    Document   : GRProfitability
    Created on : May 2, 2016, 10:32:36 AM
    Author     : Gulshan Kumar
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
        <style type="text/css">
            .container {width: 960px; margin: 0 auto; overflow: hidden;}
            .content {width:800px; margin:0 auto; padding-top:50px;}
            .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

            /* STOP ANIMATION */

            /* Second Loadin Circle */

            .circle1 {
                background-color: rgba(0,0,0,0);
                border:5px solid rgba(100,183,229,0.9);
                opacity:.9;
                border-left:5px solid rgba(0,0,0,0);
                /*	border-right:5px solid rgba(0,0,0,0);*/
                border-radius:50px;
                /*box-shadow: 0 0 15px #2187e7; */
                /*	box-shadow: 0 0 15px blue;*/
                width:40px;
                height:40px;
                margin:0 auto;
                position:relative;
                top:-50px;
                -moz-animation:spinoffPulse 1s infinite linear;
                -webkit-animation:spinoffPulse 1s infinite linear;
                -ms-animation:spinoffPulse 1s infinite linear;
                -o-animation:spinoffPulse 1s infinite linear;
            }

            @-moz-keyframes spinoffPulse {
                0% { -moz-transform:rotate(0deg); }
                100% { -moz-transform:rotate(360deg);  }
            }
            @-webkit-keyframes spinoffPulse {
                0% { -webkit-transform:rotate(0deg); }
                100% { -webkit-transform:rotate(360deg);  }
            }
            @-ms-keyframes spinoffPulse {
                0% { -ms-transform:rotate(0deg); }
                100% { -ms-transform:rotate(360deg);  }
            }
            @-o-keyframes spinoffPulse {
                0% { -o-transform:rotate(0deg); }
                100% { -o-transform:rotate(360deg);  }
            }
        </style>
        <script>
            $(document).ready(function() {
                $('.ball, .ball1').removeClass('stop');
                $('.trigger').click(function() {
                    $('.ball, .ball1').toggleClass('stop');
                });
            });

        </script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>

        <script type="text/javascript">
            function submitPage(value) {
                //  alert(value);
                if (document.getElementById('fromDate').value == '') {
                    alert("Please select from Date");
                    document.getElementById('fromDate').focus();
                } else if (document.getElementById('toDate').value == '') {
                    alert("Please select to Date");
                    document.getElementById('toDate').focus();
                } else {
                    if (value == 'ExportExcel') {
                        document.accountReceivable.action = '/throttle/handleGRProfitabilityReport.do?param=ExportExcel';
                        document.accountReceivable.submit();
                    } else {
                        document.accountReceivable.action = '/throttle/handleGRProfitabilityReport.do?param=Search';
                        document.accountReceivable.submit();
                    }
                }
            }
            function setValue() {
                if ('<%=request.getAttribute("page")%>' != 'null') {
                    var page = '<%=request.getAttribute("page")%>';
                    if (page == 1) {
                        submitPage('search');
                    }
                }
            }
//
//            function viewCustomerProfitDetails(tripIds) {
////            alert(tripIds);
//                window.open('/throttle/viewGrReportExpenseDetails.do?tripId=' + tripIds + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
//            }
        </script>
    </head>
    <body>
        <!--<body>-->
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>

            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="center">
                        <div class="tabs" align="center" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">G R Wise Profitability</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="right"><input type="button" class="button" name="ExportExcel"   value="Export Excel" onclick="submitPage(this.name);">&nbsp;&nbsp;</td>
                                        <td colspan="3"><input type="button" class="button" name="Search"  id="Search"  value="Search" onclick="submitPage(this.name);"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>


            <br>
            <br>
            <c:if test = "${GRProfitList != null}" >
                <table>
                    <center>
                        <font size="4" color="Black"><b>GR Wise Profitability - TT</b></font>
                    </center>
                </table>
                <br>
                <table border="1"  align="center" width="800" cellpadding="0" cellspacing="1" >
                    <thead>
                        <tr>
                            <td rowspan="2" class="contenthead">Sr. No</td>
                            <td rowspan="2" class="contenthead">Rake  No.</td>
                            <td rowspan="2" class="contenthead">Train No.</td>
                            <td rowspan="2" class="contenthead">Container No.</td>
                            <td rowspan="2" class="contenthead">Container Size</td>
                            <td rowspan="2" class="contenthead">I/E/Repo</td>
                            <td rowspan="2" class="contenthead">Commodity</td>
                            <td rowspan="2" class="contenthead">GR No.</td>
                            <td rowspan="2" class="contenthead">GR Issuing Date</td>
                            <td rowspan="2" class="contenthead">Invoice No.</td>
                            <td rowspan="2" class="contenthead">Invoice Date</td>
                            <td rowspan="2" class="contenthead">Status</td>
                            <td rowspan="2" class="contenthead">TT Type</td>
                            <td rowspan="2" class="contenthead">TT Owner Name</td>
                            <td rowspan="2" class="contenthead">TT No.</td>
                            <td rowspan="2" class="contenthead">From</td>
                            <td rowspan="2" class="contenthead">To</td>
                            <td rowspan="2" class="contenthead">RouteInfo</td>
                            <td rowspan="2" class="contenthead">Chargable Y/N</td>
                            <td rowspan="2" class="contenthead">Reposition Y/N</td>
                            <td colspan="3" class="contenthead" style="text-align:center">Party Name</td>
                            <td colspan="6" class="contenthead" style="text-align:center">Revenue</td>
                            <td colspan="16" class="contenthead" style="text-align:center">TT-Expenses </td>
                            <td rowspan="2" class="contenthead">Un Recoverable Reposition Cost</td>
                            <td rowspan="2" class="contenthead">Profit</td>
                        </tr> 
                        <tr>
                            <td class="contenthead">Consignee Name</td>
                            <td class="contenthead">Consigner Name</td>
                            <td class="contenthead">Pay Party</td>

                            <td class="contenthead">Freight</td>
                            <td class="contenthead">Detention</td>
                            <td class="contenthead">Toll charges</td>
                            <td class="contenthead">Weightment</td>
                            <td class="contenthead">Other charges</td>
                            <td class="contenthead">Total Revenue</td>

                            <td class="contenthead">Diesel Ltr's</td>
                            <td class="contenthead">Rate</td>
                            <td class="contenthead">Diesel Amount</td>
                            <td class="contenthead">Toll Charges</td>
                            <td class="contenthead">Fooding Expenses</td>
                            <td class="contenthead">Entry Tax</td>
                            <td class="contenthead">RTO</td>
                            <td class="contenthead">Driver Salary</td>
                            <td class="contenthead">Lease Charges</td>
                            <td class="contenthead">Repair</td>
                            <td class="contenthead">Weightment</td>
                            <td class="contenthead">Octroi</td>
                            <td class="contenthead">Behati/Dala</td>
                            <td class="contenthead">Parking</td>
                            <td class="contenthead">Misc Exp</td>
                            <td class="contenthead">Total Expenses</td>
                        </tr>
                    </thead>
                    <% int index = 0, sno = 1;%>
                    <c:forEach items="${GRProfitList}" var="csList">
                        <c:set var="unrecoverablecost" value="${csList.tripExpense - csList.estimatedRevenue }" ></c:set>

                        <c:set var="Profit" value="${ csList.estimatedRevenue - csList.tripExpense  }" ></c:set>

                            <tr>
                                <td align="center"><%=sno%></td>
                            <td align="center"><c:out value=""/></td>
                            <td align="center"><c:out value=""/></td>
                            <td align="center"><c:out value="${csList.containerNo}"/></td>
                            <td align="center"><c:out value="${csList.containerSize}"/></td>
                            <td align="center"><c:out value="${csList.movementType}"/></td>
                            <td align="center"><c:out value="${csList.articleName}"/></td>
                            <td align="center"><c:out value="${csList.grNo}"/></td>
                            <td align="center"><c:out value="${csList.grDate}"/></td>
                            <td align="center"><c:out value="${csList.invoiceCode}"/></td>
                            <td align="center"><c:out value="${csList.invoiceDate}"/></td>
                            <td align="center"><c:out value="${csList.statusName}"/></td>
                            <td align="center"><c:out value="${csList.transportType}"/></td>
                            <td align="center"><c:out value="${csList.transporter}"/></td>
                            <td align="center"><c:out value="${csList.vehicleNo}"/></td>
                            <td align="center"><c:out value="${csList.origin}"/></td>
                            <td align="center"><c:out value="${csList.destination}"/></td>
                            <td align="center"><c:out value="${csList.routeInfo}"/></td>
                            <td align="center">
                                <c:if test = "${csList.movementType == 'Repo'}">
                                    N
                                </c:if>
                                <c:if test = "${csList.movementType != 'Repo'}">
                                    Y
                                </c:if>
                            </td>
                            <td align="center">
                                <c:if test = "${csList.movementType == 'Repo'}">
                                    Y
                                </c:if>
                                <c:if test = "${csList.movementType != 'Repo'}">
                                    N
                                </c:if>
                            </td>
                            <td align="center"><c:out value="${csList.consigneeName}"/></td>
                            <td align="center"><c:out value="${csList.consignorName}"/></td>
                            <td align="center"><c:out value="${csList.billingParty}"/></td>

                            <td align="center"><c:out value="${csList.estimatedRevenue}"/></td>
                            <td align="center"><c:out value="${csList.detaintionAmount}"/></td>
                            <td align="center"><c:out value="${csList.revenueTollAmount}"/></td>
                            <td align="center"><c:out value="${csList.revenueWeightmentAmount}"/></td>
                            <td align="center"><c:out value="${csList.revenueOtherAmount}"/></td>

                         <c:set var="totalRevenue" value="${csList.estimatedRevenue + csList.detaintionAmount + csList.revenueTollAmount + csList.revenueWeightmentAmount + csList.revenueOtherAmount}"></c:set>

                                <td align="right" class="text1">

                                    <fmt:formatNumber pattern="##0.00" value="${totalRevenue}"/>
                                </td>

                            <td align="center" class="text1"><c:out value="${csList.dieselQty}"/></td>
                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.tripFuelAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.tollAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.foodingAmount}"/></td>


                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.behatiAmount}"/></td>
                            <td align="center" class="text1"><c:out value="${0}"/></td>
                            <td align="center" class="text1"><c:out value="${csList.miscAmount}"/></td>

                            <c:set var="TTExpense" value="${0+csList.tripFuelAmount+csList.tollAmount+csList.foodingAmount+0+0+0+0+0+0+0+csList.behatiAmount+0+csList.miscAmount}"></c:set>

                                <td align="right" class="text1">
                                    <fmt:formatNumber pattern="##0.00" value="${TTExpense}"/>
                                </td>
                            <td align="center" class="text1">
                                <c:if test = "${csList.movementType == 'Repo' }">
                                    <c:if test = "${unrecoverablecost > 0 }">
                                        <c:out value="${unrecoverablecost}"/>
                                    </c:if>
                                </c:if>
                            </td>
                            <c:set var="Profit" value="${ totalRevenue - TTExpense}" ></c:set>
                                <td align="right" class="text1">
                                <%--<c:if test = "${Profit > 0 }">--%>
                                <%--<c:out value="${Profit}"/>--%>
                                <%--</c:if>--%>
                                <c:if test = "${csList.movementType != 'Repo' }">
<!--                                    <c:out value="${Profit}"/>&nbsp; (<c:out value="${Profit*100/totalRevenue}"/> %)-->
                                    <fmt:formatNumber pattern="##0.00" value="${Profit}"/>&nbsp;(<fmt:formatNumber pattern="##0.00" value="${Profit*100/totalRevenue}"/>%)
                                </c:if>
                            </td>

                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</html>
