
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>

<script type="text/javascript">
    function submitPage(value) {

        if (value == 'Update') {
            document.repair.action = '/throttle/repairAndMaintanance.do';
            document.repair.submit();
        }
    }

</script>       

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.PrimaryOperation22"  text="Repair and Maintanance Approval"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.PrimaryOperation"  text="PrimaryOperation"/></a></li>
            <li class="active"><c:out value="${menuPath}"/></li>
        </ol>
    </div>
</div>

<script>
    function setValue() {

        var selectedTrip = document.getElementsByName('selectedIndex');
        var temp = "";
        var cntr = 0;
        for (var i = 0; i < selectedTrip.length; i++) {
            if (selectedTrip[i].checked == true) {
                 document.getElementById('selectedIndexs'+i).value = 1;
            }else{
              document.getElementById('selectedIndexs'+i).value = 0;  
            }
        }
    }
    function searchPage(val) {

        var selectedTrip = document.getElementsByName('selectedIndex');
        var tripExpenseId = document.getElementsByName('tripExpenseId');
        var approvStatus = document.getElementsByName('approvStatus');
        var temp = "";
        var cntr = 0;
        for (var i = 0; i < selectedTrip.length; i++) {
//            alert("i="+i);
            if (selectedTrip[i].checked == true) {
                 document.getElementById('selectedIndexs'+i).value = 1;
//                alert("selected :"+selectedTrip[i].value);
//                alert("trip expense id :"+tripExpenseId[i].value);
//                alert("approvStatus"+approvStatus[i].value);
                if (cntr == 0) {
                    temp = tripExpenseId[i].value;
                } else {
                    temp = temp + "," + tripExpenseId[i].value;
                }
                cntr++;
            }else{
              document.getElementById('selectedIndexs'+i).value = 0;  
            }
        }

        if (temp != "") {
//            alert("Hai:"+temp); 
            document.repair.action = "/throttle/repairAndMaintanance.do?param=" + val + "&tripExpenseIds=" + temp;
            document.repair.submit();

        } else {
            alert("Please select any one and proceed....");
        }

    }

//     function searchPage(val) {
//        alert("valueee"+val)
//        var temp = "";
//        var selectedTrip = document.getElementsByName('selectedIndex');
//        var tripExpenseId = document.getElementsByName('tripExpenseId');
//        var cntr = 0;
//        for (var i = 0; i < selectedTrip.length; i++) {
//            alert("i="+i);
//            if (selectedTrip[i].checked == true) {
//                alert("selected :"+selectedTrip[i]);
//                alert("trip expense id :"+tripExpenseId[i]);
//                if (cntr == 0) {
//                    temp = tripExpenseId[i].value;
//                } else {
//                    temp = temp + "," + tripExpenseId[i].value;
//                }
//                cntr++;
//            }
//        }
//
//        if (cntr > 0 || val == 'Update') {
//            alert("mmmmm=="+temp);
//            alert("val=="+temp);
////            document.repair.action = "/throttle/repairAndMaintanance.do?param=" + val + "&tripExpenseIds=" + temp;
////            document.repair.submit();
//        } else(cntr == 0 ) {
//            alert("Please select trip  and proceed");
//        }
//    }
</script>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="sorter.size(5);
                    setValues();">
                <form name="repair" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>

                    <table  class="table table-info mb30 table-hover"  align="left" style="width:50%;">



                        <tr style="display:none;" >
                            <td>From Date</td>
                            <td>
                                <input name="fromDate" id="fromDate" type="text" class="form-control datepicker" onclick="ressetDate(this);" value="">
                            </td>

                            <td>To Date</td>
                            <td>
                                <input name="toDate" id="toDate" type="text" class="form-control datepicker"  onClick="ressetDate(this);" value="">
                            </td>
                            <td colspan="4" >&nbsp;</td>
                        </tr>



                    </table>


                    <c:if test="${repairAndMaintanance == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>


                    <table class="table table-info mb30 table-bordered" id="table" >
                        <thead >


                            <tr >
                                <th>S.No</th>
                                <th>Trip Code</th>
                                <th>Vehicle Number</th>
                                <th>Driver Name</th>
                                <th>Expense Name </th>
                                <th>Expense Value</th>
                                <th>Status</th>
                                <th>Action</th>

                            </tr>

                        </thead>
                        <c:if test="${repairAndMaintanance != null}">
                            <% int index = 0;
                                    int sno = 1;
                            %>
                            <tbody>

                                <c:forEach items="${repairAndMaintanance}" var="rm">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        <td class="form-control" >
                                            <%=sno%>
                                        </td>
                                        <td> <input type="hidden" id="tripCode" name="tripCode"  /><c:out value="${rm.tripCode}"/></td>
                                        <td> <input type="hidden"  id="vehicleNo" name="vehicleNo" /><c:out value="${rm.vehicleNo}"/></td>
                                        <td> <input type="hidden"  id="driverName" name="driverName" /><c:out value="${rm.driverName}"/></td>
                                        <td> <input type="hidden" id="expenseName" name="expenseName" /><c:out value="${rm.expenseName}"/></td>
                                        <td> <input type="hidden" id="expenseValue" name="expenseValue"/><c:out value="${rm.expenseValue}"/></td>
                                        <td>
                                            <select   id="approvStatus<%=sno - 1%>"  name="approvStatus"  class="form-control" style="width:200px;height:40px;">
                                                <option value=2>Waiting for Approval</option>
                                                <option value=1>Approved</option>
                                                <option value=3>Rejected</option>
                                            </select>
                                        </td>
                                        <td  ><input type="hidden" name="selectedIndexs" id="selectedIndexs<%=sno - 1%>" value="0"/>
                                            <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno - 1%>" value="1"  style="margin-left: 30px" onclick="setValue();"/>
                                            <input type="text" name="tripExpenseId" id="tripExpenseId<%=sno - 1%>" value="<c:out value="${rm.tripExpenseId}"/>"/>
                                            <script>
                                                $("#approvStatus" +<%=sno - 1%>).val('<c:out value="${rm.approvalStatus}"/>');
                                            </script>
                                        </td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>     
                            </tbody>
                        </c:if>
                    </table>
                        <div>
                            <center>
                        <input type="button" class="btn btn-success" name="Update" value="Update" onclick="searchPage(this.name);" style="width:100px;height:35px;">&nbsp;&nbsp;
                            <center>
                        </div>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="loader"></div>

                    <div id="controls"  >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>

                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>


                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

