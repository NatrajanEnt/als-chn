

package ets.domain.goods.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import ets.domain.util.FPLogUtils;
import ets.arch.exception.FPRuntimeException;
import ets.domain.goods.business.GoodsTO;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;
import ets.domain.racks.business.RackTO;
import ets.domain.security.business.SecurityTO;
//import ets.domain.vendor.business.VendorTO;
import java.util.Date;

/**
 *
 * @author vidya
 */
public class GoodsDAO extends SqlMapClientDaoSupport {

   
    private final static String CLASS = "GoodsDAO";
    

   // getGoodsList
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getGoodsList(GoodsTO goodsTO) {
        Map map = new HashMap();
              
            map.put("status", goodsTO.getStatus());
            map.put("goodsFromDate", goodsTO.getGoodsFromDate());
            //////System.out.println("gdType in dao"+goodsTO.getGdType());
            map.put("goodsToDate", goodsTO.getGoodsToDate());
            map.put("gdNo", goodsTO.getGdNo());
            map.put("gdType", goodsTO.getGdType());
            
        ArrayList searchGoodsList = new ArrayList();
        try {
            //////System.out.println("i am in Search GoodsList"+map);
            searchGoodsList = (ArrayList) getSqlMapClientTemplate().queryForList("goods.SearchGoodsList", map);
            //////System.out.println("searchGoodsList"+searchGoodsList.size());
             
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS, "SearchGoodsList", sqlException);
        }
        return searchGoodsList;

    }
    
    // getGoodsList
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getGoodsList() {
        Map map = new HashMap();
        ArrayList getGoodsList = new ArrayList();
        try {
            //////System.out.println("i am in getGoodsList");
            getGoodsList = (ArrayList) getSqlMapClientTemplate().queryForList("goods.GetgoodsList", map);
            
             //////System.out.println("i in doa workorderDate :"+getGoodsList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return getGoodsList;

    }
    
    //getGdTypeList
     /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getGdTypeList() {
        Map map = new HashMap();
        ArrayList outList = new ArrayList();
        try {
            //////System.out.println("i am in getGoodsList");
            outList = (ArrayList) getSqlMapClientTemplate().queryForList("goods.OutList", map);
            
             //////System.out.println("i in doa workorderDate :"+outList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return outList;

    }
    
    //getGdInList
    /**
     * This method used to Get MFR Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public ArrayList getGdInList() {
        Map map = new HashMap();
        ArrayList inList = new ArrayList();
        try {
            //////System.out.println("i am in getGoodsList");
            inList = (ArrayList) getSqlMapClientTemplate().queryForList("goods.InList", map);
            
             //////System.out.println("i in doa workorderDate :"+inList.size());
        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-VND-01", CLASS, "VendorList", sqlException);
        }
        return inList;

    }
    
        /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doProcessSaveGoods(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
 //////System.out.println("i am in save goods ");
        int status = 0;
        try {
            //////System.out.println("i am in save goods");
            Iterator itr = List.iterator();
            GoodsTO goodsTO = null;
            while (itr.hasNext()) {
                goodsTO = (GoodsTO) itr.next();
                 
                map.put("gdId", goodsTO.getGdId());
                map.put("remark", goodsTO.getRemark());
                map.put("userId", UserId);
               
//                //////System.out.println("getDesigId"+designationTO.getDesigId());
//                //////System.out.println("getDesigName"+designationTO.getDesigName());
//                //////System.out.println("getDescription"+designationTO.getDescription());
                status = (Integer) getSqlMapClientTemplate().update("goods.saveGoodsInTime", map);
                 //////System.out.println("insert status - dao :"+status);
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,"getUserAuthorisedFunctions", sqlException);
        }
        return status;



    }
   
     // doProcessSaveVehicleOutTime
    
       /**
     * This method used to Modify Desgintion Details.
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doProcessGoodsOut(ArrayList List, int UserId) {
        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */

        int status = 0;
        try {
            Iterator itr = List.iterator();
            GoodsTO goodsTO = null;
            while (itr.hasNext()) {
                goodsTO = (GoodsTO) itr.next();
                 
                map.put("gdId", goodsTO.getGdId());
                map.put("remark", goodsTO.getRemark());
                map.put("userId", UserId);
                status = (Integer) getSqlMapClientTemplate().update("goods.saveGoods", map);
                
            }

        } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,"getUserAuthorisedFunctions", sqlException);
        }
        return status;



    }
     /**
     * This method used to Insert MFR Details
     *
     * @param request - Http request object.
     *
     * @param command - command object to bind the request values.
     *
     * @throws Exception -Throws when a Exception araises
     */
    public int doGoodsDetails(GoodsTO goodsTO, int UserId) {

        Map map = new HashMap();
        /*
         * set the parameters in the map for sending to ORM
         */
        map.put("userId", UserId);
        map.put("refNo", goodsTO.getRefNo());
        map.put("description", goodsTO.getDescription());

        int status = 0;
        try {
            status = (Integer) getSqlMapClientTemplate().update("goods.insertGoods", map);
            } catch (Exception sqlException) {
            /*
             * Log the exception and propagate to the calling class
             */
            FPLogUtils.fpDebugLog("getUserAuthorisedFunctions Error" + sqlException.toString());
            FPLogUtils.fpErrorLog("sqlException" + sqlException);
            throw new FPRuntimeException("EM-SYS-01", CLASS,
                    "getUserAuthorisedFunctions", sqlException);
        }
        return status;

    }
  
    
    
}
