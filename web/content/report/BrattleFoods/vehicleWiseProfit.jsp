<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<style type="text/css">
    .container {width: 960px; margin: 0 auto; overflow: hidden;}
    .content {width:800px; margin:0 auto; padding-top:50px;}
    .contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

    /* STOP ANIMATION */



    /* Second Loadin Circle */

    .circle1 {
        background-color: rgba(0,0,0,0);
        border:5px solid rgba(100,183,229,0.9);
        opacity:.9;
        border-left:5px solid rgba(0,0,0,0);
        /*	border-right:5px solid rgba(0,0,0,0);*/
        border-radius:50px;
        /*box-shadow: 0 0 15px #2187e7; */
        /*	box-shadow: 0 0 15px blue;*/
        width:40px;
        height:40px;
        margin:0 auto;
        position:relative;
        top:-50px;
        -moz-animation:spinoffPulse 1s infinite linear;
        -webkit-animation:spinoffPulse 1s infinite linear;
        -ms-animation:spinoffPulse 1s infinite linear;
        -o-animation:spinoffPulse 1s infinite linear;
    }

    @-moz-keyframes spinoffPulse {
        0% { -moz-transform:rotate(0deg); }
        100% { -moz-transform:rotate(360deg);  }
    }
    @-webkit-keyframes spinoffPulse {
        0% { -webkit-transform:rotate(0deg); }
        100% { -webkit-transform:rotate(360deg);  }
    }
    @-ms-keyframes spinoffPulse {
        0% { -ms-transform:rotate(0deg); }
        100% { -ms-transform:rotate(360deg);  }
    }
    @-o-keyframes spinoffPulse {
        0% { -o-transform:rotate(0deg); }
        100% { -o-transform:rotate(360deg);  }
    }
</style>
<script>
    $(document).ready(function() {
        $('.ball, .ball1').removeClass('stop');
        $('.trigger').click(function() {
            $('.ball, .ball1').toggleClass('stop');
        });
    });

</script>


<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    //auto com
$(document).ready(function() {
 $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getVehicleNo.do",
                    dataType: "json",
                    data: {
                        vehicleNo: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Vehicle No");
                        $('#vehicleNo').val('');
                        $('#vehicleId').val('');    
                        $('#vehicleNo').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#vehicleNo').val(value);
                $('#vehicleId').val(id);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
 });

//    $(document).ready(function() {
//        // Use the .autocomplete() method to compile the list based on input from user
//        $('#vehicleNo').autocomplete({
//            source: function(request, response) {
//                $.ajax({
//                    url: "/throttle/getRegistrationNo.do",
//                    dataType: "json",
//                    data: {
//                        regno: request.term
//                    },
//                    success: function(data, textStatus, jqXHR) {
//                        var items = data;
//                        response(items);
//                    },
//                    error: function(data, type) {
//                        console.log(type);
//                    }
//                });
//            },
//            minLength: 1,
//            select: function(event, ui) {
//                var value = ui.item.Name;
//                var tmp = value.split('-');
//                $('#vehicleId').val(tmp[0]);
//                $('#vehicleNo').val(tmp[1]);
//                return false;
//            }
//        }).data("ui-autocomplete")._renderItem = function(ul, item) {
//            var itemVal = item.Name;
//            var temp = itemVal.split('-');
//            itemVal = '<font color="green">' + temp[1] + '</font>';
//            return $("<li></li>")
//                    .data("item.autocomplete", item)
//                    .append("<a>" + itemVal + "</a>")
//                    .appendTo(ul);
//        };
//    });


</script>



<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/handleVehicleWiseProfit.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/handleVehicleWiseProfit.do?param=Search';
                document.accountReceivable.submit();
            }
        }
    }
    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
    }

    function viewVehicleDetails(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewCustomerProfitDetails(tripIds) {
//            alert(tripIds);
        var fromDate= $("#fromDate").val();
        var toDate= $("#toDate").val();
        window.open('/throttle/viewCustomerWiseProfitDetailsNew.do?tripId=' + tripIds +'&fromDate=' + fromDate +'&toDate=' + toDate +  "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
    }
</script>


<script>
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<c:if test="${jcList != null}">
    <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');
            setValues();
            getVehicleNos();">
    </c:if>

    <!--	  <span style="float: right">
                    <a href="?paramName=en">English</a>
                    |
                    <a href="?paramName=ar">Ø§Ù„Ø¹Ø±Ø¨ÙŠØ©</a>
              </span>-->

    <style>
        #index td {
            color:white;
        }
    </style>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Vehicle Wise Profitability</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Finance</a></li>
                <li class="active">Vehicle Wise Profitability</li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="setValue();">
                    <form name="accountReceivable" action=""  method="post">

                        <br>
                        <br>
                        <br>
                        <table class="table table-bordered"  align="left"   style="width:60%" >
                            <tr height="30" id="index">
                                <td colspan="4"  style="background-color:#5BC0DE;">

                                    <b><spring:message code="operations.reports.label.VehicleWiseProfitability" text="default text"/></b>
                                </td></tr>

                            <div id="first">
                                <td style="border-color:#5BC0DE;padding:16px;">
                                    <table class="table table-info mb30 table-hover"  style="width:60%" align="left"   >
                                        <tr>
                                            <td><font color="red"></font><spring:message code="operations.reports.label.VehicleNo" text="default text"/></td>
                                            <td height="30">
                                                <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                                                <input type="text" class="form-control" name="vehicleNo" id="vehicleNo"  style="width:250px;height:40px;"  value="<c:out value="${vehicleNo}"/>" class="textbox"/>
                                            </td>
                                            <td><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                            <td height="30"><input name="fromDate" id="fromDate" style="width:250px;height:40px;" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                            <td height="30"><input name="toDate" id="toDate" style="width:250px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>
                                        </tr>
                                        <tr >
                                            <td style="display:none;"><font color="red"></font><spring:message code="operations.reports.label.OperationType" text="default text"/></td>
                                            <td style="display:none;" height="30">
                                                <select name="operationTypeId" id="operationTypeId" class="form-control"  style="width:250px;height:40px;" >
                                                    <option value="0">--<spring:message code="operations.reports.label.Select" text="default text"/>--</option>
                                                    <option value="Primary"><spring:message code="operations.reports.label.Primary" text="default text"/></option>
                                                    <option value="Secondary"><spring:message code="operations.reports.label.Secondary" text="default text"/></option>
                                                    <option value="All" selected><spring:message code="operations.reports.label.All" text="default text"/></option>
                                                </select>
                                                <script>
                                                    document.getElementById("operationTypeId").value = '<c:out value="${operationTypeId}"/>'
                                                </script>
                                            </td>
                                            <td><font color="red"></font><spring:message code="operations.reports.label.FleetCenter" text="default text"/></td>
                                            <td height="30">
                                                <select name="fleetCenterId" id="fleetCenterId" class="form-control"  style="width:250px;height:40px;" >
                                                    <!--<option value="0">--<spring:message code="operations.reports.label.Select" text="default text"/>--</option>-->
                                                    <c:if test="${companyList != null}">
                                                        <c:forEach items="${companyList}" var="cmpList">
                                                            <c:if test="${cmpList.cmpId == companyId}">
                                                              <option value="<c:out value="${cmpList.cmpId}"/>"><c:out value="${cmpList.name}"/></option>
                                                            </c:if>
                                                        </c:forEach>
                                                    </c:if>
                                                </select>
                                                <script>
                                                    document.getElementById("fleetCenterId").value = '<c:out value="${fleetCenterId}"/>'
                                                </script>
                                            </td>
                                            <td><font color="red"></font><spring:message code="operations.reports.label.Profit/Non-Profit" text="default text"/></td>
                                            <td height="30">
                                                <select name="profitType" id="profitType" class="form-control"  style="width:250px;height:40px;" >
                                                    <option value="All"><spring:message code="operations.reports.label.All" text="default text"/></option>
                                                    <option value="Profit"><spring:message code="operations.reports.label.Profitable" text="default text"/></option>
                                                    <option value="nonProfit"><spring:message code="operations.reports.label.Non-Profitable" text="default text"/></option>
                                                </select>
                                                <script>
                                                    document.getElementById("profitType").value = '<c:out value="${profitType}"/>'
                                                </script>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td colspan="6" align="center"><input type="button" class="btn btn-success"  name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);">&nbsp;&nbsp;
                                            <input type="button" class="btn btn-success" name="Search"  id="Search"  value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"></td>
                                        </tr>
                                    </table>
                            </div></div>
                            </td>
                            </tr>
                        </table>


                        <br>
                        <br>
                        <br>
                        <c:if test="${vehicleDetailsList == null && vehicleWiseProfitListSize == null}">
                            <center>
                                <font color="blue"><spring:message code="operations.reports.label.PleaseWaitYourRequestisProcessing" text="default text"/></font>
                                <br>
                                <div class="container">
                                    <div class="content">
                                        <div class="circle"></div>
                                        <div class="circle1"></div>
                                    </div>
                                </div>
                            </center>
                        </c:if>

                        <c:if test="${vehicleDetailsList != null && vehicleWiseProfitList != null}">
                            <table class="table table-info mb30 table-border" border="1" >
                                <thead>
                                    <tr>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.SNo" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.VehicleNo" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.VehicleModel" text="default text"/></th>
                                        <!--                            <th rowspan="2" >MFR Name</th>-->
                                        <th rowspan="2" ><spring:message code="operations.reports.label.Trips" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.Earnings" text="default text"/></th>
                                        <th colspan="5"  style="text-align:center"><spring:message code="operations.reports.label.FixedExpenses" text="default text"/> </th>
                                        <th colspan="5"  style="text-align:center"><spring:message code="operations.reports.label.OperationExpenses" text="default text"/> </th>
                                        <th rowspan="2"  style="text-align:center"><spring:message code="operations.reports.label.Perday" text="default text"/><br><spring:message code="operations.reports.label.FixedExpenses" text="default text"/>  </th>
                                        <th rowspan="2"  style="text-align:center"><spring:message code="operations.reports.label.FixedExpenses" text="default text"/> <br><spring:message code="operations.reports.label.fortheReportPeriod" text="default text"/></th>
                                        <th rowspan="2"  style="text-align:center"><spring:message code="operations.reports.label.OperationExpenses" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.Maint" text="default text"/> <br><spring:message code="operations.reports.label.Expenses" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.Nett" text="default text"/>  <br><spring:message code="operations.reports.label.Expenses" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.Profit" text="default text"/></th>
                                        <th rowspan="2" ><spring:message code="operations.reports.label.Profit" text="default text"/>%</th>
                                    </tr> 
                                    <tr>
                                        <td bgcolor="#FFA233"><spring:message code="operations.reports.label.Insurance" text="default text"/></td>
                                        <td bgcolor="#FFA233"><spring:message code="operations.reports.label.RoadTax" text="default text"/> </td>
                                        <td bgcolor="#FFA233"><spring:message code="operations.reports.label.FCAmount" text="default text"/> </td>
                                        <td bgcolor="#FFA233"><spring:message code="operations.reports.label.Permit" text="default text"/> </td>
                                        <td bgcolor="#FFA233"><spring:message code="operations.reports.label.EMI" text="default text"/> </td>
                                        <td bgcolor="#B8FF33"><spring:message code="operations.reports.label.TollAmount" text="default text"/></td>
                                        <td bgcolor="#B8FF33"><spring:message code="operations.reports.label.FuelAmount" text="default text"/> </td>
                                        <td bgcolor="#B8FF33"><spring:message code="operations.reports.label.OtherExpAmount" text="default text"/> </td>
                                        <td bgcolor="#B8FF33"><spring:message code="operations.reports.label.Driver/CleanerSalary" text="default text"/> </td>
                                        <td bgcolor="#B8FF33"><spring:message code="operations.reports.label.Driver(Incentive" text="default text"/>,<br><spring:message code="operations.reports.label.Bata,Misc)" text="default text"/></td>
                                    </tr>
                                </thead>
                                <% int index = 0,sno = 1;%>
                                <c:set var="totalTrip" value="${0}"/>    
                                <c:set var="totalIncome" value="${0}"/>    
                                <c:set var="totalFixedExpense" value="${0}"/>    
                                <c:set var="totalOperationExpense" value="${0}"/>    
                                <c:set var="totalNetExpense" value="${0}"/>    
                                <c:set var="totalProfitGained" value="${0}"/>    
                                <c:forEach items="${vehicleDetailsList}" var="veh">
                                    <c:if test="${vehicleWiseProfitList != null}">
                                        <c:forEach items="${vehicleWiseProfitList}" var="profitList">
                                            <c:if test="${veh.vehicleId == profitList.vehicleId}">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <tbody>
                                                    <c:set var="profit" value="${0}"/>    
                                                    <c:set var="profitPercent" value="${0}"/>
                                                    <c:set var="countPercent" value="${0}"/>
                                                    <c:if test="${profitType == 'All'}">
                                                        <tr>
                                                            <td  class="<%=classText%>" rowspan="2" align="center"><font color="#050404"><%=sno++%></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" ><a href="#" onclick="viewVehicleDetails('<c:out value="${veh.vehicleId}"/>')"><font color="#050404"><c:out value="${veh.regNo}"/></font></a></td>
                                                            <td  class="<%=classText%>"  rowspan="2" ><font color="#050404"><c:out value="${veh.modelName}"/></font></td>
                <!--                                            <td class="<%=classText%>" rowspan="2" ><c:out value="${veh.mfrName}"/></td>-->
                                                            <c:if test="${profitList.tripNos > 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="center"><a href="#" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><font color="#050404"><c:out value="${profitList.tripNos}"/></font></a></td>
                                                                </c:if>
                                                                <c:if test="${profitList.tripNos == 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="center"><font color="#050404"><c:out value="${profitList.tripNos}"/></font></td>
                                                            </c:if>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><c:out value="${profitList.freightAmount}"/></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tripOtherExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></font></td>
                                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                                </c:if>
                                                                <c:if test="${profitList.netProfit gt 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                                </c:if>
                                                                <c:if test="${profitList.freightAmount != '0.00'}">
                                                                    <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                                                    <c:set var="profitPercent" value="${profit*100}"/>
                                                                </c:if>    
                                                                <c:if test="${profitPercent <= 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                                                </c:if>  
                                                                <c:if test="${profitPercent > 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                                                </c:if>  
                                                                <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                                                <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                                                <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                                                <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                                                <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                                                <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                                                <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                                                <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                                                <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                                                <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                                                <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                                                <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                                                <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                                                <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                                                <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/>    
                                                                <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>    
                                                                <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                                                <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/>  
                                                                <c:set var="totalProfitPercentValue" value="${profitPercent + totalProfitPercentValue}"/>  
                                                        </tr>
                                                    </c:if>    
                                                    <c:if test="${profitType == 'Profit' && profitList.netProfit > 0}">
                                                        <tr>
                                                            <td  class="<%=classText%>" rowspan="2" align="center"><font color="#050404"><%=sno++%></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" ><a href="#" onclick="viewVehicleDetails('<c:out value="${veh.vehicleId}"/>')"><font color="#050404"><c:out value="${veh.regNo}"/></font></a></td>
                                                            <td  class="<%=classText%>"  rowspan="2" align="center"><font color="#050404"><c:out value="${veh.modelName}"/></font></td>
                <!--                                            <td class="<%=classText%>" rowspan="2" align="center"><c:out value="${veh.mfrName}"/></td>-->
                                                            <c:if test="${profitList.tripNos > 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="center"><a href="#" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><font color="#050404"><c:out value="${profitList.tripNos}"/></font></a></td>
                                                                </c:if>
                                                                <c:if test="${profitList.tripNos == 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="center"><font color="#050404"><c:out value="${profitList.tripNos}"/></font></td>
                                                            </c:if>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><c:out value="${profitList.freightAmount}"/></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.miscAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></font></td>
                                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                                </c:if>
                                                                <c:if test="${profitList.netProfit gt 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                                </c:if>
                                                                <c:if test="${profitList.freightAmount != '0.00'}">
                                                                    <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                                                    <c:set var="profitPercent" value="${profit*100}"/>
                                                                </c:if>
                                                                <c:if test="${profitPercent <= 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                                                </c:if>       
                                                                <c:if test="${profitPercent > 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                                                </c:if>  
                                                                <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                                                <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                                                <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                                                <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                                                <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                                                <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                                                <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                                                <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                                                <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                                                <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                                                <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                                                <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                                                <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                                                <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                                                <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/>    
                                                                <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                                                <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                                                <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 
                                                                <c:set var="totalProfitPercentValue" value="${profitPercent + totalProfitPercentValue}"/>
                                                        </tr>
                                                    </c:if>
                                                    <c:if test="${profitType == 'nonProfit' && profitList.netProfit <= 0}">
                                                        <tr>
                                                            <td  class="<%=classText%>" rowspan="2" align="center"><font color="#050404"><%=sno++%></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" ><a href="#" onclick="viewVehicleDetails('<c:out value="${veh.vehicleId}"/>')"><font color="#050404"><c:out value="${veh.regNo}"/></font></a></td>
                                                            <td  class="<%=classText%>"  rowspan="2" align="center"><font color="#050404"><c:out value="${veh.modelName}"/></font></td>
                <!--                                            <td class="<%=classText%>" rowspan="2" align="center"><c:out value="${veh.mfrName}"/></td>-->
                                                            <c:if test="${profitList.tripNos > 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="center"><a href="#" onclick="viewCustomerProfitDetails('<c:out value="${profitList.tripId}"/>');"><font color="#050404"><c:out value="${profitList.tripNos}"/></font></a></td>
                                                                </c:if>
                                                                <c:if test="${profitList.tripNos == 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="center"><font color="#050404"><c:out value="${profitList.tripNos}"/></font></td>
                                                            </c:if>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><c:out value="${profitList.freightAmount}"/></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.insuranceAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.roadTaxAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fcAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.permitAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.emiAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.tollAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fuelAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.miscAmount}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.vehicleDriverSalary}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.driverExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.fixedExpensePerDay}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalFixedExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.totlalOperationExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.maintainExpense}" /></font></td>
                                                            <td  class="<%=classText%>" rowspan="2" align="right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netExpense}" /></font></td>
                                                            <c:if test="${profitList.netProfit lt 0 || profitList.netProfit eq 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                                </c:if>
                                                                <c:if test="${profitList.netProfit gt 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitList.netProfit}" /></font></td>
                                                                </c:if>
                                                                <c:if test="${profitList.freightAmount != '0.00'}">
                                                                    <c:set var="profit" value="${profitList.netProfit/profitList.freightAmount}"/>    
                                                                    <c:set var="profitPercent" value="${profit*100}"/>
                                                                </c:if>
                                                                <c:if test="${profitPercent <= 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                                                </c:if>       
                                                                <c:if test="${profitPercent > 0}">
                                                                <td  class="<%=classText%>" rowspan="2" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${profitPercent}" />&nbsp;%</font></td>
                                                                </c:if> 
                                                                <c:set var="totalTrip" value="${profitList.tripNos + totalTrip}"/>    
                                                                <c:set var="totalIncome" value="${profitList.freightAmount + totalIncome}"/>    
                                                                <c:set var="totalInsurance" value="${profitList.insuranceAmount + totalInsurance}"/>    
                                                                <c:set var="totalRoadTax" value="${profitList.roadTaxAmount + totalRoadTax}"/>    
                                                                <c:set var="totalFcAmount" value="${profitList.fcAmount + totalFcAmount}"/>    
                                                                <c:set var="totalPermitAmount" value="${profitList.permitAmount + totalPermitAmount}"/>    
                                                                <c:set var="totalEmiAmount" value="${profitList.emiAmount + totalEmiAmount}"/>    
                                                                <c:set var="totalTollAmount" value="${profitList.tollAmount + totalTollAmount}"/>    
                                                                <c:set var="totalFuelAmount" value="${profitList.fuelAmount + totalFuelAmount}"/>    
                                                                <c:set var="totalOtherExpenseAmount" value="${profitList.tripOtherExpense + totalOtherExpenseAmount}"/>    
                                                                <c:set var="totalDriverSalaryAmount" value="${profitList.vehicleDriverSalary + totalDriverSalaryAmount}"/>    
                                                                <c:set var="totalDriverExpense" value="${profitList.driverExpense + totalDriverExpense}"/>    
                                                                <c:set var="totalFixedExpensePerDay" value="${profitList.fixedExpensePerDay + totalFixedExpensePerDay}"/>    
                                                                <c:set var="totalFixedExpense" value="${profitList.totlalFixedExpense + totalFixedExpense}"/>    
                                                                <c:set var="totalOperationExpense" value="${profitList.totlalOperationExpense + totalOperationExpense}"/> 
                                                                <c:set var="totalMaintainExpense" value="${profitList.maintainExpense + totalMaintainExpense}"/>  
                                                                <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                                                                <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/>  
                                                        </tr>
                                                    </c:if>
                                                </tbody>
                                            </c:if>

                                        </c:forEach>
                                    </c:if>
                                    <%
                               index++;
                                    %>
                                </c:forEach>
                                <tr>
                                    <td colspan="3"  style="text-align:center"><font color="#050404"><spring:message code="operations.reports.label.Total" text="default text"/></font></td>
                                    <td   style="text-align:center"><font color="#050404"><c:out value="${totalTrip}"/></font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalIncome}" /></font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalInsurance}" /></font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalRoadTax}" /></font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFcAmount}" /></font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalPermitAmount}" /></font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalEmiAmount}" /></font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalTollAmount}" /> </font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFuelAmount}" /> </font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOtherExpenseAmount}" /></font> </td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDriverSalaryAmount}" /> </font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalDriverExpense}" /></font> </td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpensePerDay}" /></font> </td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpense}" /></font> </td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOperationExpense}" /> </font></td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalMaintainExpense}" /></font> </td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetExpense}" /></font> </td>
                                    <td   style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                                    <c:set var="totalProfitPercentValue1" value="${0}"/>
                                    <c:set var="totalProfitPercentValue" value="${0}"/>
                                    <c:if test="${totalIncome != 0}">
                                        <c:set var="totalProfitPercentValue1" value="${totalProfitGained / totalIncome}"/>
                                        <c:set var="totalProfitPercentValue" value="${totalProfitPercentValue1 * 100}"/>
                                    </c:if>
                                    <td  style="text-align:right"><font color="#050404"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercentValue}" /> %</font></td>
                                </tr>                   
                            </table>
                        </c:if>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <c:if test="${vehicleDetailsList != null && vehicleWiseProfitList != null}">

                            <c:set var="totalNetExpense" value="${profitList.netExpense + totalNetExpense}"/>    
                            <c:set var="totalProfitGained" value="${profitList.netProfit + totalProfitGained}"/> 

                            <table border="2" style="border: 1px solid #666666;"  align="center"  cellpadding="0" cellspacing="1" >
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalTripsCarriedOut" text="default text"/>
                                    </td>
                                    <td width="150" align="right"><c:out value="${totalTrip}"/></td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalIncome" text="default text"/>
                                    </td>
                                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalIncome}" /></td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalFixedExpenses" text="default text"/>
                                    </td>
                                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalFixedExpense}" /> </td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalOperationExpenses" text="default text"/>
                                    </td>
                                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalOperationExpense}" /> </td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalNettExpenses" text="default text"/></td>
                                    <td width="150" align="right"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalNetExpense}" /> </td>
                                </tr>
                                <tr height="25">
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalProfitgained" text="default text"/></td>
                                    <c:if test="${totalProfitGained lt 0 || totalProfitGained eq 0}">
                                        <td width="150" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                                        </c:if>
                                        <c:if test="${totalProfitGained gt 0 }">
                                        <td width="150" align="right"><font color="green"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitGained}" /> </font></td>
                                        </c:if>
                                </tr>
                                <tr height="25">
                                    <c:set var="totalProfit" value="${0}"/>    
                                    <c:set var="totalProfitPercent" value="${0}"/>
                                    <c:if test="${totalIncome > 0}">
                                        <c:set var="totalProfit" value="${totalProfitGained/totalIncome}"/>    
                                        <c:set var="totalProfitPercent" value="${totalProfit*100}"/>
                                    </c:if>
                                    <td style="background-color: #6374AB; color: #ffffff"><spring:message code="operations.reports.label.TotalProfitgained" text="default text"/> &nbsp;%</td>
                                    <c:if test="${totalProfitPercent lt 0 || totalProfitPercent eq 0}">
                                        <td width="150" align="right"><font color="red"><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" /> &nbsp;%</font></td>
                                        </c:if>
                                        <c:if test="${totalProfitPercent gt 0 }">
                                        <td width="150" align="right"><font color="green" ><fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${totalProfitPercent}" /> &nbsp;%</font></td>
                                        </c:if>
                                </tr>
                            </table>
                        </c:if>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                </body>    
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>