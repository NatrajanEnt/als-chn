
/*---------------------------------------------------------------------------
 * ProcessAttendance.java
 * Jul 28, 2008
 *
 * Copyright (c) ETS.
 * All Rights Reserved.
 *
 * This software is the confidential and proprietary information of
 * ETS ("Confidential Information"). You shall
 * not disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with ETS.
 ----------------------------------------------------------------------------*/
/**
 * ****************************************************************************
 *
 * Modification Log:
 * ---------------------------------------------------------------------------
 * Ver Date Author Change
 * ---------------------------------------------------------------------------
 * 1.0 Jul 28, 2008 SriniR	Created
 *
 *************************************************************************
 */
import java.io.*;
import java.util.*;
import java.text.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.*;

/**
 * ProcessAttendance: The business process class that processes the attendance
 * data.
 *
 * @author Srinivasan.R
 * @version 1.0 28 Jul 2008
 */
public class updateVehicleSequenceId {

    /**
     * retriveJobStatus method used to get the jobs status from the factory and
     * updates in the factory portal.
     *
     * @return boolean
     *
     * @throws Exception
     */
    public static void main(String[] args) {
        System.out.println("Update Records Example...");
        Connection con = null;
        Statement statement = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        String url = "jdbc:mysql://203.124.105.244:3306/";
//        String url = "jdbc:mysql://localhost:3306/";
        String dbName = "throttlebrattle";
        String driverName = "com.mysql.jdbc.Driver";
        String userName = "root";
        String password = "etsadmin";
//        String password = "admin";
        try {
            Class.forName(driverName);

// Connecting to the database
            con = DriverManager.getConnection(url + dbName, userName, password);
            try {
                PreparedStatement vehicleRegNo = null;
                PreparedStatement tripVehicle = null;
                statement = con.createStatement();

// updating records
                String sql = "UPDATE ts_trip_vehicle SET vehicle_sequence=? WHERE trip_Id=? and active_Ind = ? and vehicle_sequence is null ";
                System.out.println("Updated successfully");
                vehicleRegNo = con.prepareStatement(sql);
                String sql1 = "select trip_Id from ts_trip_master where trip_status_Id > 8 and trip_type = 1 order by trip_Id ";
                String sql2 = "select active_Ind from ts_trip_vehicle where vehicle_sequence is null and trip_Id = ? ";
                tripVehicle = con.prepareStatement(sql2);
                rs = statement.executeQuery(sql1);
                System.out.println("----------------------");
                int i=1;
                int updateStatus = 0;
                while (rs.next()) {
                    String tripId = rs.getString(1);
                    tripVehicle.setString(1,tripId);
                    rs1 = tripVehicle.executeQuery();
                    int size = 0;
                    String activeInd = "";
                    while(rs1.next()){
                    size++;
                    activeInd = rs1.getString(1);
                    i++;
                    }
                    System.out.println("size = " + size);
                    if(size == 1){
                        vehicleRegNo.setInt(1,1);
                        vehicleRegNo.setString(2,tripId);
                        vehicleRegNo.setString(3,"Y");
                        updateStatus = vehicleRegNo.executeUpdate();
                    }else if(size == 2){
                        vehicleRegNo.setInt(1,1);
                        vehicleRegNo.setString(2,tripId);
                        vehicleRegNo.setString(3,"N");
                        updateStatus = vehicleRegNo.executeUpdate();
                        vehicleRegNo.setInt(1,2);
                        vehicleRegNo.setString(2,tripId);
                        vehicleRegNo.setString(3,"Y");
                        updateStatus = vehicleRegNo.executeUpdate();
                    }
                }
                System.out.println("totalRecordsUpdate = " + i);

            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("Table doesn't exist.");
            }
            con.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
