<%-- 
    Document   : consignorAddressDetails
    Created on : May 13, 2016, 3:25:31 PM
    Author     : Gulshan kumar
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>
    </head>


    <script type="text/javascript">

        function SetName() {
            if (window.opener != null && !window.opener.closed) {

                var txtName = window.opener.document.getElementById("consignorAddress");

                if (document.getElementById("entryType1").checked == true) {
                    txtName.value = document.getElementById("custAddress").value;
                    
                }
                var txtName2 = window.opener.document.getElementById("consignorAddress");
                if (document.getElementById("entryType2").checked == true) {
                    txtName2.value = document.getElementById("custAddresstwo").value;
                }
                var txtName3 = window.opener.document.getElementById("consignorAddress");
                 if (document.getElementById("entryType3").checked == true) {
                    txtName3.value = document.getElementById("custAddressthree").value;
                }
            }
            window.close();
        }

    </script>


 <div class="pageheader">
        <h2><i class="fa fa-edit"></i> Address Details</h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Sales</a></li>
            <li class="active">Consignment Note</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body >
        <form name="manufacturer"  method="post" >
            <%@ include file="/content/common/message.jsp" %>
            <br>
           <table  class="table table-default mb30 table-hover">
               <thead>
                    <tr>
                    <th colspan="4" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"  height="30">Select Customer Address</th>
                    </tr>
            </thead>
                <c:forEach items="${ConsignorAddressDetails}" var="Address">
                    <tr>
                        <td  width="350" height="30">&nbsp;&nbsp;&nbsp;<font color="red">*</font>Customer Name</td>
                        <td  height="30"><input name="custName" id="custName" type="text" class="textbox" value="<c:out value="${Address.customerName}"/>" readonly maxlength="50"></td>
                    </tr>

                    <tr>
                        <td  height="30"><font color="red">*</font>Customer Address</td>
                        <td  height="30">
                            <textarea name="custAddress" id="custAddress" readonly ><c:out value="${Address.custAddress}"/></textarea>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="entryType1"  id="entryType1" value="1" >
                        </td>
                    </tr>

                    <tr>
                        <td  height="30">&nbsp;Customer Address 2</td>
                        <td  height="30" >
                            <textarea name="custAddresstwo" id="custAddresstwo" readonly ><c:out value="${Address.custAddressTwo}"/></textarea>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="entryType2"  id="entryType2" value="2"  >
                        </td>
                    </tr>

                    <tr>
                        <td  height="30">&nbsp;Customer Address 3</td>
                        <td  height="30">
                            <textarea name="custAddressthree" id="custAddressthree" readonly ><c:out value="${Address.custAddressthree}"/></textarea>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="entryType3"  id="entryType3" value="3" >
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <br>
            <center>
                <input type="button" value="Set" id="set" class="btn btn-info" onClick="SetName();">
            </center>
        </form>
    </body>
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

