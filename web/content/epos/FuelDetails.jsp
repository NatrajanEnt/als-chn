<%
        if(session == null) {
            response.sendRedirect("login.do");
        }
        String user = "101";        
        Date today = new Date();
        SimpleDateFormat sdftSql = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Connection conn = null;
        int status = 0, aStatus = 0, fStatus = 0;
        try{
            //IdentityNo,DeviceId,TripId,FuelStationCode,Liters,datetime
            String tripId = request.getParameter("TripId");
            String identityNo = request.getParameter("IdentityNo");
            String deviceId = request.getParameter("DeviceId");
            String fuelLocatId = request.getParameter("FuelStationCode");
            String liters = request.getParameter("Liters");
            String datetime = request.getParameter("datetime"); //07-08-2012$15:48:46&
            String date = "";
            String time = "";
            if(datetime != null && datetime.contains("$")) {
                date = datetime.split("\\$")[0];
                time = datetime.split("\\$")[1];
                date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
                datetime = date+" "+time;
            } else if(datetime != null && datetime.contains(" ")) {
                date = datetime.split(" ")[0];
                time = datetime.split(" ")[1];
                date = date.split("-")[2]+"-"+date.split("-")[1]+"-"+date.split("-")[0];
                datetime = date+" "+time;
            }else {
                datetime = sdftSql.format(today);
            }

            String Remarks = request.getParameter("Remarks");


            String applicationPath = application.getRealPath("WEB-INF/classes");
            applicationPath = applicationPath.replace("\\", "/");
            java.io.FileInputStream fis = new java.io.FileInputStream(applicationPath+"/jdbc_url.properties");
            java.util.Properties props = new java.util.Properties();
            props.load(fis);
            String driverClass = props.getProperty("jdbc.driverClassName");
            String jdbcUrl = props.getProperty("jdbc.url");
            String userName = props.getProperty("jdbc.username");
            String password = props.getProperty("jdbc.password");
            Class.forName(driverClass);
            conn = DriverManager.getConnection(jdbcUrl, userName, password);
            
            //tripsheetid, bunkname, place, filldate, totalliters, totalamount, ermarks, createddate
	    	String[] temp = null, temp1 = null;
		temp = datetime.split(" ");
		String sDate = temp[0];
		
		Statement stmt = conn.createStatement();
		ResultSet SAR = stmt.executeQuery("SELECT status FROM ra_trip_open_close_epos where TripId="+tripId+" and IdentityNo="+identityNo);
		String tripStatus="";
		while(SAR.next()){
			tripStatus = SAR.getString("status");
		}
		if(tripStatus.equals("Open") || tripStatus.equals("Close")){		
			Statement stmt1 = conn.createStatement();
			ResultSet rs1 = stmt1.executeQuery("SELECT Fuel_Locat_Name, Bunk_Name, State,currentrate FROM ra_fuel_location_master_epos flm, ra_bunk_details rbd "
			+ "where flm.Fuel_Locat_Id=rbd.bunkid and flm.Fuel_Locat_Id="+fuelLocatId);
			String locationName="", bunkName="", state="", currentRate="";
			while(rs1.next()){
				locationName = rs1.getString("Fuel_Locat_Name");
				bunkName = rs1.getString("Bunk_Name");
				state = rs1.getString("State");
				currentRate = rs1.getString("currentrate");						
			}
			if(currentRate.equals("")){currentRate="0";}
			FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Fuel liters --> "+liters));
			FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Fuel currentRate --> "+currentRate));
			float dAmount = (Float.parseFloat(liters) * Float.parseFloat(currentRate));
			FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Fuel --> "+dAmount));

			PreparedStatement pstmt1 = conn.prepareStatement("INSERT INTO tripfueldetails "
			+ "(tripsheetid, bunkname, place, filldate, totalliters, totalamount, remarks, createddate) "
			+ " VALUES (?,?,?,?,?,?,?,now())");
			pstmt1.setString(1, tripId);
			pstmt1.setString(2, bunkName);
			pstmt1.setString(3, locationName);
			pstmt1.setString(4, datetime);
			pstmt1.setString(5, liters);
			pstmt1.setString(6, String.valueOf(dAmount));
			pstmt1.setString(7, "from EPOS");
			fStatus = pstmt1.executeUpdate();            
            
		    PreparedStatement pstmt = conn.prepareStatement("INSERT INTO ra_trip_fuel_epos "
			    + "( TripId, IdentityNo, DeviceId, Fuel_Locat_Id, "
			    + "Liters, FuelAmount, Place, Remarks, FuelDateTime, Act_Ind, CreatedBy, CreatedDate) VALUES (?,?,?,?,?,?,?,?,?,?,?,now())");
		    pstmt.setString(1, tripId);
		    pstmt.setString(2, identityNo);
		    pstmt.setString(3, deviceId);
		    pstmt.setString(4, fuelLocatId);
		    pstmt.setString(5, liters);
		    pstmt.setString(6, String.valueOf(dAmount));
		    pstmt.setString(7, locationName);
		    pstmt.setString(8, "from EPOS");
		    pstmt.setString(9, datetime);
		    pstmt.setString(10, "Y");
		    pstmt.setString(11, user);
		    status = pstmt.executeUpdate();

                    if(status==1){
                        Statement stmt2 = conn.createStatement();
                        ResultSet rs2 = stmt.executeQuery("SELECT totalliters,totalamount FROM ra_trip_open_close_epos where TripId="+tripId);
                        String tLiters="", tFuAmount="";
                        while(rs2.next()){
                            tLiters = rs2.getString("totalliters");
                            tFuAmount = rs2.getString("totalamount");
                        }
                        if(("").equals(tLiters) || tLiters==null){tLiters="0";}
                        if(("").equals(tFuAmount) || tFuAmount==null){tFuAmount="0";}
                        double tFuelLiters = Double.parseDouble(tLiters) + Double.parseDouble(liters);
                        double tFuelAmount = Double.parseDouble(tFuAmount) + dAmount;

                        PreparedStatement pstmt2 = conn.prepareStatement("UPDATE ra_trip_open_close_epos SET totalliters=?, totalamount=? WHERE  TripId=?");
			pstmt2.setDouble(1, tFuelLiters);
			pstmt2.setDouble(2, tFuelAmount);
			pstmt2.setString(3, tripId);
			aStatus = pstmt2.executeUpdate();
                    }
		    out.print((status+"$").trim());            
             }
             else{out.println("0$");}
            if(conn != null) {
                conn.close();
            }

        }catch(Exception e) {
            out.println("0$");
            //System.out.println("Exception in EPOS Advance "+e.getMessage());
            FPLogUtils.fpErrorLog((new StringBuilder()).append("Exception in EPOS Fuel --> ").append(e.getMessage()).toString());
        }
        finally{
            if(conn != null) {
                conn.close();
            }
        }
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.sql.*,java.text.SimpleDateFormat,java.util.Date,ets.domain.util.FPLogUtils" %>