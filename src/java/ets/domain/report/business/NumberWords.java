/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ets.domain.report.business;

import java.text.DecimalFormat;

import java.text.NumberFormat;
/**
 *
 * @author parveen
 */
public class NumberWords {
   
  private String numberInWords = "";  
  private String roundedValue = "";
  private static final String[] tensNames = {
    "",
    " Ten",
    " Twenty",
    " Thirty",
    " Forty",
    " Fifty",
    " Sixty",
    " Seventy",
    " Eighty",
    " Ninety"
  };

  private static final String[] numNames = {
    "",
    " One",
    " Two",
    " Three",
    " Four",
    " Five",
    " Six",
    " Seven",
    " Eight",
    " Nine",
    " Ten",
    " Eleven",
    " Twelve",
    " Thirteen",
    " Fourteen",
    " Fifteen",
    " Sixteen",
    " Seventeen",
    " Eighteen",
    " Nineteen"
  };

    public String getNumberInWords() {
        return numberInWords;
    }

    public void setNumberInWords(String numberInWords) {
        String[] temp = new String[2];
        numberInWords = numberInWords.replace(".", "-");
        
        temp = numberInWords.split("-");        
        this.numberInWords = convert(Long.parseLong(temp[0]));
        if(Integer.parseInt(temp[1])!=0 ){
        this.numberInWords = this.numberInWords + " and "+ convert(Long.parseLong(temp[1]))+ " Paise";
        }
        
    }


    public String getRoundedValue() {
        return roundedValue;
    }

    public void setRoundedValue(String roundedValue) {
        //////System.out.println("roundedValue first"+roundedValue);

        roundedValue = roundedValue.replace(".","-");
        String fin = "";

        String[] test1 = roundedValue.split("-");

        if(test1.length > 1){
                fin = test1[0] + "." + test1[1].substring(0,1)+"0";

        }else{
                fin = test1[0] +".00";
        }
        //////System.out.println("last "+fin);
        this.roundedValue = fin;
//        NumberFormat formatter = new DecimalFormat("#0.00");
//        this.roundedValue=""+formatter.format(java.lang.Math.ceil(Double.valueOf(roundedValue)));
//        //////System.out.println("This rounded value"+roundedValue);
    }    
    
    
  
  private static String convertLessThanOneThousand(int number) {
    String soFar;

    if (number % 100 < 20){
      soFar = numNames[number % 100];
      number /= 100;
    }
    else {
      soFar = numNames[number % 10];
      number /= 10;

      soFar = tensNames[number % 10] + soFar;
      number /= 10;
    }
    if (number == 0) return soFar;
    return numNames[number] + " Hundred" + soFar;
  }


  public static String convert(long number) {
    // 0 to 999 999 999 999
    if (number == 0) { return "Zero"; }

    String snumber = Long.toString(number);

    // pad with "0"
    String mask = "000000000000";
    DecimalFormat df = new DecimalFormat(mask);
    snumber = df.format(number);

    // XXXnnnnnnnnn 
    int billions = Integer.parseInt(snumber.substring(0,3));
    // nnnXXXnnnnnn
    int millions  = Integer.parseInt(snumber.substring(3,6)); 
    // nnnnnnXXXnnn
    int hundredThousands = Integer.parseInt(snumber.substring(6,9)); 
    // nnnnnnnnnXXX
    int thousands = Integer.parseInt(snumber.substring(9,12));    

    String tradBillions;
    switch (billions) {
    case 0:
      tradBillions = "";
      break;
    case 1 :
      tradBillions = convertLessThanOneThousand(billions) 
      + " Billion ";
      break;
    default :
      tradBillions = convertLessThanOneThousand(billions) 
      + " Billion ";
    }
    String result =  tradBillions;

    String tradMillions;
    switch (millions) {
    case 0:
      tradMillions = "";
      break;
    case 1 :
      tradMillions = convertLessThanOneThousand(millions) 
      + " Million ";
      break;
    default :
      tradMillions = convertLessThanOneThousand(millions) 
      + " Million ";
    }
    result =  result + tradMillions;

    String tradHundredThousands;
    switch (hundredThousands) {
    case 0:
      tradHundredThousands = "";
      break;
    case 1 :
      tradHundredThousands = "One Thousand ";
      break;
    default :
      tradHundredThousands = convertLessThanOneThousand(hundredThousands) 
      + " Thousand ";
    }
    result =  result + tradHundredThousands;

    String tradThousand;
    tradThousand = convertLessThanOneThousand(thousands);
    result =  result + tradThousand;

    // remove extra spaces!
    return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");
  }

  
}



    
    
    
    

