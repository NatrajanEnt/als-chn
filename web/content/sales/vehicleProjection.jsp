<!DOCTYPE html>
<!--<html>-->
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<!--<html>-->
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <title></title>
</head>
<meta http-equiv="refresh" content="300">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<body>
    <div class="pageheader">
        <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Vehicle Projection" text="Vehicle Projection"/> </h2>
        <div class="breadcrumb-wrapper">
            <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
            <ol class="breadcrumb">
                <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
                <li><a href="general-forms.html"><spring:message code="hrms.label.Sales" text="Sales"/></a></li>
                <li class=""><spring:message code="hrms.label.Vehicle Projection" text="Vehicle Projection"/></li>

            </ol>
        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="panel panel-warning panel-stat">
                        <div class="panel-heading">
                            <div class="stat">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="ion ion-android-boat"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <h4> <spring:message code="dashboard.label.Containers"  text="Containers"/></h4>
                                        <h1><span id="containers"></span></h1>
                                        <br>
                                        <font><b>20feet :</b></font><div  id="20fc"></div>

                                        <br>
                                        <br>
                                        <font><b>40feet : <div id="40fc" ></div>
                                        </b> </font>
                                    </div>
                                </div>

                                <div class="mb15"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="panel panel-success panel-stat">
                        <div class="panel-heading">

                            <div class="stat">
                                <div class="row">
                                    <div class="col-xs-4">

                                        <i class="ion ion-android-bus"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <h4> <spring:message code="dashboard.label.Own Vehicles"  text="Own Vehicles"/></h4>
                                        <h1><span id="ownVehicle"></span></h1>
                                        <br>
                                        <font><b>20feet :</b> <div  id="20fv"></div>
                                        </font>
                                        <br>
                                        <br>
                                        <font><b>40feet : </b> <div  id="40fv"></div>
                                        </font>
                                    </div>
                                </div>
                                <div class="mb15"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-6 col-md-3">
                    <div class="panel panel-danger panel-stat">
                        <div class="panel-heading">

                            <div class="stat">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <i class="ion ion-alert-circled"></i>
                                    </div>
                                    <div class="col-xs-8">
                                        <h4> <spring:message code="dashboard.label.Market Vehicle"  text="Market Vehicle"/></h4>
                                        <h1><span id="marketVehicle"></span></h1>
                                        <br>
                                        <font><b>20feet : </b> <div  id="20fvm"></div>
                                        </font>
                                        <br>
                                        <br>
                                        <font><b>40feet : </b> <div  id="40fvm"></div>
                                        </font>
                                    </div>
                                </div>


                                <div class="mb15"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="pnr">
                <center><b>PNR Details</b></center>
                <br>
                <table class="table table-info mb30 table-bordered" id="table" >
                    <thead>
                    <th>S.No</th>
                    <th>PNR No</th>
                    <th>Date</th>
                    <th>20' Container</th>
                    <th>40' Container</th>
                    <th align="center">Select</th>
                    </thead>

                    <c:if test = "${PNRprojection != null}">
                        <% int sno = 0;%>
                        <tbody>
                            <c:forEach items="${PNRprojection}" var="val">

                                <%
                                    sno++;
                                    String className = "text1";
                                    if ((sno % 1) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                                %>

                                <tr>
                                    <td  > <%=sno%></td>
                                    <td  > <c:out value="${val.custOrderRefNo}"/></td>
                                    <td >
                                        <c:out value="${val.date}"/>
                                    </td>
                                    <td >
                                        <input type="hidden" id="twentyContainer" name="twentyContainer" value="<c:out value="${val.twentyTypeContainer - val.twentyPlanned}"/>" >
                                        <c:out value="${val.twentyTypeContainer - val.twentyPlanned}"/>
                                    </td>
                                    <td> 
                                        <input type="hidden" id="fortyContainer" name="fortyContainer" value="<c:out value="${val.fortyTypeContainer - val.fortyPlanned}" />" >
                                        <c:out value="${val.fortyTypeContainer - val.fortyPlanned}" /></td>
                                    <td align="center">
                                        <input type="checkbox" name="selectedIndex" id="selectedIndex<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${val.twentyTypeContainer - val.twentyPlanned}"/>', '<c:out value="${val.fortyTypeContainer - val.fortyPlanned}" />')"   />
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                        <input  type="hidden" name="vehicle" id="vehicle" value="<%=sno%>" />
                    </c:if>
                </table>

                <script>
                    function setValues(sno, twenty, forty) {
                        var vehicle = parseInt(document.getElementById("vehicle").value);
                        var vehicleCountty = document.getElementById("vehicleCount20").value;
                        var vehicleCountfy = document.getElementById("vehicleCount40").value;
                        for (i = 1; i <= vehicle; i++) {
                            if (i != sno) {
                                document.getElementById("selectedIndex" + i).checked = false;
                            } else {
                                document.getElementById("selectedIndex" + i).checked = true;
                            }
                        }
                        
                        if (twenty > vehicleCountty || twenty < vehicleCountty) {
                            var markettwenty = twenty - vehicleCountty;
                           var value = Math.max(0, markettwenty);
                            document.getElementById("20fvm").innerHTML = value;
                        } else {
                            document.getElementById("20fvm").innerHTML = 0;
                        }

                        document.getElementById("20fc").innerHTML = twenty;
                        document.getElementById("20fv").innerHTML = vehicleCountty;
                        document.getElementById("40fc").innerHTML = forty;
                        document.getElementById("40fv").innerHTML = vehicleCountfy;

                        if (forty > vehicleCountfy) {
                            var marketforty = forty - vehicleCountfy;
                            document.getElementById("40fvm").innerHTML = marketforty;
                        } else {
                            document.getElementById("40fvm").innerHTML = 0;

                        }
                        
                    }
                </script>
                <script language="javascript" type="text/javascript">
                    setFilterGrid("table");
                </script>
                <div id="controls">
                    <div id="perpage">
                        <select onchange="sorter.size(this.value)">
                            <option value="5" selected="selected">5</option>
                            <option value="10">10</option>
                            <option value="20">20</option>
                            <option value="50">50</option>
                            <option value="100">100</option>
                        </select>
                        <span>Entries Per Page</span>
                    </div>
                    <div id="navigation">
                        <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                        <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                        <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                        <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                    </div>
                    <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                </div>
                <script type="text/javascript">
                    var sorter = new TINY.table.sorter("sorter");
                    sorter.head = "head";
                    sorter.asc = "asc";
                    sorter.desc = "desc";
                    sorter.even = "evenrow";
                    sorter.odd = "oddrow";
                    sorter.evensel = "evenselected";
                    sorter.oddsel = "oddselected";
                    sorter.paginate = true;
                    sorter.currentid = "currentpage";
                    sorter.limitid = "pagelimit";
                    sorter.init("table", 1);
                </script>
            </div>
            <br>
            <div id="va">
                <center><b>Vehicle Availability</b></center>
                <br>
                <table class="table table-info mb30 table-bordered" id="table">
                    <thead>
                    <th>S.No</th>
                    <th>Vehicle No</th>
                    <th>Type</th>
                    <th>Vehicle Status</th>
                    <th>Expected Date && Time</th>
                    <th>Trip Possibilities</th>
                    </thead>
                    <c:if test = "${vehicleAvailability != null}">
                        <% int sno1 = 0;%>
                        <tbody>
                            <c:forEach items="${vehicleAvailability}" var="vap">
                                <%
                                    sno1++;
                                    String className1 = "text1";
                                    if ((sno1 % 1) == 0) {
                                        className1 = "text1";
                                    } else {
                                        className1 = "text2";
                                    }
                                %>

                                <tr>
                                    <td> 
                                        <input type="hidden" name="vehicleCount20" id="vehicleCount20" value="<c:out value="${vap.totalVehicletwenty -vap.twentyNotAvail - vap.workSho20}"/>" >
                                        <input type="hidden" name="vehicleCount40" id="vehicleCount40" value="<c:out value="${vap.totalVehicleforty -vap.fortyNotAvail - vap.workShop40}"/>" >
                                        <%=sno1%></td>
                                    <td> <c:out value="${vap.regno}"/></td>
                                    <td>
                                        <c:out value="${vap.vehicleTypeName}"/>
                                    </td>
                                    <td  >
                                        <c:if test="${vap.activeInd == 'Y'}">
                                            <font color="green">Available</font>
                                        </c:if>
                                        <c:if test="${vap.activeInd == 'N'}">
                                            <font color="blue"> In Trip Progress </font>
                                        </c:if>
                                        <c:if test="${vap.activeInd == null && vap.service==null}">
                                            <font color="red">Vehicle Not Used </font>
                                        </c:if>
                                        <c:if test="${vap.service!=null}">
                                            <font color="orange">In WorkShop </font>
                                        </c:if>
                                    </td>
                                    <td >
                                        <c:out value="${vap.expectedArrivalDate}"/><br>
                                        <c:out value="${vap.expectedArrivalTime}"/>
                                    </td>
                                    <td>
                                        <c:if test="${vap.expectedArrivalDate !=null && vap.expectedArrivalDate==vap.currdate}">
                                            1
                                        </c:if>
                                        <c:if test="${vap.expectedArrivalDate !=null && vap.expectedArrivalDate != vap.currdate}">
                                            0
                                        </c:if>
                                        <c:if test="${vap.activeInd == 'Y'}">
                                            2
                                        </c:if>
                                        <c:if test="${vap.service!=null}">
                                            -
                                        </c:if>
                                        <c:if test="${vap.activeInd == null && vap.service==null}">
                                            2
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </tbody>
                        <input type="hidden" name="vehicle" id="vehicle" value="<%=sno1%>" />
                    </c:if>
                </table>
            </div>
        </div>
    </div>
</body>
<%@ include file="/content/common/NewDesign/settings.jsp" %>