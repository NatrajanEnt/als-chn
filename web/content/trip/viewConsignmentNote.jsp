<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });
    
    
function searchPage(val) {
//            alert("val="+val);
            document.CNoteSearch.action = "/throttle/consignmentNoteView.do?menuClick=1&pageType=<c:out value="${pageType}"/>&param="+val;
            document.CNoteSearch.submit();

    }    
//    setInterval(searchPage, 40000);
</script>

<script>
      function printCNote(orderId) {
//          alert("orderId==="+orderId);
        window.open('/throttle/consignmentNoteView.do?menuClick=1&param=Print&consignmentOrderId=' + orderId,'PopupPage','height = 500, width = 800, scrollbars = yes, resizable = yes');
    
    }
</script>

<style>

    .loading {

        border:10px solid red;

        display:none;

    }

    .button1{border: 1px black solid ; color:#000 !important;  }



    .spinner{

        position: fixed;

        top: 50%;

        left: 50%;

        margin-left: -50px; /* half width of the spinner gif */

        margin-top: -50px; /* half height of the spinner gif */

        text-align:center;

        z-index:1234;

        overflow: auto;

        width: 300px; /* width of the spinner gif */

        height: 300px; /*hight of the spinner gif +2px to fix IE8 issue */

    }

    .ui-loader-background {

        width:100%;

        height:100%;

        top:0;

        padding: 0;

        margin: 0;

        background: rgba(0, 0, 0, 0.3);

        display:none;

        position: fixed;

        z-index:100;

    }

    .ui-loader-background {

        display:block;



    }



</style>
<script type="text/javascript">
//google Map Plot Script
    var rendererOptions = {
        draggable: true
    };
    var directionsDisplay = new google.maps.DirectionsRenderer(rendererOptions);
    ;
    var directionsService = new google.maps.DirectionsService();
    var map;





    function viewConsignmentDetails(orderId) {
        window.open('/throttle/getConsignmentDetails.do?consignmentOrderId=' + orderId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewConsignmentContainerDetails(orderId, conType) {
        window.open('/throttle/viewConsignmentContainerDetails.do?consignmentOrderId=' + orderId + '&conType=' + conType, 'PopupPage', 'height = 800, width = 1400, scrollbars = yes, resizable = yes');
    }





    function importExcel() {
        document.CNoteSearch.action = '/throttle/BrattleFoods/tripPlanningImport.jsp';
        document.CNoteSearch.submit();
    }
    function checkShowTable() {
        var value = document.getElementById("summaryOption").value;
        if (parseInt(value) == 1) {
            $("#summaryTable").show();
        } else {
            $("#summaryTable").hide();
        }
    }
</script>
<style>
#rcorners1 {
    border-radius: 35px;
    background: url(paper.gif);
    background-position: left top;
    background-repeat: repeat;
    padding: 40px; 
    width: 850px;
    height: 400px;
}

</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Consignment Note View</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Sales</a></li>
            <li class="active">Consignment Note View</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <%
                            String menuPath = "Operation >> Trip Planning";
                            request.setAttribute("menuPath", menuPath);
                %>
                <form name="CNoteSearch" method="post" enctype="multipart">

                      <%
                                Date today = new Date();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                                String endDate = sdf.format(today);
                                
                                Calendar cal = Calendar.getInstance();
                                cal.add(Calendar.MONTH, -1);
                                Date result = cal.getTime();
                                String startDate = sdf.format(result);
                    %>
                    
                    <table class="table table-info mb30 table-hover" style="width:100%">

                        <tr   ><td colSpan="8" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">CONSIGNMENT ORDERS LIST</td></tr>
                        <tr>
                            <td style="display:none;">Customer Reference No</td>
                            <td  style="display:none;">
                                <input type="text" name="orderRefNo" id="orderRefNo" class="form-control" style="width:180px;height:40px;">
                            </td>	
                            <td style="display:none">LR No</td>
                            <td style="display:none">
                                <input type="text" name="lrNo" id="lrNo" class="form-control" style="width:180px;height:40px;">
                            </td>	

                            <td><font color="red">*</font>From Date</td>
                            <td><input name="fromDate" id="fromDate" type="text" class="datepicker" style="width:180px;height:40px;"   value="<c:out value="${fromDate}"/>"></td>
                            <td><font color="red">*</font>To Date</td>
                            <td><input name="toDate" id="toDate" type="text" class="datepicker" style="width:180px;height:40px;"  value="<c:out value="${toDate}"/>"></td>
                            
                        </tr>
                       
                    </table>
                        <center>
                                <input type="button" class="btn btn-info"   value="Search" onclick="searchPage('Search')">
                                &emsp;<input type="button"  class="btn btn-info" value="Generate Excel" name="ExcelExport" onClick="searchPage(this.name)">
                        </center>
                            <br>
                            <br>

                    <input type="hidden" name="routePoints" id="routePoints" value='<c:out value="${routePoints}"/>' />
                    <input type="hidden" name="consignmentListSize" id="routePoints" value='<c:out value="${consignmentListSize}"/>' />
                    <input type="hidden" name="orderedUniquePickPoints" id="routePoints" value='<c:out value="${orderedUniquePickPoints}"/>' />
                  
                    <table style="display:none;">
                        <tr>
                            <td>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</td>
                            <td>
                    <c:if test = "${consignmentListViewSummary != null}" >
                            <c:forEach items="${consignmentListViewSummary}" var="cls">
                           
                            <marquee direction="up" onmouseover="this.stop();" onmouseout="this.start();">
                            <table id="rcorners1" style="background-color:#AAE4F1" style  ="width:70%;height:40px" >
                        <tr >
                            <td colspan="6" align="center" size='5'><b><u>Previous Summary</u></b></td>
                        </tr>
                        <tr>
                           
                            <td  style="text-align: left"  >
                                <font color="#006829" size='4'><b> &emsp;40 FT:&nbsp;</b></font><font color="#006829" size='3'> <b>&nbsp;<c:out value="${cls.fortyFeetTotal}"/></b></font>
                            </td>
                            <td style="text-align: center">
                                <font color="#006829" size='4'><b> &emsp;20 FT:&nbsp;<font color="#006829" size='3'><b>&nbsp;<c:out value="${cls.twentyFeetTotal}"/></b> </font>
                                    </b></font>
                            </td>
                            <td  style="text-align: left">
                                <font color="#006829" size='4'><b> &emsp;Count:&nbsp;<font color="#006829" size='3'><b>&nbsp;<c:out value="${cls.fortyFeetTotal + cls.twentyFeetTotal}"/></b> </font>
                                    </b></font>
                            </td>
                            <td style="text-align: left">&emsp;</td>
                             <td colspan="2" style="text-align: left" >
                                <font color="#380099" size='4'><b>&emsp;Booking:&nbsp;
                                </b></font>
                                <font color="#380099" size='3'><b><c:out value="${cls.totalOrders}"/>
                                </b></font>
                            </td>
                        </tr>
                        <tr style="border-top:1pt solid black;" >
                            <td  style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&nbsp;Customer </b></font></td>
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&nbsp; Movement </b></font></td>
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&nbsp; Point </b></font></td>
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&nbsp; 40 FT</b></font></td>
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&nbsp; 20 FT</b> </font></td>
                            <td style="text-align: center"> <font color="black"><b>&nbsp; Total </b></font></td>
                        </tr>
                        <c:forEach items="${consignmentListViewCustomerPreviousSummary}" var="cls">
                        <tr style="border-top:1pt solid black;" >
                            <td  style="border-right:1pt solid black;text-align: center" >
                                <font color="#380099" size='3'><b>&nbsp;<c:out value="${cls.customerName}"/>
                                </b></font>
                            </td>
                            <td  style="border-right:1pt solid black;text-align: center" >
                                <font color="#380099" size='3'><b>&nbsp;<c:out value="${cls.movementType}"/>
                                </b></font>
                            </td>
                            <td  style="border-right:1pt solid black;text-align: center" >
                                <font color="#380099" size='3'><b>&nbsp;<c:out value="${cls.finalPointName}"/>
                                </b></font>
                            </td>
                            <td  style="border-right:1pt solid black;text-align: center" >
                                <font color="#380099" size='3'><b>&nbsp;<c:out value="${cls.fortyFeetTotal}"/>
                                </b></font>
                            </td>
                            
                            <td style="border-right:1pt solid black;text-align: center" >
                               <font color="#380099" size='3'> <b>&nbsp;<c:out value="${cls.twentyFeetTotal}"/></b></font>
                            </td>
                            
                            <td style="text-align: center" >
                                <font color="red" size='3'><b>&nbsp;<c:out value="${cls.twentyFeetPending + cls.fortyFeetPending}"/></b> </font>/ 
                                <font color="#006829" size='3'> <b>&nbsp;<c:out value="${cls.twentyFeetTotal + cls.fortyFeetTotal}"/></b></font>
                            </td>
                            
                        </tr>
                        </c:forEach>
                    </table>
                <!--</marquee>-->
                    
                       </c:forEach>
                        </c:if>
                        
                             
                        <br>
                        <br>
                            <c:if test = "${consignmentListLiveSummary != null}" >
                            <c:forEach items="${consignmentListLiveSummary}" var="cls">
                         
                        <!--<marquee direction="up" onmouseover="this.stop();" onmouseout="this.start();">-->
                            <table id="rcorners1" style="background-color:#AAE4F1" style  ="width:70%;height:40px" >
                        <tr >
                            <td colspan="6" align="center" size='5'><b><u>Live Summary</u></b></td>
                        </tr>
                        <tr>
                            <td  style="text-align: left"  >
                                <font color="#006829" size='4'><b> &emsp;40 FT:&nbsp;</b></font><font color="#006829" size='3'> <b>&nbsp;<c:out value="${cls.fortyFeetTotal}"/></b></font>
                            </td>
                            <td style="text-align: center">
                                <font color="#006829" size='4'><b> &emsp;20 FT:&nbsp;<font color="#006829" size='3'><b>&nbsp;<c:out value="${cls.twentyFeetTotal}"/></b> </font>
                                    </b></font>
                            </td>
                            <td  style="text-align: center">
                                <font color="#006829" size='4'><b> &emsp;Count:&nbsp;<font color="#006829" size='3'><b>&nbsp;<c:out value="${cls.fortyFeetTotal + cls.twentyFeetTotal}"/></b> </font>
                                    </b></font>
                            </td>
                            <td style="text-align: center">&emsp;</td>
                             <td colspan="2" style="text-align: left" >
                                <font color="#380099" size='4'><b>&emsp;Booking:&nbsp;
                                </b></font>
                                <font color="#380099" size='3'><b><c:out value="${cls.totalOrders}"/>
                                </b></font>
                            </td>
                        </tr>
                        <tr style="border-top:1pt solid black;">
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&emsp;Customer </b></font></td>
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&emsp; Movement </b></font></td>
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&emsp; Point </b></font></td>
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&emsp; 40' FT</b></font></td>
                            <td style="border-right:1pt solid black;text-align: center"> <font color="black"><b>&emsp; 20' FT</b> </font></td>
                            <td> <font color="black"><b>&emsp; Total </b></font></td>
                        </tr>
                          <c:forEach items="${consignmentListViewCustomerSummary}" var="cls">
                        <tr style="border-top:1pt solid black;">
                            <td   style="border-right:1pt solid black;text-align: center" >
                                <font color="#380099" size='3'><b><c:out value="${cls.customerName}"/>
                                </b></font>
                            </td>
                            <td   style="border-right:1pt solid black;text-align: center" >
                                <font color="#380099" size='3'><b><c:out value="${cls.movementType}"/>
                                </b></font>
                            </td>
                            <td   style="border-right:1pt solid black;text-align: center" >
                                <font color="#380099" size='3'><b><c:out value="${cls.finalPointName}"/>
                                </b></font>
                            </td>
                            <td   style="border-right:1pt solid black;text-align: center" >
                                <font color="#380099" size='3'><b><c:out value="${cls.fortyFeetTotal}"/>
                                </b></font>
                            </td>
                            
                            <td  style="border-right:1pt solid black;text-align: center" >
                               <font color="#380099" size='3'> <b>&emsp;<c:out value="${cls.twentyFeetTotal}"/></b></font>
                            </td>
                            
                            <td style="text-align: center" >
                                <font color="red" size='3'><b><c:out value="${cls.twentyFeetPending + cls.fortyFeetPending}"/></b> </font>/ 
                                <font color="#006829" size='3'> <b>&nbsp;<c:out value="${cls.twentyFeetTotal + cls.fortyFeetTotal}"/></b></font>
                            </td>
                            
                        </tr>
                        </c:forEach>
                    </table>
                        </marquee>
                    
                       </c:forEach>
                        </c:if>
                      </td>
                      </tr>
                    </table>
                   <br>
                    <c:if test = "${consignmentList != null}" >
                        <table class="table table-info mb30 table-bordered" id="table" width="100%" >
                            <thead>
                                <c:if test = "${pnrUser == 1}" >
                                    <tr >
                                        <th align="center">Sno:<c:out value="${pnrUser}"/>:</th>
                                        <th align="center">ConsigNo</th>
                                        <th align="center">PNR No</th>
                                        <th align="center">VesselName</th>
                                        <th align="center">Move Type</th>
                                        <th align="center">Consignment Date</th>
                                        <th align="center">PNR Date</th>
                                        <th align="center">Interim Details </th>
                                        <th align="center">20ft</th>
                                        <th align="center">40ft </th>                                        
                                        <th align="center">Total</th>                                        
                                        <th align="center">Status </th>
                                        <th align="center">Vehicle Required On </th>
                                        <th align="center">Documents</th>
                                        <th align="center">Created By</th>
                                        
                                    </tr>
                                </c:if>
                                <c:if test = "${pnrUser != 1}" >
                                    <tr >
                                        <th align="center">Sno</th>
                                        <th align="center">ConsigNo</th>
                                        <th align="center">Customer</th>
                                        <th align="center">Booking No / BOE</th>                                        
                                        <th align="center">Move Type</th>
                                        <th align="center">SLN/BL</th>
                                        <th align="center">Consignment Date</th>                                        
                                        <th align="center">Route Details </th>
                                       <th align="center">20ft</th>
                                        <th align="center">40ft </th>                                        
                                        <th align="center">Total</th>   
                                        <th align="center">Ton</th>
                                        <th align="center">Goods Desc</th>
                                        <th align="center">Status </th>
                                        <th align="center">Veh.Req.On </th>
                                        <th align="center">Loading/Unloading On </th>
                                        <th align="center">Documents</th>
                                        <th align="center">Created By</th>
                                        <!--<th align="center">Action</th>-->
                                    </tr>
                                </c:if>
                            </thead>
                            <% int index = 0;
                                        int sno = 1;
                            %>
                            <tbody>
                                <c:set var="totalVolume" value="${0 }"/>
                                <c:set var="totalWeight" value="${0 }"/>
                                <c:forEach items="${consignmentList}" var="cnl">
                                    <%--<c:if test = "${cnl.plannedStatus != 1}" >--%>
                                        <c:set var="totalVolume" value="${totalVolume + cnl.totalVolume }"/>
                                        <c:set var="totalWeight" value="${totalWeight + cnl.totalWeights }"/>
                                        <%-- <input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/><c:out value="${cnl.consignmentNoteNo}"/>--%>
                                        <tr>

                                            <td align="center" ><%=sno%></td>
                                           

                                            <td align="center" >
                                                <input type="hidden" name="consignmentOrderId" id="consignmentOrderId<%=index%>" value="<c:out value="${cnl.consignmentOrderId}"/>"/>
                                                <input type="hidden" name="containerId" id="containerId<%=index%>" value="<c:out value="${cnl.containerTypeId}"/>"/>
                                                <input type="hidden" name="movementType" id="movementType<%=index%>" value="<c:out value="${cnl.movementType}"/>"/>

                                                <a href="#" onclick="viewConsignmentDetails('<c:out value="${cnl.consignmentOrderId}"/>');"><c:out value="${cnl.consignmentNoteNo}"/></a>
                                            </td>
                                             <td align="center" ><c:out value="${cnl.customerName}"/></td>
                                             
                                            <td align="center" ><c:out value="${cnl.orderReferenceNo}"/></td>
                                            <c:if test = "${pnrUser != 1}" >
                                            </c:if>
                                            <c:if test = "${pnrUser == 1}" >
                                            <td align="center" ><c:out value="${cnl.linerNames}"/></td>
                                            </c:if>

                                            <td align="center" ><c:out value="${cnl.movementType}"/></td>
                                            <td align="center" >
                                                <c:if test="${cnl.movementTypeId == 1 || cnl.movementTypeId == 25 || cnl.movementTypeId == 26  || cnl.movementTypeId == 27 || cnl.movementTypeId == 31 || cnl.movementTypeId == 32}">
                                                    <c:out value="${cnl.shippingLineTwo}"/>
                                                </c:if>
                                                <c:if test="${cnl.movementTypeId == 2 || cnl.movementTypeId == 28 || cnl.movementTypeId == 29 || cnl.movementTypeId == 30}">
                                                    <c:out value="${cnl.billOfEntry}"/>
                                                </c:if>
                                                
                                            </td>
                                            <td align="center" ><c:out value="${cnl.consignmentOrderDate}"/> <br> <c:out value="${cnl.consignmentTime}"/></td>
                                            <c:if test = "${pnrUser == 1}" >
                                                <td align="center" >
                                                    <c:out value="${cnl.consignmentOrderDate}"/> 
                                                </td>
                                            </c:if>

                                            <td align="center" ><c:out value="${cnl.routeName}"/></td>

                                          <td align="center"  >
                                                <a  href="#" onclick="viewConsignmentContainerDetails('<c:out value="${cnl.consignmentOrderId}"/>', '1');">
                                                    <font size="3"> <c:out value="${cnl.containerTypeName}"/> </font>
                                                </a>
                                            </td>
                                            <td align="center"  >
                                                <a  href="#" onclick="viewConsignmentContainerDetails('<c:out value="${cnl.consignmentOrderId}"/>', '2');">
                                                    <font size="3"> <c:out value="${cnl.containerTypeNameForty}"/> </font>
                                                </a>
                                            </td>
                                            <td align="center"  >
                                                <c:out value="${cnl.totalContainer}"/>
                                                <input type="hidden" name="totalContainer" id="totalContainer<%=index%>" value="<c:out value="${cnl.totalContainer}"/>">
                                            </td>
                                            <c:if test = "${pnrUser != 1}" >
                                            <c:if test = "${movementTypeId == 14 || movementTypeId == 16}" >
                                            <td align="center" ><c:out value="${cnl.packageNos}"/>/<c:out value="${cnl.totalWeights}"/>/<c:out value="${cnl.totalVolume}"/></td>
                                            </c:if>
                                            <c:if test = "${movementTypeId != 14 && movementTypeId != 16}" >
                                            <td align="center" >
                                            <c:if test="${(cnl.movementTypeId =='1')}" >
                                                    Tonnage : &emsp;<c:out value="${cnl.conWeight}"/>
                                                </c:if>
                                                <c:if test="${(cnl.movementTypeId !='1')}" >
                                                <c:out value="${cnl.conPackages}"/>/<c:out value="${cnl.conWeight}"/>/<c:out value="${cnl.conVolume}"/>
                                                </c:if>    
                                            </td>
                                            </c:if>
                                            </c:if>
                                            <td align="center" ><c:out value="${cnl.desc}"/></td>
                                            <td align="center" >
                                                <input type="hidden" name="typeName" id="typeName<%=index%>" value="<c:out value="${cnl.typeName}"/>"/>
                                                <input type="hidden" name="routeName" id="routeName<%=index%>" value="<c:out value="${cnl.routeName}"/>"/>
                                                <input type="hidden" name="originId" id="originId<%=index%>" value="<c:out value="${cnl.origin}"/>"/>
                                                <input type="hidden" name="destinationId" id="destinationId<%=index%>" value="<c:out value="${cnl.destination}"/>"/>

                                                <input type="hidden" name="consignmentOrderStatus" id="consignmentOrderStatus<%=index%>" value="<c:out value="${cnl.consginmentOrderStatus}"/>"/>

                                                <c:if test="${(cnl.consginmentOrderStatus=='5')}" >
                                                    Order Created
                                                </c:if>

                                                <c:if test="${(cnl.consginmentOrderStatus=='25')}" >
                                                    First Leg completed
                                                </c:if>
                                                <c:if test="${(cnl.consginmentOrderStatus=='28')}" >
                                                    Second Leg completed
                                                </c:if>

                                            </td>
                                            <td align="center" ><c:out value="${cnl.tripScheduleDate}"/><br>
                                                <c:out value="${cnl.tripScheduleTime}"/></td>
                                            <td align="center" ><c:out value="${cnl.loadUnloadDate}"/><br>
                                                <c:out value="${cnl.loadUnloadTime}"/></td>
                                            
                                            <td align="center" >
                                                <a href="viewConsignmentDocuments.do?consignmentId=<c:out value="${cnl.consignmentOrderId}"/>"  onclick="return !window.open(this.href, 'somesite', 'width=500,height=500')"
                                                   target="_blank" >   
                                                    <c:if test = "${cnl.borderCount == 0}" > 
                                                        <img src="images/Podinactive.png" alt="Y"   title="click to upload Documents"/>
                                                    </c:if>

                                                    <c:if test = "${cnl.borderCount > 0}" > 
                                                        <img src="images/Podactive.png" alt="Y"   title="click to upload Documents"/>
                                                    </c:if>


                                                </a>
                                            </td>
                                            <td align="center" ><c:out value="${cnl.userName}"/></td>
<%--                                            <td align="center" >
                                                <a href="#"  onclick="printCNote('<c:out value="${cnl.consignmentOrderId}"/>')" >
                                                       CNote-Print
                                                    </a>
                                            </td>--%>

                                        </tr>
                                        <%index++;%>
                                        <%sno++;%>
                                    <%--</c:if>--%>
                                </c:forEach>
                            </tbody>
                        </table>
                        <br/>
                            <c:forEach items="${vehicleTypeList}" var="vehicle">
                                <tr height="40">
                                    <td><c:out value="${vehicle.vehicleTypeName}"/></td>
                                <input type="hidden"  id="vehicleTypeId" name="vehicleTypeId" value='<c:out value="${vehicle.vehicleTypeId}"/>'/>
                                <c:set var="totalVehicle" value="${totalWeight / (vehicle.vehicleTonnage*1000)}"/>
                                <c:set var="totalVehicles" value="${(totalVehicle - ( totalVehicle % 1)) +1 }" />
                                <td >
                                <fmt:formatNumber type="number"  maxFractionDigits="2" value="${totalVehicles }" />
                            </td>
                            <td >
                                <fmt:formatNumber type="number"  maxFractionDigits="2" value="${(totalWeight / (vehicle.vehicleTonnage*1000 * totalVehicles )) * 100    }" />&nbsp;%
                            </td>
                            <td id="vehicleTypeExpense">
    
                                <a href="#" onClick="viewExpenseDetails('<c:out value="${vehicle.vehicleTypeName}"/>', '<c:out value="${vehicle.vehicleTypeId}"/>', '<c:out value="${routeDistance}"/>', '<c:out value="${totalVehicles}"/>');" > view</a>
                                
                                <fmt:formatNumber type="number"  maxFractionDigits="2" value="${vehicle.costPerKm * routeDistance  * totalVehicles }" />
                                
                            </td>
    
                            </tr>
                            </c:forEach>
                        



                        </c:if>
                    
                    <input type="hidden" name="customerId" id="customerId" value=""/>
                    <input type="hidden" name="summaryOption" id="summaryOption" value="0"/>
                    <input type="hidden" name="ismultiple" id="ismultiple" value="0"/>
                    <input type="hidden" name="googleDistance" id="googleDistance" value="0"/>
                    <script language="javascript" type="text/javascript">



                        function setTripType(v) {
                            var ismultiPle = document.getElementsByName("multiPleTrip");
                            var flag = 0;
                            for (var j = 0; j < ismultiPle.length; j++) {

                                if (ismultiPle[j].checked == true) {
                                    flag = 1;

                                    if (j == v) {
                                        document.getElementById("ismultiple").value = "1";
                                    } else {
                                        document.getElementById("multiPleTrip" + j).checked = false;
                                        document.getElementById("ismultiple").value = "1";
                                    }
                                } else {
                                    if (flag == 0) {
                                        document.getElementById("ismultiple").value = "0";
                                    }
                                }
                            }
                        }

                        function checkSelectedItem(sno, containerTypeName, routeName,movementType) {
                            var check = 0;
                            var routeName1 = "";
                            var movementType1 = "";
                            if(containerTypeName != ''){
                            var containerTypeNameTemp = containerTypeName.split("/");
                            if (parseInt(containerTypeNameTemp[0]) < parseInt(containerTypeNameTemp[1])) {
                                var selectedConsignment = document.getElementsByName('selectedIndex');
                                for (var i = 0; i < selectedConsignment.length; i++) {
                                    if (document.getElementById("selectedIndex" + i).checked) {
                                        check = check + 1;
                                    }
                                }
                                alert(movementType);
                                if (check > 2 && movementType != 14) {
                                    alert("Please select only 2 orders and proceed");
                                    document.getElementById("selectedIndex" + sno).checked = false;
                                    return;
                                }
                                
                                 var selectedConsignment1 = document.getElementsByName('selectedIndex');
                                for (var i = 0; i < selectedConsignment1.length; i++) {
                                    if(sno != i){
                                    if (document.getElementById("selectedIndex" + i).checked) {
                                       routeName1 = document.getElementById("routeName" + i).value;
                                       movementType1 = document.getElementById("movementType" + i).value;
                                    }
                                }
                                }
                                if(check > 1){
                                if(routeName1 != routeName || movementType != movementType1 ){
                                   alert("selected routes are different please select same route"); 
                                    document.getElementById("selectedIndex" + sno).checked = false;
                                    return;
                                }
                            }
                                
                            } else {
                                
                                alert("selected order does not contain 20 feet container1");
                                return;
                            }
                            } 
//                            else {
//                                alert("selected order does not contain 20 feet container2");
                                return;
//                            }

                        }

                    </script>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>
                    <br/>
                    
                    <table>
                        <tr>
                            <td>
                                <div id="mapdiv"   style="width:550px; height:400px">

                                </div>
                                <div id="mapdistance"   style="width:550px; height:400px">

                                </div>
                            </td>
                            <td valign="top" >&nbsp;
                                <%if(request.getAttribute("routePoints") != null){%>
                                <table width="100%" align="center" border="0" id="table1" class="sortable" style="width:360px;" >
                                    <thead>
                                        <tr>
                                            <td>Sno</td>
                                            <td>Legend</td>
                                            <td>Point Name</td>
                                            <td>Point Type</td>
                                            <td>Planned Date Time</td>
                                        </tr>
                                    </thead>

                                    <%
                                    String routeDetails = (String)request.getAttribute("routePoints");
                                    String[] routeData = routeDetails.split("@");
                                    String[] tempData = null;
                                    int cntr = 0;
                                    String tempLegend = "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z";
                                    String[] tempLegendArray = tempLegend.split(",");
                                    for(int j=0; j< routeData.length; j++){
                                        cntr++;
                                        tempData = routeData[j].split("~");
                                        if(tempData.length > 4){// pickup
                                    %>
                                    <tr>
                                        <td><%=cntr%></td>
                                        <td><%=tempLegendArray[j]%></td>
                                        <td><%=tempData[3]%></td>
                                        <td>PickUp</td>
                                        <td><%=tempData[4]%></td>
                                    </tr>
                                    <%
                                }else{
                                    %>
                                    <tr>
                                        <td><%=cntr%></td>
                                        <td><%=tempLegendArray[j]%></td>
                                        <td><%=tempData[3]%></td>
                                        <td>Drop</td>
                                        <td>-</td>
                                    </tr>
                                    <%
                                }

                            }
                                    %>

                                </table>
                                <%}%>
                            </td>
                    </table>


                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                <div id="loader"></div>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
