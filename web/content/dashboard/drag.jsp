<!DOCTYPE html>
<%@page contentType="text/html" pageEncoding="UTF-8" import="java.sql.*"%>
<html lang="en">
<head>

        <!--<link href="ui/css/style.css" rel="stylesheet" type="text/css" />
        <link href="prettify/prettify.css" rel="stylesheet" type="text/css" /> -->
        <script language="JavaScript" src="FusionCharts.js"></script>
        <script type="text/javascript" src="ui/js/jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="prettify/prettify.js"></script>
        <script type="text/javascript" src="ui/js/json2.js"></script>

        <!--[if IE 6]>
        <script type="text/javascript" src="../../Contents/assets/ui/js/DD_belatedPNG_0.0.8a-min.js"></script>
        <script>
          /* select the element name, css selector, background etc */
          DD_belatedPNG.fix('img');

          /* string argument can be any CSS selector */
        </script>
		  <p>&nbsp;</p>
		  <P align="center"></P>
        <![endif]-->
        <script type="text/javascript">
            $(document).ready ( function () {
                $("a.view-chart-data").click( function () {
                    var chartDATA = '';
                    if ($(this).children("span").html() == "View XML" ) {
                        chartDATA = FusionCharts('ChartId').getChartData('xml').replace(/\</gi, "&lt;").replace(/\>/gi, "&gt;");
                    } else if ($(this).children("span").html() == "View JSON") {
                        chartDATA = JSON.stringify( FusionCharts('ChartId').getChartData('json') ,null, 2);
                    }
                    $('pre.prettyprint').html( chartDATA );
                    $('.show-code-block').css('height', ($(document).height() - 56) ).show();
                    prettyPrint();
                })

                $('.show-code-close-btn a').click(function() {
                    $('.show-code-block').hide();
                });
            })
        </script>
	<meta charset="utf-8">
	<title></title>
	<link rel="stylesheet" href="css/jquery.ui.theme.css">
	<script src="js/jquery-1.4.4.js"></script>
	<script src="js/jquery.ui.core.js"></script>
	<script src="js/jquery.ui.widget.js"></script>
	<script src="js/jquery.ui.mouse.js"></script>
	<script src="js/jquery.ui.sortable.js"></script>
<style type="text/css">
.link {
	font: normal 12px Arial;
	text-transform:uppercase;
	padding-left:10px;
	font-weight:bold;
}

.link a  {
	color:#7f8ba5;
	text-decoration:none;
}

.link a:hover {
		color:#7f8ba5;
	text-decoration:underline;

}

</style>
	<style type="text/css">
            #expand {
                width:130%;
}
	.column { width: 250px; float: left; }
	.portlet { margin: 0 1em 1em 0; }
	.portlet-header { margin: 0.3em; padding-bottom: 4px; padding-left: 0.2em; cursor:move; }
	.portlet-header .ui-icon { float: right; }
	.portlet-content { padding: 0.4em; }
	.ui-sortable-placeholder { border: 1px dotted black; visibility: visible !important; height: 50px !important; }
	.ui-sortable-placeholder * { visibility: hidden; }
	</style>
	<script>
	$(function() {
		$( ".column" ).sortable({
			connectWith: ".column"
		});

		$( ".portlet" ).addClass( "ui-widget ui-widget-content ui-helper-clearfix ui-corner-all" )
			.find( ".portlet-header" )
				.addClass( "ui-widget-header ui-corner-all" )
				.prepend( "<span class='ui-icon ui-icon-minusthick'></span>")
				.end()
			.find( ".portlet-content" );

		$( ".portlet-header .ui-icon" ).click(function() {
			$( this ).toggleClass( "ui-icon-minusthick" ).toggleClass( "ui-icon-plusthick" );
			$( this ).parents( ".portlet:first" ).find( ".portlet-content" ).toggle();
		});

		$( ".column" ).disableSelection();
	});
	</script>
</head>
<body>

    <%
    
String qry1 = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " (rfsdate between '2011-04-01' and '2012-03-31' "+
            " or "+
            " est_rfsdate between '2011-04-01' and '2012-03-31' "+
            " ) and curr_status in ('Active','Completed','Suspended') "+
            " group by curr_Status";

String qry2 = " select "+
            " sum(if((delay <=0),1,0)) as 'On Track', "+
            " sum(if((delay >0 && delay <=20),1,0)) as 'Slightly Delayed', "+
            " sum(if(delay >20,1,0)) as 'Severely Delayed' "+
            " FROM projectdetails "+
            " where "+
            " (rfsdate between '2011-04-01' and '2012-03-31'"+
            " or "+
            " est_rfsdate between '2011-04-01' and '2012-03-31'"+
            " ) and curr_status in (?)";

String qry3 = " select "+
            " sum(if((delay <=0),1,0)) as 'On Track', "+
            " sum(if((delay >0 && delay <=20),1,0)) as 'Slightly Delayed', "+
            " sum(if(delay >20,1,0)) as 'Severely Delayed' "+
            " FROM projectdetails "+
            " where "+
            " (rfsdate between '2011-04-01' and '2012-03-31' "+
            " or "+
            " est_rfsdate  between '2011-04-01' and '2012-03-31' "+
            " ) and curr_status in (?) "+
            " and projecttype=? ";
Connection con1 = null;
Connection con2 = null;
Connection con3 = null;
Connection con4 = null;

try {

        System.out.println("in osp");
        Class.forName("com.mysql.jdbc.Driver").newInstance();
        con1 = DriverManager.getConnection("jdbc:mysql://localhost:3306/tatacomm", "tcl", "admin");
        con2 = DriverManager.getConnection("jdbc:mysql://localhost:3306/tatacommOSP", "tcl", "admin");
        con3 = DriverManager.getConnection("jdbc:mysql://localhost:3306/tatacommPOI", "tcl", "admin");
        con4 = DriverManager.getConnection("jdbc:mysql://localhost:3306/tatacommit", "tcl", "admin");
        PreparedStatement ps2;
        PreparedStatement ps3;
        ps2 = con1.prepareStatement(qry2);
        ps3 = con1.prepareStatement(qry3);
        //ps2.setString(1, prmSRNO);
        //res = ps2.executeQuery();
        ResultSet rs2 = null;
        ResultSet rs3 = null;

        Statement stm = con1.createStatement();
        ResultSet res = stm.executeQuery(qry1);

    String cityStrXML="";

    cityStrXML = "<chart caption='' bgColor='FFFFFF' showPercentageValues='0'  numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>";
    String cityStrDataRev = "";
    String[] colors = {"CCFB5D","0099FF","FF0000","FFE87C"};
    String[] colors1 = {"82CAFA","99CC00","F88017","FFE87C"};
    int i=0;
    while(res.next()) {
     //  cityStrDataRev += "<set value='"+res.getInt("total")+"' label='"+res.getString("curr_status")+"' color='"+colors[i]+"' alpha='60'/>";
        i++;
    }
    
   //cityStrXML += cityStrDataRev + "</chart>";
    
    String qryAct = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " (rfsdate between '2011-04-01' and '2012-03-31' "+
            " or "+
            " est_rfsdate between '2011-04-01' and '2012-03-31' or rfsdate = '2006-01-01'"+
            " ) and curr_status in ('Active') ";
     int active = 0;
     ResultSet rsAct = stm.executeQuery(qryAct);
     while(rsAct.next()){
         active = rsAct.getInt("total");
     
     }
     String qryCom = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " rfsdate between '2011-04-01' and '2012-03-31' "+
            " and curr_status in ('Completed') ";
     int complete = 0;
     ResultSet rsCom = stm.executeQuery(qryCom);
     while(rsCom.next()){
     complete = rsCom.getInt("total");
     }
     
    String qrySus = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " curr_status in ('Suspended') ";
     int suspnd = 0;
     ResultSet rsSus = stm.executeQuery(qrySus);
     while(rsSus.next()){
     suspnd = rsSus.getInt("total");
     }
    cityStrDataRev += "<set value='"+active+"' label='Active' color='"+colors[0]+"' alpha='60'/>";
    cityStrDataRev += "<set value='"+complete+"' label='Completed' color='"+colors[1]+"' alpha='60'/>";
    cityStrDataRev += "<set value='"+suspnd+"' label='Suspended' color='"+colors[2]+"' alpha='60'/>";
    cityStrXML += cityStrDataRev + "</chart>";
    
    
    %>
<div id="expand" >

<div class="column" >

	<div class="portlet" >
            <div class="portlet-header">&nbsp;&nbsp;PI Overall Summary YTD</div>
                    <div class="portlet-content">
                            <table align="center"   cellspacing="0px" >
                    <tr>
                        <td>

                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/Doughnut2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=cityStrXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                            
                        </td>
                    </tr>
                    


                    </table>
                        
                    </div>
	</div>
        <%
        stm = con2.createStatement();
        res = stm.executeQuery(qry1);

        cityStrXML="";

        cityStrXML = "<chart caption='' bgColor='FFFFFF' showPercentageValues='0'  numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>";
        cityStrDataRev = "";
        i=0;
        while(res.next()) {
          //  cityStrDataRev += "<set value='"+res.getInt("total")+"' label='"+res.getString("curr_status")+"' color='"+colors[i]+"' alpha='60'/>";
            i++;
        }
        //cityStrXML += cityStrDataRev + "</chart>";
        
        
        
        
         qryAct = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " (rfsdate between '2011-04-01' and '2012-03-31' "+
            " or "+
            " est_rfsdate between '2011-04-01' and '2012-03-31' or rfsdate = '2006-01-01'"+
            " ) and curr_status in ('Active') ";
     active = 0;
     rsAct = stm.executeQuery(qryAct);
     while(rsAct.next()){
         active = rsAct.getInt("total");
     
     }
     qryCom = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " rfsdate between '2011-04-01' and '2012-03-31' "+
            " and curr_status in ('Completed') ";
     complete = 0;
     rsCom = stm.executeQuery(qryCom);
     while(rsCom.next()){
     complete = rsCom.getInt("total");
     }
     
    qrySus = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " curr_status in ('Suspended') ";
     suspnd = 0;
     rsSus = stm.executeQuery(qrySus);
     while(rsSus.next()){
        suspnd = rsSus.getInt("total");
     }
    cityStrDataRev += "<set value='"+active+"' label='Active' color='"+colors[0]+"' alpha='60'/>";
    cityStrDataRev += "<set value='"+complete+"' label='Completed' color='"+colors[1]+"' alpha='60'/>";
    cityStrDataRev += "<set value='"+suspnd+"' label='Suspended' color='"+colors[2]+"' alpha='60'/>";
    cityStrXML += cityStrDataRev + "</chart>";
        
        
        
    %>
	<div class="portlet">
		<div class="portlet-header">OSP Overall Summary YTD</div>
                    <div class="portlet-content">
                    <table align="center"   cellspacing="1px" >
                    <tr>
                        <td>
                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/Doughnut2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=cityStrXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>                            
                        </td>
                    </tr>


                    </table>
                    </div>
	</div>
 <%
        stm = con3.createStatement();
        
        res = stm.executeQuery(qry1);

        cityStrXML="";

        cityStrXML = "<chart caption='' bgColor='FFFFFF' showPercentageValues='0'  numberPrefix='' isSmartLineSlanted='0' showValues='1' showLabels='0' showLegend='1'>";
        cityStrDataRev = "";
        i=0;
        while(res.next()) {
         //   cityStrDataRev += "<set value='"+res.getInt("total")+"' label='"+res.getString("curr_status")+"' color='"+colors[i]+"' alpha='60'/>";
            i++;
        }
        //cityStrXML += cityStrDataRev + "</chart>";
          qryAct = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " (rfsdate between '2011-04-01' and '2012-03-31' "+
            " or "+
            " est_rfsdate between '2011-04-01' and '2012-03-31' or rfsdate = '2006-01-01'"+
            " ) and curr_status in ('Active') ";
     active = 0;
     rsAct = stm.executeQuery(qryAct);
     while(rsAct.next()){
         active = rsAct.getInt("total");
     
     }
     qryCom = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " rfsdate between '2011-04-01' and '2012-03-31' "+
            " and curr_status in ('Completed') ";
     complete = 0;
     rsCom = stm.executeQuery(qryCom);
     while(rsCom.next()){
     complete = rsCom.getInt("total");
     }
     
    qrySus = " select curr_status, count(1) as total FROM projectdetails "+
            " where "+
            " curr_status in ('Suspended') ";
     suspnd = 0;
     rsSus = stm.executeQuery(qrySus);
     while(rsSus.next()){
        suspnd = rsSus.getInt("total");
     }
    cityStrDataRev += "<set value='"+active+"' label='Active' color='"+colors[0]+"' alpha='60'/>";
    cityStrDataRev += "<set value='"+complete+"' label='Completed' color='"+colors[1]+"' alpha='60'/>";
    cityStrDataRev += "<set value='"+suspnd+"' label='Suspended' color='"+colors[2]+"' alpha='60'/>";
    cityStrXML += cityStrDataRev + "</chart>";
    %>
	<div class="portlet">
		<div class="portlet-header">POI Overall Summary YTD</div>
                    <div class="portlet-content">
                                   <table align="center"   cellspacing="1px" >
                    <tr>
                        <td>

                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/Doughnut2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=cityStrXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                            
                        </td>

                    </tr>


                    </table>
                    </div>
	</div>
</div>
    <%


    ps2 = con1.prepareStatement(qry2);
    ps2.setString(1, "Active");
    rs2 = ps2.executeQuery();

    String strXML="";

    strXML = "<chart caption='' showPercentageInLabel='0' showValues='1' showLabels='0' showLegend='0'>";
    String strDataRev = "";
    if(rs2.next()){
        strDataRev += "<set value='"+rs2.getInt(1)+"' color='"+colors1[1]+"'  label=''/> ";
        strDataRev += "<set value='"+rs2.getInt(2)+"' color='"+colors1[3]+"'   label='' />";
        strDataRev += "<set value='"+rs2.getInt(3)+"' color='"+colors1[2]+"'   label='' />";
    }
    strXML += strDataRev + "</chart>";
    %>
<div class="column" >

	<div class="portlet">
		<div class="portlet-header">PI Active Projects Status</div>
		<div class="portlet-content">
                             <table align="center"   cellspacing="1px" >
                    <tr>
                        <td>

                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/Pie2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=strXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>                            
                        </td>
                    </tr>


                    </table>
        </div>
	</div>
      <%


    ps2 = con2.prepareStatement(qry2);
    ps2.setString(1, "Active");
    rs2 = ps2.executeQuery();

     strXML="";

    strXML = "<chart caption='' showPercentageInLabel='0' showValues='1' showLabels='0' showLegend='0'>";
    strDataRev = "";
    if(rs2.next()){
        strDataRev += "<set value='"+rs2.getInt(1)+"' color='"+colors1[1]+"'  label=''/> ";
        strDataRev += "<set value='"+rs2.getInt(2)+"' color='"+colors1[3]+"'   label='' />";
        strDataRev += "<set value='"+rs2.getInt(3)+"' color='"+colors1[2]+"'   label='' />";
    }
    strXML += strDataRev + "</chart>";
    %>
    <div class="portlet">
		<div class="portlet-header">OSP Active Projects Status</div>
		<div class="portlet-content">
                  <table align="center"   cellspacing="1px" >
                    <tr>
                        <td>

                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/Pie2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=strXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>                           
                        </td>
                    </tr>

                    </table>
        
        </div>
	</div>
        <%


    ps2 = con3.prepareStatement(qry2);
    ps2.setString(1, "Active");
    rs2 = ps2.executeQuery();

     strXML="";

    strXML = "<chart caption='' showPercentageInLabel='0' showValues='1' showLabels='0' showLegend='0'>";
    strDataRev = "";
    if(rs2.next()){
        strDataRev += "<set value='"+rs2.getInt(1)+"' color='"+colors1[1]+"'  label=''/> ";
        strDataRev += "<set value='"+rs2.getInt(2)+"' color='"+colors1[3]+"'   label='' />";
        strDataRev += "<set value='"+rs2.getInt(3)+"' color='"+colors1[2]+"'   label='' />";
    }
    strXML += strDataRev + "</chart>";
    %>
    <div class="portlet">
		<div class="portlet-header">POI Active Projects Status</div>
		<div class="portlet-content">
                  <table align="center"   cellspacing="1px" >
                    <tr>
                        <td>

                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/Pie2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=strXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>                            
                        </td>
                    </tr>


                    </table>

        </div>
	</div>
    

</div>
 
    <%

    ps3 = con1.prepareStatement(qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Complex");
    rs3 = ps3.executeQuery();
    //String[] colors1 = {"82CAFA","0099FF","F88017","FFE87C"};
    String dataSet1 = "<dataset seriesName='' color='8BBA00' showValues='0'> ";
    String dataSet2 = " <dataset seriesName='' color='F6BD0F' showValues='0'> ";
    String dataSet3 = " <dataset seriesName='' color='F88017' showValues='0'> ";
                
    if(rs3.next()){        
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }
    ps3 = con1.prepareStatement(qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Intermediate");
    rs3 = ps3.executeQuery();
    if(rs3.next()){
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }
    ps3 = con1.prepareStatement(qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Standard");
    rs3 = ps3.executeQuery();
    if(rs3.next()){
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }


    strXML="";
    strXML = "<chart palette='2' caption='' showLabels='1' showvalues='0' showSum='1' decimals='0' useRoundEdges='1' legendBorderAlpha='0'>";
    String categoryStr = "<categories> "+
            " <category label='Complex' /> "+
            " <category label='Intermedi' /> "+
            " <category label='Standard' /> "+
            " </categories>";

    dataSet1 = dataSet1 + " </dataset>";
    dataSet2 = dataSet2 + " </dataset>";
    dataSet3 = dataSet3 + " </dataset>";
    
    strXML += categoryStr + dataSet1 + dataSet2 + dataSet3 + "</chart>";
    %>
<div class="column" >

	<div class="portlet">
		<div class="portlet-header">PI Active Projects Distribution</div>
		<div class="portlet-content">
        
                    <table align="center"   cellspacing="1px" >
                    <tr>
                        <td>

                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/StackedColumn2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=strXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>                            
                        </td>
                    </tr>


                    </table>
        
        </div>
	</div>
   <%

    ps3 = con2.prepareStatement(qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Complex");
    rs3 = ps3.executeQuery();
     dataSet1 = "<dataset seriesName='' color='8BBA00' showValues='0'> ";
     dataSet2 = " <dataset seriesName='' color='F6BD0F' showValues='0'> ";
     dataSet3 = " <dataset seriesName='' color='F88017' showValues='0'> ";

    if(rs3.next()){
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }
    ps3 = con2.prepareStatement(qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Intermediate");
    rs3 = ps3.executeQuery();
    if(rs3.next()){
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }
    ps3 = con2.prepareStatement(qry3);
    System.out.println("OSP DB QRY"+qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Standard");
    rs3 = ps3.executeQuery();
    if(rs3.next()){
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }


    strXML="";
    strXML = "<chart palette='2' caption='' showLabels='1' showvalues='0' showSum='1' decimals='0' useRoundEdges='1' legendBorderAlpha='0'>";
    categoryStr = "<categories> "+
            " <category label='Complex' /> "+
            " <category label='Intermedi' /> "+
            " <category label='Standard' /> "+
            " </categories>";

    dataSet1 = dataSet1 + " </dataset>";
    dataSet2 = dataSet2 + " </dataset>";
    dataSet3 = dataSet3 + " </dataset>";

    strXML += categoryStr + dataSet1 + dataSet2 + dataSet3 + "</chart>";
    %>

	
	<div class="portlet">
		<div class="portlet-header">OSP Active Projects Distribution</div>
		<div class="portlet-content">
        
        		<table align="center"   cellspacing="1px" >
                    <tr>
                        <td>

                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/StackedColumn2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=strXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                            
                        </td>
                    </tr>


                    </table>
        
        </div>
	</div>
   <%

    ps3 = con3.prepareStatement(qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Complex");
    rs3 = ps3.executeQuery();

     dataSet1 = "<dataset seriesName='' color='8BBA00' showValues='0'> ";
     dataSet2 = " <dataset seriesName='' color='F6BD0F' showValues='0'> ";
     dataSet3 = " <dataset seriesName='' color='F88017' showValues='0'> ";

    if(rs3.next()){
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }
    ps3 = con3.prepareStatement(qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Intermediate");
    rs3 = ps3.executeQuery();
    if(rs3.next()){
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }
    ps3 = con3.prepareStatement(qry3);
    ps3.setString(1, "Active");
    ps3.setString(2, "Standard");
    rs3 = ps3.executeQuery();
    if(rs3.next()){
         dataSet1 = dataSet1 + " <set value='"+rs3.getInt(1)+"' />";
         dataSet2 = dataSet2 + " <set value='"+rs3.getInt(2)+"' />";
         dataSet3 = dataSet3 + " <set value='"+rs3.getInt(3)+"' />";
    }


    strXML="";
    strXML = "<chart palette='2' caption='' showLabels='1' showvalues='0' showSum='1' decimals='0' useRoundEdges='1' legendBorderAlpha='0'>";
    categoryStr = "<categories> "+
            " <category label='Complex' /> "+
            " <category label='Intermedi' /> "+
            " <category label='Standard' /> "+
            " </categories>";

    dataSet1 = dataSet1 + " </dataset>";
    dataSet2 = dataSet2 + " </dataset>";
    dataSet3 = dataSet3 + " </dataset>";

    strXML += categoryStr + dataSet1 + dataSet2 + dataSet3 + "</chart>";
    %>


	<div class="portlet">
		<div class="portlet-header">POI Active Projects Distribution</div>
		<div class="portlet-content">

        		<table align="center"   cellspacing="1px" >
                    <tr>
                        <td>

                            <jsp:include page="FusionChartsRenderer.jsp" flush="true">
                                <jsp:param name="chartSWF" value="/tatacomm/swf/StackedColumn2D.swf" />
                                <jsp:param name="strURL" value="" />
                                <jsp:param name="strXML" value="<%=strXML %>" />
                                <jsp:param name="chartId" value="productSales" />
                                <jsp:param name="chartWidth" value="230" />
                                <jsp:param name="chartHeight" value="180" />
                                <jsp:param name="debugMode" value="false" />
                                <jsp:param name="registerWithJS" value="false" />
                            </jsp:include>
                            
                        </td>
                    </tr>


                    </table>

        </div>
	</div>
</div>

  

</div><!-- End demo -->

<%
}catch (Exception e) {
    e.printStackTrace();
}finally {
    try{
        con1.close();
        con2.close();
        con3.close();
        con4.close();
    }catch(Exception ex){
    }
}
%>



</body>
</html>
