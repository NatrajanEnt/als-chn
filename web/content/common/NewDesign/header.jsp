<html >
<head>
  <%@page language="java" contentType="text/html; charset=UTF-8"%>

  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">
  <link rel="shortcut icon" href="images/favicon.png" type="image/png">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <title>Throttle - Leading Transport Management System</title>

  <link href="content/NewDesign/css/style.default.css" rel="stylesheet">
  <link rel="stylesheet" href="/throttle/css/jquery-ui.css">
  <%
    String selectedLanguage = (String)session.getAttribute("paramName");
    System.out.println("selectedLanguage:"+selectedLanguage);
    if("ar".equals(selectedLanguage)){
    %>
    <link href="content/NewDesign/css/style.default-rtl.css" rel="stylesheet">
  <%
  }
  %>

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->


<script>
    disableRC();
          $(document).ready(function () {
           jQuery('body').addClass('stickyheader');
        });

  </script>

  
<!--  <script >
    var ALERT_TITLE = "Oops!";
    var ALERT_BUTTON_TEXT = "Ok";

    if(document.getElementById) {
            window.alert = function(txt) {
                    createCustomAlert(txt);
            }
    }

    function createCustomAlert(txt) {
            d = document;

            if(d.getElementById("modalContainer")) return;

            mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
            mObj.id = "modalContainer";
            mObj.style.height = d.documentElement.scrollHeight + "px";

            alertObj = mObj.appendChild(d.createElement("div"));
            alertObj.id = "alertBox";
            if(d.all && !window.opera) alertObj.style.top = document.documentElement.scrollTop + "px";
            alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth)/2 + "px";
            alertObj.style.visiblity="visible";

            h1 = alertObj.appendChild(d.createElement("h1"));
            h1.appendChild(d.createTextNode(ALERT_TITLE));

            msg = alertObj.appendChild(d.createElement("p"));
            //msg.appendChild(d.createTextNode(txt));
            msg.innerHTML = txt;

            btn = alertObj.appendChild(d.createElement("a"));
            btn.id = "closeBtn";
            btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
            btn.href = "#";
            btn.focus();
            btn.onclick = function() { removeCustomAlert();return false; }

            alertObj.style.display = "block";

    }

    function removeCustomAlert() {
            document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));
    }
    function ful(){
    alert('Alert this pages');
    }
</script>-->
  
  <style>
    #modalContainer {
            background-color:rgba(0, 0, 0, 0.3);
            position:absolute;
            width:100%;
            height:100%;
            top:0px;
            left:0px;
            z-index:10000;
            background-image:url(tp.png); /* required by MSIE to prevent actions on lower z-index elements */
    }

    #alertBox {
            position:relative;
            width:500px;
            min-height:100px;
            margin-top:50px;
            border:1px solid #666;
            background-color:#fff;
            background-repeat:no-repeat;
            background-position:20px 30px;
    }

    #modalContainer > #alertBox {
            position:fixed;
    }

    #alertBox h1 {
            margin:0;
            font:bold 0.9em verdana,arial;
            background-color:#3073BB;
            color:#FFF;
            border-bottom:1px solid #000;
            padding:2px 0 2px 5px;
    }

    #alertBox p {
            font:1.2em verdana,arial;
            height:50px;
            padding-left:5px;
            margin-left:55px;
            color:red;
    }

    #alertBox #closeBtn {
            display:block;
            position:relative;
            margin:5px auto;
            padding:7px;
            border:0 none;
            width:70px;
            font:0.7em verdana,arial;
            text-transform:uppercase;
            text-align:center;
            color:#FFF;
            background-color:#357EBD;
            border-radius: 3px;
            text-decoration:none;
    }

    /* unrelated styles */

    #mContainer {
            position:relative;
            width:600px;
            margin:auto;
            padding:5px;
            border-top:2px solid #000;
            border-bottom:2px solid #000;
            font:0.7em verdana,arial;
    }

    code {
            font-size:1.2em;
            color:#069;
    }

    #credits {
            position:relative;
            margin:25px auto 0px auto;
            width:350px;
            font:0.7em verdana;
            border-top:1px solid #000;
            border-bottom:1px solid #000;
            height:90px;
            padding-top:4px;
    }

    #credits img {
            float:left;
            margin:5px 10px 5px 0px;
            border:1px solid #000000;
            width:80px;
            height:79px;
    }

    .important {
            background-color:#F5FCC8;
            padding:2px;
    }

    code span {
            color:green;
    }
</style>




</head>

<body class="horizontal-menu-sidebar stickyheader leftpanel-collapsed">
<!-- Preloader -->
<div id="preloader">
    <div id="status"><i class="fa fa-spinner fa-spin"></i></div>
</div>

<section>
  
  <div class="leftpanel sticky-leftpanel" style="background: #418BCA;">
    
    <div class="logopanel">
<!--        <h1><span>[</span> bracket <span>]</span></h1>-->
        <img src="/throttle/images/Throttle_Logo.png" alt="throttle Logo" width="210" height="30" />
    </div><!-- logopanel -->
        
    <div class="leftpanelinner">    
        
        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">   
            <div class="media userlogged">
<!--                <img alt="" src="content/NewDesign/images/photos/loggeduser.png" class="media-object">-->
                <div class="media-body">
                    <h4><%= session.getAttribute("userName") %></h4>
                    
                </div>
            </div>
          
            <h5 class="sidebartitle actitle">Account</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
<!--              <li><a href="profile.html"><i class="fa fa-user"></i> <span>Profile</span></a></li>-->
              <li><a href="#"><i class="fa fa-cog"></i> <span>Account Settings</span></a></li>
              <li><a href="#"><i class="fa fa-question-circle"></i> <span>Help</span></a></li>
              <li><a href="/throttle/logout.do"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
            </ul>
        </div>