<%--
    Document   : cityMaster
    Created on : Dec 3, 2013, 10:43:13 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
                        <script src="/throttle/js/jquery.ui.core.js"></script>
                        <script src="/throttle/js/jquery.ui.datepicker.js"></script>a
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- js for start   <tab>-->
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!--    <end>tab-->

<!-- jQuery libs -->


<!-- Our jQuery Script to make everything work -->

<script  type="text/javascript" src="js/jq-ac-script.js"></script>
<!--FOR google City Starts-->

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
<html>
    <head id="Head1" runat="server">
        
        <script type="text/javascript">
                $(document).ready(function () {

                    $("#datepicker").datepicker({
                        showOn: "button",
                        buttonImage: "calendar.gif",
                        buttonImageOnly: true

                    });



                });

                $(function () {
                    $(".datepicker").datepicker({
                        changeMonth: true, changeYear: true,
//                        dateFormat: 'dd-mm-yy'
                        dateFormat: 'yy-mm-dd'
                    });

                });


                        </script>
        <script type="text/javascript">
            var source, destination;
            var directionsDisplay;
            var directionsService = new google.maps.DirectionsService();
            google.maps.event.addDomListener(window, 'load', function() {
                new google.maps.places.SearchBox(document.getElementById('googleCityName'));

                directionsDisplay = new google.maps.DirectionsRenderer({'draggable': true});

            });
           /* function setCityName() {
                var tempdestination = document.getElementById("googleCityName").value;

                var tempdestination1 = tempdestination.split(",");
                //  var tempdestination2=tempdestination1.split("-");
                // alert(tempdestination2[0]);
                //  var tempdestination2 = tempdestination1.split("-");
                document.getElementById("googleCityName").value = tempdestination1[0];
//                document.getElementById("cityName").value = tempdestination1[0];
                getLatLng();
            }*/

           

            function getLatLng() {

                var address = document.getElementById("googleCityName").value;
                var geocoder = new google.maps.Geocoder();
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var longaddress = results[0].address_components[0].long_name;
                        var latitute = results[0].geometry.location.lat();
                        //                         var latitude = results[0].geometry.location.lat();
                        //alert(latitute);
                        var longitude = results[0].geometry.location.lng();
                        // alert(longitude);
                        //                         latitude.value = latitute;
                        //                         longitute.value = longitude;
                        document.getElementById("latitude").value = latitute.toFixed(5);
                        document.getElementById("longitude").value = longitude.toFixed(5);
                        initialize(results[0].geometry.location.lat(), results[0].geometry.location.lng(), longaddress);
                    } else {
//                        alert('Geocode error: ' + status);
                    }
                });
            }
             function initialize(lat, lng, address) {
//                 alert(lat+"lng"+ lng+"add"+ address);
                var mapOptions = {
                    center: new google.maps.LatLng(lat, lng),
                    zoom: 4,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById("myMap"),
                        mapOptions);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    title: address
                });

                marker.setMap(map);

                var infotext = address + '<hr>' +
                        'Latitude: ' + lat + '<br>Longitude: ' + lng;
                var infowindow = new google.maps.InfoWindow();
                infowindow.setContent(infotext);
                infowindow.setPosition(new google.maps.LatLng(lat, lng));
                infowindow.open(map);
            }
        </script>
        <!--FOR google City ends-->

        <script type="text/javascript">
           
            $(document).ready(function() {
                // Use the .autocomplete() method to compile the list based on input from user
                $('#cityName').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityTollList.do",
                            dataType: "json",
                            data: {
                                cityName: request.term
                            },
                            success: function(data, textStatus, jqXHR) {
                                var items = data;
                                if (items == '') {
                                    $('#cityId').val('');
                                    $('#cityName').val('');
                                }
                                response(items);
                            },
                            error: function(data, type) {
                                console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('-');
                        $('#cityId').val(tmp[0]);
                        $('#cityName').val(tmp[1]);
                        return false;
                    }
                }).data("autocomplete")._renderItem = function(ul, item) {
                    var itemVal = item.Name;
                    var temp = itemVal.split('-');
                    itemVal = '<font color="green">' + temp[1] + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
            });
</script>
        
        
 <script type="text/javascript">
             var httpRequest;
            function getTollCityName(cityName)
            {
//                alert(cityName);
                if (cityName != "") {
                    var url = "/throttle/cityNameList.do?cityName=" + cityName;
                    url = url + "&sino=" + Math.random();
                    if (window.ActiveXObject) {
                        httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    else if (window.XMLHttpRequest) {
                        httpRequest = new XMLHttpRequest();
                    }
                    httpRequest.open("GET", url, true);
                    httpRequest.onreadystatechange = function() {
                        processRequest1();
                    };
                    httpRequest.send(null);
                }
            }


            function processRequest1()
            {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        if (httpRequest.responseText.valueOf() != "") {
                            var detail = httpRequest.responseText.valueOf();
                            if (detail != "null") {
                                var vehicleValues = detail.split("~");
                                document.cityMaster.zoneId.value = vehicleValues[2];
                                document.cityMaster.zoneName.value = vehicleValues[3];
                                document.cityMaster.countryId.value = vehicleValues[4];
                                document.cityMaster.countryName.value = vehicleValues[5];
                                document.cityMaster.currencyCode.value = vehicleValues[6];

                            } else {
                                document.cityMaster.zoneId.value = "";
                                document.cityMaster.zoneName.value = "";
                                document.cityMaster.countryId.value = "";
                                document.cityMaster.countryName.value = "";
                                document.cityMaster.currencyCode.value = "";
                            }
                        }
                    }
                    else
                    {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }
        </script>

        <script>
            function citysubmit()
            {
                var errStr = "";
                var nameCheckStatus = $("#cityNameStatus").text();
                if (document.getElementById("cityName").value == "") {
                    errStr = "Please enter cityName.\n";
                    alert(errStr);
                    document.getElementById("cityName").focus();
                } else if (nameCheckStatus != "") {
                    if (document.getElementById("editValue").value != "1") {
                        errStr = "City Name Already Exists.\n";
                        alert(errStr);
                        document.getElementById("cityName").focus();
                    }
                } else if (document.getElementById("zoneName").value == "") {
                    errStr = "Please select valid zoneName.\n";
                    alert(errStr);
                    document.getElementById("zoneName").focus();
                } else if (document.getElementById("zoneName").value != "" && document.getElementById("zoneId").value == "") {
                    errStr = "Please select valid zoneName.\n";
                    alert(errStr);
                    document.getElementById("zoneName").focus();
                } else if (document.getElementById("cityCode").value == "") {
                    errStr = "Please enter cityCode.\n";
                    alert(errStr);
                    document.getElementById("cityCode").focus();

                } else if (document.getElementById("countryName").value == "") {
                    errStr = "Please select valid countryName.\n";
                    alert(errStr);
                    document.getElementById("countryName").focus();
                } else if (document.getElementById("countryName").value != "" && document.getElementById("countryId").value == "") {
                    errStr = "Please select valid countryName.\n";
                    alert(errStr);
                    document.getElementById("countryName").focus();
                } else if (document.getElementById("latitude").value == "") {
                    errStr = "Please enter latitude.\n";
                    alert(errStr);
                    document.getElementById("latitude").focus();
                } else if (document.getElementById("longitute").value == "") {
                    errStr = "Please enter longitute.\n";
                    alert(errStr);
                    document.getElementById("longitute").focus();
                }
                if (errStr == "") {
                    document.cityMaster.action = "/throttle/saveCityMaster.do";
                    document.cityMaster.method = "post";
                    document.cityMaster.submit();
                }
            }



            function setValues(sno, cityId, cityName, zoneName, zoneId, status, countryId, countryName, cityCode, latitude, longitute, icdLocation, googleCityName, borderStatus) {
                var count = parseInt(document.getElementById("count").value);
                document.getElementById('inActive').style.display = 'block';
                for (i = 1; i <= count; i++) {
                    if (i != sno) {
                        document.getElementById("edit" + i).checked = false;
                    } else {
                        document.getElementById("edit" + i).checked = true;
                    }
                }
                document.getElementById("cityId").value = cityId;
                document.getElementById("cityName").value = cityName;
                document.getElementById("zoneName").value = zoneName;
                document.getElementById("zoneId").value = zoneId;
                document.getElementById("status").value = status;
                document.getElementById("countryId").value = countryId;
                document.getElementById("countryName").value = countryName;
                document.getElementById("cityCode").value = cityCode;
                document.getElementById("latitude").value = latitude;
                document.getElementById("longitute").value = longitute;
                document.getElementById("icdLocation").value = icdLocation;
                document.getElementById("googleCityName").value = googleCityName;
                document.getElementById("editValue").value = "1";
                document.getElementById("borderStatus").value = borderStatus;
                //        $("#cityName").val().readOnly;
            }
          
            function onKeyPressBlockNumbers(e)
            {
                var key = window.event ? e.keyCode : e.which;
                var keychar = String.fromCharCode(key);
                reg = /\d/;
                return !reg.test(keychar);
            }

            var httpRequest;
            function checkCityName() {

                var cityName = document.getElementById('cityName').value;
                var url = '/throttle/checkCityName.do?cityName=' + cityName;
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);

            }


            function processRequest() {
                if (httpRequest.readyState == 4) {
                    if (httpRequest.status == 200) {
                        var val = httpRequest.responseText.valueOf();
                        if (val != "" && val != 'null') {
                            $("#nameStatus").show();
                            $("#cityNameStatus").text('Please Check City Name  :' + val + ' is Already Exists');
                        } else {
                            $("#nameStatus").hide();
                            $("#cityNameStatus").text('');
                        }
                    } else {
                        alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                    }
                }
            }


        </script>
        <script>
           

            function getEvents(e, val) {
                var key;
                if (window.event) {
                    key = window.event.keyCode;
                } else {
                    key = e.which;
                }
                if (key == 0) {
                    getTollCityName(val);
                    //getVehicleExisting(val);
                } else {
                    getTollCityName(val);
                    //getVehicleExisting(val);
                }
            }
            
 function tollCitysubmit() {
    var tollId = $("#tollId").val();
    var tollName = document.getElementById("tollName").value;
    var cityId = document.getElementById("cityId").value;
    var zoneId = document.getElementById("zoneId").value;
    var countryId = document.getElementById("countryId").value;
    var googleCityName = document.getElementById("googleCityName").value;
    var latitude = document.getElementById("latitude").value;
    var longitude = document.getElementById("longitude").value;
    var fromDate = document.getElementById("fromDate").value;
    var toDate = document.getElementById("toDate").value;
    var currencyCode = $("#currencyCode").val();
    var insertStatus = 0;
    if (document.cityMaster.tollName.value == '') {
        alert('Please Enter tollName ');
        $("#tollName").focus();
        return;
    } else if (document.cityMaster.cityId.value == '') {
        alert('Please Enter city Name');
        $("#cityId").focus();
        return;
    } else if (document.cityMaster.zoneId.value == '') {
        alert('Please Enter zone Name');
        $("#zoneId").focus();
        return;
    } else if (document.cityMaster.countryId.value == '') {
        alert('Please Enter country Name');
        $("#countryId").focus();
        return;
    } else if (document.cityMaster.googleCityName.value == '') {
        alert('Please Enter Location');
        $("#googleCityName").focus();
        return;
    }  else if (document.cityMaster.fromDate.value == '') {
        alert('Please Enter  Valid FromDate');
        $("#fromDate").focus();
        return;
    } else if (document.cityMaster.toDate.value == '') {
        alert('Please Enter  Valid Todate');
        $("#toDate").focus();
        return;
    } 
    
    var url = '';
    var tollRateId=[];
    var axleName=[];
    var singleJourney=[];
    var returnJourney=[];
    var monthlyPass=[];
    var localVehicleCost=[];
    var vehTypeId=[];
    var tollRateIds = document.getElementsByName("tollRateId");
    var axleNames = document.getElementsByName("axleName");
    var singleJourneys = document.getElementsByName("singleJourney");
    var returnJourneys = document.getElementsByName("returnJourney");
    var monthlyPasses = document.getElementsByName("monthlyPass");
    var localVehicleCosts = document.getElementsByName("localVehicleCost");
    var vehTypeIds = document.getElementsByName("vehTypeId");
    for(var i=0 ;i<axleNames.length; i++){
//        alert(tollRateIds[i].value);
        tollRateId.push(tollRateIds[i].value);
        axleName.push(axleNames[i].value);
        singleJourney.push(singleJourneys[i].value);
        monthlyPass.push(monthlyPasses[i].value);
        returnJourney.push(returnJourneys[i].value);
        localVehicleCost.push(localVehicleCosts[i].value);
        vehTypeId.push(vehTypeIds[i].value);
        if(document.getElementById("singleJourney"+i).value == '0'){
        alert('Please Enter singleJourney ');
        $("#singleJourney").focus();
        return;    
        }
        else if(document.getElementById("returnJourney"+i).value == '0'){
        alert('Please Enter returnJourney ');
        $("#returnJourney").focus();
        return;    
        }
        else if(document.getElementById("monthlyPass"+i).value == '0'){
        alert('Please Enter monthlyPass ');
        $("#monthlyPass").focus();
        return;    
        }
        else if(document.getElementById("localVehicleCost"+i).value == '0'){
        alert('Please Enter localVehicleCost ');
        $("#localVehicleCost").focus();
        return;    
        }
    }
    url = './insertTollgetDetails.do';
    $.ajax({
        url: url,
        data: {tollName: tollName, cityId: cityId, zoneId: zoneId, countryId: countryId, googleCityName: googleCityName, latitude: latitude,
            longitude: longitude,fromDate: fromDate, toDate: toDate, currencyCode: currencyCode,tollId: tollId,
            axleNameVal: axleName,
            singleJourneyVal: singleJourney, returnJourneyVal: returnJourney,
            monthlyPassVal: monthlyPass, localVehicleCostVal: localVehicleCost, 
            vehTypeIdVal: vehTypeId,tollRateIdVal:tollRateId
        },
        type: "POST",
        dataType:'json',
        success: function (response) {
            insertStatus = response.toString().trim();
            if (insertStatus == 0) {
                $("#Status").text("Tollget added failed ");
            } else {
                document.getElementById("tollId").value=insertStatus;
                
                $("#Status").text("Tollget added sucessfully ");
            }
        },
        error: function (xhr, status, error) {
        }
    });
    document.getElementById("tollSave").style.visibility = 'hidden';
    document.getElementById("tollUpdate").style.display ='block';
   }    

</script>
        
        <script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });

  function visibility(){
//      alert(par);
          if(document.getElementById("tollId").value == 0 ){
            document.getElementById("tollSave").style.disply = 'block';
            document.getElementById("tollUpdate").style.display = "none"; 
            }
            
          if('<%=request.getAttribute("param")%>'==2){
            document.getElementById("tollUpdate").style.display = "none"; 
            document.getElementById("tollSave").style.visibility = 'hidden';
            }
          if('<%=request.getAttribute("paramEdit")%>'==1){
            document.getElementById("tollUpdate").style.display = "block"; 
            document.getElementById("tollSave").style.visibility = 'hidden';
            }
     
        }
  
        </script>       

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>City Master Brattle Food</title>
    </head>
    <body onload="visibility();">
        <!--<body onload="initialize(28.3213, 77.5435, 'New Delhi')">-->
        <form name="cityMaster"  method="POST">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
             <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">

                                    <div align="center" id="Status">&nbsp;&nbsp;
                                    </div>
                                </font>
            <table  border="0" class="border" align="center" width="980" cellpadding="0" cellspacing="0" id="bg">
                
                <input type="hidden" name="tollId" id="tollId" value="<c:out value="${tollId}"/>"/>
                <input type="hidden" name="param" id="param" value="<%=request.getAttribute("param")%>"/>
                
                <tr>
                <table width="980" align="center" class="table2" cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="contenthead" colspan="4" >Toll Gate Master</td>
                    </tr>
                    <tr>
                        <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="cityNameStatus" style="color: red"></label></td>
                    </tr>
                    <tr>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Tollgate Name</td>
                        <td class="text2"><input type="text" name="tollName" id="tollName" class="textbox"  maxlength="50" value="<c:out value="${tollName}"/>"></td>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Throttle City Name</td>
                        <td class="text1"><input type="hidden" name="cityId" id="cityId" value="<c:out value="${cityId}"/>"  class="textbox">
                            <input type="text" name="cityName" id="cityName" class="textbox" value="<c:out value="${cityName}"/>" onkeypress="getEvents(event, this.value);" onclick="getTollCityName(this.value);" onkeypress="return onKeyPressBlockNumbers(event);"  maxlength="50"></td>
                    </tr>
                    <tr>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>City Zone Name</td>
                        <td class="text2"><input type="hidden" name="zoneId" id="zoneId" value="<c:out value="${zoneId}"/>" onfocus="getTollCityName(cityName.value);"  class="textbox">
                            <input type="text" name="zoneName" id="zoneName" value="<c:out value="${zoneName}"/>" onfocus="getTollCityName(cityName.value);" class="textbox" readonly></td>
                        <td 
                            class="text1">&nbsp;&nbsp;<font color="red">*</font>City Country</td>
                        <td class="text1">
                            <input type="hidden" name="countryId" id="countryId" value="<c:out value="${countryId}"/>"  onfocus="getTollCityName(cityName.value);" class="textbox">
                            <input type="hidden" name="editValue" id="editValue" value="0" class="textbox">
                            <input type="hidden" name="currencyCode" id="currencyCode" value="<c:out value="${currencyCode}"/>" onfocus="getTollCityName(cityName.value);" class="textbox">
                            <input type="text" name="countryName" id="countryName" value="<c:out value="${countryName}"/>" onfocus="getTollCityName(cityName.value);" readonly class="textbox"></td>

                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;<font color="red">*</font>Location</td>
                        <td class="text1" colspan="4">
                            <input type="text" name="googleCityName" id="googleCityName" class="textbox" value="<c:out value="${googleCityName}"/>" onkeypress="return onKeyPressBlockNumbers(event);"  onchange="getLatLng();"   autocomplete="off" maxlength="50" placeholder="Enter a location" style="width:500px;height: 30px;">
                            <!--<textarea name="googleCityName" id="googleCityName" class="textbox" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checkCityName();"  autocomplete="off" maxlength="50" placeholder="Enter a location"  style="width:800px"></textarea>-->
                        </td>
                    </tr>

                    <tr>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Latitude</td>
                        <td class="text2"><input type="text" name="latitude" id="latitude" class="textbox"  value="<c:out value="${latitude}"/>" maxlength="50"></td>
                        <td class="text2">&nbsp;&nbsp;<font color="red">*</font>Longitute</td>
                        <td class="text2"><input type="text" name="longitude" id="longitude" value="<c:out value="${longitude}"/>"  class="textbox"></td>

                    </tr>
                    <tr>
                        <td class="text1">&nbsp;&nbsp;ValidFrom</td>
                        <td class="text1">
                            <input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" size="20">
                        </td>
                        <td class="text1">&nbsp;&nbsp;ValidTo</td>
                        <td class="text1">
                            <input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"  size="20">
                        </td>
                    </tr>


                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="button" value="Save" id="tollSave" name="Submit"  onClick="tollCitysubmit()">
                            <input type="button" class="button" value="Update" id="tollUpdate" name="Submit" style="display:none" onClick="tollCitysubmit()">


                        </center>
                    </td>
                </tr>
            </table>

            <br>
            <br>
            <br>
            <div id="tabs" style="overflow: auto">
                <ul style="width: auto">
                    <li><a href="#tollrate"><span>Toll Rate</span></a></li>
                    <li><a href="#tollMap"><span>Toll Map</span></a></li>
                </ul>
                <% int count = 0;%>
                <div id="tollrate">     
                    <c:if test="${tollMasterList != null}">
                        <table width="96%" align="center" border="0" id="table1" class="sortable">
                            <thead>
                                <tr height="70">
                                    <th><h3>S.No</h3></th>
                                    <!--<th><h3>Mfr </h3></th>-->
                                    <th><h3>Vehicle Type </h3></th>
                                    <th><h3>Axle Count </h3></th>
                                    <th><h3>Single Journey </h3></th>
                                    <th><h3>Return Journey</h3></th>
                                    <th><h3>Monthly Pass </h3></th>
                                    <th><h3>Local Vehicle Cost </h3></th>
                                    
                                </tr>
                            </thead>
                            <% int index = 0;
                                        int sno = 1;
                            %>
                            <tbody>
                                <c:forEach items="${tollMasterList}" var="mcl">
                                    <%
                                                String classText = "";
                                                int oddEven = index % 2;
                                                if (oddEven > 0) {
                                                    classText = "text1";
                                                } else {
                                                    classText = "text2";
                                                }
                                    %>

                                    <tr height="30">
                                        <td align="left" class="<%=classText%>"><%=sno%>
                                        <input type="hidden" name="tollRateId" id="tollRateId<%=index%>" value="<c:out value="${mcl.tollRateId}"/>"/></td>
                                        <td align="left" class="<%=classText%>" style="width: 200px"><input type="hidden" name="vehTypeId" id="vehTypeId" value="<c:out value="${mcl.vehTypeId}"/>" readonly/><c:out value="${mcl.vehicleTonnage}"/>
                                        </td>
                                        <td align="left" class="<%=classText%>">
                                            <input type="hidden" name="axleName" id="axleName<%=index%>" readonly value="<c:out value="${mcl.axleName}"/>"/> <c:out value="${mcl.axleName}"/>                                            
                                        </td>
                                        <td align="left" class="<%=classText%>">
                                            <input type="text" name="singleJourney" id="singleJourney<%=index%>"   style="width: 90px" value="<c:out value="${mcl.singleJourney}"/>"/></td>
                                        <td align="left" class="<%=classText%>">
                                            <input type="text" name="returnJourney" id="returnJourney<%=index%>"   style="width: 90px" value="<c:out value="${mcl.returnJourney}"/>"/></td>
                                        <td align="left" class="<%=classText%>">
                                            <input type="text" name="monthlyPass" id="monthlyPass<%=index%>"   style="width: 90px" value="<c:out value="${mcl.monthlyPass}"/>"/></td>
                                        <td align="left" class="<%=classText%>">
                                            <input type="text" name="localVehicleCost" id="localVehicleCost<%=index%>"   style="width: 90px" value="<c:out value="${mcl.localVehicleCost}"/>"/></td>
                                       
                                    </tr>
                                    <%sno++;%>
                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                           
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table1");</script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="200">200</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 0);
                        </script>
                    </c:if>
                    <br/>
                    <br/>
                    <center>
                    
                        <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Next" /></a>
                    </center>
                </div>
                <div id="tollMap">
                    <div id="myMap" style="width: 1000px; height: 400px; margin-top:20px;"></div>
                </div>
                
                
            </div>
            <br>
            <script>
                $(".nexttab").click(function () {
                    var selected = $("#tabs").tabs("option", "selected");
                    $("#tabs").tabs("option", "selected", selected + 1);
                });
                $(".prevtab").click(function () {
                    var selected = $("#tabs").tabs("option", "selected");
                    $("#tabs").tabs("option", "selected", selected - 1);
                });
            </script>


            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>
            <br>
            <br>
            
            <br>
            <br>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
        <c:if test="${tollId != 0}">
            <script>
                var latitude = $("#latitude").val();
                var longitude = $("#longitude").val();
                var googleCityName = $("#googleCityName").val();
                initialize(latitude,longitude,googleCityName);
            </script>
        </c:if>         
    </body>
</html>