
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>

<script type="text/javascript">
    function printTripAdvance(tripSheetId) {
        window.open('getExpensePrintDetails.do?tripId=' + tripSheetId + '&param=advance', 'PopupPage', 'height = 700, width = 900, scrollbars = yes, resizable = yes');
    }
    function printTripFuel(tripSheetId) {
        window.open('getExpensePrintDetails.do?tripId=' + tripSheetId + '&param=fuel', 'PopupPage', 'height = 700, width = 900, scrollbars = yes, resizable = yes');
    }
    function mailTriggerPage(tripId) {
        window.open('/throttle/viewTripDetailsMail.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewCurrentLocation(vehicleId) {
        window.open('/throttle/viewVehicleLocation.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
//        var url = ' http://dict.throttletms.com/api/vehicleonmap/map.php?uname=interem&group_id=5087&height=600&width=700&vehicle=' + vehicleRegNo;
        //alert(url);
        window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewTripDetails(tripId, vehicleId, tripType) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails1(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        document.tripSheet.action = '/throttle/viewVehicle.do?vehicleId=' + vehicleId;
        document.tripSheet.submit();
    }
    function viewTripInwardDetails(subStatusId, movement, vendorType) {
//        alert("subStatusId::"+subStatusId);
//        alert("vendorType::"+vendorType);
    document.tripSheet.action = '/throttle/viewTripSheets.do?menuClick=1&statusId=10&tripType=1&admin=No&movement=' + movement + '&subStatusId=' + subStatusId + "&vendorType=" + vendorType;
//      document.tripSheet.action = '/throttle/viewTripSheets.do?tripSheetId=' + tripId;
    document.tripSheet.submit();
    }
    
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<style>
    #myTableStyle{
        border: 1px solid white;
    }  
</style>


<script type="text/javascript">
    function setValues() {
        if ('<%=request.getAttribute("statusId")%>' != 'null') {
            document.getElementById('stsId').value = '<%=request.getAttribute("statusId")%>';
        }
        if ('<%=request.getAttribute("statusId")%>' != 'null') {
            document.getElementById('tripStatusId').value = '<%=request.getAttribute("statusId")%>';
        }
        if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
            document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
        }
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
            document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
        }
        if ('<%=request.getAttribute("cityFromId")%>' != 'null') {
            document.getElementById('cityFromId').value = '<%=request.getAttribute("cityFromId")%>';
        }
        if ('<%=request.getAttribute("cityFrom")%>' != 'null') {
            document.getElementById('cityFrom').value = '<%=request.getAttribute("cityFrom")%>';
        }
        if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
            document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
        }
        if ('<%=request.getAttribute("zoneId")%>' != 'null') {
            document.getElementById('zoneId').value = '<%=request.getAttribute("zoneId")%>';
        }

        if ('<%=request.getAttribute("tripSheetId")%>' != 'null') {
            document.getElementById('tripSheetId').value = '<%=request.getAttribute("tripSheetId")%>';
        }
        if ('<%=request.getAttribute("customerId")%>' != 'null') {
            document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
        }
//            if('<%=request.getAttribute("tripStatusId")%>' != 'null' ){
//                document.getElementById('tripStatusId').value= '<%=request.getAttribute("tripStatusId")%>';
//            }
        if ('<%=request.getAttribute("podStatus")%>' != 'null') {
            document.getElementById('podStatus').value = '<%=request.getAttribute("podStatus")%>';
        }

    }


    function submitPage() {
        //document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('stsId').value + "&param=search";
        document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('tripStatusId').value + "&param=search";
        document.tripSheet.submit();
    }

    function submitPage1() {
        var subStatusId = document.getElementById('subStatusId').value;
//       alert(subStatusId);
        document.tripSheet.action = '/throttle/viewTripSheets.do?subStatusId=' + subStatusId + '&param=ExportExcel';
        document.tripSheet.submit();
    }
    function changeVehicleAfterTripStart(tripId, vehicleId, tripType) {
        document.tripSheet.action = '/throttle/changeVehicleAfterTripStart.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId;
        document.tripSheet.submit();
    }


</script>

<script type="text/javascript">
    var httpRequest;
    function getLocation() {
        var zoneid = document.getElementById("zoneId").value;
        if (zoneid != '') {

            // Use the .autocomplete() method to compile the list based on input from user
            $('#cityFrom').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/throttle/getLocationName.do",
                        dataType: "json",
                        data: {
                            cityId: request.term,
                            zoneId: document.getElementById('zoneId').value
                        },
                        success: function (data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function (data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#cityFromId').val(tmp[0]);
                    $('#cityFrom').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function (ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        }
    }

</script>


<style>
    html {
        -webkit-font-smoothing: antialiased!important;
        -moz-osx-font-smoothing: grayscale!important;
        -ms-font-smoothing: antialiased!important;
    }
    body {
        font-family: 'Open Sans', sans-serif;
        font-size:16px;
        color:#555555;
    }
    .md-stepper-horizontal {
        display:table-cell;
        position:relative;
        width:70%;
        margin:0 auto;
        background-color:#FFFFFF;
        box-shadow: 0 3px 8px -6px rgba(0,0,0,.50);
    }
    .md-stepper-horizontal .md-step {
        display:table-cell;
        position:relative;
        padding:24px;
    }
    .md-stepper-horizontal .md-step:hover,
    .md-stepper-horizontal .md-step:active {
        background-color:rgba(0,0,0,0.04);
    }
    .md-stepper-horizontal .md-step:active {
        border-radius: 15% / 75%;
    }
    .md-stepper-horizontal .md-step:first-child:active {
        border-top-left-radius: 0;
        border-bottom-left-radius: 0;
    }
    .md-stepper-horizontal .md-step:last-child:active {
        border-top-right-radius: 0;
        border-bottom-right-radius: 0;
    }
    .md-stepper-horizontal .md-step:hover .md-step-circle {
        background-color:#757575;
    }
    .md-stepper-horizontal .md-step:first-child .md-step-bar-left,
    .md-stepper-horizontal .md-step:last-child .md-step-bar-right {
        display:none;
    }
    .md-stepper-horizontal .md-step .md-step-circle {
        width:30px;
        height:30px;
        margin:0 auto;
        background-color:#999999;
        border-radius: 50%;
        text-align: center;
        line-height:30px;
        font-size: 16px;
        font-weight: 600;
        color:#FFFFFF;
    }
    .md-stepper-horizontal.green .md-step.active .md-step-circle {
        background-color:#00AE4D;
    }
    .md-stepper-horizontal.orange .md-step.active .md-step-circle {
        background-color:#F96302;
    }
    .md-stepper-horizontal .md-step.active .md-step-circle {
        background-color: rgb(33,150,243);
    }
    .md-stepper-horizontal .md-step.done .md-step-circle:before {
        font-family:'FontAwesome';
        font-weight:100;
        content: "\f00c";
    }
    .md-stepper-horizontal .md-step.done .md-step-circle *,
    .md-stepper-horizontal .md-step.editable .md-step-circle * {
        display:none;
    }
    .md-stepper-horizontal .md-step.editable .md-step-circle {
        -moz-transform: scaleX(-1);
        -o-transform: scaleX(-1);
        -webkit-transform: scaleX(-1);
        transform: scaleX(-1);
    }
    .md-stepper-horizontal .md-step.editable .md-step-circle:before {
        font-family:'FontAwesome';
        font-weight:100;
        content: "\f040";
    }
    .md-stepper-horizontal .md-step .md-step-title {
        margin-top:16px;
        font-size:16px;
        font-weight:600;
    }
    .md-stepper-horizontal .md-step .md-step-title,
    .md-stepper-horizontal .md-step .md-step-optional {
        text-align: center;
        color:rgba(0,0,0,.26);
    }
    .md-stepper-horizontal .md-step.active .md-step-title {
        font-weight: 600;
        color:rgba(0,0,0,.87);
    }
    .md-stepper-horizontal .md-step.active.done .md-step-title,
    .md-stepper-horizontal .md-step.active.editable .md-step-title {
        font-weight:600;
    }
    .md-stepper-horizontal .md-step .md-step-optional {
        font-size:12px;
    }
    .md-stepper-horizontal .md-step.active .md-step-optional {
        color:rgba(0,0,0,.54);
    }
    .md-stepper-horizontal .md-step .md-step-bar-left,
    .md-stepper-horizontal .md-step .md-step-bar-right {
        position:absolute;
        top:36px;
        height:1px;
        border-top:1px solid #DDDDDD;
    }
    .md-stepper-horizontal .md-step .md-step-bar-right {
        right:0;
        left:50%;
        margin-left:20px;
    }
    .md-stepper-horizontal .md-step .md-step-bar-left {
        left:0;
        right:50%;
        margin-right:20px;
    }
</style>


<div class="pageheader">
    <h2><i class="fa fa-edit"></i><c:if test="${movement == '1'}"> Export</c:if><c:if test="${movement != '1'}"> Import</c:if><c:if test="${vendorType == '0'}"> Own</c:if><c:if test="${vendorType != '0'}"> Hire</c:if> - Tracking Update</h2>
        <div class="breadcrumb-wrapper">
                        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.PrimaryOperation"  text="PrimaryOperation"/></a></li>
            <li class="active"><c:if test="${movement == '1'}"> Export</c:if><c:if test="${movement != '1'}"> Import</c:if><c:if test="${vendorType == '0'}"> Own</c:if><c:if test="${vendorType != '0'}"> Hire</c:if> - Tracking Update</li>
            </ol>
        </div>
    </div>


    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="sorter.size(100);
                    setValues();">
                    <form name="tripSheet" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <input type="hidden"   value="<c:out value="${movement}"/>" class="text" name="movement" id="movement">
                    <input type="hidden"   value="<c:out value="${subStatusId}"/>" class="text" name="subStatusId" id="subStatusId">

                    <div class="md-stepper-horizontal orange" >
                        <c:if test="${tripMovementDetails != null}">
                            <c:forEach items="${tripMovementDetails}" var="tripDetails">
                                <div class="md-step">
                                    <c:if test="${tripDetails.subStatusId != 12}">  
                                        <a href="#" onclick="viewTripInwardDetails('<c:out value="${tripDetails.subStatusId}"/>', '<c:out value="${movement}"/>', '<c:out value="${vendorType}"/>');"><img src="images/<c:out value="${tripDetails.logo}" />" alt="Y"   title="<c:out value="${tripDetails.statusName}" />" height="55" width="55"  /></a>
                                    </c:if>
                                   <c:if test="${tripDetails.subStatusId == 12}">  
                                        <img src="images/<c:out value="${tripDetails.logo}" />" alt="Y"   title="<c:out value="${tripDetails.statusName}" />" height="55" width="55"  />
                                    </c:if>
                                    <div class="step-circle">&nbsp;&nbsp;&nbsp;&emsp;<span><font color='green'><c:out value="${tripDetails.totalPoints}"/></font></span></div>
                                    <!--            <div class="md-step-bar-left"></div>
                                                <div class="md-step-bar-right"></div>-->
                                </div>
                            </c:forEach>
                        </c:if>
                    </div>                   



                    <div class="row" style="display:none;">
                        <div  style="width:35%;" class="col-sm-6 col-md-3" >
                            <div class="panel panel-success panel-stat">
                                <div class="panel-heading">
                                    <div >
                                        <div >
                                            <h4 >
                                                <font  color="white" >      
                                                <c:if test="${movement == '1'}">
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;    Export Movement Live Status Count  
                                                </c:if>
                                                <c:if test="${movement == '2'}">
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;    Import Movement Live Status Count  
                                                </c:if>
                                                <c:if test="${movement == '17'}">
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;    Vessel Empty Movement Live Status Count  
                                                </c:if>
                                                <c:if test="${movement == '18'}">
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;    Plot to Plot Movement Live Status Count  
                                                </c:if>
                                                <c:if test="${movement == '19'}">
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;    Local PNR Movement Live Status Count 
                                                </c:if>
                                                <c:if test="${movement == '19'}">
                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;    Ex-Bonding  Movement Live Status Count 
                                                </c:if>
                                                </font>
                                            </h4>
                                            <div >
                                                <c:if test="${tripMovementDetails != null}">
                                                    <table id="myTableStyle" align="center">
                                                        <c:forEach items="${tripMovementDetails}" var="tripDetails">
                                                            <tr id="myTableStyle">
                                                                <td style="height:10px;width: 340px;" id="myTableStyle" colspan="2">&emsp;<font  color="white" size="3"><b> <c:out value="${tripDetails.statusName}"/></b>&emsp;&emsp;&emsp;</font></td>
                                                                <td id="myTableStyle" style="height:10px;width: 40px;" align="center" ><c:if test="${tripDetails.statusName != 'End'}"><a href="#" onclick="viewTripInwardDetails('<c:out value="${tripDetails.subStatusId}"/>', '<c:out value="${movement}"/>', '<c:out value="${vendorType}"/>');"> <font color="white" size="4"><b>&emsp;<c:out value="${tripDetails.totalPoints}"/></b>&emsp;</font></a></c:if> <c:if test="${tripDetails.statusName == 'End'}"><font color="white" size="4"><b>&emsp;<c:out value="${tripDetails.totalPoints}"/></b>&emsp;</font></c:if></td>
                                                                </tr>
                                                        </c:forEach>
                                                    </table>      
                                                </c:if>

                                            </div>
                                        </div><!-- row -->
                                        <div class="mb15"></div>

                                    </div><!-- stat -->

                                </div><!-- panel-heading -->
                            </div><!-- panel -->
                        </div>
                    </div>

                    <br>
                    <left>
                        <input type="button" class="btn btn-info" name="ExportExcel" id="ExportExcel"  value="<spring:message code="sales.label.customer.Export Excel"  text="Export Excel"/>" onclick="submitPage1(this.name);" >
                    </left>
                    <br>

                    <% int index = 1;%>
                    <c:if test="${tripDetails == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                        <c:if test="${tripDetails != null}">

                        <table class="table table-info mb30 table-bordered" id="table" >
                            <thead >
                                <c:if test = "${pnrUser == 1}" >
                                    <tr >
                                        <th>Sno</th>
                                        <th>PNR No</th>
                                        <th>VesselName</th>
                                        <th>Vehicle No </th>
                                            <c:if test="${statusId >= 12}">
                                            <th>Container No(s) </th>
                                            </c:if>
                                        <th>Trip Code</th>
                                        <th>Order Type </th>
                                        <th>Transporter</th>
                                        <th>InterimDetails </th>
                                        <th>Driver Name with Contact </th>
                                        <th>Vehicle Type </th>
                                        <th >Container Qty</th>
                                            <c:if test="${statusId == 10}">
                                            <th>LinkToMap</th>
                                            </c:if>
                                        <th>Select</th>
                                    </tr>
                                </c:if>
                                <c:if test = "${pnrUser != 1}" >
                                    <tr >
                                        <th>Sno</th>
                                        <th >POD</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Vehicle No </th>
                                        <th>Trip Code</th>
                                        <th>Container No(s) </th>
                                        <th>Seal No(s) </th>
                                        <th><c:if test="${movement == '1'}">Tonnage </c:if><c:if test="${movement != '1'}">pkg/wgt/cbm</c:if> </th>
                                            <th>Goods Desc</th>
                                            <th>Trip Date </th>
                                            <th>Transporter</th>
                                            <th>Route </th>
                                            <th>Driver Name with Contact </th>
                                            <th>Vehicle Type </th>                                    
                                            <th>Status</th>                                    
                                            <th>WaitingPeriod(HH:MM)/ Urgency</th>                                    
                                            <c:if test="${statusId == 10}">
                                            <th>LinkToMap</th>
                                            </c:if>
                                        <th>Select</th>
                                    </tr>
                                </c:if>
                            </thead>
                            <tbody>


                                <c:set var="totalHours" value=""/>
                                <c:set var="tripSubStatusId" value=""/>

                                <c:forEach items="${tripDetails}" var="tripDetails">
                                    <c:set var="totalHours" value="${tripDetails.totalHours}"/>
                                    <c:set var="tripSubStatusId" value="${tripDetails.tripSubStatus}"/>

                                    <%
                                            String totalHours = (String)pageContext.getAttribute("totalHours");    
                                            String tripSubStatusId = (String)pageContext.getAttribute("tripSubStatusId");    
                                            String colorStatus = "normal";
                                            int hours = 0;
                                            int mins = 0;
                                            String[] temp = totalHours.split(":");
                                            hours = Integer.parseInt(temp[0]);
                                            mins = Integer.parseInt(temp[1]);
                                            
                                            String colorName = "White";
                                            
                                            
                                            //start of export classification
                                            if("10".equals(tripSubStatusId)){ // trip in start status
                                                if(hours > 2 || (hours == 2 && mins > 0)){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("33".equals(tripSubStatusId)){ // trip in empty in 
                                                if(hours > 2 || (hours == 2 && mins > 0)){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("34".equals(tripSubStatusId)){ // trip in empty out, moving towards factory 
                                                if(
                                                    (hours > 8 || (hours == 8 && mins > 0))
                                                        &&
                                                    (hours <12 || (hours == 12 && mins == 0))    
                                                        ){
                                                    colorName = "#f8c471";
                                                }else if(
                                                    (hours > 12 || (hours == 12 && mins > 0))
                                                        &&
                                                    (hours <15 || (hours == 15 && mins == 0))    
                                                        ){
                                                    colorName = "#f5b041";
                                                }else if(hours > 15 || (hours == 15 && mins > 0 )){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("35".equals(tripSubStatusId)){ // trip in factory in 
                                                if(hours > 5 || (hours == 5 && mins > 0)){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("36".equals(tripSubStatusId)){ // trip in factory out, moving towards clearance 
                                                if(
                                                    (hours > 8 || (hours == 8 && mins > 0))
                                                        &&
                                                    (hours <12 || (hours == 12 && mins == 0))    
                                                        ){
                                                    colorName = "#f8c471";
                                                }else if(
                                                    (hours > 12 || (hours == 12 && mins > 0))
                                                        &&
                                                    (hours <15 || (hours == 15 && mins == 0))    
                                                        ){
                                                    colorName = "#f5b041";
                                                }else if(hours > 15 || (hours == 15 && mins > 0 )){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("37".equals(tripSubStatusId)){ // trip in clearance in 
                                                if(hours > 3 || (hours == 3 && mins > 0)){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("38".equals(tripSubStatusId)){ // trip in clearance out 
                                                if(hours > 1 || (hours == 1 && mins > 0)){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("39".equals(tripSubStatusId)){ // trip in port in
                                                if(hours > 1 ||  mins > 30){
                                                    colorName = "#fd857d";
                                                }
                                            }  
                                            //end of export classification
                                            else if("41".equals(tripSubStatusId)){ // import trip in port in 
                                                if(hours > 2 || (hours == 2 && mins > 0)){
                                                    colorName = "#fd857d";
                                                }
                                            } else if("42".equals(tripSubStatusId)){ // import trip in port out, moving towards factory 
                                                if(
                                                    (hours > 8 || (hours == 8 && mins > 0))
                                                        &&
                                                    (hours <12 || (hours == 12 && mins == 0))    
                                                        ){
                                                    colorName = "#f8c471";
                                                }else if(
                                                    (hours > 12 || (hours == 12 && mins > 0))
                                                        &&
                                                    (hours <15 || (hours == 15 && mins == 0))    
                                                        ){
                                                    colorName = "#f5b041";
                                                }else if(hours > 15 || (hours == 15 && mins > 0 )){
                                                    colorName = "#fd857d";
                                                }
                                            } else if("43".equals(tripSubStatusId)){ // import trip in factory in 
                                                if(hours > 5 || (hours == 5 && mins > 0)){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("44".equals(tripSubStatusId)){ // import trip in factory out, moving towards empty plot 
                                                if(
                                                    (hours > 8 || (hours == 8 && mins > 0))
                                                        &&
                                                    (hours <12 || (hours == 12 && mins == 0))    
                                                        ){
                                                    colorName = "#f8c471";
                                                }else if(
                                                    (hours > 12 || (hours == 12 && mins > 0))
                                                        &&
                                                    (hours <15 || (hours == 15 && mins == 0))    
                                                        ){
                                                    colorName = "#f5b041";
                                                }else if(hours > 15 || (hours == 15 && mins > 0 )){
                                                    colorName = "#fd857d";
                                                }
                                            }else if("45".equals(tripSubStatusId)){ // import trip in empty in 
                                                if(hours > 1 || (hours == 1 && mins > 0)){
                                                    colorName = "#fd857d";
                                                }
                                            }                               
                                            
                                            
                                            if("#fd857d".equalsIgnoreCase(colorName)){
                                                colorStatus = "critical";
                                            }else if("#f8c471".equalsIgnoreCase(colorName)){
                                                colorStatus = "minor";
                                            }else if("#f5b041".equalsIgnoreCase(colorName)){
                                                colorStatus = "major";
                                            }
                                            String className = "";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                                
                                    %>
                                    <tr >
                                        <td class="form-control" >
                                            <%=index%>
                                        </td>
                                        <c:if test="${tripType == 1}">
                                            <td class="<%=className%>" align="center">
                                                <c:if test="${tripDetails.statusId == 10 || tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                                    <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                                        <c:if test="${tripDetails.podCount == '0' && tripDetails.customerOrderReferenceNo != 'Empty Trip'}">
                                                            <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                                            </c:if>
                                                            <c:if test="${tripDetails.podCount != '0' && tripDetails.customerOrderReferenceNo != 'Empty Trip'}">
                                                            <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                                            </c:if>
                                                        </c:if>
                                                    </c:if>
                                            </td>
                                        </c:if>
                                        <td class="form-control" >
                                            <c:out value="${tripDetails.customerOrderReferenceNo}"/>
                                        </td>
                                        <td class="form-control" >
                                            <c:out value="${tripDetails.customerName}"/>
                                        </td>
                                        <c:if test = "${pnrUser == 1}" >                                            
                                            <td class="form-control" >
                                                <c:out value="${tripDetails.linerId}"/>
                                            </td>
                                        </c:if>

                                        <c:if test="${tripDetails.vendorId !=1 || vendorType == 1}">
                                            <td class="form-control" >
                                              <c:out value="${tripDetails.regNoHire}"/>  <font color="red"> <c:out value="${tripDetails.regNoHireOld}"/> </font>
                                            </td>
                                        </c:if>
                                        <c:if test="${tripDetails.vendorId == 1}">
                                        <td class="form-control" >
                                            <a href="#" onclick="viewVehicleDetails(<c:out value="${tripDetails.vehicleId}"/>)"><c:out value="${tripDetails.vehicleNo}"/></a><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font>
                                        </td>
                                        </c:if>
                                        
                                        <c:if test = "${pnrUser == 1}" >                                            
                                            <c:if test="${statusId >= 12}">
                                                <td class="form-control" >
                                                    <c:if test="${tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                                        <c:out value="${tripDetails.containerNo}"/>
                                                    </c:if>
                                                </td>
                                            </c:if>
                                        </c:if>

                                        <td class="form-control"  >
                                            <c:if test="${tripDetails.oldVehicleNo != null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                            <c:if test="${tripDetails.oldVehicleNo == null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 0);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                        </td>
                                        <c:if test = "${pnrUser != 1}" >                                            
                                            <td class="form-control" >
                                                <c:out value="${tripDetails.containerNo}"/>
                                            </td>
                                        </c:if>
                                        <c:if test = "${pnrUser != 1}" >  
                                            <td align="center" class="form-control" >
                                                <c:out value="${tripDetails.sealNo}"/>
                                            </td>
                                        </c:if>

                                        <c:if test = "${pnrUser != 1}" > 
                                            <c:if test="${tripDetails.movementType == 1  || tripDetails.movementType == 2 
                                                          || tripDetails.movementType == 17  || tripDetails.movementType == 18 
                                                          || tripDetails.movementType == 19  || tripDetails.movementType == 20  
                                                          || tripDetails.movementType == 21  || tripDetails.movementType == 23  
                                                          || tripDetails.movementType == 24 || tripDetails.movementType == 25 
                                                          || tripDetails.movementType == 26 || tripDetails.movementType == 27 
                                                          || tripDetails.movementType == 28 || tripDetails.movementType == 29 
                                                          || tripDetails.movementType == 30 || tripDetails.movementType == 31
                                                          || tripDetails.movementType == 32 || tripDetails.movementType == 33 
                                                          || tripDetails.movementType == 34 || tripDetails.movementType == 35 || tripDetails.movementType == 36
                                                  }">
                                                <td class="form-control" >
                                                    <c:if test="${tripDetails.movementType == 1  }">
                                                        <c:out value="${tripDetails.estimatedTonnage}"/>
                                                    </c:if>   
                                                    <c:if test="${tripDetails.movementType != 1  }">
                                                        <c:out value="${tripDetails.containerDetail}"/>
                                                    </c:if>   
                                                </td> 
                                            </c:if>
                                            <c:if test="${tripDetails.movementType == 14 || tripDetails.movementType == 16 }">
                                                <td class="form-control" >
                                                    <c:out value="${tripDetails.consignmentArticles}"/>
                                                </td> 
                                            </c:if>
                                        </c:if>
                                        <td align="center" class="form-control" >
                                            <c:out value="${tripDetails.description}"/>
                                        </td>
                                        <c:if test = "${pnrUser != 1}" >                                            
                                            <td class="form-control" >
                                                <c:out value="${tripDetails.tripScheduleDate}"/>
                                            </td>
                                        </c:if>
                                        <td class="form-control" ><c:out value="${tripDetails.vehicleVendor}"/></td>
                                        <td class="form-control" >
                                            <c:if test = "${pnrUser == 1}" >
                                                <c:out value="${tripDetails.portName}"/>
                                            </c:if>
                                            <c:if test = "${pnrUser != 1}" >
                                                <c:out value="${tripDetails.routeInfo}"/>
                                            </c:if>
                                        </td>

                                        <td class="form-control" ><c:out value="${tripDetails.driverName}"/><br/><c:out value="${tripDetails.mobileNo}"/><br><font color="red"><c:out value="${tripDetails.oldDriverName}"/></font></td>
                                        <td class="form-control" ><c:out value="${tripDetails.vehicleTypeName}"/></td>
                                        <c:if test = "${pnrUser == 1}" >
                                            <td class="form-control" >
                                                <input type="hidden" name="containerQuantity" id="containerQuantity" value="<c:out value="${tripDetails.containerQuantity}" />">
                                                <c:if test = "${tripDetails.containerQuantity == 1}" >
                                                    1 * 40 
                                                </c:if> 
                                                <c:if test = "${tripDetails.containerQuantity == 2}" >
                                                    1 * 20 
                                                </c:if> 
                                                <c:if test = "${tripDetails.containerQuantity == 3}" >
                                                    2 * 20 
                                                </c:if> 
                                            </td>
                                        </c:if>

                                        <td class="<%=className%>"  align="left"> 
                                            <c:if test="${tripDetails.statusId >= 8 && tripDetails.statusId <= 10 }"> <c:out value="${tripDetails.subStatusId}" /> </c:if>
                                            <c:if test="${tripDetails.statusId > 13}"> <c:out value="${tripDetails.status}" /> </c:if>

                                            </td>  
                                            <td bgcolor='<%=colorName%>' class="<%=className%>"  align="left"> 

                                            <c:out value="${tripDetails.totalHours}" />/<%=colorStatus%> 

                                        </td>  
                                        <c:if test="${statusId == 10}">
                                            <td class="<%=className%>"  align="left">
                                                <c:if test="${tripDetails.gpsLocation == 'NA'}">
                                                    NA
                                                </c:if>
                                                <c:if test="${tripDetails.gpsLocation != 'NA'}">
                                                    <a href="#" onClick="viewCurrentLocation('<c:out value="${tripDetails.vehicleId}" />');"><c:out value="${tripDetails.gpsLocation}" /></a>
                                                </c:if>
                                            </td>
                                        </c:if>
                                        <td class="form-control" >
                                            <c:if test="${tripDetails.statusId < 13}">
                                                <c:if test="${roleId != '1030'}">
                                                    <c:if test="${RoleId == 1023 || RoleId == 1069 || RoleId == 1068 && tripDetails.statusId != 10}">
                                                        <c:if test="${tripDetails.statusId != 8}">
                                                            <c:if test="${subStatusId == 0}">
<!--                                                                <a href="#" onclick="changeVehicleAfterTripStart('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);" style="color:#272E39;"   >
                                                                    ChangeVehicle
                                                                </a>-->
                                                            </c:if>
                                                        </c:if>
                                                    </c:if>

                                                    <c:if test="${tripDetails.emptyTripApprovalStatus == 0}">
                                                        Empty Trip Waiting For Approval
                                                    </c:if>

                                                    <c:if test="${tripDetails.emptyTripApprovalStatus == 1}">


                                                        <c:if test="${tripDetails.statusId == 6}">
                                                            <a href="viewTripSheetForVehicleAllotment.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentOrderNos=<c:out value="${tripDetails.consignmentId}"/>&customerName=<c:out value="${tripDetails.customerName}"/>">AllotVehicle</a>


                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 7}">
                                                            <c:if test="${RoleId != 1033}">
                                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Freeze</a>
                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 8 }">
                                                            <c:if test="${RoleId != 1033}">
                                                                <c:if test="${tripDetails.nextTrip != 2}">
    <!--                                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">UnFreeze</a>-->
                                                                    <c:if test="${tripDetails.movementType == 7 && tripDetails.statusId != 8 }">
<!--                                                                        <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                            UnFreeze
                                                                        </a>-->
                                                                    </c:if>
                                                                    <c:if test="${tripDetails.movementType != 7 && tripDetails.statusId == 8 }">
<!--                                                                        <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                            UnFreeze
                                                                        </a>-->
                                                                    </c:if>
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.nextTrip == 0 && tripDetails.nextTripCountStatus == 0}">
                                                                <c:if test="${tripDetails.tripCountStatus == 0}">
    <!--                                                                1&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>-->
                                                                    <%--<c:if test="${tripDetails.movementType == 7 && tripDetails.statusId != 8 }">--%>
                                                                    <c:if test="${tripDetails.movementType != 7 }">
<!--                                                                        <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                            <font color="gray">  StartTrip </font>
                                                                        </a><br>-->
                                                                    </c:if>
                                                                    <c:if test="${tripDetails.movementType == 7 }">
                                                                        <!--<a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>-->
                                                                    </c:if>    
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.nextTrip != 0 }">
                                                                <c:if test="${tripDetails.tripCountStatus == 0}">
                                                                    2&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.tripCountStatus == 1 && tripDetails.vehicleId == 0}">
                                                                3&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>
                                                            </c:if>
                                                            <c:if test="${tripDetails.tripCountStatus == 1 && tripDetails.vehicleId != 0}">
                                                                &nbsp;Started
                                                                <c:if test="${tripDetails.nextTrip == 0 && tripDetails.nextTripCountStatus == 0}">
                                                                    &nbsp;<br> <a href="confirmNextTrip.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Next Trip</a>
                                                                </c:if>
                                                                <c:if test="${tripDetails.nextTrip == 1 && tripDetails.nextTripCountStatus != 0}">
                                                                    &nbsp;<br> 
    <!--                                                                <a href="manualfinanceadvice.do?nextTrip=1&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>-->


<!--                                                                <a href="manualfinanceadvice.do?nextTrip=1&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:#272E39;"   >
                                                                   Request Advance
                                                                 </a>-->
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${RoleId == 1023 || RoleId == 1033}">

                                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                                    </c:if>
                                                                </c:if>
                                                            </c:if>

                                                        </c:if>
                                                        <c:if test="${userId == '1789' || userId == '1790' || userId == '1796' ||  userId == '1841' }">
                                                            <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                UnFreeze
                                                            </a>
                                                        </c:if>                
                                                        <a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>" >

                                                            <c:if test="${tripDetails.movementType == 1 }">
                                                                <c:if test="${tripDetails.tripSubStatus == 40 || tripDetails.tripSubStatus == 48}">
                                                                    <!--StatusView-->
                                                                </c:if>
                                                                <c:if test="${tripDetails.tripSubStatus != 40 && tripDetails.tripSubStatus != 48}">
                                                                    StatusUpdate
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.movementType == 2 }">
                                                                <c:if test="${tripDetails.tripSubStatus == 46}">
                                                                    StatusView
                                                                </c:if>
                                                                <c:if test="${tripDetails.tripSubStatus != 46 }">
                                                                    StatusUpdate
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.movementType != 1 && tripDetails.movementType != 2}">
                                                                StatusUpdate
                                                            </c:if>
                                                            <c:if test="${tripDetails.vehicleCount == 2 }">
                                                                StatusUpdate
                                                            </c:if>
                                                        </a>    
                                                        <c:if test="${vendorType == 0}">
                                                        <a href="#" style="color:green"  onclick="viewTripStartPrint('<c:out value="${tripDetails.tripSheetId}"/>','<c:out value="${tripDetails.vehicleId}" />')" >
                                                            <br>TripSheetPrint 
                                                        </a>
                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 10}">

                                                            <c:if test="${tripDetails.emptyTrip == 0}">
                                                                <c:if test="${subStatusId == 0}">
                                                                    <!--<a href="viewWFUTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">WFU</a>-->
                                                                </c:if>
                                                            </c:if>

                                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                                &nbsp;
                                                            </c:if>

                                                            <%--<c:if test="${tripDetails.routeNameStatus > '2'}">--%>
                                                                <!--<a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">InterimUpdate</a>-->
                                                            <%--</c:if>--%>

                                                            <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                                <c:if test="${subStatusId == 0}">
                                                                    &nbsp;
                                                                    <!--<a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>-->
                                                                </c:if>
                                                            </c:if>

                                                            <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">
                                                                <c:if test="${subStatusId == 0}">
                                                                    &nbsp;
                                                                    <!--<a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>-->
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.lastPointId == 617}">
                                                               <%-- <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                                    &nbsp;
    <!--                                                                <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&movementType=<c:out value="${tripDetails.movementType}" />">closure3</a>-->
                                                                    <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&movementType=<c:out value="${tripDetails.movementType}" />" style="color:#272E39;"   >
                                                                        Trip Closure
                                                                    </a>

                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.lastPointId != 617}">
                                                                <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                                    <c:if test="${tripDetails.tripSubStatus == 40 || tripDetails.tripSubStatus == 46 || tripDetails.tripSubStatus == 48}"> 
                                                                        <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a>
                                                                    </c:if>
                                                                </c:if>--%>
                                                            </c:if>
                                                            &nbsp;

                                                            <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1041 || RoleId == 1032}">

                                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                        <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                                    </c:if>
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                                        <!--<b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance4</a>-->
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1002}">
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                        <!--&nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance2</a>-->
                                                                    </c:if>
                                                                    <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                                        <!--<b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance3</a>-->
                                                                    </c:if>
                                                                </c:if>

                                                                <c:if test="${tripDetails.tripType != 2}">
    <!--                                                                <a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance1</a>-->
    <!--                                                                <a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:#272E39;"   >
                                                                       Request Advance
                                                                     </a>-->
                                                                </c:if>


                                                            </c:if>
                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId == 18}">

                                                            &nbsp;
                                                            <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a> <br>                                                           

                                                            &nbsp;
                                                            <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1032}">
                                                                <!--<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>-->
                                                            </c:if>
                                                        </c:if>

                                                        <c:if test="${tripDetails.statusId == 12 }">
                                                            <%--<c:if test="${RoleId != 1033}">
                                                                <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />"> Trip Closure</a>
                                                                &nbsp;
                                                                <c:if test="${tripDetails.extraExpenseStatus == 1}">
                                                                    <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                                                </c:if>

                                                            </c:if>
                                                            <c:if test="${RoleId == 1033 && tripDetails.extraExpenseStatus == 0}">
                                                                <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />">Trip closure2</a>
                                                                &nbsp;
                                                            </c:if>--%>

                                                            <c:if test="${RoleId == 1033 && tripDetails.extraExpenseStatus == 1}">
                                                                <img title="Extra Expense" alt="Extra Expense" src="images/thumbDone.jpg" style="width: 30px;height: 30px" >
                                                            </c:if>
                                                        </c:if>

                                                        <%--<c:if test="${tripDetails.statusId == 13 }">
                                                            <a href="viewTripSettlement.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Settlement</a>
                                                        </c:if>--%>
                                                    </c:if>

                                                </c:if>
                                            </c:if>
                                           <%-- <c:if test="${tripDetails.statusId == 13 }">
                                                <a href="viewTripSettlement.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Settlement</a>
                                            </c:if>--%>
<!--                                            <a href="handleTripFuel.do?nextTrip=0&tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:green;"  >
                                                FuelUpdate 
                                            </a>-->

                                           <%-- <a href="#" style="color:#283747;"  onclick="printTripGrnNewFcl('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                LRPrint
                                            </a> --%>
                                            <a href="#" onclick="changeVehicleAfterTripStart('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);" style="color:#0b5345;"   >
                                                ChangeVehicle
                                            </a>    
                                             
                                            <a href="#" style="color:red"  onclick="routeDeviation('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                Route Deviation
                                            </a>     
                                         
                                                
                                            <c:if test="${RoleId == '1023' }">
                                                <c:if test="${tripDetails.statusId >= 8 && tripDetails.statusId <= 14}">
                                                    <br>

                                                </c:if>
                                            </c:if>
                                            <c:if test="${tripDetails.statusId == 18}">


                                                <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a> <br>                                                           


                                                <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1032}">
                                                    <!--<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>-->
                                                </c:if>
                                            </c:if>


                                            <c:if test="${tripDetails.statusId >= 13}">
                                                <div id="porints" style="display:none">
                                                </c:if>       
                                                <c:if test="${subStatusId == 0}">
                                                    <c:if test="${ tripDetails.statusId != 12 && tripDetails.statusId != 13 &&  tripDetails.statusId != 14 }">

<!--                                                        <a href="#" style="color:#3399ff;" onclick="printTripFuel('<c:out value="${tripDetails.tripSheetId}"/>')"   >
                                                            FuelPrint
                                                        </a>-->
                                                    </c:if>
<!--                                                    <a href="#" style="color:green;" onclick="printTripAdvance('<c:out value="${tripDetails.tripSheetId}"/>')"  >
                                                        AdvancePrint
                                                    </a>-->
                                                </div>
                                            </td>

                                        </c:if>

                                    </tr>

                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>

                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="loader" style="display:none"></div>

                    <div id="controls" style="display: none" >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100" selected="selected" >100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>

                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $(".clsr").click(function () {

                                document.getElementById("spinner").style.display = "block";
                                document.getElementById("loader").style.display = "block";
                                document.getElementById('loader').className += 'ui-loader-background';
                            });
                        });
                        
                        function routeDeviation(tripSheetId) {
                            window.open('/throttle/updateRouteDeviation.do?tripId=' + tripSheetId, 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        
                           function viewTripStartPrint(tripSheetId,vehicleId) {
                                window.open('/throttle/viewTripStartPrint.do?tripSheetId='+tripSheetId + '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                            }
                        
                        function GenTripGr(tripSheetId) {
                            $.ajax({
                                url: "/throttle/handleTripGRGeneration.do",
                                dataType: "text",
                                data: {
                                    tripId: tripSheetId

                                },
                                success: function (temp) {
                                    // alert(data);
                                    if (temp != '') {
                                        alert("GR Generated Successfully!!");

                                    } else {
                                        alert('Something Went wrong! Please check.');
                                    }
                                }
                            });

                        }
                        function printTripGrn(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripGrnNew(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1', '&moffusilPrint=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripGrnNewLcl(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1' + '&moffusilPrint=1' + '&multiplePrint=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripGrnNewFcl(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1' + '&moffusilPrint=1' + '&lclMultiPrint=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripChallan(tripSheetId) {
                            window.open('/throttle/handleTripChallanPrint.do?tripSheetId=' + tripSheetId, 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                    </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

