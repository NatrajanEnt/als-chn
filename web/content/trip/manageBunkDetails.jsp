
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $( ".datepicker" ).datepicker({
                    changeMonth: true,changeYear: true
                });
            });
        </script>
    </head>
    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#CC3333;
        }
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#F2F2F2;
        }
    </style>

    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>

    <script type="text/javascript">
        function submitPage(obj) {            
            var bunkName =document.viewbunkDetails.bunkName.value;
            if(obj.name == "search"){
                //if(!isEmpty(bunkName)){
                    document.viewbunkDetails.action="/throttle/viewTripBunkDetailsV.do";
                    document.viewbunkDetails.submit();
                /*}else {
                    alert("Please Enter Bunk Name");
                    document.viewbunkDetails.bunkName.focus();
                }*/            }
            if(obj.name == "add"){
                document.viewbunkDetails.action="/throttle/handleTripCreateBunkDetails.do";
                document.viewbunkDetails.submit();
            }
        }

        //function getBunkName(){
        //  var oTextbox = new AutoSuggestControl(document.getElementById("bunkName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));

        //}
        // window.onload = getVehicleNos;

        //function getVehicleNos(){
        //  var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
        // }
        function displayCollapse(){
            if(document.getElementById("exp_table").style.display=="block"){
                document.getElementById("exp_table").style.display="none";
                document.getElementById("openClose").innerHTML="Open";                }
            else{
                document.getElementById("exp_table").style.display="block";
                document.getElementById("openClose").innerHTML="Close";
            }
        }
    </script>
    <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">Bunk Master</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body>
        <form name="viewbunkDetails" method="post">
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>
            <td><input type="hidden" name="settlementId" id="settlementId" value=""/></td>

           <table class="table table-info mb30 table-hover" style="width:100%">
                        <tr height="30"   ><td colSpan="3" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Bunk Master - Search</td></tr>
                
                                    <tr>
                                        <td><font color="red">*</font>Bunk Name</td>
                                        <td height="30">
                                            <input name="bunkName" id="bunkName" type="text" class="form-control" style="width:240px;height:40px;" size="20" value="" onKeyPress="getBunkName();" autocomplete="off">
                                        </td>
                                        <td><input type="button" class="btn btn-info" name="search" onclick="submitPage(this);" value="Search"> <input type="button" class="btn btn-info" name="add" onclick="submitPage(this);" value="Add"></td>
                                    </tr>
                                </table>
            <c:if test="${bunkList != null}">
                    <%int flag = 0;
                                int index = 0;
                    %>
                    <table class="table table-info mb30 table-hover" id="table" style="width:100%">
                        <thead height="30">
                            <tr id="tableDesingTH" height="30">
                        <th> S.No </th>
                        <th> Bunk Name </th>
                        <th> Fuel Type </th>
                        <th> Current Rate </th>
                        <th> Location </th>
                        <th> State </th>
                        <th> Status </th>
                        <th> Alter </th>
                    </tr>
                     </thead>
                    <c:forEach items="${bunkList}" var="bd">
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "text2";
                            } else {
                                classText = "text1";
                            }
                        %>
                        <tr>
                            <td height="30"><%=index + 1%></td>
                            <td height="30"><c:out value="${bd.bunkName}"/></td>
                            <td height="30"><c:out value="${bd.fuelType}"/></td>
                            <td height="30"><c:out value="${bd.currRate}"/></td>
                            <td height="30"><c:out value="${bd.currlocation}"/></td>
                            <td height="30"><c:out value="${bd.bunkState}"/></td>
                            <c:if test="${bd.bunkStatus == 'Y'}">
                                <td    height="30"><c:out value="Active"/></td>
                                <%flag = 2;%>
                            </c:if>
                            <c:if test="${bd.bunkStatus == 'N'}">
                                <td    height="30"><c:out value="In Active"/></td>
                                <%flag = 1;%>
                            </c:if>
                            <td  height="30"><a href="/throttle/alterbunkDetails.do?bunkId=<c:out value="${bd.bunkId}"/>"> Edit</a></td>


                        </tr>
                        <%
                                    index++;
                        %>
                    </c:forEach>
                    <tr>
                    </tr>
                </table>
            </c:if>
        </form>
             <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="loader"></div>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"  selected="selected" >5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
    </body>
  </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>