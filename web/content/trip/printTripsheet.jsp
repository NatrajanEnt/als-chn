<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : RAM
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <title>PrintTripSheet</title>
    </head>
    <body>
        <script>

//            window.onunload = function() {
//                window.opener.location.reload();
//            }
        </script>
        <form name="enter" method="post">
            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            String billingPartyId = (String)request.getAttribute("billingPartyId");
            %>
            <div id="printContent">
                <table align="center"  style="border-collapse: collapse;width:400px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                       background:url(images/dict-logoWatermark.png);
                       background-repeat:no-repeat;
                       background-position:center;
                       border:1px solid;
                       /*border-color:#CD853F;width:800px;*/
                       border:1px solid;
                       /*opacity:0.8;*/
                       /*font-weight:bold;*/
                       ">
                    <tr>
                        <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" >
                            <table align="left" >
                                <tr>
                                    <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                    <td align="left"><img src="images/Triway_CFS_Logo.jpg" width="170" height="60"/></td>
                                        <%}else {%>
                                    <td align="left"><img src="images/Route Logo.JPG" width="170" height="60"/></td>
                                        <%}%>

                                    <td style="padding-left: 180px; padding-right:42px;">
                                        &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;




                                <center>
                                    <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                    <font face="verdana" size="4"><b>Triway CFS Pvt Ltd.</b></font></center><br>
                                    <%}else {%>
                                <font face="verdana" size="4"><b>Route Logistics India Pvt.Ltd.</b></font></center><br>
                                <%}%>

                                <center>
                                    <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                    <font face="verdana" size="3" >148, Ponneri High Road, Edayanchavadi, New Nappalayam, Chennai,Tamilnadu.
                                    </font>
                                    <%}else if ("1464".equals(companyId)) {%>
                                    <font face="verdana" size="3" >14,Jaffer Street, Chennai,Tamilnadu.
                                    </font>
                                    <%} else{%>
                                        <font face="verdana" size="3" >Industrial Subrub, Yeshwanthpur, Bangalore 560022  </font>
                                    <% } %>
                                </center>
                        </td>
                        <td align="right">
                            <br>
                            <div id="externalbox" style="width:3in">
                                <div id="inputdata" ></div>
                            </div>
                        </td>
                    </tr>
                </table>
                </td>
                </tr>

                <tr>
                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <tr>
                                <td  style="border-right:solid #888 1px;">
                                    <font face="verdana" size="2">&nbsp;&nbsp;<b>Consignor's Name & Address:</b></font><br> <font face="verdana" size="2"> &nbsp;&nbsp;<c:out value="${consignorName}"/>,<br>&nbsp;&nbsp;<c:out value='${consignorAddress}'/>.</font>  <br> <c:out value='${consigneeGST}'/><br>
                                    <br>
                                    <font face="verdana" size="2">&nbsp;&nbsp;<b>Consignee's Name & Address:</b></font><br><font face="verdana" size="2">&nbsp;&nbsp;<c:out value="${consigneeName}"/>,<br>&nbsp;&nbsp;<c:out value="${consigneeAddress}"/>.</font><br> <c:out value="${consignorGST}"/>
                                    <br>
                                    <font face="verdana" size="2">&nbsp;&nbsp;<b>Billing Party & Address:</b> </font><br><font face="verdana" size="2">&nbsp;&nbsp;<c:out value="${billingParty}"/>,<br>&nbsp;&nbsp;<c:out value="${billingAddress}"/>.</font> <br> <c:out value="${billingPartyGST}"/>

                                </td>
                                <td >


                                    <font face="verdana" size="2"><b>Movement Type:</b></font><font face="verdana" size="2">&nbsp;&nbsp;<c:out value='${orderType}'/></font>
                                    <br>

                                    <font face="verdana" size="2">
                                    <b>Trip.NO:</b></font><font face="verdana" size="2">&nbsp;&nbsp;<c:out value="${tCode}"/></font>
                                    <br>

                                    <font face="verdana" size="2">
                                    <b>Trip Created On:</b></font><font size="2">&nbsp;&nbsp;<c:out value="${grDate}"/></font>
                                    <br>

                                    <font face="verdana" size="2">
                                    <b>Booking No:</b></font><font size="2">&nbsp;&nbsp;<c:out value="${cNotes}"/></font>
                                    <br>
                                    
                                    <font size="2">
                                    <b>Job No:</b></font><font face="verdana" size="2">&nbsp;&nbsp;<c:out value="${customerOrderReferenceNo}"/></font>
                                    <br>

                                    <font face="verdana" size="2">
                                    <b>Transporter Name:</b></font><font face="verdana" size="2">&nbsp;&nbsp;<c:out value="${vendor}"/></font>
                                    <br>

                                    <font face="verdana" size="2">
                                    <b>Vehicle No:</b></font><font face="verdana" size="2">&nbsp;&nbsp;<c:out value="${vehicleNo}"/></font>
                                    
                                    <br>
                                    <font face="verdana" size="2">
                                    <b>E-Way Bill No:</b></font><font size="2">&nbsp;&nbsp;<c:out value="${eWayBillNo}"/></font>
                                </td>
                            </tr>

                        </table>
                    </td>
                </tr>
                <tr height='100'>
                    <td style="border: solid #888 1px;">

                        <table width='100%' height='100'>
                            <tr>
                                <td>
                                    <table  width='100%' height='100'>
                                        <tr>
                                            <td>
                                                <table  style="border-collapse: collapse;" width='100%' height='100'>
                                                    <thead height="100">
                                                        <tr style="height:50px;white-space: pre; " >
                                                            <th style="border: solid #888 1px;"><h1><font face="verdana" size="2">No of PKG</font></h1></th>
                                                    <th style="border: solid #888 1px;"><h1><font face="verdana" size="2">Description Of Goods<br></font><font size='1'>(Said To Contain)</font></h1></th>
                                                    <th style="border: solid #888 1px;"><h1><font face="verdana" size="2">Container No</font></h1></th>
                                                    <th style="border: solid #888 1px;"><h1><font face="verdana" size="2">Vessel Name</font></h1></th>
                                                    <th style="border: solid #888 1px;"><h1><font face="verdana" size="2">&nbsp;Size&nbsp;</font></h1></th>
                                                    <th style="border: solid #888 1px; "><h1><font face="verdana" size="2">&nbsp;&nbsp;Seal No&nbsp;&nbsp;</font> </h1></th>
                                                    <th style="border: solid #888 1px; "><h1><font face="verdana" size="2">Actual Weight</font></h1></th>
                                                    <!--<th style="border: solid #888 1px;width:20px "><h1><font size="2">Charged Weight</font></h1></th>-->
                                                    <th style="border: solid #888 1px; "><h1><font face="verdana" size="2">Rate</font></h1></th>
                                                    <th style="border: solid #888 1px;"><h1><font face="verdana"  size="2">Amount To Pay/Paid Rs.</font></h1></th>
                                        </tr>
                                        </thead>
                                        <tbody height="100">

                                            <tr style="height:50px;padding:0px;margin:0px; " >
                                                <td rowspan="2" style="border: solid #888 1px; ">   </td>
                                                <td rowspan="2" style="border: solid #888 1px;"> <c:out value='${articleName}'/>  </td>
                                                <td style="border: solid #888 1px;">

                                                    <c:out value='${containerNo}'/>
                                                    <br>  <c:out value='${containerNo1}'/>
                                                </td>
                                                <td style="border: solid #888 1px;  ">  <c:out value='${linerName}'/>  <br>  <c:out value='${linerName1}'/></td>
                                                <td style="border: solid #888 1px;  "> <c:out value='${containerTypeId}'/>   <br>  <c:out value='${containerTypeId1}'/></td>
                                                <td style="border: solid #888 1px;  "><c:out value='${sealNo}'/>  <br>  <c:out value='${sealNo1}'/> </td>
                                                <td style="border: solid #888 1px;"> </td>
                                                <td style="border: solid #888 1px;"> </td>
                                                <td  style="border: solid #888 1px;"><c:out value='${grStatus}'/> </td>
                                            </tr>
                                            <tr style="height:20px;padding:0px;margin:0px; " >
                                                <td colspan="8" style="border: solid #888 1px; border-top: none; margin:10px;">
                                                    <br>
                                                    <table >
                                                        <tr ><td style="padding-bottom:10px"><b>&nbsp;&nbsp;In  Date:</b></td><td style="padding-bottom:10px"><c:out value="${endDate}"/></td><td style="padding-bottom:10px"><b>&nbsp;&nbsp;&nbsp;&nbsp;Time:</b></td><td style="padding-bottom:10px"><c:out value="${endTime}"/></td></tr>
                                                        <tr> <td style="padding-bottom:10px"><b>&nbsp;&nbsp;Out Date:</b></td><td style="padding-bottom:10px"><c:out value="${startDate}"/></td><td style="padding-bottom:10px"><b>&nbsp;&nbsp;&nbsp;&nbsp;Time:</b></td><td style="padding-bottom:10px"><c:out value="${startTime}"/></td></tr>
                                                    </table>
                                                </td>
                                            </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>

                <tr height="100">
                    <td >

                        <table width="100%" height="100"  cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <td  style=" border: solid #888 1px;">
                                <table width='100%' ><tr>
                                        <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                    <font face="verdana" size='2' ><b>&nbsp;&nbsp;&nbsp;Booking Office:</b></font></tr>
                                    <td ><font face="verdana" size='2' >
<!--                                        &nbsp;&nbsp;&nbsp;Triway CFS India Pvt.Ltd<br>
                                        &nbsp;&nbsp;&nbsp;148, Ponneri High Road,<br>
                                        &nbsp;&nbsp;&nbsp;Edayanchavadi, New Nappalayam, <br>
                                        &nbsp;&nbsp;&nbsp;Chennai-600001.-->
                                        &nbsp;&nbsp;&nbsp;Route Logistics India Pvt.Ltd<br>
                                        &nbsp;&nbsp;&nbsp;No-14,Jaffer Street,<br>
                                        &nbsp;&nbsp;&nbsp;Behind Custom House,<br>
                                        &nbsp;&nbsp;&nbsp;Chennai-600001.
                                        </font>
                                        <%}else {%>
                                        <font face="verdana" size='2' ><b>&nbsp;&nbsp;&nbsp;Booking Office:</b></font></tr>
                                    <td ><font face="verdana" size='2' >
                                        &nbsp;&nbsp;&nbsp;Route Logistics India Pvt.Ltd<br>
                                        &nbsp;&nbsp;&nbsp;No-14,Jaffer Street,<br>
                                        &nbsp;&nbsp;&nbsp;Behind Custom House,<br>
                                        &nbsp;&nbsp;&nbsp;Chennai-600001.
                                        </font>
                                        <%}%>


                                    </td></table></td>

                            <td style=" border: solid #888 1px;" ><table  width='100%' style="border-collapse: collapse;" height="100">
                                    <tr><td  ><font face="verdana" size="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Truck Type:</b></font><font size="2"><c:out value='${vehicleType}'/></font></td>
                                        <td  ><font face="verdana" size="2"><b>&nbsp;Route Info:</b></font><font face="verdana" size="2"><c:out value="${routeInfo}"/></font></td></tr>
                                    <tr><td  ><font face="verdana" size="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Driver Name:</b></font><font size="2"><c:out value='${driverName}'/></font></td>
                                        <td  ><font face="verdana" size="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Mob.No:</b></font><font size="2"><c:out value='${driverMobile}'/></font></td>
                                    </tr>  
                                    <tr> <td  ><font face="verdana" size="2"><b>&nbsp;&nbsp;Type Of Delivery:</b></font><font face="verdana" size="2"></font></td>
                                        <td  ><font face="verdana" size="2"><b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Terms:</b></font><font size="2"></font></td>
                                    </tr>

                                </table></td>


                        </table>
                    </td>
                </tr>
                <tr height="100">
                    <td >
                        <table height="100" width="100%"  cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse">
                            <td style="border: solid #888 1px;" width="30%">
                                <table width='100%' style="border-collapse: collapse;"> <tr>
                                    <font face="verdana" size="2" >&nbsp;&nbsp;&nbsp;<b>List Of Documents:</b></font></tr>
                                    <td>
                                        <font size="2">
                                        &nbsp;&nbsp;&nbsp; Form 13, EIR
                                        </font>
                                    </td>
                                    <!--</tr>-->
                                </table>
                            </td>
                            <td style="border: solid #888 1px;" width="40%">
                                <table width='100%' style="border-collapse: collapse;">
                                    <tr>  <td ><font face="verdana" size="2">&nbsp;&nbsp;<b>Prepared By:</b></font></td></tr>


                                    <!--</tr>-->
                                    <!--                                    </table>
                                                                        </td>-->
                                    <!--<table>-->
                                    <!--<td><font size="2">Prepared By</font></td>-->
                                    <!--</tr>-->
                                    <!--<tr>-->
                                    <td>
                                        <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                                        <font face="verdana" size="2">
                                        &nbsp;&nbsp;Triway CFS India Pvt.Ltd.<br>
                                        &nbsp;&nbsp;Chennai<br>
                                        &nbsp;&nbsp;chennai@triwayCFS.net<br>
                                        </font><font face="verdana" size="2">
                                        &nbsp;&nbsp;<b>Prepared On:</b><c:out value="${grDate}"/><br>
                                        </font>
                                        <%}else {%>
                                        <font face="verdana" size="2">
                                        &nbsp;&nbsp;Route Logistics(I) Pvt.Ltd.<br>
                                        &nbsp;&nbsp;Chennai<br>
                                        &nbsp;&nbsp;chennai@routelogistics.net<br>
                                        </font><font face="verdana" size="2">
                                        &nbsp;&nbsp;<b>Prepared On:</b><c:out value="${grDate}"/><br>
                                        </font>
                                        <%}%>

                                    </td>

                                </table>
                            </td>
                        </table>
                    </td>
                </tr>
                <tr height="80">
                    <td>
                        <table width='100%' style="border-collapse: collapse;" height="90">
                            <tr >
                                <td style="border: solid #888 1px;" width='40%'><table   style="border-collapse: collapse;" >
                                        <tr><td><font face="verdana" size="2">&nbsp;&nbsp;<b>Party Invoice No & Date:</b></font></td></tr>
                                        <tr><td><font face="verdana" size="2">&nbsp;&nbsp;<b>Party  CST/TIN No:</b></font></td></tr>
                                        <tr><td><font face="verdana" size="2">&nbsp;&nbsp;<b>Value Of Goods(INR.):As per Bill.</b> </font>
                                                <%-- <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWords" >
                                                                     <% spareTotalRound.setRoundedValue(String.valueOf(pageContext.getAttribute("totalFreight")));%>
                                                                           <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                         <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; </b>
                         </jsp:useBean></b> --%>
                                            </td></tr>
                                    </table></td>
                                <td style="border: solid #888 1px;" width='30%'><font face="verdana" sze='2' style="float: top;"  >&nbsp;&nbsp;<b>Permit No:</b></font></td>
                                <td style="border: solid #888 1px;border-left: none;" width='30%'><font face="verdana" sze='2' style="float: top;" >&nbsp;&nbsp;<b>Remarks:</b></font></td>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr height="100">
                    <td>
                        <table width='100%' style="border-collapse: collapse;" height="100">
                            <% if (ThrottleConstants.pnrBillingPartyId.equals(billingPartyId)) {%>
                            <tr>
                                <td style="border: solid #888 1px;" width='40%'>
                                    <font size="2">
                                    &nbsp;&nbsp;<b>GST No:</b> <font face="verdana" size="2">33AACCR6979E1Z6</font><br>
                                    &nbsp;&nbsp;<b>PAN NO:</b><font face="verdana" size="2">AACCR6979E </font> <br>
                                    &nbsp;&nbsp;<b>CIN NO:</b> <font face="verdana" size="2">U63090TN2001PTCO48035</font><br> </font>
                                </td>
                                <td style="border: solid #888 1px;" width='40%'>
                                    <table width='100%' style="border-collapse: collapse;">
                                        <tr>
                                        <font size="2">&nbsp;&nbsp;<b>AT OWNER'S RISK:</b></font>
                            </tr>
                            <%}else{%>
                            <tr>
                                <td style="border: solid #888 1px;" width='40%'>
                                    <font size="2">
                                    &nbsp;&nbsp;<b>GST No:</b> <font face="verdana" size="2">33AACCR6979E1Z6</font><br>
                                    &nbsp;&nbsp;<b>PAN NO:</b><font face="verdana" size="2">AACCR6979E </font> <br>
                                    &nbsp;&nbsp;<b>CIN NO:</b> <font face="verdana" size="2">U63011TN2004PTC052290</font><br> </font>
                                </td>
                                <td style="border: solid #888 1px;" width='40%'>
                                    <table width='100%' style="border-collapse: collapse;">
                                        <tr>
                                        <font size="2">&nbsp;&nbsp;<b>AT OWNER'S RISK:</b></font>
                            </tr>
                            <%}%>
                            <td >
                                <font face="verdana" size="2">
                                &nbsp;&nbsp;Goods to be insured by party at Owners Risk.<br>
                                &nbsp;&nbsp;Not Responsible for Breakage,Damages,etc.<br>
                                &nbsp;&nbsp;Subject to Chennai Jurisdiction Only.<br>
                            </td>

                        </table>
                    </td> 
                </tr>

                </table>
                </td>
                </tr> 
                </table>


                </td>
                </tr>

                <tr height="100">
                    <td style="border: solid #888 1px; border-top: none; ">
                        <table align="left" style="border-collapse: collapse" >
                            <br>
                            <br>
                            <br>
                            <br>
                            <tr><td><font face="verdana" size="2">&nbsp;&nbsp;&nbsp;&nbsp;<b>Signature of consignee or Agent with Rubber Stamp</b>

                                    </font>
                                </td>
                            </tr>
                            <tr> <td></td></tr>
                            <tr> <td></td></tr>
                            <tr><td align="left"><font face="verdana" size="2">&nbsp;&nbsp;&nbsp;&nbsp;<b>Date:</b></font>


                                </td>
                                <td align="right"><font face="verdana" size="2"><b>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Authorized Signature&nbsp;&nbsp;&nbsp;&nbsp;</b></font></td>
                            </tr>

                        </table>
                    </td>
                </tr>

                <br>

                </table>
            </div>
            <br>
            <center>
                <input type="button" class="button"  value="Back" onClick="viewTripDetails();" />
                <input type="button" class="button"  value="Print" onClick="print('printContent');" />
                &nbsp;&nbsp;&nbsp;
                    <c:if test="${tripDetailsList != null}">
                        <c:forEach items="${tripDetailsList}" var="tripStatus">
                            <c:if test="${tripStatus.tripStatusId < 8}">
                <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripId}"/>&tripType=1&statusId=8"><input type="button" value="Start Trip"></a>
                            </c:if>
                        </c:forEach>
                </c:if>
                
            </center>                 
        </form>
        <script type="text/javascript">
            function viewTripDetails() {
                document.enter.action = '/throttle/viewTripSheets.do?&statusId=10&tripType=1&admin=No';
                document.enter.submit();
            }
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            function get_object(id) {
                var object = null;
                if (document.layers) {
                    object = document.layers[id];
                } else if (document.all) {
                    object = document.all[id];
                } else if (document.getElementById) {
                    object = document.getElementById(id);
                }
                return object;
            }
            //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
            get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
            /* ]]> */
        </script>
    </body>
</html>