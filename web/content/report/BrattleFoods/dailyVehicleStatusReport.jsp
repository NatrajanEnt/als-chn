<%--
    Document   : BPCLTransactionReport
    Created on : Oct 31, 2013, 1:48:05 PM
    Author     : Arul
--%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
          <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
         <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                            altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>


        <script type="text/javascript">
            function submitPage(value){
        
//                if (document.getElementById('fromDate').value == '') {
//                    alert("Please select from Date");
//                    document.getElementById('fromDate').focus();
//                } else if (document.getElementById('toDate').value == '') {
//                    alert("Please select to Date");
//                    document.getElementById('toDate').focus();
//                }else{}
                    var vendorName= document.getElementById("vendorName").value;
                     if(value == "ExportExcel"){
                        document.BPCLTransaction.action = '/throttle/handleDailyVehicleStatusExcel.do?param=ExportExcel';
                        document.BPCLTransaction.submit();
                    }
                    else{
                        document.BPCLTransaction.action = '/throttle/handleDailyVehicleStatusExcel.do?param=Search';
                        document.BPCLTransaction.submit();
                    }
                
            }
        </script>



    </head>
    <body onload="setValues();">
        <form name="BPCLTransaction" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <!-- pointer table -->
            <!-- message table -->
            <%@ include file="/content/common/message.jsp"%>

            <table width="75%" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr>
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <b>Vehicle Status Report</b>
                            <div id="first">
                                <table width="830" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <!--                                    <tr>
                                                                            <td><font color="red">*</font>Driver Name</td>
                                                                            <td height="30">
                                                                                <input name="driName" id="driName" type="text" class="textbox" size="20" value="" onKeyPress="getDriverName();" autocomplete="off">
                                                                        </tr>-->
                                    <script type="text/javaScript">
                                        function setVendorName(){

                                        var vendor=document.getElementById("vendorId");
                                        document.getElementById("vendorName").value= vendor.options[vendor.selectedIndex].text
                                        }
                                        function setValues(){
                                            if('<%=request.getAttribute("vendorId")%>' != 'null' ){
                                            document.getElementById('vendorId').value= '<%=request.getAttribute("vendorId")%>';
                                        }
                                          if('<%=request.getAttribute("vendorName")%>' != 'null' ){
                                            document.getElementById('vendorName').value= '<%=request.getAttribute("vendorName")%>';
                                        }
                                        }
                                    </script>
                                    <input type="hidden" id="vendorName" name="vendorName" value=""/>
                                    <tr>
                                        <td><font color="red">*</font>Vendor</td>

                                         <td height="30">
                                             <select name="vendorId" id="vendorId" onchange="setVendorName();" >
                                                 <option value="">---select----</option>
                                                 <option value="0">own</option>
                                                 <c:if test="${vendorList != null}">
                                                     <c:forEach items="${vendorList}" var="ven">
                                                    
                                                       
                                                    <option value="<c:out value="${ven.vendorId}"/>"><c:out value="${ven.vendorName}"/></option>
                                                         </c:forEach>
                                                 </c:if>
                                             </select>
                                         </td>
                                      <td><font color="red">*</font>Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                     <%--   <td><font color="red">*</font>From Date</td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" value="<c:out value="${toDate}"/>"></td>  --%>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="button" class="button" name="search" onclick="submitPage(this.name);" value="Search"></td>
                                        <td><input type="button" class="button" name="ExportExcel" onclick="submitPage(this.name);" value="ExportExcel"></td>
                                        <td><center><input type="button" class="button"  value="Print" onClick="print('printContent');" ></center></td>
                                        <td></td>
                                    </tr>

                                </table>
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
               <div id="printContent">
            <c:if test="${vehicleDetails != null}">
                <table align="center" border="0" id="table" class="sortable" width="100%" >

                    <thead>
                        <tr height="50">
                            <th align="center"><h3>S.No</h3></th>
                            <th align="center"><h3>Date</h3></th>
                            <th align="center"><h3>Vehicle No </h3></th>
                            <th align="center"><h3>Route</h3></th>
                            <th align="center"><h3>KM</h3></th>
                            <th align="center"><h3>Status</h3></th>
                            <th align="center"><h3>Remarks</h3></th>
                           
                            
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 1;%>
                        
                        <c:forEach items="${vehicleDetails}" var="BPCLTD">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr>
                                <td width="30"class="<%=classText%>"><%=index++%></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.date}"/></td>
                                <td  width="30" class="<%=classText%>"><c:out value="${BPCLTD.vehicleNo}"/></td>
                                <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.routeInfo}"/></td>
                                <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.totalRunKm}"/></td>
                                <td width="30" class="<%=classText%>"><c:out value="${BPCLTD.vehicleStatus}"/></td>
                                <td width="30" class="<%=classText%>" ><c:out value="${BPCLTD.remarks}"/></td>
                               
                               
                            </tr>

                        </c:forEach>
                    </tbody>
                </table>

             
            </c:if>
                                    </div>                <br>
                                <br>
                  <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5"selected="selected" >5</option>
                    <option value="10">10</option>
                    <option value="20" >20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>

                                <script type="text/javascript">
                                     function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
                                </script>
    </body>
</html>

