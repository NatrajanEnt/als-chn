<%--
    Document   : tripPlanningExcel
    Created on : Nov 4, 2013, 10:56:05 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style> table, td {border:1px solid black} table {border-collapse:collapse}</style>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>
    </head>
    <body>

        <form name="accountReceivable" action=""  method="post">
             <%
            Date dNow = new Date();
            SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
            //System.out.println("Current Date: " + ft.format(dNow));
            String curDate = ft.format(dNow);
            String expFile = "Vehicle_Availability_"+curDate+".xls";

            String fileName = "attachment;filename=" + expFile;
            response.setContentType("application/vnd.ms-excel;charset=UTF-8");
            response.setHeader("Content-disposition", fileName);
        %>

            <br>
            <br>
            <br>
        <c:if test = "${vehicleAvailabilityList != null}" >
            <table class="table table-info mb30 table-bordered" id="table" width="100%" >
                            <thead>
                            <tr >
                                <th width="100"  style="text-align: center">S.No</th>
                                <th width="100"  style="text-align: center">Vehicle No</th>
                                <th width="100"  style="text-align: center">Status</th>
                                <th width="100"  style="text-align: center">Customer Name</th>
                                <th width="100"  style="text-align: center">Driver</th>
                                 <th width="100"  style="text-align: center">Route</th>
                                <th width="100"  style="text-align: center">Movement</th>
                                <th width="100"  style="text-align: center">CustRef No</th>
                                <th width="250"  style="text-align: center">LastKnownGPSLocation</th>
                               


                            </tr>
                            </thead>
                             <% int sno = 0;%>
                         
                            <tbody>
                                <c:forEach items="${vehicleAvailabilityList}" var="val">
                              <%
                                            sno++;
                                            String className = "text1";
                                            if ((sno % 1) == 0) {
                                                className = "text1";
                                            } else {
                                                className = "text2";
                                            }
                                %>
                                    
                            <tr>
                                <td   align="left" width="120"><%=sno%> </td>
                                <td   align="left" width="120"> <c:out value="${val.regno}"/></td>
                               
                                <td >&nbsp;
                                <c:if test="${val.activeInd == 'N'}">
                                    <%--<c:out value="${val.statusName}"/>--%>
                                      <c:out value="${val.subStatus}"/>
                                </c:if>
                               
                                <c:if test="${val.activeInd == 'Y'}">
                                    <spring:message code="operations.label.WFL"  text="default text"/>
                                </c:if>
                                </td>
                                <td  align="center">&nbsp;

                                <c:out value="${val.customerName}"/>
                                </td>
                                <td >&nbsp;<c:out value="${val.driverName}"/><br/>
                                <c:if test="${val.mobileNo != null}"><c:out value="${val.mobileNo}"/></c:if></td>
                                 <td   align="left">&nbsp; 
                                 <c:out value="${val.routeName}" /> 
                                 </td>
                                <td  align="left"><c:out value="${val.movementTypeName}" />
                                </td>
                                <td  align="left"><c:out value="${val.orderRefNo}" />
                                </td>
                                <td   align="left">&nbsp; <c:out value="${val.point3Id}" /></td>
                                
                                </tr>
                                </c:forEach>
                            </tbody>
                        </table>
        </c:if>

    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</html>
