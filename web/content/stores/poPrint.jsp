<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@page language="java" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <%@ page import="java.text.DecimalFormat" %>
        <%@ page import="java.text.NumberFormat" %>
         <%@ page import="ets.domain.purchase.business.PurchaseTO" %>
   
        <!--        <script type="text/javascript" src="/throttle/js/suest"></script>
                <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
                <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
                <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
                <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />-->

        <!--        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
                <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
                <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>-->

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>




        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css"><!--For Currency Symbol -->

        <style type="text/css" title="currentStyle">
            @import "/throttle/css/layout-styles.css";
            @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
        </style>

        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

        <!-- jQuery libs -->
        <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
        <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

        <!-- Our jQuery Script to make everything work -->
        <!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

       
<!--        <style>
        body {
            background-image: url("img_tree.png");
            background-repeat: no-repeat;
            background-position: right top;
            margin-right: 200px;
        }
        </style>-->
        
        <script type="text/javascript">

             



         
            $(document).ready(function () {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function () {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });



        </script>
        <script  type="text/javascript" src="js/jq-ac-script.js"></script>


        <script type="text/javascript" language="javascript">
           

            $(document).ready(function () {
                $("#tabs").tabs();
            });

            
             function numberWithCommas(x) {
            var parts = x.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            return parts.join(".");
                 }
        </script>
    </head>
    <body  onload="checkSpace();">
        <form name="trip" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%int sno = 1;%>
            <div id="tabs" >
                <ul>
<!--                    <li><a href="#tripDetail"><span>Billing Trip Details</span></a></li>-->
                    <li><a href="#invoiceDetail"><span>PO Details</span></a></li>
                </ul>
                <c:set var="totalRevenue" value="0" />
                <c:set var="totalExpToBeBilled" value="0" />

    
      <%--          <div id="tripDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contenthead" >Sno</td>
                            <td class="contenthead" >Customer</td>
                            <td class="contenthead" >C Note</td>
                            <td class="contenthead" >Trip Code</td>
                            <td class="contenthead" >InvoiceCode</td>
                            <td class="contenthead" >FreightAmount</td>
                            <td class="contenthead" >OtherExp</td>
                            <td class="contenthead" >TotalAmount</td>
                            <td class="contenthead" >PodStatus</td>
                        </tr>
                        <% int submissionStatus = 0 ;%>
                        <c:if test = "${tripDetails != null}" >
                            <c:forEach items="${tripDetails}" var="trip">
                                <tr>
                                    <td class="text1" ><%=sno%></td>
                                    <td class="text1" ><c:out value="${trip.customerName}"/></td>
                                    <td class="text1" ><c:out value="${trip.consignmentNoteNo}"/></td>
                                    <td class="text1" ><c:out value="${trip.tripCode}"/></td>
                                    <td class="text1" ><c:out value="${trip.invoiceCode}"/></td>
                                    <td class="text1"  align="right"><c:out value="${trip.freightAmount}"/></td>
                                    <td class="text1"  align="right"><c:out value="${trip.otherExpenseAmount}"/></td>
                                    <td class="text1"  align="right"><c:out value="${trip.totalAmount}"/></td>
                                    <td class="text1"  align="right">
                                    <c:if test="${trip.podCount == 1}">
                                        <a href="viewTripPod.do?tripSheetId=<c:out value="${trip.tripId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                    </c:if>
                                    <c:if test="${trip.podCount == 0}">
                                        <a href="viewTripPod.do?tripSheetId=<c:out value="${trip.tripId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                    </c:if>
                                    </td>
                                    <c:if test="${trip.podCount > 0}">
                                        <%submissionStatus = 1 ;%>

                                    </c:if>

                                <%sno++;%>
                            </c:forEach>
                        </c:if>
                    </table>
                    <br>
                    <center>
                        <% if(submissionStatus == 1){ %>
                        <c:if test="${submitStatus == 0}">
                        <input type="button" class="button" value="SubmitBill" id="SubmitBill" name="Next" onclick="submitOrderBill('<c:out value="${invoiceId}"/>');" />
                        </c:if>
                        <%}else{%>
                        Waiting for POD Upload, we cant submit bill.
                        <%}%>
                    </center>
                </div>  --%>
      <% 
       int index=0;
ArrayList poDetail = new ArrayList();
poDetail = (ArrayList) request.getAttribute("poDetail");
PurchaseTO purch = new PurchaseTO();
int listSize=10;
int itemNameLimit = 30;
int mfrCodeLimit = 10;

PurchaseTO headContent = new PurchaseTO();
System.out.println("poDetail size in jsp="+poDetail.size());
headContent = (PurchaseTO)poDetail.get(0);
String[] address = headContent.getAddress();
String itemName = "";
String mfrCode = "";
int PoId=headContent.getPoId(); 
String poCode = "ICTRIPL/17-18/PO/" + headContent.getPoId();
String poCode1 = "17-18/" + headContent.getPoId();
String poDate=headContent.getCreatedDate();
String vendorName=headContent.getVendorName();
String vendorAddress=headContent.getVendorAddress();
String vendorPhone=headContent.getVendorPhone();
      %>
 
                <div id="invoiceDetail" >
                    <div id="print" >
                       
                       
                       <table  align="center" cellpadding="0" cellspacing="0" border="2"  rules="all" frame="hsides" 
                                style="border-collapse: collapse;width:1000px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;height:800px;border-left: none;border-right: none;">     
                        
                           <tr>
                <td height="100" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border" colspan="3" >
                    <table align="left" >
                        <tr>
                            <td align="left"><img src="images/dict-logo11.png" width="100" height="50"/></td>
                            <td style="padding-left: 75px; padding-right:42px;">
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                        <font size="3"><b>International Cargo Terminals And Rail Infrastructure Pvt.Ltd.</b></font><br>
                        <center> <font size="2" style="padding-left: 85px" >  (Formely Known As Boxtrans Logistics (India) Services Pvt Ltd.)</font><br>
                           <font size="2" style="padding-left: 85px" >   Registered Office Address: Godrej Coliseum,Office No. 801, C-Wing,<br>
                             </font>
                            <font size="2" style="padding-left: 85px">Behind Everard Nagar, Off Somaiya Hospital Road,Sion East,Mumbai, Maharashtra -400022</font>
                        </center>
                </td>
                <td></td>
                </tr>
                </table>
                </td>
                </tr>

                            <tr style="border-color:#00008B;">
                                <td  height="30" colspan="3" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse">        <center><h3><font size="3"><b>Purchase Order</b></font></h3></center>
                                </td>

                            </tr>
                            <tr style="border-color:#00008B;">
                                <td  height="30"  style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse"> <h3><font size="3"><b>Purchase Order No:<%=poCode%></b></font></h3> </td>
                                <td  height="30"  style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse"> <h3><font size="3"><b>Status:Approved</b></font></h3> </td>
                                <td  height="30"  style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse"> <h3><font size="3"><b>Date of Issue:<%=poDate%></b></font></h3> </td>
                               

                            </tr>
                            <tr>
                                <td style="width: 460px; height:100px;">
                                    <table width="100%">

<!--                                        <tr>
                                            <td style="border-bottom-style: solid;border-right-style: solid;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                                                <font size="2">Billed to:</font>
                                            </td>
                                        </tr>-->
                                        <tr>
                                            <td style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                                                <font size="2"><b>Vendor:</b></font> <br>

                                              <font size="2">  <b><%=vendorName%></b></font><br>
                                               <font size="2"> <%= vendorAddress%></font><br>
                                               <font size="2"> <b>Phone:</b><%=vendorPhone%></font><br>
                                               <font size="2"><b> Fax:</b></font><br>
                                               <font size="2"><b> Attention:</b></font><br>
<!--                                             <font size="2">   </font><br>
                                                 <font size="2">   </font>-->
                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                <td colspan="2" style="width: 460px;">
                                    <table width="100%">

                                        <tr>
                                            <td style="border-bottom-style: none;border-right-style: none;border-collapse: collapse;border-bottom-color:#000000;border-right-color:#000000;border-right-width: 1px;border-bottom-width: 1px;padding: 5px;">
                                                <font size="2"><b>Ship To:</b></font> <br>

                                              <font size="2">  <b>International Cargo Terminals & Rail Infrastructure Pvt. Ltd.</b></font><br>
                                               <font size="2"> Panchi Gujaran,Tehsil-Ganaur,Dist. Sonepat-131101</font><br>
                                               <font size="2"> Haryana, INDIA</font><br>
                                               <font size="2"> Tel. No: 0130 3053490</font><br>
                                               <font size="2"> Fax No: 0130 3053400</font><br>
                                               <font size="2"> E-mail: dictreception@ict.in</font><br>
                                               <font size="2"> Website Address: www.ict.in</font><br>
                                               <font size="2"> PAN NO. AACCB8054G</font><br>
                                               <font size="2"> GST NO. 06AACCB8054G1ZT</font><br>
                                               <font size="2"> CIN. U63040MH2006PTC159885</font><br>
                                                <font size="2"><b> Attention:</b></font>

                                            </td>
                                        </tr>

                                    </table>
                                </td>
                                   
                            </tr>
                            <tr>
                                <td height="30" ><font size="2"><b>Vendor Ref No:</b></font></td>
                                <td height="30" ><font size="2"><b>Requested Delivery Date:</b></font></td>
                                <td height="30" ><font size="2"><b>Validity Date:</b></font></td>
                            </tr>
                            <tr>
                                <td height="30"  colspan="3"><font size="2">Dear Sirs,
We are pleased to place the purchase order for the supply of the following items as per the under mentioned terms and conditions,
attached enclosure if any which shall form part of this order.</font></td>
                            </tr>
                           
                           <tr >
                                <td height="80" colspan="3" >
                                  
<table width="100%" align="center" border="0" cellpadding="0" cellspacing="0" class="border" style="margin-bottom:25px; ">


        <tr>
        <Td class="text2"  height="28" valign="top" class="border" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;"> <strong>S.NO</strong></Td>
        <Td class="text2" valign="top" width="130" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>ITEM NO</strong></Td>
        <Td class="text2"  valign="top" width="220" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>DESCRIPTION</strong></Td>
        <Td  class="text2" valign="top" width="30" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UOM</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>UNIT PRICE</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>QTY</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>DIS %</strong></Td>
        <Td class="text2"  valign="top" width="70" style="padding-left:2px; border:1px; border-color:#000000; border-style:solid;" class="border" align="center"><strong>TOTAL</strong></Td>
        </tr>

 <%
    
     
     
     
     
     
     index = 0;
        float totalPrice = 0.00f;
        float totalTax = 0.00f;
        float nettPrice = 0.00f;
        float igst = 0.00f;
        float cgst = 0.00f;
        float sgst = 0.00f;
        int i=0;
        String mask = "0.00";
         String totalPrice1 = "";
        DecimalFormat df = new DecimalFormat(mask);
	for(int j=i; ( j < (listSize+i) ) && (j<poDetail.size() ) ; j++){
		purch = new PurchaseTO();
		purch = (PurchaseTO)poDetail.get(j);
                mfrCode = purch.getMfrCode();
                itemName  = purch.getItemName();
                if(purch.getMfrCode().length() > mfrCodeLimit ){
                    mfrCode = mfrCode.substring(0,mfrCodeLimit-1);
                }if(purch.getItemName().length() > itemNameLimit ){
                    itemName = itemName.substring(0,itemNameLimit-1);
                }
                System.out.println("j=="+j);
                
               
%>

                <tr>
                    <Td valign="top" HEIGHT="20" class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"   > <%= j+1 %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"> <%= mfrCode %> &nbsp; </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-right-style:solid; border-left-style:solid;" align="left"> <%= itemName %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="center"> <%= purch.getUomName() %> </td>
<!--                    <Td valign="top" HEIGHT="20"  class="text1" width="60"  style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getReqQty() %> </td>-->
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getUnitPrice() %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getApprovedQty() %> </td>
                    <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getDiscount() %> </td><!--
-->                 <%--   <Td valign="top" HEIGHT="20"  class="text1"   style="border:1px;  border-right-style:solid; border-left-style:solid;" align="right"> <%= purch.getVat() %> </td>--%>
                    <%  //compute total price
                        totalPrice = Float.parseFloat(purch.getApprovedQty()) * Float.parseFloat(purch.getBuyPrice());
                        //deduct discount
                        //totalPrice = totalPrice - (totalPrice*Float.parseFloat(purch.getDiscount())/100);
                        //add tax
//                        totalTax = totalPrice*Float.parseFloat(purch.getVat())/100;
//                        totalPrice = totalPrice + totalTax;

                        nettPrice = nettPrice + totalPrice;
                        totalPrice1 = df.format(totalPrice);
                     %>

                    <td valign="top" HEIGHT="20"  class="text1"   style="border:1px; border-color:#000000; border-right-color:#000000; border-right-style:solid; border-left-style:solid;" align="right"> <%=df.format(totalPrice)%> </td>
                </tr>

                <%  index++; }  %>

            </table></td></tr>
               
                <tr >
                    <td colspan="3">
                        <table border="1" width="100%">
                            <tr>
                    <td colspan="4" align="left">
                        <font size="3"><b>TERMS AND CONDITIONS:</b></font><br>
                        <font size="2"><b>1.Payment Terms:&nbsp;<c:out value="${paymentDays}" /></b></font><br>
                        <font size="2"><b>2.Ship Via:&nbsp;<c:out value="${shipVia}" /></b></font><br>
                        <font size="2"><b>3.Warranty::&nbsp;<c:out value="${warranty}" /></b></font><br>
                        <font size="2"><b>4.Fright Terms:&nbsp;<c:out value="${freightTerms}" /></b></font><br>
                        <font size="2"><b>5.Special Instructions:&nbsp;</b></font><br>
                        <font size="2"><b><c:out value="${remarks}" /></b></font><br>
                              
                        </td>
                    <td colspan="4" align="left">
                        <font size="2"><b>Total Line Cost:<%=df.format(nettPrice)%></b></font><br>
                        <font size="2"><b>Total Discount:</b></font><br>
                        <font size="2"><b>Total Pck & Fwd:0.00</b></font><br>
                        <font size="2"><b>Total Excise Duty:0.00</b></font><br>
                         <% String igstTaxPercentage= (String)request.getAttribute("igst");
                    if(!"0".equalsIgnoreCase(igstTaxPercentage)){%>
                   <% igst = nettPrice*Float.parseFloat(igstTaxPercentage)/100; %>
                    <font size="2"><b>Total IGST:<%=df.format(igst)%></b></font><br>
                <%}%>
                
                <% String cgstTaxPercentage= (String)request.getAttribute("cgst");
                    if(!"0".equalsIgnoreCase(cgstTaxPercentage)){%>
                      <font size="2"><b>Total CGST:<%=df.format(cgst)%></b></font><br>
                   <% cgst = nettPrice*Float.parseFloat(cgstTaxPercentage)/100; %>
                <%}%>
                <% String sgstTaxPercentage= (String)request.getAttribute("sgst");
                    if(!"0".equalsIgnoreCase(sgstTaxPercentage)){%>
                    <font size="2"><b>Total SGST:<%=df.format(sgst)%></b></font><br>
                   <% sgst = nettPrice*Float.parseFloat(sgstTaxPercentage)/100; %>
                <%}%>
                <% if(!"0".equalsIgnoreCase(igstTaxPercentage)&& "0".equalsIgnoreCase(sgstTaxPercentage) && "0".equalsIgnoreCase(sgstTaxPercentage)){%>
                    <font size="2"><b>Total CGST:0.00</b></font><br>
                <font size="2"><b>Total SGST:0.00</b></font><br>
                 <%}%>
                    <%if("0".equalsIgnoreCase(igstTaxPercentage) && !"0".equalsIgnoreCase(sgstTaxPercentage) && !"0".equalsIgnoreCase(sgstTaxPercentage)){%>
                 <font size="2"><b>Total IGST:0.00</b></font><br>
                  <%}%>
                      
                        
                          <% nettPrice = nettPrice + sgst+cgst+igst; %>
                        <font size="2"><b>PO Total Value: <%=df.format(nettPrice)%></b></font><br>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="8" >
                                    <font size="3"> <b>PO Total Value in words :</b></font><br>
                                    <font size="3"> <b>Rs.</b>
                                    <jsp:useBean id="spareTotalRound"   class="ets.domain.report.business.NumberWordsIndianRupees" >
                                    <% spareTotalRound.setRoundedValue(String.valueOf(nettPrice));%>
                                    <% spareTotalRound.setNumberInWords(spareTotalRound.getRoundedValue());%>
                                    <b><jsp:getProperty name="spareTotalRound" property="numberInWords" />&nbsp; </b>
                                    </jsp:useBean>
                                     </font>
                              </td>
  </tr>
  <tr>
<td colspan="8" align="center"><font size="3"> <b>Acknowledgement</b></font><br>
</td>
  </tr>
  <tr>
      <td>
          <font size="3"> To,</font><br/>
          <font size="3"> Materials Department,</font><br/>
          <font size="3"> ICTRIPL</font><br/>
      </td>
      <td colspan="7">
          &nbsp;
      </td>
  </tr>
  <tr>
      <td colspan="3"><font size="3">The receipt of P.O. No.&emsp;<%=poCode1%></font></td>
      <td colspan="2"><font size="3">Dt.&emsp;<%=poDate%></font></td>
      <td colspan="3"><font size="3">is hereby acknowledged</font></td>
  </tr>
  <tr><td colspan="8"><font size="3">We will complete the supply on or before the expiry of the Delivery date</td></tr>
  <tr height="30"><td colspan="8">&nbsp;</td></tr>
  <tr height="30"><td colspan="8">&nbsp;</td></tr>
  <tr height="30"><td colspan="7">&nbsp;</td><td align="left"><font size="3"><b>Supplier's signature</b></font></td></tr>
  <tr height="30"><td colspan="7">&nbsp;</td><td align="left"><font size="3"><b>with seal & date</b></font></td></tr>
  
                        </table></td></tr>
</table>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                        <br/>
                <table  align="center" cellpadding="0" cellspacing="0" border="2"  rules="all" frame="hsides" 
                                style="border-collapse: collapse;width:1000px;border-color:#000000;border-style:solid;
                                font-size:12px;font-family:Arial;height:800px;border-left: none;border-right: none;">     
                           <tr>
                                <td colspan="3" height="30">
                                    <br>
                                    <br>
                                    <br>
                                    <% int i1=1;%>
                                    <font size="3"><b>GENERAL TERMS AND CONDITIONS</b></font><br><br>
                                    <font size="2"><b>1.GENERAL TERMS:</b> <br></font> <br>
                                    <font size="2"> a) Vendor/Contractor should follow all safety & others rules as instructed by ICTRIPL<br></font>
                                    <font size="2"> b) Vendor/Contractor should provide Photo Identity Proof (Driving license / Voter card / Adhaar card or pan card / TIC / ESIC room for issuing gate pass.<br></font>
                                    <font size="2"> c) Vendor/Contractor should provide and maintain all the requisite PPE (Safety helmet, Safety Shoe, Reflective Jacket, Safety Shoe, Reflective Jacket, <br></font>
                                   <font size="2"> Safety Harness, Welding goggles etc.) for all your workmen and staff engaged for the work and shall ensure all your workmen and staff uses that PPE on job.<br></font>
                                   <font size="2"> ICTRIPL will have full right to remove such workers & staff, who do follow safety rule or avoid wearing PPE.<br></font>
                                    <font size="2"> d) Vendor/Contractor shall strictly follow security instructions at premises / site and proper control of your  <br></font>
                                   <font size="2"> men and material, entry of your personnel in company premises shall be with valid entry passes only, <br></font>
                                   <font size="2"> issued and approved by company’s .<br></font>
                                    <font size="2"> e) Vendor/Contractor shall record entry of all his incoming Tools & Machinery, which are returnable, at company’s security gate.<br></font>
                                    <font size="2"> f) Vendor/Contractor should ensure tools and machinery used for job in such a way so that it may not directly or indirectly the environment and people and should be in good working condition.<br></font>
                                    <font size="2"> g) Vendor/Contractor should ensure that no damage is done to the property of ICTRIPL at the time of execution of job<br></font>
                                    <font size="2"> h) Driver of the vehicle should have valid driving license, otherwise will not be permitted inside terminal.<br></font>
                                    <font size="2"> i) ICTRIPL is not responsible for any damage / accident for vendors men and machine during execution of<br></font>
                                    <font size="2"> j) Vendor to acknowledge understanding of company policy / assets and undertake to adhere with it claims<br></font>
                                    <font size="2"><b>2. Liquidate Damage:</b>  If you fail to complete/deliver the job/material within the Date of Delivery / Completion, <br></font>
                                    <font size="2">the liquidated will be applicable at the rate of 5% per week on the PO value, Subject to a maximum of 10% of PO Value.<br></font>
                                    <font size="2"><b>3. Warranty </b>Standard warranty will be applicable as per manufacturer. Supplier should arrange for free replacement if any defect is reported within warranty period. <br></font>
                                    <font size="2"><b>4. </b>Supplier should provide Guarantee / Test certificate for the goods supplied and make available such facilities for the inspection as required to confirm the product specification <br></font>
                                    <font size="2"><b>5. </b>Supplier of material should accompany Delivery challan or Invoice (in triplicate) and the material should be delivered at Gate / Store only. Original Invoice along with one copy of Delivery challan,<br></font>
                                    <font size="2">acknowledged by gate security / store keeper Purchase-Dept. within one week.<br></font>
                                    <font size="2"><b>6. Packing:</b>Packing to be done properly to ensure no damage occurs during transportation and environmental friendly. <br></font>
                                    <font size="2"><b>7.</b>The goods / services delivered are subjected to final inspection and approval by the purchaser regarding quantity, quality and specifications.<br></font>
                                    <font size="2"><b>8.</b>. Any rejected material should be removed by the supplier at his own cost within 7 days from the date of intimation and should arrange the replacement with in same period.<br></font>
                                    
                                    <br><br>
                                    
                           <font size="2"><center><b>This is system generated document and hence does not need a signature. </b></center></font>
                                    

                                     </td>
                        </tr>  
                        <tr>
                            
                         </tr>
                        </table>

                    </div>
                    <br>
                    <br>
                    <center>
                        <input type="button" class="button"  value="Print" onClick="print('print');" >
                        
                        <%--<%=String status = '<%=request.getAttribute("status")%>';%>--%>
                        <br>
                        <br>
                        <!--<span id="cancel" style="display:none"><input type="button" class="button"  value="cancel" onClick="cancel('<c:out value="${invoicecode}"/>');" ></span>-->
                        
                        
                    </center>
                </div>
                                 
                                            <script type="text/javascript">
                                     function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
                </script>
                <script>
                    $(".nexttab").click(function () {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                </script>
                
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                    

    </body>
</html>