


<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="java.util.Calendar"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->


        <script type="text/javascript">
            $(document).ready(function() {
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $( ".datepicker" ).datepicker({
                    /*altField: "#alternate",
                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });
            });

        </script>
    </head>
    <script language="javascript">
        function submitPage() {
            
                document.customer.action = '/throttle/handlefinanceAdvice.do';
                document.customer.submit();
        }
    </script>
    
    <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

	 <div class="contentpanel">
            <div class="panel panel-default">
             <div class="panel-body">

    
    <body>
        <form name="customer" method="post" >
            <%--<%@ include file="/content/common/path.jsp" %>--%>
            <%@ include file="/content/common/message.jsp" %>
           


                               <table  class="table table-info mb30 table-hover">
                                     
            
                           <%!
                           public String NullCheck(String inputString)
                                {
                                
                                        try
                                        {
                                                if ((inputString == null) || (inputString.trim().equals("")))
                                                                inputString = "";
                                        }
                                        catch(Exception e)
                                        {
                                                                inputString = "";
                                        }
                                        return inputString.trim();
                                }
                           %>

                           <%

                            String today="";
                            String fromday="";
                           
                            fromday = NullCheck((String) request.getAttribute("fromdate"));
                            today = NullCheck((String) request.getAttribute("todate"));

                            if(today.equals("") && fromday.equals("")){
                            Date dNow = new Date();
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(dNow);
                            cal.add(Calendar.DATE, 0);
                            dNow = cal.getTime();

                            Date dNow1 = new Date();
                            Calendar cal1 = Calendar.getInstance();
                            cal1.setTime(dNow1);
                            cal1.add(Calendar.DATE, -6);
                            dNow1 = cal1.getTime();

                            SimpleDateFormat ft = new SimpleDateFormat("dd-MM-yyyy");
                            today = ft.format(dNow);
                            fromday = ft.format(dNow1);
                            }

            %>
              
             <thead>
                                    <th colspan="6"><spring:message code="operations.label.ViewFinanceAdvice"  text="default text"/> </th>
                                     </thead>
            <tr>
                                        <td><font color="red">*</font><spring:message code="operations.label.FromDate"  text="default text"/></td>
                                        <td><input name="fromDate" id="fromDate" type="text" style="width:260px;height:40px;"  class=" datepicker form-control" autocomplete="off" onclick="ressetDate(this);" value="<%=fromday%>"></td>
                                        <td><font color="red">*</font><spring:message code="operations.label.ToDate"  text="default text"/></td>
                                        <td ><input name="toDate" id="toDate" type="text" style="width:260px;height:40px;"  class="datepicker form-control" autocomplete="off" onclick="ressetDate(this);" value="<%=today%>"></td>
                                        <td></td>
                                        <td><input type="button" class="btn btn-success" name="search" onClick="submitPage();" value="<spring:message code="operations.label.SEARCH"  text="default text"/>"></td>
                                        
                                    </tr>
                                    <td></td>
                                </table>
            <c:if test = "${financeAdvice != null}" >
                <table  class="table table-info mb30 table-hover">
                    <thead>
                        <tr height="40">
                            <th><spring:message code="operations.label.SNo"  text="default text"/></th>
                            <th><spring:message code="operations.label.AdviceDate"  text="default text"/></th>                            
                            <th><spring:message code="operations.label.AdviceType"  text="default text"/></th>                            
                            <th><spring:message code="operations.label.NoofTrips"  text="default text"/></th>
                            <th><spring:message code="operations.label.EstimatedAmt"  text="default text"/>.</th>
                            <th><spring:message code="operations.label.RequestedAmt"  text="default text"/>.</th>
                            <th><spring:message code="operations.label.PaidAmt"  text="default text"/>.</th>                            
                            <th><spring:message code="operations.label.View"  text="default text"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${financeAdvice}" var="fd">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" ><c:out value="${fd.advicedate}"/> </td>                                

                                <td align="left" >
                                <c:if test="${(fd.batchType=='b') || (fd.batchType=='B')}" >
                                   <spring:message code="operations.label.Batch"  text="default text"/> 
                                    </c:if>
                                    <c:if test="${(fd.batchType=='a') || (fd.batchType=='A')}" >
                                   <spring:message code="operations.label.Adhoc"  text="default text"/> 
                                    </c:if>
                                    <c:if test="${(fd.batchType=='m') || (fd.batchType=='M')}" >
                                    <spring:message code="operations.label.Manual"  text="default text"/>
                                    </c:if>
                                </td>

                                <td align="left" ><c:out value="${fd.tripday}"/></td>                                
                                <td align="left" >
                                    <c:if test="${(fd.estimatedadvance=='')||(fd.estimatedadvance==null)}" >
                                    0
                                    </c:if>
                                    <c:if test="${(fd.estimatedadvance!='')||(fd.estimatedadvance!=null)}" >
                                    <c:out value="${fd.estimatedadvance}"/>
                                    </c:if>
                                </td>
                                <td align="left" ><c:out value="${fd.requestedadvance}"/></td>
                                <td align="left" ><c:out value="${fd.actualadvancepaid}"/></td>
                                <td align="left" >
                                    <a href="/throttle/viewFinanceAdvice.do?dateval=<c:out value="${fd.advicedate}"/>&active=<c:out value="${fd.isactive}"/>&type=<c:out value="${fd.batchType}"/>&tripType=<c:out value="${tripType}"/>"><spring:message code="operations.label.viewdetails"  text="default text"/></a>
                                </td>                                
                                
                            </tr>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <br/>
            <br/>
            <br/>
            <c:if test = "${vehicleAdvanceRequest != null}" >
                <table  class="table table-info mb30 table-hover">
                    <thead>
                        <tr height="40">
                            <th><spring:message code="operations.label.SNo"  text="default text"/></th>
                            <th><spring:message code="operations.label.AdviceDate"  text="default text"/></th>                            
                            <th><spring:message code="operations.label.AdviceType"  text="default text"/></th>                            
                            <th><spring:message code="operations.label.RequestedAmt"  text="default text"/>.</th>
                            <th><spring:message code="operations.label.PaidAmt"  text="default text"/>.</th>                            
                            <th><spring:message code="operations.label.View"  text="default text"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${vehicleAdvanceRequest}" var="vehicle">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                             <c:if test = "${vehicle.approvalStatus != null}" >
                            <tr height="30">
                                <td align="left" ><%=sno%></td>
                                <td align="left" ><c:out value="${vehicle.advanceDate}"/> </td>                                

                                <td align="left" >
                                  <spring:message code="operations.label.VehicleDriver"  text="default text"/> 
                                </td>

                                
                                <td align="left" ><c:out value="${vehicle.requestedAdvance}"/></td>
                                <td align="left" ><c:out value="${vehicle.paidAdvance}"/></td>
                                <td align="left" >
                                     <a href="/throttle/viewVehicleDriverAdvanceDetails.do?tripAdvanceId=<c:out value="${vehicle.vehicleDriverAdvanceId}"/>&vehicleId=<c:out value="${vehicle.vehicleId}"/>"><spring:message code="operations.label.viewdetails"  text="default text"/></a>
                                </td>                                
                                
                            </tr>
                             </c:if>
                        <%
                                   index++;
                                   sno++;
                        %>
                    </c:forEach>

                    </tbody>
                </table>
            </c:if>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.label.EntriesPerPage"  text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="operations.label.of"  text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 0);
            </script>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>


</div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>

