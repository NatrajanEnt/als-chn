<%-- 
    Document   : driverSettlementReportExcel
    Created on : Dec 13, 2013, 01:31:16 PM
    Author     : Arul
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@page import="java.text.SimpleDateFormat"%>

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <%@ page import="java.util.* "%>
        <%@ page import="java.text.* "%>

        <%@ page import="ets.domain.report.business.ReportTO" %>
        <style type="text/css">
            .contentsub {
                padding:3px;
                height:24px;
                text-align:left;
                font-weight:bold;
                font-size:14px;
                background:#129fd4;
                color:#ffffff;
                background:url(../images/title_bg1.jpg) repeat-x top left #7f8ba5;
            }
            .text1 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                font-size:12px;
                font-weight:normal;
            }

            .text2 {
                height:25px;
                border-bottom:1px solid;
                border-bottom-color: #f2f2f2;
                background:#f2f2f2;
                font-size:12px;
                font-weight:normal;
            }
        </style>

    </head>
      <%
                String menuPath = "Finance >> Daily Advance Advice";
                request.setAttribute("menuPath", menuPath);
                String dateval = request.getParameter("dateval");
                String active = request.getParameter("active");
                String type = request.getParameter("type");
    %>
     
    <body>

        <form name="tripSheet" action=""  method="post">
             <%
                        Date dNow = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("dd/MM/yyyy:hh:mma");
                        //System.out.println("Current Date: " + ft.format(dNow));
                        String curDate = ft.format(dNow);
                        String expFile = "VehicleComplaintsHistoryDetails-" + curDate + ".xls";

                        String fileName = "attachment;filename=" + expFile;
                        response.setContentType("application/vnd.ms-excel;charset=UTF-8");
                        response.setHeader("Content-disposition", fileName);
        %>
        
      

            <br>
            <br>
            <br>

<br><br>
            
            <br>
            <br>
              
            <table align="center" border="0" id="table" class="sortable" style="width:1700px;" >
                <thead>
                    <tr height="50">
                                        <th  height="30">S.No</th>
                                        <th  height="30">JobCard </th>
                                        <th  height="30">Date</th>
                                        <th  height="30">RegNo</th>
                                        <th  height="30">KM</th>
                                        <th  height="30">Technician</th>
                                        <th  height="30">ServicePoint</th>
                                        <th  height="30">ServiceType</th>
                                        <th  height="30">Status</th>
                                        <th  height="30">Section</th>                    
                                        <th  height="30">Complaints</th>
                                        <th  height="30">Item</th>
                                        <th  height="30">UOM</th>
                                        <th  height="30">ReqQty</th>
                                        <th  height="30">IssuedQty</th>
                                        <th  height="30">Activity</th>
                                        <th  height="30">Remarks</th>
                    </tr>
                </thead>
                <c:if test="${complaintList != null}">
                <tbody>
                    <% int index = 1;%>
                    <c:forEach items="${complaintList}" var="complaint">
                        <%
                                    String className = "text1";
                                    if ((index % 2) == 0) {
                                        className = "text1";
                                    } else {
                                        className = "text2";
                                    }
                        %>
                        <tr >
                                        <td class="form-control"  >
                                            <%=index%>
                                        </td>
                                        <td  height="30"><c:out value="${complaint.jobCardId}" /></td>
                                    <td  height="30"><c:out value="${complaint.createdDate}" /></td>
                                    <td  height="30"><c:out value="${complaint.regNo}" /></td>
                                    <td  height="30"><c:out value="${complaint.vehicleKm}" /></td>
                                    <td  height="30"><c:out value="${complaint.technician}" /></td>
                                    <td  height="30"><c:out value="${complaint.servicePointId}" /></td>
                                    <td  height="30"><c:out value="${complaint.serviceTypeName}" /></td>
                                    <td  height="30"><c:out value="${complaint.problemStatus}" /></td>
                                    <td  height="30"><c:out value="${complaint.section}" /></td>
                                    <td  height="30"><c:out value="${complaint.problem}" /></td>
                                    <td  height="30"><c:out value="${complaint.mfrCode}" /></td>
                                    <td  height="30"><c:out value="${complaint.itemName}" /></td>
                                    <td  height="30"><c:out value="${complaint.reqQty}" /></td>
                                    <td  height="30"><c:out value="${complaint.issuedQty}" /></td>
                                    <td  height="30"><c:out value="${complaint.activity}" />&emsp;</td>                        
                                    <td  height="30"><c:out value="${complaint.remarks}" /></td>
                                    </tr>

                        <%index++;%>
                    </c:forEach>
                </tbody>
            </table>
        </c:if>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>