<%-- 
    Document   : viewbillgeneration
    Created on : Nov 4, 2013, 8:33:48 PM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>
        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <%@ page import="ets.domain.operation.business.OperationTO" %>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>
        <link href="/throttle/css/tableFilter.css" rel="stylesheet" type="text/css"/>
        <link href="/throttle/css/filtergrid.css" rel="stylesheet" type="text/css"/>
        <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>
        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script src="/throttle/js/jquery.ui.core.js"></script>
        <script src="/throttle/js/jquery.ui.datepicker.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                //alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script>
            function submitPage()
            {
                document.billGeneration.action = "/throttle/content/report/viewbillgeneration.jsp";
                document.billGeneration.submit();
            }
            function savePage()
            {
                document.billGeneration.action = "/throttle/content/report/printbillgeneration.jsp";
                document.billGeneration.submit();
            }

        </script>
    </head>
    <body>
        <form name="billGeneration" method="post">
            <%@ include file="/content/common/path.jsp" %>



            <%@ include file="/content/common/message.jsp"%>
            <table width="800" cellpadding="0" cellspacing="0" align="center" border="0" id="report" style="margin-top:0px;">
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Bill Generation</li>
                            </ul>
                            <div id="first">
                                <table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >
                                    <tr>
                                        <td><font color="red">*</font>Customer</td>
                                        <td height="30">
                                            <select class="form-control" name="Customer" id="Customer"  style="width:125px;">
                                                <option value="0">---Select---</option>
                                                <option value='1' >Dell</option>
                                                <option value='2' >Intel</option>
                                                <option value='3' >Samsung</option>
                                            </select>
                                        </td>
                                        <td><font color="red">*</font>Ownership</td>
                                        <td height="30">
                                            <select class="form-control" name="ownership" id="ownership"  style="width:125px;">
                                                <option value="0">---Select---</option>
                                                <option value='1' >Own</option>
                                                <option value='2' >Attach</option>
                                            </select>
                                        </td>
                                        <td><font color="red">*</font>Bill Type</td>
                                        <td height="30">
                                            <select class="form-control" name="billtype" id="billtype"  style="width:125px;">
                                                <option value="0">---Select---</option>
                                                <option value='1' >Depot</option>
                                                <option value='2' >Tancem</option>
                                                <option value='3' >Party</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><font color="red">*</font>From Date</td>
                                        <td height="30"><input name="fromDate" id="fromDate" type="text" class="datepicker"  onclick="ressetDate(this);"></td>
                                        <td><font color="red">*</font>To Date</td>
                                        <td height="30"><input name="toDate" id="toDate" type="text" class="datepicker" onclick="ressetDate(this);"></td>

                                        <td> <input type="hidden" name="days" id="days" value="" /></td>
                                        <!--                                        <td><input type="button" class="button"   value="Search" onclick="submitPage();"></td>-->
                                    </tr>


                                </table>
                            </div></div>
                    </td>
                </tr>
            </table>
            <br>
            <br>
            <h2>View Bill's</h2>
            <br>
            <table width="95%"  align="center" cellpadding="0" cellspacing="0" class="border">
                <thead>
                    <tr>
                        <td width="30" class="contentsub">Sno</td>
                        <td width="60" class="contentsub">CN No</td>
                        <th width="50" class="contentsub">Trip Id</th>
                        <th width="50" class="contentsub">Trip Date</th>
                        <td width="50" class="contentsub">Customer Name </td>
                        <td width="50" class="contentsub">Customer Code </td>
                        <td width="50" class="contentsub">Origin </td>
                        <td width="50" class="contentsub">Destination </td>
                        <td width="60" class="contentsub">Vehicle Type </td>
                        <td width="50" class="contentsub">Weight(MT)</td>
                        <td width="60" height="30" class="contentsub">Required Pickup Date &amp; Time </td>
                        <td width="60" height="30" class="contentsub">Estimated Delivery Date &amp; Time</td>
                        <td width="50" height="30" class="contentsub">Transit Days</td>
                        <td width="40" height="30" class="contentsub">Bill Status</td>
                        <td width="40" height="30" class="contentsub">Status</td>
                        <td width="40"height="30" class="contentsub">Select</td>
                    </tr>
                </thead>
                <tr>
                    <td class="text1">1</td>
                    <td class="text1">AD16700</td>
                    <td class="text1">19</td>
                    <td class="text1">01/11/2013</td>
                    <td class="text1">Sundaram</td>
                    <td class="text1">BF001</td>
                    <td class="text1">Kashipur</td>
                    <td class="text1">Delhi</td>
                    <td class="text1">TATA/3118</td>
                    <td class="text1">20</td>
                    <td class="text1">01/11/2013&amp;07:30AM</td>
                    <td class="text1">03/11/2013&amp;05:30PM</td>
                    <td class="text1">2.5Days</td>
                    <td class="text1">Paid</td>
                    <td class="text1">Close</td>
                    <td class="text1"><input type="checkbox" name="select" value="Alter"></td>
                </tr>
                <tr>
                    <td class="text2">2</td>
                    <td class="text2">AD14500</td>
                    <td class="text2">17</td>
                    <td class="text2">05/11/2011</td>
                    <td class="text2">John</td>
                    <td class="text2">BF019</td>
                    <td class="text2">Kashipur</td>
                    <td class="text2">Balwal</td>
                    <td class="text2">TATA/3118</td>
                    <td class="text2">35</td>
                    <td class="text2">05/11/2011&amp;10:00AM</td>
                    <td class="text2">07/11/2011&amp;10:00AM</td>
                    <td class="text2" >2 Days</td>
                    <td class="text2" >Paid</td>
                    <td class="text2" >Close</td>
                    <td class="text2"><input type="checkbox" name="select" value="Alter"></td>
                </tr>
                <tr>
                    <td class="text1">3</td>
                    <td class="text1">AD1034</td>
                    <td class="text1">15</td>
                    <td class="text1">06/11/2013</td>
                    <td class="text1">Balaji</td>
                    <td class="text1">BF005</td>
                    <td class="text1">Balwal</td>
                    <td class="text1">Delhi</td>
                    <td class="text1">AL/2516</td>
                    <td class="text1">25</td>
                    <td class="text1">06/11/2013&amp;04:00PM</td>
                    <td class="text1">09/11/2013&amp;04:00PM</td>
                    <td class="text1">3 Days</td>
                    <td class="text1">Paid</td>
                    <td class="text1">Close</td>
                    <td class="text1"><input type="checkbox" name="select" value="Alter"></td>
                </tr>
                <tr>
                    <td class="text2">4</td>
                    <td class="text2">AD100</td>
                    <td class="text2" >10</td>
                    <td class="text2" >01/11/2013</td>
                    <td class="text2" >Prakash</td>
                    <td class="text2">BF147</td>
                    <td class="text2">Kashipur</td>
                    <td class="text2">Chennai</td>
                    <td class="text2">TATA/3118</td>
                    <td class="text2">27</td>
                    <td class="text2">01/11/2013&amp;11:00AM</td>
                    <td class="text2">04/11/2013&amp;11:00AM</td>
                    <td class="text2">3 Days</td>
                    <td class="text2">Paid</td>
                    <td class="text2">Close</td>
                    <td class="text2" ><input type="checkbox" name="select" value="Alter"></td>
                </tr>
                <tr>
                    <td class="text1">5</td>
                    <td class="text1">AD1036</td>
                    <td class="text1">12</td>
                    <td class="text1">05/11/2013</td>
                    <td class="text1">John</td>
                    <td class="text1">BF019</td>
                    <td class="text1">Balwal</td>
                    <td class="text1">Chennai</td>
                    <td class="text1">AL/2516</td>
                    <td class="text1">30</td>
                    <td class="text1">05/11/2013&amp;05:00AM</td>
                    <td class="text1">09/11/2013&amp;05:00AM</td>
                    <td class="text1">4 Days</td>
                    <td class="text1">Paid</td>
                    <td class="text1">Close</td>
                    <td class="text1"><input type="checkbox" name="select" value="Alter"></td>
                </tr>
                <tr>
                    <td class="text2">6</td>
                    <td class="text2">AD100</td>
                    <td class="text2" >10</td>
                    <td class="text2" >01/11/2013</td>
                    <td class="text2" >Prakash</td>
                    <td class="text2">BF147</td>
                    <td class="text2">Chandigarh</td>
                    <td class="text2">Chennai</td>
                    <td class="text2">TATA/2518</td>
                    <td class="text2">28</td>
                    <td class="text2">01/11/2013&amp;09:00AM</td>
                    <td class="text2">03/11/2013&amp;09:00AM</td>
                    <td class="text2">2 Days</td>
                    <td class="text2">Paid</td>
                    <td class="text2">Close</td>
                    <td class="text2" ><input type="checkbox" name="select" value="Alter"></td>
                </tr>
                <tr>
                    <td class="text1">7</td>
                    <td class="text1">AD1034</td>
                    <td class="text1">15</td>
                    <td class="text1">01/11/2013</td>
                    <td class="text1">John</td>
                    <td class="text1">BF019</td>
                    <td class="text1">Balwal</td>
                    <td class="text1">Delhi</td>
                    <td class="text1">AL/2516</td>
                    <td class="text1">25</td>
                    <td class="text1">01/11/2013&amp;12:00PM</td>
                    <td class="text1">04/11/2013&amp;12:00PM</td>
                    <td class="text1">3 Days</td>
                    <td class="text1">Paid</td>
                    <td class="text1">Close</td>
                    <td class="text1"><input type="checkbox" name="select" value="Alter"></td>
                </tr>
            </table>
            <tr>
                <td>
                    <br>
                    <center>
                        <input type="button" class="button" value="billgenerate" name="billgenerate" onClick="savePage()">
                    </center>
                </td>
            </tr>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
