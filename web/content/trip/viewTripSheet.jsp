
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>

<script src="//select2.github.io/select2/select2-3.3.2/select2.js"></script>

<link rel="stylesheet" type="text/css" href="//select2.github.io/select2/select2-3.3.2/select2.css"/>

<link rel="stylesheet" type="text/css" href="/throttle/css/select2-bootstrap.css"/>

<style type="text/css">
    option.red {font-color: red;}
    option.green {font-color: greenyellow;}
</style>
<script>
    $('.btnNext').click(function () {
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
    });
    $('.btnPrevious').click(function () {
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
    });
</script>

<script type="text/javascript">
    function viewSettlementPrint(tripSheetId, vehicleId, driverId, status) {
        window.open('/throttle/viewDriverSettlementPrint.do?tripSheetId=' + tripSheetId + '&vehicleId=' + vehicleId + '&driverId=' + driverId + '&activeStatus=' + status, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewTripStartPrint(tripSheetId, vehicleId) {
        window.open('/throttle/viewTripStartPrint.do?tripSheetId=' + tripSheetId + '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function printTripAdvance(tripSheetId) {
        window.open('getExpensePrintDetails.do?tripId=' + tripSheetId + '&param=advance', 'PopupPage', 'height = 700, width = 900, scrollbars = yes, resizable = yes');
    }
    function printTripFuel(tripSheetId) {
        window.open('getExpensePrintDetails.do?tripId=' + tripSheetId + '&param=fuel', 'PopupPage', 'height = 700, width = 900, scrollbars = yes, resizable = yes');
    }
    function mailTriggerPage(tripId) {
        window.open('/throttle/viewTripDetailsMail.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewCurrentLocation(vehicleId) {
        window.open('/throttle/viewVehicleLocation.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
//        var url = ' http://dict.throttletms.com/api/vehicleonmap/map.php?uname=interem&group_id=5087&height=600&width=700&vehicle=' + vehicleRegNo;
        //alert(url);
        window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

    function viewTripDetails(tripId, vehicleId, tripType) {
        window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails1(vehicleId) {
        window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
    function viewVehicleDetails(vehicleId) {
        document.tripSheet.action = '/throttle/viewVehicle.do?vehicleId=' + vehicleId;
        document.tripSheet.submit();
    }
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>
<script type="text/javascript">
    function setValues() {
        if ('<%=request.getAttribute("statusId")%>' != 'null') {
            document.getElementById('stsId').value = '<%=request.getAttribute("statusId")%>';
        }
        if ('<%=request.getAttribute("statusId")%>' != 'null') {
            document.getElementById('tripStatusId').value = '<%=request.getAttribute("statusId")%>';
        }
        if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
            document.getElementById('vehicleTypeId').value = '<%=request.getAttribute("vehicleTypeId")%>';
        }
        if ('<%=request.getAttribute("fromDate")%>' != 'null') {
            document.getElementById('fromDate').value = '<%=request.getAttribute("fromDate")%>';
        }
        if ('<%=request.getAttribute("toDate")%>' != 'null') {
            document.getElementById('toDate').value = '<%=request.getAttribute("toDate")%>';
        }
        if ('<%=request.getAttribute("fleetCenterId")%>' != 'null') {
            document.getElementById('fleetCenterId').value = '<%=request.getAttribute("fleetCenterId")%>';
        }
        if ('<%=request.getAttribute("cityFromId")%>' != 'null') {
            document.getElementById('cityFromId').value = '<%=request.getAttribute("cityFromId")%>';
        }
        if ('<%=request.getAttribute("cityFrom")%>' != 'null') {
            document.getElementById('cityFrom').value = '<%=request.getAttribute("cityFrom")%>';
        }
        if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
            document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
        }
        if ('<%=request.getAttribute("zoneId")%>' != 'null') {
            document.getElementById('zoneId').value = '<%=request.getAttribute("zoneId")%>';
        }

        if ('<%=request.getAttribute("tripSheetId")%>' != 'null') {
            document.getElementById('tripSheetId').value = '<%=request.getAttribute("tripSheetId")%>';
        }
        if ('<%=request.getAttribute("customerId")%>' != 'null') {
            document.getElementById('customerId').value = '<%=request.getAttribute("customerId")%>';
        }
//            if('<%=request.getAttribute("tripStatusId")%>' != 'null' ){
//                document.getElementById('tripStatusId').value= '<%=request.getAttribute("tripStatusId")%>';
//            }
        if ('<%=request.getAttribute("podStatus")%>' != 'null') {
            document.getElementById('podStatus').value = '<%=request.getAttribute("podStatus")%>';
        }

    }


    function submitPage() {
        //document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('stsId').value + "&param=search";
        document.tripSheet.action = '/throttle/viewTripSheets.do?statusId=' + document.getElementById('tripStatusId').value + "&param=search" + "&vendorType=<c:out value="${vendorType}"/>";
        document.tripSheet.submit();
    }

    function submitPage1() {
//        var subStatusId= document.getElementById('subStatusId').value;
//        alert(subStatusId);
        document.tripSheet.action = '/throttle/viewTripSheets.do?subStatusId=<c:out value="${subStatusId}"/>&param=ExportExcel&vendorType=<c:out value="${vendorType}"/>';
        document.tripSheet.submit();
    }
    function changeVehicleAfterTripStart(tripId, vehicleId, tripType) {
        document.tripSheet.action = '/throttle/changeVehicleAfterTripStart.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId;
        document.tripSheet.submit();
    }


</script>

<script type="text/javascript">
    var httpRequest;
    function getLocation() {
        var zoneid = document.getElementById("zoneId").value;
        if (zoneid != '') {

            // Use the .autocomplete() method to compile the list based on input from user
            $('#cityFrom').autocomplete({
                source: function (request, response) {
                    $.ajax({
                        url: "/throttle/getLocationName.do",
                        dataType: "json",
                        data: {
                            cityId: request.term,
                            zoneId: document.getElementById('zoneId').value
                        },
                        success: function (data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function (data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function (event, ui) {
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $('#cityFromId').val(tmp[0]);
                    $('#cityFrom').val(tmp[1]);
                    return false;
                }
            }).data("autocomplete")._renderItem = function (ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        }
    }

</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Trip<c:if test="${statusId == 8}"> Start</c:if><c:if test="${statusId == 10}"> End</c:if></h2>
        <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.PrimaryOperation"  text="PrimaryOperation"/></a></li>
            <li class="active">Trip<c:if test="${statusId == 8}"> Start</c:if><c:if test="${statusId == 10}"> End</c:if></li>
            </ol>
        </div>
    </div>


    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="sorter.size(5);
                        setValues();">
                    <form name="tripSheet" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <center><span id="errorMessages"><font color="red" size="4"><c:out value="${errorMessages}"/> </font></span></center>
                    <br>

                    <table class="table table-info mb30 table-hover"  align="left" style="width:70%;">

                        <tr style="display:none;" >
                            <td>Billing Type</td>
                            <td>
                                <select name="billingType" id="billingType" class="form-control"  >
                                    <option value="" selected>--Select--</option>
                                    <option value="1" >Primary</option>
                                    <option value="2" >Secondary</option>
                                    <option value="3" >All</option>
                                </select>
                            </td>

                            <td>Trip Sheet No</td>
                            <td>
                                <input name="tripSheetId" id="tripSheetId" type="text" class="form-control"   value="<c:out value="${tripSheetId}"/>"  >
                            </td>

                            <td>Vehicle Type</td>
                            <td>
                                <select name="vehicleTypeId" id="vehicleTypeId" class="form-control"  >
                                    <c:if test="${vehicleTypeList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${vehicleTypeList}" var="vehicleTypeList">
                                            <option value='<c:out value="${vehicleTypeList.typeId}"/>'><c:out value="${vehicleTypeList.typeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                        </tr>
                        <tr style="display:none;" >
                            <td>Fleet Center</td>
                            <td>
                                <select name="fleetCenterId" id="fleetCenterId" class="form-control"   >
                                    <c:if test="${companyList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${companyList}" var="companyList">
                                            <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>Zone</td>
                            <td>
                                <select name="zoneId" id="zoneId" class="form-control"   onchange="getLocation();" >
                                    <c:if test="${zoneList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${zoneList}" var="zoneList">
                                            <option value='<c:out value="${zoneList.zoneId}"/>'><c:out value="${zoneList.zoneName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <td>Location</td>
                            <td>
                                <input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityName}"/>" class="form-control" >
                                <input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityId}"/>" class="form-control"  >

                            </td>
<!--                            <td>Customer</td>
                            <td>
                                <select name="customerId" id="customerId" class="form-control"  >
                                    <c:if test="${customerList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${customerList}" var="customerList">
                                            <option value='<c:out value="${customerList.customerName}"/>'><c:out value="${customerList.customerName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>-->
                        </tr>
                        <tr >
                            <td>From Date</td>
                            <td>
                                <input name="fromDate" id="fromDate" type="text" class="form-control datepicker" onclick="ressetDate(this);" value="<c:out value="${fromDate}"/>">
                            </td>

                            <td>To Date</td>
                            <td>
                                <input name="toDate" id="toDate" type="text" class="form-control datepicker"  onClick="ressetDate(this);" value="<c:out value="${toDate}"/>">
                            </td>
                            
                            <td>Customer</td>
                            <td>
                                <select name="customerId" id="customerId" class="form-control"  >
                                    <c:if test="${customerList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${customerList}" var="customerList">
                                            <option value='<c:out value="${customerList.customerId}"/>'><c:out value="${customerList.customerName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>
                            <c:if test = "${userId != '1831'}" >
                                <td>Status</td>
                                <td>
                                    <select name="tripStatusId" id="tripStatusId" id="vehicleId" style="width:180px" class="select2 input-default form-control"   >
                                        <option value="0" selected>All</option>
                                        <c:if test="${statusDetails != null}">                                        
                                            <c:forEach items="${statusDetails}" var="statusDetails">
                                                <option value='<c:out value="${statusDetails.statusId}"/>'><c:out value="${statusDetails.statusName}"/></option>
                                            </c:forEach>
                                        </c:if>
                                        <script>
                                            $('#tripStatusId').select2({placeholder: 'Fill Status'});
                                        </script>
                                    </select>
                                </td>
                            </c:if>
                            <td>Vehicle No</td>
                            <td>
                                <select name="vehicleId" id="vehicleId" style="width:180px" class="select2 input-default form-control"   >
                                    <c:if test="${vehicleList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${vehicleList}" var="vehicleList">
                                            <option value='<c:out value="${vehicleList.vehicleId}"/>'><c:out value="${vehicleList.regNo}"/></option>
                                        </c:forEach>
                                    </c:if>
                                    <script>
                                        $('#vehicleId').select2({placeholder: 'Fill Vehicle No'});
                                    </script>
                                </select>
                            </td>
                        </tr>
                        <tr style="display:none;" >

                            <td colspan="4" >&nbsp;</td>
                            <td>POD Status</td>
                            <td>
                                <select name="podStatus" id="podStatus" class="form-control"  >
                                    <option value="" selected>--Select--</option>
                                    <option value="0" >POD Uploaded</option>
                                    <option value="1" >POD not Uploaded</option>
                                </select>
                            </td>
                        </tr>

                        <tr >
                            <td colspan="8" align="center">
                                <input type="hidden"   value="<c:out value="${statusId}"/>" class="text" name="stsId" id="stsId">
                                <input type="hidden"   value="<c:out value="${tripType}"/>" class="text" name="tripType">
                                <input type="button"   value="Search" class="btn btn-success" name="Search" onclick="submitPage();" style="width:100px;height:30px;font-weight: bold;padding:1px;">

                                <%--<c:if test="${statusId == 100}">--%>
                                <input type="button" class="btn btn-success" name="ExportExcel" id="ExportExcel" onclick="submitPage1(this.name);" value="ExportExcel" style="width:100px;height:30px;font-weight: bold;padding:1px;">
                                <%--</c:if>--%>
                                <c:if test="${statusId != 100}">
                                    &nbsp;
                                </c:if>
                        </tr>
                    </table>
                    <left>
                      <!--&emsp; <input type="button" class="btn btn-info" name="ExportExcel" id="ExportExcel"  value="<spring:message code="sales.label.customer.Export Excel"  text="Export Excel"/>" onclick="submitPage1(this.name);" >-->
                    </left>
                    <% int index = 1;%>
                    <c:if test="${tripDetails == null }" >
                        <center><font color="red" size="2"><spring:message code="trucks.label.NoRecordsFound"  text="default text"/>  </font></center>
                        </c:if>
                        <c:if test="${tripDetails != null}">

                        <table class="table table-info mb30 table-bordered" id="table" >
                            <thead >
                                <c:if test = "${pnrUser == 1}" >
                                    <tr >
                                        <th>Sno</th>
                                        <th>PNR No</th>
                                        <th>VesselName</th>
                                        <th>Vehicle aaaNo </th>
                                            <c:if test="${statusId >= 12}">
                                            <th>Container No(s) </th>
                                            </c:if>
                                        <th>Trip Code</th>
                                        <th>Order Type </th>
                                        <th>Transporter</th>
                                        <th>InterimDetails </th>
                                        <th>Driver Name with Contact </th>
                                        <th>Vehicle Type </th>
                                        <th >Container Qty</th>
                                            <c:if test="${statusId == 10}">
                                            <th>LinkToMap</th>
                                            </c:if>
                                        <th>Select</th>
                                    </tr>
                                </c:if>
                                <c:if test = "${pnrUser != 1}" >
                                    <tr >
                                        <th>Sno</th>
                                        <th>CustRefNo</th>
                                        <th>Customer</th>
                                        <th>Goods</th>
                                        <th>Vehicle No </th>
                                        <th>Trip Code</th>
                                        <th>Container No(s) </th>
                                        <th>LR No(s) </th>
                                        <th>pkg/wgt/cbm </th>
                                            <%--<th>Advance</th>
                                            <th>Fuel</th>--%>
                                        <th>Trip Date </th>
                                        <th>Order Type </th>
                                        <th>Transporter</th>
                                        <th>Route </th>
                                        <th>Driver Name with Contact </th>
                                        <th>Vehicle Type </th>                                    
                                        <th>Status</th>  
                                            <c:if test = "${userId != '1831'}" >
                                                <c:if test="${statusId == 10}">
                                                <th>LinkToMap</th>
                                                </c:if>
                                            <th>ClickToAction</th>
                                            </c:if>
                                    </tr>
                                </c:if>
                            </thead>
                            <tbody>

                                <c:forEach items="${tripDetails}" var="tripDetails">

                                    <%
                                                String className = "text1";
                                                if ((index % 2) == 0) {
                                                    className = "text1";
                                                } else {
                                                    className = "text2";
                                                }
                                    %>
                                    <tr >
                                        
                                        <td class="form-control" >
                                            <%=index%>
                                        </td>
                                        <%--   <c:if test="${tripType == 1}">
                                              <td class="<%=className%>" align="center">
                                                  <c:if test="${tripDetails.statusId == 10 || tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                                      <c:if test="${tripDetails.customerName != 'Empty Trip'}">
                                                          <c:if test="${tripDetails.podCount == '0' && tripDetails.customerOrderReferenceNo != 'Empty Trip'}">
                                                              <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podinactive.png" alt="Y"   title="click to upload pod"/></a>
                                                              </c:if>
                                                              <c:if test="${tripDetails.podCount != '0' && tripDetails.customerOrderReferenceNo != 'Empty Trip'}">
                                                              <a href="viewTripPod.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">  <img src="images/Podactive.png" alt="Y"   title="click to upload pod"/></a>
                                                              </c:if>
                                                          </c:if>
                                                      </c:if>
                                              </td>
                                          </c:if> --%>
                                        <td class="form-control" >
                                            <c:out value="${tripDetails.customerOrderReferenceNo}"/>
                                        </td>
                                        <td class="form-control" >
                                            <c:out value="${tripDetails.customerName}"/>
                                        </td>
                                        <td class="form-control" >
                                            <c:out value="${tripDetails.description}"/>
                                        </td>
                                        <c:if test = "${pnrUser == 1}" >                                            
                                            <td class="form-control" >
                                                <c:out value="${tripDetails.linerId}"/>
                                            </td>
                                        </c:if>
                                        <%--<c:out value="${tripDetails.vendorId}"/>--%>
                                        <%--<c:out value="${tripDetails.vendorId}"/>--%>
                                        <c:if test="${tripDetails.vendorId !=1 || vendorType == 1}">
                                            <td class="form-control" >
                                                <c:out value="${tripDetails.regNoHire}"/>  <font color="red"> <c:out value="${tripDetails.regNoHireOld}"/> </font>
                                            </td>
                                        </c:if>
                                        <c:if test="${tripDetails.vendorId == 1}">
                                            <td class="form-control" >
                                                <a href="#" onclick="viewVehicleDetails(<c:out value="${tripDetails.vehicleId}"/>)"><c:out value="${tripDetails.vehicleNo}"/></a><br><font color="red"><c:out value="${tripDetails.oldVehicleNo}"/></font>
                                            </td>
                                        </c:if>
                                        <c:if test = "${pnrUser == 1}" >                                            
                                            <c:if test="${statusId >= 12}">
                                                <td class="form-control" >
                                                    <c:if test="${tripDetails.statusId == 12 || tripDetails.statusId == 13  || tripDetails.statusId == 14  || tripDetails.statusId == 16}">
                                                        <c:out value="${tripDetails.containerNo}"/>
                                                    </c:if>
                                                </td>
                                            </c:if>
                                        </c:if>

                                        <td class="form-control"  >

                                            <c:if test="${tripDetails.oldVehicleNo != null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                            <c:if test="${tripDetails.oldVehicleNo == null}">
                                                <a href="#" onclick="viewTripDetails('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 0);"><c:out value="${tripDetails.tripCode}"/></a>
                                            </c:if>
                                            <br/>
                                            <font color="darkOrange"><c:out value="${tripDetails.tripSheetId}"/></font>
                                        </td>
                                        <c:if test = "${pnrUser != 1}" >                                            
                                            <td class="form-control" >
                                                <c:out value="${tripDetails.containerNo}"/>
                                            </td>
                                        </c:if>
                                        <c:if test = "${pnrUser != 1}" >                                            
                                            <td class="form-control" >
                                                <!--                                                MAA
                                                <c:if test="${tripDetails.movementType == 12 }">
                                                    EX 
                                                </c:if>
                                                <c:if test="${tripDetails.movementType == 13 || tripDetails.movementType == 14 }">
                                                    IM 
                                                </c:if>
                                                <c:if test="${tripDetails.movementType == 15 }">
                                                    IC 
                                                </c:if>/-->
                                                <c:if test="${tripDetails.movementType != '16' }">
                                                    <c:out value="${tripDetails.lrNumber}"/>
                                                </c:if>
                                                <c:if test="${tripDetails.movementType == '16' }">
                                                    <c:out value="${tripDetails.manualLrNumber}"/>
                                                </c:if>
                                                <!--/ 2017-18&nbsp;-->
                                            </td>
                                        </c:if>

                                        <c:if test = "${pnrUser != 1}" > 
                                            <c:if test="${tripDetails.movementType == 1  || tripDetails.movementType == 2 
                                                          || tripDetails.movementType == 17  || tripDetails.movementType == 18 
                                                          || tripDetails.movementType == 19  || tripDetails.movementType == 20  
                                                          || tripDetails.movementType == 21  || tripDetails.movementType == 23  
                                                          || tripDetails.movementType == 24 || tripDetails.movementType == 25 
                                                          || tripDetails.movementType == 26 || tripDetails.movementType == 27 
                                                          || tripDetails.movementType == 28 || tripDetails.movementType == 29 
                                                          || tripDetails.movementType == 30 || tripDetails.movementType == 31
                                                          || tripDetails.movementType == 32 || tripDetails.movementType == 33 
                                                          || tripDetails.movementType == 34 || tripDetails.movementType == 35 || tripDetails.movementType == 36
                                                  }">
                                                <td class="form-control" >
                                                    <c:out value="${tripDetails.containerDetail}"/>
                                                </td> 
                                            </c:if>
                                            <c:if test="${tripDetails.movementType == 14 || tripDetails.movementType == 16 }">
                                                <td class="form-control" >
                                                    <c:out value="${tripDetails.consignmentArticles}"/>
                                                </td> 
                                            </c:if>
                                        </c:if>
                                        <%--
                                            <c:if test = "${pnrUser != 1}" >                                            
                                                <td class="form-control" >
                                                    <c:out value="${tripDetails.tripAdvance}"/>
                                                </td>
                                            </c:if>
                                            <c:if test = "${pnrUser != 1}" >                                            
                                                <td class="form-control" >
                                                   Liters: <c:out value="${tripDetails.fuelLtr}"/> <br> 
                                                   Amount: <c:out value="${tripDetails.fuelAmount}"/> <br> 
                                                   Bunk: <c:out value="${tripDetails.fuelBunk}"/>
                                                </td>


                                            </c:if>
                                        --%>
                                        <c:if test = "${pnrUser != 1}" >                                            
                                            <td class="form-control" >
                                                <c:out value="${tripDetails.tripScheduleDate}"/>
                                            </td>
                                        </c:if>
                                        <td class="form-control" align="center">

                                            <c:out value="${tripDetails.movementTypeName}"/>
                                        </td>
                                        <td class="form-control" ><c:out value="${tripDetails.vehicleVendor}"/></td>
                                        <td class="form-control" >
                                            <c:if test = "${pnrUser == 1}" >
                                                <c:out value="${tripDetails.portName}"/>
                                            </c:if>
                                            <c:if test = "${pnrUser != 1}" >
                                                <c:out value="${tripDetails.routeInfo}"/>
                                            </c:if>
                                        </td>

                                        <td class="form-control" ><c:out value="${tripDetails.driverName}"/><br/><c:out value="${tripDetails.mobileNo}"/><font color="red"><c:out value="${tripDetails.oldDriverName}"/></font></td>
                                        <td class="form-control" ><c:out value="${tripDetails.vehicleTypeName}"/></td>
                                        <c:if test = "${pnrUser == 1}" >
                                            <td class="form-control" >
                                                <input type="hidden" name="containerQuantity" id="containerQuantity" value="<c:out value="${tripDetails.containerQuantity}" />">
                                                <c:if test = "${tripDetails.containerQuantity == 1}" >
                                                    1 * 40 
                                                </c:if> 
                                                <c:if test = "${tripDetails.containerQuantity == 2}" >
                                                    1 * 20 
                                                </c:if> 
                                                <c:if test = "${tripDetails.containerQuantity == 3}" >
                                                    2 * 20 
                                                </c:if> 
                                            </td>
                                        </c:if>

                                        <td class="<%=className%>"  align="left"> 
                                            <c:if test="${tripDetails.statusId == 10 }"> <c:out value="${tripDetails.subStatusId}" /> </c:if>
                                            <c:if test="${tripDetails.statusId != 10}"> 
                                                <c:out value="${tripDetails.status}" /> 
                                            </c:if>

                                        </td>   
                                        <c:if test = "${userId != '1831'}" >
                                            <c:if test="${statusId == 10}">
                                                <td class="<%=className%>"  align="left">
                                                    <c:if test="${tripDetails.gpsLocation == 'NA'}">
                                                        NA
                                                    </c:if>
                                                    <c:if test="${tripDetails.gpsLocation != 'NA'}">
                                                        <a href="#" onClick="viewCurrentLocation('<c:out value="${tripDetails.vehicleId}" />');"><c:out value="${tripDetails.gpsLocation}" /></a> 
                                                    </c:if>
                                                </td>
                                            </c:if>

                                            <td class="form-control" >
                                                <c:if test="${tripDetails.statusId < 13}">
                                                    <c:if test="${roleId != '1030'}">
                                                        <c:if test="${(RoleId == 1023 || RoleId == 1069 || RoleId == 1068) && tripDetails.statusId != 10 && tripDetails.statusId < 12}">
                                                            <c:if test="${tripDetails.statusId != 8 && tripDetails.statusId < 12}">
                                                                <c:if test="${subStatusId == 0}">
            <!--                                                    <a href="#" onclick="changeVehicleAfterTripStart('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);">ChangeVehicle</a>-->
                                                                    <a href="#" onclick="changeVehicleAfterTripStart('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}"/>', '<c:out value="${tripType}"/>', 1);" style="color:#272E39;"   >
                                                                        ChangeVehicle
                                                                    </a>
                                                                </c:if>
                                                            </c:if>
                                                        </c:if>

                                                        <c:if test="${tripDetails.emptyTripApprovalStatus == 0}">
                                                            Empty Trip Waiting For Approval
                                                        </c:if>

                                                        <c:if test="${tripDetails.emptyTripApprovalStatus == 1}">


                                                            <c:if test="${tripDetails.statusId == 6}">
                                                                <a href="viewTripSheetForVehicleAllotment.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentOrderNos=<c:out value="${tripDetails.consignmentId}"/>&customerName=<c:out value="${tripDetails.customerName}"/>">AllotVehicle</a>


                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId == 7}">
                                                                <c:if test="${RoleId != 1033}">
                                                                    <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Freeze</a>
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId == 8 }">
                                                                <c:if test="${RoleId != 1033}">
                                                                    <c:if test="${tripDetails.nextTrip != 2}">
        <!--                                                                <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">UnFreeze</a>-->
                                                                        <c:if test="${tripDetails.movementType == 7 && tripDetails.statusId != 8 }">
                                                                            <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                                UnFreeze
                                                                            </a>
                                                                        </c:if>
                                                                        <c:if test="${tripDetails.movementType != 7 && tripDetails.statusId == 8 }">
                                                                            <a href="viewTripFreezeUnFreeze.do?tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  style="color:#272E39;"   >
                                                                                UnFreeze
                                                                            </a>
                                                                        </c:if>
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.nextTrip == 0 && tripDetails.nextTripCountStatus == 0}">
                                                                    <c:if test="${tripDetails.tripCountStatus == 0}">
        <!--                                                                1&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Start</a>-->
                                                                        <%--<c:if test="${tripDetails.movementType == 7 && tripDetails.statusId != 8 }">--%>
                                                                        <c:if test="${tripDetails.movementType != 7 }">
                                                                            <br>
                                                                            <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&vendorId=<c:out value="${tripDetails.vendorId}"/>"  style="color:#272E39;"   >
                                                                                <font color="gray">  StartTrip </font>
                                                                            </a><br>
                                                                        </c:if>
                                                                        <c:if test="${tripDetails.movementType == 7 }">
                                                                            <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>
                                                                        </c:if>    
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.nextTrip != 0 }">
                                                                    <c:if test="${tripDetails.tripCountStatus == 0}">
                                                                        &nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&vendorId=<c:out value="${tripDetails.vendorId}"/>">StartTrip</a>
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.tripCountStatus == 1 && tripDetails.vehicleId == 0}">
                                                                    3&nbsp; <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&vendorId=<c:out value="${tripDetails.vendorId}"/>">Start</a>
                                                                </c:if>

                                                                <c:if test="${tripDetails.tripCountStatus == 1 && tripDetails.vehicleId != 0}">
                                                                    &nbsp;Started
                                                                    <c:if test="${tripDetails.nextTrip == 0 && tripDetails.nextTripCountStatus == 0}">
                                                                        &nbsp;<br> <a href="confirmNextTrip.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Next Trip</a>
                                                                    </c:if>
                                                                    <c:if test="${tripDetails.nextTrip == 1 && tripDetails.nextTripCountStatus != 0}">
                                                                        &nbsp;<br> 
        <!--                                                                <a href="manualfinanceadvice.do?nextTrip=1&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>-->


<!--                                                                <a href="manualfinanceadvice.do?nextTrip=1&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:#272E39;"   >
                                                                   Request Advance
                                                                 </a>-->
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${RoleId == 1023 || RoleId == 1033}">

                                                                    <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                            <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                                        </c:if>
                                                                    </c:if>
                                                                </c:if>

                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId >= 10 && vendorType == 0 && tripDetails.vehicleCount == 1}">
                                                            <%--<c:if test="${tripDetails.statusId >= 10 && tripDetails.vehicleCount == 1}">--%>
                                                                <a href="#" style="color:green"  onclick="viewTripStartPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />')" >
                                                                    <br>TripSheetPrint <br>
                                                                </a> 
                                                            </c:if>
                                                                            
                                                            <c:if test="${tripDetails.statusId == 10}">
                                                                <c:if test="${tripDetails.movementType != 1 && tripDetails.movementType != 2 }">
                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>
                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId > 12}">

                                                                <c:if test="${tripDetails.movementType == 1 || tripDetails.movementType == 2 }">
                                                                    <a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>"  onclick="return !window.open(this.href, 'somesite', 'width=500,height=500')"
                                                                       target="_blank" >  </a>

<!--<a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">StatusUpdate</a>-->                
                                                                </c:if>
                                                            </c:if>

                                                            <c:if test="${tripDetails.statusId == 10}">

<!--                                                        <a href="#" style="color:green"  onclick="viewTripStartPrint('<c:out value="${tripDetails.tripSheetId}"/>','<c:out value="${tripDetails.vehicleId}" />')" >
<br>  Trip Start Print
</a> -->
                                                                <%--
                                                                <c:if test="${tripDetails.emptyTrip == 0}">
                                                                    <c:if test="${subStatusId == 0}">
                                                                            <a href="viewWFUTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">WFU</a>
                                                                    </c:if>
                                                                </c:if>
                                                                --%>
                                                                <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                                    &nbsp;
                                                                </c:if>

                                                                <%--<c:if test="${tripDetails.routeNameStatus > '2'}">--%>
                                                                    <!--<a href="viewTripRouteDetails.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">InterimUpdate</a>-->
                                                                <%--</c:if>--%>

                                                                <c:if test="${tripDetails.paymentType == 3 && tripDetails.advanceToPayStatus == 0}">
                                                                    <c:if test="${subStatusId == 0}">
                                                                        &nbsp;
                                                                        <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>
                                                                    </c:if>
                                                                </c:if>
 &nbsp;
                                                                            <!--<a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>-->
                                                                <c:if test="${tripDetails.paymentType == 4 && tripDetails.advanceToPayStatus == 0}">
                                                                    <c:if test="${subStatusId == 0}">
                                                                        <c:if test="${tripDetails.tripSubStatus == 39}"> 
                                                                            &nbsp;
                                                                            <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>
                                                                        </c:if>
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.lastPointId == 617}">
                                                                    <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                                        &nbsp;
        <!--                                                                <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&movementType=<c:out value="${tripDetails.movementType}" />">closure3</a>-->
                                                                        <a href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&movementType=<c:out value="${tripDetails.movementType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>" style="color:#272E39;"   >
                                                                            Trip Closure
                                                                        </a>

                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${tripDetails.lastPointId != 617}">
                                                                    <c:if test="${tripDetails.paymentType == 1 || tripDetails.paymentType == 2 || tripDetails.paymentType == 0 }">
                                                                        <c:if test="${tripDetails.tripSubStatus == 39}"> 
                                                                            &nbsp;
                                                                            <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a><br>
                                                                        </c:if>
                                                                    </c:if>
                                                                </c:if>
    <!--                                                        <a href="#" style="color:green"  onclick="viewTripStartPrint('<c:out value="${tripDetails.tripSheetId}"/>','<c:out value="${tripDetails.vehicleId}" />')" >
                                                                <br>  Trip Start Print
                                                            </a> -->

                                                                <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1041 || RoleId == 1032}">

                                                                    <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1003}">
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                            <a href="payManualfinanceadvice.do?tripid=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">pay</a><br>
                                                                        </c:if>
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                                            <!--<b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance4</a>-->
                                                                        </c:if>
                                                                    </c:if>
                                                                    <c:if test="${tripDetails.tripType == 2 && tripDetails.fuelTypeId == 1002}">
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) > 1}">
                                                                            <!--&nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance2</a>-->
                                                                        </c:if>
                                                                        <c:if test="${ (tripDetails.orderExpense - tripDetails.actualAdvancePaid) < 1}">
                                                                            <!--<b>rcmpaid</b> &nbsp;<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance3</a>-->
                                                                        </c:if>
                                                                    </c:if>

                                                                    <c:if test="${tripDetails.tripType != 2}">
        <!--                                                                <a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance1</a>-->
        <!--                                                                <a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:#272E39;"   >
                                                                           Request Advance
                                                                         </a>-->
                                                                    </c:if>
                                                                    <%--<c:if test="${tripDetails.statusId >= 10 }">--%>
                                                             <!--<a href="#" style="color:green"  onclick="viewTripStartPrint('<c:out value="${tripDetails.tripSheetId}"/>','<c:out value="${tripDetails.vehicleId}" />')" >-->
                                                                    <!--<br>  Trip Start Print-->
                                                                    <!--</a>--> 
                                                                    <%--</c:if>--%>

                                                                </c:if>
                                                            </c:if>
                                                            <c:if test="${tripDetails.statusId == 18}">
                                                                <c:if test="${tripDetails.tripSubStatus == 39}"> 
                                                                    &nbsp;
                                                                    <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a> <br>                                                           

                                                                    &nbsp;
                                                                    <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1032}">
                                                                        <!--<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>-->
                                                                    </c:if>
                                                                </c:if>
                                                            </c:if>

                                                            <c:if test="${tripDetails.statusId == 12 && tripDetails.vehicleCount == 1 && tripDetails.offloadStatus != 2}">

                                                                <c:if test="${RoleId != 1033}">
                                                                    <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>&activeVehicle=Y"> Trip Closure</a>
                                                                    &nbsp;
                                                                </c:if>

                                                            </c:if>
                                                           


                                                            <c:if test="${tripDetails.statusId == 12 && tripDetails.vehicleCount != 1 && tripDetails.offloadStatus != 2}">

                                                                <c:if test="${RoleId != 1033}">
                                                                    <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Closure</a>
                                                                    &nbsp;
                                                                </c:if>

                                                            </c:if>

                                                        </c:if>

                                                    </c:if>
                                                </c:if>

                                                <!--starts offload--> 
                                               <c:set var = "clsrStatus" value = "${tripDetails.closureStatus}"/>
                                               <c:set var = "actStatus" value = "${tripDetails.actStatus}"/>
                                                <%--<c:out value="${tripDetails.offloadStatus}"/>  ---- <c:out value="${clsrStatus}"/> ---- <c:out value="${tripDetails.vehicleCount}"/>--%>
                                                <input type="hidden" id="closurStatus" name="closureStatus" value="<c:out value="${clsrStatus}"/>"/>
                                                
                                                <%--<c:forEach items="${tripDetails}" var="tripDetails">--%>
                                               
                                                 <c:forEach var="clrStatus" items="${clsrStatus}">
                                                   
                                                <c:if test="${tripDetails.tripSubStatus == 48 && tripDetails.offloadStatus == '2'}">
                                                    <c:if test="${tripDetails.vehicleCount == 1  && clrStatus == '0'}">
                                                        <c:if test="${RoleId != 1033}">
                                                            <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>&activeVehicle=Y"> Trip Closure</a>
                                                              &nbsp;
                                                        </c:if>
                                                    </c:if>
                                                    <c:if test="${tripDetails.vehicleCount == 1  && clrStatus == '1'}">
                                                        <c:if test="${RoleId != 1033}">
                                                            <br> <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>&activeVehicle=Y"> Trip Settlement</a>
                                                            &nbsp;
                                                            <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                            <br>  Settlement Print 
                                                           </a>
                                                        </c:if>
                                                    </c:if>
                                                    <c:if test="${tripDetails.vehicleCount == 1  && clrStatus == '2'}">
                                                        <c:if test="${RoleId != 1033}">
                                                            <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', '<c:out value="${tripDetails.actStatus}" />')" >
                                                            <br>  Settlement Print 
                                                           </a>
                                                        </c:if>
                                                    </c:if>
                                                    <c:if test="${tripDetails.vehicleCount == 2 && statusId == 12 && clsrStatus != '0,0'}">
                                                        <c:if test="${tripDetails.vehicleCount == 2 && statusId == 12 && clrStatus == '0'}">
                                                            <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Closure</a>
                                                            &nbsp;
                                                        </c:if>
                                                    </c:if>
                                                    <c:if test="${tripDetails.vehicleCount == 2 && statusId == 12 && clsrStatus != '1,1'}">
                                                        <c:if test="${tripDetails.vehicleCount == 2  && statusId == 13 && clrStatus == '1'}">
                                                            <c:if test="${RoleId != 1033}">
                                                                <br> <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Settlement</a>
                                                                &nbsp;
                                                            </c:if>
                                                        </c:if>
                                                    </c:if>
                                                </c:if>
                                                </c:forEach>
                                                   
                                                    <c:if test="${tripDetails.vehicleCount == 2 && statusId == 12 && clsrStatus == '0,0'}">
                                                            <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Closure</a>
                                                            &nbsp;
                                                    </c:if>          
                                                    <c:if test="${tripDetails.vehicleCount == 2 && statusId == 13 && clsrStatus == '1,1'}">
                                                            <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Settlement</a>
                                                            &nbsp;
                                                    </c:if>          
                                                   <c:if test="${tripDetails.vehicleCount == 2 && statusId == 13 && clsrStatus == '0,1'}">
                                                            <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Settlement</a>
                                                            &nbsp;
                                                    </c:if>    
                                                    <c:if test="${tripDetails.vehicleCount == 2 && statusId == 13 && clsrStatus == '1,0'}">
					                  <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Settlement</a>
					                  &nbsp;
                                                    </c:if>  
                                                    <c:if test="${tripDetails.vehicleCount == 2 && statusId == 13 && clsrStatus == '2,1'}">
					                  <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Settlement</a>
					                  &nbsp;
                                                    </c:if>  
                                                    <c:if test="${tripDetails.vehicleCount == 2 && statusId == 13 && clsrStatus == '1,2'}">
					                  <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Settlement</a>
					                  &nbsp;
                                                    </c:if> 
                                                          
<!--                                                    <c:if test="${tripDetails.vehicleCount == 1  && tripDetails.statusId == 14}">      
                                                        <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                       <br>  Settlement Print SSS
                                                      </a>
                                                    </c:if> -->
                                                          
                                                   <c:if test="${tripDetails.vehicleCount != 1}">
                                                        <c:if test="${RoleId != 1033}">
                                                            <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                            <br>  Settlement Print 
                                                            </a>
                                                            <a href="#" style="color:orange"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.oldVehicleId}" />', '<c:out value="${tripDetails.oldDriverId}" />', 'N')" >
                                                            <br>  Settlement Print 2
                                                            </a>
                                                        </c:if>
                                                    </c:if>
                                                   <c:if test="${ tripDetails.vehicleCount == 2 && tripDetails.offloadStatus == 2 && clrStatus == '0'}">
                                                        <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                            <br>  Settlement Print 
                                                        </a>
                                                        <a href="#" style="color:orange"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.oldVehicleId}" />', '<c:out value="${tripDetails.oldDriverId}" />', 'N')" >
                                                            <br>  Settlement Print 2
                                                        </a>
                                                    </c:if> 
                                                            
                                               
                                                <!--ends offload
                                                <c:if test="${tripDetails.statusId >= 13}">
                                                    
                                                    <c:if test="${tripDetails.statusId == 13 && tripDetails.vehicleCount == 1 && tripDetails.offloadStatus != 2}">
                                                        <br> <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>&activeVehicle=Y"> Trip Settlement</a>
                                                        &nbsp;
                                                    </c:if>
                                                    <%--<c:if test="${tripDetails.statusId == 13  && tripDetails.oldVehicleId !=  null &&  tripDetails.oldVehicleId !=  '' }">--%>
                                                    <c:if test="${tripDetails.statusId == 13  && tripDetails.vehicleCount != 1 && tripDetails.offloadStatus != 2}">
                                                        <br> <a href="viewVehicleTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&orderType=<c:out value="${tripDetails.orderType}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>"> Trip Settlement</a>
                                                        &nbsp;
                                                    </c:if>     
                                                    <c:if test="${tripDetails.statusId >= 13 && tripDetails.vehicleCount == 1 && tripDetails.offloadStatus != 2}">
                                                        <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                            <br>  Settlement Print 
                                                        </a>
                                                    </c:if>
                                                    <%--<c:if test="${tripDetails.statusId == 13  && tripDetails.oldVehicleId !=  null &&  tripDetails.oldVehicleId !=  '' }">
                                                    <c:if test="${tripDetails.statusId >=13  && tripDetails.vehicleCount == 1 && tripDetails.offloadStatus != 2}">
                                                        <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                            <br>  Settlement Print 
                                                        </a>
                                                        <a href="#" style="color:orange"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.oldVehicleId}" />', '<c:out value="${tripDetails.oldDriverId}" />', 'N')" >
                                                            <br>  Settlement Print 2
                                                        </a>
                                                    </c:if>     --%>
<!--                                                   <c:if test="${statusId == 12  && tripDetails.vehicleCount == 2 && tripDetails.offloadStatus == 2 && clrStatus == '0'}">
                                                        <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                            <br>  Settlement Print 
                                                        </a>
                                                        <a href="#" style="color:orange"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.oldVehicleId}" />', '<c:out value="${tripDetails.oldDriverId}" />', 'N')" >
                                                            <br>  Settlement Print 2
                                                        </a>
                                                    </c:if>    -->
                                                       <c:if test="${statusId == 13 && tripDetails.vehicleCount == 1 && tripDetails.offloadStatus != 2}">

                                                                <c:if test="${RoleId != 1033}">
                                                                   <br> <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=13&admin=<c:out value="${admin}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>&activeVehicle=Y"> Trip Settlement</a>
                                                                    &nbsp;
                                                                </c:if>
                                                                  <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                            <br>  Settlement Print 
                                                        </a>
                                                            </c:if>
                                                       <c:if test="${statusId == 14 && tripDetails.vehicleCount == 1 && tripDetails.offloadStatus != 2}">
                                                            <br> <a class="clsr" href="viewTripExpense.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=14&admin=<c:out value="${admin}" />&moTypeName=<c:out value="${tripDetails.movementTypeName}"/>&activeVehicle=Y"> Settled Trips</a>
                                                             &nbsp;
                                                            <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                            <br>  Settlement Print 
                                                            </a>
                                                       </c:if>
                                                </c:if>  
                                                 
                                                        
                                                        
                                                <c:if test="${tripDetails.statusId == 13 }">
                                                    <!--<a href="viewTripSettlement.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&vehicleId=<c:out value="${tripDetails.vehicleId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">Settlement</a>-->
                                                </c:if>
<!--                                                <a href="handleTripFuel.do?nextTrip=0&tripId=<c:out value="${tripDetails.tripSheetId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>" style="color:violet;"   >
                                                    FuelUpdate 
                                                </a>-->

<!--                                            <a href="#" style="color:#ff6699;"  onclick="printTripGrnNewFcl('<c:out value="${tripDetails.tripSheetId}"/>')" >
                                                    LRPrint
                                                </a> -->

                                                <%-- <c:if test="${tripDetails.statusId == 14 && tripDetails.offloadStatus == 0}">
                                                                           
                                                     <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                         <br>  Settlement Print 
                                                     </a>

                                                        <a href="#" style="color:orange"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.oldVehicleId}" />', '<c:out value="${tripDetails.oldDriverId}" />', 'N')" >
                                                            <br>  Settlement Print 2
                                                        </a>
                                                </c:if> --%> 

                                                <c:if test="${RoleId == '1023' }">
                                                    <c:if test="${tripDetails.statusId >= 8 && tripDetails.statusId <= 14}">
                                                        <br>

                                                    </c:if>
                                                </c:if>
                                                <c:if test="${tripDetails.statusId == 18}">

                                                    <c:if test="${tripDetails.tripSubStatus == 39}"> 
                                                        <a href="viewEndTripSheet.do?tripSheetId=<c:out value="${tripDetails.tripSheetId}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>&consignmentId=<c:out value="${tripDetails.consignmentId}"/>&movementType=<c:out value="${tripDetails.movementType}"/>&containerQuantity=<c:out value="${tripDetails.containerQuantity}"/>">End</a> <br>                                                           
                                                    </c:if>

                                                    <c:if test="${RoleId == 1023 || RoleId == 1033 || RoleId == 1032}">
                                                        <!--<a href="manualfinanceadvice.do?nextTrip=0&tripid=<c:out value="${tripDetails.tripSheetId}"/>&consignmentOrderId=<c:out value="${tripDetails.consignmentId}"/>&tripCode=<c:out value="${tripDetails.tripCode}"/>&tripType=<c:out value="${tripType}"/>&statusId=<c:out value="${statusId}"/>">RequestAdvance</a>-->
                                                    </c:if>
                                                </c:if>


                                                <c:if test="${tripDetails.statusId >= 13}">
                                                    <div id="porints" style="display:none">
                                                    </c:if>       
                                                    <c:if test="${subStatusId == 0}">
                                                        <c:if test="${ tripDetails.statusId != 12 && tripDetails.statusId != 13 &&  tripDetails.statusId != 14 }">

                                                            <a href="#" style="color:#3399ff;" onclick="printTripFuel('<c:out value="${tripDetails.tripSheetId}"/>')"   >
                                                                FuelPrint
                                                            </a>
                                                        </c:if>
                                                        <c:if test="${tripDetails.statusId >= 13  && vendorType == 0}">
                                                            <a href="#" style="color:green"  onclick="viewSettlementPrint('<c:out value="${tripDetails.tripSheetId}"/>', '<c:out value="${tripDetails.vehicleId}" />', '<c:out value="${tripDetails.driverId}" />', 'Y')" >
                                                                <br>  Settlement Print
                                                            </a> 
                                                        </c:if> 
        <!--                                                        <a href="#" style="color:green;" onclick="printTripAdvance('<c:out value="${tripDetails.tripSheetId}"/>')"  >
                                                                    AdvancePrint
                                                                </a>-->
                                                    </div>
                                                </td>
                                            </c:if>
                                        </c:if>

                                    </tr>

                                    <%index++;%>
                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="loader"></div>

                    <div id="controls"  >
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5"  selected="selected" >5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>

                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $(".clsr").click(function () {

                                document.getElementById("spinner").style.display = "block";
                                document.getElementById("loader").style.display = "block";
                                document.getElementById('loader').className += 'ui-loader-background';
                            });
                        });
                        function GenTripGr(tripSheetId) {
                            $.ajax({
                                url: "/throttle/handleTripGRGeneration.do",
                                dataType: "text",
                                data: {
                                    tripId: tripSheetId

                                },
                                success: function (temp) {
                                    // alert(data);
                                    if (temp != '') {
                                        alert("GR Generated Successfully!!");

                                    } else {
                                        alert('Something Went wrong! Please check.');
                                    }
                                }
                            });

                        }
                        function printTripGrn(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripGrnNew(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1', '&moffusilPrint=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripGrnNewLcl(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1' + '&moffusilPrint=1' + '&multiplePrint=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripGrnNewFcl(tripSheetId) {
                            window.open('/throttle/handleTripSheetPrint.do?tripSheetId=' + tripSheetId + '&value=1' + '&moffusilPrint=1' + '&lclMultiPrint=1', 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                        function printTripChallan(tripSheetId) {
                            window.open('/throttle/handleTripChallanPrint.do?tripSheetId=' + tripSheetId, 'PopupPage', 'height = 800, width = 1200, scrollbars = yes, resizable = yes');
                        }
                    </script>

                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>

