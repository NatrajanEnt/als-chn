<%-- 
    Document   : tripPlanningImport
    Created on : Nov 4, 2013, 11:17:44 PM
    Author     : Arul
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<!--<html>-->
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <title></title>
    <script>
        function submitPage() {
            document.tripPlanning.action = '/throttle/handleUploadDATfile.do';
            document.tripPlanning.submit();
        }
    </script>
</head>
<% String menuPath = "Operations >>  Upload Trip Planning";
    request.setAttribute("menuPath", menuPath);
    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
    Date date = new Date();
    String curDate = dateFormat.format(date);
%>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.PNR Upload" text="PNR Upload"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="hrms.label.Sales" text="Sales"/></a></li>
            <li class=""><spring:message code="hrms.label.PNR Upload" text="PNR Upload"/></li>

        </ol>
    </div>
</div>

<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                <form name="tripPlanning" method="post" enctype="multipart/form-data">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <br>
                    <br>
                    <table class="table table-info mb30 table-hover" id="bg"   >
                        <thead>
                            <tr>
                                <th colspan="4" height="30" >Upload PNR Files</th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table table-info mb30 table-hover"  align="center"  >
                        <tr>
                            <td  colspan="4">Upload PNR Data</td>
                        </tr>
                        <tr>
                            <td >Select file</td>
                            <td ><input type="file" name="importCnote" id="importCnote" class="form-control" ></td>
<!--                            <td >Planning Date</td>
                            <td ><input type="text" name="planDate" id="planDate" class="form-control" style="width:240px;height:40px" value="<%=curDate%>" readonly></td>-->
                        </tr>
                        <tr >
                            <td align="center"  colspan="4"><input type="button" class="btn btn-success" value="Submit" name="Search" onclick="submitPage()">
                        </tr>
                    </table>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
