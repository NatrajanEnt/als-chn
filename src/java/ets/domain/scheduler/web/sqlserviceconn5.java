package ets.domain.scheduler.web;

import ets.domain.report.business.ReportTO;
import ets.domain.thread.web.SendEmail;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

class sqlserviceconn5 extends Thread {

    String TallyCompId = "";
    String TripNo = "";
    String transactionType = "";
    String tranNo = "";
    String throttleTranId = "";
    String transactionAmount = "";

    public sqlserviceconn5(ReportTO reportTO) {

        TallyCompId = reportTO.getTallyCmpId();
        TripNo = reportTO.getTripNos();
        transactionType = reportTO.getTransactionType();
        tranNo = reportTO.getTranNo();
        throttleTranId = reportTO.getThrottleTranId();
        transactionAmount = reportTO.getTransactionAmount();

    }

    public void run() {
        int updateStatus = 0;
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        String sqlQuery = "";
        try {

            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver").newInstance();
            conn = DriverManager.getConnection("jdbc:sqlserver://123.136.170.227:1433;ABCTEST=company", "sa", "triway");

            if (conn != null) {
                System.out.println("Database Successfully connected");

                sqlQuery = "INSERT INTO [ABCTEST].[dbo].[AccountTransationDetails](TallyCmpId,TripNo,TransactionType,TranNo,ThrottleTrnNameId,TransAmount) VALUES "
                        + "(?, ?, ?, ?, ?, ?)";

                stmt = conn.prepareStatement(sqlQuery);

                System.out.println("TallyCompId==========" + TallyCompId);
                System.out.println("TripNo==========" + TripNo);
                System.out.println("transactionType==========" + transactionType);
                System.out.println("tranNo==========" + tranNo);
                System.out.println("throttleTranId==========" + throttleTranId);
                System.out.println("transactionAmount==========" + transactionAmount);

                stmt.setString(1, TallyCompId);
                stmt.setString(2, TripNo);
                stmt.setString(3, transactionType);
                stmt.setString(4, tranNo);
                stmt.setString(5, throttleTranId);
                stmt.setString(6, transactionAmount);

                updateStatus = stmt.executeUpdate();
//                System.out.println("UPDATE = " + updateStatus);

//                while (rs.next()) {
//                    tripNo = rs.getString("TripNo");
//                }
//                System.out.println("tripNo==========" + tripNo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
              if (conn != null) {            
                  try {
                      conn.close();
                  } catch (SQLException ex) {
                      Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                  }
              }
            }
        if (updateStatus != 0) {
//        if (!"".equals(updateStatus) && "0".equals(updateStatus)) {

            String fileNameNew = "jdbc_url.properties";
            Properties dbProps = new Properties();
            InputStream is = getClass().getResourceAsStream("/" + fileNameNew);
            try {
                dbProps.load(is);//this may throw IOException
            } catch (IOException ex) {
                Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
            }
            String dbClassName = dbProps.getProperty("jdbc.driverClassName");
            String dbUrl = dbProps.getProperty("jdbc.url");
            String dbUserName = dbProps.getProperty("jdbc.username");
            String dbPassword = dbProps.getProperty("jdbc.password");
            int status = 0;
            Connection con = null;
            try {
                Class.forName(dbClassName).newInstance();
                con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
                try {
                    PreparedStatement updateMailDetails = null;
                    String sql = "UPDATE AccountTransationDetails SET \n"
                            + " throttle_postind = 1"
                            + " WHERE  TranNo = ?";
                    updateMailDetails = con.prepareStatement(sql);
                    updateMailDetails.setString(1, tranNo);
                    status = updateMailDetails.executeUpdate();
                    System.out.println("NEW CHANGES = " + status);

                } catch (SQLException e) {
                    e.printStackTrace();
                    System.out.println("TripNo doesn't exist.");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }finally{
              if (con != null) {            
                  try {
                      con.close();
                  } catch (SQLException ex) {
                      Logger.getLogger(sqlserviceconn.class.getName()).log(Level.SEVERE, null, ex);
                  }
              }
            }
        }
    }
}
