<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : RAM
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <title>PrintTripSheet</title>
    </head>
    <body>
        <style type="text/css">
            @media print {
                table {
                    page-break-inside: auto;
                }
            }  
        </style>
        <script>

            window.onunload = function() {
                window.opener.location.reload();
            }
        </script>
        <form name="enter" method="post">
            <!--            <style type="text/css">
                            #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
                        </style>-->
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            String billingPartyId = (String)request.getAttribute("billingPartyId");
            %>
            
            <div id="printContent" class="page-break">
                <c:forEach items="${tripDetailsListLCL}" var="tripStatus">
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                 <table align="center" style="height: 151px; width: 700px;" border="1"><caption>&nbsp;</caption>
<!--                <table align="center"  border="1" style="border-collapse: collapse;width:400px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                       background:url(images/dict-logoWatermark.png);
                       background-repeat:no-repeat;
                       background-position:center;
                       border:1px solid;
                       /*border-color:#CD853F;width:800px;*/
                       border:1px solid;
                       /*opacity:0.8;*/
                       /*font-weight:bold;*/
                       ">-->
                    <tbody>
                        <tr style="height: 26px;">
                            <td align="center" style="width: 663px; height: 26px;" colspan="3"><br>
                                <font size="3">&nbsp;&nbsp;<b>GOODS RECEIPT</b></font><br><br>
                            </td>
                        </tr>
                        <tr >

                            <td  colspan="2">&nbsp;
                                <font size="2">&nbsp;&nbsp;<b>LR NO & DATE :</b></font><font size="2">MAA/<c:out value="${tripStatus.consignmentId}"/>/2017-18&nbsp; & &nbsp;<c:out value="${tripDate}"/></font>
                            </td>

                            <td align="center" style="width: 295px; height: 54px;" rowspan="3">
                               <font size="2"><b>Route Logistics India Pvt.Ltd.</b></font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <img src="images/Route Logo.JPG" width="100" height="50"/>
                            </td>

                        </tr>

                        <tr style="height: 18px;">
                            <td style="width: 368px; height: 18px;" colspan="2">&nbsp;
                                <font size="2">&nbsp;&nbsp;<b>BOOKING NO :</b></font><font size="2">&nbsp;&nbsp;<c:out value="${tripStatus.tripCode}"/></font>
                               &nbsp; &nbsp;<font>Job No :&nbsp;&nbsp;<c:out value="${tripStatus.customerOrderReferenceNo}"/></font>
                            </td>
                        </tr>

                        <tr style="height: 18px;">
                            <td style="width: 368px; height: 18px;" colspan="2">&nbsp;
                                <font size="2">&nbsp;&nbsp;<b>MOVEMENT TYPE:</b></font><font size="2">&nbsp;&nbsp;<c:out value="${orderType}"/></font>
                            </td>
                        </tr>

                        <tr style="height: 102px;">
                            <td style="width: 184px; height: 102px;">&nbsp;
                                <font size="2">&nbsp;&nbsp;<b>Consignor</b></font><br> <font size="2"> &nbsp;&nbsp;<c:out value="${tripStatus.consignorName}"/>,<br>&nbsp;&nbsp;<c:out value='${tripStatus.consignorAddress}'/>.</font>  <br>
                            </td>

                            <td style="width: 184px; height: 102px;">&nbsp;
                                <font size="2">&nbsp;&nbsp;<b>Consignee</b></font><br><font size="2">&nbsp;&nbsp;<c:out value="${tripStatus.consigneeName}"/>,<br>&nbsp;&nbsp;<c:out value="${tripStatus.consigneeAddress}"/>.</font><br>
                            </td>

                            <td style="width: 295px; height: 102px;">&nbsp;
                                <font size="2">&nbsp;&nbsp;<b>Billing Party</b></font><br><font size="2">&nbsp;&nbsp;<c:out value="${tripStatus.consigneeName}"/>,<br>&nbsp;&nbsp;<c:out value="${tripStatus.consigneeAddress}"/>.</font><br>
<!--                                <font size="2">&nbsp;&nbsp;<b>Delivery At</b></font><br><font size="2">&nbsp;&nbsp;<c:out value="${deliveryAddress}"/>.</font><br>-->
                            </td>
                        </tr>

                        <tr style="height: 83px;">
                            <td style="width: 663px; height: 83px;" colspan="3">&nbsp;
                                <font size="2"><b>Cargo Type:</b></font>
                                <c:if test="${tripStatus.uom == 1}">
                                <font size="2">&emsp;Box</font>
                                </c:if>
                                <c:if test="${tripStatus.uom == 2}">
                                <font size="2">&emsp;Bag</font>
                                </c:if>
                                <c:if test="${tripStatus.uom == 3}">
                                <font size="2">&emsp;Pallet</font>
                                </c:if>
                                <c:if test="${tripStatus.uom == 4}">
                                <font size="2">&emsp;Each</font>
                                </c:if>
                                <c:if test="${tripStatus.uom == 5}">
                                <font size="2">&emsp;Package</font>
                                </c:if>
                                
                                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&nbsp;<b>Containers :</b></font><font size="2">&emsp;&emsp;<c:out value='${containerNo}'/>&emsp;<c:out value='${containerNo1}'/></font><br><br>
                                &nbsp;<font size="2"><b>pkgs/wgt/CBM</b></font><font size="2">&emsp;<c:out value="${tripStatus.packageNos}"/>/ <c:out value="${tripStatus.weight}"/> /<c:out value="${tripStatus.volumeDocNo}"/></font>
                                &emsp;&emsp;&emsp;&emsp;&emsp;<b>Seal No/Weight:</b></font><font size="2">&emsp;<c:out value="${sealNo}"/><b>/</b></font><br><br>
                                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<font size="2">&emsp;<b>E-Way Bill No :</b></font><font size="2">&emsp;<c:out value="${tripStatus.ewayBillNo}"/></font><br><br>
                                &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;<font size="2">&emsp;<b>Route :</b></font><font size="2">&emsp;<c:out value='${tripStatus.routeInfo}'/></font><br><br>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 184px; height: 96px;" rowspan="6">&nbsp;
                                <font size="2">&nbsp;<b>Booking Office</b></font><br><font size="2">&nbsp; CHENNAI ROUTE LOGISTICS <br>&nbsp;&nbsp;(I) P LTD</font><br><br>
                                <font size="2">&nbsp;<b>Contact :</b></font><font size="2">&emsp; Mr.Siva (89398 44114)</font><br>
                                <font size="2">&nbsp;<b>Email :</b></font><font size="2"> &emsp; routeteam@triway.in</font><br>
                            </td>

                            <td style="width: 184px; height: 96px;" rowspan="6">&nbsp;
                                <font size="2">&nbsp;&nbsp;<b>Delivery At</b></font><br><font size="2">&nbsp;&nbsp;<c:out value="${deliveryAddress}"/>.</font><br>
                                <!--<font size="2">&nbsp;<b>Delivery Office</b></font><br><font size="2">&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;</font><br>-->
                                <br><br><br><br>
                            </td>

                            <td style="width: 295px; height: 16px;">
                                <font size="2"><b>&nbsp;&nbsp;Transport :</b></font><font size="2">&emsp;<c:out value='${transport}'/></font>
                            </td>
                        </tr>

                        <tr style="height: 16px;">
                            <td style="width: 295px; height: 16px;">
                                <font size="2"><b>&nbsp;&nbsp;Vehicle No :</b></font><font size="2">&emsp;<c:out value='${vehicleNo}'/></font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 295px; height: 16px;">
                                <font size="2"><b>&nbsp;&nbsp;Vehicle Type :</b></font><font size="2">&emsp;<c:out value='${vehicleType}'/></font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 295px; height: 16px;">
                                <font size="2"><b>&nbsp;&nbsp;Driver :</b></font><font size="2">&emsp;<c:out value='${driverName}'/></font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 295px; height: 16px;">
                                <font size="2"><b>&nbsp;&nbsp;Contact :</b></font><font size="2">&emsp;<c:out value='${driverMobile}'/></font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 295px; height: 16px;">
                                <font size="2"><b>&nbsp;&nbsp;DL NO :</b></font><font size="2">&emsp;<c:out value='${licenceNo}'/></font>
                            </td>
                        </tr>

                        <tr style="height: 18px;">
                            <td style="width: 368px; height: 18px;" colspan="2">
                                <font size="2">&nbsp;&nbsp;<b>List of Document</b></font>
                            </td>
                            <td style="width: 295px; height: 18px;">
                                <font size="2">&nbsp;&nbsp;<b>Vessel Name:</b></font><font size="2">&emsp;<c:out value='${vesselName}'/></font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 368px; height: 48px;" colspan="2" rowspan="3">
                                <font size="2">&nbsp;&nbsp;Invoice,&emsp; Packing List,&emsp; Bill of Entry, Form KK</font><br>
                            </td>
                            <td style="width: 295px; height: 16px;">
                                <font size="2">&nbsp;&nbsp;<b>Party CST/TIN No:</b></font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 295px; height: 16px;">
                                <font size="2">&nbsp;&nbsp;<b>Value Of Goods(RS.):</b></font><font size="2">&emsp;<c:out value='${freightAmount}'/></font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 295px; height: 16px;">
                                <font size="2">&nbsp;&nbsp;<b>BE No:</b></font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 184px; height: 16px;">
                                <font size="2">&nbsp;&nbsp;<b>GST No:</b></font><font size="2">&nbsp;33AACCR6979E1Z6</font>
                            </td>
                            <td style="width: 184px; height: 48px;" rowspan="3" >&nbsp;Remarks:</td>
                            <td style="width: 295px; height: 48px;" rowspan="3" >&nbsp;</td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 184px; height: 16px;">
                                <font size="2">&nbsp;&nbsp;<b>PAN No:</b></font><font size="2">&nbsp;AACCR6979E </font>
                            </td>
                        </tr>
                        <tr style="height: 16px;">
                            <td style="width: 184px; height: 16px;">
                                <font size="2">&nbsp;&nbsp;<b>CINno:</b></font><font size="2">&nbsp;U63011TN2004PTC052290</font>
                            </td>
                        </tr>
                        <tr style="height: 26px;">
                            <td style="width: 663px; height: 26px;" colspan="3"><br>
                                <font size="2">&nbsp;&nbsp;<b>AT OWNER'S RISK:</b></font>
                                <font size="2">Goods to be insured by party at Owners Risk.
                                <font size="2">Not Responsible for Breakage,Damages,etc.
                                <font size="2">Subject to Chennai Jurisdiction Only.<br><br>
                            </td>
                        </tr>
                    </tbody>
                    <!--muthu-->
                </table>
            <br>
            <br>
            <br>
            <br>
            </c:forEach>
            
            </div>
            <center>
                <input type="button" class="button"  value="Back" onClick="viewTripDetails();" />
                <input type="button" class="button"  value="Print" onClick="print('printContent');" />
                &nbsp;&nbsp;&nbsp;
                <c:if test="${tripDetailsList != null}">
                    <c:forEach items="${tripDetailsList}" var="tripStatus">
                        <c:if test="${tripStatus.tripStatusId < 8}">
                            <a href="viewStartTripSheet.do?tripSheetId=<c:out value="${tripId}"/>&tripType=1&statusId=8"><input type="button" value="Start Trip"></a>
                            </c:if>
                        </c:forEach>
                    </c:if>

            </center>
            <br>
            <br>
            
        </form>
        <script type="text/javascript">
            function viewTripDetails() {
                document.enter.action = '/throttle/viewTripSheets.do?&statusId=10&tripType=1&admin=No';
                document.enter.submit();
            }
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            function get_object(id) {
                var object = null;
                if (document.layers) {
                    object = document.layers[id];
                } else if (document.all) {
                    object = document.all[id];
                } else if (document.getElementById) {
                    object = document.getElementById(id);
                }
                return object;
            }
            //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
            get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
            /* ]]> */
        </script>
    </body>
</html>