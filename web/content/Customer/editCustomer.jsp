<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ page import="ets.domain.util.ThrottleConstants" %>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<script type="text/javascript">


    $(document).ready(function() {
        $('#billingCustName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCustomerNameDetails.do",
                    dataType: "json",
                    data: {
                        custName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                        var value = ui.item.Name;
                        var tmp = value.split('~');
                        $('#billingCustId').val(tmp[0]);
                        $('#billingCustName').val(tmp[1]);
                        $('#stateId').val(tmp[2]);
                        $('#stateCode').val(tmp[3]);
                        $('#stateIdTemp').val(tmp[2]+'~'+tmp[3]);
                        $('#billingNameAddress').val(tmp[4]);
                        $('#panNo').val(tmp[5]);
                        $('#gstNo').val(tmp[6]);
                        return false;
                    } 
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('~');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


    function show() {
        var flag = document.getElementById("billingCustFlag").value;
//            alert("trtr="+flag);
        if (flag == 1) {
            $("#showme").hide();
            $("#showme1").hide();
        } else {
            $("#showme").show();
            $("#showme1").show();
        }
    }

    function setValues(val) {

        // alert("vsl....."+val);
        var temp = val.split("~");

        document.getElementById("stateId").value = temp[0];
        document.getElementById("stateCode").value = temp[1];
    }
</script>
<script type="text/javascript">
    function validatePanNo() {
        var nPANNo = document.getElementById("panNo").value;
        if (nPANNo != "") {
//            document.getElementById("panNo").value = nPANNo.toUpperCase();
//            var ObjVal = nPANNo;
//            var pancardPattern = /^([ABCFGHLJPT]{1})([A-Z]{2})([PCFHABTGL]{1})([A-Z]{1})(\d{4})([A-Z]{1})$/;
//            var patternArray = ObjVal.match(pancardPattern);
//            if (patternArray == null) {
//                alert("PAN Card No Invalid ");
//                return false;
//            } else {
            return true;
//            }
        } else {
            alert("Please enter pan no");
            return false;
        }
    }

    function validateGSTNo() {
        var nPANNo = document.getElementById("panNo").value;
        var gstNo = document.getElementById("gstNo").value;
        var stateCode = document.getElementById("stateCode").value;
        var tempSateCode = gstNo.substring(0, 2);
        var tempPanNo = gstNo.substring(2, 12);
 
//        if((stateCode != tempSateCode || nPANNo!=tempPanNo) && gstNo!=''){
//            alert("Invalid GST No.Please check pan No and state code.");
//            return false;
//        } else{
        return true;
//    }
    }

</script>

<script language="javascript">
    function submitPage() {
        var check = validatePanNo();
        var gstValidate = validateGSTNo();
        if (check == true && gstValidate == true) {
            var nPANNo = document.getElementById("panNo").value;
            var orgId = document.getElementById("organizationId").value;
            var companyType = document.getElementById("companyType").value;
            var gstNo = document.getElementById("gstNo").value;
            var stateId = document.getElementById("stateId").value;
              var cityId = document.getElementById("cityId").value;
              var custTypeid = document.getElementById("custTypeid").value;
//            alert(orgId);
//            alert(nPANNo.charAt(3).toUpperCase());
            if (companyType == '0') {
                alert("Company Type cannot be empty ");
                document.editcustomer.companyType.focus();
                return;
            }

            if (stateId == '0' || stateId == '') {
                alert("Billing State  cannot be empty ");
                document.editcustomer.stateIdTemp.focus();
                return;
            }
            /*  if(nPANNo.charAt(3).toUpperCase() =='P' && orgId != '1' ){
             alert("Please choose correct organisation name as individual.since 4th letter of Pan no is p ");
             document.editcustomer.organizationId.focus();
             return;
             }else if(nPANNo.charAt(3).toUpperCase()=='C' && orgId != '2' ){
             alert("Please choose correct organisation name as Company .since 4th letter of Pan no is C ");
             document.editcustomer.organizationId.focus();
             return;
             }else if(nPANNo.charAt(3).toUpperCase() =='A' && orgId != '3'){
             alert("Please choose correct organisation name as Association of Persons (AOP).since 4th letter of Pan no is  " + nPANNo.charAt(3));
             document.editcustomer.organizationId.focus();
             return; 
             }else if(nPANNo.charAt(3).toUpperCase() =='B' && orgId != '4'){
             alert("Please choose correct organisation name as Body of Individuals (BOI).since 4th letter of Pan no is  " + nPANNo.charAt(3));
             document.editcustomer.organizationId.focus();
             return; 
             }else if(nPANNo.charAt(3).toUpperCase() =='F' && orgId != '5'){
             alert("Please choose correct organisation name as Firm.since 4th letter of Pan no is  " + nPANNo.charAt(3));
             document.editcustomer.organizationId.focus();
             return; 
             }else if(nPANNo.charAt(3).toUpperCase() =='G' && orgId != '6'){
             alert("Please choose correct organisation name as Government.since 4th letter of Pan no is  " + nPANNo.charAt(3));
             document.editcustomer.organizationId.focus();
             return; 
             }else if(nPANNo.charAt(3).toUpperCase() =='H' && orgId != '7'){
             alert("Please choose correct organisation name as HUF (Hindu Undivided Family).since 4th letter of Pan no is  " + nPANNo.charAt(3));
             document.editcustomer.organizationId.focus();
             return; 
             }else if(nPANNo.charAt(3).toUpperCase() =='L' && orgId != '8'){
             alert("Please choose correct organisation name as Local Authority.since 4th letter of Pan no is  " + nPANNo.charAt(3));
             document.editcustomer.organizationId.focus();
             return; 
             }else if(nPANNo.charAt(3).toUpperCase() =='T' && orgId != '9'){
             alert("Please choose correct organisation name as Trust(AOP).since 4th letter of Pan no is  " + nPANNo.charAt(3));
             document.editcustomer.organizationId.focus();
             return; 
             }else{
             
             }*/
            if (orgId == '0') {
                alert("organization  cannot be empty ");
                document.editcustomer.gstNo.focus();
                return;
            }
            if (gstNo == '' && companyType == '1') {
                alert("GST No cannot be empty ");
                document.editcustomer.gstNo.focus();
                return;
            }
            document.editcustomer.action = '/throttle/addCustomer.do';
            document.editcustomer.submit();
        }
    }
    function setFocus() {
        document.editcustomer.custCode.focus();
    }
</script>

<script>
    $(document).ready(function () {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#accountManager').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/throttle/getAccountManager.do",
                    dataType: "json",
                    data: {
                        accountManagerName: request.term
                    },
                    success: function (data, textStatus, jqXHR) {
                        var items = data;
                        if (items == '') {
                            $('#accountManagerId').val('');
                            $('#accountManager').val('');
                        } else {
                            response(items);
                        }
                    },
                    error: function (data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function (event, ui) {
                $("#accountManager").val(ui.item.Name);
                var $itemrow = $(this).closest('tr');
                var value = ui.item.Name;
                var tmp = value.split('-');
                $itemrow.find('#accountManagerId').val(tmp[0]);
                $itemrow.find('#accountManager').val(tmp[1]);
                return false;
            }
        }).data("autocomplete")._renderItem = function (ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

    });
</script>
<style>
    #index div {
        color:white;

    }
</style>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Customer </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.Customer"  text="Customer"/></a></li>
            <li class="active">View/Edit Customer</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <body onload="show();
                    setFocus();">
                <form name="editcustomer"  method="post" >
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <c:if test="${getCustomerDetails != null}">
                        <c:forEach items="${getCustomerDetails}" var="cudl">
                            <table class="table table-hover  mb30 table-hover" >
                                <thead>
                                <th colspan="4"  height="30" id="index" style="background-color:#5BC0DE;">
                                    <div  align="center">View/Edit Customer</div>
                                </th>
                                </thead>
                                <tr height="35">
                                    <td width="25%"><font color="red">*</font>Customer Name</td>
                                    <td width="25%">
                                        <input name="customerId" id="customerId" type="hidden" class="form-control" value="<c:out value="${cudl.customerId}"/>" onClick="ressetDate(this);">
                                        <input name="customerName" readonly id="customerName" type="text" class="form-control"  value="<c:out value="${cudl.custName}"/>" style="width:200px;height:40px;"></td>

                                </tr>
                                    
                        <tr>
                            <td><font color="red">*</font>City</td>
                            <td>
                                
                                <select class="form-control" name="cityId" id="cityId"  required>
                                    <option value="" >--select--</option>
                                    <c:if test="${cityList != null}">
                                        <c:forEach items="${cityList}" var="city">
                                            <option value="<c:out value="${city.cityId}"/>" ><c:out value="${city.cityName}"/></option>
                                        </c:forEach>
                                        
                                    </c:if>
                                </select>
                         </td>
                            
                        <script> 
                           document.getElementById("cityId").value = '<c:out value="${cudl.cityId}"/>';
                        
                        </script>
                             <td>Customer Type</td>
                            <td>
                                <select  multiple="multiple" class="form-control" name="custTypeid" id="custTypeid" readonly>
                                      
                                    
                                    <c:if test="${cudl.custTypeid== ''}">
                                                
                                                <option value="1">consignor</option>
                                                <option value="2">consignee</option>
                                            </c:if>
                                        <c:if test="${cudl.custTypeid== '1'}">
                                                
                                                <option value="1" selected>consignor</option>
                                                <option value="2">consignee</option>
                                            </c:if>
                                                  <c:if test="${cudl.custTypeid== '2'}">
                                                
                                                <option value="1" >consignor</option>
                                                <option value="2"selected>consignee</option>
                                            </c:if>
                                                <c:if test="${cudl.custTypeid== '1,2'}">
                                                
                                                <option value="1" selected>consignor</option>
                                                <option value="2"selected>consignee</option>
                                            </c:if>
                                                
                                </select>
                                
                            </td>
                             
                            
                        </tr>
                       
                                <tr height="35">
                                    <td><font color="red">*</font>Tally BillingType</td>
                                    <td>
                                        <select class="form-control" name="tallyBillingType" id="tallyBillingType" style="width:200px;height:40px;">

                                            <option value="0">--Select--</option>
                                            <option value="1" selected>RCM</option>
                                            <option value="2">BOS</option>
                                        </select>
                                    </td>
                                    <td  ><font color="red">*</font>Contract Type&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td>
                                        <select  class="form-control" name="billingTypeId" id="billingTypeId" style="width:200px;height:40px;">
                                            <c:if test="${billingTypeList != null}">
                                                <c:forEach items="${billingTypeList}" var="btl">
                                                    <c:choose>
                                                        <c:when test="${btl.billingTypeId==cudl.billingTypeId}">
                                                            <option selected value='<c:out value="${btl.billingTypeId}"/>'><c:out value="${btl.billingTypeName}"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <c:if test="${btl.billingTypeId == 1 || btl.billingTypeId == 6}">
                                                                <option value="<c:out value="${btl.billingTypeId}"/>" ><c:out value="${btl.billingTypeName}"/></option>
                                                            </c:if>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>

                                    <td style="display:none"><font color="red">*</font>Company Type</td>
                                    <td style="display:none">
                                        <select class="form-control" name="companyType" id="companyType" >
                                            <option value="0">--Select--</option>
                                            <option value="1">Registered</option>
                                            <option value="2">Unregistered</option>
                                        </select>
                                        <script>
                                            document.getElementById("companyType").value = '<c:out value="${cudl.companyType}"/>';
                                            document.getElementById("tallyBillingType").value = '<c:out value="${cudl.tallyBillingType}"/>';
                                        </script>
                                    </td>
                                </tr>
                                <tr height="35">
                                    <td  width="25%"><font color="red">*</font>Customer as Billing Customer</td>
                                    <td  width="25%"><select id="billingCustFlag" name="billingCustFlag" onchange="show();" class="form-control"  style="width:200px;height:40px;">
                                            <c:if test="${cudl.customerId == cudl.billingCustId}">
                                                <option value="1" selected> Yes </option>
                                                <option value="2"> No </option>    
                                            </c:if>

                                            <c:if test="${cudl.customerId != cudl.billingCustId}">
                                                <option value="1" > Yes </option>
                                                <option value="2" selected> No </option>    
                                            </c:if>

                                        </select> </td>   


                                    <td id="showme" style="display:none;"><font color="red">*</font>Billing Customer Name</td>
                                    <td id="showme1" style="display:none;">
                                        <input name="billingCustName" id="billingCustName" type="text" class="form-control" value="<c:out value="${cudl.billingCustName}"/>"  maxlength="500" placeholder="Type billing customer name..." style="width:200px;height:40px;" required>
                                        <input name="billingCustId" id="billingCustId" type="hidden" class="form-control" value="<c:out value="${cudl.billingCustId}"/>" onKeyPress="return onKeyPressBlockNumbers(event);"  style="width:200px;height:40px;"></td>

                                </tr>
                                <tr style="display:none" height="35">

                                    <td style="display:none" >&nbsp;&nbsp;&nbsp;<font color="red">*</font>PrimaryContractType 2</td>
                                    <td>
                                        <select style="display:none" class="form-control" name="secondaryBillingTypeId" id="secondaryBillingTypeId" style="width:200px;height:40px;">
                                            <option value="" selected>--Select--</option>
                                            <c:if test="${billingTypeList != null}">
                                                <c:forEach items="${billingTypeList}" var="btl">
                                                    <c:choose>
                                                        <c:when test="${btl.billingTypeId==cudl.secondaryBillingTypeId}">
                                                            <option selected value='<c:out value="${btl.billingTypeId}"/>'><c:out value="${btl.billingTypeName}"/>
                                                            </c:when>
                                                            <c:otherwise>
                                                            <option value="<c:out value="${btl.billingTypeId}"/>" ><c:out value="${btl.billingTypeName}"/></option>
                                                        </c:otherwise>
                                                    </c:choose>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                </tr>
                                <tr height="35">
                                    <td><font color="red">*</font>Account Manager</td>
                                    <td>
                                        <select class="form-control" name="accountManagerId" id="accountManagerId" style="width:200px;height:40px;">
                                            <option value="<c:out value="${cudl.accountManagerId}"/>" selected ><c:out value="${cudl.empName}"/></option>
                                            <c:if test="${accountManagerList != null}">
                                                <c:forEach items="${accountManagerList}" var="btl">
                                                    <c:if test="${cudl.accountManagerId != btl.empId}">
                                                        <option value="<c:out value="${btl.empId}"/>" ><c:out value="${btl.empName}"/></option>
                                                    </c:if>
                                                </c:forEach>
                                            </c:if>
                                        </select>
                                    </td>
                                    <td >Customer Branch</td>
                                    <td>
                                        <input name="organizationIdTemp" id="organizationIdTemp"  type="hidden" class="form-control" >
                                        <input name="organizationId" id="organizationId"  type="hidden" value="<c:out value="${cudl.orgId}"/>" class="form-control" >
                                        <select class="form-control" name="compId" id="compId" style="width:200px;height:40px;"required data-placeholder="Choose One">
                                            <option value="0">--Select--</option>
                                            <c:if test = "${companyList!= null}" >
                                                <c:forEach  items="${companyList}" var="comp">
                                                    <option value='<c:out value="${comp.cmpId}" />'><c:out value="${comp.name}" />(<c:out value="${comp.companyType}" />)</option>
                                                </c:forEach >
                                            </c:if>
                                            <!--<option value="<c:out value="${cudl.orgId}"/>" selected ><c:out value="${cudl.orgId}"/></option>-->
                                            <%--<c:if test="${organizationList != null}">
                                                <c:forEach items="${organizationList}" var="org">
                                                    <option value="<c:out value="${org.orgId}"/>" ><c:out value="${org.orgName}"/></option>
                                                </c:forEach>
                                            </c:if>--%>
                                        </select>
                                    </td>
                                </tr>
                                <script>
                                    document.getElementById("compId").value = <c:out value="${cudl.compId}"/>;
                                    document.getElementById("organizationId").value = '<c:out value="${cudl.orgId}"/>';
                                </script>
                                <tr height="35">
                                    <td>Payment Type </td>
                                    <td> 
                                        <select name="paymentType" id="paymentType" class="form-control" style="width:200px;height:40px;">
                                            <c:if test="${cudl.paymentType == ''}">
                                                <option value="" selected>--Select--</option>
                                                <option value="1" >Credit</option>
                                                <option value="2" >Advance</option>
                                            </c:if>
                                            <c:if test="${cudl.paymentType == '1'}">
                                                <option value="" >--Select--</option>
                                                <option value="1" selected>Credit</option>
                                                <option value="2" >Advance</option>
                                            </c:if>
                                            <c:if test="${cudl.paymentType == '2'}">
                                                <option value="" >--Select--</option>
                                                <option value="1" >Credit</option>
                                                <option value="2" selected>Advance</option>
                                            </c:if>
                                            <c:if test="${cudl.paymentType == '0'}">
                                                <option value="" selected>--Select--</option>
                                                <option value="1" >Credit</option>
                                                <option value="2" >Advance</option>
                                                <option value="3" >To Pay and Advance</option>
                                                <option value="4" >To Pay</option>
                                            </c:if>
                                        </select></td>
                                    <td>&nbsp;&nbsp;&nbsp;<font color="red">*</font>Contactperson</td>
                                    <td><input name="custContactPerson" id="custContactPerson" type="text" class="form-control" value="<c:out value="${cudl.custContactPerson}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:200px;height:40px;"></td>
                                </tr>

                                <tr height="35">
                                    <td><font color="red">*</font>Customer Billing  Address</td>
                                    <td><textarea name="custAddress" id="custAddress"  style="width:200px;"><c:out value="${cudl.custAddress}"/></textarea></td>
<!--                                    <td>&nbsp;&nbsp;&nbsp;<font color="red">*</font> City</td>
                                    <td><input name="custCity" id="custCity" type="text" class="form-control"  value="<c:out value="${cudl.custCity}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:200px;height:40px;"></td>-->
                                </tr>
                                <tr height="35" style="display:none">
                                    <td>&nbsp;Customer Delivery Address</td>
                                    <td ><textarea name="custAddresstwo" id="custAddresstwo" style="width:200px;"><c:out value="${cudl.custAddresstwo}"/></textarea></td>

                                    <td>&nbsp;&nbsp;&nbsp;Customer  Additional Address</td>
                                    <td><textarea name="custAddressthree" id="custAddressthree" style="width:200px;"><c:out value="${cudl.custAddressthree}"/></textarea></td>
                                </tr>
                                <tr height="35">
                                    <td><font color="red">*</font> State</td>
                                    <td><input name="custState" id="custState" type="text" class="form-control" value="<c:out value="${cudl.custState}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:200px;height:40px;"></td>
                                    <td>&nbsp;&nbsp;&nbsp;<font color="red">*</font>Phone No</td>
                                    <td><input name="custPhone" id="custPhone" type="text" class="form-control" value="<c:out value="${cudl.custPhone}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:40px;"></td>
                                </tr>
                                <tr height="35">
                                    <td><font color="red">*</font>Mobile No</td>
                                    <td><input name="custMobile" id="custMobile" type="text" class="form-control" value="<c:out value="${cudl.custMobile}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="12" style="width:200px;height:40px;"></td>
                                    <td>&nbsp;&nbsp;&nbsp;Email</td>
                                    <td><input name="custEmail" id="custEmail" type="text" class="form-control" value="<c:out value="${cudl.custEmail}"/>" maxlength="20" style="width:200px;height:40px;"></td>
                                </tr>
                                <input name="creditLimit" id="creditLimit" type="hidden" class="form-control" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:40px;">
                                <input name="creditDays" id="creditDays" type="hidden" class="form-control" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="10" style="width:200px;height:40px;">
                                </tr>
                                <tr height="35">
                                    <td><font color="red">*</font>ERP ID</td>
                                    <td><input name="erpId" id="erpId" type="text" class="form-control" value="<c:out value="${cudl.erpId}"/>" maxlength="10" style="width:200px;height:40px;"></td>
                                    <td><font color="red">*</font>PAN No</td>
                                    <td><input name="panNo" id="panNo" type="text" onBlur="validatePanNo()" class="form-control" value="<c:out value="${cudl.panNo}"/>" maxlength="10" style="width:200px;height:40px;"></td>

                                </tr>

                                <tr height="35">
                                    <td><font color="red">*</font>Status</td>
                                    <td>
                                        <select name="custStatus" id="custStatus" class="form-control" style="width:200px;height:40px;">
                                            <c:if test="${cudl.custStatus == 'Y'}">
                                                <option value="Y" selected >Active</option>
                                                <option value="N"  >In Active</option>
                                            </c:if>
                                            <c:if test="${cudl.custStatus == 'N'}">
                                                <option value="Y" >Active</option>
                                                <option value="N" selected>In Active</option>
                                            </c:if>
                                        </select>
                                    </td>
                                    <td><font color="red">*</font>Billing Name and Address</td>
                                    <td colspan="3"><textarea name="billingNameAddress" id="billingNameAddress" cols="100" rows="4" style="width:200px;"><c:out value="${cudl.billingNameAddress}"/></textarea></td>
                                </tr>
                                <tr>
                                    <td><font color="red">*</font>Billing State</td>
                                    <td>
                                        <input name="stateId" id="stateId"  type="hidden" class="form-control" value="<c:out value="${cudl.stateId}"/>">
                                        <input name="stateCode" id="stateCode"  type="hidden" class="form-control"value="<c:out value="${cudl.stateCode}"/>" >
                                        <select name="stateIdTemp" id="stateIdTemp" class="form-control" style="width:200px;height:40px;" required data-placeholder="Choose One" onchange="setValues(this.value);">
                                            <option value="">Choose One</option>
                                            <c:if test = "${stateList != null}" >
                                                <c:forEach items="${stateList}" var="Type">
                                                    <option value='<c:out value="${Type.stateId}" />~<c:out value="${Type.stateCode}" />'><c:out value="${Type.stateName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                    </td>
                                    <td><font color="red">*</font>GST No</td>
                                    <td>
                                        <input name="gstNo" id="gstNo"  type="text" class="form-control" value="<c:out value="${cudl.gstNo}"/>" >

                                    </td>
                                </tr>
                                <script>
//                                document.getElementById("categoryName").value = '<c:out value="${getGstCategoryCode}"/>';

                                    document.getElementById("stateIdTemp").value = '<c:out value="${cudl.stateId}"/>~<c:out value="${cudl.stateCode}"/>';


                                </script>
                            </table>
                        </c:forEach >
                    </c:if>
                    <br>
                    <c:if test="${roleId != '1115'}">
                    <center>
                        <input type="button" value="SAVE" class="btn btn-info" onClick="submitPage();" style="width:100px;height:35px;">
                        &emsp;<input type="reset" class="btn btn-info" value="Clear" style="width:100px;height:35px;">
                    </center>
                    </c:if>
                    
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>
