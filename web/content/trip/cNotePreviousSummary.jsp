<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->



<style>

    .loading {

        border:10px solid red;

        display:none;

    }

    .button1{border: 1px black solid ; color:#000 !important;  }



    .spinner{

        position: fixed;

        top: 50%;

        left: 50%;

        margin-left: -50px; /* half width of the spinner gif */

        margin-top: -50px; /* half height of the spinner gif */

        text-align:center;

        z-index:1234;

        overflow: auto;

        width: 300px; /* width of the spinner gif */

        height: 300px; /*hight of the spinner gif +2px to fix IE8 issue */

    }

    .ui-loader-background {

        width:100%;

        height:100%;

        top:0;

        padding: 0;

        margin: 0;

        background: rgba(0, 0, 0, 0.3);

        display:none;

        position: fixed;

        z-index:100;

    }

    .ui-loader-background {

        display:block;



    }



</style>

<style>
#rcorners2 {
    /*border-radius: 35px;*/
    /*background: url(paper.gif);*/
/*    background-color: white ;
    background-position: left top;
    background-repeat: repeat;*/
    padding: 80px; 
    width: 1000px;
    height: 100px;
}
#rcorners1 {
    width: 1000px;
    height: 450px;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> C-Note Pending Summary</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Sales</a></li>
            <li class="active">C-Note Pending Summary</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>
                
                <form name="CNoteSearch" method="post">

                     <right>
                         <div align="right">
                            <a href="cNotePreviousSummary.do?pageType=2&flag=1"  target="_blank" >
                                <img src="images/download.jpg" alt="Y"   title="Download as Excel" style="height:80px;width:200px;margin-left:-01px;" />
                            </a>    
                        </div>  
                         <div>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                         </div>
                     </right>
                                <center>
                                    <font colspan="6" align="center" size='5' color="black"><b><u>Previous Summary</u></b></font>
                                </center>
                    <br>
                         <c:if test = "${consignmentListViewSummary != null}" >
                     <table id="rcorners2" align="center"  >
                            <c:forEach items="${consignmentListViewSummary}" var="cls">
                        <tr>
                           
                            <td  style="text-align: left"  >
                                <font color="#006829" size='4'><b> &emsp;40 ft:&nbsp;</b></font><font color="black" size='3'> <b>&nbsp;<c:out value="${cls.fortyFeetTotal}"/></b></font>
                            </td>
                            <td style="text-align: center">
                                <font color="#006829" size='4'><b> &emsp;20 ft:&nbsp;<font color="black" size='3'><b>&nbsp;<c:out value="${cls.twentyFeetTotal}"/></b> </font>
                                    </b></font>
                            </td>
                            <td  style="text-align: right">
                                <font color="#006829" size='4'><b> &emsp;Count:&nbsp;<font color="black" size='3'><b>&nbsp;<c:out value="${cls.fortyFeetTotal + cls.twentyFeetTotal}"/></b> </font>
                                    </b></font>
                            </td>
                            <td style="text-align: left">&emsp;</td>
                             <td colspan="2" style="text-align: right" >
                                <font color="#380099" size='4'><b>Booking:&nbsp;
                                </b></font>
                                <font color="black" size='3'><b><c:out value="${cls.totalOrders}"/>
                                </b></font>
                            </td>
                        </tr>
                        </c:forEach>
                        </c:if>
                      </table> 
                      <!--<table class="table table-info mb30 table-bordered"  width="100%">-->   
                      <table class="table table-info mb30 table-hover " id="table" border="1">    
                        <thead>   
                        <tr>
                            <th align="center">S.No</th>
                            <th align="center">ConsigDate</th>
                            <th align="center">CustRefNo</th>
                            <th align="center">CustName</th>
                            <th align="center">MovementType</th>
                            <th align="center">SLN/BOE</th>
                            <th align="center">Point</th>
                            <th align="center">Veh.Req.On </th>
                            <th align="center">Loading/Unloading On</th>
                            <th align="center">40 FT</th>
                            <th align="center">20 FT</th>
                            <th align="center">Total</th>
                            <th align="center">Created By</th>
                            <th align="center">Age(in Days)</th>

                        </tr>
                        </thead> 
                      <tbody>
                           <% int index = 1;%>
                        <c:forEach items="${consignmentListViewCustomerPreviousSummary}" var="cls">
                        <tr>
                            <td align="center" ><%= index%></td>
                            <td align="center" ><c:out value="${cls.consignmentOrderDate}"/></td>
                            <td align="center" ><c:out value="${cls.customerOrderReferenceNo}"/></td>
                            <td align="center" ><c:out value="${cls.customerName}"/></td>
                            <td align="center" ><c:out value="${cls.movementType}"/></td>
                            <td align="center" >
                                <c:if test="${cls.movementTypeId == 1 || cls.movementTypeId == 25 || cls.movementTypeId == 26  || cls.movementTypeId == 27 || cls.movementTypeId == 31 || cls.movementTypeId == 32}">
                                    <c:out value="${cls.shippingLineTwo}"/>
                                </c:if>
                                <c:if test="${cls.movementTypeId == 2 || cls.movementTypeId == 28 || cls.movementTypeId == 29 || cls.movementTypeId == 30}">
                                    <c:out value="${cls.billOfEntry}"/>
                                </c:if>

                            </td>
                            <td align="center" ><c:out value="${cls.finalPointName}"/></td>
                            <td align="center" ><c:out value="${cls.tripScheduleDate}"/><br><c:out value="${cls.tripScheduleTime}"/></td>
                            <td align="center" ><c:out value="${cls.loadUnloadTime}"/><br><c:out value="${cls.loadUnloadTime}"/></td>
                            <td align="center" ><c:out value="${cls.fortyFeetTotal}"/></td>
                            <td align="center" ><c:out value="${cls.twentyFeetTotal}"/></td>
                            <td align="center" ><font size='3'><c:out value="${cls.twentyFeetPending + cls.fortyFeetPending}"/></font>/ 
                                <font  size='3'> &nbsp;<c:out value="${cls.twentyFeetTotal + cls.fortyFeetTotal}"/></td>
                            <td align="center" ><c:out value="${cls.userName}"/></td>
                            <td align="center" ><c:out value="${cls.age}"/></td>
                        </tr>
                         <% index++;%>
                        </c:forEach>
                        </tbody>
                    </table> 
                            
                    
                      
                                              <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="service.label.EntriesPerPage"  text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="service.label.DisplayingPageof"  text="default text"/> <span id="currentpage"></span>  <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                      
                      
                      
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
                <div id="loader"></div>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
