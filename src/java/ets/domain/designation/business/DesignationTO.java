/*-------------------------------------------------------------------------
  * __NAME__.java
  * __DATE__
  *
  * Copyright (c) Entitle.
  * All Rights Reserved.
  *
  * This software is the confidential and proprietary information of
  * Entitle ("Confidential Information"). You shall
  * not disclose such Confidential Information and shall use it only in
  * accordance with the terms of the license agreement you entered into
  * with Entitle.
  -------------------------------------------------------------------------*/

package ets.domain.designation.business;
/******************************************************************************
 *
 * Modification Log:
 * ----------------------------------------------------------------------------
 * Ver    Date                               Author                    Change
 * ----------------------------------------------------------------------------
 * 1.0   __DATE__                Your_Name ,Entitle      Created
 *
 ******************************************************************************/

public class DesignationTO {

    /** Creates a new instance of __NAME__ */
    public DesignationTO() {
    }
   
        
            
    private String  activeInd = "";

    
    private  String desigName = "";
    private int desigId = 0;
   
    private String gradeName = "";
   private String  description = "";
    private int gradeId = 0;

     
public String getActiveInd() {
        return activeInd;
    }

    public void setActiveInd(String activeInd) {
        this.activeInd = activeInd;
    }
    public int getDesigId() {
        return desigId;
    }

    public void setDesigId(int desigId) {
        this.desigId = desigId;
    }

    public String getDesigName() {
        return desigName;
    }

    public void setDesigName(String desigName) {
        this.desigName = desigName;
    }

    public int getGradeId() {
        return gradeId;
    }

    public void setGradeId(int gradeId) {
        this.gradeId = gradeId;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }

   
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    

     

     

     

            
}
