<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>


        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

        </script>
        <script type="text/javascript">
            function submitPage() {
                document.accountReceivable.action = '/throttle/handleAccountsReceivableDetails.do?param=ExportExcel';
                document.accountReceivable.submit();
                 }
            
            function setValue(){
                if('<%=request.getAttribute("page")%>' !='null'){
                var page = '<%=request.getAttribute("page")%>';
                    if(page == 1){
                      submitPage('search');
                    }
                }
            }
        </script>
                    
                    <script>
	   function changePageLanguage(langSelection){
	   if(langSelection== 'ar'){
	   document.getElementById("pAlign").style.direction="rtl";
	   }else if(langSelection== 'en'){
	   document.getElementById("pAlign").style.direction="ltr";
	   }
	   }
	 </script>

	  <c:if test="${jcList != null}">
	  <body onLoad="changePageLanguage('<%=request.getAttribute("language")%>');setValues();getVehicleNos();">
	  </c:if>

<!--	  <span style="float: right">
		<a href="?paramName=en">English</a>
		|
		<a href="?paramName=ar">???????</a>
	  </span>-->

<style type="text/css">





.container {width: 960px; margin: 0 auto; overflow: hidden;}
.content {width:800px; margin:0 auto; padding-top:50px;}
.contentBar {width:90px; margin:0 auto; padding-top:50px; padding-bottom:50px;}

/* STOP ANIMATION */



/* Second Loadin Circle */

.circle1 {
	background-color: rgba(0,0,0,0);
	border:5px solid rgba(100,183,229,0.9);
	opacity:.9;
	border-left:5px solid rgba(0,0,0,0);
	border-right:5px solid rgba(0,0,0,0);
	border-radius:50px;
	/*box-shadow: 0 0 15px #2187e7; */
	box-shadow: 0 0 15px blue;
	width:40px;
	height:40px;
	margin:0 auto;
	position:relative;
	top:-50px;
	-moz-animation:spinoffPulse 1s infinite linear;
	-webkit-animation:spinoffPulse 1s infinite linear;
}

@-moz-keyframes spinoffPulse {
	0% { -moz-transform:rotate(0deg); }
	100% { -moz-transform:rotate(360deg);  }
}



</style>
    <style>
    #index td {
   color:white;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i>Accounts Receivable Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li><a href="general-forms.html">Finance</a></li>
            <li class="active">Accounts Receivable Report</li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="setValue();">
        <form name="accountReceivable" action=""  method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <br>
            <br>
            <br>
            <c:if test = "${accountReceivableList == null}" >
                <center>
                <font color="blue"><spring:message code="operations.reports.label.PleaseWaitYourRequestisProcessing" text="default text"/>
</font>
                <div class="container">
                    <div class="content">
                <div class="circle"></div>
                <d<center>
                <font color="blue"><spring:message code="operations.reports.label.PleaseWaitYourRequestisProcessing" text="default text"/>
</font>
                <div class="container">
                    <div class="content">
                <div class="circle"></div>
                <div class="circle1"></div>
                </div>
            </div>
                </center>iv class="circle1"></div>
                </div>
            </div>
                </center>
            </c:if>
            <c:if test = "${accountReceivableList != null}" >
                <table class="table table-info mb30 table-hover" id="table" >
                    <thead>
                        <tr height="80">
                            <th><h3><spring:message code="operations.reports.label.SNo" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.InvoiceCode" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.CNote" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.InvoiceAmount" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.InvoiceCreationDate" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.InvoiceSubmittedDate" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.InvoiceStatus" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.InvoiceOverdueOn" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.DaysToOverDue" text="default text"/></h3></th>
                            <th><h3><spring:message code="operations.reports.label.NoOfDaysWithOverdue" text="default text"/><br></h3></th>
                        </tr>
                    </thead>
                    <tbody>
                        <% int index = 0,sno = 1;%>
                        <c:forEach items="${accountReceivableList}" var="arList">
                            <%
                                        String classText = "";
                                        int oddEven = index % 2;
                                        if (oddEven > 0) {
                                            classText = "text2";
                                        } else {
                                            classText = "text1";
                                        }
                            %>
                            <tr height="30">
                                <td align="left" class="text2"><%=sno%></td>
                                <td align="left" class="text2"><c:out value="${arList.invoiceCode}"/> </td>
                                <td align="left" class="text2"><c:out value="${arList.consignmentNote}"/> </td>
                                <td align="left" class="text2"><c:out value="${arList.freightAmount}"/></td>
                                <td align="left" class="text2"><c:out value="${arList.invoiceDate}"/></td>
                                <td align="left" class="text2"><c:out value="${arList.invoiceDate}"/></td>
                                <td align="left" class="text2"><c:out value="${arList.invoiceStatus}"/></td>
                                <td align="left" class="text2"><c:out value="${arList.overDueOn}"/></td>
                                <td align="left" class="text2"><c:out value="${arList.daysToOverDue}"/></td>
                                <td align="left" class="text2"><c:out value="${arList.daysWithOverDue}"/></td>
                                </td>
                            </tr>
                            <%
                                       index++;
                                       sno++;
                            %>
                        </c:forEach>

                    </tbody>
                </table>
            </c:if>
                <table style="width: 1100px" align="center" border="0">
            <tr>
                <td align="center">
                    <input type="hidden" name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>"/>
                    <input type="hidden" name="toDate" id="toDate" value="<c:out value="${toDate}"/>"/>
                    <input type="hidden" name="customerId" id="customerId" value="<c:out value="${customerId}"/>"/>
                    <input type="button" align="center" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage();">
                </td>
            </tr>
            </table>

            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span><spring:message code="operations.reports.label.of" text="default text"/>  <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
            
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>