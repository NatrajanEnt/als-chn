<%-- 
    Document   : viewVehiclePage
    Created on : 23 Jun, 2016, 12:56:36 PM
    Author     : pavithra
--%>

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(function () {
            $("#fromDate1").datepicker({
                defaultDate: "-d",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd-mm-yy",
//                                        maxDate:"-d",
                onClose: function (selectedDate) {
                    var d = selectedDate.split("-");
                    var da = d[0];
                    var mo = d[1];
                    var yr = parseInt(d[2]) + 1;
                    var nd = da + "-" + mo + "-" + yr;
                    $("#toDate1").datepicker("setDate", nd);
                }
            });
            $("#toDate1").datepicker({
                changeMonth: true,
                dateFormat: "dd-mm-yy",
            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        $(function () {
            $("#fcDate").datepicker({
                defaultDate: "-d",
                changeMonth: true,
                numberOfMonths: 1,
                dateFormat: "dd-mm-yy",
//                                        maxDate:"-d",
                onClose: function (selectedDate) {
                    var d = selectedDate.split("-");
                    var da = d[0];
                    var mo = d[1];
                    var yr = parseInt(d[2]) + 1;
                    var nd = da + "-" + mo + "-" + yr;
                    $("#fcExpiryDate").datepicker("setDate", nd);
                }
            });
            $("#fcExpiryDate").datepicker({
                changeMonth: true,
                dateFormat: "dd-mm-yy",
            });
        });
    });
</script>

<script>
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true,
            dateFormat: 'dd-mm-yy'
        });

    });


</script>

<script language="javascript">

    function CalWarrantyDate(selDate) {

        var seleDate = selDate.split('-');
        var dd = seleDate[0];
        var mm = seleDate[1];
        var yyyy = seleDate[2];

        var today = new Date();
        var dd1 = today.getDate();
        var mm1 = today.getMonth() + 1; //January is 0!
        var yyyy1 = today.getFullYear();

        //                if(dd<10){dd='0'+dd}if(mm<10){mm='0'+mm}today = mm+'-'+dd+'-'+yyyy;alert("today"+today);alert("value"+value);

        var selecedDate = new Date(yyyy, mm, dd);
        var currentDate = new Date(yyyy1, mm1, dd1);
        var Days = Math.floor((selecedDate.getTime() - currentDate.getTime()) / (1000 * 60 * 60 * 24));
        //alert("DAYS--"+Days);
        document.addVehicle.war_period.value = Days;

    }



    function submitPage2(value) {
        if (value == "Save") {


            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }

    }
    function submitPage(value) {
        if (value == "save") {

            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }
    function submitPageDedicate(value) {

        var owner = document.getElementById("asset").value;
        document.getElementById("asset").value = owner;

        if (validate() == 'fail') {
            return;
        } else if (document.addVehicle.vendorId.value == '') {
            alert('Please Enter Vendor name');
            document.addVehicle.vendorId.focus();
            return;
        } else if (document.addVehicle.regNo.value == '') {
            alert('Please Enter Vehicle Registration Number');
            document.addVehicle.regNo.focus();
            return;
        } else if (document.addVehicle.regNoCheck.value == 'exists') {
            alert('Vehicle RegNo already Exists');
            return;
            //                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
            //                    return;

        } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
            return;
        } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
            return;
        } else if (isSelect(document.addVehicle.typeId, 'Vehicle type')) {
            return;
        }
        if (value == "save") {

            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }
    function submitPage1(value) {

        var owner = document.getElementById("asset").value;
        document.getElementById("asset").value = owner;

        if (validate() == 'fail') {
            return;
        } else if (document.addVehicle.vendorId.value == '') {
            alert('Please Enter Vendor name');
            document.addVehicle.vendorId.focus();
            return;
        } else if (document.addVehicle.regNo.value == '') {
            alert('Please Enter Vehicle Registration Number');
            document.addVehicle.regNo.focus();
            return;
        } else if (document.addVehicle.regNoCheck.value == 'exists') {
            alert('Vehicle RegNo already Exists');
            return;
            //                }else if(isSelect(document.addVehicle.usageId,'Usage Type')){
            //                    return;

        } else if (isSelect(document.addVehicle.mfrId, 'Manufacturer')) {
            return;
        } else if (isSelect(document.addVehicle.modelId, 'Vehicle Model')) {
            return;
        } else if (isSelect(document.addVehicle.typeId, 'Vehicle type')) {
            return;
        }

        if (value == "save1") {
            // alert("cgsd77f");
            //                    document.addVehicle.nextFCDate.value = dateFormat(document.addVehicle.nextFCDate);
            //     document.addVehicle.dateOfSale.value = dateFormat(document.addVehicle.dateOfSale);
            document.addVehicle.action = '/throttle/addVehicle.do';
            document.addVehicle.submit();
        }
    }


    var httpRequest;
    function getVehicleDetails() {
        var vehicleId = $("#vehicleId").val();
        if (document.addVehicle.regNo.value != '' && vehicleId == '') {
            var url = '/throttle/checkVehicleExists.do?regno=' + document.addVehicle.regNo.value;

            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("POST", url, true);
            httpRequest.onreadystatechange = function () {
                go1();
            };
            httpRequest.send(null);
        }
    }


    function go1() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var response = httpRequest.responseText;
                var temp = response.split('-');
                if (response != "") {
                    alert('Vehicle Already Exists');
                    document.getElementById("StatusMsg").innerHTML = httpRequest.responseText.valueOf() + " Already Exists";
                    document.addVehicle.regNo.focus();
                    document.addVehicle.regNo.select();
                    document.addVehicle.regNoCheck.value = 'exists';
                } else
                {
                    document.addVehicle.regNoCheck.value = 'Notexists';
                    document.getElementById("StatusMsg").innerHTML = "";
                }
            }
        }
    }

</script>




<script>

    function deleteRows(sno) {
        if (sno != 1) {
            rowCount--;
            document.getElementById("addTyres").deleteRow(sno);
        } else {
            alert("Can't Delete The Row");
        }
    }

    var httpReq;
    var temp = "";
    function getVehicleType(mfrId) { //alert(str);
        var fleetTypeId = $("#fleetTypeId").val();
        $.ajax({
            url: "/throttle/getMfrVehicleType.do",
            dataType: "json",
            data: {
                mfrId: mfrId,
                fleetTypeId: fleetTypeId
            },
            success: function (temp) {
                // alert(data);
                if (temp != '') {
                    $('#vehicleTypeId').empty();
                    $('#vehicleTypeId').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    $.each(temp, function (i, data) {
                        $('#vehicleTypeId').append(
                                $('<option style="width:150px"></option>').val(data.Id + "~" + data.AxleTypeId).html(data.Name)
                                )
                    });
                } else {
                    $('#vehicleTypeId').empty();
                }
            }
        });

    }



    function getModelAndTonnage(str) {
        var fleetTypeId = $("#fleetTypeId").val();
        var vehicleTypeId = str.split("~")[0];
        document.getElementById("typeId").value = vehicleTypeId;
        var fleetTypeId = $("#fleetTypeId").val();
        $.ajax({
            url: "/throttle/getModels1.do",
            dataType: "text",
            data: {
                typeId: vehicleTypeId,
                mfrId: document.addVehicle.mfrId.value,
                fleetTypeId: fleetTypeId
            },
            success: function (temp) {
                // alert(data);
                if (temp != '') {
                    setOptions1(temp, document.addVehicle.modelId);
                    document.getElementById("modelId").value = '<c:out value="${modelId}"/>';
                    getVehAxleName(str);
                } else {
                    setOptions1(temp, document.addVehicle.modelId);
                    alert('There is no model based on Vehicle Type Please add first');
                }
            }
        });
        // for tonnage now
        $.ajax({
            url: "/throttle/getTonnage.do",
            dataType: "text",
            data: {
                typeId: vehicleTypeId,
                fleetTypeId: fleetTypeId
            },
            success: function (temp) {
                if (temp != '') {
                    setOptions2(temp, document.addVehicle.seatCapacity);
                } else {

                    alert('Tonnage is not there please set first in vehicle type');
                }
            }
        });
    }


    function setOptions1(text, variab) {
//                                                                alert("setOptions on page")
        //          
//                                                                                      alert("variab = "+variab)
        variab.options.length = 0;
        var option0 = new Option("--select--", '0');
        variab.options[0] = option0;


        if (text != "") {
//                                                                        alert("inside the condition")
            var splt = text.split('~');
            var temp1;
            variab.options[0] = option0;
            for (var i = 0; i < splt.length; i++) {
//                                                                            alert("splt.length ="+splt.length)
//                                                                            alert("for loop ="+splt[i])
                temp1 = splt[i].split('-');
                option0 = new Option(temp1[1], temp1[0])
//                                        alert("option0 ="+option0)
                variab.options[i + 1] = option0;
            }
        }
    }

    function setOptions2(text) {
        //                                alert("setOptions on page")
        //                               alert("text = "+text)
        if (text != "") {
            //                                        alert(text);
            document.getElementById('seatCapacity').value = text;

        }
    }


    function getInsCompanyName(val) {

        var issueCmpId = document.getElementById("IssuingCmnyName").value;
        //alert(issueCmpId);
        $.ajax({
            url: '/throttle/getInsCompanyName.do?issueCmpId=' + issueCmpId,
            // alert(url);
            data: {issueCmpId: issueCmpId},
            dataType: 'json',
            success: function (data) {

                if (data !== '') {
                    $.each(data, function (i, data) {
                        var temp = data.Name.split('~');
                        document.getElementById("ins_AgentName").value = temp[0];
                        document.getElementById("ins_AgentCode").value = temp[1];
                        document.getElementById("ins_AgentMobile").value = temp[2];
                    });
                }
            }
        });
    }





    function validate()
    {
        var tyreIds = document.getElementsByName("tyreIds");
        var tyreDate = document.getElementsByName("tyreDate");
        var positionIds = document.getElementsByName("positionIds");
        var itemIds = document.getElementsByName("itemIds");
        var tyreExists = document.getElementsByName("tyreExists");

        var cntr = 0;
        for (var i = 0; i < tyreIds.length; i++) {
            for (var j = 0; j < tyreIds.length; j++) {
                if ((tyreIds[i].value == tyreIds[j].value) && (tyreIds[i].value == tyreIds[j].value) && (i != j) && (tyreIds[i].value != '')) {
                    cntr++;
                }
            }
            if (parseInt(cntr) > 0) {
                alert("Same Tyre Number should not exists twice");
                return "fail";
                break;
            }
        }

        for (var i = 0; i < positionIds.length; i++) {
            for (var j = 0; j < positionIds.length; j++) {
                if ((positionIds[i].value == positionIds[j].value) && (positionIds[i].value == positionIds[j].value) && (i != j) && (positionIds[i].value != '0')) {
                    cntr++;
                }
            }
            if (parseInt(cntr) > 0) {
                alert("Tyre Positions should not be repeated");
                return "fail";
                break;
            }
        }

        for (var i = 0; i < tyreIds.length; i++) {

            if (itemIds[i].value != '0') {
                if (positionIds[i].value == '0') {
                    alert('Please Select Position');
                    positionIds[i].focus();
                    return "fail";
                } else if (tyreIds[i].value == '') {
                    alert('Please Enter Tyre No');
                    tyreIds[i].focus();
                    return "fail";
                } else if (tyreExists[i].value == 'exists') {
                    alert('Tyre number ' + tyreIds[i].value + ' is already fitted to another vehicle');
                    tyreIds[i].focus();
                    tyreIds[i].select();
                    return "fail";
                }
            }
        }

        return "pass";
    }




    var httpRequest;
    function checkTyreId(val)
    {
        val = val - 1;
        var tyreNo = document.getElementsByName("tyreIds");
        var tyreExists = document.getElementsByName("tyreExists");
        if (tyreNo[val].value != '') {
            var url = '/throttle/checkVehicleTyreNo.do?tyreNo=' + tyreNo[val].value;
            if (window.ActiveXObject)
            {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest)
            {
                httpRequest = new XMLHttpRequest();
            }
            httpRequest.open("GET", url, true);
            httpRequest.onreadystatechange = function () {
                processRequest(tyreNo[val].value, val);
            };
            httpRequest.send(null);
        }
    }


    function processRequest(tyreNo, index)
    {
        if (httpRequest.readyState == 4)
        {
            if (httpRequest.status == 200)
            {
                var tyreExists = document.getElementsByName("tyreExists");
                if (httpRequest.responseText.valueOf() != "") {
                    document.getElementById("userNameStatus").innerHTML = "Tyre No " + tyreNo + " Already Exists in " + httpRequest.responseText.valueOf();
                    tyreExists[index].value = 'exists';
                } else {
                    document.getElementById("userNameStatus").innerHTML = "";
                    tyreExists[index].value = 'notExists';
                }
            } else
            {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }

    function openAttachedInfo() {
        document.getElementById("asset").value = "2";
        document.getElementById("attachInfo").style.display = "block";
    }
    function closeAttachedInfo() {
        document.getElementById("asset").value = "1";
        document.getElementById("attachInfo").style.display = "none";
    }

    function blockNonNumbers(obj, e, allowDecimal, allowNegative)
    {
        var key;
        var isCtrl = false;
        var keychar;
        var reg;

        if (window.event) {
            key = e.keyCode;
            isCtrl = window.event.ctrlKey
        } else if (e.which) {
            key = e.which;
            isCtrl = e.ctrlKey;
        }

        if (isNaN(key))
            return true;

        keychar = String.fromCharCode(key);

        // check for backspace or delete, or if Ctrl was pressed
        if (key == 8 || isCtrl)
        {
            return true;
        }

        reg = /\d/;
        var isFirstN = allowNegative ? keychar == '-' && obj.value.indexOf('-') == -1 : false;
        var isFirstD = allowDecimal ? keychar == '.' && obj.value.indexOf('.') == -1 : false;

        return isFirstN || isFirstD || reg.test(keychar);
    }


</script>

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();

        $("#insurance").focus();
        $("#vehicleInsurane").focus();
    });
    function selectTabssf(listId) {
        if (listId == '1') {
//                                        alert(listId)
            $("#insurance").focus();

        }
    }


    function vehicleValidation() {//pavi
        var owner = document.getElementById("ownerShips").value;
        document.getElementById("asset").value = owner;
// alert("owner:"+owner);
        if (owner == 1 || owner == 5) {
            if (document.addVehicle.regNo.value == '') {
                alert('Please Enter Vehicle Registration Number');
                document.addVehicle.regNo.focus();
                return false;
            } else if (document.addVehicle.regNoCheck.value == 'exists') {
                alert('Vehicle RegNo already Exists');
                document.addVehicle.regNoCheck.focus();
                return false;
            } else if (document.addVehicle.mfrId.value == 0) {
                alert('Please select Manufacturer');
                document.addVehicle.mfrId.focus();
                return false;
            } else if (document.addVehicle.typeId.value == 0) {
                alert('Please select Vehicle Type');
                document.addVehicle.typeId.focus();
                return false;
            } else if (document.addVehicle.modelId.value == 0) {
                alert('Please select Vehicle Model');
                document.addVehicle.modelId.focus();
                return false;
            } else if (document.addVehicle.dateOfSale.value == '') {
                alert("Please select Registration Date");
                document.addVehicle.dateOfSale.focus();
                return false;
            } else if (document.addVehicle.engineNo.value == '') {
                alert("Please enter Engine Number");
                document.addVehicle.engineNo.focus();
                return false;
            } else if (document.addVehicle.chassisNo.value == '') {
                alert("Please enter Chassis Number");
                document.addVehicle.chassisNo.focus();
                return false;
            } else if (document.addVehicle.seatCapacity.value == '' || document.addVehicle.seatCapacity.value == 0) {
                alert("Please enter Vehicle Tonnage");
                document.addVehicle.seatCapacity.focus();
                return false;
            } /*else if (document.addVehicle.warrantyDate.value == '') {
                alert("Please select Warranty Date");
                document.addVehicle.warrantyDate.focus();
                return false;
            } else if (document.addVehicle.war_period.value == '' || document.addVehicle.war_period.value == 0) {
                alert("Please select Warranty Period");
                document.addVehicle.war_period.focus();
                return false;
            }*/ else if (document.addVehicle.vehicleCost.value == '') {
                alert("Please select Vehicle Cost");
                document.addVehicle.vehicleCost.focus();
                return false;
            } else if (document.addVehicle.vehicleDepreciation.value == '') {
                alert("Please select Vehicle Depreciation");
                document.addVehicle.vehicleDepreciation.focus();
                return false;
                //                }else if(document.addVehicle.classId == 0){
                //                    alert("Please select Vehicle Class");
                //                    document.addVehicle.classId.focus();
                //                    return false;
            } else if (document.addVehicle.kmReading.value == '') {
                alert("Please select Km Reading");
                document.addVehicle.kmReading.focus();
                return false;
            } else if (document.addVehicle.dailyKm.value == '') {
                alert("Please select Daily Km Reading");
                document.addVehicle.dailyKm.focus();
                return false;
            } else if (document.addVehicle.dailyHm.value == '') {
                alert('Please select Daily Hm reading');
                document.addVehicle.dailyHm.focus();
                return false;
            } else if (document.addVehicle.vehicleColor.value == '') {
                alert('Please fill vehicle color');
                document.addVehicle.vehicleColor.focus();
                return false;
            } else {
                //alert("am here :2");
                saveVehicleDetails();
                return true;

            }
        } else {
            if (document.addVehicle.regNo.value == '') {
                alert('Please Enter Vehicle Registration Number');
                document.addVehicle.regNo.focus();
                return false;
            } else if (document.addVehicle.regNoCheck.value == 'exists') {
                alert('Vehicle RegNo already Exists');
                document.addVehicle.regNoCheck.focus();
                return false;
            } else {
                saveVehicleDetails();
                return true;

            }
        }

    }
    function saveVehicleDetails() {
//alert("a m here 3");
//$("#vehicleDetailSaveButton").hide();
        var usageId = document.addVehicle.usageId.value;
        var warPeriod = document.addVehicle.war_period.value;
        var mfrId = document.addVehicle.mfrId.value;
        var classId = document.addVehicle.classId.value;
        var modelId = document.addVehicle.modelId.value;
        var dateOfSale = document.addVehicle.dateOfSale.value;
        var regNo = document.addVehicle.regNo.value;
        var description = document.addVehicle.description.value;
        var engineNo = document.addVehicle.engineNo.value;
        var chassisNo = document.addVehicle.chassisNo.value;
        var opId = document.addVehicle.opId.value;
        var seatCapacity = document.addVehicle.seatCapacity.value;
        var kmReading = document.addVehicle.kmReading.value;
        var vehicleCost = document.addVehicle.vehicleCost.value;
        var vehicleColor = document.addVehicle.vehicleColor.value;
        var groupId = document.addVehicle.groupId.value;
        var gpsSystem = document.addVehicle.gpsSystem.value;
        var warrantyDate = document.addVehicle.warrantyDate.value;
        var asset = document.addVehicle.asset.value;
        var axles = document.addVehicle.axles.value;
        var vehicleDepreciation = document.addVehicle.vehicleDepreciation.value;
        var dailyKm = document.addVehicle.dailyKm.value;
        var dailyHm = document.addVehicle.dailyHm.value;
        var gpsSystemId = document.addVehicle.gpsSystemId.value;
        var fastagTransactionId = document.addVehicle.fastagTransactionId.value;
        var ownerShips = document.addVehicle.ownerShips.value;
        var typeId = document.addVehicle.typeId.value;
        var vendorId = document.addVehicle.vendorId.value;
        var axleTypeId = document.addVehicle.axleTypeId.value;
        var vehicleId = document.addVehicle.vehicleId.value;
        var activeInd = document.addVehicle.activeInd.value;
// alert("csh")
        var insertStatus = 0;
        if (vendorId == '') {
            vendorId = 0;
        }
//                alert("typeId == "+typeId);
        var vehicleId = $("#vehicleId").val();
        if (vehicleId == '') {
            vehicleId = 0;
        }
        var statusName = "";
        var url = "";
        if (vehicleId == 0) {
            //alert("added")
            url = "./saveVehicleDetails.do";
            statusName = "added";
        } else {
//                            alert("updated")
            url = "./updateVehicleDetails.do";
            statusName = "updated";
        }
//  alert("test url");
        $.ajax({
            url: url,
            data: {usageId: usageId,
                war_period: warPeriod, mfrId: mfrId, classId: classId, modelId: modelId,
                dateOfSale: dateOfSale, regNo: regNo, description: description, regNo: regNo,
                        engineNo: engineNo, chassisNo: chassisNo, opId: opId, seatCapacity: seatCapacity,
                kmReading: kmReading, vehicleColor: vehicleColor, groupId: groupId,
                gpsSystem: gpsSystem, warrantyDate: warrantyDate, asset: asset, axles: axles, vehicleCost: vehicleCost,
                vehicleDepreciation: vehicleDepreciation, dailyKm: dailyKm, dailyHm: dailyHm,
                gpsSystemId: gpsSystemId, fastagTransactionId:fastagTransactionId,ownerShips: ownerShips, typeId: typeId, vendorId: vendorId,
                axleTypeId: axleTypeId, vehicleId: vehicleId, activeInd: activeInd
            },
            type: "GET",
            success: function (response) {

                var vehicleDetails = response.toString().trim();
                var temp = vehicleDetails.split("~");
                vehicleId = parseInt(temp[0]);
                // alert(vehicleId);
                if (vehicleId == 0) {
                    insertStatus = 0;
                    $("#StatusMsg").text("Vehicle " + regNo + " " + statusName + " failed ").css("color", "red");
                } else {
                    insertStatus = vehicleId;
                    $("#StatusMsg").text("Vehicle " + regNo + " " + statusName + " sucessfully. Door No is " + temp[1]).css("color", "green");
                    $("#vehicleId").val(vehicleId);
                    $("#vehicleDetailSaveButton").hide();
                    // $("#tyreSave").show();
                    $("#depreciationSave").show();
                    $("#uploadBtn").show();
                    $("#oEmNext").show();
                    alert("Vehicle " + regNo + " " + statusName + " sucessfully. Door No is " + temp[1]);
                }
            },
            error: function (xhr, status, error) {
            }
        });
        return insertStatus;
    }
    function checkaddRow()
    {
        if ('<%=request.getAttribute("vehicleUploadsDetails")%>' != 'null') {
        } else {
            addRow();
        }

    }
//                   function testPage(){
//                                        var str = new String("Hello world");
//         alert(str.css("color", "green"));
//                                    }
</script>

<script>
function showPdf(val) {
              window.open('/throttle/displayVehicleLogoBlobData.do?uploadId=' + val, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
     }
     function deleteVehUploadData(val) {
//         alert(val);
          var confirms = confirm("Do you really want to delete?");
          if (confirms == true) {
              var  myWindow;
              myWindow = window.open('/throttle/deleteUploadVehData.do?uploadId=' + val);
              location.reload();
              myWindow.close();
          }
}
              
</script>

<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="stores.label.Vehicle"  text="Vehicle"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></a></li>
            <li class="active"><spring:message code="stores.label.Vehicle"  text="Vehicle"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">


        <div class="panel-body">

            <!--    <body onLoad="addRow();setImages(0,0,0,0,0,0);hideAdds(1),hideAdds1(1);advanceFuelTabHide()">-->
            <body onLoad="checkaddRow();
                    vehicleStepneyDetails(1);">
<!--                        <body onLoad="checkaddRow();checkPaymentMode();getBankBranch('<c:out value="${bankId}"/>');getBankBranchRoadTax('<c:out value="${bankIdRT}"/>');
                    getBankBranchFC('<c:out value="${bankIdFC}"/>');getBankBranchPermit('<c:out value="${bankIdPermit}"/>');selectTabs('<c:out value="${listId}"/>');
                    addOem(0, 1, '', '', '', 0, 0, 0);
                    vehicleOEMRow();vehicleStepneyDetails(1);
                    addRow1();
                    setImages(0, 0, 0, 0, 0, 0);
                    hideAdds(1);hideAdds1(1);">-->

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>
                <form name="addVehicle" method="post" enctype="multipart/form-data" >

                    <%@ include file="/content/common/message.jsp" %>
                    <center><font color="black" size="4"><c:out value="${regNo}"/></font></center>
                    <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="<c:out value="${fleetTypeId}"/>" />
                    <font  style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold;">
                    <div align="center" style="height:30px;" id="StatusMsg">&nbsp;&nbsp;
                    </div>
                    </font>

                    <input type="hidden" name="fleetTypeId" id="fleetTypeId" value="1" />
                    <div id="tabs">
                        <ul>
                            <li id="tab1"><a href="#vehicleInfo"><span>Vehicle</span></a></li>
                            <li id="tab2"><a href="#vehicleOem"><span>OEM</span></a></li>
                            <li id="tab3"><a href="#vehicleDepreciationTab"><span>Depreciation</span></a></li>
                            <li><a href="#vehicleFileAttachments"><span>File Attachments</span></a></li>
<!--                            <li id="tab4"><a href="#vehicleInsuranceeDetail"><span>Insurance</span></a></li>
                            <li id="tab5"><a href="#vehicleFitnessCertificate"><span>Vehicle FC</span></a></li>
                            <li id="tab6"><a href="#vehicleRoadTax"><span>Road Tax</span></a></li>
                            <li id="tab7"><a href="#vehiclePermit"><span>Vehicle Permit</span></a></li>-->

                        </ul>
                        <div id="vehicleInfo">
                            <input type="hidden" name="regNoCheck" id="regNoCheck" value='' >
                            
                            <table border="0" class="border" align="center" width="1000" cellpadding="0" cellspacing="0" id="bg">
                                <tr>
                                    <td colspan="6">
                                        <table class="table table-info mb30 table-hover">
                                            <thead>
                                            <th colspan="6"  height="30">
                                            <div  align="center">Vehicle </div>
                                            </th>
                                            </thead>
                                            <tr width="100%">
                                            <div id="groupInfo" style="display: none;">

                                                <td height="30" width="20%"><font color="red" size="4">*</font><font style="bold">Vehicle OwnerShip</font></td>
                                                <td width="20%">  <select class="form-control" name="ownerShips" id="ownerShips"  style="width:250px;" disabled>
                                                        <option value="0">---select---</option>
                                                        <option value="1" >CA LOG</option>
                                                        <option value="2" >Dedicate</option>
                                                        <option value="3" >Hire </option>
                                                        <option value="4" >Replacement</option>
                                                        <input type="hidden" name="groupNo" id="groupNo" class="form-control" value="1">
                                                    </select>
                                                    <script>
                                                        document.getElementById("ownerShips").value = '<c:out value="${ownership}"/>';
                                                    </script>
                                                    <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>" /></td>
                                            </div>

                                            <input type="hidden" name="asset" id="asset" class="form-control" value="1">
                                            <td  colspan="4">
                                                <div id="attachInfo" style="display: none">
                                                    <table width="100%" cellpadding="0" cellspacing="2"  id="vandornameId" >
                                                        <tr>
                                                            <td height="30%" width="20%"><font color="red">*</font>Vendor Name</td>

                                                            <td width="10%">
                                                                <c:if test = "${vendorNameList != null}" >
                                                                    <c:forEach items="${vendorNameList}" var="vnl">
                                                                        <input type="text" id ="vendorId" value='<c:out value="${vnl.vendorId}"/>' style="width:250px;"/>
                                                                        <input type="text" id ="vendorName" value='<c:out value="${vnl.vendorName}"/>' style="width:250px;"/>
                                                                    </c:forEach >
                                                                </c:if>
                                                                <c:if test = "${leasingCustList != null}" >
                                                                    &emsp;&emsp;&emsp;&emsp;&emsp;&emsp; <select class="form-control" name="vendorId" id="vId"  style="width:250px;" disabled>
                                                                        <option value="0" selected>--Select--</option>
                                                                        <c:forEach items="${leasingCustList}" var="LCList">
                                                                            <option value='<c:out value="${LCList.vendorId}" />'><c:out value="${LCList.vendorName}" /></option>
                                                                        </c:forEach >
                                                                    </c:if>
                                                                </select>
                                                                <script>
                                                                    document.getElementById("vId").value = '<c:out value="${vendorId}"/>';
                                                                </script>        
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </td>
                                </tr>
                                <tr>
                                    <td height="30"><font color="red">*</font>Vehicle Number</td>
                                    <td height="30"><input maxlength='13' readonly name="regNo" type="text" class="form-control" value="<c:out value="${regNo}"/>" readonly style="width:250px;"></td>


                                    <td height="30" width="20%"><font color="red">*</font>MFRs</td>
                                    <td  width="20%" style="border-right-color:#5BC0DE;"><select class="form-control" name="mfrId" id="mfrId"  style="width:250px;" disabled>
                                            <option value="0">---Select---</option>
                                            <c:if test = "${MfrList != null}" >
                                                <c:forEach items="${MfrList}" var="Dept">
                                                    <option value='<c:out value="${Dept.mfrId}" />'><c:out value="${Dept.mfrName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <script>
                                            document.getElementById("mfrId").value = '<c:out value="${mfrId}"/>';
                                        </script>
                                    </td>


                                </tr>
                                <tr>
                                    <td height="30" width="20%"><font color="red">*</font>Vehicle Type</td>
                                    <td  width="20%">
                                        <input type="hidden" name="typeId" id="typeId" value="<c:out value="${typeId}"/>" />
                                        <input type="hidden" name="axleTypeId" id="axleTypeId" value="<c:out value="${axleTypeId}"/>" />
                                        <select class="form-control" name="vehicleTypeId" id="vehicleTypeId" disabled style="width:250px;">
                                            <option value="0">---Select---</option>
                                            <c:if test = "${TypeList != null}" >
                                                <c:forEach items="${TypeList}" var="Type">
                                                    <option value='<c:out value="${Type.typeId}" />~<c:out value="${Type.axleTypeId}" />'><c:out value="${Type.typeName}" /></option>
                                                </c:forEach >
                                            </c:if>
                                        </select>
                                        <c:if test="${vehicleId != null}">
                                            <script>
                                                document.getElementById("vehicleTypeId").value = '<c:out value="${typeId}"/>~<c:out value="${axleTypeId}"/>';
                                                    getModelAndTonnage('<c:out value="${typeId}"/>~<c:out value="${axleTypeId}"/>');
                                            </script>
                                        </c:if>
                                    </td>

                                    <td height="30" width="20%"><font color="red">*</font>Model </td>
                                    <td style="border-right-color:#5BC0DE;" height="30" >
                                        <select class="form-control" name="modelId" id="modelId" style="width:250px;" disabled>
                                            <option value="0">---Select---</option>
                                        </select>
                                        <script>
                                            document.getElementById("modelId").value = '<c:out value="${modelId}"/>'
                                        </script>
                                    </td>
                                </tr>
                            </table>
                            </td></tr>

                            <tr>
                                <td colspan="6">

                                    <div id="hiredGroup" >
                                        <table class="table table-info mb30 table-hover">
                                            <tr  width="20%">
                                                <td height="30" width="20%"><font color="red">*</font>Registration Date</td>

                                                <td height="30" width="20%"><input name="dateOfSale" type="text" class="datepicker" value="<c:out value="${dateOfSale}" />" size="20" style="width:250px;" disabled>

                                                </td>
                                                <!--                    <td></td>-->

                                                <td height="30"><font color="red">*</font>Fleet Center</td>
                                                <td  >
                                                    <select class="form-control" name="opId" id="opId"  style="width:250px;" disabled>
                                                        <c:if test = "${OperationPointList != null}" >
                                                            <c:forEach items="${OperationPointList}" var="Dept">
                                                                <option value='<c:out value="${Dept.opId}" />'> <c:out value="${Dept.opName}" /> </option>
                                                            </c:forEach >
                                                        </c:if>
                                                    </select>
                                                    <script>
                                                        document.getElementById("opId").value = '<c:out value="${opId}"/>';
                                                    </script>
                                                </td>
                                            </tr>

                                            <tr width="20%">
                                                <td height="30" ><font color="red">*</font>Engine No</td>
                                                <td height="30" ><input maxlength='20' name="engineNo" type="text" class="form-control"readonly value="<c:out value="${engineNo}"/>" size="20" style="width:250px;"></td>
                                                <td height="30" width="20%"><font color="red">*</font>Chassis No</td>
                                                <td height="30" width="20%"><input maxlength='20'  type="text" name="chassisNo" size="20" readonly class="form-control" value="<c:out value="${chassisNo}"/>" style="width:250px;"> </td>
                                            </tr>
                                            <tr>
                                                <td height="30" style="display: none"><font color="red">*</font>No Of Axles</td>
                                                <td height="30" style="display: none"><select name="axles" class="form-control" style="width:250px;" disabled>
                                                        <option value="2">Double Axle</option>
                                                        <option value="3">Triple Axle</option>
                                                        <option value="3">Four Axle</option>
                                                        <option value="3">Five Axle</option>
                                                        <option value="3">Six Axle</option>
                                                        <option value="3">More Than 6 Axle</option>
                                                    </select></td>
                                                <td height="30">Tonnage (MT)</td>
                                                <td height="30"><input name="seatCapacity" id="seatCapacity" type="text" readonly class="form-control" value="<c:out value="${seatCapacity}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" maxlength="15" value="0" size="20" style="width:250px;"></td>
                                                <td colspan="2">&nbsp;</td>
                                            </tr>
                                            <tr style="display:none">
                                                <td height="30"><font color="red">*</font>Warranty Date</td>
                                                <td height="30"><input name="warrantyDate" id="warrantyDate" type="text" disabled class="datepicker" value="<c:out value="${warrantyDate}"/>" size="20" onchange="CalWarrantyDate(this.value)" style="width:250px;"></td>
                                                <td height="30">Warranty (days)</td>
                                                <td height="30"><input name="war_period" id="war_period" type="text" readonly  class="form-control" value="<c:out value="${warPeriod}"/>" size="20"  onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"></td>
                                            </tr>
                                            <tr>
                                                <td height="30">Vehicle Cost As On Date</td>
                                                <td  ><input type="text" name="vehicleCost" class="form-control" maxlength="15" readonly value="<c:out value="${vehicleCost}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" onkeyup="setVehicleCost(this.value);" style="width:250px;"/></td>
                                                <td height="30">Depreciation(%)</td>
                                                <td  ><input type="text" name="vehicleDepreciation" id="vehicleDepreciation" readonly class="form-control" maxlength="15" value="<c:out value="${vehicleDepreciation}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"/></td>
                                            </tr>
                                            <script>
                                                function setVehicleCost(value) {
                                                    $("#vehicleDepreciationCost").val(value);
                                                }
                                            </script>
                                            <tr>
                                                <td height="30"><font color="red">*</font>Cold Storage</td>
                                                <td  ><select class="form-control" name="classId" id="classId" style="width:250px;" disabled>
                                                        <c:if test = "${ClassList != null}" >
                                                            <c:forEach items="${ClassList}" var="Dept">
                                                                <option value="<c:out value="${Dept.classId}" />"><c:out value="${Dept.className}" /></option>
                                                            </c:forEach >
                                                        </c:if>
                                                    </select>
                                                    <script>
                                                        document.getElementById("classId").value = "1012";
                                                    </script>
                                                </td>
                                                <td height="30"> <font color="red">*</font>KM Reading</td>
                                                <td  ><input maxlength='20'  name="kmReading" type="text" class="form-control" readonly value="<c:out value="${kmReading}"/>" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"></td>
                                            </tr>

                                            <tr>
                                                <td height="30"><font color="red">*</font>Daily Run KM</td>
                                                <td height="30"> <input  type="text"  id="dailyKm"  name="dailyKm" readonly class="form-control" maxlength="10" value="<c:out value="${dailyKm}"/>" size="20"  onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"> </td>
                                                <td height="30"> <font color="red">*</font>Daily Run HM</td>
                                                <td  > <input type="text" id="dailyHm" name="dailyHm"  class="form-control" readonly value="<c:out value="${dailyHm}"/>" maxlength="10" size="20" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:250px;"></td>
                                            </tr>
                                            <tr>
                                                <td height="30"> <font color="red">*</font>Vehicle Color</td>
                                                <td><input type="text"  name="vehicleColor" id="vehicleColor" class="form-control" readonly maxlength="20" value="<c:out value="${vehicleColor}"/>" onKeyPress="return onKeyPressBlockNumbers(event);" style="width:250px;"></td>
                                                <td height="30"><font color="red">*</font>Vehicle Usage</td>
                                                <td  ><select class="form-control" name="usageId"  style="width:250px;" disabled>
                                                        <option value="1" >Short Trip</option>
                                                        <option value="2" selected>Long Trip</option>
                                                    </select></td>
                                            </tr>
                                            <tr>

                                                <td height="30"> <font color="red">*</font>GPS Tracking System</td>
                                                <td  >
                                                    <select class="form-control" name="gpsSystem" id="gpsSystem" onchange="openGpsInfo(this.value)" style="width:250px;" disabled>
                                                        <option value='Yes' >Yes</option>
                                                        <option value='No' selected>No</option>
                                                    </select>
                                                    <script type="text/javascript">
                                                        function openGpsInfo(val) {
                                                            if (val == 'Yes') {
                                                                document.getElementById("trckingId").style.display = "block";
                                                            } else {
                                                                document.getElementById("trckingId").style.display = "none";
                                                            }
                                                        }
                                                    </script>
                                                </td>
                                                <td> &nbsp;<input type="hidden" name="groupId" value="0"/>Status</td> 
                                                <td>
                                                    <select name="activeInd" id="activeInd" style="width:250px;"  class="form-control" disabled>
                                                        <option value="Y">Active</option>
                                                        <option value="N">In-Active</option>
                                                    </select>
                                                </td>

                                            </tr>

                                            <tr>
                                                <td> Description</td>
                                                <td  colspan="3"><textArea id="description" name="description" cols="5" rows="5" style="width:250px;" readonly><c:out value="${description}" /></textArea></td>
                                                                        </tr>



                                <script type="text/javascript">


                                    function ChangeDropdowns(value) {

                                        if (value == "1") {
                                            document.getElementById("attachInfo").style.display = "none";
                                        }

                                        if (value == "2" || value == "3" || value == "4") {

                                            document.getElementById("attachInfo").style.display = "block";
                                        }
                                        //
                                        if (value == "1") {
                                            document.getElementById("hiredGroup").style.display = "block";
                                            //                         document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2" || value == "3" || value == "4") {
                                            document.getElementById("hiredGroup").style.display = "none";
                                        }
                                        if (value == "1") {
                                            document.getElementById("hiredGroup").style.display = "block";
                                            //                         document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("hiredGroup").style.display = "none";
                                        }
                                        if (value == "2" || value == "3" || value == "4") {

                                            document.getElementById("attachInfo").style.display = "block";
                                        }

                                        //    function hideAdd(value){

                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("add").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "1") {
                                            document.getElementById("add").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }

                                        //    function hideAdds(value){


                                        if (value == "1" || value == "2") {
                                            document.getElementById("adds").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4") {
                                            document.getElementById("adds").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }

                                        //       function hideAdds1(value){


                                        if (value == "1" || value == "3" || value == "4") {
                                            document.getElementById("adds1").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2") {
                                            document.getElementById("adds1").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }


                                        if (value == "2") {
                                            document.getElementById("groupNo").style.display = "block";

                                        }
                                        if (value == "1") {
                                            document.getElementById("groupNo").style.display = "block";
                                        }

                                        if (value == "4") {
                                            ocument.getElementById("groupNo").style.display = "block";
                                        }

                                        if (value == "3") {
                                            document.getElementById("groupNo").style.display = "block";
                                        }

                                        $('#vandornameId').show();

                                        if (value != "1") {
                                            document.getElementById("bankPayment1").style.display = "block";
                                            document.getElementById("bankPayment2").style.display = "block";
                                            document.getElementById("bankPayment3").style.display = "block";
                                            document.getElementById("bankPayment4").style.display = "block";
                                        }
                                        else {
                                            document.getElementById("bankPayment1").style.display = "none";
                                            document.getElementById("bankPayment2").style.display = "none";
                                            document.getElementById("bankPayment3").style.display = "none";
                                            document.getElementById("bankPayment4").style.display = "none";
                                        }

                                    }

                                    function hideAdd(value) {

                                        if (value == "3" || value == "4" || value == "2") {
                                            document.getElementById("add").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "1") {
                                            document.getElementById("add").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }
                                    function hideAdds(value) {


                                        if (value == "1" || value == "2") {
                                            document.getElementById("adds").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "3" || value == "4") {
                                            document.getElementById("adds").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }
                                    function hideAdds1(value) {


                                        if (value == "1" || value == "3" || value == "4") {
                                            document.getElementById("adds1").style.visibility = 'hidden';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                        if (value == "2") {
                                            document.getElementById("adds1").style.visibility = 'visible';
                                            //   document.getElementById('vendor').style.display='none';
                                        }
                                    }


                                                                                                                                                                         </script>
</table></div>
</td>
</tr>
<tr>
  <td height="30"> <font color="red">*</font><spring:message code="trucks.label.FastagTransactionId"  text="FastagTransactionId"/></td>
  <td ><input type="text"  name="fastagTransactionId" id="fastagTransactionId" class="form-control" maxlength="20" value="<c:out value="${fastagTransactionId}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:260px;height:40px;"></td>
                                                
</tr>
<tr>
                                <td colspan="6">
                                    <div id="trckingId" style="display: none;">
                                        <table width="50%" cellpadding="0" cellspacing="2">
                                            <tr>
                                                <td height="30"><font color="red">*</font>GPS System Id</td>
                                                <td align="center">
                                                    <select class="form-control" name="gpsSystemId" style="width:250px;">
                                                        <option value="1" selected>0001</option>
                                                        <option value="2" >0002</option>
                                                        <option value="3" >0003</option>
                                                    </select>
                                                </td>
                                                <td height="30">&nbsp;</td>
                                                <td>&nbsp;</td>

                                            </tr>
                                        </table>
                        </div> </td>
</tr>
</table>
                    <center>
                            <a  class="nexttab" ><input type="button" class="btn btn-success" value="Next" name="Next" style="width:100px;height:35px;"/></a>

                    </center>
                                                                        </table>
            </div>
            <div id="vehicleOem">
                <table class="table table-info mb30 table-hover" id="positionViewTable">
                <thead>
                    <tr>
                        <th>Axle Type</th>
                        <th>Left</th>
                        <th>Right</th>
                        <input type="hidden" name="selectedRowCount" id="selectedRowCount" value="0"/>
                    </tr>
                    </thead>
                </table>
                  <script type="text/javascript">
                      function getVehAxleName(str) {
                          //alert(str)
                          var axleTypeId = str.split("~")[1];
                          var typeId = axleTypeId;
                          $("#axleTypeId").val(axleTypeId);
                          //                        var typeId=document.getElementById("typeId").value;

                          var axleTypeName = "";
                          var leftSideTyreCount = 0;
                          var rightSideTyreCount = 0;
                          var axleDetailId = 0;
                          var count = 1;

                          $.ajax({
                              url: '/throttle/getVehAxleName.do',
                              // alert(url);
                              data: {typeId: typeId},
                              dataType: 'json',
                              success: function (data) {
                                  if (data !== '') {
                                      $.each(data, function (i, data) {
                                          axleTypeName = data.AxleTypeName;
                                          leftSideTyreCount = data.LeftSideTyreCount;
                                          rightSideTyreCount = data.RightSideTyreCount;
                                          axleDetailId = data.AxleDetailId;
                                          positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, count, axleDetailId);
                                          count++;
                                      });
                                  }
                              }
                          });
                      }


                      function positionAddRow(axleTypeName, leftSideTyreCount, rightSideTyreCount, rowCount, axleDetailId) {
                          var rowCount = "";
                          var style = "";

                          rowCount = document.getElementById('selectedRowCount').value;
                          rowCount++;
                          var tab = document.getElementById("positionViewTable");
                          var newrow = tab.insertRow(rowCount);
                          newrow.id = 'rowId' + rowCount;

                          cell = newrow.insertCell(0);
                          var cell2 = "<td  height='30'><input type='hidden' name='vehicleAxleDetailId' id='vehicleAxleDetailId" + rowCount + "' value='" + axleDetailId + "'  class='form-control' /><input type='hidden' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='" + axleTypeName + "'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "' readonly>" + axleTypeName + "</label></td>";
                          //                        var cell2 = "<td  height='30'><input type='text' name='vehicleFrontAxle' id='vehicleFrontAxle" + rowCount + "' value='"+ axleTypeName +"'  class='form-control' /><label name='vehicleFronts' id='vehicleFronts" + rowCount + "'>" + axleTypeName + "</label></td>";
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell2;

                          cell = newrow.insertCell(1);
                          var cell3 = "<td height='30'  name='leftTyreIds'  id='leftTyreIds2' colspan=" + leftSideTyreCount + ">";
                          for (var i = 0; i < leftSideTyreCount; i++) {
                              var pos = "";
                              if (axleTypeName != "Front") {
                                  if (i == 0) {
                                      pos = "Outer:";
                                  } else {
                                      pos = "Inner:";
                          }
                              }
                              cell3 += "<b>" + pos + "</b>Tyre No:<input type='text'  style='width:175px;' id='tyreNoLeft" + axleDetailId + parseInt(i + 1) + "' readonly/>Depth<input type='text' maxlength='3'  name='treadDepthLeft' id='treadDepthLeft" + axleDetailId + parseInt(i + 1) + "' style='width:55px;' onkeypress='return blockNonNumbers(this, event, true, false);' readonly/>mm <select name='leftItemIds' class='form-control' id='tyreMfrLeft" + axleDetailId + parseInt(i + 1) + "' disabled><option value='0'>--Tyre Make & Spec--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select><br>";
                          }
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell3 + "</td>";
                          for (var i = 0; i < leftSideTyreCount; i++) {
                              fillVehicleTyreDetails(axleDetailId, 0, i);
                          }

                          cell = newrow.insertCell(2);
                          var cell4 = "<td  height='30' name='rightTyreIds' id='rightTyreIds2' colspan=" + rightSideTyreCount + ">";
                          for (var j = 0; j < rightSideTyreCount; j++) {
                              var pos = "";
                              if (axleTypeName != "Front") {
                                  if (j == 0) {
                                      pos = "Outer:";
                                  } else {
                                      pos = "Inner:";
                          }
                              }
                              cell4 += "<b>" + pos + "</b>Tyre No:<input type='text'  style='width:175px;' id='tyreNoRight" + axleDetailId + parseInt(j + 1) + "' readonly/>Depth<input type='text' maxlength='3' name='treadDepthRight' id='treadDepthRight" + axleDetailId + parseInt(j + 1) + "'   style='width:55px;' onkeypress='return blockNonNumbers(this, event, true, false);' readonly/>mm<select name='rightItemIds' id='tyreMfrRight" + axleDetailId + parseInt(j + 1) + "' class='form-control' disabled><option value='0'>--Tyre Make & Spec--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select><br>";
                          }
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell4 + "</td>";
                          for (var j = 0; j < rightSideTyreCount; j++) {
                              fillVehicleTyreDetails(axleDetailId, 1, j);
                          }


                          cell = newrow.insertCell(3);
                          var cell5 = "<td  height='30'><input type='hidden' name='position' id='position'  value='" + parseInt(i) + "' ></td>";
                          cell.setAttribute("className", style);
                          cell.innerHTML = cell5;

                          document.getElementById('selectedRowCount').value = rowCount;


                      }

                      function fillVehicleTyreDetails(axleDetailId, positionName, positionNo) {
                          var vehicleId = $("#vehicleId").val();
                          var axleTypeId = $("#axleTypeId").val();
                          if (vehicleId != 0) {
                              positionNo = parseInt(positionNo + 1);
                              if (positionName == '0') {
                                  positionName = 'Left';
                              } else {
                                  positionName = 'Right';
                              }
                              $.ajax({
                                  url: '/throttle/getVehAxleTyreNo.do',
                                  // alert(url);
                                  data: {axleDetailId: axleDetailId, positionName: positionName,
                                      positionNo: positionNo, vehicleId: vehicleId, axleTypeId: axleTypeId},
                                  dataType: 'json',
                                  success: function (data) {
                                      if (data !== '') {
                                          $.each(data, function (i, data) {
                                              //                                                axleTypeName = data.AxleTypeName;
                                              document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = data.TyreNo;
                                              document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = data.TyreDepth;
                                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = data.TyreMfr;
                                          });
                                      }
                                  }
                              });
                          }
                          //                            alert(document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value);
                          //                            document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = 100;
                      }
                      function updateVehicleTyreDetails(axleDetailId, positionName, positionNo, val, updateType) {
                          var vehicleId = $("#vehicleId").val();
                          positionNo = parseInt(positionNo + 1);
                          var url = '';
                          if (positionName == '0') {
                              positionName = 'Left';
                          } else {
                              positionName = 'Right';
                          }
                          if (updateType == 1) {

                          }
                          var treadDepth = document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value;
                          var tyreNo = document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value;
                          if (treadDepth == '') {
                              alert("Please fill tread depth for " + positionName + " " + positionNo);
                              document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).focus();
                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                              return;
                          } else if (tyreNo == '') {
                              alert("Please fill tyre no for " + positionName + " " + positionNo);
                              document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                              document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                              return;
                          } else {
                              url = './updateVehicleTyreNo.do';
                              $.ajax({
                                  url: url,
                                  data: {
                                      axleDetailId: axleDetailId, positionName: positionName, positionNo: positionNo,
                                      updateValue: val, depthVal: treadDepth, updateType: updateType, vehicleId: vehicleId,
                                      tyreNo: tyreNo
                                  },
                                  type: "GET",
                                  success: function (response) {
                                      if (response.toString().trim() == 0) {
                                          document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).value = '';
                                          document.getElementById("treadDepth" + positionName + axleDetailId + positionNo).value = '';
                                          document.getElementById("tyreMfr" + positionName + axleDetailId + positionNo).value = 0;
                                          alert("Tyre No Already Mapped Please Enter Valid Tyre No");
                                          $("#StatusMsg").text("Vehicle TyreNo added sucessfully").css("color", "red");
                                          document.getElementById("tyreNo" + positionName + axleDetailId + positionNo).focus();
                                      } else {
                                          alert("Vehicle TyreNo Added Successfully");
                                          $("#StatusMsg").text("Vehicle TyreNo added sucessfully").css("color", "green");
                                      }
                                  },
                                  error: function (xhr, status, error) {
                                  }
                              });
                          }

                      }

                      //jp End Ajex insCompny list
                                                                                                </script>


                   <table class="table table-info mb30 table-hover"  id="oEMtable">
                    <thead>
                    <tr>
                        <th  align="center" height="30" >Sno</th>
                        <th   align="center" height="30">Stepney/Battery</th>
                        <th   align="center" height="30">Details</th>
                    </tr>
                    </thead>
                    
                 <input type="hidden" name="oemIdCount" id="oemIdCount" value="<c:out value="${oemListSize}"/>" />
                                  <!--<td><input type="button" name="AddRow" id="AddRow" value="AddRow" onclick="addOem(0, 1, '', '', '', 0, 0, 0);" class="btn btn-success" style="width:100px;height:35px;"/></td>-->
                <input type="hidden" name="rowCounted" id="rowCounted" value=""/>
               <script>
                   function vehicleStepneyDetails(val, sno) {

                       if (val == 1) {

                           $("#oemMfr" + sno).show();
                           $("#oemDepth" + sno).show();
                           $("#oemTyreNo" + sno).show();
                           $("#oemBattery" + sno).hide();
                           //                        
                       } else if (val == 2) {
                           $("#oemBattery" + sno).show();
                           $("#oemMfr" + sno).hide();
                           $("#oemDepth" + sno).hide();
                           $("#oemTyreNo" + sno).hide();

                       }
                   }

                                                                                                                  </script>
               <script>

                   function insertVehicleAxle() {

                       var vehicleId = $("#vehicleId").val();
                       var url = '';
                       var oemId = [];
                       var oemDetailsId = [];
                       var oemMfr = [];
                       var oemDepth = [];
                       var oemTyreNo = [];
                       var oemBattery = [];
                       var oemDetailsIds = document.getElementsByName("oemDetailsId");
                       var oemMfrs = document.getElementsByName("oemMfr");
                       var oemDepths = document.getElementsByName("oemDepth");
                       var oemTyreNos = document.getElementsByName("oemTyreNo");
                       var oemBatterys = document.getElementsByName("oemBattery");
                       var oemIds = document.getElementsByName("oemId");
                       for (var i = 0; i < oemDetailsIds.length; i++) {
                           oemDetailsId.push(oemDetailsIds[i].value);
                           oemMfr.push(oemMfrs[i].value);
                           oemTyreNo.push(oemTyreNos[i].value);
                           oemDepth.push(oemDepths[i].value);
                           oemBattery.push(oemBatterys[i].value);
                           oemId.push(oemIds[i].value);
                           if (oemDetailsIds[i].value == 1) {
                               if (oemMfrs[i].value == '') {
                                   alert('Please select MFR ');
                                   return;
                               } else if (oemTyreNos[i].value == '') {
                                   alert('Please Enter The TyreNo ');
                                   return;
                               } else if (oemDepths[i].value == '') {
                                   alert('Please Enter The Depth ');
                                   return;
                               }
                           } else if (oemDetailsIds[i].value == 2) {
                               if (oemBattery[i].value == '') {
                                   alert('Please select battty ');
                                   return;
                               }
                           }

                       }
                       var insertStatus = 0;
                       var oemIdResponse = "";
                       url = './insertOemDetails.do';
                       $("#oEmNext").hide();
                       $.ajax({
                           url: url,
                           data: {oemDetailsIdVal: oemDetailsId, vehicleId: vehicleId, oemMfrVal: oemMfr, oemTyreNoVal: oemTyreNo, oemDepthVal: oemDepth,
                               oemBatteryVal: oemBattery, oemIdVal: oemId
                           },
                           type: "GET",
                           dataType: 'json',
                           success: function (data) {
                               var r = 0;
                               $.each(data, function (i, data) {
                                   //                                alert(data.OemId);
                                   oemIds[r].value = data.OemId;
                                   r++;
                               });
                               $("#StatusMsg").text("Vehicle VehicleAxle added sucessfully").css("color", "green");
                               alert("Vehicle VehicleAxle added sucessfully");
                               $("#oEmNext").show();
                           },
                           error: function (xhr, status, error) {
                           }
                       });
                       //                        $("#oEmNext").hide();
                   }

                   function addOem(sno, oemDetailsId, batteryNo, tyreNo, depth, mfrId, oemId, addType) {
                       var rowCount = sno;
                       var style = "";
                       rowCount = document.getElementById('rowCounted').value;
                       rowCount++;


                       var tab = document.getElementById("oEMtable");
                       var newrow = tab.insertRow(rowCount);
                       newrow.id = 'rowId' + rowCount;

                       cell = newrow.insertCell(0);
                       var cell0 = "<td  height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell0;

                       cell = newrow.insertCell(1);
                       var cell1 = "";
                       var oemDetailsName = "";
                       if (oemDetailsId == 1) {
                           oemDetailsName = "Stepney";
                       } else {
                           oemDetailsName = "Battery";
                       }
                       if (addType == 1) {
                           cell1 = "<td  height='30'><input type='hidden' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "'>'" + oemDetailsName + "'</td>";
                       } else {
                           cell1 = "<td  height='30'><select class='form-control' id='oemDetailsId" + rowCount + "' style='width:125px'  name='oemDetailsId' value='" + oemDetailsId + "' onchange='batteryTextHideShow(this.value," + rowCount + ");' disabled><option value='1'>Stepney</option><option value='2'>battery</option></select></td>";
                       }
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell1;

                       cell = newrow.insertCell(2);
                       var cell2 = "<td  height='30'  ><span id='batterySpan" + rowCount + "' style='display:none'>Bat:<input type='text'  name='oemBattery' id='oemBattery" + rowCount + "' maxlength='13' placeholder='BatteryNo'  size='20' class='form-control' value='" + batteryNo + "'  readonly/></span><span id='tyreSpan" + rowCount + "'>Tyre Make & Spec:<select name='oemMfr' id='oemMfr" + rowCount + "' class='form-control' disabled ><option value=0>--Select--</option><c:if test = "${tyreItemList != null}" ><c:forEach items="${tyreItemList}" var="Type"><option value='<c:out value="${Type.itemId}" />'><c:out value="${Type.itemName}" /></option></c:forEach > </c:if></select>\n\
                                                                                                                                        TyreNo:<input type='text'  name='oemTyreNo' id='oemTyreNo" + rowCount + "' placeholder='TyreNo'   size='20' class='form-control' value='" + tyreNo + "' readonly />Depth:<input type='text'  name='oemDepth' id='oemDepth" + rowCount + "' maxlength='13'   size='20' class='form-control' placeholder='TyreDepth' value='" + depth + "' readonly/>mm</span></td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell2;
                       $("#oemMfr" + rowCount).val(mfrId);
                       if (addType == 1) {
                           if (oemDetailsId == 2) {
                               $("#batterySpan" + rowCount).show();
                               $("#tyreSpan" + rowCount).hide();
                           }
                       }
                       cell = newrow.insertCell(3);
                       var cell3 = "<td  height='25' ><input type='hidden' name='oemId' id='oemId" + rowCount + "' value='" + oemId + "'  class='form-control' /></td>";
                       cell.setAttribute("className", style);
                       cell.innerHTML = cell3;
                       document.getElementById('rowCounted').value = rowCount;
                   }

                   function batteryTextHideShow(value, rowCount) {
                       if (value == 1) {
                           $("#batterySpan" + rowCount).hide();
                           $("#tyreSpan" + rowCount).show();
                       } else {
                           $("#batterySpan" + rowCount).show();
                           $("#tyreSpan" + rowCount).hide();
                       }
                   }

                                                      </script>  
                                <%int addRowCount = 1;%>    
                                <c:if test="${oemList != null}">
                                    <c:forEach items="${oemList}" var="item">
                        <script>
                            // alert("v");
                            addOem('<%=addRowCount%>', '<c:out value="${item.oemDetailsId}"/>', '<c:out value="${item.oemBattery}"/>', '<c:out value="${item.oemTyreNo}"/>', '<c:out value="${item.oemDepth}"/>', '<c:out value="${item.oemMfr}"/>', '<c:out value="${item.oemId}"/>', 1);
                                                        </script>
                                    </c:forEach>
                                </c:if>                     
                       
                </table>  
                            

                <br>
                <center>
                    &emsp;<a  class="nexttab" ><input type="button" class="btn btn-success" value="Next" name="Next" style="width:100px;height:35px;"/></a>

                </center>

                
            </div>
                   


            <div id="vehicleDepreciationTab">
                <table class="table table-info mb30 table-hover" >
                    <tr>
                        <td >Vehicle Cost</td>
                        <td ><input type="text" name="vehicleDepreciationCost" id="vehicleDepreciationCost" class="form-control" value='<c:out value="${vehicleCost}"/>' style="width:250px;" readonly></td>
                        
                        <td >Depreciation Year</td>
                        <td >

                            <select name="depreciationType" id="depreciationType" class="form-control"  style="width:250px;" onChange="removeRowDetails();" disabled>
                                <option value="0">Select</option>
                                <option value="1">1year</option>
                                <option value="2">2year</option>
                                <option value="3">3year</option>
                                <option value="4">4year</option>
                                <option value="5">5year</option>
                            </select>
                        </td>
                        <script>
                            document.getElementById("depreciationType").value = '<c:out value="${depreciationSize}"/>';
                        </script>
                    </tr>
                   
                </table>
              <table class="table table-info" id="vehicleCostTable">
                     <thead>
                        <th  align="center" height="30" >Sno</th>
                        <th   align="center" height="30">Year</th>
                        <th  height="30" >Depreciation(%)</th>
                        <th  height="30" >Annual Depreciation Cost</th>
                        <th  height="30" >Monthly Depreciation Cost</th>
                        </thead>
                                <%int rcount = 1;%>
                                <c:if test="${depreciationList != null}">
                                    <c:forEach items="${depreciationList}" var="dep">
                        <tr>
                            <td  height='25' ><input type='hidden' name='serNO' id='serNO<%=rcount%>' value='<%=rcount%>'  class='form-control' /><%=rcount%></td>
                            <td  height='30'>
                                <input type='hidden' name='vehicleYear' id='vehicleYear<%=rcount%>' value='<%=rcount%>'  class='form-control' />
                                <label name='vehicleYears' id='vehicleYears<%=rcount%>'><%=rcount%>-Year</label>
                            </td>
                            <td height='30' >
                                <input type='text' value='<c:out value="${dep.depreciation}"/>' name='depreciation' id='depreciation<%=rcount%>' maxlength='13'   size='20' class='form-control'  onchange="vehicleCostCalculation('<%=rcount%>');"  readonly/>
                            </td>
                            <td  height='30'><input type='hidden' value='<c:out value="${dep.yearCost}"/>' name='yearCost' id='yearCost<%=rcount%>' maxlength='13'   size='20' class='form-control'    /><span id='yearCostSpan<%=rcount%>' readonly><c:out value="${dep.yearCost}"/></span></td>
                            <td height='30' ><input type='hidden' value='<c:out value="${dep.perMonth}"/>' name='perMonth' id='perMonth<%=rcount%>' maxlength='13'   size='20' class='form-control' onchange='perMonthCalculation(<%=rcount%>);'  /><span id='perMonthSpan<%=rcount%>' readonly><c:out value="${dep.perMonth}"/></span></td>
                        </tr>
                                        <%rcount++;%>
                                    </c:forEach>
                                </c:if>
                                                                                                                            
                                                                                                                            
                </table>
                <br>
                <br>
                <center>
                    <a  class="nexttab" ><input type="button" class="btn btn-success" value="Next" name="Next" style="width:100px;height:35px;" /></a>
                </center>
                <script type="text/javascript">
                    function saveVehicleDepreciation() {
                        var depreciationType = document.getElementById("depreciationType").value;
                        var vehicleYear = document.getElementsByName("vehicleYear");
                        var depreciation = document.getElementsByName("depreciation");
                        var yearCost = document.getElementsByName("yearCost");
                        var perMonth = document.getElementsByName("perMonth");
                        var status = 0;
                        for (var i = 1; i <= depreciationType; i++) {
                            if ($("#depreciation" + i).val() == '') {
                                var vehicleYear = $("#vehicleYear" + i).val();
                                alert("Please enter the depreciation for Year " + vehicleYear);
                                $("#depreciation" + i).focus();
                                return;
                            } else {
                                if (i == 1) {
                                    vehicleYear = $("#vehicleYear" + i).val();
                                    depreciation = $("#depreciation" + i).val();
                                    yearCost = $("#yearCost" + i).val();
                                    perMonth = $("#perMonth" + i).val();
                                } else {
                                    vehicleYear = vehicleYear + "~" + $("#vehicleYear" + i).val();
                                    depreciation = depreciation + "~" + $("#depreciation" + i).val();
                                    yearCost = yearCost + "~" + $("#yearCost" + i).val();
                                    perMonth = perMonth + "~" + $("#perMonth" + i).val();
                                }
                                status = 1;
                            }
                            //alert("i == " + i);
                        }
                        if (status == 1) {
                            var responseStatus = 0;
                            var vehicleId = $("#vehicleId").val();
                            var url = '';
                            url = './saveVehicleDepreciation.do';
                            $.ajax({
                                url: url,
                                data: {vehicleYear: vehicleYear, depreciation: depreciation, yearCost: yearCost, perMonth: perMonth, vehicleId: vehicleId
                                },
                                type: "GET",
                                success: function (response) {
                                    responseStatus = response.toString().trim();
                                    if (responseStatus == 0) {
                                        $("#StatusMsg").text("Vehicle Depreciation added failed ").css("color", "red");
                                    } else {
                                        $("#StatusMsg").text("Vehicle Depreciation added sucessfully ").css("color", "green");
                                        alert("Vehicle Depreciation added sucessfully");
                                        $("#depreciationSave").hide();
                                    }

                                },
                                error: function (xhr, status, error) {
                                }
                            });
                        }

                    }
                    function removeRowDetails() {
                        var tab = document.getElementById("vehicleCostTable");
                        //find current no of rows
                        var rowCountLength = document.getElementById('vehicleCostTable').rows.length;
                        rowCountLength = parseInt(rowCountLength);
                        for (var s = rowCountLength; s > 1; s--) {
                            $('#vehicleCostTable tr:last-child').remove();
                        }
                        vehicleCostAddRow();
                    }

                    function vehicleCostAddRow() {
                        var rowCount = "";
                        var style = "text2";
                        var axleCount = document.getElementById('depreciationType').value;
                        for (var i = 0; i < parseInt(axleCount); i++) {

                            var tab = document.getElementById("vehicleCostTable");
                            var rowCount = document.getElementById("vehicleCostTable").rows.length;
                            var newrow = tab.insertRow(rowCount);
                            newrow.id = 'rowId' + rowCount;

                            var cell = newrow.insertCell(0);
                            var cell1 = "<td  height='25' ><input type='hidden' name='serNO' id='serNO" + rowCount + "' value='" + rowCount + "'  class='form-control' />" + rowCount + "</td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell1;

                            cell = newrow.insertCell(1);
                            var cell2 = "<td  height='30'><input type='hidden' name='vehicleYear' id='vehicleYear" + rowCount + "' value='" + rowCount + "'  class='form-control' /><label name='vehicleYears' id='vehicleYears" + rowCount + "'>" + (rowCount) + "-Year</label></td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell2;

                            cell = newrow.insertCell(2);
                            var cell3 = "";
                            if (parseInt(axleCount - 2) == parseInt(i)) {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");lastRowPercent(" + rowCount + ")' /></td>";
                            } else if (parseInt(axleCount - 1) == parseInt(i)) {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");' readonly /></td>";
                            } else {
                                cell3 = "<td height='30' ><input type='text' value = '' name='depreciation' id='depreciation" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='vehicleCostCalculation(" + rowCount + ");' /></td>";
                            }
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell3;

                            cell = newrow.insertCell(3);
                            var cell4 = "<td  height='30'><input type='hidden' value = '' name='yearCost' id='yearCost" + rowCount + "' maxlength='13'   size='20' class='form-control'    /><span id='yearCostSpan" + rowCount + "'>0.00</span></td>";
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell4;

                            cell = newrow.insertCell(4);
                            var cell5 = "";
                            if (parseInt(axleCount - 1) == parseInt(i)) {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            } else if (parseInt(axleCount) == parseInt(i) + 1) {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control'  onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            } else {
                                cell5 = "<td height='30' ><input type='hidden' value = '' name='perMonth' id='perMonth" + rowCount + "' maxlength='13'   size='20' class='form-control' onchange='perMonthCalculation(" + rowCount + ");'  /><span id='perMonthSpan" + rowCount + "'>0.00</span></td>";
                            }
                            cell.setAttribute("className", style);
                            cell.innerHTML = cell5;

                        }

                    }


                    function lastRowPercent(rowCount) {
                        var depreciation = document.getElementsByName("depreciation");
                        var yearCost = document.getElementsByName("yearCost");
                        var perMonth = document.getElementsByName("perMonth");
                        var depreciationLength = parseInt(depreciation.length - 1);
                        var percentSum = 0;
                        var row = parseInt(rowCount + 1);
                        for (var i = 0; i < depreciationLength; i++) {
                            percentSum += parseInt(depreciation[i].value);
                        }
                        var balancePercent = parseInt(100 - percentSum);
                        if (balancePercent <= 0) {
                            alert("Depreciation planning is incorrect please plan again");
                            for (var i = 1; i <= depreciationLength; i++) {
                                $("#depreciation" + i).val('');
                                $("#yearCost" + i).val('');
                                $("#yearCostSpan" + i).text('0.00');
                                $("#perMonth" + i).val('');
                                $("#perMonthSpan" + i).text('0.00');
                            }
                            $("#depreciation" + row).val('');
                            $("#yearCost" + row).val('');
                            $("#yearCostSpan" + row).text('0.00');
                            $("#perMonth" + row).val('');
                            $("#perMonthSpan" + row).text('0.00');
                        } else {
                            var row = parseInt(rowCount + 1);
                            $("#depreciation" + row).val(balancePercent);
                            vehicleCostCalculation(row);
                            $("#depreciation" + 1).focus();
                        }
                    }
                    function vehicleCostCalculation(rowCount) {
                        for (var i = 0; i < parseInt(rowCount); i++) {
                            var depCost = document.getElementById('depreciation' + rowCount).value;
                            var vehicleCost = document.getElementById('vehicleDepreciationCost').value;
                            var depCostPercent = parseInt(depCost) / 100;
                            var depCostPerYear = parseFloat(depCostPercent * parseInt(vehicleCost)).toFixed(2);
                            document.getElementById('yearCost' + rowCount).value = depCostPerYear
                            $('#yearCostSpan' + rowCount).text(depCostPerYear);
                            var perMothCost = parseFloat(depCostPerYear / 12);
                            document.getElementById('perMonth' + rowCount).value = perMothCost.toFixed(2);
                            $('#perMonthSpan' + rowCount).text(perMothCost.toFixed(2));
                        }
                    }



                                                                                                                                                    </script>
            </div>            
            <div id="vehicleFileAttachments">
                
                   <script type="text/javascript">

                       $(document).ready(function ()
                       {
                           $("#uploadBtn").click(function ()
                           {
                               var uploadRemarks = "";
                               var count = 0;
                               $('input[name="file"]').each(function (index, value)
                               {
                                   var file = value.files[0];
                                   if (file)
                                   {
                                       var vehicleId = document.addVehicle.vehicleId.value;
                                       uploadRemarks = $("#narration" + count).val();
                                       var formData = new FormData();
                                       formData.append('file', file);
                                       $.ajax({
                                           url: './uploadVehicleDocuments.do?narrationVal=' + uploadRemarks + '&vehicleId=' + vehicleId,
                                           type: 'POST',
                                           data: formData,
                                           cache: false,
                                           contentType: false,
                                           processData: false,
                                           success: function (data, textStatus, jqXHR) {
                                               var message = jqXHR.responseText;
                                               // $("#messages").append("<li>" + message + "</li>");
                                               $("#StatusMsg").text("Files Uploaded sucessfully ").css("color", "green");
                                               alert("Files Uploaded sucessfully");
                                           },
                                           error: function (jqXHR, textStatus, errorThrown) {
                                               // $("#messages").append("<li style='color: red;'>" + textStatus + "</li>");
                                           }
                                       });
                                   }
                                   count++;
                               });
                           });
                       });
                                                                                                                                           </script>

         

                            <c:if test="${vehicleUploadsDetails != null}">
                                <%int img = 0;%>
                            <table class="table table-info"   style="width: 950px;" align="center">
                                <thead>
                                <th>SNo</th>
                                <th>&emsp;&emsp;&emsp;&emsp;File Name</th>
                                <th>&nbsp;DocumentType</th>
                                <th>&nbsp;Expiry Date</th>
                                <th>&nbsp;Remarks</th>
                                <th>&nbsp;Action</th>
                                </thead>
                                    <c:forEach items="${vehicleUploadsDetails}" var="cust">
                                    <tr>
                                        <td>&nbsp;<%=img + 1%></td>
                                        <td>&emsp;&emsp;<c:out value ="${cust.fileName}"/></td>
                                        <td>
                                            <c:if test="${cust.documentType == '0'}">
                                                Vehicle
                                            </c:if> 
                                            <c:if test="${cust.documentType == '1'}">
                                                State permit
                                            </c:if> 
                                            <c:if test="${cust.documentType == '2'}">
                                                National permit
                                            </c:if> 
                                            <c:if test="${cust.documentType == '101'}">
                                                Insurance
                                            </c:if> 
                                            <c:if test="${cust.documentType == '102'}">
                                                Road Tax
                                            </c:if> 
                                            <c:if test="${cust.documentType == '108'}">
                                                FC
                                            </c:if> 
                                        </td>
                                        <td>
                                            <c:if test="${cust.documentType == '0'}">
                                                -
                                            </c:if> 
                                            <c:if test="${cust.documentType == '1'}">
                                                <c:out value ="${cust.stateExpiry}"/>
                                            </c:if> 
                                            <c:if test="${cust.documentType == '2'}">
                                                <c:out value ="${cust.nationalExpiry}"/>
                                            </c:if> 
                                            <c:if test="${cust.documentType == '101'}">
                                                <c:out value ="${cust.insuranceExpiry}"/>
                                            </c:if> 
                                            <c:if test="${cust.documentType == '102'}">
                                                <c:out value ="${cust.roadExpiry}"/>
                                            </c:if> 
                                            <c:if test="${cust.documentType == '108'}">
                                                <c:out value ="${cust.fcExpiry}"/>
                                            </c:if> 
                                        </td>
                                        <td><c:out value ="${cust.remarks}"/></td>
                                        <td > 
                                            <!--<img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>"  style="width: 90px;height: 40px" data-toggle="modal" data-target="#myModal<%=img%>" title="<c:out value ="${cust.remarks}"/>"/>-->
                                            <a href="#" onclick="showPdf(<c:out value ="${cust.uploadId}"/>);">
		                                                 <img src="images/download.png" title="click here to download" style="height:30px;width:30px;margin-left:-01px;">
		                                             </a> &emsp; 
<!--                                            <a href="#" onclick="deleteVehUploadData(<c:out value ="${cust.uploadId}"/>);">
		                                               <img src="images/delete.jpg" title="click here to delete" style="height:30px;width:30px;margin-left:-01px;">
                                                             </a>  -->
                                        </td>
                                    </tr>
                                    <div class="modal fade" id="myModal<%=img%>" role="dialog" style="width: 100%;height: 100%">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" onclick="resetTheSlabDetails()">&times;</button>
                                                    <h4 class="modal-title"><c:out value ="${cust.fileName}"/></h4>
                                                </div>
                                                <div class="modal-body" id="slabRateListSet" style="width: 100%;height: 80%">
                                                    <img src="/throttle/displayVehicleLogoBlobData.do?uploadId=<c:out value ="${cust.uploadId}"/>" style="width: 95%;height: 75%" class="img-responsive" title="<c:out value ="${cust.remarks}"/>">
                                                </div>
                                                <div class="modal-footer">


                                                </div>
                                            </div>
                                        </div>
                                    </div>   
                                    <input  type="hidden" id="uploadId" name="uploadId" value="<c:out value ="${cust.uploadId}"/>"/>
                                        <%img++;%>
                                    </c:forEach>
                            </table>
                            </c:if>
                            <c:if test="${vehicleUploadsDetails == null}">
        
        <table class="table table-info" id="fileAddRow" width="700">
                       <thead>
                         <th>SNo</th>
                         <th>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;File</th>
                         <th>&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;Remarks</th>
                         <th>&nbsp;Delete</th>
                </thead>
               
            </table>
        <p><br>
            </p>
            
                            </c:if>
            
           
                <script>
                    var rowCount = 1;
                    var sno = 0;
                    var style = "text2";
                    var align = "center";
                    function addRow()
                    {
                        // alert("swdbgf");
                        if (rowCount % 2 == 0) {
                            style = "text2";
                        } else {
                            style = "text1";
                        }

                        var serialNo = parseInt(sno) + 1;
                        var tab = document.getElementById("fileAddRow");
                        var newrow = tab.insertRow(rowCount);

                        var cell = newrow.insertCell(0);
                        var cell1 = "<td  height='25' >" + serialNo + "</td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell1;

                        cell = newrow.insertCell(1);
                        var cell2 = "<td  height='30'><input type='file' name='file' size='110'  multiple /></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell2;

                        cell = newrow.insertCell(2);
                        var cell3 = "<td  height='30'><textarea name='narration' class='form-control'  id='narration" + sno + "'  style='width:300px;'/></textarea></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell3;
                        cell = newrow.insertCell(3);
                        var cell4 = "<td align='left' ><input type='checkbox' class='checkbox form-control' id='selectedIndex" + sno + "' name='selectedIndex' value='" + sno + "'style='width:15px;'/></td>";
                        cell.setAttribute("align", align);
                        cell.innerHTML = cell4;

                        var temp = sno - 1;

                        rowCount++;
                        sno++;

                    }
                    function DeleteRow() {
                        document.getElementById('AddRow').style.display = 'block';
                        try {
                            var table = document.getElementById("fileAddRow");
                            rowCount = table.rows.length;
                            // alert(rowCount);
                            for (var i = 1; i < rowCount; i++) {
                                var row = table.rows[i];
                                var checkbox = row.cells[3].childNodes[0];
                                if (null != checkbox && true == checkbox.checked) {
                                    if (rowCount <= 1) {
                                        alert("Cannot delete all the rows");
                                        break;
                                    }
                                    table.deleteRow(i);
                                    rowCount--;
                                    i--;
                                    sno--;
                                    // snumber--;
                                }
                            }
                        } catch (e) {
                            alert(e);
                        }
                    }
                                        </script>

            </div>
                                
                                <div style="display:none;" id="vehicleInsuranceeDetail">
                                    <c:if test="${vehicleInsuranceDetails == null}" >
                                        <font color="red" ><center>Insurance Not Add For This Vehicle </center></font>
                                        </c:if>
                                           
                                        <c:if test="${vehicleInsuranceDetails != null }" >
        <c:forEach items="${vehicleInsuranceDetails}" var="ins" >
            <c:if test = "${oldVehicleInsurance != null}">
    <c:forEach items="${oldVehicleInsurance}" var="oldIn">
        <c:set var="activeInd" value="${oldIn.activeInd}"/>
        <%--<c:out value="${oldIn.activeInd}"/>--%>
    </c:forEach>
     <c:if test="${activeInd == 'N'}">
<table align="right">
                        <tr align="right">
                            <td align="right"><a href='/throttle/viewOldVehicleInsurance.do?vehicleId=<c:out value="${vehicleId}" />&regNo=<c:out value="${regNo}"/>&insuranceId=<c:out value="${insuranceId}"/>&listId=7&fleetTypeId=1'><font color="blue" >Old Insurance Details</font></a></td>
                        </tr>
                    </table>
                            <br>
</c:if>
                    
</c:if>
<table class="table table-info mb30 table-hover">
<thead>
<tr>
<th colspan="6"  height="30">
<div  align="center"><spring:message code="trucks.label.AddInsurance"  text="default text"/></div>
</th>
</tr>
</thead>
<tr>
<td height="30"><spring:message code="trucks.label.Policy"  text="default text"/></td>
<td height="30">
<select class="form-control" style="width:260px;height:40px;"  name="insPolicy" id="insPolicy" style="width:260px;height:40px;" onchange="dispalyPackageType(this.value);" disabled>
    <option value="0"><spring:message code="trucks.label.New"  text="default text"/></option>
    <option value="1"><spring:message code="trucks.label.Renewal"  text="default text"/></option>
</select>
</td>
<script>
       document.getElementById("insPolicy").value = '<c:out value="${ins.insPolicy}" />';
</script>
<td height="30" style ="display:none" id ="labelPrevPolicy"><spring:message code="trucks.label.PreviousPolicyNo"  text="default text"/></td>
<td height="30"  id ="fieldPrevPolicy"   >
<input name="prevPolicy" id ="prevPolicy" readonly style ="display:none;width:250px" maxlength="25" type="text" value="" onkeypress="return blockNonNumbers(this, event, true, false);">
</td>
</tr>
<tr>
<td height="30"><spring:message code="trucks.label.InsuranceType"  text="default text"/> :</td>
<td height="30">
<select class="form-control" style="width:260px;height:40px;" readonly name="packageType" id="packageType" style="width:260px;height:40px;" disabled>
    <option value="1"><spring:message code="trucks.label.ComprehensivePolicy"  text="default text"/></option>
    <option value="2"><spring:message code="trucks.label.ThirdPartyClaim"  text="default text"/></option>
</select>
</td>
<script>
                            document.getElementById("packageType").value = '<c:out value="${ins.packageType}" />';
                                                   </script>
<td height="30" id ="blankid1">&nbsp;</td>
<td height="30"  id ="blankid2">&nbsp;</td>
</tr>
<td colspan="6"  height="30">
<div  align="center"><b><spring:message code="trucks.label.OwnerDetails"  text="default text"/></b></div>
</td>
</tr>
<tr>
<td height="30"><spring:message code="trucks.label.Name"  text="default text"/></td>
<td height="30">
<input type="text" name="insName" id="insName" readonly class="form-control" style="width:260px;height:40px;" value='<c:out value="${ins.insName}" />' style="width:260px;height:40px;"></td>
<td height="30"><spring:message code="trucks.label.Address"  text="default text"/></td>
<td  colspan="3"><input type="textArea" name="insAddress" id="insAddress" value='<c:out value="${ins.insAddress}" />'   cols="20" rows="2" style="width:260px;height:40px;"  readonly></td> 
</tr>
<tr>
<td height="30"><spring:message code="trucks.label.MobileNo"  text="default text"/></td>
<td height="30">
<input type="text" name="insMobileNo" id="insMobileNo" readonly  class="form-control" style="width:260px;height:40px;" value='<c:out value="${ins.insMobileNo}" />' onkeypress="return blockNonNumbers(this, event, true, false);"></td>
<td height="30"></td>
<td height="30"></td>
</tr>
<td colspan="6"  height="30">
<div  align="center"><b><spring:message code="trucks.label.InsuranceDetails"  text="default text"/></b></div>
</td>
</tr>
<tr>
<td height="30"><spring:message code="trucks.label.PolicyNo"  text="default text"/></td>
<td height="30"><input name="insPolicyNo" id="insPolicyNo" readonly maxlength="45" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${ins.insPolicyNo}" />' style="width:260px;height:40px;"></td>
<td height="30"><spring:message code="trucks.label.IDVValue"  text="default text"/> :</td>
<td height="30">
<input name="insIdvValue" id="insIdvValue"  readonly style="width:260px;height:40px;" maxlength="7" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${ins.insIdvValue}" />' onkeypress="return blockNonNumbers(this, event, true, false);">
</td>
</tr>

<tr>
<td height="30"><font color="red">*</font><spring:message code="trucks.label.ValidFromDate"  text="default text"/></td>
<td><input name="fromDate1" id="fromDate1" type="text" readonly style="width:260px;height:40px;" class="form-control datepicker" id="fromDate1"   value='<c:out value="${ins.premiumPaidDate}" />' ></td>
<td height="30"><font color="red">*</font><spring:message code="trucks.label.ValidToDate"  text="default text"/></td>
<td>
<input name="toDate1" id="toDate1" type="text" readonly style="width:260px;height:40px;" class="form-control datepicker" id="toDate1"  value='<c:out value="${ins.expirydate}" />'  >
</td>
</tr>

<tr >
<input type="hidden" id="insuranceid" name="insuranceid" value=''>
<input type="hidden" id="clearanceId" name="clearanceId" value=''>
<input type="hidden" id="cashOnHandLedgerId" name="cashOnHandLedgerId" value='<%=ThrottleConstants.CashOnHandLedgerId%>'>
<input type="hidden" id="cashOnHandLedgerCode" name="cashOnHandLedgerCode" value='<%=ThrottleConstants.CashOnHandLedgerCode%>'>
<td height="30"><spring:message code="trucks.label.PremiumAmount"  text="default text"/></td>
<td height="30"><input name="premiunAmount" readonly id="premiunAmount" maxlength="7"  type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${ins.premiumamount}" />' onkeypress="return blockNonNumbers(this, event, true, false);" onchange="premiumAmountSet(this.value);"></td>
<td id="paymentDateIns" style="visibility: hidden;"><font color="red">*</font><spring:message code="trucks.label.PaymentDate"  text="default text"/></td>
<td id="paymentDateIns1" style="visibility: hidden;" height="30"><input style="width:260px;height:40px;" name="paymentDate" id="paymentDate" type="text" class="datepicker" value='<c:out value="${ins.paymentDate}" />' ></td>
</tr>
<tr>
<td id="vendorIns" style="visibility: hidden;" ><spring:message code="trucks.label.InsuranceCompany"  text="default text"/></td>
<td id="vendorIns1" style="visibility: hidden;"><input type="text" name="vendorId1" id="vendorId1" readonly class="form-control" value="<c:out value="${ins.vendorName}" />" style="width:260px;height:40px;" >
<!--    <c:if test="${vendorList != null}">
<option value="0" >---<spring:message code="trucks.label.Select"  text="default text"/>---</option>
        <c:forEach items="${vendorList}" var="vendor">
<option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
        </c:forEach>
    </c:if>          
</select>
<script>
                            document.getElementById("vendorId1").value = '<c:out value="${ins.vendorId}" />'
                                                   </script>-->
</td>  

<td id="bankPaymentModeIns" style="visibility: hidden;" height="30"><spring:message code="trucks.label.PaymentMode"  text="Payment Mode"/>:</td>
<td id="bankPaymentModeIns1" style="visibility: hidden;" height="30" >
<select class="form-control" readonly style="width:260px;height:40px;" name="paymentType" id="paymentType" style="width:260px;height:40px;" onchange="getBankPayment(this.value)" disabled >
<option value="0">----<spring:message code="trucks.label.Select"  text="default text"/>----</option>
<!--<option value="1">Cash Payment</option>-->
<option value="2"><spring:message code="trucks.label.BankPayment"  text="default text"/></option>
</select> 
</td>
                    <script>
document.getElementById("paymentType").value = '<c:out value="${ins.paymentType}"/>'
</script>
</tr>
<tr>
<td><spring:message code="trucks.label.IssuingOffice"  text="default text"/></td>
<td>
<input name="issuingOffice" readonly id="issuingOffice" maxlength="45" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${ins.issuingOffice}"/>' style="width:260px;height:40px;">         
</select>
</td>  
<td  height="30"></td>
<td  height="30">

</td>
</tr>
<tr >
<tr id="bankPayment1" style="visibility: hidden;">
<td colspan="6"  height="30">
<div   align="center"><b><spring:message code="trucks.label.BankPaymentDetails"  text="default text"/></b></div>
</td>
</tr>
<tr id="bankPayment2" style="visibility: hidden;">
<td height="30"><spring:message code="trucks.label.BankHead"  text="default text"/></td>
<td height="30"><select name="bankId" readonly id="bankId" class="form-control" style="width:260px;height:40px;"  onchange="getBankBranch(this.value)" style="width:260px;height:40px;" disabled>
    <c:if test="${primarybankList != null}">
<option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
        <c:forEach items="${primarybankList}" var="bankVar">
<option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>

        </c:forEach>
    </c:if>          
</select>
<script>
document.getElementById("bankId").value = '<c:out value="${ins.bankId}"/>'
</script>
</td>
<td height="30"><spring:message code="trucks.label.Bankbranch"  text="default text"/> :</td>
<td height="30"> <select name="bankBranchId" readonly id="bankBranchId" class="form-control" style="width:260px;height:40px;" style="width:260px;height:40px;" disabled>
    <c:if test="${bankBranchList != null}">
<option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
        <c:forEach items="${bankBranchList}" var="branch">
<option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.branchName}"/></option>
        </c:forEach>
    </c:if>
</select>
</td>
<script>
document.getElementById("bankBranchId").value = '<c:out value="${ins.bankBranchId}"/>'
</script>
</tr>
<tr id="bankPayment3" style="visibility: hidden;">
<td ><font color="red">*</font><spring:message code="trucks.label.ChequeDate"  text="default text"/></td>
<td><input name="chequeDate" id="chequeDate" readonly type="text" class="form-control datepicker" value='<c:out value="${ins.chequeDate}"/>' style="width:260px;height:40px;"></td>
<td ><font color="red">*</font><spring:message code="trucks.label.ChequeNo"  text="default text"/></td>
<td><input name="chequeNo" id="chequeNo" readonly maxlength="45" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${ins.chequeNo}"/>' style="width:260px;height:40px;"></td>
</tr>
<tr id="bankPayment4" style="visibility: hidden;">
<td ><spring:message code="trucks.label.ChequeAmount"  text="default text"/></td>
<td ><input name="chequeAmount" id="chequeAmount" readonly maxlength="7" type="text" class="form-control" style="width:260px;height:40px;" value='<c:out value="${ins.chequeAmount}"/>' onkeypress="return blockNonNumbers(this, event, true, false);" readonly style="width:260px;height:40px;"></td>
<td></td>  
<td></td>  
</tr>
</tr>


<script>
   function premiumAmountSet(val) {
       document.getElementById("chequeAmount").value = val;
   }

   function getBankBranch(bankId) {
       var temp = "";
       $.ajax({
           url: '/throttle/getBankBranchDetails.do',
           data: {bankId: bankId},
           dataType: 'json',
           success: function (temp) {
//                                             alert(url);
               if (temp != '') {
                   $('#bankBranchId').empty();
                   $('#bankBranchId').append(
                           $('<option style="width:150px"></option>').val(0 + "~" + 0).html('---Select----')
                           )
                   $.each(temp, function (i, data) {
                       $('#bankBranchId').append(
                               $('<option value="' + data.branchId + "~" + data.bankLedgerId + '" style="width:150px"></option>').val(data.branchId + "~" + data.bankLedgerId).html(data.bankName)
                               )
                       document.getElementById("bankBranchId").value = '<c:out value="${vendorId1}"/>'
                   });
               } else {
                   $('#bankBranchId').empty();
               }
           }
       });
   }

   function getBankPayment(val) {
       var ownerShips = document.getElementById("ownerShips").value;
       if (val == 2) {
document.getElementById("bankPayment1").style.visibility = 'visible';
document.getElementById("bankPayment2").style.visibility = 'visible';
document.getElementById("bankPayment3").style.visibility = 'visible';
document.getElementById("bankPayment4").style.visibility = 'visible';
}
else if (val == 1) {
document.getElementById("bankPayment1").style.visibility = 'hidden';
document.getElementById("bankPayment2").style.visibility = 'hidden';
document.getElementById("bankPayment3").style.visibility = 'hidden';
document.getElementById("bankPayment4").style.visibility = 'hidden';
}
}

function checkPaymentMode() {
var ownerShips = document.getElementById("ownerShips").value;
if (ownerShips == "1") {
document.getElementById("paymentDateIns").style.visibility = 'visible';
document.getElementById("paymentDateIns1").style.visibility = 'visible';
document.getElementById("vendorIns").style.visibility = 'visible';
document.getElementById("vendorIns1").style.visibility = 'visible';
document.getElementById("bankPaymentModeIns").style.visibility = 'visible';
document.getElementById("bankPaymentModeIns1").style.visibility = 'visible';
document.getElementById("bankPayment1").style.visibility = 'visible';
document.getElementById("bankPayment2").style.visibility = 'visible';
document.getElementById("bankPayment3").style.visibility = 'visible';
document.getElementById("bankPayment4").style.visibility = 'visible';
document.getElementById("bankPayment9").style.visibility = 'visible';
document.getElementById("bankPayment10").style.visibility = 'visible';
document.getElementById("bankPayment11").style.visibility = 'visible';
document.getElementById("bankPayment12").style.visibility = 'visible';

} else {
document.getElementById("paymentDateIns").style.visibility = 'hidden';
document.getElementById("paymentDateIns1").style.visibility = 'hidden';
document.getElementById("vendorIns").style.visibility = 'hidden';
document.getElementById("vendorIns1").style.visibility = 'hidden';
//                               document.getElementById("vendorRT").style.visibility = 'hidden';
//                               document.getElementById("vendorRT1").style.visibility = 'hidden';
document.getElementById("vendorFC").style.visibility = 'hidden';
document.getElementById("vendorFC1").style.visibility = 'hidden';
//                               document.getElementById("vendorPermits").style.visibility = 'hidden';
//                               document.getElementById("vendorPermit1").style.visibility = 'hidden';
document.getElementById("bankPaymentModeIns").style.visibility = 'hidden';
document.getElementById("bankPaymentModeIns1").style.visibility = 'hidden';
//                               document.getElementById("bankPaymentModeRT").style.visibility = 'hidden';
//                               document.getElementById("bankPaymentModeRT1").style.visibility = 'hidden';
document.getElementById("bankPaymentModeFC").style.visibility = 'hidden';
document.getElementById("bankPaymentModeFC1").style.visibility = 'hidden';
document.getElementById("bankPaymentDateFC").style.visibility = 'hidden';
document.getElementById("bankPaymentDateFC1").style.visibility = 'hidden';
//                               document.getElementById("bankPaymentModePermit").style.visibility = 'hidden';
//                               document.getElementById("bankPaymentModePermit1").style.visibility = 'hidden';
document.getElementById("bankPayment1").style.visibility = 'hidden';
document.getElementById("bankPayment2").style.visibility = 'hidden';
document.getElementById("bankPayment3").style.visibility = 'hidden';
document.getElementById("bankPayment4").style.visibility = 'hidden';
document.getElementById("bankPayment9").style.visibility = 'hidden';
document.getElementById("bankPayment10").style.visibility = 'hidden';
document.getElementById("bankPayment11").style.visibility = 'hidden';
document.getElementById("bankPayment12").style.visibility = 'hidden';
}

}
</script>

<script type="text/javascript">

var ar_ext = ['pdf', 'txt', 'gif', 'jpeg', 'jpg', 'png', 'Gif', 'GIF', 'Png', 'PNG', 'JPG', 'Jpg'];        // array with allowed extensions

function checkName(sno) {
var name = document.getElementById("podFile" + sno).value;
var ar_name = name.split('.');

var ar_nm = ar_name[0].split('\\');
for (var i = 0; i < ar_nm.length; i++)
var nm = ar_nm[i];

var re = 0;
for (var i = 0; i < ar_ext.length; i++) {
if (ar_ext[i] == ar_name[1]) {
re = 1;
break;
}
}

if (re == 1) {
} else {
alert('".' + ar_name[1] + '" is not an file type allowed for upload');
document.getElementById("podFile" + sno).value = '';
}
}


function dispalyPackageType(val) {
if (val == 1) {
document.getElementById("labelPrevPolicy").style.display = "block";
document.getElementById("prevPolicy").style.display = "block";
} else {
document.getElementById("labelPrevPolicy").style.display = "none";
document.getElementById("prevPolicy").style.display = "none";
}
}
     </script>
</table>
                       </c:forEach> 
        </c:if>
<center>
<!--<input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Save"  text="default text"/>" name="Save" id="insuranceNext" onClick="updateInsuranceDetails();" style="visibility: block"/>-->
<!--<a  class="nexttab" ><input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Next"  text="default text"/>" name="Next" /></a>-->
</center>
</div>


<div id="vehicleFitnessCertificate" style="display:none;">
              <c:if test="${vehicleFCDetails != null}">
                <c:forEach items="${vehicleFCDetails}" var="Fc">
                    <c:if test = "${oldVehicleFC != null}">
                        <c:forEach items="${oldVehicleFC}" var="oldFC">
                            <c:if test="${oldFC.activeInd == 'N'}">
                                 <c:set var="vehicleId" value="${Fc.vehicleId}"/>   
                                <c:set var="fcid" value="${Fc.fcid}"/>   
                                <c:set var="activeInd" value="${oldFC.activeInd}"/> 
                         </c:if>
                        </c:forEach>
                        <c:if test="${activeInd == 'N'}">
                        <table align="right">
                                    <tr align="right">
                                        <td align="right"><a href='/throttle/viewOldVehicleFC.do?vehicleId=<c:out value="${vehicleId}" />&regNo=<c:out value="${regNo}"/>&fcid=<c:out value="${fcid}" />&listId=3&fleetTypeId=1'><font color="blue"> Old Fc Details</font></a></td>
                                    </tr>
                                </table>
                </c:if>
                        </c:if>
                  <table class="table table-info mb30 table-hover">
                      <thead>
                                    <tr>
                                        <th colspan="6"  height="30">
                                <div  align="center"><spring:message code="trucks.label.AddFC"  text="default text"/></div>
                                </th>
                                </tr>
                                </thead>
                    <tr>
                        <td ><spring:message code="trucks.label.RTODetails"  text="default text"/></td>
                        <td ><input type="text" name="rtoDetail" readonly id="rtoDetail" maxlength="25" class="form-control" style="width:260px;height:40px;"  value='<c:out value="${Fc.rtoDetail}" />' ></td>
                          <td ><spring:message code="trucks.label.ReceiptNo"  text="default text"/></td>
                        <td><input type="text" name="fcReceiptNo" readonly maxlength="25" id="fcReceiptNo" class="form-control" style="width:260px;height:40px;"  value='<c:out value="${Fc.fcReceiptNo}" />' ></td>  
                    </tr>
                    
                    <tr>
                        <td ><spring:message code="trucks.label.ReportNo"  text="ReportNo"/></td>
                        <td ><input name="reportNo" id="reportNo" readonly type="text" class="form-control" style="width:260px;height:40px;" id="toDate1"  date-format="dd-mm-yyyy" style="width:250px" value='<c:out value="${Fc.reportNo}" />' ></td> 
                        <td ><spring:message code="trucks.label.Odometer"  text="default text"/></td>
                        <td ><input type="text" name="odometer" readonly id="odometer"  class="form-control" style="width:260px;height:40px;" value='<c:out value="${Fc.odometer}" />' ></td>
                      </tr>
                    <tr>
                        <td ><spring:message code="trucks.label.DateofStart"  text="default text"/></td>
                        <td ><input name="fcDate" id="fcDate" readonly type="text" class="form-control datepicker" style="width:260px;height:40px;" value='<c:out value="${Fc.fcDate}"/>' ></td>  
                  
                        <td ><spring:message code="trucks.label.DateofExpiry"  text="default text"/></td>
                        <td ><input name="fcExpiryDate" id="fcExpiryDate" readonly type="text" class="form-control datepicker"    style="width:260px;height:40px;" value='<c:out value="${Fc.fcExpiryDate}"/>' ></td> 
                        
                    </tr>
                    <tr>
                        <td ><spring:message code="trucks.label.FCAmount"  text="default text"/></td>
                        <td ><input type="text" name="fcAmount" maxlength="7" readonly id="fcAmount" class="form-control" style="width:260px;height:40px;"  value='<c:out value="${Fc.fcAmount}" />' onkeypress="return onKeyPressBlockCharacters(event)" onchange="fcAmountSet(this.value)"></td>
                      <td ><spring:message code="trucks.label.Remarks"  text="default text"/></td>
                        <td ><input type="text" name="fcRemarks" readonly id="fcRemarks"  class="form-control" style="width:260px;height:40px;" value='<c:out value="${Fc.fcRemarks}" />' ></td>
 <input type="hidden" id="fcid" name="fcid" value=''>
                    </tr>
                    
                   <tr style="display:none">
                       <td class="text2" id="vendorFC" style="visibility: hidden;" class="text1" height="30"><spring:message code="trucks.label.VendorName"  text="default text"/></td>
                        <td class="text2" id="vendorFC1" style="visibility: hidden;"><select name="vendorIdFC" id="vendorIdFC" class="form-control" style="width:260px;height:40px;"  style="width:129px;height:20px;">
                            <c:if test="${vendorListCompliance != null}">
                                <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                <c:forEach items="${vendorListCompliance}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
                                </c:forEach>
                            </c:if>          
                            </select>
<!--                        <script>
                            document.getElementById("vendorIdFC").value = '<c:out value="${bankBranchIdFC}"/>'
                        </script>-->
                            </td>    
                      <td class="text2" id="bankPaymentModeFC" style="visibility: hidden;" class="text1" style="width:129px;" height="30"><spring:message code="trucks.label.PaymentMode"  text="default text"/>:</td>
                        <td class="text2" id="bankPaymentModeFC1" style="visibility: hidden;" class="text1" style="width:129px;" height="30">
                    <select class="form-control" style="width:260px;height:40px;" name="paymentTypeFC" id="paymentTypeFC" style="width:129px" onchange="getFCBankPayment()" >
                                <option value="0">----<spring:message code="trucks.label.Select"  text="default text"/>----</option>
                                <!--<option value="1">Cash Payment</option>-->
                                <option value="2"><spring:message code="trucks.label.BankPayment"  text="default text"/></option>
                            </select> 
                        </td>
<!--                    <script>
                        document.getElementById("paymentTypeFC").value = '<c:out value="${paymentTypeFC}"/>'
                    </script>-->
                    </tr>
                    <tr style="display:none">
                         <td class="text1" id="bankPaymentDateFC" style="visibility: hidden;"><spring:message code="trucks.label.PaymentDate"  text="default text"/></td>
                        <td class="text1" id="bankPaymentDateFC1" style="visibility: hidden;"><input type="text" name="paymentDateFC" id="paymentDateFC" class="datepicker" style="width:129px;" value='' ></td>
                    </tr>
                  <c:if test="${paymentTypeFC == '2' || fcid == null}"> 
                    <tr style="display:none">
                    <tr id="bankPayment9" style="display:none">
                     <td colspan="6" class="contenthead" height="30">
                        <div  class="contenthead" align="center"><spring:message code="trucks.label.BankPaymentDetails"  text="default text"/></div>
                    </td>
                      </tr>
                  <tr id="bankPayment10" style="visibility: hidden;">
                        <td class="text2" height="30"><spring:message code="trucks.label.BankHead"  text="default text"/></td>
                        <td class="text2"><select name="bankIdFC" id="bankIdFC" class="form-control" style="width:260px;height:40px;"  onchange="getBankBranchFC(this.value)" style="width:129px; height:20px;">
                            <c:if test="${primarybankList != null}">
                                <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                <c:forEach items="${primarybankList}" var="bankVar">
                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>
                                </c:forEach>
                            </c:if>          
                            </select>
<!--                        <script>
                            document.getElementById("bankIdFC").value = '<c:out value="${bankIdFC}"/>'
                        </script>-->
                        </td>
                        <td class="text2" height="30"><spring:message code="trucks.label.Bankbranch"  text="default text"/> :</td>
                        <td class="text2"> <select name="bankBranchIdFC" id="bankBranchIdFC" class="form-control" style="width:260px;height:40px;" style="width:129px; height:20px;">
                                 <c:if test="${bankBranchList != null}">
                                <option value="0" selected>--<spring:message code="trucks.label.Select"  text="default text"/>--</option>
                                <c:forEach items="${bankBranchList}" var="branch">
                                    <option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.bankname}"/></option>
                                </c:forEach>
                            </c:if>
                            </select>
<!--                         <script>
                            document.getElementById("bankBranchIdFC").value ='<c:out value="${vendorIdFC}"/>'
                        </script>            -->
                        </td>

                    </tr> 
                  <tr id="bankPayment11" style="visibility: hidden;">
                        <td class="text1"><font color="red">*</font><spring:message code="trucks.label.ChequeDate"  text="default text"/></td>
<!--                        <input type="hidden" id="fcid" name="fcid" value='<c:out value="${fcid}"/>'>
                        <input type="hidden" id="clearanceIdFC" name="clearanceIdFC" value='<c:out value="${clearanceIdFC}"/>'>-->
                        <!--<input type="hidden" id="fcid" name="fcid" value=''>-->
                        <input type="hidden" id="clearanceIdFC" name="clearanceIdFC" value=''>
                        <td class="text1" height="30"><input name="chequeDateFC" id="chequeDateFC" type="text" style="width:129px;" class="datepicker" value='' ></td>
                        <td><font color="red">*</font><spring:message code="trucks.label.ChequeNo"  text="default text"/></td>
                        <td height="30"><input name="chequeNoFC" id="chequeNoFC" style="width:129px;" maxlength="45" type="text" class="form-control" style="width:260px;height:40px;" value=''></td>
                    </tr>
                    <tr id="bankPayment12" style="visibility: hidden;">
                        <td class="text2" height="30"><spring:message code="trucks.label.ChequeAmount"  text="default text"/></td>
                        <td class="text2" height="30"><input name="chequeAmountFC" id="chequeAmountFC" style="width:129px;" maxlength="7" type="text" class="form-control" style="width:260px;height:40px;" value='' onkeypress="return blockNonNumbers(this, event, true, false);" readonly></td>
                    
                    </tr>
                        
                   <!--</table>-->
                  
                   </c:if>
<!--                    <tr>
                     <br>

<%
            int index3 = 1;

%>


            <c:if test="${FCList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
            </c:if>
            <c:if test="${FCList != null }" >
                <table align="center" width="700" cellpadding="0" cellspacing="0"  class="border">

                    <tr>

                        <td class="contentsub" height="30"><div class="contentsub">Sno</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Vehicle Number</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">FC Date</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">RDO Detail</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Receipt Number</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">FC Amount</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">FC Expiry Date</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">&nbsp;</div></td>
                    </tr>
                    <%
            String style2 = "text1";%>
                    <c:forEach items="${FCList}" var="FCL" >
                        <%
            if ((index3 % 2) == 0) {
                style2 = "text1";
            } else {
                style2 = "text2";
            }%>
                        <tr>
                            <td class="<%= style2 %>" height="30" style="padding-left:30px; "> <%= index3 %> </td>
                            <td class="<%= style2 %>" height="30" style="padding-left:30px; "> <c:out value="${FCL.regNo}" /></td>
                            <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.fcdate}" /></td>
                            <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.rtodetail}" /></td>
                            <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.receiptno}" /></td>
                            <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.fcamount}" /></td>
                            <td class="<%= style2 %>" height="30" style="padding-left:30px; "><c:out value="${FCL.fcexpirydate}" /></td>
                            <td class="<%= style2 %>" height="30" style="padding-left:30px; "><a href='/throttle/vehicleFCDetail.do?vehicleId=<c:out value="${FCL.vehicleId}" />&fcid=<c:out value="${FCL.fcid}" />'>alter </a> </td>
                        </tr>
                        <% index3++; %>
                    </c:forEach>
                </table>
            </c:if>
            <br>    
                    </tr>-->
                </table>
</c:forEach>
        </c:if >
                <center>

                    <!--<input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Save"  text="default text"/>" id="fcNext" name="Save"  onClick="updateFCDetails();" style="visibility: block"/>-->
                        <a  class="nexttab" ><input type="button" class="btn btn-success" value="<spring:message code="trucks.label.Next"  text="default text"/>" name="Next" /></a>

                </center>
                <script> 
                     function fcAmountSet(val){
                               document.getElementById("chequeAmountFC").value =val;  
                             }
                     function getBankBranchFC(bankIdFC) {
                              var temp = "";
                                  $.ajax({
                                      url: '/throttle/getBankBranchDetails.do',
                                      data: {bankId: bankIdFC},
                                      dataType: 'json',
                                       success: function (temp) {
//                                             alert(url);
                                  if (temp != '') {
                                      $('#bankBranchIdFC').empty();
                                      $('#bankBranchIdFC').append(
                                              $('<option style="width:150px"></option>').val(0+"~"+0).html('---Select----')
                                              )
                                      $.each(temp, function (i, data) {
                                          $('#bankBranchIdFC').append(
                                         $('<option value="'+data.branchId+"~"+data.bankLedgerId+'" style="width:150px"></option>').val(data.branchId+"~"+data.bankLedgerId).html(data.bankName)
                                         )
                                 document.getElementById("bankBranchIdFC").value ='<c:out value="${vendorIdFC}"/>'
                                      });
                                  } else {
                                      $('#bankBranchIdFC').empty();
                                  }
                              }
                                  });
                              }  
                       
                        function getFCBankPayment(){
                           var val=document.getElementById("paymentTypeFC").value;
//                           alert(ownerShips);
                           if(val == 2){
                             document.getElementById("bankPayment9").style.visibility = 'visible';
                             document.getElementById("bankPayment10").style.visibility = 'visible';
                             document.getElementById("bankPayment11").style.visibility = 'visible';
                             document.getElementById("bankPayment12").style.visibility = 'visible';
                           }
                           else if(val == 1){
                             document.getElementById("bankPayment9").style.visibility = 'hidden'; 
                             document.getElementById("bankPayment10").style.visibility = 'hidden'; 
                             document.getElementById("bankPayment11").style.visibility = 'hidden'; 
                             document.getElementById("bankPayment12").style.visibility = 'hidden'; 
                           }
                       }
                        
    
                    </script>
               

            </div>
                                      <div id="vehicleRoadTax" style="display:none;">
                <c:if test="${roadTaxDetails != null}">
        <c:forEach items="${roadTaxDetails}" var="road">
            <input type="hidden" name="roadTaxId" id="roadTaxId" value="<c:out value="${roadTaxId}"/>" />
            <c:if test="${oldVehicletax != null}">
                        <c:forEach items="${oldVehicletax}" var="old">
                            <c:set var="activeInd" value="${old.activeInd}"/>
                        </c:forEach>
                        <c:if test="${activeInd == 'N'}">
                            <table align="right">
                                <tr align="right">
                                    <td align="right"><a href='/throttle/viewOldVehicleTax.do?vehicleId=<c:out value="${vehicleId}" />&regNo=<c:out value="${regNo}"/>&roadTaxId=<c:out value="${roadTaxId}"/>&listId=7&fleetTypeId=1'><font color="blue">Old Tax Details</font></a></td>
                                </tr>
                            </table>
                            <!--<input type="text" value="<c:out value="${old.activeInd}" />"/>-->
                        </c:if>
                    </c:if>
                 <table class="table table-info mb30 table-hover">
                      <thead>
                                    <tr>
                                        <th colspan="6"  height="30">
                                <div  align="center">Add Road Permit</div>
                                </th>
                                </tr>
                                </thead>
                    <tr>
                        <td ><font color="red">*</font>Receipt No</td>
                        <td ><input type="text" name="roadTaxReceiptNo" readonly id="roadTaxReceiptNo" maxlength="15" class="form-control" value='<c:out value="${road.roadTaxReceiptNo}"/>' style="width:250px;"></td>
                        <td ><font color="red">*</font>Receipt Date</td>
                        <td ><input type="text" name="roadTaxReceiptDate" disabled id="roadTaxReceiptDate" class="datepicker" value='<c:out value="${road.roadTaxReceiptDate}"/>' style="width:250px;"></td>
                    </tr>
                    <tr>
                        <td ><font color="red">*</font>Location</td>
                        <td ><input type="text" name="roadTaxPaidLocation" readonly id="roadTaxPaidLocation" class="form-control" value='<c:out value="${road.roadTaxPaidLocation}"/>' maxlength="15" onkeypress="return onKeyPressBlockNumbers(event);" style="width:250px;"></td>
                        <td ><font color="red">*</font>Amount</td>
                        <td ><input type="text" name="roadTaxPaidAmount" readonly value='<c:out value="${road.roadtaxamount}"/>' id="roadTaxPaidAmount"  maxlength="7" class="form-control" value='' onkeypress="return blockNonNumbers(this, event, true, false);" style="width:250px;"></td>
                    </tr>
                    <tr>
                        <td ><font color="red">*</font>Period</td>
                        <td >
                            <select name="roadTaxPeriod" id="roadTaxPeriod" disabled class="form-control" style="width:250px;" >
                                <option value="0">Quarterly</option>
                                <option value="1">Half Yearly</option>
                                <option value="2">Yearly</option>
                            </select>
                        <script>
                            document.getElementById("roadTaxPeriod").value = '<c:out value="${roadTaxPeriod}"/>'
                        </script>     
                        </td>
                        <td ><font color="red">*</font>Road Tax From Date</td>
                        <td ><input type="text" name="roadTaxFromDate" disabled id="roadTaxFromDate" class="datepicker" value='<c:out value="${road.roadTaxFromDate}"/>' style="width:250px;"></td>
                    </tr>
                    <tr>
                        <td ><font color="red">*</font>Road Tax To Date</td>
                        <td ><input type="text" name="nextRoadTaxDate" disabled id="nextRoadTaxDate" class="datepicker" value='<c:out value="${road.roadTaxToDate}"/>' style="width:250px;"></td>
                        <td >Remarks</td>
                        <td ><input type="text" name="roadTaxRemarks" readonly id="roadTaxRemarks" class="form-control" value='<c:out value="${road.roadTaxRemarks}"/>' style="width:250px;"></td>
                    </tr>
                    <tr style="display:none">
                        <tr style="display:none">
                      <td id="vendorRT" style="visibility: hidden;"  height="30">Vendor Name</td>
                        <td id="vendorRT1" style="visibility: hidden;"><select name="vendorIdRT" id="vendorIdRT" class="form-control" style="width:140px;height:20px;">
                            <c:if test="${vendorListCompliance != null}">
                                <option value="0" selected>--Select--</option>
                                <c:forEach items="${vendorListCompliance}" var="vendor">
                                    <option value='<c:out value="${vendor.vendorId}"/>~<c:out value="${vendor.ledgerId}"/>~<c:out value="${vendor.ledgerCode}"/>'><c:out value="${vendor.vendorName}"/></option>
                                </c:forEach>
                            </c:if>          
                            </select>
                        <script>
//                             alert('<c:out value="${bankBranchIdRT}"/>')
                            document.getElementById("vendorIdRT").value = '<c:out value="${bankBranchIdRT}"/>'
                        </script>
                            </td>  
                        
                      <td id="bankPaymentModeRT" style="visibility: hidden;" height="30">Payment Mode:</td>
                        <td id="bankPaymentModeRT1" style="visibility: hidden;"  height="30">
                    <select class="form-control" name="paymentTypeRT" id="paymentTypeRT" style="width:129px" onchange="getRoadTaxBankPayment()" >
                                <option value="0">----select----</option>
                                <option value="1">Cash Payment</option>
                                <option value="2">Bank Payment</option>
                        <input type="hidden" id="roadTaxId" name="roadTaxId" value=''>
                        <!--<input type="hidden" id="roadTaxId" name="roadTaxId" value='<c:out value="${roadTaxId}"/>'>-->
                        <input type="hidden" id="clearanceIdRT" name="clearanceIdRT" value=''>
                        <!--<input type="hidden" id="clearanceIdRT" name="clearanceIdRT" value='<c:out value="${clearanceIdRT}"/>'>-->
                            </select> 
                        </td>
                    <script>
                       // document.getElementById("paymentTypeRT").value = '<c:out value="${paymentTypeRT}"/>';
                    </script>
                    </tr>
                    <tr style="display:none">
                         <td class="text1">Payment Date</td>
                        <td class="text1"><input type="text" name="paymentDateRT" id="paymentDateRT" class="datepicker" value='' ></td>
                    </tr>
                    <tr>
                      <%--<c:if test="${paymentTypeRT == '2' || roadTaxId == null}">--%>   
                    <tr id="bankPayment5" style="display:none">
                     <td colspan="6" class="contenthead" height="30">
                        <div  class="contenthead" align="center">Bank Payment Details</div>
                    </td>
                      </tr>
                  <tr id="bankPayment6" style="display:none">
                     
                        <td class="text1" height="30">Bank Head</td>
                        <td><select name="bankIdRT" id="bankIdRT" class="form-control"  onchange="getBankBranchRoadTax(this.value)" style="width:140px;height:20px;">
                            <c:if test="${primarybankList != null}">
                                <option value="0" selected>--Select--</option>
                                <c:forEach items="${primarybankList}" var="bankVar">
                                    <option value='<c:out value="${bankVar.primaryBankId}"/>'><c:out value="${bankVar.primaryBankName}"/></option>
                                </c:forEach>
                            </c:if>          
                            </select>
                        <script>
                            document.getElementById("bankIdRT").value = '<c:out value="${bankIdRT}"/>'
                        </script>
                        </td>
                        <td class="text2" height="30">Bank branch :</td>
                        <td> <select name="bankBranchIdRT" id="bankBranchIdRT" class="form-control" style="width:140px;height:20px;">
                                 <c:if test="${bankBranchList != null}">
                                <option value="0" selected>--Select--</option>
                                <c:forEach items="${bankBranchList}" var="branch">
                                    <option value='<c:out value="${branch.branchId}"/>'><c:out value="${branch.bankname}"/></option>
                                </c:forEach>
                            </c:if>
                            </select>
                         <script>
                            document.getElementById("bankBranchIdRT").value ='<c:out value="${vendorIdRT}"/>'
                        </script>            
                        </td>
                    </tr> 
                  <tr id="bankPayment7" style="display:none">
                        <td><font color="red">*</font>Cheque Date</td>
                       
                        <td height="30"><input name="chequeDateRT" id="chequeDateRT" type="text" class="datepicker" value='<c:out value="${chequeDateRT}"/>' ></td>
                        <td><font color="red">*</font>Cheque No</td>
                        <td height="30"><input name="chequeNoRT" id="chequeNoRT" style="width:125px;" maxlength="45" type="text" class="form-control" value='<c:out value="${chequeNoRT}"/>'></td>
                    </tr>
                    <tr id="bankPayment8" style="display:none">
                        <td class="text1" height="30">Cheque Amount</td>
                        <td class="text1" height="30"><input name="chequeAmountRT" id="chequeAmountRT" style="width:125px;" maxlength="7" type="text" class="form-control" value='<c:out value="${chequeAmountRT}"/>' onkeypress="return blockNonNumbers(this, event, true, false);"></td>
                    
<!--                    <td class="text1" height="30">Clearance Date</td>
                        <td height="30"><input name="clearanceDateRT" id="clearanceDateRT" type="text" class="datepicker" value='<c:out value="${clearanceDateRT}"/>' ></td>-->
                    </tr>
                   <!--</table>-->
                      <%--</c:if>--%>
                      <br>
<%--
<%
            int index2 = 1;

%>


            <c:if test="${RoadTaxList == null }" >
                <br>
                <center><font color="red" size="2"> no records found </font></center>
            </c:if>
            <c:if test="${RoadTaxList != null }" >
                <table align="center" width="70%" cellpadding="0" cellspacing="0"  class="border">

                    <tr>

                        <td class="contentsub" height="30"><div class="contentsub">Sno</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Vehicle Number</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Tax Receipt No</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Tax Receipt Date</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Tax Paid Location</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Tax Amount</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">Next Tax Date</div></td>
                        <td class="contentsub" height="30"><div class="contentsub">&nbsp;</div></td>
                    </tr>
                    <%
            String style1 = "text1";%>
                    <c:forEach items="${RoadTaxList}" var="Rtl" >
                        <%
            if ((index2 % 2) == 0) {
                style1 = "text1";
            } else {
                style1 = "text2";
            }%>
                        <tr>
                            <td class="<%= style1 %>" height="30" style="padding-left:30px; "> <%= index2 %> </td>
                            <td class="<%= style1 %>" height="30" style="padding-left:30px; "> <c:out value="${Rtl.regNo}" /></td>
                            <td class="<%= style1 %>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptno}" /></td>
                            <td class="<%= style1 %>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxreceiptdate}" /></td>
                            <td class="<%= style1 %>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxpaidlocation}" /></td>
                            <td class="<%= style1 %>" height="30" style="padding-left:30px; "><c:out value="${Rtl.roadtaxamount}" /></td>
                            <td class="<%= style1 %>" height="30" style="padding-left:30px; "><c:out value="${Rtl.nextRoadTaxDate}" /></td>
                            <td class="<%= style1 %>" height="30" style="padding-left:30px; "><a href='/throttle/vehicleTaxDetail.do?vehicleId=<c:out value="${Rtl.vehicleId}" />&id=<c:out value="${Rtl.id}" />'>alter </a> </td>
                        </tr>
                        <% index2++; %>
                    </c:forEach>
                </table>
            </c:if>--%>
            <br>
                </tr>
                </table>
            </c:forEach>
        </c:if >
                <br>
                <br>
                <center>

                    <!--<input type="button" class="btn btn-info" value="Save" id="roadTaxNext" name="Save"  onClick="updateRoadTaxDetails();" style="visibility: block"/>-->
                        <!--<a  class="nexttab" href=""><input type="button" class="btn btn-info" value="Next" name="Next" /></a>-->

                </center>
                    <script> 
                     function getBankBranchRoadTax(bankIdRT) {
                              var temp = "";
                                  $.ajax({
                                      url: '/throttle/getBankBranchDetails.do',
                                      data: {bankId: bankIdRT},
                                      dataType: 'json',
                                       success: function (temp) {
//                                             alert(url);
                                  if (temp != '') {
                                      $('#bankBranchIdRT').empty();
                                      $('#bankBranchIdRT').append(
                                              $('<option style="width:150px"></option>').val(0+"~"+0).html('---Select----')
                                              )
                                      $.each(temp, function (i, data) {
                                          $('#bankBranchIdRT').append(
                                         $('<option value="'+data.branchId+"~"+data.bankLedgerId+'" style="width:150px"></option>').val(data.branchId+"~"+data.bankLedgerId).html(data.bankName)
                                         )
                                         document.getElementById("bankBranchIdRT").value ='<c:out value="${vendorIdRT}"/>'
                                      });
                                  } else {
                                      $('#bankBranchIdRT').empty();
                                  }
                              }
                                  });
                              }  
                       
                        function getRoadTaxBankPayment(){
                        var val= document.getElementById("paymentTypeRT").value;
                           if(val == 2){
                             document.getElementById("bankPayment5").style.visibility = 'visible';
                             document.getElementById("bankPayment6").style.visibility = 'visible';
                             document.getElementById("bankPayment7").style.visibility = 'visible';
                             document.getElementById("bankPayment8").style.visibility = 'visible';
                           }
                           else if(val == 1){
                             document.getElementById("bankPayment5").style.visibility = 'hidden'; 
                             document.getElementById("bankPayment6").style.visibility = 'hidden'; 
                             document.getElementById("bankPayment7").style.visibility = 'hidden'; 
                             document.getElementById("bankPayment8").style.visibility = 'hidden'; 
                           }
                       }
                        
    
                    </script>     
                    
               <script type="text/javascript">
                   function updateRoadTaxDetails() {
                       var vehicleId = $("#vehicleId").val();
                       var roadTaxReceiptNo = document.getElementById("roadTaxReceiptNo").value;
                       var roadTaxReceiptDate = document.getElementById("roadTaxReceiptDate").value;
                       var roadTaxPaidLocation = document.getElementById("roadTaxPaidLocation").value;
                       var roadTaxPaidAmount = document.getElementById("roadTaxPaidAmount").value;
                       var roadTaxPeriod = document.getElementById("roadTaxPeriod").value;
                       var roadTaxFromDate = document.getElementById("roadTaxFromDate").value;
                       var nextRoadTaxDate = document.getElementById("nextRoadTaxDate").value;
                       var roadTaxRemarks = document.getElementById("roadTaxRemarks").value;
                        var paymentTypeRT = document.getElementById("paymentTypeRT").value;
                        var roadTaxId = document.getElementById("roadTaxId").value;
                        var ledgerId = document.getElementById("vendorIdRT").value;
                        var cashOnHandLedgerId = document.getElementById("cashOnHandLedgerId").value;
                        var cashOnHandLedgerCode = document.getElementById("cashOnHandLedgerCode").value;
                        var paymentDateRT = document.getElementById("paymentDateRT").value;
                        
                         var bankIdRT = "0";
                             if(document.getElementById("bankIdRT").value == null){
                                bankIdRT = "0";
                            }
                            else{
                                bankIdRT = document.getElementById("bankIdRT").value;
                            }
                            var clearanceIdRT = "0";
                            if(document.getElementById("clearanceIdRT").value == null){
                                clearanceIdRT = "0";
                            }
                            else{
                               clearanceIdRT =document.getElementById("clearanceIdRT").value;
                            }
                           
                            var bankBranchIdRT = "0";
                             if(document.getElementById("bankBranchIdRT").value == null){
                                bankBranchIdRT = "0";
                            }
                            else{
                                bankBranchIdRT = document.getElementById("bankBranchIdRT").value;
                            }
                            var chequeDateRT = "0";
                            if(document.getElementById("chequeDateRT").value == null){
                              chequeDateRT = "";
                            }else{
                              chequeDateRT = document.getElementById("chequeDateRT").value;
                            }
                            var chequeNoRT = "0";
                             if(document.getElementById("chequeNoRT").value == null){
                              chequeNoRT = "";
                            }else{
                              chequeNoRT = document.getElementById("chequeNoRT").value;
                            }
                            var chequeAmountRT ="0";
                             if(document.getElementById("chequeAmountRT").value == null){
                              chequeAmountRT = "0";
                            }else{
                              chequeAmountRT = document.getElementById("chequeAmountRT").value;
                            }
                        
                       if (document.addVehicle.roadTaxReceiptNo.value == '') {
                           alert('Please Enter Vehicle ReceiptNo');
                           $("#roadTaxReceiptNo").focus();
                           return;
                       } else if (document.addVehicle.roadTaxReceiptDate.value == '') {
                           alert('Please Enter Vehicle ReceiptDate');
                           $("#roadTaxReceiptDate").focus();
                           return;
                       } else if (document.addVehicle.roadTaxPaidLocation.value == '') {
                           alert('Please Enter Vehicle PaidLocation');
                           $("#roadTaxPaidLocation").focus();
                           return;
                       } else if (document.addVehicle.roadTaxPaidAmount.value == '') {
                           alert('Please Enter Vehicle PaidAmount');
                           $("#roadTaxPaidAmount").focus();
                           return;
                       } else if (document.addVehicle.roadTaxPeriod.value == '') {
                           alert('Please Enter Vehicle Period');
                           $("#roadTaxPeriod").focus();
                           return;
                       } else if (document.addVehicle.roadTaxFromDate.value == '') {
                           alert('Please Enter Vehicle Tax Valid FromDate');
                           $("#roadTaxFromDate").focus();
                           return;
                       } else if (document.addVehicle.nextRoadTaxDate.value == '') {
                           alert('Please Enter Vehicle Tax Valid Todate');
                           $("#nextRoadTaxDate").focus();
                           return;
                       }
                       var url = '';
                        var insertStatus = 0;
                       url = './insertRoadTaxDetails.do';
                       $.ajax({
                           url: url,
                           data: {roadTaxReceiptNo: roadTaxReceiptNo, roadTaxReceiptDate: roadTaxReceiptDate, roadTaxPaidLocation: roadTaxPaidLocation, roadTaxPaidAmount: roadTaxPaidAmount, roadTaxPeriod: roadTaxPeriod, vehicleId: vehicleId,
                               roadTaxFromDate: roadTaxFromDate, nextRoadTaxDate: nextRoadTaxDate, roadTaxRemarks: roadTaxRemarks,
                               paymentTypeRT:paymentTypeRT,bankIdRT:bankIdRT,bankBranchIdRT:bankBranchIdRT,chequeDateRT:chequeDateRT,
                               chequeNoRT:chequeNoRT,chequeAmountRT:chequeAmountRT,clearanceIdRT:clearanceIdRT,
                               roadTaxId:roadTaxId,ledgerId:ledgerId,cashOnHandLedgerId:cashOnHandLedgerId,cashOnHandLedgerCode:cashOnHandLedgerCode,
                               paymentDateRT:paymentDateRT
                           },
                           type: "GET",
                           success: function (response) {
                                insertStatus = response.toString().trim();
                               if (insertStatus == 0) {
                                   var str = "Vehicle RoadTax added failed ";
                    $("#status1").text(str).css("color", "red");
                                  
                               } else {
                                    var str = "Vehicle RoadTax added sucessfully ";
                    $("#status1").text(str).css("color", "green");
                                  
                               }

                           },
                           error: function (xhr, status, error) {
                           }
                       });
                       document.getElementById("roadTaxNext").style.visibility = 'hidden';
                   }
                                                                                                                                                                                  </script>


            </div>
                                      <div id="vehiclePermit" class="tabContent" style="display:none;">
                                          <c:if test="${permitDetails != null}">
        <c:forEach items="${permitDetails}" var="permit">
                    <c:if test="${oldVehiclePermit != null}">
                        <c:forEach items="${oldVehiclePermit}" var="old">
                            <%--<c:if test="${old.activeInd == 'N'}">--%>
                                <c:set var="vehicleId" value="${permit.vehicleId}"/>   
                                <c:set var="permitid" value="${permit.permitid}"/>   
                                <c:set var="activeInd" value="${old.activeInd}"/>   
                    <%--</c:if>--%>
                        </c:forEach>
                        <c:if test="${activeInd == 'N'}">
                    <table align="right">
                        <tr align="right">
                    <td align="right"><a href='/throttle/viewOldVehiclePermit.do?vehicleId=<c:out value="${vehicleId}" />&regNo=<c:out value="${regNo}"/>&permitId=<c:out value="${permitid}" />&listId=7&fleetTypeId=1'>Old Permit Details</a></td>
                        </tr>
                    </table>
                    </c:if>
                    </c:if>
    
               <table class="table table-info mb30 table-hover">
               <tr>
                <td colspan="4" align="center">
                    <div id="detail" style="display: block;">
                        <table class="table table-info mb30 table-hover" width="100%">
                            <thead><tr>
                                <th  colspan="4">Vehicle Permit Details</th>
                                </tr></thead>
                            <tr>
                                <td ><font color="red">*</font>Vehicle No</td>
                    <td >
                        <input type="text" name="regNo" id="regno" style="width:260px;height: 38px;" class="form-control" onkeypress="getEvents(event,this.value);"  readonly autocomplete="off"  value="<c:out value="${regNo}"/>"/>
                        <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/></td>
                                <td ><font color="red">*</font>Permit Type</td>
                                <td ><input type="text" name="permitType" id="permitType" style="width:260px;height: 38px;" class="form-control" readonly value="<c:out value="${permit.permitType}" />"/></td>
                            <tr>
                                <td ><font color="red">*</font>Permit No</td>
                                <td ><input type="text" name="permitNo" id="permitNo" style="width:260px;height: 38px;" class="form-control" readonly value="<c:out value="${permit.permitNo}" />"/></td>
                                <td ><font color="red">*</font>Permit Amount</td>
                                <td ><input type="text" name="permitAmount" id="permitAmount" style="width:260px;height: 38px;" class="form-control" readonly value="<c:out value="${permit.permitAmount}" />"/></td>
                            </tr>
                            <tr>
                                <td ><font color="red">*</font>Permit Paid Date</td>
                                <td ><input type="text" disabled name="permitPaidDate" id="permitPaidDate" style="width:260px;height: 38px;" class="datepicker"  value="<c:out value="${permit.permitPaidDate}" />"/><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleInsurance.premiumPaidDate,'dd-mm-yyyy',this)"/>--></td>
                                <td ><font color="red">*</font>Permit Expiry Date</td>
                                <td ><input type="text" disabled name="permitExpiryDate" id="permitExpiryDate" style="width:260px;height: 38px;" class="datepicker"  value="<c:out value="${permit.permitExpiryDate}" />"/><!--&nbsp;<img src="/throttle/images/cal.gif" name="Calendar"  onclick="displayCalendar(document.VehicleInsurance.premiumExpiryDate,'dd-mm-yyyy',this)"/>--></td>
                            </tr>
                            <tr>
                                <td >&nbsp;&nbsp;Remarks</td>
                                <td colsapn="3"><input type="text" name="permitRemarks" id="permitRemarks" style="width:260px;height: 38px;" class="form-control" readonly value="<c:out value="${permit.remarks}" />"/></td>
                            </tr>
                            
                        </table>
                        <br>
                    </div>
                </td>
                </tr>
            </table>
                                
                                </c:forEach>
        </c:if >

            </div>
        </div>

                    <script>

                        $(".nexttab").click(function () {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected + 1);
                        });
                        $(".pretab").click(function () {
                            var selected = $("#tabs").tabs("option", "selected");
                            $("#tabs").tabs("option", "selected", selected - 1);
                        });



                        function saveVehicleTyre() {
                            var itemIds = document.getElementsByName("itemIds");
                            var positionIds = document.getElementsByName("typeId");
                            var tyreIds = document.getElementsByName("tyreIds");
                            var tyreNos = document.getElementsByName("tyreNos");
                            var tyreExists = document.getElementsByName("tyreExists");
                            //                var tyreDate = document.getElementsByName("tyreDate");
                            //                var description = document.getElementById("description").value;
                            var tyreId = "";
                            var item = "";
                            var pos = "";
                            var tyreNo = "";
                            var date = "";
                            var vehicleId = "";
                            for (var i = 0; i < itemIds.length; i++) {
                                //                    if(itemIds[i+1].value == 0){
                                //                        alert("Please select item for row "+i)
                                //                        document.getElementById("itemIds"+i).focus();
                                //                        return;
                                //                    }else if(positionIds[i+1].value == 0){
                                //                        alert("Please select position for row "+i)
                                //                        document.getElementById("positionIds"+i).focus();
                                //                        return;
                                //                    }else if(tyreIds[i+1].value == 0){
                                //                        alert("Please enter tyre no for row "+i)
                                //                        document.getElementById("tyreIds"+i).focus();
                                //                        return;
                                //                    }else if(tyreDate[i+1].value == 0){
                                //                        alert("Please select tyre date for row "+i)
                                //                        document.getElementById("tyreDate"+i).focus();
                                //                        return;
                                //                    }else{
                                tyreId = document.getElementById("tyreIds" + i).value;
                                item = document.getElementById("itemIds" + i).value;
                                pos = document.getElementById("positionIds" + i).value;
                                tyreNo = document.getElementById("tyreNos" + i).value;
                                date = document.getElementById("tyreDate" + i).value;
                                vehicleId = document.getElementById("vehicleId").value;
                                var urlInfo = "";
                                if (tyreId == 0) {
                                    urlInfo = "./saveVehicleTyre.do";
                                } else {
                                    urlInfo = "./updateVehicleTyre.do";
                                }
                                $.ajax({
                                    url: urlInfo,
                                    data: {itemId: item, pos: pos,
                                        tyreId: tyreId,
                                        tyreNo: tyreNo,
                                        date: date, vehicleId: vehicleId, description: description
                                    },
                                    type: "GET",
                                    success: function (response) {
                                        $("#tyreId" + i).val(response.toString().trim());
                                    },
                                    error: function (xhr, status, error) {
                                    }
                                });
                                //                    }
                            }


                        }

                                                                                                                                </script>

        <script type="text/javascript">


            function advanceFuelTabHide() {

                //alert(<%=request.getAttribute("vehicle_id")%>);
                if ('<%=request.getAttribute("vehicle_id")%>' == 'null') {//Mk and topay


                    $('#vehicleInsurane').hide();
                } else if ('<%=request.getAttribute("vehicle_id")%>' != 'null') {


                    $('#vehicleInsurane').show();
                }

            }
                                </script>
<!--                    <c:if test="${vehicleId > '0' && editStatus == '1'}">
        <script>
            $("#vehicleDetailUpdateButton").show();
            $("#vehicleDetailSaveButton").hide();
            // $("#tyreSave").show();
            $("#depreciationSave").show();
            $("#oEmNext").show();
            //            $("#insuranceNext").show();
            //            $("#roadTaxNext").show();
            //            $("#fcNext").show();
            //            $("#permitNext").show();
                                                                                                                                                                        </script>

                    </c:if>       
                    <c:if test="${vehicleId > '0' && editStatus == '0'}">
        <script>
            $("#vehicleDetailUpdateButton").hide();
            $("#vehicleDetailSaveButton").hide();
            // $("#tyreSave").hide();
            $("#depreciationSave").hide();
            $("#oEmNext").hide();
            //            $("#insuranceNext").hide();
            //            $("#roadTaxNext").hide();
            //            $("#fcNext").hide();
            //            $("#permitNext").hide();
                                        </script>

                    </c:if>  -->
    </div>
<%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
</body>
</div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>





           


                                      

