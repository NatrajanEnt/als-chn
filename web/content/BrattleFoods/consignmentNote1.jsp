<!DOCTYPE html>
<%@page import="java.text.SimpleDateFormat" %>
<html>
    <head>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <%@ page import="java.util.* "%>
        <%@ page import=" javax. servlet. http. HttpServletRequest" %>

        <script type="text/javascript" src="/throttle/js/suest"></script>
        <script type="text/javascript" src="/throttle/js/autosuggestions.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest1.js"></script>
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 

        <link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>

        <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
        <script type="text/javascript" src="/throttle/js/jquery.ui.core.js"></script>
        <script type="text/javascript" src="/throttle/js/jquery.ui.datepicker.js"></script>

        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script type="text/javascript">

    //this autocomplete is used for consignee and consignor names
         $(document).ready(function() {
            // Use the .autocomplete() method to compile the list based on input from user
            $('#consignorName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                    
                        url: "/throttle/getConsignorName.do",
                        dataType: "json",
                        data: {
                            consignorName: request.term,
                            customerId: document.getElementById('customerId').value

                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }

                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    $('#consignorName').val(value);
                    $('#consignorPhoneNo').val(ui.item.Mobile);
                    $('#consignorAddress').val(ui.item.Address);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

            $('#consigneeName').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getConsigneeName.do",
                        dataType: "json",
                        data: {
                            consigneeName: request.term,
                            customerId: document.getElementById('customerId').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    var value = ui.item.Name;
                    $('#consigneeName').val(value);
                    $('#consigneePhoneNo').val(ui.item.Mobile);
                    $('#consigneeAddress').val(ui.item.Address);
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                itemVal = '<font color="green">' + itemVal + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            }

    });

    </script>
        <script type="text/javascript" language="javascript">
            $(document).ready(function() {
                $("#tabs").tabs();
            });
        </script>
        <script type="text/javascript">

        function convertStringToDate(y,m,d,h,mi,ss) {
            //1            
            //var dateString = "2013-08-09 20:02:03";
            var dateString = y+'-'+m+'-'+d+' '+h+':'+mi+':'+ss;
            //alert(dateString);
            var reggie = /(\d{4})-(\d{2})-(\d{2}) (\d{2}):(\d{2}):(\d{2})/;
            var dateArray = reggie.exec(dateString);
            var dateObject = new Date(
                (+dateArray[1]),
                (+dateArray[2])-1, // Careful, month starts at 0!
                (+dateArray[3]),
                (+dateArray[4]),
                (+dateArray[5]),
                (+dateArray[6])
            );
                //alert(dateObject);
                return dateObject;
            /*
            //2
            var today = new Date();
            var otherDay = new Date("12/30/2013");
            alert(today >= otherDay);

            //3
            var time1 = new Date();
            alert(time1);
            var time1ms= time1.getTime(time1); //i get the time in ms
            var time2 = otherDay;
            alert(time2);
            var time2ms= time2.getTime(time2);
            var difference= time2ms-time1ms;
            var hours = Math.floor(difference / 36e5);
            var minutes = Math.floor(difference % 36e5 / 60000);
            var seconds = Math.floor(difference % 60000 / 1000);
            alert(hours/24);
            alert(minutes);
            alert(seconds);

            var lapse = new Date(difference);
            var tz_correction_minutes = new Date().getTimezoneOffset() - lapse.getTimezoneOffset();
            lapse.setMinutes(time2.getMinutes() + tz_correction_minutes);
            alert(lapse.getDate()-1+' days and '  +lapse.getHours()+':'+lapse.getMinutes()+':'+lapse.getSeconds());
            */

        }

        function viewCustomerContract() {
            window.open('/throttle/viewCustomerContract.do?custId='+$("#customerId").val(), 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        

        function setOriginDestination(){



            var tempVar = document.cNote.contractRoute.value;
            //alert(tempVar);
            var tempSplit = tempVar.split("-");
            document.cNote.vehTypeId.value = tempSplit[3];
            document.cNote.vehicleTypeId.value = tempSplit[3];
            //alert(document.cNote.vehTypeId.value);
            document.cNote.origin.value = tempSplit[1];
            document.cNote.destination.value = tempSplit[2];
            //alert(document.cNote.origin.value);
            //alert(document.cNote.destination.value);
            document.getElementById("routeContractId").value = tempSplit[0];
            var routeContractId = tempSplit[0];
            //fetch route course
            var pointTypeVal = "";
            var cntr = 0;
            $.ajax({
                url: '/throttle/getContractRouteCourse.do',
                data: {routeContractId: routeContractId},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        //data.Id).html(data.Name)
                        var tempId = data.Id.split("~");
                        var tempName = data.Name.split("~");
                        for(var x=0; x<tempId.length; x++){
                            cntr++;
                            if(x==0){
                               pointTypeVal = 'Pick Up';
                            }else if(x==(tempId.length-1)){
                               pointTypeVal = 'Drop';
                            }else {
                                pointTypeVal = 'select';
                            }
                            //alert(tempId[x]);
                            var tempIdSplit = tempId[x].split("-");
                            addRouteCourse1a(tempIdSplit[0],tempName[x],cntr,pointTypeVal,tempIdSplit[1]);
                        }
                    });
                }
            });


            
        }
        function setHubId(sno){
        
        if(sno==2){
       var origin= document.getElementById("pointNameTemp"+sno).value;
       
      
       var  temp=origin.split('~');
      // alert(temp[0]);
       //alert(temp[1]);

       document.getElementById("originHub").value=temp[0];
       document.getElementById("pointId2").value=temp[0];
       document.getElementById("pointName2").value=temp[1];

        } else if(sno==3){
        var destination= document.getElementById("pointNameTemp"+sno).value;
         var  temp=destination.split('~');
       //alert(temp[0]);
       //alert(temp[1]);
        document.getElementById("destinationHub").value=temp[0];
       document.getElementById("pointId3").value=temp[0];
       document.getElementById("pointName3").value=temp[1];
        }

        }

        function checkRoute(val){
            if(val > 1) {
                var pointOrderValue = document.getElementById("pointOrder"+val).value
                var pointIdValue = document.getElementById("pointId"+val).value
                var prevVal = val-1;
                var nextVal = val+1;
                var pointIdValue = document.getElementById("pointId"+val).value
                var pointIdValue = document.getElementById("pointId"+val).value
            }
            
        }
        function checkOrderSequence(val){
            var pointOrderValue = document.getElementById("pointOrder"+val).value;
            if (pointOrderValue == ''){
                alert("please fill in the route order sequence");
                document.getElementById("pointOrder"+val).focus();
                document.getElementById("pointName"+val).value='';
            }else{
                var pointOrderValue = document.getElementsByName("pointOrder");
                var prevPointSeq = 0;
                for(var m=1; m<=pointOrderValue.length; m++){
                    if(document.getElementById("pointOrder"+m).value == prevPointSeq){
                        alert("please crosscheck and correct the  route order sequence");
                        document.getElementById("pointOrder"+val).focus();
                    }
                    prevPointSeq = document.getElementById("pointOrder"+m).value;
                }
            }
        }
        
        function validateRoute(val){
//            alert(val);
            var contractType = document.cNote.billingTypeId.value;
            //alert(contractType);
            if(contractType !='1' && contractType != '2'){

                var pointName = document.getElementById("pointName"+val).value;
                var pointIdValue = document.getElementById("pointId"+val).value;
                var pointOrderValue = document.getElementById("pointOrder"+val).value;
                var pointOrder = document.getElementsByName("pointOrder");
                var pointNames = document.getElementsByName("pointName");
                var pointIdPrev = 0;
                var pointIdNext = 0;
                var pointNamePrev = '';
                var pointNameNext = '';
    //            alert(pointOrderValue);
    //            alert(pointOrder.length);
                var prevPointOrderVal = parseInt(pointOrderValue)-1;
                var nextPointOrderVal = parseInt(pointOrderValue)+1;
                for(var m=1; m<= pointOrder.length; m++){
    //                    alert("loop:"+m+" :"+document.getElementById("pointOrder"+m).value +"pointOrderValue:"+pointOrderValue+"prevPointOrderVal:"+prevPointOrderVal+"nextPointOrderVal:"+nextPointOrderVal);
                        if(document.getElementById("pointOrder"+m).value == prevPointOrderVal){
                            pointIdPrev = document.getElementById("pointId"+m).value
                            pointNamePrev = document.getElementById("pointName"+m).value
    //                        alert("pointIdPrev:"+pointIdPrev);
                        }
                        if(document.getElementById("pointOrder"+m).value == nextPointOrderVal){
    //                        alert("am here..");
                            pointIdNext = document.getElementById("pointId"+m).value
                            pointNameNext = document.getElementById("pointName"+m).value
    //                           alert("pointIdNext:"+pointIdNext);
                        }
                }
    //            alert(pointIdValue);
    //            alert(pointIdPrev);
    //            alert(pointIdNext);

                ajaxRouteCheck(pointIdValue,pointIdPrev,pointIdNext,val, pointName,pointNamePrev,pointNameNext);
            }
//            else{
//                var pointName = document.getElementById("pointName"+val).value;
//                var pointIdValue = document.getElementById("pointId"+val).value;
//                //alert(pointIdValue)
//                if(pointIdValue == ''){
//                    alert('please enter valid interim point');
//                    document.getElementById("pointName"+val).value = '';
//                    document.getElementById("pointName"+val).focus();
//                }
//            }

        }

        

        var podRowCount1 = 1;
        var podSno1 = 0;
        function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr,startDate) {
            if (parseInt(podRowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            podSno1++;
            var tab = document.getElementById("routePlan");
            var newrow = tab.insertRow(podRowCount1);

//            var cell = newrow.insertCell(0);
//            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;



            cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder"+podSno1+"' name='pointOrder' class='form-control' value='"+order+"' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointName"+podSno1+"' name='pointName' class='form-control' value='"+name+"' ><input type='hidden' id='pointTransitHrs"+podSno1+"'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm"+podSno1+"'  name='pointRouteKm' value='"+routeKm+"' ><input type='hidden' id='pointRouteReeferHr"+podSno1+"'  name='pointRouteReeferHr' value='"+routeReeferHr+"' ><input type='hidden' id='pointRouteId"+podSno1+"'  name='pointRouteId' value='"+routeId+"' ><input type='hidden' id='pointId"+podSno1+"'  name='pointId' value='"+id+"' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            if(type == 'select'){
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType"+podSno1+"' class='form-control'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }else {
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointType"+podSno1+"'  name='pointType' class='form-control' value='"+type+"' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }
            cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='form-control' ></textarea></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='text1' height='25' ><input type='text' onChange='validateTripSchedule();' value='"+startDate+"'  readonly name='pointPlanDate'  id='pointPlanDate"+podSno1+"'   class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour'  id='pointPlanHour"+podSno1+"' onChange='validateTripSchedule();validateTransitTime("+podSno1+");'class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='25' >&nbsp;</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            podRowCount1++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        }
        function addRouteCourseNew(id, name, order, type,routeId,routeKm,routeReeferHr,startDate) {
            if (parseInt(podRowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            podSno1++;
            var tab = document.getElementById("routePlan");
            var newrow = tab.insertRow(podRowCount1);

//            var cell = newrow.insertCell(0);
//            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;



            cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder"+podSno1+"' name='pointOrder' class='form-control' value='"+order+"' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' ><select   id='pointNameTemp"+podSno1+"' name='pointNameTemp' onchange='setHubId("+podSno1+");' class='form-control' ><option value='0'>--select--</option> <c:if test="${hubList != null}" ><c:forEach items="${hubList}" var="hub"><option  value='<c:out value="${hub.hubId}"/>~<c:out value="${hub.hubName}"/>'><c:out value="${hub.hubName}"/></c:forEach > </c:if></select><input type='hidden' id='pointTransitHrs"+podSno1+"'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm"+podSno1+"'  name='pointRouteKm' value='"+routeKm+"' ><input type='hidden' id='pointRouteReeferHr"+podSno1+"'  name='pointRouteReeferHr' value='"+routeReeferHr+"' ><input type='hidden' id='pointRouteId"+podSno1+"'  name='pointRouteId' value='"+routeId+"' ><input type='hidden' id='pointId"+podSno1+"'  name='pointId' value='' ><input type='hidden' id='pointName"+podSno1+"'  name='pointName' value='' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            if(type == 'select'){
                cell = newrow.insertCell(2);
               var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType"+podSno1+"' class='form-control'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }else {
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointType"+podSno1+"'  name='pointType' class='form-control' value='"+type+"' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }
            cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='form-control' ></textarea></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='text1' height='25' ><input type='text'  onChange='validateTripSchedule();' value='"+startDate+"'  readonly name='pointPlanDate'  id='pointPlanDate"+podSno1+"'   class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour' onChange='validateTransitTime("+podSno1+");'  id='pointPlanHour"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='25' >&nbsp;</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            podRowCount1++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        }

        function addRouteCourse3(id, name, order, type,routeId,routeKm,routeReeferHr,startDate) {
            if (parseInt(podRowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            podSno1++;
            var tab = document.getElementById("routePlan");
            var newrow = tab.insertRow(podRowCount1);


            cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder"+podSno1+"' name='pointOrder' class='form-control' value='"+order+"' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointName"+podSno1+"' name='pointName' class='form-control' value='"+name+"' ><input type='hidden' id='pointTransitHrs"+podSno1+"'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm"+podSno1+"'  name='pointRouteKm' value='"+routeKm+"' ><input type='hidden' id='pointRouteReeferHr"+podSno1+"'  name='pointRouteReeferHr' value='"+routeReeferHr+"' ><input type='hidden' id='pointRouteId"+podSno1+"'  name='pointRouteId' value='"+routeId+"' ><input type='hidden' id='pointId"+podSno1+"'  name='pointId' value='"+id+"' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            if(type == 'select'){
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType"+podSno1+"' class='form-control'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }else {
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointType"+podSno1+"'  name='pointType' class='form-control' value='"+type+"' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }
            cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='form-control' ></textarea></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            if(type == 'Pick Up'){
                cell = newrow.insertCell(4);
                var cell0 = "<td class='text1' height='25' ><input type='text' onChange='validateTripSchedule();'  value='"+startDate+"'   readonly name='pointPlanDate'  id='pointPlanDate"+podSno1+"'   class='datepicker' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }else{
                cell = newrow.insertCell(4);
                var cell0 = "<td class='text1' height='25' ><input type='text' onChange='validateTransitTime("+podSno1+");'  value='"+startDate+"'   readonly name='pointPlanDate'  id='pointPlanDate"+podSno1+"'   class='datepicker' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

            }
            if(type == 'Pick Up'){
                cell = newrow.insertCell(5);
                var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour' onChange='validateTripSchedule();'   id='pointPlanHour"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }else{
                cell = newrow.insertCell(5);
                var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour' onChange='validateTransitTime("+podSno1+");'   id='pointPlanHour"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

            }



            podRowCount1++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        }
        function addRouteCourse1a(id, name, order, type,routeId) {
            if (parseInt(podRowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            podSno1++;
            var tab = document.getElementById("routePlan");
            var newrow = tab.insertRow(podRowCount1);

//            var cell = newrow.insertCell(0);
//            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
//            cell.setAttribute("className", styl);
//            cell.innerHTML = cell0;



            cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder"+podSno1+"' name='pointOrder' class='form-control' value='"+order+"' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointName"+podSno1+"' name='pointName' class='form-control' value='"+name+"' ><input type='hidden' id='pointTransitHrs"+podSno1+"'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm"+podSno1+"'  name='pointRouteKm' value='' ><input type='hidden' id='pointRouteReeferHr"+podSno1+"'  name='pointRouteReeferHr' value='' ><input type='hidden' id='pointRouteId"+routeId+"'  name='pointRouteId' value='"+id+"' ><input type='hidden' id='pointId"+podSno1+"'  readonly name='pointId' class='form-control' value='"+id+"' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            if(type == 'select'){
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType"+podSno1+"' class='form-control'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }else {
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointType"+podSno1+"'  name='pointType' class='form-control' value='"+type+"' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
            }
            cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='25' ><textarea name='pointAddresss' rows='2' cols='20' class='form-control' ></textarea></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell0 = "<td class='text1' height='25' ><input type='text'  value='"+startDate+"'   readonly name='pointPlanDate'  id='pointPlanDate"+podSno1+"'   class='datepicker' ></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(5);
            var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour'  id='pointPlanHour"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            podRowCount1++;

            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        }

        function addRouteCourse2() {
            if (parseInt(podRowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }

            var pointOrderValue = document.getElementById("pointOrder2").value;
            var prevRowRouteNameValue = document.getElementById("pointName"+parseInt(podRowCount1-1)).value;
            var prevRowPointIdValue = document.getElementById("pointId"+parseInt(podRowCount1-1)).value;
            var prevRowRouteOrderValue = document.getElementById("pointOrder"+parseInt(podRowCount1-1)).value;
            if(prevRowRouteNameValue.trim() == '' || prevRowPointIdValue == ''){
                alert('please enter the enroute name for route order '+prevRowRouteNameValue);
                document.getElementById("pointName"+parseInt(podRowCount1-1)).focus();
            }else {

                podSno1++;
                var tab = document.getElementById("routePlan");
                var newrow = tab.insertRow(podRowCount1-1);

                //alert(pointOrderValue);
                pointOrderValue =  parseInt(pointOrderValue) + 1;
                document.getElementById("pointOrder2").value = pointOrderValue;

                pointOrderValue =  parseInt(pointOrderValue) - 1;

    //            var cell = newrow.insertCell(0);
    //            var cell0 = "<td class='text1' height='25' >" + podSno1 + "</td>";
    //            cell.setAttribute("className", styl);
    //            cell.innerHTML = cell0;


                cell = newrow.insertCell(0);
                var cell0 = "<td class='text1' height='25' ><input type='text' readonly  id='pointOrder"+podSno1+"' name='pointOrder' class='form-control' value='"+pointOrderValue+"' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(1);
                var cell0 = "<td class='text1' height='25' ><input type='text' id='pointName"+podSno1+"' onChange='validateRoute("+podSno1+");'  name='pointName' class='form-control' value='' ><input type='hidden' id='pointTransitHrs"+podSno1+"'  name='pointTransitHrs' value='' ><input type='hidden' id='pointRouteKm"+podSno1+"'  name='pointRouteKm' value='' ><input type='hidden' id='pointRouteReeferHr"+podSno1+"'  name='pointRouteReeferHr' value='' ><input type='hidden' id='pointRouteId"+podSno1+"'  name='pointRouteId' value='0' ><input type='text' id='pointId"+podSno1+"'  readonly name='pointId' class='form-control' value='' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;
                callAjax(podSno1);
                cell = newrow.insertCell(2);
                var cell0 = "<td class='text1' height='25' ><select name='pointType' id='pointType"+podSno1+"' class='form-control'><option value='Pick Up'>Pick Up</option><option value='Drop'>Drop</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(3);
                var cell0 = "<td class='text1' height='25' ><textarea id='pointAddresss"+podSno1+"' name='pointAddresss' rows='2' cols='20' class='form-control' ></textarea></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(4);
                var cell0 = "<td class='text1' height='25' ><input type='text'  value=''  onChange='validateTransitTime("+podSno1+");'     readonly name='pointPlanDate'  id='pointPlanDate"+podSno1+"'   class='datepicker' ></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                cell = newrow.insertCell(5);
                var cell0 = "<td class='text1' height='25' >HH:<select name='pointPlanHour'  onChange='validateTransitTime("+podSno1+");'    id='pointPlanHour"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option></select>MI:<select name='pointPlanMinute'   id='pointPlanMinute"+podSno1+"'   class='form-control'><option value='00'>00</option><option value='01'>01</option><option value='02'>02</option><option value='03'>03</option><option value='04'>04</option><option value='05'>05</option><option value='06'>06</option><option value='07'>07</option><option value='08'>08</option><option value='09'>09</option><option value='10'>10</option><option value='11'>11</option><option value='12'>12</option><option value='13'>13</option><option value='14'>14</option><option value='15'>15</option><option value='16'>16</option><option value='17'>17</option><option value='18'>18</option><option value='19'>19</option><option value='20'>20</option><option value='21'>21</option><option value='22'>22</option><option value='23'>23</option><option value='24'>24</option><option value='25'>25</option><option value='26'>26</option><option value='27'>27</option><option value='28'>28</option><option value='29'>29</option><option value='30'>30</option><option value='31'>31</option><option value='32'>32</option><option value='33'>33</option><option value='34'>34</option><option value='35'>35</option><option value='36'>36</option><option value='37'>37</option><option value='38'>38</option><option value='39'>39</option><option value='40'>40</option><option value='41'>41</option><option value='42'>42</option><option value='43'>43</option><option value='44'>44</option><option value='45'>45</option><option value='46'>46</option><option value='47'>47</option><option value='48'>48</option><option value='49'>49</option><option value='50'>50</option><option value='51'>51</option><option value='52'>52</option><option value='53'>53</option><option value='54'>54</option><option value='55'>55</option><option value='56'>56</option><option value='57'>57</option><option value='58'>58</option><option value='59'>59</option><option value='60'>60</option></select></td>";
                cell.setAttribute("className", styl);
                cell.innerHTML = cell0;

                podRowCount1++;

                document.getElementById('pointName'+podSno1).focus();

                $(document).ready(function() {
                    $("#datepicker").datepicker({
                        showOn: "button",
                        buttonImage: "calendar.gif",
                        buttonImageOnly: true

                    });
                });

                $(function() {
                    $(".datepicker").datepicker({
                        changeMonth: true, changeYear: true
                    });
                });
            }
        }


        


        </script>


        <script type="text/javascript">

            	var httpReqRouteCheck;
                var temp = "";
                function ajaxRouteCheck(pointIdValue,pointIdPrev,pointIdNext,val, pointName,pointNamePrev,pointNameNext)
                {

                    var url = "/throttle/checkForRoute.do?pointIdValue="+pointIdValue+"&pointIdPrev="+pointIdPrev+"&pointIdNext="+pointIdNext;
                    if(pointIdValue != ''){
                        if (window.ActiveXObject)
                        {
                            httpReqRouteCheck = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        else if (window.XMLHttpRequest)
                        {
                            httpReqRouteCheck = new XMLHttpRequest();
                        }
                        httpReqRouteCheck.open("GET", url, true);
                        httpReqRouteCheck.onreadystatechange = function() {
                            processAjaxRouteCheck(val, pointName,pointNamePrev,pointNameNext);
                        };
                        httpReqRouteCheck.send(null);
                    }else{
                        alert("invalid interim point: "+ pointName);
                        document.getElementById("pointName"+val).value='';
                        document.getElementById("pointName"+val).focus();
                    }
                }

                function processAjaxRouteCheck(val, pointName,pointNamePrev,pointNameNext)
                {
                    if (httpReqRouteCheck.readyState == 4)
                    {
                        if (httpReqRouteCheck.status == 200)
                        {
                            temp = httpReqRouteCheck.responseText.valueOf();
                            alert(temp);
                            var tempVal = temp.split('~');
                            if(tempVal[0] == 0){
                                alert("valid route does not exists for the selected point: "+ pointName);
                                document.getElementById("pointName"+val).value='';
                                document.getElementById("pointName"+val).focus();
                            }
                            if(tempVal[0] == 1){
                                alert("valid route does not exists between "+ pointName + " and "+pointNameNext);
                                document.getElementById("pointName"+val).value='';
                                document.getElementById("pointName"+val).focus();
                            }
                            if(tempVal[0] == 2){                                
                                document.getElementById("pointOrder"+val).focus();
                                var tempValSplit = tempVal[1].split("-");
                                document.getElementById("pointRouteId"+(val-1)).value=tempValSplit[0];
                                document.getElementById("pointRouteKm"+(val-1)).value=tempValSplit[1];
                                document.getElementById("pointRouteReeferHr"+(val-1)).value=tempValSplit[2];
                                document.getElementById("pointTransitHrs"+(val-1)).value=tempValSplit[3];


                                tempValSplit = tempVal[2].split("-");
                                document.getElementById("pointRouteId"+(val+1)).value=tempValSplit[0];
                                document.getElementById("pointRouteKm"+(val+1)).value=tempValSplit[1];
                                document.getElementById("pointRouteReeferHr"+(val+1)).value=tempValSplit[2];
                                document.getElementById("pointTransitHrs"+(val+1)).value=tempValSplit[3];
                            }
                        }
                        else
                        {
                            alert("Error loading page\n" + httpReqRouteCheck.status + ":" + httpReqRouteCheck.statusText);
                        }
                    }
                }




            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                $(".datepicker").datepicker({
                    changeMonth: true, changeYear: true
                });
            });
        </script>
        <script type="text/javascript" language="javascript">
            var poItems = 0;
            var rowCount = '';
            var snumber = '';
            function addAllowanceRow()
            {
                if (snumber < 19) {
                    var tab = document.getElementById("expenseTBL");
                    var rowCount = tab.rows.length;

                    snumber = parseInt(rowCount) - 1;

                    var newrow = tab.insertRow(parseInt(rowCount) - 1);
                    newrow.height = "30px";
                    // var temp = sno1-1;
                    var cell = newrow.insertCell(0);
                    var cell0 = "<td><input type='hidden'  name='itemId' /> " + snumber + "</td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(1);
                    cell0 = "<td class='text1'><input name='driName' type='text' class='form-control' id='driName' onkeyup='getDriverName(" + snumber + ")' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(2);
                    cell0 = "<td class='text1'><input name='cleanerName' type='text' class='form-control' id='cleanerName' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    cell = newrow.insertCell(3);
                    cell0 = "<td class='text1'><input name='date' type='text' id='date' class='datepicker' /></td>";
                    //cell.setAttribute(cssAttributeName,"text1");
                    cell.innerHTML = cell0;

                    // rowCount++;

                    $(".datepicker").datepicker({
                        /*altField: "#alternate",
                         altFormat: "DD, d MM, yy"*/
                        changeMonth: true, changeYear: true
                    });
                }
                document.cNote.expenseId.value = snumber;
            }

            function delAllowanceRow() {
                try {
                    var table = document.getElementById("expenseTBL");
                    rowCount = table.rows.length - 1;
                    for (var i = 2; i < rowCount; i++) {
                        var row = table.rows[i];
                        var checkbox = row.cells[6].childNodes[0];
                        if (null != checkbox && true == checkbox.checked) {
                            if (rowCount <= 1) {
                                alert("Cannot delete all the rows");
                                break;
                            }
                            table.deleteRow(i);
                            rowCount--;
                            i--;
                            sno--;
                        }
                    }
                    document.cNote.expenseId.value = sno;
                } catch (e) {
                    alert(e);
                }
            }
            function parseDouble(value) {
                if (typeof value == "string") {
                    value = value.match(/^-?\d*/)[0];
                }
                return !isNaN(parseInt(value)) ? value * 1 : NaN;
            }

            function estimateFreight(){
                var billingTypeId = document.getElementById("billingTypeId").value;
                var customerTypeId = document.getElementById("customerTypeId").value;
                //customertypeid 1-> contract; 2-> walk in
                //billling type id 1 -> point to point; 2 -> point to point based on wt; 3 -> actual weight
                var totalKm = 0;
                var totalHr = 0;
                var rateWithReefer = 0;
                var rateWithoutReefer = 0;
                if(customerTypeId == 1 && billingTypeId == 1){//point to point
                    var temp = document.cNote.vehTypeIdContractTemp.value;
                    if(temp == 0){
                        alert("Freight cannot be estimated as the vehicle type is not chosen");
                    }else {
                        var temp = document.cNote.vehTypeIdContractTemp.value;
                        //alert(temp);
                        var tempSplit = temp.split("-");
                        rateWithReefer = tempSplit[5];
                        rateWithoutReefer = tempSplit[6];
                        totalKm = tempSplit[2];
                        totalHr = tempSplit[3];
                        if(document.cNote.reeferRequired.value == 'Yes'){
                            document.cNote.totFreightAmount.value = rateWithReefer;
                        }else {
                            document.cNote.totFreightAmount.value = rateWithoutReefer;
                        }
                        document.cNote.totalKm.value = totalKm;
                        document.cNote.totalHours.value = totalHr;
                    }
                }else if(customerTypeId == 1 && billingTypeId == 2){//point to point weight
                    var temp = document.cNote.vehTypeIdContractTemp.value;
                    if(temp == 0){
                        alert("Freight cannot be estimated as the vehicle type is not chosen");
                    }else {
                        var tempSplit = temp.split("-");
                        rateWithReefer = tempSplit[7];
                        rateWithoutReefer = tempSplit[8];
                        totalKm = tempSplit[2];
                        totalHr = tempSplit[3];
                        var totalWeight = document.cNote.totalWeightage.value;
                        if(document.cNote.reeferRequired.value == 'Yes'){
                            document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                        }else {
                            document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                        }
                        document.cNote.totalKm.value = totalKm;
                        document.cNote.totalHours.value = totalHr;
                    }
                }else if(customerTypeId == 1 && billingTypeId == 3){//actual kms
                    var pointRouteKm = document.getElementsByName("pointRouteKm");
                    var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");

                    //calculate total km and total hrs
                    for (var i = 0; i < pointRouteKm.length; i++) {
                        totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                        totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
                    }

                    var temp = document.cNote.vehTypeIdTemp.value;
                    if(temp == 0){
                        alert("Freight cannot be estimated as the vehicle type is not chosen");
                    }else {
                        var tempSplit = temp.split("-");
                        var vehicleRatePerKm = tempSplit[1];
                        var reeferRatePerHour = tempSplit[2];


                        var vehicleFreight = parseFloat(totalKm) * parseFloat(vehicleRatePerKm);
                        var reeferFreight = parseFloat(totalHr) * parseFloat(reeferRatePerHour);

                        if(document.cNote.reeferRequired.value == 'Yes'){
                            document.cNote.totFreightAmount.value = (parseFloat(vehicleFreight) + parseFloat(reeferFreight)).toFixed(2);
                        }else {
                            document.cNote.totFreightAmount.value = parseFloat(vehicleFreight).toFixed(2);
                        }
                    }
                    document.cNote.totalKm.value = totalKm;
                    document.cNote.totalHours.value = totalHr;
                }else if(customerTypeId == 2 ){ //walk in

                    var pointRouteKm = document.getElementsByName("pointRouteKm");
                    var pointRouteReeferHr = document.getElementsByName("pointRouteReeferHr");
                    var walkInBillingTypeId = document.cNote.walkInBillingTypeId.value;
                    //calculate total km and total hrs
                    for (var i = 0; i < pointRouteKm.length; i++) {
                        totalKm = totalKm + parseFloat(pointRouteKm[i].value);
                        totalHr = totalHr + parseFloat(pointRouteReeferHr[i].value);
                    }
                    if(walkInBillingTypeId == 1){//fixed rate
                        rateWithReefer = document.cNote.walkinFreightWithReefer.value;
                        rateWithoutReefer = document.cNote.walkinFreightWithoutReefer.value;
                        if(document.cNote.reeferRequired.value == 'Yes'){
                            document.cNote.totFreightAmount.value = parseFloat(rateWithReefer).toFixed(2);
                        }else {
                            document.cNote.totFreightAmount.value = parseFloat(rateWithoutReefer).toFixed(2);
                        }
                    }else if(walkInBillingTypeId == 2){//rate per km
                        rateWithReefer = document.cNote.walkinRateWithReeferPerKm.value;
                        rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKm.value;

                        if(document.cNote.reeferRequired.value == 'Yes'){
                            document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithReefer)).toFixed(2);
                        }else {
                            document.cNote.totFreightAmount.value = (parseFloat(totalKm) * parseFloat(rateWithoutReefer)).toFixed(2);

                        }
                    }else if(walkInBillingTypeId == 3){// rate per kg
                        var totalWeight = document.cNote.totalWeightage.value;
                        rateWithReefer = document.cNote.walkinRateWithReeferPerKg.value;
                        rateWithoutReefer = document.cNote.walkinRateWithoutReeferPerKg.value;
                        if(document.cNote.reeferRequired.value == 'Yes'){
                            document.cNote.totFreightAmount.value = (parseFloat(rateWithReefer) * parseFloat(totalWeight)).toFixed(2);
                        }else {
                            document.cNote.totFreightAmount.value = (parseFloat(rateWithoutReefer) * parseFloat(totalWeight)).toFixed(2);
                        }

                    }
                    document.cNote.totFreightAmount1.value = document.cNote.totFreightAmount.value;
                    document.cNote.totalKm.value = totalKm;
                    document.cNote.totalHours.value = totalHr;
                }
            }
            
            function saveExp(obj) {
                document.cNote.buttonName.value = "save";
                obj.name = "none";
                if (document.cNote.expenseId.value != "") {
                    document.cNote.action = '/throttle/saveDriverExpenses.do';
                    document.cNote.submit();
                }
            }

            function getDriverName(sno) {
                var oTextbox = new AutoSuggestControl(document.getElementById("driName"), new ListSuggestions("driName", "/throttle/handleDriverSettlement.do?"));

            }
        </script>
        <script language="">
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
    </head>

    <script type="text/javascript">
        var rowCount = 1;
        var sno = 0;
        var rowCount1 = 1;
        var sno1 = 0;
        var httpRequest;
        var httpReq;
        var styl = "";

        function addRow1() {
            if (parseInt(rowCount1) % 2 == 0)
            {
                styl = "text2";
            } else {
                styl = "text1";
            }
            sno1++;
            var tab = document.getElementById("addTyres1");
            //find current no of rows
            var rowCountNew = document.getElementById('addTyres1').rows.length;
            rowCountNew--;
            var newrow = tab.insertRow(rowCountNew);

            var cell = newrow.insertCell(0);
            var cell0 = "<td class='text1' height='25' > " + sno1 + "</td>";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            // Positions
            cell = newrow.insertCell(1);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productCodes' class='form-control' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            // TyreIds
            var cell = newrow.insertCell(2);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='batchCode' class='form-control' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            var cell = newrow.insertCell(3);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productNames' class='form-control' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;

            cell = newrow.insertCell(4);
            var cell1 = "<td class='text1' height='30' ><input type='text' name='packagesNos'   onKeyPress='return onKeyPressBlockCharacters(event);' id='packagesNos' class='form-control' value='' onkeyup='calcTotalPacks(this.value)'>";

            cell1 = cell1 + "<input type='hidden' name='tyreExists' value='' > </td>"
            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;

            cell = newrow.insertCell(5);
            var cell1 = "<td class='text1' height='30' ><select name='uom' id='uom'><option value='1' selected >Box</option> <option value='2'>Bag</option><option value='3'>Pallet</option><option value='4'>Each</option></select> </td>";

            cell.setAttribute("className", "text1");
            cell.innerHTML = cell1;
           cell = newrow.insertCell(6);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='productVolume'   onKeyPress='return onKeyPressBlockCharacters(event);' id='productVolume' class='form-control' value='' onkeyup='calcTotalWeights(this.value)' >";
           cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            
            cell = newrow.insertCell(7);
            var cell0 = "<td class='text1' height='30' ><input type='text' name='weights'   onKeyPress='return onKeyPressBlockCharacters(event);' id='weights' class='form-control' value='' onkeyup='calcTotalWeights(this.value)' >";
            cell.setAttribute("className", styl);
            cell.innerHTML = cell0;
            rowCount1++;
        }


        function calculateTravelHours(sno) {
            var endDates = document.getElementById('endDates' + sno).value;
            var endTimeIds = document.getElementById('endTimeIds' + sno).value;
            var tempDate1 = endDates.split("-");
            var tempTime1 = endTimeIds.split(":");
            var stDates = document.getElementById('stDates' + sno).value;
            var stTimeIds = document.getElementById('stTimeIds' + sno).value;
            var tempDate2 = stDates.split("-");
            var tempTime2 = stTimeIds.split(":");
            var prevTime = new Date(tempDate2[2], tempDate2[1], tempDate2[0], tempTime2[0], tempTime2[1]);  // Feb 1, 2011
            var thisTime = new Date(tempDate1[2], tempDate1[1], tempDate1[0], tempTime1[0], tempTime1[1]);              // now
            var difference = thisTime.getTime() - prevTime.getTime();   // now - Feb 1
            var hoursDifference = Math.floor(difference / 1000 / 60 / 60);
            document.getElementById('timeDifferences' + sno).value = hoursDifference;
        }

        function goToImportPage() {
            document.cNote.action = '/throttle/cnoteUploadPage.do';
            document.cNote.submit();
        }
    </script>
    <script>
        //endingPointIds
        function resetAddRow(sno) {
            if (document.getElementById('endingPointIds' + sno).value == document.getElementById('destination').value) {
//           addRow 
                document.getElementById('addRowDetails').style.display = 'none';
            }
        }
    </script> 

<%--
    <script>
        var v = "Roseindia-1,Vhghg-2,hgdhjsgdjsd-3,hkjsdhksjdhk-4,jhdkjshdksd-5";
    </script>
    <% ArrayList list  new ArrayList();
        String st="<script>document.writeln(v)</script>";
        String[] temp = st.split(",");
        list.add(temp[0]);
        list.add(temp[1]);
        list.add(temp[2]);
        list.add(temp[3]);
        list.add(temp[4]);
        out.println("value="+list.size()); 
    %>
--%>

    <script>
        //start ajax for vehicle Nos
                function callAjaxNew(val) {
                // Use the .autocomplete() method to compile the list based on input from user
                //alert(val);
                var pointNameId = 'pointName'+val;
                var pointIdId = 'pointId'+val;
                var prevPointId = 'pointId'+(parseInt(val)-1);
                var pointOrder = 'pointOrder'+val;
                var pointOrderVal = $('#'+pointOrder).val();
                var prevPointOrderVal = parseInt(pointOrderVal) - 1;
                //alert(pointOrderVal);
                //alert(prevPointOrderVal);
                var prevPointId = 0;
                var pointIds = document.getElementsByName("pointId");
                var pointOrders = document.getElementsByName("pointOrder");
                for(var m=0; m< pointIds.length; m++){
                    //alert("loop value:"+pointOrders[m].value +" : "+pointIds[m].value );
                    if(pointOrders[m].value == prevPointOrderVal){
                        //alert("am here:"+pointOrders[m].value +" : "+prevPointOrderVal);
                        prevPointId =  pointIds[m].value;
                    }
                }


                $.ajax({
                url: "/throttle/getCityList.do",
                            dataType: "json",
                            data: {
                                cityName: request.term,
                                prevPointId: prevPointId,
                                pointOrder: $('#'+pointOrder).val(),
                                billingTypeId: $('#billingTypeId').val(),
                                contractId: $('#contractId').val(),
                                textBox: 1
                            },
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#originTemp').append(
                                $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
                });

                

            }
                function callAjax(val) {
                // Use the .autocomplete() method to compile the list based on input from user
                //alert(val);
                var pointNameId = 'pointName'+val;
                var pointIdId = 'pointId'+val;
                var prevPointId = 'pointId'+(parseInt(val)-1);
                var pointOrder = 'pointOrder'+val;
                var pointOrderVal = $('#'+pointOrder).val();
                var prevPointOrderVal = parseInt(pointOrderVal) - 1;
                //alert(pointOrderVal);
                //alert(prevPointOrderVal);
                var prevPointId = 0;
                var pointIds = document.getElementsByName("pointId");
                var pointOrders = document.getElementsByName("pointOrder");
                for(var m=0; m< pointIds.length; m++){
                    //alert("loop value:"+pointOrders[m].value +" : "+pointIds[m].value );
                    if(pointOrders[m].value == prevPointOrderVal){
                        //alert("am here:"+pointOrders[m].value +" : "+prevPointOrderVal);
                        prevPointId =  pointIds[m].value;
                    }
                }
                //alert(prevPointId);
                $('#'+pointNameId).autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityList.do",
                            dataType: "json",
                            data: {
                                cityName: request.term,
                                prevPointId: prevPointId,
                                pointOrder: $('#'+pointOrder).val(),
                                billingTypeId: $('#billingTypeId').val(),
                                contractId: $('#contractId').val(),
                                textBox: 1
                            },
                            success: function(data, textStatus, jqXHR) {
                                var contractType = document.cNote.billingTypeId.value;
                                //alert(contractType);
                                if(contractType =='1' || contractType == '2'){
                                    if(data == '') {
                                        $('#'+pointIdId).val(0);
                                        alert('please enter valid interim point');
                                        $('#'+pointNameId).val('');
                                        $('#'+pointNameId).focus();
                                    }
                                }

                                var items = data;
                                response(items);
                            },
                            error: function(data, type) {
                                
                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var id = ui.item.Id;
                        //alert(id+" : "+value);
                        $('#'+pointIdId).val(id);
                        $('#'+pointNameId).val(value);
                        //validateRoute(val,value);

                        return false;
                    }
                    
                    // Format the list menu output of the autocomplete
                }).data("autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };

                
            }
            //end ajax for vehicle Nos
//new start
$(document).ready(function() {
   $('#containerLoc').autocomplete({
                    source: function(request, response) {
                        $.ajax({
                            url: "/throttle/getCityName.do",
                            dataType: "json",
                            data: {
                                cityName: request.term
                                                          },
                            success: function(data, textStatus, jqXHR) {
                                 //alert(contractType);
                                // alert("My city");
                                       if(data == '') {
                                        alert('please enter valid interim point');
                                        $('#containerLoc').val('');
                                        $('#containerLoc').focus();
                                    }
                                 var items = data;
                                response(items);
                            },
                            error: function(data, type) {

                                //console.log(type);
                            }
                        });
                    },
                    minLength: 1,
                    select: function(event, ui) {
                        var value = ui.item.Name;
                        var id = ui.item.Id;
                        //alert(id+" : "+value);
                        $('#locationId').val(id);
                        $('#containerLoc').val(value);
                        //validateRoute(val,value);

                        return false;
                    }

                    // Format the list menu output of the autocomplete
                }).data("autocomplete")._renderItem = function(ul, item) {
                    //alert(item);
                    var itemVal = item.Name;
                    itemVal = '<font color="green">' + itemVal + '</font>';
                    return $("<li></li>")
                            .data("item.autocomplete", item)
                            .append("<a>" + itemVal + "</a>")
                            .appendTo(ul);
                };
});

//new end

        $(document).ready(function() { 
            // Use the .autocomplete() method to compile the list based on input from user
            $('#customerName').autocomplete({
               
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerName: request.term,
                            customerCode: document.getElementById('customerCode').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerName").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('~');
                    $itemrow.find('#customerId').val(tmp[0]);
                    $itemrow.find('#customerName').val(tmp[1]);
                    $itemrow.find('#customerCode').val(tmp[2]);
                    $('#customerAddress').text(tmp[3]);
                    $('#pincode').val(tmp[4]);
                    $('#customerMobileNo').val(tmp[6]);
                    $('#customerPhoneNo').val(tmp[5]);
                    $('#mailId').val(tmp[7]);
                    $('#billingTypeId').val(tmp[8]);
                    $('#billingTypeName').text(tmp[9]);
                    $('#billTypeName').text(tmp[9]);
                    $('#contractNo').text(tmp[10]);
                    $('#contractExpDate').text(tmp[11]);
                    $('#contractId').val(tmp[12]);

                    $('#paymentType').val(tmp[13]);
                    $('#consignorName').val(tmp[1]);
                    $('#consignorPhoneNo').val(tmp[6]);
                    $('#consignorAddress').val(tmp[3]);
                    $('#consigneeName').val(tmp[1]);
                    $('#consigneePhoneNo').val(tmp[6]);
                    $('#consigneeAddress').val(tmp[3]);
                    
                    $('#creditLimitTemp').text(tmp[14]);
                    $('#creditDaysTemp').text(tmp[15]);
                    $('#outStandingTemp').text(tmp[16]);
                    $('#customerRankTemp').text(tmp[17]);
                    if(tmp[18] == "0"){
                    $('#approvalStatusTemp').text("No");
                    }else{
                    $('#approvalStatusTemp').text("Yes");
                    }
                    $('#outStandingDateTemp').text(tmp[19]);
                    
                    $('#creditLimit').val(tmp[14]);
                    $('#creditDays').val(tmp[15]);
                    $('#outStanding').val(tmp[16]);
                    $('#customerRank').val(tmp[17]);
                    $('#approvalStatus').val(tmp[18]);
                    $('#outStandingDate').val(tmp[19]);
                    
                    $('#creditLimitTable').show();
                    
                    //$('#ahref').attr('href', '/throttle/viewCustomerContract.do?custId=' + tmp[0]);
                    $("#contractDetails").show();
                    document.getElementById('customerCode').readOnly = true;
                    $('#origin').empty();
                    $('#origin').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    if(tmp[8] == '3'){//actual kms contract
                        setOriginList(tmp[12]);
                        setVehicleTypeForActualKM(tmp[12]);
                        $('#vehicleTypeDiv').show();
                        $('#vehicleTypeContractDiv').hide();
                        $('#contractRouteDiv2').show();
                        $('#contractRouteDiv1').hide();
                    }else {
                        getContractRoutes(tmp[12]);
                        getContractRoutesOrigin(tmp[12]);


                        $('#vehicleTypeDiv').hide();
                        $('#vehicleTypeContractDiv').show();
                        $('#contractRouteDiv1').show();
                        $('#contractRouteDiv2').hide();
                    }
                    fetchCustomerProducts(tmp[1]);
                    return false;
                    
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('~');
                itemVal = '<font color="green">' + temp[1] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

            $('#customerCode').autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: "/throttle/getCustomerDetails.do",
                        dataType: "json",
                        data: {
                            customerCode: request.term,
                            customerName: document.getElementById('customerName').value
                        },
                        success: function(data, textStatus, jqXHR) {
                            var items = data;
                            response(items);
                        },
                        error: function(data, type) {
                            console.log(type);
                        }
                    });
                },
                minLength: 1,
                select: function(event, ui) {
                    $("#customerCode").val(ui.item.Name);
                    var $itemrow = $(this).closest('tr');
                    var value = ui.item.Name;
                    var tmp = value.split('-');
                    $itemrow.find('#customerId').val(tmp[0]);
                    $itemrow.find('#customerName').val(tmp[1]);
                    $itemrow.find('#customerCode').val(tmp[2]);
                    $itemrow.find('#customerAddress').val(tmp[3]);
                    $itemrow.find('#customerMobileNo').val(tmp[4]);
                    $itemrow.find('#customerPhoneNo').val(tmp[5]);
                    $itemrow.find('#mailId').val(tmp[6]);
                    $itemrow.find('#billingTypeId').val(tmp[7]);
                    $('#billingTypeName').val(tmp[8]);
                    $('#billTypeName').val(tmp[8]);
                    $("#contractDetails").show();
                    document.getElementById('customerName').readOnly = true;
                    return false;
                }
            }).data("autocomplete")._renderItem = function(ul, item) {
                var itemVal = item.Name;
                var temp = itemVal.split('-');
                itemVal = '<font color="green">' + temp[2] + '</font>';
                return $("<li></li>")
                        .data("item.autocomplete", item)
                        .append("<a>" + itemVal + "</a>")
                        .appendTo(ul);
            };

        });

        function setOriginList(val) {
            $.ajax({
                url: '/throttle/getContractRouteList.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#originTemp').append(
                                $('<option style="width:150px"></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });

        }
        function fetchCustomerProducts(val) {
            var custId = document.cNote.customerId.value;
            //alert(custId + ":::" +val );
            document.cNote.customerName.value = val;
            if(custId != '' && custId != '0'){
                $.ajax({
                    url: '/throttle/fetchCustomerProducts.do',
                    data: {customerId: custId},
                    dataType: 'json',
                    success: function(data) {                        
                        if(data != ''){
                            $('#productCategoryIdTemp').empty();
                                $('#productCategoryIdTemp').append(
                                                $('<option ></option>').val(0).html('--select--')
                                                )
                            $.each(data, function(i, data) {                                
                                
                                $('#productCategoryIdTemp').append(
                                        $('<option></option>').val(data.Id).html(data.Name)
                                        )

                            });
                        }
                    }
                });
            }

        }
        function setVehicleTypeForActualKM(val) {
            $('#vehTypeIdTemp').empty();
            $('#vehTypeIdTemp').append(
                            $('<option ></option>').val(0).html('--select--')
                            )
            $.ajax({
                url: '/throttle/getVehicleTypeForActualKM.do',
                data: {contractId: val},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#vehTypeIdTemp').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });

        }

        function getContractRoutes(val) {
            $.ajax({
                url: '/throttle/getContractRoutes.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(i, data) {
                        $('#contractRoute').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });
        }
        function getContractRoutesOrigin(val) {
            $.ajax({
                url: '/throttle/getContractRoutesOrigin.do',
                data: {contractId: val, billingTypeId: $('#billingTypeId').val(), customerTypeId: 0},
                dataType: 'json',
                success: function(data) {
                    $('#contractRouteOrigin').empty();
                    $('#contractRouteOrigin').append(
                            $('<option></option>').val(0).html('-select-')
                            )
                    $.each(data, function(i, data) {
                        $('#contractRouteOrigin').append(
                                $('<option ></option>').val(data.Id).html(data.Name)
                                )
                    });
                }
            });
        }
        function setContractOriginDestination() {
            
            if(document.cNote.contractRouteOrigin.value == '0'){
                $('#contractRouteDestination').empty();
                        $('#contractRouteDestination').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                 setContractRouteDetails();
            }else{

                $.ajax({
                    url: '/throttle/getContractRoutesOriginDestination.do',
                    data: {contractId: $('#contractId').val(), contractRouteOrigin: $('#contractRouteOrigin').val(), customerTypeId: 0},
                    dataType: 'json',
                    success: function(data) {
                        $('#contractRouteDestination').empty();
                        $('#contractRouteDestination').append(
                                $('<option></option>').val(0).html('-select-')
                                )
                        $.each(data, function(i, data) {
                            $('#contractRouteDestination').append(
                                    $('<option ></option>').val(data.Id).html(data.Name)
                                    )
                        });
                        setContractRouteDetails();
                    }
                });
            }
        }


        var httpReq;
        var temp = "";
        function setDestination()
        {
            document.cNote.origin.value = document.cNote.originTemp.value;
            var billingTypeId = document.getElementById('billingTypeId').value;
            var url = "/throttle/getDestinationList.do?originId=" + document.cNote.originTemp.value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&contractId=" + document.getElementById('contractId').value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setDestinationOptions(temp, document.cNote.destinationTemp);
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }

        function setDestinationOptions(text, variab) {
            variab.options.length = 0;
            option0 = new Option("--select--", '0');
            variab.options[0] = option0;
            if (text != "") {
                var splt = text.split('~');
                var temp1;
                var id;
                var name;
                for (var i = 0; i < splt.length; i++) {
                    temp1 = splt[i].split('#');
//                    alert(temp1[0]);
//                    alert(temp1[1]);
                    /*
                    if (document.getElementById('customerTypeId').value == 2) {
                        id = temp1[0];
                        name = temp1[1];
                        //setVehicleType();
                    } else if (document.getElementById('billingTypeId').value == 3 && document.getElementById('customerTypeId').value == 1) {
                        id = temp1[0] + "-" + temp1[1];
                        name = temp1[1];
                        //setVehicleType();
                    } else if (document.getElementById('billingTypeId').value != 3 && document.getElementById('customerTypeId').value == 1) {
                        id = temp1[0];
                        name = temp1[1];
                        //setVehicleType();
                    }
                */
                  //  alert(name);
                  //  alert(id);
                    id = temp1[0];
                    name = temp1[1];
                    option1 = new Option(name, id)
                    variab.options[i + 1] = option1;
                }
                setRouteDetails();
            }
        }

        function setVehicleType() {
            //alert();
            //alert();
            var url = "/throttle/getContractVehicleTypeList.do?contractId=" + document.getElementById('contractId').value + "&billingTypeId=" + document.getElementById('billingTypeId').value + "&customerTypeId=" + document.getElementById('customerTypeId').value + "&origin=" + document.getElementById('origin').value + "&destination=" + document.getElementById('destination').value;
            if (window.ActiveXObject) {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            } else if (window.XMLHttpRequest) {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processVehicleTypeAjax();
            };
            httpReq.send(null);
        }
        function getContractVehicleType(){
            var contractType = document.cNote.billingTypeId.value;
            //alert(contractType);
            if(contractType =='1' || contractType == '2' || contractType =='4'){
                //alert("am here....");
                var pointIds = document.getElementsByName("pointId");
                //alert("am here...."+pointIds.length);
                var pointId="";
                var j=0;
                var statusCheck = true;
                for (var i=0; i < pointIds.length; i++) {
                    //alert(pointIds[i].value);
                    j = i + 1;
                    if(i != 0 && i != pointIds.length){
                        j = j + 1;
                    }
                    if(pointIds[i].value == '' || pointIds[i].value ==0){
                        alert('please enter valid interim point');
                        //alert(document.getElementById("pointName"+j).value);
                        document.getElementById("pointName"+j).focus();
                        statusCheck = false;
                    }

                    if(i==0){
                        pointId = pointIds[i].value;
                    }else {
                        pointId = pointId + "~" + pointIds[i].value;
                    }
                }
                //alert(pointId);
                if(statusCheck){
                    $.ajax({
                        url: '/throttle/getContractVehicleTypeForPointsList.do',
                        data: {contractId: $('#contractId').val(), pointId: pointId},
                        dataType: 'json',
                        success: function(data) {
                            if(data == '' || data == null){
                                alert('chosen contract route does not exists. please check.');
                            }else{
                                $("#routePlanAddRow").hide();
                                $("#freezeRoute").hide();
                                $("#resetRoute").hide();
                                $("#unFreezetRoute").show();
                                $('#vehTypeIdContractTemp').empty();
                                $('#vehTypeIdContractTemp').append(
                                        $('<option></option>').val(0).html('-select-')
                                        )
                                $.each(data, function(i, data) {
                                    $('#vehTypeIdContractTemp').append(
                                            $('<option ></option>').val(data.Id).html(data.Name)
                                            )
                                });
                            }
                        }
                    });
                }
             }else{
                $("#routePlanAddRow").hide();
                $("#freezeRoute").hide();
                $("#resetRoute").hide();
                $("#unFreezetRoute").show();
             }

        }

        function unFreezetRouteFn(){
            $("#unFreezetRoute").hide();
            $("#routePlanAddRow").show();
            $("#freezeRoute").show();
            $("#resetRoute").show();

            var contractType = document.cNote.billingTypeId.value;
            //alert(contractType);
            if(contractType =='1' || contractType == '2'){
                $('#vehTypeIdContractTemp').empty();
                $('#vehTypeIdContractTemp').append(
                $('<option></option>').val(0).html('-select-')
                )
            }
        }
        function processVehicleTypeAjax() {
            if (httpReq.readyState == 4) {
                if (httpReq.status == 200) {
                    temp = httpReq.responseText.valueOf();
                    //alert(temp);
                    $('#vehTypeId').append(
                            $('<option style="width:150px"></option>').val(0).html('--Select--')
                            )
                    setVehicleTypeOptions(temp, document.cNote.vehTypeId);
                } else {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }

        function setVehicleTypeOptions(text, variab) {
            variab.options.length = 0;
            option0 = new Option("--select--", '0');
            variab.options[0] = option0;
            if (text != "") {
                var splt = text.split('~');
                var temp1;
                variab.options[0] = option0;
                for (var i = 0; i < splt.length; i++) {
                    temp1 = splt[i].split('#');
                    //alert(temp1[1]+"-"+temp1[0]);
                    option0 = new Option(temp1[1], temp1[0])
                    variab.options[i + 1] = option0;
                }
            }
        }


        function multiPickupShow() {
            var billingTypeId = document.getElementById('billingTypeId').value;
            if (document.getElementById('multiPickup').checked == true) {
                document.getElementById('multiPickupCharge').readOnly = false;
                document.getElementById('multiPickup').value = "Y";
                //alert(document.getElementById('multiPickup').value);
//                if (billingTypeId == "3") {
//                    $("#routeDetail").show();
//                    $("#showRouteCourse").show();
//                }
            } else if (document.getElementById('multiPickup').checked == false) {
                document.getElementById('multiPickupCharge').readOnly = true;
                document.getElementById('multiPickup').value = "N";
                //alert(document.getElementById('multiPickup').value);
//                if (billingTypeId == "3") {
//                    $("#routeDetail").hide();
//                    $("#showRouteCourse").hide();
//                }
            }
        }
        function multiDeliveryShow() {
            var billingTypeId = document.getElementById('billingTypeId').value;
            if (document.getElementById('multiDelivery').checked == true) {
                document.getElementById('multiDeliveryCharge').readOnly = false;
                document.getElementById('multiDelivery').value = "Y";
//                if (billingTypeId == "3") {
//                    $("#routeDetail").show();
//                    $("#showRouteCourse").show();
//                }
            } else if (document.getElementById('multiDelivery').checked == false) {
                document.getElementById('multiDeliveryCharge').readOnly = true;
                document.getElementById('multiDelivery').value = "N";
//                if (billingTypeId == "3") {
//                    $("#routeDetail").hide();
//                    $("#showRouteCourse").hide();
//                }
            }
        }

        function calcTotalPacks(val) {
            var totVal = 0;
            var packagesNos = document.getElementsByName('packagesNos');
            for (var i = 0; i < packagesNos.length; i++) {
                if (packagesNos[i].value != '') {
                    totVal += parseInt(packagesNos[i].value);
                }
            }
            $('#totalPackages').text(totVal);
            $('#totalPackage').val(totVal);
        }


        function calcTotalWeights(val) {
            var totVal = 0;
            var reeferRequired = document.getElementById('reeferRequired').value;
            var weights = document.getElementsByName('weights');
            var billingTypeId = document.getElementById('billingTypeId').value;
            var rateWithReefer = document.getElementById('rateWithReefer').value;
            var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
            for (var i = 0; i < weights.length; i++) {
                if (weights[i].value != '') {
                    totVal += parseInt(weights[i].value);
                }
            }
            $('#totalWeight').text(totVal);
            $('#totalWeightage').val(totVal);
            var freigthTotal = 0;
            if (billingTypeId == 2 && reeferRequired == 'Yes') {
                freigthTotal = parseInt(rateWithReefer) * parseInt(totVal);
                $('#freightAmount').text(freigthTotal)
                $('#totalCharges').val(freigthTotal)
            } else if (billingTypeId == 2 && reeferRequired == 'No') {
                freigthTotal = parseInt(rateWithoutReefer) * parseInt(totVal);
                $('#freightAmount').text(freigthTotal)
                $('#totalCharges').val(freigthTotal)
            }
        }
        function resetRouteInfo() {
            var customerType = document.cNote.customerTypeId.value;
            var billingType = document.cNote.billingTypeId.value;
            if(customerType == '1' && (billingType == '1' || billingType == '2')){
                setContractRouteDetails();
            }else {
                setRouteDetails();
            }
        }
        function validateTransitTime(val){

            var transitHours = document.getElementById("pointTransitHrs"+val).value;

            if(transitHours == ''){
                var pointIdValue = document.getElementById("pointId"+val).value;
                var pointOrderValue = document.getElementById("pointOrder"+val).value;
                var pointOrder = document.getElementsByName("pointOrder");
                var pointNames = document.getElementsByName("pointName");
                var pointIdPrev = 0;
                var pointIdNext = 0;
                var prevPointOrderVal = parseInt(pointOrderValue)-1;
                var nextPointOrderVal = parseInt(pointOrderValue)+1;
                for(var m=1; m<= pointOrder.length; m++){
                        if(document.getElementById("pointOrder"+m).value == prevPointOrderVal){
                            pointIdPrev = document.getElementById("pointId"+m).value;
                        }
                        if(document.getElementById("pointOrder"+m).value == nextPointOrderVal){
                            pointIdNext = document.getElementById("pointId"+m).value;
                        }
                }
                //fetch transit hours via ajax for prepointid and current point id and set transit hours value to hidden field

            }

        }

        function validateTripSchedule(){
            //alert(document.getElementById("pointPlanDate1").value);
            document.cNote.vehicleRequiredDate.value = document.getElementById("pointPlanDate1").value;
            document.cNote.vehicleRequiredHour.value = document.getElementById("pointPlanHour1").value;
            document.cNote.vehicleRequiredMinute.value = document.getElementById("pointPlanMinute1").value;
            var scheduleDate = document.cNote.vehicleRequiredDate.value;
            var scheduleHour = document.cNote.vehicleRequiredHour.value;
            var scheduleMinute = document.cNote.vehicleRequiredMinute.value;

            var temp = scheduleDate.split("-");
            var tripScheduleTime = convertStringToDate(temp[2],temp[1],temp[0],scheduleHour,scheduleMinute,'00');
            
            var time1 = new Date();            
            var time1ms= time1.getTime(time1); //i get the time in ms
            var time2 = tripScheduleTime;
            var time2ms= time2.getTime(time2);
            var difference= time2ms-time1ms;
            var hours = Math.floor(difference / 36e5);
            //alert(hours);
            if(hours < 0){
                alert('vehicle required time is in the past. please check ');
                //alert('Orders cannot be accepted as the cut off time has elapsed.');
                return true;
            }else{
                return true;
            }

        }
        function submitPage(value) {  document.cNote.action = "/throttle/saveConsignmentNote.do";
                    document.cNote.submit();}
           function abcd(value){
           var totFreightAmount = $("#totFreightAmount").val();
            if(totFreightAmount >  0){
            $("#createOrder").hide();
            }
            if(!validateTripSchedule()){
                return;
            }
            
//            if (textValidation(document.cNote.orderReferenceNo, 'Customer Order Reference')) {
//                return;
//            }
            if(document.cNote.freightAcceptedStatus.checked) {
                var billingTypeId = $("#billingTypeId").val();
                if(billingTypeId == 4){
                        document.getElementById('orderButton').style.display='block';
                }else {
                    if(parseFloat(document.cNote.totFreightAmount.value) > 0) {
                        document.getElementById('orderButton').style.display='block';
                        }else{

                            document.cNote.freightAcceptedStatus.checked = false;
                            alert("Invalid freight value");
                        }
                }
                
            }else{
                alert("select freight agree for order creation");
                return;
            }

            if (document.cNote.productCategoryIdTemp.value=='0') {
                alert('please select product category');
                document.cNote.productCategoryIdTemp.focus();
                return;
            }
            var customerType = document.cNote.customerTypeId.value;
            if(customerType == '2'){
                if (textValidation(document.cNote.walkinCustomerName, 'Customer Name')) {
                    return;
                }
                if (textValidation(document.cNote.walkinCustomerCode, 'Customer Code')) {
                    return;
                }
                if (textValidation(document.cNote.walkinCustomerAddress, 'Customer Address')) {
                    return;
                }
                if (textValidation(document.cNote.walkinPincode, 'Customer pincode')) {
                    return;
                }
                if (textValidation(document.cNote.walkinCustomerMobileNo, 'Customer Mobile')) {
                    return;
                }
                if (document.cNote.walkinMailId.value=='') {
                    alert('please enter customer email id');
                    document.cNote.walkinMailId.focus();
                    return;
                }
            }

  
//            if (isEmail(document.cNote.walkinMailId.value)) {
//                return;
//            }
            
            var billingType = document.cNote.billingTypeId.value;
            if(customerType == '1' && (billingType == '1' || billingType == '2' || billingType == '4')){
                if (document.cNote.contractRouteOrigin.value=='0') {
                    alert('please select consignment origin');
                    document.cNote.contractRouteOrigin.focus();
                    return;
                }
                if (document.cNote.contractRouteDestination.value=='0') {
                    alert('please select consignment destination');
                    document.cNote.contractRouteDestination.focus();
                    return;
                }

                if (document.cNote.vehTypeIdContractTemp.value == '0') {
                    alert('please select vehicle type');
                    document.cNote.vehTypeIdContractTemp.focus();
                    return;
                }
            }else { //walk in or actual kms
                if (document.cNote.originTemp.value=='0') {
                    alert('please select consignment origin');
                    document.cNote.originTemp.focus();
                    return;
                }
                if (document.cNote.destinationTemp.value=='0') {
                    alert('please select consignment destination');
                    document.cNote.destinationTemp.focus();
                    return;
                }
            }
            document.cNote.vehicleRequiredDate.value = document.getElementById("pointPlanDate1").value;
            document.cNote.vehicleRequiredHour.value = document.getElementById("pointPlanHour1").value;
            document.cNote.vehicleRequiredMinute.value = document.getElementById("pointPlanMinute1").value;
            //var tripScheduleTime = convertStringToDate(2013,12,12,22,30,'00');

            if(isEmpty(document.cNote.vehicleRequiredDate.value)){
                alert('please enter vehicle required date');
                document.getElementById("pointPlanDate1").focus();
                return;
            }





            if(isEmpty(document.cNote.consignorName.value)){
                alert('please enter consignorName');
                document.cNote.consignorName.focus();
                return;
            }
            if(isEmpty(document.cNote.consignorPhoneNo.value)){
                alert('please enter consignorPhoneNo');
                document.cNote.consignorPhoneNo.focus();
                return;
            }
            if(isEmpty(document.cNote.consignorAddress.value)){
                alert('please enter consignorAddress');
                document.cNote.consignorAddress.focus();
                return;
            }
            if(isEmpty(document.cNote.consigneeName.value)){
                alert('please enter consigneeName');
                document.cNote.consigneeName.focus();
                return;
            }
            if(isEmpty(document.cNote.consigneePhoneNo.value)){
                alert('please enter consigneePhoneNo');
                document.cNote.consigneePhoneNo.focus();
                return;
            }
            if(isEmpty(document.cNote.consigneeAddress.value)){
                alert('please enter consigneeAddress');
                document.cNote.consigneeAddress.focus();
                return;
            }

            estimateFreight();
            var totFreightAmount = document.cNote.totFreightAmount.value;
            var customerId = document.getElementById('customerId').value;
            var creditLimit = $('#creditLimit').val();
            var outStanding = $('#outStanding').val();
            if( (totFreightAmount == '' || parseFloat(totFreightAmount) <= 0) && customerId != 1040 && billingType != 4){
                alert('Revenue cannot be estimated, please check the customer contract and proceed');
            }else if(parseFloat(creditLimit) <= parseFloat(outStanding)){
                var txt;
                var r = confirm("Credit Limit is over! press ok to create order with approval matrix");
                if (r == true) {
                    document.cNote.action = "/throttle/saveConsignmentNote.do";
                    document.cNote.submit();
                } else {
                     $("#createOrder").show();
                }
            } else  {
                document.cNote.action = "/throttle/saveConsignmentNote.do";
                document.cNote.submit();
            }
        }

    function setProductCategoryValues(){
        var temp = document.cNote.productCategoryIdTemp.value;
        //alert(temp);
        if(temp != 0){
            var tempSplit = temp.split('~');
            document.getElementById("temperatureInfo").innerHTML  = 'Reefer Temp (deg Celcius): Min '+tempSplit[1]+' Max '+tempSplit[2] ;
            document.cNote.productCategoryId.value=tempSplit[0];
            var reeferRequired = tempSplit[3];
            if (reeferRequired == 'Y') {
                reeferRequired = 'Yes';
            }else{
                reeferRequired = 'No';
            }
            document.cNote.reeferRequired.value=reeferRequired;
        }else {
            document.getElementById("temperatureInfo").innerHTML  = '';
            document.cNote.productCategoryId.value=0;
            document.cNote.reeferRequired.value='';
        }
    }
    function checkFreightAcceptedStatus(){
        //alert("am here.."+document.cNote.freightAcceptedStatus.checked);
        if(document.cNote.freightAcceptedStatus.checked) {
            //alert("am here..");
            var billingTypeId = $("#billingTypeId").val();
            if(billingTypeId  == 4){
                document.getElementById('orderButton').style.display='block';
            }else{
            if(parseFloat(document.cNote.totFreightAmount.value) > 0 ) {
                document.getElementById('orderButton').style.display='block';
            }else{
                document.cNote.freightAcceptedStatus.checked = false;
                alert("Invalid freight value");
            }

            }
        }else{
            document.getElementById('orderButton').style.display='none';
        }
    }
    </script>

    <body onload="enableContract(1);
            addRow1();testNew();
            multiPickupShow();
            multiDeliveryShow();">
        <% String menuPath = "Operations >>  Create New Consignment Note";
            request.setAttribute("menuPath", menuPath);
        %>
        <form name="cNote" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@include file="/content/common/message.jsp" %>
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            %>
            <input type="hidden" name="startDate" value='<%=startDate%>' />
            
            <table cellpadding="0" cellspacing="0" width="1000" border="0px" class="border" align="center">
                <tr>
                    <td class="contenthead" colspan="4">Consignment Note </td>
                </tr>
                <tr>
                    <td class="text2">Entry Option</td>
                    <td class="text2"><input type="radio" name="entryType" value="1" checked="">Manual</td>
                    <td class="text2"><input type="radio" name="entryType" value="2" onclick="goToImportPage();">Import</td>
                    <td class="text2" >&nbsp;</td>
                </tr>
                <tr>
<!--                    <td class="text1">Consignment Note </td>
                    <td class="text1"><input type="text" name="consignmentNoteNo" id="consignmentNoteNo" value="<c:out value="${consignmentOrderNo}"/>" class="textbox"readonly style="width: 120px"/></td>-->
                    <td class="text1"><font color="red">*</font>Customer Type</td>
                    <td class="text1">
                        <c:if test="${customerTypeList != null}">
                            <select name="customerTypeId" id="customerTypeId" onchange="enableContract(this.value);" class="textbox" style="width:120px;" >
                                <c:forEach items="${customerTypeList}" var="cusType">
                                    <option value="<c:out value="${cusType.customerTypeId}"/>"><c:out value="${cusType.customerTypeName}"/></option>
                                </c:forEach>
                            </select>
                        </c:if>
                    </td>
                    <td class="text1">Consignment Date </td>
                    <td class="text1" ><input type="text" name="consignmentDate" id="consignmentDate" value="<c:out value="${curDate}"/>" class="textbox"readonly style="width: 120px"/></td>
                <tr>
                <tr>
                    <td class="text2"><font color="red">*</font>Customer Reference No</td>
                    <td class="text2"><input type="text" name="orderReferenceNo" id="orderReferenceNo" value="" class="textbox" style="width: 120px"/></td>
                    <td class="text2">Customer Reference  Remarks</td>
                    <td class="text2"><textarea name="orderReferenceRemarks" id="orderReferenceRemarks"></textarea></td>

            </table>
            <br>
            <br>
            <script type="text/javascript">

                function enableContract(val) {
                    //alert(val);
                    if (val == 1) {
                        $("#contractCustomerTable").show();
                        $("#walkinCustomerTable").hide();
                        $("#contractFreightTable").show();
                        $("#walkinFreightTable").hide();
                        document.getElementById('customerAddress').readOnly = true;
                        document.getElementById('pincode').readOnly = true;
                        document.getElementById('customerMobileNo').readOnly = true;
                        document.getElementById('mailId').readOnly = true;
                        document.getElementById('customerPhoneNo').readOnly = true;
                        document.getElementById('walkinCustomerName').value = "";
                        document.getElementById('walkinCustomerCode').value = "";
                        document.getElementById('walkinCustomerAddress').value = "";
                        document.getElementById('walkinPincode').value = "";
                        document.getElementById('walkinCustomerMobileNo').value = "";
                        document.getElementById('walkinMailId').value = "";
                        document.getElementById('walkinCustomerPhoneNo').value = "";
                    } else {
                        //alert(val);
                        $("#contractCustomerTable").hide();
                        $("#walkinCustomerTable").show();
                        $("#contractFreightTable").hide();
                        $("#walkinFreightTable").show();
                        document.getElementById('customerId').value = "";
                        document.getElementById('customerName').value = "";
                        document.getElementById('customerCode').value = "";
                        document.getElementById('customerAddress').value = "";
                        document.getElementById('pincode').value = "";
                        document.getElementById('customerMobileNo').value = "";
                        document.getElementById('mailId').value = "";
                        document.getElementById('customerPhoneNo').value = "";
                        setOriginList(0);
                        $('#vehicleTypeDiv').show();
                        $('#vehicleTypeContractDiv').hide();
                        $('#contractRouteDiv2').show();
                        $('#contractRouteDiv1').hide();
                    }
                }

                function showOrderType(){
                    document.cNote.destinationTemp.value = '0';
                      setRouteDetails();
                }
            </script>
            <div id="tabs">
                <ul>
                    <li><a href="#customerDetail"><span>Order Details</span></a></li>
<!--                    <li><a href="#routeDetail" id="showRouteCourse"><span>Route Course</span></a></li>-->
<!--                    <li><a href="#paymentDetails"><span>Invoice Info</span></a></li>-->

                </ul>

                <div id="customerDetail">
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="contractCustomerTable">
                        <tr>
                            <td class="contenthead" colspan="4" >Contract Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Customer Name</td>
                            <td class="text1"><input type="hidden" name="customerId" id="customerId" class="textbox" />
                                <input type="text" name="customerName" onKeyPress="return onKeyPressBlockNumbers(event);"  id="customerName" class="textbox"  /></td>
                            <td class="text1"><font color="red">*</font>Customer Code</td>
                            <td class="text1"><input type="text" name="customerCode" id="customerCode" class="textbox"  /></td>
                            <input type="hidden" name="paymentType" id="paymentType" class="textbox" />
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><textarea rows="1" cols="16" id="customerAddress" name="customerAddress"></textarea></td>
                            <td class="text2"><font color="red">*</font>Pincode</td>
                            <td class="text2"><input type="text"  name="pincode" id="pincode"  onKeyPress="return onKeyPressBlockCharacters(event);"  class="textbox" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Mobile No</td>
                            <td class="text1"><input type="text"  name="customerMobileNo"  onKeyPress="return onKeyPressBlockCharacters(event);"  id="customerMobileNo" class="textbox"  /></td>
                            <td class="text1"><font color="red">*</font>E-Mail Id</td>
                            <td class="text1"><input type="text"  name="mailId" id="mailId" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="text" name="customerPhoneNo" id="customerPhoneNo"  onKeyPress="return onKeyPressBlockCharacters(event);"   class="textbox" maxlength="10"   /></td>

                            <td class="text2">Billing Type</td>
                            <td class="text2" id="billingType"><input type="hidden" name="billingTypeId" id="billingTypeId" class="textbox" /><label id="billingTypeName"></label></td>

                        </tr>
                    </table>
                    <!-- walkin customer code -->
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinCustomerTable">
                        <tr>
                            <td class="contenthead" colspan="4" >Walkin Customer Details</td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font> Customer Name</td>
                            <td class="text1"><input type="text" name="walkinCustomerName" id="walkinCustomerName" class="textbox"   /></td>
                            <td class="text1"><font color="red">*</font> Customer Code</td>
                            <td class="text1"><input type="text" name="walkinCustomerCode" id="walkinCustomerCode" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><textarea rows="1" cols="16" id="walkinCustomerAddress" name="walkinCustomerAddress"></textarea></td>
                            <td class="text2"><font color="red">*</font>Pincode</td>
                            <td class="text2"><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinPincode" id="walkinPincode" class="textbox" /></td>
                        </tr>
                        <tr>
                            <td class="text1"><font color="red">*</font>Mobile No</td>
                            <td class="text1"><input type="text"    onKeyPress='return onKeyPressBlockCharacters(event);'   name="walkinCustomerMobileNo" id="walkinCustomerMobileNo" class="textbox"  /></td>
                            <td class="text1"><font color="red">*</font>E-Mail Id</td>
                            <td class="text1"><input type="text"  name="walkinMailId" id="walkinMailId" class="textbox"  /></td>
                        </tr>
                        <tr>
                            <td class="text2">Phone No</td>
                            <td class="text2"><input type="text"     onKeyPress='return onKeyPressBlockCharacters(event);' name="walkinCustomerPhoneNo" id="walkinCustomerPhoneNo"  class="textbox" maxlength="10"   /></td>

                            <td class="text2" colspan="2">&nbsp</td>

                        </tr>
                    </table>
                    <div id="contractDetails" style="display: none">
                        <table>
                            <tr>
                                <td class="text1">Contract No :</td>
                                <td class="text1"><b><input type="hidden" name="contractId" id="contractId" class="textbox" /><label id="contractNo"></label></b></td>
                                <td class="text1">Contract Expiry Date :</td>
                                <td class="text1"><font color="green"><b><label id="contractExpDate"></label></b></font></td>
                                <td class="text1">&nbsp;&nbsp;&nbsp;</td>
<!--                                <td class="text1"><a id="ahref" href="/throttle/BrattleFoods/customerContractMaster.jsp"><b>View Contract</b></a></td>-->
                                <td class="text1">
<!--                                    <input type="button" class="button" value="View Contract" onclick="viewCustomerContract()">-->
                                    <a href="" onclick="viewCustomerContract();">view</a>
                                </td>
                            </tr>
                        </table>
                    </div>




                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Order Type </td>
                        </tr>
                      <tr>

                    <td class="text1"><font color="red">*</font>Select Order Type &nbsp;&nbsp;
                    <!--    <c:if test="${productCategoryList != null}">
                            <input type="hidden" name="productCategoryId" id="productCategoryId" class="textbox" />
                            <select name="productCategoryIdTemp" id="productCategoryIdTemp" onchange="setProductCategoryValues();"  class="textbox" >
                                <option value="0">--Select--</option>
                                <c:forEach items="${productCategoryList}" var="proList">
                                    <option value="<c:out value="${proList.productCategoryId}"/>"><c:out value="${proList.customerTypeName}"/></option>
                                </c:forEach>
                            </select>
                        </c:if>  -->
                        <select name="orderType" id="orderType" onchange="showOrderType();" >
                            <option value="0">--Select---</option>
                            <option value="1">Direct Order</option>
                            <option value="2">HUB Order</option>
                        </select>

                    </td>
                    <td class="text1" colspan="4" align="left" >&nbsp;<label id="temperatureInfo"></label></td>
                </tr>
                    </table>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Consignment / Route Details</td>
                        </tr>
                        
                            <tr id="contractRouteDiv1" style="display:none;">
                                <td class="text1"><font color="red">*</font>Origin&nbsp;&nbsp;
                                    <select id="contractRouteOrigin" name="contractRouteOrigin" onchange='setContractOriginDestination();' style="width:100px">
                                        <option value="0">-Select-</option>
                                    </select>
                                </td>
                                <td class="text1"><font color="red">*</font>Destination&nbsp;&nbsp;
                                    <select id="contractRouteDestination" name="contractRouteDestination" onchange='setContractRouteDetails();' style="width:100px">
                                        <option value="0">-Select-</option>
                                    </select>
                                </td>
                                <td class="text1" colspan="2" >&nbsp;</td>
<!--                                <td class="text1"><font color="red">*</font>Contract Route</td>
                                <td class="text1" colspan="3" >
                                    <select id="contractRoute" name="contractRoute" onchange='setOriginDestination();' style="width:350px">
                                        <option value="0">-Select-</option>
                                    </select>
                                </td>-->
                            </tr>
                            <input type="hidden" name="origin" value="" />
                            <input type="hidden" name="destination" value="" />
                            <input type="hidden" name="destinationHub" id="destinationHub" value="" />
                            <input type="hidden" name="originHub" id="originHub"value="" />
                        
                        <tr  id="contractRouteDiv2" style="display:none;">
                            <td class="text1"><font color="red">*</font>Origin</td>
                            <td class="text1">
                                <select id="originTemp" name="originTemp" onchange='setDestination();' class="textbox" style="width:150px">
                                    <option value="0">-Select-</option>
                                </select>
                            </td>
                            <td class="text1"><font color="red">*</font>Destination</td>
                            <td class="text1"><select id="destinationTemp" name="destinationTemp" class="textbox" onchange="setRouteDetails();" style="width:150px">
                                    <option value="0">-Select-</option>
                                </select></td>                            
                        </tr>'

                        <tr id="routeChart" style="display:none;" >
                            <td colspan="6" align="left" >
                                <table class="border" align="left" width="900" cellpadding="0" cellspacing="0" id="routePlan">
                                    <tr>
                                        <td class="contenthead" height="30" ><font color="red">*</font>Order Sequence</td>
                                        <td class="contenthead" height="30" ><font color="red">*</font>Point Name</td>
                                        <td class="contenthead" height="30" ><font color="red">*</font>Point Type</td>
                                        <td class="contenthead" height="30" ><font color="red">*</font>Address</td>
                                        <td class="contenthead" height="30" ><font color="red">*</font>Required Date</td>
                                        <td class="contenthead" height="30" ><font color="red"></font>Required Time</td>
                                    </tr>

                                </table>
                                <br>
                                <table class="border" align="left" width="900" cellpadding="0" cellspacing="0" >
                                    <tr>
                                        <td>
                                            <input type="button" class="button" id="routePlanAddRow" style="display:none;" value="Interim Point" name="save" onClick="addRouteCourse2();">
                                            &nbsp;&nbsp;<input type="button" class="button" id="freezeRoute" style="display:none;"  value="Freeze Route" name="save" onClick="getContractVehicleType();">
                                            &nbsp;&nbsp;<input type="button" class="button" id="resetRoute" style="display:none;"  value="Reset" name="save" onClick="resetRouteInfo();">
                                            &nbsp;&nbsp;<input type="button" class="button" id="unFreezetRoute" style="display:none;"  value="UnFreeze Route" name="save" onClick="unFreezetRouteFn();">
                                        </td>
                                    </tr>
                                </table>
<!--                                    <a  class="previoustab" href=""><input type="button" class="button" value="<back" name="Save" /></a>-->
                                    
<!--                                    <a  class="nexttab" href=""><input type="button" class="button" value="next>" name="Save" /></a>-->
                                </center>
                                <br>


                            </td>
                        </tr>

                        
<!--                                 <select name="businessType" class="textbox"  id="businessType" style="width:120px;">
                                    <option value="1" selected> Primary  </option>
                                    <option value="2"> Secondary  </option>
                                </select>-->
                                <input type="hidden" name="businessType" value="1" />
                        <tr>
                            <td class="text2">Special Instruction &nbsp;&nbsp;&nbsp;&nbsp;<textarea rows="1" cols="16" name="consignmentOrderInstruction" id="consignmentOrderInstruction"></textarea></td>
<!--                            <td class="text2">Multi Pickup</td>-->
                            <td class="text2"><input type="hidden" class="textbox" name="multiPickup" id="multiPickup" onclick="multiPickupShow()" value="N"></td>
<!--                            <td class="text2">Multi Delivery</td>-->
                                <td class="text2" colspan="4" ><input type="hidden" class="textbox" name="multiDelivery" id="multiDelivery" onclick="multiDeliveryShow()" value="N"></td>
                        </tr>

                        <tr>
                            <td colspan="4" >

                                <table border="0" class="border" align="left" width="700" cellpadding="0" cellspacing="0" id="addTyres1">
                                    <tr>
                                        <td width="20" class="contenthead" align="center" height="30" >Sno</td>
                                        <td class="contenthead" height="30" >Product/Article Code</td>
                                        <td class="contenthead" height="30" >Product/Article Name </td>
                                        <td class="contenthead" height="30" >Batch </td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>No of Packages</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Uom</td>
                                         <td class="contenthead" height="30" ><font color='red'>*</font>Product Volume</td>
                                        <td class="contenthead" height="30" ><font color='red'>*</font>Total Weight (in Kg)</td>
                                    </tr>
                                    <br>

                                    <tr>
                                        <td colspan="5" align="left">
<!--                                            &nbsp;&nbsp;&nbsp;<input type="reset" class="button" value="Clear">-->
                                            <input type="button" class="button" value="Add Row" name="save" onClick="addRow1()">

                                        </td>
                                    </tr>
                                </table>
                                <br>
                                <br>
                            </td>
                            <td colspan="2">
                                <table border="0"  align="center" width="300" cellpadding="0" cellspacing="0">
                                   
                                    <tr>
                                        <td>
                                            <label class="contentsub">Total No Packages</label>
                                            <label id="totalPackages">0</label>
                                            <input type="hidden" id="totalPackage" name="totalPackage" />
                                        </td>
                                        <td>
                                            <label class="contentsub">Total Weight (Kg)</label>
                                            <label id="totalWeight">0</label>
                                            <input type="hidden" id="totalWeightage" name="totalWeightage" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
<!--                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                         <tr>
                            <td class="contenthead" colspan="6" >CFS Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Bill Entry No</td>
                            <td class="text1">Bill Entry Date</td>
                            <td class="text1">Duty Payment Date</td>
                            <td class="text1">OCC Date</td>
                            <td class="text1">CFS Person Name</td>
                            <td class="text1">CFS Person No</td>
                        </tr>
                        <tr>
                            <td class="text2"> <input type="text" name="billEntryNo" id="billEntryNo" class="textbox" > </td>
                            <td class="text2"> <input type="text" name="billEntryDate" id="billEntryDate" class="datepicker" > </td>
                            <td class="text2"> <input type="text" name="dutyPaymentDate" id="dutyPaymentDate" class="datepicker" > </td>
                            <td class="text2"> <input type="text" name="occDate" id="occDate" class="datepicker" > </td>
                            <td class="text2"> <input type="text" name="cfsPersonName" id="cfsPersonName" class="textbox" > </td>
                            <td class="text2"> <input type="text" name="cfsPersonNo" id="cfsPersonNo" class="textbox" value="0" > </td>
                        </tr>
                    </table>
                    <br>-->
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">

                        <tr>
                            <td class="contenthead" colspan="6" >Vehicle (Required) Details</td>
                        </tr>
                       <tr>
                            <td class="text1"><font color="red">*</font>Container Type</td>
                          <td class="text2"  colspan="5" > <select name="containerType" id="containerType" class="textbox"  style="width:120px;" onchange="setFreightRate();">
                                    <c:if test="${containerTypeList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${containerTypeList}" var="containerType">
                                            <option value="<c:out value="${containerType.containerId}"/>"><c:out value="${containerType.containerName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>              </tr>
                        <tr> <td class="text1"><font color="red">*</font>Container NO</td>
                            <td class="text1">
                          <input type="text" name="containerNO" id="containerNO" class="textbox"  >
                                                    </td></tr>
                        <td class="text1"><font color="red">*</font>Container Loacation</td>
                            <td class="text1">
                          <input type="text" name="containerLoc" id="containerLoc" onKeyPress="return onKeyPressBlockNumbers(event);" class="textbox" >
                                                    </td>
                            <input type="hidden" name="locationId" id="locationId" value="1" />
                        
<!--                            <td class="text1">Service Type</td>
                            <td class="text1">
                                <select name="serviceType" class="textbox"  id="paymentType" style="width:120px;">
                                    <option value="0" selected>--Select--</option>
                                    <option value="1"> FTL </option>
                                    <option value="2"> LTL </option>
                                </select>
                            </td>-->
                            <input type="hidden" name="serviceType" value="1" />
                            <input type="hidden" name="vehTypeId" value="0" />
                            <tr id="vehicleTypeDiv" style="display:none;">
                            <td class="text2">Vehicle Type</td>
                            <td class="text2"  colspan="5" > <select name="vehTypeIdTemp" id="vehTypeIdTemp" class="textbox"  style="width:120px;" onchange="setFreightRate();">
                                    <c:if test="${vehicleTypeList != null}">
                                        <option value="0" selected>--Select--</option>
                                        <c:forEach items="${vehicleTypeList}" var="vehicleType">
                                            <option value="<c:out value="${vehicleType.vehicleTypeId}"/>"><c:out value="${vehicleType.vehicleTypeName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                            </tr>
                            <tr id="vehicleTypeContractDiv" style="display:none;">
                            <td class="text2">
                                Vehicle Type &nbsp;&nbsp;                                
                                
                            </td>
                            <td class="text2"  colspan="5" > <select name="vehTypeIdContractTemp" id="vehTypeIdContractTemp"  onchange="setFreightRate1();" class="textbox"  style="width:120px;">
                                    <option value="0" selected>--Select--</option>
                                </select></td>
                            </tr>
                            <tr>
                            
                            <td class="text1" colspan="5" >
                                <input type="hidden" readonly  name="reeferRequired" id="reeferRequired"  class="textbox"  value="" />
<!--                                <select name="reeferRequired" id="reeferRequired"  class="textbox"  style="width:120px;" onchange="calculateFreightTotal()">
                                    <option value="Yes" > Yes </option>
                                    <option value="No"> No </option>
                                </select>-->
                            </td>
                        </tr>
                        <script>

                            function setRouteDetails() {
                                $("#routeChart").show();
                                var temp = document.getElementById('destinationTemp').value;
                               // alert(temp);
                                var destinationSelect = document.getElementById('destinationTemp');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;

                                var originSelect = document.getElementById('originTemp');
                                var originName = originSelect.options[originSelect.selectedIndex].text;

                                var tempVal = temp.split('-');
                                document.cNote.destination.value = tempVal[1];
                                //alert(document.cNote.destination.value);
                                document.cNote.routeId.value = tempVal[0];
                                document.cNote.routeBased.value = 'Y';
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if(podRowCount1 > 1){
                                    for(var x=1; x<podRowCount1; x++){
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                //alert(document.cNote.destinationTemp.value);
                                if((document.cNote.destinationTemp.value != '0') &&(document.cNote.destinationTemp.value != '')){
                                    //alert("am here...");
                                    var startDate = document.cNote.startDate.value;
                                   if(document.getElementById("orderType").value=="2"){
                                    addRouteCourse1(document.cNote.origin.value,originName,'1','Pick Up',0,0,0,startDate);
                                    addRouteCourseNew(document.cNote.origin.value,originName,'2','Origin Hub',0,0,0,startDate);
                                    addRouteCourseNew(document.cNote.origin.value,originName,'3','Destination Hub',0,0,0,startDate);
                                    addRouteCourse1(document.cNote.destination.value,destinationName,'4','Drop',tempVal[0],tempVal[1],tempVal[2],'');
                                   }else{

                                    addRouteCourse1(document.cNote.origin.value,originName,'1','Pick Up',0,0,0,startDate);
                                    addRouteCourse1(document.cNote.destination.value,destinationName,'2','Drop',tempVal[0],tempVal[1],tempVal[2],'');
                                   }
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                }else{
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }
                            }
                            
                            function setContractRouteDetails() {

                                $("#routeChart").show();
                                var origin = document.getElementById('contractRouteOrigin').value;
                                var destination = document.getElementById('contractRouteDestination').value;
                                //alert(origin + " ::: " + destination);
                                var destinationSelect = document.getElementById('contractRouteDestination');
                                var destinationName = destinationSelect.options[destinationSelect.selectedIndex].text;

                                var originSelect = document.getElementById('contractRouteOrigin');
                                var originName = originSelect.options[originSelect.selectedIndex].text;

                                document.cNote.origin.value = origin;
                                document.cNote.destination.value = destination;
                                //remove table rows and reset values
                                var tab = document.getElementById("routePlan");
                                //alert(tab.rows.length);
                                //alert(podRowCount1);
                                if(podRowCount1 > 1){
                                    for(var x=1; x<podRowCount1; x++){
                                        document.getElementById("routePlan").deleteRow(1);
                                    }
                                }
                                podRowCount1 = 1;
                                podSno1 = 0;
                                if(document.cNote.origin.value != '0' && document.cNote.destination.value != '0'){
                                    //alert("am here...");
                                    //function addRouteCourse1(id, name, order, type,routeId,routeKm,routeReeferHr) {
                                    var startDate = document.cNote.startDate.value;
                                    addRouteCourse3(document.cNote.origin.value,originName,'1','Pick Up',0,0,0,startDate);
                                    addRouteCourse3(document.cNote.destination.value,destinationName,'2','Drop',0,0,0,'');
                                    $("#routePlanAddRow").show();
                                    $("#freezeRoute").show();
                                    $("#resetRoute").show();
                                }else{
                                    $("#routePlanAddRow").hide();
                                    $("#freezeRoute").hide();
                                    $("#resetRoute").hide();
                                }


                            }

                            function setFreightRate() {
                                var temp = document.getElementById("vehTypeIdTemp").value;
                                var tempVal = temp.split("-");
                                document.cNote.vehTypeId.value = tempVal[0];
                                document.cNote.vehicleTypeId.value = tempVal[0];

                                //alert(document.cNote.vehTypeId.value);
                                //alert(document.cNote.vehicleTypeId.value);

                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;                                
                                estimateFreight();
                            }
                            function setFreightRate1() {
                                var temp = document.getElementById("vehTypeIdContractTemp").value;
                                var tempVal = temp.split("-");
                                document.cNote.vehTypeId.value = tempVal[1];
                                document.cNote.vehicleTypeId.value = tempVal[1];
                                document.cNote.routeContractId.value = tempVal[0];


                                //alert(document.cNote.vehTypeId.value);
                                //alert(document.cNote.vehicleTypeId.value);

                                var billingTypeId = document.getElementById("billingTypeId").value;
                                var customerTypeId = document.getElementById("customerTypeId").value;
                                var temp;
                                estimateFreight();
                            }

                            function calculateFreightTotal() {
                                var reeferRequired = document.getElementById('reeferRequired').value;
                                var billingTypeId = document.getElementById('billingTypeId').value;
                                var rateWithReefer = document.getElementById('rateWithReefer').value;
                                var rateWithoutReefer = document.getElementById('rateWithoutReefer').value;
                                var totalKm = document.getElementById('totalKm').value;
                                var totalWeight = $("#totalWeight").text();
                                var totalAmount = 0;
                                if (reeferRequired == 'Yes' && billingTypeId == 1) {
                                    $('#freightAmount').text(rateWithReefer)
                                    $('#totFreightAmount').val(rateWithReefer)
                                    $('#totalCharges').val(rateWithReefer)
                                } else if (reeferRequired == 'No' && billingTypeId == 1) {
                                    $('#freightAmount').text(rateWithoutReefer)
                                    $('#totFreightAmount').val(rateWithoutReefer)
                                    $('#totalCharges').val(rateWithoutReefer)
                                } else if (reeferRequired == 'Yes' && billingTypeId == 2) {
                                    totalAmount = totalWeight * rateWithReefer;
                                    alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'No' && billingTypeId == 2) {
                                    totalAmount = totalWeight * rateWithReefer;
                                    alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'Yes' && billingTypeId == 3) {
                                    totalAmount = totalKm * rateWithReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                } else if (reeferRequired == 'No' && billingTypeId == 3) {
                                    totalAmount = totalKm * rateWithoutReefer;
                                    //alert(totalAmount);
                                    $('#freightAmount').text(totalAmount)
                                    $('#totFreightAmount').val(totalAmount)
                                    $('#totalCharges').val(totalAmount)
                                }
                            }
                        </script>
                        <tr>
<!--                            <td class="text2"><font color="red">*</font>Vehicle Required Date</td>
                            <td class="text2">-->
                                <input type="hidden" class="textbox" name="destinationId" id="destinationId" >
                                <input type="hidden" class="textbox" name="routeContractId" id="routeContractId" >
                                <input type="hidden" class="textbox" name="routeId" id="routeId" >
                                <input type="hidden" class="textbox" name="routeBased" id="routeBased" >
                                <input type="hidden" class="textbox" name="contractRateId" id="contractRateId" >
                                <input type="hidden" class="textbox" name="totalKm" id="totalKm" >
                                <input type="hidden" class="textbox" name="totalHours" id="totalHours" >
                                <input type="hidden" class="textbox" name="totalMinutes" id="totalMinutes" >
                                <input type="hidden" class="textbox" name="totalPoints" id="totalPoints" >
                                <input type="hidden" class="textbox" name="vehicleTypeId" id="vehicleTypeId" >
                                <input type="hidden" class="textbox" name="rateWithReefer" id="rateWithReefer" >
                                <input type="hidden" class="textbox" name="rateWithoutReefer" id="rateWithoutReefer" >
                                <input type="hidden" readonly class="datepicker" name="vehicleRequiredDate" id="vehicleRequiredDate" >
                            
                                <input type="hidden"   name="vehicleRequiredHour" id="vehicleRequiredHour" >
                                <input type="hiddden"   name="vehicleRequiredMinute" id="vehicleRequiredMinute" >
                               
                            <td class="text2">Special Instruction</td>
                            <td class="text2" colspan="5" ><textarea rows="1" cols="16" name="vehicleInstruction" id="vehicleInstruction"></textarea></td>
                        </tr>
                        <script>
                            function calculateTime() {
                                var date1 = new Date();
                                var tempDate = $("#vehicleRequiredDate").val();
                                var temp = tempDate.split("-");
                                var requiredDay = temp[0];
                                var requiredMonth = temp[1];
                                var requiredYear = temp[2];
                                var requiredHour = $("#vehicleRequiredHour").val();
                                if (requiredHour == "") {
                                    requiredHour = "00";
                                }
                                var requiredMinute = $("#vehicleRequiredMinute").val();
                                if (requiredMinute == "") {
                                    requiredMinute = "00";
                                }
                                var requiredSeconds = "00";
                                var date2 = new Date(requiredYear, requiredMonth, requiredDay, requiredHour, requiredMinute, requiredSeconds)
                                var diff = Math.abs(date1.getTime() - date2.getTime()) / 1000 / 60 / 60;
                                alert(diff);
                                var diffDays = diff.toFixed(2);
                                alert(diffDays);
                                if (diffDays < 10) {
                                    alert("No")
                                } else {
                                    alert("Yes");
                                }
                            }
                        </script>
                    </table>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Consignor Details</td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Consignor Name</td>
                            <td class="text2"><input type="text" class="textbox" id="consignorName" onKeyPress="return onKeyPressBlockNumbers(event);" name="consignorName" ></td>
                            <td class="text2"><font color="red">*</font>Mobile No</td>
                            <td class="text2"><input type="text" class="textbox" id="consignorPhoneNo" maxlength="12" onKeyPress="return onKeyPressBlockCharacters(event);"  name="consignorPhoneNo" ></td>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><textarea rows="1" cols="16" name="consignorAddress" id="consignorAddress"></textarea> </td>
                        </tr>
                    </table>
                    <br>
                    <br>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Consignee Details</td>
                        </tr>
                        <tr>
                            <td class="text2"><font color="red">*</font>Consignee Name</td>
                            <td class="text2"><input type="text" class="textbox" id="consigneeName" onKeyPress="return onKeyPressBlockNumbers(event);"  name="consigneeName" ></td>
                            <td class="text2"><font color="red">*</font>Mobile No</td>
                            <td class="text2"><input type="text" class="textbox" id="consigneePhoneNo" maxlength="12"  onKeyPress="return onKeyPressBlockCharacters(event);"   name="consigneePhoneNo" ></td>
                            <td class="text2"><font color="red">*</font>Address</td>
                            <td class="text2"><textarea rows="1" cols="16" name="consigneeAddress" id="consigneeAddress"></textarea> </td>
                        </tr>
                    </table>
                    <br/>
                    <br/>
<!--                    <center>
                        <a  class="nexttab" href=""><input type="button" class="button" value="Next" name="Save" ></a>
                    </center>-->
                </div>





                <script>
                    $(".nexttab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected + 1);
                    });
                    $(".previoustab").click(function() {
                        var selected = $("#tabs").tabs("option", "selected");
                        $("#tabs").tabs("option", "selected", selected - 1);
                    });
                </script>
<!--                <div id="paymentDetails">-->
        <table border="0" class="border" align="center" width="97.5%" cellpadding="0" cellspacing="0"  id="creditLimitTable" style="display: none">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6" >Credit Limit Details</td>
                        </tr>
                        <tr>
                            <td class="text2">Credit Days &nbsp; </td>
                            <td class="text2"><label id="creditDaysTemp"></label><input type="hidden" name="creditDays" id="creditDays" /> </td>
                            <td class="text2" align="left" >Credit Limit &nbsp; </td>
                            <td class="text2"><label id="creditLimitTemp"></label><input type="hidden" name="creditLimit" id="creditLimit" /></td>
                        </tr>
                        <tr>
                            <td class="text1" align="left" >Customer Rank &nbsp; </td>
                            <td class="text1"><label id="customerRankTemp"></label><input type="hidden" name="customerRank" id="customerRank" /></td>
                            <td class="text1" align="left" >Approval Status&nbsp; </td>
                            <td class="text1"><label id="approvalStatusTemp"></label><input type="hidden" name="approvalStatus" id="approvalStatus" /></td>
                        </tr>
                        <tr>
                            <td class="text2" align="left" >Out Standing &nbsp; </td>
                            <td class="text2"><label id="outStandingTemp"></label><input type="hidden" name="outStanding" id="outStanding" /></td>
                            <td class="text2" align="left" >Out Standing Date&nbsp; </td>
                            <td class="text2"><label id="outStandingDateTemp"></label><input type="hidden" name="outStandingDate" id="outStandingDate" /></td>
                        </tr>
                        
                    </table>
                    <table  border="0" class="border" align="center" width="97.5%" cellpadding="0" cellspacing="0"  id="contractFreightTable">
                        
                        <tr>
                            <td class="contentsub"  height="30" colspan="6" > Freight Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Billing Type</td>
                            <td class="text1"><label id="billTypeName"></label></td>
                            <td class="text1" align="right" >Estimated Freight Charges &nbsp; </td>
                            <td class="text1">SAR.
                                <input type="text" readonly class="textbox" name="totFreightAmount" id="totFreightAmount" value="0"/>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                Freight Agree &nbsp;&nbsp;
                                <input type="checkbox" name="freightAcceptedStatus" id="freightAcceptedStatus" onclick="checkFreightAcceptedStatus();" class="textbox" checked="checked" />
                            </td>
                        </tr>
                    </table>
                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="walkinFreightTable">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6" >Walkin Freight Details</td>
                        </tr>
                        <tr>
                            <td class="text1">Billing Type</td>
                            <td class="text1"><select name="walkInBillingTypeId" id="walkInBillingTypeId" class="texttbox" onchange="displayWalkIndetails(this.value)" style="width: 120px">                                    
                                        <option value="0" selected>--Select--</option>
                                        <option value="1" >Fixed Freight</option>
                                        <option value="2" >Rate Per KM</option>
                                        <option value="3" >Rate Per Kg</option>
                                </select></td>
                            <td class="text1">Freight Charges (SAR.)</td>
                            <td class="text1">
                                <input type="text" readonly class="textbox" name="totFreightAmount1" id="totFreightAmount1" value="0"/>
                                &nbsp;
                                &nbsp;
                                &nbsp;
                                
                            </td>
                        </tr>
                        <tr id="WalkInptp" style="display: none">
                            <td class="text2">Freight With Reefer</td>
                            <td class="text2"><input type="text" name="walkinFreightWithReefer" id="walkinFreightWithReefer" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                            <td class="text2">Freight Without Reefer</td>
                            <td class="text2"><input type="text" name="walkinFreightWithoutReefer" id="walkinFreightWithoutReefer" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        </tr>
                        <tr id="WalkInPtpw" style="display: none">
                            <td class="text2">Rate With Reefer/Kg</td>
                            <td class="text2"><input type="text" name="walkinRateWithReeferPerKg" id="walkinRateWithReeferPerKg" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                            <td class="text2">Rate Without Reefer/Kg</td>
                            <td class="text2"><input type="text" name="walkinRateWithoutReeferPerKg" id="walkinRateWithoutReeferPerKg" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        </tr>
                        <tr id="WalkinKilometerBased" style="display: none">
                            <td class="text2">Rate With Reefer/Km</td>
                            <td class="text2"><input type="text" name="walkinRateWithReeferPerKm" id="walkinRateWithReeferPerKm" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                            <td class="text2">Rate Without Reefer/Km</td>
                            <td class="text2"><input type="text" name="walkinRateWithoutReeferPerKm" id="walkinRateWithoutReeferPerKm" class="textbox" style="width: 120px" onkeyup="calculateSubTotal()"/></td>
                        </tr>
                    </table>
                    <script type="text/javascript">
                        function displayWalkIndetails(val) {
                            var reeferRequired = document.getElementById('reeferRequired').value;
                            var vehicleTypeId = document.getElementById('vehTypeIdTemp').value;
                            //alert(vehicleTypeId);
                            if (vehicleTypeId == '0') {
                                alert("Please Choose Vehicle Type");
                                document.cNote.walkInBillingTypeId.value=0;
                                document.getElementById('vehTypeIdTemp').focus();
                            } else if (reeferRequired == '0') {
                                alert("Please Choose Reefer Details");
                                document.getElementById('reeferRequired').focus();
                            } else {
                                if (val == 1) {
                                    $("#WalkInptp").show();
                                    $("#WalkInPtpw").hide();
                                    $("#WalkinKilometerBased").hide();
                                    if (reeferRequired == "Yes") {
                                        $('#walkinFreightWithReefer').attr('readonly', false);
                                        $('#walkinFreightWithoutReefer').attr('readonly', true);
                                    } else if (reeferRequired == "No") {
                                        $('#walkinFreightWithReefer').attr('readonly', true);
                                        $('#walkinFreightWithoutReefer').attr('readonly', false);
                                    }
                                    $("#walkinRateWithReeferPerKm").val("");
                                    $("#walkinRateWithoutReeferPerKm").val("");
                                    $("#walkinRateWithReeferPerKg").val("");
                                    $("#walkinRateWithoutReeferPerKg").val("");
                                } else if (val == 2) {
                                    $("#WalkInptp").hide();
                                    $("#WalkInPtpw").show();
                                    $("#WalkinKilometerBased").hide();
                                    if (reeferRequired == "Yes") {
                                        $('#walkinRateWithReeferPerKg').attr('readonly', false);
                                        $('#walkinRateWithoutReeferPerKg').attr('readonly', true);
                                    } else if (reeferRequired == "No") {
                                        $('#walkinRateWithReeferPerKg').attr('readonly', true);
                                        $('#walkinRateWithoutReeferPerKg').attr('readonly', false);
                                    }
                                    $("#walkinFreightWithReefer").val("");
                                    $("#walkinFreightWithoutReefer").val("");
                                    $("#walkinRateWithReeferPerKm").val("");
                                    $("#walkinRateWithoutReeferPerKm").val("");
                                } else if (val == 3) {
                                    $("#WalkInptp").hide();
                                    $("#WalkInPtpw").hide();
                                    $("#WalkinKilometerBased").show();
                                    if (reeferRequired == "Yes") {
                                        $('#walkinRateWithReeferPerKm').attr('readonly', false);
                                        $('#walkinRateWithoutReeferPerKm').attr('readonly', true);
                                    } else if (reeferRequired == "No") {
                                        $('#walkinRateWithReeferPerKm').attr('readonly', true);
                                        $('#walkinRateWithoutReeferPerKm').attr('readonly', false);
                                    }
                                    $("#walkinFreightWithReefer").val("");
                                    $("#walkinFreightWithoutReefer").val("");
                                    $("#walkinRateWithReeferPerKg").val("");
                                    $("#walkinRateWithoutReeferPerKg").val("");
                                }
                            }
                        }
                    </script>
                    <br/>
                    <br/>
<!--                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="6">Standard / Additional Charges</td>
                        </tr>
                        <tr>
                            <td class="text1">Document Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="docCharges" name="docCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text1">ODA Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="odaCharges" name="odaCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text1">Multi Pickup Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="multiPickupCharge" name="multiPickupCharge" readonly onkeyup="calculateSubTotal()"></td>
                        </tr>
                        <tr>
                            <td class="text2">Multi Delivery Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="multiDeliveryCharge" name="multiDeliveryCharge" readonly onkeyup="calculateSubTotal()"></td>
                            <td class="text2">Handling Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="handleCharges" name="handleCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text2">Other Charges</td>
                            <td class="text2"><input type="text" class="textbox" id="otherCharges" name="otherCharges" onkeyup="calculateSubTotal()"></td>
                        </tr>
                        <tr>                            
                            <td class="text1">Unloading Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="unloadingCharges" name="unloadingCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text1">Loading Charges</td>
                            <td class="text1"><input type="text" class="textbox" id="loadingCharges" name="loadingCharges" onkeyup="calculateSubTotal()"></td>
                            <td class="text1">Remarks</td>
                            <td class="text1"><textarea rows="3" cols="20" nmae="standardChargeRemarks" id="standardChargeRemarks"></textarea></td>
                        </tr>
                        <tr>
                            <td class="text2" colspan="4"></td>
                            <td class="text2">Sub Total</td>
                            <td class="text2"><input type="text" class="textbox" id="subTotal" name="subTotal"  value="0" readonly=""></td>
                        </tr>
                    </table>-->
                    <input type="hidden" class="textbox" id="subTotal" name="subTotal"  value="0" readonly="">
                    <script type="text/javascript">
                        function calculateSubTotal1() {
                            var customerTypeId = document.getElementById("customerTypeId").value;
                            if (customerTypeId == 2) {
                                var fixedRateWithReefer = $("#walkinFreightWithReefer").val();
                                var fixedRateWithoutReefer = $("#walkinFreightWithoutReefer").val();
                                var rateWithReeferPerKm = $("#walkinRateWithReeferPerKm").val();
                                var rateWithoutReeferPerKm = $("#walkinRateWithoutReeferPerKm").val();
                                var rateWithReeferPerKg = $("#walkinRateWithReeferPerKg").val();
                                var rateWithoutReeferPerKg = $("#walkinRateWithoutReeferPerKg").val();
                            }
                            var freightAmount = $('#freightAmount').text();
                            var docCharges = document.getElementById('docCharges').value;
                            var odaCharges = document.getElementById('odaCharges').value;
                            var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                            var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                            var handleCharges = document.getElementById('handleCharges').value;
                            var otherCharges = document.getElementById('otherCharges').value;
                            var unloadingCharges = document.getElementById('unloadingCharges').value;
                            var loadingCharges = document.getElementById('loadingCharges').value;
                            var total = 0;
                            var walkInTotal = 0;
                            var kmRate = 0;
                            var kgRate = 0;
                            if (customerTypeId == 2) {
                                var billingTypeId = $("#walkInBillingTypeId").val();
                                var totalKm = $("#totalKm").val();
                                var totalWeight = $("#totalWeight").text();
                                if (fixedRateWithReefer != '' && billingTypeId == 1) {
                                    walkInTotal += parseInt(fixedRateWithReefer);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (fixedRateWithoutReefer != '' && billingTypeId == 1) {
                                    walkInTotal += parseInt(fixedRateWithoutReefer);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithReeferPerKm != '' && billingTypeId == 3) {
                                    kmRate = parseInt(totalKm) * parseInt(rateWithReeferPerKm);
                                    walkInTotal += parseInt(kmRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithoutReeferPerKm != '' && billingTypeId == 3) {
                                    kmRate = parseInt(totalKm) * parseInt(rateWithoutReeferPerKm);
                                    walkInTotal += parseInt(kmRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithReeferPerKg != '' && billingTypeId == 2) {
                                    kgRate = parseInt(totalWeight) * parseInt(rateWithReeferPerKg);
                                    walkInTotal += parseInt(kgRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                                if (rateWithoutReeferPerKg != '' && billingTypeId == 2) {
                                    kgRate = parseInt(totalWeight) * parseInt(rateWithoutReeferPerKg);
                                    walkInTotal += parseInt(kgRate);
                                } else {
                                    walkInTotal += parseInt(0);
                                }
                            }
                            if (docCharges != '') {
                                total += parseInt(docCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (odaCharges != '') {
                                total += parseInt(odaCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiPickupCharge != '') {
                                total += parseInt(multiPickupCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiDeliveryCharge != '') {
                                total += parseInt(multiDeliveryCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (handleCharges != '') {
                                total += parseInt(handleCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (otherCharges != '') {
                                total += parseInt(otherCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (unloadingCharges != '') {
                                total += parseInt(unloadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (loadingCharges != '') {
                                total += parseInt(loadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            document.getElementById('subTotal').value = total;
                            if (customerTypeId == 1) {
                                if (freightAmount != '') {
                                    total += parseInt(freightAmount);
                                } else {
                                    total += parseInt(0);
                                }
                            } else if (customerTypeId == 2) {
                                total += parseInt(walkInTotal);
                            }
                            document.getElementById('totalCharges').value = total;
                        }
                        function calculateSubTotal() {
                            estimateFreight();
                            var freightAmount = document.cNote.totFreightAmount.value;
                            var docCharges = document.getElementById('docCharges').value;
                            var odaCharges = document.getElementById('odaCharges').value;
                            var multiPickupCharge = document.getElementById('multiPickupCharge').value;
                            var multiDeliveryCharge = document.getElementById('multiDeliveryCharge').value;
                            var handleCharges = document.getElementById('handleCharges').value;
                            var otherCharges = document.getElementById('otherCharges').value;
                            var unloadingCharges = document.getElementById('unloadingCharges').value;
                            var loadingCharges = document.getElementById('loadingCharges').value;
                            var total = 0;
                            var walkInTotal = 0;
                            var kmRate = 0;
                            var kgRate = 0;

                            if (docCharges != '') {
                                total += parseInt(docCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (odaCharges != '') {
                                total += parseInt(odaCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiPickupCharge != '') {
                                total += parseInt(multiPickupCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (multiDeliveryCharge != '') {
                                total += parseInt(multiDeliveryCharge);
                            } else {
                                total += parseInt(0);
                            }
                            if (handleCharges != '') {
                                total += parseInt(handleCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (otherCharges != '') {
                                total += parseInt(otherCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (unloadingCharges != '') {
                                total += parseInt(unloadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            if (loadingCharges != '') {
                                total += parseInt(loadingCharges);
                            } else {
                                total += parseInt(0);
                            }
                            document.getElementById('subTotal').value = total.toFixed(2);
                            total = total + parseFloat(freightAmount);
                            document.getElementById('totalCharges').value = total.toFixed(2);
                        }
                    </script>
                    <br/>
<!--                    <table  border="0" class="border" align="center" width="100%" cellpadding="0" cellspacing="0" id="bg">
                        <tr>
                            <td class="contentsub"  height="30" colspan="60">&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </td>
                            <td class="contentsub"  height="30" colspan="6" align="right">Total Charges</td>
                            <td class="contentsub"  height="30" align="right">SAR.<input align="right" value="" type="text" readonly class="textbox" id="totalCharges" name="totalCharges" ></td>
                        </tr>

                    </table>-->
                    <input align="right" value="" type="hidden" readonly class="textbox" id="totalCharges" name="totalCharges" >
                    <br/>
                    <br/>

                    <center>
<!--                        <input type="button" class="button" name="Save" value="Estimate Freight" onclick="estimateFreight();" >-->
                        <div id="orderButton" >
                            <input type="button" class="button" name="Save" value="Create Order" id="createOrder" onclick="submitPage(this.value);" >
                        </div>
                    </center>
<!--                </div>-->

            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>