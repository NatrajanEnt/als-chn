<%-- 
    Document   : dailyStockReport
    Created on : Apr 15, 2016, 12:58:37 PM
    Author     : hp
--%>

<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
        "http://www.w3.org/TR/html4/loose.dtd">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
    <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <script language="javascript" src="/throttle/js/validate.js"></script>
    <script type="text/javascript" src="/throttle/js/TableSort.js"></script>
    <link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
    <script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
    <script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
    <style type="text/css" title="currentStyle">
        @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

    <!-- jQuery libs -->
    <script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
    <script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

    <!-- Our jQuery Script to make everything work -->

    <script  type="text/javascript" src="js/jq-ac-script.js"></script>


    <link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
    <script src="/throttle/js/jquery.ui.core.js"></script>
    <script src="/throttle/js/jquery.ui.datepicker.js"></script>

    <script type="text/javascript">


        function viewTripDetails(tripId) {
            window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        function viewVehicleDetails(vehicleId) {
            window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
        }
        $(document).ready(function () {
            $("#datepicker").datepicker({
                showOn: "button",
                buttonImage: "calendar.gif",
                buttonImageOnly: true

            });
        });

        $(function () {
            //alert("cv");
            $(".datepicker").datepicker({
                /*altField: "#alternate",
                 altFormat: "DD, d MM, yy"*/
                changeMonth: true, changeYear: true
            });
        });

    </script>


</head>
<style>
    table.display thead th {
        text-align: center;
        color: #ffffff;
        /*background: #66a9bd;*/
        background: #595959;
    }
</style>
<body>
    <%@ include file="/content/common/path.jsp" %>
    <center>
        <%@include file="/content/common/message.jsp"%>
    </center>
    <form name="commisionReport"  method="post">

        <style>
            .modal {
                opacity: 0;
                visibility: hidden;
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                text-align: left;
                background: rgba(0,0,0, .9);
                transition: opacity .25s ease;
            }

            .modal__bg {
                position: absolute;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                cursor: pointer;
            }

            .modal-state {
                display: none;
            }

            .modal-state:checked + .modal {
                opacity: 1;
                visibility: visible;
            }

            .modal-state:checked + .modal .modal__inner {
                top: 0;
            }

            .modal__inner {
                transition: top .25s ease;
                position: absolute;
                top: -20%;
                right: 0;
                bottom: 0;
                left: 0;
                width: 95%;
                margin: auto;
                overflow: auto;
                background: #fff;
                border-radius: 5px;
                padding: 1em 2em;
                height: 80%;
            }

            .modal__close {
                position: absolute;
                right: 2em;
                top: 1em;
                width: 1.1em;
                height: 1.1em;
                cursor: pointer;
            }

            .modal__close:after,
            .modal__close:before {
                content: '';
                position: absolute;
                width: 2px;
                height: 1.5em;
                background: #ccc;
                display: block;
                transform: rotate(45deg);
                left: 50%;
                margin: -3px 0 0 -1px;
                top: 0;
            }

            .modal__close:hover:after,
            .modal__close:hover:before {
                background: #aaa;
            }

            .modal__close:before {
                transform: rotate(-45deg);
            }

            @media screen and (max-width: 768px) {

                .modal__inner {
                    width: 90%;
                    height: 90%;
                    box-sizing: border-box;
                }
            }


            /* Other
             * =============================== */
            body {
                padding: 1%;
                font: 1/1.5em sans-serif;
                text-align: center;
            }

            .btn {
                cursor: pointer;
                background: #27ae60;
                display: inline-block;
                padding: .5em 1em;
                color: #fff;
                border-radius: 3px;
            }

            .btn:hover,
            .btn:focus {
                background: #2ecc71;
            }

            .btn:active {
                background: #27ae60;
                box-shadow: 0 1px 2px rgba(0,0,0, .2) inset;
            }

            .btn--blue {
                background: #2980b9;
            }

            .btn--blue:hover,
            .btn--blue:focus {
                background: #3498db;
            }

            .btn--blue:active {
                background: #2980b9;
            }

            p img {
                max-width: 200px;
                height: auto;
                float: left;
                margin: 0 1em 1em 0;
            }
        </style>
        <input class="modal-state" id="modal-1" type="checkbox" style="width: 900px;height: 900px"/>
    <div class="modal">
        <label class="modal__bg" for="modal-1"></label>
        <div class="modal__inner">
            <label class="modal__close" for="modal-1" onclick="clearData();"></label>
            <center><h2 style="font-size:16px;"><span id="mailSpan"></span></h2></center>
            <br>
            <div id="viewMailContentData"></div>

        </div>
    </div> 
        <br>
        <c:if test="${emailTemplate != null}">
            <span id="status" style="text-align: center;color: green"></span>
            <table id="" class="table table-info mb30 table-hover" border="1" cellspacing="0" align="center" >
                <thead>            
                    <tr  >
                        <th bgcolor="skyblue">S.No</th>
                        <th bgcolor="skyblue">EVENT NAME</th>
                        <th bgcolor="skyblue">EVENT CONTENT-TEXT</th>
                        <th bgcolor="skyblue">EVENT CONTENT-DESIGN</th>
                        <th bgcolor="skyblue">ACTION</th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>

                    <%
                    sno++;
                    String className = "text1";
                    if ((sno % 1) == 0) {
                    className = "text1";
                    } else {
                    className = "text2";
                    }
                    int checkBillType=0;
                    %>
                    <tr>
                        <c:forEach items="${emailTemplate}" var="list">
                            <td><%=sno%></td>
                                <td style="text-align: center"><input type="hidden" name="eventId" id="eventId" value="<c:out value="${list.eventId}"/>" /><c:out value="${list.eventName}"/></td>
                                <td style="text-align: center;width: 100px">
                                    <textarea id="eventTemplate" name="eventTemplate" style="width: 600px;height: 300px"><c:out value="${list.eventTemplate}"/></textarea>
                                </td>
                                <td style="text-align: center">
                                    <label class="btn" for="modal-1" id="arrivalMail" onclick="viewContent('<c:out value="${list.eventId}" />')">CONTENT</label><br><br>
                                </td>
                                <td style="text-align: center">
                                    <label class="btn" for="modal-1" id="arrivalMail" onclick="saveTemplate()">UPDATE</label><br><br>
                                </td>
                            <%sno++;%>
                        </tr>
                    </c:forEach>


                </tbody>
            </table>
        </c:if>
<input type="hidden" name="Param" id="Param" value="" >
    </form>
    <script type="text/javascript">
        document.getElementById("branchId").value = '<c:out value="${branch}" />';
//            document.getElementById("fromDate").value = '<c:out value="${fromDate}" />';
//            document.getElementById("toDate").value = '<c:out value="${toDate}" />';
 function submitPage(value)
    {
        document.getElementById("Param").value = value;
            document.commisionReport.action = '/throttle/eMailAndSmsStatusReport.do?Param=' + value;
            document.commisionReport.submit();

    }
        function submitPage1(val) {
            //alert(val);
            var fromDate = document.getElementById("date1").value;
            var todate = document.getElementById("date2").value;
            var param = 1;
            document.commisionReport.action = '/throttle/eMailAndSmsStatusReport.do';
            document.commisionReport.submit();
        }
        function setEmailIds(mailSendingId) {
            alert(mailSendingId);
            $("#mailIdSpan" + mailSendingId).hide();
            $("#mailId" + mailSendingId).show();
        }
        function updateEmailResend(mailSendingId,sendingType) {
            var mailIdTo = $("#mailIdTo" + mailSendingId).text();
            var mailIdCc = $("#mailIdCc" + mailSendingId).text();
            $.ajax({
                url: "/throttle/updateMailResending.do",
                dataType: "json",
                data: {
                    sendingType: sendingType,
                    mailSendingId: mailSendingId,
                    mailIdTo: mailIdTo,
                    mailIdCc: mailIdCc
                },
                success: function (data, textStatus, jqXHR) {
                    if (data.Status == 1) {
                        $("#image" + mailSendingId).hide();
                        $("#mailDeliveredStatus" + mailSendingId).text("Resending");
                        $("#status").text("Resend Updated Sucessfuly");
                    } else {
                        $("#status").text("Resend Updated Failed");
                    }
                },
                error: function (data, type) {
                    console.log(type);
                }
            });
        }
        function viewContent(eventId) {
//            window.open("/throttle/viewMailContent.do?mailSendingId"+mailSendingId);
            $.ajax({
                url: "/throttle/content/report/viewTemplateContent.jsp",
                data: {
                    eventId: eventId
                },
                success: function (data, textStatus, jqXHR) {
                    $("#viewMailContentData").html(data);
                },
                error: function (data, type) {
                    console.log(type);
                }
            });
        }
        
        function saveTemplate(){
            document.commisionReport.action = '/throttle/eMailTemplateSettings.do';
            document.commisionReport.submit();
        }
    </script>


</body>
</html>

