<%--
    Document   : printTripsheet
    Created on : Sep 28, 2015, 11:46:12 AM
    Author     : RAM
--%>


<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@ page import="java.util.* "%>
<%@ page import="ets.domain.util.ThrottleConstants"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script type="text/javascript" src="/throttle/js/code39.js"></script>
        <title>PrintCNote</title>
    </head>
    <body>
        <style type="text/css">
            @media print {
                #printbtn {
                    display :  none;
                }
            }
        </style>
        <style type="text/css" media="print">
            @media print
            {
                @page {
                    margin-top: 0;
                    margin-bottom: 0;
                }
            } 
        </style>
        
        <script>

//            window.onunload = function() {
//                window.opener.location.reload();
//            }
        </script>
        <form name="enter" method="post">
            <style type="text/css">
                #barcode {font-weight: normal; font-style: normal; line-height:normal; font-size: pt}
            </style>
            <%
            Date today = new Date();
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            String startDate = sdf.format(today);
            String billingPartyId = (String)request.getAttribute("billingPartyId");
            String companyId = (String)request.getAttribute("companyId");
            %>
            <div id="printContent">

                <br>
                <br>
                <br>
                <br>
                <br>
                

                    <table align="center"  style="border-collapse: collapse;width:400px;border-color:#000000;border-style:solid;font-size:12px;font-family:Arial;border-left: none;border-right: none;
                           background:url(images/dict-logoWatermark.png);
                           background-repeat:no-repeat;
                           background-position:center;
                           border:1px solid;
                           /*border-color:#CD853F;width:800px;*/
                           border:1px solid;
                           /*opacity:0.8;*/
                           /*font-weight:bold;*/
                           ">
                        <tr >
                            <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border" >
                                <table align="left" >
                                    <tr>
                                        
                                        <td align="left"/>
                                            <!--<img src="images/Chakiat Logo.png" width="190" height="70"/>-->
                                            <font size="2">&emsp;&emsp;&emsp;<b>GSTIN </b>
                                            <c:if test = "${companyId != 1472}" >
                                                33AABFC5853E2ZS
                                            </c:if>
                                            <c:if test = "${companyId == 1472}" >
                                                32AABFC5853E2ZU
                                            </c:if>
                                            </font>                                           
                                        </td>
                                           

                                        <td style="padding-left: 180px; padding-right:42px;">
                                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;




                                    <center>
                                        
                                    <font face="verdana" size="4"><b>
                                        <c:if test = "${companyId != 1472}" >
                                            C.A Logistics India Pvt.Ltd.
                                        </c:if>
                                        <c:if test = "${companyId == 1472}" >
                                            Chakiat Agencies
                                        </c:if>
                                        
                                    </b></font></center>

                                    <center>
                                        
                                        <font face="verdana" size="3" >
                                        <c:if test = "${companyId != 1472}" >
                                            40,Rajaji Salai, Chennai,Tamilnadu.
                                        </c:if>  
                                        <c:if test = "${companyId == 1472}" >
                                            Willingdon Island Cochin – 3, Kerala
                                        </c:if>  
                                        
                                        </font><br><br>
                                       
                                    </center>
                            </td>
                            <td align="right">
                                <br>
                                <div id="externalbox" style="width:3in">
                                    <div id="inputdata" ></div>
                                </div>
                            </td>
                        </tr>
                    </table>
                    </td>
                    </tr>
                 <c:forEach items="${consignmentList}" var="cnl">
                    <td style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                        <table width="100%" cellpadding="0" cellspacing="0" border="0" class="border" style="border-collapse: collapse;border-right: solid #888 1px">
                            <tbody>
                                
                                <tr>
                                    <td valign="top" width="35%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;CNote No:</b></font> <font face="verdana" size="3"> &nbsp;<c:out value="${cnl.consignmentNoteNo}"/></font>
                                        <br>
                                    </td>
                                    <td valign="top" width="35%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;CNote Date:</b></font> <font face="verdana" size="3"> &nbsp;<c:out value="${cnl.consignmentOrderDate}"/></font>
                                        <br>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td valign="top" width="50%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Customer Order Ref No:</b></font> <font face="verdana" size="3"> &nbsp;<c:out value="${cnl.orderReferenceNo}"/></font>
                                        <br>
                                    </td>
                                    <td valign="top" width="50%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Customer Name:</b></font> <font face="verdana" size="3"> &nbsp;<c:out value="${cnl.customerName}"/></font>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" width="35%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Movement Type:</b></font> <font face="verdana" size="3"> &nbsp;<c:out value="${cnl.movementType}"/></font>
                                        <br>
                                    </td>
                                    <td valign="top" width="35%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Goods Desc:</b></font> <font face="verdana" size="3"> &nbsp;<c:out value="${cnl.desc}"/></font>
                                        <br>
                                    </td>

                                </tr>

                                <tr>
                                    <td colspan="2" valign="top" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse" cellpadding="0" cellspacing="0" border="0" class="border">
                            <center><font face="verdana" size="5"><b><br>&nbsp;Route Interim :</b></font>&emsp;&emsp;<font face="verdana" size="5"><c:out value="${cnl.routeName}"/>
                            </center>
                            <br>
                                    </td>
                                </tr>
                                <tr>
                                    
                                    <td colspan="2" valign="top" width="50%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Container Type:</b></font> <font face="verdana" size="3"> &nbsp;20ft:&nbsp;<c:out value="${cnl.containerTypeName}"/>&emsp;&emsp;40ft:&nbsp;<c:out value="${cnl.containerTypeNameForty}"/></font>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td  valign="top" width="50%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Vehicle Required On:</b></font> <font face="verdana" size="3"> &nbsp;<c:out value="${cnl.tripScheduleDate}"/>&nbsp;<c:out value="${cnl.tripScheduleTime}"/></font>
                                                
                                        <br>
                                    </td>
                                    <td valign="top" width="50%" style=" border: solid #888 1px;border-bottom: none;border-collapse: collapse;" cellpadding="0" cellspacing="0" border="0" class="border">
                                        <br>
                                        <font face="verdana" size="3"><b>&nbsp;Ton:</b></font> <font face="verdana" size="3"> &nbsp;
                                        <c:if test = "${pnrUser != 1}" >
                                            <c:if test = "${movementTypeId == 14 || movementTypeId == 16}" >
                                            <c:out value="${cnl.packageNos}"/>/<c:out value="${cnl.totalWeights}"/>/<c:out value="${cnl.totalVolume}"/>
                                            </c:if>
                                            <c:if test = "${movementTypeId != 14 && movementTypeId != 16}" >
                                            
                                            <c:if test="${(cnl.movementTypeId =='1')}" >
                                                    &emsp;<c:out value="${cnl.conWeight}"/>
                                                </c:if>
                                                <c:if test="${(cnl.movementTypeId !='1')}" >
                                                <c:out value="${cnl.conPackages}"/>/<c:out value="${cnl.conWeight}"/>/<c:out value="${cnl.conVolume}"/>
                                                </c:if>    
                                            
                                            </c:if>
                                            </c:if>
                                         </font>
                                                
                                        <br>
                                    </td>
                                    
                                </tr>

                                
                            </tbody>
                        </table>
                                    </c:forEach>
                        </table>
                        <!--Muthu....-:) -->
                </div>
            
            <br>
            <center>
                <input type="button" id="printbtn" class="button"  value="Back" onClick="viewTripDetails();" />
                <input type="button" class="button" id="printbtn" value="Print" onClick="print('printContent');" />
                &nbsp;&nbsp;&nbsp;
            </center>
            <br>
            <br>
        </form>
        <script type="text/javascript">
            function viewTripDetails() {
                document.enter.action = '/throttle/consignmentNoteView.do?&menuClick=1&param=Search';
                document.enter.submit();
            }
            function print(val)
            {
                var DocumentContainer = document.getElementById(val);
                var WindowObject = window.open('', "TrackHistoryData",
                        "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
                WindowObject.document.writeln(DocumentContainer.innerHTML);
                WindowObject.document.close();
                WindowObject.focus();
                WindowObject.print();
                WindowObject.close();
            }
        </script>
        <script type="text/javascript">
            /* <![CDATA[ */
            function get_object(id) {
                var object = null;
                if (document.layers) {
                    object = document.layers[id];
                } else if (document.all) {
                    object = document.all[id];
                } else if (document.getElementById) {
                    object = document.getElementById(id);
                }
                return object;
            }
            //get_object("inputdata").innerHTML = DrawCode39Barcode(get_object("inputdata").innerHTML, 0);
            get_object("inputdata").innerHTML = DrawCode39Barcode('<c:out value="${tripCode}"/>', 0);
            /* ]]> */
        </script>
    </body>
</html>