<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<head>
    <script language="javascript" src="/throttle/js/validate.js"></script>    
    <%@ page import="ets.domain.operation.business.OperationTO" %>   
    <script language="javascript" src="/throttle/js/validate.js"></script>  
    <%@ page import="java.util.*" %>      
    <%@ page import="java.http.*" %>      
    <%@ page import="ets.domain.mrs.business.MrsTO" %>      
    <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
    <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">    

</head>
<script>
    var c = 0;
    function setSelectbox(i)
    {
        c++;
        var selected = document.getElementsByName("selectedIndex");
        selected[i].checked = 1;
    }

    function submitPage()
    {

        var checValidate = selectedItemValidation();
        if (checValidate == 'SubmitForm') {
            document.generateJobCard.action = '/throttle/saveBodePartsWO.do';
            document.generateJobCard.submit();
        }
    }

    function selectedItemValidation() {

        var index = document.getElementsByName("selectedindex");
        var vendorId = document.getElementsByName("vendorId");

        var chec = 0;
        var mess = "SubmitForm";
        for (var i = 0; (i < index.length && index.length != 0); i++) {
            if (index[i].checked) {
                chec++;
                if (isSelect(vendorId[i], 'vendor')) {
                    return 'notSubmit';

                }
            }
            if (chec == 0) {
                alert("Please Select Any One And Then Proceed");
                vendorId[0].focus();
                return 'notSubmit';
            }
            return 'SubmitForm';
        }
    }

</script>   
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Trip Planning</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Operation</a></li>
            <li class="active">TRIP PLANNING</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body>

                <form name="generateJobCard" method="post">


                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <!-- message table -->
                    <%@ include file="/content/common/message.jsp"%>

                    <c:if test = "${vehicleDetails != null}" >    
                        <br>
                        <table align="center" class="table table-info mb30 table-hover" id="bg" >

                            <tr><thead>
                            <th  colspan="4" align="center" height="30">Vehicle Details</th></thead>
                            </tr>
                            <c:forEach items="${vehicleDetails}" var="fservice"> 

                                <tr>
                                    <td   width="200" height="30">Vehicle Number</td>
                                    <td   width="200" height="30"><c:out value="${fservice.regno}"/></td>
                                <td  width="200" height="30">Delivery Time</td>
                                <td  width="200" height="30"><%=request.getAttribute("reqDate")%></td>
                                </tr>

                                <tr>
                                    <td   width="200" height="30">Vehicle Type:</td>
                                    <td   width="200" height="30"><c:out value="${fservice.vehicleTypeName}"/></td>
                                <td  height="30" width="200" >MFG:</td>
                                <td  height="30" width="200" ><c:out value="${fservice.mfrName}"/></td>
                                </tr>

                                <tr>
                                    <td  height="30" width="200" >Use Type:</td>
                                    <td  height="30" width="200" ><c:out value="${fservice.usageName}"/></td>
                                <td  height="30" width="200" >Model:</td>
                                <td  height="30" width="200" ><c:out value="${fservice.modelName}"/></td>
                                </tr>

                                <tr>
                                    <td  height="30" width="200" >Engine No:</td>
                                    <td  height="30" width="200" ><c:out value="${fservice.engineNo}"/></td>
                                <td  height="30" width="200" >Chasis No:</td>
                                <td  height="30" width="200" ><c:out value="${fservice.chassNo}"/></td>
                                </tr>
                            </c:forEach>
                        </table>
                    </c:if>
                    <br>
                    <br>

                    <%
                    int index=0;
                     ArrayList problemList = (ArrayList) request.getAttribute("problemList"); 
                     System.out.println("problemList size in jsp"+problemList.size());
                    %>
                    <c:if test = "${problemList != null}" >  
                        <table align="center" class="table table-info mb30 table-hover">
                            <tr><thead>
                            <th width="41" height="30" >SNo</th>
                            <th width="90" height="30" >Section</th>
                            <th width="200" height="30" >Compliants</th>
                            <th width="92" height="30" >Symptoms</th>
                            <th width="58" height="30" >Severity</th>
                            <th width="92" height="30" >Agent</th>
                            <th width="30" height="30" >Select</th></thead>
                            </tr>

                            <c:forEach items="${problemList}" var="prob"> 
                                <%  
                                String  classText = "";
                                int oddEven = index % 2;
                                if (oddEven > 0) {
                                classText = "text1";
                                } else {
                                classText = "text2";
                                }


                                %>
                                <tr>

                                    <td height="30"> <%=index+1%> 
                                        <input name="probId" type="hidden" value='<c:out value="${prob.probId}"/>' /> 
                                    </td>
                                    <td height="30" > <c:out value="${prob.secName}"/></td>  
                                <td height="30" > <c:out value="${prob.probName}"/></td>    
                                <td height="30" > <c:out value="${prob.symptoms}"/></td>
                                <c:if test = "${prob.severity ==1}" >      
                                    <td height="30" >  Low</td>
                                </c:if>      
                                <c:if test = "${prob.severity ==2}" >
                                    <td height="30" >  Medium</td>
                                </c:if>      
                                <c:if test = "${prob.severity ==3}" >
                                    <td height="30" >  High</td>
                                </c:if>  
                                <td class="form-control" height="30"> <select name="vendorId" onchange="setSelectbox(<%=index%>)" 
                                                                              <option selected value="0">--select--</option>
                                        <c:if test = "${vendorList != null}" >            
                                            <c:forEach items="${vendorList}" var="mfr">  
                                                <c:if test = "${mfr.vendorTypeId == 1017}" >            
                                                    <option  value='<c:out value="${mfr.vendorId}" />'><c:out value="${mfr.vendorName}" />                
                                                </c:if> 
                                            </c:forEach >
                                        </c:if> 
                                    </select>   </td>
                                <td  height="30" ><input type="checkbox" name="selectedindex" value='<%= index %>'></td>
                                </tr>
                                <%
                                  index++;
                                %>
                            </c:forEach>

                        </table>
                    </c:if> 

                    <br>
                    <center><b>Remarks </b> &nbsp;&nbsp;<textarea name="remarks" rows="5" cols="30"></textarea></center>
                    <br>

                    <center>
                        <input type="button" class="btn btn-success" value="Generate W.O" onclick="submitPage();">
                    </center>
                    <input name="jobcardId" type="hidden" value='<%=request.getAttribute("jobcardId")%>'>  
                    <input name="vehicleId" type="hidden" value='<%=request.getAttribute("vehicleId")%>'>  
                    <input name="workOrderId" type="hidden" value='<%=request.getAttribute("workOrderId")%>'>  
                    <input name="km" type="hidden" value='<%=request.getAttribute("km")%>'>  
                    <input name="reqDate" type="hidden" value='<%=request.getAttribute("reqDate")%>'>  

                    </body>
                    </div>
                    </div>
                    </div>

                    <%@ include file="/content/common/NewDesign/settings.jsp" %>
