<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="ets.domain.report.business.VwpTO"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<!--        <script type="text/javascript" src="js/jquery-1.4.2.min.js"></script>-->
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->

<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>


<script>
     function viewDayWiseDetails(weekId) {
//         alert("Inside");
        window.open('/throttle/viewDayWiseDetails.do?weekId=' + weekId , 'PopupPage', 'height = 800, width = 1400, scrollbars = yes, resizable = yes');
    }
    function viewCustomerProfitDetails(vehicleTypeId,movementType,dateRange,companyId) {
//            alert(tripIds);
        window.open('/throttle/viewCustomerWiseProfitDetails.do?vehicleTypeId=' + vehicleTypeId +"&movementType=" + movementType +"&dateRange="+ dateRange +"&companyId="+ companyId + "&param=Search", 'PopupPage', 'height = 500, width = 1150, scrollbars = yes, resizable = yes');
    }
</script>

<script>
     function submitPage(value) {
//        alert("hiiii");
        if (document.getElementById('fromDate').value == '') {
            alert("please select From Date");
            document.getElementById('fromDate').focus();
        }
        if (document.getElementById('toDate').value == '') {
            alert("please select To Date");
            document.getElementById('toDate').focus();
        }else if(value == "ExportExcel"){
                document.accountReceivable.action = '/throttle/handleVehicleWeeklyProductivityReport.do?param=ExportExcel';
                document.accountReceivable.submit();
//            }else{
            }else{
               document.accountReceivable.action = '/throttle/handleVehicleWeeklyProductivityReport.do?param=Search';
               document.accountReceivable.submit(); 
                
            }
        }
        
    function changePageLanguage(langSelection) {
        if (langSelection == 'ar') {
            document.getElementById("pAlign").style.direction = "rtl";
        } else if (langSelection == 'en') {
            document.getElementById("pAlign").style.direction = "ltr";
        }
    }
</script>

<body >

    <div class="pageheader">
        <h2><i class="fa fa-edit"></i>Vehicle Weekly Productivity Report</h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li><a href="general-forms.html">Report</a></li>
                <li class="active">Vehicle Weekly Productivity Report</li>
            </ol>
        </div>
    </div>
    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <body onload="sorter.size(20);">
                    <form name="accountReceivable"   method="post">

                        <br>
                        <br>
                        <br>
                        <table class="table table-info mb30 table-hover" >
                                        <tr height="30">
                                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.Fleet Center" text="Fleet Center"/></td>
                                            <td height="30">
                                            <select style="width:260px;height:40px;" name="fleetCenterId" id="fleetCenterId" onchange="setStatusDiv();" >
                                                <!--<option value="0" selected>--Select--</option>-->
                                                <c:if test="${companyList != null}">                                        
                                                    <c:forEach items="${companyList}" var="cmpList">
                                                        <c:if test="${cmpList.cmpId == companyId}">
                                                          <option value="<c:out value="${cmpList.cmpId}"/>"><c:out value="${cmpList.name}"/></option>
                                                        </c:if>
                                                    </c:forEach>
                                                </c:if>
                                                <script>
                                                    document.getElementById("fleetCenterId").value = '<c:out value="${companyId}"/>';
                                                </script>
                                            </select >     
                                            </td>
                                            <td align="center"><font color="red">*</font><spring:message code="operations.reports.label.FromDate" text="default text"/></td>
                                            <td height="30"><input name="fromDate" id="fromDate" type="text" style="width:260px;height:40px;" class="datepicker" value="<c:out value="${fromDate}"/>" ></td>
                                            <td><font color="red">*</font><spring:message code="operations.reports.label.ToDate" text="default text"/></td>
                                            <td height="30"><input name="toDate" id="toDate" style="width:260px;height:40px;" type="text" class="datepicker" value="<c:out value="${toDate}"/>" > </td>
                                                
                                        </tr>
                                        <tr height="30" >
                                            <td colspan="6" align="center">
                                                <input type="button" class="btn btn-success" name="Search"  id="Search"  value="<spring:message code="operations.reports.label.SEARCH" text="default text"/>" onclick="submitPage(this.name);"> &nbsp;&nbsp;
                                                <input type="button" class="btn btn-success" name="ExportExcel"   value="<spring:message code="operations.reports.label.EXPORTEXCEL" text="default text"/>" onclick="submitPage(this.name);"></td>
                                        </tr>
                                    </table>


                        <br>
                        <c:if test="${resultList != null}">
                            <table class="table table-info mb30 table-hover" id="table" class="sortable" border="1" >
                                <thead>
                                    <tr>
                                        <th rowspan="2" align="center">Week</th>
                                        <th rowspan="2" align="center">WeekDateRange</th>
                                        <th rowspan="2" align="center">NoOfDays</th>
                                        <th colspan="2" align="center">40'TripleAxle</th>
                                        <th colspan="2" align="center">40'DoubleAxle</th>
                                        <!--<th colspan="2" align="center">20'DoubleAxle</th>-->
                                        <th rowspan="2" align="center">TotalMofussil</th>
                                        <th rowspan="2" align="center">TotalLocal</th>
                                        <th rowspan="2" align="center">Total Movements</th>
                                        <th rowspan="2" align="center">TotalMoff Movements</th>
                                        <th rowspan="2" align="center">Ave.Per.Vehicle</th>
                                        <th rowspan="2" align="center">Ave.Pro.Per Vehicle/Month</th>

                                    </tr> 
                                    <tr>
                                        <th align="center">Mof</th>
                                        <th align="center">Loc</th>
                                        <th align="center">Mof</th>
                                        <th align="center">Loc</th>
<!--                                        <th align="center">Mof</th>
                                        <th align="center">Loc</th>-->

                                    </tr> 
                                </thead>
                                <tbody>
                                    <% int index = 1;%>
                                    <c:set var="days" value="0"/>
                                    <c:set var="mof403" value="0"/>
                                    <c:set var="loc403" value="0"/>
                                    <c:set var="mof402" value="0"/>
                                    <c:set var="loc402" value="0"/>
                                    <c:set var="mof202" value="0"/>
                                    <c:set var="loc202" value="0"/>
                                    <c:set var="totMof" value="0"/>
                                    <c:set var="totLoc" value="0"/>
                                    <c:set var="nettTotal" value="0"/>
                                    <c:set var="nettMof" value="0"/>
                                    <c:set var="nettAve1" value="0"/>
                                    <c:set var="nettAve2" value="0"/>

                                    <c:forEach items="${resultList}" var="utilList">
                                        <c:set var="days" value="${days + utilList.noOfDays}"/>
                                        <c:set var="mof403" value="${mof403 + utilList.fortyTripleAxleMof}"/>
                                        <c:set var="loc403" value="${loc403 + utilList.fortyTripleAxleLoc}"/>
                                        <c:set var="mof402" value="${mof402 + utilList.fortyDoubleAxleMof}"/>
                                        <c:set var="loc402" value="${loc402 + utilList.fortyDoubleAxleLoc}"/>
                                        <c:set var="mof202" value="${mof202 + utilList.twentyDoubleAxleMof}"/>
                                        <c:set var="loc202" value="${loc202 + utilList.twentyDoubleAxleLoc}"/>

                                        <c:set var="totMof" value="${totMof + utilList.totalMof}"/>
                                        <c:set var="totLoc" value="${totLoc + utilList.totalLoc}"/>
                                        <c:set var="nettTotal" value="${nettTotal + utilList.nettTotal}"/>
                                        <c:set var="nettMof" value="${nettMof + utilList.nettMof}"/>
                                        <c:set var="nettAve1" value="${nettAve1 + utilList.avePerVehicleWeek}"/>
                                        <c:set var="nettAve2" value="${nettAve2 + utilList.avePerVehicleMonth}"/>

                                        <tr>
                                            <td  align="center"    >
                                                <a  href="#" onclick="viewDayWiseDetails('<c:out value="${utilList.weekId}"/>');">
                                                    <c:out value="${utilList.weekName}"/>
                                                </a>
                                                
                                            </td>
                                            <td  align="center"    >
                                                 <a  href="#" onclick="viewDayWiseDetails('<c:out value="${utilList.weekId}"/>');">
                                                    <c:out value="${utilList.dateRange}"/>
                                                </a>
                                            </td>
                                            <td  align="center"    ><c:out value="${utilList.noOfDays}"/></td>

                                            <td  align="center"    >
                                                <a href="#" onclick="viewCustomerProfitDetails(1060,1,' <c:out value="${utilList.dateRange}"/>','<c:out value="${companyId}"/>');"><font color="#050404">
                                                 <c:out value="${utilList.fortyTripleAxleMof}"/>
                                                </font></a>
                                            </td>
                                            <td  align="center"    >
                                                <a href="#" onclick="viewCustomerProfitDetails(1060,0,' <c:out value="${utilList.dateRange}"/>','<c:out value="${companyId}"/>');"><font color="#050404">
                                                <c:out value="${utilList.fortyTripleAxleLoc}"/>
                                                </font></a>
                                            </td>
                                            <td  align="center"    >
                                                <a href="#" onclick="viewCustomerProfitDetails(1059,1,' <c:out value="${utilList.dateRange}"/>','<c:out value="${companyId}"/>');"><font color="#050404">
                                                <c:out value="${utilList.fortyDoubleAxleMof}"/>
                                                </font></a>
                                            </td>
                                            <td  align="center"    >
                                                <a href="#" onclick="viewCustomerProfitDetails(1059,0,' <c:out value="${utilList.dateRange}"/>','<c:out value="${companyId}"/>');"><font color="#050404">
                                                <c:out value="${utilList.fortyDoubleAxleLoc}"/>
                                                </font></a>
                                            </td>
<%--                                            <td  align="center"    >
                                                <a href="#" onclick="viewCustomerProfitDetails(1058,1,' <c:out value="${utilList.dateRange}"/>','<c:out value="${companyId}"/>');"><font color="#050404">
                                                <c:out value="${utilList.twentyDoubleAxleMof}"/>
                                                </font></a>
                                            </td>
                                            <td  align="center"    >
                                                <a href="#" onclick="viewCustomerProfitDetails(1058,0,' <c:out value="${utilList.dateRange}"/>','<c:out value="${companyId}"/>');"><font color="#050404">
                                                <c:out value="${utilList.twentyDoubleAxleLoc}"/>
                                                </font></a>
                                            </td>--%>

                                            <td  align="center"    ><c:out value="${utilList.totalMof}"/></td>
                                            <td  align="center"    ><c:out value="${utilList.totalLoc}"/></td>
                                            <td  align="center"    ><c:out value="${utilList.nettTotal}"/></td>
                                            <td  align="center"    ><c:out value="${utilList.nettMof}"/></td>
                                            <td  align="center"  ><c:out value="${utilList.avePerVehicleWeek}"/></td>
                                            <td  align="center"  ><c:out value="${utilList.avePerVehicleMonth}"/></td>
                                        </tr>

                                    </c:forEach>
                                    <tr>
                                        <td  align="center"    >&nbsp;</td>
                                        <td  align="center"    ><font color="blue"><b>Total</b></font></td>
                                        <td  align="center"    ><c:out value="${days}"/></td>

                                        <td  align="center"    ><c:out value="${mof403}"/></td>
                                        <td  align="center"    ><c:out value="${loc403}"/></td>
                                        <td  align="center"    ><c:out value="${mof402}"/></td>
                                        <td  align="center"    ><c:out value="${loc402}"/></td>
<%--                                        <td  align="center"    ><c:out value="${mof202}"/></td>
                                        <td  align="center"    ><c:out value="${loc202}"/></td>--%>
                                        <td  align="center"    ><c:out value="${totMof}"/></td>
                                        <td  align="center"    ><c:out value="${totLoc}"/></td>
                                        <td  align="center"    ><c:out value="${totMof+totLoc}"/></td>
                                        <td  align="center"    ><c:out value="${nettMof}"/></td>
                                        <td  align="center"    >
                                            <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${nettAve1}" />
                                            <%--<c:out value="${nettAve1}"/>--%>
                                        </td> 
                                        <td  align="center"    >
                                            <fmt:formatNumber type="number" minFractionDigits="2" maxFractionDigits="2" value="${nettAve2}" />
                                            <%--<c:out value="${nettAve2}"/>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </c:if>
                        <br/>
                        <br/>
                        <br/>
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" >5</option>
                                    <option value="10">10</option>
                                    <option value="20" selected="selected">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span><spring:message code="operations.reports.label.EntriesPerPage" text="default text"/></span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text"><spring:message code="operations.reports.label.DisplayingPage" text="default text"/> <span id="currentpage"></span> <spring:message code="operations.reports.label.of" text="default text"/> <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>



                        <br>
                        <br>                        
                        <br>

                        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %>
                        
                        <div id="barchart" style="min-width: 1200px; height: 400px; margin: 0 auto"></div>


                        <%
                            ArrayList dataList = (ArrayList)request.getAttribute("resultList");
                            Iterator itr1 = dataList.iterator();
                            VwpTO repTO1 = new VwpTO();
                            String[] xAxisDataArray = new String[dataList.size()];
                            String[] yAxisDataArray = new String[dataList.size()];
                            int i=0;
                            while (itr1.hasNext()) {
                                repTO1 = (VwpTO) itr1.next();
                                xAxisDataArray[i]=repTO1.getWeekName();
                                yAxisDataArray[i]=repTO1.getAvePerVehicleWeek();                            
                                i++;                            
                            }
                        
                            for(int j=0; j<xAxisDataArray.length;j++){
                                System.out.println("j="+j+"x value is="+xAxisDataArray[j]);    
                                System.out.println("j="+j+"y value is="+yAxisDataArray[j]);    
                            }
                        %>

                        
<script>
//        alert("xData");
        //var xData = <%=xAxisDataArray%>;
        //var yData = <%=yAxisDataArray%>;
        var xData = [2, 3, 4];
        var yData = [5, 6, 7];
//        $('#barchart').highcharts({
//            chart: {
//                type: 'column'
//            },
//            title: {
//                text: 'Week'
//            },
//            subtitle: {
//                text: ''
//            },
//            xAxis: {
//                type: 'category',
//                categories: [5, 6, 7],
//                labels: {
//                    rotation: -45,
//                    style: {
//                        fontSize: '13px',
//                        fontFamily: 'Verdana, sans-serif'
//                    }
//                }
//            },
//            yAxis: {
//                min: 0,
//                title: {
//                    text: 'Ave Trip Per Vehicle'
//                }
//            },
//            legend: {
//                enabled: false
//            },
//            tooltip: {
//                pointFormat: 'AveTripPerVehicle : <b>{point.y:f} </b>'
//            },
//            series: [{
//                    name: 'AveTripPerVehicle',
//                    data: [6, 8, 9],
//                    dataLabels: {
//                        enabled: true,
//                        rotation: -90,
//                        color: '#FFFFFF',
//                        align: 'right',
//                        format: '{point.y:f}', // one decimal
//                        y: 10, // 10 pixels down from the top
//                        style: {
//                            fontSize: '13px',
//                            fontFamily: 'Verdana, sans-serif'
//                        }
//                    }
//                }]
//        });

        $('#barchart').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Truck Make'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                categories: xData,
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'No Of Vehicles'
                }
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: 'Truck : <b>{point.y:f} Nos</b>'
            },
            series: [{
                    name: 'Truck Make',
                    data: yData,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#FFFFFF',
                        align: 'right',
                        format: '{point.y:f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                            fontSize: '13px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }]
        });
    </script>          
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>    
<script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>                        
                    </form>
                </body>    
            </div>
        </div>
    </div>
    <%@ include file="/content/common/NewDesign/settings.jsp" %>    