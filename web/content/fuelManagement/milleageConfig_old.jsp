

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
        <link href="/throttle/css/parveen.css" rel="stylesheet"/>
        <link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
        <script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
        <script language="javascript" src="/throttle/js/validate.js"></script>
        <script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
        <link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />
        <script type="text/javascript" src="/throttle/js/suggestions.js"></script>
        <script language="javascript" src="/throttle/js/ajaxFunction.js"></script> 
        <%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
  
    <script language="javascript">

        function show_src() {
            document.getElementById('exp_table').style.display = 'none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display = 'block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display = 'none';
        }


        var httpReq;
        var temp = "";
        function ajaxData()
        {

            var url = "/throttle/getModels1.do?mfrId=" + document.mileage.mfrId.value;
            if (window.ActiveXObject)
            {
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest)
            {
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() {
                processAjax();
            };
            httpReq.send(null);
        }

        function processAjax()
        {
            if (httpReq.readyState == 4)
            {
                if (httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    setOptions(temp, document.mileage.modelId);
                    setTimeout("fun()", 1);
                }
                else
                {
                    alert("Error loading page\n" + httpReq.status + ":" + httpReq.statusText);
                }
            }
        }
        function submitGo() {
            document.mileage.action = '/throttle/viewVehicleMilleage.do';
            document.mileage.submit();

        }
        function deleteMilleage(indx) {
            var mfrId = document.getElementsByName("mfrIds");
            var modelId = document.getElementsByName("modelIds");
            var fromYear = document.getElementsByName("fromYears");
            var toYear = document.getElementsByName("toYears");

            document.mileage.action = '/throttle/deleteVehicleMilleage.do?mfrId=' + mfrId[indx].value + '&modelId=' + modelId[indx].value + '&fromYear=' + fromYear[indx].value + '&toYear=' + toYear[indx].value;
            document.mileage.submit();

        }

        function fun() {
            document.mileage.modelId.value = '<%=request.getAttribute("modelId")%>';
        }



        function setValues() {
            if ('<%=request.getAttribute("vehicleTypeId")%>' != 'null') {
                document.mileage.vehicleTypeId.value = '<%=request.getAttribute("vehicleTypeId")%>';
            }
            ajaxData();
        }

        function setSelectbox(i,obj) {
            var selected = document.getElementsByName("selectedIndex");
            if(obj.value != ''){
            selected[i].checked = 1;
            }    
        }
        
        function submitPage() {
            var checValidate = selectedItemValidation();
//                document.mileage.action = '/throttle/saveVehicleMilleage.do';
//                document.mileage.submit();
        }
        function selectedItemValidation(){
            var index = document.getElementsByName("selectedIndex");
            var chec=0;
            for(var i=0;(i<index.length && index.length!=0);i++){
                if(index[i].checked){
                chec++;
                }
            }
            if(chec == 0){
            alert("Please Select Any One And Then Proceed");
            }else{
            document.mileage.action = '/throttle/saveVehicleMilleage.do';
            document.mileage.submit();
            }
        }
    </script>
      <div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">Location Master</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    
    <body onload="setValues();">
        <form name="mileage" method="post">

            
                                <table width="850" cellpadding="0" cellspacing="1" border="0" align="center" class="table table-info mb30 table-hover">
                                    <tr align="center">
                                        <td height="30">Vehicle Type</td>
                                        <td height="30"><select class="form-control" name="vehicleTypeId" id="vehicleTypeId">
                                                <option value="0">---Select---</option>
                                                <c:if test = "${vehicleTypeList != null}" >
                                                    <c:forEach items="${vehicleTypeList}" var="vtList">
                                                        <option value='<c:out value="${vtList.typeId}" />'><c:out value="${vtList.typeName}" /></option>
                                                    </c:forEach >
                                                </c:if>
                                            </select></td>
                                        
                                        <td><input type="button" class="btn btn-info" onclick="submitGo();" value="Go"></td>
                                    </tr>
                                </table>
                           


            <br>   

            <c:if test = "${milleageList!= null}">

                <table align="center" width="950" class="table table-info mb30 table-hover" border="0" cellspacing="0" cellpadding="0">
                    <thead>
                    <tr >

                        <th>S no</th>                              
                        <th>Vehicle Type</th>                              
                        <th>From Year</th>                              
                        <th>To Year</th>                              
                        <th>Road Type</th>                              
                        <th>Load Type</th>                              
                        <th>Vehicle Mileage</th>
                        <th>Reefer Mileage</th>
                        <th>Select</th>

                    </tr>
                    </thead>
                    <% int index = 0;
                    int sno = 1;%>
                    <c:forEach items="${milleageList}" var="fuel"> 
                        <%
                            String classText = "";
                            int oddEven = index % 2;
                            if (oddEven > 0) {
                                classText = "form-control";
                            } else {
                                classText = "form-control";
                            }
                            
                        %>	
                        <tr height="30">

                            <td ><%=sno%></td>                                                         
                            <td ><input type="hidden" name="vehicleTypeIds"  value='<c:out value="${fuel.vehicleTypeId}"/>'><c:out value="${fuel.vehicleTypeName}" /></td>                                                         
                            <td ><input type="text" name="fromAge" style="width: 80px; height: 30px" value="<c:out value="${fuel.fromYear}" />" onchange="setSelectbox('<%= index %>',this);"/></td>
                            <td ><input type="text" name="toAge" style="width: 80px; height: 30px" value="<c:out value="${fuel.toYear}" />" onchange="setSelectbox('<%= index %>',this);"/></td>
                            <td ><select name="roadType" id="roadType" style="width: 100px; height: 30px" onchange="setSelectbox('<%= index %>',this);"/>
                                <c:if test = "${fuel.roadType == 0}">
                                    <option value="1" >High Way</option>
                                    <option value="2">Hilly Road</option>
                                </c:if>
                                <c:if test = "${fuel.roadType == 1}">
                                    <option value="1" selected>High Way</option>
                                    <option value="2">Hilly Road</option>
                                </c:if>
                                <c:if test = "${fuel.roadType == 2}">
                                    <option value="1" >High Way</option>
                                    <option value="2" selected>Hilly Road</option>
                                </c:if>
                            </select>
                            
                            </td>
                            <td ><select name="loadType" id="loadType"  style="width: 100px; height: 30px"  onchange="setSelectbox('<%= index %>',this);"/>
                                <c:if test = "${fuel.loadTypeId == 0 || fuel.loadTypeId == null}">
                                    <option value="1" >Empty</option>
                                    <option value="2">Loaded</option>
                                </c:if>
                                <c:if test = "${fuel.loadTypeId == 1}">
                                    <option value="1" selected>Empty</option>
                                    <option value="2">Loaded</option>
                                </c:if>
                                <c:if test = "${fuel.loadTypeId == 2}">
                                    <option value="1" >Empty</option>
                                    <option value="2" selected>Loaded</option>
                                </c:if>
                            </select>
                            </td>
                            <td ><input type="text" name="vehicleMileage"  style="width: 80px; height: 30px"  value="<c:out value="${fuel.milleage}" />" onchange="setSelectbox('<%= index %>',this);"/></td>
                            <td ><input type="text" name="reeferMileage"  style="width: 80px; height: 30px"  value="<c:out value="${fuel.reeferMileagePerKm}" />" onchange="setSelectbox('<%= index %>',this);"/></td>
                            <td ><input type="checkbox" name="selectedIndex" value="<%=index%>"/></td>
                        </tr>
                        <%
                        index++;
                        sno++;
                        %>
                    </c:forEach>
                </table>
            </c:if>
            <br>
            <center><input type="button" class="btn btn-info" value="Update Mileage" name="update" onclick="submitPage();" >
            </center><br>
            <br>
            <br>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
     </div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>