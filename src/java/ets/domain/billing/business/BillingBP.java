/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.billing.business;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.domain.billing.data.BillingDAO;
import java.io.IOException;
import java.util.ArrayList;
import com.ibatis.sqlmap.client.SqlMapClient;
import java.util.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author vinoth
 */
public class BillingBP {

    private BillingDAO billingDAO;

    public BillingDAO getBillingDAO() {
        return billingDAO;
    }

    public void setBillingDAO(BillingDAO billingDAO) {
        this.billingDAO = billingDAO;
    }

    public ArrayList getOrdersForBilling(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList clisedTrips = null;
        clisedTrips = billingDAO.getOrdersForBilling(billingTO);
        return clisedTrips;
    }

    public ArrayList getRepoOrdersForBilling(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList clisedTrips = null;
        clisedTrips = billingDAO.getRepoOrdersForBilling(billingTO);
        return clisedTrips;
    }

    public ArrayList getOrdersToBeBillingDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = billingDAO.getOrdersToBeBillingDetails(billingTO);
        return tripDetails;
    }

    public ArrayList getOrdersOtherExpenseDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = billingDAO.getOrderOtherExpenseDetails(billingTO);
        return tripDetails;
    }

    public ArrayList getTripDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList tripDetails = new ArrayList();
        tripDetails = billingDAO.getTripDetails(billingTO);
        return tripDetails;
    }

    public int saveOrderBill(BillingTO billingTO, int userId) throws FPRuntimeException, FPBusinessException, IOException, Exception {
        int status = 0;
        int invoiceId = 0;
        SqlMapClient session = billingDAO.getSqlMapClient();
        boolean exceptionStatus = false;
        Exception excp = null;
        try {
            session.startTransaction();
            
            ArrayList ordersToBeBilledDetails = billingDAO.getOrdersToBeBillingDetails1(billingTO, session);
            ArrayList ordersOtherExpenseDetails = new ArrayList();

            String grDate = "";
            Iterator itr7 = ordersToBeBilledDetails.iterator();
            BillingTO billingTONew12 = null;
            while (itr7.hasNext()) {
                billingTONew12 = new BillingTO();
                billingTONew12 = (BillingTO) itr7.next();
                //grDate = billingTONew12.getToDate();
                grDate = billingTONew12.getGrDate();
            }
            System.out.println(" grdate"+grDate);
              String compareDate = "31-03-";
              String june="30-06-2017";
            String tempArr[] = grDate.split("-");
            compareDate = compareDate + tempArr[2];
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            Date d1 = sdf.parse(compareDate);
            Date d2 = sdf.parse(grDate);
            Date june_17=sdf.parse(june);
            System.out.println("compareDate " + compareDate);
            System.out.println("grDate " + grDate);
            System.out.println("june_17 " + june_17);
            System.out.println("d2... " + d2);
            String finacialYear = "";
            String invoiceCode = "";
            String currentYear = "";
            String fromDate = "";
            String invoiceCodeSequence = "";
             System.out.println("tempArr[2]"+tempArr[2]);
            if (d2.after(d1)) { // After marc 31st 2017
                finacialYear = String.valueOf(Integer.parseInt(tempArr[2]) + 1);
                System.out.println("finacialYear "+finacialYear);
                finacialYear = (finacialYear.charAt(finacialYear.length()-2) + "") + (finacialYear.charAt(finacialYear.length()-1)+"");
                System.out.println("finacialYear:" + finacialYear);
                currentYear = (tempArr[2].charAt(tempArr[2].length()-2)+"") + (tempArr[2].charAt(tempArr[2].length()-1) + "");
           //1 
                if(d2.after(june_17)){
                    System.out.println("if111");
                invoiceCode = currentYear + finacialYear + "TPG";
                }else{
                invoiceCode = "ICTR/"+currentYear + "-" + finacialYear + "/TPT/";
                }
                Calendar now = Calendar.getInstance();
                System.out.println("Current Year is : " + now.get(Calendar.YEAR));
                String curYear = now.get(Calendar.YEAR)+"";
                System.out.println("curYear "+curYear);
               
                if(curYear.equals(tempArr[2])){
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
                int gstCheck = 0;
                 if(d2.after(june_17)){
                     gstCheck = 1;
                     fromDate = dateFormat.format(date);
                     System.out.println("fromDate/InvoiceDate after july 17 "+fromDate);
                 }else{
                     gstCheck = 2;
                    fromDate= "2017-06-30 14:00:00";
                     System.out.println("fromDate/InvoiceDate before july 17 "+fromDate);
                 }
                invoiceCodeSequence = billingDAO.getInvoiceCodeSequence1718(session,gstCheck);
                
//                New Invoice sequence
                }else{
                   fromDate= curYear + "-03-31 22:00:00"; 
//                   Old Invoice sequence
                   invoiceCodeSequence = billingDAO.getInvoiceCodeSequence(session);
                }
            } else { //Befoe
                // System.out.println("checkdate is after Date");
                finacialYear = String.valueOf(Integer.parseInt(tempArr[2]) - 1);
                System.out.println("finacialYear "+finacialYear);
                finacialYear = (finacialYear.charAt(finacialYear.length()-2) + "") + (finacialYear.charAt(finacialYear.length()-1)+"");
                System.out.println("finacialYear:" + finacialYear);
                 currentYear = (tempArr[2].charAt(tempArr[2].length()-2)+"") + (tempArr[2].charAt(tempArr[2].length()-1) + "");
       // 2        if()
                if(d2.after(june_17)){
                    System.out.println("if2222");
                invoiceCode = currentYear + finacialYear + "TPG";
                }else{
                invoiceCode = "ICTR/"+currentYear + "-" + finacialYear + "/TPT/";
                }
                 
                fromDate = tempArr[2] + "-03-31 22:00:00";
               
                System.out.println("tempArr[2]"+tempArr[2]);
//                  Old Invoice sequence
                invoiceCodeSequence = billingDAO.getInvoiceCodeSequence(session);      
            }
             billingTO.setFromDate(fromDate);
            System.out.println(" invoiceCode" + invoiceCode);

            invoiceCode = invoiceCode + invoiceCodeSequence;
            billingTO.setInvoiceCode(invoiceCode);
            billingTO.setInvoiceNo(billingTO.getInvoiceNo());
            String totalRevenue = billingDAO.getBillingTotalRevenue(billingTO,session);
            String totalExpense =  billingDAO.getBillingTotalExpense(billingTO,session);
            System.out.println("billingTO.getTotalTax()"+billingTO.getTotalTax());
            String grandTotal = Float.parseFloat(totalRevenue) + Float.parseFloat(totalExpense) + Float.parseFloat(billingTO.getTotalTax()) +"";
            billingTO.setTotalRevenue(totalRevenue);
            billingTO.setOtherExpense(totalExpense);
            billingTO.setGrandTotal(grandTotal);
            
            //savebillheader
            invoiceId = billingDAO.saveBillHeader(billingTO, session);
            billingTO.setInvoiceId(""+invoiceId);
            int invoiceDetailId = 0;
            if (invoiceId > 0) {
             int tax=billingDAO.saveBillTax(billingTO, session);
                Iterator itr = ordersToBeBilledDetails.iterator();
                BillingTO billingTONew = null;
                while (itr.hasNext()) {
                    billingTONew = new BillingTO();
                    billingTONew = (BillingTO) itr.next();
                    billingTONew.setInvoiceId("" + invoiceId);
                    billingTONew.setInvoiceCode(invoiceCode);
                    billingTONew.setTotalRevenue(billingTONew.getEstimatedRevenue());
                    // billingTONew.setOtherExpense(otherExpense);
                    billingTONew.setGrandTotal(grandTotal);
                    billingTONew.setIgstAmount(billingTO.getIgstAmount());
                    invoiceDetailId = billingDAO.saveBillDetails(billingTONew, session);
                   
                    ordersOtherExpenseDetails = billingDAO.getOrderOtherExpenseDetails1(billingTONew, session);
                    Iterator itr1 = ordersOtherExpenseDetails.iterator();
                    BillingTO billingTONew1 = null;
                    while (itr1.hasNext()) {
                        billingTONew1 = new BillingTO();
                        billingTONew1 = (BillingTO) itr1.next();
                        billingTONew1.setTripId(billingTONew.getTripId());
                        billingTONew1.setInvoiceId("" + invoiceId);
                        billingTONew1.setInvoiceDetailId("" + invoiceDetailId);
                        billingTONew1.setInvoiceCode(invoiceCode);
                        status = billingDAO.saveBillDetailExpense(billingTONew1, session);
                    }

                    billingTONew.setTripSheetId(billingTONew.getTripId());
                    billingTONew.setStatusId("16");
                    status = billingDAO.updateStatus(billingTONew, userId, session);
                    //update trip status to billed**********************************************************************
                }
                int insertTally = billingDAO.updateTripInvoiceTally(billingTO, session);
                System.out.println("insertTally="+insertTally);
            }
            session.commitTransaction();
        } catch (Exception e) {
            exceptionStatus = true;
            excp = e;
            e.printStackTrace();
        } finally {
            //System.out.println("am here 7");
            try {
                session.endTransaction();
            } catch (Exception ex) {
                ex.printStackTrace();
                //System.out.println("am here 8" + ex.getMessage());
            } finally {
                session.getSession().close();
            }
            if (exceptionStatus) {
                throw excp;
            }
        }
        return invoiceId;
    }

//    public int saveBillHeader(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = billingDAO.saveBillHeader(billingTO);
//        return status;
//    }
//    
//     public int saveBillDetails(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = billingDAO.saveBillDetails(billingTO);
//        return status;
//    }
//    public int saveBillDetailExpense(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = billingDAO.saveBillDetailExpense(billingTO);
//        return status;
//    }
//    
//    public int updateStatus(BillingTO billingTO, int userId) throws FPRuntimeException, FPBusinessException {
//        int status = 0;
//        status = billingDAO.updateStatus(billingTO, userId);
//        return status;
//    }
    public ArrayList viewBillForSubmission(String customerId, String fromDate, String toDate, String tripType, String submitStatus, String billNo, String grNo) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.viewBillForSubmission(customerId, fromDate, toDate, tripType, submitStatus, billNo, grNo);
        return closedBill;
    }

    public int submitBill(String invoiceId, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = billingDAO.submitBill(invoiceId, userId);
        return status;
    }

    public int updateCancel(String billingNo, int userId) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = billingDAO.updateCancel(billingNo, userId);
        return status;
    }

    public ArrayList getPreviewHeader(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.getPreviewHeader(billingTO);
        return closedBill;
    }

    public ArrayList getBillingDetailsForPreview(BillingTO billingTO) throws FPRuntimeException, FPBusinessException {
        ArrayList closedBill = null;
        closedBill = billingDAO.getBillingDetailsForPreview(billingTO);
        return closedBill;
    }

     public ArrayList getGSTTaxDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getGSTTaxDetails(billingTO);
        return gstDetails;
    }
     public ArrayList getInvoiceGSTTaxDetails(BillingTO billingTO) throws FPBusinessException, FPRuntimeException {
        ArrayList gstDetails = new ArrayList();
        gstDetails = billingDAO.getInvoiceGSTTaxDetails(billingTO);
        return gstDetails;
    }
     
      public int updateTripGst(String invoiceId,String[] tripIds,String[] tripIdIGSTAmount,String[] tripIdCGSTAmount,String[] tripIdSGSTAmount) throws FPRuntimeException, FPBusinessException {
        int status = 0;
        status = billingDAO.updateTripGst(invoiceId, tripIds,tripIdIGSTAmount,tripIdCGSTAmount,tripIdSGSTAmount);
        return status;
    }

}
