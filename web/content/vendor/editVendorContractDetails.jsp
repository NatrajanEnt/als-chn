

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function() {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript" language="javascript">
    function setContainerTypeListDistance(val, sno) {
        if (val == '1058') {
            // 20FT vehicle
            $('#containerTypeIdDistance' + sno).empty();
            $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(1).html('20'))
            $('#containerQtyDistance' + sno).empty();
            $('#containerQtyDistance' + sno).append($('<option ></option>').val(1).html('1'))
        } else if (val == '1059') {
            // 40FT vehicle
            $('#containerTypeIdDistance' + sno).empty();
            $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(0).html('--Select--'))
            $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(1).html('20'))
            $('#containerTypeIdDistance' + sno).append($('<option ></option>').val(2).html('40'))
            $('#containerQtyDistance' + sno).empty();

        }
    }
    function setContainerQtyListDistance(val, sno) {
        if (val == '1') {
            // 20FT vehicle
            $('#containerQtyDistance' + sno).empty();
            $('#containerQtyDistance' + sno).append($('<option ></option>').val(2).html('2'))
        } else if (val == '2') {
            // 40FT vehicle
            $('#containerQtyDistance' + sno).empty();
            $('#containerQtyDistance' + sno).append($('<option ></option>').val(1).html('1'))

        }
    }
</script>
<script type="text/javascript" language="javascript">
    function getDistanceMarketHireRateHidden(sno, tableRow) {
        var vehicleTypeId = $("#vehicleTypeIdDistance" + sno).val();
        var loadTypeId = $("#loadTypeIdDistance" + sno).val();
        var containerTypeId = $("#containerTypeIdDistance" + sno).val();
        var containerQty = $("#containerQtyDistance" + sno).val();
        var fromDistance = $("#fromDistance" + sno).val();
        var toDistance = $("#toDistance" + sno).val();
        if (vehicleTypeId != '0' && containerTypeId != '0' && containerQty != '0'
                && fromDistance != '' && toDistance != '' && loadTypeId != '0') {
            $.ajax({
                url: "/throttle/getDistanceMarketHireRate.do",
                dataType: "json",
                data: {
                    vehicleTypeId: vehicleTypeId,
                    loadTypeId: loadTypeId,
                    containerTypeId: containerTypeId,
                    containerQty: containerQty,
                    fromDistance: fromDistance,
                    toDistance: toDistance
                },
                success: function(data, textStatus, jqXHR) {
                    if (data.MarketHireRate == '0') {
                        $("#distanceStatusSpan").text("Route not defined in organisation");
                        $("#distanceMarketRate" + sno).text(0);
//                                                $("#distanceMarketRateSpan"+sno).text(0);
                        $("#vehicleTypeIdDistance" + sno).val(0);
                        $("#containerTypeIdDistance" + sno).val(0);
                        $("#containerQtyDistance" + sno).val(0);
                        checkDistanceMarketRate(sno, data.MarketHireRate);
                    } else if (data.MarketHireRate != '') {
                        $("#distanceStatusSpan").text("");
                        $("#distanceMarketRate" + sno).text(data.MarketHireRate);
//                                                $("#distanceMarketRate"+sno).val(data.MarketHireRate);
                        checkDistanceMarketRate(sno, data.MarketHireRate);
                    }
                },
                error: function(data, type) {
                }
            });
        }

    }

    function checkDistanceMarketRate(sno, value) {
        var additionalCost = $("#rateWithoutReeferDistance" + sno).val();
        var marketRate = value;
        var sts = false;
        if (parseFloat(additionalCost) > parseFloat(marketRate)) {
            sts = true;
        }
        if (sts) {
            $("#distanceStatusSpan").text("Needs Approval");
            $("#distanceStatusSpan").removeClass("noErrorClass");
            $("#distanceStatusSpan").addClass("errorClass");
        } else {
            $("#distanceStatusSpan").text("Auto Approval");
            $("#distanceStatusSpan").removeClass("errorClass");
            $("#distanceStatusSpan").addClass("noErrorClass");
        }
    }


</script>
<script>
    function submitPage() {
        var spot = $("[name=spotCostE]");
        for (var i = 0; i < spot.length; i++) {
//            if (spot[i].value == '0' || spot[i].value == '0.00') {
//                alert("contract amount cannot be 0")
//                spot[i].focus();
//                return;
//            }
            if (spot[i].value == '') {
                alert("please enter the contract amount")
                spot[i].focus();
                return;
            }
        }
        var spot1 = $("[name=spotCost]");
        for (var i = 0; i < spot1.length; i++) {
//            if (spot1[i].value == '0' || spot1[i].value == '0.00') {
//                alert("contract amount cannot be 0")
//                spot1[i].focus();
//                return;
//            }
            if (spot1[i].value == '') {
                alert("please enter the contract amount")
                spot[i].focus();
                return;
            }
        }
        document.vehicleVendorContract.action = '/throttle/saveEditVehicleVendorContract.do';
        document.vehicleVendorContract.submit();
    }

</script>
<script>
    function setVehicleTypeDetails(sel, sno) {
        var vehicleTypeName = sel.options[sel.selectedIndex].text;
        if (vehicleTypeName == "20'") {
            $("#containerTypeId" + sno + " option[value='2']").remove();
            document.getElementById("containerTypeId" + sno).value = 1;
            $('#containerQty' + sno).val('1');
        } else {
            var exist = 0;
            $('#containerTypeId' + sno + ' option').each(function() {
                if (this.value == '2') {
                    exist = 1;
                }
            });
            if (exist == 1) {
            } else {
                $('#containerTypeId' + sno).append(new Option("40'", '2'));
            }
            document.getElementById("containerTypeId" + sno).value = 0;
            $('#containerQty' + sno).val('');
        }
    }
    function setContainerValue(cnt, containerType) {
        var containers = containerType;
        var vehicleType = $('#vehicleTypeId' + cnt).val();
        if (vehicleType == "1059~40'") {
            if (containers == "1") {
                $('#containerQty' + cnt).val('2');
            } else if (containers == "2") {
                $('#containerQty' + cnt).val('1');
            } else {
                $('#containerQty' + cnt).val('0');
            }
        }

    }


    var count = 1;
//      $(document).ready(function() {  //alert("hiiii");
//            // Get the table object to use for adding a row at the end of the table
//
//            var $itemsTable = $('#distanceTable');
//
//            // Create an Array to for the table row. ** Just to make things a bit easier to read.
//            var rowTemp = "";
//                rowTemp = [
//                '<tr class="item-row">',
//                '<td><a id="deleteRow"><img src="/throttle/images/icon-minus.png" alt="Remove Item" title="Remove Item"></a></td>',
//
//                    '<td><span id="distanceStatusSpan" style="font-size: 14px"></span><input type="hidden" name="distanceApprovalStatus" id="distanceApprovalStatus" value="1" /></td>',
//                '<td><input name="referenceName" value="" class="form-control" id="referenceName"  style="width: 150px;" /></td>',
//                '<td><c:if test="${vehicleTypeList != null}"><select name="vehicleTypeIdDistance" id="vehicleTypeIdDistance"  class="form-control" style="width:150px" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></td>',
//
//                '<td><select name="containerTypeIdDistance" id="containerTypeIdDistance"  class="form-control" style="width:150px" ></td>',
//                '<td><select name="containerQtyDistance" id="containerQtyDistance"  class="form-control" style="width:150px" ></td>',
//                '<td><select name="loadTypeIdDistance" id="loadTypeIdDistance"  class="form-control" style="width:150px" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></td>',
//                '<td><input name="fromDistance" value="" class="form-control" id="fromDistance"  style="width: 150px;"  /></td>',
//
//                '<td><input name="toDistance" value="" class="form-control" id="toDistance"  style="width: 150px;"  /></td>',
//
//                '<td><input name="rateWithReeferDistance" value="" class="form-control" id="rateWithReeferDistance" onchange="contractDistanceCheck();" style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
//                '<td><input name="rateWithoutReeferDistance" value="" class="form-control" id="rateWithoutReeferDistance" onchange="contractDistanceCheck();" style="width: 150px;" onKeyPress="return onKeyPressBlockCharacters(event);"/><input type="hidden" name="fuelVehicle" value="" class="form-control" id="fuelVehicle"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>',
//                  '<td><span id="distanceMarketRate" style="font-size: 12px; color:green"></span></td>',
//                '<td><span id="distanceMarketRateSpan" style="font-size: 12px;color:green"></span></td>',
//                '</tr>'
//            ].join('');
//
//            // Add row to list and allow user to use autocomplete to find items.
//
//
//
//            $("#addRowDistance").bind('click', function() {
//
//              //  count++;
////             alert("hiiii");
//
////              if( document.getElementById("distanceTable").style.visibility == "hidden"){
////                 document.getElementById("distanceTable").style.visibility = "visible";
////
////                } else{
//                var $row = $(rowTemp);
//
//                // save reference to inputs within row
//
//                var $referenceName = $row.find('#referenceName');
//                var $ptpVehicleTypeId = $row.find('#vehicleTypeIdDistance');
//
//                var $containerTypeId = $row.find('#containerTypeIdDistance');
//                var $containerQty = $row.find('#containerQtyDistance');
//                var $ptpPickupPoint = $row.find('#fromDistance');
//
//
//                var $ptpDropPoint = $row.find('#toDistance');
//
//                var $ptpRateWithReefer = $row.find('#ptpRateWithReefer');
//                var $ptpRateWithoutReefer = $row.find('#ptpRateWithoutReefer');
//                 $row.find('#vehicleTypeIdDistance').change(function(){
////                        var $itemrow = $(this).closest('tr');
//                        var val = $row.find('#vehicleTypeIdDistance').val();
//                        if(val == '1058'){
//                        // 20FT vehicle
//                        $row.find('#containerTypeIdDistance').empty();
//                        $row.find('#containerTypeIdDistance').append($('<option ></option>').val(1).html('20'))
//                        $row.find('#containerQtyDistance').empty();
//                        $row.find('#containerQtyDistance').append($('<option ></option>').val(1).html('1'))
//                        }else if(val == '1059'){
//                        // 40FT vehicle
//                        $row.find('#containerTypeIdDistance').empty();
//                        $row.find('#containerTypeIdDistance').append($('<option ></option>').val(0).html('--Select--'))
//                        $row.find('#containerTypeIdDistance').append($('<option ></option>').val(1).html('20'))
//                        $row.find('#containerTypeIdDistance').append($('<option ></option>').val(2).html('40'))
//                        $row.find('#containerQtyDistance').empty();
//
//                        }
//                    });
//                    $row.find('#containerTypeIdDistance').change(function(){
////                        var $itemrow = $(this).closest('tr');
//                        var val = $row.find('#containerTypeIdDistance').val();
//                        if(val == '1'){
//                        // 20FT Container
//                        $row.find('#containerQtyDistance').empty();
//                        $row.find('#containerQtyDistance').append($('<option ></option>').val(2).html('2'))
//                        }else if(val == '2'){
//                        // 40FT Container
//                        $row.find('#containerQtyDistance').empty();
//                        $row.find('#containerQtyDistance').append($('<option ></option>').val(1).html('1'))
//
//                        }
//                    });
//                $('.item-row:last', $itemsTable).after($row);
////                if ($('#ptpRouteContractCode:last').val() !== '') {}else{ // End if last itemCode input is empty
////                    alert("please fill route code");
////                }
//                return false;
//           // }
//            });
//
//
//     });// End DOM



        </script>
        <div class="pageheader">
            <h2><i class="fa fa-edit"></i> Vendor</h2>
            <div class="breadcrumb-wrapper">
                <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Vendor</a></li>
            <li class="active">Edit Fleet Vendor Contract</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body >
                <%--  <%try{%>--%>

                <style>
                    body {
                        font:13px verdana;
                        font-weight:normal;
                    }
                </style>

                <style>
                    .errorClass { 
                        background-color: red;
                    }
                    .noErrorClass { 
                        background-color: greenyellow;
                    }
                </style>

                <form name="vehicleVendorContract" method="post" >
                    <%--<%@ include file="/content/common/path.jsp" %>--%>
                    <%@ include file="/content/common/message.jsp" %>
                    <br>

                    <table width="990px;" align="center" border="0"  class="table table-info mb30 table-hover">
                        <thead ><tr  height="30">
                                <th  colspan="4" >Edit Vendor Contract Info</th>
                            </tr></thead>
                        <tr height="30">
                            <td class="text1">Vendor Name</td>

                            <td class="text1">
                                <input type="hidden" name="vendorId" id="vendorId" value="<c:out value="${vendorId}"/>" class="textbox">
                                <input type="hidden" name="vendorName" id="vendorName" value="<c:out value="${vendorName}"/>" class="textbox">
                                <c:out value="${vendorName}"/></td>
                            <td class="text1">Contract Type</td>
                            <td class="text1">
                                <select name="contractTypeId" id="contractTypeId" class="textbox" onchange="checkContractType(this.value);"  style="height:22px;width:150px;">
                                    <!--<option value="0" selected>--Select--</option>-->
                                    <option value="1" >Dedicated</option>
                                    <option value="2" >Hired</option>
                                    <option value="3" selected>Both</option>
                                </select>
                                <script>
                                    document.getElementById("contractTypeId").value =<c:out value="${contractTypeId}" />;
                                </script>
                            </td>
                        </tr>
                        <tr height="30">
                            <td class="text2">Contract From</td>
                            <td class="text2">
                                <c:out value="${startDate}" />
                                <input type="hidden" name="contractId" id="contractId" value="<c:out value="${contractId}" />" >
                            </td>

                            <td class="text2">Contract To</td>
                            <td class="text2"><input type="text" name="endDate" id="endDate" value="<c:out value="${endDate}" />" class="datepicker"  style="height:22px;width:150px;color:black;"></td>
                        </tr>
                        <tr height="30">
                            <td class="text1">Payment Type</td>
                            <td class="text1">
                                <select name="paymentType" id="paymentType" class="textbox"  style="height:22px;width:150px;">
                                    <option value="1" >Monthly</option>
                                    <option value="2" >Trip Wise</option>
                                    <option value="3" >FortNight</option>
                                </select>
                                <script>
                                    document.getElementById("paymentType").value =<c:out value="${paymentType}" />;
                                </script>
                                <input type="hidden"  name="contractFlag" id="contractFlag" value="<c:out value="${contractFlag}" />">
                            </td>
                            <td class="text1" colspan="2"></td>

                    </table>
                    <br>

                    <div id="tabs" >
                        <ul class="nav nav-tabs">
                            <!--<li class="active" data-toggle="tab"><a href="#deD"><span>DEDICATED</span></a></li>-->
                            <li id="fullTruck1" data-toggle="tab"><a href="#fullTruck"><span>Market Hire</span></a></li>
                            <!--<li data-toggle="tab"><a href="#distanceRate"><span>Distance - Based - Rate Details</span></a></li>-->
                            <!--<li><a href="#weightBreak"><span>Weight Break </span></a></li>-->
                        </ul>



                        <div id="distanceRate" class="tab-pane active" style="padding-left:1px;overflow: auto;display: none" >
                            <div class="inpad" style="padding-left:1px;">
                                <c:if test = "${vendorDistanceContractList != null}" >
                                    <table class="table table-info mb30 table-hover" width="98%" >
                                        <thead>
                                            <tr>
                                                <th>Sno</th>

                                                <th  ><font color="red"></font>Reference name</th>


                                                <th  ><font color="red"></font>Vehicle Type</th>
                                                <th  ><font color="red"></font>Container Type</th>
                                                <th  ><font color="red"></font>Container Qty</th>
                                                <th  ><font color="red"></font>Load Type</th>
                                                <th ><font color="red"></font>From Distance</th>

                                                <th  ><font color="red"></font>To Distance</th>

                                                <th  ><font color="red">*</font>Rate With Reefer</th>
                                                <th   ><font color="red">*</font>Rate Without Reefer</th>
                                                <th   ><font color="red">*</font>status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <%
                                                   int sno = 0;
                                            %>
                                            <c:forEach items="${vendorDistanceContractList}" var="cList">
                                                <%sno++;%>
                                                <tr>
                                                    <td><%=sno%><input type="hidden" id="distanceContractId<%=sno%>" name="distanceContractId" value="<c:out value="${cList.distanceContractId}"/>"/></td>
                                                    <td></td>
                                                    <td><c:out value="${cList.vehicleType}"/></td>
                                                    <td><c:out value="${cList.containerTypeName}"/></td>
                                                    <td><c:out value="${cList.containerQty}"/></td>
                                                    <td><c:out value="${cList.loadType}"/></td>
                                                    <td><c:out value="${cList.fromDistance}"/></td>
                                                    <td><c:out value="${cList.toDistance}"/></td>
                                                    <td>  <input type="text" name="rateWithReefer1" id="rateWithReefer1<%=sno%>" class="form-control"  value="<c:out value="${cList.marketHireWithReefer}"/>" style="width:120px;"/>
                                                    </td>
                                                    <td>  <input type="text" name="rateWithoutReeferDistance1" id="rateWithoutReeferDistance1<%=sno%>" class="form-control"  value="<c:out value="${cList.marketHire}"/>" style="width:120px;"/>
                                                    </td>

                                                    <td>   <input type="hidden" name="approvalStatus1" id="approvalStatus1<%=sno%>" value="<c:out value="${cList.activeStatus}"/>"/>
                                                        <select name="activeStatus1" id="activeStatus1<%=sno%>" class="form-control" style="width:90px;">
                                                            <option value="Y" selected >Active</option>
                                                            <option value="N"  >In Active</option>
                                                        </select></td>
                                                </tr>
                                            </c:forEach> 
                                        </tbody>
                                    </table>
                                </c:if> 

                                <br>
                                <br>

                                <table class="table table-info mb30 table-hover sortable" width="98%" id="distanceTable" >
                                    <thead>
                                        <tr height="40" >
                                            <th>Sno</th>
                                            <th>Approval Required?</th>
                                            <th  height="80" style="width: 10px;"><font color="red">*</font>Reference name</th>


                                            <th  height="30" style="width: 10px;"><font color="red">*</font>Vehicle Type</th>
                                            <th  height="30" style="width: 10px;"><font color="red">*</font>Container Type</th>
                                            <th  height="30" style="width: 10px;"><font color="red">*</font>Container Qty</th>
                                            <th  height="30" style="width: 10px;"><font color="red">*</font>Load Type</th>
                                            <th  height="30" style="width: 10px;"><font color="red">*</font>From Distance</th>

                                            <th  height="30" ><font color="red">*</font>To Distance</th>

                                            <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate With Reefer</th>
                                            <th  height="30" style="width: 90px;" ><font color="red">*</font>Rate Without Reefer</th>
                                            <th  height="30" style="width: 90px;" ><font color="red">*</font>Org Market HireRate</th>

                                        </tr>
                                    </thead>

                                    <tbody>
                                        <tr class="item-row">
                                            <td>1</td>


                                            <td align="left" ><span id="distanceStatusSpan" style="font-size: 14px"></span>  </td>

                                            <td><input type="text" name="referenceName" id="referenceName" value="" class="form-control"  style="width:150px;height:44px;"/></td>


                                            <td><c:if test="${vehicleTypeList != null}"><select onchange='setContainerTypeListDistance(this.value, "1")' name="vehicleTypeIdDistance" id="vehicleTypeIdDistance1"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType"><option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></select></c:if></td>

                                                    <td><select onchange='setContainerQtyListDistance(this.value, "1")' name="containerTypeIdDistance" id="containerTypeIdDistance1"  class="form-control" style="width:150px;height:44px;"></select></td>
                                                    <td><select name="containerQtyDistance" id="containerQtyDistance1" onchange="getDistanceMarketHireRate('1', '');" class="form-control" style="width:150px;height:44px;"></select></td>
                                                    <td><select name="loadTypeIdDistance" id="loadTypeIdDistance1"  class="form-control" style="width:150px;height:44px;" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></select></td>
                                                    <td><input name="fromDistance" value="" class="form-control" id="fromDistance1"  style="width:150px;height:44px;"  /></td>

                                                    <td><input name="toDistance" value="" class="form-control" id="toDistance1"  style="width:150px;height:44px;" /></td>

                                                    <td><input name="rateWithReeferDistance" value="0" class="form-control" id="rateWithReeferDistance1"  style="width:150px;height:44px;" 
                                                               onKeyPress="return onKeyPressBlockCharacters(event);" onchange="getDistanceMarketHireRate('1', '');"/></td>
                                                    <td><input name="rateWithoutReeferDistance" value="0" class="form-control" id="rateWithoutReeferDistance1"  style="width:150px;height:44px;" onKeyPress="return onKeyPressBlockCharacters(event);" onchange="getDistanceMarketHireRate('1', '');" /></td>
                                                    <td align="left" ><span id="distanceMarketRate1" style="font-size: 14px;color:green;"></span></td>
                                                    <!--<td align="left" ><span id="distanceMarketRateSpan" style="font-size: 14px;color:green;"></span></td>-->
                                                </tr> 
                                            </tbody>
                                        </table>

                                        <a href="#" id="addRowDistance" class="button-clean large"><span> <img src="/throttle/images/icon-plus.png" alt="Add" title="Add Row" /><b> Add Distance Contract</b></span></a>
                                        <br>
                                        <br>
                                        <center>
                                            <!--<input type="button" class="btn btn-success" value="Save" style="width: 120px" onclick="submitDistancePage(this.value)"/>-->
                                        </center>
                                        <br>
                                        <br>
                                    </div></div>



                                <div id="fullTruck" class="tab-pane active">
                                    <div id="routeFullTruck">
                                <c:if test="${hireList != null}">
                                    <table align="center" border="0" id="ondemandTable" class="table table-info mb30 table-hover" style="width:100%" >
                                        <thead>
                                            <tr style="height: 40px;" id="tableDesingTH">
                                                <th align="center">S.No</th>
                                                <th align="center">From Date</th>
                                                <th align="center">To Date</th>
                                                <th align="center">Vehicle Type</th>
                                                <th align="center">Origin</th>
                                                <th align="center">Interim Point1</th>
                                                <th align="center">Interim Point2</th>
                                                <th align="center">Interim Point3</th>
                                                <th align="center">Interim Point4</th>
                                                <th align="center">Destination</th>
                                                <th align="center">Load Type</th>
                                                <th align="center">Rate with reefer</th>
                                                <th align="center">Rate without reefer</th>
                                                
                                                <th align="center">Advance Type</th>
                                                <th align="center">Advance Value</th>
                                                <th align="center">Initial Advance</th>
                                                <th align="center">Remaining Amount</th>
                                                
                                                <th align="center">Container Type</th>
                                                <th align="center">Container Qty</th>
                                                <c:if test="${contractFlag == '2'}">
                                                    <th align="center">From(Tonnage)</th>
                                                    <th align="center">To(Tonnage)</th>
                                                </c:if>
                                                <th align="center">Current Status</th>
                                                <th align="center">Active Status</th>
                                                <th align="center">select</th>
                                                <th align="center">LHC</th>
                                                <th align="center">Used</th>                                                
                                                <th align="center">Not Used</th> 
                                            </tr></thead>
                                        <tbody>
                                            <% int index = 1;%>
                                            <c:forEach items="${hireList}" var="route">
                                                <%
                                                    String classText = "";
                                                    int oddEven = index % 2;
                                                    if (oddEven > 0) {
                                                        classText = "text2";
                                                    } else {
                                                        classText = "text1";
                                                    }
                                                %>
                                                <c:if test="${route.activeInd == 'Y'}">
                                                    <tr height="30">
                                                        <td class="<%=classText%>"> <%=index++%></td>
                                                        <td class="<%=classText%>"><c:out value="${route.startDate}"/><input type="hidden" name="fromDateE" id="fromDateE<%=index%>" value="<c:out value="${route.startDate}"/>" class="datepicker form-control" style="height:22px;width:130px;"/></td>
                                                        <td class="<%=classText%>"><c:out value="${route.endDate}"/><input type="hidden" name="toDateE" id="toDateE<%=index%>"  value="<c:out value="${route.endDate}"/>" class="datepicker form-control" style="height:22px;width:130px;" /></td>
                                                        <td class="<%=classText%>">
                                                            <input type="hidden" id="originNameFullTruckE<%=index%>" name="originNameFullTruckE" value="<c:out value="${route.originNameFullTruck}"/>" />
                                                            <input type="hidden" id="originIdFullTruckE<%=index%>" name="originIdFullTruckE" value="<c:out value="${route.originIdFullTruck}"/>" />
                                                            <input type="hidden" id="pointName1E<%=index%>" name="pointName1E" value="<c:out value="${route.point1Name}"/>" />
                                                            <input type="hidden" id="point1IdE<%=index%>" name="point1IdE" value="<c:out value="${route.point1Id}"/>" />
                                                            <input type="hidden" id="pointName2E<%=index%>" name="pointName2E" value="<c:out value="${route.point2Name}"/>" />
                                                            <input type="hidden" id="point2IdE<%=index%>" name="point2IdE" value="<c:out value="${route.point2Id}"/>" />
                                                            <input type="hidden" id="pointName3E<%=index%>" name="pointName3E" value="<c:out value="${route.point3Name}"/>" /> 
                                                            <input type="hidden" id="point3IdE<%=index%>" name="point3IdE" value="<c:out value="${route.point3Id}"/>" /> 
                                                            <input type="hidden" id="pointName4E<%=index%>" name="pointName4E" value="<c:out value="${route.point4Name}"/>" />
                                                            <input type="hidden" id="point4IdE<%=index%>" name="point4IdE" value="<c:out value="${route.point4Id}"/>" />
                                                            <input type="hidden" id="destinationNameFullTruckE<%=index%>" name="destinationNameFullTruckE" value="<c:out value="${route.destinationNameFullTruck}"/>" />
                                                            <input type="hidden" id="destinationIdFullTruckE<%=index%>" name="destinationIdFullTruckE" value="<c:out value="${route.destinationIdFullTruck}"/>" />
                                                            <input type="hidden" name="approvalStatusE" id="approvalStatusE<%=index%>"  value="<c:out value="${route.approvalStatus}"/>" />
                                                            <input type="hidden" name="vehicleTypeIdE" id="vehicleTypeIdE<%=index%>" value="<c:out value="${route.vehicleTypeId}"/>" /> 
                                                            <input type="hidden" name="vehicleTypeE" id="vehicleTypeE<%=index%>" value="<c:out value="${route.vehicleTypeName}"/>" /> 
                                                            <input type="hidden" name="loadTypeE1" id="loadTypeE1<%=index%>" value="<c:out value="${route.loadTypeId}"/>" /> 
                                                            <input type="hidden" name="containerTypeIdE1" id="containerTypeIdE1<%=index%>" value="<c:out value="${route.containerTypeId}"/>" /> 
                                                            <input type="hidden" name="containerQtyE1" id="containerQtyE1<%=index%>" value="<c:out value="${route.containerQty}"/>" /> 
                                                            <input type="hidden" name="fromDateE1" id="fromDateE1<%=index%>" value="<c:out value="${route.startDate}"/>" /> 
                                                            <input type="hidden" name="toDateE1" id="toDateE1<%=index%>" value="<c:out value="${route.endDate}"/>" /> 
                                                            <input type="hidden" name="vehicleTypeIdEdit"  value="<c:out value="${route.vehicleTypeId}"/>" />
                                                            <c:out value="${route.vehicleTypeName}"/></td>
                                                        <!--<td class="<%=classText%>"><c:out value="${route.vehicleUnits}"/></td>-->
                                                        <td class="<%=classText%>"><input type="hidden" name="originIdFullTruckEdit" value="<c:out value="${route.originIdFullTruck}"/>" />
                                                            <c:out value="${route.originNameFullTruck}"/></td>
                                                        <td class="<%=classText%>"><input type="hidden" name="point1IdEdit" value="<c:out value="${route.point1Id}"/>" />
                                                            <c:out value="${route.point1Name}"/></td>
                                                        <td class="<%=classText%>"><input type="hidden" name="point2IdEdit" value="<c:out value="${route.point2Id}"/>" />
                                                            <c:out value="${route.point2Name}"/></td>
                                                        <td class="<%=classText%>"><input type="hidden" name="point3IdEdit" value="<c:out value="${route.point3Id}"/>" /> 
                                                            <c:out value="${route.point3Name}"/></td>
                                                        <td class="<%=classText%>"><input type="hidden" name="point4IdEdit" value="<c:out value="${route.point4Id}"/>" />
                                                            <c:out value="${route.point4Name}"/></td><input type="hidden" name="destinationIdFullTruckEdit" value="<c:out value="${route.destinationIdFullTruck}"/>" />
                                                <td class="<%=classText%>"><c:out value="${route.destinationNameFullTruck}"/></td>
                                                <td class="<%=classText%>"><input type="hidden" name="loadTypeEdit"  value="<c:out value="${route.loadTypeId}"/>" /> 
                                                    <c:if test="${route.loadTypeId == '1'}">
                                                        Empty Trip
                                                    </c:if>
                                                    <c:if test="${route.loadTypeId == '2'}">
                                                        Load Trip
                                                    </c:if>
                                                </td>
    <!--                                            <td class="<%=classText%>"><c:out value="${route.trailerType}"/></td>
                                                <td class="<%=classText%>"   ><c:out value="${route.trailorTypeUnits}"/></td>-->
                                                <td class="<%=classText%>"   >
                                                    <input type="hidden" name="contractHireId" id="contractHireId<%=index%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.contractHireId}"/>" />
                                                    <input type="hidden" name="spotCostOld" id="spotCostOld<%=index%>" value="<c:out value="${route.spotCost}"/>" />
                                                    <input type="text" name="spotCostE" id="spotCostE<%=index%>" onChange="findAdvanceAmountE(<%=index%>)" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.spotCost}"/>" style="height:22px;width:130px;" /></td>
                                                <td class="<%=classText%>"   >
                                                    <input type="hidden" name="additionalCostOld" id="additionalCostOld<%=index%>" value="<c:out value="${route.additionalCost}"/>" />
                                                    <input type="text" name="additionalCostE" id="additionalCostE<%=index%>" onChange="findAdvanceAmountE(<%=index%>)" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${route.additionalCost}"/>" style="height:22px;width:130px;" />
                                                </td>
                                                <td class="<%=classText%>"><select name="advanceModeE" id="advanceModeE<%=index%>"  onChange="findAdvanceAmountE(<%=index%>)" style="height:22px;width:130px;" >
                                                    <option value=0>-Select-</option>
                                                    <option value=1>PERCENTAGE</option>
                                                    <option value=2>FLAT</option>
                                                    <option value=3>CREDIT</option>
                                                </select>
                                            </td>
                                                        
                                            <script>
                                            var modetype = '<c:out value="${route.advanceTripMode}"/>';                                            
                                            if (modetype == 'PERCENTAGE') {
                                                document.getElementById("advanceModeE"+<%=index%>).value = 1;
                                            } else if(modetype == 'FLAT'){
                                                document.getElementById("advanceModeE"+<%=index%>).value = 2;
                                            } else {
                                                document.getElementById("advanceModeE"+<%=index%>).value = 3;
                                            }
                                            </script> 

                                                <td class="<%=classText%>"><input type="text" name="modeRateE" id="modeRateE<%=index%>" value="<c:out value="${route.modeTripRate}"/>"  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmountE(<%=index%>)" style="height:22px;width:160px;"/></td>
                                                <td class="<%=classText%>"><input type="text" name="initialAdvanceE" id="initialAdvanceE<%=index%>" value="<c:out value="${route.initialTripAdvance}"/>" readonly  style="height:22px;width:160px;"/></td>
                                                <td class="<%=classText%>"><input type="text" name="endAdvanceE" id="endAdvanceE<%=index%>" value="<c:out value="${route.endTripAdvance}"/>"  readonly style="height:22px;width:160px;"/></td>
                                                  
<!--                                                    <td class="<%=classText%>"   ><c:out value="${route.travelKmFullTruck}"/></td>
                                                <td class="<%=classText%>"   ><c:out value="${route.travelHourFullTruck}"/></td>
                                                <td class="<%=classText%>"   ><c:out value="${route.travelMinuteFullTruck}"/></td>-->
                                                <td>
                                                    <input type="hidden" name="containerTypeIdEdit" value="<c:out value="${route.containerTypeId}"/>" /> 
                                                    <c:if test="${containerTypeList != null}">
                                                        <select name="containerTypeIdE" id="containerTypeIdE<%=index%>" style="height:22px;width:130px;" >
                                                            <option value="0">-Select-</option>
                                                            <c:forEach items="${containerTypeList}" var="conType">
                                                                <option value="<c:out value="${conType.containerId}"/>"><c:out value="${conType.containerName}"/></option>
                                                            </c:forEach>
                                                        </c:if>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="hidden" name="fromId" id="fromId" value="<c:out value="${route.fromId}"/>" /> 
                                                    <input type="hidden" name="toId" id="toId" value="<c:out value="${route.toId}"/>" /> 
                                                    <input type="hidden" name="containerQtyEdit" value="<c:out value="${route.containerQty}"/>" /> 
                                                    <input type="hidden" name="vehName" id="vehName" value="<c:out value="${route.vehicleTypeName}"/>" /> 
                                                    <select name="containerQtyE" id="containerQtyE<%=index%>" style="height:22px;width:130px;" >
                                                        <option value="">-Select-</option>
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                    </select>
                                                </td>
                                                <c:if test="${contractFlag == '1'}">
                                                     <td style="display: none"><input type="hidden" name="fromCBMEdit" id="fromCBMEdit" value="<c:out value="${route.fromTon}"/>" style="height:22px;width:130px;" /> </td>
                                                     <td style="display: none"><input type="hidden" name="toCBMEdit" id="toCBMEdit" value="<c:out value="${route.toTon}"/>" style="height:22px;width:130px;" /> </td>
                                                </c:if>
                                                <c:if test="${contractFlag == '2'}">
                                                     <td><input type="text" name="fromCBMEdit" id="fromCBMEdit" value="<c:out value="${route.fromTon}"/>" style="height:22px;width:130px;" /> </td>
                                                     <td><input type="text" name="toCBMEdit" id="toCBMEdit" value="<c:out value="${route.toTon}"/>" style="height:22px;width:130px;" /> </td>
                                                </c:if>
                                               
                                                <script>
                                                    document.getElementById("containerTypeIdE" +<%=index%>).value =<c:out value="${route.containerTypeId}"/>
                                                    document.getElementById("containerQtyE" +<%=index%>).value =<c:out value="${route.containerQty}"/>
                                                    //  alert("contype:"+document.getElementById("containerTypeIdE" +<%=index%>).value);
                                                </script>
                                                <c:if test="${route.approvalStatus == 1}">
                                                    <td class="<%=classText%>"   ><font color="green" size="2">Approved</font></td>
                                                    </c:if>
                                                    <c:if test="${route.approvalStatus == 2}">
                                                    <td class="<%=classText%>"   ><font color="blue" size="2">WFA</font></td>
                                                    </c:if>
                                                    <c:if test="${route.approvalStatus == 3}">
                                                    <td class="<%=classText%>"   ><font color="red" size="2">Reject</font></td>
                                                    </c:if>
                                                <td class="<%=classText%>"   >
                                                    <select name="activeIndD" id="activeIndD<%=index%>" style="height:22px;width:130px;">
                                                        <c:if test="${route.activeInd == 'Y'}" >
                                                            <option value="Y" selected>Yes</option>
                                                            <option value="N">No</option>
                                                        </c:if>
                                                        <c:if test="${route.activeInd == 'N'}" >
                                                            <option value="N" selected>No</option>
                                                            <option value="Y" >Yes</option>
                                                        </c:if>
                                                    </select>
                                                </td>
                                                <td class="<%=classText%>"   >
                                                    <input type="checkbox" name="editStatus" id="editStatus<%=index%>" value="1" onclick="setActiveInd('<%=index%>')"/>  
                                                    <input type="hidden" id="activeInd<%=index%>" name="activeInd" value="N"/>
                                                    <!--<input type="checkbox" name="select"/>-->
                                                </td>
                                                <td class="<%=classText%>">
                                                    <a href="#" >                                                                
                                                        <input type="button" class="btn1c"  name="create" value="Create" onclick="createLHCDetails('<c:out value="${route.contractHireId}"/>', '<c:out value="${route.vehicleTypeId}"/>', '<c:out value="${route.additionalCost}"/>', '<c:out value="${vendorName}"/>')"/>
                                                    </a>&nbsp;<br/>
                                                    <a href="#" >
                                                        <input type="button" class="btn1v"   name="view" value="View" onclick="viewLHCDetails('<c:out value="${route.contractHireId}"/>', 'all', '<c:out value="${vendorName}"/>');"/>                                                            
                                                    </a>
                                                </td>                                                        
                                                <td class="<%=classText%>">
                                                    <a href="#" >
                                                        <input type="button" class="btn1u" name="used" value="<c:out value="${route.lhcUsed}"/>" onclick="viewLHCDetails('<c:out value="${route.contractHireId}"/>', 'used', '<c:out value="${vendorName}"/>');"/>
                                                    </a>
                                                </td>
                                                <td class="<%=classText%>">
                                                    <a href="#" ><input type="button" class="btn1n"  name="notused" value="<c:out value="${route.lhcNotUsed}"/>" onclick="viewLHCDetails('<c:out value="${route.contractHireId}"/>', 'notused', '<c:out value="${vendorName}"/>');"/></a>
                                                </td>
                                                </tr>
                                            </c:if>
                                        </c:forEach>
                                        </tbody>
                                    </table>

                                    <style>
                                        body {
                                            font:13px verdana;
                                            font-weight:normal;
                                        }
                                    </style>

                                    <style>
                                        .errorClass { 
                                            background-color: red;
                                        }
                                        .noErrorClass { 
                                            background-color: greenyellow;
                                        }
                                    </style>
                                    <style>
                                        .btn1v {
                                            background-color: #83C9FD;
                                            border: none;
                                            color: white;    
                                            text-align: center;
                                            text-decoration: none;    
                                            font-size: 9px;
                                        }
                                        .btn1c {
                                            background-color: #45F13D;
                                            border: none;
                                            color: white;    
                                            text-align: center;
                                            text-decoration: none;    
                                            font-size: 9px;
                                        }
                                        .btn1n {
                                            background-color: #901A07;
                                            border: none;
                                            color: white;    
                                            text-align: center;
                                            text-decoration: none;    
                                            font-size: 9px;
                                        }
                                        .btn1u {
                                            background-color: #FA9A54;
                                            border: none;
                                            color: white;    
                                            text-align: center;
                                            text-decoration: none;    
                                            font-size: 9px;
                                        }
                                    </style>


                                    <script>
                                        function createLHCDetails(hireId, vehtype, rate, vendor) {
                                            var vehName = document.getElementById("vehName").value;
                                            var fromId = document.getElementById("fromId").value;
                                            var toId = document.getElementById("toId").value;
                                            window.open('/throttle/createLHCDetails.do?contractHireId=' + hireId + "&vehicleTypeId=" + vehtype + "&vehicleTypeName="+vehName+ "&rate=" + rate + "&vendor=" + vendor + "&originId=" + fromId + "&destinationId=" + toId , 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                        }


                                        function viewLHCDetails(hireId, type, vendor) {
                                            window.open('/throttle/viewLHCDetails.do?contractHireId=' + hireId + "&Type=" + type + "&vendor=" + vendor, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
                                        }

                                        function setActiveInd(sno) {
    //                                        alert(sno)
                                            if (document.getElementById("editStatus" + sno).checked == true) {
                                                document.getElementById("activeInd" + sno).value = 'Y';
                                            } else {
                                                document.getElementById("activeInd" + sno).value = 'N';
                                            }
                                        }



                                        function checkKey(obj, e, id, cnt) {
                                            var orgName = 'originNameFullTruck' + cnt;
                                            var point1Name = 'pointName1' + cnt;
                                            var point2Name = 'pointName2' + cnt;
                                            var point3Name = 'pointName3' + cnt;
                                            var point4Name = 'pointName4' + cnt;
                                            var destinationName = 'destinationNameFullTruck' + cnt;
                                            var pointId = "";
                                            if (id == orgName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }
                                            if (id == point1Name) {
                                                pointId = 'pointId1' + cnt;
                                            }
                                            if (id == point2Name) {
                                                pointId = 'pointId2' + cnt;
                                            }
                                            if (id == point3Name) {
                                                pointId = 'pointId3' + cnt;
                                            }
                                            if (id == point4Name) {
                                                pointId = 'pointId4' + cnt;
                                            }
                                            if (id == destinationName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }

                                            var idVal = $('#' + pointId).val();
                                            if (e.keyCode == 46 || e.keyCode == 8) {
                                                $('#' + id).val('');
                                                $('#' + pointId).val('');
                                            } else if (idVal != '' && e.keyCode != 13) {
//                                                $('#' + id).val('');
//                                                $('#' + pointId).val('');
                                            }
                                        }

                                        function checkPointNames(id, cnt) {
                                            var orgName = 'originNameFullTruck' + cnt;
                                            var point1Name = 'pointName1' + cnt;
                                            var point2Name = 'pointName2' + cnt;
                                            var point3Name = 'pointName3' + cnt;
                                            var point4Name = 'pointName4' + cnt;
                                            var destinationName = 'destinationNameFullTruck' + cnt;
                                            var pointId = "";
                                            if (id == orgName) {
                                                pointId = 'originIdFullTruck' + cnt;
                                            }
                                            if (id == point1Name) {
                                                pointId = 'pointId1' + cnt;
                                            }
                                            if (id == point2Name) {
                                                pointId = 'pointId2' + cnt;
                                            }
                                            if (id == point3Name) {
                                                pointId = 'pointId3' + cnt;
                                            }
                                            if (id == point4Name) {
                                                pointId = 'pointId4' + cnt;
                                            }
                                            if (id == destinationName) {
                                                pointId = 'destinationIdFullTruck' + cnt;
                                            }
                                            var idValue = $('#' + id).val();
                                            var idValue1 = $('#' + pointId).val();
                                            if (idValue != '' && idValue1 == '') {
                                                alert('Invalid Point Name');
                                                $('#' + id).focus();
//                                                $('#'+id).val('');
//                                                $('#'+pointId).val('');
                                            }
                                            getMarketHireRate(cnt, cnt);
                                        }
                                        var container = "";
                                        function addRouteFullTruck() {

                                            var iCnt = <%=index%>;
                                            var iCnt1 = 1;
                                            var rowCnt = <%=index%>;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            container = $($("#routeFullTruck")).css({
                                                padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            var routeInfoSize = $('#routeDetails' + iCnt + ' tr').size();
                                            //                                    alert(routeInfoSize);
                                            //                                    iCnt = parseFloat(iCnt) + parseFloat(routeInfoSize / 2);
                                            rowCnt = parseFloat(iCnt) + parseFloat(routeInfoSize / 2);
                                            $(container).last().after('<span id="statusSpan" style="color: red"></span>\n\
                                        <table  id="routeDetails' + iCnt + '"  border="1" >\n\
                                <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                <td>Sno</td>\n\
                                <td>Origin</td>\n\
                                <td>Interim Point1</td>\n\
                                <td>Interim Point2</td>\n\
                                <td>Interim Point3</td>\n\
                                <td>Interim Point4</td>\n\
                                <td>Destination</td>\n\
                                <tr style="height:22px;"><td>Route&nbsp; ' + rowCnt + '</td>\n\
                                <td><input type="hidden" name="originIdFullTruck" id="originIdFullTruck' + iCnt + '" value="" />\n\
                                 <input type="text" name="originNameFullTruck" id="originNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/><input type="hidden" name="iCnt" id="iCnt' + iCnt + '" value="' + iCnt + '"/></td>\n\
                                    <td><input type="hidden" name="pointId1" id="pointId1' + iCnt + '" value="0" />\n\
                                    <input type="text" name="pointName1" id="pointName1' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')" onKeyPress="return onKeyPressBlockNumbers(event);"  style="height:22px;width:160px;"/>\n\</td>\n\
                                    <td><input type="hidden" name="pointId2" id="pointId2' + iCnt + '" value="0" />\n\
                                    <input type="text" name="pointName2" id="pointName2' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                    <td><input type="hidden" name="pointId3" id="pointId3' + iCnt + '" value="0" />\n\
                                    <input type="text" name="pointName3" id="pointName3' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                    <td><input type="hidden" name="pointId4" id="pointId4' + iCnt + '" value="0" />\n\
                                    <input type="text" name="pointName4" id="pointName4' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                    <td><input type="hidden" name="destinationIdFullTruck" id="destinationIdFullTruck' + iCnt + '" value="" />\n\
                                    <input type="text" name="destinationNameFullTruck" id="destinationNameFullTruck' + iCnt + '" value="" onchange="checkPointNames(this.id, ' + iCnt + ')" onkeyup="return checkKey(this, event, this.id,' + iCnt + ')"  onKeyPress="return onKeyPressBlockNumbers(event);" style="height:22px;width:160px;"/>\n\</td>\n\
                                <input type="hidden" name="travelKmFullTruck" id="travelKmFullTruck' + rowCnt + '" value="" style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                                <input type="hidden" name="travelHourFullTruck" id="travelHourFullTruck' + rowCnt + '" value="" style="height:22px;width:160px;"  onKeyPress="return onKeyPressBlockCharacters(event);"/>\n\
                                <input type="hidden" name="travelMinuteFullTruck" id="travelMinuteFullTruck' + rowCnt + '" value="" style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                <input type="hidden" name="routeIdFullTruck" id="routeIdFullTruck' + rowCnt + '" value="" /></tr>\n\
                                 </table>\n\
                                <table  id="routeInfoDetailsFullTruck' + iCnt1 + rowCnt + '"  border="1">\n\
                                <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:30px;">\n\
                                <td>Status</td><td>Sno</td><td>FromDate</td><td>ToDate</td><td>Vehicle Type</td><td>Load Type</td><td>Container Type</td><td>Container Qty</td>\n\
                                            <c:if test="${contractFlag == '2'}"><td>From(Tonnage)</td><td>To(Tonnage)</td></c:if>\n\
<td>Rate with Reefer</td><td>Rate without Reefer</td>\n\
                                <td>Advance Type</td><td>Advance Value</td><td>Initial Advance</td><td>Remaining Amount</td><td>Agreed No.of Vehicle</td></tr>\n\
                                <tr style="height:22px;">\n\
                                <td><span id="approvalStatusSpan' + iCnt + '">Not Filled</span></td><td>' + iCnt1 + '\n\
                                <input type="hidden" name="iCnt1" id="iCnt1' + iCnt1 + '" value="' + rowCnt + '"/></td>\n\
                                <td><input type="text" name="fromDate1" style="width:160px;"  id="fromDate1' + iCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                <td><input type="text" name="toDate1"  style="width:160px;" id="toDate1' + iCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + iCnt1 + '" value="1" /><select type="text" name="vehicleTypeId" id="vehicleTypeId' + iCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + iCnt + ');setContainerTypeList(this.value,' + iCnt1 + ');getMarketHireRate(' + iCnt + ',' + iCnt + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                <td><select name="loadTypeId" id="loadTypeId' + iCnt + '"  class="textbox" style="height:22px;width:160px;" onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');" ><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></select></td>\n\
                                <td><c:if test="${containerTypeList != null}"><select name="containerTypeId" id="containerTypeId' + iCnt + '"  onChange="setContainerValue(' + iCnt1 + ',this.value);setContainerQtyList(this.value,' + iCnt + ',' + iCnt + ');getMarketHireRate(' + iCnt + ',' + iCnt + ');" class="textbox" style="height:22px;width:110px;"><option value="0">-Select-</option><c:forEach items="${containerTypeList}" var="conType"><option value="<c:out value="${conType.containerId}"/>"><c:out value="${conType.containerName}"/></option></c:forEach></c:if></select></td>\n\
                                <td><select name="containerQty" id="containerQty' + iCnt + '" onchange="getMarketHireRate(' + iCnt + ',' + iCnt + ');" class="textbox" style="height:22px;width:110px;" ><option value="0">-Select-</option><option value="1">1</option><option value="2">2</option></select></td>\n\
\n\ <c:if test="${contractFlag == '1'}"><input type="hidden" name="fromCBM" id="fromCBM" value="0" ><input type="hidden" name="toCBM" id="toCBM" value="0" > </c:if><c:if test="${contractFlag == '2'}"><td><input type="text" name="fromCBM" id="fromCBM" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"></td>\n\
<td><input type="text" name="toCBM" id="toCBM" value="" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"></td></c:if>\n\
                                <td><input type="text" name="spotCost" id="spotCost' + iCnt + '" value="0" onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/></td>\n\
                                <td><input type="text" name="additionalCost" id="additionalCost' + iCnt + '" value="0" onkeyup="checkMarketRate(this.value,' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;" />\n\
                                <input type="hidden" name="marketRate" id="marketRate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><span id="marketRateSpan' + iCnt + '"></span></td>\n\
                                <td><select name="advanceMode" id="advanceMode' + iCnt + '"  onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;" >\n\
                                <option value=0>-Select-</option>\n\
                                <option value=1>PERCENTAGE</option>\n\
                                <option value=2>FLAT</option>\n\
                                <option value=3>CREDIT</option>\n\
                                </select>\n\
                                </td>\n\
                                <td><input type="text" name="modeRate" id="modeRate' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + iCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                <td><input type="text" name="initialAdvance" id="initialAdvance' + iCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
                                <td><input type="text" name="endAdvance" id="endAdvance' + iCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\
                                <td><input type="text" name="lHCNO" id="lHCNO' + iCnt + '" value=""  style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                </tr></table>\n\
                                <br><table border="0"  id="routebutton' + iCnt1 + rowCnt + '" ><tr>\n\
                                <td><input class="btn btn-info" type="button" name="addRouteDetailsFullTruck" id="addRouteDetailsFullTruck' + iCnt1 + rowCnt + '" value="Add" \n\
                                onclick="addRow(' + iCnt1 + rowCnt + ',' + iCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/>\n\
                                <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck" id="removeRouteDetailsFullTruck' + iCnt1 + rowCnt + '" value="Remove"  onclick="deleteRow(' + iCnt1 + rowCnt + ')" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></td>\n\
                                </tr></table></td></tr></table>');
                                            callOriginAjaxFullTruck(iCnt);
                                            callPointId1AjaxFullTruck(iCnt);
                                            callPointId2AjaxFullTruck(iCnt);
                                            callPointId3AjaxFullTruck(iCnt);
                                            callPointId4AjaxFullTruck(iCnt);
                                            callDestinationAjaxFullTruck(iCnt);
                                            $('.datepicker').datepicker();
                                        }
                                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                                        var divValue, values = '';
                                        function GetTextValue() {
                                            $(divValue).empty();
                                            $(divValue).remove();
                                            values = '';
                                            $('.input').each(function() {
                                                divValue = $(document.createElement('div')).css({
                                                    padding: '5px', width: '200px'
                                                });
                                                values += this.value + '<br />'
                                            });
                                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                                            $('body').append(divValue);
                                        }

//
                                        function addRow(val, v1) {
                                            var loadCnt2 = val;
                                            var loadCnt1 = v1;
                                            var routeInfoSize = $('#routeInfoDetailsFullTruck' + loadCnt2 + ' tr').size();
                                            var concatVar = v1 + "" + (routeInfoSize - 1);
                                            var loadCnt = parseInt(concatVar);
                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            $('#routeInfoDetailsFullTruck' + val + ' tr').last().after(
                                                    '<tr>\n\
                                    <td><span id="approvalStatusSpan' + loadCnt + '">Not Filled</span><td>' + routeInfoSize + '<input type="hidden" name="iCnt1" id="iCnt1' + loadCnt + '" value="' + loadCnt1 + '"/></td>\n\
                                    <td><input type="text" name="fromDate1" style="width:160px;"  id="fromDate1' + loadCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                    <td><input type="text" name="toDate1"  style="width:160px;" id="toDate1' + loadCnt + '" class="datepicker form-control"  value=""/></td>\n\
                                    <td><input type="hidden" name="vehicleUnits" id="vehicleUnits' + loadCnt + '" value="1" style="height:22px;width:160px;"/><select type="text" name="vehicleTypeId" id="vehicleTypeId' + loadCnt + '" style="height:22px;width:160px;" onchange="setVehicleTypeDetails(this,' + loadCnt + ');setContainerTypeList(this.value,' + loadCnt + ');getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ')"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                    <td><select name="loadTypeId" id="loadTypeId' + loadCnt + '"  class="textbox" style="height:22px;width:160px;" onchange="getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ')"><option value="0">-Select-</option><option value="1">Empty Trip</option><option value="2">Load Trip</option></select></td>\n\
                                    <td><c:if test="${containerTypeList != null}"><select name="containerTypeId" id="containerTypeId' + loadCnt + '"  class="textbox" style="height:22px;width:110px;" onChange="setContainerValue(' + loadCnt + ',this.value);setContainerQtyList(this.value,' + loadCnt + ');getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ')"><option value="0">-Select-</option><c:forEach items="${containerTypeList}" var="conType"><option value="<c:out value="${conType.containerId}"/>"><c:out value="${conType.containerName}"/></option></c:forEach></c:if></select></td>\n\
                                    <td><select name="containerQty" id="containerQty' + loadCnt + '"  class="textbox" style="height:22px;width:110px;" onchange="getMarketHireRate(' + loadCnt + ',' + loadCnt1 + ')"><option value="0">-Select-</option><option value="1">1</option><option value="2">2</option></select></td>\n\
                                    <td><input type="text" name="spotCost" id="spotCost' + loadCnt + '" value="0" onkeyup="checkMarketRate(this.value,' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="additionalCost" id="additionalCost' + loadCnt + '" value="0"  onkeyup="checkMarketRate(this.value,' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="hidden" name="marketRate" id="marketRate' + loadCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:160px;"/><span id="marketRateSpan' + loadCnt + '"></span>\n\
                                    <td><select name="advanceMode" id="advanceMode' + loadCnt + '"  onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;" >\n\
                                    <option value=0>-Select-</option>\n\
                                    <option value=1>PERCENTAGE</option>\n\
                                    <option value=2>FLAT</option>\n\
                                    <option value=3>CREDIT</option>\n\
                                    </select>\n\
                                    </td>\n\
                                    <td><input type="text" name="modeRate" id="modeRate' + loadCnt + '" value="0" onChange="findAdvanceAmount(' + loadCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" onChange="findAdvanceAmount(' + loadCnt + ')" style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="initialAdvance" id="initialAdvance' + loadCnt + '" value="" readonly  style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="endAdvance" id="endAdvance' + loadCnt + '" value=""  readonly style="height:22px;width:160px;"/></td>\n\
                                    <td><input type="text" name="lHCNO" id="lHCNO' + loadCnt + '" value=""  style="height:22px;width:160px;" onKeyPress="return onKeyPressBlockCharacters(event);"/></td>\n\
                                    </td></tr>');
                                            
                                        $('.datepicker').datepicker();
                                            loadCnt++;
                                        }
                                        function deleteRow(val) {
                                            var loadCnt = val;

                                            var addRouteDetails = "addRouteDetailsFullTruck" + loadCnt;
                                            var routeInfoDetails = "routeInfoDetailsFullTruck" + loadCnt;
                                            if ($('#routeInfoDetailsFullTruck' + loadCnt + ' tr').size() > 2) {
                                                $('#routeInfoDetailsFullTruck' + loadCnt + ' tr').last().remove();
                                                loadCnt = loadCnt - 1;
                                            } else {
                                                alert('One row should be present in table');
                                            }
                                        }
                                            </script>
                                            <script>
                                                 
        function findAdvanceAmount(sno) {

        var additionalCost = $("#additionalCost" + sno).val();
        var advanceMode = $("#advanceMode" + sno).val();
        var modeRate = $("#modeRate" + sno).val();

        var endAdvance = "";
        var initialAdvance = "";

        if (advanceMode != "") {

            if (advanceMode == 1) {
                var percentValue = "";
                if(modeRate==0){
                  percentValue = 0;  
                }else{
                    percentValue = parseFloat(additionalCost) * (parseFloat(modeRate) / 100)
                }
                
                initialAdvance = percentValue;
                endAdvance = parseFloat(additionalCost) - parseFloat(percentValue);
            } else if (advanceMode == 2) {
                var flatValue = parseFloat(additionalCost) - parseFloat(modeRate);
                initialAdvance = parseFloat(modeRate);
                endAdvance = parseFloat(flatValue);
            } 
            else if (advanceMode == 3) {                
                initialAdvance = 0;
                endAdvance = parseFloat(additionalCost);
                document.getElementById("modeRate" + sno).value = 0;
            }
            else {
                //alert("Please Select advance mode");
                
                document.getElementById("initialAdvance" + sno).value = 0;
                document.getElementById("endAdvance" + sno).value = 0;
                document.getElementById("modeRate" + sno).value = 0;
                return false;
            }

            document.getElementById("initialAdvance" + sno).value = initialAdvance;
            document.getElementById("endAdvance" + sno).value = endAdvance;
        }
    }
        function findAdvanceAmountE(sno) {

        var additionalCost = $("#additionalCostE" + sno).val();
        var advanceMode = $("#advanceModeE" + sno).val();
        var modeRate = $("#modeRateE" + sno).val();

        var endAdvance = "";
        var initialAdvance = "";

        if (advanceMode != "") {

            if (advanceMode == 1) {
                var percentValue = "";
                if(modeRate==0){
                    percentValue = 0;  
                }else{
                    percentValue = parseFloat(additionalCost) * (parseFloat(modeRate) / 100)
                }
                    initialAdvance = percentValue;
                    endAdvance = parseFloat(additionalCost) - parseFloat(percentValue);
            } else if (advanceMode == 2) {
                var flatValue = parseFloat(additionalCost) - parseFloat(modeRate);
                initialAdvance = parseFloat(modeRate);
                endAdvance = parseFloat(flatValue);
            } else if (advanceMode == 3) {                
                initialAdvance = 0;
                endAdvance = parseFloat(additionalCost);            
                document.getElementById("modeRateE" + sno).value = 0;
            }
            else {
                //alert("Please Select advance mode");
                document.getElementById("initialAdvanceE" + sno).value = 0;
                document.getElementById("endAdvanceE" + sno).value = 0;
                document.getElementById("modeRateE" + sno).value = 0;
                return false;
            }

            document.getElementById("initialAdvanceE" + sno).value = initialAdvance;
            document.getElementById("endAdvanceE" + sno).value = endAdvance;
        }
    }
    

</script>

                                            <script>
                                                function callOriginAjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'originNameFullTruck' + val;
                                                    var pointId = 'originIdFullTruck' + val;
                                                    var desPointName = 'destinationNameFullTruck' + val;


                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + desPointName).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }

                                                function callPointId1AjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'pointName1' + val;
                                                    var pointId = 'pointId1' + val;
                                                    var point2Name = 'pointName2' + val;


                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + point2Name).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }
                                                function callPointId2AjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'pointName2' + val;
                                                    var pointId = 'pointId2' + val;
                                                    var point3Name = 'pointName3' + val;


                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + point3Name).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }
                                                function callPointId3AjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
//                                            alert(val);
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'pointName3' + val;
                                                    var pointId = 'pointId3' + val;
                                                    var point4Name = 'pointName4' + val;


                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + point4Name).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }
                                                function callPointId4AjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //       var pointNameId = 'originNameFullTruck' + val;
                                                    var pointNameId = 'pointName4' + val;
                                                    var pointId = 'pointId4' + val;
                                                    var desPointName = 'destinationNameFullTruck' + val;


                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityFromList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    //                                                    cityName: request.term,
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    //                                                    cityName: $("#" + pointNameId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                    //alert("asdfasdf")
                                                                },
                                                                error: function(data, type) {
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Value;
                                                            //alert("value ="+value);
                                                            var temp = value.split("-");
                                                            var textId = temp[0];
                                                            var textName = temp[1];
                                                            var id = ui.item.Id;
                                                            //alert("id ="+id);
                                                            //alert(id+" : "+value);
                                                            $('#' + pointId).val(textId);
                                                            $('#' + pointNameId).val(textName);
                                                            $('#' + desPointName).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        //var temp [] = "";
                                                        var itemVal = item.Value;
                                                        var temp = itemVal.split("-");
                                                        var textId = temp[0];
                                                        var t2 = temp[1];
                                                        //                                        alert("t1 = "+t1)
                                                        //                                        alert("t2 = "+t2)
                                                        //alert("itemVal = "+itemVal)
                                                        t2 = '<font color="green">' + t2 + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + t2 + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }
                                                //
                                                //
                                                //                                }
                                                function callDestinationAjaxFullTruck(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    var pointNameId = 'destinationNameFullTruck' + val;
                                                    var pointIdId = 'destinationIdFullTruck' + val;
                                                    var originPointId = 'originIdFullTruck' + val;
                                                    var pointId1 = 'pointId1' + val;
                                                    var pointId2 = 'pointId2' + val;
                                                    var pointId3 = 'pointId3' + val;
                                                    var pointId4 = 'pointId4' + val;
                                                    var truckRouteId = 'routeIdFullTruck' + val;
                                                    var travelKm = 'travelKmFullTruck' + val;
                                                    var travelHour = 'travelHourFullTruck' + val;
                                                    var travelMinute = 'travelMinuteFullTruck' + val;


                                                    //                                    if($("#" + originPointId).val() == ""){
                                                    //                                        alert("id inside condition ==")
                                                    //                                    }else{
                                                    //                                        alert("else inside condition ==")
                                                    //                                    }
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getCityToList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    cityName: $("#" + pointNameId).val(),
                                                                    originCityId: $("#" + originPointId).val(),
                                                                    pointId1: $("#" + pointId1).val(),
                                                                    pointId2: $("#" + pointId2).val(),
                                                                    pointId3: $("#" + pointId3).val(),
                                                                    pointId4: $("#" + pointId4).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                },
                                                                error: function(data, type) {

                                                                    //console.log(type);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.cityName;
                                                            var id = ui.item.cityId;
                                                            //                                            alert(id+" : "+value);
                                                            $('#' + pointIdId).val(id);
                                                            $('#' + pointNameId).val(value);
                                                            $('#' + travelKm).val(ui.item.Distance);
                                                            $('#' + travelHour).val(ui.item.TravelHour);
                                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                                            $('#vehicleTypeId' + val).val(0);
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        var itemVal = item.cityName;
                                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + itemVal + "</a>")
                                                                .appendTo(ul);
                                                    };
                                                }
                                            </script>

                                        </div>

                                        <br>
                                        <br>
                                        <center>
                                            <a><input type="button" class="btn btn-info" value="Add Route" id="AddRouteFullTruck" name="AddRouteFullTruck" onclick="addRouteFullTruck()" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>

                                            <a><input type="button" class="btn btn-info btnPrevious" value="Previous" name="Previous" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                                        </center>

                            </c:if>
                        </div>


                        <div id="deD"  style="padding-left: 1px;display:none" class="tab-pane">
                            <div id="addDedicate"  style="padding-left: 1px;">
                                <table  style="display:none;border-spacing: 40px 10px;height:25px; " cellspacing="10" id="dedicateDetails" border="1">
                                    <c:if test="${fuelHike != null}">
                                        <c:forEach items="${fuelHike}" var="fuelHike">
                                            <tr><td style="padding-left:10px;padding-right:10px;color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:25px;">Agreed Fuel Price</td>
                                                <td style="margin-left:-200px;">
                                                    <input type="text"  style="width:90px;height:25px;" class="textbox" name="agreedFuelPrice" id="agreedFuelPrice" value="<c:out value="${fuelHike.agreedFuelPrice}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                <td style="width:3px;">
                                                    <select type="text" name="uom" id="uom" style="width:120px;height:25px;" class="textbox">
                                                        <option value="0">--Select--</option>
                                                        <option value="1">Litre</option>
                                                        <option value="2">Gallon</option>
                                                        <option value="3">Kilogram</option>
                                                    </select>
                                                    <script>
                                                        document.getElementById("uom").value =<c:out value="${fuelHike.uom}"/>;
                                                    </script>
                                                </td>
                                                <td style="padding-left:10px;padding-right:10px;color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;height:25px;">Fuel Hike % for Revising Cost</td>
                                                <td><input type="text"  name="hikeFuelPrice" class="textbox" id="hikeFuelPrice" value="<c:out value="${fuelHike.fuelHikePercentage}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" style="width:120px;height:25px;"/></td>
                                            </tr>
                                        </c:forEach>
                                    </c:if>
                                </table>
                                <br>

                                <c:if test="${dedicateList != null}">
                                    <table align="center" border="1" id="mainTableFullDedicate" class="table table-info mb30" style="width:70%" >
                                        <thead height="30">
                                            <tr id="tableDesingTH" height="30">
                                                <th >S.No</th>
                                                <th >Vehicle Type</th>
                                                <th >Vehicle Units</th>
                                                <!--                                <th >Trailer Type</th>
                                                                                <th >Trailer Units</th>-->
                                                <th >Contract Category</th>
                                                <th >Fixed Cost Per Vehicle & Month</th>
                                                <th colspan="2" >Fixed Duration Per day<br>&emsp14;
                                        <table border="0" class="table table-info mb30" >
                                            <tr  >
                                                <td width="250px;"><center>Hours</center></td>
                                            <td width="250px;"><center>Minutes</center></td>
                                            </tr>
                                        </table>
                                        </th>
                                        <th >Total Fixed Cost Per Month</th>
                                        <th >Rate Per KM</th>
                                        <th >Rate Exceeds Limit KM</th>
                                        <th >Max Allowable KM Per Month</th>
                                        <!--                            <table  class="contenthead" id="routeDetails1"  border="1">
        
                                                                    </table>-->

                                        <th  colspan="2">Over Time Cost Per HR<br>&emsp14;<table  border="0"><tr  id="tableDesingTD"><td width="250px;"><center>Work Days</center></td><td width="250px;"><center>Holidays</center></td></tr></table></th>
                                        <th >Additional Cost</th>
                                        <th >Active Status</th>
                                        <th >Vehicle & Trailer Details</th>

                                        </tr>
                                        </thead>
                                        <tbody>
                                            <% int index1 = 1;%>
                                            <c:forEach items="${dedicateList}" var="weight">
                                                <%
                                                    String classText1 = "";
                                                    int oddEven1 = index1 % 2;
                                                    if (oddEven1 > 0) {
                                                        classText1 = "text2";
                                                    } else {
                                                        classText1 = "text1";
                                                    }
                                                %>
                                                <tr height="25">
                                                    <td class="<%=classText1%>"  ><%=index1++%></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.vehicleTypeIdDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.vehicleUnitsDedicate}"/></td>
        <!--                                            <td class="<%=classText1%>"   ><c:out value="${weight.trailorTypeDedicate}"/></td>
                                                    <td class="<%=classText1%>"   ><c:out value="${weight.trailorUnitsDedicate}"/></td>-->
                                                    <td class="<%=classText1%>"   >
                                                        <c:if test="${weight.contractCategory == '1'}" >
                                                            Fixed
                                                        </c:if>
                                                        <c:if test="${weight.contractCategory == '2'}" >
                                                            Actual
                                                        </c:if>
                                                        <input type="hidden" name="category" id="category<%=index1%>" value="<c:out value="${weight.contractCategory}"/>"/>
                                                        </select></td>
                                                    <td class="<%=classText1%>"   >
                                                        <input type="hidden" name="total" id="total<%=index1%>" value="<c:out value="${weight.vehicleUnitsDedicate}"/>"/>
                                                        <input type="hidden" name="contractDedicateId" id="contractDedicateId<%=index1%>"  value="<c:out value="${weight.contractDedicateId}"/>"/>
                                                        <input type="text" name="fixedCostE" id="fixedCostE<%=index1%>" onKeyPress="return onKeyPressBlockCharacters(event);" value="<c:out value="${weight.fixedCost}"/>" onchange="calculateTotalFixedCost1(<%=index1%>, this.value);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="fixedHrsE" id="fixedHrsE<%=index1%>" value="<c:out value="${weight.fixedHrs}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="fixedMinE" id="fixedMinE<%=index1%>" value="<c:out value="${weight.fixedMin}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="totalFixedCostE" id="totalFixedCostE<%=index1%>" value="<c:out value="${weight.totalFixedCost}"/>"  onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="rateCostE" id="rateCostE<%=index1%>" value="<c:out value="${weight.rateCost}"/>"  onclick="setTextBoxEdit('<%=index1%>');" onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="rateLimitE" id="rateLimitE<%=index1%>" value="<c:out value="${weight.rateLimit}"/>"  onclick="setTextBoxEdit('<%=index1%>')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="maxAllowableKME" id="maxAllowableKME<%=index1%>" value="<c:out value="${weight.maxAllowableKM}"/>" onclick="setTextBoxEdit('<%=index1%>')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="workingDaysE" id="workingDaysE<%=index1%>" value="<c:out value="${weight.workingDays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="holidaysE" id="holidaysE<%=index1%>" value="<c:out value="${weight.holidays}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   ><input type="text" name="addCostDedicateE" id="addCostDedicateE<%=index1%>" value="<c:out value="${weight.addCostDedicate}"/>" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>
                                                    <td class="<%=classText1%>"   >
                                                        <select name="activeIndE" id="activeIndE<%=index1%>" >
                                                            <c:if test="${weight.activeInd == 'Y'}" >
                                                                <option value="Y" selected>Active</option>
                                                                <option value="N">In-Active</option>
                                                            </c:if>
                                                            <c:if test="${weight.activeInd == 'N'}" >
                                                                <option value="N" selected>In-Active</option>
                                                                <option value="Y" >Active</option>
                                                            </c:if>
                                                        </select></td>
                                                    <td class="<%=classText1%>"   >

                                                        <c:if test="${weight.existingVehicleUnit == 0 }" >
                                                            <a href="/throttle/configureVehicleTrailerPage.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>&contractDedicateId=<c:out value="${weight.contractDedicateId}"/>&trailorTypeIdDedicate=<c:out value="${weight.trailorTypeIdDedicate}"/>" >Configure</a>

                                                        </c:if>
                                                        <c:if test="${weight.existingVehicleUnit > 0 }" >
                                                            <a href="/throttle/viewVehicleTrailerContract.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeIdDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>&contractDedicateId=<c:out value="${weight.contractDedicateId}"/>&trailorTypeIdDedicate=<c:out value="${weight.trailorTypeIdDedicate}"/>" >View</a>
                                                        </c:if>
                                                        <c:if test="${weight.existingVehicleUnit > 0}" >
                                                            <a href="/throttle/editVehicleTrailerContract.do?vendorId=<c:out value="${vendorId}"/>&vehicleTypeId=<c:out value="${weight.vehicleTypeId}"/>&vehicleTypeIdDedicate=<c:out value="${weight.vehicleTypeIdDedicate}"/>&vehicleUnitsDedicate=<c:out value="${weight.vehicleUnitsDedicate}"/>&trailorTypeDedicate=<c:out value="${weight.trailorTypeDedicate}"/>&trailorUnitsDedicate=<c:out value="${weight.trailorUnitsDedicate}"/>&contractDedicateId=<c:out value="${weight.contractDedicateId}"/>&trailorTypeIdDedicate=<c:out value="${weight.trailorTypeIdDedicate}"/>" >Edit</a>
                                                        </c:if>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                    <br/>

                                    <script>
                                        //                              function addNewRowDedicate1() {
                                        //                                alert("Hi Iam Here");
                                        //                            }
                                        var containerDedicate = "";
                                        function addNewRowDedicate() {
                                            //                                alert("Hi Iam Here");
                                            $('#AddNewRowFullTruck').hide();
                                            var iCnt = <%=index1%>;
                                            var rowCnt = <%=index1%>;
                                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                                            containerDedicate = $($("#addDedicate")).css({
                                                padding: '5px', margin: '20px', width: '100%', border: '0px dashed',
                                                borderTopColor: '#999', borderBottomColor: '#999',
                                                borderLeftColor: '#999', borderRightColor: '#999'
                                            });
                                            //                                    alert(iCnt);
                                            $(containerDedicate).last().after('\
                                           <table align="center"  id="routeDetails1' + iCnt + '"  border="1" class="table table-info mb30 table-hover" style="width:90%;">\n\
                                            <thead >\n\
                                            <tr id="tableDesingTH" height="30">\n\
                                            <td>Sno</td>\n\
                                            <td>Vehicle Type</td>\n\
                                            <td>Vehicle Units</td>\n\
                                             <td>Contract Category</td>\n\
                                           <td>Fixed Cost Per Vehicle & Month</td>\n\
                                            <td colspan="2"><br>Fixed Duration <br>Per day<br>\n\
                                            <table  border="0">\n\
                                              <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"><td width="235px;">Hours</td><td width="206px;">Minutes</td></tr></table></td>\n\
                                            <td>Total Fixed Cost Per Month</td>\n\
                                            <td>Rate Per KM</td>\n\
                                            <td>Rate Exceeds Limit KM</td>\n\
                                            <td>Max Allowable KM Per Month</td>\n\
                                            <td colspan="2"><br><br>Over Time Cost Per HR<br><br>\n\
                                            <table  border="0">\n\
                                              <tr style="color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;"><td width="245px;">Work Days</td><td width="216px;height="20px;">Holidays</td></tr></table></td>\n\
                                            <td>Additional Cost</td></tr>\n\
                                             <tr>\n\
                                           <td> ' + iCnt + ' </td>\n\
                                           <td><select type="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + iCnt + '" onchange="onSelectVal(this.value,' + iCnt + ');" ><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                           <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:80px;"/></td>\n\
                                            <td><select ype="text" name="contractCategory" id="contractCategory' + iCnt + '" onchange="setTextBoxEnable(this.value,' + iCnt + ');" style="height:22px;width:80px;"><option value="0">--Select--</option>\n\
                                                <option value="1" >Fixed</option>\n\
                                                 <option value="2" >Actual</option></select></td>\n\
                                           <td><input type="text" name="fixedCost" id="fixedCost' + iCnt + '" value="0" onchange="calculateTotalFixedCost(' + iCnt + ')" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                           <td><select name="fixedHrs" id="fixedHrs' + iCnt + '" class="textbox" style="height:22px;width:45px;">\n\
                                                      <option value="00">00</option>\n\
                                                    <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                           </td>\n\
                                            <td><select ype="text" name="fixedMin" id="fixedMin' + iCnt + '" class="textbox" style="height:22px;width:45px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                           </td>\n\
                                            <td><input type="text" name="totalFixedCost" id="totalFixedCost' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                            <td><input type="text" name="rateCost" id="rateCost' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="rateLimit" id="rateLimit' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="maxAllowableKM" id="maxAllowableKM' + iCnt + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                            <td><input type="text" name="workingDays" id="workingDays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="holidays" id="holidays' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                            <td><input type="text" name="addCostDedicate" id="addCostDedicate' + iCnt + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                             </tr>\n\
                                            </table>\n\
                                           <br><table border="0" id="buttonDetails1' + iCnt + '" ><tr>\n\
                                            <td>\n\
                                            <input class="btn btn-info" type="button" name="addRouteDetailsFullTruck1" id="addRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Add" onclick="addRows(' + iCnt + ')" style="width:90px;height:30px;font-weight: bold;padding: 1px;">\n\
                                            <input class="btn btn-info" type="button" name="removeRouteDetailsFullTruck1" id="removeRouteDetailsFullTruck1' + iCnt + rowCnt + '" value="Remove"  onclick="deleteRows(' + iCnt + ')" style="width:90px;height:30px;font-weight: bold;padding: 1px;">\n\
                                       </tr></thead></table></td></tr></table>');
                                        }
                                            </script>
                                            <script>
                                                var routeInfoSizeSub = 0;
                                                function addRows(val) {
                                                    //                                            alert(val);
                                                    var loadCnt = val;
                                                    //    alert(loadCnt)
                                                    //alert("loadCnt");
                                                    //var loadCnt1 = ve;
                                                    var routeInfoSize = $('#routeDetails1' + loadCnt + ' tr').size();
                                                    //                                            alert(routeInfoSize);
                                                    if (val == 2) {
                                                        routeInfoSizeSub = routeInfoSize - 1;
                                                    } else if (val == 4) {
                                                        routeInfoSizeSub = routeInfoSize + 1;
                                                    } else {
                                                        routeInfoSizeSub = routeInfoSize;
                                                    }
                                                    //                                            alert("routeInfoSizeSub" + routeInfoSizeSub);
                                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                                    $('#routeDetails1' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSizeSub + '</td>\n\
                                        <%--   <td><input type="text" name="vehicleTypeIddeDTemp" id="vehicleTypeIddeDTemp' + loadCnt + '" /></td>--%>\n\
                                                   <td><select ype="text" name="vehicleTypeIdDedicate" id="vehicleTypeIdDedicate' + routeInfoSizeSub + '" onchange="onSelectVal(this.value,' + routeInfoSizeSub + ');" ><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="vehType"><option value="<c:out value="${vehType.typeId}"/>"><c:out value="${vehType.typeName}"/></option></c:forEach></c:if></select></td>\n\
                                                   <td><input type="text" name="vehicleUnitsDedicate" id="vehicleUnitsDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:80px;"/></td>\n\
                                                   <td><select ype="text" name="contractCategory" id="contractCategory' + routeInfoSizeSub + '"  onchange="setTextBoxEnable(this.value,' + routeInfoSizeSub + ');" style="height:22px;width:80px;"><option value="">--Select--</option>\n\
                                                    <option value="1" >Fixed</option>\n\
                                                    <option value="2" >Actual</option></select></td>\n\
                                                   <td><input type="text" name="fixedCost" id="fixedCost' + routeInfoSizeSub + '" onchange="calculateTotalFixedCost(' + routeInfoSizeSub + ')" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                                   <td><select type="text" name="fixedHrs" id="fixedHrs' + routeInfoSizeSub + '" class="textbox" style="height:22px;width:45px;">\n\
                                                              <option value="00">00</option>\n\
                                                            <option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option></select>\n\
                                                   </td>\n\
                                                    <td><select type="text" name="fixedMin" id="fixedMin' + routeInfoSizeSub + '" class="textbox" style="height:22px;width:45px;"><option value="00">00</option><option value="01">01</option><option value="02">02</option><option value="03">03</option><option value="04">04</option><option value="05">05</option><option value="06">06</option><option value="07">07</option><option value="08">08</option><option value="09">09</option><option value="10">10</option><option value="11">11</option><option value="12">12</option><option value="13">13</option><option value="14">14</option><option value="15">15</option><option value="16">16</option><option value="17">17</option><option value="18">18</option><option value="19">19</option><option value="20">20</option><option value="21">21</option><option value="22">22</option><option value="23">23</option><option value="24">24</option><option value="25">25</option><option value="26">26</option><option value="27">27</option><option value="28">28</option><option value="29">29</option><option value="30">30</option><option value="31">31</option><option value="32">32</option><option value="33">33</option><option value="34">34</option><option value="35">35</option><option value="36">36</option><option value="37">37</option><option value="38">38</option><option value="39">39</option><option value="40">40</option><option value="41">41</option><option value="42">42</option><option value="43">43</option><option value="44">44</option><option value="45">45</option><option value="46">46</option><option value="47">47</option><option value="48">48</option><option value="49">49</option><option value="50">50</option><option value="51">51</option><option value="52">52</option><option value="53">53</option><option value="54">54</option><option value="55">55</option><option value="56">56</option><option value="57">57</option><option value="58">58</option><option value="59">59</option></select>\n\
                                                   </td>\n\
                                                    <td><input type="text" name="totalFixedCost" id="totalFixedCost' + routeInfoSizeSub + '" value="0" /></td>\n\
                                                    <td><input type="text" name="rateCost" id="rateCost' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);"readonly /></td>\n\
                                                    <td><input type="text" name="rateLimit" id="rateLimit' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                                    <td><input type="text" name="maxAllowableKM" id="maxAllowableKM"' + routeInfoSizeSub + '" value="0"  onKeyPress="return onKeyPressBlockCharacters(event);" readonly /></td>\n\
                                                    <td><input type="text" name="workingDays" id="workingDays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                                    <td><input type="text" name="holidays" id="holidays' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" style="height:22px;width:50px;"/></td>\n\
                                                    <td><input type="text" name="addCostDedicate" id="addCostDedicate' + routeInfoSizeSub + '" value="0" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                                                  </tr>');
                                                    // alert("loadCnt = "+loadCnt)
                                                    loadCnt++;
                                                    //   alert("loadCnt = +"+loadCnt)
                                                }
                                                function onSelectVal(val, countVal) {
                                                    //                            alert("vehicleTypeIdDedicate"+val);
                                                    document.getElementById("vehicleTypeIddeDTemp" + countVal).value = val;
                                                }


                                                function deleteRows(val) {
                                                    var loadCnt = val;
                                                    //     alert(loadCnt);
                                                    var addRouteDetails = "addRouteDetailsFullTruck1" + loadCnt;
                                                    var routeInfoDetails = "routeDetails1" + loadCnt;
                                                    // alert(routeInfoDetails);
                                                    if ($('#routeDetails1' + loadCnt + ' tr').size() > 4) {
                                                        $('#routeDetails1' + loadCnt + ' tr').last().remove();
                                                        loadCnt = loadCnt - 1;
                                                    } else {
                                                        //                                               alert('One row should be present in table');
                                                        $('#dedicateDetails' + loadCnt).remove();
                                                        $('#routeDetails1' + loadCnt).remove();
                                                        $('#buttonDetails1' + loadCnt).remove();
                                                        $('#AddNewRowFullTruck').show();
                                                    }
                                                }
                                                function calculateTotalFixedCost(sno) {
                                                    //        alert(sno);
                                                    var vehicleUnits = document.getElementById("vehicleUnitsDedicate" + sno).value;
                                                    var fixedCost = document.getElementById("fixedCost" + sno).value;
                                                    var totalCost = parseInt(vehicleUnits) * parseInt(fixedCost);
                                                    document.getElementById("totalFixedCost" + sno).value = totalCost;
                                                }


                                                function calculateTotalFixedCost1(sno1) {
                                                    //     alert("dfgfgdfg"+sno1);
                                                    var vehicleUnits1 = document.getElementById("total" + sno1).value;

                                                    var fixedCostE = document.getElementById("fixedCostE" + sno1).value;
                                                    // alert("fixedCostE"+fixedCostE);
                                                    var total = parseInt(vehicleUnits1) * parseInt(fixedCostE);
                                                    // alert(total)
                                                    // document.getElementByName("total" ).value = total;
                                                    document.getElementById("totalFixedCostE" + sno1).value = total;
                                                }



                                                function setTextBoxEnable(val, count) {
                                                    //                                    alert("enterd----"+val);
                                                    //                                alert(count);

                                                    if (val == 1) {
                                                        //                                                       alert(count);
                                                        document.getElementById("rateCost" + count).readOnly = true;
                                                        document.getElementById("rateLimit" + count).readOnly = false;
                                                        document.getElementById("maxAllowableKM" + count).readOnly = false;
                                                    }
                                                    if (val == 2) {

                                                        document.getElementById("rateCost" + count).readOnly = false;
                                                        document.getElementById("rateLimit" + count).readOnly = true;
                                                        document.getElementById("maxAllowableKM" + count).readOnly = true;

                                                    }
                                                }
                                                function setTextBoxEdit(v2) {
                                                    //                                alert("enterd----"+v1);
                                                    //                                alert("enterd----"+v2);
                                                    //  alert(count);
                                                    var val = document.getElementById("category" + v2).value
                                                    //  alert(val)
                                                    if (val == 1) {
                                                        // alert("fin0")
                                                        document.getElementById("rateCostE " + v2).readOnly = true;
                                                        // alert("fin1")

                                                        document.getElementById("rateLimitE" + v2).readOnly = false;
                                                        //                                                      alert("fin2")
                                                        document.getElementById("maxAllowableKME" + v2).readOnly = false;
                                                        //                                                    alert("fin3")
                                                    }
                                                    if (val == 2) {

                                                        document.getElementById("rateCostE" + v2).readOnly = false;
                                                        document.getElementById("rateLimitE" + v2).readOnly = true;
                                                        document.getElementById("maxAllowableKME" + v2).readOnly = true;

                                                    }
                                                }
                                            </script>

                                            <script>
                                                function callOriginAjaxdeD(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    var pointNameId = 'originNamedeD' + val;
                                                    var pointIdId = 'originIddeD' + val;
                                                    var desPointName = 'destinationNamedeD' + val;


                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getTruckCityList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    cityName: request.term,
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                },
                                                                error: function(data, type) {

                                                                    //console.log(type);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Name;
                                                            var id = ui.item.Id;
                                                            //alert(id+" : "+value);
                                                            $('#' + pointIdId).val(id);
                                                            $('#' + pointNameId).val(value);
                                                            $('#' + desPointName).focus();
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        var itemVal = item.Name;
                                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + itemVal + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }

                                                function callDestinationAjaxdeD(val) {
                                                    // Use the .autocomplete() method to compile the list based on input from user
                                                    //alert(val);
                                                    var pointNameId = 'destinationNamedeD' + val;
                                                    var pointIdId = 'destinationIddeD' + val;
                                                    var originPointId = 'originIddeD' + val;
                                                    var truckRouteId = 'routeIddeD' + val;
                                                    var travelKm = 'travelKmdeD' + val;
                                                    var travelHour = 'travelHourdeD' + val;
                                                    var travelMinute = 'travelMinutedeD' + val;

                                                    //alert(prevPointId);
                                                    $('#' + pointNameId).autocomplete({
                                                        source: function(request, response) {
                                                            $.ajax({
                                                                url: "/throttle/getTruckCityList.do",
                                                                dataType: "json",
                                                                data: {
                                                                    cityName: request.term,
                                                                    originCityId: $("#" + originPointId).val(),
                                                                    textBox: 1
                                                                },
                                                                success: function(data, textStatus, jqXHR) {
                                                                    var items = data;
                                                                    response(items);
                                                                },
                                                                error: function(data, type) {

                                                                    //console.log(type);
                                                                }
                                                            });
                                                        },
                                                        minLength: 1,
                                                        select: function(event, ui) {
                                                            var value = ui.item.Name;
                                                            var id = ui.item.Id;
                                                            //alert(id+" : "+value);
                                                            $('#' + pointIdId).val(id);
                                                            $('#' + pointNameId).val(value);
                                                            $('#' + travelKm).val(ui.item.TravelKm);
                                                            $('#' + travelHour).val(ui.item.TravelHour);
                                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                                            //validateRoute(val,value);

                                                            return false;
                                                        }

                                                        // Format the list menu output of the autocomplete
                                                    }).data("ui-autocomplete")._renderItem = function(ul, item) {
                                                        //alert(item);
                                                        var itemVal = item.Name;
                                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                                        return $("<li></li>")
                                                                .data("item.autocomplete", item)
                                                                .append("<a>" + itemVal + "</a>")
                                                                .appendTo(ul);
                                                    };


                                                }

                                                function setContainerQtyList(val, sno) {
//                                                    alert($('#vehicleTypeId' + sno).val());
                                                    if (val == '1') {
                                                        // 20FT vehicle
                                                        $('#containerQty' + sno).empty();
                                                        if($('#vehicleTypeId' + sno).val() == '1058'){
                                                            $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))
                                                        }else{
                                                            $('#containerQty' + sno).append($('<option ></option>').val(2).html('2'))
                                                        }
                                                    } else if (val == '2') {
                                                        // 40FT vehicle
                                                        $('#containerQty' + sno).empty();
                                                        $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))

                                                    } else {
                                                        $('#containerQty' + sno).empty();
                                                        $('#containerQty' + sno).append($('<option ></option>').val(0).html('0'))
                                                    }
                                                }

                                                function setContainerTypeList(val, sno) {
                                                    if (val == '1058') {
                                                        // 20FT vehicle herererere
                                                        $('#containerTypeId' + sno).empty();
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(1).html('20'))
                                                        $('#containerQty' + sno).empty();
                                                        $('#containerQty' + sno).append($('<option ></option>').val(1).html('1'))
                                                    } else if (val == '1059') {
                                                        // 40FT vehicle
                                                        $('#containerTypeId' + sno).empty();
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(0).html('--Select--'))
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(1).html('20'))
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(2).html('40'))
                                                        $('#containerQty' + sno).empty();

                                                    } else {
                                                        $('#containerTypeId' + sno).empty();
                                                        $('#containerTypeId' + sno).append($('<option ></option>').val(4).html('0'));
                                                        $('#containerQty' + sno).empty();
                                                    }
                                                }


                                                function getMarketHireRateHidden(sno, tableRow) {

                                                    contractExists(sno, tableRow);
                                                    var vehicleTypeId = $("#vehicleTypeId" + sno).val();
                                                    var containerTypeId = $("#containerTypeId" + sno).val();
                                                    var containerQty = $("#containerQty" + sno).val();
                                                    var loadTypeId = $("#loadTypeId" + sno).val();
                                                    var originIdFullTruck = $("#originIdFullTruck" + tableRow).val();
                                                    var pointId1 = $("#pointId1" + tableRow).val();
                                                    var pointId2 = $("#pointId2" + tableRow).val();
                                                    var pointId3 = $("#pointId3" + tableRow).val();
                                                    var pointId4 = $("#pointId4" + tableRow).val();
                                                    var destinationIdFullTruck = $("#destinationIdFullTruck" + tableRow).val();
//                                    alert("vehicleTypeId"+vehicleTypeId);
//                                    alert("containerTypeId"+containerTypeId);
//                                    alert("containerQty"+containerQty);
//                                    alert("loadTypeId"+loadTypeId);
//                                    alert("originIdFullTruck"+originIdFullTruck);
//                                    alert("pointId1"+pointId1);
//                                    alert("destinationIdFullTruck"+destinationIdFullTruck);

                                                    if (loadTypeId != '0' && loadTypeId != '' && vehicleTypeId != '0' && containerTypeId != '0' && vehicleTypeId != '' && containerTypeId != '' && containerQty != ''
                                                            && originIdFullTruck != '' && destinationIdFullTruck != '') {
                                                        $.ajax({
                                                            url: "/throttle/getMarketHireRate.do",
                                                            dataType: "json",
                                                            data: {
                                                                vehicleTypeId: vehicleTypeId,
                                                                containerTypeId: containerTypeId,
                                                                containerQty: containerQty,
                                                                originIdFullTruck: originIdFullTruck,
                                                                pointId1: pointId1,
                                                                pointId2: pointId2,
                                                                pointId3: pointId3,
                                                                pointId4: pointId4,
                                                                destinationIdFullTruck: destinationIdFullTruck,
                                                                loadTypeId: loadTypeId
                                                            },
                                                            success: function(data, textStatus, jqXHR) {
                                                                if (data.MarketHireRate == '0') {
                                                                    $("#statusSpan").text("Route not defined in organisation");
                                                                    $("#marketRate" + sno).val(0);
                                                                    $("#marketRateSpan" + sno).text(0);
                                                                    $("#vehicleTypeId" + sno).val(0);
                                                                    $("#containerTypeId" + sno).val(0);
                                                                    $("#containerQty" + sno).val(0);
                                                                } else if (data.MarketHireRate != '') {
                                                                    $("#statusSpan").text("");
                                                                    $("#marketRate" + sno).val(data.MarketHireRate);
                                                                    $("#marketRateSpan" + sno).text(data.MarketHireRate);
                                                                }
                                                            },
                                                            error: function(data, type) {
                                                            }
                                                        });
                                                    }

                                                }


                                                function contractExists(sno, tableRow) {

//                                                    alert("im hereexists");
                                                    var vehicleTypeId = $("#vehicleTypeId" + sno).val();
                                                    var containerTypeId = $("#containerTypeId" + sno).val();
                                                    var containerQty = $("#containerQty" + sno).val();
                                                    var loadTypeId = $("#loadTypeId" + sno).val();
                                                    var originIdFullTruck = $("#originIdFullTruck" + tableRow).val();
                                                    var pointId1 = $("#pointId1" + tableRow).val();
                                                    var pointId2 = $("#pointId2" + tableRow).val();
                                                    var pointId3 = $("#pointId3" + tableRow).val();
                                                    var pointId4 = $("#pointId4" + tableRow).val();
                                                    var destinationIdFullTruck = $("#destinationIdFullTruck" + tableRow).val();



                                                    var vehicleTypeIdE = document.getElementsByName("vehicleTypeIdEdit");
                                                    var containerTypeIdE = document.getElementsByName("containerTypeIdEdit");
                                                    var containerQtyE = document.getElementsByName("containerQtyEdit");
                                                    var loadTypeIdE = document.getElementsByName("loadTypeEdit");
                                                    var originIdFullTruckE = document.getElementsByName("originIdFullTruckEdit");
                                                    var pointId1E = document.getElementsByName("point1IdEdit");
                                                    var pointId2E = document.getElementsByName("point2IdEdit");
                                                    var pointId3E = document.getElementsByName("point3IdEdit");
                                                    var pointId4E = document.getElementsByName("point4IdEdit");
                                                    var destinationIdFullTruckE = document.getElementsByName("destinationIdFullTruckEdit");

                                                    if (vehicleTypeIdE.length > 0) {
                                                        for (var i = 0; i < vehicleTypeIdE.length; i++) {
                                                            if ((vehicleTypeIdE[i].value == vehicleTypeId) && (loadTypeIdE[i].value == loadTypeId) && (containerTypeIdE[i].value == containerTypeId) && (containerQtyE[i].value == containerQty) && (originIdFullTruckE[i].value == originIdFullTruck) && (pointId1E[i].value == pointId1) && (pointId2E[i].value == pointId2) && (pointId3E[i].value == pointId3) && (pointId4E[i].value == pointId4) && (destinationIdFullTruckE[i].value == destinationIdFullTruck)) {
                                                                alert("This Contract Details Already Exists in row " + [i + 1]);

                                                                $("#vehicleTypeId" + sno).val(0);
                                                                $("#loadTypeId" + sno).val(0);
                                                                $("#containerTypeId" + sno).val(0);
                                                                $("#containerQty" + sno).val(0);
                                                            }
//                                                    alert("vehicleTypeId" + vehicleTypeIdE);
//                                                    alert("containerTypeId" + containerTypeIdE);
//                                                    alert("containerQty" + containerQtyE);
//                                                    alert("loadTypeId" + loadTypeIdE);
//                                                    alert("originIdFullTruck" + originIdFullTruckE);
//                                                    alert("pointId1" + pointId1E);
//                                                    alert("pointId2" + pointId2E);
//                                                    alert("pointId3" + pointId3E);
//                                                    alert("pointId4" + pointId4E);
//                                                    alert("destinationIdFullTruck" + destinationIdFullTruckE);
                                                        }
                                                    }
                                                }

                                                function checkMarketRateHidden(val, sno) {
                                                    var spotCost = $("#spotCost" + sno).val();
                                                    var additionalCost = $("#additionalCost" + sno).val();
                                                    var marketRate = $("#marketRate" + sno).val();
                                                    var sts = false;
                                                    if (parseFloat(spotCost) > parseFloat(marketRate)) {
                                                        sts = true;
                                                    }
                                                    if (parseFloat(additionalCost) > parseFloat(marketRate)) {
                                                        sts = true;
                                                    }
                                                    if (sts) {
                                                        $("#approvalStatusSpan" + sno).text("Needs Approval");
                                                        $("#approvalStatusSpan" + sno).removeClass("noErrorClass");
                                                        $("#approvalStatusSpan" + sno).addClass("errorClass");
                                                    } else {
                                                        $("#approvalStatusSpan" + sno).text("Auto Approval");
                                                        $("#approvalStatusSpan" + sno).removeClass("errorClass");
                                                        $("#approvalStatusSpan" + sno).addClass("noErrorClass");
                                                    }
                                                }
                                            </script>



                                </c:if>
                                <!--</div>-->

                            </div>

                            <br>
                            <center>
                                <a href="#"><input type="button" class="btn btn-info" value="AddNewRow" id="AddNewRowFullTruck" name="addRowDedicate" onclick="addNewRowDedicate();" style="width:90px;height:30px;font-weight: bold;padding:1px;"/></a>
                                <a><input type="button" class="btn btn-info btnNext" value="Next" name="Next" style="width:70px;height:30px;font-weight: bold;padding:1px;"/></a>
                            </center>

                        </div>
                        <br>
                        <center>
                            <input type="button" class="btn btn-info" value="Submit" onclick="submitPage();" style="width:150px;height:30px;font-weight: bold;padding:1px;"/>

                        </center>
                        <br>
                        <br>
                        <br>
                    </div>
                    <script>
                        $('.btnNext').click(function() {
                            $('.nav-tabs > .active').next('li').find('a').trigger('click');
                        });
                        $('.btnPrevious').click(function() {
                            $('.nav-tabs > .active').prev('li').find('a').trigger('click');
                        });
                    </script>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>