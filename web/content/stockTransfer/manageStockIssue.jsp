<%-- 
    Document   : manageVendor
    Created on : Mar 8, 2009, 10:51:13 AM
    Author     : karudaiyar Subramaniam
--%>



<%@ include file="../common/NewDesign/header.jsp" %>
	<%@ include file="../common/NewDesign/sidemenu.jsp" %> 

<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript">
        function submitPage(value)
        {
            if (value=='add')
                {
                    
                    document.vendorDetail.action ='/throttle/addVendorPage.do';
                }else if(value == 'modify'){
                
                document.vendorDetail.action ='/throttle/';
            }
            document.vendorDetail.submit();
        }
    </script>
    
    <div class="pageheader">
		            <h2><i class="fa fa-edit"></i> <spring:message code="hrms.label.Transfer Stock" text="Transfer Stock"/> </h2>
		            <div class="breadcrumb-wrapper">
		                <span class="label"><spring:message code="head.label.Youarehere" text="Youarehere"/></span>
		                <ol class="breadcrumb">
		                    <li><a href="index.html"><spring:message code="head.label.Home" text="Home"/></a></li>
		                    <li><a href="general-forms.html"><spring:message code="hrms.label.Stock Transfer" text="Stock Transfer"/></a></li>
		                    <li class=""><spring:message code="hrms.label.Transfer Stock" text="Transfer Stock"/></li>
		
		                </ol>
		            </div>
       			</div>
        
             <div class="contentpanel">
             <div class="panel panel-default">
             <div class="panel-body">
    
    
    <body>
        
        <form method="post" name="vendorDetail">
           
<%--<%@ include file="/content/common/path.jsp" %>--%>
                    

<%@ include file="/content/common/message.jsp" %>

 <% int index = 0;  %>    
            <br>
            <c:if test = "${ApprovedStockList != null}" >
                <!--<table width="550" align="center" cellpadding="0" cellspacing="0" id="bg" class="border">-->
                    <table class="table table-info mb30 table-hover" id="bg" >
                <thead>
                    <tr align="center">
                        <th>S.No</td>
                        <th>Request No</th>
                        <th>Service Point</th>
                        <th>Required date</th>
                        <th>Status</th>
                        <th>Issue</th>
                         
                    </tr>
                    </thead>
                    <%

                    %>
                    
                    <c:forEach items="${ApprovedStockList}" var="list"> 
                    
                        <%

            String classText = "";
            int oddEven = index % 2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>         
                        <tr  width="208" height="40" > 
                            <td class="<%=classText %>" align="left" height="30"><%=index + 1%></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${list.requestId}"/></td>
                            <td class="<%=classText %>" align="left" height="30"><c:out value="${list.servicePointName}"/></td>
                             <td class="<%=classText %>" align="left" height="30"><c:out value="${list.requiredDate}"/></td>
                              <td class="<%=classText %>" align="left" height="30"><c:out value="${list.status}"/></td>
                              <c:if test = "${list.reqType == 'NEW'}" >
                             <td class="<%=classText %>" align="left"><a href="/throttle/issueItemPage.do?requestId=<c:out value='${list.requestId}'/>" >Issue</a> </td>
                             </c:if>
                              <c:if test = "${list.reqType == 'New'}" >
                             <td class="<%=classText %>" align="left"><a href="/throttle/issueItemPage.do?requestId=<c:out value='${list.requestId}'/>" >Issue</a> </td>
                             </c:if>
                             <c:if test = "${list.reqType == 'RC'}">
                             <td class="<%=classText %>" align="left"><a href="/throttle/rcIssue.do?requestId=<c:out value='${list.requestId}'/>" >Issue</a> </td>
                             </c:if>
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
            </c:if> 
            
            <br>
        <%@ include file="../common/NewDesign/commonParameters.jsp" %></form>
    </body>
 </div>
            </div>
            </div>
            <%@ include file="../common/NewDesign/settings.jsp" %>

