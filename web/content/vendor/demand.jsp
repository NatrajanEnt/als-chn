<%--
    Document   : contractPointToPointWeight
    Created on : Jan 27, 2015, 8:35:48 PM
    Author     : Nivan
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="/throttle/js/jquery.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-ui.min.js"></script>




<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!--<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function () {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<html>
    <body>

        <style>
            body {
                font:13px verdana;
                font-weight:normal;
            }
        </style>



        <form name="vehicleVendorContract" method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp" %>
            <br>



            <div id="tabs">
                <ul class="">
                    <li><a href="#deD"><span>DEDICATED</span></a></li>
                    <li><a href="#d"><span>ON DEMAND</span></a></li>
                    <!--<li><a href="#weightBreak"><span>Weight Break </span></a></li>-->
                </ul>

<div id="deD">


                    <script>
                        var contain = "";
                        $(document).ready(function () {
                            var iCnt = 1;
                            var rowCnt = 1;
                            // CREATE A "DIV" ELEMENT AND DESIGN IT USING JQUERY ".css()" CLASS.
                            contain = $($("#routedeD")).css({
                                padding: '5px', margin: '20px', width: '170px', border: '0px dashed',
                                borderTopColor: '#999', borderBottomColor: '#999',
                                borderLeftColor: '#999', borderRightColor: '#999'
                            });

                            callOriginAjaxdeD(iCnt);
                            callDestinationAjaxdeD(iCnt);
                            $('#btAdd').click(function () {
                                iCnt = iCnt + 1;
                                $(contain).last().after('<table id="mainTabledeD" ><tr><td>\\n\
 <table class="contentsub" id="routeInfoDetailsdeD' + iCnt + rowCnt + '" border="1" width="100%">\n\
                     <tr><td><h3>Agreed Fuel Price</h3></td><td><input type="text" name="rateWithReeferdeD" id="rateWithReeferdeD' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" />\n\\n\
<input type="text" name="rateWithReeferdeD" id="rateWithReeferdeD' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\\n\
<td><h3>Fuel Hike % for Revising Cost</h3></td><td><input type="text" name="rateWithOutReeferdeD" id="rateWithOutReeferdeD' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                          </tr></table>\n\
                            <table  class="contenthead" id="routeDetailsdeD' + iCnt + '" border="1" width="100%">\n\
                            <tr><td>Sno</td>\n\
                            <td>Vehicle Type</td>\n\
                            <td>Units</td>\n\
                            <td>Trailer Type</td>\n\
                            <td>Trailer Units</td>\n\
                            <td>Fixed Cost per month</td></tr>\n\
                            <td>Fixed Duration per day</td></tr>\n\
                            <td>Variable Cost per km</td></tr>\n\
                            <td>Max Allowable km per month</td></tr>\n\
                            <td>Slab Limit Mode</td></tr>\n\
                            <td>Slab Limit km</td></tr>\n\
                            <td>Over Time Cost per hr</td></tr>\n\
                            <td>Additional Cost</td></tr>\n\
                            <td>Vehicle & Trailer Details</td></tr>\n\
                            <td>Route-' + iCnt  + '</td>\n\
                           </tr>\n\
                            </table>\n\
                            <table class="contentsub" id="routeInfoDetailsdeD' + iCnt + rowCnt + '" border="1" width="100%">\n\
                            <tr><td>Sno</td><td>Vehicle Type</td><td>Spot Cost Per Trip</td><td>Additional Cost</td></tr>\n\
                            <tr>\n\
                            <td>' + rowCnt + '</td>\n\
                         <td><select ype="text" name="vehicleTypeIddeD" id="vehicleTypeIddeD' + iCnt + '"><option value="0">--Select--</option><c:if test="${TypeList != null}"><c:forEach items="${TypeList}" var="Type"><option value="<c:out value="${Type.vehicleTypeId}"/>"><c:out value="${Type.vehicleTypeName}"/></option></c:forEach></c:if></select></td>\n\
                            <td><input type="text" name="rateWithReeferdeD" id="rateWithReeferdeD' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                            <td><input type="text" name="rateWithOutReeferdeD" id="rateWithOutReeferdeD' + iCnt + '" value="" onKeyPress="return onKeyPressBlockCharacters(event);" /></td>\n\
                           </tr></table>\n\\n\
                              <table border="1" width="100%"><tr>\n\
                            <td><input class="button"  type="button" name="addRouteDetailsdeD" id="addRouteDetailsdeD' + iCnt + rowCnt + '" value="Add" onclick="addRow(' + iCnt + rowCnt + ')" />\n\
                            <input class="button"  type="button" name="removeRouteDetailsdeD" id="removeRouteDetailsdeD' + iCnt + rowCnt + '" value="Remove" onclick="deleteRow(' + iCnt + rowCnt + ')"  /></td>\n\
                            </tr></table></td></tr></table><br><br>');
                                callOriginAjaxdeD(iCnt);
                                callDestinationAjaxdeD(iCnt);
                                $('#maindeD').after(contain);
                            });
                            $('#btRemove').click(function () {
                                alert($('#mainTabledeD tr').size());
                                if ($(contain).size() > 1) {
                                    $(contain).last().remove();
                                    iCnt = iCnt - 1;
                                }
                            });
                        });

                        // PICK THE VALUES FROM EACH TEXTBOX WHEN "SUBMIT" BUTTON IS CLICKED.
                        var divValue, values = '';
                        function GetTextValue() {
                            $(divValue).empty();
                            $(divValue).remove();
                            values = '';
                            $('.input').each(function () {
                                divValue = $(document.createElement('div')).css({
                                    padding: '5px', width: '200px'
                                });
                                values += this.value + '<br />'
                            });
                            $(divValue).append('<p><b>Your selected values</b></p>' + values);
                            $('body').append(divValue);
                        }

                        function addRow(val) {
                            //alert(val);
                            var loadCnt = val;
                            var routeInfoSize = $('#routeInfoDetailsdeD' + loadCnt + ' tr').size();
                            var addRouteDetails = "addRouteDetailsdeD" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsdeD" + loadCnt;
                            $('#routeInfoDetailsdeD' + loadCnt + ' tr').last().after('<tr><td>' + routeInfoSize + '</td><td><select ype="text" name="vehicleTypeIddeD" id="vehicleTypeIddeD' + loadCnt + '"><option value="0">--Select--</option><c:if test="${vehicleTypeList != null}"><c:forEach items="${vehicleTypeList}" var="vehType"><option value="<c:out value="${vehType.vehicleTypeId}"/>"><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach></c:if></select></td><td><input type="text" name="rateWithReeferdeD" id="rateWithReeferdeD' + loadCnt + '" value=""/></td><td><input type="text" name="rateWithOutReeferdeD" id="rateWithOutReeferdeD' + loadCnt + '" value="" /></td></tr>');
                            loadCnt++;
                        }
                        function deleteRow(val) {
                            var loadCnt = val;
                            var addRouteDetails = "addRouteDetailsdeD" + loadCnt;
                            var routeInfoDetails = "routeInfoDetailsdeD" + loadCnt;
                            if ($('#routeInfoDetailsdeD' + loadCnt + ' tr').size() > 2) {
                                $('#routeInfoDetailsdeD' + loadCnt + ' tr').last().remove();
                                loadCnt = loadCnt - 1;
                            } else {
                                alert('One row should be present in table');
                            }
                        }
                            </script>

                            <script>
                                function callOriginAjaxdeD(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'originNamedeD' + val;
                                    var pointIdId = 'originIddeD' + val;
                                    var desPointName = 'destinationNamedeD' + val;


                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function (request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    textBox: 1
                                                },
                                                success: function (data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function (data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function (event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + desPointName).focus();
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function (ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }

                                function callDestinationAjaxdeD(val) {
                                    // Use the .autocomplete() method to compile the list based on input from user
                                    //alert(val);
                                    var pointNameId = 'destinationNamedeD' + val;
                                    var pointIdId = 'destinationIddeD' + val;
                                    var originPointId = 'originIddeD' + val;
                                    var truckRouteId = 'routeIddeD' + val;
                                    var travelKm = 'travelKmdeD' + val;
                                    var travelHour = 'travelHourdeD' + val;
                                    var travelMinute = 'travelMinutedeD' + val;

                                    //alert(prevPointId);
                                    $('#' + pointNameId).autocomplete({
                                        source: function (request, response) {
                                            $.ajax({
                                                url: "/throttle/getTruckCityList.do",
                                                dataType: "json",
                                                data: {
                                                    cityName: request.term,
                                                    originCityId: $("#" + originPointId).val(),
                                                    textBox: 1
                                                },
                                                success: function (data, textStatus, jqXHR) {
                                                    var items = data;
                                                    response(items);
                                                },
                                                error: function (data, type) {

                                                    //console.log(type);
                                                }
                                            });
                                        },
                                        minLength: 1,
                                        select: function (event, ui) {
                                            var value = ui.item.Name;
                                            var id = ui.item.Id;
                                            //alert(id+" : "+value);
                                            $('#' + pointIdId).val(id);
                                            $('#' + pointNameId).val(value);
                                            $('#' + travelKm).val(ui.item.TravelKm);
                                            $('#' + travelHour).val(ui.item.TravelHour);
                                            $('#' + travelMinute).val(ui.item.TravelMinute);
                                            $('#' + truckRouteId).val(ui.item.RouteId);
                                            //validateRoute(val,value);

                                            return false;
                                        }

                                        // Format the list menu output of the autocomplete
                                    }).data("autocomplete")._renderItem = function (ul, item) {
                                        //alert(item);
                                        var itemVal = item.Name;
                                        itemVal = '<font color="green">' + itemVal + '</font>';
                                        return $("<li></li>")
                                                .data("item.autocomplete", item)
                                                .append("<a>" + itemVal + "</a>")
                                                .appendTo(ul);
                                    };


                                }
                            </script>

                            <div id="routedeD">

                            </div>
                            <a  href="#"><input type="button" class="button" value="Save" onclick="saveVendor();" /></a>
                        </div>

                <script>
                    function saveVendor(){
                        document.customerContract.action = "/throttle/saveVehicleVendorContract.do";
                        document.customerContract.submit();
                    }
                 </script>


            </div>

        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>
