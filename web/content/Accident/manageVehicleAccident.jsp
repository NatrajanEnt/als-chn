<%--
    Document   : manageVehicleAccident
    Created on : 21 Sep, 2012, 12:54:03 PM
    Author     : Hari
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%@ page import="ets.domain.util.ThrottleConstants" %>
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>

<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<script language="javascript" src="/throttle/js/validate.js"></script>  
<%@ page import="java.util.*" %>      
<%@ page import="java.http.*" %>      
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>   
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                //alert('hai');
                $( "#datepicker" ).datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });



            });

            $(function() {
                //	alert("cv");
                $( ".datepicker" ).datepicker({

                    /*altField: "#alternate",
                                altFormat: "DD, d MM, yy"*/
                    changeMonth: true,changeYear: true
                });

            });
        </script>
    </head>
    <style type="text/css">
        .blink {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#CC3333;
        }
        .blink1 {
            font-family:Tahoma;
            font-size:11px;
            color:#333333;
            padding-left:10px;
            background-color:#F2F2F2;
        }
    </style>
    <script type="text/javascript" language="javascript">
        $(document).ready(function() {
            $("#tabs").tabs();
        });
    </script>

    <script>
        function show_src() {
            document.getElementById('exp_table').style.display='none';
        }
        function show_exp() {
            document.getElementById('exp_table').style.display='block';
        }
        function show_close() {
            document.getElementById('exp_table').style.display='none';
        }

        function submitPage(value)
        {
            var regno =document.viewVehicle.regno.value;
            document.viewVehicle.action="/throttle/viewVehicleAccidentDetails.do?regno="+regno;
            document.viewVehicle.submit();
        }


        function nextPage(){
            document.viewVehicle.action="/throttle/handleVehicleAccidentDetail.do";
            document.viewVehicle.submit();
        }

        //function getBunkName(){
        //  var oTextbox = new AutoSuggestControl(document.getElementById("bunkName"),new ListSuggestions("driName","/throttle/handleDriverSettlement.do?"));

        //}
        // window.onload = getVehicleNos;




    </script>
    <body onLoad="getVehicleNos();setImages(1,0,0,0,0,0);setDefaultVals('<%= request.getAttribute("regNo")%>','<%= request.getAttribute("typeId")%>','<%= request.getAttribute("mfrId")%>','<%= request.getAttribute("usageId")%>','<%= request.getAttribute("groupId")%>');">
        <form name="viewVehicle" method="post">
            <%@ include file="/content/common/path.jsp" %>
            <%@ include file="/content/common/message.jsp"%>
            <table class="table table-bordered" id="report" >
                <tr><td colspan="7" align="center"><h2 style="margin:0px;padding:0px;padding-left:20px;font-size: 14px; font-weight: bold;">
                        </h2></td>
                    <td align="right"><div style="height:17px;margin-top:0px;"><img src="images/icon_report.png" alt="Show Search" onclick="show_exp();" class="arrow" />&nbsp;<img src="images/icon_close.jpg" alt="Close" onClick="show_close();"  /></div></td>
                </tr>
                <tr id="exp_table" >
                    <td colspan="8" bgcolor="#97caff" style="padding:10px;" align="left">
                        <div class="tabs" align="left" style="width:850;">
                            <ul class="tabNavigation">
                                <li style="background:#76b3f1">Vehicle Accident Details </li>
                            </ul>
                            <div id="first">
                                <table width="90%" cellpadding="0" cellspacing="2" border="0" align="center" class="table4">
                                    <tr>
                                        <td><font color="red">*</font>Vehicle Number</td>
                                        <td height="30">
                                            <input name="regno" id="regno" type="text" class="form-control" size="20" value="" >
                                        </td>
                                        <td><input type="button" class="button"  onclick="submitPage(0);" value="Search"></td>
                                    </tr>
                                </table>
                            </div></div>
                    </td>
                </tr>
                <br>
                <% int flag = 0;
                            int index = 0;
                %>
                <c:if test="${accidentVehicleList != null}">
                   <table class="table table-info mb30 table-hover" id="table">
                        <thead>
                           
                        <th >S.No</th>
                        <th >Reg.No</th>
                        <th >Accident Spot</th>
                        <th >Accident Date</th>
                        <th >Driver Name</th>
                        <th >Police Station</th>
                        <th >FIR No</th>
                        <th >FIR Date</th>
                        <th >FIR Prepared By</th>
                        <th >&nbsp;</th>
                        </thead>
                   
                        <tbody>
                    <%
                                String style = "text1";%>
                    <c:forEach items="${accidentVehicleList}" var="avl">
                        <%
                                    if ((index % 2) == 0) {
                                        style = "text1";
                                    } else {
                                        style = "text2";
                                     }%>
                        <tr>
                            <td class="<%= style%>"><%=index + 1%></td>
                            <td class="<%= style%>"><c:out value="${avl.regno}"/></td>
                            <td class="<%= style%>"><c:out value="${avl.accidentSpot}"/></td>
                            <td class="<%= style%>"><c:out value="${avl.accidentDate}"/></td>
                            <td class="<%= style%>"><c:out value="${avl.driverName}"/></td>
                            <td class="<%= style%>"><c:out value="${avl.policeStation}"/></td>
                            <td class="<%= style%>"><c:out value="${avl.firNo}"/></td>
                            <td class="<%= style%>"><c:out value="${avl.firDate}"/></td>
                            <td class="<%= style%>"><c:out value="${avl.firPreparedBy}"/></td>
                            <td class="<%= style%>"><a href="/throttle/alterVehicleAccidentDetails.do?regNo=<c:out value="${avl.regno}"/>"> Edit  </a>
                            </td>
                        </tr>
                        <%
                                    index++;
                        %>
                    </c:forEach>
                        </table>
                    <tr><td colspan="8" >
                            <table width="830" cellpadding="0" cellspacing="2" border="0" align="center">
                                <tr>
                                    <td>&nbsp;</td>
                                    <!--                    <td align="center"><input type="button" class="button"  onclick="nextPage(0);" value="Add"></td>-->
                                </tr>
                            </table></td></tr>
                        </c:if>

            </table>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
    <script>

        function submitPage(value){

            if(value == 'search' || value == 'Prev' || value == 'Next' || value == 'GoTo' || value =='First' || value =='Last'){
                if(value=='GoTo'){
                    var temp=document.viewVehicle.GoTo.value;
                    document.viewVehicle.pageNo.value=temp;
                    document.viewVehicle.button.value=value;
                    document.viewVehicle.action = '/throttle/handleVehicleAccident.do';
                    document.viewVehicle.submit();
                }else if(value == "First"){
                    temp ="1";
                    document.viewVehicle.pageNo.value = temp;
                    value='GoTo';
                }else if(value == "Last"){
                    temp =document.viewVehicle.last.value;
                    document.viewVehicle.pageNo.value = temp;
                    value='GoTo';
                }
                //document.viewVehicleDetails.button.value=value;
                document.viewVehicle.action = '/throttle/handleVehicleAccident.do';
                document.viewVehicle.submit();

            }else if(value == 'add'){
                document.viewVehicle.action = '/throttle/handleVehicleAccidentDetail.do';
                document.viewVehicle.submit();
               
            }else{
                document.viewVehicle.action='/throttle/handleVehicleAccident.do'
                document.viewVehicle.submit();
            }
        }
        function setDefaultVals(regNo,typeId,mfrId,usageId,groupId){
            if( regNo!='null'){
                document.viewVehicle.regNo.value=regNo;
            }
            if( typeId!='null'){
                document.viewVehicle.typeId.value=typeId;
            }
            if( mfrId!='null'){
                document.viewVehicle.mfrId.value=mfrId;
            }
            if( usageId!='null'){
                document.viewVehicle.usageId.value=usageId;
            }
            if( groupId!='null'){
                document.viewVehicle.groupId.value=groupId;
            }
        }


        function getVehicleNos(){
            var oTextbox = new AutoSuggestControl(document.getElementById("regno"),new ListSuggestions("regno","/throttle/handleVehicleNo.do?"));
        }
    </script>
</div>
    </div>
    </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
