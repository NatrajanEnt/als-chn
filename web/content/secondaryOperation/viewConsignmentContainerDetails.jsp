
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>


<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>


<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>

<script type="text/javascript" language="javascript">
    $(document).ready(function () {
        $("#tabs").tabs();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function () {
        $(".datepicker").datepicker({
            changeMonth: true, changeYear: true
        });
    });
</script>
<script type="text/javascript" language="javascript">
    function submitPage() {
        if (document.getElementById('consignmentStatusId').value == '0') {
            alert("please select status");
            document.getElementById('consignmentStatusId').focus();
        } else if (document.getElementById('cancelRemarks').value == '') {
            alert("please enter the remarks");
            document.getElementById('cancelRemarks').focus();
        } else {
            document.cNote.action = '/throttle/cancelConsignment.do';
            document.cNote.submit();
        }
    }

    function viewTripDetails(tripId, vehicleId, tripType) {
        document.cNote.action = '/throttle/viewTripSheets.do?tripSheetId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId;
        document.cNote.submit();
//            window.open('/throttle/viewTripSheetDetails.do?tripId=' + tripId + '&tripType=' + tripType, '&vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }

</script>
<script language="">
    function print(val)
    {
        var DocumentContainer = document.getElementById(val);
        var WindowObject = window.open('', "TrackHistoryData",
                "width=740,height=500,top=200,left=250,toolbars=no,scrollbars=yes,status=no,resizable=no");
        WindowObject.document.writeln(DocumentContainer.innerHTML);
        WindowObject.document.close();
        WindowObject.focus();
        WindowObject.print();
        WindowObject.close();
    }

    function viewCurrentLocation(vehicleId) {
        window.open('/throttle/viewVehicleLocation.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
//        var url = ' http://dict.throttletms.com/api/vehicleonmap/map.php?uname=interem&group_id=5087&height=600&width=700&vehicle=' + vehicleRegNo;
//alert(url);
        window.open(url, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
    }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> <spring:message code="subMenus.label.PrimaryOperation22"  text="Trip Sheet"/> </h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html"><spring:message code="subMenus.label.PrimaryOperation"  text="PrimaryOperation"/></a></li>
            <li class="active"><c:out value="${menuPath}"/></li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">

            <body>
                <br/>
                <br/>
                <br/>
                <form name="cNote" method="post">
                    <c:if test="${consignmentContainerList != null}">
                        <table class="table table-info mb30 table-hover" id="table" >
                            <thead >
                                <tr>
                                    <th height="30" >S.No</th>
                                    <th height="30" >Booking No / BOE</th>
                                    <th height="30" >Vehicle Type</th>
                                    <th height="30" >Container Type</th>
                                    <th height="30" >Container No</th>
                                    <th height="30" >Seal No</th>
                                    <th height="30" >Liner Name</th>
                                    <th height="30" >Planned Status</th>
                                    <th height="30" >Trip Code</th>
                                    <th height="30" >Vehicle No</th>
                                    <th height="30" >Driver</th>
                                    <th height="30" >Trip Status</th>
                                    <th height="30" >Current Location</th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int index = 0;
                                    int sno = 1;
                                %>
                                <c:forEach items="${consignmentContainerList}" var="cnoteContainer">
                                    <tr>
                                        <td class="text1" height="30" ><%=sno%></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.orderReferenceNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.vehicleTypeName}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.containerTypeName}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.containerNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.sealNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.linerName}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.plannedStatus}"/></td>
                                        <td class="text1" height="30" >
                                            <c:out value="${cnoteContainer.tripCode}"/>
                                            <!--<a href="#" onclick="viewTripDetails('<c:out value="${cnoteContainer.tripSheetId}"/>', '<c:out value="${cnoteContainer.vehicleId}"/>', '1', 0);"><c:out value="${cnoteContainer.tripCode}"/></a>-->
                                        </td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.vehicleRegNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.driverName}"/><br><c:out value="${cnoteContainer.mobileNo}"/></td>
                                        <td class="text1" height="30" ><c:out value="${cnoteContainer.tripStatus}"/></td>
                                        <td class="text1"  height="30">
                                            <c:if test="${cnoteContainer.gpsLocation == 'NA'}">
                                                NA
                                            </c:if>
                                            <c:if test="${cnoteContainer.gpsLocation != 'NA'}">
                                                <c:out value="${cnoteContainer.gpsLocation}" />
                                                <!--<a href="#" onClick="viewCurrentLocation('<c:out value="${cnoteContainer.vehicleId}" />');"><c:out value="${cnoteContainer.gpsLocation}" /></a>--> 
                                            </c:if>
                                        </td>
                                    </tr>
                                    <%index++;%>
                                    <%sno++;%>
                                </c:forEach>
                            </tbody>
                        </c:if>
                    </table>
                    <br>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" selected="selected">5</option>
                                <option value="10">10</option>
                                <option value="20">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 1);
                    </script>





                </form>
            </body>
        </div>
    </div>
</div>

<%@ include file="/content/common/NewDesign/settings.jsp" %>