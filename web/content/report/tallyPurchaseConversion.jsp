<%-- 
    Document   : tripPlanningImport
    Created on : Nov 4, 2013, 11:17:44 PM
    Author     : Arul
--%>

<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>


<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<!DOCTYPE html>
<!--<html>-->
<head>
    <!--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">-->
    <link  href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
    <title></title>
</head>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
 <style>
        .outer {
            width: 600px;
            color: navy;
            background-color: #1caf9a;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer1 {
            width: 600px;
            color: navy;
            background-color: #428bca;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer2 {
            width: 600px;
            color: navy;
            background-color: #a94442;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer3 {
            width: 600px;
            color: navy;
            background-color: #f0ad4e;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .outer4 {
            width: 1210px;
            color: navy;
            background-color: pink;
            border: 2px solid darkblue;
            padding: 5px;
        }
        .ben{
            width: 600px;
        }
    </style>

<script type="text/javascript">
    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#cityFrom').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityFromName.do",
                    dataType: "json",
                    data: {
                        cityFrom: request.term,
                        cityToId: document.getElementById('cityToId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityFromId').val(tmp[0]);
                $('#cityFrom').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };

        $('#cityTo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getCityToName.do",
                    dataType: "json",
                    data: {
                        cityTo: request.term,
                        cityFromId: document.getElementById('cityFromId').value
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#cityToId').val(tmp[0]);
                $('#cityTo').val(tmp[1]);
                checkRouteCode();
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        }

    });

</script>

<script>
    function convertToTally(tripNo, tranNo) {
//        alert(tripNo)
        var r = confirm("Are you sure to proceed...");
        if (r == true){
        document.PNRchart.action = '/throttle/saveTallyPurchaseConversion.do?tripNo=' + tripNo ;
        document.PNRchart.submit();
//        window.open('/throttle/saveTallySalesConversion.do?tripNo=' + tripNo + '&tranNo=' + tranNo, 'PopupPage', 'height = 800, width = 1400, scrollbars = yes, resizable = yes');
        }
    }
    
    function onKeyPressBlockNumbers(e)
    {
        var key = window.event ? e.keyCode : e.which;
        var keychar = String.fromCharCode(key);
        reg = /\d/;
        return !reg.test(keychar);
    }

    function submitPage(value) {
//        alert("alert=" + document.getElementById("vehicleTypeIdDistance").value);
        document.PNRchart.action = '/throttle/saveTallyHeads.do?value=1';
        document.PNRchart.submit();

    }

    function checkValue(value, id) {
        if (value == '' && id == 'cityFrom') {
            $('#cityTo').attr('readonly', true);
            $('#routeCode').attr('readonly', true);
            document.getElementById('cityFromId').value = '';
        }
        if (value == '' && id == 'cityTo') {
            $('#cityFrom').attr('readonly', true);
            $('#routeCode').attr('readonly', true);
            document.getElementById('cityToId').value = '';
        }
        if (value == '' && id == 'routeCode') {
            $('#cityFrom').attr('readonly', true);
            $('#cityTo').attr('readonly', true);
            document.getElementById('routeId').value = '';
        }
    }
</script>



<!--<style>
  progress { background-color: red; border: 1px solid black; }
  progress::-moz-progress-bar { background-color: green; }
</style>-->
<body onload="vendorChart();" >
    <div class="pageheader">
        <h2><i class="fa fa-home"></i> Tally Heads
        </h2>
        <div class="breadcrumb-wrapper">
            <span class="label">You are here:</span>
            <ol class="breadcrumb">
                <li><a href="index.html">Home</a></li>
                <li>Tally Heads</li>
            </ol>
        </div>
    </div>

    <div class="contentpanel">
        <div class="panel panel-default">
            <div class="panel-body">
                <form name="PNRchart" method="post">
                    <%@ include file="/content/common/message.jsp" %>
                    <br>
                    <div id="pnr">
                        <center><b>Tally Purchase Conversion</b></center>
                        <br>
                        <table class="table table-info mb30 table-hover" style="width:55%;" align="center">
                            
                            <tr style="display:none">
                                <td><font color="red">*</font>Vehicle Type </td>
                                <td>
                                    <c:if test="${vehicleTypeList != null}">
                                        <select  name="vehicleTypeIdDistance" id="vehicleTypeIdDistance"  class="form-control" style="width:240px;height:40px;" ><option value="0">-Select-</option><c:forEach items="${vehicleTypeList}" var="vehType">
                                                <option value='<c:out value="${vehType.vehicleTypeId}"/>'><c:out value="${vehType.vehicleTypeName}"/></option></c:forEach>
                                            </select>
                                    </c:if> 
                                </td>
                                <td><font color="red">*</font>From Location</td>
                                <td><input type="hidden" name="cityFromId" id="cityFromId" value="<c:out value="${cityId}"/>" class="form-control" style="width:240px;height:40px;"><input type="text" name="cityFrom" id="cityFrom" value="<c:out value="${cityName}"/>" class="form-control" style="width:240px;height:40px;" onchange="checkValue(this.value, this.id)"></td>
                                <td><font color="red">*</font>To Location</td>
                                <td><input type="hidden" name="cityToId" id="cityToId" value="<c:out value="${cityId}"/>" class="form-control" style="width:240px;height:40px;"><input type="text" name="cityTo" id="cityTo" value="<c:out value="${cityName}"/>" class="form-control" style="width:240px;height:40px;" onchange="checkValue(this.value, this.id)"></td>
                            </tr>
                            <td style="display: none">Transporter</td>
                            <td style="display:none">
                                <select name="vendorId" id="vendorId"  class="form-control" style="width:240px;height:40px;">
                                    <c:if test="${vendorList != null}">
                                        <option value="0" selected>--select--</option>
                                        <c:forEach items="${vendorList}" var="vehNo">
                                            <option value='<c:out value="${vehNo.vendorId}"/>'><c:out value="${vehNo.vendorName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select>
                            </td>

                            <tr style="display:none">
                                <td colspan="6" align="center">
                                    <input type="button" class="btn btn-info" name="Search" value="<spring:message code="sales.label.customer.search"  text="default text"/>" onClick="submitPage('search');" style="width:100px;height:35px;">&nbsp;&nbsp;
                            </tr>
                        </table>

                            <c:if test = "${tallyPurchaceList != null}" >
                            <table align="center" class="table table-info mb30 table-hover" id="table" >
                                <thead>
                                    <tr>
                                        <th>Sno</th>
                                        <th>GRN No</th>
                                        <th>Date</th>
                                        <th>Transaction</th>
                                        <th>Item Name</th>
                                        <th>UOM</th>
                                        <th>Quantity</th>
                                        <th>Item Rate</th>
                                        <th>HSN Code</th>
                                        <th>Value</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <% int index = 0, sno = 1;%>
                                    <c:forEach items="${tallyPurchaceList}" var="customer">
                                        <tr>
                                            <td><%=sno%></td>
                                            <td>
                                                <input type="hidden" name="tripNo" id="tripNo<%=sno%>" value="<c:out value="${customer.tripNos}"/>">
                                                <!--<input type="hidden" name="tranNo" id="tranNo<%=sno%>" value="<c:out value="${customer.tranNo}"/>">-->
                                                <c:out value="${customer.tripNos}"/> </td>
                                            <td> <c:out value="${customer.trnDate}"/> </td>
                                            <td> <c:out value="${customer.transactionType}"/></td>
                                            <td> <c:out value="${customer.itemName}"/> </td>
                                            <td> <c:out value="${customer.uomName}"/> </td>
                                            <td> <c:out value="${customer.itemQty}"/> </td>
                                            <td> <c:out value="${customer.itemRate}"/> </td>
                                            <td> <c:out value="${customer.itemHSNSACCode}"/> </td>
                                            <td> <c:out value="${customer.trnValue}"/> </td>
                                            <td> 
                                                <a href="#" onclick="convertToTally('<c:out value="${customer.tripNos}"/>');">Convert</a>
                                            </td>
                                        </tr>
                                        <%
                                            index++;
                                            sno++;
                                        %>
                                    </c:forEach>

                                </tbody>
                            </table>
                                    <center>
                                        <input type="button" class="btn btn-info" name="Save" value="Save" onClick="submitPage('Save');" style="width:100px;height:35px;display:none">&nbsp;&nbsp;
                                    </center>
                                    <br>
                                    <br>
                        </c:if>
                            
                        <script language="javascript" type="text/javascript">
                            setFilterGrid("table");
                        </script>
                        <div id="controls">
                            <div id="perpage">
                                <select onchange="sorter.size(this.value)">
                                    <option value="5" selected="selected">5</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                </select>
                                <span>Entries Per Page</span>
                            </div>
                            <div id="navigation">
                                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                            </div>
                            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                        </div>
                        <script type="text/javascript">
                            var sorter = new TINY.table.sorter("sorter");
                            sorter.head = "head";
                            sorter.asc = "asc";
                            sorter.desc = "desc";
                            sorter.even = "evenrow";
                            sorter.odd = "oddrow";
                            sorter.evensel = "evenselected";
                            sorter.oddsel = "oddselected";
                            sorter.paginate = true;
                            sorter.currentid = "currentpage";
                            sorter.limitid = "pagelimit";
                            sorter.init("table", 1);
                        </script>
                    </div>
                    <br>
                    <div id="va" style="display:none;" >
                    </div>

                </form>
                </body>
            </div>
        </div>
    </div>

    <script>
        <c:if test = "${value == 1}" >
        dayWiseCollection();
        </c:if>
        function dayWiseCollection() {
//            alert("546");
            var vehicleTypeIdDistance = <c:out value="${vehicleType}"/>;
            var cityFromId =<c:out value="${origin}"/>;
            var cityToId = <c:out value="${destination}"/>;
            var vendorId = <c:out value="${vendorId}"/>;
            var time = [];
            var qtyArray = [];
            var amtArray = [];

            $.ajax({
                url: '/throttle/dayWiseCollection.do',
                data: {vehicleTypeIdDistance: vehicleTypeIdDistance,
                    cityFromId: cityFromId,
                    cityToId: cityToId,
                    vendorId: vendorId
                },
                dataType: 'json',
                success: function(data) {
                    $.each(data, function(key, value) {
                        time.push(value.date);
                        qtyArray.push([parseFloat(value.totalCost)]);
                    });


                    $('#container').highcharts({
                        chart: {
                            type: 'spline'
                        },
                        title: {
                            text: 'Transporter Hire Vehicle Price History MTD'
                        },
                        subtitle: {
                            text: ''
                        },
                        xAxis: {
                            type: 'category',
                            categories: time,
                            title: {
                                text: 'Date'
                            }
                        },
                        yAxis: {
                            min: 0,
                            title: {
                                text: 'Amount (INR)'
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        series: [
                            {
                                name: 'Amount (INR)',
                                data: qtyArray,
                                dataLabels: {
                                    enabled: true,
                                    rotation: -90,
                                    color: '#FFFFFF',
                                    align: 'right',
                                    format: '{point.y:f}', // one decimal
                                    y: 10, // 10 pixels down from the top
                                    style: {
                                        fontSize: '13px',
                                        fontFamily: 'Verdana, sans-serif'
                                    }
                                }
                            }]
                    });
                }
            });
        }
        
    </script>
    <script>
        function vendorChart(){
        var vendorId = <c:out value="${vendorId}"/>;
        if(vendorId == 0){
//            alert("hide"+vendorId)
            $("#vendorChart").hide(); 
        }else{
//            alert("show"+vendorId)
            $("#vendorChart").show(); 
        }
    }
    </script>

    <%@ include file="/content/common/NewDesign/settings.jsp" %>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>