<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

<script type="text/javascript">

    function linersubmit() {
//    alert("save name")
        var errStr = "";
//        var nameCheckStatus = $("#linerNameStatus").text();
        if (document.getElementById("linername").value == "") {
            errStr = "Please enter linername.\n";
            alert(errStr);
            document.getElementById("linername").focus();
        } 
//        else if (nameCheckStatus != "") {
//            errStr = "City Name Already Exists.\n";
//            alert(errStr);
//            document.getElementById("linername").focus();
//        }
        if (errStr == "") {
            document.linerMaster.action = "/throttle/insertOrEditLinerMaster.do";
            document.linerMaster.method = "post";
            document.linerMaster.submit();
    }
    }

    function setValues(sno, linerid, linername,activeInd) {
        var count = parseInt(document.getElementById("count").value);
        document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("linerid").value = linerid;
        document.getElementById("linername").value = linername;
        document.getElementById("activeInd").value = activeInd;
    }

//    function onKeyPressBlockNumbers(e)
//    {
//        var key = window.event ? e.keyCode : e.which;
//        var keychar = String.fromCharCode(key);
//        reg = /\d/;
//        return !reg.test(keychar);
//    }
//
//    var httpRequest;
//    function checklinerName() {
//        alert("name check")
//
//        var linername = document.getElementById('linername').value;
//
//        var url = '/throttle/checklinerName.do?linername=' + linername;
//        if (window.ActiveXObject) {
//            httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
//        } else if (window.XMLHttpRequest) {
//            httpRequest = new XMLHttpRequest();
//        }
//        httpRequest.open("GET", url, true);
//        httpRequest.onreadystatechange = function () {
//            processRequest();
//        };
//        httpRequest.send(null);
//
//    }
//
//    function processRequest() {
//         alert("request check")
//        if (httpRequest.readyState == 4) {
//            if (httpRequest.status == 200) {
//                var val = httpRequest.responseText.valueOf();
//                if (val != "" && val != 'null') {
//                    $("#nameStatus").show();
//                    $("#linerNameStatus").text('Please Check Liner Name  :' + val + ' is Already Exists');
//                } else {
//                    $("#nameStatus").hide();
//                    $("#linerNameStatus").text('');
//                }
//            } else {
//                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
//            }
//        }
//    }

</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Master</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Master</a></li>
            <li class="active">Liner Master</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
    <body onload="document.linerMaster.linername.focus();">
        <form name="linerMaster"  method="POST">
            <table class="table table-info mb30 table-hover" style="width:100%">
               <tr height="30"   ><td colSpan="6" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Liner Master</td></tr>
<!--                    <tr>
                        <td class="text1" colspan="4" align="center" style="display: none" id="nameStatus"><label id="linerNameStatus" style="color: red"></label></td>
                    </tr>-->
                    <tr>
                        <td  >&nbsp;&nbsp;<font color="red">*</font>liner Name</td>
                        <td  ><input type="text" name="linername" id="linername" class="form-control" style="width:180px;height:40px;" onkeypress="return onKeyPressBlockNumbers(event);" onchange="checklinerName();" autocomplete="off" maxlength="50"></td>
                        <td  >&nbsp;&nbsp;&nbsp;&nbsp;Status</td>
                        <td  >
                            <select  align="center" class="form-control" style="width:180px;height:40px;" name="activeInd" id="activeInd" >
                                <option value='Y'>Active</option>
                                <option value='N' id="inActive" style="display: none">In-Active</option>
                            </select>
                        </td>
                        <td  >&nbsp;&nbsp;</td>
                        <td  >&nbsp;&nbsp;</td>
                    </tr>
                </table>
                </tr>
                <tr>
                    <td>
                        <br>
                        <center>
                            <input type="button" class="btn btn-info" value="Save" name="Submit" onClick="linersubmit()">
                        </center>
                    </td>
            </table>
            <br>
            <br>
            <h2 align="center">liner List</h2>
            <br>
            <br>
               <table class="table table-info mb30 table-hover" id="table" style="width:80%" align="center">
                    <thead height="30">
                        <tr id="tableDesingTH" height="30">
                        <th> S.No </th>
                        <th> liner Name  </th>
                        <th> status </th>
                        <th> Select </th>
                    </tr>
                </thead>
                <tbody>

                                                
                    <% int sno = 0;%>
                    <c:if test = "${Linerlist != null}">
                        <c:forEach items="${Linerlist}" var="lm">
                            <%
                                sno++;
                                String className = "text1";
                                if ((sno % 1) == 0) {
                                    className = "text1";
                                } else {
                                    className = "text2";
                                }
                            %>

                            <tr>
                                <td align="left"> <%= sno + 1%> </td>
                                <td align="left"> <c:out value="${lm.linername}" /></td>
                                <td align="left"><c:out value="${lm.status}"/></td>
                                <td> <input type="checkbox" id="edit<%=sno%>" onclick="setValues(<%= sno%>, '<c:out value="${lm.linerid}" />', '<c:out value="${lm.linername}" />', '<c:out value="${lm.status}" />');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                    <input type="hidden" name="count" id="count" value="<%=sno%>" />
                </c:if>
            </table>
        <br>
        <br>
        <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)">
                    <option value="5" selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span>Entries Per Page</span>
            </div>
            <div id="navigation">
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.desc = "desc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 1);
        </script>


    </form>
</body>
</div>
            </div>
        </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
