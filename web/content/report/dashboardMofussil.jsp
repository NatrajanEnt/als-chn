<%@ include file="../common/NewDesign/header.jsp" %>
<%@ include file="../common/NewDesign/sidemenu.jsp" %>


<!--<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>

<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>-->

<!-- Data from www.netmarketshare.com. Select Browsers => Desktop share by version. Download as tsv. -->
<pre id="tsv" style="display:none"></pre>


<div class="pageheader">
    <h2><i class="fa fa-home"></i> Dashboard <span>Mofussil DashBoard</span></h2>
    <div class="breadcrumb-wrapper">
        <span class="label">You are here:</span>
        <ol class="breadcrumb">
            <li><a href="index.html">Home</a></li>
            <li>Dashboard</li>
            <li class="active">Mofussil DashBoard</li>
        </ol>
    </div>
</div>

<div class="contentpanel">

    <table>
        <tr>
            <td>
                <div class="col-sm-8 col-md-9">
                    <div class="panel panel-default">

                        <div class="outer1">
                            <div class="panel-btns">
                                <a href="" class="panel-close">&times;</a>
                                <a href="" class="minimize">&minus;</a>
                            </div><!-- panel-btns -->
                            <h5 class="panel-title"><spring:message code="dashboard.label.Mofussil Order MTD"  text="Mofussil Order MTD"/></h5>
                            <div id="containerFleetUtilisation1" style="min-width: 500px; height: 400px; margin: 0 auto"></div>
                        </div>
                        <!--            <div class="panel-body">


                                        <div class="col-sm-8">

                                        </div>
                                  </div>-->
                    </div>
                </div>
            </td>
        </tr>




    </table>

    <table>
        <div class="outer4">
            <!--                    <div class="panel-btns">
                                    <a href="" class="panel-close">&times;</a>
                                    <a href="" class="minimize">&minus;</a>
                                </div> panel-btns -->
            <!--<h5 class="panel-title">Fleet Utilisation</h5>-->
            <div id="barchart1" style="width:100%"></div>
        </div>
        <!--            <div class="panel-body">


                        <div class="col-sm-8">

                        </div>
                  </div>-->
        <!--</div>-->
        <!--</div>-->

    </table>     
    <!-- contentpanel -->





</div><!-- mainpanel -->


<%@ include file="../common/NewDesign/settings.jsp" %>
<script>
    loadTrucknos();
    function loadTrucknos() {
//           alert("ben Here");
        $.ajax({
            url: '/throttle/getDashBoardWorkShop.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    if (value.Name == 'TruckName') {
                        $("#totalTruckNosSpan").text(value.Count);
                    } else if (value.Name == 'TechnicianNo') {
                        $("#totalDriversNosSpan").text(value.Count);
                    }



                });
            }
        });

    }
</script>


<style>
    .outer {
        width: 600px;
        color: navy;
        background-color: #1caf9a;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer1 {
        width: 600px;
        color: navy;
        background-color: #428bca;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer2 {
        width: 600px;
        color: navy;
        background-color: #a94442;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer3 {
        width: 600px;
        color: navy;
        background-color: #f0ad4e;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .outer4 {
        width: 1210px;
        color: navy;
        background-color: pink;
        border: 2px solid darkblue;
        padding: 5px;
    }
    .ben{
        width: 600px;
    }
</style>
<script>

    getTruckJobCardMTD();
    function getTruckJobCardMTD() {
        var x_values = [];
        var x_values_sub = {};
        $.ajax({
            url: '/throttle/getMofussilOrderdMTD.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    x_values_sub['name'] = value.StatusName;
                    x_values_sub['y'] = parseInt(value.Total);
                    x_values.push(x_values_sub);
                    x_values_sub = {};


                });
//                alert(vehicleCountArray)
                $('#containerFleetUtilisation1').highcharts({
                    chart: {
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Mofussil Order MTD'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.y}</b>'
                    },
                    plotOptions: {
                        pie: {
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: true,
                                format: '<b>{point.name}</b>: {point.y}',
                                style: {
                                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                                }
                            }
                        }
                    },
                    series: [{
                            type: 'pie',
                            name: 'Nos',
                            data: x_values
                        }]
                });
            }
        });
    }


    loadMonthlyTripStatus1();
    function loadMonthlyTripStatus1() {
        var vehicleMakeArray = [];
        var vehicleCountArray = [];
        var vehicleCountArray1 = [];
        var vehicleCountArray2 = [];
        var vehicleCountArray3 = [];
        var vehicleCountArray4 = [];
        var vehicleCountArray5 = [];
        $.ajax({
            url: '/throttle/getMovementWiseOrderNos.do',
            dataType: 'json',
            success: function(data) {
                $.each(data, function(key, value) {
                    vehicleMakeArray.push(value.custName);
                    vehicleCountArray.push([parseInt(value.exportFCL)]);
                    vehicleCountArray1.push([parseInt(value.importFCL)]);
                    vehicleCountArray2.push([parseInt(value.importLCL)]);
                    vehicleCountArray3.push([parseInt(value.ICD)]);
                    vehicleCountArray4.push([parseInt(value.PNR)]);
                    vehicleCountArray5.push([parseInt(value.EXBONDING)]);
                });
                $('#barchart1').highcharts({
                    chart: {
                        type: 'bar'
                    },
                    title: {
                        text: 'Customer Based Movement type orders'
                    },
                    subtitle: {
                        text: ''
                    },
                    xAxis: {
                        type: 'category',
                        categories: vehicleMakeArray,
                        
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'No Of Orders'
                        }, labels: {
                            overflow: 'justify'
                        }
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'top',
                        x: -40,
                        y: 100,
                        floating: true,
                        borderWidth: 1,
                        backgroundColor: (
                                (Highcharts.theme && Highcharts.theme.legendBackgroundColor) ||
                                '#FFFFFF'),
                        shadow: true
                    },
                    tooltip: {
                        pointFormat: 'Order : <b>{point.y:f} Nos</b>'
                    },
                    series: [{
                            name: 'Export',
                            data: vehicleCountArray,
                            
                        }, 
                        {
                            name: 'Import',
                            data: vehicleCountArray1,
                            
                        },
                        {
                            name: 'Vessel Empty',
                            data: vehicleCountArray2,
                            
                        },
                        {
                            name: 'Plot to Plot',
                            data: vehicleCountArray3,
                            
                        },
                        {
                            name: 'Local PNR',
                            data: vehicleCountArray4,
                            
                        },
                        {
                            name: 'Ex-Bonding',
                            data: vehicleCountArray5,
                            
                        }
                    ]
                });
            }
        });
    }
</script>

<!--dataLabels: {
                                enabled: true,
                                rotation: 360,
                                color: '#FFFFFF',
                                align: 'right',
                                format: '{point.y:f}', // one decimal
                                y: 0, // 10 pixels down from the top
                                style: {
                                    fontSize: '13px',
                                    fontFamily: 'Verdana, sans-serif'
                                }
                            }-->
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>





