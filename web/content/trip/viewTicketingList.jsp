<%--
    Document   : viewTripClosureVehicleList
    Created on : Jul 18, 2014, 11:32:08 AM
    Author     : Arul
--%>

<%@page import="ets.domain.operation.business.OperationTO"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });

    function createTicket(){
        document.ticketing.action=" /throttle/createTicket.do";
        document.ticketing.method="post";
        document.ticketing.submit();
    }
</script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>


        <form name="ticketing"  method="post" >
            <%@ include file="/content/common/path.jsp" %>
            <br>
            <%@ include file="/content/common/message.jsp" %>
            <br>
            <br>
            <table width="815" align="center" border="1">
                <tr>
                    <td>Ticket From</td>
                    <td><input type="text" name="fromDate" id="fromDate" class="datepicker"/></td>
                    <td>Ticket To</td>
                    <td><input type="text" name="toDate" id="toDate" class="datepicker"/></td>
                </tr>
                <tr>
                    <td>Ticket Priority</td>
                    <td>
                        <select name="priority" id="priority">
                                <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'Priority'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                                </c:if>
                        </select>
                    </td>
                    <td>Ticket Status</td>
                    <td>
                        <select name="status" id="status">
                            <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'status'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                            </c:if>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Ticket Type</td>
                    <td>
                        <select name="type" id="type">
                            <c:if test="${ticketingStatusList != null}">
                            <c:forEach items="${ticketingStatusList}" var="sts">
                                <c:if test="${sts.type == 'type'}">
                                <option value="<c:out value="${sts.statusId}"/>"><c:out value="${sts.statusName}"/></option>
                                </c:if>
                            </c:forEach>
                            </c:if>
                        </select>
                    </td>
                    <td><input type="button" class="button" name="Search" value="Search"></td>
                    <td><input type="button" class="button" name="Create Ticket" value="Create Ticket" onclick="createTicket()"></td>
                </tr>
            </table>
            <br>
            <table width="815" align="center" border="0" id="table" class="sortable">

                <thead>

                    <tr height="30">
                        <th><h3>S.No</h3></th>
                        <th><h3>Ticket No</h3></th>
                        <th><h3>Title</h3></th>
                        <th><h3>Message</h3></th>
                        <th><h3>Mail From</h3></th>
                        <th><h3>Mail To</h3></th>
                        <th><h3>Mail Cc</h3></th>
                        <th><h3>Status</h3></th>
                        <th><h3>Type</h3></th>
                        <th><h3>Priority</h3></th>
                        <th><h3>Raised By</h3></th>
                        <th><h3>Raised On</h3></th>
                        <th><h3>Details</h3></th>
                    </tr>
                </thead>
                <tbody>


                    <% int sno = 0;%>
                    <c:if test = "${ticketingList != null}">
                        <c:forEach items="${ticketingList}" var="ticketList">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.serialNumber}" /> </td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.ticketNo}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.title}" /></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.message}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.from}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.to}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.cc}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.status}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.type}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.priority}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.raisedBy}"/></td>
                                <td class="<%=className%>"  align="left"> <c:out value="${ticketList.raisedOn}"/></td>
                                <td class="<%=className%>"  align="left">
                                <a href="viewTicketDetails.do?ticketId=<c:out value="${ticketList.ticketId}"/>">Follow Up</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>


            <input type="hidden" name="count" id="count" value="<%=sno%>" />

            <br>
            <br>
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");
            </script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span>Entries Per Page</span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 4);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
</html>