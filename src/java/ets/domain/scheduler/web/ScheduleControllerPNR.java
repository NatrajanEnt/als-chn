/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ets.domain.scheduler.web;

import ets.arch.exception.FPBusinessException;
import ets.arch.exception.FPRuntimeException;
import ets.arch.web.BaseController;
import ets.domain.operation.business.OperationBP;
import ets.domain.operation.business.OperationTO;
import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.scheduler.business.SchedulerBP;
import ets.domain.scheduler.business.SchedulerTO;
import ets.domain.thread.web.SendEmail;
import ets.domain.thread.web.SendEmailDetails;
import ets.domain.thread.web.SendSMS;
import ets.domain.trip.business.TripBP;
import ets.domain.trip.business.TripTO;
import ets.domain.users.business.LoginBP;
import ets.domain.users.business.LoginTO;
import ets.domain.report.business.ReportBP;
import ets.domain.report.business.ReportTO;
import ets.domain.sales.business.SalesBP;
import ets.domain.sales.business.SalesTO;
import ets.domain.util.FPLogUtils;
import ets.domain.util.ThrottleConstants;
//import static ets.domain.util.ThrottleConstants.smsSchedulerStatus;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
//import org.apache.poi.ss.usermodel.Row;
//import org.apache.poi.ss.usermodel.Sheet;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
//import throttlegps.gpsActivity;
//import throttlegps.main.GpsLocationMain;

//excel
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

//excel
/**
 *
 * @author Nivan
 */
public class ScheduleControllerPNR extends BaseController {

    TripBP tripBP;
    LoginBP loginBP;
    SchedulerBP schedulerBP;
    ReportBP reportBP;
    OperationBP operationBP;
    SalesBP salesBP;

    public SalesBP getSalesBP() {
        return salesBP;
    }

    public void setSalesBP(SalesBP salesBP) {
        this.salesBP = salesBP;
    }

    public OperationBP getOperationBP() {
        return operationBP;
    }

    public void setOperationBP(OperationBP operationBP) {
        this.operationBP = operationBP;
    }

    public TripBP getTripBP() {
        return tripBP;
    }

    public void setTripBP(TripBP tripBP) {
        this.tripBP = tripBP;
    }

    public LoginBP getLoginBP() {
        return loginBP;
    }

    public void setLoginBP(LoginBP loginBP) {
        this.loginBP = loginBP;
    }

    public SchedulerBP getSchedulerBP() {
        return schedulerBP;
    }

    public void setSchedulerBP(SchedulerBP schedulerBP) {
        this.schedulerBP = schedulerBP;
    }

    public ReportBP getReportBP() {
        return reportBP;
    }

    public void setReportBP(ReportBP reportBP) {
        this.reportBP = reportBP;
    }

    protected void bind(HttpServletRequest request, Object command)
            throws Exception {
        FPLogUtils.fpDebugLog("Binding request parameters onto MultiActionController command");
        ServletRequestDataBinder binder = createBinder(request, command);
        binder.bind(request);
        FPLogUtils.fpDebugLog("command -->" + command);
        binder.closeNoCatch();
        ////System.out.println("request.getRequestURI() = " + request.getRequestURI());

    }
    public static String applicationServer;
    public static String serverIpAddress;
    int runHour = 0;
    int runMinute = 0;

    public void executeScheduler() throws FPRuntimeException, FPBusinessException {
//         printing current system time
        try {
            System.out.println("Current Time : " + Calendar.getInstance().getTime());
            boolean executionStatus = false;
            ThrottleConstants.PROPS_VALUES = loadProps();
            int smsSchedulerStatus = Integer.parseInt(ThrottleConstants.smsSchedulerStatus);
            int gpsSchedulerStatus = 1;

            System.out.println("Current Time : " + Calendar.getInstance().getTime());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SimpleDateFormat sdfNew = new SimpleDateFormat("dd-MM-yyyy");
            Calendar c = Calendar.getInstance();
            Date date = new Date();
            c.setTime(date);
            String asOnTime = sdfNew.format(c.getTime()).toString();
            String systemTime = sdf.format(c.getTime()).toString();
            String fromDate = sdfNew.format(c.getTime()).toString();
            String[] dateTemp = systemTime.split(" ");
            String[] timeTemp = dateTemp[1].split(":");
            runHour = Integer.parseInt(timeTemp[0]);
            runMinute = Integer.parseInt(timeTemp[1]);

            ThrottleConstants.PROPS_VALUES = loadProps();

            smsSchedulerStatus = Integer.parseInt(ThrottleConstants.smsSchedulerStatus);
            //gpsSchedulerStatus = Integer.parseInt(ThrottleConstants.gpsSchedulerStatus);
            serverIpAddress = InetAddress.getLocalHost().getHostAddress();
            ServletContext servletContext = this.getServletContext();
            String path = servletContext.getRealPath(File.separator);
            System.out.println("path = " + path);
            System.out.println("serverIpAddress = " + serverIpAddress);
            String[] pathSplit = null;
            /*
             try {
             path = path.replace("\\", "/");
             System.out.println("path = " + path);
             pathSplit = path.split("/");
             System.out.println("pathSplit = " + pathSplit[3]);
             applicationServer = pathSplit[3];
             } catch (Exception ex) {
             ex.printStackTrace();
             }
             */

            // Gps Main Method start
//            gpsActivity gps =new gpsActivity();
//            gps.gpsData();
            // Gps End   
            System.out.println("throttle constant dvmTime...." + ThrottleConstants.PROPS_VALUES.get("dvmTime"));
            System.out.println("throttle constant key dvmTime...." + ThrottleConstants.PROPS_VALUES.containsKey("dvmTime"));
            System.out.println("smsSchedulerStatus:" + smsSchedulerStatus);
            if (smsSchedulerStatus == 1) {
                System.out.println("am here....");

                String dailyAlertTime = ThrottleConstants.dailyProcessTime;

                String[] daTimeTemp = dailyAlertTime.split(":");
                System.out.println("runHour:" + runHour);
                System.out.println("runMinute:" + runMinute);
                System.out.println("daTimeTemp[0]:" + Integer.parseInt(daTimeTemp[0]));
                System.out.println("daTimeTemp[1]:" + Integer.parseInt(daTimeTemp[1]));
                if (runHour == Integer.parseInt(daTimeTemp[0]) && runMinute == Integer.parseInt(daTimeTemp[1])) {

                    ////truncate ts_mail_master
                    //daily pnr report start
                    ReportTO repTO = new ReportTO();
                    ArrayList pnrList = new ArrayList();
                    ArrayList PNRavailabilityChart = new ArrayList();
                    OperationTO operationTO1 = new OperationTO();
                    PNRavailabilityChart = operationBP.getPNRavailabilityChart(operationTO1);
                    SalesTO operationTO = new SalesTO();
                    Iterator itr = null;
                    itr = PNRavailabilityChart.iterator();
                    String totalContent = "";
                    String rowContent = "";
                    int total20 = 0;
                    int total40 = 0;
                    int total20Pending = 0;
                    int total40Pending = 0;
                    int total20In = 0;
                    int total40In = 0;

                    while (itr.hasNext()) {
                        operationTO = (SalesTO) itr.next();
                        rowContent = "";
                        rowContent = "<tr>";
                        rowContent = rowContent + "<td>" + operationTO.getCreatedOn() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getPnrNo() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getPortName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getVesselName() + "</td>";
                        total20 = total20 + Integer.parseInt(operationTO.getTwentyTypeContQty());
                        total40 = total40 + Integer.parseInt(operationTO.getFortyTypeContQty());

                        total20Pending = total20Pending + Integer.parseInt(operationTO.getTwentyTypeContQty());
                        total40Pending = total40Pending + Integer.parseInt(operationTO.getFortyTypeContQty());

                        rowContent = rowContent + "<td>" + (Integer.parseInt(operationTO.getTwentyTypeContQty()) + Integer.parseInt(operationTO.getFortyTypeContQty())) + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getTwentyTypeContQtyPlanned() + "/" + operationTO.getTwentyTypeContQty() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getFortyTypeContQtyPlanned() + "/" + operationTO.getFortyTypeContQty() + "</td>";
                        rowContent = rowContent + "<td>" + (Integer.parseInt(operationTO.getTwentyTypeContQtyCompleted()) + Integer.parseInt(operationTO.getFortyTypeContQtyCompleted())) + "/"
                                + (Integer.parseInt(operationTO.getTwentyTypeContQty()) + Integer.parseInt(operationTO.getFortyTypeContQty())) + "</td>";
                        rowContent = rowContent + "</tr>";

                        totalContent = totalContent + rowContent;

                    }

                    String mailTemplate = "";
                    mailTemplate = salesBP.getMailTemplate("3", null);
                    mailTemplate = mailTemplate.replaceAll("PNRDATA", totalContent);

                    SalesTO salesTO2 = new SalesTO();
                    salesTO2.setMailTypeId("3");
                    salesTO2.setMailSubjectTo("Live PNR Board");
                    salesTO2.setMailSubjectCc("Live PNR Board");
                    salesTO2.setMailSubjectBcc("Live PNR Board");
                    salesTO2.setMailContentTo(mailTemplate);
                    salesTO2.setMailContentCc(mailTemplate);
                    salesTO2.setMailContentBcc(mailTemplate);

                    salesTO2.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                    salesTO2.setMailIdCc(ThrottleConstants.pnrUploadCcMailId);
                    salesTO2.setMailIdBcc(ThrottleConstants.pnrUploadBccMailId);
                    String movementType = "0";
                    if ("7".equals(movementType)) {
                        int insertStatus = salesBP.insertMailDetails(salesTO2, 1403, null);
                    }
                    //daily pnr report end

                    //daily pending pnr report start
                    repTO = new ReportTO();
                    pnrList = new ArrayList();
                    ArrayList pendingPNRList = new ArrayList();
                    operationTO1 = new OperationTO();
                    pendingPNRList = operationBP.getPendingPNR(operationTO1);
                    operationTO = new SalesTO();
                    itr = null;
                    itr = pendingPNRList.iterator();
                    totalContent = "";
                    rowContent = "";
                    total20 = 0;
                    total40 = 0;

                    while (itr.hasNext()) {
                        operationTO = (SalesTO) itr.next();
                        rowContent = "";
                        rowContent = "<tr>";
                        asOnTime = operationTO.getEndtime();
                        rowContent = rowContent + "<td>" + operationTO.getPnrNo() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getCreatedOn() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getPortName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getVesselName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getContainerType() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getContainerNos() + "</td>";

                        if ("1".equals(operationTO.getContainerTypeId())) {
                            total20++;
                        } else if ("2".equals(operationTO.getContainerTypeId())) {
                            total40++;
                        }
                        rowContent = rowContent + "</tr>";

                        totalContent = totalContent + rowContent;

                    }

                    mailTemplate = "";
                    mailTemplate = salesBP.getMailTemplate("4", null);
                    mailTemplate = mailTemplate.replaceAll("DATETIME", asOnTime);
                    mailTemplate = mailTemplate.replaceAll("20TOTAL", total20 + " Nos");
                    mailTemplate = mailTemplate.replaceAll("40TOTAL", total40 + " Nos");
                    mailTemplate = mailTemplate.replaceAll("PNRDATA", totalContent);

                    salesTO2 = new SalesTO();
                    salesTO2.setMailTypeId("4");
                    salesTO2.setMailSubjectTo("Pending PNR Containers as on " + asOnTime);
                    salesTO2.setMailSubjectCc("Pending PNR Containers as on " + asOnTime);
                    salesTO2.setMailSubjectBcc("Pending PNR Containers as on " + asOnTime);
                    salesTO2.setMailContentTo(mailTemplate);
                    salesTO2.setMailContentCc(mailTemplate);
                    salesTO2.setMailContentBcc(mailTemplate);

                    salesTO2.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                    salesTO2.setMailIdCc(ThrottleConstants.pnrUploadCcMailId);
                    salesTO2.setMailIdBcc(ThrottleConstants.pnrUploadBccMailId);
                    if ("7".equals(movementType)) {
                        int insertStatus = salesBP.insertMailDetails(salesTO2, 1403, null);
                    }
                    //daily pending pnr report end

                    //daily inward pnr report start
                    repTO = new ReportTO();
                    pnrList = new ArrayList();
                    ArrayList inwardPNRList = new ArrayList();
                    operationTO1 = new OperationTO();
                    inwardPNRList = operationBP.getPNRInward(operationTO1);
                    operationTO = new SalesTO();
                    itr = null;
                    itr = inwardPNRList.iterator();
                    totalContent = "";
                    rowContent = "";
                    total20 = 0;
                    total40 = 0;
                    String startTime = "";
                    String endTime = "";

                    int rowSize = 0;
                    while (itr.hasNext()) {
                        rowSize++;
                        operationTO = (SalesTO) itr.next();
                        rowContent = "";
                        rowContent = "<tr>";
                        startTime = operationTO.getStarttime();
                        endTime = operationTO.getEndtime();
                        rowContent = rowContent + "<td>" + operationTO.getPnrNo() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getCreatedOn() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getPortName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getVesselName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getContainerType() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getContainerNos() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getDate() + "</td>";
                        if ("1".equals(operationTO.getContainerTypeId())) {
                            total20++;
                        } else if ("2".equals(operationTO.getContainerTypeId())) {
                            total40++;
                        }

                        rowContent = rowContent + "</tr>";

                        totalContent = totalContent + rowContent;

                    }
                    if (rowSize == 0) {
                        totalContent = "<tr><td colspan='8'>No Data</td></tr>";
                        Calendar cal = Calendar.getInstance();
                        cal.add(Calendar.DATE, -1);
                        startTime = sdfNew.format(cal.getTime()) + " 06:00 AM";
                        endTime = fromDate + " 06:00 AM";
                    }

                    mailTemplate = "";
                    mailTemplate = salesBP.getMailTemplate("5", null);
                    mailTemplate = mailTemplate.replaceAll("20TOTAL", total20 + " Nos");
                    mailTemplate = mailTemplate.replaceAll("40TOTAL", total40 + " Nos");
                    mailTemplate = mailTemplate.replaceAll("PNRDATA", totalContent);

                    salesTO2 = new SalesTO();
                    salesTO2.setMailTypeId("5");
                    salesTO2.setMailSubjectTo("Inward PNR Containers between " + startTime + " and " + endTime);
                    salesTO2.setMailSubjectCc("Inward PNR Containers between " + startTime + " and " + endTime);
                    salesTO2.setMailSubjectBcc("Inward PNR Containers between " + startTime + " and " + endTime);
                    salesTO2.setMailContentTo(mailTemplate);
                    salesTO2.setMailContentCc(mailTemplate);
                    salesTO2.setMailContentBcc(mailTemplate);

                    salesTO2.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                    salesTO2.setMailIdCc(ThrottleConstants.pnrUploadCcMailId);
                    salesTO2.setMailIdBcc(ThrottleConstants.pnrUploadBccMailId);
                    if ("7".equals(movementType)) {
                        int insertStatus = salesBP.insertMailDetails(salesTO2, 1403, null);
                    }
                    //daily inward pnr report end

                    //Empty Bond Validity Starts here by Muthu++++++++++++++
                    repTO = new ReportTO();
                    pnrList = new ArrayList();
                    ArrayList pendingEmptyBondList = new ArrayList();
                    operationTO1 = new OperationTO();
                    System.out.println("getEmptyBondList+++++");
                    pendingEmptyBondList = operationBP.getPendingEmptyBondList(operationTO1);
                    System.out.println("getEmptyBondList-----------");
                    ArrayList expiredEmptyBondList = new ArrayList();
                    expiredEmptyBondList = operationBP.getExpiredEmptyBondList(operationTO1);
                    operationTO = new SalesTO();
                    itr = null;
                    itr = pendingEmptyBondList.iterator();
                    totalContent = "";
                    rowContent = "";
                    total20 = 0;
                    total40 = 0;
                    System.out.println("pendingEmptyBondList srini size:" + pendingEmptyBondList.size());
                    while (itr.hasNext()) {
                        System.out.println("srini 000000000000");
                        operationTO = (SalesTO) itr.next();
                        rowContent = "";
                        rowContent = "<tr>";
                        asOnTime = operationTO.getEndtime();
                        rowContent = rowContent + "<td>" + operationTO.getConsolName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getVesselName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getContainerType() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getContainerNos() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getPortName() + "</td>";
                        rowContent = rowContent + "<td>" + operationTO.getEmptyBondValidity() + "</td>";

//                        rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";                    
                        if ("1".equals(operationTO.getContainerTypeId())) {
                            total20++;
                        } else if ("2".equals(operationTO.getContainerTypeId())) {
                            total40++;
                        }
                        rowContent = rowContent + "</tr>";

                        totalContent = totalContent + rowContent;

                    }
                    System.out.println("totalContenttotalContent::%%%%::" + totalContent);
                    mailTemplate = "";
                    mailTemplate = salesBP.getMailTemplate("7", null);
                    mailTemplate = mailTemplate.replaceAll("DATETIME", asOnTime);
                    mailTemplate = mailTemplate.replaceAll("20TOTAL", total20 + " Nos");
                    mailTemplate = mailTemplate.replaceAll("40TOTAL", total40 + " Nos");
                    mailTemplate = mailTemplate.replaceAll("EmptyBondList", totalContent);

                    Iterator itr1 = null;
                    itr1 = expiredEmptyBondList.iterator();
                    System.out.println("expiredEmptyBondList srini size:" + expiredEmptyBondList.size());
                    String totalContent1 = "";
                    String rowContent1 = "";
                    total20Pending = 0;
                    total40Pending = 0;

                    while (itr1.hasNext()) {
                        System.out.println("srin11111");
                        operationTO = (SalesTO) itr1.next();
                        rowContent1 = "";
                        rowContent1 = "<tr>";
                        asOnTime = operationTO.getEndtime();
                        rowContent1 = rowContent1 + "<td>" + operationTO.getConsolNames() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + operationTO.getLinernames() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + operationTO.getContainerTypes() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + operationTO.getConName() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + operationTO.getPort() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + operationTO.getEmptyValidity() + "</td>";
                        System.out.println("operationTO.getConsolNames()" + operationTO.getConsolNames());
//                      rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";                    

                        if ("1".equals(operationTO.getConTypeId())) {
                            total20Pending++;
                        } else if ("2".equals(operationTO.getConTypeId())) {
                            total40Pending++;
                        }
                        rowContent1 = rowContent1 + "</tr>";

                        totalContent1 = totalContent1 + rowContent1;
                        System.out.println("totalContent1_____" + totalContent1);
                    }
//                    System.out.println("total20Pending:"+total20Pending);
//                    System.out.println("total40Pending:"+total40Pending);
//                    System.out.println("totalContent:"+totalContent);
//                    System.out.println("totalContent1:"+totalContent1);
                    String mailTemplate1 = "";
//                    mailTemplate1 = salesBP.getMailTemplate("7", null);
                    mailTemplate = mailTemplate.replaceAll("20TYPE", total20Pending + " Nos");
                    mailTemplate = mailTemplate.replaceAll("40TYPE", total40Pending + " Nos");
                    mailTemplate = mailTemplate.replaceAll("ExpiredContainersList", totalContent1);
//                    mailTemplate = mailTemplate + mailTemplate1;
                    System.out.println("RRRRRRRRRRRRRRRR" + mailTemplate);
                    salesTO2 = new SalesTO();
                    salesTO2.setMailTypeId("7");
                    salesTO2.setMailSubjectTo("Pending Empty Bond Containers as on " + asOnTime);
                    salesTO2.setMailSubjectCc("Pending Empty Bond Containers as on " + asOnTime);
                    salesTO2.setMailSubjectBcc("Pending Empty Bond Containers as on " + asOnTime);
                    salesTO2.setMailContentTo(mailTemplate);
                    salesTO2.setMailContentCc(mailTemplate);
                    salesTO2.setMailContentBcc(mailTemplate);
//                    salesTO2.setMailContentTo(mailTemplate1);
//                    salesTO2.setMailContentCc(mailTemplate1);
//                    salesTO2.setMailContentBcc(mailTemplate1);

                    salesTO2.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                    salesTO2.setMailIdCc(ThrottleConstants.pnrUploadCcMailId);
                    salesTO2.setMailIdBcc(ThrottleConstants.pnrUploadBccMailId);
                    if ("7".equals(movementType)) {
                        int insertStatus = salesBP.insertMailDetails(salesTO2, 1403, null);
                    }
                    //Empty Bond Validity Ends here by Muthu...............

                    //Moffusil Movements Starts here by Muthu++++++++++++++
                    pnrList = new ArrayList();

                    ArrayList importLCLmovement = new ArrayList();
                    repTO = new ReportTO();
//                  operationTO1 = new OperationTO();
                    System.out.println("getImportLCLmovement+++++");
                    importLCLmovement = reportBP.getImportLCLmovement(repTO);
                    System.out.println("importLCLmovement size in controller-----------" + importLCLmovement.size());

                    ArrayList importICDmovement = new ArrayList();
                    importICDmovement = reportBP.getImportICDmovement(repTO);
                    System.out.println("importICDmovement--importICDmovement---------" + importICDmovement.size());
                    operationTO = new SalesTO();

                    ArrayList importFCLmovement = new ArrayList();
                    importFCLmovement = reportBP.getImportFCLmovement(repTO);

                    ArrayList exportFCLmovement = new ArrayList();
                    exportFCLmovement = reportBP.getExportFCLmovement(repTO);

                    itr = null;
                    itr = importLCLmovement.iterator();
                    totalContent = "";
                    rowContent = "";
                    int totalOrders = 0;
                    int totalPlanned = 0;
                    int totalStarted = 0;
                    int totalEnded = 0;
                    int lcltotal = 0;
                    System.out.println(" importLCLmovement size:" + importLCLmovement.size());
                    while (itr.hasNext()) {
                        System.out.println("srini 000000000000");
                        repTO = (ReportTO) itr.next();
                        rowContent = "";
                        rowContent = "<tr>";
                        asOnTime = repTO.getOutTime();
//                        rowContent = rowContent + "<td>" + repTO.getMovementTypeName() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getCustomerOrderReferenceNo() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getBillingParty() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getArticleName() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getPkgs() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getPkgsWgt() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getVolume() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getVehicleTypeName() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getVehicleNo() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getDriverName() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getOrigin() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getDestination() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getOutTime() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getRemarks() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getLastKnownGPSLocation() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getTripStatus() + "</td>";
                        lcltotal++;
                        if ("5".equals(repTO.getOrderStatus())) {
                            totalOrders++;
                        } else if ("8".equals(repTO.getOrderStatus())) {
                            totalPlanned++;
                        } else if ("10".equals(repTO.getOrderStatus())) {
                            totalStarted++;
                        } else if ("12".equals(repTO.getOrderStatus())) {
                            totalEnded++;
                        }
                        rowContent = rowContent + "</tr>";

                        totalContent = rowContent;
                    }

                    System.out.println("totalContenttotalContent::%%%%::" + totalContent);
                    mailTemplate = "";
                    mailTemplate = salesBP.getMailTemplate("10", null);
                    mailTemplate = mailTemplate.replaceAll("DATETIME", asOnTime);
                    mailTemplate = mailTemplate.replaceAll("SUMLCL", lcltotal + "");
                    mailTemplate = mailTemplate.replaceAll("ORDERCREATED", totalOrders + "");
                    mailTemplate = mailTemplate.replaceAll("TRIPPLANNED", totalPlanned + "");
                    mailTemplate = mailTemplate.replaceAll("TOTALSTARTED", totalStarted + "");
                    mailTemplate = mailTemplate.replaceAll("ENDEDTRIPS", totalEnded + "");
                    mailTemplate = mailTemplate.replaceAll("IMPORTLCLMOVEMENT", totalContent);

                    itr1 = null;
                    itr1 = importICDmovement.iterator();
                    System.out.println("fffffffffff srini size:" + importICDmovement.size());
                    totalContent1 = "";
                    rowContent1 = "";
                    int ONE = 0;
                    int TWO = 0;
                    int THREE = 0;
                    int FOUR = 0;
                    int icdAll = 0;

                    while (itr1.hasNext()) {
                        System.out.println("srin11111");
                        repTO = (ReportTO) itr1.next();
                        rowContent1 = "";
                        rowContent1 = "<tr>";
                        asOnTime = repTO.getOutTime();
//                        rowContent1 = rowContent1 + "<td>" + repTO.getMovementTypeName() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getCustomerOrderReferenceNo() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getBillingParty() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getArticleName() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getPkgs() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getPkgsWgt() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getVolume() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getVehicleTypeName() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getVehicleNo() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getDriverName() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getOrigin() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getDestination() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getOutTime() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getRemarks() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getLastKnownGPSLocation() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getTripStatus() + "</td>";
                        icdAll++;
                        if ("5".equals(repTO.getOrderStatus())) {
                            ONE++;
                        } else if ("8".equals(repTO.getOrderStatus())) {
                            TWO++;
                        } else if ("10".equals(repTO.getOrderStatus())) {
                            THREE++;
                        } else if ("12".equals(repTO.getOrderStatus())) {
                            FOUR++;
                        }
//                      rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";                    

                        rowContent1 = rowContent1 + "</tr>";

                        System.out.println("totalContent1_____" + totalContent1);
                        totalContent1 = rowContent1;
                    }
                    mailTemplate1 = "";
                    mailTemplate = mailTemplate.replaceAll("ICDALL", icdAll + " ");
                    mailTemplate = mailTemplate.replaceAll("ONE", ONE + " ");
                    mailTemplate = mailTemplate.replaceAll("TWO", TWO + " ");
                    mailTemplate = mailTemplate.replaceAll("THREE", THREE + " ");
                    mailTemplate = mailTemplate.replaceAll("FOUR", FOUR + " ");
                    mailTemplate = mailTemplate.replaceAll("ICDMOVEMENT", totalContent1);

                    Iterator itr2 = null;
                    itr2 = null;
                    itr2 = importFCLmovement.iterator();
                    System.out.println("importICDmovement srini size:" + importFCLmovement.size());
                    String totalContent2 = "";
                    String rowContent2 = "";
                    int FIVE = 0;
                    int SIX = 0;
                    int SEVEN = 0;
                    int EIGHT = 0;
                    int imfcl = 0;

                    while (itr2.hasNext()) {
                        System.out.println("srin11111");
                        repTO = (ReportTO) itr2.next();
                        rowContent2 = "";
                        rowContent2 = "<tr>";
                        asOnTime = repTO.getOutTime();
//                        rowContent2 = rowContent2 + "<td>" + repTO.getMovementTypeName() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getCustomerOrderReferenceNo() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getBillingParty() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getArticleName() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getPkgs() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getPkgsWgt() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getVolume() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getVehicleTypeName() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getVehicleNo() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getDriverName() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getOrigin() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getDestination() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getOutTime() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getRemarks() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getLastKnownGPSLocation() + "</td>";
                        rowContent2 = rowContent2 + "<td>" + repTO.getTripStatus() + "</td>";
                        imfcl++;
                        if ("5".equals(repTO.getOrderStatus())) {
                            FIVE++;
                        } else if ("8".equals(repTO.getOrderStatus())) {
                            SIX++;
                        } else if ("10".equals(repTO.getOrderStatus())) {
                            SEVEN++;
                        } else if ("12".equals(repTO.getOrderStatus())) {
                            EIGHT++;
                        }
//                      rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";                    

                        rowContent2 = rowContent2 + "</tr>";
                        totalContent2 = rowContent2;
                        System.out.println("totalContent2_____" + totalContent2);
                    }
                    mailTemplate1 = "";
                    mailTemplate = mailTemplate.replaceAll("IMPORTFCL", imfcl + " ");
                    mailTemplate = mailTemplate.replaceAll("FIVE", FIVE + " ");
                    mailTemplate = mailTemplate.replaceAll("SIX", SIX + " ");
                    mailTemplate = mailTemplate.replaceAll("SEVEN", SEVEN + " ");
                    mailTemplate = mailTemplate.replaceAll("EIGHT", EIGHT + " ");
                    mailTemplate = mailTemplate.replaceAll("IMPFCLMOVEMENT", totalContent2);

                    Iterator itr3 = null;
                    itr3 = null;
                    itr3 = exportFCLmovement.iterator();
                    System.out.println("exportFCLmovement srini size:" + exportFCLmovement.size());
                    String totalContent3 = "";
                    String rowContent3 = "";
                    int NINE = 0;
                    int TEN = 0;
                    int ELEVEN = 0;
                    int TWELVE = 0;
                    int netExport = 0;

                    while (itr3.hasNext()) {
                        System.out.println("srin11111");
                        repTO = (ReportTO) itr3.next();
                        rowContent3 = "";
                        rowContent3 = "<tr>";
                        asOnTime = repTO.getOutTime();
//                        rowContent3 = rowContent3 + "<td>" + repTO.getMovementTypeName() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getCustomerOrderReferenceNo() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getBillingParty() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getArticleName() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getPkgs() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getPkgsWgt() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getVolume() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getVehicleTypeName() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getVehicleNo() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getDriverName() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getOrigin() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getDestination() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getOutTime() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getRemarks() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getLastKnownGPSLocation() + "</td>";
                        rowContent3 = rowContent3 + "<td>" + repTO.getTripStatus() + "</td>";
                        netExport++;
                        if ("5".equals(repTO.getOrderStatus())) {
                            NINE++;
                        } else if ("8".equals(repTO.getOrderStatus())) {
                            TEN++;
                        } else if ("10".equals(repTO.getOrderStatus())) {
                            ELEVEN++;
                        } else if ("12".equals(repTO.getOrderStatus())) {
                            TWELVE++;
                        }
//                      rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";                    

                        rowContent3 = rowContent3 + "</tr>";
                        totalContent3 = rowContent3;
                        System.out.println("totalContent3_____" + totalContent3);
                    }
                    mailTemplate1 = "";
                    mailTemplate = mailTemplate.replaceAll("NETEXPORT", netExport + " ");
                    mailTemplate = mailTemplate.replaceAll("NINE", NINE + " ");
                    mailTemplate = mailTemplate.replaceAll("TEN", TEN + " ");
                    mailTemplate = mailTemplate.replaceAll("ELEVEN", ELEVEN + " ");
                    mailTemplate = mailTemplate.replaceAll("TWELVE", TWELVE + " ");
                    mailTemplate = mailTemplate.replaceAll("EXPORTFCLMOVEMENT", totalContent3);

//                    mailTemplate = mailTemplate + mailTemplate1;
                    System.out.println("RRRRRRRRRRRRRRRR" + mailTemplate);
                    salesTO2 = new SalesTO();
                    salesTO2.setMailTypeId("10");
                    salesTO2.setMailSubjectTo("Moffusil Chennai Movements  " + asOnTime);
                    salesTO2.setMailSubjectCc("Moffusil Chennai Movements " + asOnTime);
                    salesTO2.setMailSubjectBcc("Moffusil Chennai Movements " + asOnTime);
                    salesTO2.setMailContentTo(mailTemplate);
                    salesTO2.setMailContentCc(mailTemplate);
                    salesTO2.setMailContentBcc(mailTemplate);
//                    salesTO2.setMailContentTo(mailTemplate1);
//                    salesTO2.setMailContentCc(mailTemplate1);
//                    salesTO2.setMailContentBcc(mailTemplate1);

                    salesTO2.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                    salesTO2.setMailIdCc(ThrottleConstants.pnrUploadCcMailId);
                    salesTO2.setMailIdBcc(ThrottleConstants.pnrUploadBccMailId);

                    int insertStatus = salesBP.insertMailDetails(salesTO2, 1403, null);

                    //Moffusil Movements Ends here by Muthu...............
                    //Vessel Cut-Off Time Starts here by Muthu++++++++++++++
                    pnrList = new ArrayList();

                    ArrayList pendingVesselList = new ArrayList();
                    repTO = new ReportTO();
                    System.out.println("pendingVesselList+++++");
                    pendingVesselList = reportBP.getPendingVesselList(repTO);
                    System.out.println("pendingVesselList size in controller-----------" + pendingVesselList.size());

                    ArrayList expiredVesselList = new ArrayList();
                    expiredVesselList = reportBP.getExpiredVesselList(repTO);
                    System.out.println("expiredVesselList--expiredVesselList---------" + expiredVesselList.size());
                    operationTO = new SalesTO();

                    itr = null;
                    itr = pendingVesselList.iterator();
                    totalContent = "";
                    rowContent = "";
                    total20 = 0;
                    total40 = 0;

                    System.out.println(" pendingVesselList size:" + pendingVesselList.size());
                    while (itr.hasNext()) {
                        System.out.println("srini pendingVesselList000000");
                        repTO = (ReportTO) itr.next();
                        rowContent = "";
                        rowContent = "<tr>";
//                        asOnTime = repTO.getOutTime();
                        rowContent = rowContent + "<td>" + repTO.getConsolNames() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getLinernames() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getContainerTypes() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getContainerNo() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getPortNames() + "</td>";
                        rowContent = rowContent + "<td>" + repTO.getCutoffValidity() + "</td>";
                        rowContent = rowContent + "</tr>";

                        if ("1".equals(repTO.getConTypeId())) {
                            total20++;
                        } else if ("2".equals(repTO.getConTypeId())) {
                            total40++;
                        }
                        totalContent = totalContent + rowContent;
                    }

                    System.out.println("totalContenttota??????????::%%%%::" + totalContent);
                    mailTemplate = "";
                    mailTemplate = salesBP.getMailTemplate("8", null);
//                    mailTemplate = mailTemplate.replaceAll("DATETIME", asOnTime);
                    mailTemplate = mailTemplate.replaceAll("20TOTAL", total20 + " Nos");
                    mailTemplate = mailTemplate.replaceAll("40TOTAL", total40 + " Nos");
                    mailTemplate = mailTemplate.replaceAll("VesselCutOffList", totalContent);

                    itr1 = null;
                    itr1 = expiredVesselList.iterator();
                    System.out.println("fffffffffff expiredVesselList size:" + expiredVesselList.size());
                    totalContent1 = "";
                    rowContent1 = "";
                    total20Pending = 0;
                    total40Pending = 0;

                    while (itr1.hasNext()) {
                        System.out.println("itrrrrrr1111111111");
                        repTO = (ReportTO) itr1.next();
                        rowContent1 = "";
                        rowContent1 = "<tr>";
//                        asOnTime = repTO.getOutTime();
                        rowContent1 = rowContent1 + "<td>" + repTO.getConsolNames() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getLinernames() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getContainerTypes() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getContainerNo() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getPortNames() + "</td>";
                        rowContent1 = rowContent1 + "<td>" + repTO.getCutoffValidity() + "</td>";
                        System.out.println("repTO.getConsolNames()");
//                      rowContent = rowContent + "<td>" + operationTO.getDelayAge() + "</td>";                    
                        rowContent1 = rowContent1 + "</tr>";
                        if ("1".equals(repTO.getConTypeId())) {
                            total20Pending++;
                        } else if ("2".equals(repTO.getConTypeId())) {
                            total40Pending++;
                        }

                        totalContent1 = totalContent1 + rowContent1;
                        System.out.println("totalContent1_____" + totalContent1);
                    }
                    mailTemplate1 = "";
                    mailTemplate = mailTemplate.replaceAll("20TYPE", total20Pending + " Nos");
                    mailTemplate = mailTemplate.replaceAll("40TYPE", total40Pending + " Nos");
                    mailTemplate = mailTemplate.replaceAll("ExpiredVesselList", totalContent1);

//                    mailTemplate = mailTemplate + mailTemplate1;
                    System.out.println("RRRRRRRRRRRRRRRR" + mailTemplate);
                    salesTO2 = new SalesTO();
                    salesTO2.setMailTypeId("8");
                    salesTO2.setMailSubjectTo("Vessel Cut-Off Time  " + asOnTime);
                    salesTO2.setMailSubjectCc("Vessel Cut-Off Time " + asOnTime);
                    salesTO2.setMailSubjectBcc("Vessel Cut-Off Time " + asOnTime);
                    salesTO2.setMailContentTo(mailTemplate);
                    salesTO2.setMailContentCc(mailTemplate);
                    salesTO2.setMailContentBcc(mailTemplate);
//                    salesTO2.setMailContentTo(mailTemplate1);
//                    salesTO2.setMailContentCc(mailTemplate1);
//                    salesTO2.setMailContentBcc(mailTemplate1);

                    salesTO2.setMailIdTo(ThrottleConstants.pnrUploadToMailId);
                    salesTO2.setMailIdCc(ThrottleConstants.pnrUploadCcMailId);
                    salesTO2.setMailIdBcc(ThrottleConstants.pnrUploadBccMailId);
                    if ("7".equals(movementType)) {
                        insertStatus = salesBP.insertMailDetails(salesTO2, 1403, null);
                    }
                    //Vessel Cut-Off Time Ends here by Muthu...............

                }

                ReportTO reportTO = new ReportTO();
                ArrayList emailList = new ArrayList();
                Iterator itr = null;
                try {
                    emailList = reportBP.getEmailList();
                    System.out.println("emailList ------------- " + emailList);
                    System.out.println("emailList ----------------------- " + emailList.size());
                    itr = emailList.iterator();
                    while (itr.hasNext()) {
                        reportTO = (ReportTO) itr.next();
                        System.out.println("reportTO ----- mail contenet  " + reportTO.getMailContentTo());
                        System.out.println("applicationServer ----------- " + applicationServer);
                        reportTO.setServerName(applicationServer);
                        reportTO.setServerIpAddress(serverIpAddress);
                        new EmailSSL(reportTO).start();
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
//                FPLogUtils.fpErrorLog("Email not delivered --> " + exp.getMessage());
                    ReportTO rTO = new ReportTO();
                    rTO.setMailSubjectTo("Email not delivered");
                    rTO.setMailContentTo(exp.getMessage());
                    rTO.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO = null;
                } finally {
                    emailList = null;
                    itr = null;
                }
//        if ( runMinute == 00) {
                // Tally list of tripsheet. -1
                ReportTO reportTO1 = new ReportTO();
                ArrayList listOfTripSHeet = new ArrayList();
                Iterator itr1 = null;
                try {
//                    listOfTripSHeet = reportBP.getListOfTripSHeet();
                    System.out.println("list of tripsheet ---------- " + listOfTripSHeet.size());
                    if(listOfTripSHeet.size() > 0) {
                    itr1 = listOfTripSHeet.iterator();
                    while (itr1.hasNext()) {
                        reportTO1 = (ReportTO) itr1.next();
                        System.out.println("iteratorrr");
//                        new sqlserviceconn(reportTO1).start();
                        }
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                    ReportTO rTO1 = new ReportTO();
                    rTO1.setMailSubjectTo("tally list of tripsheet error...");
                    rTO1.setMailContentTo(exp.getMessage());
                    rTO1.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO1.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO1 = null;
                } finally {
                    listOfTripSHeet = null;
                    itr1 = null;
                }

                // Tally TripedContainerDetails. -2
                ReportTO reportTO2 = new ReportTO();
                ArrayList tripContainerDetails = new ArrayList();
                Iterator itr2 = null;
                try {
//                    tripContainerDetails = reportBP.getTripContainerDetails();
                    System.out.println("list of tripContainerDetails ---------- " + tripContainerDetails.size());
                    if(tripContainerDetails.size() > 0) {
                    itr2 = tripContainerDetails.iterator();
                    while (itr2.hasNext()) {
                        reportTO2 = (ReportTO) itr2.next();
                        System.out.println("tripContainerDetails");
//                        new sqlserviceconn1(reportTO2).start();
                      }
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                    ReportTO rTO2 = new ReportTO();
                    rTO2.setMailSubjectTo("tally list of containers error...");
                    rTO2.setMailContentTo(exp.getMessage());
                    rTO2.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO2.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO2 = null;
                } finally {
                    tripContainerDetails = null;
                    itr2 = null;
                }

                // Tally vehicleList. -3
                ReportTO reportTO3 = new ReportTO();
                ArrayList tripVehicleList = new ArrayList();
                Iterator itr3 = null;
                try {
//                    tripVehicleList = reportBP.getTripVehicleListForTally();
                    System.out.println("list of tripVehicleList ---------- " + tripVehicleList.size());
                    if(tripVehicleList.size() > 0){
                    itr3 = tripVehicleList.iterator();
                    while (itr3.hasNext()) {
                        reportTO3 = (ReportTO) itr3.next();
                        System.out.println("tripVehicleList");
//                        new sqlserviceconn2(reportTO3).start();
                     }
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                    ReportTO rTO3 = new ReportTO();
                    rTO3.setMailSubjectTo("tally list of vehicle list error...");
                    rTO3.setMailContentTo(exp.getMessage());
                    rTO3.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO3.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO3 = null;
                } finally {
                    tripVehicleList = null;
                    itr3 = null;
                }
                // Tally TripedContainerDetails. -4
                ReportTO reportTO4 = new ReportTO();
                ArrayList tripNameMapping = new ArrayList();
                Iterator itr4 = null;
                try {
//                    tripNameMapping = reportBP.getTripNameMappingList();
                    System.out.println("list of tripNameMapping ---------- " + tripNameMapping.size());
                    if(tripNameMapping.size() > 0){
                    itr4 = tripNameMapping.iterator();
                    while (itr4.hasNext()) {
                        reportTO4 = (ReportTO) itr4.next();
                        System.out.println("tripNameMapping");
//                        new sqlserviceconn3(reportTO4).start();
                      }
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                    ReportTO rTO4 = new ReportTO();
                    rTO4.setMailSubjectTo("tally list of name Mapping error...");
                    rTO4.setMailContentTo(exp.getMessage());
                    rTO4.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO4.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO4 = null;
                } finally {
                    tripNameMapping = null;
                    itr4 = null;
                }
                // Tally AccountTransation. -5
                ReportTO reportTO5 = new ReportTO();
                ArrayList accountTransation = new ArrayList();
                Iterator itr5 = null;
                try {
//                    accountTransation = reportBP.getAccountTransationList();
                    System.out.println("list of accountTransation ---------- " + accountTransation.size());
                    if(accountTransation.size() > 0) {
                    itr5 = accountTransation.iterator();
                    while (itr5.hasNext()) {
                        reportTO5 = (ReportTO) itr5.next();
                        System.out.println("accountTransation");
//                        new sqlserviceconn4(reportTO5).start();
                      }
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                    ReportTO rTO5 = new ReportTO();
                    rTO5.setMailSubjectTo("tally list of accountTransation error...");
                    rTO5.setMailContentTo(exp.getMessage());
                    rTO5.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO5.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO5 = null;
                } finally {
                    accountTransation = null;
                    itr5 = null;
                }

                // Tally AccountTransation. -6
                ReportTO reportTO6 = new ReportTO();
                ArrayList transationDetails = new ArrayList();
                Iterator itr6 = null;
                try {
//                    transationDetails = reportBP.getTransationDetailsList();
                    System.out.println("list of transationDetails ---------- " + transationDetails.size());
                    if(transationDetails.size() > 0){
                    itr6 = transationDetails.iterator();
                    while (itr6.hasNext()) {
                        reportTO6 = (ReportTO) itr6.next();
                        System.out.println("transationDetails");
//                        new sqlserviceconn5(reportTO6).start();
                      }
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                    ReportTO rTO6 = new ReportTO();
                    rTO6.setMailSubjectTo("tally list of transationDetails error...");
                    rTO6.setMailContentTo(exp.getMessage());
                    rTO6.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO6.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO6 = null;
                } finally {
                    transationDetails = null;
                    itr6 = null;
                }
                // Tally AccountTransation. -7
                ReportTO reportTO7 = new ReportTO();
                ArrayList inventoryDetails = new ArrayList();
                Iterator itr7 = null;
                try {
//                    inventoryDetails = reportBP.getInventoryDetailsList();
                    System.out.println("list of InventoryDetails ---------- " + inventoryDetails.size());
                    if(inventoryDetails.size() > 0){
                    itr7 = inventoryDetails.iterator();
                    while (itr7.hasNext()) {
                        reportTO7 = (ReportTO) itr7.next();
                        System.out.println("inventoryDetails");
//                        new sqlserviceconn6(reportTO7).start();
                     }
                    }
                } catch (Exception exp) {
                    exp.printStackTrace();
                    ReportTO rTO7 = new ReportTO();
                    rTO7.setMailSubjectTo("tally list of inventoryDetails error...");
                    rTO7.setMailContentTo(exp.getMessage());
                    rTO7.setMailIdTo(ThrottleConstants.developerSupport);
                    rTO7.setMailIdCc("throttleerror@gmail.com");
                    //new EmailSSL(rTO).start();
                    rTO7 = null;
                } finally {
                    transationDetails = null;
                    itr7 = null;
                }
//            }  
                //run gps program every 5 min start
                if ((runMinute % 5) == 0) {
                    System.out.println("gps running started................");
//                    GpsCurrentLocationTriway.main(null);
                    System.out.println("gps running started................");
                }
                //run gps program every 5 min end

            }
            //  }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public HashMap<String, String> loadProps() throws FPRuntimeException, FPBusinessException {
        Map map = new HashMap();
        HashMap<String, String> maps = new HashMap<String, String>();
        try {
            ArrayList list = new ArrayList();
            list = loginBP.LoadProps();
//            System.out.println("IM HERE-------");
            ThrottleConstants.PROPS_VALUES = new HashMap<String, String>();
            for (Iterator it = list.iterator(); it.hasNext();) {
                LoginTO object = (LoginTO) it.next();
                try {
//                    System.out.println("object.getKeyName() = " + object.getKeyName());
//                    System.out.println("object.getValue() = " + object.getValue());
                    ThrottleConstants.PROPS_VALUES.put(object.getKeyName().trim(), object.getValue().trim());
                } catch (Exception e) {
                }
            }

        } catch (Exception e) {
            String error = e.getMessage();
        }
        map.clear();
        return ThrottleConstants.PROPS_VALUES;
    }

}
