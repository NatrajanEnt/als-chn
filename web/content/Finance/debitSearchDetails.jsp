
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>  
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<!--<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">-->
<script language="javascript" src="/throttle/js/validate.js"></script>  
<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>

        <script type="text/javascript">
            $(document).ready(function() {
                $("#datepicker").datepicker({
                    showOn: "button",
                    buttonImage: "calendar.gif",
                    buttonImageOnly: true

                });
            });

            $(function() {
                // alert("cv");
                $(".datepicker").datepicker({
                    /*altField: "#alternate",
                     altFormat: "DD, d MM, yy"*/
                    changeMonth: true, changeYear: true
                });
            });

         $(document).ready(function(){
             $("#showTable").hide();

             });
        </script>
        <script type="text/javascript">
            function submitPage(value) {
                
               $("#showTable").show();  
            }
            
           
        </script>

        
        
         </script>
    <style>
    #index td {
   color:white;
}
</style>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.DebitDetails"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.DebitDetails"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
        

    <body onload="setValue();sorter.size(10);">
        <form name="accountReceivable" action=""  method="post">
            <table class="table table-info mb30 table-hover" >
                <thead>
                <tr >
                    <th colspan="8" ><spring:message code="finance.label.DebitDetails"  text="default text"/></th>
                </tr>
                </thead>
                        
                                <!--<table width="800" cellpadding="0" cellspacing="2" border="0" align="center" class="table4" >-->
                                    <tr>
                                        <td><font color="red">*</font><spring:message code="finance.label.CustomerName"  text="default text"/></td>
                                        <td height="30">
                                            <select class="form-control" name="customerId" id="customerId" style="width:260px;height:40px;">
                                                <option value="">---<spring:message code="finance.label.Select"  text="default text"/>---</option>
                                                <option value="">Sakthi Traders</option>
                                                <option value="">Palanisamy .K</option>
                                                <option value="">MALNI TRADERS</option>
                                                <option value="">Sarada Company</option>
                                                
                                            </select>
                                        </td>
                                        <td colspan="2"></td>
<!--                                    </tr>
                                    <tr>-->
                                        <td><font color="red">*</font><spring:message code="finance.label.FromDate"  text="default text"/></td>
                                        <td height="30"><input name="fromDate" autocomplete="off" id="fromDate" type="text" class="datepicker" value="" style="width:260px;height:40px;"></td>
                                        <td><font color="red">*</font><spring:message code="finance.label.ToDate"  text="default text"/></td>
                                        <td height="30"><input name="toDate" id="toDate" autocomplete="off" type="text" class="datepicker" value="" style="width:260px;height:40px;"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" align="center"><input type="button" class="btn btn-success" name="Search"   value="<spring:message code="finance.label.Search"  text="default text"/>" onclick="submitPage(this.name);" style="width:100px;height:35px;"></td>
                                    </tr>
                                </table>
             <c:if test = "${CRJDetailList != null}" >
             <table class="table table-info mb30 table-hover" id="table">
                    <thead>
                            <th><spring:message code="finance.label.Sno"  text="default text"/></th>
                            <th><spring:message code="finance.label.CustomerName"  text="default text"/></th>
                            <th><spring:message code="finance.label.DebitNoteNo"  text="default text"/></th>
                            <th><spring:message code="finance.label.Date"  text="default text"/></th>
                            <th><spring:message code="finance.label.DebitAmount"  text="default text"/></th>
                            <th><spring:message code="finance.label.Reason"  text="default text"/></th>
                            <th><spring:message code="finance.label.Delete"  text="default text"/></th>
                            <th><spring:message code="finance.label.Modify"  text="default text"/></th>
                    </thead>
                    <tbody>
                      <% int sno = 0;
                            int index = 1;%>
                    <c:forEach items="${CRJDetailList}" var="list"> 

                        <tr  width="208" height="40" > 
                            <td  height="20"><%=index%></td>
                            <td  height="20"><c:out value="${list.code}"/></td>
                            <td  height="20"><c:out value="${list.vendorName}"/></td>
                            <td  height="20"><c:out value="${list.vendorTypeName}"/></td>
                            <td  height="20"><c:out value="${list.crjDate}"/></td>
                            <td  height="20"><c:out value="${list.invoiceNo}"/></td>
                            <td  height="20"><c:out value="${list.invoiceAmount}"/></td>
                            <td  height="20"><c:out value="${list.invoiceDate}"/></td>
                            <td  height="20"><c:out value="${list.headerNarration}"/></td>
                            <td  height="20"><c:out value="${list.paidAmount}"/></td>
                            </td>
                        </tr>
                        <%
                            index++;
                            sno++;
                        %>
                    </c:forEach>
                </tbody>
            </table>

            <input type="hidden" name="count" id="count" value="<%=sno%>" />
        </c:if>
        <br>
        <br>
<!--                <center>
                                        <td><input type="button" class="btn btn-success" name="ExportExcel"   value="Print" onclick="submitPage(this.name);"></td>
                                        <td><input type="button" class="btn btn-success" name="Search"   value="Search" onclick="submitPage(this.name);"></td>
                </center>-->
             <script language="javascript" type="text/javascript">
            setFilterGrid("table");
        </script>
        <div id="controls">
            <div id="perpage">
                <select onchange="sorter.size(this.value)" style="width:60px;height:20px;">
                    <option value="5"  selected="selected">5</option>
                    <option value="10">10</option>
                    <option value="20">20</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select>
                <span><spring:message code="finance.label.EntriesPerPage"  text="default text"/></span>
            </div>
            <div id="navigation" >
                <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
            </div>
            <div id="text"><spring:message code="finance.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="finance.label.of"  text="default text"/> <span id="pagelimit"></span></div>
        </div>
        <script type="text/javascript">
            var sorter = new TINY.table.sorter("sorter");
            sorter.head = "head";
            sorter.asc = "asc";
            sorter.even = "evenrow";
            sorter.odd = "oddrow";
            sorter.evensel = "evenselected";
            sorter.oddsel = "oddselected";
            sorter.paginate = true;
            sorter.currentid = "currentpage";
            sorter.limitid = "pagelimit";
            sorter.init("table", 0);
        </script>
            
            </div>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>    
</div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>