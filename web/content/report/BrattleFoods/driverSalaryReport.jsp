
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@page import="java.util.Locale"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ page import="java.util.* "%>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<%@ page import="java.text.DecimalFormat" %>
<%@ page import="java.text.NumberFormat" %>
<%@page import="java.text.SimpleDateFormat" %>
<!DOCTYPE html>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>

<link rel="stylesheet" href="/throttle/css/jquery-ui.css">
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<link rel="stylesheet" href="/throttle/css/TableSort.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>            
<link rel="stylesheet" href="/throttle/js/datepicker/datepicker3.css">
<script src="/throttle/js/datepicker/bootstrap-datepicker.js"></script>
<script src="content/NewDesign/js/jquery-1.11.1.min.js"></script>
<script src="content/NewDesign/js/jquery-ui-1.10.3.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });
    });

    $(function() {
        //alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });
    });

</script>

<script type="text/javascript">
    //auto com

    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#vehicleNo').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getRegistrationNo.do",
                    dataType: "json",
                    data: {
                        regno: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        response(items);
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var tmp = value.split('-');
                $('#vehicleId').val(tmp[0]);
                $('#vehicleNo').val(tmp[1]);
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            var temp = itemVal.split('-');
            itemVal = '<font color="green">' + temp[1] + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });


</script>



<script type="text/javascript">
    function submitPage(value) {
        if (document.getElementById('fromDate').value == '') {
            alert("Please select from Date");
            document.getElementById('fromDate').focus();
        } else if (document.getElementById('toDate').value == '') {
            alert("Please select to Date");
            document.getElementById('toDate').focus();
        } else {
            if (value == 'ExportExcel') {
                document.accountReceivable.action = '/throttle/driverSalaryReport.do?param=ExportExcel';
                document.accountReceivable.submit();
            } else {
                document.accountReceivable.action = '/throttle/driverSalaryReport.do?param=Search';
                document.accountReceivable.submit();
            }
        }
    }
    function setValue() {
        if ('<%=request.getAttribute("page")%>' != 'null') {
            var page = '<%=request.getAttribute("page")%>';
            if (page == 1) {
                submitPage('search');
            }
        }
        if ('<%=request.getAttribute("vehicleId")%>' != 'null') {
            document.getElementById('vehicleId').value = '<%=request.getAttribute("vehicleId")%>';
        }
        if ('<%=request.getAttribute("vehicleNo")%>' != 'null') {
            document.getElementById('vehicleNo').value = '<%=request.getAttribute("vehicleNo")%>';
        }
    }
    function calculateDays() {
        var diff = 0;
        var one_day = 1000 * 60 * 60 * 24;
        var fromDate = document.getElementById("fromDate").value;
        var toDate = document.getElementById("toDate").value;
        var earDate = fromDate.split("-");
        var nexDate = toDate.split("-");
        var fD = parseFloat(earDate[0]).toFixed(0);
        var fM = parseFloat(earDate[1]).toFixed(0);
        var fY = parseFloat(earDate[2]).toFixed(0);

        var tD = parseFloat(nexDate[0]).toFixed(0);
        var tM = parseFloat(nexDate[1]).toFixed(0);
        var tY = parseFloat(nexDate[2]).toFixed(0);
        var d1 = new Date(fY, fM, fD);
        var d2 = new Date(tY, tM, tD);
        diff = (d2.getTime() - d1.getTime()) / one_day;
        document.getElementById("totalDays").value = diff;
    }

    function viewVehicleDetails(vehicleId) {
        document.accountReceivable.action = '/throttle/viewVehicle.do?vehicleId=' + vehicleId;
        document.accountReceivable.submit();
    }
//                function viewVehicleDetails(vehicleId) {
//                window.open('/throttle/viewVehicle.do?vehicleId=' + vehicleId, 'PopupPage', 'height = 800, width = 1000, scrollbars = yes, resizable = yes');
//            }
</script>
<div class="pageheader">
    <h2><i class="fa fa-edit"></i> Report</h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
            <li><a href="general-forms.html">Report</a></li>
            <li class="active">Driver Salary Report</li>
        </ol>
    </div>
</div>


<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body">
            <body onload="setValue();
            calculateDays();
            sorter.size(20);">
                <form name="accountReceivable"   method="post">
                    
                    <%@ include file="/content/common/message.jsp" %>
                    <table style="width:100%" align="center" border="0"  class="table table-info mb30 table-hover"   >
                       
                        <tr height="30"   >
                            <td colSpan="8" style=" color:white;font-weight: bold;background-color:#5BC0DE;font-size:14px;">Driver Salary Report</td></tr>
                        <tr height="40" style="display:none;">
                            <td></font>Driver</td>
                            <td height="30" > <select name="fleetVendorId" id="fleetVendorId" class="form-control" style="width: 200px" >
                                    <c:if test="${vendorList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${vendorList}" var="vehNo">
                                            <option value='<c:out value="${vehNo.vendorId}"/>~<c:out value="${vehNo.contractTypeId}"/>'><c:out value="${vehNo.vendorName}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                            <td align="left" >Vehicle No</td>
                            <td height="30" >
                                <input type="hidden" name="vehicleId" id="vehicleId" value="<c:out value="${vehicleId}"/>"/>
                                <input type="text" name="vehicleNo" id="vehicleNo" value="<c:out value="${vehicleNo}"/>" class="form-control" style="width: 200px"/>
                            </td>
                            </tr>
                            <tr height="40">
                            <tr height="40" style="display:none;">
                            <td height="30" > <select name="fleetCenterId" id="fleetCenterId" class="form-control" style="width: 200px" >
                                    <c:if test="${companyList != null}">
                                        <option value="" selected>--Select--</option>
                                        <c:forEach items="${companyList}" var="companyList">
                                            <option value='<c:out value="${companyList.cmpId}"/>'><c:out value="${companyList.name}"/></option>
                                        </c:forEach>
                                    </c:if>
                                </select></td>
                                  </tr>
                             <tr height="40">
                            <td align="left" >From Date</td>
                            <td height="30" ><input name="fromDate" id="fromDate" value="<c:out value="${fromDate}"/>" type="text" class="form-control datepicker" style="width: 200px"></td>
                            <td align="left">To Date</td>
                            <td height="30" ><input name="toDate" id="toDate" value="<c:out value="${toDate}" />" type="text" class="form-control datepicker" style="width: 200px" onchange="calculateDays();">
                            
                                <input type="hidden" value="" name="totalDays" id="totalDays" /></td>
                        </tr>
                        <tr height="40" align="center">
                            
                            <td colspan="8" align="center"> <input type="button" class="btn btn-info" name="ExportExcel" id="ExportExcel"  value="<spring:message code="sales.label.customer.Export Excel"  text="Export Excel"/>" onclick="submitPage(this.name);" >&nbsp;&nbsp;
                                <input type="button" class="btn btn-info" name="Search"  id="Search"  value="<spring:message code="sales.label.customer.search"  text="Search"/>" onclick="submitPage(this.name);" ></td>
                        </tr>
                    </table>
                    <br>
                    <c:if test="${driverSalaryReportDetails == null}">
                        <center>
                            <font color="red">No Records</font>
                        </center>
                    </c:if>
                    <c:if test="${driverSalaryReportDetails != null}">
                        <table align="center" id="table" class="table table-info mb30 table-hover"style="width:100%;" >
                            <thead height="30">
                                <tr id="tableDesingTH" height="30">
                                    <th align="center">Trip No</th>
                                    <th align="center">Vehicle No</th>
                                    <th align="center">Driver Name</th>
                                    <th align="center">Movement Date - From</th>
                                    <th align="center">Movement Date - To</th>
                                    <th align="center">Place</th>
                                    <th align="center">Customer</th>
                                    <th align="center">Collection Bata</th>
                                    <th align="center">Wages</th>
                                    <th align="center">Diesel Recovery (in Negative Figure)</th>
                                    <th align="center">Damages (in Negative Figure)</th>
                                    <th align="center">Salary Advance (in Negative Figure)</th>
                                    <th align="center">Net Payable</th>
                                   

                            </tr> 
                            </thead>
                            <tbody>
                               
                                <c:forEach items="${driverSalaryReportDetails}" var="driverSR">
                                   <c:set var="totlaNetPayable" value="${0 }"/>
                                    <tr>
                                        <td   ><c:out value="${driverSR.tripCode}"/></td>
                                        <td   ><c:out value="${driverSR.regNo}"/></td>
                                        <td   ><c:out value="${driverSR.empName}"/></td>
                                        <td   ><c:out value="${driverSR.startDate}"/></td>
                                        <td   ><c:out value="${driverSR.endDate}"/></td>
                                        <td   ><c:out value="${driverSR.routeInfo}"/></td>
                                        <td   ><c:out value="${driverSR.customerName}"/></td>
                                        <td   ><c:out value="${driverSR.collectionBata}"/></td>
                                        <td   ><c:out value="${driverSR.wages}"/></td>
                                        <td   ><c:out value="${driverSR.recoveryAmount}"/></td>
                                        <td   ><c:out value="${driverSR.damage}"/></td>
                                        <td   ><c:out value="${driverSR.salaryAdvance}"/></td>
                                        <td   >
                                             <c:set var="totlaNetPayable" value="${(driverSR.collectionBata + driverSR.wages) - (driverSR.recoveryAmount + driverSR.damage)}"/>
                                             <fmt:formatNumber type="number"  maxFractionDigits="2" value="${totlaNetPayable }" />
                                        </td>
                                    </tr>

                                </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <script language="javascript" type="text/javascript">
                        setFilterGrid("table");
                    </script>
                    <div id="controls">
                        <div id="perpage">
                            <select onchange="sorter.size(this.value)">
                                <option value="5" >5</option>
                                <option value="10">10</option>
                                <option value="20" selected="selected">20</option>
                                <option value="50">50</option>
                                <option value="100">100</option>
                            </select>
                            <span>Entries Per Page</span>
                        </div>
                        <div id="navigation">
                            <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                            <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                            <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                            <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                        </div>
                        <div id="text">Displaying Page <span id="currentpage"></span> of <span id="pagelimit"></span></div>
                    </div>
                    <script type="text/javascript">
                        var sorter = new TINY.table.sorter("sorter");
                        sorter.head = "head";
                        sorter.asc = "asc";
                        sorter.desc = "desc";
                        sorter.even = "evenrow";
                        sorter.odd = "oddrow";
                        sorter.evensel = "evenselected";
                        sorter.oddsel = "oddselected";
                        sorter.paginate = true;
                        sorter.currentid = "currentpage";
                        sorter.limitid = "pagelimit";
                        sorter.init("table", 0);
                    </script>
                    <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
            </body>    
        </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
