
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="ets.domain.renderservice.business.RenderServiceTO" %>
<%@ page import="ets.domain.renderservice.business.ProblemActivityTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BUS</title>
        <link href="/amw/css/parveen.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript" src="/amw/js/validate.js"></script>    
    <script>
        function deleteRow() {
            try {
            var table = document.getElementById("addRows");
            //rowCount = table.rows.length;
 	    //alert(rowCount);

            for(var i=1; i<rowCount; i++) {
                var row = table.rows[i];
                var chkbox = row.cells[7].childNodes[0];
                if(null != chkbox && true == chkbox.checked) {

                    if(rowCount <= 2) {
                        alert("Cannot delete all the rows.");
                        break;
                    }
                    table.deleteRow(i);
                    rowCount--;
                    i--;
                }

            }

                if(rowCount > 2){
                        document.jobCardClose.deleterowbutton.disabled = false;
                }else {
                        document.jobCardClose.deleterowbutton.disabled = true;
                }

            }catch(e) {
                alert(e);
            }
        }
        var rowCount=2;
          var sno=0;
          var style="text1";
          function addRow()
          {

               if(rowCount%2==0){
                   style="text2";
               }else{
                    style="text1";
               }
               sno++;
               var tab = document.getElementById("addRows");
               var newrow = tab.insertRow(rowCount);

               var cell = newrow.insertCell(0);
               var temp=sno-1;
               var cell0 = "<td class='text1' height='25' ><input type='hidden' name='selectedindex' value='"+sno+"'>"+sno+" </td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell0;

               
               
               cell = newrow.insertCell(1);
               cell1 = "<td class='text1' height='25'><select class='textbox' name='typeId'  id='typeId"+sno+"' ><option  selected value=0>---Select---</option><option value=1011>Issue</option><option value=1012>Return</option></select></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell1;

               cell = newrow.insertCell(2);
               var cell1 = "<td class='text1' height='25'><input id='mfrcode"+sno+"'   name='mfrcode'  size='10'  value='' type='text' onChange='callAjax(this.value,"+sno+");'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell1;


               cell = newrow.insertCell(3);
               var cell2 = "<td class='text1' height='25'><input  id='itemName"+sno+"' name='itemName' value='' readonly type='text'><input  id='itemId"+sno+"' name='itemId' value='' type='hidden'></td>";
               cell.setAttribute("className",style);
               //alert(cell2);
               cell.innerHTML = cell2;

               cell = newrow.insertCell(4);
               cell4 = "<td class='text1' height='25'><input  id='rate"+sno+"'  size='10' name='rate' readonly value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell4;

               cell = newrow.insertCell(5);
               cell4 = "<td class='text1' height='25'><input  id='tax"+sno+"'  size='10' name='tax' readonly value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell4;

               cell = newrow.insertCell(6);
               cell4 = "<td class='text1' height='25'><input  id='stock"+sno+"'  size='10' name='stock' readonly value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell4;

               cell = newrow.insertCell(7);
               var cell3 = "<td class='text1' height='25'><input  id='qty"+sno+"'  size='10' name='qty' onBlur ='computeValue(this.value,"+sno+");' value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell3;



               cell = newrow.insertCell(8);
               var cell5 = "<td class='text1' height='25'><input  id='amt"+sno+"'  size='10' name='amt' readonly value='' type='text'></td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell5;

               cell = newrow.insertCell(9);
               var cell6 = "<td class='text1' height='25' ><input type='checkbox' name='deleteInd' value='"+sno+"'   > </td>";
               cell.setAttribute("className",style);
               cell.innerHTML = cell6;

                var elementName = "typeId"+sno;
                document.getElementById(elementName).focus();
                //document.getElementById(elementName).select();

               rowCount++;
               document.jobCardClose.deleterowbutton.disabled = false;
               
          }

        function submitPage()
        {
            
            var selectedIndex=document.getElementsByName("selectedIndex");
            var status=document.getElementsByName("approve");           
            var cntr = 0;
            var temp;
            var c=0; 
            var s=0;

            for(var i=0;i<status.length;i++) {                                
                if(status[i].checked==true){
                    s++;                   
                }
            }                                       

             document.jobCardClose.action="saveSpareIssue.do";
             document.jobCardClose.submit();
            
        }
        



function computeValue(qty, sno){
          var elementName = "qty"+sno;
          var priceElement = "rate"+sno;
          var priceValue = document.getElementById(priceElement).value;
          if(qty == '' || qty == 0){
            alert("Please Enter Valid Data");            
            document.getElementById(elementName).select();
            document.getElementById(elementName).focus();
            return;
          }else {
                var typeName = "typeId"+sno;
                var typeId = document.getElementById(typeName).value;
                var retQty = document.jobCardClose.retQty.value;
                if(typeId != 1011) {
                    if( parseFloat(qty) > parseFloat(retQty) ){
                        alert("Returned Quantity Cannot be more than Issued Qty: Issued Qty is:"+retQty);
                        document.getElementById(elementName).select();
                        document.getElementById(elementName).focus();
                        return;
                    }else {
                        alert(document.getElementById("issueValue").innerHTML+":"+qty+":"+priceValue);
                        document.getElementById("issueValue").innerHTML = parseFloat(document.getElementById("issueValue").innerHTML)- (parseFloat(qty)*parseFloat(priceValue));
                    }
                }
                if(typeId == 1011) {
                    var stockElement = "stock"+sno;
                    var stockValue = document.getElementById(stockElement).value;
                    //alert(stockValue);
                    if( parseFloat(qty) > parseFloat(stockValue)){
                        alert("Issued Quantity Cannot be more than Stock Qty");
                        document.getElementById(elementName).select();
                        document.getElementById(elementName).focus();
                        return;
                    }else {
                        alert(document.getElementById("issueValue").innerHTML+":"+qty+":"+priceValue);
                        
                    document.getElementById("issueValue").innerHTML = parseFloat(document.getElementById("issueValue").innerHTML) + (parseFloat(qty)*parseFloat(priceValue));

                    }
                }
                
          }
            //alert(elementName);
            
            elementName = "rate"+sno;
            var rate = document.getElementById(elementName).value;
            var amountVal = parseFloat(rate)*parseFloat(qty);
            //alert(amountVal);
            elementName = "amt"+sno;
            document.getElementById(elementName).value=amountVal;

}
var httpReq;
function callAjax(itemcode, tempsno){

        var selectTypeId = "";
        var elementName = itemcode+sno;
        var sno = parseInt(tempsno);
          if( itemcode == '' ){
            alert("Please Enter Item Code");
            document.getElementById(elementName).focus();
            return;
            }
            elementName = "typeId"+sno;
            var typeId = document.getElementById(elementName).value;


            var jcno = document.jobCardClose.jobCardId.value;
       
       

        var url='/amw/getItemDetails.do?mfrcode='+itemcode+'&typeId='+typeId+'&jcno='+jcno;
        //alert(url);
        
            if (window.ActiveXObject){
                httpReq = new ActiveXObject("Microsoft.XMLHTTP");
            }
            else if (window.XMLHttpRequest){
                httpReq = new XMLHttpRequest();
            }
            httpReq.open("GET", url, true);
            httpReq.onreadystatechange = function() { processAjax(sno,typeId);};
            httpReq.send(null);
        

}
function processAjax(sno, typeId)
{
    if (httpReq.readyState == 4)
        {
            if(httpReq.status == 200)
                {
                    temp = httpReq.responseText.valueOf();
                    //alert(temp);
                    if(temp != "" && temp != null){
                    var itemInfo=temp.split('~');
                    var elementName = "itemName"+sno;
                    //alert(elementName);
                    document.getElementById(elementName).value=itemInfo[2];
                    elementName = "section"+sno;
                    

                    
                    elementName = "rate"+sno;                    
                    document.getElementById(elementName).value=itemInfo[3];

                    elementName = "amt"+sno;                    
                    document.getElementById(elementName).value=itemInfo[3];

                    elementName = "tax"+sno;
                    document.getElementById(elementName).value=itemInfo[4];

                    elementName = "stock"+sno;
                    document.getElementById(elementName).value=itemInfo[5];

                    elementName = "itemId"+sno;
                    document.getElementById(elementName).value=itemInfo[0];

                    elementName = "qty"+sno;
                    if(typeId != 1011) {
                        document.getElementById(elementName).value=itemInfo[6];
                        document.jobCardClose.retQty.value = itemInfo[6];
                    }else {
                        document.getElementById(elementName).value=1;
                        document.jobCardClose.retQty.value = itemInfo[5];
                    }

                    var elementName = "qty"+sno;
                    document.getElementById(elementName).select();
                }else {
                    alert("Item Code Does Not Exists");
                    elementName = "mfrcode"+sno;
                    document.getElementById(elementName).select();
                }
                }
                else
                    {
                        alert("Error loading page\n"+ httpReq.status +":"+ httpReq.statusText);
                    }
                }
}
function setValues(){
    document.jobCardClose.deleterowbutton.disabled = true;
}
    
    </script>
    
    <body onload="setValues();addRow();">
      <% int ifCloseReqd = 0; %>  
        <form name="jobCardClose" method="get">
            <!-- copy there from end -->

                    <table width="100%" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;" >
                        <tr>
                            <td >
                                <%@ include file="/content/common/path.jsp" %>
                    </td></tr></table>
                   
           

            <!-- message table -->
            <table width="850" cellpadding="0" cellspacing="0" border="0" align="center" style="margin-top:0px;">
                <tr>
                    <td >
                        <%@ include file="/content/common/message.jsp" %>
                    </td>
                </tr>
            </table>
            <!-- message table -->
<!-- copy there  end -->
   
            
            <c:if test = "${jcDetails != null}" >
            
            <c:forEach items="${jcDetails}" var="list">

		<table id="bg" align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border" >
		<tbody>
				<tr>
		    <td colspan="6" class="contentsub" height="30"><div class="contentsub">Job Card Details</div></td>
		</tr>
		<tr>
		<td class="text1" height="30"><b>Job Card Number</b></td>
		<td class="text1" height="30"><b>Vehicle No</b></td>
                <td class="text1" height="30"><b>MFG</b></td>
                <td class="text1" height="30"><b>Model</b></td>
		<td class="text1" height="30"><b>Customer</b></td>
		</tr>
		<tr>
                    
		
		<td class="text2" height="30"><c:out value="${list.mrsJobCardNumber}"/></td>
		<td class="text2" height="30"><c:out value="${list.mrsVehicleNumber}"/></td>
		<input type="hidden" name="jobCardId" value='<c:out value="${list.mrsJobCardNumber}"/>' />
		<input type="hidden" name="jcId" value='<c:out value="${list.mrsJobCardNumber}"/>' />
		<input type="hidden" name="retQty" value='0' />
		
		<td class="text2" height="30"><c:out value="${list.mrsVehicleMfr}"/></td>
		<td class="text2" height="30"><c:out value="${list.mrsVehicleModel}"/></td>
		<td class="text2" height="30"><c:out value="${list.customerName}"/></td>
		</tr>
		 </tbody></table>
		<br>
        <table width="90%" cellpadding="0" cellspacing="0" border="0" align="center" class="table5">
            <tr>
            <td class="bottom" width="5%" align="left"><img src="images/left_status.jpg" alt=""  /></td>
            <td  width="45%" align="right"><h2>Total Value Rs.</h2></td>
            <td  width="50%" align="left"><h2><div id="issueValue"><%=request.getAttribute("spareIssueValue")%></div></h2></td>
            </tr>

        </table>
        <br>
<div style="height:150px;overflow:auto;">
<table align="center" id="addRows" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
<tr>
<td colspan="10" align="left" class="text1" height="30"><strong>Spare Issue</strong></td>
</tr>

<tr>
<td height="30" class="contentsub"><div class="contentsub">S.No</div></td>
<td height="30" class="contentsub"><div class="contentsub">Type</div></td>
<td height="30" class="contentsub"><div class="contentsub">Item Code</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Item Name</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Price</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Tax</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Stock</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Quantity</div></td>
<td  height="30" class="contentsub"><div class="contentsub">Amount</div></td>
<td  height="30" class="contentsub"><div class="contentsub">select</div></td>

</tr>

</table>
</div>

<center>
<input type="button" value="AddRow" name="addrow" class="button" onclick="addRow();">
<input type="button" class="button" name="deleterowbutton" value="Delete"  onClick="deleteRow();" >
</center>
                    </c:forEach >
                    
                
            </c:if> 
            <center>
                <br>


           <% int index = 0; 
           	String classText = "";
           	int oddEven = 0;
           	
           %>  
<c:if test = "${issueDetails != null}" >



	    
		<br>
		<table align="center" border="0" cellpadding="0" cellspacing="0" width="90%" class="border">
		<tbody><tr>
		<td class="text1" align="left" colspan="9" height="30"><strong>Spares Issue / Return Details</strong></td>
		</tr>
		<tr height="30">
		<th class="contenthead" align="left"><div class="contenthead">SNo</div></th>
                <td height="30" class="contentsub"><div class="contentsub">Type</div></td>
                <td height="30" class="contentsub"><div class="contentsub">Item Code</div></td>
                <td  height="30" class="contentsub"><div class="contentsub">Item Name</div></td>
                <td  height="30" class="contentsub"><div class="contentsub">Quantity</div></td>
                <td  height="30" class="contentsub"><div class="contentsub">Rate</div></td>
                <td  height="30" class="contentsub"><div class="contentsub">Amount</div></td>
                <td  height="30" class="contentsub"><div class="contentsub">Issued By</div></td>
                <td  height="30" class="contentsub"><div class="contentsub">Issued On</div></td>
		</tr>                 

		<c:forEach items="${issueDetails}" var="spares">
                        <%
     oddEven=index%2;
            if (oddEven > 0) {
                classText = "text2";
            } else {
                classText = "text1";
            }
                        %>
                        <tr height="30">
                            <td class="<%=classText %>" height="30"><%=index + 1%></td>
                            <c:if test = "${spares.actionType == 1011}" >
                                <td class="<%=classText %>" height="30"><font color="green">Issue</font></td>
                            </c:if>
                            <c:if test = "${spares.actionType != 1011}" >
                                <td class="<%=classText %>" height="30"><font color="red">Return</font></td>
                            </c:if>
                            <td class="<%=classText %>" height="30"><c:out value="${spares.mfrCode}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${spares.itemName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${spares.mrsIssueQuantity}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${spares.price}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${spares.nettAmount}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${spares.userName}"/></td>
                            <td class="<%=classText %>" height="30"><c:out value="${spares.date}"/></td>
                            
                        </tr>
                        <%
            index++;
                        %>
                    </c:forEach >
                    
                </table>
                
            
            
     

</c:if>   


  <br>
 <input class="button" type="button" value="Save" onclick="submitPage()">
 

                
            </center>

            <br>
        </form>
    </body>
</html>
