
<%@ include file="/content/common/NewDesign/header.jsp" %>
<%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%@page import="ets.domain.users.web.CryptoLibrary"%>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ page import="ets.domain.employee.business.EmployeeTO" %>
<%@ page import=" java. util. * "%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page import=" javax. servlet. http. HttpServletRequest" %>


<!--<title>PAPL</title>-->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>

<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css"/>
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen"/>


<script type="text/javascript" src="/throttle/js/autosuggest.js"></script>
<script type="text/javascript" src="/throttle/js/suggestions.js"></script>
<link rel="stylesheet" type="text/css" href="/throttle/css/autosuggest.css" />

<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.4.2.min.js"></script>
<script type="text/javascript" src="/throttle/js/jquery-1.3.2.min.js"></script>
<!--<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">-->
<script src="/throttle/js/jquery.ui.core.js"></script>
<!--<script src="/throttle/js/jquery.ui.datepicker.js"></script>-->


   <div class="pageheader">
    <h2><i class="fa fa-edit"></i><spring:message code="finance.label.AddBankMaster"  text="default text"/></h2>
    <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="head.label.Youarehere"  text="default text"/>:</span>
        <ol class="breadcrumb">
            <li><a href="index.html"><spring:message code="head.label.Home"  text="default text"/></a></li>
            <li><a href="general-forms.html"><spring:message code="finance.label.Finance"  text="default text"/></a></li>
            <li class="active"><spring:message code="finance.label.AddBankMaster"  text="default text"/></li>
        </ol>
    </div>
</div>
<div class="contentpanel">
    <div class="panel panel-default">
        <div class="panel-body"> 
    <script>
        function submitPage()
        {

            if (textValidation(document.add.bankName, 'bankName')) {
                return;
            }
            if (textValidation(document.add.bankCode, 'bankCode')) {
                return;
            }
            
            if($("#bankNameStatus").text() == ''){
            document.add.action = '/throttle/savePrimaryBank.do';
            document.add.submit();
            }else {
                alert("Bank Name is Already Exists");
                $("#bankName").focus();
            }
        }
        function setFocus() {
                $("#bankName").focus();
        }


        var httpRequest;
        function checkBankAccountName() {
            var bankName = document.getElementById('bankName').value;            
            if (bankName != '' ) {
                var url = '/throttle/checkBankAccountName.do?bankName='+bankName;
                if (window.ActiveXObject) {
                    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } else if (window.XMLHttpRequest) {
                    httpRequest = new XMLHttpRequest();
                }
                httpRequest.open("GET", url, true);
                httpRequest.onreadystatechange = function() {
                    processRequest();
                };
                httpRequest.send(null);
            }
        }


        function processRequest() {
            if (httpRequest.readyState == 4) {
                if (httpRequest.status == 200) {
                    var val = httpRequest.responseText.valueOf();                    
                    if (val != 'null') {                    
                        $("#bankNameStatus").text('Bank Name is Already  Exists :' + val);
                        document.getElementById('bankName').value="";
                        document.getElementById('bankName').focus();
                    } else {
                        $("#bankNameStatus").text('');
                    }
                } else {
                    alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
                }
            }
        }

    </script>
    <style>
    #index td {
   color:white;
}
</style>
    <body onload="setFocus();">
        <form name="add" method="post" class="form-horizontal form-bordered">
 
              <table class="table table-info mb30 table-hover">
                <tr height="30">
                    <tr height="30" id="index">
                             <td colspan="4"  style="background-color:#5BC0DE;"><b><spring:message code="finance.label.AddBank"  text="default text"/></b></td>&nbsp;&nbsp;&nbsp;                    
                    </tr>
<!--                    <font color="red" style="font-family:Arial, Helvetica, sans-serif; font-size:12px; font-weight:bold; ">
                    </font>-->
                </tr>
                <tr height="30">
                    <td ><font color="red">*</font><spring:message code="finance.label.BankName"  text="default text"/></td>
                    <td ><input name="bankName" id="bankName" type="text" class="form-control" value="" style="width:260px;height:40px;" onchange="checkBankAccountName();" ></td>
<!--                </tr>
                <tr height="30">-->
                    <td ><font color="red">*</font><spring:message code="finance.label.BankCode"  text="default text"/></td>
                    <td ><input name="bankCode" type="text" class="form-control" value="" style="width:260px;height:40px;"  ></td>
                </tr>
                 <tr height="30">
                    <td ><font color="red">*</font><spring:message code="finance.label.Status"  text="default text"/></td>
                    <td >
                        <select name="active_ind" class="form-control" style="width:260px;height:40px;">                            
                            <option value="Y"><spring:message code="finance.label.Active"  text="default text"/></option>
                            <option value="N"><spring:message code="finance.label.InActive"  text="default text"/></option>
                        </select>
                    </td>
                    <td></td>
                    <td></td>
                    <!--<td></td>-->
                </tr>

            </table>
                <center>
                <input type="button"  value="<spring:message code="finance.label.Save"  text="default text"/>" onclick="submitPage();" class="btn btn-success" style="width:100px;height:35px;"/>
                &emsp;<input type="reset"  value="<spring:message code="finance.label.Clear"  text="default text"/>" class="btn btn-success" style="width:100px;height:35px;">
            </center>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </body>
 
      </div>
    </div>
</div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
