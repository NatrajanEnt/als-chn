<%--
    Document   : standardChargesMaster
    Created on : Oct 29, 2013, 11:32:08 AM
    Author     : srinivasan
--%>
<%@ include file="/content/common/NewDesign/header.jsp" %>
    <%@ include file="/content/common/NewDesign/sidemenu.jsp" %>
<%--<%@page contentType="text/html" pageEncoding="UTF-8"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/throttle/css/dhtmlgoodies_calendar.css" rel="stylesheet" type="text/css" media="screen">
<script type="text/javascript" language="javascript" src="/throttle/js/dhtmlgoodies_calendar.js"></script>
<link href="/throttle/css/parveen.css" rel="stylesheet" type="text/css">
<script language="javascript" src="/throttle/js/validate.js"></script>
<script type="text/javascript" src="/throttle/js/TableSort.js"></script>
<link rel="stylesheet" href="/throttle/css/filtergrid.css"  type="text/css" />
<script language="javascript" type="text/javascript" src="/throttle/js/actb.js"></script>
<script language="javascript" type="text/javascript" src="/throttle/js/tablefilter.js"></script>
<style type="text/css" title="currentStyle">
    @import "/throttle/css/layout-styles.css";
    @import "/throttle/css/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />

<!-- jQuery libs -->
<script  type="text/javascript" src="/throttle/js/jquery-1.6.1.min.js"></script>
<script  type="text/javascript" src="/throttle/js/jquery-ui-1.8.14.custom.min.js"></script>

<!-- Our jQuery Script to make everything work -->
<link rel="stylesheet" href="/throttle/css/jquery.ui.datepicker.css">
<script src="/throttle/js/jquery.ui.core.js"></script>
<script src="/throttle/js/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $("#datepicker").datepicker({
            showOn: "button",
            buttonImage: "calendar.gif",
            buttonImageOnly: true

        });



    });

    $(function() {
        //	alert("cv");
        $(".datepicker").datepicker({
            /*altField: "#alternate",
             altFormat: "DD, d MM, yy"*/
            changeMonth: true, changeYear: true
        });

    });
</script>
<script>

    var httpRequest;
    function checkDriverMapped(driverId) {
         $("#tripStatusShow").hide();
        if (driverId != '') {
            var driverIds = document.getElementsByName("driverIds");
            var statusNames = document.getElementsByName("statusNames");
            for(var i=0; i < driverIds.length; i++){
                if(driverIds[i].value == driverId){
                    var status = "This Driver is already marked as in "+ statusNames[i].value +". please see in the below list.";
                    $("#tripStatusShow").show();
                    $("#tripStatus").text(status);
                    $('#driverName').val('');
                    $('#driverId').val(0);
                    $('#driverName').focus();
                    document.getElementById("endDate").readOnly = true;
                    $("#endDate").removeClass('datepicker');
                    $("#startDate").addClass('datepicker');                    
                    document.getElementById("mappingId").value ='';
                    document.getElementById("startDate").readOnly = false;
                }
            }
            
        }
    }
    function processRequest() {
        if (httpRequest.readyState == 4) {
            if (httpRequest.status == 200) {
                var val = httpRequest.responseText.valueOf();
                //alert(val.trim());
                if (val.trim() !== "") {
                    var temp = val.split("~");
                    document.getElementById("mappingId").value = temp[0];
                    document.getElementById("primaryDriverId").value = temp[1];
                    document.getElementById("primaryDriver").value = temp[2];
                    document.getElementById("secondaryDriverIdOne").value = temp[3];
                    document.getElementById("secondaryDriverOne").value = temp[4];
                    document.getElementById("secondaryDriverIdTwo").value = temp[5];
                    document.getElementById("secondaryDriverTwo").value = temp[6];
                    if(temp[7] == '0'){
                            $("#tripStatus").text("");
                            $("#tripStatusShow").hide();
                        }else if(temp[7] != '0'){
                            //alert(temp[7]);
                            $("#tripStatusShow").show();
                            if(temp[8] == 8){
                            //alert(temp[8]);
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (pre-start status) and trip sheet no is '+temp[7]);
                            }else if(temp[8] == 9){
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (start status) and trip sheet no is '+temp[7]);
                            }else if(temp[8] == 10){
                            $("#tripStatus").text('Now Vehicle is assigned to a trip (in progress status) and trip sheet no is '+temp[7]);
                            }
                        }
                } else {
                    document.getElementById("mappingId").value = 0;
                    document.getElementById("primaryDriverId").value = '';
                    document.getElementById("primaryDriver").value = '';
                    document.getElementById("secondaryDriverIdOne").value = '';
                    document.getElementById("secondaryDriverOne").value = '';
                    document.getElementById("secondaryDriverIdTwo").value = '';
                    document.getElementById("secondaryDriverTwo").value = '';
                    $("#tripStatus").text("");
                }
            } else {
                alert("Error loading page\n" + httpRequest.status + ":" + httpRequest.statusText);
            }
        }
    }


    $(document).ready(function() {
        // Use the .autocomplete() method to compile the list based on input from user
        $('#driverName').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: "/throttle/getDriverName.do",
                    dataType: "json",
                    data: {
                        driverName: request.term
                    },
                    success: function(data, textStatus, jqXHR) {
                        var items = data;
                        if(items == ''){
                        alert("Invalid Driver");
                        $('#driverName').val('');
                        $('#driverId').val('');    
                        $('#driverName').fous();
                        }else{
                        response(items);
                        }
                    },
                    error: function(data, type) {
                        console.log(type);
                    }
                });
            },
            minLength: 1,
            select: function(event, ui) {
                var value = ui.item.Name;
                var id = ui.item.Id;
                $('#driverName').val(value);
                $('#driverId').val(id);
                 
                 $("#endDate").removeClass('datepicker');
                $("#startDate").addClass('datepicker');
                document.getElementById("mappingId").value ='';
                checkDriverMapped(id);
                
                return false;
            }
        }).data("ui-autocomplete")._renderItem = function(ul, item) {
            var itemVal = item.Name;
            itemVal = '<font color="green">' + itemVal + '</font>';
            return $("<li></li>")
                    .data("item.autocomplete", item)
                    .append("<a>" + itemVal + "</a>")
                    .appendTo(ul);
        };
    });

    //Update Vehicle Planning
    function setValues(mappingId, driverId, driverName, startDate, endDate, remarks, status,statusId,sno) {
        var statusName = statusId.split("~");
        var count = parseInt(document.getElementById("count").value);
        //document.getElementById('inActive').style.display = 'block';
        for (i = 1; i <= count; i++) {
            if (i != sno) {
                document.getElementById("edit" + i).checked = false;
            } else {
                document.getElementById("edit" + i).checked = true;
            }
        }
        document.getElementById("mappingId").value = mappingId;
        document.getElementById("driverId").value = driverId;
        document.getElementById("driverName").value = driverName;
        document.getElementById("driverName").readOnly = true;
        document.getElementById("startDate").value = startDate;
        document.getElementById("statusId").value = statusId;
        document.getElementById("endSpan1").style.display = "block";
        document.getElementById("endDate").style.display = "block";
        document.getElementById("endDate").value = '';
        document.getElementById("endDate").readOnly = false;
        $("#startDate").removeClass('datepicker');
        $("#endDate").addClass('datepicker');
        document.getElementById("stDateSpan1").style.display = "none";
        document.getElementById("startDateTmp").style.display = "block";
        document.getElementById("statusSpan1").style.display = "none";
        document.getElementById("statusName").style.display = "block";
        document.getElementById("vehSpan1").style.display = "block";
        document.getElementById("vehicleId").style.display = "block";
        document.getElementById("startDateTmp").value = startDate;
        document.getElementById("statusName").value = statusName[1];
        document.getElementById("statusName").readOnly = true;
        document.getElementById("startDate").readOnly = true;
        document.getElementById("remarks").value = remarks;
        setVehicle();
    }

    //savefunction
    function submitPage() {
        if (document.getElementById("driverName").value == '' || document.getElementById("driverId").value == '') {
            alert("Please Select Valid Driver");
            document.getElementById("driverName").focus();
         } else if (document.getElementById("startDate").value == '' ) {
            alert("Please Enter Start Date");
            document.getElementById("startDate").focus();
        } else if (document.getElementById("endDate").value == ''  && document.getElementById("mappingId").value != ''  ) {
            alert("Please Enter Joining Date");
            document.getElementById("endDate").focus();
        } else if (document.getElementById("endDate").value < document.getElementById("startDate").value  && document.getElementById("mappingId").value != ''  ) {
            alert("End Date should not be lesser than Start date");
            document.getElementById("endDate").value = "";
            document.getElementById("endDate").focus();
        } else if (document.getElementById("vehicleId").value == "0"  && document.getElementById("mappingId").value != ''  ) {
            alert("Please Select Vehicle No");
            document.getElementById("vehicleId").focus();
        } else if (document.getElementById("remarks").value == '' ) {
            alert("Please enter Remarks");
            document.getElementById("remarks").focus();
        } else {
            $("#save").hide();
            document.vehicleDriverPlanning.action = '/throttle/saveDriverRegisterNote.do';
            document.vehicleDriverPlanning.submit();
        }
    }
    
    
    
</script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <div class="pageheader">
      <h2><i class="fa fa-edit"></i> <h2><spring:message code="operations.label.nodrivermanage"  text="Manage Driver Register"/></h2>
      <div class="breadcrumb-wrapper">
        <span class="label"><spring:message code="general.label.youAreHere"  text="You are here"/>:</span>
        <ol class="breadcrumb">
          <li><a href="index.html"><spring:message code="general.label.home"  text="Home"/></a></li>
          <li><a href="general-forms.html"><spring:message code="operations.label.DailyOperation"  text="default text"/></a></li>
          <li class="active"><spring:message code="operations.label.Manage Driver Register"  text="Manage Driver Register"/></li>
        </ol>
      </div>
      </div>
 <div class="contentpanel">
<div class="panel panel-default">
 <div class="panel-body">
    <body onload="document.vehicleDriverPlanning.vehicleNo.focus()">
        <form name="vehicleDriverPlanning"  method="post" >
           
            
            <%@ include file="/content/common/message.jsp" %>
            
            

           <table class="table table-info mb30 table-hover" >
                <input type="hidden" name="stChargeId" id="stChargeId" value=""  />
                <thead>
                <tr>
                    <th  colspan="4" ><spring:message code="operations.label.Driver Register Note"  text="Driver Register Note"/></th>
                </tr>
                </thead>
                <tr>
                    <td  colspan="4" align="center" id="tripStatusShow" style="display: none;"><label id="tripStatus" style="color: red"></label></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.Driver Name"  text="Driver Name"/></td>
                    <td ><input type="hidden" name="mappingId" id="mappingId" value="" class="textbox"  >
                        <input type="hidden" name="driverId" id="driverId" class="textbox"  >
                        <input type="text" name="driverName" id="driverName" style="width:260px;height:40px;"  class="form-control" ></td>
                    <td></td> 
                    <td></td> 
                </tr>
                
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.startDate"  text="Start Date"/></td>
                    <td id="stDateSpan1"><input type="text"  autocomplete="off" value=""  name="startDate" id="startDate" style="width:260px;height:40px;"  class="form-control datepicker" /></td>
                    <td>
                        <input  type="text" name="startDateTmp" id="startDateTmp" readonly style="width:260px;height:40px;display:none"  class="form-control" value="" >
                    </td>
                    <td style="display:none" id="endSpan1">&nbsp;&nbsp;<font color="red" >*</font><spring:message code="operations.label.endDate"  text="Joining Date"/></td>
                    <td><input type="text" autocomplete="off"  value=""  name="endDate" readonly id="endDate" style="width:260px;height:40px;display:none"  class="form-control datepicker" /></td>
                </tr>
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.Status"  text="Status"/></td>
                            <td id="statusSpan1">
                                <select type="text" name="statusId" id="statusId" style="width:260px;height:40px;"  class="form-control" onChange="setVehicle();">
                                    <option value="0">--select--</option>
                                    <!--<option value="1~On-Duty">On-Duty</option>-->
                                    <option value="2~Rest">Rest</option>
                                    <option value="3~DL Renewal">DL Renewal</option>
                                    <option value="4~Releived">Relieved</option>
                                </select>
                            </td>                                 
                            <td>
                                <input  type="text" name="statusName" id="statusName" style="width:260px;height:40px;display:none"  class="form-control" value="" >
                            </td>                                 
                    <td id="vehSpan1" style="display:none">&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.Vehicle"  text="Vehicle"/></td>
                    <td>
                        <select type="text" name="vehicleId"  id="vehicleId" style="width:260px;height:40px;display:none"  class="form-control" >
                            <option value="0">--select--</option>
                        </select>
                    </td>                                 
                </tr>
                
                <tr>
                    <td >&nbsp;&nbsp;<font color="red">*</font><spring:message code="operations.label.remarks"  text="Remarks"/></td>
                    <td ><textarea rows="3" cols="100" class="form-control" name="remarks" id="remarks"   style="width:260px"></textarea></td>
                    <td></td> 
                    <td></td> 
                </tr>
                <tr>
                    <td  colspan="4" align="center"><input type="button" class="btn btn-success" value="<spring:message code="operations.label.SAVE"  text="default text"/>" id="save" onClick="submitPage();"   />
                    <!--&nbsp;&nbsp;   <input type="button"   value="<spring:message code="hrms.label.Export Excel" text="Export Excel"/>" class="btn btn-success" id="ExcelExport" name="ExcelExport" onClick="exportexcel(this.name);" style="width:150px">-->
                    </td>
                </tr>
                
                <script>
                    function setVehicle(){
//                        if($("#statusId").val()== '1~On-Duty'){
                            $.ajax({
                            url: "/throttle/getDriverMappedVehicle.do",
                            dataType: "json",
                            data: {
                                driverId: $("#driverId").val(),
                                statusId: $("#statusId").val()
                            },
                            success: function (data) {
//                                 alert(data);
                                if (data != '') {
                                    $('#vehicleId').empty();
                                    $('#vehicleId').append(
                                            $('<option></option>').val(0).html('--select--'))
                                    $.each(data, function (i, data) {
                                        $('#vehicleId').append(
                                                $('<option style="width:90px" ></option>').attr("value", data.Id).text(data.Name).attr("style", "text-color:red")
                                                )
                                    });
                                } else {
                                    $('#vehicleId').empty();
                                    $('#vehicleId').append(
                                            $('<option></option>').val(0).html('--select--'))
                                }
                            }
                        });

                }
                    
                    
                         function exportexcel(value){
                            if (value == 'ExcelExport') {
                             document.vehicleDriverPlanning.action = "/throttle/vehicleDriverPlanning.do?param=" + value;
                             document.vehicleDriverPlanning.submit();

                         }
                         }
       
                    function setPrimaryDriverEmpty(){
                       var primaryDriver = $("#primaryDriver").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(primaryDriver == ''){
                           $("#primaryDriverId").val('');
                       }
                    }
                    function setSecondaryDriverEmpty(){
                       var secondaryDriverOne = $("#secondaryDriverOne").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(secondaryDriverOne == ''){
                           $("#secondaryDriverIdOne").val('');
                       }
                    }
                    function setTertiaryDriverEmpty(){
                       var secondaryDriverTwo = $("#secondaryDriverTwo").val(); 
                       var mappingId = $("#mappingId").val(); 
                       if(secondaryDriverTwo == ''){
                           $("#secondaryDriverIdTwo").val('');
                       }
                    }
                </script>
            </table>
            
            
           <table class="table table-info mb30 table-hover" id="table">
                <thead>
                    <tr height="30">
                        <th><spring:message code="operations.label.SNo"  text="default text"/></th>
                        <th><spring:message code="operations.label.Driver"  text="Driver"/> </th>
                        <th><spring:message code="operations.label.MobileNo"  text="MobileNo"/> </th>
                        <th><spring:message code="operations.label.DrivingLicenseNo"  text="DrivingLicenseNo"/> </th>
                        <th><spring:message code="operations.label.startDate"  text="StartDate"/></th>
                        <th><spring:message code="operations.label.endDate"  text="End Date"/></th>
                        <th><spring:message code="operations.label.Status"  text="Status"/></th>
                        <th><spring:message code="operations.label.remarks"  text="Remarks"/></th>
                        <th><spring:message code="operations.label.Select"  text="default text"/></th>
                    </tr>
                </thead>
                <tbody>
                    <% int sno = 0;%>
                    <c:if test = "${driverRegisterDetails != null}">
                        <c:forEach items="${driverRegisterDetails}" var="vehicleDriver">
                            <%
                                        sno++;
                                        String className = "text1";
                                        if ((sno % 1) == 0) {
                                            className = "text1";
                                        } else {
                                            className = "text2";
                                        }
                            %>

                            <tr>
                                <td   align="left"> <%= sno + 1%> </td>
                                <td   align="left">
                                    <input type="hidden" name="statusNames" id="statusNames" value="<c:out value="${vehicleDriver.statusName}" />"/>
                                    <input type="hidden" name="driverIds" id="dri0verIds" value="<c:out value="${vehicleDriver.primaryDriverId}" />"/> <c:out value="${vehicleDriver.primaryDriverName}" /></td>
                                 <td   align="left"><c:out value="${vehicleDriver.mobileNo}" /></td>
                                 <td   align="left"><c:out value="${vehicleDriver.drivingLicenseNo}" /></td>
                                 <td   align="left"><c:out value="${vehicleDriver.tripStartDate}" /></td>
                                 <td   align="left"><c:out value="${vehicleDriver.endDate}" /></td>
                                 <td   align="left"><c:out value="${vehicleDriver.statusName}" /></td>
                                 <td   align="left"><c:out value="${vehicleDriver.remarks}" /></td>
                                <td > <input type="checkbox" id="edit<%=sno%>" onclick="setValues('<c:out value="${vehicleDriver.mappingId}" />', '<c:out value="${vehicleDriver.primaryDriverId}" />', '<c:out value="${vehicleDriver.primaryDriverName}" />', '<c:out value="${vehicleDriver.tripStartDate}" />', '<c:out value="${vehicleDriver.endDate}" />', '<c:out value="${vehicleDriver.remarks}" />',  '<c:out value="${vehicleDriver.status}" />','<c:out value="${vehicleDriver.statusId}" />~<c:out value="${vehicleDriver.statusName}" />','<%=sno%>');" /></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </c:if>
            </table>
            <input type="hidden" name="count" id="count" value="<%=sno%>" />
            
            
            <script language="javascript" type="text/javascript">
                setFilterGrid("table");</script>
            <div id="controls">
                <div id="perpage">
                    <select onchange="sorter.size(this.value)">
                        <option value="5" selected="selected">5</option>
                        <option value="10">10</option>
                        <option value="20">20</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                    <span><spring:message code="operations.label.EntriesPerPage"  text="default text"/></span>
                </div>
                <div id="navigation">
                    <img src="images/Previous2.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1, true)" />
                    <img src="images/previous.gif" width="16" height="16" alt="First Page" onclick="sorter.move(-1)" />
                    <img src="images/Next.gif" width="16" height="16" alt="First Page" onclick="sorter.move(1)" />
                    <img src="images/Next2.gif" width="16" height="16" alt="Last Page" onclick="sorter.move(1, true)" />
                </div>
                <div id="text"><spring:message code="operations.label.DisplayingPage"  text="default text"/> <span id="currentpage"></span> <spring:message code="operations.label.of"  text="default text"/> <span id="pagelimit"></span></div>
            </div>
            <script type="text/javascript">
                var sorter = new TINY.table.sorter("sorter");
                sorter.head = "head";
                sorter.asc = "asc";
                sorter.desc = "desc";
                sorter.even = "evenrow";
                sorter.odd = "oddrow";
                sorter.evensel = "evenselected";
                sorter.oddsel = "oddselected";
                sorter.paginate = true;
                sorter.currentid = "currentpage";
                sorter.limitid = "pagelimit";
                sorter.init("table", 1);
            </script>
        <%@ include file="/content/common/NewDesign/commonParameters.jsp" %></form>
    </div>
      </div>
      </div>
<%@ include file="/content/common/NewDesign/settings.jsp" %>
